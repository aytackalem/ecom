﻿using Helpy.Shared.Response;

namespace Helpy.Shared.Http.Base
{
    public interface IHttpHelper
    {
        #region Methods
        Task<HttpResponse<TContent>> SendAsync<TContent>(HttpRequest request);

        Task<HttpResponse<TContent>> SendAsync<TRequest, TContent>(HttpRequest<TRequest> request);

        Task<HttpResponse<TOkContent, TBadRequestContent>> SendAsync<TRequest, TOkContent, TBadRequestContent>(HttpRequest<TRequest> request);

        Task<HttpResponse> SendAsync<TRequest>(HttpRequest<TRequest> request);

        Task<HttpResponse<TContent>> SendAsync<TContent>(HttpJsonFileRequest request);

        Task<HttpResponse<TContent>> SendAsync<TContent>(HttpFormUrlEncodedRequest request);

        Task<HttpResponse<TContent>> SendAsync<TContent>(HttpSoapRequest request);
        #endregion
    }
}
