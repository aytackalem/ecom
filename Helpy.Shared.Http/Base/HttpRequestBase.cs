﻿namespace Helpy.Shared.Http.Base
{
    public abstract class HttpRequestBase
    {
        #region Fields
        private int _tryCount = 1;

        private int _tryWaitSeconds = 1;

        private string _cacheKey = string.Empty;
        #endregion

        #region Properties
        public int TryCount { get => this._tryCount; set => this._tryCount = value; }

        public int TryWaitSeconds { get => this._tryWaitSeconds; set => this._tryWaitSeconds = value; }

        public bool UseCache { get; set; }

        public string CacheKey
        {
            get
            {
                if (this._cacheKey == string.Empty)
                    this._cacheKey = RequestUri.Replace(@"/", "").Replace(":", "").Replace("?", "").Replace("*", "");

                return this._cacheKey;
            }
            set => this._cacheKey = value;
        }

        public string RequestUri { get; set; }

        public HttpMethod HttpMethod { get; set; }

        public AuthorizationType? AuthorizationType { get; set; }

        public string Authorization { get; set; }

        public string Agent { get; set; }

        public string Accept { get; set; }

        public Dictionary<string,string> Headers { get; set; }

        public int Timeout { get; set; } = 0;
        #endregion
    }
}
