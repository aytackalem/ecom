﻿using Helpy.Shared.Http.Base;
using Microsoft.Extensions.DependencyInjection;

namespace Helpy.Shared.Http.Extensions
{
    public static class ServiceRegistration
    {
        #region Methods
        public static IServiceCollection AddHttpHelper(this IServiceCollection services, ServiceLifetime serviceLifetime)
        {
            services.Add(new ServiceDescriptor(typeof(IHttpHelper), typeof(HttpHelper), serviceLifetime));

            return services;
        } 
        #endregion
    }
}
