﻿namespace Helpy.Shared.Http
{
    public enum AuthorizationType
    {
        Basic,
        XApiKey,
        Bearer
    }
}
