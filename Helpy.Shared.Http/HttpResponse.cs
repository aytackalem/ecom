﻿using Helpy.Shared.Response.Base;
using System.Net;

namespace Helpy.Shared.Http
{
    public class HttpResponse : ResponseBase
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }
        #endregion
    }

    public class HttpResponse<TContent> : ResponseBase
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }

        public TContent Content { get; set; }
        #endregion
    }

    public class HttpResponse<TOkContent, TBadRequestContent> : ResponseBase
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }

        public TOkContent OkContent { get; set; }

        public TBadRequestContent BadRequestContent { get; set; }
        #endregion
    }
}