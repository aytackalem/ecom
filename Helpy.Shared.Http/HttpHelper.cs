﻿using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http.Base;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Helpy.Shared.Http
{
    public class HttpHelper : IHttpHelper
    {
        #region Fields
        private readonly string _cacheDirectory = @"C:\HttpHelperCache";
        #endregion

        #region Methods
        public async Task<HttpResponse<TContent>> SendAsync<TContent>(HttpRequest request)
        {
            return await ExceptionHandler.HandleTryAsync<HttpResponse<TContent>>(async response =>
            {
                HttpStatusCode? httpStatusCode = null;
                string responseJson = null;

                var path = @$"{this._cacheDirectory}\{request.CacheKey}";
                if (request.UseCache && File.Exists(path))
                {
                    httpStatusCode = HttpStatusCode.OK;
                    responseJson = File.ReadAllText(path);
                }
                else
                    await this.Template(request, async httpClient =>
                    {
                        var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri));
                        httpStatusCode = httpResponseMessage.StatusCode;
                        responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                        {
                            if (request.UseCache)
                            {
                                Directory.CreateDirectory(this._cacheDirectory);
                                File.WriteAllText(@$"{this._cacheDirectory}\{request.CacheKey}", responseJson);
                            }
                        }
                    });

                if (httpStatusCode.HasValue && httpStatusCode.Value == HttpStatusCode.OK)
                {
                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
                    response.Success = true;
                    response.HttpStatusCode = httpStatusCode.Value;
                }
            }, request.TryCount, request.TryWaitSeconds);
        }

        public async Task<HttpResponse<TContent>> SendAsync<TRequest, TContent>(HttpRequest<TRequest> request)
        {
            return await ExceptionHandler.HandleTryAsync<HttpResponse<TContent>>(async response =>
            {
                await this.Template(request, async httpClient =>
                {
                    if (request.Timeout > 0)
                        httpClient.Timeout = TimeSpan.FromSeconds(request.Timeout);

                    Directory.CreateDirectory(@"C:\Logs\Helpy\Request");
                    Directory.CreateDirectory(@"C:\Logs\Helpy\Response");

                    Guid requestId = Guid.NewGuid();

                    var requestJson = JsonConvert.SerializeObject(request.Request);

                    File.WriteAllText(@$"C:\Logs\Helpy\Request\{requestId}.txt", requestJson);

                    var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                    {
                        Content = new StringContent(
                                requestJson,
                                Encoding.UTF8,
                                "application/json")
                    });

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                    File.WriteAllText(@$"C:\Logs\Helpy\Response\{requestId}.txt", responseJson);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                        response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);

                    response.Success = true;
                });
            }, request.TryCount, request.TryWaitSeconds);
        }

        public async Task<HttpResponse<TOkContent, TBadRequestContent>> SendAsync<TRequest, TOkContent, TBadRequestContent>(HttpRequest<TRequest> request)
        {
            HttpResponse<TOkContent, TBadRequestContent> response = new();
            await this.Template(request, async httpClient =>
            {
                var requestJson = JsonConvert.SerializeObject(request.Request);
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new StringContent(
                            requestJson,
                            Encoding.UTF8,
                            "application/json")
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.IsSuccessStatusCode)
                    response.OkContent = JsonConvert.DeserializeObject<TOkContent>(responseJson);
                else if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    response.BadRequestContent = JsonConvert.DeserializeObject<TBadRequestContent>(responseJson);
            });
            return response;
        }

        public async Task<HttpResponse> SendAsync<TRequest>(HttpRequest<TRequest> request)
        {
            HttpResponse response = new();
            await this.Template(request, async httpClient =>
            {
                var requestJson = JsonConvert.SerializeObject(request.Request);
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new StringContent(
                            requestJson,
                            Encoding.UTF8,
                            "application/json")
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();
            });
            return response;
        }

        public async Task<HttpResponse<TContent>> SendAsync<TContent>(HttpJsonFileRequest request)
        {
            HttpResponse<TContent> response = new();
            await this.Template(request, async httpClient =>
            {
                using (MultipartFormDataContent content = new("------" + DateTime.Now.Ticks.ToString("x")))
                {
                    StreamContent streamContent = new(GenerateStreamFromString(request.Request));

                    content.Add(streamContent, "file", request.FileName);
                    content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        FileName = request.FileName,
                        Name = "file"
                    };

                    var httpRequestMessage = new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                    {
                        Content = content
                    };
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

                    string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
                    response.HttpStatusCode = httpResponseMessage.StatusCode;
                }
            });
            return response;
        }

        public async Task<HttpResponse<TContent>> SendAsync<TContent>(HttpFormUrlEncodedRequest request)
        {
            HttpResponse<TContent> response = new();
            await this.Template(request, async httpClient =>
            {
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new FormUrlEncodedContent(request.Data)
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.IsSuccessStatusCode)
                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
            });
            return response;
        }

        public async Task<HttpResponse<TContent>> SendAsync<TContent>(HttpSoapRequest request)
        {
            return await ExceptionHandler.HandleTryAsync<HttpResponse<TContent>>(async response =>
            {
                HttpStatusCode? httpStatusCode = null;
                string responseXml = null;

                var path = @$"{this._cacheDirectory}\{request.CacheKey}";
                if (request.UseCache && File.Exists(path))
                {
                    httpStatusCode = HttpStatusCode.OK;
                    responseXml = File.ReadAllText(path);

                    Console.WriteLine("From cache");
                }
                else
                    await this.Template(request, async httpClient =>
                    {
                        var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                        {
                            Content = new StringContent(request.Request, Encoding.UTF8, "text/xml")
                        });
                        httpStatusCode = httpResponseMessage.StatusCode;
                        responseXml = await httpResponseMessage.Content.ReadAsStringAsync();

                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                        {
                            if (request.UseCache)
                            {
                                Directory.CreateDirectory(this._cacheDirectory);
                                File.WriteAllText(@$"{this._cacheDirectory}\{request.CacheKey}", responseXml);
                            }
                        }
                        else
                        {

                        }
                    });

                if (httpStatusCode.HasValue && httpStatusCode.Value == HttpStatusCode.OK)
                {
                    XmlSerializer serializer = new(typeof(TContent));
                    using (XmlReader reader = XmlReader.Create(new StringReader(responseXml)))
                    {
                        response.Content = (TContent)serializer.Deserialize(reader);
                        response.Success = true;
                    }
                }
            }, request.TryCount, request.TryWaitSeconds);
        }
        #endregion

        #region Helper Methods
        private async Task Template(HttpRequestBase request, Func<HttpClient, Task> func)
        {
            using (var httpClient = new HttpClient())
            {
                if (request.AuthorizationType.HasValue)
                    switch (request.AuthorizationType.Value)
                    {
                        case AuthorizationType.Basic:
                            httpClient.DefaultRequestHeaders.Authorization = new(request.AuthorizationType.ToString(),
                                                                                 request.Authorization);
                            break;
                        case AuthorizationType.Bearer:
                            httpClient.DefaultRequestHeaders.Authorization = new(request.AuthorizationType.ToString(),
                                                                                 request.Authorization);
                            break;
                        case AuthorizationType.XApiKey:
                            httpClient.DefaultRequestHeaders.Add("x-api-key", request.Authorization);
                            break;
                    }


                if (string.IsNullOrEmpty(request.Agent) == false)
                    httpClient.DefaultRequestHeaders.Add("User-Agent", request.Agent);

                if (string.IsNullOrEmpty(request.Accept) == false)
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(request.Accept));

                if (request.Headers != null && request.Headers.Count > 0)
                    foreach (var header in request.Headers)
                        httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);

                await func(httpClient);
            }
        }

        private Stream GenerateStreamFromString(string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
    }
}