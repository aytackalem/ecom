﻿using Helpy.Shared.Http.Base;

namespace Helpy.Shared.Http
{
    public class HttpRequest : HttpRequestBase
    {
    }

    public class HttpRequest<TRequest> : HttpRequestBase
    {
        #region Properties
        public TRequest Request { get; set; }
        #endregion
    }

    public class HttpJsonFileRequest : HttpRequestBase
    {
        #region Properties
        public string Request { get; set; }

        public string FileName { get; set; }
        #endregion
    }

    public class HttpFormUrlEncodedRequest : HttpRequestBase
    {
        #region Properties
        public List<KeyValuePair<string, string>> Data { get; set; }
        #endregion
    }

    public class HttpSoapRequest : HttpRequestBase
    {
        #region Properties
        public string Request { get; set; }
        #endregion
    }
}