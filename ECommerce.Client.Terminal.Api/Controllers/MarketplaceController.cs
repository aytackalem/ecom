


using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MarketplaceController : ControllerBase
    {
        #region Fields
        private readonly IMarketplaceService _marketplaceService;
        #endregion

        #region Constructors
        public MarketplaceController(IMarketplaceService marketplaceService)
        {
            #region Fields
            this._marketplaceService = marketplaceService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok(await this._marketplaceService.GetAsync());
        }
        #endregion
    }
}