﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        #region Fields
        private readonly IProductService _productService;
        #endregion

        #region Constructors
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get(string barcode)
        {
            return Ok(await _productService.ReadByBarcode(barcode));
        }

        [HttpPost("List")]
        public async Task<IActionResult> List(PagedRequest pagedRequest)
        {
            return Ok(await _productService.Read(pagedRequest));
        }
        #endregion
    }
}
