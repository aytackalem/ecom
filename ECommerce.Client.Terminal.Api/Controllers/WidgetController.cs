﻿
using ECommerce.Application.Common.Wrappers;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class WidgetController : ControllerBase
    {
        #region Fields
        private readonly IWidgetService _widgetService;
        #endregion

        #region Constructors
        public WidgetController(IWidgetService widgetService)
        {
            #region Fields
            _widgetService = widgetService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _widgetService.Read(new Core.Application.Parameters.Widgets.WitgetRequest
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date
            }));
        }
        #endregion
    }
}
