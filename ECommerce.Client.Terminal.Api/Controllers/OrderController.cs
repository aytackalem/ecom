﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        #region Fields
        private readonly IOrderService _orderService;
        #endregion

        #region Constructors
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> GetAsync([FromQuery] int orderId, string marketplaceId, string orderType, string shelf)
        {
            return Ok(await _orderService.Get(orderId, marketplaceId, orderType, shelf));

        }

        [HttpPost("Skip")]
        public async Task<IActionResult> Skip(Order order)
        {
            return Ok(await _orderService.Skip(order));
        }

        [HttpPost("Approved")]
        public async Task<IActionResult> Approved(Order order)
        {
            var username = User.Claims.FirstOrDefault(c => c.Type == "name").Value;

            return Ok(await _orderService.Approved(order, username));
        }

        [HttpPost("List")]
        public async Task<IActionResult> List(PagedRequest pagedRequest)
        {
            return Ok(await _orderService.Read(pagedRequest));
        }

        [HttpGet("Summary")]
        public async Task<IActionResult> Summary()
        {
            return Ok(await _orderService.Summary());
        }
        #endregion
    }
}
