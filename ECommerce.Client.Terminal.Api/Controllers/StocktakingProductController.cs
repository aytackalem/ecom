using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class StocktakingProductController : ControllerBase
    {
        #region Fields
        private readonly IStocktakingProductService _service;
        #endregion

        #region Constructors
        public StocktakingProductController(IStocktakingProductService service)
        {
            #region Fields
            _service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get(string barcode, string? modelCode, bool checkGroup)
        {
            return Ok(await _service.GetAsync(barcode, modelCode, checkGroup));
        }
        #endregion
    }
}