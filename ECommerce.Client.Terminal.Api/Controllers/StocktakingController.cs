using ECommerce.Application.Common.Constants;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class StocktakingController : ControllerBase
    {
        #region Fields
        private readonly IStocktakingService _service;
        #endregion

        #region Constructors
        public StocktakingController(IStocktakingService service)
        {
            #region Fields
            _service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get(int page, int pageRecordsCount)
        {
            return Ok(await _service.GetAsync(page, pageRecordsCount, StocktakingTypes.Beklemede));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _service.GetAsync(id));
        }

        [HttpPut]
        public async Task<IActionResult> Put(Stocktaking dto)
        {
            return Ok(await _service.PutAsync(dto));
        }
        #endregion
    }
}