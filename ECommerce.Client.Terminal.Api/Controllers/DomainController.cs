
using ECommerce.Application.Common.Wrappers;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class DomainController : ControllerBase
    {
        #region Fields
        private readonly IDomainService _domainService;
        #endregion

        #region Constructors
        public DomainController(IDomainService domainService)
        {
            #region Fields
            this._domainService = domainService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok(await this._domainService.GetAsync());
        }
        #endregion
    }
}