﻿using Helpy.Shared.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IDomainService
    {
        #region Methods
        Task<DataResponse<List<Core.Application.DataTransferObjects.Domain>>> GetAsync(); 
        #endregion
    }
}
