﻿using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Parameters.Widgets;
using Helpy.Shared.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IWidgetService
    {
        #region Methods
        Task<DataResponse<Widget>> Read(WitgetRequest request);
        #endregion
    }
}
