﻿
using ECommerce.Application.Common.Parameters;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IProductService
    {
        #region Method

        Task<DataResponse<ProductInformation>> ReadByBarcode(string barcode);

        Task<PagedResponse<Product>> Read(PagedRequest pagedRequest);
        #endregion
    }
}
