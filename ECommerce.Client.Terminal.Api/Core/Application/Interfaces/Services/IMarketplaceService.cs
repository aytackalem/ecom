﻿using Helpy.Shared.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IMarketplaceService
    {
        #region Methods
        Task<DataResponse<List<Core.Application.DataTransferObjects.MarketPlace>>> GetAsync(); 
        #endregion
    }
}
