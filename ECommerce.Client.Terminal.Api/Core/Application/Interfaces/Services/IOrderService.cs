﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IOrderService
    {
        #region Method
        /// <summary>
        /// Siparişler Terminalde gösterilir
        /// </summary>
        /// <returns></returns>
        Task<DataResponse<Order>> Get(int orderId, string marketplaceId, string orderType, string shelf);

        /// <summary>
        /// Ürün terminalde hazılanırken kullanıcı atla derse güncelleme yapılır
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Task<NoContentResponse> Skip(Order order);

        /// <summary>
        /// Ürün terminalde onaylanırsa güncelleme yapılır
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Task<NoContentResponse> Approved(Order order, string username);

        /// <summary>
        /// Sipariş listesi mobilde gösterilir.
        /// </summary>
        /// <param name="pagedRequest"></param>
        /// <returns></returns>
        Task<PagedResponse<Order>> Read(PagedRequest pagedRequest);

        Task<DataResponse<Summary>> Summary();
        #endregion
    }
}
