﻿using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IStocktakingService
    {
        #region Methods
        Task<PagedResponse<Stocktaking>> GetAsync(int page, int pageRecordsCount, string? stocktakingTypeId = null);

        Task<DataResponse<Stocktaking>> GetAsync(int id);

        Task<NoContentResponse> PutAsync(Stocktaking dto);
        #endregion
    }
}
