﻿using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.Interfaces.Services
{
    public interface IStocktakingProductService
    {
        #region Methods
        Task<DataResponse<List<ProductInformation>>> GetAsync(string barcode, string? modelCode, bool checkGroup);
        #endregion
    }
}
