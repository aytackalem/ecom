﻿using System;

namespace ECommerce.Client.Terminal.Core.Application.Parameters.Widgets
{
    public class WitgetRequest
    {
        #region Properties
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        #endregion
    }
}
