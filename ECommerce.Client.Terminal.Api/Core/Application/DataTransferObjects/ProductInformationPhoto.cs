﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        #region Property


        /// <summary>
        /// Dosya adı.
        /// </summary>
        public string FileName { get; set; }


        /// <summary>
        /// Ürün Sırası
        /// </summary>
        public int DisplayOrder { get; set; }

        #endregion


    }
}
