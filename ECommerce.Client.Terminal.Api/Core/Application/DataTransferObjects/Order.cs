﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Order
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// İlgili para birim toplamı.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// İlgili para birimine ait id bilgisi.
        /// </summary>
        public string CurrencyId { get; set; }


        /// <summary>
        /// Siparişin ait olduğu pazar yerine ait id bilgisi.
        /// </summary>
        public string MarketPlaceId { get; set; }

        /// <summary>
        /// İlgili sipariş tipine ait id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }


        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyy HH:MM");

        /// <summary>
        /// Sipariş Kaynağı
        /// </summary>
        public string OrderSourceName { get; set; }


        /// <summary>
        /// Paketleme Barkodu
        /// </summary>
        public string PackerBarcode { get; set; }

        /// <summary>
        /// Requesttin vageçmi seçilde yoksa siparişi atlamı seçildi
        /// </summary>
        public bool IsEstimatedPackingDate { get; set; }

        /// <summary>
        /// Firmat Adı
        /// </summary>
        public string CompanyName { get; set; }

        public int TotalQuantity { get; set; }
        #endregion

        #region Navigation Property



        /// <summary>
        /// Siparişe ait detaylar.
        /// </summary>
        public List<OrderDetail> OrderDetails { get; set; }


        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// İlgili market
        /// </summary>
        public MarketPlace MarketPlace { get; set; }
        public string MarketplaceOrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal ListTotal { get; set; }
        public OrderType OrderType { get; set; }
        public bool MutualBarcode { get; set; }
        public List<OrderShipment> OrderShipments { get; set; }


        #endregion
    }
}
