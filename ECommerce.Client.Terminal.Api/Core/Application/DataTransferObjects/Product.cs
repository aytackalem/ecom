﻿using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Product
    {
        #region Properties
        public int Id { get; set; }
        public bool Active { get; set; }
        public string SellerCode { get; set; }
        public int TotalStock { get; set; }
        public int VariantsCount { get; set; }
        public string ProductSourceDomainName { get; set; }
        #endregion

        #region Navigation Properties
        public List<ProductInformation> ProductInformations { get; set; }

        #endregion
    }
}
