﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class ProductInformationPrice
    {
        #region Properties
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal VatRate { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa istediği ürünün fiyatını erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingPrice { get; set; }
        #endregion
    }
}
