﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class StocktakingDetail
    {
        public int Id { get; set; }

        public int ProductInformationId { get; set; }

        public int Quantity { get; set; }

        public ProductInformation ProductInformation { get; set; }
    }
}
