﻿using ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Widget
    {
        #region Navigation Properties
        public List<MarketplaceCount> MarketplaceCounts { get; set; }

        public List<MarketplaceProductCount> MarketplaceProductCounts { get; set; }

        public FinancialSummary FinancialSummary { get; set; }

        public OrderSummary OrderSummary { get; set; }
        #endregion
    }
}
