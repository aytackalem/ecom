﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class OrderDetail
    {
        #region Property
        /// <summary>
        /// Detayin ait olduğu siparişin id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        public decimal UnitPrice { get; set; }
        public bool Payor { get; set; }
        public decimal VatRate { get; set; }
        #endregion
    }
}
