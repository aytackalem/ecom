﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Domain
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// Şirket adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<Company> Companies { get; set; }
        #endregion
    }
}
