﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class TopSeller
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string Name { get; set; }

        public int Quantity  { get; set; }

        public decimal SalesAmount { get; set; }

        public string FileName { get; set; }
        #endregion
    }
}
