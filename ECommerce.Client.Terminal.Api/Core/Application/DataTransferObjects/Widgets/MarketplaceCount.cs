﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class MarketplaceCount
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int Count { get; set; }

        public decimal SalesAmount { get; set; }
        #endregion
    }
}
