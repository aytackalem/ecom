﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class MarketplaceProductCount
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int Count { get; set; }
        #endregion
    }
}
