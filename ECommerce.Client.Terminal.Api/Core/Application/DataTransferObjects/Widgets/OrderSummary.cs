﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class OrderSummary
    {
        #region Properties
        public int OrdersCount { get; set; }

        public int PackedOrdersCount { get; set; }

        public int UnpackedOrdersCount => this.OrdersCount - this.PackedOrdersCount;
        #endregion
    }
}
