﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class FinancialSummary
    {
        #region Fields
        private int? _hour = null;

        private SaleSummary _todaySaleSummary;

        private SaleSummary _yesterdaySaleSummary;
        #endregion

        #region Properties
        public int Hour
        {
            get
            {
                if (_hour.HasValue == false)
                    this._hour = DateTime.Now.Hour;

                return _hour.Value;
            }
        }

        public bool SalesAmountComparePossitive => this.SalesAmountCompare > 0;

        public decimal SalesAmountCompare
        {
            get
            {
                var difference = this.TodaySaleSummary.CumulativeSalesAmount - this.YesterdaySaleSummary.CumulativeSalesAmount;
                if (difference == 0)
                    return 0M;

                return (difference / (this.YesterdaySaleSummary.CumulativeSalesAmount == 0 ? 1 : this.YesterdaySaleSummary.CumulativeSalesAmount)) * 100;
            }
        }

        public bool CostAmountComparePossitive => this.CostAmountCompare <= 0;

        public decimal CostAmountCompare
        {
            get
            {
                var difference = this.TodaySaleSummary.CumulativeCostAmount - this.YesterdaySaleSummary.CumulativeCostAmount;
                if (difference == 0)
                    return 0M;

                return (difference / (this.YesterdaySaleSummary.CumulativeCostAmount == 0 ? 1 : this.YesterdaySaleSummary.CumulativeCostAmount)) * 100;
            }
        }

        public decimal YesterdayProfitAmount { get; set; }

        public decimal YesterdayProfitRate { get; set; }
        #endregion

        #region Navigation Properties
        public SaleSummary TodaySaleSummary
        {
            get
            {
                if (_todaySaleSummary == null)
                    _todaySaleSummary = this.TodaySaleSummaries.OrderByDescending(tss => tss.Hour).FirstOrDefault(tss => tss.Hour <= this.Hour) ?? new SaleSummary
                    {
                        Day = "Today",
                        CostAmount = 0,
                        CumulativeCostAmount = 0,
                        CumulativeSalesAmount = 0,
                        Hour = 0,
                        SalesAmount = 0
                    };

                return _todaySaleSummary;
            }
        }

        public SaleSummary YesterdaySaleSummary
        {
            get
            {
                if (_yesterdaySaleSummary == null)
                    _yesterdaySaleSummary = this.YesterdaySaleSummaries.OrderByDescending(tss => tss.Hour).FirstOrDefault(tss => tss.Hour <= this.Hour) ?? new SaleSummary
                    {
                        Day = "Yesterday",
                        CostAmount = 0,
                        CumulativeCostAmount = 0,
                        CumulativeSalesAmount = 0,
                        Hour = 0,
                        SalesAmount = 0
                    };

                return _yesterdaySaleSummary;
            }
        }

        public List<SaleSummary> TodaySaleSummaries { get; set; }

        public List<SaleSummary> YesterdaySaleSummaries { get; set; }
        #endregion
    }
}
