﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets
{
    public class SaleSummary
    {
        #region Properties
        public string Day { get; set; }

        public int Hour { get; set; }

        public decimal SalesAmount { get; set; }

        public decimal CumulativeSalesAmount { get; set; }

        public decimal CostAmount { get; set; }

        public decimal CumulativeCostAmount { get; set; }

        public decimal ProfitAmount => this.SalesAmount - this.CostAmount;

        public decimal CumulativeProfitAmount => this.CumulativeSalesAmount == 0 ? 0 : this.CumulativeSalesAmount - this.CumulativeCostAmount;

        public decimal CumulativeProfitRate => this.CumulativeProfitAmount == 0 ? 0 : this.CumulativeProfitAmount / this.CumulativeSalesAmount * 100M;
        #endregion
    }
}
