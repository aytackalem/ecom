﻿using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Variant
    {
        #region Properties
        public bool Photoable { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
