﻿namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Summary
    {
        #region Properties
        public int TodayOrdersCount { get; set; }

        public int PackedOrdersCount { get; set; }

        public int UnpackedOrdersCount { get; set; }
        #endregion
    }
}
