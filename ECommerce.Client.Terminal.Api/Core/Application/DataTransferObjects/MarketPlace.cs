﻿using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class MarketPlace
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

    }
}
