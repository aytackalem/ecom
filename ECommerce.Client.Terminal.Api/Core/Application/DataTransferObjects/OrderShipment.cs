﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class OrderShipment
    {
        #region Property
        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public string ShipmentCompanyId { get; set; }

        /// <summary>
        /// Kargo takip numarası.
        /// </summary>
        public string TrackingCode { get; set; }


        /// <summary>
        /// Kargo takip numarası.
        /// </summary>
        public string TrackingUrl { get; set; }



        public bool Payor { get; set; }
        #endregion

        #region Navigation Property


        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public ShipmentCompany ShipmentCompany { get; set; }
        public string PackageNumber { get; set; }
        #endregion
    }
}
