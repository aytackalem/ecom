﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class OrderType
    {
        #region Property
        /// <summary>
        /// Tip  kısa ad.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tipin uzun ad
        /// </summary>
        public string OrderTypeName { get; set; }
        #endregion

      
    }
}
