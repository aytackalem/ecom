﻿using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class ProductInformationVariant
    {
        #region Properties
        public int VariantValueId { get; set; }
        #endregion

        public VariantValue VariantValue { get; set; }
    }
}
