﻿using System.Collections.Generic;

namespace ECommerce.Client.Terminal.Core.Application.DataTransferObjects
{
    public class Stocktaking
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string? ModelCode { get; set; }

        public bool CheckGroup { get; set; }

        public bool Manuel { get; internal set; }

        public string StocktakingTypeId { get; set; }

        public List<StocktakingDetail> StocktakingDetails { get; set; }
    }
}
