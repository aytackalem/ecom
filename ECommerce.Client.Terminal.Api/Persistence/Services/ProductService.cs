﻿
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class ProductService : IProductService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        public readonly ITenantFinder _tenantFinder;

        public readonly IDomainFinder _domainFinder;

        public readonly ICompanyFinder _companyFinder;

        public readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructor
        public ProductService(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._tenantFinder = tenantFinder;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Method
        public async Task<DataResponse<ProductInformation>> ReadByBarcode(string barcode)
        {

            return await ExceptionHandler.HandleAsync<DataResponse<ProductInformation>>(async response =>
            {
                this._dbNameFinder.Set("Lafaba");

                var entity = await _unitOfWork.ProductInformationRepository.DbSet()
                     .Include(x => x.ProductInformationVariants)
                     .ThenInclude(x => x.VariantValue.VariantValueTranslations)
                     .Include(x => x.ProductInformationVariants)
                     .ThenInclude(x => x.VariantValue.Variant.VariantTranslations)
                     .Include(x => x.ProductInformationTranslations)
                     .Include(x => x.ProductInformationPhotos)
                     .Include(x => x.Product)
                     .FirstOrDefaultAsync(x => x.Barcode == barcode);

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                var domain = await this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(x => x.DomainSetting)
                    .FirstOrDefaultAsync(d => d.Id == this._domainFinder.FindId());

                var productPath = $"{domain.DomainSetting.ImageUrl}/product";

                var dto = new ProductInformation
                {
                    Id = entity.Id,
                    Barcode = entity.Barcode,
                    StockCode = entity.StockCode,
                    ModelCode = entity.Product.SellerCode,
                    ProductVariants = entity.ProductInformationVariants.Select(pv => new ProductInformationVariant
                    {
                        VariantValue = new VariantValue
                        {
                            Value = pv.VariantValue.VariantValueTranslations.FirstOrDefault(y => y.LanguageId == "TR")?.Value,
                            Variant = new Variant
                            {
                                Name = pv.VariantValue.Variant.VariantTranslations.FirstOrDefault(y => y.LanguageId == "TR")?.Name,
                                Photoable = pv.VariantValue.Variant.Photoable
                            }
                        },
                        VariantValueId = pv.VariantValueId
                    }).ToList(),
                    ShelfCode = entity.ShelfCode,
                    ShelfZone = entity.ShelfZone,
                    ProductInformationPhotos = entity.ProductInformationPhotos.Count > 0 ? entity.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                    {
                        FileName = $"{productPath}/{pip.FileName}",
                        DisplayOrder = pip.DisplayOrder
                    }).ToList()
                                : new List<ProductInformationPhoto>(),
                    ProductInformationName = entity.ProductInformationTranslations[0].Name,
                };

                response.Data = dto;
                response.Success = true;
            });
        }

        public async Task<PagedResponse<Product>> Read(PagedRequest pagedRequest)
        {
            return await ExceptionHandler.HandleAsync<PagedResponse<Product>>(async response =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Where(x => !x.IsOnlyHidden);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Value:"))
                    {
                        var value = pLoop.Replace("Value:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(iq =>
                               iq.SellerCode.Contains(value)
                            || iq.ProductInformations.Any(pi =>
                                        pi.ProductInformationTranslations.Any(pit => pit.Name.Contains(value))
                                    || pi.StockCode.Contains(value)
                                    || pi.Barcode.Contains(value)));
                        }
                    }
                    else if (pLoop.StartsWith("BrandId:"))
                    {
                        var value = pLoop.Replace("BrandId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.BrandId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("OnlyStockAvailable:"))
                    {
                        var value = pLoop.Replace("OnlyStockAvailable:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.Stock > 0));
                    }
                    else if (pLoop.StartsWith("OnlyNoPhoto:"))
                    {
                        var value = pLoop.Replace("OnlyNoPhoto:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => !pi.ProductInformationPhotos.Any()));
                    }
                    else if (pLoop.StartsWith("ProductSourceDomainId:"))
                    {
                        var value = pLoop.Replace("ProductSourceDomainId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => value == "0" ? !iq.ProductSourceDomainId.HasValue : iq.ProductSourceDomainId == Convert.ToInt32(value));
                    }
                }

                var domain = this
               ._unitOfWork
               .DomainRepository
               .DbSet()
               .Include(x => x.DomainSetting)
               .Include(c => c.Companies)
               .ThenInclude(x => x.CompanySetting)
               .FirstOrDefault(d => d.Id == this._domainFinder.FindId());
                var productPath = $"{domain.DomainSetting.ImageUrl}/product";
                var company = domain.Companies.FirstOrDefault(x => x.Id == this._companyFinder.FindId());


                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = await iQueryable.CountAsync();
                response.Data = await iQueryable
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Product
                    {
                        Active = b.Active,
                        Id = b.Id,
                        SellerCode = b.SellerCode,
                        TotalStock = b.ProductInformations.Sum(bi => bi.Stock),
                        VariantsCount = b.ProductInformations.Count,
                        ProductSourceDomainName = b.ProductSourceDomainId.HasValue ? b.ProductSourceDomain.Name : "Helpy",
                        ProductInformations = b
                            .ProductInformations
                            .Select(pi => new ProductInformation
                            {
                                Active = pi.Active,
                                Stock = pi.Stock,
                                StockCode = pi.StockCode,
                                Barcode = pi.Barcode,
                                ProductInformationName = pi.ProductInformationTranslations[0].Name,
                                ProductInformationUrl = company.CompanySetting.IncludeECommerce && !string.IsNullOrEmpty(pi.ProductInformationTranslations[0].Url) ? $"{company.CompanySetting.WebUrl}/{pi.ProductInformationTranslations[0].Url}" : null,
                                ProductInformationPriceses = pi.ProductInformationPriceses.Where(x => x.CurrencyId == "TL").Select(pip => new ProductInformationPrice
                                {
                                    ListUnitPrice = pip.ListUnitPrice,
                                    UnitPrice = pip.UnitPrice,
                                    AccountingPrice = pip.AccountingPrice,
                                    VatRate = pip.VatRate,
                                })
                                .ToList(),
                                ProductInformationPhotos = new()
                                {
                                    new()
                                    {
                                        FileName =$"{productPath}/{pi.ProductInformationPhotos.FirstOrDefault(pip=>!pip.Deleted).FileName}"
                                    }
                                },
                                ProductInformationMarketplaces = pi
                                    .ProductInformationMarketplaces
                                    .Where(pim => pim.CompanyId == this._companyFinder.FindId())
                                    .Select(pim => new ProductInformationMarketplace
                                    {
                                        MarketplaceId = pim.MarketplaceId,
                                        Url = pim.Url,
                                        Opened = pim.Opened,
                                        ListUnitPrice = pim.ListUnitPrice,
                                        UnitPrice = pim.UnitPrice,
                                        Active = pim.Active
                                    })
                                    .ToList()
                            })
                            .ToList()
                    })
                    .ToListAsync();
                response.Success = true;
            });
        }
        #endregion


    }
}
