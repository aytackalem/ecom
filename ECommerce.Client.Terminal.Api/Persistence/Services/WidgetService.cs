﻿using Dapper;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects.Widgets;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using ECommerce.Client.Terminal.Core.Application.Parameters.Widgets;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class WidgetService : IWidgetService
    {
        #region Fields

        private readonly ICompanyFinder _companyFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ITenantFinder _tenantFinder;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public WidgetService(ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder,
            IConfiguration configuration, IDbNameFinder dbNameFinder, IUnitOfWork unitOfWork)
        {
            #region Fields
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            this._dbNameFinder = dbNameFinder;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methodss
        private async Task<DataResponse<List<MarketplaceCount>>> GetMarketplaceCounts(WitgetRequest request, string connectionString)
        {
            SqlConnection sqlConnection = null;

            return await ExceptionHandler.HandleAsync<DataResponse<List<MarketplaceCount>>>(async response =>
            {
                using (sqlConnection = new(connectionString))
                {
                    var query = @"
Select		M.Id As MarketplaceId,
			Count(O.Id) As [Count],
			Sum(IsNull(O.Total, 0)) As SalesAmount

From		Marketplaces As M With(NoLock)

Left Join	Orders As O With(NoLock)
On			O.MarketplaceId = M.Id
			And Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
			And O.TenantId = @TenantId
			And O.DomainId = @DomainId
			And O.CompanyId = @CompanyId
			And O.OrderTypeId <> 'IP'

Group By	M.Id
Order By	Count(O.Id) Desc";

                    response.Data = sqlConnection
                        .Query<MarketplaceCount>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        }).ToList();

                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (reponse) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private async Task<DataResponse<List<MarketplaceProductCount>>> GetMarketplaceProductCounts(WitgetRequest request, string connectionString)
        {
            SqlConnection sqlConnection = null;

            return await ExceptionHandler.HandleAsync<DataResponse<List<MarketplaceProductCount>>>(async response =>
            {
                using (sqlConnection = new(connectionString))
                {
                    var query = @"
Select		M.Id As MarketplaceId,
			Count(PIM.Id) As [Count]
From		Marketplaces As M With(NoLock)

Left Join	ProductInformationMarketplaces As PIM With(NoLock)
On			PIM.MarketplaceId = M.Id
			And PIM.TenantId = @TenantId
			And PIM.DomainId = @DomainId
			And PIM.CompanyId = @CompanyId
			And PIM.Opened = 1

Group By	M.Id
Order By	Count(PIM.Id) Desc";

                    response.Data = sqlConnection
                        .Query<MarketplaceProductCount>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (reponse) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private async Task<DataResponse<FinancialSummary>> GetFinancialSummary(WitgetRequest request, string connectionString)
        {
            SqlConnection sqlConnection = null;

            return await ExceptionHandler.HandleAsync<DataResponse<FinancialSummary>>(async response =>
            {
                #region Fetch Data
                List<SaleSummary> saleSummaries = null;
                using (sqlConnection = new(connectionString))
                {
                    var query = @"
Select	[Day],
		[Hour],
		SalesAmount,
		SUM(SalesAmount) Over(Order By [Day], [Hour] Rows Between UnBounded Preceding And Current Row) As [CumulativeSalesAmount],
		CostAmount,
		SUM(CostAmount) Over(Order By [Day], [Hour] Rows Between UnBounded Preceding And Current Row) As [CumulativeCostAmount]
From
(
    Select	    'Today' As [Day],
                DatePart(Hour, O.OrderDate) As [Hour],
                IsNull(Sum(OD.Quantity * OD.UnitPrice), 0) As SalesAmount,
    		    IsNull(Sum(OD.Quantity * OD.UnitCost), 0) As CostAmount
    From	    Orders As O With(NoLock)
    Join	    OrderDetails As OD With(NoLock)
    On		    O.Id = OD.OrderId
    Where	    Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
    		    And O.TenantId = @TenantId
    		    And O.DomainId = @DomainId
    		    And O.CompanyId = @CompanyId
                And O.OrderTypeId <> 'IP'
    Group By	DatePart(Hour, O.OrderDate)
) As T

Union

Select	[Day],
		[Hour],
		SalesAmount,
		SUM(SalesAmount) Over(Order By [Day], [Hour] Rows Between UnBounded Preceding And Current Row) As [CumulativeSalesAmount],
		CostAmount,
		SUM(CostAmount) Over(Order By [Day], [Hour] Rows Between UnBounded Preceding And Current Row) As [CumulativeCostAmount]
From
(
    Select	    'Yesterday' As [Day],
                DatePart(Hour, O.OrderDate) As [Hour],
                IsNull(Sum(OD.Quantity * OD.UnitPrice), 0) As SalesAmount,
    		    IsNull(Sum(OD.Quantity * OD.UnitCost), 0) As CostAmount
    From	    Orders As O With(NoLock)
    Join	    OrderDetails As OD With(NoLock)
    On		    O.Id = OD.OrderId
    Where	    Cast(O.OrderDate As Date) Between Cast(DateAdd(Day, -1, @StartDate) As Date) And Cast(DateAdd(Day, -1, @EndDate) As Date)
    		    And O.TenantId = @TenantId
    		    And O.DomainId = @DomainId
    		    And O.CompanyId = @CompanyId
                And O.OrderTypeId <> 'IP'
    Group By	DatePart(Hour, O.OrderDate)
) As T";

                    saleSummaries = sqlConnection
                        .Query<SaleSummary>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        })
                        .ToList();
                }
                #endregion

                response.Data = new FinancialSummary
                {
                    TodaySaleSummaries = saleSummaries.Where(ss => ss.Day == "Today").ToList(),
                    YesterdaySaleSummaries = saleSummaries.Where(ss => ss.Day == "Yesterday").ToList(),
                };
            }, (reponse) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private async Task<DataResponse<OrderSummary>> GetOrderSummary(WitgetRequest request, string connectionString)
        {
            SqlConnection sqlConnection = null;

            return await ExceptionHandler.HandleAsync<DataResponse<OrderSummary>>(async response =>
            {
                using (sqlConnection = new(connectionString))
                {
                    var query = @"
With T As (
	Select  Count(0) As [OrdersCount]
	From    Orders As O With(NoLock)
    Where   Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		    And O.TenantId = @TenantId
		    And O.DomainId = @DomainId
		    And O.CompanyId = @CompanyId
            And O.OrderTypeId <> 'IP'
), T2 As (
	Select  Count(0) As [PackedOrdersCount]
	From    Orders As O With(NoLock)
    Where   Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		    And O.TenantId = @TenantId
		    And O.DomainId = @DomainId
		    And O.CompanyId = @CompanyId
            And O.OrderTypeId In ('KTE', 'TA', 'TS')
)
Select	(Select [OrdersCount] From T) As [OrdersCount], (Select [PackedOrdersCount] From T2) As [PackedOrdersCount]";

                    response.Data = sqlConnection
                        .QueryFirstOrDefault<OrderSummary>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        });
                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (response) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }


        public async Task<DataResponse<Widget>> Read(WitgetRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Widget>>(async response =>
            {


                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                var _getFinancialSummary = await GetFinancialSummary(request, connectionString);
                var _getMarketplaceCounts = await GetMarketplaceCounts(request, connectionString);
                var _getMarketplaceProductCounts = await GetMarketplaceProductCounts(request, connectionString);
                var _getOrderSummary = await GetOrderSummary(request, connectionString);
                response.Data = new Widget
                {
                    FinancialSummary = _getFinancialSummary.Data,
                    MarketplaceCounts = _getMarketplaceCounts.Data,
                    MarketplaceProductCounts = _getMarketplaceProductCounts.Data,
                    OrderSummary = _getOrderSummary.Data,
                };

                response.Success = true;
            });
        }
        #endregion
    }
}
