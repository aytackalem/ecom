﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class MarketplaceService : IMarketplaceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.MarketPlace>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.MarketPlace>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .MarketPlaceRepository
                    .DbSet()
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new Core.Application.DataTransferObjects.MarketPlace
                    {
                        Id = e.Id,
                        Name = e.Name
                    })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
