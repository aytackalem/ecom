﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class StocktakingProductService : IStocktakingProductService
    {
        #region Fields
        protected readonly IUnitOfWork _unitOfWork;

        protected readonly IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public StocktakingProductService(IUnitOfWork unitOfWork, IDomainFinder domainFinder)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<ProductInformation>>> GetAsync(string barcode, string? modelCode, bool checkGroup)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<ProductInformation>>>(async response =>
            {
                var iQueryable = this._unitOfWork.ProductInformationRepository.DbSet().Where(pi => pi.Barcode == barcode);
                if (!string.IsNullOrEmpty(modelCode))
                    iQueryable = iQueryable.Where(pi => pi.Product.SellerCode == modelCode);

                var productInformation = await iQueryable
                    .Include(pi => pi.ProductInformationTranslations.Where(pit => pit.LanguageId == "TR"))
                    .Include(pi => pi.ProductInformationPhotos)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (productInformation == null)
                {
                    string message = "Ürün bulunamadı.";
                    if (!string.IsNullOrEmpty(modelCode))
                        message += " Model kodunu kontrol ediniz.";

                    response.Message = message;
                    return;
                }

                response.Success = true;
                response.Data = new()
                {
                    new()
                    {
                        Id = productInformation.Id,
                        ProductInformationName = productInformation.ProductInformationTranslations.FirstOrDefault().Name,
                        Barcode = productInformation.Barcode
                    }
                };

                if (!checkGroup)
                    return;

                var iQueryable2 = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(pi => pi.Id != productInformation.Id && pi.GroupId == productInformation.GroupId);
                if (!string.IsNullOrEmpty(modelCode))
                    iQueryable2 = iQueryable2.Where(pi => pi.Product.SellerCode == modelCode);

                var productInformations = await iQueryable2
                    .Include(pi => pi.ProductInformationTranslations.Where(pit => pit.LanguageId == "TR"))
                    .Include(pi => pi.ProductInformationPhotos)
                    .AsNoTracking()
                    .ToListAsync();

                response.Data.AddRange(productInformations.Select(pi => new ProductInformation()
                {
                    Id = pi.Id,
                    ProductInformationName = pi.ProductInformationTranslations.FirstOrDefault().Name,
                    Barcode = pi.Barcode
                }));
            });
        }
        #endregion
    }
}
