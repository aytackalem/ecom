﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.DataTransferObjects;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class StocktakingService : IStocktakingService
    {
        #region Fields
        protected readonly IUnitOfWork _unitOfWork;

        protected readonly IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public StocktakingService(IUnitOfWork unitOfWork, IDomainFinder domainFinder)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<PagedResponse<Stocktaking>> GetAsync(int page, int pageRecordsCount, string? stocktakingTypeId = null)
        {
            return await ExceptionHandler.HandleAsync<PagedResponse<Stocktaking>>(async response =>
            {
                var iQueryable = this._unitOfWork.StocktakingRepository.DbSet();

                if (!string.IsNullOrEmpty(stocktakingTypeId))
                    iQueryable = iQueryable.Where(q => q.StocktakingTypeId == stocktakingTypeId);

                var stocktakings = await iQueryable.Skip((page - 1) * pageRecordsCount).Take(pageRecordsCount).ToListAsync();
                var count = await iQueryable.CountAsync();

                response.Success = true;
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;
                response.RecordsCount = count;
                response.Data = stocktakings
                        .Select(s => new Stocktaking
                        {
                            Id = s.Id,
                            Name = s.Name,
                            Description = s.Description,
                            ModelCode = s.ModelCode,
                            CheckGroup = s.CheckGroup
                        })
                        .ToList();
            });
        }

        public async Task<DataResponse<Stocktaking>> GetAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Stocktaking>>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .StocktakingRepository
                    .DbSet()
                    .Include(s => s.StocktakingDetails)
                        .ThenInclude(sd => sd.ProductInformation)
                            .ThenInclude(sd => sd.ProductInformationTranslations.Where(pit => pit.LanguageId == "TR"))
                    .FirstOrDefaultAsync(s => s.Id == id);

                if (entity == null)
                {
                    response = new()
                    {
                        Message = "Stok sayımı bulunamadı."
                    };
                    return;
                }

                entity.StocktakingTypeId = StocktakingTypes.DevamEdiyor;

                await this
                    ._unitOfWork
                    .StocktakingRepository
                    .UpdateAsync(entity);

                response.Success = true;
                response.Data = new Stocktaking
                {
                    ModelCode = entity.ModelCode,
                    CheckGroup = entity.CheckGroup,
                    Description = entity.Description,
                    Manuel = entity.Manuel,
                    Id = entity.Id,
                    Name = entity.Name,
                    StocktakingDetails = entity
                            .StocktakingDetails
                            .Select(sd => new StocktakingDetail
                            {
                                ProductInformationId = sd.ProductInformationId,
                                Quantity = sd.Quantity,
                                ProductInformation = new ProductInformation
                                {
                                    Barcode = sd.ProductInformation.Barcode,
                                    ProductInformationName = sd.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name
                                }
                            })
                            .ToList()
                };
            });
        }

        public async Task<NoContentResponse> PutAsync(Stocktaking dto)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .StocktakingRepository
                    .DbSet()
                    .Include(s => s.StocktakingDetails)
                    .FirstOrDefaultAsync(s => s.Id == dto.Id);

                if (entity == null)
                {
                    response = new()
                    {
                        Message = "Stok sayımı bulunamadı."
                    };
                    return;
                }

                if (dto.StocktakingTypeId == "BE")
                {
                    response.Success = await this._unitOfWork.StocktakingRepository.UpdateAsync(entity);
                    response.Message = $"Stok sayımı iptal {(response.Success ? "edildi" : "edilemedi")}.";
                    return;
                }

                entity.StocktakingTypeId = StocktakingTypes.OnayBekliyor;

                dto.StocktakingDetails.ForEach(theStocktakingDetail =>
                {
                    if (entity.StocktakingDetails.Any(sd => sd.ProductInformationId == theStocktakingDetail.ProductInformationId))
                        entity
                            .StocktakingDetails
                            .First(sd => sd.ProductInformationId == theStocktakingDetail.ProductInformationId)
                            .Quantity = theStocktakingDetail.Quantity;
                    else
                        entity
                            .StocktakingDetails
                            .Add(new Domain.Entities.Domainable.StocktakingDetail
                            {
                                ProductInformationId = theStocktakingDetail.ProductInformationId,
                                Quantity = theStocktakingDetail.Quantity,
                            });
                });

                response.Success = await this._unitOfWork.StocktakingRepository.UpdateAsync(entity);
                response.Message = $"Stok sayımı {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
