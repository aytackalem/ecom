﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Persistence.Services
{
    public class DomainService : IDomainService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public DomainService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.Domain>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.Domain>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(x => x.DomainSetting)
                    .Include(x => x.Companies)
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new Core.Application.DataTransferObjects.Domain
                    {
                        Id = e.Id,
                        Name = e.Name,
                        Companies = e.Companies.Select(co => new Core.Application.DataTransferObjects.Company
                        {
                            Logo = co.FileName != null ? $"{e.DomainSetting.ImageUrl}/company/{co.FileName}" : "https://erp.helpy.com.tr/logo.png",
                            Id = co.Id,
                            Name = co.Name,
                        }).ToList()
                    })
                    .ToList();
                response.Success = true;
                response.Message = $"Domain kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
