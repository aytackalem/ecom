﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;

namespace ECommerce.Client.Terminal.Persistence.Finders
{
    public class DomainFinder : IDomainFinder
    {
        #region Fields
        private int _domainId = 0;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public DomainFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            if (this._domainId == 0)
            {
                if (this._httpContextAccessor.HttpContext.Request.Headers.ContainsKey("domainId"))
                    this._domainId = int.Parse(this._httpContextAccessor.HttpContext.Request.Headers["domainId"].ToString());
                else
                    this._domainId = 1;

            }

            return this._domainId;
        }

        public void Set(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
