﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace ECommerce.Client.Terminal.Persistence.Finders
{
    public class DbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName = string.Empty;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public DbNameFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public void Set(string dbName)
        {
            this._dbName = dbName;

        }

        public string FindName()
        {
            if (this._dbName == string.Empty)
            {
                var user = this._httpContextAccessor.HttpContext.User;
                if (user != null)
                {
                    var claim = user.Claims.FirstOrDefault(c => c.Type == "brand");
                    if (claim != null)
                    {
                        this._dbName = claim.Value;
                    }
                }
            }

            return this._dbName;
        }
        #endregion
    }
}
