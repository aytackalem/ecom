﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Client.Terminal.Core.Application.Interfaces.Services;
using ECommerce.Client.Terminal.Persistence.Finders;
using ECommerce.Client.Terminal.Persistence.Services;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Client.Terminal.Persistence
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddHttpContextAccessor();

            serviceCollection.AddDbContext<ECommerce.Persistence.Context.ApplicationDbContext>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            #region Finders
            serviceCollection.AddScoped<IDbNameFinder, DbNameFinder>();
            serviceCollection.AddScoped<ITenantFinder, TenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, DomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, CompanyFinder>();
            #endregion

            #region Services
            serviceCollection.AddTransient<IOrderService, OrderService>();
            serviceCollection.AddTransient<IProductService, ProductService>();
            serviceCollection.AddTransient<IWidgetService, WidgetService>();
            serviceCollection.AddTransient<IDomainService, DomainService>();
            serviceCollection.AddTransient<IMarketplaceService, MarketplaceService>();
            serviceCollection.AddTransient<IStocktakingService, StocktakingService>();
            serviceCollection.AddTransient<IStocktakingProductService, StocktakingProductService>();
            #endregion
        }
        #endregion
    }
}
