﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Pazarama.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;

            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
               


                var _categoryResponse = this
                    ._httpHelper
                    .Get<Common.Categories.CategoryResponse>(
                        "https://isortagimapi.pazarama.com/category/getCategoryTree",
                        null,
                        null);

                var categories = _categoryResponse.data.Where(x => x.parentCategories.Contains("Kozmetik")).ToList();
                var _categories = new List<CategoryResponse>();

                foreach (var cLoop in categories)
                {
                    var categoryResponse = new CategoryResponse()
                    {
                        Code = cLoop.id.ToString(),
                        Name = cLoop.name,
                        ParentCategoryCode = cLoop.parentId

                    };

                    var categoryAttributeResponse = this
                        ._httpHelper
                        .Get<Common.CategoryAttributes.CategoryAttributeResponse>(
                            $"https://isortagimapi.pazarama.com/category/getCategoryWithAttributes?Id={cLoop.id}",
                            null,
                            null);

                    categoryResponse.CategoryAttributes = categoryAttributeResponse
                        .data
                        .attributes
                        .Select(a => new CategoryAttributeResponse
                        {
                            Code = a.id,
                            Mandatory = a.isRequired,
                            AllowCustom = false,
                            CategoryCode = cLoop.id,
                            MultiValue = false,
                            Name = a.name,
                            CategoryAttributesValues = a
                                .attributeValues
                                .Select(av => new CategoryAttributeValueResponse
                                {
                                    VarinatCode = a.id,
                                    Code = av.id,
                                    Name = av.value
                                })
                                .ToList()
                        })
                        .ToList();

                    _categories.Add(categoryResponse);
                }

                response.Data = _categories;
                response.Success = true;
            });
        }
        #endregion
    }
}
