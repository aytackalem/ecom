﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Stock;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.ProductFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper, ICacheHandler cacheHandler)
        {
            _httpHelper = httpHelper;
            _cacheHandler = cacheHandler;

        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {



                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var tokenOperation = new TokenOperation(_cacheHandler);
                var token = tokenOperation.GetToken(stockRequest.Configurations);

                if (!token.Success)
                {
                    response.Success = false;
                    response.Message = token.Message;
                    return;
                }

                var stokRequest = new Common.Stok.StokRequest
                {
                    Items = new List<Common.Stok.Stok>
                    {
                        new Common.Stok.Stok
                        {
                            Code=stockRequest.PriceStock.Barcode,
                            StockCount=stockRequest.PriceStock.Quantity
                        }
                    }

                };

                var stockResponse = _httpHelper.Post<Common.Stok.StokRequest, Common.Stok.StokResponse>(stokRequest, "https://isortagimapi.pazarama.com/product/updateStock", "Bearer", token.Data.data.accessToken);
                if (stockResponse.success)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[PAZARAMA] [{stockRequest.PriceStock.Barcode}]  stok güncellendi");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    response.Success = true;

                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        private DataResponse<ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductFilterResponse>>((response) =>
            {


                response.Success = true;
            });
        }
        #endregion
    }
}
