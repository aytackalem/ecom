﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Pazarama.Common.Token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Operation
{
    public class TokenOperation
    {
        private readonly ICacheHandler _cacheHandler;

        public TokenOperation(ICacheHandler cacheHandler)
        {
            _cacheHandler = cacheHandler;
        }

        public DataResponse<Token> GetToken(Dictionary<string, string> configurations)
        {

            return this._cacheHandler.ResponseHandle(() =>
            {

                return ExceptionHandler.ResultHandle<DataResponse<Token>>((response) =>
            {
                var url = "https://isortagimapi.pazarama.com/connect/token";

                Token token = null;
                var authType = "basic";
                var byteArray = Encoding.ASCII.GetBytes($"{configurations["ApiKey"]}:{configurations["SecretKey"]}");
                var _token = Convert.ToBase64String(byteArray);


                using (HttpClient client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(10);

                    if (!string.IsNullOrEmpty(authType))
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, _token);

                    var dict = new Dictionary<string, string>();
                    dict.Add("grant_type", "client_credentials");
                    dict.Add("scope", "merchantgatewayapi.fullaccess");


                    using (HttpResponseMessage httpResponseMessage = client.PostAsync(url, new FormUrlEncodedContent(dict)).Result)
                    using (HttpContent content = httpResponseMessage.Content)
                    {
                        string responseString = content.ReadAsStringAsync().Result;
                        if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");
                        token = JsonConvert.DeserializeObject<Token>(responseString);

                        response.Data = token;
                        response.Success = true;
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
            }, "PazarmaCached");
        }
    }

}
