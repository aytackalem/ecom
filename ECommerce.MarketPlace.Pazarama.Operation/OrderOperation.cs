﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Request.Order;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Pazarama.Common.CargoTraking;
using ECommerce.MarketPlace.Pazarama.Common.SellerDelivery;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Pazarama.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper, ICacheHandler cacheHandler)
        {
            #region Fields
            this._httpHelper = httpHelper;
            this._cacheHandler = cacheHandler;

            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var tokenOperation = new TokenOperation(_cacheHandler);
                var token = tokenOperation.GetToken(orderRequest.Configurations);

                if (!token.Success)
                {
                    response.Success = false;
                    response.Message = token.Message;
                    return;
                }

                var request = new Common.Order.OrderRequest
                {
                    startDate = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd"),
                    endDate = DateTime.Now.ToString("yyyy-MM-dd")
                };

                var orderResponse = _httpHelper.Post<Common.Order.OrderRequest, Common.Order.OrderResponse>(request, "https://isortagimapi.pazarama.com/order/getOrdersForApi", "Bearer", token.Data.data.accessToken);
                if (orderResponse.success)
                {
                    var orders = new List<Order>();

                    foreach (var pOrder in orderResponse.data)
                    {
                        var customerNameSurname = pOrder.customerName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var ordershipmentNameSurname = pOrder.shipmentAddress.nameSurname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderBillingNameSurname = pOrder.billingAddress.nameSurname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        var cargo = pOrder.items[0].cargo;

                        string shipmentId = "";
                        switch (cargo.companyName.ToLower())
                        {
                            case "aras":
                                shipmentId = ShipmentCompanies.ArasKargo;
                                break;
                            case "ptt":
                                shipmentId = ShipmentCompanies.PttKargo;
                                break;
                            case "mng":
                                shipmentId = ShipmentCompanies.MngKargo;
                                break;
                            case "ups":
                                shipmentId = ShipmentCompanies.Ups;
                                break;

                        }

                        Order order = new Order();
                        order.OrderSourceId = OrderSources.Pazaryeri;
                        order.MarketPlaceId = Marketplaces.Pazarama;
                        order.CurrencyId = pOrder.currency;
                        order.OrderDate = pOrder.orderDate;
                        //order.TotalAmount = Convert.ToDecimal(pOrder.orderAmount);
                        //order.ListTotalAmount = Convert.ToDecimal(pOrder.discountAmount) + Convert.ToDecimal(pOrder.orderAmount);
                        //order.Discount = Convert.ToDecimal(pOrder.discountAmount);
                        order.ShippingCost = 0;
                        order.Source = "PA";
                        order.OrderTypeId = "BE";
                        order.OrderCode = pOrder.orderNumber.ToString();
                        order.OrderShipment = new OrderShipment
                        {
                            TrackingCode = cargo.trackingNumber,
                            ShipmentTypeId = "PND",
                            TrackingUrl = cargo.trackingUrl,
                            ShipmentCompanyId = shipmentId
                        };
                        order.Customer = new Customer()
                        {
                            FirstName = customerNameSurname[0],
                            LastName = customerNameSurname.Length > 1 ? customerNameSurname[1] : customerNameSurname[0],
                            IdentityNumber = pOrder.customerId,
                            Mail = pOrder.customerEmail,
                            TaxNumber = string.Empty,
                            Phone = pOrder.shipmentAddress.phoneNumber //TODO set phone number
                        };
                        order.OrderDeliveryAddress = new OrderDeliveryAddress()
                        {
                            FirstName = ordershipmentNameSurname[0],
                            LastName = ordershipmentNameSurname.Length > 1 ? ordershipmentNameSurname[1] : ordershipmentNameSurname[0],
                            Country = "Türkiye",
                            Email = "",
                            Phone = pOrder.shipmentAddress.phoneNumber, //TODO set phone number
                            City = pOrder.shipmentAddress.cityName,
                            District = pOrder.shipmentAddress.districtName,
                            Neighborhood = pOrder.shipmentAddress.neighborhoodName,
                            Address = $"{pOrder.shipmentAddress.displayAddressText}",
                            Code = String.Empty
                        };
                        order.OrderInvoiceAddress = new OrderInvoiceAddress()
                        {
                            FirstName = orderBillingNameSurname[0],
                            LastName = orderBillingNameSurname.Length > 1 ? orderBillingNameSurname[1] : orderBillingNameSurname[0],
                            Email = pOrder.billingAddress.customerEmail,
                            Phone = pOrder.billingAddress.phoneNumber, //TODO set phone number
                            City = pOrder.billingAddress.cityName,
                            District = pOrder.billingAddress.districtName,
                            Address = $"{pOrder.billingAddress.displayAddressText}",
                            Neighborhood = pOrder.shipmentAddress.neighborhoodName,

                        };
                        order.Payments = new List<Payment>();
                        order.Payments.Add(new Payment()
                        {
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Amount = Convert.ToDecimal(pOrder.orderAmount),
                            Date = pOrder.orderDate
                        });

                        #region Order Details
                        order.OrderDetails = new List<OrderDetail>();

                        double cargoFee = 0;
                        foreach (var orderDetail in pOrder.items)
                        {

                            if (orderDetail.orderItemStatus == 3)
                            {
                                cargoFee += orderDetail.shipmentAmount.value;

                                var unitDiscount = Convert.ToDecimal(orderDetail.discountAmount.value);

                                order.OrderDetails.Add(new OrderDetail()
                                {
                                    TaxRate = Convert.ToDouble(orderDetail.product.vatRate) / 100,
                                    UnitPrice = Convert.ToDecimal(orderDetail.salePrice.value),
                                    ListPrice = Convert.ToDecimal(orderDetail.listPrice.value),
                                    UnitCommissionAmount = 0,
                                    UnitCost = 0,
                                    UnitDiscount = unitDiscount,
                                    Payor = true,
                                    Product = new Product()
                                    {
                                        Barcode = orderDetail.product.code,
                                        Name = orderDetail.product.name,
                                        StockCode = orderDetail.product.stockCode.ToString()

                                    },
                                    Quantity = orderDetail.quantity
                                });
                            }
                        }
                        #endregion
                        order.TotalAmount = order.OrderDetails.Sum(x => x.UnitPrice) + Convert.ToDecimal(cargoFee);
                        order.ListTotalAmount = order.OrderDetails.Sum(x => x.ListPrice);
                        order.Discount = order.OrderDetails.Sum(x => x.UnitDiscount);
                        order.CargoFee = Convert.ToDecimal(cargoFee);

                        order.OrderPicking = new MarketPlace.Common.Request.Order.OrderPicking
                        {
                            MarketPlaceOrderId = pOrder.orderNumber.ToString(),
                            OrderDetails = pOrder.items.Select(x => new OrderPickingDetail()
                            {
                                Id = x.orderItemId,
                                Quantity = x.quantity
                            }).ToList()
                        };
                        if (order.OrderDetails.Count > 0)
                            orders.Add(order);
                    }

                    response.Success = true;
                    response.Data = new OrderResponse { Orders = orders };
                }



            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (request.Status == MarketPlaceStatus.Picking)
                {
                    var tokenOperation = new TokenOperation(_cacheHandler);
                    var token = tokenOperation.GetToken(request.Configurations);

                    if (!token.Success)
                    {
                        response.Success = false;
                        response.Message = token.Message;
                        return;
                    }
                    var sellerDelivery = _httpHelper.Get<GetSellerDelivery>("https://isortagimapi.pazarama.com/sellerRegister/getSellerDelivery", "Bearer", token.Data.data.accessToken);
                    if (sellerDelivery.success)
                    {

                        foreach (var item in request.OrderPicking.OrderDetails)
                        {
                            var cargoTrakingRequest = new CargoTraking();
                            cargoTrakingRequest.orderNumber = Convert.ToInt64(request.OrderPicking.MarketPlaceOrderId);
                            cargoTrakingRequest.item = new Item
                            {
                                status = 3,
                                orderItemId = new Guid(item.Id)
                            };

                            var cargoUpdateOrderStatus = _httpHelper.Put<CargoTraking, CargoTrakingData>(cargoTrakingRequest, "https://isortagimapi.pazarama.com/order/updateOrderStatus", "Bearer", token.Data.data.accessToken);



                            var cargoTrakingRequestCargo = new CargoTraking();
                            cargoTrakingRequestCargo.orderNumber = Convert.ToInt64(request.OrderPicking.MarketPlaceOrderId);
                            cargoTrakingRequestCargo.item = new Item
                            {
                                cargoCompanyId = new Guid(sellerDelivery.data.cargoCompany.cargoCompanyId),
                                deliveryType = 1,
                                status = 5,
                                orderItemId = new Guid(item.Id),
                                shippingTrackingNumber = request.OrderPicking.TrackingNumber,
                                trackingUrl = String.Empty
                            };

                            cargoUpdateOrderStatus = _httpHelper.Put<CargoTraking, CargoTrakingData>(cargoTrakingRequestCargo, "https://isortagimapi.pazarama.com/order/updateOrderStatus", "Bearer", token.Data.data.accessToken);
                        }
                    }





                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}