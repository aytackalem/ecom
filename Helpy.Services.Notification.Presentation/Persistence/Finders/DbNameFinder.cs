﻿using Helpy.Services.Notification.Core.Application.Interfaces.Finders;

namespace Helpy.Services.Notification.Persistence.Finders
{
    public class DbNameFinder : IDbNameFinder
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
