﻿using Helpy.Services.Notification.Core.Application.Interfaces.Finders;
using Helpy.Services.Notification.Persistence.Repositories.MongoDb.Base;

namespace Helpy.Services.Notification.Presentation.Persistence.Repositories.MongoDb
{
    public class NotificationRepository : RepositoryBase<Notification.Core.Domain.Entities.Notification>
    {
        public NotificationRepository(MongoConfiguration mongoConfiguration, IDbNameFinder dbNameFinder) : base(dbNameFinder, mongoConfiguration.ConnectionString, mongoConfiguration.NotificationCollectionName)
        {
        }
    }
}
