﻿namespace Helpy.Services.Notification.Presentation.Persistence.Repositories.MongoDb
{
    public class MongoConfiguration
    {
        #region Properties
        public string ConnectionString { get; set; }

        public string NotificationCollectionName { get; set; }
        #endregion
    }
}
