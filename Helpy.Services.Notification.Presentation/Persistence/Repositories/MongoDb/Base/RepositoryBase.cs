﻿using Helpy.Services.Notification.Core.Application.Interfaces.Finders;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Core.Domain.Entities.Base;
using MongoDB.Driver;
using System.Text.RegularExpressions;

namespace Helpy.Services.Notification.Persistence.Repositories.MongoDb.Base
{
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        #region Fields
        protected readonly IMongoCollection<TEntity> _mongoCollection;
        #endregion

        #region Constructors
        public RepositoryBase(IDbNameFinder dbNameFinder, string connectionString, string collectionName)
        {
            #region Fields
            var mongoClient = new MongoClient(connectionString);
            var mongoDatabase = mongoClient.GetDatabase(dbNameFinder.Name);
            this._mongoCollection = mongoDatabase.GetCollection<TEntity>(collectionName);
            #endregion
        }
        #endregion

        #region Methods
        public async Task<long> CountAsync(string marketplaceId, string barcode, string stockCode, string type, string modelCode, string productInformationName)
        {
            var filterBuilder = Builders<TEntity>.Filter;
            var filter = filterBuilder.Empty;

            if (!string.IsNullOrEmpty(marketplaceId))
                filter &= filterBuilder.Eq("MarketplaceId", marketplaceId);

            if (!string.IsNullOrEmpty(barcode))
                filter &= filterBuilder.Eq("Barcode", barcode);

            if (!string.IsNullOrEmpty(stockCode))
                filter &= filterBuilder.Eq("StockCode", stockCode);

            if (!string.IsNullOrEmpty(type))
                filter &= filterBuilder.Eq("Type", type);

            if (!string.IsNullOrEmpty(modelCode))
                filter &= filterBuilder.Eq("Modelcode", modelCode);

            if (!string.IsNullOrEmpty(productInformationName))
                filter &= filterBuilder.Regex("ProductInformationName", new Regex(productInformationName, RegexOptions.None));


            return await this._mongoCollection.CountAsync(filter);
        }

        public async Task CreateAsync(TEntity t)
        {
            await this._mongoCollection.InsertOneAsync(t);
        }

        public async Task UpsertAsync(TEntity t)
        {
            var updateDefinition = Builders<TEntity>
                .Update
                .Set(e => e.Title, t.Title)
                .Set(e => e.Message, t.Message)
                .Set(e => e.MarketplaceId, t.MarketplaceId)
                .Set(e => e.ProductId, t.ProductId)
                .Set(e => e.PhotoUrl, t.PhotoUrl)
                .Set(e => e.Barcode, t.Barcode)
                .Set(e => e.StockCode, t.StockCode)
                .Set(e => e.CreatedDateTime, t.CreatedDateTime)
                .Set(e => e.ProductInformationId, t.ProductInformationId)
                .Set(e => e.ModelCode, t.ModelCode)
                .Set(e => e.ProductInformationName, t.ProductInformationName)
                .Set(e => e.ProductInformationMarketplaceId, t.ProductInformationMarketplaceId)
                .Set(e => e.Type, t.Type);

            await this
                ._mongoCollection
                .UpdateOneAsync<TEntity>(e => e.ProductInformationMarketplaceId == t.ProductInformationMarketplaceId && e.Type == t.Type, updateDefinition, new UpdateOptions { IsUpsert = true });
        }

        public async Task DeleteAsync(string id)
        {
            var deleteResult = await this._mongoCollection.DeleteOneAsync(_ => _.Id == id);
        }

        public async Task<List<TEntity>> GetAsync(int page, int recordsCount, string marketplaceId, string barcode, string stockCode, string type, string modelCode, string productInformationName)
        {
            var filterBuilder = Builders<TEntity>.Filter;
            var filter = filterBuilder.Empty;

            if (!string.IsNullOrEmpty(marketplaceId))
                filter &= filterBuilder.Eq("MarketplaceId", marketplaceId);

            if (!string.IsNullOrEmpty(barcode))
                filter &= filterBuilder.Eq("Barcode", barcode);

            if (!string.IsNullOrEmpty(stockCode))
                filter &= filterBuilder.Eq("StockCode", stockCode);

            if (!string.IsNullOrEmpty(type))
            {
                var types = type.Split(new char[] { ',' }, StringSplitOptions.TrimEntries).AsEnumerable();
                filter &= filterBuilder.In("Type", types);
            }

            if (!string.IsNullOrEmpty(modelCode))
                filter &= filterBuilder.Eq("ModelCode", modelCode);

            if (!string.IsNullOrEmpty(productInformationName))
                filter &= filterBuilder.Regex("ProductInformationName", new Regex(productInformationName, RegexOptions.None));



            var query = this._mongoCollection.Find(filter);

            return await query.SortByDescending(x => x.CreatedDateTime).Skip(page * recordsCount).Limit(recordsCount).ToListAsync();
        }
        #endregion
    }
}
