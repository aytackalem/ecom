﻿using Helpy.Services.Notification.Core.Application.Interfaces.Finders;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.DeleteNotification;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.InsertNotification;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Extensions;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationCount;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationQuery;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.Notification.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationController : ControllerBase
    {
        #region Fields
        private readonly IMediator _mediator;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public NotificationController(IMediator mediator, IDbNameFinder dbNameFinder)
        {
            _mediator = mediator;
            _dbNameFinder = dbNameFinder;
        }
        #endregion

        #region Methods
        [HttpGet("Count")]
        public async Task<IActionResult> Count([FromQuery] GetNotificationCountQuery query)
        {
            this._dbNameFinder.Name = query.GetDbName();

            return Ok(await this._mediator.Send(query));
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetNotificationQuery query)
        {
            this._dbNameFinder.Name = query.GetDbName();

            return Ok(await this._mediator.Send(query));
        }

        [HttpPost]
        public async Task<IActionResult> Post(InsertNotificationCommand command)
        {
            this._dbNameFinder.Name = command.GetDbName();
            if (string.IsNullOrEmpty(command.PhotoUrl))
            {
                command.PhotoUrl = "/img/product/0/no-image.jpg";
            }

            return Ok(await this._mediator.Send(command));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] DeleteNotificationCommand command)
        {
            this._dbNameFinder.Name = command.GetDbName();

            return Ok(await this._mediator.Send(command));
        }
        #endregion
    }
}
