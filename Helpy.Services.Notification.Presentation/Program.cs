using Helpy.Services.Notification.Core.Application.Interfaces.Finders;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Core.Domain.Entities;
using Helpy.Services.Notification.Persistence.Finders;
using Helpy.Services.Notification.Presentation.Persistence.Repositories.MongoDb;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Reflection;

namespace Helpy.Services.Notification.Presentation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddMediatR(o => o.RegisterServicesFromAssembly(assembly));
            builder.Services.AddAutoMapper(assembly);
            //builder
            //    .Services
            //    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
            //    {
            //        o.Audience = "resource_panel";
            //        o.Authority = "https://identity.helpy.com.tr";
            //    });

            builder.Services.Configure<MongoConfiguration>(builder.Configuration.GetSection("MongoConfiguration"));
            builder.Services.AddSingleton<MongoConfiguration>(serviceProvider =>
            {
                return serviceProvider.GetRequiredService<IOptions<MongoConfiguration>>().Value;
            });
            builder.Services.AddScoped<IRepository<Notification.Core.Domain.Entities.Notification>, NotificationRepository>();
            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();

            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}