﻿namespace Helpy.Services.Notification.Core.Application.Interfaces.Repositories
{
    public interface IRepository<T> where T : class
    {
        #region Methods
        Task CreateAsync(T t);

        Task UpsertAsync(T t);

        Task DeleteAsync(string id);

        Task<List<T>> GetAsync(int page, int pageRecordsCount, string marketplaceId, string barcode, string stockCode, string type, string modelCode, string productInformationName);

        Task<long> CountAsync(string marketplaceId, string barcode, string stockCode, string type, string modelCode, string productInformationName);
        #endregion
    }
}
