﻿namespace Helpy.Services.Notification.Core.Application.Interfaces.Finders
{
    public interface IDbNameFinder
    {
        #region Properties
        public string Name { get; set; } 
        #endregion
    }
}
