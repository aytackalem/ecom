﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Extensions
{
    public static class EventBaseExtension
    {
        #region Methods
        public static string GetDbName(this EventBase eventBase)
        {
            return $"{eventBase.Brand}_{eventBase.CompanyId}";
        }
        #endregion
    }
}
