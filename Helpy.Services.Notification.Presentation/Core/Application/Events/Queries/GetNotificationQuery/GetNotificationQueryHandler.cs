﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationQuery
{
    public class GetNotificationQueryHandler : GetNotificationHandlerBase<Notification.Core.Domain.Entities.Notification, GetNotificationQuery>
    {
        public GetNotificationQueryHandler(IMapper mapper, IRepository<Notification.Core.Domain.Entities.Notification> repository) : base(mapper, repository)
        {
        }
    }
}
