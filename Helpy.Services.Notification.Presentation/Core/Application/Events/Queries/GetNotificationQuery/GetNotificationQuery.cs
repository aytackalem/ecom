﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Common;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationQuery
{
    public class GetNotificationQuery : GetNotificationQueryBase, IRequest<PagedResponse<GetNotification>>
    {
    }
}
