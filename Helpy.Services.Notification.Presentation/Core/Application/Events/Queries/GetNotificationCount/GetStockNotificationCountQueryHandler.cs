﻿using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationCount
{
    public class GetNotificationCountQueryHandler : GetCountHandlerBase<Notification.Core.Domain.Entities.Notification, GetNotificationCountQuery>
    {
        public GetNotificationCountQueryHandler(IRepository<Notification.Core.Domain.Entities.Notification> repository) : base(repository)
        {
        }
    }
}
