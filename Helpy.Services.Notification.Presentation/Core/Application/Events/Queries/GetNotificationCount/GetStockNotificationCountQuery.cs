﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.GetNotificationCount
{
    public class GetNotificationCountQuery : GetCountQueryBase, IRequest<DataResponse<long>>
    {
    }
}
