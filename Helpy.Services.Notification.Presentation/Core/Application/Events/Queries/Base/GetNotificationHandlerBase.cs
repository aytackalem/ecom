﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Common;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MediatR;
using System.Linq;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base
{
    public abstract class GetNotificationHandlerBase<TEntity, TRequest> : IRequestHandler<TRequest, PagedResponse<GetNotification>>
        where TEntity : class
        where TRequest : GetNotificationQueryBase, IRequest<PagedResponse<GetNotification>>
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IRepository<TEntity> _repository;
        #endregion

        #region Constructors
        public GetNotificationHandlerBase(IMapper mapper, IRepository<TEntity> repository)
        {
            #region Fields
            _mapper = mapper;
            _repository = repository;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<PagedResponse<GetNotification>> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return await ExceptionHandler.HandleAsync<PagedResponse<GetNotification>>(async response =>
            {
                if (request.PageRecordsCount == 0) request.PageRecordsCount = 1000;

                var entities = await _repository.GetAsync(request.Page, request.PageRecordsCount, request.MarketplaceId, request.Barcode, request.Stockcode, request.Type, request.ModelCode, request.ProductInformationName);


                response.Page = request.Page;
                response.PageRecordsCount = request.PageRecordsCount;
                response.RecordsCount = await _repository.CountAsync(request.MarketplaceId, request.Barcode, request.Stockcode, request.Type, request.ModelCode, request.ProductInformationName);

                response.Data = _mapper.Map<List<GetNotification>>(entities);
                response.Success = true;
                response.Message = "Uyarı başarılı bir şekilde getirildi";
            });
        }
        #endregion
    }
}
