﻿using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base
{
    public abstract class GetCountHandlerBase<TEntity, TRequest> : IRequestHandler<TRequest, DataResponse<long>>
        where TEntity : class
        where TRequest : GetCountQueryBase, IRequest<DataResponse<long>>
    {
        #region Fields
        private readonly IRepository<TEntity> _repository;
        #endregion

        #region Constructors
        public GetCountHandlerBase(IRepository<TEntity> repository)
        {
            #region Fields
            _repository = repository;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<long>> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<long>>(async response =>
            {
                response.Data = await _repository.CountAsync(null, null, null, null, null, null);
                response.Success = true;
                response.Message = "Uyarı başarılı bir şekilde getirildi";
            });
        }
        #endregion
    }
}
