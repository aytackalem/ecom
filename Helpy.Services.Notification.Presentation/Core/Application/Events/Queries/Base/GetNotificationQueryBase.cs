﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Base
{
    public abstract class GetNotificationQueryBase : EventBase
    {
        #region Properties
        public int Page { get; set; }

        public int PageRecordsCount { get; set; }

        public string? MarketplaceId { get; set; }

        public string? Stockcode { get; set; }

        public string? Barcode { get; set; }

        public string? ModelCode { get; set; }

        public string? ProductInformationName { get; set; }

        public string? Type { get; set; }
        #endregion
    }
}
