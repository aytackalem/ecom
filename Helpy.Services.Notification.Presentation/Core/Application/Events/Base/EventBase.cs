﻿namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Base
{
    public abstract class EventBase
    {
        #region Properties
        public string Brand { get; set; }

        public int CompanyId { get; set; }
        #endregion
    }
}
