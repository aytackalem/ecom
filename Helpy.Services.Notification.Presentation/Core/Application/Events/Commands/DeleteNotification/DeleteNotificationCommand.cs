﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.DeleteNotification
{
    public class DeleteNotificationCommand : DeleteNotificationBase, IRequest<NoContentResponse>
    {
    }
}
