﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.DeleteNotification
{
    public class DeleteNotificationCommandHandler : DeleteNotificationHandlerBase<Notification.Core.Domain.Entities.Notification, DeleteNotificationCommand>
    {
        #region Constructors
        public DeleteNotificationCommandHandler(IMapper mapper, IRepository<Notification.Core.Domain.Entities.Notification> repository) : base(mapper, repository)
        {
        }
        #endregion
    }
}
