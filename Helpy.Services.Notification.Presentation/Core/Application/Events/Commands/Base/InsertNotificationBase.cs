﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base
{
    public abstract class InsertNotificationBase : EventBase
    {
        #region Properties
        public string? Title { get; set; }

        public string? Message { get; set; }

        public string MarketplaceId { get; set; }

        public int ProductId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string? PhotoUrl { get; set; } = String.Empty;

        public string Barcode { get; set; } = String.Empty;

        public string StockCode { get; set; } = String.Empty;

        public DateTime CreatedDateTime { get; set; }

        public string Type { get; set; }

        public int ProductInformationId { get; set; }

        public string ModelCode { get; set; } = String.Empty;

        public string ProductInformationName { get; set; } = String.Empty;
        #endregion
    }
}
