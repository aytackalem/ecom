﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base
{
    public abstract class DeleteNotificationHandlerBase<TEntity, TRequest> : IRequestHandler<TRequest, NoContentResponse>
        where TEntity : class
        where TRequest : DeleteNotificationBase, IRequest<NoContentResponse>, new()
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IRepository<TEntity> _repository;
        #endregion

        #region Constructors
        protected DeleteNotificationHandlerBase(IMapper mapper, IRepository<TEntity> repository)
        {
            this._mapper = mapper;
            this._repository = repository;
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                await _repository.DeleteAsync(request.Id);

                response.Success = true;
                response.Message = "Uyarı başarılı bir şekilde silindi.";
            });
        }
        #endregion
    }
}
