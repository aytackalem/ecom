﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Core.Domain.Entities.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base
{
    public abstract class InsertNotificationHandlerBase<TEntity, TRequest> : IRequestHandler<TRequest, NoContentResponse>
        where TEntity : EntityBase
        where TRequest : IRequest<NoContentResponse>, new()
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IRepository<TEntity> _repository;
        #endregion

        #region Constructors
        protected InsertNotificationHandlerBase(IMapper mapper, IRepository<TEntity> repository)
        {
            this._mapper = mapper;
            this._repository = repository;
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = _mapper.Map<TEntity>(request);
                entity.CreatedDateTime = DateTime.Now;

                await _repository.UpsertAsync(entity);

                response.Success = true;
                response.Message = "Uyarı başarılı bir şekilde oluşturuldu.";
            });
        }
        #endregion
    }
}
