﻿using Helpy.Services.Notification.Presentation.Core.Application.Events.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base
{
    public abstract class DeleteNotificationBase : EventBase
    {
        #region Properties
        public string Id { get; set; }
        #endregion
    }
}
