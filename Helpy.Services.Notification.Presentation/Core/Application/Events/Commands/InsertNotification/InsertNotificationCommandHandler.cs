﻿using AutoMapper;
using Helpy.Services.Notification.Core.Application.Interfaces.Repositories;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.Base;

namespace Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.InsertNotification
{
    public class InsertNotificationCommandHandler : InsertNotificationHandlerBase<Notification.Core.Domain.Entities.Notification, InsertNotificationCommand>
    {
        #region Constructors
        public InsertNotificationCommandHandler(IMapper mapper, IRepository<Notification.Core.Domain.Entities.Notification> repository) : base(mapper, repository)
        {
        }
        #endregion
    }
}
