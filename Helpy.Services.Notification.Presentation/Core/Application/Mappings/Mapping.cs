﻿using AutoMapper;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Commands.InsertNotification;
using Helpy.Services.Notification.Presentation.Core.Application.Events.Queries.Common;

namespace Helpy.Services.Notification.Presentation.Core.Application.Mappings
{
    public class Mapping : Profile
    {
        #region Constructors
        public Mapping()
        {
            CreateMap<Notification.Core.Domain.Entities.Notification, GetNotification>();

            CreateMap<InsertNotificationCommand, Notification.Core.Domain.Entities.Notification>();
        }
        #endregion
    }
}
