﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kiracıların sahibi olduğu etki alanlarına ait ayarları temsil eden sınıf.
    /// </summary>
    public class DomainSetting : TenantEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Kiracıya ait görsellere ulaşım için kullanılacak url bilgisi.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Kiracıya ait görsellerin saklanacağı dizin bilgisi.
        /// </summary>
        public string ImagePath { get; set; }
        #endregion

        #region Navigation Property
        public Domain Domain { get; set; }
        #endregion
    }
}