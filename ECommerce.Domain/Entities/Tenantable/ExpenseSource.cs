﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    public class ExpenseSource : TenantEntityBase<int>
    {
        #region Properties
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<Expense> Expenses { get; set; }
        #endregion
    }
}
