﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kiracıların sahibi olduğu etki alanları temsil eden sınıf.
    /// </summary>
    public class Domain : TenantEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Şirket adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Property
        public DomainSetting DomainSetting { get; set; }

        public List<Company> Companies { get; set; }

        //public List<MarketplaceVariantValue> MarketplaceVariantValues { get; set; }
        #endregion
    }
}