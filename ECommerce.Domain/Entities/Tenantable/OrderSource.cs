﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş kaynağını temsil eden sınıf.
    /// </summary>
    public class OrderSource  : TenantEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Şirket adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<Order> Orders { get; set; }
        #endregion
    }
}
