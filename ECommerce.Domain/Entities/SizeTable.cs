﻿using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities;

public class SizeTable
{
    public int Id { get; set; }

    public int ProductId { get; set; }

    public bool Generated { get; set; }

    public string FileName { get; set; }

    public Product Product { get; set; }

    public List<SizeTableItem> SizeTableItems { get; set; }
}

public class SizeTableItem
{
    public int Id { get; set; }

    public int SizeTableId { get; set; }

    public string Size { get; set; }

    /// <summary>
    /// Göğüs
    /// </summary>
    public string Chest { get; set; }

    /// <summary>
    /// Bel
    /// </summary>
    public string Waist { get; set; }

    /// <summary>
    /// Basen
    /// </summary>
    public string Hip { get; set; }
}