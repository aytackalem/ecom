﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş tipini temsil eden sınıf.
    /// </summary>
    public class OrderType : EntityBase<string>
    {
        #region Property
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili tipe ait siparişler.
        /// </summary>
        public List<Order> Orders { get; set; }

        /// <summary>
        /// Sipariş tip dil bilgileri.
        /// </summary>
        public List<OrderTypeTranslation> OrderTypeTranslations { get; set; }
        #endregion
    }
}
