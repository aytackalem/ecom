﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Dilleri temsil eden sınıf.
    /// </summary>
    public class Language : EntityBase<string>, IActivable
    {
        #region Properties
        /// <summary>
        /// Dil adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Dil aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Dil ile oluşturulmuş siparişler.
        /// </summary>
        public List<Order> Orders { get; set; }
        #endregion
    }
}
