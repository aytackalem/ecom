﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Domain.Entities.Enum
{
    /// <summary>
    /// Banka listesini temsil eder.
    /// </summary>
    public enum BankType
    {
        [Description("HepsiPay")]
        HepsiPay = 1,
        [Description("Sipay")]
        Sipay = 2,
        [Description("Paytr")]
        PayTr = 3,

    }
}
