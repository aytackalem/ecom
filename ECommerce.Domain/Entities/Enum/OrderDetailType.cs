﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Domain.Entities.Enum
{
    public enum OrderDetailType
    {
        [Description("Başarılı")]
        Success = 1,
        [Description("İade")]
        Return = 2,
        [Description("Zayi İade")]
        Loss = 3,
        [Description("İptal")]
        Canceled = 4

    }
}
