﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Para birimlerini temsil eden sınıf.
    /// </summary>
    public class Currency : EntityBase<string>, IActivable
    {
        #region Properties
        /// <summary>
        /// Para birimi adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Para birimi aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Para biriminin kullanıldığı siparişler.
        /// </summary>
        public List<Order> Orders { get; set; }
        #endregion
    }
}
