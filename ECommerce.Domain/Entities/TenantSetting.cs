﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sistem içerisinde rol alan kiracılara ait ayarları temsil eden sınıf.
    /// </summary>
    public class TenantSetting : EntityBase<int>
    {
        #region Properties
        
        #endregion

        #region Navigation Properties
        public Tenant Tenant { get; set; }
        #endregion
    }
}
