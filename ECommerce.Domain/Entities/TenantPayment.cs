﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class TenantPayment : EntityBase<int>
    {
        #region Properties
        public int TenantId { get; set; }

        /// <summary>
        /// İlgili ödeme tipi ödenmiş mi?.
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// Lisans kodu.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Ödeme tutarı.
        /// </summary>
        public decimal Amount { get; set; }
        #endregion

        #region Navigation Properties
        public Tenant Tenant { get; set; }
        #endregion
    }
}
