﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class FeedTemplate : EntityBase<string>
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
