﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class ReceiptItem : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Kesim föyü id bilgisi.
        /// </summary>
        public int ReceiptId { get; set; }

        /// <summary>
        /// Ürün id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }
        #endregion

        #region Navigation Properties
        public Receipt Receipt { get; set; }

        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
