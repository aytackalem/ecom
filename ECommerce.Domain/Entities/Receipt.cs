﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kesim föyü sınıfı.
    /// </summary>
    public class Receipt : DomainEntityBase<int>, ICreatable
    {
        #region Properties
        /// <summary>
        /// Tedarikçi id bilgisi.
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// Birim metre değeri.
        /// </summary>
        public double UnitMeter { get; set; }

        /// <summary>
        /// Not.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Askı.
        /// </summary>
        public string Hanger { get; set; }

        /// <summary>
        /// Yıkama talimatı.
        /// </summary>
        public string Wash { get; set; }

        /// <summary>
        /// Şeffaf lastik.
        /// </summary>
        public string Rubber { get; set; }

        /// <summary>
        /// Ense etiketi.
        /// </summary>
        public string Ticket { get; set; }

        /// <summary>
        /// Kart.
        /// </summary>
        public string Card { get; set; }

        /// <summary>
        /// Model kodu.
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Teslim alınma durumunu temsil eder.
        /// </summary>
        public bool Delivered { get; set; }

        /// <summary>
        /// İptal edilmiş kayıtları temsil eder.
        /// </summary>
        public bool Cancelled { get; set; }

        /// <summary>
        /// Termin süresi.
        /// </summary>
        public DateTime Deadline { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Supplier Supplier { get; set; }

        public List<ReceiptItem> ReceiptItems { get; set; }
        #endregion
    }
}
