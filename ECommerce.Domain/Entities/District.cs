﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// İlçeleri temsil eden sınıf.
    /// </summary>
    public class District : EntityBase<int>
    {
        #region Properties
        /// <summary>
        /// İlçe adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// İlçenin ait olduğu şehre ait id bilgisi.
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// İlçe aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlçenin ait olduğu şehir.
        /// </summary>
        public City City { get; set; }

        /// <summary>
        /// İlçeye ait olan mahalleler.
        /// </summary>
        public List<Neighborhood> Neighborhoods { get; set; }
        #endregion
    }
}