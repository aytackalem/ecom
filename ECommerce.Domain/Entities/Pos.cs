﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sanal pos bilgilerini temsil eden sınıf.
    /// </summary>
    public class Pos : EntityBase<string>, IActivable
    {
        #region Properties
        /// <summary>
        /// Pos adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Pos aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// İşlem yapılacak varsayılan pos mu?
        /// </summary>
        public bool Default { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Pos konfigürasyonu.
        /// </summary>
        public List<PosConfiguration> PosConfigurations { get; set; }

        /// <summary>
        /// Online işlemleri posdan gerçekleştirilecek bankalar.
        /// </summary>
        public List<Bank> Banks { get; set; }
        #endregion
    }
}
