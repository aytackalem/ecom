﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Mobil, web gibi istemci uygulamalarını temsil eden sınıf.
    /// </summary>
    public class Application : EntityBase<string>
    {
        #region Properties
        /// <summary>
        /// Uygulama adı.
        /// </summary>
        public string Name { get; set; }
        #endregion
    }
}
