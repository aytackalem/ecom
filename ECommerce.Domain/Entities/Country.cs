﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ülkeleri temsil eden sınıf.
    /// </summary>
    public class Country : EntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Ülke adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ülke aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public List<City> Cities { get; set; }
        #endregion
    }
}