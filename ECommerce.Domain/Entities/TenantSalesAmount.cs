﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class TenantSalesAmount : EntityBase<int>
    {
        #region Properties
        public decimal Monthly { get; set; }

        public decimal HalfYearly { get; set; }

        public decimal Yearly { get; set; }
        #endregion

        #region Navigation Properties
        public Tenant Tenant { get; set; }
        #endregion
    }
}
