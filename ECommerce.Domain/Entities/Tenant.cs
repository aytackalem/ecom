﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sistem içerisinde rol alan kiracıları temsil eden sınıf.
    /// </summary>
    public class Tenant : EntityBase<int>
    {
        #region Properties
        public string Name { get; set; }

        public int QueueId { get; set; }

        /// <summary>
        /// Suresiz kullanim.
        /// </summary>
        public bool Indefinite { get; set; }

        /// <summary>
        /// Kiraci kontrat tarihi.
        /// </summary>
        public DateTime ContractDate { get; set; }

        /// <summary>
        /// Son kullanim tarihi.
        /// </summary>
        public DateTime ExpirationDate { get; set; }
        #endregion

        #region Navigation Properties
        public TenantInvoiceInformation TenantInvoiceInformation { get; set; }

        public TenantSetting TenantSetting { get; set; }

        public TenantSalesAmount TenantSalesAmount { get; set; }

        public List<TenantPayment> TenantPayments { get; set; }

        public List<Domain> Domains { get; set; }
        #endregion
    }
}
