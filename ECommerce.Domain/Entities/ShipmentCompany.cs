﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kargo firmalarını temsil eden sınıf.
    /// </summary>
    public class ShipmentCompany : EntityBase<string>
    {
        #region Properties
        /// <summary>
        /// Kargo firması adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Kargo firması yapılandırmaları.
        /// </summary>
        public List<ShipmentCompanyCompany> ShipmentCompanyCompanies { get; set; }
        #endregion
    }
}
