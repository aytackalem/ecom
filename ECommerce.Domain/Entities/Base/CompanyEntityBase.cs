﻿namespace ECommerce.Domain.Entities.Base
{
    public abstract class CompanyEntityBase<TId> : DomainEntityBase<TId>, ICompanyable
    {
        #region Properties
        public int CompanyId { get; set; }
        #endregion

        #region Navigation Properties
        public Company Company { get; set; }
        #endregion
    }
}
