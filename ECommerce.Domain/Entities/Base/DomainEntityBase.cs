﻿namespace ECommerce.Domain.Entities.Base
{
    public abstract class DomainEntityBase<TId> : TenantEntityBase<TId>, IDomainable
    {
        #region Properties
        public int DomainId { get; set; }
        #endregion

        #region Navigation Properties
        public Domain Domain { get; set; }
        #endregion
    }
}
