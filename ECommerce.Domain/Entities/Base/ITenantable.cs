﻿namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Kiracı varlıklar için temel olarak kullanılan sınıfı temsil eder.
    /// </summary>
    /// <typeparam name="TId">Id belirtilen tip olarak ayarlanır.</typeparam>
    public interface ITenantable
    {
        #region Properties
        /// <summary>
        /// Kiracı varlığı id bilgisi.
        /// </summary>
        public int TenantId { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Kiracı varlığını temsil eden sınıf.
        /// </summary>
        public Tenant Tenant { get; set; }
        #endregion
    }
}
