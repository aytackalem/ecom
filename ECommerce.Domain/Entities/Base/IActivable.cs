﻿using System;

namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Aktif/Pasif olabilen entitylerin uygulayacağı arayüz.
    /// </summary>
    public interface IActivable
    {
        #region Property
        /// <summary>
        /// Varlık aktif mi?
        /// </summary>
        bool Active { get; set; }
        #endregion
    }
}
