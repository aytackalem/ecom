﻿using System;

namespace ECommerce.Domain.Entities.Base
{
    public interface IDomainable
    {
        #region Properties
        public int DomainId { get; set; }
        #endregion

        #region Navigation Properties
        public Domain Domain { get; set; }
        #endregion
    }
}
