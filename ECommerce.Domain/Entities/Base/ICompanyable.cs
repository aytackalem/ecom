﻿using System;

namespace ECommerce.Domain.Entities.Base
{
    public interface ICompanyable
    {
        #region Properties
        public int CompanyId { get; set; }
        #endregion

        #region Navigation Properties
        public Company Company { get; set; }
        #endregion
    }
}
