﻿namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Varlık sınıfları için temel olarak kullanılan sınıfı temsil eder.
    /// </summary>
    /// <typeparam name="TId">Id belirtilen tip olarak ayarlanır.</typeparam>
    public abstract class EntityBase<TId>
    {
        #region Property
        /// <summary>
        /// Entity id bilgisi.
        /// </summary>
        public TId Id { get; set; }
        #endregion
    }
}
