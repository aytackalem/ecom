﻿namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Bir müşterinin oluşturabileceği entitylerin uygulayacağı arayüz.
    /// </summary>
    public interface ICustomerUserCreatable : ICreatable
    {
        #region Properties
        /// <summary>
        /// Kaydı oluşturan müşteriye ait id bilgisi.
        /// </summary>
        int CustomerUserId { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Kaydı oluşturan müşteri.
        /// </summary>
        CustomerUser CustomerUser { get; set; }
        #endregion
    }
}
