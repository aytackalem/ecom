﻿using System;

namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Silinebilen entitylerin uygulayacağı arayüz.
    /// </summary>
    public interface IDeletable
    {
        #region Property
        /// <summary>
        /// Varlık silinmiş mi?
        /// </summary>
        bool Deleted { get; set; }

        /// <summary>
        /// Varlığın silinmiş olduğu tarih.
        /// </summary>
        DateTime? DeletedDateTime { get; set; }
        #endregion
    }
}
