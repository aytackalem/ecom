﻿using System;

namespace ECommerce.Domain.Entities.Base
{
    /// <summary>
    /// Oluşturulabilen entitylerin uygulayacağı arayüz.
    /// </summary>
    public interface ICreatable
    {
        #region Property
        /// <summary>
        /// Kaydın oluşturulduğu tarih/saat bilgisi.
        /// </summary>
        DateTime CreatedDate { get; set; }
        #endregion
    }
}
