﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Companyable;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pazaryerlerini temsil eden sınıf.
    /// </summary>
    public class Marketplace : EntityBase<string>, IActivable
    {
        #region Properties
        /// <summary>
        /// Pazaryeri adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Pazaryeri aktif olarak kullanılıyor mu?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Pazaryerine ait marka bilisi Autocomplete'den mi alınsın
        /// </summary>
        public bool BrandAutocomplete { get; set; }

        public bool CreateProductTrackable { get; set; }

        public bool UpdateProductTrackable { get; set; }

        public bool UpdatePriceTrackable { get; set; }

        public bool UpdateStockTrackable { get; set; }

        public bool IsECommerce { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Pazaryeri kategorileri.
        /// </summary>
        public List<ECommerceCategory> MarketplaceCategories { get; set; }

        public List<ECommerceVariant> MarketplaceVariants { get; set; }

        public List<NebimMarketplaceSalesPersonnelCode> NebimMarketplaceSalesPersonnelCodes { get; set; }

        public List<NebimMarketplaceCreditCardCode> NebimMarketplaceCreditCardCodes { get; set; }
        #endregion
    }
}
