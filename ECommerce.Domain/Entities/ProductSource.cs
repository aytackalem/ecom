﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class ProductSource : EntityBase<string>
    {
        #region Properties
        public string Name { get; set; }
        #endregion
        //Manual = 0,
        //Xml = 1,
        //Simsar = 2,
        //Winka = 3,
        //Mikro = 4
    }
}