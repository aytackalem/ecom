﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pazaryeri isteklerinin tiplerini temsil eden sinif.
    /// CPR: Create Product
    /// UPR: Update Product
    /// UP: Update Price 
    /// US: Update Stock
    /// </summary>
    public class MarketplaceRequestType : EntityBase<string>
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
