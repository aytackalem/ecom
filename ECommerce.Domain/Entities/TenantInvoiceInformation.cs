﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sistem içerisinde rol alan kiracılara ait ayarları temsil eden sınıf.
    /// </summary>
    public class TenantInvoiceInformation : EntityBase<int>
    {
        #region Properties
        public string TaxOffice { get; set; }

        public string TaxNumber { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }
        #endregion

        #region Navigation Properties
        public Tenant Tenant { get; set; }
        #endregion
    }
}
