﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Muhasebe firmalarını temsil eden sınıf.
    /// </summary>
    public class Accounting : EntityBase<string>
    {
        #region Properties
        /// <summary>
        /// Muhasebe firması adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Muhasebe firması yapılandırmaları.
        /// </summary>
        public List<AccountingConfiguration> AccountingCompanyConfigurations { get; set; }
        #endregion
    }
}
