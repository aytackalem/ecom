﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kargo şirketi yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class ShipmentCompanyConfiguration : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Kargo şirketine ait id bilgisi.
        /// </summary>
        public int ShipmentCompanyCompanyId { get; set; }

        /// <summary>
        /// Anahtar adı.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili kargo şirketi sağlayıcısı.
        /// </summary>
        public ShipmentCompanyCompany ShipmentCompanyCompany { get; set; }
        #endregion
    }
}
