﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kargo şirketi yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class ShipmentCompanyCompany : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Kargo şirketine ait id bilgisi.
        /// </summary>
        public string ShipmentCompanyId { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili kargo şirketi sağlayıcısı.
        /// </summary>
        public ShipmentCompany ShipmentCompany { get; set; }


        public List<ShipmentCompanyConfiguration> ShipmentCompanyConfigurations { get; set; }
        #endregion
    }
}
