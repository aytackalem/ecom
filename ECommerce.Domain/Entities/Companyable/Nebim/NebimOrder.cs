﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Nebime atılan sipariş kayıtlarını temsil eder.
    /// </summary>
    public class NebimOrder : CompanyEntityBase<int>, ICreatable
    {
        #region Properties
        public string OrderNumber { get; set; }

        public string DocumentNumber { get; set; }

        public string HeaderId { get; set; }

        public string CurrAccCode { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Stok transfer fişi iptal mi
        /// </summary>
        public bool Cancelled { get; set; }

        /// <summary>
        /// Trendyol Micro siparişi mi
        /// </summary>
        public bool Micro { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; }
        public List<NebimOrderDetail> NebimOrderDetails { get; set; }
        #endregion
    }
}