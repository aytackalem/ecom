﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Nebime atılan sipariş detay kayıtlarını temsil eder.
    /// </summary>
    public class NebimOrderDetail : CompanyEntityBase<int>
    {
        #region Properties

        public string UsedBarcode { get; set; }

        public string LineId { get; set; }

        public int Qty1 { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal VatRate { get; set; }
        #endregion

        #region Navigation Properties
        public OrderDetail OrderDetail { get; set; }

        #endregion
    }
}