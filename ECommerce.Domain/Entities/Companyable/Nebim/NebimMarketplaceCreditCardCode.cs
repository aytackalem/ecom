﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Enum;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Nebime atılan pazar yeri bazlı personel kodlarını temsil eder.
    /// </summary>
    public class NebimMarketplaceCreditCardCode : CompanyEntityBase<int>
    {
        #region Properties
        public string MarketplaceId { get; set; }
        
        /// <summary>
        /// Pazaryerinden gelen E-ticaret siparişlerini eşleştiği bankaya göre atıyoruz
        /// Pazaryerinde boş gelir E-ticaret pazaryeri dolu gelir.
        /// </summary>
        public BankType? BankId { get; set; }

        public string PaymentTypeId { get; set; }

        public bool Micro { get; set; }
        
        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        public Marketplace Marketplace { get; set; }

        public PaymentType PaymentType { get; set; }

        #endregion
    }
}