﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Nebime atılan pazar yeri bazlı personel kodlarını temsil eder.
    /// </summary>
    public class NebimMarketplaceSalesPersonnelCode : CompanyEntityBase<int>
    {
        #region Properties
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Micro ticaret
        /// </summary>
        public bool Micro { get; set; }

        /// <summary>
        /// Nebimden gelen sorumluluk kodu
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Sorumlu olduğu web adresi
        /// </summary>
        public string SalesUrl { get; set; }
        #endregion

        #region Navigation Properties
        public Marketplace Marketplace { get; set; }
        #endregion
    }
}