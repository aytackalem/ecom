﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    public class MarketplaceCompany : CompanyEntityBase<int>, IActivable
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public bool Active { get; set; }

        public bool ReadOrder { get; set; }

        public bool CreateProduct { get; set; }

        public bool UpdatePrice { get; set; }

        /// <summary>
        /// Ürünlerde stok kontrolü yapılıp yapılmayacağı bilgisini barındıran özellik.
        /// </summary>
        public bool UpdateStock { get; set; }
        #endregion

        #region Navigation Properties
        public Marketplace Marketplace { get; set; }

        public List<MarketplaceConfiguration> MarketplaceConfigurations { get; set; }
        #endregion
    }
}
