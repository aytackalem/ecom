﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms sağlayıcısı yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class EmailProviderConfiguration : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// E-posta sağlayıcısına ait id bilgisi.
        /// </summary>
        public int EmailProviderCompanyId { get; set; }

        /// <summary>
        /// Anahtar adı.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili e-posta sağlayıcısı.
        /// </summary>
        public EmailProviderCompany EmailProviderCompany { get; set; }
        #endregion
    }
}
