﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    public class Expense : CompanyEntityBase<int>
    {
        #region Properties
        public int ExpenseSourceId { get; set; }

        public int? OrderSourceId { get; set; }

        public string MarketplaceId { get; set; }

        public int? ProductInformationId { get; set; }

        public DateTime ExpenseDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Amount { get; set; }

        public string Note { get; set; }
        #endregion

        #region Navigation Properties
        public ExpenseSource ExpenseSource { get; set; }

        public OrderSource OrderSource { get; set; }

        public Marketplace Marketplace { get; set; }

        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
