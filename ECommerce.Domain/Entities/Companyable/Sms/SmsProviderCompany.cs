﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms sağlayıcısı yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class SmsProviderCompany : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Sms sağlayıcısına ait id bilgisi.
        /// </summary>
        public string SmsProviderId { get; set; }
  
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sms sağlayıcısı.
        /// </summary>
        public SmsProvider SmsProvider { get; set; }


        public List<SmsProviderConfiguration> SmsProviderConfigurations { get; set; }
        #endregion
    }
}
