﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms şablonlarını temsil eden sınıf.
    /// </summary>
    public class SmsTemplate : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// İlgili sipariş tipine ait id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// İlgili ödeme tipine ait id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// Şablon adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Şablon.
        /// </summary>
        public string Template { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş tipi.
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// İlgili ödeme tipi.
        /// </summary>
        public PaymentType PaymentType { get; set; }
        #endregion
    }
}
