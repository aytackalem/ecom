﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms sağlayıcısı yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class SmsProviderConfiguration : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Sms sağlayıcısına ait id bilgisi.
        /// </summary>
        public int SmsProviderCompanyId { get; set; }

        /// <summary>
        /// Anahtar adı.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sms sağlayıcısı.
        /// </summary>
        public SmsProviderCompany SmsProviderCompany { get; set; }
        #endregion
    }
}
