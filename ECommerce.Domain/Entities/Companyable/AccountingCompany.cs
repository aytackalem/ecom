﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Muhasebe firmalarını temsil eden sınıf.
    /// </summary>
    public class AccountingCompany : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Muhasebe firması adı.
        /// </summary>
        public string AccountingId { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Muhasebe firması yapılandırmaları.
        /// </summary>
        public Accounting Accounting { get; set; }


        public List<AccountingConfiguration> AccountingConfigurations { get; set; }
        #endregion
    }
}
