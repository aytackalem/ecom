﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş paketleme hareketini temsil eden sınıf.
    /// </summary>
    public class OrderPacking : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Paketleme yapan kullanıcı adı.
        /// </summary>
        public string PackedUsername { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; }

        
        #endregion
    }
}