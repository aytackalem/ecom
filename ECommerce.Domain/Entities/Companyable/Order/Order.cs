﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Siparişleri temsil eden sınıf.
    /// </summary>
    public class Order : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        public string UUId { get; set; }

        /// <summary>
        /// Siparişin oluşturuğu uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Sipariş kaynağına ait id bilgisi.
        /// </summary>
        public int OrderSourceId { get; set; }

        /// <summary>
        /// Siparişlerin kdv dahil indirim hariç toplam tutarı.
        /// </summary>
        public decimal ListTotal { get; set; }

        /// <summary>
        /// Sipariş kdv dahil toplam tutarı.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Sipariş kdv dahil toplam indirimi.
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Kargo Ücreti
        /// </summary>
        public decimal CargoFee { get; set; }

        /// <summary>
        /// Ek Ücret
        /// </summary>
        public decimal SurchargeFee { get; set; }

        /// <summary>
        /// Sepette Indirim Tutarı
        /// </summary>
        public decimal ShoppingCartDiscount { get; set; }

        /// <summary>
        /// Kdv dahil Komisyon Ücreti
        /// </summary>
        public decimal CommissionAmount { get; set; }

        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        public decimal VatExcListTotal { get; set; }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        public decimal VatExcTotal { get; set; }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        public decimal VatExcDiscount { get; set; }

        /// <summary>
        /// İlgili para birimine ait id bilgisi.
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// Doviz kuru. TL icin 1 degeri atilir.
        /// </summary>
        public decimal ExchangeRate { get; set; }

        /// <summary>
        /// İlgili dile ait id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Siparişin ait olduğu pazar yerine ait id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// İlgili sipariş tipine ait id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Toplu faturalama. Modanisa gibi toplu faturalandirma yapilacak siparisler icin bu deger false atilir.
        /// </summary>
        public bool BulkInvoicing { get; set; }

        /// <summary>
        /// Terminal paketlenme tarihi
        /// </summary>
        public DateTime EstimatedPackingDate { get; set; }

        /// <summary>
        /// Siparişin ait olduğu pazar yerinin sipariş numarası
        /// </summary>
        public string MarketplaceOrderNumber { get; set; }

        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Sipariş Tarihi
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Mikro ihracat mi?
        /// </summary>
        public bool Micro { get; set; }

        /// <summary>
        /// Kurumsal fatura mı?
        /// </summary>
        public bool Commercial { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Siparişin oluşturuğu uygulama.
        /// </summary>
        public Application Application { get; set; }

        public Marketplace Marketplace { get; set; }

        public OrderSource OrderSource { get; set; }

        public OrderTechnicInformation OrderTechnicInformation { get; set; }

        /// <summary>
        /// Sipariş kargo bilgileri.
        /// </summary>
        public List<OrderShipment> OrderShipments { get; set; }

        /// <summary>
        /// Sipariş teslimat bilgileri.
        /// </summary>
        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        /// <summary>
        /// Sipariş fatura bilgileri.
        /// </summary>
        public OrderInvoiceInformation OrderInvoiceInformation { get; set; }

        /// <summary>
        /// Siparişe ait notlar.
        /// </summary>
        public List<OrderNote> OrderNotes { get; set; }

        /// <summary>
        /// Siparişe ait ödemeler.
        /// </summary>
        public List<Payment> Payments { get; set; }

        /// <summary>
        /// Siparişe ait detaylar.
        /// </summary>
        public List<OrderDetail> OrderDetails { get; set; }

        /// <summary>
        /// Siparişe ait iade detayları.
        /// </summary>
        public List<OrderReturnDetail> OrderReturnDetails { get; set; }

        /// <summary>
        /// Siparişe ait smsler.
        /// </summary>
        public List<OrderSms> OrderSmsses { get; set; }

        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// İlgili para birimi.
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// İlgili sipariş tipi.
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// Sipariş sonrası kazanılan para puan.
        /// </summary>
        public MoneyPoint MoneyPoint { get; set; }

        /// <summary>
        /// Sipariş paketleme hareketleri.
        /// </summary>
        public OrderPacking OrderPacking { get; set; }

        /// <summary>
        /// Sipariş toplama hareketleri.
        /// </summary>
        public OrderPicking OrderPicking { get; set; }

        public NebimOrder NebimOrder { get; set; }

        /// <summary>
        /// Sipariş fatura hareketleri.
        /// </summary>
        public OrderBilling OrderBilling { get; set; }

        public OrderReserve OrderReserve { get; set; }

        public List<OrderViewing> OrderViewings { get; set; }

        public List<MarketplaceCargoFee> MarketplaceCargoFees { get; set; }

        public MarketplaceInvoice MarketplaceInvoice { get; set; }
        #endregion
    }
}