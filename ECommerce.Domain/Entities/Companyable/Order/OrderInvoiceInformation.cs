﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş fatura bilgilerini temsil eden sınıf.
    /// </summary>
    public class OrderInvoiceInformation : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        /// <summary>
        /// Fatura ile ilgili işlemlerin yapıldığı muhasebe firması.
        /// </summary>
        public string AccountingCompanyId { get; set; }

        /// <summary>
        /// İlgili mahalleye ait id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Ad
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Soyad
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Açık adres.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// E-posta.
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Vergi dairesi.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Vergi numarası.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Resmi faturaya erişim sağlayacak olan url bilgisi.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Muhasebe firmasına iletilen resmi faturaya ilişkin takip bilgisi.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Resmileşmiş fatura numara bilgisi.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Sipariş fatura bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
