﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Enum;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş detayını temsil eden sınıf.
    /// </summary>
    public class OrderDetail : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        public string UUId { get; set; }

        /// <summary>
        /// Detayin ait olduğu siparişin id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Kdv dahil liste satış fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Kdv dahil birim fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Kdv dahil birim indirimi.
        /// </summary>
        public decimal UnitDiscount { get; set; }

        /// <summary>
        /// Kdv dahil birim maliyeti.
        /// </summary>
        public decimal UnitCost { get; set; }

        /// <summary>
        /// Kdv dahil Kargo Ücreti
        /// </summary>
        public decimal UnitCargoFee { get; set; }

        /// <summary>
        /// Kdv dahil Komisyon Ücreti
        /// </summary>
        public decimal UnitCommissionAmount { get; set; }

        /// <summary>
        /// Kdv hariç liste satış fiyatı.
        /// </summary>
        public decimal VatExcListUnitPrice { get; set; }

        /// <summary>
        /// Kdv hariç birim fiyatı.
        /// </summary>
        public decimal VatExcUnitPrice { get; set; }

        /// <summary>
        /// Kdv hariç birim indirimi.
        /// </summary>
        public decimal VatExcUnitDiscount { get; set; }

        /// <summary>
        /// Kdv hariç birim maliyeti.
        /// </summary>
        public decimal VatExcUnitCost { get; set; }

        /// <summary>
        /// Vergi oranı.
        /// </summary>
        public decimal VatRate { get; set; }

        /// <summary>
        /// Sipariş detayının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Ödeyen karşı taraf mı
        /// </summary>
        public bool Payor { get; set; }


        public OrderDetailType Type { get; set; }


        /// <summary>
        /// İade Fatura Numarası
        /// </summary>
        public string ReturnNumber { get; set; }


        /// <summary>
        /// İade Fatura sebebi
        /// </summary>
        public string ReturnDescription { get; set; }

        public DateTime? ReturnDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        public List<MarketplaceCommission> MarketplaceCommissions { get; set; }

        public NebimOrderDetail NebimOrderDetail { get; set; }
        #endregion
    }
}