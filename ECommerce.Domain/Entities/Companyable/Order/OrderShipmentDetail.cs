﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş kargo bilgilerini temsil eden sınıf.
    /// </summary>
    public class OrderShipmentDetail : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        /// <summary>
        /// İlgili kargo sipariş numarası.
        /// </summary>
        public int OrderShipmentId { get; set; }


        public string TrackingCode { get; set; }


        public string Weight { get; set; }
        /// <summary>
        /// Sipariş kargo bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public OrderShipment OrderShipment { get; set; }
        #endregion
    }
}
