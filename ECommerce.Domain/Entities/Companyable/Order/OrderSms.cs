﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş sms bilgilerini temsil eden sınıf.
    /// </summary>
    public class OrderSms : CompanyEntityBase<long>, ICreatable
    {
        #region Property
        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Mesaj.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Sipariş sms bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}
