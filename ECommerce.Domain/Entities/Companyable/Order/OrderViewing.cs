﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş depo görüntüleme hareketini temsil eden sınıf.
    /// </summary>
    public class OrderViewing : CompanyEntityBase<int>
    {
        #region Properties
        public int OrderId { get; set; }

        

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; }

        
        #endregion
    }
}