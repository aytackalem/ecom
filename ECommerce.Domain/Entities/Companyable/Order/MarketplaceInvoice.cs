﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    public class MarketplaceInvoice : CompanyEntityBase<int>, ICreatable
    {
        public string Number { get; set; }

        public DateTime CreatedDate { get; set; }

        public Order Order { get; set; }
    }
}
