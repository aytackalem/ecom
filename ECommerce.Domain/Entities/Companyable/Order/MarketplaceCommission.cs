﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ilgili urun detayina ait pazaryeri komisyon hareketlerini temsil eden sinif.
    /// </summary>
    public class MarketplaceCommission : CompanyEntityBase<long>
    {
        #region Property
        public int OrderDetailId { get; set; }

        /// <summary>
        /// Komisyon tutari.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Komisyon orani.
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Komisyon tipi Satis, iade gibi.
        /// </summary>
        public string Type { get; set; }
        #endregion

        #region Navigation Property
        public OrderDetail OrderDetail { get; set; }
        #endregion
    }
}
