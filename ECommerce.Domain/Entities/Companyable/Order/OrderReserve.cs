﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities.Companyable
{
    public class OrderReserve : CompanyEntityBase<int>, ICreatable
    {
        #region Properties
        /// <summary>
        /// Rezerve islemi sirasinda kullanilan belge numarasi.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Sipariş fatura bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; } 
        #endregion
    }
}
