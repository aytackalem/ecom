﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ilgili siparise ait pazaryeri kargo hareketlerini temsil eden sinif.
    /// </summary>
    public class MarketplaceCargoFee : CompanyEntityBase<long>
    {
        #region Property
        /// <summary>
        /// Siparis id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Kargo ucreti tutari.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Kargo ucreti tipi Satis, iade gibi.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Kargo desi bilgisi.
        /// </summary>
        public int Desi { get; set; }
        #endregion

        #region Navigation Property
        public Order Order { get; set; }
        #endregion
    }
}
