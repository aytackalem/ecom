﻿using ECommerce.Domain.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş notunu temsil eden sınıf.
    /// </summary>
    public class OrderNote : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Not.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 500, MinimumLength = 1)]
        public string Note { get; set; }

        /// <summary>
        /// Sipariş notunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}
