﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş kargo bilgilerini temsil eden sınıf.
    /// </summary>
    public class OrderShipment : CompanyEntityBase<int>, ICreatable
    {
        #region Property

        /// <summary>
        /// İlgili sipariş numarası
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public string ShipmentCompanyId { get; set; }

        /// <summary>
        /// Kargo takip numarası.
        /// </summary>
        public string TrackingUrl { get; set; }


        /// <summary>
        /// Kargo takip base64 formatı.
        /// </summary>
        public string TrackingBase64 { get; set; }

        /// <summary>
        /// kardo barkodu
        /// </summary>
        public string TrackingCode { get; set; }

        /// <summary>
        /// Pazaryerleri için açılmış paket numarası bilgisini içeren alandır.
        /// </summary>
        public string PackageNumber { get; set; }

        /// <summary>
        /// Ödeyen karşı taraf mı
        /// </summary>
        public bool Payor { get; set; }

        /// <summary>
        /// Sipariş kargo bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public ShipmentCompany ShipmentCompany { get; set; }


        public List<OrderShipmentDetail> OrderShipmentDetail { get; set; }
        #endregion
    }
}
