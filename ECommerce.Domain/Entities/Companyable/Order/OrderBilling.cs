﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş faturalandırma hareketini temsil eden sınıf.
    /// </summary>
    public class OrderBilling : CompanyEntityBase<int>, ICreatable
    {
        #region Properties
        public string InvoiceNumber { get; set; }

        public string ReturnNumber { get; set; }

        public string ReturnDescription { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; }
        #endregion
    }
}