﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş teslimat adresini temsil eden sınıf.
    /// </summary>
    public class OrderDeliveryAddress : CompanyEntityBase<int>, ICreatable
    {
        #region Property
        /// <summary>
        /// Alıcı adı soyadı
        /// </summary>
        public string Recipient { get; set; }

        /// <summary>
        /// Alıcı telefon numarası
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// İlgili mahalleye ait id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Açık adres.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Sipariş teslimat adresinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
