﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş paketleme hareketini temsil eden sınıf.
    /// </summary>
    public class OrderPicking : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Paketleme islemini yapan kullanici.
        /// </summary>
        public string PickedUsername { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Toplayıcı tarafından toplanan ürünlerin bulunduğu sepetin barkod bilgisi.
        /// </summary>
        public string PackerBarcode { get; set; }
        #endregion

        #region Navigation Properties
        public Order Order { get; set; }

        
        #endregion
    }
}