﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class ProductMarketplace : CompanyEntityBase<int>, IActivable
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int ProductId { get; set; }

        /// <summary>
        /// İkas için eklendi
        /// </summary>
        public string UUId { get; set; }

        /// <summary>
        /// Pazaryeri Marka id bilgisi.
        /// </summary>
        public string MarketplaceBrandCode { get; set; }

        /// <summary>
        /// Pazaryerine ait marka adı
        /// </summary>
        public string MarketplaceBrandName { get; set; }

        /// <summary>
        /// Pazaryeri kategori id bilgisi.
        /// </summary>
        public string MarketplaceCategoryCode { get; set; }

        /// <summary>
        /// Pazaryeri kategori id bilgisi.
        /// </summary>
        public string MarketplaceCategoryName { get; set; }

        /// <summary>
        /// Bağlı ürün bilgi id bilgisi.
        /// </summary>
        public string SellerCode { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// Teslimat süresi
        /// </summary>
        public int DeliveryDay { get; set; }
        #endregion

        #region Navigation Properties
        public Product Product { get; set; }
        #endregion
    }
}
