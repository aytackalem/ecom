﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteri iletişim bilgilerini temsil eden sınıf.
    /// </summary>
    public class CustomerContact  : CompanyEntityBase<int>, ICreatable
    {
        #region Properties
        

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// E-posta.
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Vergi dairesi.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Vergi numarası.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Müşteri iletişim bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Properties


        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }
        #endregion
    }
}