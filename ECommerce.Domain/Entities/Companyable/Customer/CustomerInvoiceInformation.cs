﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteri adreslerini temsil eden sınıflar. Örn: Ev, iş gibi.
    /// </summary>
    public class CustomerInvoiceInformation  : CompanyEntityBase<int>, ICreatable,IDeletable
    {
        #region Properties
        

        /// <summary>
        /// Adres başlığı
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Alıcı adı 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Alıcı soyadı 
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Mahalle id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Açık adres veya adres tarifi.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// E-posta.
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Vergi dairesi.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Vergi numarası.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Seçili adresi gösterir
        /// </summary>
        public bool Default { get; set; }

        /// <summary>
        /// Müşteri adresinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Müşteri adresi silmiş mi
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Müşteri Adresi silme tarihi
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }

        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
