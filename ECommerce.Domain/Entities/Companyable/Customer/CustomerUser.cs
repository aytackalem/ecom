﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteri üyelik bilgilerini temsil eden sınıf.
    /// </summary>
    public class CustomerUser  : CompanyEntityBase<int>
    {
        #region Properties
        public string CustomerRoleId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        /// <summary>
        /// Şifremi resetle dediğin zaman gelecek mail doğrulamadı kodu
        /// </summary>
        public Guid? Token { get; set; }

        /// <summary>
        /// Gerekli Token bitiş süresi
        /// </summary>
        public DateTime? TokenExpried { get; set; }

        /// <summary>
        /// Kullancısı eklenme süresi
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Müşterinin toplam bonusu
        /// </summary>
        public decimal MoneyPointAmount { get; set; }

        /// <summary>
        /// Login olmak için Sms doğrulması yapılır.
        /// Müşteri kodu girmişsse login olunur.
        /// </summary>
        public bool Verified { get; set; }
        #endregion

        #region Navigation Properties
        public Customer Customer { get; set; }

        public CustomerRole CustomerRole { get; set; }
        #endregion
    }
}
