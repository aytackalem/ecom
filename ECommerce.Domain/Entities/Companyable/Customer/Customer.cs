﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteleri temsil eden sınıf.
    /// </summary>
    public class Customer : CompanyEntityBase<int>, ICreatable
    {
        #region Properties
        /// <summary>
        /// Müşteri adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Müşteri soyadı.
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Müşteri kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Anonimleştirildi mi?
        /// </summary>
        public bool Anonymized { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Müşteri iletişim bilgileri.
        /// </summary>
        public CustomerContact CustomerContact { get; set; }

        /// <summary>
        /// Müşteriye kayıtlı adresler. Örn: Ev, iş gibi.
        /// </summary>
        public List<CustomerAddress> CustomerAddresses { get; set; }

        /// <summary>
        /// Müşteriye kayıtlı fatura adresleri. Örn: Ev, iş gibi.
        /// </summary>
        public List<CustomerInvoiceInformation> CustomerInvoiceInformations { get; set; }

        /// <summary>
        /// Müşteriye ait notlar. Bu notlar müşteriye gösterilmek için değil müşteri hakkında bilgi toplamak için kullanılmaktadır.
        /// </summary>
        public List<CustomerNote> CustomerNotes { get; set; }

        /// <summary>
        /// Müşteri kullanıcı bilgileri.
        /// </summary>
        public CustomerUser CustomerUser { get; set; }
        #endregion
    }
}