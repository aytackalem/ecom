﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteri notlarını temsil eden sınıf.
    /// </summary>
    public class CustomerNote  : CompanyEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Müşteri notunu oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Müşteri hakkında düşülmüş olan not.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Müşteri notunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }
        #endregion
    }
}
