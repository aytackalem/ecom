﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class MarketplaceConfiguration : CompanyEntityBase<int>
    {
        #region Properties
        public int MarketplaceCompanyId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        public MarketplaceCompany MarketplaceCompany { get; set; }
        #endregion
    }
}
