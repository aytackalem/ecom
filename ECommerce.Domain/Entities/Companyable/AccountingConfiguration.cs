﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kargo şirketi yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class AccountingConfiguration : CompanyEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Kargo şirketine ait id bilgisi.
        /// </summary>
        public int AccountingCompanyId { get; set; }

        /// <summary>
        /// Anahtar adı.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili kargo şirketi sağlayıcısı.
        /// </summary>
        public AccountingCompany AccountingCompany { get; set; }
        #endregion
    }
}
