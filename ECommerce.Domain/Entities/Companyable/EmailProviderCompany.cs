﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms sağlayıcısı yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class EmailProviderCompany : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// E-posta sağlayıcısına ait id bilgisi.
        /// </summary>
        public string EmailProviderId { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// E-posta sağlayıcılarını temsil eden sınıf.
        /// </summary>
        public EmailProvider EmailProvider { get; set; }

        /// <summary>
        /// İlgili e-posta sağlayıcısı.
        /// </summary>
        public List<EmailProviderConfiguration> EmailProviderConfigurations { get; set; }
        #endregion
    }
}
