﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pazaryeri kategorileri ile sistem kategorilerinin eşleşmesini temsil eden sınıf.
    /// </summary>
    public class MarketplaceBrandMapping : CompanyEntityBase<int>, ITenantable
    {
        #region Properties
        /// <summary>
        /// Pazaryeri id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Sistem kategori id bilgisi.
        /// </summary>
        public int BrandId { get; set; }

        /// <summary>
        /// Pazaryerine ait marka id bilgisi
        /// </summary>
        public string MarketplaceBrandCode { get; set; }

        /// <summary>
        /// Pazaryerine ait marka adı
        /// </summary>
        public string MarketplaceBrandName { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Pazaryeri kategorisi.
        /// </summary>
        public Marketplace Marketplace { get; set; }

        /// <summary>
        /// Sistem kategorisi.
        /// </summary>
        public Brand Brand { get; set; }
        #endregion
    }
}
