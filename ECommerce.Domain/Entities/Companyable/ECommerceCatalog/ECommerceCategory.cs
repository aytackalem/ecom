﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities.Companyable
{
    /// <summary>
    /// Pazaryeri kategorilerini temsil eden sınıf.
    /// </summary>
    public class ECommerceCategory : CompanyEntityBase<long>
    {
        #region Properties
        /// <summary>
        /// Pazaryeri id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// E-Ticaret kategori kodu.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Pazaryeri kategori adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Pazaryerine ait üst kategori id bilgisi.
        /// </summary>
        public string ParentId { get; set; }
        #endregion
    }
}
