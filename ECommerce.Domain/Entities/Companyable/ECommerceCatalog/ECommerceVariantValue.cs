﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities.Companyable
{
    /// <summary>
    /// Pazaryeri kategorileri ile pazaryeri varyasyonlarını temsil eden sınıf.
    /// </summary>
    public class ECommerceVariantValue : CompanyEntityBase<long>
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string ECommerceVariantCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
