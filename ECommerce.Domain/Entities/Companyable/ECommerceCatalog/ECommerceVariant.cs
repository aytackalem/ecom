﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities.Companyable
{
    /// <summary>
    /// Pazaryeri kategorileri ile pazaryeri varyasyonlarını temsil eden sınıf.
    /// </summary>
    public class ECommerceVariant : CompanyEntityBase<long>
    {
        #region Properties
        /// <summary>
        /// Pazaryeri id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        public string ECommerceCategoryCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Zorunlu alan.
        /// </summary>
        public bool Mandatory { get; set; }

        /// <summary>
        /// Özel değer girilebilir mi?
        /// </summary>
        public bool AllowCustom { get; set; }

        /// <summary>
        /// Birden çok değer girilebilir.
        /// </summary>
        public bool MultiValue { get; set; }

        public bool Variantable { get; set; }
        #endregion
    }
}
