﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationMarketplace : CompanyEntityBase<int>, IActivable
    {
        #region Properties
        /// <summary>
        /// İlgili ürün detaya ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Bağlantılı olduğu market
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Ürün indirimsiz fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Ürün Fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Stok kodu.
        /// </summary>
        public string StockCode { get; set; }

        /// <summary>
        /// Ürüne ait barkod bilgisi.
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// İkas için eklendi
        /// </summary>
        public string UUId { get; set; }

        /// <summary>
        /// İlgili ürünün pazaryerine gönderilip/gönderilmeyeceği bilgisini tutan özellik.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Pazaryerinde stok bilgisi güncellenmeli mi? bilgisini barındıran özellik.
        /// </summary>
        public bool DirtyStock { get; set; }

        /// <summary>
        /// Pazaryerinde fiyat bilgisi güncellenmeli mi? bilgisini barındıran özellik.
        /// </summary>
        public bool DirtyPrice { get; set; }

        /// <summary>
        /// Pazaryerinde ürün detay bilgisi iletilecek mi? bilgisini barındıran özellik.
        /// </summary>
        public bool DirtyProductInformation { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa isteiği ürünün fiyatını erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingPrice { get; set; }

        /// <summary>
        /// Ürün pazar yerinde açık mı bilgisini gösterir
        /// </summary>
        public bool Opened { get; set; }

        public bool OnSale { get; set; }

        /// <summary>
        /// Herhangi bir sebepten dolayi satisi durdurulmus/yasaklanmis stok ve fiyat guncellemesi yapilayan urunler icin "true" degeri set edilir.
        /// </summary>
        public bool Locked { get; set; }

        /// <summary>
        /// Notification'dan gelen ürünleri birdaha gösterme 
        /// </summary>
        public bool IgnoreSystemNotification { get; set; }

        /// <summary>
        /// Ürünün Pazaryerinde açılan Url'i
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Status jobunun en son çalıştığı zamanı gösterir
        /// </summary>
        public DateTime? StatusCheckDate { get; set; }

        /// <summary>
        /// Ürün fiyat ve ürün bilgilerinin güncellenmesini engelleyen ozellik. Stok guncellemesini etkilemez.
        /// </summary>
        public bool DisabledUpdateProduct { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        public ProductInformationMarketplaceBulkPrice ProductInformationMarketplaceBulkPrice { get; set; }

        public List<ProductInformationMarketplaceVariantValue> ProductInformationMarketplaceVariantValues { get; set; }
        #endregion
    }
}