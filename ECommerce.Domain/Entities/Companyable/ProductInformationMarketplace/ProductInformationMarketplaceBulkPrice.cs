﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Toplu fiyat bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationMarketplaceBulkPrice : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Ürün indirimsiz fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Ürün Fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }


        /// <summary>
        /// Erp/Xml için ürün fiyat güncellemesi açık mı?
        /// </summary>
        public bool AccountingPrice { get; set; }

        /// <summary>
        /// İlgili ürünün pazaryerine gönderilip/gönderilmeyeceği bilgisini tutan özellik.
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties


        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public ProductInformationMarketplace ProductInformationMarketplace { get; set; }
        #endregion
    }
}