﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class ProductInformationMarketplaceVariantValue : CompanyEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Bağlı ürün bilgi id bilgisi.
        /// </summary>
        public int ProductInformationMarketplaceId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }
        #endregion
    }
}
