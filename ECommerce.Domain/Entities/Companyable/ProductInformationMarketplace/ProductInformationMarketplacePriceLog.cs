﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductInformationMarketplacePriceLog : EntityBase<int>
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public DateTime CreatedDateTime { get; set; }
        #endregion
    }
}