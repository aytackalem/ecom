﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Bankaları temsil eden sınıf.
    /// </summary>
    public class Bank : EntityBase<string>, IActivable
    {
        #region Properties
        /// <summary>
        /// Bankaya ait online işlemlerin gerçekleştirileceği pos id bilgisi.
        /// </summary>
        public string PosId { get; set; }

        /// <summary>
        /// Banka adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Banka aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Bankaya ait bin numaraları.
        /// </summary>
        public List<Bin> Bins { get; set; }

        /// <summary>
        /// Bankaya ait online işlemlerin gerçekleştirileceği pos.
        /// </summary>
        public Pos Pos { get; set; }
        #endregion
    }
}
