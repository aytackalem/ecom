﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Stok sayim tiplerini temsil eden sinif.
    /// Orn: Devam Ediyor, Onay Bekleyen, Onaylanan.
    /// </summary>
    public class StocktakingType : EntityBase<string>
    {
        /// <summary>
        /// Stok tipine ait isim degeri.
        /// </summary>
        public string Name { get; set; }
    }
}
