﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Domain.Entities
{
    public class UpsCity : EntityBase<int>
    {
        #region Properties


        public string Name { get; set; }

        #endregion

    }
}
