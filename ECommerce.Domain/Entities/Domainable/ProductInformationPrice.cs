﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay fiyat bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationPrice : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// İlgili para birimine ait id bilgisi.
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// Ürün maliyeti herhangi bir farklı para biriminden günlük olanlar hesaplanmak isteniyor ise bu alan doldurulur. Çevrim yapılacak olan para birimine ait id bilgisi.
        /// </summary>
        public string ConversionCurrencyId { get; set; }

        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün detaya ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Ürün indirimsiz fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Ürün Fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Birim maliyeti.
        /// </summary>
        public decimal UnitCost { get; set; }

        /// <summary>
        /// Ürün maliyeti herhangi bir farklı para biriminden günlük olanlar hesaplanmak isteniyor ise bu alan doldurulur. Çevrim yapılacak olan farklı para birimi birim maliyeti.
        /// </summary>
        public decimal ConversionUnitCost { get; set; }

        /// <summary>
        /// Ürünün Gerçek indirimli fiyatı kampanya zamanı dolu olur
        /// </summary>
        public decimal? OriginalUnitPrice { get; set; }

        /// <summary>
        /// Vergi oranı.
        /// </summary>
        public decimal VatRate { get; set; }

        /// <summary>
        /// Ürünün oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa isteiği ürünün fiyatını erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingPrice { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili para birimi.
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// İlgili cost para birimi
        /// </summary>
        public Currency ConversionCurrency { get; set; }

        /// <summary>
        /// Ürün bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün bilgisi.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}