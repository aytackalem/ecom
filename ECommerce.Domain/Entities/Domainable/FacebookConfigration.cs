﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Facebook konfig temsil eden sınıf.
    /// </summary>
    public class FacebookConfigration  : DomainEntityBase<int>, IActivable
    {
        #region Properties
        

        /// <summary>
        /// kullanıcı kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Anahtar adı kullanıcı ekranda görür
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Anahtar key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Facebook code.
        /// </summary>
        public string Code { get; set; }   


        /// <summary>
        /// Marka kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Marka aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// Markayı oluşturan kullanıcı.
        /// </summary>
             
        #endregion
    }
}
