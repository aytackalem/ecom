﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Siparişe ait teknik bilgileri temsil eden sınıf.
    /// </summary>
    public class OrderTechnicInformation : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Siparişi oluşturan kullanıcının ip adresi.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Siparişi oluşturan kullanıcının tarayıcı bilgisi.
        /// </summary>
        public string Browser { get; set; }

        /// <summary>
        /// Siparişi oluşturan kullanıcının işletim sistemi.
        /// </summary>
        public string Platform { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}