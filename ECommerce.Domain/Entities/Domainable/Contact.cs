﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class Contact  : DomainEntityBase<int>
    {
        #region Properties
        public string Address { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Whatsapp { get; set; }

        public string Email { get; set; }

        

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        

        public List<ContactTranslation> ContactTranslations { get; set; }
        #endregion
    }
}
