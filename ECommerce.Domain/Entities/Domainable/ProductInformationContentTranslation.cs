﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün açıklama dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationContentTranslation : DomainEntityBase<int>
    {
        #region Property


        /// <summary>
        /// Ürün içerik dil bilgilerini oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ürüne ait Uzman görüşü
        /// </summary>
        public string ExpertOpinion { get; set; }

        /// <summary>
        /// Ürünye ait kısa açıklama.
        /// </summary>
        public string Content { get; set; }


        /// <summary>
        /// Ürünye ait içerik.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ürün kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ürün içerik bilgisi.
        /// </summary>
        public ProductInformationTranslation ProductInformationTranslation { get; set; }

        /// <summary>
        /// Ürünyi oluşturan kullanıcı.
        /// </summary>
        


        #endregion
    }
}