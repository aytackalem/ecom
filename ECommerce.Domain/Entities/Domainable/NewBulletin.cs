﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    public class NewBulletin : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Properties
        /// <summary>
        /// E-bülten Mail adresi
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Kullanıcı aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Kullanıcı Eklenme Tarihi
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Kullanıcı silindi mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Kullanıcı silinme zamanı?
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }

        /// <summary>
        /// Kullanıcı Ip Adresi
        /// </summary>
        public string IpAddress { get; set; }
        #endregion

        #region Navigation Properties

        public List<NewBulletinInformation> NewBulletinInformations { get; set; }
        #endregion

    }
}
