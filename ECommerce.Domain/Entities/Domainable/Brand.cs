﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Domainable;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Markaları temsil eden sınıf.
    /// </summary>
    public class Brand : DomainEntityBase<int>, IActivable
    {
        #region Property
        /// <summary>
        /// Marka kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>


        /// <summary>
        /// Marka adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Marka url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Marka kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Marka aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Markayı oluşturan kullanıcı.
        /// </summary>


        /// <summary>
        /// Markaya ait ürünler.
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Marka indirimleri.
        /// </summary>
        public List<BrandDiscountSetting> BrandDiscountSettings { get; set; }

        /// <summary>
        /// Marka vitrinleri.
        /// </summary>
        public List<Showcase> Showcases { get; set; }

        public List<MarketplaceBrandMapping> MarketplaceBrandMappings { get; set; }

        public List<XmlDefinationBrand> XmlDefinationBrands { get; set; }
        #endregion
    }
}
