﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay birden fazla ürün detayın birleişiminden oluştuğunda kullanıcak olan sınıf.
    /// </summary>
    public class ProductInformationCombine : DomainEntityBase<int>, IDeletable
    {
        #region Properties
        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ürün detay id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Ürün detayı oluşturan ilişkili ürün detay id bilgisi.
        /// </summary>
        public int ContainProductInformationId { get; set; }

        /// <summary>
        /// Ürün detayının kaç adet satılacağı bilgisi.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Ürünün oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Ürün bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        /// <summary>
        /// İlgili ürün detayı oluşturan ürün detay.
        /// </summary>
        public ProductInformation ContainProductInformation { get; set; }
        #endregion
    }
}