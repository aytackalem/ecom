﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori dil bilgisini temsil eden sınıf.
    /// </summary>
    public class CategoryTranslationBreadcrumb  : DomainEntityBase<int>
    {
        #region Properties
        

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Breadcrumb html.
        /// </summary>
        public string Html { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili kategori dil bilgisi.
        /// </summary>
        public CategoryTranslation CategoryTranslation { get; set; }

        
        #endregion
    }
}