﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürünü temsil eden sınıf.
    /// </summary>
    public class ProductTag : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// İlgili ürün id bilgisi.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// İlgili tag id bilgisi.
        /// </summary>
        public int TagId { get; set; }

        /// <summary>
        /// Kaydı oluşturan yöneticiye ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ürün oluşturulma tarihi.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ürünü oluşturan kullanıcı.
        /// </summary>
        

        public Product Product { get; set; }

        public Tag Tag { get; set; }
        #endregion
    }
}