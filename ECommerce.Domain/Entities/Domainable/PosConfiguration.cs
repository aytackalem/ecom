﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pos konfigürasyonunu temsil eden sınıf.
    /// </summary>
    public class PosConfiguration : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Anahtar kelime.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// İlgili pos'a ait id bilgisi.
        /// </summary>
        public string PosId { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili pos.
        /// </summary>
        public Pos Pos { get; set; }
        #endregion
    }
}
