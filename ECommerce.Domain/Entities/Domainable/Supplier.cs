﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün tedarikçilerini temsil eden sınıf.
    /// </summary>
    public class Supplier : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Genel indirimi oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Firma adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tedarikçinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Tedarik edilen ürünler.
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Genel indirimi oluşturan kullanıcı.
        /// </summary>
        
        #endregion
    }
}
