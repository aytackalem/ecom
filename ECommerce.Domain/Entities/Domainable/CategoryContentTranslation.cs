﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori açıklama dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class CategoryContentTranslation : DomainEntityBase<int>
    {
        #region Property
        

        /// <summary>
        /// Kategoriye ait içerik.
        /// </summary>
        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili kategori içerik bilgisi.
        /// </summary>
        public CategoryTranslation CategoryTranslation { get; set; }

        
        #endregion
    }
}