﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Varyasyon değerlerini temsil eden sınıf.
    /// </summary>
    public class VariantValueTranslation : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Varyasyon değerini oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili varyasyon değeri id bilgisi.
        /// </summary>
        public int VariantValueId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Varyasyon değeri.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Varyasyon değerinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Varyasyon değerini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili varyasyon değeri.
        /// </summary>
        public VariantValue VariantValue { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }
        #endregion
    }
}
