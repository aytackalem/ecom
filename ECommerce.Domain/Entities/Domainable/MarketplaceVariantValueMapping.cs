﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pazaryeri varyasyon değerleri ile sistem varyasyon değeleri eşleşmesini temsil eden sınıf.
    /// </summary>
    public class MarketplaceVariantValueMapping : DomainEntityBase<int>, ITenantable
    {
        #region Properties
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Sistem varyasyon id bilgisi.
        /// </summary>
        public int VariantValueId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        /// <summary>
        /// Pazaryeri varyasyon id bilgisi.
        /// </summary>
        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }

        /// <summary>
        /// E-Ticaret eşleşmeleri için kullanılacak olan özellik.
        /// </summary>
        public int? CompanyId { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Sistem kategorisi.
        /// </summary>
        public VariantValue VariantValue { get; set; }
        #endregion
    }
}
