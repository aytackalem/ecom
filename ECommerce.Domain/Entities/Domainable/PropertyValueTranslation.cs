﻿using ECommerce.Domain.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün özelliği değerini temsil eden sınıf.
    /// </summary>
    public class PropertyValueTranslation : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İlgili ürün özelliği değerine ait id bilgisi.
        /// </summary>
        public int PropertyValueId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        [Required]
        public string LanguageId { get; set; }

        /// <summary>
        /// Ürün özelliği değeri.
        /// </summary>
        public string Value { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Ürün özelliği değeri.
        /// </summary>
        public PropertyValue PropertyValue { get; set; }
        #endregion
    }
}