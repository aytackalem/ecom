﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori dil bilgisini temsil eden sınıf.
    /// </summary>
    public class CategoryTranslation : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Dil bilgisinin bağlı olduğu kategoriye ait id bilgisi.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Dil bilgisinin bağlı olduğu dile ait id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Dil bilgisi kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Dil bilgisi kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Kategori adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Kategori url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Kategorinin iconu
        /// </summary>
        public string IconFileName { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili kategori.
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Kategori dil bilgisini oluşturan kullanıcı.
        /// </summary>
        

        public CategoryTranslationBreadcrumb CategoryTranslationBreadcrumb { get; set; }

        public CategorySeoTranslation CategorySeoTranslation { get; set; }

        public CategoryContentTranslation CategoryContentTranslation { get; set; }
        #endregion
    }
}