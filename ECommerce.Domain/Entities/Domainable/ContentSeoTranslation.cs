﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori SEO dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class ContentSeoTranslation  : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İçerik SEO dil bilgilerini oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Başlık.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama.
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Anahtar kelimeler.
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Kategori kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// Kategoriyi oluşturan kullanıcı.
        /// </summary>
        


        public ContentTranslation ContentTranslation { get; set; }

        #endregion
    }
}