﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün ve ürün özelliğini temsil eden sınıf.
    /// </summary>
    public class ProductProperty : DomainEntityBase<int>, IActivable
    {
        #region Properties
        

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// İlgili ürün özelliği değerine ait id bilgisi.
        /// </summary>
        public int PropertyValueId { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// İlgili ürün özelliği değeri.
        /// </summary>
        public PropertyValue PropertyValue { get; set; }
        #endregion
    }
}