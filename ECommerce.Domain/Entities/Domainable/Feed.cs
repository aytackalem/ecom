﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    public class Feed : DomainEntityBase<int>
    {
        #region Properties
        public string FeedTemplateId { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }
        #endregion

        #region Navigation Properties
        public FeedTemplate FeedTemplate { get; set; }

        public List<FeedDetail> FeedDetails { get; set; }
        #endregion
    }
}
