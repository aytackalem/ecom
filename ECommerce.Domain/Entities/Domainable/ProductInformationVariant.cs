﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün varyasyon bilgisini temsil eden sınıftır.
    /// </summary>
    public class ProductInformationVariant : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Ürün varyasyon bilgisini oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün bilgisine ait id.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Varyasyon değer bilgisine ait id.
        /// </summary>
        public int VariantValueId { get; set; }

        /// <summary>
        /// Ürün varyasyon bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ürün varyasyon bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün bilgi.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        /// <summary>
        /// İlgili varyasyon değeri.
        /// </summary>
        public VariantValue VariantValue { get; set; }
        #endregion
    }
}