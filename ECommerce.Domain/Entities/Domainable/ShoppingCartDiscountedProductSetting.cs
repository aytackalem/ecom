﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sepette indirimli ürün ayarlarını temsil eden sınıf.
    /// </summary>
    public class ShoppingCartDiscountedProductSetting : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// İndirim kuponu oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// İndirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Sepette indirimli ürün.
        /// </summary>
        public ShoppingCartDiscountedProduct ShoppingCartDiscountedProduct { get; set; }
        #endregion
    }
}
