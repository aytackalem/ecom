﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class Information : DomainEntityBase<int>
    {
        #region Properties
        

        

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// E-Bülten gönderimi yapıldımı
        /// Otamatik job çalışır false olan kayıtları alır ve NewBulletins tablosuna göre kullancılara mail gönderir sonrasında true olarak güncellenir
        /// </summary>
        public bool NewBulletinSend { get; set; }
        #endregion

        #region Navigation Properties
        

        

        public List<InformationTranslation> InformationTranslations { get; set; }

        public List<NewBulletinInformation> NewBulletinInformations { get; set; }
        #endregion
    }
}
