﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Şirket detay bilgilerini temsil eden sınıftır.
    /// </summary>
    public class CompanyConfiguration  : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// İlgili şirket id bilgisi.
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// Anahtar adı kullanıcı ekranda görür
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Anahtar key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        public Company Company { get; set; }
        #endregion
    }
}
