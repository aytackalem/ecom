﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün özelliği değerini temsil eden sınıf.
    /// </summary>
    public class PropertyValue : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İlgili ürün özelliğine ait id bilgisi.
        /// </summary>
        public int PropertyId { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// Ürün özellikleri.
        /// </summary>
        public Property Property { get; set; }

        /// <summary>
        /// Ürün özellik değeri dil bilgileri.
        /// </summary>
        public List<PropertyValueTranslation> PropertyValueTranslations { get; set; }
        #endregion
    }
}