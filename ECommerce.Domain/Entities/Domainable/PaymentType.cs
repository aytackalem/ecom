﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş tiplerini temsil eden sınıf.
    /// </summary>
    public class PaymentType : EntityBase<string>, IActivable
    {
        #region Property
        /// <summary>
        /// Ödeme tipi aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ödeme tipi ayarları.
        /// </summary>
        public List<PaymentTypeDomain> PaymentTypeDomains { get; set; }

        /// <summary>
        /// İlgili tipe ait ödemeler.
        /// </summary>
        public List<Payment> Payments { get; set; }

        /// <summary>
        /// İlgili tipe ait dil bilgileri.
        /// </summary>
        public List<PaymentTypeTranslation> PaymentTypeTranslations { get; set; }

        /// <summary>
        /// Nebim ilgili ödeme tiplerini tutar
        /// </summary>
        public List<NebimMarketplaceCreditCardCode> NebimMarketplaceCreditCardCodes { get; set; }
        #endregion
    }
}
