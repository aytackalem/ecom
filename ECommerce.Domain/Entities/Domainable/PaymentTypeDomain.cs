﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sipariş tiplerini temsil eden sınıf.
    /// </summary>
    public class PaymentTypeDomain : DomainEntityBase<int>, ITenantable, IActivable
    {
        #region Property
        public string PaymentTypeId { get; set; }
        /// <summary>
        /// Ödeme tipi aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ödeme tipi ayarlarını temsil eden sınıf.
        /// </summary>
        public PaymentTypeSetting PaymentTypeSetting { get; set; }
        /// <summary>
        /// Ödeme tipi ayarları.
        /// </summary>
        public PaymentType PaymentType { get; set; }
        #endregion
    }
}
