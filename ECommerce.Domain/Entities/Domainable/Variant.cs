﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Varyasyonları temsil eden sınıf.
    /// </summary>
    public class Variant : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Ürünlerin hangi özellik ile dilimleneceğini belirler.
        /// </summary>
        public bool Slicer { get; set; }

        /// <summary>
        /// Ürünlerin hangi özellik ile varyantlanacağını belirler.
        /// </summary>
        public bool Varianter { get; set; }

        /// <summary>
        /// Varyasyona ait fotoğraflanabilirlik bilgisi.
        /// </summary>
        public bool Photoable { get; set; }

        /// <summary>
        /// Eğer açıksa bu variantta ait özellikler gösterilir siparişlerim ekranında ürün eşleme yaptğımız için bütün varinatlar geliyor. 
        /// Fatura çıkardığımız kağıtta bütün variantlar veya site tarafında bir sürü varinat özelliği geliyor sadece açık olan variantlar gelecek.
        /// </summary>
        public bool ShowVariant { get; set; }

        public string Code { get; set; }

        /// <summary>
        /// Varyasyonunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Varyasyonu oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Varyasyon dil bilgileri
        /// </summary>
        public List<VariantTranslation> VariantTranslations { get; set; }

        /// <summary>
        /// Varyasyona ait değerler.
        /// </summary>
        public List<VariantValue> VariantValues { get; set; }
        #endregion
    }
}
