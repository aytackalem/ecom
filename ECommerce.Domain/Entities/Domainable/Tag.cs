﻿using System;
using System.Collections.Generic;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Tagleri temsil eden sınıf.
    /// </summary>
    public class Tag : DomainEntityBase<int>, IDeletable
    {
        #region Properties
        /// <summary>
        /// Kaydı oluşturan yöneticiye ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Kaydın oluşturulduğu tarih/saat bilgisi.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// Etiket silindi mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Etiket silinme zamanı?
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Kaydı oluşturan yönetici.
        /// </summary>
        

        /// <summary>
        /// Tag dil bilgilerini temsil eden sınıf
        /// </summary>
        public List<TagTranslation> TagTranslation { get; set; }

        /// <summary>
        /// Ürün tagleri.
        /// </summary>
        public List<ProductTag> ProductTags { get; set; }
        #endregion
    }
}
