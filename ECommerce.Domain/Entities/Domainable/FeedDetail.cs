﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class FeedDetail : DomainEntityBase<int>
    {
        #region Properties
        public int FeedId { get; set; }

        public int ProductInformationId { get; set; }
        #endregion

        #region Navigation Properties
        public Feed Feed { get; set; }

        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
