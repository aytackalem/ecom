﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ödeme Loglarını temsil eden sınıf.
    /// </summary>
    public class PaymentLog : DomainEntityBase<int>, ITenantable, ICreatable
    {
        #region Property
        public string Pay3dMessage { get; set; }

        public string Check3dMessage { get; set; }

        /// <summary>
        /// Ödeme bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ödeme.
        /// </summary>
        public Payment Payment { get; set; }
        #endregion
    }
}
