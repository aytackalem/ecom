﻿using System;
using System.ComponentModel.DataAnnotations;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Tag dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class TagTranslation : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Tag id bilgisi.
        /// </summary>
        public int TagId { get; set; }

        /// <summary>
        /// Kaydı oluşturan yöneticiye ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Tag değeri.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Kaydın oluşturulduğu tarih/saat bilgisi.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        [Required]
        public string LanguageId { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili tag.
        /// </summary>
        public Tag Tag { get; set; }

        /// <summary>
        /// Kaydı oluşturan yönetici.
        /// </summary>
        

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }
        #endregion
    }
}
