﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    public class MarketplaceCategoryVariantValueMapping : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Bağlı ürün bilgi id bilgisi.
        /// </summary>
        public int MarketplaceCategoryMappingId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }
        #endregion

        #region Navigation Property
        public MarketplaceCategoryMapping MarketplaceCategoryMapping { get; set; }
        #endregion
    }
}
