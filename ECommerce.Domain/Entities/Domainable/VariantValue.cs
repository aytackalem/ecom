﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Varyasyon değerlerini temsil eden sınıf.
    /// </summary>
    public class VariantValue : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Varyasyon değerini oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili varyasyon id bilgisi.
        /// </summary>
        public int VariantId { get; set; }

        public string Code { get; set; }

        /// <summary>
        /// Varyasyon değerinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Varyasyon değerini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili varyasyon.
        /// </summary>
        public Variant Variant { get; set; }

        /// <summary>
        /// Varyasyon değeri dil bilgileri.
        /// </summary>
        public List<VariantValueTranslation> VariantValueTranslations { get; set; }

        public List<MarketplaceVariantValueMapping> MarketplaceVariantValueMappings { get; set; }
        #endregion
    }
}
