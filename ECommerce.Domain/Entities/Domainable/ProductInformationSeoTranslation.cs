﻿using System;
using System.ComponentModel.DataAnnotations;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün SEO dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationSeoTranslation :  DomainEntityBase<int>
    {
        #region Property

        /// <summary>
        /// Ürün SEO dil bilgilerini oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Başlık.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama.
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Anahtar kelimeler.
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Ürün SEO kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation Property


        /// <summary>
        /// Kategoriyi oluşturan kullanıcı.
        /// </summary>
        

        public ProductInformationTranslation ProductInformationTranslation { get; set; }
        #endregion
    }
}