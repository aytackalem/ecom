﻿using System;
using System.Collections.Generic;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Tagleri temsil eden sınıf.
    /// </summary>
    public class Label : DomainEntityBase<int>, IDeletable
    {
        #region Properties
        

        /// <summary>
        /// Kaydın oluşturulduğu tarih/saat bilgisi.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Etiket aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Etiket silindi mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Etiket silinme zamanı?
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }

        /// <summary>
        /// Etiket sıralaması
        /// </summary>
        public int DisplayOrder { get; set; }

        #endregion

        #region Navigation Property
        

        /// <summary>
        /// Tag dil bilgilerini temsil eden sınıf
        /// </summary>
        public List<LabelTranslation> LabelTranslations { get; set; }

        /// <summary>
        /// Ürün tagleri.
        /// </summary>
        public List<ProductLabel> ProductLabels { get; set; }

        #endregion
    }
}
