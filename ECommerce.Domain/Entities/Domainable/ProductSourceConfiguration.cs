﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities.Domainable
{
    public class ProductSourceConfiguration : DomainEntityBase<int>
    {
        #region Properties
        public int ProductSourceDomainId { get; set; }

        /// <summary>
        /// Anahtar adı kullanıcı ekranda görür
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Anahtar key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        public ProductSourceDomain ProductSourceDomain { get; set; }
        #endregion
    }
}