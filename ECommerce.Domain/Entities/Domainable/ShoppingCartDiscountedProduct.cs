﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sepette indirimli ürünleri temsil eden sınıf.
    /// </summary>
    public class ShoppingCartDiscountedProduct : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Property
        /// <summary>
        /// İndirimin geçerli olduğu uygulama id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// İndirim kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// İndirim kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// İndirim kaydının silindiği tarih.
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }

        /// <summary>
        /// İndirim kaydı silinmiş mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Genel indirim aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İndiriminin geçerli olacağı uygulama.
        /// </summary>
        public Application Application { get; set; }

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// İndirim kaydını oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Sepette indirimli ürün ayarları.
        /// </summary>
        public ShoppingCartDiscountedProductSetting ShoppingCartDiscountedProductSetting { get; set; }
        #endregion
    }
}
