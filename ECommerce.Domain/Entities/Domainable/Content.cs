﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// İçerikleri temsil eden sınıf.
    /// </summary>
    public class Content  : DomainEntityBase<int>
    {
        #region Properties
        

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        

        public List<ContentTranslation> ContentTranslations { get; set; }
        #endregion
    }
}
