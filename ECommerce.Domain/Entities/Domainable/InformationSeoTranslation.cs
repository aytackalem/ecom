﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori SEO dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class InformationSeoTranslation : DomainEntityBase<int>
    {
        #region Properties
        

        

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Başlık.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama.
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Anahtar kelimeler.
        /// </summary>
        public string MetaKeywords { get; set; }

        #endregion

        #region Navigation Properties
        

        

        public InformationTranslation InformationTranslation { get; set; }
        #endregion
    }
}