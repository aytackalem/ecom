﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Domainable;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürünü temsil eden sınıf.
    /// </summary>
    public class Product : DomainEntityBase<int>, IActivable
    {
        #region Constructors
        public Product()
        {
            #region Properties
            this.ProductInformations = new();
            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili marka id bilgisi.
        /// </summary>
        public int BrandId { get; set; }

        /// <summary>
        /// Ürünü tedarik eden tedarikçiye ait id bilgisi.
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// İlgili kategori id bilgisi.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Ürün aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Ürün oluşturulma tarihi.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Markette olmayan ürünleri yüklerken sadece panelde göster olarak yüklenir ve  kullanıcı ürün ekranında datasını görmez     
        /// Bu ürün panelde sadece siparişlerim ekranında gözükür ve sipariş kargoya çıkartılırken gözükür
        /// </summary>
        public bool IsOnlyHidden { get; set; }

        /// <summary>
        /// Ürün model kodu.
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa isteiği ürünün özellikleri erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingProperty { get; set; }

        /// <summary>
        /// Ürünün sisteme giriş yaptığı kaynak bilgisi.
        /// </summary>
        public int? ProductSourceDomainId { get; set; }

        /// <summary>
        /// İade oranı.
        /// </summary>
        public float ReturnRate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ürünü oluşturan kullanıcı.
        /// </summary>


        /// <summary>
        /// İlgili kategori.
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Kategori ürünleri.
        /// </summary>
        public List<CategoryProduct> CategoryProducts { get; set; }

        /// <summary>
        /// Ürün etiketleri.
        /// </summary>
        public List<ProductLabel> ProductLabels { get; set; }

        /// <summary>
        /// İlgili marka.
        /// </summary>
        public Brand Brand { get; set; }

        /// <summary>
        /// Ürünü tedarik eden tedarikçi.
        /// </summary>
        public Supplier Supplier { get; set; }

        /// <summary>
        /// Ürün bilgileri. Stok kodu, barkod gibi.
        /// </summary>
        public List<ProductInformation> ProductInformations { get; set; }

        /// <summary>
        /// Ürün sepet indirimleri.
        /// </summary>
        public List<ShoppingCartDiscountedProduct> ShoppingCartDiscountedProducts { get; set; }

        /// <summary>
        /// Ürün özellikleri.
        /// </summary>
        public List<ProductProperty> ProductProperties { get; set; }

        /// <summary>       
        /// Ürün tagleri.
        /// </summary>
        public List<ProductTag> ProductTags { get; set; }

        /// <summary>
        /// Pazaryerine gönderilecek ürünlerin detayları 
        /// </summary>
        public List<ProductMarketplace> ProductMarketplaces { get; set; }

        public List<ProductTranslation> ProductTranslations { get; set; }

        public ProductSourceDomain ProductSourceDomain { get; set; }

        /// <summary>
        /// Ürün kategorizasyon verileri.
        /// </summary>
        public List<ProductCategorization> ProductCategorizations { get; set; }

        public List<ProductGenericProperty> ProductGenericProperties { get; set; }

        public List<SizeTable> SizeTables { get; set; }
        #endregion
    }
}