﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities.Domainable
{
    /// <summary>
    /// Stok sayimlarini temsil eden sinif.
    /// </summary>
    public class Stocktaking : DomainEntityBase<int>
    {
        /// <summary>
        /// Stok tipine iliskin id degeri.
        /// </summary>
        public string StocktakingTypeId { get; set; }

        /// <summary>
        /// Stok sayimina verilen isim degeri.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Stok sayimina verilen aciklama degeri.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Sayim yapilacak urunlerin kontrol edilecegi model kodu kisiti.
        /// </summary>
        public string? ModelCode { get; set; }

        /// <summary>
        /// Sayim sirasinda okutulan urune ait grup kontrol edilmeli mi? Eger "true" set edilmis ise okutulan urune ait grup ile eslesen tum urunler sayima dahil edilir.
        /// </summary>
        public bool CheckGroup { get; set; }

        /// <summary>
        /// Sayim esnasinda manuel stok girilip girilemeyecegini belirleyen ozellik.
        /// </summary>
        public bool Manuel { get; set; }

        /// <summary>
        /// Stok sayiminin olusturuldugu tarih ve saat bilgisi.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }

        public StocktakingType StocktakingType { get; set; }

        public List<StocktakingDetail> StocktakingDetails { get; set; }
    }
}
