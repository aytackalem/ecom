﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class ContactTranslation  : DomainEntityBase<int>
    {
        #region Properties
        

        

        public string LanguageId { get; set; }

        public string Description { get; set; }      

        public DateTime CreatedDate { get; set; }

        public int ContactId { get; set; }
        #endregion

        #region Navigation Properties
        

        public Contact Contact { get; set; }

        public Language Language { get; set; }

        
        #endregion
    }
}
