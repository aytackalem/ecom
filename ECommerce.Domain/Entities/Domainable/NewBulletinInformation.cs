﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberler tablosunki haberler ve bültene kayıt olan mailler buraya eklenir
    /// </summary>
    public class NewBulletinInformation : DomainEntityBase<int>, ITenantable
    {
        #region Properties
        public int NewBulletinId { get; set; }

        public int InformationId { get; set; }

        /// <summary>
        /// Mail gönderilme Tarihi
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public NewBulletin NewBulletin { get; set; }

        public Information Information { get; set; }
        #endregion

    }
}
