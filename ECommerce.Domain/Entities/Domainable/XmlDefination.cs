﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities.Domainable;

public class XmlDefination : DomainEntityBase<int>
{
    public string Name { get; set; }

    public string FileName { get; set; }

    public List<string> Fields { get; set; }


    #region Navigation Property
    public List<XmlDefinationBrand> XmlDefinationBrands { get; set; }
    #endregion
}
