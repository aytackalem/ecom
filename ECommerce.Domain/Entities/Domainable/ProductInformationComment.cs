﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün yorumlarını temsil eden sınıf.
    /// </summary>
    public class ProductInformationComment : DomainEntityBase<int>, IActivable, ICreatable
    {
        #region Property
        /// <summary>
        /// İlgili ürün id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Yorum yapan kişi.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Yorum yapan kişinin verdiği Puan
        /// </summary>
        public float Rating { get; set; }

        /// <summary>
        /// Yorum.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Yorum aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Yorum onaylanmış mı?
        /// </summary>
        public bool Approved { get; set; }

        /// <summary>
        /// Yorum oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Kullanıcı Ip Adresi
        /// </summary>
        public string IpAddress { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}