﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kiracıların sistem içerisinde kullanmak istedikleri şirketleri temsil eden sınıf.
    /// </summary>
    public class Company  : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Şirket adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Şirket resmi adı.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Ikon dosya adı.
        /// </summary>
        public string FileName { get; set; }
        #endregion

        #region Navigation Properties
        public CompanySetting CompanySetting { get; set; }

        public CompanyContact CompanyContact { get; set; }

        public List<CompanyConfiguration> CompanyConfigurations { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }
        #endregion
    }
}
