﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kiracıların sistem içerisinde kullanmak istedikleri şirketleri temsil eden sınıf.
    /// </summary>
    public class CompanyContact : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Şirket adresi.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Web site adresi
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// Mersis No
        /// </summary>
        public string RegistrationNumber { get; set; }
        #endregion

        #region Navigation Properties
        public Company Company { get; set; }
        #endregion
    }
}
