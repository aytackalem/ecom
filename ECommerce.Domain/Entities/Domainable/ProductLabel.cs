﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürünü temsil eden sınıf.
    /// </summary>
    public class ProductLabel : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Ürüne ait id bilgisi.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Etikete ait id bilgisi.
        /// </summary>
        public int LabelId { get; set; }

        /// <summary>
        /// Kaydı oluşturan yöneticiye ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ürün oluşturulma tarihi.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Kategoriyi oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Ürün
        /// </summary>

        public Product Product { get; set; }

        /// <summary>
        /// Etiket.
        /// </summary>
        public Label Label { get; set; }
        #endregion
    }
}