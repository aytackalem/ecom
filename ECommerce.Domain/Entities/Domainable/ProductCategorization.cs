﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Ürün kategorizasyon sinifini temsil eder. Sistem yöneticileri tarafından kullanılır ve son kullanıcı ile alakalı değildir. Örneğin: "Stoğu Bitirilecek" olarak kategorilendirilen ürünlere personel farklı aksiyonlar alabilir.
/// </summary>
public class ProductCategorization : DomainEntityBase<int>
{
    #region Properties
    /// <summary>
    /// Kaydın ait olduğu ürün id bilgisi.
    /// </summary>
    public int ProductId { get; set; }

    /// <summary>
    /// Kategorizasyona ilişkin açıklama.
    /// </summary>
    public string Description { get; set; }
    #endregion

    #region Navigation Properties
    /// <summary>
    /// Kaydın ait olduğu ürün.
    /// </summary>
    public Product Product { get; set; }
    #endregion
}
