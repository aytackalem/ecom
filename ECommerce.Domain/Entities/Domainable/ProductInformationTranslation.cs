﻿using ECommerce.Domain.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationTranslation : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// İlgili ürün detay id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        [Required]
        public string LanguageId { get; set; }

        /// <summary>
        /// Ürün adı.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 300, MinimumLength = 1)]
        public string Name { get; set; }

        /// <summary>
        /// İlgili ürün varyasyon değerlerine ait açıklama.
        /// </summary>
        public string VariantValuesDescription { get; set; }

        public string FullVariantValuesDescription { get; set; }

        /// <summary>
        /// Ürün kısa açıklama.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 300, MinimumLength = 1)]
        public string SubTitle { get; set; }

        /// <summary>
        /// Ürün url.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 300, MinimumLength = 1)]
        public string Url { get; set; }

        /// <summary>
        /// Ürün dil bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Ürün dil bilgisinin oluşturulduğu zaman.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Ürün dil bilgisini oluşturan kullanıcı.
        /// </summary>
        

        public ProductInformationSeoTranslation ProductInformationSeoTranslation { get; set; }

        public ProductInformationContentTranslation ProductInformationContentTranslation { get; set; }

        public ProductInformationTranslationBreadcrumb ProductInformationTranslationBreadcrumb { get; set; }
        #endregion
    }
}