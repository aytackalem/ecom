﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// İçerikleri temsil eden sınıf.
    /// </summary>
    public class ContentTranslation  : DomainEntityBase<int>
    {
        #region Properties
        

        

        public string LanguageId { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ContentId { get; set; }
        #endregion

        #region Navigation Properties
        

        public ContentSeoTranslation ContentSeoTranslation { get; set; }

        public Content Content { get; set; }

        

        public Language Language { get; set; }

        public ContentTranslationContent ContentTranslationContent { get; set; }
        #endregion
    }
}
