﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// İndirim kuponlarını temsil eden sınıf.
    /// </summary>
    public class DiscountCoupon  : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İndirim kuponunun geçerli olacağı uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// İndirim kuponunu oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Müşteriye iletilecek olan kod.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// İndirimin kuponunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// İndirim kuponu aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İndirim kuponu ayarları.
        /// </summary>
        public DiscountCouponSetting DisountCouponSetting { get; set; }

        /// <summary>
        /// İndirim kuponunun geçerli olacağı uygulama.
        /// </summary>
        public Application Application { get; set; }

        /// <summary>
        /// İndirim kuponunu oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İndirim kuponunun kullanıldığı ödemeler.
        /// </summary>
        public List<Payment> Payments { get; set; }
        #endregion
    }
}
