﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Ürünler için özel alanların tanımlanmasını sağlar.
/// </summary>
public class ProductGenericProperty : DomainEntityBase<int>
{
    #region Properties
    /// <summary>
    /// Kaydın ait olduğu ürün id bilgisi.
    /// </summary>
    public int ProductId { get; set; }

    /// <summary>
    /// Özellik adı.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Özellik değeri.
    /// </summary>
    public string Value { get; set; }
    #endregion

    #region Navigation Properties
    /// <summary>
    /// Kaydın ait olduğu ürün.
    /// </summary>
    public Product Product { get; set; }
    #endregion
}
