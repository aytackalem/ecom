﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class FrequentlyAskedQuestion  : DomainEntityBase<int>, IDeletable
    {
        #region Properties
        

        

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        

        

        public List<FrequentlyAskedQuestionTranslation> FrequentlyAskedQuestionTranslations { get; set; }
        #endregion
    }
}
