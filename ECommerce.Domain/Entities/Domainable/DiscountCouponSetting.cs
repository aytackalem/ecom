﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// İndirim kuponu ayarlarını temsil eden sınıf.
    /// </summary>
    public class DiscountCouponSetting  : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// İndirim kuponu ayarını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İndirim kuponunun uygulanabilir olacağı satın alma alt limiti.
        /// </summary>
        public decimal Limit { get; set; }

        /// <summary>
        /// İndirim kuponu tek kullanımlık mı?
        /// </summary>
        public bool SingleUse { get; set; }

        /// <summary>
        /// İndirim kuponu kullanılmış mı?
        /// </summary>
        public bool Used { get; set; }

        /// <summary>
        /// İndirim kuponu oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// İndirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// İndirim kuponu ayarının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İndirim kuponu ayarını oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili indirim kuponu.
        /// </summary>
        public DiscountCoupon DisountCoupon { get; set; }
        #endregion
    }
}
