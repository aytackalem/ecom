﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    public class Showcase : DomainEntityBase<int>
    {
        #region Properties
        

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public int? CategoryId { get; set; }

        public int? BrandId { get; set; }

        public string Json { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        

        public Language Language { get; set; }

        public Category Category { get; set; }

        public Brand Brand { get; set; }

        //public List<_RowComponent> RowComponents { get; set; }
        #endregion
    }
}
