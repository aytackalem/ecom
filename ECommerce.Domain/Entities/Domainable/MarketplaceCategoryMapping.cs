﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Pazaryeri kategorileri ile sistem kategorilerinin eşleşmesini temsil eden sınıf.
    /// </summary>
    public class MarketplaceCategoryMapping : DomainEntityBase<int>, ITenantable
    {
        #region Properties
        /// <summary>
        /// Sistem kategori id bilgisi.
        /// </summary>
        public int CategoryId { get; set; }

        public string MarketplaceId { get; set; }

        /// <summary>
        /// E-Ticaret eşleşmeleri için kullanılacak olan özellik.
        /// </summary>
        public int? CompanyId { get; set; }

        public string MarketplaceCategoryName { get; set; }

        /// <summary>
        /// Pazaryeri kategori id bilgisi.
        /// </summary>
        public string MarketplaceCategoryCode { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Sistem kategorisi.
        /// </summary>
        public Category Category { get; set; }


        public List<MarketplaceCategoryVariantValueMapping> MarketplaceCategoryVariantValueMappings { get; set; }

        #endregion
    }
}
