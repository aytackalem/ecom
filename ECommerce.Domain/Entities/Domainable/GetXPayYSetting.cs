﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// X al Y öde ayarlarını temsil eder.
    /// </summary>
    public class GetXPayYSetting  : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Properties
        

        /// <summary>
        /// X al Y öde geçerli olacağı uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// X al Y öde ayarını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Satın alınması gereken ürün adedi.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Satın alınması gereken ürünlere ait alt limit.
        /// </summary>
        public decimal XLimit { get; set; }

        /// <summary>
        /// Hediye edilecek ürün adedi.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Başlangıç tarihi.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Bitiş tarihi.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// X al Y öde ayarının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// X al Y öde aktif mi?
        /// </summary>
        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// X al Y öde ayarını oluşturan kullanıcı.
        /// </summary>
        
        #endregion
    }
}
