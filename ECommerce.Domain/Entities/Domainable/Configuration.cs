﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kargo şirketi yapılandırmalarını temsil eden sınıf.
    /// </summary>
    public class Configuration  : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Anahtar adı kullanıcı ekranda görür
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Anahtar key.
        /// </summary>
        public string Key { get; set; }     

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        #endregion
    }
}
