﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities.Domainable;

public class XmlDefinationBrand : DomainEntityBase<int>
{
    public int XmlDefinationId { get; set; }

    public int BrandId { get; set; }


    #region Navigation Property

    /// <summary>
    /// İlgili varyasyon.
    /// </summary>
    public XmlDefination XmlDefination { get; set; }

    public Brand Brand { get; set; }
    #endregion

}
