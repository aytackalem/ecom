﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sıkça sorulan soruları temsil eden sınıf;
    /// </summary>
    public class FrequentlyAskedQuestionTranslation  : DomainEntityBase<int>
    {
        #region Properties
        

        

        public string LanguageId { get; set; }

        public string Header { get; set; }
        
        public string Description { get; set; }      

        public DateTime CreatedDate { get; set; }

        public int FrequentlyAskedQuestionId { get; set; }
        #endregion

        #region Navigation Properties
        

        public FrequentlyAskedQuestion FrequentlyAskedQuestion { get; set; }

        public Language Language { get; set; }

        
        #endregion
    }
}
