﻿using ECommerce.Domain.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Özellik dil bilgisini temsil eden sınıf.
    /// </summary>
    public class PropertyTranslation : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// Özellik id bilgisi.
        /// </summary>
        public int PropertyId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        [Required]
        public string LanguageId { get; set; }

        /// <summary>
        /// Ürün adı.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 300, MinimumLength = 1)]
        public string Name { get; set; }

        

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// İlgili ürün özelliği.
        /// </summary>
        public Property Property { get; set; }
        #endregion
    }
}