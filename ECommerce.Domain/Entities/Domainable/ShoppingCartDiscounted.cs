﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Genel indirimi temsil eden sınıf.
    /// </summary>
    public class ShoppingCartDiscounted : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Properties
        
        /// <summary>
        /// Genel indirimin geçerli olacağı uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// Genel indirimi oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Genel indirim başlangıç tarihi.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Genel indirim bitiş tarihi.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Genel indirim aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Genel indirimin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// Genel indirimin geçerli olacağı uygulama.
        /// </summary>
        public Application Application { get; set; }

        /// <summary>
        /// Genel indirimi oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Sepette indirim ayarları.
        /// </summary>
        public ShoppingCartDiscountedSetting ShoppingCartDiscountedSetting { get; set; }
        #endregion
    }
}
