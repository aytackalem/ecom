﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Para puan objesini temsil eden sınıf.
    /// </summary>
    public class MoneyPoint : DomainEntityBase<int>, ICustomerUserCreatable
    {
        #region Properties
        public int CustomerUserId { get; set; }

        /// <summary>
        /// Para puan ile ilişkili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Eklenme Tarihi
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Tutar
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Bonus Aktif mi?
        /// </summary>
        public bool Approved { get; set; }
        #endregion

        #region Navigation Properties
        public CustomerUser CustomerUser { get; set; }

        /// <summary>
        /// Para puan ile ilişkili sipariş.
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}
