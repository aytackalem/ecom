﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class InformationTranslationContent : DomainEntityBase<int>
    {
        #region Properties
        

        

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// İçerik html.
        /// </summary>
        public string Html { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İlgili içerik dil bilgisi.
        /// </summary>
        public InformationTranslation InformationTranslation { get; set; }

        
        #endregion
    }
}
