﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategori ürünlerini temsil eden sınıf. *Recursive yapmamak için.
    /// </summary>
    public class CategoryProduct : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Kategoriye ait id bilgisi.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Ürüne ait id bilgisi.
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// Variantlı Ürüne ait id bilgisi.
        /// </summary>
        public int? ProductInformationId { get; set; }

        /// <summary>
        /// Kategori kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Kategori kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Çoklu kategori seçimlerinde en alt kategori belirlemek için kullanan property
        /// </summary>
        public bool Leaf { get; set; }

        /// <summary>
        /// Bu değişken True olduğu taktirde ürün kendi kategorisi dışında extra bir kategoriyede maplenmiştir.
        /// </summary>
        public bool Multi { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Kategori.
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Ürün.
        /// </summary>
        public Product Product { get; set; }


        /// <summary>
        /// Variantlı Ürüne ait ekstra kategori verilir.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        /// <summary>
        /// Kategoriyi oluşturan kullanıcı.
        /// </summary>
        
        #endregion
    }
}