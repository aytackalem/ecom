﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Marka bazlı indirim ayarlarını temsil eden sınıf.
    /// </summary>
    public class BrandDiscountSetting : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Property
        /// <summary>
        /// İndiriminin geçerli olacağı uygulama ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// İndiriminin geçerli olacağı markaya ait id bilgisi.
        /// </summary>
        public int BrandId { get; set; }

        /// <summary>
        /// İndirim kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İndirim oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// İndirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// İndirimin başlangıç tarihi.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// İndirimin bitiş tarihi.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// İndirim aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// İndirim kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Property
        

        /// <summary>
        /// İndiriminin geçerli olacağı uygulama.
        /// </summary>
        public Application Application { get; set; }

        /// <summary>
        /// İndiriminin geçerli olacağı marka.
        /// </summary>
        public Brand Brand { get; set; }

        /// <summary>
        /// İndirimi oluşturan kullanıcı.
        /// </summary>
        
        #endregion
    }
}
