﻿using System;
using System.Collections.Generic;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Özellikleri temsil eden sınıf.
    /// </summary>
    public class Property : DomainEntityBase<int>, IDeletable
    {
        #region Properties
        

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Özellik silindi mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Özellik silinme zamanı?
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// Ürün özelliği dil bilgileri.
        /// </summary>
        public List<PropertyTranslation> ProductPropertyTranslations { get; set; }

        /// <summary>
        /// Ürün özelliği değerleri.
        /// </summary>
        public List<PropertyValue> ProductPropertyValues { get; set; }
        #endregion
    }
}