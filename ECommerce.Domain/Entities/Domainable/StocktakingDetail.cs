﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities.Domainable
{
    /// <summary>
    /// Stok sayim detaylarini temsil eden sinif.
    /// </summary>
    public class StocktakingDetail : DomainEntityBase<int>
    {
        public int StocktakingId { get; set; }

        /// <summary>
        /// Ilgili urun id.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Okutulan adet.
        /// </summary>
        public int Quantity { get; set; }

        public ProductInformation ProductInformation { get; set; }

        public Stocktaking Stocktaking { get; set; }
    }
}
