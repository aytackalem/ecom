﻿using ECommerce.Domain.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ECommerce.Domain.Entities
{
    public class ProductTranslation : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// İlgili ürün detay id bilgisi.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        [Required]
        public string LanguageId { get; set; }

        /// <summary>
        /// Ürün adı.
        /// </summary>
        [Required]
        [StringLength(maximumLength: 300, MinimumLength = 1)]
        public string Name { get; set; }

        /// <summary>
        /// Ürün dil bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Ürün dil bilgisinin oluşturulduğu zaman.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Ürün dil bilgisini oluşturan kullanıcı.
        /// </summary>
        
        #endregion
    }
}
