﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Varyasyon dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class VariantTranslation : DomainEntityBase<int>
    {
        #region Property
        /// <summary>
        /// İlgili varyasyon id bilgisi.
        /// </summary>
        public int VariantId { get; set; }

        /// <summary>
        /// Varyasyonu oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Varyasyon adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Varyasyonunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Varyasyonu oluşturan kullanıcı.
        /// </summary>
        
        
        /// <summary>
        /// İlgili varyasyon.
        /// </summary>
        public Variant Variant { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }
        #endregion
    }
}
