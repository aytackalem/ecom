﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Haberleri temsil eden sınıf;
    /// </summary>
    public class InformationTranslation : DomainEntityBase<int>
    {
        #region Properties
        

        

        public string LanguageId { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }

        public DateTime CreatedDate { get; set; }

        public int InformationId { get; set; }
        #endregion

        #region Navigation Properties
        

        public Information Information { get; set; }

        public Language Language { get; set; }

        

        public InformationSeoTranslation InformationSeoTranslation { get; set; }

        public InformationTranslationContent InformationTranslationContent { get; set; }
        #endregion
    }
}
