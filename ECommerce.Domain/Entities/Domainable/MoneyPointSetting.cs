﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Para puan ayarlarını temsil eden sınıf.
    /// </summary>
    public class MoneyPointSetting : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Para puan kazanımlarında uygulanacak olan limit bilgisi.
        /// </summary>
        public decimal Limit { get; set; }

        /// <summary>
        /// Para puan oran olarak mı yoksa rakam olarak mı verilecek?
        /// </summary>
        public bool EarningIsRate { get; set; }

        /// <summary>
        /// Para puan kazanım oran/rakam bilgisi.
        /// </summary>
        public decimal Earning { get; set; }
        #endregion
    }
}
