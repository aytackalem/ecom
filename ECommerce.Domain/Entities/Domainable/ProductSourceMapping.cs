﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities.Domainable
{
    /*
     * XML - JSON - Excel gibi bulk insertler için Mapping gerekeceğinden bu class daha sonra geliştirilecek.
     */
    public class ProductSourceMapping : DomainEntityBase<int>
    {
        #region Properties
        public int ProductSourceDomainId { get; set; }
        #endregion

        #region Navigation Properties
        public ProductSourceDomain ProductSourceDomain { get; set; }
        #endregion
    }
}