﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Genel indirim ayarlarını temsil eden sınıf.
    /// </summary>
    public class EntireDiscountSetting  : DomainEntityBase<int>
    {
        #region Properties
        

        /// <summary>
        /// Genel indirimin uygulabilir olacağı ürün alt limiti.
        /// </summary>
        public decimal Limit { get; set; }

        /// <summary>
        /// Genel indirim oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// Genel indirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }
        #endregion

        #region Navigation Properties
        

        /// <summary>
        /// İlgili genel indirim.
        /// </summary>
        public EntireDiscount EntireDiscount { get; set; }
        #endregion
    }
}
