﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ödeme tipi ayarlarını temsil eden sınıf.
    /// </summary>
    public class PaymentTypeSetting : EntityBase<int>
    {
        #region Property
        /// <summary>
        /// İndirim oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// İndirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Maliyet oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool CostIsRate { get; set; }

        /// <summary>
        /// Maliyet oran/rakam bilgisi.
        /// </summary>
        public decimal Cost { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ödeme tipi.
        /// </summary>
        public PaymentTypeDomain PaymentTypeDomain { get; set; }
        #endregion
    }
}
