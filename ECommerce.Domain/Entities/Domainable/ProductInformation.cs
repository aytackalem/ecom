﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Ürün detay bilgilerini temsil eden sınıf.
/// </summary>
public class ProductInformation : DomainEntityBase<int>, IActivable
{
    #region Properties
    /// <summary>
    /// Ürünü oluşturan kullanıcı id bilgisi.
    /// </summary>
    

    /// <summary>
    /// İlgili ürüne ait id bilgisi.
    /// </summary>
    public int ProductId { get; set; }

    /// <summary>
    /// Barkod bilgisi.
    /// </summary>
    public string Barcode { get; set; }

    /// <summary>
    /// Stok kodu.
    /// </summary>
    public string StockCode { get; set; }

    /// <summary>
    /// Ürünün Variant Sku Kodu
    /// </summary>
    public string SkuCode { get; set; }

    /// <summary>
    /// Stok adedi.
    /// </summary>
    public int Stock { get; set; }

    /// <summary>
    /// Rezerve edilmis stok adedi.
    /// </summary>
    public int ReservedStock { get; set; }

    /// <summary>
    /// Dış kaynaklardan stok bilgisi alınırken. İşlem süresince hata olmadan devamlılık sağlanabilmesi için veriler öncelikle buraya yazılır.
    /// </summary>
    public int UpdateStock { get; set; }

    /// <summary>
    /// Dış kaynaklardan stok bilgisi alınırken. İşlem süresince hata olmadan devamlılık sağlanabilmesi için veriler öncelikle buraya yazılır.
    /// </summary>
    public int UpdateReservedStock { get; set; }

    /// <summary>
    /// Gerçek olmayan sanal stoğu ifade eder.
    /// </summary>
    public int VirtualStock { get; set; }

    /// <summary>
    /// Ürünün oluşturulduğu tarih.
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    /// Ürün satışa açık mı.
    /// </summary>
    public bool IsSale { get; set; }

    /// <summary>
    /// Ürün bazlı kargo ücretsiz mi?
    /// </summary>
    public bool Payor { get; set; }

    /// <summary>
    /// Ürün detay bilgisine ait grup id.
    /// </summary>
    public string GroupId { get; set; }

    /// <summary>
    /// Ürün detay bilgisine ait fotoğraflanabilir özelliği.
    /// </summary>
    public bool Photoable { get; set; }

    /// <summary>
    /// Ürün Aktif mi
    /// </summary>
    public bool Active { get; set; }

    /// <summary>
    /// Ürünün bulunduğu raf bölge bilgisi.
    /// </summary>
    public string ShelfZone { get; set; }

    /// <summary>
    /// Ürünün bulunduğu raf bilgisi.
    /// </summary>
    public string ShelfCode { get; set; }

    /// <summary>
    /// Ürün tip bilgisini temsil eden özellik. PI = Product Information, PIP = Product Information Package, PIC = Product Information Combine.
    /// </summary>
    public string Type { get; set; }

    /// <summary>
    /// Ürünün kategorizasyonunu temsil eder.
    /// </summary>
    public string Categorization { get; set; }

    /// <summary>
    /// Özel kategorizasyon zamanlanmış görevlerden veri girilmesini engellemek amacıyla kullanılır.
    /// </summary>
    public bool SpecialCategorization { get; set; }

    /// <summary>
    /// İade oranı.
    /// </summary>
    public float ReturnRate { get; set; }

    /// <summary>
    /// Renk bazlı iade oranı.
    /// </summary>
    public float ColorReturnRate { get; set; }
    #endregion

    #region Navigation Properties
    /// <summary>
    /// Ürün bilgisini oluşturan kullanıcı.
    /// </summary>


    /// <summary>
    /// İlgili ürün.
    /// </summary>
    public Product Product { get; set; }

    /// <summary>
    /// Ürüne bilgisine ait ürün varyasyonları.
    /// </summary>
    public List<ProductInformationVariant> ProductInformationVariants { get; set; }

    /// <summary>
    /// Ürün bilgisine ait fiyatlar.
    /// </summary>
    public List<ProductInformationPrice> ProductInformationPriceses { get; set; }

    /// <summary>
    /// Ürün bilgisine ait fotoğraflar.
    /// </summary>
    public List<ProductInformationPhoto> ProductInformationPhotos { get; set; }

    /// <summary>
    /// Ürün detay dil bilgileri.
    /// </summary>
    public List<ProductInformationTranslation> ProductInformationTranslations { get; set; }

    /// <summary>
    /// ilgili market listesi
    /// </summary>
    public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

    /// <summary>
    /// Ürün yorumları listesi
    /// </summary>
    public List<ProductInformationComment> ProductInformationComments { get; set; }

    /// <summary>
    /// Stokta olmayan ürünler için haber ver listesi
    /// </summary>
    public List<ProductInformationSubscription> ProductInformationSubscriptions { get; set; }

    /// <summary>
    /// ...
    /// </summary>
    public List<ProductInformationCombine> ProductInformationCombines { get; set; }

    /// <summary>
    /// ...
    /// </summary>
    public List<ProductInformationCombine> ContainProductInformationCombines { get; set; }


    /// <summary>
    /// Variantlı Ürüne ait ekstra kategori verilir.
    /// </summary>
    public List<CategoryProduct> CategoryProducts { get; set; }
    #endregion
}