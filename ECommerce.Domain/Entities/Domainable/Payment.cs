﻿using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Enum;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ödemeleri temsil eden sınıf.
    /// </summary>
    public class Payment : DomainEntityBase<int>, ITenantable, ICreatable
    {
        #region Property
        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ödeme tipine ait id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// İndirim kuponu kullanılmış ise indirim kuponu id bilgisi.
        /// </summary>
        public int? DiscountCouponId { get; set; }

        /// <summary>
        /// İlgili ödeme tipi ödenmiş mi?.
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// Ödeme tutarı.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Ödeme bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Pazaryerinden gelen E-ticaret siparişlerini eşleştiği bankaya göre atıyoruz
        /// Pazaryerinde boş gelir E-ticaret pazaryeri dolu gelir.
        /// </summary>
        public BankType? BankId { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ödeme tipi.
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// İndirim kuponu kullanılmış ise ilgili indirim kuponu.
        /// </summary>
        public DiscountCoupon DiscountCoupon { get; set; }

        public PaymentLog PaymentLog { get; set; }
        #endregion
    }
}
