﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationSubscription : DomainEntityBase<int>
    {
        #region Properties
        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// İlgili kişinin telefon numarası
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Ürünün oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Sms gönderildi mi?.
        /// </summary>
        public bool IsSend { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        #endregion
    }
}