﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay fotoğraf bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationPhoto : DomainEntityBase<int>, IDeletable
    {
        #region Property

        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün detayına ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Dosya adı.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Ürünün oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Ürün Sırası
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Fotağraf silindi mi?
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Fotağraf silinme zamanı?
        /// </summary>
        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ürün bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}