﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ödeme tip dil bilgilerini temsil eden sınıf.
    /// </summary>
    public class PaymentTypeTranslation : EntityBase<short>
    {
        #region Property
        /// <summary>
        /// Ödeme tip id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Ödeme tipi adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili ödeme tipi.
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }
        #endregion
    }
}
