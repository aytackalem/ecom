﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Ürün detay paket bilgilerini temsil eden sınıf.
    /// </summary>
    public class ProductInformationPackage : DomainEntityBase<int>, IActivable, IDeletable
    {
        #region Properties
        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ürün detay id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Ürün detayının kaç adet satılacağı bilgisi.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Ürünün oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime? DeletedDateTime { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Ürün bilgisini oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// İlgili ürün detay.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}