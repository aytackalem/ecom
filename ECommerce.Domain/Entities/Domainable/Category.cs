﻿using System;
using System.Collections.Generic;
using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Kategorileri temsil eden sınıf.
    /// </summary>
    public class Category : DomainEntityBase<int>, IActivable
    {
        #region Property
        /// <summary>
        /// Üst kategoriye ait id bilgisi.
        /// </summary>
        public int? ParentCategoryId { get; set; }

        /// <summary>
        /// Kategori kaydını oluşturan kullanıcıya ait id bilgisi.
        /// </summary>
        

        /// <summary>
        /// Ikon dosya adı.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Kategori kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Kategori aktif mi?
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Kategori sıralamasını belirler
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// Muhasebeden gelen kategorileri eşlemek için kullanıyoruz.
        /// </summary>
        public string Code { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Üst kategori.
        /// </summary>
        public Category ParentCategory { get; set; }

        /// <summary>
        /// Kategori ürünleri.
        /// </summary>
        public List<CategoryProduct> CategoryProducts { get; set; }

        /// <summary>
        /// Kategoriyi oluşturan kullanıcı.
        /// </summary>
        

        /// <summary>
        /// Kategoriye ait dil bilgileri.
        /// </summary>
        public List<CategoryTranslation> CategoryTranslations { get; set; }

        /// <summary>
        /// Kategori indirimleri.
        /// </summary>
        public List<CategoryDiscountSetting> CategoryDiscountSettings { get; set; }

        /// <summary>
        /// Kategori vitrinleri.
        /// </summary>
        public List<Showcase> Showcases { get; set; }

        /// <summary>
        /// Ürün bilgileri.
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Kategori ile pazaryeri kategori eşleşmeleri.
        /// </summary>
        //public List<MarketplaceCategoryMapping> MarketplaceCategoryMappings { get; set; }
        public List<MarketplaceCategoryMapping> MarketplaceCategoryMappings { get; set; }
        #endregion
    }
}