﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteleri temsil eden sınıf.
    /// </summary>
    public class TwoFactorCustomerUser : DomainEntityBase<int>, ICreatable, ICustomerUserCreatable
    {
        #region Properties
        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Telefona gelen sms kodu 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Sms doğrulama için bu müşteriye kaç kere sms gönderilmiş
        /// </summary>
        public int TryCount { get; set; }
        
        /// <summary>
        /// Müşteri numarası
        /// </summary>
        public int CustomerUserId { get; set; }

        /// <summary>
        /// Müşteri kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation Properties
        /// <summary>
        /// Müşteri kullanıcı bilgileri.
        /// </summary>
        public CustomerUser CustomerUser { get; set; }
        #endregion
    }
}