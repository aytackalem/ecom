﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities;

public class ProductCategorizationHistory : EntityBase<int>
{
    public string SkuCode { get; set; }

    public string OldValue { get; set; }

    public string NewValue { get; set; }

    public DateTime CreatedDate { get; set; }
}
