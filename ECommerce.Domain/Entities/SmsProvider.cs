﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Sms sağlayıcılarını temsil eden sınıf.
    /// </summary>
    public class SmsProvider : EntityBase<string>
    {
        #region Properties
        /// <summary>
        /// Sms sağlayıcı adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<SmsProviderCompany> SmsProviderCompanies { get; set; }
        #endregion
    }
}
