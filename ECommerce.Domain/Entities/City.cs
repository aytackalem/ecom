﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Şehirleri temsil eden sınıf.
    /// </summary>
    public class City : EntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Şehir adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ait olduğu ülkenin id bilgisi.
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Şehir aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Ait olduğu ülke.
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Şehre ait olan ilçeler.
        /// </summary>
        public List<District> Districts { get; set; }
        #endregion
    }
}