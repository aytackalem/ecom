﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Müşteri üyelikleri için rol bilgisini temsil eden sınıf. Bu sınıfın amacı müşterileri segmente etmektir Örn: gold, silver gibi.
    /// </summary>
    public class CustomerRole : EntityBase<string>
    {
        #region Properties
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<CustomerUser> CustomerUsers { get; set; }
        #endregion
    }
}
