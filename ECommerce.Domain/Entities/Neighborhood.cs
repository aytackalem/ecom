﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Mahalleleri temsil eden sınıf.
    /// </summary>
    public class Neighborhood : EntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Mahalle adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mahallenin ait olduğu ilçeye ait id bilgisi.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Mahalle aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Mahallenin ait olduğu ilçe.
        /// </summary>
        public District District { get; set; }
        #endregion
    }
}