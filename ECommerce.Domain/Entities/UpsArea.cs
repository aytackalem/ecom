﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Domain.Entities
{
    public class UpsArea : EntityBase<int>
    {
        #region Properties


        public int? ParentUpsAreaId { get; set; }

        public int UpsCityId { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties

        public UpsArea ParentUpsArea { get; set; }

        public UpsCity UpsCity { get; set; }
        #endregion

    }
}
