﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// Bankalara ait bin numaralarını temsil eden sınıf. *Bin numarası: hesap veya kredi kartlarının ilk 6 hanesinden hangi bankaya ait olduğunu saptamaya yarar.
    /// </summary>
    public class Bin : EntityBase<int>
    {
        #region Properties
        /// <summary>
        /// Bin numarasının bağlı olduğu banka id bilgisi.
        /// </summary>
        public string BankId { get; set; }

        /// <summary>
        /// Bin numarası değeri.
        /// </summary>
        public string Number { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Bin numarasının bağlı olduğu banka.
        /// </summary>
        public Bank Bank { get; set; }
        #endregion
    }
}
