﻿using ECommerce.Domain.Entities.Base;
using System;

namespace ECommerce.Domain.Entities
{
    public class ExchangeRate : EntityBase<int>
    {
        #region Properties
        public string CurrencyId { get; set; }

        public decimal Rate { get; set; }

        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Properties
        public Currency Currency { get; set; }
        #endregion
    }
}
