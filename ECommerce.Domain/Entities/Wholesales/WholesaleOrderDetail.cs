﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Toptan sipariş detayları.
/// </summary>
public class WholesaleOrderDetail : EntityBase<int>
{
    /// <summary>
    /// Toptan sipariş id bilgisi.
    /// </summary>
    public int WholesaleOrderId { get; set; }

    /// <summary>
    /// Ürün bilgi id bilgisi.
    /// </summary>
    public int ProductInformationId { get; set; }

    /// <summary>
    /// Satın alınan adet.
    /// </summary>
    public int Quantity { get; set; }

    /// <summary>
    /// Toptan sipariş.
    /// </summary>
    public WholesaleOrder WholesaleOrder { get; set; }

    /// <summary>
    /// Ürün bilgi.
    /// </summary>
    public ProductInformation ProductInformation { get; set; }
}
