﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Toptan sipariş tipleri. ON: Onaylanan, TO: Toplandı, TE: Teslim Edildi.
/// </summary>
public class WholesaleOrderType : EntityBase<string>
{
    public string Name { get; set; }
}
