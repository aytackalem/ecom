﻿using ECommerce.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities;

/// <summary>
/// Toptan siparişler.
/// </summary>
public class WholesaleOrder : EntityBase<int>, ICreatable
{
    /// <summary>
    /// Toptan sipariş tipi id bilgisi.
    /// </summary>
    public string WholesaleOrderTypeId { get; set; }

    /// <summary>
    /// İhracat mı?
    /// </summary>
    public bool IsExport { get; set; }

    /// <summary>
    /// Toptan sipariş oluşturma tarihi.
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    /// Toptan sipariş tipi.
    /// </summary>
    public WholesaleOrderType WholesaleOrderType { get; set; }

    /// <summary>
    /// Toptan sipariş detayları.
    /// </summary>
    public List<WholesaleOrderDetail> WholesaleOrderDetails { get; set; }
}
