﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Domain.Entities
{
    /// <summary>
    /// E-posta sağlayıcılarını temsil eden sınıf.
    /// </summary>
    public class EmailProvider : EntityBase<string>
    {
        #region Properties
        /// <summary>
        /// E-posta sağlayıcı adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// E-Posta sağlayıcısı yapılandırmaları.
        /// </summary>
        public List<EmailProviderCompany> EmailProviderCompanies { get; set; }
        #endregion
    }
}
