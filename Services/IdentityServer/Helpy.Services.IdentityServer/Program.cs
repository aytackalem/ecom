﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using Helpy.Services.IdentityServer.Data;
using Helpy.Services.IdentityServer.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Linq;

namespace Helpy.Services.IdentityServer
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                // uncomment to write to Azure diagnostics stream
                //.WriteTo.File(
                //    @"D:\home\LogFiles\Application\identityserver.txt",
                //    fileSizeLimitBytes: 1_000_000,
                //    rollOnFileSizeLimit: true,
                //    shared: true,
                //    flushToDiskInterval: TimeSpan.FromSeconds(1))
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Code)
                .CreateLogger();

            try
            {
                //var seed = args.Contains("/seed");
                //if (seed)
                //{
                //    args = args.Except(new[] { "/seed" }).ToArray();
                //}

                var host = CreateHostBuilder(args).Build();

                //if (seed)
                //{
                //    Log.Information("Seeding database...");
                //    var config = host.Services.GetRequiredService<IConfiguration>();
                //    var connectionString = config.GetConnectionString("DefaultConnection");
                //    SeedData.EnsureSeedData(connectionString);
                //    Log.Information("Done seeding database.");
                //    return 0;
                //}

                using (var scope = host.Services.CreateScope())
                {
                    var serviceProvider = scope.ServiceProvider;
                    var applicationDbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();
                    applicationDbContext.Database.Migrate();

                    var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    if (userManager.Users.Any() == false)
                    {
                        userManager.CreateAsync(new ApplicationUser
                        {
                            UserName = "ilknurtekin",
                            Email = "ilknur.tekin@moonsea.com",
                            Brand = "Moonsea",
                            TenantId = 1,
                            Role = "user"//user
                        }, "Tek.1553").Wait();


                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "ebsumupersonel",
                        //    Email = "furkanguzel@Ebsumu.com",
                        //    Brand = "Ebsumu",
                        //    TenantId = 1,
                        //    Role = "customerservice"
                        //}, "Ebsumu.9374").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "StokKontrol",
                        //    Email = "info@lafaba.com",
                        //    Brand = "Lafaba",
                        //    TenantId = 1,
                        //    Role = "sales"
                        //}, "LsM*83274.").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "mertcankaya",
                        //    Email = "mertcankaya@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1,
                        //    Role = "user"
                        //}, "Mertcan.5562").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "muhammet.tekin",
                        //    Email = "muhammet.tekin@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1,
                        //    Role = "user"
                        //}, "Muhammet.9449").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "faruk.cakir",
                        //    Email = "faruk.cakir@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1,
                        //    Role = "user"
                        //}, "Faruk.5148").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "onur.guzel",
                        //    Email = "onur.guzel@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1,
                        //    Role = "user"
                        //}, "Onur.3928").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "yasin.tekin",
                        //    Email = "yasin.tekin@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1,
                        //    Role = "user"
                        //}, "Yasin.4181").Wait();

                        //userManager.CreateAsync(new ApplicationUser
                        //{
                        //    UserName = "Moonsea",
                        //    Email = "info@moonsea.com",
                        //    Brand = "Moonsea",
                        //    TenantId = 1
                        //}, "Games*21").Wait();


                    }
                }

                Log.Information("Starting host...");
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}