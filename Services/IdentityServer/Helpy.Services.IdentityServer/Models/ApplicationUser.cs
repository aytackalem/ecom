﻿using Microsoft.AspNetCore.Identity;

namespace Helpy.Services.IdentityServer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        #region Properties
        public string Brand { get; set; }

        public int TenantId { get; set; }

        public string Role { get; set; }
        #endregion
    }
}
