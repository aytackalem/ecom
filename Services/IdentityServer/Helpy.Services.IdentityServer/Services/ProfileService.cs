﻿using Helpy.Services.IdentityServer.Models;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Helpy.Services.IdentityServer.Services
{
    public class ProfileService : IProfileService
    {
        #region Fields
        private readonly UserManager<ApplicationUser> _userManager;
        #endregion

        #region Constructors
        public ProfileService(UserManager<ApplicationUser> userManager)
        {
            #region Fields
            this._userManager = userManager;
            #endregion
        }
        #endregion

        #region Methods
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            var claims = new List<Claim>
            {
                new Claim("name", user.UserName),
                new Claim("brand", user.Brand),
                new Claim("tenantId", user.TenantId.ToString()),
                new Claim("role", user?.Role),
            };

            context.IssuedClaims.AddRange(claims);
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            context.IsActive = (user != null);
        }
        #endregion
    }
}
