﻿using Helpy.Services.IdentityServer.Models;
using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace Helpy.Services.IdentityServer.Services
{
    public class IdentityResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        #region Fields
        private readonly UserManager<ApplicationUser> _userManager;
        #endregion

        #region Constructors
        public IdentityResourceOwnerPasswordValidator(UserManager<ApplicationUser> userManager)
        {
            #region Fields
            _userManager = userManager;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (CheckUsername(context.UserName) == false || CheckPassword(context.Password) == false)
            {
                context.Result.CustomResponse ??= new System.Collections.Generic.Dictionary<string, object>();
                context.Result.CustomResponse.Add("Error", "Username or password incorrect.");
                return;
            }

            var user = await this._userManager.FindByNameAsync(context.UserName);
            if (user == null)
            {
                context.Result.CustomResponse ??= new System.Collections.Generic.Dictionary<string, object>();
                context.Result.CustomResponse.Add("Error", "Username or password incorrect.");
                return;
            }

            var checkPassword = await this._userManager.CheckPasswordAsync(user, context.Password);
            if (checkPassword == false)
            {
                context.Result.CustomResponse ??= new System.Collections.Generic.Dictionary<string, object>();
                context.Result.CustomResponse.Add("Error", "Username or password incorrect.");
                return;
            }

            context.Result = new GrantValidationResult(user.Id.ToString(), OidcConstants.AuthenticationMethods.Password);
        }
        #endregion

        #region Helper Methods
        private bool CheckUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                return false;

            foreach (var theChar in username)
            {
                if (char.IsNumber(theChar) == false && char.IsLetter(theChar) == false)
                    return false;
            }

            return true;
        }

        private bool CheckPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;

            var chars = new char[] { '$', '*', '!', '-', '&', '.' };

            foreach (var theChar in password)
            {
                if (char.IsNumber(theChar) == false && char.IsLetter(theChar) == false)
                {
                    if (Array.IndexOf(chars, theChar) == -1)
                        return false;
                }
            }

            return true;
        }
        #endregion
    }
}
