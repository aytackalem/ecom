﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Helpy.Services.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                // Panel
                new ApiResource("resource_panel") 
                { 
                    Scopes = 
                    { 
                        "panel" 
                    } 
                },
                // Ordering
                new ApiResource("ApiResourceOrdering")
                {
                    Scopes =
                    {
                        "ScopeOrdering"
                    }
                },
                // Warehouse
                new ApiResource("ApiResourceWarehouse")
                {
                    Scopes =
                    {
                        "ScopeWarehouse"
                    }
                },
                // Exchange
                new ApiResource("ApiResourceExchange")
                {
                    Scopes =
                    {
                        "ScopeExchange"
                    }
                },



                new ApiResource("ApiResourceCatalog")
                {
                    Scopes =
                    {
                        "ScopeCatalog"
                    }
                },
                new ApiResource("resource_packing") { Scopes = { "packing" } },
                new ApiResource("resource_terminal") { Scopes = { "terminal" } },
                new ApiResource(IdentityServerConstants.LocalApi.ScopeName)
            };

        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("panel", "Panel"),
                new ApiScope("ScopeOrdering", "Scope Ordering"),
                new ApiScope("ScopeWarehouse", "Scope Warehouse"),
                new ApiScope("ScopeExchange", "Scope Exchange"),

                new ApiScope("ScopeCatalog", "Scope Catalog"),
                new ApiScope("packing", "Packing"),
                new ApiScope("terminal", "Terminal"),
                new ApiScope(IdentityServerConstants.LocalApi.ScopeName)
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // Panel
                new Client
                {
                    ClientName = "Panel",
                    ClientId = "PanelForUser",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess = true,
                    AllowedScopes = {
                        "panel",
                        "ScopeOrdering",
                        "ScopeWarehouse",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.LocalApi.ScopeName
                    },
                    AccessTokenLifetime = 30 * 24 * 60 * 60,
                    RefreshTokenExpiration = TokenExpiration.Absolute,
                    AbsoluteRefreshTokenLifetime = 30 * 24 * 60 * 60,
                    RefreshTokenUsage = TokenUsage.ReUse
                },
                //Paketleme 
                new Client
                {
                    ClientName = "Packing Application Client",
                    ClientId = "packing.application.client.66ffee4f-1dbf-45af-8306-be6dfa70328b",
                    ClientSecrets = 
                    {
                        new Secret("0a6c1098-2052-4c21-bcb1-385380973d46".Sha256()) 
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    AllowedScopes =
                    {
                        "ScopeOrdering",
                        "ScopeExchange"
                    },
                    AccessTokenLifetime = 3600
                },


                
                new Client
                {
                    ClientName = "Packing",
                    ClientId = "PackingForUser",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess = true,
                    AllowedScopes = {
                        "packing",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.LocalApi.ScopeName
                    },
                    AccessTokenLifetime = 24 * 60 * 60,
                    RefreshTokenExpiration = TokenExpiration.Absolute,
                    AbsoluteRefreshTokenLifetime = 30 * 60 * 60,
                    RefreshTokenUsage = TokenUsage.ReUse
                },
                new Client
                {
                    ClientName = "Terminal",
                    ClientId = "TerminalForUser",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess = true,
                    AllowedScopes = {
                        "terminal",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.LocalApi.ScopeName
                    },
                    AccessTokenLifetime = 24 * 60 * 60,
                    RefreshTokenExpiration = TokenExpiration.Absolute,
                    AbsoluteRefreshTokenLifetime = 30 * 60 * 60,
                    RefreshTokenUsage = TokenUsage.ReUse
                }


            };
    }
}