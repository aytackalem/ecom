﻿using AutoMapper;

namespace Helpy.Services.MarketplaceCatalog.Persistence
{
    public class PersistenceMapping : Profile
    {
        #region Constructors
        public PersistenceMapping()
        {
            CreateMap<Core.Domain.Documents.Category, Core.Application.DataTransferObjects.Category>().ReverseMap();
            CreateMap<Core.Domain.Documents.Category, Core.Application.Wrappers.Services.CategoryService.Category>();
            CreateMap<Core.Domain.Documents.Variant, Core.Application.DataTransferObjects.Variant>().ReverseMap();
            CreateMap<Core.Domain.Documents.Variant, Core.Application.Wrappers.Services.VariantService.Variant>();
            CreateMap<Core.Domain.Documents.VariantValue, Core.Application.Wrappers.Services.VariantValueService.VariantValue>();
            CreateMap<Core.Domain.Documents.VariantValue, Core.Application.DataTransferObjects.VariantValue>().ReverseMap();


            CreateMap<Core.Domain.Documents.Brand, Core.Application.Wrappers.Services.BrandService.Brand>();
            CreateMap<Core.Domain.Documents.Brand, Core.Application.DataTransferObjects.Brand>().ReverseMap();


        }
        #endregion
    }
}
