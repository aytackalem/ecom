﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Persistence.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Helpy.Services.MarketplaceCatalog.Persistence
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.Configure<MongoConfiguration>(configuration.GetSection("MongoConfiguration"));
            serviceCollection.AddSingleton<MongoConfiguration>(serviceProvider =>
            {
                return serviceProvider.GetRequiredService<IOptions<MongoConfiguration>>().Value;
            });

            serviceCollection.AddAutoMapper(a => a.AddProfile(new PersistenceMapping()));

            serviceCollection.AddScoped<IBrandService, BrandService>();
            serviceCollection.AddScoped<ICategoryService, CategoryService>();
            serviceCollection.AddScoped<IVariantService, VariantService>();
            serviceCollection.AddScoped<IVariantValueService, VariantValueService>();
        }
        #endregion
    }
}
