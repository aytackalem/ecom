﻿namespace Helpy.Services.MarketplaceCatalog.Persistence
{
    public class MongoConfiguration
    {
        #region Properties
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }

        public string BrandCollectionName { get; set; }

        public string CategoryCollectionName { get; set; }

        public string VariantCollectionName { get; set; }

        public string VariantValueCollectionName { get; set; }
        #endregion
    }
}
