﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.VariantValueService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantService;
using Helpy.Services.MarketplaceCatalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Helpy.Services.MarketplaceCatalog.Persistence.Services
{
    public class VariantService : ServiceBase<Core.Domain.Documents.Variant>, IVariantService
    {
        #region Constructors
        public VariantService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.VariantCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.Variant>>> Insert(List<Core.Application.DataTransferObjects.Variant> dtos)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.Variant>>>(async response =>
            {
                var variants = this._mapper.Map<List<Core.Domain.Documents.Variant>>(dtos);
                await base._mongoCollection.InsertManyAsync(variants);

                response.Data = this._mapper.Map<List<Core.Application.DataTransferObjects.Variant>>(variants);
                response.Success = true;
            });
        }

        public async Task<DataResponse<GetByCategoryCodesResponse>> GetByCategoryCodesAsync(GetByCategoryCodesParameters parameters)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<GetByCategoryCodesResponse>>(async response =>
            {
                var results = await IAggregateFluentExtensions.Project<BsonDocument>(base
                    ._mongoCollection
                    .Aggregate()
                    .Match(new BsonDocument
                    {
                        { "CategoryCode", new BsonDocument("$in", new BsonArray(parameters.CategoryCodes)) },
                        { "MarketplaceId", parameters.MarketplaceId }
                    })
                    .Group(new BsonDocument("_id", new BsonDocument
                    {
                        { "Code", "$Code" },
                        { "Name", "$Name" },
                        { "AllowCustom", "$AllowCustom" },
                        { "Mandatory", "$Mandatory" },
                    }))
, new BsonDocument
                    {
                        { "_id", 0 },
                        { "Code", "$_id.Code" },
                        { "Name", "$_id.Name" },
                        { "AllowCustom", "$_id.AllowCustom" },
                        { "Mandatory", "$_id.Mandatory" },
                    })
                    .ToListAsync<BsonDocument>();

                var variants = results.Select(r => BsonSerializer.Deserialize<Core.Domain.Documents.Variant>(r));
                response.Data = new GetByCategoryCodesResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = this._mapper.Map<List<Variant>>(variants)
                };
                response.Success = true;
            });
        }
        #endregion
    }
}
