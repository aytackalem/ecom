﻿using AutoMapper;
using MongoDB.Driver;

namespace Helpy.Services.MarketplaceCatalog.Persistence.Services.Base
{
    public abstract class ServiceBase<TDocument>
    {
        #region Fields
        protected readonly IMongoCollection<TDocument> _mongoCollection;

        public readonly IMapper _mapper;
        #endregion

        #region Constructors
        public ServiceBase(string connectionString, string databaseName, string collectionName, IMapper mapper)
        {
            #region Fields
            var mongoClient = new MongoClient(connectionString);
            var mongoDatabase = mongoClient.GetDatabase(databaseName);
            this._mongoCollection = mongoDatabase.GetCollection<TDocument>(collectionName);
            this._mapper = mapper;
            #endregion
        }
        #endregion
    }
}
