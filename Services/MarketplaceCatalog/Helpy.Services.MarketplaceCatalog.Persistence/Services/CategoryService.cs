﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.CategoryService;
using Helpy.Services.MarketplaceCatalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Driver;

namespace Helpy.Services.MarketplaceCatalog.Persistence.Services
{
    public class CategoryService : ServiceBase<Core.Domain.Documents.Category>, ICategoryService
    {
        #region Constructors
        public CategoryService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.CategoryCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.Category>>> Insert(List<Core.Application.DataTransferObjects.Category> dtos)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.Category>>>(async response =>
            {
                var categories = this._mapper.Map<List<Core.Domain.Documents.Category>>(dtos);
                await base._mongoCollection.InsertManyAsync(categories);

                response.Data = this._mapper.Map<List<Core.Application.DataTransferObjects.Category>>(categories);
                response.Success = true;
            });
        }

        public async Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<GetByMarketplaceIdResponse>>(async response =>
            {
                var result = await base._mongoCollection.FindAsync(c => c.MarketplaceId == marketplaceId);
                var categories = await result.ToListAsync();

                response.Data = new GetByMarketplaceIdResponse
                {
                    MarketplaceId = marketplaceId,
                    Items = this._mapper.Map<List<Category>>(categories)
                };
                response.Success = true;
            });
        }
        #endregion
    }
}
