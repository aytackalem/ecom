﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces.Resolvers;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.BrandService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.BrandService;
using Helpy.Services.MarketplaceCatalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Helpy.Services.MarketplaceCatalog.Persistence.Services
{
    public class BrandService : ServiceBase<Core.Domain.Documents.Brand>, IBrandService
    {
        #region Fields
        private readonly BrandSearchableResolver _brandSearchableResolver;
        #endregion

        #region Constructors
        public BrandService(BrandSearchableResolver brandSearchableResolver, MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.BrandCollectionName, mapper)
        {
            #region Fields
            this._brandSearchableResolver = brandSearchableResolver;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.Brand>>> Insert(List<Core.Application.DataTransferObjects.Brand> dtos)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.Brand>>>(async response =>
            {
                var brands = this._mapper.Map<List<Core.Domain.Documents.Brand>>(dtos);
                await base._mongoCollection.InsertManyAsync(brands);

                response.Data = this._mapper.Map<List<Core.Application.DataTransferObjects.Brand>>(brands);
                response.Success = true;
            });
        }

        public async Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters)
        {
            var marketplace = this._brandSearchableResolver(parameters.MarketplaceId);
            if (marketplace != null)
            {
                var marketplaceResponse = await marketplace.SearchBrandByNameAsync(parameters.Name);

                var response = new DataResponse<SearchByNameResponse>
                {
                    Success = marketplaceResponse.Success,
                    Data = new SearchByNameResponse
                    {
                        MarketplaceId = parameters.MarketplaceId
                    }
                };

                if (response.Success)
                    response.Data.Items = this._mapper.Map<List<Brand>>(marketplaceResponse.Data);

                return response;
            }

            return await ExceptionHandler.HandleAsync<DataResponse<SearchByNameResponse>>(async resoponse =>
            {
                var result = await base._mongoCollection.FindAsync(new BsonDocument
                    {
                        { "MarketplaceId", parameters.MarketplaceId },
                        { "Name",
                            new BsonDocument
                            {
                                { "$regex", parameters.Name },
                                { "$options", "i" }
                            }
                        }
                    });
                var brands = await result.ToListAsync();

                resoponse.Data = new SearchByNameResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = this._mapper.Map<List<Brand>>(brands)
                };
                resoponse.Success = true;
            });
        }
        #endregion
    }
}
