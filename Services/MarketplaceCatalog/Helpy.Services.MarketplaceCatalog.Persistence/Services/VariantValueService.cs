﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.ValueService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantValueService;
using Helpy.Services.MarketplaceCatalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Helpy.Services.MarketplaceCatalog.Persistence.Services
{
    public class VariantValueService : ServiceBase<Core.Domain.Documents.VariantValue>, IVariantValueService
    {
        #region Constructors
        public VariantValueService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.VariantValueCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.VariantValue>>> Insert(List<Core.Application.DataTransferObjects.VariantValue> dtos)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.VariantValue>>>(async response =>
            {
                var variantValues = this._mapper.Map<List<Core.Domain.Documents.VariantValue>>(dtos);

                foreach (var theVariantValue in variantValues)
                {
                    theVariantValue.Id = $"{theVariantValue.MarketplaceId}-{theVariantValue.VariantCode}-{theVariantValue.Code}";
                    theVariantValue.Len = theVariantValue.Name.Length;

                    //await base._mongoCollection.ReplaceOneAsync(v => v.Code == theVariantValue.Code && v.VariantCode == theVariantValue.VariantCode && v.MarketplaceId == theVariantValue.MarketplaceId, theVariantValue, new ReplaceOptions
                    //{
                    //    IsUpsert = true
                    //});
                }

                await base._mongoCollection.InsertManyAsync(variantValues);

                response.Data = this._mapper.Map<List<Core.Application.DataTransferObjects.VariantValue>>(variantValues);
                response.Success = true;
            });
        }

        public async Task<DataResponse<SearchByNameResponse>> GetAsync(GetParameters parameters)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<SearchByNameResponse>>(async response =>
            {
                var filter = new BsonDocument
                {
                    { "MarketplaceId", parameters.MarketplaceId },
                    { "VariantCode", parameters.VariantCode }
                };

                if (string.IsNullOrEmpty(parameters.CategoryCode) == false)
                    filter.Add(new BsonElement("CategoryCodes", new BsonDocument
                    {
                        { "$in", new BsonArray { parameters.CategoryCode } }
                    }));

                var values = await base
                    ._mongoCollection
                    .Find(filter)
                    .SortBy(v => v.Len)
                    .ToListAsync();
                response.Data = new SearchByNameResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = this._mapper.Map<List<VariantValue>>(values)
                };
                response.Success = true;
            });
        }

        public async Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<SearchByNameResponse>>(async response =>
            {
                var filter = new BsonDocument
                {
                    { "MarketplaceId", parameters.MarketplaceId },
                    { "VariantCode", parameters.VariantCode },
                    { "Name",
                        new BsonDocument
                        {
                            { "$regex", parameters.Name },
                            { "$options", "i" }
                        }
                    }
                };

                if (string.IsNullOrEmpty(parameters.CategoryCode) == false)
                    filter.Add(new BsonElement("CategoryCodes", new BsonDocument
                    {
                        { "$in", new BsonArray { parameters.CategoryCode } }
                    }));

                var values = await base
                    ._mongoCollection
                    .Find(filter)
                    .SortBy(v => v.Len)
                    .Limit(3)
                    .ToListAsync();
                response.Data = new SearchByNameResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = this._mapper.Map<List<VariantValue>>(values)
                };
                response.Success = true;
            });
        }

        public async Task<DataResponse<bool>> ExsistNameAsync(SearchByNameParameters parameters)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<bool>>(async response =>
            {
                var filter = new BsonDocument
                {
                    { "MarketplaceId", parameters.MarketplaceId },
                    { "VariantCode", parameters.VariantCode },
                    { "Code",parameters.Code }
                };

                if (string.IsNullOrEmpty(parameters.CategoryCode) == false)
                    filter.Add(new BsonElement("CategoryCodes", new BsonDocument
                    {
                        { "$in", new BsonArray { parameters.CategoryCode } }
                    }));

                var exsist = await base
                    ._mongoCollection
                    .Find(filter)
                    .SortBy(v => v.Len)
                    .AnyAsync();

                response.Data = exsist;
                response.Success = true;
            });
        }
        #endregion
    }
}
