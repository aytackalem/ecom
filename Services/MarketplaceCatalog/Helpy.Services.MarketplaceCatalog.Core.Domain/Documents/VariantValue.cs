﻿using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.MarketplaceCatalog.Core.Domain.Documents
{
    public class VariantValue
    {
        #region Properties
        [BsonId]
        public string Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string MarketplaceId { get; set; }

        public int Len { get; set; }

        public string VariantCode { get; set; }

        public List<string> CategoryCodes { get; set; }
        #endregion
    }
}