﻿using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.MarketplaceCatalog.Core.Domain.Documents
{
    public class Brand
    {
        #region Properties
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string MarketplaceId { get; set; }
        #endregion
    }
}