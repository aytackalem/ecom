﻿using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.MarketplaceCatalog.Core.Domain.Documents
{
    public class Variant
    {
        #region Properties
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string MarketplaceId { get; set; }

        public string CategoryCode { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }
        #endregion
    }
}