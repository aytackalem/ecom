using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.ValueService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.MarketplaceCatalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VariantValueController : ControllerBase
    {
        #region Fields
        private readonly IVariantValueService _variantValueService;
        #endregion

        #region Constructors
        public VariantValueController(IVariantValueService variantValueService)
        {
            #region Fields
            this._variantValueService = variantValueService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost("Insert")]
        public async Task<IActionResult> Insert(List<VariantValue> dtos)
        {
            return Ok(await this._variantValueService.Insert(dtos));
        }

        [EnableCors("DefaultCorsPolicy")]
        [HttpGet("SearchByName")]
        public async Task<IActionResult> SearchByName([FromQuery] SearchByNameParameters parameters)
        {
            return Ok(await this._variantValueService.SearchByNameAsync(parameters));
        }

        [EnableCors("DefaultCorsPolicy")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetParameters parameters)
        {
            return Ok(await this._variantValueService.GetAsync(parameters));
        }

        [EnableCors("DefaultCorsPolicy")]
        [HttpGet("ExsistName")]
        public async Task<IActionResult> ExsistName([FromQuery] SearchByNameParameters parameters)
        {
            return Ok(await this._variantValueService.ExsistNameAsync(parameters));
        }
        #endregion
    }
}