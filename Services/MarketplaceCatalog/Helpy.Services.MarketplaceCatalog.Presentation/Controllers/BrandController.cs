using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.BrandService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.MarketplaceCatalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrandController : ControllerBase
    {
        #region Fields
        private readonly IBrandService _brandService;
        #endregion

        #region Constructors
        public BrandController(IBrandService brandService)
        {
            #region Fields
            this._brandService = brandService; 
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Insert(List<Brand> dtos)
        {
            return Ok(await this._brandService.Insert(dtos));
        }

        [EnableCors("DefaultCorsPolicy")]
        [HttpGet("SearchByName")]
        public async Task<IActionResult> SearchByName([FromQuery] SearchByNameParameters parameters)
        {
            return Ok(await this._brandService.SearchByNameAsync(parameters));
        }
        #endregion
    }
}