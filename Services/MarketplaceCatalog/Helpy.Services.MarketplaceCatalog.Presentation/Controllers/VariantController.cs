using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.VariantValueService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.MarketplaceCatalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VariantController : ControllerBase
    {
        #region Fields
        private readonly IVariantService _variantService;
        #endregion

        #region Constructors
        public VariantController(IVariantService variantService)
        {
            #region Fields
            this._variantService = variantService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost("Insert")]
        public async Task<IActionResult> Insert(List<Variant> dtos)
        {
            return Ok(await this._variantService.Insert(dtos));
        }

        [EnableCors("DefaultCorsPolicy")]
        [HttpPost("GetByCategoryCodes")]
        public async Task<IActionResult> GetByCategoryCodes(GetByCategoryCodesParameters parameters)
        {
            return Ok(await this._variantService.GetByCategoryCodesAsync(parameters));
        }
        #endregion
    }
}