using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.MarketplaceCatalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        #region Fields
        private readonly ICategoryService _categoryService;
        #endregion

        #region Constructors
        public CategoryController(ICategoryService categoryService)
        {
            #region Fields
            this._categoryService = categoryService; 
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Insert(List<Category> dtos)
        {
            return Ok(await this._categoryService.Insert(dtos));
        }

        [HttpGet("GetByMarketplaceId")]
        public async Task<IActionResult> GetByMarketplaceId(string marketplaceId)
        {
            return Ok(await this._categoryService.GetByMarketplaceIdAsync(marketplaceId));
        }
        #endregion
    }
}