using Helpy.Services.MarketplaceCatalog.Persistence;

namespace Helpy.Services.MarketplaceCatalog.Presentation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddCors(o =>
            {
                o.AddPolicy(name: "DefaultCorsPolicy", policy =>
                {
                    //policy.WithOrigins("http://localhost:5200").AllowAnyHeader().AllowAnyMethod();
                    policy.WithOrigins("http://localhost:5201").AllowAnyHeader().AllowAnyMethod();
                    //policy.WithOrigins("https://panel.helpy.com.tr").AllowAnyHeader().AllowAnyMethod();
                    policy.WithOrigins("https://erp.helpy.com.tr").AllowAnyHeader().AllowAnyMethod();
                    policy.WithOrigins("https://backoffice.helpy.com.tr").AllowAnyHeader().AllowAnyMethod();
                    policy.WithOrigins("https://erp.helpy.tr").AllowAnyHeader().AllowAnyMethod();
                });
            });

            builder.Services.AddControllers().AddNewtonsoftJson();

            builder.Services.AddInfrustructureServices();
            builder.Services.AddPersistenceServices(builder.Configuration);

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseAuthorization();

            app.UseCors();

            app.MapControllers();

            app.Run();
        }
    }
}