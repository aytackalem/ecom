﻿using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects.Base;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects
{
    public class Variant : MarketplaceDataTransferObjectBase
    {
        #region Properties
        [JsonPropertyName("CC")]
        [JsonProperty("CC")]
        public string CategoryCode { get; set; }

        [JsonPropertyName("AC")]
        [JsonProperty("AC")]
        public bool AllowCustom { get; set; }

        [JsonPropertyName("M")]
        [JsonProperty("M")]
        public bool Mandatory { get; set; }

        [JsonPropertyName("MU")]
        [JsonProperty("MU")]
        public bool Multiple { get; set; }

        [JsonPropertyName("V")]
        [JsonProperty("V")]
        public bool Varianter { get; set; }
        #endregion
    }
}
