﻿using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects.Base;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects
{
    public class Category : MarketplaceDataTransferObjectBase
    {
        #region Properties
        [JsonPropertyName("PC")]
        [JsonProperty("PC")]
        public string ParentCode { get; set; }

        [JsonPropertyName("B")]
        [JsonProperty("B")]
        public string? Breadcrumb { get; set; }
        #endregion
    }
}
