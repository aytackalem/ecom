﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects.Base
{
    public abstract class DataTransferObjectBase
    {
        #region Properties
        [JsonPropertyName("C")]
        [JsonProperty("C")]
        public string Code { get; set; }

        [JsonPropertyName("N")]
        [JsonProperty("N")]
        public string Name { get; set; }
        #endregion
    }
}
