﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects.Base
{
    public abstract class MarketplaceDataTransferObjectBase : DataTransferObjectBase
    {
        #region Properties
        [JsonPropertyName("MI")]
        [JsonProperty("MI")]
        public string MarketplaceId { get; set; }
        #endregion
    }
}
