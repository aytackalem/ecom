﻿using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects.Base;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects
{
    public class VariantValue : MarketplaceDataTransferObjectBase
    {
        #region Properties
        [JsonPropertyName("VC")]
        [JsonProperty("VC")]
        public string VariantCode { get; set; }

        [JsonPropertyName("CC")]
        [JsonProperty("CC")]
        public List<string> CategoryCodes { get; set; }
        #endregion
    }
}
