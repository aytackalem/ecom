﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces
{
    public class Category
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string Code { get; set; }

        public string ParentCode { get; set; }

        public string Name { get; set; }

        public string Breadcrumb { get; set; }
        #endregion
    }
}
