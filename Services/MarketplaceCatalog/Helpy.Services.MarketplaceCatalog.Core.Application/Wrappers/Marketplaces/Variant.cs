﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces
{
    public class Variant
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string CategoryCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public bool Variantable { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantValue> VariantValues { get; set; }
        #endregion
    }
}
