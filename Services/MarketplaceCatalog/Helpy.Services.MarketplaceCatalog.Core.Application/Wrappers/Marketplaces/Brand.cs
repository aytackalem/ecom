﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces
{
    public class Brand
    {
        #region Properties
        public string Code { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
