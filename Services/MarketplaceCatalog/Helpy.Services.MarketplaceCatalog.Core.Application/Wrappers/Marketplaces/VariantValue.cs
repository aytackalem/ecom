﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces
{
    public class VariantValue
    {
        #region Properties
        public string VariantCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public List<string> CategoryCodes { get; set; }
        #endregion
    }
}
