﻿using Newtonsoft.Json;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantValueService
{
    public class VariantValue
    {
        #region Properties
        [JsonProperty("VC")]
        public string VariantCode { get; set; }

        [JsonProperty("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        public string Name { get; set; }
        #endregion
    }
}
