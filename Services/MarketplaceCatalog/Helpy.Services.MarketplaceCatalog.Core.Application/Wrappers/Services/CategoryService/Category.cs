﻿using Newtonsoft.Json;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.CategoryService
{
    public class Category
    {
        #region Properties
        [JsonProperty("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        public string Name { get; set; }

        [JsonProperty("B")]
        public string Breadcrumb { get; set; }
        #endregion
    }
}
