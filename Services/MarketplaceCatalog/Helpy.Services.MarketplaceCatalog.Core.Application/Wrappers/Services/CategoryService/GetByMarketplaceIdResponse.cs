﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.Base;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.CategoryService
{
    public class GetByMarketplaceIdResponse : Marketplace<Category>
    {
    }
}
