﻿using Newtonsoft.Json;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantService
{
    public class Variant
    {
        #region Properties
        [JsonProperty("AC")]
        public bool AllowCustom { get; set; }

        [JsonProperty("M")]
        public bool Mandatory { get; set; }

        [JsonProperty("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        public string Name { get; set; } 
        #endregion
    }
}
