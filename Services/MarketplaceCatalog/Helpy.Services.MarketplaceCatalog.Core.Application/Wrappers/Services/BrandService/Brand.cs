﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.BrandService
{
    public class Brand
    {
        #region Properties
        [JsonProperty("C")]
        [JsonPropertyName("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        [JsonPropertyName("N")]
        public string Name { get; set; }
        #endregion
    }
}
