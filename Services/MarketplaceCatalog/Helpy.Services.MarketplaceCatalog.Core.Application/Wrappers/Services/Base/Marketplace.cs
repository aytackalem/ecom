﻿using Newtonsoft.Json;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.Base
{
    public class Marketplace<T>
    {
        #region Properties
        [JsonProperty("MI")]
        public string MarketplaceId { get; set; }
        #endregion

        #region Navigation Properties
        [JsonProperty("I")]
        public List<T> Items { get; set; }
        #endregion
    }
}
