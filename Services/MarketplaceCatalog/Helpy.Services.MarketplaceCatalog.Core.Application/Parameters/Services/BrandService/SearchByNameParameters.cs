﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.BrandService
{
    public class SearchByNameParameters
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
