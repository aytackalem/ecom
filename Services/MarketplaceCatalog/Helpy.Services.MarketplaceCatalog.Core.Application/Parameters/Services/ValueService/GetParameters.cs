﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.ValueService
{
    public class GetParameters
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string VariantCode { get; set; }

        public string CategoryCode { get; set; }
        #endregion
    }
}
