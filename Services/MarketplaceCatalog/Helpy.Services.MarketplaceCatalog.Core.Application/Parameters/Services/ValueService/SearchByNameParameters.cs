﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.ValueService
{
    public class SearchByNameParameters
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string VariantCode { get; set; }

        public string? Name { get; set; }

        public string? Code { get; set; } 

        public string? CategoryCode { get; set; }
        #endregion
    }
}
