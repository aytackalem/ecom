﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.VariantValueService
{
    public class GetByCategoryCodesParameters
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public List<string> CategoryCodes { get; set; }
        #endregion
    }
}
