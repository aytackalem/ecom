﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.BrandService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.BrandService;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services
{
    public interface IBrandService
    {
        #region Methods
        Task<DataResponse<List<DataTransferObjects.Brand>>> Insert(List<DataTransferObjects.Brand> dtos);

        Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters);
        #endregion
    }
}
