﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.ValueService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantValueService;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services
{
    public interface IVariantValueService
    {
        #region Methods
        Task<DataResponse<List<DataTransferObjects.VariantValue>>> Insert(List<DataTransferObjects.VariantValue> dtos);

        Task<DataResponse<SearchByNameResponse>> GetAsync(GetParameters parameters);

        Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters);

        Task<DataResponse<bool>> ExsistNameAsync(SearchByNameParameters parameters);
        #endregion
    }
}
