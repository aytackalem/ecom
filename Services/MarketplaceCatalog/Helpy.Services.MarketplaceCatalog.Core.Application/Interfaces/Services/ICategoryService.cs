﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.CategoryService;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services
{
    public interface ICategoryService
    {
        #region Methods
        Task<DataResponse<List<DataTransferObjects.Category>>> Insert(List<DataTransferObjects.Category> dtos);

        Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId);
        #endregion
    }
}
