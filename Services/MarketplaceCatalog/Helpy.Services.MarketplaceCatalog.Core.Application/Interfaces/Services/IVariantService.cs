﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Parameters.Services.VariantValueService;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Services.VariantService;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Services
{
    public interface IVariantService
    {
        #region Methods
        Task<DataResponse<List<DataTransferObjects.Variant>>> Insert(List<DataTransferObjects.Variant> dtos);

        Task<DataResponse<GetByCategoryCodesResponse>> GetByCategoryCodesAsync(GetByCategoryCodesParameters parameters);
        #endregion
    }
}
