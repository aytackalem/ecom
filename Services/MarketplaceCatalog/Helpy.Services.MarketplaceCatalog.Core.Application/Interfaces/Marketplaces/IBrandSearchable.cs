﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces
{
    public interface IBrandSearchable
    {
        Task<DataResponse<List<Brand>>> SearchBrandByNameAsync(string name);
    }
}
