﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces
{
    public interface IMarketplace
    {
        #region Properties
        string Id { get; }
        #endregion

        #region Methods
        Task<DataResponse<List<Category>>> GetCategoriesAsync();

        Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode);
        #endregion
    }
}
