﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces
{
    public interface IBrandDownloadable
    {
        #region Properties
        public string Id { get; }
        #endregion

        #region Methods
        Task<DataResponse<List<Brand>>> GetBrandsAsync();
        #endregion
    }
}
