﻿namespace Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces.Resolvers
{
    public delegate IBrandSearchable BrandSearchableResolver(string marketplaceId);
}
