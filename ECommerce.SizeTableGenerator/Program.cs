﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.SizeTableGenerator;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Linq;
using System.Text;

static IConfigurationRoot GetConfiguration()
{
    return new ConfigurationBuilder().SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName).AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true).Build();
}

static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
{
    Settings settings = new();
    configuration.GetSection("Settings").Bind(settings);

    ServiceCollection serviceCollection = new();
    serviceCollection.AddSingleton<IConfiguration>(c => configuration);
    serviceCollection.AddSingleton<SizeTablePhotoGenerator>();
    serviceCollection.AddSingleton<Settings>(settings);
    serviceCollection.AddDbContext<ApplicationDbContext>();
    serviceCollection.AddScoped<ITenantFinder, DynamicTenantFinder>();
    serviceCollection.AddScoped<IDomainFinder, DynamicDomainFinder>();
    serviceCollection.AddScoped<IDbNameFinder, DynamicDbNameFinder>();
    serviceCollection.AddScoped<ICompanyFinder>(cf => null);
    return serviceCollection.BuildServiceProvider();
}

var serviceProvider = ConfigureServiceCollection(GetConfiguration());

String fileName = "SizeTable.jpg";

SizeTable sizeTable = null;

while (true)
{
    StringBuilder stringBuilder = new();
    Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Loop started.");
    stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Loop started.");

    using (var scope = serviceProvider.CreateScope())
    {
        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable fetching. Where clause: Generated is false {(sizeTable is not null ? $", Id greater than {sizeTable.Id}" : "")}.");
        stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable fetching. Where clause: Generated is false {(sizeTable is not null ? $", Id greater than {sizeTable.Id}" : "")}.");

        /* 1. Fetch not generated 'SizeTables' records. */
        using var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        sizeTable = await context
            .SizeTables
                .Include(st =>
                    st
                        .SizeTableItems
                        .Where(_ =>
                            string.IsNullOrEmpty(_.Waist) == false &&
                            string.IsNullOrEmpty(_.Chest) == false &&
                            string.IsNullOrEmpty(_.Hip) == false))
                .Include(st => st.Product)
                    .ThenInclude(p => p.ProductInformations.Where(pi => pi.ProductInformationPhotos.Any()))
                        .ThenInclude(pi => pi.ProductInformationPhotos)
            .FirstOrDefaultAsync(st => st.Generated == false && (sizeTable == null || st.Id > sizeTable.Id));

        if (sizeTable is null)
        {
            Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable is null.");
            stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable is null.");

            break;
        }

        if (sizeTable.Product.ProductInformations is [])
        {
            Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable.Product.ProductInformations is [].");
            stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable.Product.ProductInformations is [].");

            continue;
        }

        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable fetched. Product Id: {sizeTable.ProductId}.");
        stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] SizeTable fetched. Product Id: {sizeTable.ProductId}.");

        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] ProductInformationPhotos upserting.");
        stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] ProductInformationPhotos upserting.");

        /* 2. Generate Upsert image. */
        var sizeTablePhotoGenerator = scope.ServiceProvider.GetRequiredService<SizeTablePhotoGenerator>();

        List<string> skus = new();

        foreach (var loopProductInformation in sizeTable.Product.ProductInformations)
        {
            if (skus.Contains(loopProductInformation.SkuCode) == false)
            {
                Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Image generating.");
                stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Image generating.");

                sizeTablePhotoGenerator.Generate(loopProductInformation.SkuCode, fileName, sizeTable.SizeTableItems);

                Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Image generated.");
                stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Image generated.");

                skus.Add(loopProductInformation.SkuCode);
            }

            var displayOrder = loopProductInformation.ProductInformationPhotos.Max(pip => pip.DisplayOrder);

            if (loopProductInformation.ProductInformationPhotos.Any(pip => pip.FileName == $"SizeTables/{loopProductInformation.SkuCode}/{fileName}") == false)
            {
                loopProductInformation.ProductInformationPhotos.Add(new ProductInformationPhoto
                {
                    ProductInformationId = loopProductInformation.Id,
                    DomainId = 1,
                    TenantId = 1,
                    CreatedDate = DateTime.Now,
                    DisplayOrder = displayOrder + 1,
                    FileName = $"SizeTables/{loopProductInformation.SkuCode}/{fileName}"
                });
            }
        }

        sizeTable.Generated = true;
        sizeTable.FileName = string.Empty;

        var changes = await context.SaveChangesAsync();

        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] ProductInformationPhotos upserted. Changes: {changes}.");
        stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] ProductInformationPhotos upserted. Changes: {changes}.");
    }

    Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] DirtyProductInformations updating.");
    stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] DirtyProductInformations updating.");

    using (var scope = serviceProvider.CreateScope())
    {
        /* 4 Update 'DirtyProductInformations'. */
        using var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        var changes = await context
            .ProductInformationMarketplaces
            .Where(pim => pim.ProductInformation.ProductId == sizeTable.ProductId)
            .ExecuteUpdateAsync(eu => eu.SetProperty(pim => pim.DirtyProductInformation, true));
        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] DirtyProductInformations updated. Changes: {changes}.");
        stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] DirtyProductInformations updated. Changes: {changes}.");
    }

    Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Loop finished.");
    stringBuilder.AppendLine($"[{DateTime.Now:yyyy-MM-dd HH-mm-ss}] Loop finished.");

    Directory.CreateDirectory(@"C:\Logs\SizeTableGenerator");

    System.IO.File.WriteAllText(@$"C:\Logs\SizeTableGenerator\{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.txt", stringBuilder.ToString());
}