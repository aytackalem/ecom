﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.SizeTableGenerator;

public class DynamicTenantFinder : ITenantFinder
{
    public int FindId() => 1;
}

public class DynamicDomainFinder : IDomainFinder
{
    public int FindId() => 1;

    public void Set(int id)
    {
        throw new NotImplementedException();
    }
}

public class DynamicDbNameFinder : IDbNameFinder
{
    public string FindName() => "Lafaba";

    public void Set(string dbName)
    {
        throw new NotImplementedException();
    }
}