﻿using ECommerce.Domain.Entities;
using System.Drawing;

namespace ECommerce.SizeTableGenerator;

public class SizeTablePhotoGenerator
{
    private readonly Settings _settings;

    public SizeTablePhotoGenerator(Settings settings)
    {
        _settings = settings;
    }

    public bool Exists(string folder, string fileName)
    {
        return System.IO.File.Exists($"{_settings.File.Target}\\{folder}\\{fileName}");
    }

    public void Generate(string folder, string fileName, List<SizeTableItem> sizeTableItems)
    {
        var source = _settings.File.Source;
        var target = _settings.File.Target;

        var emptyImage = Bitmap.FromFile(@$"{source}\empty.jpg");
        var headerImage = Bitmap.FromFile(@$"{source}\header.jpg");
        var footerSmallImage = Bitmap.FromFile(@$"{source}\footers.jpg");
        var footerLargeImage = Bitmap.FromFile(@$"{source}\footerl.jpg");

        var headerHeight = _settings.Image.Header.Height;
        var rowHeight = _settings.Image.Row.Height;

        var imageWidth = _settings.Image.Width;
        var imageHeight = _settings.Image.Height;

        using (var bitmap = new Bitmap(imageWidth, imageHeight))
        {
            bitmap.SetResolution(_settings.Image.Resolution, _settings.Image.Resolution);

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImage(emptyImage, 0, 0);
                graphics.DrawImage(headerImage, 0, rowHeight);

                for (int i = 0; i < 11/*sizeTableItems.Count*/; i++)
                {
                    int yAxis = (i * rowHeight) + rowHeight + headerHeight;

                    SizeTableItem sizeTableItem = null;

                    if (i < sizeTableItems.Count)
                    {
                        sizeTableItem = sizeTableItems[i];
                    }

                    if (sizeTableItem is null || System.IO.File.Exists(@$"{source}\{sizeTableItem.Size}.jpg") == false)
                    {
                        graphics.DrawImage(Bitmap.FromFile(@$"{source}\empty.jpg"), 0, yAxis);
                    }
                    else
                    {
                        graphics.DrawImage(Bitmap.FromFile(@$"{source}\{sizeTableItem.Size}.jpg"), 0, yAxis);

                        var brush = new SolidBrush(System.Drawing.Color.FromArgb(
                            _settings.Text.Font.Color.Red,
                            _settings.Text.Font.Color.Green,
                            _settings.Text.Font.Color.Blue));
                        var font = new System.Drawing.Font(_settings.Text.Font.Family, _settings.Text.Font.Size, FontStyle.Bold);
                        var valuePaddingTop = _settings.Text.Padding.Top;

                        graphics.DrawString($"{sizeTableItem.Chest} CM", font, brush, _settings.Position.Chest, yAxis + valuePaddingTop);
                        graphics.DrawString($"{sizeTableItem.Waist} CM", font, brush, _settings.Position.Waist, yAxis + valuePaddingTop);
                        graphics.DrawString($"{sizeTableItem.Hip} CM", font, brush, _settings.Position.Hip, yAxis + valuePaddingTop);
                    }
                }

                if (sizeTableItems.Count < 5)
                {
                    graphics.DrawImage(footerLargeImage, 0, (5 * rowHeight) + headerHeight);
                }
                else
                {
                    graphics.DrawImage(footerSmallImage, 0, (11 * rowHeight) + headerHeight);
                }
            }

            Directory.CreateDirectory(@$"{target}\{folder}");

            bitmap.Save(@$"{target}\{folder}\{fileName}");
        }
    }
}
