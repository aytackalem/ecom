﻿namespace ECommerce.SizeTableGenerator;

public class Settings
{
    public Image Image { get; set; }

    public File File { get; set; }

    public Text Text { get; set; }

    public Position Position { get; set; }
}

public class File
{
    public string Source { get; set; }

    public string Target { get; set; }
}

public class Image
{
    public int Width { get; set; }

    public int Height { get; set; }

    public int Resolution { get; set; }

    public Header Header { get; set; }

    public Row Row { get; set; }
}

public class Header
{
    public int Height { get; set; }
}

public class Text
{
    public Font Font { get; set; }

    public Padding Padding { get; set; }
}

public class Row
{
    public int Height { get; set; }
}

public class Font
{
    public float Size { get; set; }

    public string Family { get; set; }

    public Color Color { get; set; }
}

public class Color
{
    public int Red { get; set; }

    public int Green { get; set; }

    public int Blue { get; set; }
}

public class Position
{
    public int Chest { get; set; }

    public int Waist { get; set; }

    public int Hip { get; set; }
}

public class Padding
{
    public int Top { get; set; }
}