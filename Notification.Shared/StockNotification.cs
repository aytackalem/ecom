﻿namespace Notification.Shared
{
    public class StockNotification
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public string MarketplaceId { get; set; }

        public int ProductId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string PhotoUrl { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public DateTime CreatedDateTime { get; set; }
    }
}