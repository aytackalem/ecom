﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.PriceConversion.Job.Base;
using System;
using System.Linq;
using System.Xml;

namespace ECommerce.PriceConversion.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private XmlDocument _xmlDocument;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var currencyIds = new[] { "EUR" };

            if (!TryLoadExchangeRatesXml())
            {
                //TO DO: Alert
                return;
            }

            foreach (var theCurrencyId in currencyIds)
            {
                if (!TryGetValueFromXml(theCurrencyId, out string value))
                {
                    //TO DO: Alert
                    return;
                }

                if (!decimal.TryParse(value, out decimal rate))
                {
                    //TO DO: Alert
                    return;
                }

                if (!TryUpsertCurrencyValue(theCurrencyId, rate))
                {
                    //TO DO: Alert
                }
            }
        }

        private bool TryLoadExchangeRatesXml()
        {
            try
            {
                _xmlDocument = new XmlDocument();
                _xmlDocument.Load("https://www.tcmb.gov.tr/kurlar/today.xml");

                return true;
            }
            catch { }

            return false;
        }

        private bool TryGetValueFromXml(string currencyId, out string value)
        {
            value = string.Empty;

            if (_xmlDocument == null) return false;
            var node = _xmlDocument.SelectSingleNode($"Tarih_Date/Currency[@Kod='{currencyId}']/BanknoteSelling");
            if (node == null) return false;
            value = node.InnerText;
            if (string.IsNullOrEmpty(value)) return false;

            return true;
        }

        private bool TryUpsertCurrencyValue(string currencyId, decimal rate)
        {
            try
            {
                var exchangeRate = this
                            ._unitOfWork
                            .ExchangeRateRepository
                            .DbSet()
                            .Where(er => er.CreatedDate.Date == DateTime.Now.Date && er.CurrencyId == currencyId)
                            .FirstOrDefault();
                if (exchangeRate == null)
                    this
                        ._unitOfWork
                        .ExchangeRateRepository
                        .Create(new Domain.Entities.ExchangeRate
                        {
                            CreatedDate = DateTime.Now.Date,
                            CurrencyId = "EUR",
                            Rate = rate
                        });
                else
                {
                    exchangeRate.Rate = rate;

                    this
                        ._unitOfWork
                        .ExchangeRateRepository
                        .Update(exchangeRate);
                }

                return true;
            }
            catch { }

            return false;
        }
        #endregion
    }
}
