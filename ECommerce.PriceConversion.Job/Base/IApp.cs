﻿namespace ECommerce.PriceConversion.Job.Base
{
    public interface IApp
    {
        #region Methods
        void Run();
        #endregion
    }
}
