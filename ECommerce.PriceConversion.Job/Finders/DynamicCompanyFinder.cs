﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.PriceConversion.Job.Finders
{
    public class DynamicCompanyFinder : ICompanyFinder
    {
        #region Fields
        private int _id;
        #endregion

        #region Methods
        public int FindId()
        {
            return _id;
        }

        public void Set(int id)
        {
            _id = id;
        }
        #endregion
    }
}
