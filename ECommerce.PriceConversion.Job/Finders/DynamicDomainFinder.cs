﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace ECommerce.PriceConversion.Job.Finders
{
    public class DynamicDomainFinder : IDomainFinder
    {
        #region Fields
        private int _id;
        #endregion

        #region Methods
        public int FindId()
        {
            return _id;
        }

        public void Set(int id)
        {
            _id = id;
        }
        #endregion
    }
}
