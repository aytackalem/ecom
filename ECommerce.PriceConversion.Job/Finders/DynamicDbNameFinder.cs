﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.PriceConversion.Job.Finders
{
    public class DynamicDbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName;
        #endregion

        #region Methods
        public string FindName()
        {
            return _dbName;
        }

        public void Set(string dbName)
        {
            _dbName = dbName;
        }
        #endregion
    }
}
