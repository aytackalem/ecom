﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using ECommerce.PriceConversion.Job.Base;
using ECommerce.PriceConversion.Job.Finders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace ECommerce.PriceConversion.Job
{
    class Program
    {
        static IServiceProvider ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();

            var fileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(fileName).FullName)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.AddScoped<IApp, App>();
            serviceCollection.AddScoped<ITenantFinder>((x) => null);
            serviceCollection.AddScoped<IDomainFinder>((x) => null);
            serviceCollection.AddScoped<ICompanyFinder>((x) => null);
            serviceCollection.AddScoped<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddDbContext<ApplicationDbContext>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var dbNames = new[] { "Lafaba" };
            foreach (var theDbName in dbNames)
                using (var scope = serviceProvider.CreateScope())
                {
                    var app = serviceProvider.GetService<IApp>();

                    var dbNameFinder = serviceProvider.GetRequiredService<IDbNameFinder>();
                    dbNameFinder.Set(theDbName);

                    app?.Run();
                }
        }
    }
}