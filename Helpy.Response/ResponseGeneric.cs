﻿namespace Helpy.Response
{
    public class Response<T>
    {
        public T Data { get; set; }

        public ResponseCodes Code { get; set; }

        public string Message { get; set; }

        public Response(ResponseCodes code, string message, T data)
        {
            this.Code = code;
            this.Message = message;
            this.Data = data;
        }
    }
}