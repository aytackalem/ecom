﻿namespace Helpy.Response
{
    public class Response
    {
        public ResponseCodes Code { get; set; }

        public string Message { get; set; }

        public Response(ResponseCodes code, string message)
        {
            this.Code = code;
            this.Message = message;
        }
    }
}