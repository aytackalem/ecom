﻿namespace Helpy.Tenant.Api.Application.OrderSources
{
    public record OrderSource(int Id, string Name);
}
