﻿using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Tenant.Api.Application.Domains.Get
{
    public record GetQuery : IRequest<DataResponse<List<Domain>>>;
}
