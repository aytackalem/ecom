﻿using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Tenant.Api.Application.Domains.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, DataResponse<List<Domain>>>
    {
        public Task<DataResponse<List<Domain>>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
