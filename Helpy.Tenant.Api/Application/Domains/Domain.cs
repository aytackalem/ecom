﻿namespace Helpy.Tenant.Api.Application.Domains
{
    public record Domain(int Id, string Name);
}
