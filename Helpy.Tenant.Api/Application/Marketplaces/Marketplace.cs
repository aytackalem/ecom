﻿namespace Helpy.Tenant.Api.Application.Marketplaces
{
    public record Marketplace(string Id, string Name);
}
