﻿namespace Helpy.Tenant.Api.Application.Companies
{
    public record Company(int Id, string Name);
}
