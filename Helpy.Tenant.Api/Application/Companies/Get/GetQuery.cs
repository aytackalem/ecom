﻿using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Tenant.Api.Application.Companies.Get
{
    public record GetQuery : IRequest<DataResponse<List<Company>>>;
}
