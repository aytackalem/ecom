﻿using Helpy.Shared.Response;
using MediatR;

namespace Helpy.Tenant.Api.Application.Companies.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, DataResponse<List<Company>>>
    {
        public Task<DataResponse<List<Company>>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
