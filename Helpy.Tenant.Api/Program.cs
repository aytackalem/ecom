using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Tenant.Api.Finders;
using System.Reflection;

namespace Helpy.Tenant.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();
            builder.Services.AddScoped<ITenantFinder, TenantFinder>();
            builder.Services.AddScoped<IDomainFinder, DomainFinder>();
            builder.Services.AddScoped<ICompanyFinder, CompanyFinder>();
            builder.Services.AddMediatR(configuration =>
            {
                configuration.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
            });
            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}