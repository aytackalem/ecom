﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Product.Base
{
    public interface IApp
    {
        void Run();
    }
}
