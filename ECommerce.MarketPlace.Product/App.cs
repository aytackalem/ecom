﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Product;
using ECommerce.MarketPlace.Product.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Product
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IProductFacade _productFacade;

        private readonly IOptions<AppConfig> _appConfigs;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IProductFacade productFacade, IOptions<AppConfig> appConfigs)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _productFacade = productFacade;
            _appConfigs = appConfigs;
            #endregion
        }
        #endregion

        public void Run()
        {
            var marketplaceCompanies = _unitOfWork
               .MarketplaceCompanyRepository
               .DbSet()
               .Include(x => x.MarketplaceConfigurations)
               .Include(x => x.Marketplace)
               .Where(x => x.Active && x.MarketplaceId == "IK")
               .ToList();

            //var productInformationMarketplaces = _unitOfWork
            //    .ProductInformationMarketplaceRepository
            //    .DbSet()
            //    .Include(x => x.ProductInformationMarketplaceVariantValues)
            //    .Include(x => x.ProductInformation.Product.ProductMarketplaces)
            //    .Where(x => x.ProductInformation.DirtyProductInformation)
            //    .ToList();


            var products = _unitOfWork.ProductRepository
                      .DbSet()
                      .Include(x => x.ProductTranslations.Where(x => x.LanguageId == "TR"))
                      .Include(x => x.ProductMarketplaces)
                      .Include(x => x.ProductInformations)
                      .ThenInclude(x => x.ProductInformationMarketplaces)
                      .ThenInclude(x => x.ProductInformationMarketplaceVariantValues)
                      .ThenInclude(x => x.MarketplaceVariantValue.MarketplaceVariantsMarketplaceVariantValues)
                      .ThenInclude(x => x.MarketplaceVariant)
                      .Include(X => X.ProductInformations)
                      .ThenInclude(x => x.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                      .ThenInclude(x => x.ProductInformationContentTranslation)
                      .Where(x => x.ProductInformations.Any(x => x.DirtyProductInformation))
                      .ToList();






            foreach (var mpLoop in marketplaceCompanies)
            {



                foreach (var pLoop in products)
                {

                    var productMarketplace = pLoop.ProductMarketplaces.FirstOrDefault(x => x.MarketplaceCompanyId == mpLoop.Id);
                    if (productMarketplace != null)
                    {

                        var request = new ProductRequest
                        {
                            Configurations = mpLoop.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                            MarketPlaceId = mpLoop.Marketplace.Id,
                            ProductItem = new ProductItem
                            {
                                Name = pLoop.ProductTranslations.FirstOrDefault()?.Name,
                                BrandName = productMarketplace.MarketplaceBrandName,
                                BrandId = productMarketplace.MarketplaceBrandId,
                                DimensionalWeight = 1,
                                PreparingDayCount = 2,
                                UUId = productMarketplace.UUId,
                                CategoryId = productMarketplace.MarketplaceCategoryId.Replace($"{mpLoop.MarketplaceId}-", ""),
                                StockItems = new List<StockItem>(),
                                SellerCode = productMarketplace.SellerCode,
                            }
                        };

                        foreach (var piLoop in pLoop.ProductInformations)
                        {
                            var productInformationPrice = _unitOfWork.ProductInformationPriceRepository.DbSet().FirstOrDefault(x => x.ProductInformationId == piLoop.Id && x.CurrencyId == "TL");

                            var productInformationPhotos = _unitOfWork.ProductInformationPhotoRepository.DbSet().Where(x => x.ProductInformationId == piLoop.Id).ToList();

                            var productInformationMarketplace = piLoop.ProductInformationMarketplaces.FirstOrDefault(x => x.MarketplaceCompanyId == mpLoop.Id && x.Active);
                            if (productInformationMarketplace != null && productInformationPhotos.Count > 0)
                            {
                                var stockItem = new StockItem
                                {
                                    Active = productInformationMarketplace.Active,
                                    Barcode = productInformationMarketplace.Barcode,
                                    SkuCode = piLoop.SkuCode,
                                    Quantity = piLoop.Stock,
                                    UUId = productInformationMarketplace.UUId,
                                    StockCode = productInformationMarketplace.StockCode,
                                    VatRate = productInformationPrice.VatRate,
                                    ListPrice = productInformationMarketplace.ListUnitPrice,
                                    SalePrice = productInformationMarketplace.UnitPrice,
                                    Title = piLoop.ProductInformationTranslations.FirstOrDefault().Name,
                                    Subtitle = piLoop.ProductInformationTranslations.FirstOrDefault().SubTitle,
                                    Images = productInformationPhotos.OrderBy(x => x.DisplayOrder).Select(im => new Image
                                    {
                                        Url = $"{this._appConfigs.Value.Tenant.WebUrl}/img/product/{im.FileName}",
                                        FileName = im.FileName,
                                        FilePath = $"{this._appConfigs.Value.Tenant.Product.FileName}\\{im.FileName.Replace("/", "\\")}",
                                        Order = productInformationPhotos.IndexOf(im)
                                    }).ToList(),
                                    Description = piLoop.ProductInformationTranslations.FirstOrDefault()?.ProductInformationContentTranslation?.Description,
                                    Attributes = productInformationMarketplace.ProductInformationMarketplaceVariantValues.Select(x =>
                                    {
                                        var categoryAttribute = new CategoryAttribute();
                                        var marketplaceVariantsMarketplaceVariantValues = x.MarketplaceVariantValue.MarketplaceVariantsMarketplaceVariantValues.FirstOrDefault();
                                        if (marketplaceVariantsMarketplaceVariantValues == null) return new CategoryAttribute();

                                        categoryAttribute.AttributeCode = marketplaceVariantsMarketplaceVariantValues.MarketplaceVariantId.Replace($"{mpLoop.MarketplaceId}-", "").ToString();
                                        categoryAttribute.AttributeName = marketplaceVariantsMarketplaceVariantValues.MarketplaceVariant.Name;
                                        categoryAttribute.AttributeValueCode = x.MarketplaceVariantValueId.Replace($"{mpLoop.MarketplaceId}-", "");
                                        categoryAttribute.AttributeValueName = x.MarketplaceVariantValue.Value;


                                        return categoryAttribute;
                                    }
                                    ).ToList()
                                };

                                request.ProductItem.StockItems.Add(stockItem);
                            }


                        }

                        if (request.ProductItem.StockItems.Count > 0)
                        {
                            var response = _productFacade.Create(request);

                            Console.BackgroundColor = ConsoleColor.Green;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine($"[{mpLoop.Marketplace.Name}] Id:[{pLoop.Id}] SellerCode:[{pLoop.SellerCode}] [{request.ProductItem.Name}] ürünü eklendi");
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                            //if (response.Success)
                            //{
                            //    pLoop.ProductInformations.ForEach(x =>
                            //    {
                            //        x.DirtyProductInformation = false;

                            //    });

                            //    _unitOfWork.ProductRepository.Update(pLoop);

                            //}
                        }
                    }


                }


            }

        }
    }
}
