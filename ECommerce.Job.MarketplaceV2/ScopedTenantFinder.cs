﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.MarketplaceV2
{
    public class ScopedTenantFinder : ISetableTenantFinder
    {
        #region Fields
        public int _tenantId;
        #endregion

        #region Methods
        public int FindId()
        {
            return _tenantId;
        }

        public void SetId(int id)
        {
            this._tenantId = id;
        }
        #endregion
    }
}
