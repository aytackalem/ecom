﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.MarketplaceV2
{
    public class ScopedCompanyFinder : ISetableCompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Methods
        public int FindId()
        {
            return _companyId;
        }

        public void SetId(int id)
        {
            this._companyId = id;
        }
        #endregion
    }
}
