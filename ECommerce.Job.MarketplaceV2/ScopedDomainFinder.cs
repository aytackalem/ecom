﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.MarketplaceV2
{
    public class ScopedDomainFinder : ISetableDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }

        public void SetId(int id)
        {
            this._domainId = id;
        }
        #endregion
    }
}
