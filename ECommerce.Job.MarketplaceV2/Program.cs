﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using ECommerce.Infrastructure;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Marketplace;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.MarketplaceV2
{
    class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            var currentProcess = Process.GetCurrentProcess();
            var mainModule = currentProcess.MainModule;
            if (mainModule == null)
                throw new Exception("Main module is null.");

            var mainModuleFileName = mainModule.FileName;
            if (mainModuleFileName == null)
                throw new Exception("Main module file name is null.");

            var configurationBasePathDirectoryInfo = Directory.GetParent(mainModuleFileName);
            if (configurationBasePathDirectoryInfo == null)
                throw new Exception("Configuration base path directory info is null.");

            var basePath = configurationBasePathDirectoryInfo.FullName;

            return new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddMemoryCache();

            serviceCollection.AddSingleton<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<ITenantFinder, ScopedTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, ScopedDomainFinder>();

            serviceCollection.AddScoped<ICompanyFinder, ScopedCompanyFinder>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Ecommerce"));
            }, ServiceLifetime.Scoped);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();

            serviceCollection.AddScoped<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();

            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();

            serviceCollection.AddInfrastructureServices();

            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            Stopwatch stopwatch = new();
            stopwatch.Start();

            args = new[] { "RawOrders" };

            if (args == null || args.Length == 0)
                throw new ArgumentNullException("Args is null.");

            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);

            List<Company> companies;

            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetService<IUnitOfWork>();
                if (unitOfWork == null)
                    throw new Exception("Unit of work is null.");

                companies = unitOfWork
                    .TenantRepository
                    .DbSet()
                    .Include(t => t.Domains)
                        .ThenInclude(d => d.Companies)
                    .SelectMany(t => t.Domains.SelectMany(d => d.Companies))
                    .ToList();
            }

            if (companies != null && companies.Count > 0)
                Parallel.ForEach(companies, theCompany =>
                {
                    using (var scope = serviceProvider.CreateScope())
                    {
                        /* Inject tenant, domain, company. */
                        var tenantFinder = scope.ServiceProvider.GetService<ITenantFinder>() as ScopedTenantFinder;
                        var domainFinder = scope.ServiceProvider.GetService<IDomainFinder>() as ScopedDomainFinder;
                        var companyFinder = scope.ServiceProvider.GetService<ICompanyFinder>() as ScopedCompanyFinder;
                        if (tenantFinder == null || domainFinder == null || companyFinder == null)
                            throw new Exception("Tenant finder or domain finder or company finder is null.");

                        tenantFinder._tenantId = theCompany.TenantId;
                        domainFinder._domainId = theCompany.DomainId;
                        companyFinder._companyId = theCompany.Id;

                        var marketplaceService = scope.ServiceProvider.GetRequiredService<IMarketplaceService>();
                        if (marketplaceService == null)
                            throw new Exception("Marketplace service is null.");

                        switch (args[0])
                        {
                            case "RawOrders":
                                marketplaceService.RawOrders();
                                break;
                            case "ProcessRawOrders":
                                marketplaceService.ProcessRawOrders();
                                break;
                            case "ProcessOrderTypeUpdates":
                                marketplaceService.ProcessOrderTypeUpdates();
                                break;
                            case "ProcessMutualBarcodes":
                                marketplaceService.ProcessMutualBarcodes();
                                break;
                        }
                    }
                });

            stopwatch.Stop();
        }
        #endregion
    }
}