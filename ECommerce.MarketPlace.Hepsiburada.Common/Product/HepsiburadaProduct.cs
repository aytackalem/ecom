﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Product
{
    public class HepsiburadaProduct
    {
        #region Property
        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("merchant")]
        public string Merchant { get; set; }

        [JsonProperty("attributes")]
        public HepsiburadaProductAttribute Attributes { get; set; }
        #endregion
    }
}
