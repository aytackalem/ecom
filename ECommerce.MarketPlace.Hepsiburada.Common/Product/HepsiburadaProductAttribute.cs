﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Product
{
    public class HepsiburadaProductAttribute
    {
        #region Property
        [JsonProperty("merchantSku")]
        public string MerchantSku { get; set; }

        public string VaryantGroupID { get; set; }

        public string Barcode { get; set; }

        public string UrunAdi { get; set; }

        public string UrunAciklamasi { get; set; }

        public string Marka { get; set; }

        public int GarantiSuresi { get; set; }

        [JsonProperty("kg")]
        public string Kg { get; set; }
        [JsonProperty("tax_vat_rate")]
        public string Tax_vat_rate { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("stock")]
        public string Stock { get; set; }

        public string Image1 { get; set; }

        public string Image2 { get; set; }

        public string Image3 { get; set; }

        public string Image4 { get; set; }

        public string Image5 { get; set; }

        public string Renk_variant_property { get; set; }

        public string Ebatlar_variant_property { get; set; }
        #endregion
    }
}
