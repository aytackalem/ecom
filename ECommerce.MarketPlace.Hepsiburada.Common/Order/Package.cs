﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class Package
    {
        public string PackageNumber { get; set; }
        public string Barcode { get; set; }
    }
}
