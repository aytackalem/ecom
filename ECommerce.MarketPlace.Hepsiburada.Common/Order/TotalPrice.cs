﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class TotalPrice
    {
        public string Currency { get; set; }
        public double Amount { get; set; }
    }
}
