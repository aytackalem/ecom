﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class CargoPage
    {
        public string PackageNumber { get; set; }
        public string Barcode { get; set; }
        public string Status { get; set; }
        public string CargoCompany { get; set; }
        public string TrackingInfoCode { get; set; }
        public string TrackingInfoUrl { get; set; }
    }
}
