﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class LineItemRequest
    {
        public string Id { get; set; }
        public string Quantity { get; set; }
    }
}
