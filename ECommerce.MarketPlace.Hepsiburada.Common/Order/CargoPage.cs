﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class MutualBarcode
    {
        public string Format { get; set; }
        
        public List<string> Data { get; set; }
        
        public string Description { get; set; }
        
        public string Code { get; set; }
    }
}
