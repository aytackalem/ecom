﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class OrderPage
    {
        public int TotalCount { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int PageCount { get; set; }
        public IList<Item> Items { get; set; }
    }
}
