﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Order
{
    public class Item
    {
        public DateTime DueDate { get; set; }
        public DateTime LastStatusUpdateDate { get; set; }
        public string Id { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string OrderId { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }
        public string MerchantId { get; set; }
        public TotalPrice TotalPrice { get; set; }
        public UnitPrice UnitPrice { get; set; }
        public HbDiscount HbDiscount { get; set; }
        public double Vat { get; set; }
        public double VatRate { get; set; }
        public string CustomerName { get; set; }
        public string Status { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public Invoice Invoice { get; set; }
        public string SapNumber { get; set; }
        public int DispatchTime { get; set; }
        public Commission Commission { get; set; }
        public int PaymentTermInDays { get; set; }
        public int CommissionType { get; set; }
        public CargoCompanyModel CargoCompanyModel { get; set; }
        public string CargoCompany { get; set; }
        public string CustomizedText01 { get; set; }
        public string CustomizedText02 { get; set; }
        public string CustomizedText03 { get; set; }
        public string CustomizedText04 { get; set; }
        public string CustomizedTextX { get; set; }
        public object CreditCardHolderName { get; set; }
        public bool IsCustomized { get; set; }
        public bool CanCreatePackage { get; set; }
        public bool IsCancellable { get; set; }
        public bool IsCancellableByHbAdmin { get; set; }
        public string DeliveryType { get; set; }
        public int DeliveryOptionId { get; set; }
        public object Slot { get; set; }
        public object PickUpTime { get; set; }
        public IList<object> DiscountInfo { get; set; }
        public string MerchantSKU { get; set; }
        public PurchasePrice PurchasePrice { get; set; }
    }
}
