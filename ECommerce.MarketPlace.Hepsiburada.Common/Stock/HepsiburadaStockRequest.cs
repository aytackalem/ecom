﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Stock
{
    public class HepsiburadaStockRequest
    {
        public string HepsiburadaSku { get; set; }
        public string MerchantSku { get; set; }
        public int AvailableStock { get; set; }
    }
}
