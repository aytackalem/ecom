﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Category
{
    public class HepsiburadaCategoryAttribute
    {
        #region Property
        public List<HepsiburadaBaseAttribute> BaseAttributes { get; set; }

        public List<HepsiburadaAttribute> Attributes { get; set; }

        public List<HepsiburadaVariantAttribute> VariantAttributes { get; set; }
        #endregion
    }
}
