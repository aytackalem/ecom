﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Category
{
    public class HepsiburadaCategoryAttributeValuePage
    {
        #region Property
        public bool Success { get; set; }

        public int Code { get; set; }

        public bool Last { get; set; }

        public int Version { get; set; }

        public object Message { get; set; }

        public List<HepsiburadaCategoryAttributeValue> Data { get; set; }
        #endregion
    }
}
