﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Common.Category
{
    public class HepsiburadaCategoryAttributeValue
    {
        #region Property
        public string Id { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
