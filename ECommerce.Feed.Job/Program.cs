﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ECommerce.Feed.Job
{
    class Item
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }
    }

    class Feed
    {
        public int Id { get; set; }

        public string FileName { get; set; }
    }

    class Program
    {
        private readonly static string connectionString = "Server=185.67.204.114;Database=Dev_ECommerce;UId=marketplace;Pwd=f2t3Q6*Pd@Na;";

        static void Main(string[] args)
        {
            //Tüm ürünler
            Write(@"
Select		[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,Max([PIP].[FileName]) [FileName]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]
From		Products As P
Join		ProductInformations As [PI]
On			P.Id = [PI].ProductId
Join		ProductInformationTranslations As [PIT]
On			[PI].Id = [PIT].ProductInformationId
Join		ProductInformationPhotos As [PIP]
On			[PI].Id = [PIP].ProductInformationId
Join		ProductInformationPriceses As [PIPR]
On			[PI].Id = [PIPR].ProductInformationId
Where		P.Active = 1
            And P.IsOnlyPanel = 0
Group By	[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]", "feed");

            //Stok uygunluğu olan ürünler
            Write(@"
Select		[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,Max([PIP].[FileName]) [FileName]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]
From		Products As P
Join		ProductInformations As [PI]
On			P.Id = [PI].ProductId
            And [PI].Stock > 0
            And [PI].IsSale = 1
Join		ProductInformationTranslations As [PIT]
On			[PI].Id = [PIT].ProductInformationId
Join		ProductInformationPhotos As [PIP]
On			[PI].Id = [PIP].ProductInformationId
Join		ProductInformationPriceses As [PIPR]
On			[PI].Id = [PIPR].ProductInformationId
Where		P.Active = 1
            And P.IsOnlyPanel = 0
Group By	[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]", "google-feed");

            List<Feed> feeds = new();
            using (SqlConnection sqlConnection = new(connectionString))
            {
                try
                {
                    using (SqlCommand sqlCommand = new("Select Id, FileName From Feeds", sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            feeds.Add(new Feed
                            {
                                Id = sqlDataReader.GetInt32(0),
                                FileName = sqlDataReader.GetString(1)
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            feeds.ForEach(fLoop =>
            {
                Write(@"
Select		[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,Max([PIP].[FileName]) [FileName]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]
From		ProductInformations As [PI]
Join        FeedDetails As FD
On          FD.ProductInformationId = [PI].Id
            And FD.FeedId = " + fLoop.Id + @"
Join		ProductInformationTranslations As [PIT]
On			[PI].Id = [PIT].ProductInformationId
Join		ProductInformationPhotos As [PIP]
On			[PI].Id = [PIP].ProductInformationId
Join		ProductInformationPriceses As [PIPR]
On			[PI].Id = [PIPR].ProductInformationId
Group By	[PIT].[Id]
			,[PIT].[Name]
			,[PIT].[Url]
			,[PIPR].[ListUnitPrice]
			,[PIPR].[UnitPrice]", fLoop.FileName);
            });
        }

        private static void Write(string query, string name)
        {
            List<Item> items = new();

            /* Products */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            items.Add(new Item
                            {
                                Id = sqlDataReader.GetInt32(0),
                                Name = sqlDataReader.GetString(1),
                                Url = sqlDataReader.GetString(2),
                                FileName = sqlDataReader.GetString(3),
                                ListUnitPrice = sqlDataReader.GetDecimal(4),
                                UnitPrice = sqlDataReader.GetDecimal(5),
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            var xml = $"<rss xmlns:g=\"http://base.google.com/ns/1.0\" version=\"2.0\"><channel><title>Fidan İstanbul</title><link>http://www.fidanistanbul.com</link><description>Fidan Satışı, Fide Satışı, internetten Fidan Siparişi, Bodur Aşılı Sertifikalı Meyve Fidanı Süs Bitkileri</description>{string.Join("", items.Select(i => $"<item><title>{i.Name}</title><link>http://www.fidanistanbul.com/{i.Url}</link><description><![CDATA[{i.Name}]]></description><g:image_link>http://www.fidanistanbul.com/imgs/product/{i.FileName}</g:image_link><g:price>{i.ListUnitPrice} TRY</g:price><g:sale_price>{i.UnitPrice} TRY</g:sale_price><g:availability>in stock</g:availability><g:google_product_category>Ev ve Bahçe > Bitkiler</g:google_product_category><g:product_type>Ev ve Bahçe > Bitkiler</g:product_type><g:condition>new</g:condition><g:id>{i.Id}</g:id><g:brand>fidanistanbul</g:brand></item>"))}</channel></rss>";

            File.WriteAllText($@"C:\inetpub\fidanistanbul.com\www\wwwroot\{name}.xml", xml);
        }
    }
}
