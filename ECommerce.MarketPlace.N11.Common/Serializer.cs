﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ECommerce.MarketPlace.N11.Common
{
    public static class Serializer
    {
        public static DataResponse<string> Post(string data, string address)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
                webRequest.ContentType = "text/xml";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";

                using (var stream = webRequest.GetRequestStream())
                {
                    using (var streamWriter = new StreamWriter(stream))
                    {
                        streamWriter.Write(data);
                    }
                }

                var result = string.Empty;

                using (var streamReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                response.Data = result;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }       


        public static T Deserialize<T>(string response)
        {


            XmlDocument responseXmlDocument = new XmlDocument();
            responseXmlDocument.LoadXml(response);

            T t = default(T);
            if (responseXmlDocument == null) return t;
            XmlNamespaceManager ns = new XmlNamespaceManager(responseXmlDocument.NameTable);
            ns.AddNamespace("n3", "http://www.n11.com/ws/schemas");


            var typeName = typeof(T).Name;

            var tString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + $"<{typeName}>{responseXmlDocument.SelectSingleNode($"//n3:{typeName}", ns).InnerXml}</{typeName}>";



            using (var stringReader = new StringReader(tString))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                t = (T)xmlSerializer.Deserialize(stringReader);
            }

            return t;
        }
    }
}
