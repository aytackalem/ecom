﻿using System;

namespace ECommerce.Accounting.Common
{
    public class AccountingCancel
    {
        #region Properties
        public string PaymentType { get; set; }

        public string PaymentSource { get; set; }

        public string DocumentNo { get; set; }
        #endregion
    }
}
