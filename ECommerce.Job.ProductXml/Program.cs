﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.ProductXml
{
    class Program
    {
        #region Methods
        static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfiguration configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddTransient<IConfiguration>(x => configuration);

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder>(cf => null);

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IXmlService, XmlService>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main()
        {
            await ConfigureServiceCollection(GetConfiguration()).GetService<IApp>().RunAsync();
        }
        #endregion
    }
}