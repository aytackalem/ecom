﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System.Xml.Linq;

namespace ECommerce.Job.ProductXml
{
    public class DynamicDomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public DynamicDomainFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }

        public void Set(int id)
        {
            this._domainId = id;
        }
        #endregion
    }
}
