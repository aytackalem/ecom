﻿namespace ECommerce.Job.ProductXml
{
    public interface IApp
    {
        #region Methods
        Task RunAsync(); 
        #endregion
    }
}
