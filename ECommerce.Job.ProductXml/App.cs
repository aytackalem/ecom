﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Panel.DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace ECommerce.Job.ProductXml
{
    /// <summary>
    /// Bizim sistemizdeki ürünleri dışarıya xml olarak çıkartır.
    /// </summary>
    public class App : IApp
    {
        #region Fields
        private readonly IXmlService _xmlService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, IConfiguration configuration, IXmlService xmlService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._configuration = configuration;
            this._xmlService = xmlService;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync()
        {
            var tenants = this
                ._unitOfWork
                .TenantRepository
                .DbSet()
                .Include(t => t.TenantSetting)
                .ToList();
            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;

#if DEBUG
                    if (theTenant.Id != 5)
                        continue;
#endif

                    var products = this
                        ._unitOfWork
                        .ProductRepository
                        .DbSet()
                        .Where(p => p.TenantId == theTenant.Id)
                        .Select(p => new Product
                        {
                            Category = new Category
                            {
                                Id = p.Category.Id,
                                CategoryTranslations = p
                                    .Category
                                    .CategoryTranslations
                                    .Where(ct => ct.LanguageId == "TR")
                                    .Select(ct => new CategoryTranslation
                                    {
                                        Name = ct.Name
                                    })
                                    .ToList()
                            },
                            SellerCode = p.SellerCode,
                            ProductInformations = p
                                .ProductInformations
                                .Where(pi => pi.Active &&
                                             pi.Type == "PI")
                                .Select(pi => new ProductInformation
                                {
                                    StockCode = pi.StockCode,
                                    Stock = pi.Stock,
                                    ProductInformationTranslations = pi
                                        .ProductInformationTranslations
                                        .Where(pit => pit.LanguageId == "TR")
                                        .Select(pit => new ProductInformationTranslation
                                        {
                                            Name = pit.Name,
                                            SubTitle = pit.SubTitle,
                                            ProductInformationContentTranslation = new()
                                            {
                                                Description = pit.ProductInformationContentTranslation.Description
                                            }
                                        })
                                        .ToList(),
                                    ProductInformationPriceses = pi
                                        .ProductInformationPriceses
                                        .Where(pir => pir.CurrencyId == "TL")
                                        .Select(pir => new ProductInformationPrice
                                        {
                                            ListUnitPrice = pir.ListUnitPrice,
                                            UnitPrice = pir.UnitPrice,
                                            VatRate = pir.VatRate
                                        })
                                        .ToList(),
                                    ProductInformationPhotos = pi
                                        .ProductInformationPhotos
                                        .Where(pip => pip.Deleted == false)
                                        .OrderBy(pip => pip.DisplayOrder)
                                        .Select(pip => new ProductInformationPhoto
                                        {
                                            FileName = pip.FileName
                                        })
                                        .ToList()
                                })
                                .ToList()
                        })
                        .ToList();

                    var values = products
                        .SelectMany(p => p.ProductInformations.Select(pi => new DataTransferObjects.Product
                        {
                            Name = pi
                                .ProductInformationTranslations
                                .FirstOrDefault()
                                .Name,
                            CategoryName = p
                                .Category
                                .CategoryTranslations
                                .FirstOrDefault()?.Name,
                            Photos = pi
                                .ProductInformationPhotos
                                .Select(pip => new DataTransferObjects.Photo
                                {
                                    Url = pip.FileName
                                })
                                .ToList(),
                            Stock = pi.Stock,
                            StockCode = pi.StockCode,
                            ListUnitPrice = pi
                                .ProductInformationPriceses
                                .FirstOrDefault()
                                .ListUnitPrice,
                            UnitPrice = pi
                                .ProductInformationPriceses
                                .FirstOrDefault()
                                .UnitPrice,
                            VatRate = Convert.ToInt32(pi
                                .ProductInformationPriceses
                                .FirstOrDefault()
                                .VatRate * 100),
                            Description = pi
                                .ProductInformationTranslations
                                .FirstOrDefault()
                                .ProductInformationContentTranslation
                                .Description,
                            SubTitle = pi
                                .ProductInformationTranslations
                                .FirstOrDefault()
                                .SubTitle
                        }))
                        .ToList();

                    this._xmlService.Write<DataTransferObjects.Product>(@"C:\Logs\a.xml", values);
                }
            }
        }
        #endregion
    }
}
