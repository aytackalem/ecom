﻿namespace ECommerce.Job.ProductXml.DataTransferObjects
{
    public class Photo
    {
        #region Properties
        public string Url { get; set; }
        #endregion
    }
}
