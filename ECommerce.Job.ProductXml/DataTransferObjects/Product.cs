﻿namespace ECommerce.Job.ProductXml.DataTransferObjects
{
    public class Product
    {
        #region Properties
        public string Name { get; set; }

        public string StockCode { get; set; }

        public int Stock { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public int VatRate { get; set; }

        public string Description { get; set; }

        public string SubTitle { get; set; }

        public string CategoryName { get; set; }

        public List<Photo> Photos { get; set; }
        #endregion
    }
}
