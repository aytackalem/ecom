﻿namespace ECommerce.Job.Shipment
{
    public interface IApp
    {
        #region Methods
        void Run(); 
        #endregion
    }
}
