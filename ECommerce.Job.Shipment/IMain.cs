﻿namespace ECommerce.Job.Shipment
{
    public interface IMain
    {
        #region Methods
        Task Run(); 
        #endregion
    }
}
