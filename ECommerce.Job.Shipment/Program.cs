﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Job.Shipment;
using ECommerce.Job.Shipment.Caching;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Accounting
{
    class Program
    {
        #region Methods

        static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfiguration configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddScoped<IMain, Main>();

            serviceCollection.AddScoped<IApp, App>();

            serviceCollection.AddScoped<IDbService, DbService>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddScoped<IDbService, DbService>();

            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();

            serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();

            serviceCollection.AddInfrastructureServices(configuration);


            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            }, ServiceLifetime.Scoped);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddMemoryCache();


            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main()
        {
            await ConfigureServiceCollection(GetConfiguration()).GetService<IMain>()?.Run();
        }
        #endregion
    }
}