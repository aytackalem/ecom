﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Job.Shipment
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IShipmentProviderService _shipmentProviderService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IShipmentProviderService shipmentProviderService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._shipmentProviderService = shipmentProviderService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            Register();
        }

        public void Register()
        {
            

            var orders = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderShipments)
                .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
                .Where(o =>
                string.IsNullOrEmpty(o.MarketplaceId)
                &&
                o.OrderShipments.Any(x => String.IsNullOrEmpty(x.TrackingCode))
                &&
                ((o.OrderTypeId == OrderTypes.Beklemede
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit))
                ||
                ((o.OrderTypeId == OrderTypes.Beklemede || o.OrderTypeId == OrderTypes.OnaylanmisSiparis)
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.Havale && x.Paid))
                ||
                ((o.OrderTypeId == OrderTypes.Beklemede || o.OrderTypeId == OrderTypes.OnaylanmisSiparis)
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.OnlineOdeme && x.Paid))))
                .ToList();

            //var orders = this
            //    ._unitOfWork
            //    .OrderRepository
            //    .DbSet()
            //    .Include(o => o.OrderShipments)
            //    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
            //    .Where(o => o.OrderTypeId == OrderTypes.Beklemede && 
            //                o.OrderSourceId == OrderSources.TemsilciInfluencer)
            //    .ToList();

            Console.WriteLine(orders.Count + " found.");

            foreach (var oLoop in orders)
            {
                var postResponse = this._shipmentProviderService.Post(oLoop.Id, true, new int[] { 1 }, 1);
                if (postResponse.Success)
                {
                    oLoop.OrderTypeId = OrderTypes.OnaylanmisSiparis;

                    this
                        ._unitOfWork
                        .OrderRepository
                        .Update(oLoop);
                }
            }
        }
        #endregion
    }
}
