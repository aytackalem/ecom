﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace ECommerce.Lafaba.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();

            //app.Run();
            //app.RunPhotoVariant();
            app.RunPhoto();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("ECommerce"));
            });

            serviceCollection.AddSingleton<IApp, App>();
            serviceCollection.AddScoped<IImageHelper, ImageHelper>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();
            serviceCollection.AddScoped<IRandomHelper, RandomHelper>();
            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinderFinder>();
            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            return serviceCollection.BuildServiceProvider();
        }
    }
}
