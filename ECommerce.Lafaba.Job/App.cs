﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using ECommerce.Lafaba.Job.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace ECommerce.Lafaba.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRandomHelper _randomHelper;

        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IHttpHelper httpHelper, IRandomHelper randomHelper)
        {
            _unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            this._randomHelper = randomHelper;
        }
        #endregion

        #region Methods

        public void Run()
        {
            int count = 0;

            var request = new ProductListRequest();
            request.Configurations = new Dictionary<string, string>();
            request.Configurations.Add("ApiUrl", "https://www.lafaba.com/kmservices/kobimasterservice.svc");
            request.Configurations.Add("Username", "webservis@lafabademo.com");
            request.Configurations.Add("Password", "4ayPZbvm");
            var productListResponse = new ProductListResponse();
            productListResponse.ProductItems = new List<ProductItem>();
            while (true)
            {
                request.Count = count;
                Console.WriteLine("Request Atılıyor");

                var response = this._httpHelper.Post<ProductListRequest, DataResult<ProductListResponse>>(request, $"http://omni.helpy.com.tr/Product/Get", null, null);

                if (response.Data.ProductItems.Count == 0) break;


                productListResponse.ProductItems.AddRange(response.Data.ProductItems);
                Console.WriteLine($"Gelen ürün sayısı {response.Data.ProductItems.Count} Toplam Ürün Sayısı:{productListResponse.ProductItems.Count}");


                count += 100;

            }

            var _products = new List<Product>();
            var variantValues = this._unitOfWork.VariantValueRepository.DbSet().Include(x => x.VariantValueTranslations).ToList();
            var variants = this._unitOfWork.VariantRepository.DbSet().Include(x => x.VariantTranslations).ToList();
            var categories = _unitOfWork.CategoryRepository.DbSet().ToList();
            var productInformations = _unitOfWork.ProductInformationRepository.DbSet().ToList();

            foreach (var productItem in productListResponse.ProductItems)
            {


                var _product = new Product
                {
                    BrandId = 1,
                    CategoryId = Convert.ToInt32(productItem.CategoryId),
                    GroupId = 1,
                    Active = productItem.Active,
                    IsOnlyHidden = false,
                    ManagerUserId = 1,
                    SupplierId = 1,
                    SellerCode = productItem.SellerCode,
                    CreatedDate = DateTime.Now,
                    TenantId = 1,
                    DomainId = 1,
                    CategoryProducts = new List<CategoryProduct>(),
                    ProductInformations = new List<ProductInformation>()
                };

                var categoryProducts = new List<Domain.Entities.CategoryProduct>();
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, Convert.ToInt32(productItem.CategoryId), 0, false, false));
                foreach (var categoryId in productItem.MultiCategoryIds)
                {
                    if (categoryId == Convert.ToInt32(productItem.CategoryId)) continue;

                    categoryProducts.AddRange(RecursiveCategoryProduct(categories, categoryId, 0, true, true));

                }
                _product.CategoryProducts = categoryProducts.GroupBy(x => x.CategoryId).Select(x => x.FirstOrDefault()).ToList();

                var anyProductVariant = false;

                foreach (var stockItem in productItem.StockItems)
                {


                    anyProductVariant = productInformations.Any(x => x.StockCode == stockItem.SellerStockCode);
                    if (anyProductVariant) continue;

                    var _productInformation = new ProductInformation
                    {

                        Barcode = stockItem.Barcode,
                        Type = "PI",
                        ProductId = _product.Id,
                        StockCode = stockItem.SellerStockCode,
                        SkuCode = stockItem.SkuCode,
                        Active = stockItem.Active,
                        IsSale = stockItem.IsSale,
                        CreatedDate = DateTime.Now,
                        ManagerUserId = 1,
                        Stock = stockItem.Quantity,
                        DirtyProductInformation = false,
                        DirtyStock = false,
                        Payor = false,
                        TenantId = 1,
                        DomainId = 1,
                        ProductInformationPhotos = new List<ProductInformationPhoto>(),
                        ProductInformationPriceses = new List<ProductInformationPrice>(),
                        ProductInformationTranslations = new List<ProductInformationTranslation>(),
                        ProductInformationVariants = new List<ProductInformationVariant>()
                    };


                    _productInformation.ProductInformationTranslations.Add(new ProductInformationTranslation
                    {
                        LanguageId = "TR",
                        ManagerUserId = 1,
                        CreatedDate = DateTime.Now,
                        Name = stockItem.Title,
                        SubTitle = "",
                        Url = "",
                        TenantId = 1,
                        DomainId = 1,
                        ProductInformationContentTranslation = new ProductInformationContentTranslation
                        {
                            ExpertOpinion = string.Empty,
                            Description = stockItem.Description,
                            Content = stockItem.Description,
                            ManagerUserId = 1,
                            CreatedDate = DateTime.Now,
                            TenantId = 1,
                            DomainId = 1
                        },
                        ProductInformationSeoTranslation = new ProductInformationSeoTranslation
                        {
                            ManagerUserId = 1,
                            Title = $"{stockItem.Title} {stockItem.SellerStockCode} - Lafaba",
                            MetaDescription = " ",
                            MetaKeywords = stockItem.SearchKey,
                            CreatedDate = DateTime.Now,
                            TenantId = 1,
                            DomainId = 1
                        }
                    });

                    foreach (var image in stockItem.Images)
                    {

                        _productInformation.ProductInformationPhotos.Add(new ProductInformationPhoto
                        {
                            CreatedDate = DateTime.Now,
                            ManagerUserId = 1,
                            DisplayOrder = image.Order,
                            FileName = image.Url,
                            TenantId = 1,
                            DomainId = 1
                        });
                    }

                    _productInformation.ProductInformationPriceses.Add(new ProductInformationPrice
                    {

                        CreatedDate = DateTime.Now,
                        CurrencyId = "TL",
                        ListUnitPrice = stockItem.ListPrice,
                        ManagerUserId = 1,
                        UnitCost = stockItem.BuyPrice,
                        UnitPrice = stockItem.SalePrice,
                        VatRate = 0.18m,
                        TenantId = 1,
                        DomainId = 1

                    });



                    stockItem.Attributes.ForEach(attribute =>
                    {
                        var variantValue = variantValues.SelectMany(x => x.VariantValueTranslations).FirstOrDefault(x => x.Value.ReplaceChar() == attribute.AttributeValueName.ReplaceChar());

                        if (variantValue == null)
                        {
                            var variant = variants.SelectMany(x => x.VariantTranslations).FirstOrDefault(x => x.Name.ReplaceChar() == attribute.AttributeName.ReplaceChar());
                            if (variant == null)
                            {
                                return;
                            }

                            var _variantValue = new VariantValue
                            {

                                VariantId = variant.VariantId,
                                ManagerUserId = 1,
                                CreatedDate = DateTime.Now,
                                TenantId = 1,
                                DomainId = 1,
                                VariantValueTranslations = new List<VariantValueTranslation>
                                {
                                    new VariantValueTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    ManagerUserId = 1,
                                    LanguageId = "TR",
                                    TenantId=1,
                                    DomainId=1,
                                    Value=attribute.AttributeValueName.ReplaceChar()
                                }
                                }
                            };

                            _unitOfWork.VariantValueRepository.Create(_variantValue);
                            variantValues.Add(_variantValue);
                            variantValue = _variantValue.VariantValueTranslations[0];
                        }

                        _productInformation.ProductInformationVariants.Add(new ProductInformationVariant
                        {
                            CreatedDate = DateTime.Now,
                            ManagerUserId = 1,
                            VariantValueId = variantValue.VariantValueId,
                            TenantId = 1,
                            DomainId = 1
                        });

                    });

                    _product.ProductInformations.Add(_productInformation);
                }

                if (!anyProductVariant)
                    _products.Add(_product);
            }
            _unitOfWork.ProductRepository.BulkInsertMapping(_products);



        }


        public void RunPhotoVariant()
        {
            var _products = _unitOfWork.ProductRepository.DbSet()
                .Include(x => x.ProductInformations)
                .ThenInclude(x => x.ProductInformationVariants)
                .ThenInclude(x => x.VariantValue.Variant)
                .Where(x => x.IsOnlyHidden);

            var productInformations = new List<ProductInformation>();


            foreach (var product in _products)
            {
                var productInformationVarinats = new List<ProductInformation>();
                foreach (var productInformation in product.ProductInformations)
                {
                    var productVariant = productInformation.ProductInformationVariants.FirstOrDefault(x => x.VariantValue.VariantId == 1);
                    var productInformationSelected = productInformationVarinats.SelectMany(x => x.ProductInformationVariants).FirstOrDefault(x => x.VariantValueId == productVariant.VariantValueId);

                    if (productVariant != null && productInformationSelected != null)
                    {
                        productInformation.GroupId = productInformationVarinats.FirstOrDefault(x => x.Id == productInformationSelected.ProductInformationId).GroupId;
                    }
                    else
                    {
                        productInformation.Photoable = true;
                        productInformation.GroupId = Guid.NewGuid().ToString().Replace("-", "");
                        productInformationVarinats.Add(productInformation);
                    }
                }
                productInformations.AddRange(product.ProductInformations);

            }

            _unitOfWork.ProductInformationRepository.Update(productInformations);


        }


        public void RunPhoto()
        {
            var _products = _unitOfWork.ProductRepository.DbSet()
                .Include(x => x.ProductInformations)
                .ThenInclude(x => x.ProductInformationPhotos)
                .Include(x => x.ProductInformations)
                .ThenInclude(x => x.ProductInformationTranslations)
                .Where(x => x.IsOnlyHidden);

            var productPhotos = new List<ProductInformationPhoto>();
            foreach (var pLoop in _products)
            {
                var photoables = pLoop.ProductInformations.Where(x => x.Photoable);

                foreach (var photo in photoables)
                {
                    foreach (var photoLoop in photo.ProductInformationPhotos)
                    {
                        var splitPhoto = photoLoop.FileName.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                        var fileName = $"{photoLoop.ProductInformationId}/{splitPhoto[3]}";
                        Download(fileName, photoLoop.ProductInformationId, photoLoop.FileName);

                        photoLoop.FileName = fileName;

                        photoLoop.ProductInformation = null;
                        productPhotos.Add(photoLoop);
                    }

                    var notPhotoTables = pLoop.ProductInformations.Where(x => x.GroupId == photo.GroupId && !x.Photoable).ToList();

                    foreach (var photoLoop in notPhotoTables)
                    {
                        for (int i = 0; i < photoLoop.ProductInformationPhotos.Count; i++)
                        {
                            photoLoop.ProductInformationPhotos[i].FileName = photo.ProductInformationPhotos[i].FileName;
                            photoLoop.ProductInformationPhotos[i].ProductInformation = null;
                            productPhotos.Add(photoLoop.ProductInformationPhotos[i]);
                        }
                    }
                }
            }
            _unitOfWork.ProductInformationPhotoRepository.Update(productPhotos);


            var products = _unitOfWork.ProductRepository.DbSet().Where(x => x.IsOnlyHidden).ToList();
            products.ForEach(x => { x.IsOnlyHidden = true; });

            _unitOfWork.ProductRepository.Update(products);
        }

        private List<Domain.Entities.CategoryProduct> RecursiveCategoryProduct(List<Domain.Entities.Category> categories, int categoryId, int productId, bool leaf, bool multi)
        {
            var categoryProducts = new List<Domain.Entities.CategoryProduct>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);

            categoryProducts.Add(new Domain.Entities.CategoryProduct
            {
                ManagerUserId = 1,
                ProductId = productId,
                CategoryId = category.Id,
                CreatedDate = DateTime.Now,
                Leaf = leaf,
                Multi = multi,
                TenantId = 1,
                DomainId = 1
            });

            if (category.ParentCategoryId.HasValue)
            {
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, productId, false, multi));
            }

            return categoryProducts;
        }
        private bool Download(string fileName, int productInformationId, string url)
        {
            var success = true;


            string directory = $@"C:\inetpub\ebtkurumsal\ecommerce\wwwroot\img\product\{productInformationId}";

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            string pathUrl = $@"C:\inetpub\ebtkurumsal\ecommerce\wwwroot\img\product\{fileName.Replace("/", "\\")}";
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile($"https://www.lafaba.com{url}", pathUrl);
                    Console.WriteLine("Resim yüklendi");
                }
                catch (Exception e)
                {
                    success = false;
                }
            }

            return success;
        }
        public class DataResult<TData>
        {
            #region Property
            public TData Data { get; set; }
            #endregion
        }

        #endregion
    }
}
