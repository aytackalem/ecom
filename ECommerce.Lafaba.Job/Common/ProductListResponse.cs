﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Lafaba.Job.Common
{
    public class ProductListResponse
    {
        public List<ProductItem> ProductItems { get; set; }
    }

    public class ProductItem
    {
        public string ProviderId { get; set; }

        public string SellerCode { get; set; }

        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string Description { get; set; }

        public string Brand { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string CurrencyCode { get; set; }

        /// <summary>
        /// Ürünün aktifliğini belirler
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Ürün kargo verilme süresi gün olarak
        /// </summary>
        public int PreparingDayCount { get; set; }

        public string ShipmentCompanyId { get; set; }

        public decimal VatRate { get; set; }


        //Ürün Desi Değeri
        public double DimensionalWeight { get; set; }

        public List<StockItem> StockItems { get; set; }

        public List<ProductStatus> ProductStatues { get; set; }

        public List<int> MultiCategoryIds { get; set; }

    }
    public class StockItem
    {
        public string Barcode { get; set; }

        public int Quantity { get; set; }

        public string SellerStockCode { get; set; }

        /// <summary>
        /// Omni için gerekli ürünlerde uniqlik belilirliyor
        /// </summary>
        public string SkuCode { get; set; }

        public List<Image> Images { get; set; }

        public List<CategoryAttribute> Attributes { get; set; }

        public decimal ListPrice { get; set; }

        public decimal SalePrice { get; set; }

        public decimal BuyPrice { get; set; }

        public bool Active { get; set; }

        public bool IsSale { get; set; }

        public string Title { get; set; }
        public string SearchKey { get; set; }
        public string Description { get; set; }

        public List<ProductPriceItem> ProductPriceItems { get; set; }

    }
    public class Image
    {
        public string Url { get; set; }

        public int Order { get; set; }

        public Byte[] ImageFile { get; set; }

        public string Name { get; set; }
    }

    public class CategoryAttribute
    {
        #region Property
        public string AttributeCode { get; set; }

        public string AttributeName { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public string AttributeValueCode { get; set; }

        public string AttributeValueName { get; set; }
        #endregion

        #region Navigation Property
        public List<CategoryAttributeValue> CategoryAttributeValues { get; set; }
        #endregion
    }

    public class CategoryAttributeValue
    {
        #region Property
        public string Code { get; set; }

        public string Name { get; set; }
        #endregion
    }

    public class ProductPriceItem
    {

        public string ProviderId { get; set; }

        public string SellerStockCode { get; set; }

        public decimal ListPrice { get; set; }

        public decimal SalePrice { get; set; }
    }

    public class ProductStatus
    {
        public string ProviderId { get; set; }

        public bool Active { get; set; }
    }
  

}
