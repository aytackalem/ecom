﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.CicekSepeti.Common.Cargo;
using ECommerce.MarketPlace.CicekSepeti.Common.Order;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ECommerce.MarketPlace.CicekSepeti.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;

        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                //Siparişler ilk başta paketlenerek kargoya hazırlanıyor. Sonrasında statusid 11 olan Kargoya Verilecek siparişler çekiliyor.
                SetCargo(orderRequest);

                int page = 0;
                CicekSepetiOrderResponse orderResponse = null;
                do
                {
                    Console.WriteLine("Çicek Sepeti 30 Second Waiting");
                    Thread.Sleep(new TimeSpan(0, 0, 30));

                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 11 //1 : Yeni 2 : Hazırlanıyor 5 : Kargoya Verildi 11 : Kargoya Verilecek 7 : Teslim Edildi
                    };

                    orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"https://apis.ciceksepeti.com/api/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{orderRequest.Configurations["ApiKey"]}" }

                });

                    if (orderResponse != null && orderResponse.supplierOrderListWithBranch.Count == 0) return;

                    var _orders = orderResponse
                        .supplierOrderListWithBranch
                        .GroupBy(x => x.orderId)
                        .Select(x => x)
                        .ToList();

                    foreach (var odLoop in _orders)
                    {
                        var firstOrder = odLoop.FirstOrDefault();

                        if (!firstOrder.isOrderStatusActive)
                            continue;

                        string shipmentId = "";

                        switch (firstOrder.cargoCompany)
                        {
                            case "Aras Kargo":
                                shipmentId = ShipmentCompanies.ArasKargo;
                                break;
                            case "Yurtiçi Kargo":
                                shipmentId = ShipmentCompanies.YurtiçiKargo;
                                break;
                            case "PTT Kargo":
                                shipmentId = ShipmentCompanies.PttKargo;
                                break;
                            case "MNG Kargo":
                                shipmentId = ShipmentCompanies.MngKargo;
                                break;
                            case "Sürat Kargo":
                                shipmentId = ShipmentCompanies.SüratKargo;
                                break;

                        }


                        var customerName = firstOrder.senderName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var invoiceFirstName = "";
                        var invoiceLastName = "";
                        if (customerName.Length > 1)
                        {
                            invoiceFirstName = customerName[0];
                            invoiceLastName = customerName[1];
                        }


                        customerName = firstOrder.receiverName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var firstName = "";
                        var lastName = "";
                        if (customerName.Length > 1)
                        {
                            firstName = customerName[0];
                            lastName = customerName[1];
                        }

                        var order = new Order
                        {
                            OrderCode = odLoop.Key.ToString(),
                            Source = "CS",
                            CurrencyId = "TRY",
                            OrderTypeId = "OS",
                            TotalAmount = Convert.ToDecimal(odLoop.Sum(x => x.totalPrice)),
                            Shipper = "",
                            OrderSourceId = OrderSources.Pazaryeri,
                            MarketPlaceId = Marketplaces.CicekSepeti,
                            ShippingCost = 0,
                            Barcode = "",
                            SurchargeAmount = 0,
                            OrderNote = firstOrder.cardMessage,
                            Customer = new Customer
                            {
                                FirstName = firstName,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                LastName = lastName,
                                TaxNumber = "",
                                SourceReferenceId = "",
                                Mail = firstOrder.invoiceEmail,
                                TaxAdministration = "",
                                IdentityNumber = ""
                            },
                            OrderDetails = new List<OrderDetail>(),
                            Payments = new List<Payment>(),
                            OrderDeliveryAddress = new OrderDeliveryAddress
                            {
                                Code = "",
                                Email = firstOrder.invoiceEmail,
                                Neighborhood = CleanRegionName(firstOrder.receiverRegion),
                                FirstName = firstName,
                                LastName = lastName,
                                Phone = firstOrder.receiverPhone,
                                Country = "Türkiye",
                                City = CleanCityName(firstOrder.receiverCity),
                                District = firstOrder.receiverDistrict,
                                Address = firstOrder.receiverAddress
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress
                            {
                                Address = !string.IsNullOrEmpty(firstOrder.senderAddress) ? firstOrder.senderAddress : firstOrder.receiverAddress,
                                City = !string.IsNullOrEmpty(firstOrder.senderCity) ? CleanCityName(firstOrder.senderCity) : CleanCityName(firstOrder.receiverCity),
                                Country = "Türkiye",
                                District = !string.IsNullOrEmpty(firstOrder.senderDistrict) ? firstOrder.senderDistrict : firstOrder.receiverDistrict,
                                Email = firstOrder.invoiceEmail,
                                FirstName = invoiceFirstName,
                                LastName = invoiceLastName,
                                Neighborhood = !string.IsNullOrEmpty(firstOrder.senderRegion) ? CleanRegionName(firstOrder.senderRegion) : firstOrder.receiverRegion,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                TaxNumber = "",
                                TaxOffice = ""
                            },
                            OrderDate = DateTime.Now,
                            OrderShipment = new OrderShipment
                            {
                                ShipmentCompanyId = shipmentId,
                                ShipmentTypeId = "3",
                                TrackingCode = firstOrder.partialNumber,
                                TrackingUrl = firstOrder.shipmentTrackingUrl
                            }
                        };
                        foreach (var orderDetail in odLoop)
                        {


                            order.OrderDetails.Add(new OrderDetail
                            {
                                Quantity = orderDetail.quantity,
                                UnitPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                ListPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                TaxRate = orderDetail.tax / 100,
                                Product = new Product
                                {
                                    Name = orderDetail.name,
                                    StockCode = orderDetail.code,
                                    Barcode = orderDetail.productCode
                                }
                            });
                        }

                        order.Payments.Add(new Payment
                        {

                            Amount = Convert.ToDecimal(firstOrder.totalPrice),
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Date = DateTime.Now
                        });
                        response.Data.Orders.Add(order);

                    }

                } while (orderResponse?.pageCount < page + 1);

                response.Success = true;
            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "";
                });

        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var request = new CicekSepetiOrderRequest
                {

                    Page = 0,
                    PageSize = 100,
                    OrderNo = Convert.ToInt32(orderRequest.MarketPlaceOrderCode)
                };

                var orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"https://apis.ciceksepeti.com/api/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{orderRequest.Configurations["ApiKey"]}" }

                });

                if (orderResponse != null && orderResponse.supplierOrderListWithBranch.Count > 0 && !orderResponse.supplierOrderListWithBranch[0].isOrderStatusActive)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Çiceksepeti siparişiniz iptal statüsündedir. Siparişin devam edilemesi çiceksepeti panelinden kontrol edilmesi gerekiyor.";
                }


                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Success = false;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {

            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Success = false;


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Success = false;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Çiçeksepeti ile bağlantı kurulamıyor.";
            });
        }
        #endregion

        #region Helper Methods
        public void SetCargo(OrderRequest orderRequest)
        {
            try
            {
                int page = 0;


                CicekSepetiOrderResponse orderResponse = null;
                do
                {


                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 1 //1 : Yeni 2 : Hazırlanıyor 5 : Kargoya Verildi 11 : Kargoya Verilecek 7 : Teslim Edildi
                    };

                    orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"https://apis.ciceksepeti.com/api/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{orderRequest.Configurations["ApiKey"]}" }

                });

                    if (orderResponse != null && (orderResponse.supplierOrderListWithBranch == null || orderResponse.supplierOrderListWithBranch.Count == 0)) return;

                    var _orders = orderResponse.supplierOrderListWithBranch.GroupBy(x => x.orderId).Select(x => x).ToList();
                    foreach (var odLoop in _orders)
                    {
                        var firstOrder = odLoop.FirstOrDefault();

                        if (!firstOrder.isOrderStatusActive)
                            continue;


                        var cargoRequest = new CicekSepetiCargoRequest
                        {
                            orderItemsGroup = new List<OrderItemsGroup>
                                {
                                    new OrderItemsGroup
                                    {
                                        orderItemIds=odLoop.Select(x => x.orderItemId).ToList()
                                    }
                                }
                        };

                        //Sipariş kargoya hazır hale getiriliyor
                        var cargoResponse = _httpHelper.Put<CicekSepetiCargoRequest, CicekSepetiCargoResponse>(cargoRequest, $"https://apis.ciceksepeti.com/api/v1/Order/readyforcargowithcsintegration", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{orderRequest.Configurations["ApiKey"]}" }

                });
                    }

                } while (orderResponse?.pageCount < page + 1);
            }
            catch
            {

            }
        }

        public string CleanCityName(string cityName)
        {
            if (cityName == "ISTANBUL-ANADOLU" || cityName == "ISTANBUL-AVRUPA" || cityName == "ISTANBUL AVRUPA" || cityName == "ISTANBUL ANADOLU")
                return "İstanbul";

            return cityName;
        }

        public string CleanRegionName(string districtName)
        {
            if (districtName.EndsWith(" Mahallesi"))
                return districtName.Replace(" Mahallesi", "");

            return districtName;
        }

        public string CleanPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.StartsWith("0"))
                return phoneNumber.TrimStart(new char[] { '0' });

            return phoneNumber;
        }
        #endregion
    }
}
