﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.CicekSepeti.Common.Category;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.CicekSepeti.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper)
        {
            #region Fields
            _httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                response.Data = new List<CategoryResponse>();

                var categoryUrl = "https://apis.ciceksepeti.com/api/v1/Categories";
                var customHeaders = new Dictionary<string, string> { { "x-api-key", $"{configurations["ApiKey"]}" } };
                var cicekSepetiCategory = this._httpHelper.Get<CicekSepetiCategory>(categoryUrl, null, null, customHeaders);

                CategoryRecursive(cicekSepetiCategory.Categories
                    .First(csc => csc.Id == 255)
                    .SubCategories
                    .Where(sc => sc.Id == 12462 || sc.Id == 12463)
                    .ToList(), response.Data);


                response.Data.ForEach(d =>
                {
                    var categoryAttributeUrl = $"https://apis.ciceksepeti.com/api/v1/categories/{d.Code}/attributes";
                    var cicekSepetiCategory = this._httpHelper.Get<Common.CategoryAttribute.CicekSepetiCategory>(categoryAttributeUrl, null, null, customHeaders);
                    d.CategoryAttributes = cicekSepetiCategory
                        .CategoryAttributes.Where(x => x.Varianter == false && x.Type == "Ürün Özelliği")
                        .Select(ca => new CategoryAttributeResponse
                        {
                            Code = ca.AttributeId.ToString(),
                            Name = ca.AttributeName,
                            Mandatory = ca.Required,
                            AllowCustom = false,
                            MultiValue = false,
                            CategoryCode = d.Code,
                            CategoryAttributesValues = ca
                                .AttributeValues
                                .Select(cav => new CategoryAttributeValueResponse
                                {
                                    VarinatCode = ca.AttributeId.ToString(),
                                    Code = cav.Id.ToString(),
                                    Name = cav.Name
                                })
                                .ToList()
                        })
                        .ToList();
                });

                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void CategoryRecursive(List<CicekSepetiSubCategory> cicekSepetiCategories, List<CategoryResponse> categoryResponses)
        {
            foreach (var cscLoop in cicekSepetiCategories)
            {
                if (cscLoop.SubCategories.Count == 0)
                    categoryResponses.Add(new CategoryResponse
                    {
                        Code = cscLoop.Id.ToString(),
                        Name = cscLoop.Name,
                        ParentCategoryCode = cscLoop.ParentCategoryId.ToString(),
                    });

                CategoryRecursive(cscLoop.SubCategories, categoryResponses);
            }
        }
        #endregion
    }
}
