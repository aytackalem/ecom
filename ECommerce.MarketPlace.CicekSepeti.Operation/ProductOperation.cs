﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.CicekSepeti.Common.Product;
using ECommerce.MarketPlace.CicekSepeti.Common.ProductFilter;
using ECommerce.MarketPlace.CicekSepeti.Common.Stock;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ECommerce.MarketPlace.CicekSepeti.Operation
{
    public class ProductOperation : IProductFacade
    {

        #region Fields
        public readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;

        }
        #endregion


        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var createRequest = new CicekSepetiProductCreateRequest();
                createRequest.products = new List<CicekSepetiProductCreate>();


                var updateRequest = new CicekSepetiProductUpdateRequest();
                updateRequest.products = new List<CicekSepetiProductUpdate>();
                foreach (var stLoop in productRequest.ProductItem.StockItems)
                {
                    CultureInfo culture = new CultureInfo("en-US");

                    var filterResponse = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Stockcode = stLoop.StockCode
                    });

                    if (filterResponse.Success)
                    {
                        if (filterResponse.Data.totalCount == 1) //Ürün güncelleme
                        {
                            ///NOT
                            ///Ürünlerin attributes bilgisi güncellemeleri api üzerinden yapılamamaktadır. Güncellemeler için destek ekibine talepte bulunmanız gerekmektedir. Güncellenmek istenen kayıt bir varyant özellik ise, güncelleme yerine yeni varyant oluşturabilirsiniz. Ancak ilgili kayıt dinamik bir özellikse güncelleme yapabilirsiniz. Örneğin bileklik üzerinde yazılacak yazı karakteri için textLength 25 den 23'e düşürmek için güncelleme metodunu kullanabilirsiniz. Kategori Özellik Listesi metodunu inceleyebilirsiniz.
                            if (stLoop.Description.Length < 10) stLoop.Description += $" {stLoop.Title} {stLoop.Title}";

                            updateRequest.products.Add(new CicekSepetiProductUpdate
                            {
                                productName = stLoop.Title,
                                barcode = stLoop.Barcode,
                                description = stLoop.Description,
                                images = stLoop.Images.Select(x => x.Url).ToList(),
                                mediaLink = null,
                                deliveryMessageType = 5,
                                deliveryType = 2,
                                stockCode = stLoop.StockCode,
                                isActive = stLoop.Active,
                                mainProductCode = productRequest.ProductItem.SellerCode,
                                attributes = null,
                            });


                        }
                        else if (stLoop.Active) //Yeni Ürün Yükeleme Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                        {
                            createRequest.products.Add(new CicekSepetiProductCreate
                            {
                                productName = stLoop.Title,
                                barcode = stLoop.Barcode,
                                categoryId = Convert.ToInt32(productRequest.ProductItem.CategoryId),
                                description = stLoop.Description,
                                images = stLoop.Images.Select(x => x.Url).ToList(),
                                mediaLink = null,
                                deliveryMessageType = 5,
                                deliveryType = 2,
                                stockQuantity = stLoop.Quantity,
                                salesPrice = Convert.ToDouble(stLoop.SalePrice, culture),
                                listPrice = Convert.ToDouble(stLoop.ListPrice, culture),
                                stockCode = stLoop.StockCode,
                                mainProductCode = productRequest.ProductItem.SellerCode,
                                attributes = stLoop.Attributes.Select(x => new CicekSepetiAttribute
                                {
                                    id = Convert.ToInt32(x.AttributeCode),
                                    valueId = Convert.ToInt32(x.AttributeValueCode),
                                    textLength = 0
                                }).ToList()

                            });

                        }
                    }

                }

                if (createRequest.products.Count > 0)
                {
                    var productCreateResponse = _httpHelper.Post<CicekSepetiProductCreateRequest, CicekSepetiProductResponse>(createRequest, $"https://apis.ciceksepeti.com/api/v1/Products", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{productRequest.Configurations["ApiKey"]}" }
                    });
                }

                if (updateRequest.products.Count > 0)
                {

                    var productUpdateResponse = _httpHelper.Put<CicekSepetiProductUpdateRequest, CicekSepetiProductResponse>(updateRequest, $"https://apis.ciceksepeti.com/api/v1/Products", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{productRequest.Configurations["ApiKey"]}" }
                    });
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                CultureInfo culture = new CultureInfo("en-US");

                var request = new CicekSepetiStockRequest();
                request.items = new List<CicekSepetiStockItem>();
                request.items.Add(new CicekSepetiStockItem
                {
                    listPrice = stockRequest.PriceStock.SalePrice.HasValue ? Convert.ToDouble(stockRequest.PriceStock.ListPrice, culture) : null,
                    salesPrice = stockRequest.PriceStock.SalePrice.HasValue ? Convert.ToDouble(stockRequest.PriceStock.SalePrice, culture) : null,
                    stockCode = stockRequest.PriceStock.StockCode,
                    StockQuantity = null
                });

                //Stok ve fiyat güncelleme işlemi için istek gönderildiğinde sıraya alınır ve batch kontrolünde statü Pending olarak döner. Güncelleme işlemi maksimum 4 saat içinde tamamlanır.
                var updatePriceAndStockResponse = _httpHelper.Put<CicekSepetiStockRequest, CicekSepetiStockResponse>(request, $"https://apis.ciceksepeti.com/api/v1/Products/price-and-stock", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{stockRequest.Configurations["ApiKey"]}" }
                    });


                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                CultureInfo culture = new CultureInfo("en-US");

                var request = new CicekSepetiStockRequest();
                request.items = new List<CicekSepetiStockItem>();
                request.items.Add(new CicekSepetiStockItem
                {
                    listPrice = null,
                    salesPrice = null,
                    stockCode = stockRequest.PriceStock.StockCode,
                    StockQuantity = stockRequest.PriceStock.Quantity
                });

                //Stok ve fiyat güncelleme işlemi için istek gönderildiğinde sıraya alınır ve batch kontrolünde statü Pending olarak döner. Güncelleme işlemi maksimum 4 saat içinde tamamlanır.
                var updatePriceAndStockResponse = _httpHelper.Put<CicekSepetiStockRequest, CicekSepetiStockResponse>(request, $"https://apis.ciceksepeti.com/api/v1/Products/price-and-stock", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{stockRequest.Configurations["ApiKey"]}" }
                    });

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        private DataResponse<CicekSepetiProductFilterResponse> Filter(ProductFilterRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CicekSepetiProductFilterResponse>>((response) =>
            {

                response.Data = _httpHelper.Get<CicekSepetiProductFilterResponse>($"https://apis.ciceksepeti.com/api/v1/Products?stockCode={productRequest.Stockcode}", null, null, new Dictionary<string, string> {
                    { "x-api-key", $"{productRequest.Configurations["ApiKey"]}" }});

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
    }
}
