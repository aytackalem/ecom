using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ShipmentCompanyCompanyController : ControllerBase
    {
        #region Fields
        private readonly IShipmentCompanyCompanyService _shipmentCompanyCompanyService;
        #endregion

        #region Constructors
        public ShipmentCompanyCompanyController(IShipmentCompanyCompanyService shipmentCompanyCompanyService)
        {
            #region Fields
            this._shipmentCompanyCompanyService = shipmentCompanyCompanyService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._shipmentCompanyCompanyService.GetAsync());
        }
        #endregion
    }
}