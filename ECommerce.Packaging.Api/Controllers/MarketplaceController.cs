using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class MarketplaceController : ControllerBase
    {
        #region Fields
        private readonly IMarketplaceService _marketplaceService;
        #endregion

        #region Constructors
        public MarketplaceController(IMarketplaceService marketplaceService)
        {
            #region Fields
            this._marketplaceService = marketplaceService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._marketplaceService.GetAsync());
        }
        #endregion
    }
}