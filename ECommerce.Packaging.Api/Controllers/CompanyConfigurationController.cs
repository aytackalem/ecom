using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class CompanyConfigurationController : ControllerBase
    {
        #region Fields
        private readonly ICompanyConfigurationService _companyConfigurationService;
        #endregion

        #region Constructors
        public CompanyConfigurationController(ICompanyConfigurationService companyConfigurationService)
        {
            #region Fields
            this._companyConfigurationService = companyConfigurationService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._companyConfigurationService.GetAsync());
        }
        #endregion
    }
}