using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        #region Fields
        private readonly ICompanyService _companyService;
        #endregion

        #region Constructors
        public CompanyController(ICompanyService companyService)
        {
            #region Fields
            this._companyService = companyService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return Ok(await this._companyService.GetAsync());
        }
        #endregion
    }
}