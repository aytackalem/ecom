using ECommerce.Packaging.Core.Application.Interfaces.Services;
using ECommerce.Packaging.Core.Application.Parameters.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        #region Fields
        private readonly IOrderService _orderService;
        #endregion

        #region Constructors
        public OrderController(IOrderService orderService)
        {
            #region Fields
            this._orderService = orderService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetParameter getParameter)
        {
            return Ok(await this._orderService.GetAsync(getParameter));
        }

        [HttpGet("Reserve")]
        public async Task<IActionResult> Reserve([FromQuery] ReserveParameter parameter)
        {
            return Ok(await this._orderService.ReserveAsync(parameter));
        }

        [HttpGet("Packed")]
        public async Task<IActionResult> Packed(int id)
        {
            return Ok(await this._orderService.PackedAsync(id));
        }

        [HttpPut("Cancel")]
        public async Task<IActionResult> Cancel(int id)
        {
            return Ok(await this._orderService.CancelAsync(id));
        }

        [HttpPut("Complete")]
        public async Task<IActionResult> Complete(int id)
        {
            return Ok(await this._orderService.CompleteAsync(id));
        }

        [HttpPut("NotAvailable")]
        public async Task<IActionResult> NotAvailable(int id)
        {
            return Ok(await this._orderService.NotAvailableAsync(id));
        }

        [HttpGet("Summary")]
        public async Task<IActionResult> Summary()
        {
            return Ok(await this._orderService.SummaryAsync());
        }

        [HttpGet("TrendyolEuropePackages")]
        public async Task<IActionResult> TrendyolEuropePackages()
        {
            return Ok(await this._orderService.GetTrendyolEuropePackagesAsync());
        }

        [HttpGet("TrendyolEuropeOrderNumbers")]
        public async Task<IActionResult> CountTrendyolEurope(string packageNumber)
        {
            return Ok(await this._orderService.GetTrendyolEuropeOrderNumberAsync(packageNumber));
        }
        #endregion
    }
}