using ECommerce.Packaging.Core.Application.Interfaces.Services;
using ECommerce.Packaging.Core.Application.Parameters.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DomainController : ControllerBase
    {
        #region Fields
        private readonly IDomainService _domainService;
        #endregion

        #region Constructors
        public DomainController(IDomainService domainService)
        {
            #region Fields
            this._domainService = domainService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._domainService.GetAsync());
        }
        #endregion
    }
}