using ECommerce.Packaging.Core.Application.Interfaces.Services;
using ECommerce.Packaging.Core.Application.Parameters.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderSourceController : ControllerBase
    {
        #region Fields
        private readonly IOrderSourceService _orderSourceService;
        #endregion

        #region Constructors
        public OrderSourceController(IOrderSourceService orderSourceService)
        {
            #region Fields
            this._orderSourceService = orderSourceService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._orderSourceService.GetAsync());
        }
        #endregion
    }
}