using ECommerce.Application.Common.Interfaces.Accounting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AccountingController : ControllerBase
    {
        #region Fields
        private readonly IAccountingProviderService _accountingProviderService;
        #endregion

        #region Constructors
        public AccountingController(IAccountingProviderService accountingProviderService)
        {
            #region Fields
            this._accountingProviderService = accountingProviderService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost("CreateInvoiceList")]
        public IActionResult CreateInvoiceList([FromBody]List<int> orderIds)
        {
            return Ok(this._accountingProviderService.CreateInvoice(orderIds));
        }

        [HttpPost("CreateInvoice")]
        public IActionResult CreateInvoice(int orderId)
        {
            return Ok(this._accountingProviderService.CreateInvoice(orderId));
        }

        [HttpPost("CreateStockTransfer")]
        public IActionResult CreateStockTransfer(int orderId)
        {
            return Ok(this._accountingProviderService.CreateStockTransfer(orderId));
        }

        [HttpDelete("CancelInvoice")]
        public IActionResult CancelInvoice(int orderId)
        {
            return Ok(this._accountingProviderService.CancelInvoice(orderId));
        }

        [HttpDelete("DeleteStockTransfer")]
        public IActionResult DeleteStockTransfer(int orderId)
        {
            return Ok(this._accountingProviderService.DeleteStockTransfer(orderId));
        }
        #endregion
    }
}