using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderInvoiceInformationController : ControllerBase
    {
        #region Fields
        private readonly IOrderInvoiceInformationService _orderInvoiceInformationService;
        #endregion

        #region Constructors
        public OrderInvoiceInformationController(IOrderInvoiceInformationService orderInvoiceInformationService)
        {
            #region Fields
            this._orderInvoiceInformationService = orderInvoiceInformationService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderInvoiceInformation orderInvoiceInformation)
        {
            return Ok(await this._orderInvoiceInformationService.UpdateAsync(orderInvoiceInformation));
        }
        #endregion
    }
}