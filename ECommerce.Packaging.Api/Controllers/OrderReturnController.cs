using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderReturnController : ControllerBase
    {
        #region Fields
        private readonly IOrderReturnService _orderReturnService;
        #endregion

        #region Constructors
        public OrderReturnController(IOrderReturnService orderReturnService)
        {
            #region Fields
            this._orderReturnService = orderReturnService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPut]
        public async Task<IActionResult> Get([FromBody] OrderReturn orderReturn)
        {
            return Ok(await this._orderReturnService.UpdateAsync(orderReturn));
        }
        #endregion
    }
}