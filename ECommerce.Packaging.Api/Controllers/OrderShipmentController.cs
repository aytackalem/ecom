using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class OrderShipmentController : ControllerBase
    {
        #region Fields
        private readonly IOrderShipmentService _orderShipmentService;
        #endregion

        #region Constructors
        public OrderShipmentController(IOrderShipmentService orderShipmentService)
        {
            #region Fields
            this._orderShipmentService = orderShipmentService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Post(OrderShipment orderShipment)
        {
            return Ok(await this._orderShipmentService.CreateAsync(orderShipment));
        }

        [HttpPut]
        public async Task<IActionResult> Put(OrderShipment orderShipment)
        {
            return Ok(await this._orderShipmentService.UpdateAsync(orderShipment));
        }
        #endregion
    }
}