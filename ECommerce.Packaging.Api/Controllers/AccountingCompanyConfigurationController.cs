using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Packaging.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AccountingCompanyConfigurationController : ControllerBase
    {
        #region Fields
        private readonly IAccountingCompanyConfigurationService _accountingCompanyConfigurationService;
        #endregion

        #region Constructors
        public AccountingCompanyConfigurationController(IAccountingCompanyConfigurationService accountingCompanyConfigurationService)
        {
            #region Fields
            this._accountingCompanyConfigurationService = accountingCompanyConfigurationService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Get(string accountingId)
        {
            return Ok(await this._accountingCompanyConfigurationService.GetAsync(accountingId));
        }
        #endregion
    }
}