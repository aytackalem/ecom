﻿namespace ECommerce.Packaging.Core.Application.Parameters.Order
{
    public class ReserveParameter
    {
        #region Properties
        public string? MarketplaceId { get; set; }

        public string? ShipmentCompanyId { get; set; }

        public int? OrderSourceId { get; set; }

        public int? OrderId { get; set; }
        #endregion
    }
}
