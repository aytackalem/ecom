﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IAccountingCompanyConfigurationService
    {
        #region Methods
        Task<DataResponse<List<AccountingCompanyConfiguration>>> GetAsync(string accountingId); 
        #endregion
    }
}
