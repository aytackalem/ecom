﻿using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IDomainService
    {
        #region Methods
        Task<DataResponse<List<DataTransferObjects.Domain>>> GetAsync(); 
        #endregion
    }
}
