﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IOrderSourceService
    {
        #region Methods
        Task<DataResponse<List<OrderSource>>> GetAsync(); 
        #endregion
    }
}
