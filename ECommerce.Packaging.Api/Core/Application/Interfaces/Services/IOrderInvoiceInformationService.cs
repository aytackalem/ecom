﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IOrderInvoiceInformationService
    {
        #region Methods
        /// <summary>
        /// Faturaya ilişkin bilgileri güncelleyen fonksiyon.
        /// </summary>
        /// <param name="orderInvoiceInformation">İlgili fatura bilgileri.</param>
        /// <returns>İşlem sonucu.</returns>
        Task<NoContentResponse> UpdateAsync(OrderInvoiceInformation orderInvoiceInformation);
        #endregion
    }
}
