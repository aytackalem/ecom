﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IOrderReturnService
    {
        #region Methods
        /// <summary>
        /// Fatura iadelerini güncellenyen method
        /// </summary>
        /// <param name="orderReturn"></param>
        /// <returns></returns>
        Task<NoContentResponse> UpdateAsync(OrderReturn orderReturn);
        #endregion
    }
}
