﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IMarketplaceService
    {
        #region Methods
        Task<DataResponse<Marketplace>> GetAsync(string id);

        Task<DataResponse<List<Marketplace>>> GetAsync();
        #endregion
    }
}
