﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IOrderShipmentService
    {
        #region Methods
        Task<NoContentResponse> CreateAsync (OrderShipment orderShipment);

        Task<NoContentResponse> UpdateAsync(OrderShipment orderShipment); 
        #endregion
    }
}
