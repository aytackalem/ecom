﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface ICompanyService
    {
        #region Methods
        Task<DataResponse<List<Company>>> GetAsync(); 
        #endregion
    }
}
