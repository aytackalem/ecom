﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Parameters.Order;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface IOrderService
    {
        #region Methods
        /// <summary>
        /// OrderId veya PackarBarcode gibi degerleri kullanarak siparis filtrelemeyi saglayan metod.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task<DataResponse<Order>> GetAsync(GetParameter parameter);

        /// <summary>
        /// Paketlenmeyi bekleyen OS tipindeki siparişlerden ilk sırada olanı getiren metod.
        /// </summary>
        /// <param name="domainId"></param>
        /// <param name="marketplaceId"></param>
        /// <param name="shipmentCompanyId"></param>
        /// <param name="orderSourceId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<DataResponse<int>> ReserveAsync(ReserveParameter parameter);

        /// <summary>
        /// Siparis adetlerini donduren metod.
        /// </summary>
        /// <returns></returns>
        Task<DataResponse<OrderSummary>> SummaryAsync();

        /// <summary>
        /// İlgili siparişin paketlenip/paketlenmediği bilgisini veren metod.
        /// </summary>
        /// <param name="id">Sipariş id bilgisi.</param>
        /// <returns>Sipariş paketlenmiş ise "True" paketlenmemiş ise "False" değeri döner.</returns>
        Task<DataResponse<bool>> PackedAsync(int id);

        /// <summary>
        /// Paketleniyor statusundeki siparisi onaylanmis siparis statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<NoContentResponse> CancelAsync(int id);

        /// <summary>
        /// Ilgili siparisi kargoya teslim edildi statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<NoContentResponse> CompleteAsync(int id);

        /// <summary>
        /// Ilgili siparisi temin edilemiyor statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<NoContentResponse> NotAvailableAsync(int id);

        /// <summary>
        /// Trendyol Europe paketlerini donen metod.
        /// </summary>
        /// <returns></returns>
        Task<DataResponse<List<string>>> GetTrendyolEuropePackagesAsync();

        /// <summary>
        /// Paket numarasına göre sipariş numaralarini dönen metod.
        /// </summary>
        /// <param name="packageNumber">Trendyol paket numarası.</param>
        /// <returns></returns>
        Task<DataResponse<List<int>>> GetTrendyolEuropeOrderNumberAsync(string packageNumber);
        #endregion
    }
}
