﻿using ECommerce.Packaging.Core.Application.DataTransferObjects;
using Helpy.Shared.Response;

namespace ECommerce.Packaging.Core.Application.Interfaces.Services
{
    public interface ICompanyConfigurationService
    {
        #region Methods
        Task<DataResponse<List<KeyValue<string, string>>>> GetAsync();
        #endregion
    }
}
