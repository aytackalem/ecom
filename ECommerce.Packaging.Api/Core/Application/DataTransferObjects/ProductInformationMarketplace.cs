﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class ProductInformationMarketplace
    {
        public string MarketPlaceId { get; set; }

        public string StockCode { get; set; }
    }
}
