﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class City
    {
        public string Name { get; set; }

        public Country Country { get; set; }
    }
}
