﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class OrderSource
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
