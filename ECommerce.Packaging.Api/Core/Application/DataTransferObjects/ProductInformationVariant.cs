﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class ProductInformationVariant
    {
        #region Properties
        public int Id { get; set; }

        public int VariantValueId { get; set; }

        #endregion

        #region Navigation Properties
        public VariantValue VariantValue { get; set; } 
        #endregion
    }
}
