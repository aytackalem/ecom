﻿using System;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class OrderReturnDetail
    {
        #region Property
        
        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Geri gönderilen ürün iade alınabilir mi?
        /// </summary>
        public bool IsReturn { get; set; }

        /// <summary>
        /// Geri gönderilen ürün zayi mi?
        /// </summary>
        public bool IsLoss { get; set; }
        #endregion
    }
}
