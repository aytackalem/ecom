﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class ArasCompanyConfiguration
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
