﻿using System.Collections.Generic;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Variant
    {
        #region Properties
        public string Id { get; set; }

        public bool Photoable { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantTranslation> VariantTranslations { get; set; }

        public List<VariantValue> VariantValues { get; set; }
        #endregion
    }
}
