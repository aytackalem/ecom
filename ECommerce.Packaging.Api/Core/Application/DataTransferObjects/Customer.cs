﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }
    }
}
