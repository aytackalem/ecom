﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Country
    {
        public string Name { get; set; }
    }
}
