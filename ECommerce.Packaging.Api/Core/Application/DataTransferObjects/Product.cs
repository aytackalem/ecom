﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Product
    {
        public ProductInformation ProductInformation { get; set; }

        public string SellerCode { get; set; }
    }
}
