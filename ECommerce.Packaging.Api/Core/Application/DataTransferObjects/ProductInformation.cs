﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class ProductInformation
    {
        #region Fields
        private string _fullName = string.Empty;
        #endregion

        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public string ProductInformationUUId { get; set; }

        public string FullName { get; set; }

        //public string FullName
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_fullName))
        //        {
        //            this._fullName = string.Empty;

        //            if (this.ProductInformationVariants?.Count > 0)
        //                this._fullName = $"({string.Join(", ", this.ProductInformationVariants.GroupBy(x => x.VariantValueId).Select(pv => pv.First().VariantValue.Name))})";

        //            this._fullName += $" {this.Name}";
        //        }

        //        return this._fullName;
        //    }
        //}
        #endregion

        #region Navigation Properties
        public ProductInformationPhoto ProductInformationPhoto { get; set; }

        public List<ProductInformationVariant> ProductInformationVariants { get; set; }
        #endregion
    }
}
