﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        public string FileName { get; set; }
    }
}
