﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class KeyValue<TKey, TValue>
    {
        #region Properties
        public TKey Key { get; set; }

        public TValue Value { get; set; }
        #endregion
    }
}
