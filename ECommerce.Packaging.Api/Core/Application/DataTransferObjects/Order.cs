﻿using Microsoft.Graph;
using System.Collections.Generic;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Order
    {
        public int Id { get; set; }

        public string EncryptId { get; set; }

        public int OrderSourceId { get; set; }

        public int CompanyId { get; set; }

        public string CurrencyId { get; set; }

        public decimal Amount { get; set; }

        public string MarketPlaceId { get; set; }

        public decimal CargoFee { get; set; }

        public decimal SurchargeFee { get; set; }

        public bool BulkInvoicing { get; set; }

        public string OrderTypeId { get; set; }

        public DateTime OrderDate { get; set; }

        public OrderBilling OrderBilling { get; set; }

        public List<OrderShipment> OrderShipments { get; set; }

        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        public OrderInvoiceInformation OrderInvoiceInformation { get; set; }

        public Marketplace MarketPlace { get; set; }

        public Customer Customer { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        public string MarketPlaceOrderNumber { get; set; }

        public List<OrderNote> OrderNotes { get; set; }

        public List<Payment> Payments { get; set; }

        public Company Company { get; set; }

        public OrderSource OrderSource { get; set; }

        public List<OrderReturnDetail> OrderReturnDetails { get; set; }
        public bool Micro { get; internal set; }
    }
}
