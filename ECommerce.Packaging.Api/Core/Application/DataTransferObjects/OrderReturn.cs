﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class OrderReturn
    {
        #region Properties
        public int Id { get; set; }

        public string? ReturnNumber { get; set; }

        public string? ReturnDescription { get; set; }
        
        public List<OrderReturnDetail> OrderReturnDetails { get; set; }
        #endregion
    }
}
