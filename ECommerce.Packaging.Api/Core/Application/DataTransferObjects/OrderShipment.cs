﻿using System.Collections.Generic;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class OrderShipment
    {
        public int Id { get; set; }

        public string ShipmentCompanyId { get; set; }

        public string TrackingCode { get; set; }

        public string TrackingUrl { get; set; }

        public string TrackingBase64 { get; set; }

        public ShipmentCompany ShipmentCompany { get; set; }

        public List<OrderShipmentDetail> OrderShipmentDetails { get; set; }

        public bool Payor { get; set; }
    }
}
