﻿using System.Collections.Generic;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class VariantValue
    {
        #region Properties
        public int Id { get; set; }

        public string VariantId { get; set; }

        public string Name { get; set; }

        public string VariantName { get; set; }
        #endregion


    }
}
