﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class AccountingCompanyConfiguration
    {
        #region Properties
        public string Key { get; set; }

        public string Value { get; set; } 
        #endregion
    }
}
