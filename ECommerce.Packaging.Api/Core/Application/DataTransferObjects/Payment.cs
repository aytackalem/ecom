﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Payment
    {
        #region Property
        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ödeme tipine ait id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// ilgili ödeme tipine ait adı
        /// </summary>
        public string PaymentTypeName { get; set; }

        /// <summary>
        /// Ödeme tutarı.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Ödeme bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

       
        #endregion

        #region Navigation Property
    

      
        #endregion
    }
}
