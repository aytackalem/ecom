﻿namespace ECommerce.Packaging.Core.Application.DataTransferObjects
{
    public class Manager
    {
        #region Properties
        public int ManagerUserId { get; set; }

        public int TenantId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }
        #endregion
    }
}
