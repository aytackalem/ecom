﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class CompanyService : ICompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CompanyService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Company>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Company>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(c => c.CompanyContact)
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new Company
                    {
                        Id = e.Id,
                        Name = e.Name,
                        FullName = e.FullName,
                        CompanyContact = new CompanyContact
                        {
                            TaxNumber = e.CompanyContact.TaxNumber,
                            PhoneNumber = e.CompanyContact.PhoneNumber,
                            TaxOffice = e.CompanyContact.TaxOffice,
                            Address = e.CompanyContact.Address,
                            Email = e.CompanyContact.Email,
                            WebUrl = e.CompanyContact.WebUrl
                        }
                    })
                    .ToList();
                response.Success = true;
                response.Message = $"Şirket kayıtları başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
