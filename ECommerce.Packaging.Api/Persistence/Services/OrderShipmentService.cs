﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class OrderShipmentService : IOrderShipmentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderShipmentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> CreateAsync(OrderShipment orderShipment)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                response.Success = await this 
                    ._unitOfWork
                    .OrderShipmentRepository
                    .CreateAsync(new Domain.Entities.OrderShipment
                    {
                        Payor = orderShipment.Payor,
                        ShipmentCompanyId = orderShipment.ShipmentCompanyId,
                        TrackingCode = orderShipment.TrackingCode,
                        TrackingUrl = orderShipment.TrackingUrl,
                        TrackingBase64 = orderShipment.TrackingBase64,
                        OrderShipmentDetail = orderShipment.OrderShipmentDetails.Select(osd => new Domain.Entities.OrderShipmentDetail
                        {
                            TrackingCode = osd.TrackingCode,
                            Weight = osd.Weight
                        }).ToList()
                    });
                response.Message = $"Sipariş taşıma bilgisi başarılı bir şekilde {(response.Success ? "kaydedildi" : "kaydedilemedi")}.";
            });
        }

        public async Task<NoContentResponse> UpdateAsync(OrderShipment orderShipment)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .OrderShipmentRepository
                    .DbSet()
                    .FirstOrDefaultAsync(o => o.Id == orderShipment.Id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş taşıma bilgisi bulunamadı.";

                    return;
                }

                entity.TrackingCode = orderShipment.TrackingCode;
                entity.TrackingUrl = orderShipment.TrackingUrl;
                entity.TrackingBase64 = orderShipment.TrackingBase64;
                entity.OrderShipmentDetail = orderShipment
                    .OrderShipmentDetails
                    .Select(x => new Domain.Entities.OrderShipmentDetail
                    {
                        TrackingCode = x.TrackingCode,
                        Weight = x.Weight,
                        CreatedDate = DateTime.Now
                    })
                    .ToList();

                response.Success = await this
                    ._unitOfWork
                    .OrderShipmentRepository
                    .UpdateAsync(entity);
                response.Message = $"Sipariş taşıma bilgisi başarılı bir şekilde {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
