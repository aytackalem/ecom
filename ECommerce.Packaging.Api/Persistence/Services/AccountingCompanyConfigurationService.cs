﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class AccountingCompanyConfigurationService : IAccountingCompanyConfigurationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountingCompanyConfigurationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<DataResponse<List<AccountingCompanyConfiguration>>> GetAsync(string accountingId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<AccountingCompanyConfiguration>>>(async result =>
            {
                var entities = await this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == accountingId)
                    .ToListAsync();

                if (entities?.Count == 0)
                {
                    result.Message = "Muhasebe firması ayarları başarılı bir şekilde okunamadı.";
                    return;
                }

                result.Data = entities
                    .Select(e => new AccountingCompanyConfiguration
                    {
                        Key = e.Key,
                        Value = e.Value
                    })
                    .ToList();
                result.Success = true;
                result.Message = "Muhasebe firması ayarları başarılı bir şekilde getirildi.";
            });
        }
    }
}
