﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class CompanyConfigurationService : ICompanyConfigurationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CompanyConfigurationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<DataResponse<List<KeyValue<string, string>>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<KeyValue<string, string>>>>(async response =>
            {
                var configrations = await this
                        ._unitOfWork
                        .CompanyConfigurationRepository
                        .DbSet()
                        .OrderByDescending(x => x.Id)
                        .Select(c => new KeyValue<string, string>
                        {
                            Key = c.Key,
                            Value = c.Value
                        })
                        .ToListAsync();

                response.Data = configrations;
                response.Success = true;
                response.Message = "Konfigurasyon başarılı bir şekilde getirildi.";
            });
        }
    }
}
