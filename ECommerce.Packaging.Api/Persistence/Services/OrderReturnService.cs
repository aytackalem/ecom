﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class OrderReturnService : IOrderReturnService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderReturnService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> UpdateAsync(OrderReturn orderReturn)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(x => x.OrderReturnDetails)
                    .Include(x => x.OrderBilling)
                    .FirstOrDefaultAsync(o => o.Id == orderReturn.Id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş fatura bilgisi bulunamadı.";
                    return;
                }

                entity.OrderReturnDetails = orderReturn.OrderReturnDetails.Select(od => new Domain.Entities.OrderReturnDetail
                {
                    IsReturn = true,
                    Quantity = od.Quantity,
                    ProductInformationId = od.ProductInformationId,
                    CreatedDate = DateTime.Now,
                }).ToList();

                entity.OrderTypeId = OrderTypes.Iade;
                entity.OrderBilling.ReturnNumber = orderReturn.ReturnNumber;
                entity.OrderBilling.ReturnDescription = orderReturn.ReturnDescription;

                response.Success = this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Message = $"Sipariş iade fatura bilgisi başarılı bir şekilde {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
