﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class MarketplaceService : IMarketplaceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Marketplace>> GetAsync(string id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Marketplace>>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(mp => mp.MarketplaceConfigurations)
                    .Include(x => x.Marketplace)
                    .FirstOrDefaultAsync(mp => mp.Marketplace.Id == id);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Pazaryeri kaydı bulunamadı.";

                    return;
                }

                response.Data = new Marketplace
                {
                    Id = entity.MarketplaceId,
                    Name = entity.Marketplace.Name,
                    MarketPlaceConfigurations = entity
                        .MarketplaceConfigurations
                        .Select(mpc => new MarketPlaceConfiguration
                        {
                            Key = mpc.Key,
                            Value = mpc.Value
                        })
                        .ToList()
                };
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }

        public async Task<DataResponse<List<Marketplace>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Marketplace>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .MarketPlaceRepository
                    .DbSet()
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new Marketplace { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
