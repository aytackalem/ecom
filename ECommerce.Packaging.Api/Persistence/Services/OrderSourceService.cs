﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class OrderSourceService : IOrderSourceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderSourceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<OrderSource>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<OrderSource>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .OrderSourceRepository
                    .DbSet()
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new OrderSource { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
