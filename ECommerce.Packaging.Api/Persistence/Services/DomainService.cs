﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class DomainService : IDomainService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public DomainService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<Core.Application.DataTransferObjects.Domain>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Core.Application.DataTransferObjects.Domain>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new Core.Application.DataTransferObjects.Domain { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
