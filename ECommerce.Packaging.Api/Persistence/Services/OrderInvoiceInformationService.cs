﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class OrderInvoiceInformationService : IOrderInvoiceInformationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderInvoiceInformationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> UpdateAsync(OrderInvoiceInformation orderInvoiceInformation)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(x => x.OrderInvoiceInformation)
                    .Include(x => x.OrderBilling)
                    .FirstOrDefaultAsync(o => o.Id == orderInvoiceInformation.Id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş fatura bilgisi bulunamadı.";
                    return;
                }

                entity.OrderInvoiceInformation.AccountingCompanyId = orderInvoiceInformation.AccountingCompanyId;
                entity.OrderInvoiceInformation.Guid = orderInvoiceInformation.Guid;
                entity.OrderInvoiceInformation.Url = orderInvoiceInformation.Url;
                entity.OrderInvoiceInformation.Number = orderInvoiceInformation.Number;

                entity.OrderBilling = new Domain.Entities.OrderBilling
                {
                    Id = orderInvoiceInformation.Id,
                    CreatedDate = DateTime.Now,
                    InvoiceNumber = orderInvoiceInformation.Number
                };

                response.Success = this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Message = $"Sipariş fatura bilgisi başarılı bir şekilde {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
