﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using ECommerce.Packaging.Core.Application.Parameters.Order;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace ECommerce.Packaging.Persistence.Services
{
    public class OrderService : IOrderService
    {
        #region Fields
        protected readonly IUnitOfWork _unitOfWork;

        protected readonly IDomainFinder _domainFinder;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;

        #endregion

        #region Constructors
        public OrderService(IUnitOfWork unitOfWork, IDomainFinder domainFinder, IConfiguration configuration, IDbNameFinder dbNameFinder)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _domainFinder = domainFinder;
            _configuration = configuration;
            _dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Order>> GetAsync(GetParameter parameter)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Order>>(async response =>
            {
                var iQueryable = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Company.CompanyContact)
                    .Include(o => o.OrderSource)
                    .Include(o => o.OrderNotes)
                    .Include(o => o.OrderBilling)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(x => x.ShipmentCompany)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(os => os.OrderShipmentDetail)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.Customer)
                    .Include(x => x.OrderReturnDetails)
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDetails)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                        .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.Product)
                    .Include(o => o.Payments)
                        .ThenInclude(pt => pt.PaymentType.PaymentTypeTranslations);

                Domain.Entities.Order entity = null;
                if (parameter.OrderId > 0)
                    entity = await iQueryable.FirstOrDefaultAsync(o => o.Id == parameter.OrderId);
                else if (!string.IsNullOrEmpty(parameter.PackerBarcode))
                    entity = await iQueryable.FirstOrDefaultAsync(o => o.OrderPicking.PackerBarcode == parameter.PackerBarcode);
                else if (!string.IsNullOrEmpty(parameter.ProductBarcode))
                {
                    if (string.IsNullOrEmpty(parameter.OrderFilterType) || parameter.OrderFilterType == "Tümü")
                        entity = await iQueryable
                            .OrderBy(o => o.OrderDetails.Count)
                            .ThenBy(x => x.OrderDate)
                            .FirstOrDefaultAsync(o =>
                                o.OrderTypeId == OrderTypes.OnaylanmisSiparis &&
                                o.OrderDetails.Any(od => od.ProductInformation.Barcode == parameter.ProductBarcode));
                    else if (parameter.OrderFilterType == "Tekli")
                        entity = await iQueryable
                            .OrderBy(o => o.OrderDate)
                            .FirstOrDefaultAsync(o =>
                                o.OrderDetails.Count == 1 &&
                                o.OrderTypeId == OrderTypes.OnaylanmisSiparis &&
                                o.OrderDetails.Any(od => od.ProductInformation.Barcode == parameter.ProductBarcode));
                    else if (parameter.OrderFilterType == "Çoklu")
                        entity = await iQueryable
                            .OrderBy(o => o.OrderDate)
                            .FirstOrDefaultAsync(o =>
                                o.OrderDetails.Count > 1 &&
                                o.OrderTypeId == OrderTypes.OnaylanmisSiparis &&
                                o.OrderDetails.Any(od => od.ProductInformation.Barcode == parameter.ProductBarcode));
                }

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }

                foreach (var od in entity.OrderDetails)
                {
                    var productInformationVariants = await _unitOfWork
                        .ProductInformationVariantRepository
                        .DbSet()
                        .Include(x => x.VariantValue)
                            .ThenInclude(x => x.VariantValueTranslations)
                        .Include(x => x.VariantValue.Variant)
                            .ThenInclude(x => x.VariantTranslations)
                        .Where(x => x.ProductInformationId == od.ProductInformationId && x.VariantValue.Variant.ShowVariant)
                        .ToListAsync();

                    od.ProductInformation.ProductInformationVariants = productInformationVariants;
                }

                response.Data = new Order
                {
                    Id = entity.Id,
                    OrderTypeId = entity.OrderTypeId,
                    BulkInvoicing = entity.BulkInvoicing,
                    Amount = entity.Total,
                    Micro = entity.Micro,
                    CargoFee = entity.CargoFee,
                    SurchargeFee = entity.SurchargeFee,
                    CurrencyId = entity.CurrencyId,
                    OrderSourceId = entity.OrderSourceId,
                    MarketPlaceId = entity.MarketplaceId,
                    MarketPlaceOrderNumber = entity.MarketplaceOrderNumber,
                    OrderDate = entity.OrderDate,
                    OrderReturnDetails = entity.OrderReturnDetails.Select(x => new OrderReturnDetail
                    {
                        ProductInformationId = x.ProductInformationId,
                        Quantity = x.Quantity,
                        IsReturn = x.IsReturn,
                        IsLoss = x.IsLoss
                    }).ToList(),
                    Company = new Company
                    {
                        Id = entity.CompanyId,
                        Name = entity.Company.Name,
                        FullName = entity.Company.FullName,
                        CompanyContact = new CompanyContact
                        {
                            Address = entity.Company.CompanyContact.Address,
                            Email = entity.Company.CompanyContact.Email,
                            PhoneNumber = entity.Company.CompanyContact.PhoneNumber,
                            TaxNumber = entity.Company.CompanyContact.TaxNumber,
                            TaxOffice = entity.Company.CompanyContact.TaxOffice,
                            WebUrl = entity.Company.CompanyContact.WebUrl
                        }
                    }
                };

                response.Data.Customer = new Customer
                {
                    Id = entity.Customer.Id,
                    Name = entity.Customer.Name,
                    Surname = entity.Customer.Surname
                };

                response.Data.OrderSource = new OrderSource
                {
                    Id = entity.OrderSource.Id,
                    Name = entity.OrderSource.Name
                };

                response.Data.OrderNotes = entity.OrderNotes.Select(on => new OrderNote
                {
                    Id = on.Id,
                    Note = on.Note
                }).ToList();

                response.Data.Payments = entity
                    .Payments
                    .Select(p => new Payment
                    {
                        Amount = p.Amount,
                        CreatedDate = p.CreatedDate,
                        PaymentTypeId = p.PaymentTypeId,
                        PaymentTypeName = p.PaymentType.PaymentTypeTranslations.FirstOrDefault(x => x.LanguageId == "TR")?.Name

                    }).ToList();

                response.Data.OrderDeliveryAddress = new OrderDeliveryAddress
                {
                    Address = entity.OrderDeliveryAddress.Address,
                    Email = "",
                    Neighborhood = new Neighborhood
                    {
                        Name = entity.OrderDeliveryAddress.Neighborhood.Name,
                        District = new District
                        {
                            Name = entity.OrderDeliveryAddress.Neighborhood.District.Name,
                            City = new City
                            {
                                Name = entity.OrderDeliveryAddress.Neighborhood.District.City.Name
                            }
                        }
                    },
                    PhoneNumber = entity.OrderDeliveryAddress.PhoneNumber,
                    Recipient = entity.OrderDeliveryAddress.Recipient
                };

                response.Data.OrderShipments = entity.OrderShipments.Select(os => new OrderShipment
                {
                    Id = os.Id,
                    ShipmentCompanyId = os.ShipmentCompanyId,
                    TrackingCode = os.TrackingCode,
                    TrackingUrl = os.TrackingUrl,
                    TrackingBase64 = os.TrackingBase64,
                    OrderShipmentDetails = os.OrderShipmentDetail.Select(x => new OrderShipmentDetail
                    {
                        TrackingCode = x.TrackingCode,
                        Id = x.Id,
                        OrderShipmentId = x.OrderShipmentId,
                        Weight = x.Weight
                    }).ToList(),
                    ShipmentCompany = new ShipmentCompany
                    {
                        Name = os.ShipmentCompany.Name
                    },
                    Payor = os.Payor
                }).ToList();

                response.Data.OrderInvoiceInformation = new OrderInvoiceInformation
                {
                    Id = entity.OrderInvoiceInformation.Id,
                    AccountingCompanyId = entity.OrderInvoiceInformation.AccountingCompanyId,
                    Address = entity.OrderInvoiceInformation.Address,
                    FirstName = entity.OrderInvoiceInformation.FirstName,
                    LastName = entity.OrderInvoiceInformation.LastName,
                    Mail = entity.OrderInvoiceInformation.Mail,
                    Phone = entity.OrderInvoiceInformation.Phone,
                    TaxNumber = entity.OrderInvoiceInformation.TaxNumber,
                    TaxOffice = entity.OrderInvoiceInformation.TaxOffice,
                    Guid = entity.OrderInvoiceInformation.Guid,
                    Number = entity.OrderInvoiceInformation.Number,
                    Url = entity.OrderInvoiceInformation.Url,
                    Neighborhood = new Neighborhood
                    {
                        Name = entity.OrderInvoiceInformation.Neighborhood.Name,
                        District = new District
                        {
                            Name = entity.OrderInvoiceInformation.Neighborhood.District.Name,
                            City = new City
                            {
                                Name = entity.OrderInvoiceInformation.Neighborhood.District.City.Name,
                                Country = new Country
                                {
                                    Name = entity.OrderInvoiceInformation.Neighborhood.District.City.Country.Name
                                }
                            }
                        }
                    },
                };

                if (entity.Marketplace != null)
                    response.Data.MarketPlace = new Marketplace
                    {
                        Id = entity.Marketplace.Id,
                        Name = entity.Marketplace.Name
                    };

                if (entity.OrderBilling != null)
                    response.Data.OrderBilling = new OrderBilling
                    {
                        Id = entity.OrderBilling.Id,
                        ReturnDescription = entity.OrderBilling.ReturnDescription,
                        InvoiceNumber = entity.OrderBilling.InvoiceNumber,
                        ReturnNumber = entity.OrderBilling.ReturnNumber
                    };


                response.Data.OrderDetails = entity
                        .OrderDetails
                        .Select(od =>
                        {
                            var orderDetail = new OrderDetail
                            {
                                Payor = od.Payor,
                                ProductInformationId = od.ProductInformationId,
                                VatExcListPrice = od.VatExcListUnitPrice,
                                VatExcUnitPrice = od.VatExcUnitPrice,
                                VatExcUnitCost = od.VatExcUnitCost,
                                VatExcUnitDiscount = od.VatExcUnitDiscount,
                                UnitCost = od.UnitCost,
                                Quantity = od.Quantity,
                                UnitPrice = od.UnitPrice,
                                ListUnitPrice = od.ListUnitPrice,
                                VatRate = od.VatRate
                            };

                            var product = new Product();
                            product.SellerCode = od.ProductInformation.Product.SellerCode;

                            var productInformationMarketplace =
                                _unitOfWork
                                .ProductInformationMarketplaceRepository
                                .DbSet()
                                .FirstOrDefault(pim => pim.ProductInformationId == od.ProductInformationId &&
                                                       pim.MarketplaceId == entity.MarketplaceId);

                            var productInformation = new ProductInformation
                            {
                                Id = od.ProductInformationId,
                                Barcode = od.ProductInformation.Barcode,
                                StockCode = od.ProductInformation.StockCode,
                                Name = od.ProductInformation.ProductInformationTranslations.FirstOrDefault()?.Name,
                                ProductInformationUUId = productInformationMarketplace.UUId,
                                FullName = $"{od.ProductInformation.ProductInformationTranslations.FirstOrDefault()?.Name}  {od.ProductInformation.ProductInformationTranslations.FirstOrDefault()?.VariantValuesDescription}"
                            };

                            var domain = _unitOfWork.DomainRepository.DbSet().Include(x => x.DomainSetting)
                            .FirstOrDefault(x => x.Id == _domainFinder.FindId());

                            var fileName = $"{domain.DomainSetting.ImageUrl}/img/product/0/no-image.jpg";


                            productInformation.ProductInformationPhoto = new ProductInformationPhoto
                            {
                                FileName = od.ProductInformation.ProductInformationPhotos.Count > 0 ? $"{domain.DomainSetting.ImageUrl}/product/{od.ProductInformation.ProductInformationPhotos.FirstOrDefault().FileName}"
                                : fileName
                            };


                            if (od
                                            .ProductInformation
                                            .ProductInformationVariants != null)
                                productInformation.ProductInformationVariants = od
                                                .ProductInformation
                                                .ProductInformationVariants
                                                .Select(piv => new ProductInformationVariant
                                                {
                                                    VariantValue = new VariantValue
                                                    {
                                                        Name = piv.VariantValue.VariantValueTranslations.FirstOrDefault()?.Value,
                                                        VariantName = piv.VariantValue.Variant.VariantTranslations.FirstOrDefault()?.Name,
                                                    }
                                                }).ToList();

                            product.ProductInformation = productInformation;

                            orderDetail.Product = product;

                            return orderDetail;
                        })
                        .ToList();

                response.Success = true;
            });
        }

        public async Task<NoContentResponse> CancelAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .FirstOrDefaultAsync(o => o.Id == id);

                if (entity.OrderTypeId == OrderTypes.Paketleniyor)
                    entity.OrderTypeId = OrderTypes.OnaylanmisSiparis;

                this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Success = true;
            });
        }

        public async Task<NoContentResponse> CompleteAsync(int id)
        {

            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                     ._unitOfWork
                     .OrderRepository
                     .DbSet()
                     .Include(x => x.OrderPicking)
                     .FirstOrDefaultAsync(o => o.Id == id);

                if (entity.OrderPicking != null)
                    entity.OrderPicking.PackerBarcode = null;

                entity.OrderTypeId = "KTE";
                entity.OrderPacking = new Domain.Entities.OrderPacking
                {
                    TenantId = entity.TenantId,
                    DomainId = entity.DomainId,
                    CompanyId = entity.CompanyId,
                    CreatedDate = DateTime.Now
                };

                this
                     ._unitOfWork
                     .OrderRepository
                     .Update(entity);

                response.Success = true;
            });
        }

        public async Task<NoContentResponse> NotAvailableAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .FirstOrDefaultAsync(o => o.Id == id);

                entity.OrderTypeId = "TE";

                this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Success = true;
            });
        }

        public async Task<DataResponse<bool>> PackedAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<bool>>(async response =>
            {
                response.Data = await this._unitOfWork.OrderPackingRepository.DbSet().AnyAsync(o => o.Id == id);
                response.Success = true;
            });
        }

        public async Task<DataResponse<OrderSummary>> SummaryAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<OrderSummary>>(async response =>
            {
                var todayOrdersCount = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsNoTracking()
                    .CountAsync(o => o.CreatedDate.Date == DateTime.Now.Date && o.OrderTypeId != "IP");

                var unpackedOrdersCount = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsNoTracking()
                    .CountAsync(o => o.OrderTypeId == "OS");

                var packedOrdersCount = await this
                    ._unitOfWork
                    .OrderPackingRepository
                    .DbSet()
                    .AsNoTracking()
                    .CountAsync(op => op.CreatedDate.Date == DateTime.Now.Date);

                response.Data = new OrderSummary
                {
                    TodayOrdersCount = todayOrdersCount,
                    PackedOrdersCount = packedOrdersCount,
                    UnpackedOrdersCount = unpackedOrdersCount
                };
                response.Success = true;
            });
        }

        public async Task<DataResponse<int>> ReserveAsync(ReserveParameter parameter)
        {
            SqlConnection sqlConnection = null;

            return await ExceptionHandler.HandleAsync<DataResponse<int>>(async response =>
            {
                using (sqlConnection = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var rawSql = @"
                Update Top(1)	Orders
                Set				OrderTypeId = 'PA'
                Output			Inserted.Id
                Where			OrderTypeId = 'OS'
                                And DomainId = @DomainId";

                    if (!string.IsNullOrEmpty(parameter.ShipmentCompanyId))
                        rawSql = @"
                Update Top(1)	Orders
                Set				OrderTypeId = 'PA'
                Output			Inserted.Id
                From			Orders
                Join			OrderShipments
                On				Orders.Id = OrderShipments.OrderId
                				And OrderShipments.ShipmentCompanyId = @ShipmentCompanyId
                Where			OrderTypeId = 'OS'
                                And Orders.DomainId = @DomainId";

                    if (!string.IsNullOrEmpty(parameter.MarketplaceId))
                        rawSql += "\nAnd MarketplaceId = @MarketplaceId";

                    if (parameter.OrderSourceId.HasValue)
                        rawSql += "\nAnd OrderSourceId = @OrderSourceId";

                    if (parameter.OrderId.HasValue)
                        rawSql += "\nAnd Id = @Id";

                    using (SqlCommand sqlCommand = new(rawSql, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DomainId", _domainFinder.FindId());

                        if (!string.IsNullOrEmpty(parameter.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", parameter.MarketplaceId);

                        if (!string.IsNullOrEmpty(parameter.ShipmentCompanyId))
                            sqlCommand.Parameters.AddWithValue("@ShipmentCompanyId", parameter.ShipmentCompanyId);

                        if (parameter.OrderSourceId.HasValue)
                            sqlCommand.Parameters.AddWithValue("@OrderSourceId", parameter.OrderSourceId.Value);

                        if (parameter.OrderId.HasValue)
                            sqlCommand.Parameters.AddWithValue("@Id", parameter.OrderId.Value);

                        sqlConnection.Open();

                        var id = await sqlCommand.ExecuteScalarAsync();

                        if (id == null)
                        {
                            response.Success = false;
                            response.Message = "Sipariş bulunamadı";
                        }
                        else
                        {
                            response.Success = true;
                            response.Data = Convert.ToInt32(id);
                        }

                        sqlConnection.Close();
                    }
                }
            }, response =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        public async Task<DataResponse<List<string>>> GetTrendyolEuropePackagesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<string>>>(async response =>
            {
                response.Data = await this
                     ._unitOfWork
                     .OrderRepository
                     .DbSet()
                     .Where(o => o.MarketplaceId == Marketplaces.TrendyolEurope && o.OrderShipments.Any(os => !string.IsNullOrEmpty(os.TrackingCode)))
                     .SelectMany(o => o.OrderShipments.Select(os => os.TrackingCode))
                     .Distinct()
                     .ToListAsync();

                response.Success = true;
            });
        }

        public async Task<DataResponse<List<int>>> GetTrendyolEuropeOrderNumberAsync(string packageNumber)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<int>>>(async response =>
            {
                response.Data = await this
                     ._unitOfWork
                     .OrderRepository
                     .DbSet()
                     .Where(o => o.MarketplaceId == Marketplaces.TrendyolEurope &&
                                 o.OrderShipments.Any(os => os.TrackingCode == packageNumber))
                     .Select(o => o.Id)
                     .ToListAsync();

                response.Success = true;
            });
        }
        #endregion
    }
}
