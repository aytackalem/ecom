﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.DataTransferObjects;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Packaging.Persistence.Services
{
    public class ShipmentCompanyCompanyService : IShipmentCompanyCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ShipmentCompanyCompanyService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<ShipmentCompany>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<ShipmentCompany>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .ShipmentCompanyCompanyRepository
                    .DbSet()
                    .Include(x => x.ShipmentCompany)
                    .ToListAsync();

                response.Data = entities
                    .Select(e => new ShipmentCompany { Id = e.ShipmentCompany.Id, Name = e.ShipmentCompany.Name })
                    .ToList();

                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
