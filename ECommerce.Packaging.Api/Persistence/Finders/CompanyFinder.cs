﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Packaging.Persistence.Finders
{
    public class CompanyFinder : ICompanyFinder
    {
        #region Fields
        private int _companyId = 0;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public CompanyFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            if (this._companyId == 0)
            {
                if (this._httpContextAccessor.HttpContext.Request.Headers.ContainsKey("companyId"))
                    this._companyId = int.Parse(this._httpContextAccessor.HttpContext.Request.Headers["companyId"].ToString());
            }

            return this._companyId;
        }

        public void Set(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
