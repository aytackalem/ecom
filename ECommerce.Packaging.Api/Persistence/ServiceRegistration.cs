﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Packaging.Core.Application.Interfaces.Services;
using ECommerce.Packaging.Persistence.Finders;
using ECommerce.Packaging.Persistence.Handlers;
using ECommerce.Packaging.Persistence.Services;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;

namespace ECommerce.Packaging.Persistence
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddHttpContextAccessor();

            serviceCollection.AddDbContext<ECommerce.Persistence.Context.ApplicationDbContext>(contextLifetime: ServiceLifetime.Scoped, optionsLifetime: ServiceLifetime.Scoped);
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            #region Finders
            serviceCollection.AddScoped<IDbNameFinder, DbNameFinder>();
            serviceCollection.AddScoped<ITenantFinder, TenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, DomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, CompanyFinder>();
            #endregion

            #region Services
            serviceCollection.AddScoped<IAccountingCompanyConfigurationService, AccountingCompanyConfigurationService>();
            serviceCollection.AddScoped<ICompanyConfigurationService, CompanyConfigurationService>();
            serviceCollection.AddScoped<ICompanyService, CompanyService>();
            serviceCollection.AddScoped<IDomainService, DomainService>();
            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();
            serviceCollection.AddScoped<IOrderInvoiceInformationService, OrderInvoiceInformationService>();
            serviceCollection.AddScoped<IOrderService, OrderService>();
            serviceCollection.AddScoped<IOrderShipmentService, OrderShipmentService>();
            serviceCollection.AddScoped<IOrderSourceService, OrderSourceService>();
            serviceCollection.AddScoped<IShipmentCompanyCompanyService, ShipmentCompanyCompanyService>();
            serviceCollection.AddScoped<IOrderReturnService, OrderReturnService>();
            #endregion
        }
        #endregion
    }
}
