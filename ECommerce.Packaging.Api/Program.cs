using ECommerce.Accounting.Winka;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Infrastructure.Accounting.BizimHesap;
using ECommerce.Infrastructure.Accounting.Mikro;
using ECommerce.Infrastructure.Shipment.Aras;
using ECommerce.Infrastructure.Shipment.Artos;
using ECommerce.Infrastructure.Shipment.Ups;
using ECommerce.Packaging.Persistence;
using ECommerce.Persistence.Common.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Packaging.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var services = builder.Services;

            //services.AddScoped<BizimHesapProviderService>();
            services.AddScoped<MikroProviderService>();
            services.AddScoped<WinkaProviderService>();
            services.AddScoped<IAccountingProviderService, AccountingProviderService>();
            services.AddScoped<AccountingProviderResolver>(serviceProvider => accountingProviderId =>
            {
                IAccountingProvider accountingProvider = accountingProviderId switch
                {
                    "W" => serviceProvider.GetService<WinkaProviderService>(),
                    //"BH" => serviceProvider.GetService<BizimHesapProviderService>(),
                    "M" => serviceProvider.GetService<MikroProviderService>(),
                    _ => null
                };
                return accountingProvider;
            });

            services.AddPersistenceServices(builder.Configuration);
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
                {
#if DEBUG
                    o.RequireHttpsMetadata = false;
#endif

                    o.Authority = builder.Configuration["IdentityServerUrl"];
                    o.Audience = "resource_packing";
                });
            services.AddControllers();

            var app = builder.Build();
            app.UseAuthentication();
            app.UseAuthorization();
            app.MapControllers();

#if RELEASE
            app.UseHsts();
#endif

            app.Run();
        }
    }
}