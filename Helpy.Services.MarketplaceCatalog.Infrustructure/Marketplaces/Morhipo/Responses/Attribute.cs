﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Morhipo.Responses
{
    public class Attribute
    {
        #region Properties
        public int Id { get; set; }
        
        public string Name { get; set; } 
        #endregion
    }
}
