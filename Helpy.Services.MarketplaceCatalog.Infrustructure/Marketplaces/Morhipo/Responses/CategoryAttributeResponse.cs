﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Morhipo.Responses
{
    public class CategoryAttributeResponse
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string DisplayName { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryAttribute> CategoryAttributes { get; set; }
        #endregion
    }
}
