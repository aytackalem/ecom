﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Morhipo.Responses
{
    public class CategoryResponse
    {
        #region Navigation Properties
        public List<SubCategory> Categories { get; set; }
        #endregion
    }
}
