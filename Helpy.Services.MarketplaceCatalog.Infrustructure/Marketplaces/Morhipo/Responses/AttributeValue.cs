﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Morhipo.Responses
{
    public class AttributeValue
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
