﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Lcw.Categories;

public class Response
{
    public int count { get; set; }
    public int size { get; set; }
    public int page { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
    public object errorMessage { get; set; }
}

public class Datum
{
    public int categoryId { get; set; }
    public int? parentCategoryId { get; set; }
    public Value[] values { get; set; }
    public int categoryLevel { get; set; }
}

public class Value
{
    public string languageCode { get; set; }
    public string value { get; set; }
}