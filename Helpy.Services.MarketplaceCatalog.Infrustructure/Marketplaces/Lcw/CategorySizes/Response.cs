﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Lcw.CategorySizes;

public class Response
{
    public int count { get; set; }
    public int size { get; set; }
    public int page { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
    public object errorMessage { get; set; }
}

public class Datum
{
    public int categoryId { get; set; }
    public string categoryValue { get; set; }
    public Size[] sizes { get; set; }
}

public class Size
{
    public int sizeId { get; set; }
    public string sizeValue { get; set; }
}
