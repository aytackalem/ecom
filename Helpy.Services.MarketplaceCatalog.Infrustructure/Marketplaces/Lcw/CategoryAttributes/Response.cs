﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Lcw.CategoryAttributes;

public class Response
{
    public int count { get; set; }
    public int size { get; set; }
    public int page { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
    public object errorMessage { get; set; }
}

public class Datum
{
    public int attributeId { get; set; }
    public bool isRequired { get; set; }
    public Value[] values { get; set; }
    public Attributevalue[] attributeValues { get; set; }
}

public class Value
{
    public string languageCode { get; set; }
    public string value { get; set; }
}

public class Attributevalue
{
    public int attributeValueId { get; set; }
    public Value1[] values { get; set; }
}

public class Value1
{
    public string languageCode { get; set; }
    public string value { get; set; }
}
