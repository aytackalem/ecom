﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Lcw;

public class Lcw : IMarketplace, IBrandDownloadable
{
    #region Fields
    private readonly IHttpHelper _httpHelper;
    #endregion

    #region Constructors
    public Lcw(IHttpHelper httpHelper)
    {
        #region Fields
        this._httpHelper = httpHelper;
        #endregion
    }
    #endregion

    #region Properties
    public string Id => "LCW";
    #endregion

    #region Methods
    public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
        {
            await GetTokenAsync();

            Int32 page = 0;
            List<Categories.Datum> lcwCategories = new();
            Categories.Response categoryResponse;
            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<Categories.Response>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = $"https://mpapi.lcw.com/integration/product-integration/service/api/Category/GetCategory?Page={page}&Size=100",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _token
                });

                categoryResponse = httpHelperV3Response.Content;

                lcwCategories.AddRange(categoryResponse.data);

                page++;
            } while (page < categoryResponse.totalPages);

            List<Category> categories = new();

            foreach (var level4Category in lcwCategories.Where(_ => _.categoryLevel == 4))
            {
                Categories.Datum parentCategory = lcwCategories.FirstOrDefault(_ => _.categoryId == level4Category.parentCategoryId);

                if (parentCategory is null)
                {
                    categories.Add(new Category
                    {
                        Code = level4Category.categoryId.ToString(),
                        MarketplaceId = this.Id,
                        Name = level4Category.values[0].value,
                        ParentCode = level4Category.parentCategoryId.ToString(),
                        Breadcrumb = string.Empty
                    });

                    continue;
                }

                List<string> parents = new()
                {
                    parentCategory.values[0].value
                };

                while (parentCategory is not null)
                {
                    parentCategory = lcwCategories.FirstOrDefault(_ => _.categoryId == parentCategory.parentCategoryId);

                    if (parentCategory is not null)
                        parents.Add(parentCategory.values[0].value);
                }

                categories.Add(new Category
                {
                    Code = level4Category.categoryId.ToString(),
                    MarketplaceId = this.Id,
                    Name = level4Category.values[0].value,
                    ParentCode = level4Category.parentCategoryId.ToString(),
                    Breadcrumb = string.Join(" > ", parents)
                });
            }

            response.Data = categories;
            response.Success = true;
        });
    }

    public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
        {
            await GetTokenAsync();

            List<Variant> data = new();

            #region Category Attributes
            Int32 page = 0;
            List<CategoryAttributes.Response> categoryAttributeResponses = new();
            CategoryAttributes.Response categoryAttributeResponse;
            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<CategoryAttributes.Response>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = false,
                    RequestUri = $"https://mpapi.lcw.com/integration/product-integration/service/api/Attribute/GetAttributeFilterCategoryId?Page={page}&Size=100&CategoryId={categoryCode}",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _token
                });

                categoryAttributeResponse = httpHelperV3Response.Content;

                categoryAttributeResponses.Add(categoryAttributeResponse);

                page++;
            } while (page < categoryAttributeResponse.totalPages);

            data.AddRange(categoryAttributeResponses.SelectMany(_ => _.data.Select(ca => new Variant
            {
                MarketplaceId = this.Id,
                CategoryCode = categoryCode,
                Code = ca.attributeId.ToString(),
                Name = ca.values[0].value,
                AllowCustom = false,
                Mandatory = ca.isRequired,
                VariantValues = ca
                    .attributeValues
                    .Select(av => new VariantValue
                    {
                        VariantCode = ca.attributeId.ToString(),
                        Code = av.attributeValueId.ToString(),
                        Name = av.values[0].value,
                        CategoryCodes = new List<string> { categoryCode }
                    })
                    .ToList()
            })));
            #endregion

            #region Category Sizes
            page = 0;
            List<CategorySizes.Response> CategorySizeResponses = new();
            CategorySizes.Response CategorySizeResponse;
            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<CategorySizes.Response>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = false,
                    RequestUri = $"https://mpapi.lcw.com/integration/product-integration/service/api/Category/GetCategorySizeByCategoryId?Page={page}&Size=100&CategoryId={categoryCode}",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _token
                });

                CategorySizeResponse = httpHelperV3Response.Content;

                CategorySizeResponses.Add(CategorySizeResponse);

                page++;
            } while (page < CategorySizeResponse.totalPages);

            data.AddRange(CategorySizeResponses.SelectMany(_ => _.data.Select(ca => new Variant
            {
                MarketplaceId = this.Id,
                CategoryCode = categoryCode,
                Code = "Size",
                Name = "Beden",
                AllowCustom = false,
                Mandatory = true,
                VariantValues = ca
                    .sizes
                    .Select(av => new VariantValue
                    {
                        VariantCode = "Size",
                        Code = av.sizeId.ToString(),
                        Name = av.sizeValue,
                        CategoryCodes = new List<string> { categoryCode }
                    })
                    .ToList()
            })));
            #endregion

            #region Main Colors
            page = 0;
            List<MainColors.Response> mainColorResponses = new();
            MainColors.Response mainColorResponse;
            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<MainColors.Response>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = false,
                    RequestUri = $"https://mpapi.lcw.com/integration/product-integration/service/api/Attribute/GetMainColor?Page={page}&Size=100",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _token
                });

                mainColorResponse = httpHelperV3Response.Content;

                mainColorResponses.Add(mainColorResponse);

                page++;
            } while (page < mainColorResponse.totalPages);

            data.Add(new Variant
            {
                MarketplaceId = this.Id,
                CategoryCode = categoryCode,
                Code = "Color",
                Name = "Renk",
                AllowCustom = false,
                Mandatory = true,
                VariantValues = mainColorResponses
                    .SelectMany(_ => _.data.Select(_ => new VariantValue
                    {
                        Code = _.mainColorCode,
                        Name = _.values[0].value,
                        VariantCode = "Color",
                        CategoryCodes = new List<string> { categoryCode }
                    }))
                    .ToList()
            });
            #endregion

            response.Data = data;
            response.Success = true;
        });
    }

    public async Task<DataResponse<List<Brand>>> GetBrandsAsync()
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async (response) =>
        {

            await GetTokenAsync();

            Int32 page = 0;
            List<Brands.Response> brandResponses = new();
            Brands.Response brandResponse;
            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<Brands.Response>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = $"https://mpapi.lcw.com/integration/product-integration/service/api/Brand/GetBrand?Page={page}&Size=100",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _token
                });

                brandResponse = httpHelperV3Response.Content;

                brandResponses.Add(brandResponse);

                page++;
            } while (page < brandResponse.totalPages);

            response.Data = brandResponses
                .SelectMany(_ => _.data.SelectMany(_ => _.brands).Select(_ => new Brand
                {
                    Code = _.brandId.ToString(),
                    Name = _.name
                }))
                .ToList();
            response.Success = true;

        });
    }
    #endregion

    #region Private Methods
    #region Token Methods
    private string _token;

    private async Task GetTokenAsync()
    {
        if (string.IsNullOrEmpty(_token))
        {
            var data = new List<KeyValuePair<string, string>> {
                new("client_id", "LCWSellerEntegrator"),
                new("username", "kazim.cavus@lafaba.com"),
                new("password", "Lafaba2024-LCW!"),
                new("grant_type", "password"),
                new("client_secret", "D51C3FADCA8A40118A88F3CD6C197EEA"),
                new("scope", "openid")
            };

            var tokenResponse = await this._httpHelper.SendAsync<Token>(new HttpFormUrlEncodedRequest
            {
                RequestUri = "https://pars.lcwaikiki.com/sts/issue/oidc/token",
                Data = data,
                HttpMethod = HttpMethod.Post
            });

            _token = tokenResponse.Content.AccessToken;
        }
    }

    class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
    #endregion
    #endregion
}
