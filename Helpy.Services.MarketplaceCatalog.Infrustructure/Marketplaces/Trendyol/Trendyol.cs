﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Trendyol.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Trendyol
{
    public class Trendyol : IMarketplace, IBrandSearchable
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Trendyol(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "TY";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var categoryResponse = await this._httpHelper.SendAsync<CategoryResponse>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = "https://api.trendyol.com/sapigw/product-categories",
                    HttpMethod = HttpMethod.Get
                });

                this.FetchSubCategories(categoryResponse.Content.Categories, data, null);

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var categoryAttributeResponse = await this._httpHelper.SendAsync<CategoryAttributeResponse>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = false,
                    RequestUri = $"https://api.trendyol.com/sapigw/product-categories/{categoryCode}/attributes",
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(categoryAttributeResponse
                    .Content
                    .CategoryAttributes
                    .Select(ca => new Variant
                    {
                        MarketplaceId = "TY",
                        CategoryCode = categoryCode,
                        Code = ca.Attribute.Id.ToString(),
                        Name = ca.Attribute.Name,
                        AllowCustom = ca.AllowCustom,
                        Mandatory = ca.Required,
                        VariantValues = ca
                            .AttributeValues
                            .Select(av => new VariantValue
                            {
                                VariantCode = ca.Attribute.Id.ToString(),
                                Code = av.Id.ToString(),
                                Name = av.Name,
                                CategoryCodes = new List<string> { categoryCode }
                            })
                            .ToList()
                    }));

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Brand>>> SearchBrandByNameAsync(string name)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async (response) =>
            {

                List<Brand> data = new();

                var brandResponse = await this._httpHelper.SendAsync<List<BrandResponse>>(new HttpRequest
                {
                    RequestUri = $"https://api.trendyol.com/sapigw/brands/by-name?name={name}",
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(
                    brandResponse
                    .Content
                    .Select(x => new Brand
                    {
                        Code = x.id.ToString(),
                        Name = x.name
                    }
               ).ToList());

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategories(List<SubCategory> subCategories, List<Category> categories, List<string> parents)
        {
            if (subCategories != null && subCategories.Count > 0)
                foreach (var theSubCategory in subCategories)
                {
                    if (theSubCategory.ParentId.HasValue == false)
                        parents = new();

                    if (theSubCategory.SubCategories != null && theSubCategory.SubCategories.Count > 0)
                    {
                        var nParents = new List<string>();
                        nParents.AddRange(parents);
                        nParents.Add(theSubCategory.Name);

                        this.FetchSubCategories(theSubCategory.SubCategories, categories, nParents);
                    }
                    else
                        categories.Add(new()
                        {
                            MarketplaceId = "TY",
                            Code = theSubCategory.Id.ToString(),
                            Name = theSubCategory.Name,
                            ParentCode = theSubCategory.ParentId.ToString(),
                            Breadcrumb = string.Join(" > ", parents).TrimEnd('>', ' ')
                        });
                }
        }
        #endregion
    }
}
