﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Trendyol.Responses
{
    public class SubCategory
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }
        #endregion

        #region Navigation Properties
        public List<SubCategory> SubCategories { get; set; }
        #endregion
    }
}
