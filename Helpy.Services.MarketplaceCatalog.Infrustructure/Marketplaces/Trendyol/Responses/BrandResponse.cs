﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Trendyol.Responses
{
    public class BrandResponse
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
