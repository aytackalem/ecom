﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Requests
{
    public class TokenRequest
    {
        #region Properties
        public string Email { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
