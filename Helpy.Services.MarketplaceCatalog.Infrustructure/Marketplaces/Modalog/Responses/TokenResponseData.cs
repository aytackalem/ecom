﻿using System;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    public class TokenResponseData
    {
        #region Properties
        public string token { get; set; }

        public DateTime expiration { get; set; }
        #endregion
    }
}
