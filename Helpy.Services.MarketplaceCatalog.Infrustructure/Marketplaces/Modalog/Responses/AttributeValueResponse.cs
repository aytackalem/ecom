﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    public class AttributeValueResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public AttributeValueResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class AttributeValueResponseItem
    {
        public string name { get; set; }
        public bool isCustom { get; set; }
        public object customValue { get; set; }
        public int attributeId { get; set; }
        public string attributeName { get; set; }
        public bool isChecked { get; set; }
        public int id { get; set; }
    }

}
