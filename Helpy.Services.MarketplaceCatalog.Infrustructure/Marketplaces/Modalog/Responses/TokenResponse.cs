﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    public class TokenResponse
    {
        #region Properties
        public bool Success { get; set; }

        public string Message { get; set; }

        public TokenResponseData Data { get; set; }

        public int RowCount { get; set; }

        public int currentPageIndex { get; set; }

        public int pageSize { get; set; }

        public int pageCount { get; set; }
        #endregion
    }
}
