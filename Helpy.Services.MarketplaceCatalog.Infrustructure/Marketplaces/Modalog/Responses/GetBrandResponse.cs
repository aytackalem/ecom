﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    public class GetBrandResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<BrandResponse> data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class BrandResponse
    {
        public int brandId { get; set; }
        public string name { get; set; }
    }
}
