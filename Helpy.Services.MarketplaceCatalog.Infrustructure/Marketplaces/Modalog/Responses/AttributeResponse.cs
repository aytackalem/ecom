﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    public class AttributeResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public AttributeResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class AttributeResponseItem
    {
        public string name { get; set; }
        public bool isCustom { get; set; }
        public int id { get; set; }
    }

}
