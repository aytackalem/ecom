﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Responses
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Category
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public string parentName { get; set; }
        public string name { get; set; }
        public string languageCode { get; set; }
        public bool isExpanded { get; set; }
        public bool hasChild { get; set; }
        public List<SubCategory> subCategories { get; set; }
    }

    public class CategoryResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<Category> data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class SubCategory
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public string parentName { get; set; }
        public string name { get; set; }
        public string languageCode { get; set; }
        public bool isExpanded { get; set; }
        public bool hasChild { get; set; }
        public List<SubCategory> subCategories { get; set; }
    }


}
