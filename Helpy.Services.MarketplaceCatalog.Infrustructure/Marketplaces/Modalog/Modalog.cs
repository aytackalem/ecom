﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog.Requests;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog
{
    public class Modalog : IMarketplace, IBrandDownloadable
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Modalog(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "ML";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var token = await this.GetToken();

                var modalogCategories = new List<Responses.Category>();
                var categoryPageCount = 0;
                var categoryPage = 0;
                do
                {
                    var httpResponse = await this._httpHelper.SendAsync<Responses.CategoryResponse>(
                        new HttpRequest
                        {
                            UseCache = true,
                            RequestUri = "https://merchant-api.modalog.com/Categories",
                            AuthorizationType = AuthorizationType.Bearer,
                            Authorization = token,
                            HttpMethod = HttpMethod.Get
                        });

                    if (categoryPageCount == 0)
                        categoryPageCount = httpResponse.Content.pageCount;

                    modalogCategories.AddRange(httpResponse.Content.data);
                } while (categoryPageCount < categoryPage);

                foreach (var theModalogCategory in modalogCategories)
                {
                    if (theModalogCategory.subCategories == null)
                        data.Add(new Category
                        {
                            MarketplaceId = "ML",
                            Code = theModalogCategory.id.ToString(),
                            Name = theModalogCategory.name
                        });
                    else
                        FetchSubCategory(theModalogCategory.subCategories, data, new() { theModalogCategory.name });
                }

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var token = await this.GetToken();

                var variantValues = new List<Responses.VariantValueResponseItem>();
                var variantValuePageCount = 0;
                var variantValuePage = 1;
                do
                {
                    var variantValueResponse = await this
                        ._httpHelper
                        .SendAsync<Responses.VariantValueResponse>(
                            new HttpRequest
                            {
                                UseCache = true,
                                RequestUri = $"https://merchant-api.modalog.com/Variants?pageIndex={variantValuePage}",
                                AuthorizationType = AuthorizationType.Bearer,
                                Authorization = token,
                                HttpMethod = HttpMethod.Get
                            });

                    if (variantValuePageCount == 0)
                        variantValuePageCount = variantValueResponse.Content.pageCount;

                    variantValues.AddRange(variantValueResponse.Content.data);

                    variantValuePage++;
                } while (variantValuePageCount >= variantValuePage);

                var attributes = new List<Responses.CategoryAttribute>();
                var attributePageCount = 0;
                var attributePage = 1;
                do
                {
                    var attributeResponse = await this
                        ._httpHelper
                        .SendAsync<Responses.CategoryAttributeResponse>(
                            new HttpRequest
                            {
                                UseCache = true,
                                RequestUri = $"https://merchant-api.modalog.com/Categories/{categoryCode}/Attributes/?pageIndex={attributePage}",
                                AuthorizationType = AuthorizationType.Bearer,
                                Authorization = token,
                                HttpMethod = HttpMethod.Get
                            });

                    if (attributePageCount == 0)
                        attributePageCount = attributeResponse.Content.pageCount;

                    attributes.AddRange(attributeResponse.Content.data);

                    attributePage++;
                } while (attributePageCount >= attributePage);

                foreach (var theAttribute in attributes)
                {
                    #region Fetch Attribute Values
                    var attributeValues = new List<Responses.AttributeValueResponseItem>();
                    var attributeValuePageCount = 0;
                    var attributeValuePage = 1;
                    do
                    {
                        var attributeValueResponse = await this
                            ._httpHelper
                            .SendAsync<Responses.AttributeValueResponse>(
                                new HttpRequest
                                {
                                    UseCache = true,
                                    RequestUri = $"https://merchant-api.modalog.com/Attributes/{theAttribute.attributeId}/Values?pageIndex={attributeValuePage}",
                                    AuthorizationType = AuthorizationType.Bearer,
                                    Authorization = token,
                                    HttpMethod = HttpMethod.Get
                                });

                        if (attributeValuePageCount == 0)
                            attributeValuePageCount = attributeValueResponse.Content.pageCount;

                        attributeValues.AddRange(attributeValueResponse.Content.data);

                        attributeValuePage++;
                    } while (attributeValuePageCount >= attributeValuePage);
                    #endregion

                    theAttribute.Values = attributeValues;
                }

                data.Add(new Variant
                {
                    MarketplaceId = "ML",
                    CategoryCode = categoryCode,
                    Mandatory = true,
                    Code = "Beden",
                    Name = "Beden",
                    VariantValues = variantValues.Select(vv => new VariantValue
                    {
                        Code = vv.id.ToString(),
                        Name = vv.name,
                        VariantCode = "Beden",
                        CategoryCodes = new List<string> { categoryCode }
                    }).ToList()
                });
                data.AddRange(attributes.Select(a => new Variant
                {
                    MarketplaceId = "ML",
                    CategoryCode = categoryCode,
                    Code = a.attributeId.ToString(),
                    Name = a.attributeName,
                    AllowCustom = a.isCustom,
                    VariantValues = a.Values.Select(v => new VariantValue
                    {
                        Code = v.id.ToString(),
                        Name = v.name,
                        VariantCode = a.attributeId.ToString(),
                        CategoryCodes = new List<string> { categoryCode }
                    }).ToList()
                }));

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Brand>>> GetBrandsAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async response =>
            {
                List<Brand> brands = new();

                var page = 1;
                var totalPage = 0;

                do
                {
                    var getBrandResponse = await this
                        ._httpHelper
                        .SendAsync<Responses.GetBrandResponse>(new HttpRequest
                        {
                            UseCache = false,
                            HttpMethod = HttpMethod.Get,
                            RequestUri = $"https://merchant-api.modalog.com/Brands?pageIndex={page}"
                        });

                    if (getBrandResponse.Success)
                    {
                        totalPage = getBrandResponse.Content.pageCount;

                        brands.AddRange(getBrandResponse.Content.data.Select(b => new Brand
                        {
                            Code = b.brandId.ToString(),
                            Name = b.name
                        }));
                    }

                    page++;
                } while (page <= totalPage);

                response.Data = brands;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private async Task<string> GetToken()
        {
            var tokenResponse = await this._httpHelper.SendAsync<TokenRequest, Responses.TokenResponse>(new HttpRequest<TokenRequest>
            {
                RequestUri = "https://merchant-api.modalog.com/Auth/Login",
                Request = new TokenRequest
                {
                    Email = "kazim.cavus@lafaba.com",
                    Password = "Lafaba2023-MDLG"
                },
                HttpMethod = HttpMethod.Post
            });
            return tokenResponse.Content.Data.token;
        }

        private void FetchSubCategory(List<Responses.SubCategory> subCategories, List<Category> categories, List<string> parents)
        {
            foreach (var theSubCategory in subCategories)
            {
                if (theSubCategory.subCategories != null && theSubCategory.subCategories.Count > 0)
                {
                    var nParents = new List<string>();
                    nParents.AddRange(parents);
                    nParents.Add(theSubCategory.name);

                    FetchSubCategory(theSubCategory.subCategories, categories, nParents);
                }
                else
                {
                    categories.Add(new Category
                    {
                        MarketplaceId = "ML",
                        Code = theSubCategory.id.ToString(),
                        ParentCode = theSubCategory.parentId.ToString(),
                        Name = theSubCategory.name,
                        Breadcrumb = string.Join(" > ", parents)
                    });
                }
            }
        }
        #endregion
    }
}