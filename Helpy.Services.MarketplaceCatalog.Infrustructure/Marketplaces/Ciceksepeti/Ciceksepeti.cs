﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti
{
    public class Ciceksepeti : IMarketplace
    {
        #region Fields
        private readonly string _apiKey = "S1R0VkhMDx5MBeTLvr3vY1VIvKP1aEdQO9EZE9Qg";

        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Ciceksepeti(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "CS";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var httpResponse = await this._httpHelper.SendAsync<CicekSepetiCategory>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = $"https://apis.ciceksepeti.com/api/v1/Categories",
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = this._apiKey,
                    HttpMethod = HttpMethod.Get
                });

                httpResponse.Content.Categories.ForEach(theCategory =>
                {
                    this.FetchSubCategories(theCategory.SubCategories, data, new() { theCategory.Name });
                });

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var httpResponse = await this._httpHelper.SendAsync<CicekSepetiAttributeCategory>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = false,
                    RequestUri = $"https://apis.ciceksepeti.com/api/v1/categories/{categoryCode}/attributes",
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = this._apiKey,
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(httpResponse
                    .Content
                    .CategoryAttributes
                    .Where(x => (x.Type == "Ürün Özelliği" || x.Varianter) && x.AttributeName != "Marka")
                    .GroupBy(x => new { x.AttributeName, x.Required })
                    .Select(x => x.OrderByDescending(x => x.Varianter).First())
                    .Select(ca => new Variant
                    {
                        MarketplaceId = "CS",
                        Code = ca.AttributeId.ToString(),
                        Name = ca.AttributeName,
                        Mandatory = ca.Required || (ca.Varianter && ca.AttributeName.ToLower() == "renk"),
                        AllowCustom = false,
                        MultiValue = false,
                        Variantable = ca.Varianter,
                        CategoryCode = categoryCode,
                        VariantValues = ca
                            .AttributeValues
                            .Select(cav => new VariantValue
                            {
                                VariantCode = ca.AttributeId.ToString(),
                                Code = cav.Id.ToString(),
                                Name = cav.Name,
                                CategoryCodes = new List<string> { categoryCode }
                            })
                            .ToList()
                    }));

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategories(List<CicekSepetiSubCategory> subCategories, List<Category> categories, List<string> parents)
        {
            if (subCategories != null && subCategories.Count > 0)
                foreach (var theSubCategory in subCategories)
                {
                    if (theSubCategory.SubCategories != null && theSubCategory.SubCategories.Count > 0)
                    {
                        var nParents = new List<string>();
                        nParents.AddRange(parents);
                        nParents.Add(theSubCategory.Name);

                        this.FetchSubCategories(theSubCategory.SubCategories, categories, nParents);
                    }
                    else
                        categories.Add(new()
                        {
                            MarketplaceId = "CS",
                            Code = theSubCategory.Id.ToString(),
                            Name = theSubCategory.Name,
                            ParentCode = theSubCategory.ParentCategoryId.ToString(),
                            Breadcrumb = string.Join(" > ", parents)
                        });
                }
        }
        #endregion
    }
}
