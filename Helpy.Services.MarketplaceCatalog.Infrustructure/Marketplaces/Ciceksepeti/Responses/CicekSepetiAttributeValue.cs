﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti.Responses
{
    public class CicekSepetiAttributeValue
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
