﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti.Responses
{
    public class CicekSepetiCategoryAttribute
    {
        #region Properties
        public int AttributeId { get; set; }

        public string AttributeName { get; set; }

        public bool Required { get; set; }

        public bool Varianter { get; set; }

        public string Type { get; set; }

        public List<CicekSepetiAttributeValue> AttributeValues { get; set; }
        #endregion
    }
}
