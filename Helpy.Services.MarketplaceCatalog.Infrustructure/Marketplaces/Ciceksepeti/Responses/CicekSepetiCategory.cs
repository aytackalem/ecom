﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti.Responses
{
    public class CicekSepetiCategoryList
    {
        #region Properties
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public object ParentCategoryId { get; set; }
        
        public List<CicekSepetiSubCategory> SubCategories { get; set; }
        #endregion
    }


    public class CicekSepetiCategory
    {
        #region Properties
        public List<CicekSepetiCategoryList> Categories { get; set; }
        #endregion
    }

    public class CicekSepetiAttributeCategory
    {
        #region Properties
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public List<CicekSepetiCategoryAttribute> CategoryAttributes { get; set; }
        #endregion
    }
}
