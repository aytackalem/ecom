﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.PttAvm
{
    public class PttAvm : IMarketplace
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public PttAvm(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "PTT";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                PttAvmServiceReference.ServiceClient serviceClient = new();
                serviceClient.ClientCredentials.UserName.UserName = "sanalsaat";
                serviceClient.ClientCredentials.UserName.Password = "iF86Hi";

                await serviceClient.OpenAsync();

                var categoryResponseTree = await serviceClient.GetCategoryTreeAsync(string.Empty, string.Empty);

                this.FetchSubCategories(categoryResponseTree.category_tree, data);

                serviceClient.Close();

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategories(PttAvmServiceReference.Category1[] childrens, List<Category> categories)
        {
            if (childrens.Any())
                foreach (var theChildren in childrens)
                {
                    if (theChildren.children.Any())
                        this.FetchSubCategories(theChildren.children, categories);
                    else
                        categories.Add(new()
                        {
                            MarketplaceId = "PTT",
                            Code = theChildren.id,
                            Name = theChildren.name,
                            ParentCode = theChildren.parent_id
                        });
                }
        }
        #endregion
    }
}
