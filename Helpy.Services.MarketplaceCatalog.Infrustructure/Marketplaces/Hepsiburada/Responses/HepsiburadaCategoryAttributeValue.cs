﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaCategoryAttributeValue
    {
        #region Property
        public string Id { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
