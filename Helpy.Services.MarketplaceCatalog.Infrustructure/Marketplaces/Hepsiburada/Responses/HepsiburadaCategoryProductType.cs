﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaCategoryProductType
    {
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
    }
}
