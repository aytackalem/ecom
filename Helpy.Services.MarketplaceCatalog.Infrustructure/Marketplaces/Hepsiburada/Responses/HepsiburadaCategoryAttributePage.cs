﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaCategoryAttributePage
    {
        #region Property
        public bool Success { get; set; }

        public int Code { get; set; }

        public int Version { get; set; }

        public object Message { get; set; }

        public HepsiburadaCategoryAttribute Data { get; set; }
        #endregion
    }
}
