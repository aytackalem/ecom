﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaVariantAttribute
    {
        public HepsiburadaVariantAttribute()
        {
            this.CategoryAttributeValues = new List<HepsiburadaCategoryAttributeValue>();
        }
        #region Property
        public string Name { get; set; }

        public string Id { get; set; }

        public bool Mandatory { get; set; }

        public string Type { get; set; }

        public bool MultiValue { get; set; }

        public int MyProperty { get; set; }

        public List<HepsiburadaCategoryAttributeValue> CategoryAttributeValues { get; set; }

        #endregion
    }
}
