﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaCategory
    {
        public HepsiburadaCategory()
        {
            this.Attributes = new List<HepsiburadaAttribute>();
        }

        #region Properties
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int? ParentCategoryId { get; set; }
        public List<string> Paths { get; set; }
        /// <summary>
        /// Bir kategorinin leaf durumunu iletmektedir. Leaf true: kategoriye ürün açılabilir, leaf false : kategoriye ürün açılamaz.
        /// </summary>
        public bool Leaf { get; set; }

        /// <summary>
        /// İki değeri vardır. “AKTİF” ve “AKTİF DEĞİL”. AKTİF: İlgili kategori aktif olarak kullanımdadır. AKTİF DEĞİL: İlgili kategori kullanımda değil.
        /// </summary>
        public string Status { get; set; }
        public string Type { get; set; }
        public string SortId { get; set; }
        public string ImageURL { get; set; }

        /// <summary>
        /// Bir kategorinin available durumunu iletmektedir. available true: kategoriye ürün açılabilir, available false : kategoriye ürün açılamaz.
        /// </summary>
        public bool Available { get; set; }
        public List<HepsiburadaCategoryProductType> ProductTypes { get; set; }
        #endregion

        public List<HepsiburadaAttribute> Attributes { get; set; }

    }
}
