﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses
{
    public class HepsiburadaCategoryAttribute
    {
        #region Properties
        public List<HepsiburadaBaseAttribute> BaseAttributes { get; set; }

        public List<HepsiburadaAttribute> Attributes { get; set; }

        public List<HepsiburadaVariantAttribute> VariantAttributes { get; set; }
        #endregion
    }
}
