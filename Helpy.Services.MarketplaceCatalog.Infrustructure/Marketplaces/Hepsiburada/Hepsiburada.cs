﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using System.Linq;
using System.Text;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada
{
    public class Hepsiburada : IMarketplace
    {
        #region Fields
        private readonly string _authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes("de84d20b-0a9c-4f00-969c-0dec3c923b7f:xyDgs9fCgRJZ"));

        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Hepsiburada(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "HB";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                Int32 page = 0;
                List<HepsiburadaCategory> hepsiburadaCategories = new();
                HepsiburadaCategoryPage categoryPage;
                do
                {
                    var httpHelperV3Response = await this._httpHelper.SendAsync<HepsiburadaCategoryPage>(
                        new HttpRequest
                        {
                            UseCache = true,
                            HttpMethod = HttpMethod.Get,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = this._authorization,
                            Agent = "helpy_dev",
                            RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/get-all-categories?leaf=true&status=ACTIVE&available=true&page={page}&size=2000&version=1"
                        });

                    categoryPage = httpHelperV3Response.Content;

                    hepsiburadaCategories.AddRange(categoryPage.Data);

                    page++;
                } while (!categoryPage.Last);


                hepsiburadaCategories = hepsiburadaCategories.Where(x => x.Leaf && x.Available && x.Status == "ACTIVE").ToList();

                List<string> newCategories = new List<string>();
                newCategories.Add("Kitap".ToLower());
                newCategories.Add("Kitap Film Müzik".ToLower());
                newCategories.Add("Sesli Kitap".ToLower());
                newCategories.Add("İkinci El Kitap (Sahaf)".ToLower());
                newCategories.Add("Altın / Takı / Mücevher".ToLower());
                newCategories.Add("Külçe, Ziynet, Cumhuriyet Altını".ToLower());
                newCategories.Add("Hesaba Havale Altın".ToLower());
                //newCategories.Add("erkek");
                //newCategories.Add("kadın");
                //newCategories.Add("elbise");
                //newCategories.Add("kazak");
                //newCategories.Add("yelek");
                //newCategories.Add("abiye");
                //newCategories.Add("aksesuar");
                //newCategories.Add("ayakkabı");
                //newCategories.Add("kozmetik");
                //newCategories.Add("krem");
                //newCategories.Add("sneakers");
                //newCategories.Add("giyim / ayakkabı");
                //newCategories.Add("saatler");
                //newCategories.Add("saat/gözlük/aksesuar");
                //newCategories.Add("takı");
                //newCategories.Add("bijuteri");
                //newCategories.Add("güneş gözlüğü");
                //newCategories.Add("ev dekorasyon");
                //newCategories.Add("mobilya");
                //newCategories.Add("oturma odası");
                //newCategories.Add("ofis mobilyaları");
                //newCategories.Add("salon");
                //newCategories.Add("ofis");
                //newCategories.Add("mutfak");
                //newCategories.Add("banyo");

                hepsiburadaCategories = hepsiburadaCategories.Where(x => !x.Paths.Any(y => newCategories.Contains(y.ToLower()))).ToList();
                Console.WriteLine($"Toplam Kategori {hepsiburadaCategories.Count}");

                data.AddRange(hepsiburadaCategories.Select(hc => new Category
                {
                    MarketplaceId = "HB",
                    Code = hc.CategoryId.ToString(),
                    Name = hc.Name,
                    ParentCode = hc.ParentCategoryId.ToString(),
                    Breadcrumb = hc.Paths.Count > 0 ? string.Join(" > ", hc.Paths.GetRange(0, hc.Paths.Count - 1)).TrimEnd('>', ' ') : hc.Paths[0]
                }));

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var attributePageResponse = await this._httpHelper.SendAsync<HepsiburadaCategoryAttributePage>(new HttpRequest
                {
                    UseCache = true,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = this._authorization,
                    Agent = "helpy_dev",
                    RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/{categoryCode}/attributes"
                });

                List<Variant> variants = new List<Variant>();

                variants.AddRange(attributePageResponse.Content.Data.Attributes.Where(a => a.Type != "string").Select(a => new Variant
                {
                    MarketplaceId = "HB",
                    Code = a.Id.ToString(),
                    Name = a.Name,
                    CategoryCode = categoryCode,
                    Mandatory = a.Mandatory,
                    MultiValue = a.MultiValue
                }));

                variants.AddRange(attributePageResponse.Content.Data.VariantAttributes.Where(a => a.Type != "string").Select(a => new Variant
                {
                    MarketplaceId = "HB",
                    Code = a.Id.ToString(),
                    Name = a.Name,
                    CategoryCode = categoryCode,
                    Mandatory = a.Mandatory,
                    MultiValue = a.MultiValue,
                    Variantable = true
                }));

                foreach (var theVariant in variants)
                {
                    if (theVariant.Name == "Sayfa Sayısı[sayfa-sayisi_variant_property]" || theVariant.Name == "Yaprak Sayısı[yaprak-sayisi_variant_property]" || theVariant.Name == "Ek Özellikler")
                    {
                        continue;
                    }

                    Int32 attributeValuePage = 0;
                    HepsiburadaCategoryAttributeValuePage categoryAttributeValuePage;
                    do
                    {
                        var categoryAttributeValuePageResponse = await this
                            ._httpHelper
                            .SendAsync<HepsiburadaCategoryAttributeValuePage>(
                                new HttpRequest
                                {
                                    UseCache = true,
                                    HttpMethod = HttpMethod.Get,
                                    AuthorizationType = AuthorizationType.Basic,
                                    Authorization = this._authorization,
                                    Agent = "helpy_dev",
                                    RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/{categoryCode}/attribute/{theVariant.Code}/values?page={attributeValuePage}&size=1000&version=4"
                                });

                        categoryAttributeValuePage = categoryAttributeValuePageResponse.Content;

                        theVariant.VariantValues ??= new();
                        theVariant.VariantValues.AddRange(categoryAttributeValuePage.Data.Select(d => new VariantValue
                        {
                            Code = d.Id == null ? d.Value : d.Id,
                            Name = d.Value,
                            VariantCode = theVariant.Code,
                            CategoryCodes = new List<string> { categoryCode }
                        }));

                        attributeValuePage++;

                    } while (!categoryAttributeValuePage.Last);
                }

                data.AddRange(variants);

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion
    }
}
