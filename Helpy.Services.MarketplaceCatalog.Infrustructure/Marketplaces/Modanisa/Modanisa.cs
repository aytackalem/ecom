﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using System.Text;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa
{
    public class Modanisa : IMarketplace, IBrandDownloadable
    {
        #region Fields
        private readonly string _authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes("lafabatekstil:r=3kN3%e1"));

        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Modanisa(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "MN";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var getCategoriesResponse = await this
                    ._httpHelper
                    .SendAsync<GetCategoriesResponse>(new HttpRequest
                    {
                        UseCache = true,
                        RequestUri = $"https://marketplace.modanisa.com/api/marketplace/categories",
                        Authorization = this._authorization,
                        AuthorizationType = AuthorizationType.Basic,
                        HttpMethod = HttpMethod.Get
                    });

                getCategoriesResponse.Content.data.categories.ForEach(theCategory =>
                {
                    if (theCategory.sub_categories == null)
                        data.Add(new Category
                        {
                            MarketplaceId = "MN",
                            Code = theCategory.id.ToString(),
                            Name = theCategory.name,
                            ParentCode = theCategory.parent_id.ToString()
                        });
                    else
                        FetchSubCategory(theCategory.sub_categories, data, new List<string> { theCategory.name });
                });

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                #region Attributes
                var getAttributesResponse = await this
                ._httpHelper
                .SendAsync<GetAttributesResponse>(new HttpRequest
                {
                    UseCache = true,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = this._authorization,
                    RequestUri = $"https://marketplace.modanisa.com/api/marketplace/categories/id/{categoryCode}/attributes"
                });

                data.AddRange(getAttributesResponse
                    .Content
                    .data
                    .category
                    .attributes
                    .Select(a => new Variant
                    {
                        MarketplaceId = "MN",
                        Code = a.id.ToString(),
                        Name = a.name,
                        Mandatory = a.mandatory == "true",
                        CategoryCode = categoryCode,
                        VariantValues = a
                            .attribute_values
                            .Select(av => new VariantValue
                            {
                                Code = av.id.ToString(),
                                Name = av.name,
                                VariantCode = a.id.ToString(),
                                CategoryCodes = new List<string> { categoryCode }
                            })
                            .ToList()
                    }));
                #endregion

                #region Variant Attributes
                var getVariantAttributesResponse = await this
                    ._httpHelper
                    .SendAsync<GetVariantAttributesResponse>(new HttpRequest
                    {
                        UseCache = true,
                        RequestUri = $"https://marketplace.modanisa.com/api/marketplace/categories/id/{categoryCode}/variant-attributes",
                        Authorization = this._authorization,
                        AuthorizationType = AuthorizationType.Basic,
                        HttpMethod = HttpMethod.Get
                    });

                data.AddRange(getVariantAttributesResponse
                    .Content
                    .data
                    .category
                    .variant_attributes
                    .Select(a => new Variant
                    {
                        MarketplaceId = "MN",
                        Code = a.id.ToString(),
                        Name = a.name,
                        Mandatory = a.mandatory == "true",
                        CategoryCode = categoryCode,
                        VariantValues = a
                            .attribute_values
                            .GroupBy(av => av.name)
                            .Select(av => new VariantValue
                            {
                                Code = av.Max(av2 => av2.id).ToString(),
                                Name = av.Key,
                                VariantCode = a.id.ToString(),
                                CategoryCodes = new List<string> { categoryCode }
                            })
                            .ToList()
                    }));
                #endregion

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Brand>>> GetBrandsAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async response =>
            {
                List<Brand> brands = new();

                var page = 1;
                var totalPage = 0;

                do
                {
                    var getBrandResponse = await this
                        ._httpHelper
                        .SendAsync<GetBrandResponse>(new HttpRequest
                        {
                            UseCache = false,
                            HttpMethod = HttpMethod.Get,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = this._authorization,
                            RequestUri = $"https://marketplace.modanisa.com/api/marketplace/brands?size=200&page={page}"
                        });

                    if (getBrandResponse.Success)
                    {
                        totalPage = getBrandResponse.Content.data.pagination.total_page;

                        brands.AddRange(getBrandResponse.Content.data.brands.Select(b => new Brand
                        {
                            Code = b.id.ToString(),
                            Name = b.name
                        }));
                    }

                    page++;
                } while (page <= totalPage);

                response.Data = brands;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategory(List<SubCategory> subCategories, List<Category> categories, List<string> parents)
        {
            foreach (var theSubCategory in subCategories)
                if (theSubCategory.sub_categories != null)
                {
                    var nParents = new List<string>();
                    nParents.AddRange(parents);
                    nParents.Add(theSubCategory.name);

                    FetchSubCategory(theSubCategory.sub_categories, categories, nParents);
                }
                else
                    categories.Add(new Category
                    {
                        MarketplaceId = "MN",
                        Code = theSubCategory.id.ToString(),
                        ParentCode = theSubCategory.parent_id.ToString(),
                        Name = theSubCategory.name,
                        Breadcrumb = string.Join(" > ", parents)
                    });
        }
        #endregion
    }
}
