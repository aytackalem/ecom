namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class GetVariantAttributesResponseDataItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<VariantAttribute> variant_attributes { get; set; }
    }

}