﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{
    public class GetBrandResponse
    {
        public bool success { get; set; }
        public GetBrandResponseData data { get; set; }
        public object errors { get; set; }
    }

    public class GetBrandResponseData
    {
        public ModanisaBrand[] brands { get; set; }
        public Pagination pagination { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int total_page { get; set; }
        public int count { get; set; }
        public int limit { get; set; }
    }

    public class ModanisaBrand
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
