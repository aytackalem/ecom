namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{
    public class GetCategoriesDataItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public int parent_id { get; set; }
        public bool fabric_mandatory { get; set; }
        public List<SubCategory> sub_categories { get; set; }
    }

}