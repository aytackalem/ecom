namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class GetCategoriesData
    {
        public List<GetCategoriesDataItem> categories { get; set; }
    }

}