﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{
    public class GetAttributesResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }
}
