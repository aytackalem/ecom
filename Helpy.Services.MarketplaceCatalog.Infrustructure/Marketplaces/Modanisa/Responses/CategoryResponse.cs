using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class CategoryResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<Attribute> attributes { get; set; }
    }

}