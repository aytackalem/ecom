﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{
    public class GetVariantAttributesResponse
    {
        public bool success { get; set; }
        public GetVariantAttributesResponseData data { get; set; }
        public object errors { get; set; }
    }
}
