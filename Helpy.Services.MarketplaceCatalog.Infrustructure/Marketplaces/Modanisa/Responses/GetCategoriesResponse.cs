﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{
    public class GetCategoriesResponse
    {
        public bool success { get; set; }
        public GetCategoriesData data { get; set; }
        public object errors { get; set; }
    }
}
