namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}