namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class GetVariantAttributesResponseData
    {
        public GetVariantAttributesResponseDataItem category { get; set; }
    }

}