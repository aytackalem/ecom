namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa.Responses
{

    public class VariantAttribute
    {
        public int id { get; set; }
        public string name { get; set; }
        public string mandatory { get; set; }
        public List<AttributeValue> attribute_values { get; set; }
    }

}