﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11
{
    public class N11 : IMarketplace
    {
        #region Fields
        private int _counter = 0;

        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public N11(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "N11";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var getTopLevelCategoriesResponse = await this._httpHelper.SendAsync<Responses.GetTopLevelCategories.Envelope>(new HttpSoapRequest
                {
                    UseCache = true,
                    RequestUri = "https://api.n11.com/ws/categoryService",
                    HttpMethod = HttpMethod.Post,
                    CacheKey = "topLevelCategories",
                    Request = $@"
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
    <soapenv:Header/>
    <soapenv:Body>
        <sch:GetTopLevelCategoriesRequest>
            <auth>
                <appKey>2e4fa65b-6797-455f-8fb0-aaa0dd465433</appKey>
                <appSecret>pcGScgM6jdvAtNLE</appSecret>
            </auth>
        </sch:GetTopLevelCategoriesRequest>
    </soapenv:Body>
</soapenv:Envelope>"
                });

                foreach (var theCategory in getTopLevelCategoriesResponse
                    .Content
                    .Body
                    .GetTopLevelCategoriesResponse
                    .CategoryList
                    .Category)
                    await this.FetchSubCategories(theCategory.Id, data);

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var getCategoryAttributesIdResponse = await this._httpHelper.SendAsync<Responses.GetCategoryAttributesId.Envelope>(new HttpSoapRequest
                {
                    UseCache = true,
                    RequestUri = "https://api.n11.com/ws/categoryService",
                    HttpMethod = HttpMethod.Post,
                    CacheKey = $"categoryAttributesId_{categoryCode}",
                    Request = $@"
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
    <soapenv:Header/>
    <soapenv:Body>
        <sch:GetCategoryAttributesIdRequest>
            <auth>
                <appKey>2e4fa65b-6797-455f-8fb0-aaa0dd465433</appKey>
                <appSecret>pcGScgM6jdvAtNLE</appSecret>
            </auth>
            <categoryId>{categoryCode}</categoryId>
        </sch:GetCategoryAttributesIdRequest>
    </soapenv:Body>
</soapenv:Envelope>"
                });

                foreach (var theCategoryProductAttribute in getCategoryAttributesIdResponse
                    .Content
                    .Body
                    .GetCategoryAttributesIdResponse
                    .CategoryProductAttributeList
                    .CategoryProductAttribute)
                {
                    if (theCategoryProductAttribute.Name == "Marka")
                        continue;

                    var variant = new Variant
                    {
                        MarketplaceId = "N11",
                        AllowCustom = false,
                        Mandatory = theCategoryProductAttribute.Mandatory,
                        MultiValue = theCategoryProductAttribute.MultipleSelect,
                        CategoryCode = categoryCode,
                        Code = theCategoryProductAttribute.Id.ToString(),
                        Name = theCategoryProductAttribute.Name
                    };

                    var currentPage = 0;
                    var pageCount = 0;
                    do
                    {
                        var getCategoryAttributeValueResponse = await this._httpHelper.SendAsync<Responses.GetCategoryAttributeValue.Envelope>(new HttpSoapRequest
                        {
                            UseCache = true,
                            RequestUri = "https://api.n11.com/ws/categoryService",
                            HttpMethod = HttpMethod.Post,
                            CacheKey = $"categoryAttributeValue_{categoryCode}_{theCategoryProductAttribute.Id}",
                            Request = $@"
<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
   <env:Header/>
   <env:Body>
      <sch:GetCategoryAttributeValueRequest>
         <auth>
            <appKey>2e4fa65b-6797-455f-8fb0-aaa0dd465433</appKey>
            <appSecret>pcGScgM6jdvAtNLE</appSecret>
         </auth>
        <categoryId>{categoryCode}</categoryId>
         <categoryProductAttributeId>{theCategoryProductAttribute.Id}</categoryProductAttributeId>
         <pagingData>
            <currentPage>{currentPage}</currentPage>
            <pageSize>100</pageSize>
         </pagingData>
      </sch:GetCategoryAttributeValueRequest>
   </env:Body>
</env:Envelope>
"
                        });

                        if (getCategoryAttributeValueResponse.Success == false || getCategoryAttributeValueResponse.Content.Body.GetCategoryAttributeValueResponse.Result.Status == "failure")
                            break;

                        variant.VariantValues ??= new();
                        variant.VariantValues.AddRange(getCategoryAttributeValueResponse.Content.Body.GetCategoryAttributeValueResponse.CategoryProductAttributeValueList.CategoryProductAttributeValue.Select(cpav => new VariantValue
                        {
                            Code = cpav.Name,
                            Name = cpav.Name,
                            VariantCode = variant.Code,
                            CategoryCodes = new List<string> { categoryCode }
                        }));

                        pageCount = getCategoryAttributeValueResponse.Content.Body.GetCategoryAttributeValueResponse.PagingData.PageCount;

                        currentPage++;
                    } while (pageCount > currentPage);

                    data.Add(variant);
                }

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private async Task FetchSubCategories(int categoryId, List<Category> categories)
        {
            var getSubCategoriesResponse = await this._httpHelper.SendAsync<Responses.GetSubCategories.Envelope>(new HttpSoapRequest
            {
                UseCache = true,
                RequestUri = "https://api.n11.com/ws/categoryService",
                HttpMethod = HttpMethod.Post,
                CacheKey = $"SubCategories_{categoryId}",
                Request = $@"
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
    <soapenv:Header/>
    <soapenv:Body>
        <sch:GetSubCategoriesRequest>
            <auth>
                <appKey>2e4fa65b-6797-455f-8fb0-aaa0dd465433</appKey>
                <appSecret>pcGScgM6jdvAtNLE</appSecret>
            </auth>
            <categoryId>{categoryId}</categoryId>
        </sch:GetSubCategoriesRequest>
    </soapenv:Body>
</soapenv:Envelope>"
            });

            if (getSubCategoriesResponse.Success == false || getSubCategoriesResponse.Content.Body.GetSubCategoriesResponse.Result.Status == "failure")
                return;

            if (getSubCategoriesResponse.Content.Body.GetSubCategoriesResponse.Category.SubCategoryList != null)
                foreach (var theSubCategory in getSubCategoriesResponse
                    .Content
                    .Body
                    .GetSubCategoriesResponse
                    .Category
                    .SubCategoryList
                    .SubCategory)
                    await this.FetchSubCategories(theSubCategory.Id, categories);
            else
                categories.Add(new Category
                {
                    MarketplaceId = "N11",
                    Code = getSubCategoriesResponse.Content.Body.GetSubCategoriesResponse.Category.Id.ToString(),
                    Name = getSubCategoriesResponse.Content.Body.GetSubCategoriesResponse.Category.Name,
                    ParentCode = categoryId.ToString()
                });
        }
        #endregion
    }
}
