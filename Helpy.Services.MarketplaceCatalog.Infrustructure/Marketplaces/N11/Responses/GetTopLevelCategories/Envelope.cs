﻿using System.Xml.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11.Responses.GetTopLevelCategories
{
    [XmlRoot(ElementName = "result")]
    public class Result
    {

        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "category")]
    public class Category
    {

        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "categoryList")]
    public class CategoryList
    {

        [XmlElement(ElementName = "category")]
        public List<Category> Category { get; set; }
    }

    [XmlRoot(ElementName = "GetTopLevelCategoriesResponse", Namespace = "http://www.n11.com/ws/schemas")]
    public class GetTopLevelCategoriesResponse
    {

        [XmlElement(ElementName = "result", Namespace = "")]
        public Result Result { get; set; }

        [XmlElement(ElementName = "categoryList", Namespace = "")]
        public CategoryList CategoryList { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "GetTopLevelCategoriesResponse", Namespace = "http://www.n11.com/ws/schemas")]
        public GetTopLevelCategoriesResponse GetTopLevelCategoriesResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {

        [XmlElement(ElementName = "Header")]
        public object Header { get; set; }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
    }


}
