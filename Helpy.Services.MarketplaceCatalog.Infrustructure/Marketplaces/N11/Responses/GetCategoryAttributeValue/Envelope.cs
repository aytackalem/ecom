﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11.Responses.GetCategoryAttributeValue
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Envelope)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "result")]
    public class Result
    {

        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "categoryProductAttributeValue")]
    public class CategoryProductAttributeValue
    {

        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "categoryProductAttributeValueList")]
    public class CategoryProductAttributeValueList
    {

        [XmlElement(ElementName = "categoryProductAttributeValue")]
        public List<CategoryProductAttributeValue> CategoryProductAttributeValue { get; set; }
    }

    [XmlRoot(ElementName = "pagingData")]
    public class PagingData
    {

        [XmlElement(ElementName = "currentPage")]
        public int CurrentPage { get; set; }

        [XmlElement(ElementName = "pageSize")]
        public int PageSize { get; set; }

        [XmlElement(ElementName = "totalCount")]
        public int TotalCount { get; set; }

        [XmlElement(ElementName = "pageCount")]
        public int PageCount { get; set; }
    }

    [XmlRoot(ElementName = "GetCategoryAttributeValueResponse", Namespace = "http://www.n11.com/ws/schemas")]
    public class GetCategoryAttributeValueResponse
    {

        [XmlElement(ElementName = "result", Namespace = "")]
        public Result Result { get; set; }

        [XmlElement(ElementName = "categoryProductAttributeValueList", Namespace = "")]
        public CategoryProductAttributeValueList CategoryProductAttributeValueList { get; set; }

        [XmlElement(ElementName = "pagingData", Namespace = "")]
        public PagingData PagingData { get; set; }
    }

    [XmlRoot(ElementName = "Body")]
    public class Body
    {

        [XmlElement(ElementName = "GetCategoryAttributeValueResponse", Namespace = "http://www.n11.com/ws/schemas")]
        public GetCategoryAttributeValueResponse GetCategoryAttributeValueResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {

        [XmlElement(ElementName = "Header")]
        public object Header { get; set; }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
    }


}
