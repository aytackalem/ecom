﻿using System.Xml.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11.Responses.GetSubCategories
{
    [XmlRoot(ElementName = "result")]
    public class Result
    {

        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "subCategory")]
    public class SubCategory
    {

        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "subCategoryList")]
    public class SubCategoryList
    {

        [XmlElement(ElementName = "subCategory")]
        public List<SubCategory> SubCategory { get; set; }
    }

    [XmlRoot(ElementName = "category")]
    public class Category
    {

        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "subCategoryList")]
        public SubCategoryList SubCategoryList { get; set; }
    }

    [XmlRoot(ElementName = "GetSubCategoriesResponse", Namespace = "http://www.n11.com/ws/schemas")]
    public class GetSubCategoriesResponse
    {

        [XmlElement(ElementName = "result", Namespace = "")]
        public Result Result { get; set; }

        [XmlElement(ElementName = "category", Namespace = "")]
        public Category Category { get; set; }
    }

    [XmlRoot(ElementName = "Body")]
    public class Body
    {

        [XmlElement(ElementName = "GetSubCategoriesResponse", Namespace = "http://www.n11.com/ws/schemas")]
        public GetSubCategoriesResponse GetSubCategoriesResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {

        [XmlElement(ElementName = "Header")]
        public object Header { get; set; }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
    }


}
