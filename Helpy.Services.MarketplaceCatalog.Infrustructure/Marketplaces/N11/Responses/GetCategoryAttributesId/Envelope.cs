﻿using System.Xml.Serialization;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11.Responses.GetCategoryAttributesId
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Envelope)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "result")]
    public class Result
    {

        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "categoryProductAttribute")]
    public class CategoryProductAttribute
    {

        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "mandatory")]
        public bool Mandatory { get; set; }

        [XmlElement(ElementName = "multipleSelect")]
        public bool MultipleSelect { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "categoryProductAttributeList")]
    public class CategoryProductAttributeList
    {

        [XmlElement(ElementName = "categoryProductAttribute")]
        public List<CategoryProductAttribute> CategoryProductAttribute { get; set; }
    }

    [XmlRoot(ElementName = "GetCategoryAttributesIdResponse", Namespace = "http://www.n11.com/ws/schemas")]
    public class GetCategoryAttributesIdResponse
    {

        [XmlElement(ElementName = "result", Namespace = "")]
        public Result Result { get; set; }

        [XmlElement(ElementName = "categoryProductAttributeList", Namespace = "")]
        public CategoryProductAttributeList CategoryProductAttributeList { get; set; }
    }

    [XmlRoot(ElementName = "Body")]
    public class Body
    {

        [XmlElement(ElementName = "GetCategoryAttributesIdResponse", Namespace = "http://www.n11.com/ws/schemas")]
        public GetCategoryAttributesIdResponse GetCategoryAttributesIdResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {

        [XmlElement(ElementName = "Header")]
        public object Header { get; set; }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
    }


}
