﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2.Responses
{
    public class CategoryAttribute
    {
        public int AttributeId { get; set; }
        public int CategoryId { get; set; }
        public string AttributeName { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsVariant { get; set; }
        public bool IsSlicer { get; set; }
        public bool IsCustomValue { get; set; }
        public bool IsN11Grouping { get; set; }
        public int AttributeOrder { get; set; }
        public List<AttributeValue> AttributeValues { get; set; }
    }
}
