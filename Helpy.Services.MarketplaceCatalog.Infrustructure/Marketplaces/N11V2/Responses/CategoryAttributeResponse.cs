﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2.Responses
{
    public class CategoryAttributeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryAttribute> CategoryAttributes { get; set; }
    }
}
