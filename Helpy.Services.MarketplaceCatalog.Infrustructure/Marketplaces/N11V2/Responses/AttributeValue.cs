﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2.Responses
{
    public class AttributeValue
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
