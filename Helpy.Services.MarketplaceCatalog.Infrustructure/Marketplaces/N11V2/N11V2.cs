﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2
{
    public class N11V2 : IMarketplace
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public N11V2(IHttpHelper httpHelper)
        {
            #region Fields
            _httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "N11V2";
        #endregion

        #region Methods

        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var customHeaders = new Dictionary<string, string> {
                    { "appkey", "2e4fa65b-6797-455f-8fb0-aaa0dd465433" }
                    };

                var categoryResponse = await _httpHelper.SendAsync<CategoryResponse>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = "https://api.n11.com/cdn/categories",
                    HttpMethod = HttpMethod.Get,
                    Headers = customHeaders
                });

                foreach (var cLoop in categoryResponse.Content.Categories)
                {

                    var nParents = new List<string>();
                    nParents.Add(cLoop.Name);

                    FetchSubCategories(cLoop.SubCategories, data, nParents);

                }

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();
                var customHeaders = new Dictionary<string, string> {
                    { "appkey", "2e4fa65b-6797-455f-8fb0-aaa0dd465433" }
                    };

                var categoryAttributeResponse = await _httpHelper.SendAsync<CategoryAttributeResponse>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 5,
                    UseCache = true,
                    RequestUri = $"https://api.n11.com/cdn/category/{categoryCode}/attribute",
                    HttpMethod = HttpMethod.Get,
                    Headers = customHeaders
                });

                data.AddRange(categoryAttributeResponse
                    .Content
                    .CategoryAttributes
                    .Select(ca => new Variant
                    {
                        MarketplaceId = "N11V2",
                        CategoryCode = categoryCode,
                        Code = ca.AttributeId.ToString(),
                        Name = ca.AttributeName,
                        AllowCustom = ca
                            .AttributeValues.Count > 0 ? false : ca.IsCustomValue,
                        Mandatory = ca.IsMandatory,
                        Variantable = ca.IsVariant,
                        VariantValues = ca
                            .AttributeValues
                            .Select(av => new VariantValue
                            {
                                VariantCode = ca.AttributeId.ToString(),
                                Code = av.Id.ToString(),
                                Name = av.Value,
                                CategoryCodes = new List<string> { categoryCode }
                            })
                            .ToList()
                    }));

                response.Data = data;
                response.Success = true;
            });
        }


        #endregion

        #region Helper Methods
        private void FetchSubCategories(List<SubCategory> subCategories, List<Category> categories, List<string> parents)
        {
            if (subCategories != null && subCategories.Count > 0)
                foreach (var theSubCategory in subCategories)
                {
                    if (theSubCategory.ParentId.HasValue == false)
                        parents = new();

                    if (theSubCategory.SubCategories != null && theSubCategory.SubCategories.Count > 0)
                    {
                        var nParents = new List<string>();
                        nParents.AddRange(parents);
                        nParents.Add(theSubCategory.Name);

                        FetchSubCategories(theSubCategory.SubCategories, categories, nParents);
                    }
                    else
                        categories.Add(new()
                        {
                            MarketplaceId = "N11V2",
                            Code = theSubCategory.Id.ToString(),
                            Name = theSubCategory.Name,
                            ParentCode = theSubCategory.ParentId.ToString(),
                            Breadcrumb = string.Join(" > ", parents).TrimEnd('>', ' ')
                        });
                }
        }
        #endregion

    }
}
