﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class CategoryAttributeResponse
    {
        #region Properties
        public CategoryAttribute data { get; set; }
        
        public bool success { get; set; }
        
        public object messageCode { get; set; }
        
        public object message { get; set; }
        
        public object userMessage { get; set; } 
        #endregion
    }
}
