﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class AttributeValue
    {
        #region Properties
        public string id { get; set; }
        
        public string value { get; set; } 
        #endregion
    }
}
