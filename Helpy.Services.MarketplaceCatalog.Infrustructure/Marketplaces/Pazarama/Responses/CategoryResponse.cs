﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class CategoryResponse
    {
        #region Navigation Properties
        public List<CategoryResponseItem> data { get; set; }
        #endregion
    }
}
