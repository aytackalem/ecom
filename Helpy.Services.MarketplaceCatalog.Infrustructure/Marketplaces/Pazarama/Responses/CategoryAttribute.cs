﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class CategoryAttribute
    {
        #region Properties
        public string id { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public List<Attribute> attributes { get; set; } 
        #endregion
    }
}
