﻿using System.Collections.Generic;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class Attribute
    {
        #region Properties
        public string id { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public bool isVariantable { get; set; }
        
        public bool isRequired { get; set; }
        
        public List<AttributeValue> attributeValues { get; set; } 
        #endregion
    }
}
