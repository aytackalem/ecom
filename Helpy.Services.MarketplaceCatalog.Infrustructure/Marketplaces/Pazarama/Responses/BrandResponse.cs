﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class BrandResponse
    {
        public List<BrandData> data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
        public bool fromCache { get; set; }
    }

    public class BrandData
    {
        public string id { get; set; }
        public string name { get; set; }
        public object logoUrl { get; set; }
        public object website { get; set; }
        public bool status { get; set; }
        public object seoName { get; set; }
    }
}
