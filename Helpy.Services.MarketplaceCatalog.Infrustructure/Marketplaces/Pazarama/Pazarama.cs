﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama
{
    public class Pazarama : IMarketplace, IBrandSearchable
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Pazarama(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "PA";
        #endregion

        #region Methods
        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var categoryResponse = await this._httpHelper.SendAsync<CategoryResponse>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = "https://isortagimapi.pazarama.com/category/getCategoryTree",
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(categoryResponse.Content.data.Where(d => d.leaf).Select(d => new Category
                {
                    MarketplaceId = "PA",
                    Code = d.id,
                    Name = d.name,
                    ParentCode = d.parentId,
                    Breadcrumb = string.Join(" > ", d.parentCategories)
                }));

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var categoryAttributeResponse = await this._httpHelper.SendAsync<CategoryAttributeResponse>(new HttpRequest
                {
                    TryCount = 3,
                    TryWaitSeconds = 3,
                    UseCache = false,
                    RequestUri = $"https://isortagimapi.pazarama.com/category/getCategoryWithAttributes?Id={categoryCode}",
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(categoryAttributeResponse.Content.data.attributes.Select(a => new Variant
                {
                    MarketplaceId = "PA",
                    Code = a.id,
                    Name = a.name,
                    Mandatory = a.isRequired,
                    AllowCustom = false,
                    CategoryCode = categoryCode,
                    MultiValue = false,
                    VariantValues = a.attributeValues.Select(av => new VariantValue
                    {
                        Code = av.id,
                        Name = av.value,
                        VariantCode = a.id,
                        CategoryCodes = new List<string> { categoryCode }
                    }).ToList()
                }));

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Brand>>> SearchBrandByNameAsync(string name)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async (response) =>
            {
                List<Brand> data = new();

                var brandResponse = await this._httpHelper.SendAsync<BrandResponse>(new HttpRequest
                {
                    RequestUri = $"https://isortagimapi.pazarama.com/brand/getBrands?name={name}",
                    HttpMethod = HttpMethod.Get
                });

                data.AddRange(
                    brandResponse
                    .Content
                    .data
                    .Select(x => new Brand
                    {
                        Code = x.id,
                        Name = x.name
                    }
               ).ToList());

                response.Data = data;
                response.Success = true;
            });
        }
        #endregion
    }
}
