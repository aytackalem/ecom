﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner.Brands
{
    internal class BoynerBrand
    {
        [JsonProperty("brands")]
        public List<BoynerBrandItem> Brands { get; set; }

    }
    public class BoynerBrandItem
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
