﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner.CategoryAttributes
{
    public class BoynerCategoryAttributes
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("categoryAttributes")]
        public List<BoynerCategoryAttribute> CategoryAttributes { get; set; }
    }

    public class BoynerCategoryAttribute
    {
        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("allowCustom")]
        public bool AllowCustom { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("varianter")]
        public bool Varianter { get; set; }

        [JsonProperty("slicer")]
        public bool Slicer { get; set; }

        [JsonProperty("attribute")]
        public BoynerAttribute Attribute { get; set; }

        [JsonProperty("attributeValues")]
        public List<BoynerAttributeValue> AttributeValues { get; set; }
    }


    public class BoynerAttribute
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("attributeValues")]
        public List<BoynerAttributeValue> AttributeValues { get; set; }
    }

    public class BoynerAttributeValue
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
