﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner.Brands;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner.Categories;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner.CategoryAttributes;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Newtonsoft.Json;
using System.Text;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner;

public class Boyner : IMarketplace, IBrandDownloadable
{
    #region Fields
    private readonly string _authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes("EMy359:23ded5f6-abcb-47f5-a141-de25a9d2f912"));

    private readonly IHttpHelper _httpHelper;
    #endregion

    #region Constructors
    public Boyner(IHttpHelper httpHelper)
    {
        #region Fields
        this._httpHelper = httpHelper;
        #endregion
    }
    #endregion

    #region Properties
    public string Id => "BY";
    #endregion

    #region Methods
    public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
        {
            List<BoynerCategory> boynerCategories = new();

            var httpHelperV3Response = await this._httpHelper.SendAsync<List<BoynerCategory>>(new HttpRequest
            {
                UseCache = false,
                RequestUri = $"https://merchantapi.boyner.com.tr/sapigw/product-categories",
                HttpMethod = HttpMethod.Get,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = _authorization,
                Headers = new Dictionary<string, string>
                {
                    {"User-Agent","2008341502 - Helpy" }
                }
            });



            List<Category> categories = new();

            foreach (var bLoop in httpHelperV3Response.Content)
            {

                this.FetchSubCategories(bLoop.SubCategories, categories, new List<string>() { bLoop.Name });

            }



            response.Data = categories;
            response.Success = true;
        });
    }

    public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
        {


            var httpHelperV3Response = await this._httpHelper.SendAsync<BoynerCategoryAttributes>(new HttpRequest
            {
                UseCache = false,
                RequestUri = $"https://merchantapi.boyner.com.tr/sapigw/product-categories/{categoryCode}/attributes",
                HttpMethod = HttpMethod.Get,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = _authorization,
                Headers = new Dictionary<string, string>
                {
                    {"User-Agent","2008341502 - Helpy" }
                }
            });

            List<Variant> data = new();

            data.AddRange(httpHelperV3Response
           .Content
           .CategoryAttributes
           .Select(ca => new Variant
           {
               MarketplaceId = "BY",
               CategoryCode = categoryCode,
               Code = ca.Attribute.Id.ToString(),
               Name = ca.Attribute.Name,
               AllowCustom = ca
                   .AttributeValues.Count > 0 ? false : ca.AllowCustom,
               Mandatory = ca.Required,
               Variantable = ca.Varianter,
               VariantValues = ca
               .Attribute
               .AttributeValues
                   .Select(av => new VariantValue
                   {
                       VariantCode = ca.Attribute.Id.ToString(),
                       Code = av.Id.ToString(),
                       Name = av.Name,
                       CategoryCodes = new List<string> { categoryCode }
                   })
                   .ToList()
           }));


            response.Data = data;
            response.Success = true;
        });
    }

    public async Task<DataResponse<List<Brand>>> GetBrandsAsync()
    {
        return await ExceptionHandler.HandleAsync<DataResponse<List<Brand>>>(async (response) =>
        {
            var page = 0;
            List<BoynerBrandItem> boynerBrandItems = new();

            do
            {
                var httpHelperV3Response = await this._httpHelper.SendAsync<BoynerBrand>(new HttpRequest
                {
                    UseCache = false,
                    RequestUri = $"https://merchantapi.boyner.com.tr/sapigw/brands?Page={page}&Size=500",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = _authorization,
                    Headers = new Dictionary<string, string>
                {
                    {"User-Agent","2008341502 - Helpy" }
                }
                });

                if (httpHelperV3Response.Content != null && httpHelperV3Response.Content.Brands.Count == 0) break;


                boynerBrandItems.AddRange(httpHelperV3Response.Content.Brands);

                page++;

            }
            while (true);

            response.Data = boynerBrandItems
                .Select(_ => new Brand
                {
                    Code = _.Id.ToString(),
                    Name = _.Name
                })
                .ToList();

            response.Success = true;

        });
    }
    #endregion

    #region Helper Methods
    private void FetchSubCategories(List<BoynerSubCategory> subCategories, List<Category> categories, List<string> parents)
    {
        if (subCategories != null && subCategories.Count > 0)
            foreach (var theSubCategory in subCategories)
            {
                if (theSubCategory.ParentId.HasValue == false)
                    parents = new();

                if (theSubCategory.SubCategories != null && theSubCategory.SubCategories.Count > 0)
                {
                    var nParents = new List<string>();
                    nParents.AddRange(parents);
                    nParents.Add(theSubCategory.Name);

                    this.FetchSubCategories(theSubCategory.SubCategories, categories, nParents);
                }
                else
                    categories.Add(new()
                    {
                        MarketplaceId = "BY",
                        Code = theSubCategory.Id.ToString(),
                        Name = theSubCategory.Name,
                        ParentCode = theSubCategory.ParentId.ToString(),
                        Breadcrumb = string.Join(" > ", parents).TrimEnd('>', ' ')
                    });
            }
    }
    #endregion
}
