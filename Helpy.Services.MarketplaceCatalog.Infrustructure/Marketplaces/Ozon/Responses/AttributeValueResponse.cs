﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Responses
{
    public class AttributeValueResponse
    {
        public AttributeValue[] result { get; set; }
        public bool has_next { get; set; }
    }

    public class AttributeValue
    {
        public int id { get; set; }
        public string value { get; set; }
        public string info { get; set; }
        public string picture { get; set; }
    }

}
