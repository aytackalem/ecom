﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Responses
{
    public class CategoryResponse
    {
        public List<Item> result { get; set; }
    }

    public class Item
    {
        public int category_id { get; set; }
        public string title { get; set; }
        public List<Item> children { get; set; }
    }
}
