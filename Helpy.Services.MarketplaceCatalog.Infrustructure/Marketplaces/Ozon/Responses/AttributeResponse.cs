﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Responses
{
    public class AttributeResponse
    {
        public Result[] result { get; set; }
    }

    public class Result
    {
        public int category_id { get; set; }
        public Attribute[] attributes { get; set; }
    }

    public class Attribute
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public bool is_collection { get; set; }
        public bool is_required { get; set; }
        public int group_id { get; set; }
        public string group_name { get; set; }
        public int dictionary_id { get; set; }
        public bool is_aspect { get; set; }
        public bool category_dependent { get; set; }
    }
}
