﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Requests;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Responses;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon
{
    public class Ozon : IMarketplace
    {
        public string Id => "OZ";

        private readonly IHttpHelper _httpHelper;

        public Ozon(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
        }

        public async Task<DataResponse<List<Category>>> GetCategoriesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Category>>>(async (response) =>
            {
                List<Category> data = new();

                var categoryResponse = await this._httpHelper.SendAsync<CategoryRequest, CategoryResponse>(new HttpRequest<CategoryRequest>
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "Client-Id", "306126" },
                        { "Api-Key", "1b463067-0ea9-4e8b-b540-62e93a0d8b88" }
                    },
                    Request = new()
                    {
                        language = "TR"
                    },
                    UseCache = false,
                    RequestUri = "https://api-seller.ozon.ru/v2/category/tree",
                    HttpMethod = HttpMethod.Post
                });

                this.FetchSubCategories(categoryResponse.Content.result.Where(r => r.title == "Giysi").ToList(), data, null, null);

                response.Data = data;
                response.Success = true;
            });
        }

        public async Task<DataResponse<List<Variant>>> GetVariantsAsync(string categoryCode)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Variant>>>(async (response) =>
            {
                List<Variant> data = new();

                var attributeResponse = await this
                    ._httpHelper
                    .SendAsync<AttributeRequest, AttributeResponse>(
                        new HttpRequest<AttributeRequest>
                        {
                            Headers = new Dictionary<string, string>
                            {
                                { "Client-Id", "306126" },
                                { "Api-Key", "1b463067-0ea9-4e8b-b540-62e93a0d8b88" }
                            },
                            Request = new()
                            {
                                attribute_type = "ALL",
                                category_id = new int[] { int.Parse(categoryCode) },
                                language = "TR"
                            },
                            TryCount = 3,
                            TryWaitSeconds = 5,
                            UseCache = false,
                            RequestUri = $"https://api-seller.ozon.ru/v3/category/attribute",
                            HttpMethod = HttpMethod.Post
                        });

                if (attributeResponse.Success)
                    foreach (var theAttribute in attributeResponse.Content.result[0].attributes)
                    {
                        var attributeValues = new List<Responses.AttributeValue>();
                        if (theAttribute.dictionary_id > 0)
                        {
                            var hasNext = true;
                            var lastValueId = 0;
                            do
                            {
                                var attributeValueResponse = await this
                                    ._httpHelper
                                    .SendAsync<AttributeValueRequest, AttributeValueResponse>(
                                        new HttpRequest<AttributeValueRequest>
                                        {
                                            Headers = new Dictionary<string, string>
                                            {
                                            { "Client-Id", "306126" },
                                            { "Api-Key", "1b463067-0ea9-4e8b-b540-62e93a0d8b88" }
                                            },
                                            Request = new()
                                            {
                                                attribute_id = theAttribute.id,
                                                category_id = attributeResponse.Content.result[0].category_id,
                                                language = "TR",
                                                last_value_id = lastValueId,
                                                limit = 5000
                                            },
                                            TryCount = 3,
                                            TryWaitSeconds = 5,
                                            UseCache = false,
                                            RequestUri = $"https://api-seller.ozon.ru/v2/category/attribute/values",
                                            HttpMethod = HttpMethod.Post
                                        });

                                attributeValues.AddRange(attributeValueResponse.Content.result);

                                hasNext = attributeValueResponse.Content.has_next;
                                lastValueId = attributeValueResponse.Content.result.Max(r => r.id);
                            } while (hasNext);
                        }

                        data.Add(new Variant
                        {
                            MarketplaceId = this.Id,
                            Code = theAttribute.id.ToString(),
                            Name = theAttribute.name,
                            Mandatory = theAttribute.is_required,
                            AllowCustom = false,
                            CategoryCode = categoryCode,
                            MultiValue = false,
                            VariantValues = theAttribute.name == "Cinsiyet"
                                ? attributeValues
                                    .Where(av => av.value.Contains("Lafaba"))
                                    .Select(av => new VariantValue
                                    {
                                        Code = av.id.ToString(),
                                        Name = av.value,
                                        VariantCode = theAttribute.id.ToString(),
                                        CategoryCodes = new List<string> { categoryCode }
                                    })
                                    .ToList()
                                : attributeValues
                                    .Select(av => new VariantValue
                                    {
                                        Code = av.id.ToString(),
                                        Name = av.value,
                                        VariantCode = theAttribute.id.ToString(),
                                        CategoryCodes = new List<string> { categoryCode }
                                    })
                                    .ToList()
                        });
                    }

                response.Data = data;
                response.Success = true;
            });
        }

        private void FetchSubCategories(List<Item> items, List<Category> categories, int? parentId, List<string> parents)
        {
            if (items?.Count > 0)
                foreach (var theItem in items)
                {
                    if (theItem.children?.Count > 0)
                    {
                        parents ??= new();

                        List<string> nParents = new();
                        nParents.AddRange(parents);
                        nParents.Add(theItem.title);

                        this.FetchSubCategories(theItem.children, categories, theItem.category_id, nParents);
                    }
                    else
                    {
                        if (theItem.title == "Kadın tunik")
                            categories.Add(new()
                            {
                                MarketplaceId = this.Id,
                                Code = theItem.category_id.ToString(),
                                Name = theItem.title,
                                ParentCode = parentId?.ToString(),
                                Breadcrumb = string.Join(" > ", parents)
                            });
                    }
                }
        }
    }
}
