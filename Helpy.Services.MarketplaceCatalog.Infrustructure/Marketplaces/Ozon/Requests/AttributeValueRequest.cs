﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Requests
{
    public class AttributeValueRequest
    {
        public int attribute_id { get; set; }
        public int category_id { get; set; }
        public string language { get; set; }
        public int last_value_id { get; set; }
        public int limit { get; set; }
    }

}
