﻿namespace Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon.Requests
{
    public class AttributeRequest
    {
        public string attribute_type { get; set; }

        public int[] category_id { get; set; }

        public string language { get; set; }
    }
}
