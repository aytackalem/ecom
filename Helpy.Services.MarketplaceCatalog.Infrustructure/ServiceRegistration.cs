﻿using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces.Resolvers;
using Helpy.Services.MarketplaceCatalog.Infrustructure;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Boyner;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ciceksepeti;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Hepsiburada;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Lcw;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modalog;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Modanisa;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Morhipo;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.N11V2;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Ozon;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Pazarama;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.PttAvm;
using Helpy.Services.MarketplaceCatalog.Infrustructure.Marketplaces.Trendyol;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Microsoft.Extensions.DependencyInjection;

namespace Helpy.Services.MarketplaceCatalog.Persistence
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddInfrustructureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddAutoMapper(a => a.AddProfile(new InfrustructureMapping()));

            serviceCollection.AddScoped<IMarketplace, Trendyol>();
            serviceCollection.AddScoped<IMarketplace, Hepsiburada>();
            serviceCollection.AddScoped<IMarketplace, Ciceksepeti>();
            serviceCollection.AddScoped<IMarketplace, Pazarama>();
            serviceCollection.AddScoped<IMarketplace, Modalog>();
            serviceCollection.AddScoped<IMarketplace, N11>();
            serviceCollection.AddScoped<IMarketplace, PttAvm>();
            serviceCollection.AddScoped<IMarketplace, Modanisa>();
            serviceCollection.AddScoped<IMarketplace, Morhipo>();
            serviceCollection.AddScoped<IMarketplace, Ozon>();
            serviceCollection.AddScoped<IMarketplace, N11V2>();
            serviceCollection.AddScoped<IMarketplace, Lcw>();
            serviceCollection.AddScoped<IMarketplace, Boyner>();

            serviceCollection.AddScoped<IBrandDownloadable, Modalog>();
            serviceCollection.AddScoped<IBrandDownloadable, Modanisa>();
            serviceCollection.AddScoped<IBrandDownloadable, Lcw>();
            serviceCollection.AddScoped<IBrandDownloadable, Boyner>();

            serviceCollection.AddScoped<Trendyol>();
            serviceCollection.AddScoped<Pazarama>();
            serviceCollection.AddTransient<BrandSearchableResolver>(serviceProvider => marketplaceId =>
            {
                IBrandSearchable p = marketplaceId switch
                {
                    "TY" => serviceProvider.GetService<Trendyol>(),
                    "PA" => serviceProvider.GetService<Pazarama>(),
                    _ => null
                };
                return p;
            });
        }
        #endregion
    }
}
