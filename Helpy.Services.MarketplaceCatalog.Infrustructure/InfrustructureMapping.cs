﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.Wrappers.Marketplaces;

namespace Helpy.Services.MarketplaceCatalog.Infrustructure
{
    public class InfrustructureMapping : Profile
    {
        #region Constructors
        public InfrustructureMapping()
        {
            CreateMap<Category, Core.Application.DataTransferObjects.Category>();
            CreateMap<Variant, Core.Application.DataTransferObjects.Variant>();
            CreateMap<VariantValue, Core.Application.DataTransferObjects.VariantValue>();
            CreateMap<Brand, Core.Application.DataTransferObjects.Brand>();
            CreateMap<Brand, Core.Application.Wrappers.Services.BrandService.Brand>();
        }
        #endregion
    }
}
