import React from 'react';
import store from './src/redux/store';
import { Provider } from 'react-redux';
import Startup from './Startup';

const App = () => {
  return (
    <Provider store={store}>

      <Startup />

    </Provider>
  );

};

export default App;