import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from './src/screens/main.screen';
import PickingScreen from './src/screens/picking.screen';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateModal } from './src/redux/modal.slice';
import ScreenContainerComponent from './src/components/screen.container.component';
import LoginScreen from './src/screens/login.screen';
import StocktakingScreen from './src/screens/stocktaking.screen';
import StocktakingDetailScreen from './src/screens/stocktaking.detail.screen';
import ProductInformationScreen from './src/screens/product.information.screen';

const Stack = createNativeStackNavigator();

const Startup = () => {
  const token = useSelector((state) => state.LoginSlice.token);

  const preloaderIsOpen = useSelector((state) => state.PreloaderSlice.IsOpen);

  const modalIsOpen = useSelector((state) => state.ModalSlice.IsOpen);

  const modalText = useSelector((state) => state.ModalSlice.Text);

  const _dispatch = useDispatch();


  return (
    <ScreenContainerComponent preloaderIsOpen={preloaderIsOpen} modalIsOpen={modalIsOpen} modalText={modalText} modalClose={() => _dispatch(UpdateModal({ IsOpen: false, Text: null }))}>

      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          {
            token == null && <Stack.Screen name="Login" component={LoginScreen} />
          }
          <Stack.Screen name="Main" component={MainScreen} />
          <Stack.Screen name="Picking" component={PickingScreen} />
          <Stack.Screen name="Stocktaking" component={StocktakingScreen} />
          <Stack.Screen name="StocktakingDetail" component={StocktakingDetailScreen} />
          <Stack.Screen name="ProductInformation" component={ProductInformationScreen} />
        </Stack.Navigator>
      </NavigationContainer>

    </ScreenContainerComponent>
  );

};

export default Startup;