import { createSlice } from '@reduxjs/toolkit';

const ModalSlice = createSlice({
  name: 'modal',
  initialState: {
    IsOpen: false,
    Text: null
  },
  reducers: {
    UpdateModal: (state, action) => {
      state.IsOpen = action.payload.IsOpen;
      state.Text = action.payload.Text;
    }
  }
});

export const { UpdateModal } = ModalSlice.actions;

export default ModalSlice.reducer;