import { createSlice } from '@reduxjs/toolkit';

const LoginSlice = createSlice({
  name: 'loginSlice',
  initialState: {
    username: null,
    password: null,
    token: null,
    refreshToken: null,
    domains: null
  },
  reducers: {
    WriteManagerUser: (state, action) => {
      state.username = action.payload.username;
      state.password = action.payload.password;
      state.token = action.payload.token;
      state.refreshToken = action.payload.refreshToken;
      state.domains = action.payload.domains;
    },
    DeleteManagerUser: state => {
      state.username = null;
      state.password = null;
      state.token = null;
      state.refreshToken = null;
      state.domains = null;
    }
  }
});

export const { WriteManagerUser, DeleteManagerUser } = LoginSlice.actions;

export default LoginSlice.reducer;