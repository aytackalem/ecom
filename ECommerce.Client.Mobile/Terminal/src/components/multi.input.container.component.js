import React from 'react';
import { View } from "react-native";
import MultiInputContainerComponentTypes from '../enums/multi.input.container.component.types';
import MultiInputContainerComponentStyle from '../styles/components/multi.input.container.component.style';

const MultiInputContainerComponent = (props) => {
    const RenderStyle = () => {
        if (props.type == MultiInputContainerComponentTypes.Left) {
            return MultiInputContainerComponentStyle.Left;
        }
        else if (props.type == MultiInputContainerComponentTypes.SpaceBetween) {
            return MultiInputContainerComponentStyle.SpaceBetween;
        }
    }

    return (
        <View style={[MultiInputContainerComponentStyle.Container, RenderStyle()]}>
            {props.children}
        </View>
    )
}

export default MultiInputContainerComponent;