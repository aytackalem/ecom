import React from 'react';
import { View } from "react-native";
import InputContainerComponentStyle from '../styles/components/input.container.component.style';

const InputContainerComponent = (props) => {
    return (
        <View style={InputContainerComponentStyle.Container}>
            {props.children}
        </View>
    )
}

export default InputContainerComponent;