import React from "react";
import { Text } from "react-native";
import HeadingComponentStyle from "../styles/components/heading.component.style";

const HeadingComponent = (props) => {
    return (
        <Text style={props.style}>
            {props.text}
        </Text>
    )
}

export default HeadingComponent;