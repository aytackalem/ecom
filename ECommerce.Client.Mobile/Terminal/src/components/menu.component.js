import React, { useEffect, useState } from 'react';
import { Image, Text, View } from "react-native"
import MenuComponentStyle from "../styles/components/menu.component.style"
import { TouchableOpacity } from 'react-native';

const MenuComponent = (props) => {
    return (
        <View style={[MenuComponentStyle.Container, MenuComponentStyle.SpaceBetween]}>

            <TouchableOpacity onPress={() => {
                props.navigation.navigate('Main')
            }}>
                <Text style={[MenuComponentStyle.Text, props.name == "Main" ? MenuComponentStyle.TextActive : null]}>Sipariş</Text>
                <View style={[MenuComponentStyle.Button, props.name == "Main" ? MenuComponentStyle.ButtonActive : null]}>
                    <Image source={props.name == "Main" ? require("../images/miscellaneous/basket-white.png") : require("../images/miscellaneous/basket.png")} style={[MenuComponentStyle.Icon]} />
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                props.navigation.navigate('Stocktaking')
            }}>
                <Text style={[MenuComponentStyle.Text, props.name == "Stocktaking" ? MenuComponentStyle.TextActive : null]}>Sayım</Text>
                <View style={[MenuComponentStyle.Button, props.name == "Stocktaking" ? MenuComponentStyle.ButtonActive : null]}>
                    <Image source={props.name == "Stocktaking" ? require("../images/miscellaneous/barcode-white.png") : require("../images/miscellaneous/barcode.png")} style={[MenuComponentStyle.Icon]} />
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                props.navigation.navigate('ProductInformation')
            }}>
                <Text style={[MenuComponentStyle.Text, props.name == "ProductInformation" ? MenuComponentStyle.TextActive : null]}>Ürün Bilgi</Text>
                <View style={[MenuComponentStyle.Button, props.name == "ProductInformation" ? MenuComponentStyle.ButtonActive : null]}>
                    <Image source={props.name == "ProductInformation" ? require("../images/miscellaneous/label-white.png") : require("../images/miscellaneous/label.png")} style={[MenuComponentStyle.Icon]} />
                </View>
            </TouchableOpacity>

        </View>
    )
}

export default MenuComponent