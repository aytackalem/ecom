import React from "react";
import { Text, View } from "react-native";
import ConfirmComponentStyle from "../styles/components/confirm.component.style";
import PaddingContainerComponent from "./padding.container.component";
import PaddingContainerComponentTypes from "../enums/padding.container.component.types";
import ButtonComponent from "./button.component";
import ButtonComponentTypes from "../enums/button.component.types";
import MultiInputContainerComponentStyle from "../styles/components/multi.input.container.component.style";
import MultiInputContainerComponent from "./multi.input.container.component";
import MultiInputContainerComponentTypes from "../enums/multi.input.container.component.types";
import HeadingComponent from "./heading.component";
import HeadingComponentStyle from "../styles/components/heading.component.style";

const ConfirmComponent = (props) => {
    return (
        <View style={ConfirmComponentStyle.Container}>
            <View style={ConfirmComponentStyle.InnerContainer}>
                <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                    <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>

                        <HeadingComponent text={props.title} style={HeadingComponentStyle.H1} />
                        <HeadingComponent text={props.message} style={HeadingComponentStyle.H4} />

                    </PaddingContainerComponent>

                    <MultiInputContainerComponent type={MultiInputContainerComponentTypes.SpaceBetween}>

                        <ButtonComponent type={ButtonComponentTypes.Default} onPress={() => props.no()} text="Vazgeç" />
                        <ButtonComponent type={ButtonComponentTypes.Info} onPress={() => props.yes()} text="Evet" />

                    </MultiInputContainerComponent>
                </PaddingContainerComponent>
            </View>
        </View>
    )
}

export default ConfirmComponent;