import React from "react";
import { TextInput } from "react-native";
import TextBoxComponentStyle from "../styles/components/textbox.component.style";

const TextBoxComponent = (props) => {
    return (
        <TextInput
            secureTextEntry={props.secureTextEntry}
            placeholder={props.placeholder} style={TextBoxComponentStyle.TextBox} value={props.value} onChangeText={(text) => props.onChangeText(text)} />
    )
}

export default TextBoxComponent;