import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import ButtonComponentTypes from "../enums/button.component.types";
import ButtonComponentStyle from "../styles/components/button.component.style";

const ButtonComponent = (props) => {
    const RenderStyle = () => {
        if (props.type == ButtonComponentTypes.Default) {
            return ButtonComponentStyle.Default;
        }
        else if (props.type == ButtonComponentTypes.Danger) {
            return ButtonComponentStyle.Danger;
        }
        else if (props.type == ButtonComponentTypes.Info) {
            return ButtonComponentStyle.Info;
        }
        else if (props.type == ButtonComponentTypes.Warning) {
            return ButtonComponentStyle.Warning;
        }
        else if (props.type == ButtonComponentTypes.Extra) {
            return ButtonComponentStyle.Extra;
        }
    }

    return (
        <View style={ButtonComponentStyle.Container}>
            <TouchableOpacity disabled={props.disabled} style={ButtonComponentStyle.TouchableOpacity} onPress={() => props.onPress()}>
                <Text style={[ButtonComponentStyle.Text, RenderStyle()]}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default ButtonComponent;