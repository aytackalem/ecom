import React from 'react';
import { SafeAreaView, Text, View } from "react-native";
import ButtonComponentTypes from '../enums/button.component.types';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import ScreenContainerComponentStyle from '../styles/components/screen.container.component.style';
import ButtonComponent from './button.component';
import PaddingContainerComponent from './padding.container.component';

const ScreenContainerComponent = (props) => {
    return (
        <SafeAreaView style={ScreenContainerComponentStyle.SafeAreaView}>
                {props.children}

                {
                    props.preloaderIsOpen == true && <>
                        <View style={ScreenContainerComponentStyle.PreloaderContainer}>
                            <View style={ScreenContainerComponentStyle.PreloaderInnerContainer}>
                                <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                    <Text style={ScreenContainerComponentStyle.PreloaderText}>
                                        Lütfen Bekleyiniz
                                    </Text>
                                </PaddingContainerComponent>
                            </View>
                        </View>
                    </>
                }

                {
                    props.modalIsOpen == true && <>
                        <View style={ScreenContainerComponentStyle.PreloaderContainer}>
                            <View style={ScreenContainerComponentStyle.PreloaderInnerContainer}>
                                <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                    <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                        <Text style={ScreenContainerComponentStyle.PreloaderText}>{props.modalText}</Text>
                                    </PaddingContainerComponent>

                                    <ButtonComponent type={ButtonComponentTypes.Info} onPress={() => props.modalClose()} text="Kapat" />
                                </PaddingContainerComponent>
                            </View>
                        </View>
                    </>
                }
            </SafeAreaView>
    );
};

export default ScreenContainerComponent;