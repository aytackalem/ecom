import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ButtonComponent from '../components/button.component';
import ImageBackgroundComponent from '../components/image.background.component';
import InputContainerComponent from '../components/input.container.component';
import ModalSelectComponent from '../components/modal.select.component';
import PaddingContainerComponent from '../components/padding.container.component';
import ButtonComponentTypes from '../enums/button.component.types';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import { UpdateModal } from '../redux/modal.slice';
import { UpdatePreloader } from '../redux/preloader.slice';
import OrderService, { OrderServiceGet } from '../services/order.service';
import MainScreenStyle from '../styles/screens/main.screen.style';
import MarketplaceService from '../services/marketplace.service';
import TextBoxComponentStyle from "../styles/components/textbox.component.style";
import MenuComponent from '../components/menu.component';

const MainScreen = ({ navigation }) => {

    const _dispatch = useDispatch();

    const _domains = useSelector((state) => state.LoginSlice.domains);

    const _token = useSelector((state) => state.LoginSlice.token);

    const [DomainSelectionOpen, SetDomainSelectionOpen] = useState(false);

    const [Domain, SetDomain] = useState({ key: _domains[0].id, value: _domains[0].name });

    const [CompanySelectionOpen, SetCompanySelectionOpen] = useState(false);

    const [Company, SetCompany] = useState({ key: _domains[0].companies[0].id, value: _domains[0].companies[0].name });

    const [Marketplace, SetMarketplace] = useState({ key: '', value: 'Tümü' });

    const [Shelf, SetShelf] = useState({ key: '', value: 'Tümü' });

    const [OrderType, SetOrderType] = useState({ key: '', value: 'Tümü' });

    const [OrderId, SetOrderId] = useState(0);


    const [Marketplaces, SetMarketplaces] = useState([]);

    const [Shelfs, SetShelfs] = useState([{ id: '', name: 'Tümü' }, { id: 'alt', name: 'Alt' }, { id: 'üst', name: 'Üst' }]);

    const [OrderTypes, SetOrderTypes] = useState([{ id: '', name: 'Tümü' }, { id: 'T', name: 'Tekli' }, { id: 'Ç', name: 'Çoklu' }]);

    const [OrderTypeSelectionOpen, SetOrderTypeSelectionOpen] = useState(false);

    const [MarketplaceSelectionOpen, SetMarketplaceSelectionOpen] = useState(false);

    const [ShelfSelectionOpen, SetShelfSelectionOpen] = useState(false);


    const _loginSlice = useSelector((state) => state.LoginSlice);

    const [ButtonDisabled, SetButtonDisabled] = useState(false);

    const orderService = new OrderService(_dispatch, _loginSlice);

    const marketplaceService = new MarketplaceService(_dispatch, _loginSlice);

    useEffect(() => {
        GetMarketplace();

    }, []);


    const Get = () => {
        SetButtonDisabled(true);

        _dispatch(UpdatePreloader(true));
debugger
        orderService.OrderServiceGet(OrderId, Marketplace.key, Shelf.key, OrderType.key, { headers: { authorization: `Bearer ${_token}`, domainId: Domain.key, companyId: Company.key } }, response => {
            _dispatch(UpdatePreloader(false));

            SetButtonDisabled(false);

            if (response.data.data != null) {
                navigation.navigate('Picking', { order: response.data.data, DomainId: Domain.key, CompanyId: Company.key });
            }
            else {
                _dispatch(UpdateModal({ IsOpen: true, Text: 'Sipariş bulunamadı.' }));
            }
        }, error => {

            _dispatch(UpdatePreloader(false));

            SetButtonDisabled(false);
        });

    }

    const GetMarketplace = () => {



        marketplaceService.MarketplaceServiceGet({ headers: { authorization: `Bearer ${_token}`, domainId: Domain.key, companyId: Company.key } }, response => {

            _dispatch(UpdatePreloader(false));

            SetButtonDisabled(false);

            if (response.data.success) {
                response.data.data.push({
                    "id": "",
                    "name": "Tümü"
                },);
                SetMarketplaces(response.data.data);
            }

        }, error => {


        });

    }

    return (
        <ImageBackgroundComponent>

            <View style={MainScreenStyle.Container}>

                <View style={MainScreenStyle.Header}></View>

                <View style={MainScreenStyle.Content}>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <Text style={MainScreenStyle.Filter}>Filtre</Text>
                    </PaddingContainerComponent>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent text={`Marka Seç: ${Domain.value}`} type={ButtonComponentTypes.Default} onPress={() => SetDomainSelectionOpen(true)} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent text={`Mağaza Seç: ${Company.value}`} type={ButtonComponentTypes.Default} onPress={() => SetCompanySelectionOpen(true)} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent text={`Sipariş Kaynağı Seç: ${Marketplace.value}`} type={ButtonComponentTypes.Default} onPress={() => SetMarketplaceSelectionOpen(true)} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>








                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent text={`Depo Seç: ${Shelf.value}`} type={ButtonComponentTypes.Default} onPress={() => SetShelfSelectionOpen(true)} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent text={`Sipariş Tipi Seç: ${OrderType.value}`} type={ButtonComponentTypes.Default} onPress={() => SetOrderTypeSelectionOpen(true)} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>









                    <InputContainerComponent>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>


                            <TextInput placeholder={"Sipariş Numarası Giriniz"} style={[TextBoxComponentStyle.TextBox]} value={OrderId} onChangeText={(text) => SetOrderId(text)} keyboardType="numeric" />
                        </PaddingContainerComponent>
                    </InputContainerComponent>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            <ButtonComponent disabled={ButtonDisabled} text="Sıradaki Siparişi Getir" type={ButtonComponentTypes.Info} onPress={() => Get()} />
                        </InputContainerComponent>
                    </PaddingContainerComponent>

                </View>

                <View style={MainScreenStyle.Footer}>
                    <MenuComponent navigation={navigation} name="Main" />
                </View>

            </View>

            {
                DomainSelectionOpen && <>
                    <ModalSelectComponent onChange={(item) => {
                        SetDomainSelectionOpen(false);
                        SetDomain(item);
                    }} data={_domains.map(item => { return { key: item.id, value: item.name } })} />
                </>
            }

            {
                CompanySelectionOpen && <>
                    <ModalSelectComponent onChange={(item) => {
                        SetCompanySelectionOpen(false);
                        SetCompany(item);
                    }} data={_domains.find(value => value.id == Domain.key).companies.map(item => { return { key: item.id, value: item.name } })} />
                </>
            }

            {
                MarketplaceSelectionOpen && <>
                    <ModalSelectComponent onChange={(item) => {
                        SetMarketplaceSelectionOpen(false);
                        SetMarketplace(item);
                    }} data={Marketplaces.map(item => { return { key: item.id, value: item.name } })} />
                </>
            }



            {
                ShelfSelectionOpen && <>
                    <ModalSelectComponent onChange={(item) => {
                        SetShelfSelectionOpen(false);
                        SetShelf(item);
                    }} data={Shelfs.map(item => { return { key: item.id, value: item.name } })} />
                </>
            }

            {
                OrderTypeSelectionOpen && <>
                    <ModalSelectComponent onChange={(item) => {
                        SetOrderTypeSelectionOpen(false);
                        SetOrderType(item);
                    }} data={OrderTypes.map(item => { return { key: item.id, value: item.name } })} />
                </>
            }



        </ImageBackgroundComponent>
    )
};

export default MainScreen;