import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, FlatList, Image, Text, View, Alert, ScrollView } from 'react-native';
import BadgeComponent from '../components/badge.component';
import ButtonComponent from '../components/button.component';
import PaddingContainerComponent from '../components/padding.container.component';
import InputContainerComponent from '../components/input.container.component';
import KeyValueComponent from '../components/key.value.component';
import MultiInputContainerComponent from '../components/multi.input.container.component';
import ScreenContainerComponent from '../components/screen.container.component';
import BadgeComponentTypes from '../enums/badge.component.types';
import ButtonComponentTypes from '../enums/button.component.types';
import MultiInputContainerComponentTypes from '../enums/multi.input.container.component.types';
import PickingScreenStyle from '../styles/screens/picking.screen.style';
import ImageBackgroundComponent from '../components/image.background.component';
// import BarcodeScanner from 'react-native-scan-barcode';
import { UpdatePreloader } from '../redux/preloader.slice';
import { UpdateModal } from '../redux/modal.slice';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import { BarCodeScanner } from 'expo-barcode-scanner';
import OrderService from '../services/order.service';
import ProductService from '../services/product.service';
import ProductInformationScreenStyle from '../styles/screens/product.information.screen.style';
import MenuComponent from '../components/menu.component';

const ProductInformationScreen = ({ route, navigation }) => {

    const _dispatch = useDispatch();

    const _loginSlice = useSelector((state) => state.LoginSlice);

    const _token = useSelector((state) => state.LoginSlice.token);

    const [Completed, SetCompleted] = useState(false);

    const [Reading, SetReading] = useState(true);

    const productService = new ProductService(_dispatch, _loginSlice)

    const [Product, SetProduct] = useState(null);

    const BarcodeReceived = ({ type, data }) => {

        SetReading(false)

        _dispatch(UpdatePreloader(true));

        productService.ProductServiceGet(data, {
            headers: {
                Authorization: `Bearer ${_token}`
            }
        }, response => {

            if (response.data.success) {
                SetProduct(response.data.data);
            }
            else {
                SetReading(false)
            }

            _dispatch(UpdatePreloader(false));

        });

    };

    const [hasPermission, setHasPermission] = useState(null);

    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return <Text>Kamera erişim izni bekleniyor.</Text>;
    }

    if (hasPermission === false) {
        return <Text>Kamera erişimi olmadan uygulayı kullanamazsınız.</Text>;
    }

    return (
        <ScreenContainerComponent>

            <ImageBackgroundComponent>

                <View style={ProductInformationScreenStyle.Content}>

                    {
                        Reading && <>

                            <BarCodeScanner
                                onBarCodeScanned={BarcodeReceived}
                                style={{ flex: 1 }}
                            />

                        </>
                    }

                    {
                        Product != null && <>
                            <ScrollView>
                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                    <InputContainerComponent>
                                        <Image style={{ height: 280, resizeMode: 'contain' }} source={{ uri: Product.productInformationPhotos[0].fileName }} />
                                    </InputContainerComponent>
                                </PaddingContainerComponent>

                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                    <InputContainerComponent>
                                        <KeyValueComponent label="Raf:" value={`${Product.shelfZone} ${Product.shelfCode}`} />
                                    </InputContainerComponent>
                                </PaddingContainerComponent>

                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                    <InputContainerComponent>
                                        <KeyValueComponent label="Model Kodu:" value={Product.modelCode} />
                                    </InputContainerComponent>
                                </PaddingContainerComponent>

                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                    <InputContainerComponent>
                                        <KeyValueComponent label="Barkod:" value={Product.barcode} />
                                    </InputContainerComponent>
                                </PaddingContainerComponent>

                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                    <InputContainerComponent>
                                        <KeyValueComponent label="Ürün:" value={Product.productInformationName} />
                                    </InputContainerComponent>
                                </PaddingContainerComponent>

                                {
                                    (Product.productVariants.length > 0) && Product.productVariants.map((value) => <>
                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <KeyValueComponent label={`${value.variantValue.variant.name}:`} value={`${value.variantValue.value}`} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>
                                    </>)
                                }
                            </ScrollView>
                        </>
                    }

                    {
                        !Reading && <>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text="Barkod Oku" type={ButtonComponentTypes.Info} onPress={() => {
                                        SetReading(true)
                                        SetProduct(null)
                                    }} />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        </>
                    }

                </View>

                <View style={ProductInformationScreenStyle.Footer}>
                    <MenuComponent navigation={navigation} name="ProductInformation" />
                </View>

            </ImageBackgroundComponent>

        </ScreenContainerComponent>
    )

};

export default ProductInformationScreen;