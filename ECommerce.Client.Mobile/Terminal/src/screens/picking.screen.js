import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, FlatList, Image, Text, View, Alert } from 'react-native';
import BadgeComponent from '../components/badge.component';
import ButtonComponent from '../components/button.component';
import PaddingContainerComponent from '../components/padding.container.component';
import InputContainerComponent from '../components/input.container.component';
import KeyValueComponent from '../components/key.value.component';
import MultiInputContainerComponent from '../components/multi.input.container.component';
import ScreenContainerComponent from '../components/screen.container.component';
import BadgeComponentTypes from '../enums/badge.component.types';
import ButtonComponentTypes from '../enums/button.component.types';
import MultiInputContainerComponentTypes from '../enums/multi.input.container.component.types';
import PickingScreenStyle from '../styles/screens/picking.screen.style';
import ImageBackgroundComponent from '../components/image.background.component';
// import BarcodeScanner from 'react-native-scan-barcode';
import { UpdatePreloader } from '../redux/preloader.slice';
import { UpdateModal } from '../redux/modal.slice';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import { BarCodeScanner } from 'expo-barcode-scanner';
import OrderService from '../services/order.service';

const PickingScreen = ({ route, navigation }) => {
    const _dispatch = useDispatch();

    const _loginSlice = useSelector((state) => state.LoginSlice);

    const _token = useSelector((state) => state.LoginSlice.token);

    const orderService = new OrderService(_dispatch, _loginSlice)

    const { DomainId, CompanyId } = route.params;

    const [Order, SetOrder] = useState(route.params.order);

    const [Completed, SetCompleted] = useState(false);

    const [Reading, SetReading] = useState(false);

    const [PackerBarcode, SetPackerBarcode] = useState(null);

    const Cancel = () => {
        _dispatch(UpdatePreloader(true));

        orderService.OrderServiceCancel(Order.id, { headers: { Authorization: `Bearer ${_token}`, domainId: DomainId, companyId: CompanyId } }, response => {

            _dispatch(UpdatePreloader(false));

            navigation.navigate('Main');
        });
    };

    const Skip = () => {
        _dispatch(UpdatePreloader(true));

        orderService.OrderServiceSkip(Order.id, { headers: { Authorization: `Bearer ${_token}`, domainId: DomainId, companyId: CompanyId } }, response => {
            _dispatch(UpdatePreloader(false));

            navigation.navigate('Main');
        });
    };

    const Complete = () => {
        _dispatch(UpdatePreloader(true));

        orderService.OrderServiceComplete(Order.id, PackerBarcode, { headers: { Authorization: `Bearer ${_token}`, domainId: DomainId, companyId: CompanyId } }, response => {

            _dispatch(UpdatePreloader(false));
            
            if (!response.data.success) {

                _dispatch(UpdateModal({ IsOpen: true, Text: response.data.message }));
                SetPackerBarcode(null);
            }
            else
                navigation.navigate('Main');


        }, exception => {

            _dispatch(UpdatePreloader(false));
            _dispatch(UpdateModal({ IsOpen: true, Text: 'Hata oluştu.' }));
        });
    };

    const BarcodeReceived = ({ type, data }) => {
        setScanned(true);

        SetReading(false);

        if (Completed) {
            SetPackerBarcode(data);
        }
        else {
            IncreaseCompletedQuantity(data);
        }
    }

    const IncreaseCompletedQuantity = (barcode) => {
        let order = Order.orderDetails.find(value => value.productInformation.barcode == barcode);

        if (order == undefined) {
            _dispatch(UpdateModal({ IsOpen: true, Text: 'Hatalı barkod okuttunuz.' }));
        }
        else {
            order.completedQuantity = (order.completedQuantity || 0) + 1;

            SetOrder(JSON.parse(JSON.stringify(Order)));

            if (Order.orderDetails.find(value => value.quantity != value.completedQuantity) == undefined) {

                SetCompleted(true);
            }
        }
    }

    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return <Text>Kamera erişim izni bekleniyor.</Text>;
    }
    if (hasPermission === false) {
        return <Text>Kamera erişimi olmadan uygulayı kullanamazsınız.</Text>;
    }

    return (
        <ScreenContainerComponent>

            <ImageBackgroundComponent>


                <View style={PickingScreenStyle.Header}>

                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <MultiInputContainerComponent type={MultiInputContainerComponentTypes.SpaceBetween}>
                            <ButtonComponent text="Vazgeç" onPress={() => Cancel()} type={ButtonComponentTypes.Default} />

                            <Text style={PickingScreenStyle.CompanyName}>{`Mağaza: ${Order.companyName}`}</Text>

                            <ButtonComponent text="Atla" onPress={() => Skip()} type={ButtonComponentTypes.Danger} />
                        </MultiInputContainerComponent>
                    </PaddingContainerComponent>

                </View>

                <View style={PickingScreenStyle.Content}>

                    {
                        Reading && <>

                            <BarCodeScanner
                                onBarCodeScanned={scanned ? undefined : BarcodeReceived}
                                style={{ flex: 1 }}
                            />
                            {scanned && <Button title={'Tekrar Taramak için Dokunun'} onPress={() => setScanned(false)} />}

                        </> || <>
                            <FlatList
                                data={Order.orderDetails}
                                renderItem={({ item, index }) => {
                                    return <>
                                        {
                                            index == 0 && (
                                                <>
                                                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                                        <InputContainerComponent>
                                                            <KeyValueComponent label="Sipariş Num:" value={`${Order.id}`} />
                                                        </InputContainerComponent>
                                                    </PaddingContainerComponent>
                                                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                                        <InputContainerComponent>
                                                            <KeyValueComponent label="Adı Soyadı:" value={`${Order.customer.name} ${Order.customer.surname}`} />
                                                        </InputContainerComponent>
                                                    </PaddingContainerComponent>
                                                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                                        <InputContainerComponent>
                                                               <KeyValueComponent label="Pazaryeri:" value={`${Order.marketPlace.name}`} />
                                                            
                                                        </InputContainerComponent>
                                                    </PaddingContainerComponent>
                                                </>
                                            )
                                        }

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <KeyValueComponent label="Raf:" value={`${item.productInformation.shelfZone} ${item.productInformation.shelfCode}`} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>

                                        {
                                            (item.productInformation.productVariants.length > 0) && item.productInformation.productVariants.map((value) => <>
                                                <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                                    <InputContainerComponent>
                                                        <KeyValueComponent label={`${value.variantValue.variant.name}:`} value={`${value.variantValue.value}`} />
                                                    </InputContainerComponent>
                                                </PaddingContainerComponent>
                                            </>)
                                        }

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <KeyValueComponent label="Model Kodu:" value={item.productInformation.modelCode} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <KeyValueComponent label="Barkod:" value={item.productInformation.barcode} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <KeyValueComponent label="Ürün:" value={item.productInformation.productInformationName} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <InputContainerComponent>
                                                <Image style={{ height: 280, resizeMode: 'contain' }} source={{ uri: item.productInformation.productInformationPhotos[0].fileName }} />
                                            </InputContainerComponent>
                                        </PaddingContainerComponent>

                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            {
                                                item.quantity == item.completedQuantity && <>
                                                    <InputContainerComponent>
                                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                                            <BadgeComponent type={BadgeComponentTypes.Success} text={item.completedQuantity || 0} />
                                                        </PaddingContainerComponent>
                                                    </InputContainerComponent>
                                                </> || <>
                                                    <MultiInputContainerComponent type={MultiInputContainerComponentTypes.Left}>
                                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                                            <BadgeComponent type={BadgeComponentTypes.Warning} text={item.completedQuantity || 0} />
                                                        </PaddingContainerComponent>
                                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                                            <BadgeComponent type={BadgeComponentTypes.Danger} text={item.quantity} />
                                                        </PaddingContainerComponent>
                                                    </MultiInputContainerComponent>

                                                    {/* <InputContainerComponent>
                                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                                            <ButtonComponent text="Okutuldu İşaretle" onPress={() => IncreaseCompletedQuantity(item.productInformation.barcode)} type={ButtonComponentTypes.Default} />
                                                        </PaddingContainerComponent>
                                                    </InputContainerComponent> */}
                                                </>
                                            }
                                        </PaddingContainerComponent>
                                    </>
                                }}
                            />
                        </>
                    }

                </View>

                <View style={PickingScreenStyle.Footer}>

                    {
                        (!Completed && !Reading) && <>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text="Barkod Oku" type={ButtonComponentTypes.Info} onPress={() => {
                                        SetReading(true)
                                        setScanned(false)
                                    }
                                    } />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        </>
                    }

                    {
                        (Completed && PackerBarcode == null) && <>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text="Son Barkod Okut" onPress={() => {
                                        SetReading(true)

                                    }
                                    } type={ButtonComponentTypes.Extra} />
                                </InputContainerComponent></PaddingContainerComponent>
                        </>
                    }

                    {
                        (Completed && PackerBarcode != null) && <>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent disabled={!Completed} text="Tamamla" onPress={() => Complete()} type={ButtonComponentTypes.Extra} />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        </>
                    }

                    {
                        Reading && <>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text="Kamerayı Kapat" type={ButtonComponentTypes.Danger} onPress={() => SetReading(false)} />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        </>
                    }

                </View>

            </ImageBackgroundComponent>

        </ScreenContainerComponent>
    )
};

export default PickingScreen;