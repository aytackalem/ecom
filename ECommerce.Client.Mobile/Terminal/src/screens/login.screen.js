import React, { useEffect, useState } from 'react';
import { Alert, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ButtonComponent from '../components/button.component';
import HeadingComponent from '../components/heading.component';
import ImageBackgroundComponent from '../components/image.background.component';
import InputContainerComponent from "../components/input.container.component";
import PaddingContainerComponent from '../components/padding.container.component';
import TextBoxComponent from "../components/text.box.compnent";
import ButtonComponentTypes from '../enums/button.component.types';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import { WriteManagerUser } from '../redux/login.slice';
import { UpdatePreloader } from '../redux/preloader.slice';
import LoginService, { LoginServiceGet } from '../services/login.service';
import LoginScreenStyle from '../styles/screens/login.screen.style';


const LoginScreen = () => {


    const _loginSlice = useSelector((state) => state.LoginSlice);

    const _dispatch = useDispatch();

    const loginService = new LoginService(_dispatch, _loginSlice);


    const [ButtonDisabled, SetButtonDisabled] = useState(false);

    const [Username, SetUsername] = useState("Lafaba");
    const [Passoword, SetPassoword] = useState("3297723Az*");

    const Login = () => {
        SetButtonDisabled(true);

        _dispatch(UpdatePreloader(true));
        
        loginService.Login(Username, Passoword, response => {
            _dispatch(UpdatePreloader(false));
            
            _dispatch(WriteManagerUser({
                username: Username,
                password: Passoword,
                refreshToken: response.refreshToken,
                token: response.token,
                domains: response.domains
            }));
        }, exception => {
            _dispatch(UpdatePreloader(false));
            Alert.alert('Kullanıcı adı veya şifreniz yanlış lütfen tekrar deneyiniz');
            SetButtonDisabled(false);
        });
    };

    return (
        <ImageBackgroundComponent>

            <View style={LoginScreenStyle.Container}>

                <View style={LoginScreenStyle.Content}>

                    <InputContainerComponent>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                            <HeadingComponent text="Toplama Uygulaması Giriş" />
                        </PaddingContainerComponent>
                    </InputContainerComponent>

                    <InputContainerComponent>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                            <TextBoxComponent placeholder="Kullanıcı Adı" value={Username} onChangeText={(text) => SetUsername(text)} />
                        </PaddingContainerComponent>
                    </InputContainerComponent>

                    <InputContainerComponent>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                            <TextBoxComponent placeholder="Şifre" value={Passoword} onChangeText={(text) => SetPassoword(text)} />
                        </PaddingContainerComponent>
                    </InputContainerComponent>

                    <InputContainerComponent>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                            <ButtonComponent disabled={ButtonDisabled} text="Giriş Yap" type={ButtonComponentTypes.Info} onPress={() => Login()} />
                        </PaddingContainerComponent>
                    </InputContainerComponent>

                </View>

            </View>

        </ImageBackgroundComponent>
    )
};

export default LoginScreen;