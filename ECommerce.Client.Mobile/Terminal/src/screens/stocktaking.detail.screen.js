import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import StocktakingService from "../services/stocktaking.service";
import { UpdatePreloader } from "../redux/preloader.slice";
import ScreenContainerComponent from "../components/screen.container.component";
import { Alert, FlatList, View } from "react-native";
import PaddingContainerComponent from "../components/padding.container.component";
import MultiInputContainerComponent from "../components/multi.input.container.component";
import ButtonComponent from "../components/button.component";
import HeadingComponent from "../components/heading.component";
import ButtonComponentTypes from "../enums/button.component.types";
import PaddingContainerComponentTypes from "../enums/padding.container.component.types";
import HeadingComponentStyle from "../styles/components/heading.component.style";
import MultiInputContainerComponentTypes from "../enums/multi.input.container.component.types";
import StocktakingScreenStyle from "../styles/screens/stocktaking.screen.style";
import ImageBackgroundComponent from '../components/image.background.component';
import KeyValueComponent from '../components/key.value.component';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { UpdateModal } from '../redux/modal.slice';
import StocktakingProductService from '../services/stocktaking.product.service';
import InputContainerComponent from '../components/input.container.component';
import SoundPlayer from 'react-native-sound-player';
import ConfirmComponent from '../components/confirm.component';
import TextBoxComponent from '../components/text.box.compnent';

const StocktakingDetailScreen = ({ route, navigation }) => {
    const _dispatch = useDispatch();

    const _loginSlice = useSelector((state) => state.LoginSlice);

    const _token = useSelector((state) => state.LoginSlice.token);

    const [StocktakingId, SetStocktakingId] = useState(route.params.stocktakingId);

    const [CheckGroup, SetCheckGroup] = useState(route.params.checkGroup);

    const [ModelCode, SetModelCode] = useState(route.params.modelCode);

    const [Manuel, SetManuel] = useState(route.params.manuel);

    const [ManuelBarcode, SetManuelBarcode] = useState(null);

    const [ManuelQuantity, SetManuelQuantity] = useState("1");

    const [OpenManuel, SetOpenManuel] = useState(false);

    const stocktakingService = new StocktakingService(_dispatch, _loginSlice);

    const stocktakingProductService = new StocktakingProductService(_dispatch, _loginSlice);

    const [Stocktaking, SetStocktaking] = useState(null);

    const [Scanning, SetScanning] = useState(true);

    const [BarcodeProcessing, SetBarcodeProcessing] = useState(false);

    const [scanned, setScanned] = useState(false);

    const [Message, SetMessage] = useState("");

    const [ConfirmVisible, SetConfirmVisible] = useState(false);

    useEffect(() => {
        GetStocktaking()
    }, []);

    const GetStocktaking = () => {
        _dispatch(UpdatePreloader(true));

        stocktakingService.StocktakingServiceGetById(StocktakingId, {
            headers: {
                authorization: `Bearer ${_token}`
            }
        }, response => {

            _dispatch(UpdatePreloader(false));

            SetStocktaking(response.data.data);

        }, error => {

            _dispatch(UpdatePreloader(false));

            SetStocktaking(null);

        });
    }

    const BarcodeReceived = ({ type, data }) => {

        if (type != 32 && type != 1) {
            console.log(`Illegal type: ${type}. Passed!`)
            return
        }

        SetBarcodeProcessing(true);

        let stocktakingDetail = Stocktaking.stocktakingDetails.find(item => item.productInformation.barcode == data);

        if (stocktakingDetail == undefined) {

            stocktakingProductService.StocktakingProductServiceGet(data, ModelCode, CheckGroup, {
                headers: {
                    authorization: `Bearer ${_token}`
                }
            }, response => {

                if (!response.data.success) {
                    AlertSound()

                    SetMessage(response.data.message)

                    ClearScreen()
                }
                else {
                    response.data.data.forEach(element => {
                        Stocktaking.stocktakingDetails.push({ quantity: 0, productInformationId: element.id, productInformation: element });
                    });

                    if (Manuel) {
                        SetOpenManuel(true)
                        SetManuelQuantity("1")
                        SetManuelBarcode(data)
                    }
                    else {
                        stocktakingDetail = Stocktaking.stocktakingDetails.find(item => item.productInformation.barcode == data);

                        stocktakingDetail.quantity += 1

                        SetStocktakingState()

                        SetMessage(stocktakingDetail.quantity)

                        SuccessSound()

                        ClearScreen()
                    }
                }


            }, error => {
                AlertSound();

                SetMessage('Hatalı barkod okuttunuz.')

                ClearScreen()
            });

        }
        else {
            if (Manuel) {
                SetOpenManuel(true)
                SetManuelQuantity("1")
                SetManuelBarcode(data)
            }
            else {
                SuccessSound();

                stocktakingDetail.quantity += 1;

                SetStocktakingState()

                SetMessage(stocktakingDetail.quantity);

                ClearScreen();
            }
        }


    }

    const ClearScreen = () => {
        setTimeout(() => {
            SetMessage("");
            SetBarcodeProcessing(false)
            SetOpenManuel(false)
        }, 1000);
    }

    const SuccessSound = () => {
        SoundPlayer.playSoundFile('on', 'wav');
    }

    const AlertSound = () => {
        SoundPlayer.playSoundFile('notify', 'wav');
    }

    const SetStocktakingState = () => {
        SetStocktaking(JSON.parse(JSON.stringify(Stocktaking)));
    }

    return (
        <ScreenContainerComponent>

            {
                ConfirmVisible && <>
                    <ConfirmComponent
                        title="Emin misiniz?"
                        message="Bu işlem geri alınamaz."
                        no={() => {

                            SetConfirmVisible(false)

                        }}
                        yes={() => {

                            stocktakingService.StocktakingServicePut({
                                id: Stocktaking.id,
                                stocktakingId: 'BE'
                            }, {
                                headers: {
                                    authorization: `Bearer ${_token}`
                                }
                            }, response => {
                                if (response.data.success) {
                                    navigation.navigate('Stocktaking')
                                    _dispatch(UpdateModal({ IsOpen: true, Text: response.data.message }))
                                }
                            }, error => {
                                _dispatch(UpdateModal({ IsOpen: true, Text: response.data.message }))
                            })

                        }} />
                </>
            }

            <ImageBackgroundComponent>

                <View style={StocktakingScreenStyle.Header}>
                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <MultiInputContainerComponent type={MultiInputContainerComponentTypes.SpaceBetween}>
                            <ButtonComponent text="Vazgeç" onPress={() => {

                                SetScanning(false)
                                SetConfirmVisible(true)

                            }} type={ButtonComponentTypes.Default} />

                            <ButtonComponent text="Onayla" onPress={() => {

                                stocktakingService.StocktakingServicePut(Stocktaking, {
                                    headers: {
                                        authorization: `Bearer ${_token}`
                                    }
                                }, response => {
                                    if (response.data.success) {
                                        navigation.navigate('Stocktaking');
                                        _dispatch(UpdateModal({ IsOpen: true, Text: response.data.message }));
                                    }
                                }, error => {
                                    _dispatch(UpdateModal({ IsOpen: true, Text: response.data.message }));
                                });

                            }} type={ButtonComponentTypes.Default} />
                        </MultiInputContainerComponent>
                    </PaddingContainerComponent>
                </View>

                <View style={[StocktakingScreenStyle.Content]}>

                    {
                        OpenManuel && <>
                            <View>
                                <View>

                                    <HeadingComponent text={`Adet Giriniz:`} />
                                    <TextBoxComponent value={ManuelQuantity.toString()} onChangeText={(text) => SetManuelQuantity(text)} />

                                </View>

                                <ButtonComponent text="Tamam" onPress={() => {

                                    var stocktakingDetail = Stocktaking.stocktakingDetails.find(item => item.productInformation.barcode == ManuelBarcode);

                                    stocktakingDetail.quantity += parseInt(ManuelQuantity);

                                    SetStocktakingState()

                                    SetMessage(stocktakingDetail.quantity);

                                    SuccessSound();

                                    ClearScreen();

                                }} type={ButtonComponentTypes.Danger} />
                            </View>
                        </>
                    }

                    {
                        (!OpenManuel && !BarcodeProcessing && Scanning) && <>
                            <BarCodeScanner
                                onBarCodeScanned={scanned ? undefined : BarcodeReceived}
                                style={{ flex: 1 }}
                            />
                        </>
                    }

                    {
                        BarcodeProcessing && <>
                            <HeadingComponent text={Message} style={[{ flex: 1 }, [HeadingComponentStyle.H1, HeadingComponentStyle.TextAlignCenter]]} />
                        </>
                    }

                    {
                        (!OpenManuel && !Scanning && Stocktaking != null && (Stocktaking.stocktakingDetails == null || Stocktaking.stocktakingDetails.length == 0)) && <>
                            <HeadingComponent text="Henüz okuma yapmadınız." style={[{ flex: 1 }, [HeadingComponentStyle.H1, HeadingComponentStyle.TextAlignCenter]]} />
                        </>
                    }

                    {
                        (Stocktaking != null && !Scanning) && <>
                            <FlatList
                                data={Stocktaking.stocktakingDetails}
                                renderItem={({ item, index }) => {
                                    return <>
                                        <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                            <MultiInputContainerComponent type={MultiInputContainerComponentTypes.SpaceBetween}>
                                                <HeadingComponent text={item.productInformation.barcode} />
                                                <HeadingComponent text={item.quantity} />
                                                <ButtonComponent text="Azalt" onPress={() => {

                                                    let barcode = item.productInformation.barcode;
                                                    let stocktakingDetail = Stocktaking.stocktakingDetails.find(item => item.productInformation.barcode == barcode);
                                                    stocktakingDetail.quantity -= 1;
                                                    SetStocktakingState();

                                                }} type={ButtonComponentTypes.Danger} />
                                            </MultiInputContainerComponent>
                                        </PaddingContainerComponent>
                                    </>
                                }} />
                        </>
                    }
                </View>

                <View style={StocktakingScreenStyle.Footer}>
                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <InputContainerComponent>
                            {
                                Scanning && <>
                                    <ButtonComponent text="Sayımı Durdur" onPress={() => {
                                        SetScanning(false);
                                    }} type={ButtonComponentTypes.Danger} />
                                </> || <>
                                    <ButtonComponent text="Sayıma Devam Et" onPress={() => {
                                        SetScanning(true);
                                    }} type={ButtonComponentTypes.Info} />
                                </>
                            }
                        </InputContainerComponent>
                    </PaddingContainerComponent>
                </View>

            </ImageBackgroundComponent>

        </ScreenContainerComponent>
    )
};

export default StocktakingDetailScreen;