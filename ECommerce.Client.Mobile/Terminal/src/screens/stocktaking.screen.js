import React, { useEffect, useState } from 'react';
import ImageBackgroundComponent from "../components/image.background.component";
import ScreenContainerComponent from "../components/screen.container.component";
import { FlatList, Image, Touchable, TouchableOpacity, View } from 'react-native';
import StocktakingScreenStyle from '../styles/screens/stocktaking.screen.style';
import PaddingContainerComponent from '../components/padding.container.component';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import MultiInputContainerComponentTypes from '../enums/multi.input.container.component.types';
import ButtonComponentTypes from '../enums/button.component.types';
import MultiInputContainerComponent from '../components/multi.input.container.component';
import ButtonComponent from '../components/button.component';
import HeadingComponent from '../components/heading.component';
import HeadingComponentStyle from '../styles/components/heading.component.style';
import StocktakingService from '../services/stocktaking.service';
import { UpdatePreloader } from '../redux/preloader.slice';
import { useDispatch, useSelector } from 'react-redux';
import MenuComponent from '../components/menu.component';

const StocktakingScreen = ({ route, navigation }) => {
    const _dispatch = useDispatch();

    const _loginSlice = useSelector((state) => state.LoginSlice);

    const _token = useSelector((state) => state.LoginSlice.token);

    const stocktakingService = new StocktakingService(_dispatch, _loginSlice);

    const [Stocktakings, SetStocktakings] = useState(null);

    useEffect(() => {
        GetStocktakings()
    }, []);

    const GetStocktakings = () => {
        _dispatch(UpdatePreloader(true));

        stocktakingService.StocktakingServiceGet(1, 50, {
            headers: {
                authorization: `Bearer ${_token}`
            }
        }, response => {

            _dispatch(UpdatePreloader(false));

            SetStocktakings(response.data.data);

        }, error => {

            _dispatch(UpdatePreloader(false));

            SetStocktakings([]);

        });
    }

    return (
        <ScreenContainerComponent>

            <ImageBackgroundComponent>

                <View style={StocktakingScreenStyle.Header}>
                    <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                        <MultiInputContainerComponent type={MultiInputContainerComponentTypes.SpaceBetween}>
                            <ButtonComponent text="Geri Dön" onPress={() => {
                                navigation.navigate('Main');
                            }} type={ButtonComponentTypes.Default} />
                        </MultiInputContainerComponent>
                    </PaddingContainerComponent>
                </View>

                <View style={StocktakingScreenStyle.Content}>
                    <HeadingComponent text="Stok Sayımları" style={[HeadingComponentStyle.H4, { marginLeft: 20, fontWeight: '500' }]} />

                    {
                        Stocktakings != null && <>
                            <FlatList
                                data={Stocktakings}
                                renderItem={({ item, index }) => {
                                    return <>
                                        <PaddingContainerComponent style={StocktakingScreenStyle.Item} type={PaddingContainerComponentTypes[10]}>
                                            <HeadingComponent text={item.name} style={[HeadingComponentStyle.H4, { color: 'white' }]} />

                                            {
                                                item.description != '' && <>
                                                    <HeadingComponent text={item.description} style={[HeadingComponentStyle.H5, { color: 'white' }]} />
                                                </>
                                            }

                                            <TouchableOpacity onPress={() => navigation.navigate('StocktakingDetail', {
                                                stocktakingId: item.id, 
                                                modelCode: item.modelCode, 
                                                checkGroup: item.checkGroup,
                                                manuel: item.manuel
                                            })}>
                                                <View style={{
                                                    height: 40,
                                                    width: 40,
                                                    backgroundColor: 'white',
                                                    shadowColor: "#0b7cb0",
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 4,
                                                    },
                                                    shadowOpacity: 0.30,
                                                    shadowRadius: 4.65,

                                                    elevation: 8,
                                                    borderRadius: 20,
                                                    alignItems: 'center',
                                                }}>

                                                    <Image source={require("../images/miscellaneous/arrow.png")} style={{
                                                        height: 30,
                                                        width: 30,
                                                        margin: 5
                                                    }} />

                                                </View>
                                            </TouchableOpacity>

                                        </PaddingContainerComponent>
                                    </>
                                }} />
                        </>
                    }
                </View>

                <View style={StocktakingScreenStyle.Footer}>
                    <MenuComponent navigation={navigation} name="Stocktaking" />
                </View>

            </ImageBackgroundComponent>

        </ScreenContainerComponent>
    )

};

export default StocktakingScreen;