import ServiceBase from "./base/service-base";
import { Dispatch } from "redux";
import Token from "../models/token";
import { AxiosResponse } from "axios";

class OrderService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {

        super(dispatch, tokenSlice);

    }


    OrderServiceGet(orderId: number, marketplaceId: string, shelf: string, orderType: string, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {

        if (!orderId)
            orderId = 0;


        this
            ._httpClientV2
            .get(`/order?orderId=${orderId}&MarketplaceId=${marketplaceId}&shelf=${shelf}&orderType=${orderType}`, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

    OrderServiceCancel(id, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .post("/order/skip",
                {
                    id: id,
                    IsEstimatedPackingDate: false
                }, headers).then(response => {

                    if (then) {
                        then(response);
                    }

                })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

    OrderServiceSkip(id, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .post("/order/skip",
                {
                    id: id,
                    IsEstimatedPackingDate: true
                }, headers).then(response => {

                    if (then) {
                        then(response);
                    }

                })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

    OrderServiceComplete(id, packerBarcode, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .post("/order/approved",
                {
                    id: id,
                    packerBarcode: packerBarcode
                }, headers).then(response => {

                    if (then) {
                        then(response);
                    }

                })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };


}

export default OrderService;