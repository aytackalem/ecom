import ServiceBase from "./base/service-base";
import { Dispatch } from "redux";
import Token from "../models/token";
import { AxiosResponse } from "axios";

class  MarketplaceService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {
        
        super(dispatch, tokenSlice);

    }


    MarketplaceServiceGet(headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .get("/Marketplace", headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };




}

export default MarketplaceService;