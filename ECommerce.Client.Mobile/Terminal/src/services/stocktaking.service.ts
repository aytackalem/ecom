import { Dispatch } from "redux";
import ServiceBase from "./base/service-base";
import { AxiosResponse } from "axios";

class StocktakingService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {
        super(dispatch, tokenSlice);
    }

    StocktakingServiceGet(page: number, pageRecordsCount: number, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .get(`/Stocktaking?page=${page}&pageRecordsCount=${pageRecordsCount}`, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

    StocktakingServiceGetById(id: number, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .get(`/Stocktaking/${id}`, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

    StocktakingServicePut(stocktaking: any, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .put(`/Stocktaking`, stocktaking, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

}

export default StocktakingService;