import { Dispatch } from "redux";
import ServiceBase from "./base/service-base";
import { AxiosResponse } from "axios";

class StocktakingProductService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {
        super(dispatch, tokenSlice);
    }

    StocktakingProductServiceGet(barcode: string, modelCode: string, checkGroup: boolean, headers, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .get(`/StocktakingProduct?barcode=${barcode}&modelCode=${modelCode}&checkGroup=${checkGroup}`, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

}

export default StocktakingProductService;