import { Dimensions, StyleSheet } from "react-native";

const LoginScreenStyle = StyleSheet.create({
    Container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    Content:{
        flex: 1,
        height: 280
    }
});

export default LoginScreenStyle;