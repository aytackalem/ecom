import { Dimensions, StyleSheet } from "react-native";

const MainScreenStyle = StyleSheet.create({
    Container: {
        flex: 1
    },
    Header: {
        flex: 1
    },
    Content: {
        flex: 10
    },
    Filter:{
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    Footer: {
        flex: 1
    }
});

export default MainScreenStyle;