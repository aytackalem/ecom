import { Dimensions, StyleSheet } from "react-native";

const StocktakingScreenStyle = StyleSheet.create({
    Header: {
        flex: 1
    },
    CompanyName: {
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    Content: {
        flex: 10
    },
    Item: {
        backgroundColor: '#22b9ff',
        shadowColor: "#008bcc",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        
        elevation: 10,

        borderRadius: 20,
        margin: 15,
        // padding: 10
    },
    Footer: {
        flex: 1
    }
});

export default StocktakingScreenStyle;