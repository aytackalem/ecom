import { Dimensions, StyleSheet } from "react-native";

const ProductInformationScreenStyle = StyleSheet.create({
    Header: {
        flex: 1
    },
    CompanyName:{
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    Content: {
        flex: 10
    },
    Footer: {
        flex: 1
    }
});

export default ProductInformationScreenStyle;