import { Dimensions, StyleSheet } from "react-native";

const ScreenContainerComponentStyle = StyleSheet.create({
    SafeAreaView: {
        flex: 1
    },
    PreloaderContainer: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    PreloaderInnerContainer: {
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15
    },
    PreloaderText: {
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        color: 'gray'
    }
});

export default ScreenContainerComponentStyle;