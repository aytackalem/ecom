import { StyleSheet } from "react-native";

const PaddingContainerComponentStyle = StyleSheet.create({
    Container5: {
        padding: 5
    },
    Container10: {
        padding: 10
    },
    Container15: {
        padding: 15
    },
    Container20: {
        padding: 20
    }
});

export default PaddingContainerComponentStyle;