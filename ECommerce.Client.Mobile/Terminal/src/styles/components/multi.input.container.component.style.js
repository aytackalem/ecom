import { StyleSheet } from "react-native";

const MultiInputContainerComponentStyle = StyleSheet.create({
    Container: {
        flexDirection: 'row'
    },
    SpaceBetween: {
        justifyContent: 'space-between'
    },
    Left: {
        justifyContent: 'center'
    }
});

export default MultiInputContainerComponentStyle;