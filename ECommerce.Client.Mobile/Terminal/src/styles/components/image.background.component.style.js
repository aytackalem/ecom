import { Dimensions, StyleSheet } from "react-native";

const ImageBackgroundComponentStyle = StyleSheet.create({
    Container: {
        flex: 1
    }
});

export default ImageBackgroundComponentStyle;