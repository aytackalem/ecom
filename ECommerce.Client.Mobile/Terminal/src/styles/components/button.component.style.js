import { StyleSheet } from "react-native";

const ButtonComponentStyle = StyleSheet.create({
    Container: {
        alignItems: 'center'
    },
    TouchableOpacity: {
    },
    Text: {
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        height: 48,
        borderWidth: 1,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        borderRadius: 4
    },
    Default:{
        color: 'gray',
        borderColor: '#d3d3d3',
        backgroundColor: 'white'
    },
    Warning:{
        color: 'white',
        borderColor: '#ffb822',
        backgroundColor: '#ffb822'
    },
    Info:{
        color: 'white',
        borderColor: '#22b9ff',
        backgroundColor: '#22b9ff'
    },
    Danger:{
        color: 'white',
        borderColor: '#fd27eb',
        backgroundColor: '#fd27eb'
    },
    Extra:{
        color: 'white',
        borderColor: '#5867dd',
        backgroundColor: '#5867dd'
    }
});

export default ButtonComponentStyle;