import { StyleSheet } from "react-native";

const TextBoxComponentStyle = StyleSheet.create({
    TextBox: {
        paddingLeft: 20,
        paddingRight: 20,
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        color: 'gray',
        backgroundColor: '#ececec',
        height: 50,
        borderRadius: 4
    }
});

export default TextBoxComponentStyle;