import { StyleSheet } from "react-native";

const HeadingComponentStyle = StyleSheet.create({
    H1: {
        height: 50,
        textAlignVertical: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 24
    },
    H2: {
        height: 50,
        textAlignVertical: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 20
    },
    H3: {
        height: 50,
        textAlignVertical: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 18
    },
    H4: {
        height: 50,
        textAlignVertical: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 16
    },
    H5: {
        height: 50,
        textAlignVertical: 'center',
        fontFamily: 'Poppins-Medium',
        fontSize: 12
    },
    TextAlignLeft: {
        textAlign: 'left'
    },
    TextAlignCenter: {
        textAlign: 'center'
    },
    TextAlignRight: {
        textAlign: 'right'
    }
});

export default HeadingComponentStyle;