import { StyleSheet } from "react-native";

const MenuComponentStyle = StyleSheet.create({
    Container: {
        flexDirection: 'row',
        // backgroundColor: '#ececec',
        borderRadius: 10,
        width: 150,
        height: 60,
        alignSelf: 'center',
        alignItems: 'center'
    },
    ButtonActive: {
        backgroundColor: '#22b9ff',
        shadowColor: "#008bcc",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8
    },
    Button: {
        height: 40,
        width: 40,

        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        borderRadius: 30
    },
    Icon: {
        width: 20,
        height: 20
    },
    Text: {
        fontFamily: 'Poppins-Medium',
        fontSize: 10
    },
    TextActive: {
        color: "#22b9ff"
    },
    SpaceBetween: {
        justifyContent: 'space-around'
    }
});

export default MenuComponentStyle;