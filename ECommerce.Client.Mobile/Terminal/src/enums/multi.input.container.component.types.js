const MultiInputContainerComponentTypes = Object.freeze({ "Left": 1, "SpaceBetween": 2 });

export default MultiInputContainerComponentTypes;