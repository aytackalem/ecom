import axios from "axios";
import { Dispatch } from "redux";
import Configuration from "../configuration";
import Token from "../models/token";
import { WriteManagerUser } from "../redux/login.slice";
import HttpClientIdentityV2 from "./http-client-identity-v2";

export default class HttpClientV2 {

    _tokenSlice: any;

    _axiosInstance = axios.create({
        baseURL: Configuration.ApiUrl,
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    });

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {

        this._tokenSlice = tokenSlice;

        this._axiosInstance.interceptors.response.use((response) => {

            return response;

        }, async function (error) {
            
            if (error.response) {

                var config = error.config;

                if (error.response.status == 401 && !config._retry) {

                    config._rety = true;

                    /* 
                        Refresh Token
                    */

                        var client_id = 'PanelForUser';
                        var client_secret = 'secret';
                        var grant_type = 'password';

                    await new HttpClientIdentityV2()
                        .post<any>(
                            "/connect/token",
                            `client_id=${client_id}&client_secret=${client_secret}&grant_type=${grant_type}&username=${tokenSlice.username}&password=${tokenSlice.password}`,
                            {
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded",
                                    "Accept": "*/*"
                                }
                            }
                        )
                        .then(response => {

                            if (response.data) {
                            

                                dispatch(WriteManagerUser({
                                    username: tokenSlice.username,
                                    password: tokenSlice.password,
                                    refreshToken: response.data.refresh_token,
                                    domains: tokenSlice.domains,
                                    token: response.data.access_token,
                                    
                                }));

                                config.headers["Authorization"] = "Bearer " + response.data.access_token;
                            }

                        })
                        .catch(exception => { });

                    return axios(config);

                }

            }

            return Promise.reject(error);

        });

    }

    get<T>(url: string, headers?: any) {
        if (headers == null)
            return this._axiosInstance.get<T>(url, {
                headers: {
                    Authorization: `Bearer ${this._tokenSlice.token}`,
                }
            });
        else
            return this._axiosInstance.get<T>(url, headers);
    }

    post<T>(url: string, data: any, headers?: any) {
        if (headers == null)
            return this._axiosInstance.post<T>(url, data, {
                headers: {
                    Authorization: `Bearer ${this._tokenSlice.token}`,
                }
            });
        else
            return this._axiosInstance.post<T>(url, data, headers);
    }

    put<T>(url: string, data: any, headers?: any) {
        if (headers == null)
            return this._axiosInstance.put<T>(url, data, {
                headers: {
                    Authorization: `Bearer ${this._tokenSlice.token}`,
                }
            });
        else
            return this._axiosInstance.put<T>(url, data, headers);
    }

}