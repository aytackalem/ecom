import { createSlice } from '@reduxjs/toolkit';

const LoginSlice = createSlice({
  name: 'loginSlice',
  initialState: {
    Username: null,
    Password: null,
    Token: null,
    RefreshToken: null,
    Domains: null,
    SelectedDomainId: 0,
    SelectedCompanyId: 0
  },
  reducers: {
    WriteManagerUser: (state, action) => {
      state.Username = action.payload.Username;
      state.Password = action.payload.Password;
      state.Token = action.payload.Token;
      state.RefreshToken = action.payload.RefreshToken;
      state.Domains = action.payload.Domains;
      state.SelectedDomainId = action.payload.Domains[0].id;
      state.SelectedCompanyId = action.payload.Domains[0].companies[0].id;
    },
    DeleteManagerUser: state => {
      state.Username = null;
      state.Password = null;
      state.Token = null;
      state.RefreshToken = null;
      state.Domains = null
      state.SelectedDomainId = 0;
      state.SelectedCompanyId = 0;
    },
    SelectedManager: (state, action) => {

      return {
        ...state,
        SelectedDomainId: action.payload.selectedDomainId,
        SelectedCompanyId: action.payload.selectedCompanyId
      }


    },
  }
});

export const { WriteManagerUser, DeleteManagerUser, SelectedManager } = LoginSlice.actions;

export default LoginSlice.reducer;