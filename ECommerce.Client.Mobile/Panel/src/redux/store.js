import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoginSlice from './login.slice';
import PreloaderSlice from './preloader.slice';
import modalSlice from './modal.slice';

const reducers = combineReducers({
    LoginSlice: LoginSlice,
    PreloaderSlice: PreloaderSlice,
    ModalSlice: modalSlice
});

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
    reducer: persistedReducer
});

export default store;