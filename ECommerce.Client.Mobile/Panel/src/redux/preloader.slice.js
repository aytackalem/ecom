import { createSlice } from '@reduxjs/toolkit';

const PreloaderSlice = createSlice({
    name: 'preloader',
    initialState: {
      IsOpen: false
    },
    reducers: {
      UpdatePreloader: (state, action) => {
        state.IsOpen = action.payload;
      }
    }
  });

  export const { UpdatePreloader } = PreloaderSlice.actions;

  export default PreloaderSlice.reducer;