const PaddingContainerComponentTypes = Object.freeze({ "5": 1, "10": 2, "15": 3, "20": 4 });

export default PaddingContainerComponentTypes;