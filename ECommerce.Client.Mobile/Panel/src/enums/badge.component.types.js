const BadgeComponentTypes = Object.freeze({ "Default": 1, "Warning": 2, "Info": 3, "Danger": 4, "Extra": 5, "Success": 6 });

export default BadgeComponentTypes;