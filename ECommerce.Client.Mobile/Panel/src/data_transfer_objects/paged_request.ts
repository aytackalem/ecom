export default class PagedRequest {

    Search: string;
    Page: number;
    PageRecordsCount: number;
    Sort: string;
    constructor(search: string, page: number, pageRecordsCount: number, sort: string) {
        this.Search = search;
        this.Page = page;
        this.PageRecordsCount = pageRecordsCount;
        this.Sort = sort;
    }
}