import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';

export async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
        messaging()
            .subscribeToTopic('users')
            .then((test) => {
                console.log('Authorization')
            });

        console.log('Authorization status:', authStatus);
        getFcmToken();
    }
}

const getFcmToken = async () => {
    var fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log(fcmToken, 'old token')
    if (!fcmToken) {
        try {
            fcmToken = await messaging().getToken();
            if (fcmToken) {
                console.log(fcmToken, 'the new token')
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        } catch (error) {
            console.log(error, 'hata')

        }
    }


}

export const notificationListener = async () => {
    messaging().onNotificationOpenedApp(remoteMessage => {
        
        console.log('Notification caused tı ıoen from bacground state:', remoteMessage.notification)
    })
    messaging().sendMessage(async tst => {
        console.log('1 test');
        
    })
    messaging().onMessageSent(async tst => {
        console.log('2 test');
        
    })
    
    messaging()
        .getInitialNotification()
        .then(remoteMessage => {
            
            console.log('3 test');
            if (remoteMessage) {
                
                console.log('Notification caused to open from quit state:', remoteMessage.notification);
            }
        })
}