import PushNotificationIOS from '@react-native-community/push-notification-ios';
import messaging from '@react-native-firebase/messaging';
import React, { useEffect } from 'react';


const ForegroundHandler = () => {
    useEffect(() => {
        const unsubcribe = messaging().onMessage(async remoteMessage => {
            console.log('recived in foreground', remoteMessage);
            const { notification, messageId } = remoteMessage;
            PushNotificationIOS.addNotificationRequest({
                id: messageId,
                body: notification.body,
                title: notification.title,
                sound: 'default'
            })
        })
        return unsubcribe;
    }, [])
    return null;


}

export default ForegroundHandler;