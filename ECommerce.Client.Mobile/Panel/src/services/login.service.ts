import ServiceBase from "./base/service-base";
import { Dispatch } from "redux";
import { AxiosResponse } from "axios";

class LoginService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {

        super(dispatch, tokenSlice);

    }

    Login(username: string, password: string, then: (response: any) => any, error: (reason: any) => any) {

        var client_id = 'PanelForUser';
        var client_secret = 'secret';
        var grant_type = 'password';

        this
            ._httpClientIdentityV2
            .post<any>(
                "/connect/token",
                `client_id=${client_id}&client_secret=${client_secret}&grant_type=${grant_type}&username=${username}&password=${password}`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "*/*"
                    }
                }
            ).then(response => {

                this
                    ._httpClientV2
                    .get<any>(`/Domain`, {
                        headers: {
                            "Content-Type": "application/json",
                            Accept: "application/json",
                            Authorization: `Bearer ${response.data.access_token}`,
                        }
                    })
                    .then(response2 => {

                        if (then && response.data) {

                            then({

                                token: response.data.access_token,
                                refreshToken: response.data.refresh_token,
                                domains: response2.data.data

                            })
                        }
                    })
                    .catch(exception => {

                        if (error) {
                            error(exception);
                        }
                    });

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }


            });


    };

}

export default LoginService;