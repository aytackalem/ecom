import { Dispatch } from "redux";
import HttpClientIdentityV2 from "../../helpers/http-client-identity-v2";
import HttpClientV2 from "../../helpers/http-client-v2";

class ServiceBase {

     public _dispatch: Dispatch<any>;

     public _tokenSlice: any;

     public _httpClientV2: HttpClientV2;

     public _httpClientIdentityV2: HttpClientIdentityV2;

     constructor(dispatch: Dispatch<any>, tokenSlice: any) {

          this._dispatch = dispatch;

          this._tokenSlice = tokenSlice;

          this._httpClientV2 = new HttpClientV2(dispatch, tokenSlice);

          this._httpClientIdentityV2 = new HttpClientIdentityV2();

     }

}

export default ServiceBase;