import PagedRequest from "../data_transfer_objects/paged_request";
import ServiceBase from "./base/service-base";
import { Dispatch } from "redux";
import { AxiosResponse } from "axios";

class ProductService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {

        super(dispatch, tokenSlice);

    }

    ProductServicePost(pagedRequest: PagedRequest, headers: any, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
            ._httpClientV2
            .post("/product/list", pagedRequest, headers).then(response => {

                if (then) {
                    then(response);
                }

            })
            .catch(exception => {

                if (error) {
                    error(exception);
                }
            });
    };

}

export default ProductService;