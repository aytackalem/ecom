import PagedRequest from "../data_transfer_objects/paged_request";
import ServiceBase from "./base/service-base";
import { Dispatch } from "redux";
import { AxiosResponse } from "axios";

class WidgetService extends ServiceBase {

    constructor(dispatch: Dispatch<any>, tokenSlice: any) {

        super(dispatch, tokenSlice);

    }

     WidgetserviceGet(headers: any, then: (response: AxiosResponse<any>) => any, error: (reason: any) => any) {
        this
        ._httpClientV2
        .get("/widget", headers).then(response => {

            if (then) {
                then(response);
            }

        })
        .catch(exception => {

            if (error) {
                error(exception);
            }
        });
    };

}

export default WidgetService;