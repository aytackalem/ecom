import { StyleSheet } from 'react-native';
import ColorVariables from './color-variables';
import DimensionVariables from './dimension-variables';

export default AppStyleRules = StyleSheet.create({

    ColorTurquoise: { color: ColorVariables.Turquoise },
    ColorGray: { color: ColorVariables.Gray },
    ColorBlack: { color: ColorVariables.Black },
    ColorRed: { color: ColorVariables.Red },

    ColorPurple: { color: "#b97fcb" },
    ColorMetalicGray: { color: "#8faabe" },
    ColorYellow: { color: ColorVariables.Yellow },
    ColorWhite: { color: "white" },
    ColorDarkGray: { color: '#5a5d6e' },

    ColorBlur: { color: '#22b9ff' },
    ColorGreen: { color: '#1dc9b7' },
    ColorPink :{ color: '#fd27eb' },

    BackgroundColorTurquoise: { backgroundColor: ColorVariables.Turquoise },
    BackgroundColorGray: { backgroundColor: ColorVariables.Gray },
    BackgroundColorDarkGray: { backgroundColor: ColorVariables.DarkGray },
    BackgroundColorDarkerGray: { backgroundColor: ColorVariables.DarkerGray },
    BackgroundColorRed: { backgroundColor: ColorVariables.Red },
    BackgroundColorBlack: { backgroundColor: ColorVariables.Black },
    BackgroundColorWhiteGray: { backgroundColor: ColorVariables.WhiteGray },
    BackgroundColorBlackGray: { backgroundColor: ColorVariables.BlackGray },
    BackgroundColorBlackYellow: { backgroundColor: ColorVariables.Yellow },
    BackgroundColorBlur: { backgroundColor: ColorVariables.Blur },
    BackgroundColorGreen: { backgroundColor: ColorVariables.Green },
    BackgroundColorWhite: { backgroundColor: ColorVariables.White },

    

    BorderBottomColorRed: { borderBottomColor: ColorVariables.Red },
    BorderBottomColorTurquoise: { borderBottomColor: ColorVariables.Turquoise },
    BorderBottomColorGray: { borderBottomColor: ColorVariables.Gray },

    BorderColorGray: { borderColor: ColorVariables.Gray },
    BorderColorTurquoise: { borderColor: ColorVariables.Turquoise },

    BorderColorPurple: { borderColor: "#b97fcb" },
    BorderColorMetalicGray: { borderColor: "#8faabe" },
    BorderColorYellow: { borderColor: "#dddd7d" },

    TopBarHeight: { height: 50 },

    CalculatedByPercentHeight100WithoutTopBarNavigation: { height: DimensionVariables.CalculatedByPercent.Height100 - 1000 },

});