const ColorVariables = {
    Turquoise: '#20BFCC',
    TurquoiseDark: '#2a929c',
    TurquoiseDarkOpacity0: 'rgba(9, 100, 107, 0)',
    Red: '#FB143B',
    White: '#FFFFFF',
    Gray: '#B7B7B7',
    DarkGray: '#4b4e5a',
    DarkerGray: '#1d202b',
    Black: '#101010',
    BlackOpacity0: 'rgba(0, 0, 0, 0)',
    BlackOpacity05: 'rgba(0, 0, 0, 0, 0.5)',
    WhiteGray: '#f1f1f1',
    BlackGray: '#231d1d',
    Yellow: '#fbce44',
    LightBlue: '#5867dd',
    Blur:'#22b9ff',
    Green:'#1dc9b7'



};

export default ColorVariables;