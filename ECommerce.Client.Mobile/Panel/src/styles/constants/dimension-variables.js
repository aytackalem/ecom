import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DimensionVariables = {
    CalculatedByPercent: {
        Width1: width * 0.01,
        Width5: width * 0.05,
        Width10: width * 0.1,
        Width15: width * 0.15,
        Width20: width * 0.2,
        Width25: width * 0.25,
        Width30: width * 0.3,
        Width35: width * 0.35,
        Width40: width * 0.4,
        Width45: width * 0.45,
        Width50: width * 0.5,
        Width55: width * 0.55,
        Width60: width * 0.6,
        Width65: width * 0.65,
        Width70: width * 0.7,
        Width75: width * 0.75,
        Width80: width * 0.8,
        Width85: width * 0.85,
        Width90: width * 0.9,
        Width95: width * 0.95,
        Width100: width,

        Height5: height * 0.05,
        Height10: height * 0.1,
        Height15: height * 0.15,
        Height17_5: height * 0.175,
        Height20: height * 0.2,
        Height25: height * 0.25,
        Height30: height * 0.3,
        Height35: height * 0.35,
        Height40: height * 0.4,
        Height45: height * 0.45,
        Height50: height * 0.5,
        Height55: height * 0.55,
        Height60: height * 0.6,
        Height65: height * 0.65,
        Height70: height * 0.7,
        Height75: height * 0.75,
        Height80: height * 0.8,
        Height85: height * 0.85,
        Height90: height * 0.9,
        Height95: height * 0.95,
        Height100: height,
    }
};

export default DimensionVariables;