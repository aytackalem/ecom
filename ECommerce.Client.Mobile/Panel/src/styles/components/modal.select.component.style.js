import { Dimensions, StyleSheet } from "react-native";

const ModalSelectComponentStyle = StyleSheet.create({
    Container: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    InnerContainer: {
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15
    }
});

export default ModalSelectComponentStyle;