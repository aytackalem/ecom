import { StyleSheet } from "react-native";

const KeyValueComponentStyle = StyleSheet.create({
    Container: {
        flexDirection: 'row'
    },
    Label: {
        width: 100
    },
    Value: {
        flex: 1
    },
    Text: {
        fontFamily: 'Poppins-Medium'
    }
});

export default KeyValueComponentStyle;