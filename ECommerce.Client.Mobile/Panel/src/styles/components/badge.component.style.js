import { StyleSheet } from "react-native";

const BadgeComponentStyle = StyleSheet.create({
    Container: {
        alignItems: 'center'
    },
    TouchableOpacity: {
    },
    Text: {
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        height: 48,
        borderWidth: 1,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        borderRadius: 4
    },
    Default: {
        color: 'gray',
        borderColor: '#d3d3d3',
        backgroundColor: 'white'
    },
    Warning: {
        color: 'white',
        borderColor: '#ffb822',
        backgroundColor: '#ffb822'
    },
    Info: {
        color: 'white',
        borderColor: '#22b9ff',
        backgroundColor: '#22b9ff'
    },
    Danger: {
        color: 'white',
        borderColor: '#fd27eb',
        backgroundColor: '#fd27eb'
    },
    Success: {
        color: 'white',
        borderColor: '#1dc9b7',
        backgroundColor: '#1dc9b7'
    },
    Extra: {
        color: 'white',
        borderColor: '#5867dd',
        color: '#5867dd'
    }
});

export default BadgeComponentStyle;