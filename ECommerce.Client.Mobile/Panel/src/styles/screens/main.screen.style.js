import { Dimensions, StyleSheet } from "react-native";

const MainScreenStyle = StyleSheet.create({
    Container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    Content: {
        flex: 1
    },
    Filter:{
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        textAlign: 'center',
        textAlignVertical: 'center'
    }
});

export default MainScreenStyle;