import axios from "axios";
import Configuration from "../configuration";

export default class HttpClientIdentityV2 {

    _axiosInstance = axios.create({
        baseURL: Configuration.ApiUrlIdentityServer,
    });

    constructor() {
    }

    get<T>(url: string, headers: any) {
        return this._axiosInstance.get<T>(url, headers);
    }

    post<T>(url: string, data: any, headers: any) {
        return this._axiosInstance.post<T>(url, data, headers);
    }

}