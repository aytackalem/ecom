import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, ScrollView, Image, RefreshControl } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ImageBackgroundComponent from '../components/image.background.component';
import WidgetService, { WidgetserviceGet } from '../services/widget.service';
import AppStyleRules from '../styles/constants/app-style-rules';
import GlobalStyleRules from '../styles/constants/global-style-rules';
import { UpdatePreloader } from '../redux/preloader.slice';
import { UpdateModal } from '../redux/modal.slice';

const MainScreen = ({ navigation }) => {

    const _dispatch = useDispatch();

    const _loginSlice = useSelector((state) => state.LoginSlice);


    const widgetService = new WidgetService(_dispatch, _loginSlice);

    const _selectedDomainId = useSelector((state) => state.LoginSlice.SelectedDomainId);
    const _selectedCompanyId = useSelector((state) => state.LoginSlice.SelectedCompanyId);
    const _token = useSelector((state) => state.LoginSlice.Token);



    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setTimeout(() => {
            Get();
            setRefreshing(false);
        }, 1000);

    }, []);

    const [widget, setWidgets] = useState(null);


    useEffect(() => {

        Get();

    }, [_selectedDomainId, _selectedCompanyId]);


    const Get = () => {
        _dispatch(UpdatePreloader(true));

        widgetService.WidgetserviceGet({ headers: { Authorization: `Bearer ${_token}`, domainId: _selectedDomainId, companyId: _selectedCompanyId } }, response => {

            _dispatch(UpdatePreloader(false));


            if (response.data.success && response.data.data != null) {
                setWidgets(response.data.data);
            }
            else {
                _dispatch(UpdateModal({ IsOpen: true, Text: 'Teknik bir hata oluştu daha sonra tekrar deneyiniz.' }));
            }
        }, exception => {
            _dispatch(UpdatePreloader(false));

        });
    }


    return (
        <ImageBackgroundComponent>
            <ScrollView
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>

                {widget && (
                    <>
                        <View>
                            <View style={[
                                GlobalStyleRules.BorderWidth05,
                                GlobalStyleRules.Padding20,
                                GlobalStyleRules.MarginLeft10,
                                GlobalStyleRules.MarginRight10,
                                GlobalStyleRules.MarginTop10,
                                GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.FlexDirectionRow,
                                GlobalStyleRules.JustifyContentSpaceBetween
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Bugün Toplam Sipariş</Text>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize16,
                                    AppStyleRules.ColorBlur
                                ]}>{widget.orderSummary.ordersCount}</Text>
                            </View>
                            <View style={[
                                GlobalStyleRules.BorderWidth05,
                                GlobalStyleRules.Padding20,
                                GlobalStyleRules.MarginLeft10,
                                GlobalStyleRules.MarginRight10,
                                GlobalStyleRules.MarginTop10,
                                GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.FlexDirectionRow,
                                GlobalStyleRules.JustifyContentSpaceBetween
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Bugün Paketlenen Siparişler</Text>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize16,
                                    AppStyleRules.ColorGreen
                                ]}>{widget.orderSummary.packedOrdersCount}</Text>
                            </View>
                            <View style={[
                                GlobalStyleRules.BorderWidth05,
                                GlobalStyleRules.Padding20,
                                GlobalStyleRules.MarginLeft10,
                                GlobalStyleRules.MarginRight10,
                                GlobalStyleRules.MarginTop10,
                                GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.FlexDirectionRow,
                                GlobalStyleRules.JustifyContentSpaceBetween
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Paketlenmeyen Siparişler</Text>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                    GlobalStyleRules.FontSize16,
                                    AppStyleRules.ColorYellow
                                ]}>{widget.orderSummary.unpackedOrdersCount}</Text>
                            </View>
                        </View>


                        <View style={[
                            GlobalStyleRules.MarginLeft10,
                            GlobalStyleRules.MarginRight10,
                            GlobalStyleRules.MarginTop10,
                        ]}>
                            <View style={[
                                // GlobalStyleRules.BorderWidth05,
                                // GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.Padding20,
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Finansal Özet</Text>
                            </View>

                            <View style={[
                                GlobalStyleRules.MarginLeft10
                            ]}>
                                <View style={[
                                    GlobalStyleRules.Padding10,
                                    GlobalStyleRules.FlexDirectionRow,
                                    GlobalStyleRules.JustifyContentSpaceBetween
                                ]}>
                                    <View>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize14,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Ciro</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Dün {widget.financialSummary.yesterdaySaleSummary.cumulativeSalesAmount} ₺</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            AppStyleRules.ColorBlur
                                        ]}>{widget.financialSummary.todaySaleSummary.cumulativeSalesAmount} ₺</Text>
                                    </View>
                                    <View style={[
                                        GlobalStyleRules.AlignSelfFlexEnd
                                    ]}>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            widget.financialSummary.salesAmountComparePossitive ? AppStyleRules.ColorGreen : AppStyleRules.ColorPink
                                        ]}>
                                            %{widget.financialSummary.salesAmountCompare}
                                        </Text>
                                    </View>
                                </View>
                                <View style={[
                                    GlobalStyleRules.Padding10,
                                    GlobalStyleRules.FlexDirectionRow,
                                    GlobalStyleRules.JustifyContentSpaceBetween
                                ]}>
                                    <View>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize14,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Maliyet</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Dün {widget.financialSummary.todaySaleSummary.costAmount} ₺</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            AppStyleRules.ColorPink
                                        ]}>{widget.financialSummary.yesterdaySaleSummary.costAmount} ₺</Text>
                                    </View>
                                    <View style={[
                                        GlobalStyleRules.AlignSelfFlexEnd
                                    ]}>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            widget.financialSummary.costAmountComparePossitive ? AppStyleRules.ColorGreen : AppStyleRules.ColorPink
                                        ]}>
                                            %{widget.financialSummary.costAmountCompare}
                                        </Text>
                                    </View>
                                </View>
                                <View style={[
                                    GlobalStyleRules.Padding10,
                                    GlobalStyleRules.FlexDirectionRow,
                                    GlobalStyleRules.JustifyContentSpaceBetween
                                ]}>
                                    <View>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize14,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Kar</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Dün: {widget.financialSummary.yesterdaySaleSummary.cumulativeProfitAmount} ₺</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            AppStyleRules.ColorGreen
                                        ]}>{widget.financialSummary.todaySaleSummary.cumulativeProfitAmount} ₺</Text>
                                    </View>
                                    <View style={[
                                        GlobalStyleRules.AlignItemsFlexEnd
                                    ]}>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize14,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Karlılık Oranı</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray,
                                            GlobalStyleRules.MarginBottom5
                                        ]}>Dün: %{widget.financialSummary.yesterdaySaleSummary.cumulativeProfitRate}</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize16,
                                            AppStyleRules.ColorGreen
                                        ]}>
                                            %{widget.financialSummary.todaySaleSummary.cumulativeProfitRate}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>


                        <View style={[
                            GlobalStyleRules.MarginLeft10,
                            GlobalStyleRules.MarginRight20,
                            GlobalStyleRules.MarginTop10,
                        ]}>
                            <View style={[
                                GlobalStyleRules.BorderBottomWidth1,
                                GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.Padding20,
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Pazaryeri Sipariş Sayıları</Text>
                            </View>
                            <View style={[
                                GlobalStyleRules.MarginLeft10
                            ]}>
                                {
                                    (widget.marketplaceCounts && widget.marketplaceCounts.length > 0) && (
                                        widget.marketplaceCounts.map(mp => {
                                            return <View style={[
                                                GlobalStyleRules.Padding10,
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.JustifyContentSpaceBetween,
                                                GlobalStyleRules.BorderBottomWidth1,
                                                GlobalStyleRules.BorderColorWhiteGray
                                            ]}>
                                                <View>
                                                    <Image
                                                        style={[
                                                            GlobalStyleRules.Width60,
                                                            GlobalStyleRules.Height30,
                                                            GlobalStyleRules.ResizeModeContain
                                                        ]}
                                                        source={{ uri: `https://erp.helpy.com.tr/white/img/marketplace-icons/${mp.marketplaceId}.png` }}
                                                    />
                                                </View>
                                                <View style={[
                                                    GlobalStyleRules.AlignItemsFlexEnd,
                                                ]}>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsRegular,
                                                        GlobalStyleRules.FontSize12
                                                    ]}>
                                                        {mp.count} Adet
                                                    </Text>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsRegular,
                                                        GlobalStyleRules.FontSize16,
                                                        AppStyleRules.ColorBlur
                                                    ]}>
                                                        {mp.salesAmount} ₺
                                                    </Text>
                                                </View>
                                            </View>
                                        })
                                    )
                                }
                            </View>
                        </View>

                        <View style={[
                            GlobalStyleRules.MarginLeft10,
                            GlobalStyleRules.MarginRight10,
                            GlobalStyleRules.MarginTop10,
                        ]}>
                            <View style={[
                                GlobalStyleRules.BorderBottomWidth1,
                                GlobalStyleRules.BorderColorWhiteGray,
                                GlobalStyleRules.Padding20,
                            ]}>
                                <Text style={[
                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                    GlobalStyleRules.FontSize14,
                                    AppStyleRules.ColorDarkGray
                                ]}>Pazaryeri Aktif Ürün Sayıları</Text>
                            </View>
                            <View style={[
                                GlobalStyleRules.MarginLeft10
                            ]}>
                                {
                                    (widget.marketplaceProductCounts && widget.marketplaceProductCounts.length > 0) && (

                                        widget.marketplaceProductCounts.map(mpc => {

                                            return <View style={[
                                                GlobalStyleRules.Padding10,
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.JustifyContentSpaceBetween,
                                                GlobalStyleRules.BorderBottomWidth1,
                                                GlobalStyleRules.BorderColorWhiteGray
                                            ]}>
                                                <View>
                                                    <Image
                                                        style={[
                                                            GlobalStyleRules.Width60,
                                                            GlobalStyleRules.Height30,
                                                            GlobalStyleRules.ResizeModeContain
                                                        ]}
                                                        source={{ uri: `https://erp.helpy.com.tr/white/img/marketplace-icons/${mpc.marketplaceId}.png` }}
                                                    />
                                                </View>
                                                <View style={[
                                                    GlobalStyleRules.AlignItemsFlexEnd,
                                                ]}>

                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsRegular,
                                                        GlobalStyleRules.FontSize16,
                                                        AppStyleRules.ColorBlur
                                                    ]}>
                                                        {mpc.count}
                                                    </Text>
                                                </View>
                                            </View>
                                        })
                                    )
                                }


                            </View>
                        </View>
                    </>
                )}




            </ScrollView>

        </ImageBackgroundComponent>
    )
};

export default MainScreen;