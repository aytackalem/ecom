import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, FlatList, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ImageBackgroundComponent from '../components/image.background.component';
import AppStyleRules from '../styles/constants/app-style-rules';
import GlobalStyleRules from '../styles/constants/global-style-rules';
import OrderScreenStyle from '../styles/screens/order.screen.style';
import Clipboard from "@react-native-community/clipboard";
import { UpdatePreloader } from '../redux/preloader.slice';
import OrderService, { OrderServicePost, OrderSummaryGet } from '../services/order.service';
import PagedRequest from '../data_transfer_objects/paged_request';
import { UpdateModal } from '../redux/modal.slice';
import Barcode from "react-native-barcode-builder";
import ModalSelectComponent from '../components/modal.select.component';
import Toast from 'react-native-toast-message'

const OrderScreen = ({ navigation }) => {

    const _dispatch = useDispatch();
    const _loginSlice = useSelector((state) => state.LoginSlice);

    const orderService = new OrderService(_dispatch, _loginSlice)


    const _selectedDomainId = useSelector((state) => state.LoginSlice.SelectedDomainId);
    const _selectedCompanyId = useSelector((state) => state.LoginSlice.SelectedCompanyId);
    const _token = useSelector((state) => state.LoginSlice.Token);

    const [refreshing, setRefreshing] = useState(false);
    const [currentPage, setCurrentPage] = useState(0);
    const [order, setOrders] = useState({ orders: [], page: 0, pagesCount: 0 });
    const [summary, setSummary] = useState(null);



    useEffect(() => {

        setOrders({ orders: [], page: 0, pagesCount: 0 });
        Get();

    }, [_selectedDomainId, _selectedCompanyId])



    useEffect(() => {
        Post();
    }, [currentPage, _selectedDomainId, _selectedCompanyId])

    const Post = () => {
        _dispatch(UpdatePreloader(true));

        orderService.OrderServicePost(new PagedRequest('', currentPage, 10, 'Id'), { headers: { Authorization: `Bearer ${_token}`, domainId: _selectedDomainId, companyId: _selectedCompanyId} }, response => {

            _dispatch(UpdatePreloader(false));

            if (response.data.success && response.data.data != null) {

                setOrders(prevState => ({
                    orders: [
                        ...prevState.orders,
                        ...response.data.data
                    ],
                    page: response.data.page,
                    pagesCount: response.data.pagesCount
                }));

            }
            else {
                _dispatch(UpdateModal({ IsOpen: true, Text: 'Teknik bir hata oluştu daha sonra tekrar deneyiniz.' }));
            }
        }, exception => {

        });
    }

    const Get = () => {


        orderService.OrderSummaryGet({ headers: { Authorization: `Bearer ${_token}`, domainId: _selectedDomainId, companyId: _selectedCompanyId } }, response => {

            if (response.data.success && response.data.data != null) {

                setSummary(response.data.data);

            }

        }, exception => {

        });

    }

    const handleRefresh = () => {

        setRefreshing(true);
        setTimeout(() => {
            setCurrentPage(0);

            setOrders({ orders: [], page: 0, pagesCount: 0 });
            Get();
            Post();
            setRefreshing(false);
        }, 1000);

    };

    const handleLoadMore = () => {

        if (order.page <= currentPage && order.pagesCount >= currentPage) {
            setCurrentPage(currentPage + 1);

        }

    }


    const toastCopy = (text) => {
        Toast.show({
            type: 'info',
            text1: text,
            text2: 'Kopyalandı',
            visibilityTime: 2000
        });
    }

    return (

        <ImageBackgroundComponent>


            {


                order.orders.length > 0 && (

                    <FlatList
                        data={order.orders}
                        showsHorizontalScrollIndicator={false}
                        refreshing={refreshing}
                        onRefresh={handleRefresh}
                        onEndReached={handleLoadMore}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {

                            return <>
                                {
                                    index == 0 && summary && (
                                        <View>
                                            <View style={[
                                                GlobalStyleRules.BorderWidth05,
                                                GlobalStyleRules.Padding20,
                                                GlobalStyleRules.MarginLeft10,
                                                GlobalStyleRules.MarginRight10,
                                                GlobalStyleRules.MarginTop10,
                                                GlobalStyleRules.BorderColorWhiteGray,
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.JustifyContentSpaceBetween
                                            ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize14,
                                                    AppStyleRules.ColorDarkGray
                                                ]}>Bugün Toplam Sipariş</Text>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize16,
                                                    AppStyleRules.ColorBlur
                                                ]}>{summary.todayOrdersCount}</Text>
                                            </View>
                                            <View style={[
                                                GlobalStyleRules.BorderWidth05,
                                                GlobalStyleRules.Padding20,
                                                GlobalStyleRules.MarginLeft10,
                                                GlobalStyleRules.MarginRight10,
                                                GlobalStyleRules.MarginTop10,
                                                GlobalStyleRules.BorderColorWhiteGray,
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.JustifyContentSpaceBetween
                                            ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize14,
                                                    AppStyleRules.ColorDarkGray
                                                ]}>Bugün Paketlenen Siparişler</Text>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize16,
                                                    AppStyleRules.ColorGreen
                                                ]}>{summary.packedOrdersCount}</Text>
                                            </View>
                                            <View style={[
                                                GlobalStyleRules.BorderWidth05,
                                                GlobalStyleRules.Padding20,
                                                GlobalStyleRules.MarginLeft10,
                                                GlobalStyleRules.MarginRight10,
                                                GlobalStyleRules.MarginTop10,
                                                GlobalStyleRules.BorderColorWhiteGray,
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.JustifyContentSpaceBetween
                                            ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize14,
                                                    AppStyleRules.ColorDarkGray
                                                ]}>Paketlenmeyen Siparişler</Text>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize16,
                                                    AppStyleRules.ColorYellow
                                                ]}>{summary.unpackedOrdersCount}</Text>
                                            </View>
                                        </View>
                                    )
                                }
                                <View style={[
                                    GlobalStyleRules.BorderWidth05,
                                    GlobalStyleRules.Padding10,
                                    GlobalStyleRules.MarginLeft10,
                                    GlobalStyleRules.MarginRight10,
                                    GlobalStyleRules.MarginTop10,
                                    GlobalStyleRules.BorderColorWhiteGray
                                ]}>
                                    <View style={[
                                        GlobalStyleRules.FlexDirectionRow,
                                        GlobalStyleRules.JustifyContentSpaceBetween
                                    ]}>
                                        <View>
                                            <Image
                                                style={[
                                                    GlobalStyleRules.Width120,
                                                    GlobalStyleRules.Height60,
                                                    GlobalStyleRules.ResizeModeContain
                                                ]}
                                                source={{ uri: `https://erp.helpy.com.tr/white/img/marketplace-icons/${item.marketPlaceId}.png` }}
                                            />
                                            <TouchableOpacity onPress={() => {
                                                Clipboard.setString(item.marketplaceOrderNumber);
                                                toastCopy(item.marketplaceOrderNumber);
                                            }}
                                                style={[
                                                    GlobalStyleRules.FlexDirectionRow,
                                                    GlobalStyleRules.AlignItemsCenter
                                                ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                                    GlobalStyleRules.FontSize12,
                                                    GlobalStyleRules.ColorLightBlue
                                                ]}>{item.marketplaceOrderNumber}</Text>
                                                <Image
                                                    style={[
                                                        GlobalStyleRules.Width15,
                                                        GlobalStyleRules.Height15,
                                                        GlobalStyleRules.ResizeModeContain
                                                    ]}
                                                    source={require('../images/global_icons/copy-icon.png')}
                                                />
                                            </TouchableOpacity>
                                            <Text style={[
                                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                                GlobalStyleRules.FontSize12
                                            ]}>{item.customer.name} {item.customer.surname}</Text>
                                            <Text style={[
                                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                                GlobalStyleRules.FontSize10
                                            ]}>{item.orderType.name}</Text>
                                            <Text style={[
                                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                                GlobalStyleRules.FontSize10
                                            ]}>{item.createdDateStr}
                                            </Text>
                                        </View>
                                        <View style={[
                                            GlobalStyleRules.AlignItemsFlexEnd
                                        ]}>

                                            <Image
                                                style={[
                                                    GlobalStyleRules.Width120,
                                                    GlobalStyleRules.Height60,
                                                    GlobalStyleRules.ResizeModeContain
                                                ]}
                                                source={{ uri: `https://erp.helpy.com.tr/white/img/shipment-company-icons/${item.orderShipments[0].shipmentCompanyId}.png` }}
                                            />
                                            <TouchableOpacity onPress={() => {

                                                Clipboard.setString(item.orderShipments[0].trackingCode);
                                                toastCopy(item.orderShipments[0].trackingCode);
                                            }}
                                                style={[
                                                    GlobalStyleRules.FlexDirectionRow,
                                                    GlobalStyleRules.AlignItemsCenter
                                                ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                                    GlobalStyleRules.FontSize12,
                                                    GlobalStyleRules.AlignSelfFlexEnd,
                                                    GlobalStyleRules.ColorLightBlue
                                                ]}>{item.orderShipments[0].trackingCode}</Text>
                                                <Image
                                                    style={[
                                                        GlobalStyleRules.Width15,
                                                        GlobalStyleRules.Height15,
                                                        GlobalStyleRules.ResizeModeContain
                                                    ]}
                                                    source={require('../images/global_icons/copy-icon.png')}
                                                />
                                            </TouchableOpacity>

                                            <Text style={[
                                                GlobalStyleRules.FontFamilyPoppinsBold,
                                                GlobalStyleRules.FontSize12,
                                                GlobalStyleRules.AlignSelfFlexEnd
                                            ]}>{item.amount} TL</Text>
                                            <Text style={[
                                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                                GlobalStyleRules.FontSize10,
                                                GlobalStyleRules.AlignSelfFlexEnd
                                            ]}>{item.totalQuantity} Ürün</Text>

                                            {
                                                item.orderTypeId != 'IP' && (
                                                    <TouchableOpacity style={[
                                                        GlobalStyleRules.Padding10,
                                                        AppStyleRules.BackgroundColorTurquoise
                                                    ]}
                                                        onPress={() => {
                                                            navigation.navigate('Barcode', { order: item });

                                                        }}
                                                    >
                                                        <Text style={[
                                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                                            GlobalStyleRules.FontSize10
                                                        ]}>Barkod Göster</Text>
                                                    </TouchableOpacity>
                                                )
                                            }

                                        </View>
                                    </View>
                                    {item.orderDetails.map(od => {
                                        return <View style={[
                                            GlobalStyleRules.FlexDirectionRow,
                                            GlobalStyleRules.JustifyContentSpaceBetween,
                                            GlobalStyleRules.AlignItemsCenter,
                                            GlobalStyleRules.MarginTop10
                                        ]}>
                                            <View>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize10
                                                ]}>{od.quantity} Adet</Text>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize10
                                                ]}>Kalan Stok {od.productInformation.stock} Adet</Text>
                                            </View>
                                            <View>
                                                <Image
                                                    style={[
                                                        GlobalStyleRules.Width100,
                                                        GlobalStyleRules.Height100,
                                                        GlobalStyleRules.ResizeModeContain
                                                    ]}
                                                    source={{ uri: od.productInformation.productInformationPhotos[0].fileName }}
                                                />
                                            </View>
                                            <View style={[
                                                GlobalStyleRules.Flex1
                                            ]}>
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsRegular,
                                                    GlobalStyleRules.FontSize12
                                                ]}>{od.productInformation.productInformationName} </Text>

                                                <TouchableOpacity onPress={() => {
                                                    Clipboard.setString(od.productInformation.stockCode);
                                                    toastCopy(od.productInformation.stockCode);
                                                }}
                                                    style={[
                                                        GlobalStyleRules.FlexDirectionRow,
                                                        GlobalStyleRules.AlignItemsCenter
                                                    ]}>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsBold,
                                                        GlobalStyleRules.FontSize12,
                                                        GlobalStyleRules.ColorLightBlue
                                                    ]}>{od.productInformation.stockCode}</Text>
                                                    <Image
                                                        style={[
                                                            GlobalStyleRules.Width15,
                                                            GlobalStyleRules.Height15,
                                                            GlobalStyleRules.ResizeModeContain
                                                        ]}
                                                        source={require('../images/global_icons/copy-icon.png')}
                                                    />
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => {
                                                    Clipboard.setString(od.productInformation.barcode)
                                                    toastCopy(od.productInformation.barcode);
                                                }}
                                                    style={[
                                                        GlobalStyleRules.FlexDirectionRow,
                                                        GlobalStyleRules.AlignItemsCenter
                                                    ]}>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsBold,
                                                        GlobalStyleRules.FontSize12,
                                                        GlobalStyleRules.ColorLightBlue
                                                    ]}>{od.productInformation.barcode}</Text>
                                                    <Image
                                                        style={[
                                                            GlobalStyleRules.Width15,
                                                            GlobalStyleRules.Height15,
                                                            GlobalStyleRules.ResizeModeContain
                                                        ]}
                                                        source={require('../images/global_icons/copy-icon.png')}
                                                    />
                                                </TouchableOpacity>

                                            </View>
                                            <View >
                                                <Text style={[
                                                    GlobalStyleRules.FontFamilyPoppinsBold,
                                                    GlobalStyleRules.FontSize12,
                                                    GlobalStyleRules.AlignSelfFlexEnd
                                                ]}>{od.unitPrice} TL</Text>
                                            </View>
                                        </View>
                                    })}
                                </View>
                            </>
                        }}
                    />

                )

            }

            <Toast
                position='bottom'
                bottomOffset={20}
            />
        </ImageBackgroundComponent >
    )
};

export default OrderScreen;