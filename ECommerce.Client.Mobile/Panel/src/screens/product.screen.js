import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, Linking, TouchableOpacity, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ImageBackgroundComponent from '../components/image.background.component';
import AppStyleRules from '../styles/constants/app-style-rules';
import GlobalStyleRules from '../styles/constants/global-style-rules';
import PagedRequest from '../data_transfer_objects/paged_request';
import { UpdatePreloader } from '../redux/preloader.slice';
import ProductService, { ProductServicePost } from '../services/product.service';


const ProductScreen = ({ navigation }) => {

    const _dispatch = useDispatch();
    const _selectedDomainId = useSelector((state) => state.LoginSlice.SelectedDomainId);
    const _selectedCompanyId = useSelector((state) => state.LoginSlice.SelectedCompanyId);
    const _loginSlice = useSelector((state) => state.LoginSlice);

    const productService = new ProductService(_dispatch, _loginSlice)




    const _token = useSelector((state) => state.LoginSlice.Token);
    const [currentPage, setCurrentPage] = useState(0);
    const [result, setResult] = useState({ products: [], page: 0, pagesCount: 0 });





    useEffect(() => {
debugger
        Post();

    }, [currentPage, _selectedDomainId, _selectedCompanyId])




    useEffect(() => {

        setResult({ products: [], page: 0, pagesCount: 0 });
    }, [_selectedDomainId, _selectedCompanyId])

    const Post = () => {
        _dispatch(UpdatePreloader(true));

        productService.ProductServicePost(new PagedRequest('', currentPage, 10, 'Id'), { headers: { Authorization: `Bearer ${_token}`, domainId: _selectedDomainId, companyId: _selectedCompanyId } }, response => {
            _dispatch(UpdatePreloader(false));

debugger
            if (response.data.success && response.data.data != null) {


                setResult(prevState => ({
                    products: [
                        ...prevState.products,
                        ...response.data.data
                    ],
                    page: response.data.page,
                    pagesCount: response.data.pagesCount
                }));

            }
            else {
                _dispatch(UpdateModal({ IsOpen: true, Text: 'Teknik bir hata oluştu daha sonra tekrar deneyiniz.' }));
            }
        }, exception => {
            _dispatch(UpdatePreloader(false));

        });
    }



    const handleLoadMore = () => {

        if (result.page <= currentPage && result.pagesCount >= currentPage) {
            setCurrentPage(currentPage + 1);

        }

    }


    function handlePress(url) {
        if (url)
            Linking.canOpenURL(url).then((supported) => {
                return Linking.openURL(url);
            })
    }


    return (
        <ImageBackgroundComponent>

            {
                result.products.length > 0 && (

                    <FlatList
                        data={result.products}
                        showsHorizontalScrollIndicator={false}
                        onEndReached={handleLoadMore}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {
                            return <View style={[
                                GlobalStyleRules.MarginLeft5,
                                GlobalStyleRules.MarginRight5,
                                GlobalStyleRules.MarginTop10
                            ]}>

                                <View style={[
                                    GlobalStyleRules.FlexDirectionRow
                                ]}>
                                    <View style={[

                                    ]}>
                                        <Image
                                            style={[
                                                GlobalStyleRules.Width60,
                                                GlobalStyleRules.Height60,
                                                GlobalStyleRules.ResizeModeContain
                                            ]}
                                            source={{ uri: item.productInformations[0].productInformationPhotos[0].fileName }}
                                        />
                                    </View>

                                    <View style={[
                                        GlobalStyleRules.JustifyContentSpaceBetween
                                    ]}>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsBold,
                                            GlobalStyleRules.FontSize12
                                        ]}>{item.productInformations[0].productInformationName}</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray
                                        ]}>{item.productInformations[0].barcode} / {item.productInformations[0].stockCode}</Text>
                                        <Text style={[
                                            GlobalStyleRules.FontFamilyPoppinsRegular,
                                            GlobalStyleRules.FontSize12,
                                            AppStyleRules.ColorDarkGray
                                        ]}>{item.totalStock} Adet {item.variantsCount} Varyant</Text>
                                    </View>

                                </View>
                                <View style={[

                                    // GlobalStyleRules.JustifyContentSpaceAround,

                                    {
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        justifyContent: 'flex-start'
                                    }
                                ]}>

                                    {
                                        item.productInformations[0].productInformationMarketplaces.map(pm => {
                                            return <TouchableOpacity style={[
                                                GlobalStyleRules.FlexDirectionRow,
                                                GlobalStyleRules.AlignItemsCenter,
                                                pm.url ? AppStyleRules.BackgroundColorGreen :
                                                    (pm.active ? AppStyleRules.BackgroundColorBlackYellow : AppStyleRules.BackgroundColorRed),
                                                GlobalStyleRules.JustifyContentSpaceAround,
                                                GlobalStyleRules.BorderRadius10,
                                                GlobalStyleRules.MarginTop10,
                                                { width: '33%', marginRight: 0.5, marginLeft: 0.5 }
                                            ]}
                                                onPress={() => handlePress(pm.url)}
                                                disabled={!pm.url}>
                                                <View>
                                                    <Image
                                                        style={[
                                                            GlobalStyleRules.Width30,
                                                            GlobalStyleRules.Height30,
                                                            GlobalStyleRules.ResizeModeContain,

                                                        ]}
                                                        source={{ uri: `https://erp.helpy.com.tr/white/img/marketplace-icons/${pm.marketplaceId}.png` }}
                                                    />
                                                </View>
                                                <View>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsRegular,
                                                        GlobalStyleRules.FontSize8,
                                                        AppStyleRules.ColorDarkGray
                                                    ]}>Liste Fiyatı: {pm.listUnitPrice} ₺</Text>
                                                    <Text style={[
                                                        GlobalStyleRules.FontFamilyPoppinsRegular,
                                                        GlobalStyleRules.FontSize8,
                                                        AppStyleRules.ColorDarkGray
                                                    ]}>Satış Fiyatı: {pm.unitPrice} ₺</Text>
                                                </View>
                                            </TouchableOpacity>
                                        })
                                    }



                                </View>


                            </View>


                        }}
                    />
                )
            }




        </ImageBackgroundComponent >
    )
};

export default ProductScreen;