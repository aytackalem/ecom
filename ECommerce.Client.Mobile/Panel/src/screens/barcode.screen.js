import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ImageBackgroundComponent from '../components/image.background.component';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import Barcode from 'react-native-barcode-builder';
import AppStyleRules from '../styles/constants/app-style-rules';
import GlobalStyleRules from '../styles/constants/global-style-rules';
import BackButtonComponent from '../components/back-button.component';
import Clipboard from "@react-native-community/clipboard";
import Toast from 'react-native-toast-message'


const BarcodeScreen = ({ route, navigation }) => {
    const _dispatch = useDispatch();

    const windowWidth = Dimensions.get('window').width;

    const toastCopy = (text) => {
        Toast.show({
            type: 'info',
            text1: text,
            text2: 'Kopyalandı',
            visibilityTime: 2000
        });
    }

    return (<ImageBackgroundComponent>


        <BackButtonComponent
            onPress={() => navigation.goBack()}
        />

        <View style={[
            GlobalStyleRules.BorderWidth05,
            GlobalStyleRules.Padding10,
            GlobalStyleRules.MarginLeft10,
            GlobalStyleRules.MarginRight10,
            GlobalStyleRules.MarginTop10,
            GlobalStyleRules.BorderColorWhiteGray
        ]}>
            <View style={[
                GlobalStyleRules.FlexDirectionRow,
                GlobalStyleRules.JustifyContentSpaceBetween
            ]}>
                <View>
                    <Image
                        style={[
                            GlobalStyleRules.Width120,
                            GlobalStyleRules.Height60,
                            GlobalStyleRules.ResizeModeContain
                        ]}
                        source={{ uri: `https://erp.helpy.com.tr/white/img/marketplace-icons/${route.params.order.marketPlaceId}.png` }}
                    />
                    <TouchableOpacity onPress={() => {
                        Clipboard.setString(route.params.order.marketplaceOrderNumber)
                        toastCopy(route.params.order.marketplaceOrderNumber);

                    }}
                        style={[
                            GlobalStyleRules.FlexDirectionRow,
                            GlobalStyleRules.AlignItemsCenter
                        ]}>
                        <Text style={[
                            GlobalStyleRules.FontFamilyPoppinsBold,
                            GlobalStyleRules.FontSize12,
                            GlobalStyleRules.ColorLightBlue
                        ]}>{route.params.order.marketplaceOrderNumber}</Text>
                        <Image
                            style={[
                                GlobalStyleRules.Width15,
                                GlobalStyleRules.Height15,
                                GlobalStyleRules.ResizeModeContain
                            ]}
                            source={require('../images/global_icons/copy-icon.png')}
                        />
                    </TouchableOpacity>
                    <Text style={[
                        GlobalStyleRules.FontFamilyPoppinsRegular,
                        GlobalStyleRules.FontSize12
                    ]}>{route.params.order.customer.name} {route.params.order.customer.surname}</Text>
                    <Text style={[
                        GlobalStyleRules.FontFamilyPoppinsRegular,
                        GlobalStyleRules.FontSize10
                    ]}>{route.params.order.orderType.name}</Text>
                    <Text style={[
                        GlobalStyleRules.FontFamilyPoppinsRegular,
                        GlobalStyleRules.FontSize10
                    ]}>{route.params.order.createdDateStr}
                    </Text>
                </View>
                <View style={[
                    GlobalStyleRules.AlignItemsFlexEnd
                ]}>

                    <Image
                        style={[
                            GlobalStyleRules.Width120,
                            GlobalStyleRules.Height60,
                            GlobalStyleRules.ResizeModeContain
                        ]}
                        source={{ uri: `https://erp.helpy.com.tr/white/img/shipment-company-icons/${route.params.order.orderShipments[0].shipmentCompanyId}.png` }}
                    />
                    <TouchableOpacity onPress={() => {
                        Clipboard.setString(route.params.order.orderShipments[0].trackingCode)
                        toastCopy(route.params.order.orderShipments[0].trackingCode);

                    }}
                        style={[
                            GlobalStyleRules.FlexDirectionRow,
                            GlobalStyleRules.AlignItemsCenter
                        ]}>
                        <Text style={[
                            GlobalStyleRules.FontFamilyPoppinsBold,
                            GlobalStyleRules.FontSize12,
                            GlobalStyleRules.AlignSelfFlexEnd,
                            GlobalStyleRules.ColorLightBlue
                        ]}>{route.params.order.orderShipments[0].trackingCode}</Text>
                        <Image
                            style={[
                                GlobalStyleRules.Width15,
                                GlobalStyleRules.Height15,
                                GlobalStyleRules.ResizeModeContain
                            ]}
                            source={require('../images/global_icons/copy-icon.png')}
                        />
                    </TouchableOpacity>

                    <Text style={[
                        GlobalStyleRules.FontFamilyPoppinsBold,
                        GlobalStyleRules.FontSize12,
                        GlobalStyleRules.AlignSelfFlexEnd
                    ]}>{route.params.order.amount} TL</Text>
                    <Text style={[
                        GlobalStyleRules.FontFamilyPoppinsRegular,
                        GlobalStyleRules.FontSize10,
                        GlobalStyleRules.AlignSelfFlexEnd
                    ]}>{route.params.order.totalQuantity} Ürün</Text>


                </View>
            </View>
        </View>
        <View style={[
            GlobalStyleRules.AlignItemsCenter
        ]}>
            <Barcode text={route.params.order.orderShipments[0].trackingCode} value={route.params.order.orderShipments[0].trackingCode} format="CODE128" />


        </View>

        <Toast
                position='bottom'
                bottomOffset={20}
            />
    </ImageBackgroundComponent>
    )
};

export default BarcodeScreen;