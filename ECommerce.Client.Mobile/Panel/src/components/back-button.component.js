import React from 'react';
import { Component } from "react";
import { TouchableOpacity, Text, Image } from "react-native";
import GlobalStyleRules from '../styles/constants/global-style-rules';

export default class BackButtonComponent extends Component {
    render() {
        return (
            <TouchableOpacity
                style={[
                    GlobalStyleRules.MarginTop10
                ]}
                onPress={this.props.onPress}>
                <Image
                    style={{
                        height: 30,
                        resizeMode: 'contain'
                    }}
                    source={require("../images/global_icons/back-icon.png")}

                />
            </TouchableOpacity>
        )
    }
}