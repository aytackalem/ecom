import React from "react";
import { Text, View } from "react-native";
import BadgeComponentTypes from "../enums/badge.component.types";
import BadgeComponentStyle from "../styles/components/badge.component.style";

const BadgeComponent = (props) => {
    const RenderStyle = () => {
        if (props.type == BadgeComponentTypes.Default) {
            return BadgeComponentStyle.Default;
        }
        else if (props.type == BadgeComponentTypes.Danger) {
            return BadgeComponentStyle.Danger;
        }
        else if (props.type == BadgeComponentTypes.Info) {
            return BadgeComponentStyle.Info;
        }
        else if (props.type == BadgeComponentTypes.Warning) {
            return BadgeComponentStyle.Warning;
        }
        else if (props.type == BadgeComponentTypes.Extra) {
            return BadgeComponentStyle.Extra;
        }
        else if (props.type == BadgeComponentTypes.Success) {
            return BadgeComponentStyle.Success;
        }
    }

    return (
        <View style={BadgeComponentStyle.Container}>
            <Text style={[BadgeComponentStyle.Text, RenderStyle()]}>
                {props.text}
            </Text>
        </View>
    )
}

export default BadgeComponent;