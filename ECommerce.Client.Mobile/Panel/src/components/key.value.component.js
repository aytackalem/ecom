import React from 'react';
import { Text, View } from "react-native";
import TextTicker from 'react-native-text-ticker';
import KeyValueComponentStyle from '../styles/components/key.value.component.style';

const KeyValueComponent = (props) => {
    return (
        <View style={KeyValueComponentStyle.Container}>
            <View style={KeyValueComponentStyle.Label}>
                <Text style={KeyValueComponentStyle.Text}>{props.label}</Text>
            </View>
            <View style={KeyValueComponentStyle.Value}>
                {
                    props.value.length > 30 && <>
                        <TextTicker
                            style={KeyValueComponentStyle.Text}
                            duration={4000}
                            loop
                            bounce
                            repeatSpacer={50}
                            marqueeDelay={5000}
                        >
                            {props.value}
                        </TextTicker>
                    </> || <>
                        <Text
                            style={KeyValueComponentStyle.Text}
                        >
                            {props.value}
                        </Text>
                    </>
                }
            </View>
        </View>
    )
}

export default KeyValueComponent;