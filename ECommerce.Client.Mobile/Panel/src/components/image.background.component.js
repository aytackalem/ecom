import React from 'react';
import { ImageBackground } from "react-native";
import ImageBackgroundComponentStyle from '../styles/components/image.background.component.style';

const ImageBackgroundComponent = (props) => {
    return (
        <ImageBackground style={ImageBackgroundComponentStyle.Container} source={require('../images/backgrounds/bg-3.jpg')}>
            {props.children}
        </ImageBackground>
    )
}

export default ImageBackgroundComponent;