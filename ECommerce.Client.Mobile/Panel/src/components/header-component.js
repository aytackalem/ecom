import React, { Component, useState } from "react";
import { Image, ImageBackground, Text, TouchableOpacity, View, SafeAreaView } from "react-native";
import appStyleRules from "../styles/constants/app-style-rules";
import AppStyleRules from "../styles/constants/app-style-rules";
import GlobalStyleRules from "../styles/constants/global-style-rules";
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux'
import { DeleteManagerUser, SelectedManager } from '../redux/login.slice';
import PaddingContainerComponent from "./padding.container.component";
import PaddingContainerComponentStyle from "../styles/components/padding.container.component.style";
import PaddingContainerComponentTypes from "../enums/padding.container.component.types";
import InputContainerComponent from '../components/input.container.component';
import ButtonComponentTypes from '../enums/button.component.types';
import ButtonComponent from '../components/button.component';
import ModalSelectComponent from "./modal.select.component";

const HeaderComponent = ({ navigation }) => {


    const _dispatch = useDispatch();

    const _domains = useSelector((state) => state.LoginSlice.Domains);
    const _selectedDomainId = useSelector((state) => state.LoginSlice.SelectedDomainId);
    const _selectedCompanyId = useSelector((state) => state.LoginSlice.SelectedCompanyId);

    const domain = _domains.find(d => d.id == _selectedDomainId);
    const company = domain.companies.find(c => c.id == _selectedCompanyId);

    const [modal, setModal] = useState(false);
    const [DomainSelectionOpen, SetDomainSelectionOpen] = useState(false);
    const [CompanySelectionOpen, SetCompanySelectionOpen] = useState(false);

    const [Domain, SetDomain] = useState({ key: domain.id, value: domain.name });
    const [Company, SetCompany] = useState({ key: company.id, value: company.name });


    const OpenModal = () => {

        setModal(!modal);
    }

    const Selected = () => {
        _dispatch(SelectedManager({
            selectedDomainId: Domain.key,
            selectedCompanyId: Company.key
        }));
    };

    const Logout = () => {

        setModal(false);
        _dispatch(DeleteManagerUser());

    }


    return (
        <SafeAreaView>
            <Modal
                testID={'modal'}
                isVisible={modal}
                onSwipeComplete={OpenModal}
                swipeDirection={['down']}
                style={
                    [
                        GlobalStyleRules.Margin0,
                        GlobalStyleRules.JustifyContentFlexEnd,
                        GlobalStyleRules.Flex1
                    ]}>
                <SafeAreaView style={[
                    AppStyleRules.BackgroundColorWhite
                ]}>
                    {

                        DomainSelectionOpen && (<>
                            <ModalSelectComponent onChange={(item) => {
                                SetDomainSelectionOpen(false);
                                SetDomain(item);
                                
                            }} data={_domains.map(item => { return { key: item.id, value: item.name } })} />
                        </>)
                    }

                    {
                        CompanySelectionOpen && (<>
                            <ModalSelectComponent onChange={(item) => {
                                SetCompanySelectionOpen(false);
                                SetCompany(item);
                               
                            }} data={_domains.find(value => value.id == Domain.key).companies.map(item => { return { key: item.id, value: item.name } })} />
                        </>)
                    }

                    <View style={
                        [GlobalStyleRules.Padding15,
                        GlobalStyleRules.JustifyContentCenter,
                        AppStyleRules.BackgroundColorWhiteGray
                        ]}>
                        <Text style={[
                            GlobalStyleRules.FontFamilyPoppinsRegular,
                            GlobalStyleRules.FontSize14
                        ]}>Hesabım</Text>
                        <TouchableOpacity
                            style={[
                                GlobalStyleRules.PositionAbsolute,
                                GlobalStyleRules.Right10,
                                GlobalStyleRules.MarginTop10,
                                GlobalStyleRules.ZIndex999999999
                            ]}
                            onPress={()=>{
                                Selected();
                                OpenModal();
                            } }>

                            <Image
                                style={[
                                    GlobalStyleRules.Width20,
                                    GlobalStyleRules.Height20
                                ]}
                                resizeMode="contain"
                                source={require("../images/global_icons/close-icon.png")}

                            />

                        </TouchableOpacity>
                    </View>

                    {
                        _domains.length > 1 && (
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text={`Marka Seç: ${Domain.value}`} type={ButtonComponentTypes.Default} onPress={() => SetDomainSelectionOpen(true)} />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        )
                    }

                    {
                        domain.companies.length > 1 && (

                            <PaddingContainerComponent type={PaddingContainerComponentTypes[10]}>
                                <InputContainerComponent>
                                    <ButtonComponent text={`Mağaza Seç: ${Company.value}`} type={ButtonComponentTypes.Default} onPress={() => SetCompanySelectionOpen(true)} />
                                </InputContainerComponent>
                            </PaddingContainerComponent>
                        )
                    }



                   
                    <View style={[
                        GlobalStyleRules.BorderWidth1,
                        GlobalStyleRules.BorderColorRed,
                        GlobalStyleRules.Padding10,
                        GlobalStyleRules.MarginLeft20,
                        GlobalStyleRules.MarginRight20
                    ]}>
                        <TouchableOpacity style={[
                            GlobalStyleRules.AlignItemsCenter
                        ]}
                            onPress={Logout}>
                            <Text style={[
                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                GlobalStyleRules.FontSize14,
                                AppStyleRules.ColorRed
                            ]}>Çıkış Yap</Text>
                        </TouchableOpacity>
                    </View>


                </SafeAreaView>
            </Modal >



            <View style={[
                GlobalStyleRules.BorderBottomWidth1,
                GlobalStyleRules.Height50,
                AppStyleRules.BackgroundColorBlackYellow,
                GlobalStyleRules.FlexDirectionRow,
                GlobalStyleRules.AlignItemsCenter,
                GlobalStyleRules.JustifyContentSpaceBetween
            ]}>
                <View style={[
                    GlobalStyleRules.MarginBottom5,
                    GlobalStyleRules.MarginLeft10
                ]}>
                    <Image
                        style={[
                            GlobalStyleRules.Width120,
                            GlobalStyleRules.Height40,
                            GlobalStyleRules.ResizeModeContain
                        ]}
                        source={{ uri: company.logo }}
                    />
                </View>

                <TouchableOpacity style={[
                    GlobalStyleRules.MarginBottom5,
                    GlobalStyleRules.MarginRight10
                ]}
                    onPress={OpenModal}
                >
                    <Image
                        style={[
                            GlobalStyleRules.Width30,
                            GlobalStyleRules.Height30,
                            GlobalStyleRules.ResizeModeContain
                        ]}
                        source={require('../images/header_icons/filter-icon.png')}
                    />

                </TouchableOpacity>
            </View>
        </SafeAreaView >
    )

}

export default HeaderComponent;