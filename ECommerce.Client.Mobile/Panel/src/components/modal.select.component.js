import React from "react";
import { View } from "react-native";
import ButtonComponentTypes from "../enums/button.component.types";
import PaddingContainerComponentTypes from "../enums/padding.container.component.types";
import ModalSelectComponentStyle from "../styles/components/modal.select.component.style";
import ButtonComponent from "./button.component";
import PaddingContainerComponent from "./padding.container.component";

const ModalSelectComponent = (props) => {
    return (
        <View style={ModalSelectComponentStyle.Container}>
            <View style={ModalSelectComponentStyle.InnerContainer}>

                {
                    props.data.map(item => <>
                        <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                            <ButtonComponent type={ButtonComponentTypes.Default} text={item.value} onPress={() => props.onChange(item)} />
                        </PaddingContainerComponent>
                    </>)
                }

            </View>
        </View>
    )
}

export default ModalSelectComponent;