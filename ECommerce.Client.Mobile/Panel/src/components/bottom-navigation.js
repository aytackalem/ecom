import React, { Component } from "react";
import { Image, ImageBackground, Text, TouchableOpacity, View } from "react-native";
import AppStyleRules from "../styles/constants/app-style-rules";
import GlobalStyleRules from "../styles/constants/global-style-rules";
import { useNavigation } from '@react-navigation/native';

export default class BottomNavigation extends Component {
    navigation;

    constructor(props) {
        super(props)
        this.props = props;
        // this.props.navigation=useNavigation();
    }


    logoStyle = [
        GlobalStyleRules.Width40,
        GlobalStyleRules.Height40
    ];

    logoActiveStyle = [
        GlobalStyleRules.Width50,
        GlobalStyleRules.Height50,
        GlobalStyleRules.BorderRadius25,
        AppStyleRules.BackgroundColorTurquoise
    ];

    render() {
        return (
            <View
                style={[
                    GlobalStyleRules.PaddingLeft10,
                    GlobalStyleRules.PaddingRight10,
                    GlobalStyleRules.Height60,
                    GlobalStyleRules.CalculatedByPercentWidth100,
                    AppStyleRules.BackgroundColorBlackGray,
                    GlobalStyleRules.AlignItemsCenter,
                    GlobalStyleRules.JustifyContentSpaceBetween,
                    GlobalStyleRules.FlexDirectionRow,
                    GlobalStyleRules.BorderTopWidth1,
                    GlobalStyleRules.BorderTopColorWhiteGray
                ]}
            >

                {/* Siparişler */}
                <View
                    style={[

                        GlobalStyleRules.Flex1,
                        GlobalStyleRules.AlignItemsCenter
                    ]}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate("Order")
                        }}
                        style={[
                            GlobalStyleRules.AlignItemsCenter,
                            GlobalStyleRules.FlexDirectionRow,
                            GlobalStyleRules.JustifyContentSpaceAround
                        ]}
                    >
                        <Image
                            style={[
                                GlobalStyleRules.Height30,
                                GlobalStyleRules.Width30,
                                GlobalStyleRules.Margin5
                            ]}
                            source={require("../images/bottom-navigation/order-icon.png")}
                        />
                        <Text
                            style={[
                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                GlobalStyleRules.FontSize13,
                                AppStyleRules.ColorWhite
                            ]}
                        >
                            Siparişler
                        </Text>
                    </TouchableOpacity>
                </View>



                {/* Anasayfa */}
                <View
                    style={[GlobalStyleRules.Flex1, GlobalStyleRules.AlignItemsCenter]}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate("Main")
                        }}
                        style={[
                            GlobalStyleRules.AlignItemsCenter
                        ]}
                    >
                        <Image
                            style={this.logoStyle}
                            source={require("../images/bottom-navigation/logo.png")}
                        />
                    </TouchableOpacity>
                </View>
                {/* Ürünler */}
                <View
                    style={[

                        GlobalStyleRules.Flex1,
                        GlobalStyleRules.AlignItemsCenter
                    ]}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate("Product")
                        }}
                        style={[
                            GlobalStyleRules.AlignItemsCenter,
                            GlobalStyleRules.FlexDirectionRow,
                            GlobalStyleRules.JustifyContentSpaceAround
                        ]}
                    >
                        <Image
                            style={[
                                GlobalStyleRules.Height30,
                                GlobalStyleRules.Width30,
                                GlobalStyleRules.Margin5
                            ]}
                            source={require("../images/bottom-navigation/product-icon.png")}
                        />
                        <Text
                            style={[
                                GlobalStyleRules.FontFamilyPoppinsRegular,
                                GlobalStyleRules.FontSize13,
                                AppStyleRules.ColorWhite
                            ]}
                        >
                            Ürünler
                        </Text>
                    </TouchableOpacity>
                </View>



            </View>
        )
    }
}