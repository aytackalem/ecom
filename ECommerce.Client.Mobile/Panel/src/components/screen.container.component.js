import React from 'react';
import { Image, SafeAreaView, Text, View } from "react-native";
import ButtonComponentTypes from '../enums/button.component.types';
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import ScreenContainerComponentStyle from '../styles/components/screen.container.component.style';
import GlobalStyleRules from '../styles/constants/global-style-rules';
import BottomNavigation from './bottom-navigation';
import ButtonComponent from './button.component';
import HeaderComponent from './header-component';
import PaddingContainerComponent from './padding.container.component';
import { useSelector } from 'react-redux';

const ScreenContainerComponent = (props) => {

    const _token = useSelector((state) => state.LoginSlice.Token);
    

    return (
        <SafeAreaView style={ScreenContainerComponentStyle.SafeAreaView}>
            {
                _token && (
                    <HeaderComponent />
                )
            }


            {props.children}

            {
                props.preloaderIsOpen == true && <>
                    <View style={ScreenContainerComponentStyle.PreloaderContainer}>
                        <View style={ScreenContainerComponentStyle.PreloaderInnerContainer}>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                <Text style={ScreenContainerComponentStyle.PreloaderText}>
                                    Lütfen Bekleyiniz
                                </Text>
                            </PaddingContainerComponent>
                        </View>
                    </View>
                </>
            }

            {
                props.modalIsOpen == true && <>
                    <View style={ScreenContainerComponentStyle.PreloaderContainer}>
                        <View style={ScreenContainerComponentStyle.PreloaderInnerContainer}>
                            <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                <PaddingContainerComponent type={PaddingContainerComponentTypes[5]}>
                                    <Text style={ScreenContainerComponentStyle.PreloaderText}>{props.modalText}</Text>
                                </PaddingContainerComponent>

                                <ButtonComponent type={ButtonComponentTypes.Info} onPress={() => props.modalClose()} text="Kapat" />
                            </PaddingContainerComponent>
                        </View>
                    </View>
                </>
            }

            {
                _token && (
                    <BottomNavigation navigation={props.navigation} />
                )
            }

        </SafeAreaView>
    );
};

export default ScreenContainerComponent;