import React from "react";
import { Text } from "react-native";
import HeadingComponentStyle from "../styles/components/heading.component.style";

const HeadingComponent = (props) => {
    return (
        <Text style={[HeadingComponentStyle.H2, HeadingComponentStyle.TextAlignCenter]}>
            {props.text}
        </Text>
    )
}

export default HeadingComponent;