import React from 'react';
import { View } from "react-native";
import PaddingContainerComponentTypes from '../enums/padding.container.component.types';
import PaddingContainerComponentStyle from '../styles/components/padding.container.component.style';

const PaddingContainerComponent = (props) => {
    const RenderStyle = () => {
        if (props.type == PaddingContainerComponentTypes[5]) {
            return PaddingContainerComponentStyle.Container5;
        }
        else if (props.type == PaddingContainerComponentTypes[10]) {
            return PaddingContainerComponentStyle.Container10;
        }
        else if (props.type == PaddingContainerComponentTypes[15]) {
            return PaddingContainerComponentStyle.Container15;
        }
        else if (props.type == PaddingContainerComponentTypes[20]) {
            return PaddingContainerComponentStyle.Container20;
        }
    }

    return (
        <View style={RenderStyle()}>
            {props.children}
        </View>
    )
}

export default PaddingContainerComponent;