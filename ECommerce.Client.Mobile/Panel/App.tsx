import React, { useEffect, useState } from 'react';
import { Platform, AppState, StatusBar, Alert, SafeAreaView, View, Button } from 'react-native';

import store from './src/redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';
import Startup from './Startup';
import { requestUserPermission, notificationListener } from './src/utils/notificationService';
import GlobalStyleRules from './src/styles/constants/global-style-rules';
import AppStyleRules from './src/styles/constants/app-style-rules';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SplashScreen from "react-native-splash-screen"; //import SplashScreen

function App() {

  if (Platform.OS == 'ios') {
    StatusBar.setBarStyle('light-content', true);	//<<--- add this
  }

  useEffect(() => {
    SplashScreen.hide(); //hides the splash screen on app load.

    requestUserPermission();
    notificationListener();
  }, [])
  let persistor = persistStore(store);


  return (
  
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <StatusBar backgroundColor={"#000"} />

          <Startup />

        </PersistGate>
      </Provider>
 
  );

};

export default App;