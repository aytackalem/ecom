import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from './src/screens/main.screen';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateModal } from './src/redux/modal.slice';
import ScreenContainerComponent from './src/components/screen.container.component';
import LoginScreen from './src/screens/login.screen';
import { NavigationContainer, useNavigationContainerRef } from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';
import OrderScreen from './src/screens/order.screen';
import ProductScreen from './src/screens/product.screen';
import BarcodeScreen from './src/screens/barcode.screen';

const Stack = createNativeStackNavigator();

const Startup = () => {
  const token = useSelector((state) => state.LoginSlice.Token);

  const preloaderIsOpen = useSelector((state) => state.PreloaderSlice.IsOpen);

  const modalIsOpen = useSelector((state) => state.ModalSlice.IsOpen);

  const modalText = useSelector((state) => state.ModalSlice.Text);

  const _dispatch = useDispatch();



  const [loading, setLoading] = useState(false);

  const [initialRoute, setInitialRoute] = useState();

  const navigationRef = useNavigationContainerRef();

  useEffect(() => {

    setLoading(false)

    messaging().onNotificationOpenedApp(
      (remoteMessage) => {

        if (remoteMessage.data.deepLink && tokenSlice) {
          navigationRef.navigate(remoteMessage.data.deepLink);
        }
      }
    );

    messaging()
      .getInitialNotification()
      .then(remoteMessage => {

        if (remoteMessage.data.deepLink && tokenSlice) {

          setInitialRoute(remoteMessage.data.deepLink);

        }
        setLoading(true)



      });

  }, [loading]);

  return (
    <ScreenContainerComponent preloaderIsOpen={preloaderIsOpen} modalIsOpen={modalIsOpen} modalText={modalText} modalClose={() => _dispatch(UpdateModal({ IsOpen: false, Text: null }))} navigation={navigationRef}>
      <NavigationContainer ref={navigationRef}>



        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            gestureEnabled: false,
            gestureDirection: "horizontal",
            animation: "none"
          }}

          initialRouteName={initialRoute}
        >
          {
            token == null
              ? (
                <>
                  <Stack.Screen name="Login" component={LoginScreen} />
                </>
              )
              :
              (
                <>
                  <Stack.Screen name="Order" component={OrderScreen} />

                  <Stack.Screen name="Main" component={MainScreen} />

                  <Stack.Screen name="Product" component={ProductScreen} />

                  <Stack.Screen name="Barcode" component={BarcodeScreen} />
                </>
              )

          }




        </Stack.Navigator>



      </NavigationContainer>
    </ScreenContainerComponent>
  );

};

export default Startup;