﻿using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;

namespace ECommerce.Mikro.Return
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IAccountingProviderService _accountingProviderService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IAccountingProviderService accountingProviderService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._accountingProviderService = accountingProviderService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var orderIds = this
                ._unitOfWork
                .OrderBillingRepository
                .DbSet()
                .Where(ob => !string.IsNullOrEmpty(ob.ReturnDescription) && string.IsNullOrEmpty(ob.ReturnNumber))
                .Select(ob => ob.Id)
                .ToList();

            foreach (var oiLoop in orderIds)
            {
                var accountingCancel = this._accountingProviderService.CancelInvoice(oiLoop);
                if (accountingCancel.Success)
                {
                    var orderBilling = this
                        ._unitOfWork
                        .OrderBillingRepository
                        .DbSet()
                        .FirstOrDefault(ob => ob.Id == oiLoop);

                    orderBilling.ReturnNumber = accountingCancel.Data.DocumentNo;

                    this
                        ._unitOfWork
                        .OrderBillingRepository
                        .Update(orderBilling);
                }
            }
        }
        #endregion
    }
}
