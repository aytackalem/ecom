﻿namespace ECommerce.Application.Common.Parameters
{
    public class PagedRequest
    {
        #region Properties
        public int Page { get; set; }

        public int PageRecordsCount { get; set; }

        public string Search { get; set; }

        public string Sort { get; set; }

        public string Asc { get; set; }

        public bool? Active { get; set; }
        #endregion
    }
}
