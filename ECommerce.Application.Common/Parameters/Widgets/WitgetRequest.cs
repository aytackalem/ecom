﻿using System;

namespace ECommerce.Application.Common.Parameters.Widgets
{
    public class WitgetRequest
    {
        #region Properties
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        #endregion
    }
}
