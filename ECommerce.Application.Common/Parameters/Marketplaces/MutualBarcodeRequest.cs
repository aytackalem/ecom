﻿using ECommerce.Application.Common.Parameters.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    public class MutualBarcodeRequest : IConfigurableRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public string PackageNumber { get; set; }

        public Wrappers.Marketplaces.Order.Order Order { get; set; }
        #endregion
    }
}
