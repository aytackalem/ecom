﻿using ECommerce.Application.Common.Parameters.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    public class OrderRequest : IConfigurableRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        /// <summary>
        /// Market yerinde sipariş numarasına göre filtreleme yapar
        /// </summary>
        public string MarketplaceOrderCode { get; set; }

        /// <summary>
        /// Checkprepring için kulanılır
        /// </summary>
        public string TrackingCode { get; set; }

        /// <summary>
        /// Hangi Market yerinin olduğunu belirtir.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Sipariş başlangıç tarihi.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Sipariş bitiş tarihi.
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion
    }
}
