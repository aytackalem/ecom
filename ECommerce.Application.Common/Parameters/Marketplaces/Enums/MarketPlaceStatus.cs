﻿namespace ECommerce.Application.Common.Parameters.Enums
{
    public enum MarketplaceStatus
    {
        /// <summary>
        /// Sipariş Çekildi.
        /// </summary>
        Picking = 1,

        /// <summary>
        /// Sipariş Paketlendi.
        /// </summary>
        Packaging = 2,

        /// <summary>
        /// Sipariş Teslim Edildi.
        /// </summary>
        Delivered = 3,

    }
}
