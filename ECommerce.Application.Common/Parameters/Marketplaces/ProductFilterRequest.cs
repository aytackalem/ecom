﻿using ECommerce.Application.Common.Parameters.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    /// <summary>
    /// Ürün datalarını çektiğimiz model listesi
    /// </summary>
    public class ProductFilterRequest : IConfigurableRequest
    {
        public Dictionary<string, string> Configurations { get; set; }

        public string Barcode { get; set; }

        public string Stockcode { get; set; }

        public int Count { get; set; }

    }
}
