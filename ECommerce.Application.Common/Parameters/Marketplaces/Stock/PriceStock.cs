﻿namespace ECommerce.Application.Common.Parameters.Stock
{
    public class PriceStock
    {
        public string UUId { get; set; }

        public string SkuCode { get; set; }

        public string Barcode { get; set; }

        public int Quantity { get; set; }

        public decimal? ListPrice { get; set; }

        public decimal? SalePrice { get; set; }

        public string StockCode { get; set; }
    }
}
