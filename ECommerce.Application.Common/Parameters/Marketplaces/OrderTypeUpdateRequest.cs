﻿using ECommerce.Application.Common.Parameters.Base;
using ECommerce.Application.Common.Parameters.Enums;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    public class OrderTypeUpdateRequest : IConfigurableRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        public Wrappers.Marketplaces.Order.Order Order { get; set; }

        public MarketplaceStatus MarketplaceStatus { get; set; }
        #endregion
    }
}
