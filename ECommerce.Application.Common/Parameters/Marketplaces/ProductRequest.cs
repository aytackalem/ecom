﻿using ECommerce.Application.Common.Parameters.Base;
using ECommerce.Application.Common.Parameters.Product;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    /// <summary>
    /// Ürün yükleme için kullanılan model listesi
    /// </summary>
    public class ProductRequest : IConfigurableRequest
    {
        #region Properties
        public string MarketPlaceId { get; set; }

        public Dictionary<string, string> Configurations { get; set; }

        public ProductItem ProductItem { get; set; }
        #endregion
    }
}
