﻿using ECommerce.Application.Common.Parameters.Base;
using ECommerce.Application.Common.Parameters.Enums;
using ECommerce.Application.Common.Parameters.Order;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    public class OrderPickingRequest : IConfigurableRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        public string MarketPlaceId { get; set; }

        public string Token { get; set; }
        #endregion

        #region Navigation Properties
        public MarketplaceStatus Status { get; set; }

        /// <summary>
        /// Pazaryerlerinden dönen sipariş detay dataları
        /// </summary>
        public OrderPicking OrderPicking { get; set; }
        #endregion
    }
}
