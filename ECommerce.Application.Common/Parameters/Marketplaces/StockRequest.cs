﻿using ECommerce.Application.Common.Parameters.Base;
using ECommerce.Application.Common.Parameters.Stock;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters
{
    public class StockRequest : IConfigurableRequest
    {
        public Dictionary<string, string> Configurations { get; set; }

        public PriceStock PriceStock { get; set; }

        public string UUId { get; set; }

        public string MarketPlaceId { get; set; }

        public bool UpdatePrice { get; set; }
    }
}
