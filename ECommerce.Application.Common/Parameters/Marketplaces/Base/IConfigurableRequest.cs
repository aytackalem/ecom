﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.Base
{
    public interface IConfigurableRequest
    {
        #region Properties
        public Dictionary<string, string> Configurations { get; set; }
        #endregion
    }
}
