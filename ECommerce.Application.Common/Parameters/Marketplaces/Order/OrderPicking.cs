﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Parameters.Order
{
    public class OrderPicking
    {

        #region Property        
        public string MarketPlaceOrderId { get; set; }

        public string TrackingNumber { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Pazaryerlerinden dönen sipariş detay dataları
        /// </summary>
        public List<OrderPickingDetail> OrderDetails { get; set; }
        public string Token { get; set; }
        #endregion
    }
}
