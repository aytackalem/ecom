﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Parameters.Product
{
    public class StockItem
    {
        public string UUId { get; set; }

        /// <summary>
        /// Pazaryerlerinin kendi açtığı ürün numarası
        /// </summary>
        public string MarketplaceProductId { get; set; }

        /// <summary>
        /// Ürün barkodu
        /// </summary>
        public string Barcode { get; set; }
        /// <summary>
        /// Ürün Stok Miktarı
        /// </summary>
        public int Quantity { get; set; }


        /// <summary>
        /// Ürün markası
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Ürün stok kodu
        /// </summary>
        public string StockCode { get; set; }

        /// <summary>
        /// Ürün Resimleri
        /// </summary>
        public List<Image> Images { get; set; }
        /// <summary>
        /// Ürün kategori özellikleri
        /// </summary>
        public List<CategoryAttribute> Attributes { get; set; }

        /// <summary>
        /// Ürün liste fiyatı
        /// </summary>
        public decimal ListPrice { get; set; }
        /// <summary>
        /// Ürün Fiyatı
        /// </summary>
        public decimal SalePrice { get; set; }

        /// <summary>
        /// Ürün kdv oranı
        /// </summary>
        public decimal VatRate { get; set; }

        /// <summary>
        /// Ürün aktif mi
        /// </summary>
        public bool Active { get; set; }
        /// <summary>
        /// Ürün Adı
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Ürün Kısa adı
        /// </summary>
        public string Subtitle { get; set; }
        /// <summary>
        /// Ürün açıklaması
        /// </summary>
        public string Description { get; set; }
    }
}
