﻿namespace ECommerce.Application.Common.Parameters.FtpHelper
{
    public class FtpRequest
    {
        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Path { get; set; }
        #endregion
    }
}
