﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService
{
    public class GetByCategoryCodesParameter
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public List<string> CategoryCodes { get; set; }
        #endregion
    }
}
