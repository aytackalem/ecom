﻿namespace ECommerce.Application.Common.Parameters.MarketplaceCatalog.BrandService
{
    public class SearchByNameParameters
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
