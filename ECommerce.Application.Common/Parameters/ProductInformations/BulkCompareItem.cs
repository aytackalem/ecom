﻿namespace ECommerce.Application.Common.Parameters.ProductInformations
{
    public class BulkCompareItem
    {
        #region Properties
        public int CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public bool OnSale { get; set; }

        public bool Locked { get; set; }

        public string Key { get; set; }

        public decimal UnitPrice { get; set; }

        public int Stock { get; set; }

        public string Url { get; set; }

        public bool Opened { get; set; }

        public string UUId { get; set; }

        /// <summary>
        /// True geldiği tarktirde pricedirty true gönderilmez
        /// </summary>
        public bool DirtyPriceDisabled { get; set; }
        /// <summary>
        /// True geldiği tarktirde stokdirty true gönderilmez
        /// </summary>
        public bool DirtyStockDisabled { get; set; }
        #endregion
    }
}
