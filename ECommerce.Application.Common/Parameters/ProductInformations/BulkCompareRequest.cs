﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.ProductInformations
{
    public class BulkCompareRequest
    {
        #region Properties
        public string KeyColumnName { get; set; }

        public string MarketplaceId { get; set; }
        #endregion

        #region Navigation Properties
        public List<BulkCompareItem> BulkCompareItems { get; set; }
        #endregion
    }
}
