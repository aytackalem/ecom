﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestUpdateProductItem : IMarketplaceRequestItem
    {
        #region Properties
        public int ProductId { get; set; }

        /// <summary>
        /// Ana Ürünün Bensersiz id'si
        /// </summary>
        public string ProductMarketplaceUUId { get; set; }

        public int ProductMarketplaceId { get; set; }

        /// <summary>
        /// Ürün ana adı
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Varianların tamımının ortak ürün satış kodu
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Ürün markası
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Ürün Marka Id'si
        /// </summary>
        public string BrandId { get; set; }

        /// <summary>
        /// Ürün markası
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Pazar yerine göre ürün kategorisi
        /// </summary>
        public string CategoryCode { get; set; }

        /// <summary>
        /// Ürün kargo verilme süresi gün olarak
        /// </summary>
        public int DeliveryDay { get; set; }

        /// <summary>
        /// Ürün Desi Değeri
        /// </summary>
        public double DimensionalWeight { get; set; }

        /// <summary>
        /// Variant'a göre ürün listesi
        /// </summary>
        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

        public string ErrorMessage { get; set; }
        #endregion
    }
}
