﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestCreateProductItem : IMarketplaceRequestItem
    {
        #region Properties
        public int ProductId { get; set; }

        /// <summary>
        /// Ana Ürünün Bensersiz id'si
        /// </summary>
        public string ProductMarketplaceUUId { get; set; }

        public int ProductMarketplaceId { get; set; }

        /// <summary>
        /// Ürün ana adı
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Varianların tamımının ortak ürün satış kodu
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Ürün markası
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Ürün Marka Id'si
        /// </summary>
        public string BrandCode { get; set; }

        /// <summary>
        /// Ürün markası
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Pazar yerine göre ürün kategorisi
        /// </summary>
        public string CategoryCode { get; set; }

        /// <summary>
        /// Ürün kargo verilme süresi gün olarak
        /// </summary>
        public int DeliveryDay { get; set; }

        /// <summary>
        /// Ürün Desi Değeri
        /// </summary>
        public double DimensionalWeight { get; set; }

        /// <summary>
        /// Variant'a göre ürün listesi
        /// </summary>
        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

        public string ErrorMessage { get; set; }
        #endregion
    }

    public class ProductInformationMarketplace
    {
        /// <summary>
        /// Ürün numarası
        /// </summary>
        public int ProductInformationId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        /// <summary>
        /// Ürünün Bensersiz id'si
        /// </summary>
        public string ProductInformationUUId { get; set; }

        /// <summary>
        /// Ürün barkodu
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// Ürün Stok Miktarı
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Ürün stok kodu
        /// </summary>
        public string StockCode { get; set; }

        /// <summary>
        /// Ürün sku kodu
        /// </summary>
        public string SkuCode { get; set; }

        /// <summary>
        /// Ürün Resimleri
        /// </summary>
        public List<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        /// <summary>
        /// Ürün kategori özellikleri
        /// </summary>
        public List<ProductInformationMarketplaceVariantValue> ProductInformationMarketplaceVariantValues { get; set; }

        /// <summary>
        /// Ürün liste fiyatı
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Ürün Fiyatı
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Ürün kdv oranı
        /// </summary>
        public decimal VatRate { get; set; }

        /// <summary>
        /// Ürün Adı
        /// </summary>
        public string ProductInformationName { get; set; }


        public string VariantValuesDescription { get; set; }

        /// <summary>
        /// Ürün Kısa adı
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// Ürün açıklaması
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ürün pazaryerinde açık mı
        /// </summary>
        public bool Opened { get; set; }

        /// <summary>
        /// Tüm fotoğraflar kontrol edilir ve kırık link var ise 'True' atanır.
        /// </summary>
        public bool HasBrokenPhotoUrl { get; set; }
    }

    public class ProductInformationPhoto
    {
        public string Url { get; set; }

        public int DisplayOrder { get; set; }
    }

    public class ProductInformationMarketplaceVariantValue
    {
        #region Property
        public string MarketplaceVarinatCode { get; set; }

        public string MarketplaceVarinatName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool Mandatory { get; set; }

        public bool Varinater { get; set; }

        public bool AllowCustom { get; set; }
        #endregion

    }
}