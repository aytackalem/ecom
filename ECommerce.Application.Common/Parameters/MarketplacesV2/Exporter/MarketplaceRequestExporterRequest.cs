﻿namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter
{
    public class MarketplaceRequestExporterRequest
    {
        #region Properties
        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }
        #endregion
    }
}
