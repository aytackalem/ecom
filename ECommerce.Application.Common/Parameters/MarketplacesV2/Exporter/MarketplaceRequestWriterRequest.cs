﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter
{
    public class MarketplaceRequestWriterRequest<TMarketplaceRequest> where TMarketplaceRequest : IMarketplaceRequest
    {
        #region Properties
        public List<TMarketplaceRequest> MarketplaceRequests { get; set; }
        #endregion
    }
}
