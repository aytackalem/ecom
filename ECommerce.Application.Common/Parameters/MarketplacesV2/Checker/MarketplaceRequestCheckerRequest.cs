﻿namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Checker
{
    public class MarketplaceRequestCheckerRequest
    {
        #region Properties
        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }
        #endregion
    }
}
