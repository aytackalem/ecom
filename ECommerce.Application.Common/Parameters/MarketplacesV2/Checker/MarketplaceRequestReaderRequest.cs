﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Checker
{
    public class MarketplaceRequestReaderRequest<TMarketplaceRequest> where TMarketplaceRequest : IMarketplaceRequest
    {
        #region Properties
        public List<TMarketplaceRequest> MarketplaceRequests { get; set; }

        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }
        #endregion
    }
}
