﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using System;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestUpdateStockItem : IMarketplaceRequestItem
    {
        #region Properties
        public string ProductInformationUUId { get; set; }

        public string ProductUUId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string SkuCode { get; set; }

        public string StockCode { get; set; }

        public string Barcode { get; set; }

        public int Stock { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public bool UpdatePrice { get; set; }

        public string ErrorMessage { get; set; }

        public decimal VatRate { get; set; }

        public DateTime? LastRequestDateTime { get; set; }

        public int ProductId { get; set; }

        public int ProductInformationId { get; set; }

        public string SellerCode { get; set; }

        public string PhotoUrl { get; set; }

        public string ProductInformationName { get; set; }

        public string VariantValuesDescription { get; set; }
        #endregion
    }
}
