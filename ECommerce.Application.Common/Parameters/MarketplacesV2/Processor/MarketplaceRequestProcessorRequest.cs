﻿namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorRequest
    {
        #region Properties
        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }
        #endregion
    }
}
