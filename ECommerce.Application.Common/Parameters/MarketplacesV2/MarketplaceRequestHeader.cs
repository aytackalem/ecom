﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestHeader
    {
        #region Properties
        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public Dictionary<string, string> MarketplaceConfigurations { get; set; }
        #endregion
    }
}
