﻿using System;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Base
{
    /// <summary>
    /// Veritabanina kaydedilebilir pazaryeri isteklerinin uygulayacagi arayuz.
    /// </summary>
    public interface IMarketplaceRequestDbCreatable : IMarketplaceRequest
    {
        #region Properties
        /// <summary>
        /// Veritabanina kayit esnasinda kullanilacak olan benzersiz kimlik degeri.
        /// </summary>
        public Guid Id { get; set; }
        #endregion
    }
}
