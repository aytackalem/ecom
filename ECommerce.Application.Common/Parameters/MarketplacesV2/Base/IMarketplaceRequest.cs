﻿namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Base
{
    /// <summary>
    /// Pazaryeri isteklerinin uygulayacagi arayuz.
    /// </summary>
    public interface IMarketplaceRequest
    {
        #region Properties
        /// <summary>
        /// Pazaryeri istegine ait ust bilgi. Pazaryeri ayarlari, kuyruk bilgisi gibi gerekli olan bilgileri icerir.
        /// </summary>
        public MarketplaceRequestHeader MarketplaceRequestHeader { get; set; }
        #endregion
    }
}
