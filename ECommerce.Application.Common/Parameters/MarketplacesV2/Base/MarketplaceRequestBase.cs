﻿using ECommerce.Application.Common.Extensions;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2.Base
{
    public abstract class MarketplaceRequestBase<TMarketplaceRequestItem> where TMarketplaceRequestItem : IMarketplaceRequestItem
    {
        #region Properties
        /// <summary>
        /// Veritabani icerisinde kullanilir. MarketplaceRequest tablosunun id alanini temsil eder.
        /// </summary>
        public Guid Id { get; set; }

        public string Path { get; set; }

        public string TrackingId { get; set; }

        public List<TMarketplaceRequestItem> Items { get; set; }

        public int ItemsCount => this.Items.HasItem() ? this.Items.Count : 0;

        public List<TMarketplaceRequestItem> ErrorItems { get; set; }

        public int ErrorItemsCount => this.ErrorItems.HasItem() ? this.ErrorItems.Count : 0;

        public List<TMarketplaceRequestItem> InvalidItems { get; set; }

        public int InvalidItemsCount => this.InvalidItems.HasItem() ? this.InvalidItems.Count : 0;

        public MarketplaceRequestHeader MarketplaceRequestHeader { get; set; }
        #endregion
    }
}
