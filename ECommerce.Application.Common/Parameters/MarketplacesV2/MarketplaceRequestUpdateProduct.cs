﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestUpdateProduct : MarketplaceRequestBase<MarketplaceRequestUpdateProductItem>, IMarketplaceRequestDbCreatable
    {
    }
}
