﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Parameters.MarketplacesV2
{
    public class MarketplaceRequestGetOrder : IMarketplaceRequest
    {
        #region Properties
        public MarketplaceRequestHeader MarketplaceRequestHeader { get; set; }
        #endregion
    }
}
