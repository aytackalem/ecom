﻿namespace ECommerce.Application.Common.Parameters.ProductInformationMarketplaces
{
    public class UpdateUUIdRequest
    {
        #region Properties
        public int Id { get; set; }

        public string UUId { get; set; }
        #endregion
    }
}
