﻿namespace ECommerce.Application.Common.Parameters
{
    public class CaptchaRequest<T>
    {
        #region Properties
        public string CaptchaResponse { get; set; }
        #endregion

        #region Navigation Properties
        public T Data { get; set; }
        #endregion
    }
}
