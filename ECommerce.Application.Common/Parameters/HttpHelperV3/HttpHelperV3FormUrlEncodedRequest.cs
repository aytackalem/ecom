﻿using ECommerce.Application.Common.Parameters.HttpHelperV3.Base;
using System.Collections.Generic;
using System.Net.Http;

namespace ECommerce.Application.Common.Parameters.HttpHelperV3
{
    public class HttpHelperV3FormUrlEncodedRequest : HttpHelperV3RequestBase
    {
        #region Properties
        public List<KeyValuePair<string, string>> Data { get; set; }
        #endregion
    }
}
