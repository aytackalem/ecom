﻿using ECommerce.Application.Common.Parameters.HttpHelperV3.Base;
using System.Net.Http;

namespace ECommerce.Application.Common.Parameters.HttpHelperV3
{
    public class HttpHelperV3JsonFileRequest : HttpHelperV3RequestBase
    {
        #region Properties
        public string Request { get; set; }

        public string FileName { get; set; }
        #endregion
    }
}
