﻿using ECommerce.Application.Common.Parameters.HttpHelperV3.Base;
using System.Net.Http;

namespace ECommerce.Application.Common.Parameters.HttpHelperV3
{
    public class HttpHelperV3Request<TRequest> : HttpHelperV3RequestBase
    {
        #region Properties
        public TRequest Request { get; set; }
        #endregion
    }
}
