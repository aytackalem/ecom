﻿using System.Collections.Generic;
using System.Net.Http;

namespace ECommerce.Application.Common.Parameters.HttpHelperV3.Base
{
    public abstract class HttpHelperV3RequestBase
    {
        #region Properties
        public string RequestUri { get; set; }

        public HttpMethod HttpMethod { get; set; }

        public AuthorizationType? AuthorizationType { get; set; }

        public string Authorization { get; set; }

        public string Agent { get; set; }

        public string Accept { get; set; }

        public string ContentType { get; set; }

        public Dictionary<string, string> Headers { get; set; }
        #endregion
    }
}
