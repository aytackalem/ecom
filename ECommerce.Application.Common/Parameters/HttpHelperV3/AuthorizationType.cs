﻿namespace ECommerce.Application.Common.Parameters.HttpHelperV3
{
    public enum AuthorizationType
    {
        Basic,
        XApiKey,
        Bearer
    }
}
