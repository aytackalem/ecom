﻿using ECommerce.Application.Common.Parameters.Notification.Base;

namespace ECommerce.Application.Common.Parameters.Notification
{
    public class GetCountParameter : ParameterBase
    {
        public string MarketplaceId { get; set; }

        public string Stockcode { get; set; }

        public string Barcode { get; set; }

        public string ModelCode { get; set; }

        public string ProductInformationName { get; set; }
    }
}
