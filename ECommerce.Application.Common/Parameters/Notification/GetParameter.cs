﻿using ECommerce.Application.Common.Parameters.Notification.Base;

namespace ECommerce.Application.Common.Parameters.Notification
{
    public class GetParameter : ParameterBase
    {
        #region Properties
        public int Page { get; set; }

        public int RecordsCount { get; set; }

        public string MarketplaceId { get; set; }

        public string Stockcode { get; set; }

        public string Barcode { get; set; }

        public string ModelCode { get; set; }

        public string ProductInformationName { get; set; }

        public string Type { get; set; }
        #endregion
    }
}
