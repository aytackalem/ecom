﻿using ECommerce.Application.Common.Parameters.Notification.Base;

namespace ECommerce.Application.Common.Parameters.Notification
{
    public class DeleteNotificationParameter: ParameterBase
    {
        #region Properties
        public string Id { get; set; }
        #endregion
    }
}
