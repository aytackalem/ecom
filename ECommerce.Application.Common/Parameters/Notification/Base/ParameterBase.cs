﻿namespace ECommerce.Application.Common.Parameters.Notification.Base
{
    public abstract class ParameterBase
    {
        #region Properties
        public string Brand { get; set; }

        public int CompanyId { get; set; }
        #endregion
    }
}
