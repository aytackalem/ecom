﻿using System;

namespace ECommerce.Application.Common.Helpers
{
    public static class XConsole
    {
        public static void Info(string text)
        {
            ConsoleColor consoleColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(text);

            Console.ForegroundColor = consoleColor;
        }

        public static void Error(string text)
        {
            ConsoleColor consoleColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);

            Console.ForegroundColor = consoleColor;
        }

        public static void Warning(string text)
        {
            ConsoleColor consoleColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);

            Console.ForegroundColor = consoleColor;
        }

        public static void Success(string text)
        {
            ConsoleColor consoleColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);

            Console.ForegroundColor = consoleColor;
        }
    }
}
