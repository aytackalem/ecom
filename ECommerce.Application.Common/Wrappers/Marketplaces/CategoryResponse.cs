﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.Marketplaces
{
    public class CategoryResponse
    {
        public CategoryResponse()
        {
            CategoryAttributes = new List<CategoryAttributeResponse>();
        }

        #region Properties
        public string Code { get; set; }

        public string ParentCategoryCode { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryAttributeResponse> CategoryAttributes { get; set; }
        #endregion
    }
}
