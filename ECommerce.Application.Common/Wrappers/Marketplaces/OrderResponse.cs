﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.Marketplaces
{
    public class OrderResponse
    {
        #region Navigation Properties
        public List<Order.Order> Orders { get; set; }
        #endregion
    }
}
