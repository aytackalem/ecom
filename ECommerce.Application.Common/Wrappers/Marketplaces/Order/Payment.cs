﻿using ECommerce.Domain.Entities.Enum;
using System;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.Order
{
    public class Payment
    {
        #region Property
        public decimal Amount { get; set; }

        public DateTime Date { get; set; }
        
        public string CurrencyId { get; set; }
        
        public string PaymentTypeId { get; set; }

        public bool Paid { get; set; }

        public BankType? BankId { get; set; }
        #endregion
    }
}
