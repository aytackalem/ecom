﻿namespace ECommerce.Application.Common.Wrappers.Marketplaces.Order
{
    public class OrderShipment
    {
        #region Properties
        public string TrackingCode { get; set; }

        public string TrackingUrl { get; set; }

        public string TrackingBase64 { get; set; }

        public string ShipmentTypeId { get; set; }

        public string ShipmentCompanyId { get; set; }

        /// <summary>
        /// kargoyu ödeyen karşı taraf mı
        /// </summary>
        public bool Payor { get; set; }

        public string PackageNumber { get; set; }
        #endregion
    }
}
