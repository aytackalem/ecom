﻿using ECommerce.Application.Common.Parameters.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.Order
{
    public class Order
    {
        #region Properties
        public string UUId { get; set; }

        /// <summary>
        /// Pazaryeri bilgilerinden faydalanılarak oluşturulmuş unique id.
        /// </summary>
        public string UId { get; set; }

        public bool MutualBarcode { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public int Id { get; set; }

        public string OrderCode { get; set; }

        public decimal CargoFee { get; set; }

        public string OrderNote { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal ListTotalAmount { get; set; }

        public decimal Discount { get; set; }

        /// <summary>
        /// Kapıda ödeme tutarı
        /// </summary>
        public decimal SurchargeFee { get; set; }

        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        public decimal VatExcListTotal { get => this.ListTotalAmount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        public decimal VatExcTotal { get => this.TotalAmount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        public decimal VatExcDiscount { get => this.Discount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Siparişin toplam komiston tutarı
        /// </summary>
        public decimal CommissionAmount { get => this.OrderDetails.Sum(x => Convert.ToDecimal(x.UnitCommissionAmount) * x.Quantity); }

        public string MarketplaceId { get; set; }

        public int OrderSourceId { get; set; }

        public string OrderSource { get; set; }

        public string OrderTypeId { get; set; }

        public int MarketplaceCompanyId { get; set; }

        /// <summary>
        /// Toplu faturalama. Modanisa gibi toplu faturalandirma yapilacak siparisler icin bu deger false atilir.
        /// </summary>
        public bool BulkInvoicing { get; set; }

        /// <summary>
        /// Siparisin kaydedilecegi musteri hesabinin kontrol edilmesi isteniyor ise "true" degeri set edilir.
        /// </summary>
        public bool CheckCustomer { get; set; }

        public string CurrencyId { get; set; } = "TL";

        /// <summary>
        /// Mikro ihracat mi?
        /// </summary>
        public bool Micro { get; set; }

        /// <summary>
        /// Kurumsal fatura mı?
        /// </summary>
        public bool Commercial { get; set; }
        #endregion

        #region Navigation Properties
        public Customer Customer { get; set; }

        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        public OrderInvoiceAddress OrderInvoiceAddress { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        public OrderShipment OrderShipment { get; set; }

        public List<Payment> Payments { get; set; }

        /// <summary>
        /// Statüs atlatmak için pazaryerlerine ait bilgiler girilir.
        /// </summary>
        public OrderPicking OrderPicking { get; set; }
        #endregion
    }
}
