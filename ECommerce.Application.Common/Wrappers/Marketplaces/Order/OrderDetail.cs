﻿using System;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.Order
{
    public class OrderDetail
    {
        #region Properties
        public string UUId { get; set; }

        /// <summary>
        /// Pazaryeri bilgilerinden faydalanılarak oluşturulmuş unique id.
        /// </summary>
        public string UId { get; set; }

        /// <summary>
        /// ürün miktarı
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Kdv tutarı
        /// </summary>
        public double TaxRate { get; set; }

        /// <summary>
        /// Kdv dahil birim fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Kdv dahil liste fiyatı.
        /// </summary>
        public decimal ListPrice { get; set; }


        /// <summary>
        /// Kdv dahil indirim fiyatı.
        /// </summary>
        public decimal UnitDiscount { get; set; }

        /// <summary>
        /// Kdv dahil komisyon  fiyatı.
        /// </summary>
        public decimal UnitCommissionAmount { get; set; }

        /// <summary>
        /// Kdv dahil birim maliyeti.
        /// </summary>
        public decimal UnitCost { get; set; }

        /// <summary>
        /// Kdv hariç liste satış fiyatı.
        /// </summary>
        public decimal VatExcListUnitPrice { get => this.ListPrice / (1 + Convert.ToDecimal(this.TaxRate)); }

        /// <summary>
        /// Kdv hariç birim fiyatı.
        /// </summary>
        public decimal VatExcUnitPrice { get => this.UnitPrice / (1 + Convert.ToDecimal(this.TaxRate)); }

        /// <summary>
        /// Kdv hariç birim indirimi.
        /// </summary>
        public decimal VatExcUnitDiscount { get => this.UnitDiscount / (1 + Convert.ToDecimal(this.TaxRate)); }

        /// <summary>
        /// Kdv hariç birim maliyeti.
        /// </summary>
        public decimal VatExcUnitCost { get => this.UnitCost / (1 + Convert.ToDecimal(this.TaxRate)); }

        /// <summary>
        /// Ürün bazlı kargo ücretsiz
        /// </summary>
        public bool Payor { get; set; }
        #endregion

        #region Navigation Properties
        public Product Product { get; set; }
        #endregion
    }
}
