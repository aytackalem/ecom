﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Wrappers.Marketplaces
{
    public class CheckPreparingResponse
    {
        public bool IsCancel { get; set; }
    }
}
