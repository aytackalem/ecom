﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter
{
    public class ProductFilterImage
    {
        public string Url { get; set; }

        public int Order { get; set; }

        public Byte[] ImageFile { get; set; }

        public string Name { get; set; }
    }
}
