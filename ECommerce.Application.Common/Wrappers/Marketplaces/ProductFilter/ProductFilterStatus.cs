﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter
{
    public class ProductFilterStatus
    {
        public string ProviderId { get; set; }

        public bool Active { get; set; }
    }
}
