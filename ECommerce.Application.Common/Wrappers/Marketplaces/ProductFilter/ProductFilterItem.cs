﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter
{
    public class ProductFilterItem
    {
        public string ProviderId { get; set; }

        public string SellerCode { get; set; }

        public string Title { get; set; }

        public string Barcode { get; set; }

        public string Subtitle { get; set; }

        public string Description { get; set; }

        public string Brand { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string CurrencyCode { get; set; }

        /// <summary>
        /// Ürünün aktifliğini belirler
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Ürün kargo verilme süresi gün olarak
        /// </summary>
        public int PreparingDayCount { get; set; }

        public string ShipmentCompanyId { get; set; }

        public decimal VatRate { get; set; }


        //Ürün Desi Değeri
        public double DimensionalWeight { get; set; }

        public List<ProductFilterStockItem> StockItems { get; set; }

        public List<ProductFilterStatus> ProductStatues { get; set; }
    }
}
