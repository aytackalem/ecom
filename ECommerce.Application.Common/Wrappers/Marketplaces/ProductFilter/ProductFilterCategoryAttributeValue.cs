﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter
{
    public class ProductFilterCategoryAttributeValue
    {
        #region Property
        public string Code { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
