﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter
{
    public class ProductFilterStockItem
    {
        public string Barcode { get; set; }

        public int Quantity { get; set; }

        public string SellerStockCode { get; set; }

        /// <summary>
        /// Omni için gerekli ürünlerde uniqlik belilirliyor
        /// </summary>
        public string SkuCode { get; set; }

        public List<ProductFilterImage> Images { get; set; }

        public List<ProductFilterCategoryAttribute> Attributes { get; set; }

        public decimal ListPrice { get; set; }

        public decimal SalePrice { get; set; }

        public decimal BuyPrice { get; set; }

        public bool Active { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<ProductFilterPriceItem> ProductPriceItems { get; set; }
    }
}
