﻿namespace ECommerce.Application.Common.Wrappers.Marketplaces
{
    public class ChangeCargoCompanyResponse
    {
        #region Properties
        /// <summary>
        /// Değişim sonrası güncellenecek kargo firması id bilgisi.
        /// </summary>
        public string ShipmentCompanyId { get; set; }

        /// <summary>
        /// Güncelleme sonrası siparişin tekrar çekilip çekilmeyeceği bilgisi.
        /// </summary>
        public bool Retake { get; set; }
        #endregion
    }
}
