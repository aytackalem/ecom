﻿namespace ECommerce.Application.Common.Wrappers.Marketplaces
{
    public class OrderTypeUpdateResponse
    {
        #region Properties
        public string PackageNumber { get; set; }

        public string ShipmentCompanyId { get; set; }

        public string TrackingCode { get; set; }
        #endregion
    }
}
