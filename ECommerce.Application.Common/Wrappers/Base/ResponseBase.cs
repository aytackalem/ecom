﻿using System;

namespace ECommerce.Application.Common.Wrappers.Base
{
    public abstract class ResponseBase
    {
        #region Properties
        public string ErrorCode { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }
        #endregion
    }
}
