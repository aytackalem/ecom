﻿using ECommerce.Application.Common.Wrappers.Base;

namespace ECommerce.Application.Common.Wrappers
{
    public class DataResponse<TData> : ResponseBase
    {
        #region Properties
        public TData Data { get; set; }
        #endregion
    }
}
