﻿using System;

namespace ECommerce.Application.Common.Wrappers.Notification
{
    public class Notification
    {
        #region Properties
        public string Id { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string MarketplaceId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public string PhotoUrl { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Type { get; set; }

        public int ProductInformationId { get; set; }

        public string ModelCode { get; set; }

        public string ProductInformationName { get; set; }

        public int ProductId { get; set; }
        #endregion
    }
}
