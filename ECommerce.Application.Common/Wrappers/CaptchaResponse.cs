﻿namespace ECommerce.Application.Common.Wrappers
{
    public class CaptchaResponse
    {
        #region Properties
        public bool Success { get; set; }
        #endregion
    }
}
