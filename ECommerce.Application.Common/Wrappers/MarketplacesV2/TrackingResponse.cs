﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class TrackingResponse
    {
        #region Properties
        public bool Done { get; set; }

        public bool IncludeUrl { get; set; }

        public bool IncludeUUId { get; set; }
        #endregion

        #region Navigation Properties
        public List<TrackingResponseItem> TrackingResponseItems { get; set; }
        #endregion
    }
}
