﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders
{
    public class Product
    {
        #region Properties
        public string Barcode { get; set; }

        public string Name { get; set; }

        public string StockCode { get; set; }

        /// <summary>
        /// ürünü yükledikten sonra maplenir
        /// </summary>
        public int ProductInformationId { get; set; }
        #endregion
    }
}
