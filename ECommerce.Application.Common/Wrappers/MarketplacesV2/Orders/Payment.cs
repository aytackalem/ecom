﻿using System;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders
{
    public class Payment
    {
        #region Properties
        public decimal Amount { get; set; }

        public DateTime Date { get; set; }
        
        public string CurrencyId { get; set; }
        
        public string PaymentTypeId { get; set; }

        public bool Paid { get; set; }
        #endregion
    }
}
