﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders
{
    public class OrderInvoiceAddress
    {
        #region Properties
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string District { get; set; }

        public string Neighborhood { get; set; }

        public string TaxOffice { get; set; }

        public string TaxNumber { get; set; }
        #endregion
    }
}
