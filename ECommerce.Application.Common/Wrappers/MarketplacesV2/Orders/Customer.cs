﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders
{
    public class Customer
    {
        #region Properties
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }

        public string TaxNumber { get; set; }

        public string TaxAdministration { get; set; }
        #endregion
    }
}
