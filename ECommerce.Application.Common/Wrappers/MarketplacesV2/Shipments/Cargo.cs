﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Shipments
{
    public class Cargo
    {
        public string OrderNumber { get; set; }

        public decimal Amount { get; set; }

        public string Type { get; set; }

        public int Desi { get; set; }
    }
}
