﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class PriceObject
    {
        #region Properties
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }
        #endregion
    }
}
