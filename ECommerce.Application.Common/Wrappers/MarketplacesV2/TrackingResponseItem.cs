﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class TrackingResponseItem
    {
        #region Properties
        public int ProductId { get; set; }

        public int ProductMarketplaceId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string ProductInformationMarketplaceUUId { get; set; }

        public string Url { get; set; }

        public bool Success { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public int Stock { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public List<string> Messages { get; set; }
        #endregion
    }
}
