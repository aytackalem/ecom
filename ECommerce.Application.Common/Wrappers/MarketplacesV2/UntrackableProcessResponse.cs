﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class UntrackableProcessResponse
    {
        #region Properties
        /// <summary>
        /// Istek ile ilgili donecek hatalari temsil eder.
        /// </summary>
        public List<string> Messages { get; set; }
        #endregion

        #region Navigation Properties
        public List<UntrackableProcessResponseItem> UntrackableProcessResponseItems { get; set; }
        #endregion
    }
}
