﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class UntrackableProcessResponseItem
    {
        #region Properties
        public int ProductInformationMarketplaceId { get; set; }

        public bool Success { get; set; }

        public int Stock { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public List<string> Messages { get; set; }
        #endregion
    }
}
