﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class TrackableResponse
    {
        #region Properties
        public string TrackingId { get; set; }

        /// <summary>
        /// Istek ile ilgili donecek hatalari temsil eder.
        /// </summary>
        public List<string> Messages { get; set; }
        #endregion
    }
}
