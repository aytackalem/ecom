﻿using System;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Commissions
{
    public class Commission
    {
        public string OrderNumber { get; set; }

        public decimal CommissionAmount { get; set; }

        public string Type { get; set; }

        public DateTime TransactionDate { get; set; }

        public float CommissionRate { get; set; }

        public int PaymentPeriod { get; set; }

        public decimal SellerRevenue { get; set; }

        public string Barcode { get; set; }

        public int? PaymentOrderId { get; set; }
    }
}
