﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class StockObject
    {
        #region Properties
        public int Quantity { get; set; }
        #endregion
    }
}
