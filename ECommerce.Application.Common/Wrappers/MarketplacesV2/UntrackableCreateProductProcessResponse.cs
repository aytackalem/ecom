﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class UntrackableCreateProductProcessResponse
    {
        #region Properties
        /// <summary>
        /// Istek ile ilgili donecek hatalari temsil eder.
        /// </summary>
        public List<string> Messages { get; set; }

        public bool IncludeUrl { get; set; }

        public bool MarkDirtyStock { get; set; }

        public bool MarkDirtyPrice { get; set; }
        #endregion

        #region Navigation Properties
        public List<UntrackableCreateProductProcessResponseItem> UntrackableProcessResponseItems { get; set; }
        #endregion
    }
}
