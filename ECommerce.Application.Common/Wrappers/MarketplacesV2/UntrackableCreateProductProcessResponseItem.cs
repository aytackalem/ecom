﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2
{
    public class UntrackableCreateProductProcessResponseItem
    {
        #region Properties
        public int ProductInformationMarketplaceId { get; set; }

        public string Url { get; set; }

        public bool Success { get; set; }

        public List<string> Messages { get; set; }
        #endregion
    }
}
