﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories
{
    public class Category
    {
        public Category()
        {
            CategoryAttributes = new List<CategoryAttribute>();
        }

        #region Properties
        public string Code { get; set; }

        public string ParentCategoryCode { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryAttribute> CategoryAttributes { get; set; }
        #endregion
    }
}
