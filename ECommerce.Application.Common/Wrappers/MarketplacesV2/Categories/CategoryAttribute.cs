﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories
{
    public class CategoryAttribute
    {
        public CategoryAttribute()
        {
            CategoryAttributesValues = new List<CategoryAttributeValue>();
        }

        #region Properties
        public string CategoryCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool AllowCustom { get; set; }

        public bool MultiValue { get; set; }

        public bool Variantable { get; set; }

        public bool Mandatory { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryAttributeValue> CategoryAttributesValues { get; set; }
        #endregion
    }
}
