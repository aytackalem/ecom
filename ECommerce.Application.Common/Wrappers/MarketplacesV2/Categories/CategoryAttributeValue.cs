﻿namespace ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories
{
    public class CategoryAttributeValue
    {
        #region Properties
        public string Code { get; set; }
        
        public string VarinatCode { get; set; }
        
        public string Name { get; set; }
        #endregion
    }
}
