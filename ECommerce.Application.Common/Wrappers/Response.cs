﻿using ECommerce.Application.Common.Wrappers.Base;

namespace ECommerce.Application.Common.Wrappers
{
    public class Response : ResponseBase
    {
        #region Constructors
        public Response()
        {

        }

        public Response(bool success)
        {
            #region Properties
            this.Success = success;
            #endregion
        }

        public Response(bool success, string message)
        {
            #region Properties
            this.Success = success;
            this.Message = message;
            #endregion
        }
        #endregion
    }
}
