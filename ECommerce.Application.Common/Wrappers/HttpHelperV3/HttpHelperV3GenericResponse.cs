﻿using System.Net;

namespace ECommerce.Application.Common.Wrappers.HttpHelperV3
{
    public class HttpHelperV3Response<TContent>
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }

        public string ContentString { get; set; }

        public TContent Content { get; set; }
        #endregion
    }
}
