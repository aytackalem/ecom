﻿using System.Net;

namespace ECommerce.Application.Common.Wrappers.HttpHelperV3
{
    public class HttpHelperV3Response
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }
        #endregion
    }
}
