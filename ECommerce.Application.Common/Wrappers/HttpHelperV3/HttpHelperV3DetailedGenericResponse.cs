﻿using System.Net;

namespace ECommerce.Application.Common.Wrappers.HttpHelperV3
{
    public class HttpHelperV3Response<TOkContent, TBadRequestContent>
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }

        public string ContentString { get; set; }

        public TOkContent Content { get; set; }

        public TBadRequestContent ContentError { get; set; }
        #endregion
    }
}
