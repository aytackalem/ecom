﻿namespace ECommerce.Application.Common.Wrappers
{
    public class HttpResultResponse<TContentObject> : HttpResponse
    {
        #region Properties
        public TContentObject ContentObject { get; set; }

        public string ContentString { get; set; }

        public bool ContentIsNullOrEmpty => string.IsNullOrEmpty(ContentString);
        #endregion
    }
}
