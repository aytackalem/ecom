﻿using ECommerce.Application.Common.Wrappers.Base;
using System.Net;

namespace ECommerce.Application.Common.Wrappers
{
    public class HttpResponse : ResponseBase
    {
        #region Properties
        public HttpStatusCode HttpStatusCode { get; set; }

        public string RequestUri { get; set; }
        #endregion
    }
}
