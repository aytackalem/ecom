﻿using ECommerce.Application.Common.Wrappers.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Wrappers
{
    public class PagedResponse<TData> : ResponseBase
    {
        #region Constructors
        public PagedResponse()
        {
        }

        public PagedResponse(int page, int pageRecordsCount, int recordsCount)
        {
            #region Properties
            this.Page = page;
            this.PageRecordsCount = pageRecordsCount;
            this.RecordsCount = recordsCount;
            this.Data = new List<TData>();
            #endregion
        }
        #endregion

        #region Properties
        public List<TData> Data { get; set; }

        public int Page { get; set; }

        public int PageRecordsCount { get; set; }

        public int RecordsCount { get; set; }

        public int PagesCount => this.RecordsCount > 0 && this.PageRecordsCount > 0 ? Convert.ToInt32(Math.Ceiling(this.RecordsCount / (decimal)this.PageRecordsCount)) : 0;
        #endregion
    }
}
