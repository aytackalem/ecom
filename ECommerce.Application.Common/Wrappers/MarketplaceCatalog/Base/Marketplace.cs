﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.Base
{
    public class Marketplace<T>
    {
        #region Properties
        [JsonProperty("MI")]
        [JsonPropertyName("MI")]
        public string MarketplaceId { get; set; }
        #endregion

        #region Navigation Properties
        [JsonProperty("I")]
        [JsonPropertyName("I")]
        public List<T> Items { get; set; }
        #endregion
    }
}
