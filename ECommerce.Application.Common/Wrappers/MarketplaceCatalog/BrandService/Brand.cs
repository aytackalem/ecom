﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.BrandService
{
    public class Brand
    {
        #region Properties
        [JsonProperty("C")]
        [JsonPropertyName("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        [JsonPropertyName("N")]
        public string Name { get; set; }

        public int? CompanyId { get; set; }
        #endregion
    }
}
