﻿using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.Base;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.BrandService
{
    public class SearchByNameResponse : Marketplace<Brand>
    {
    }
}
