﻿using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.Base;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService
{
    public class GetByMarketplaceIdResponse : Marketplace<Category>
    {
    }
}
