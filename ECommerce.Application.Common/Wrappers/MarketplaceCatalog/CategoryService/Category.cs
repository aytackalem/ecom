﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService
{
    public class Category
    {
        #region Properties
        [JsonProperty("C")]
        [JsonPropertyName("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        [JsonPropertyName("N")]
        public string Name { get; set; }

        public int? CompanyId { get; set; }

        [JsonProperty("B")]
        [JsonPropertyName("B")]
        public string Breadcrumb { get; set; }
        #endregion
    }
}
