﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantValueService
{
    public class VariantValue
    {
        #region Properties
        [JsonPropertyName("VC")]
        [JsonProperty("VC")]
        public string VariantCode { get; set; }

        [JsonPropertyName("C")]
        [JsonProperty("C")]
        public string Code { get; set; }

        [JsonPropertyName("N")]
        [JsonProperty("N")]
        public string Name { get; set; }

        public int? CompanyId { get; set; }
        #endregion
    }
}
