﻿using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.Base;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService
{
    public class GetByCategoryCodesResponse : Marketplace<Variant>
    {
    }
}
