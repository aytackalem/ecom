﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService
{
    public class Variant
    {
        #region Properties
        [JsonProperty("AC")]
        [JsonPropertyName("AC")]
        public bool AllowCustom { get; set; }

        [JsonProperty("M")]
        [JsonPropertyName("M")]
        public bool Mandatory { get; set; }

        [JsonProperty("MU")]
        [JsonPropertyName("MU")]
        public bool Multiple { get; set; }

        [JsonProperty("V")]
        [JsonPropertyName("V")]
        public bool Varianter { get; set; }

        [JsonProperty("C")]
        [JsonPropertyName("C")]
        public string Code { get; set; }

        [JsonProperty("N")]
        [JsonPropertyName("N")]
        public string Name { get; set; }

        public int? CompanyId { get; set; }
        #endregion
    }
}
