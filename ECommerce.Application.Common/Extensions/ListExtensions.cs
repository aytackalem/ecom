﻿using System.Collections;

namespace ECommerce.Application.Common.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Liste null değil ve en az bir elemana sahip ise "True" değeri döner.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool HasItem(this IList list)
        {
            return list != null && list.Count > 0;
        }
    }
}
