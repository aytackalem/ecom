﻿using Helpy.Shared.Crypto;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Extensions
{
    public static class DictionaryExtension
    {
        #region Methods
        public static Dictionary<string, string> Decrypt(this Dictionary<string, string> dictionary)
        {
            var encryptDictionary = new Dictionary<string, string>();
            foreach (var key in dictionary.Keys)
            {
                var value = dictionary[key];
                encryptDictionary.Add(CryptoHelper.Decrypt(key), CryptoHelper.Decrypt(value));
            }
            return encryptDictionary;
        }
        #endregion
    }
}
