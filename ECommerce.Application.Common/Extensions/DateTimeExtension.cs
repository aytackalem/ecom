﻿using System;

namespace ECommerce.Application.Common.Extensions
{
    public static class DateTimeExtension
    {
        public static long ConvertToUnixTimeStamp(this DateTime dateTime)
        {
            var dotNetDateTime = new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                dateTime.Hour,
                dateTime.Minute,
                dateTime.Second,
                dateTime.Millisecond,
                DateTimeKind.Utc);
            var dateTimeOffset = new DateTimeOffset(dotNetDateTime);
            var value = dateTimeOffset.ToUnixTimeMilliseconds();
            return value;
        }

        public static DateTime ConvertToDateTime(this long unixDateTime)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(unixDateTime).DateTime;
        }
    }
}
