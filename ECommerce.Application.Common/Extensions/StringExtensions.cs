﻿using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Extensions
{
    public static class StringExtensions
    {
        #region Methods
        public static string ReplaceChar(this string text)
        {
            if (string.IsNullOrEmpty(text)) return "";

            text = text.ToLower();
            text = text.TrimEnd();
            text = text.Replace("ı", "i");
            text = text.Replace("ü", "u");
            text = text.Replace("ş", "s");
            text = text.Replace("ö", "o");
            text = text.Replace("ç", "c");
            text = text.Replace("ğ", "g");
            text = text.Replace("ū", "u");

            return text;
        }

        public static string GenerateUrl(this string text)
        {
            if (string.IsNullOrEmpty(text)) return text;

            text = text.TrimEnd();

            text = text.ReplaceChar();

            var url = string.Empty;

            foreach (var tLoop in text)
                if (char.IsLetterOrDigit(tLoop) || (char.IsWhiteSpace(tLoop) && !url.EndsWith(' ')))
                    url += tLoop;

            return url.Replace(" ", "-");
        }

        /// <summary>
        /// Metin içerisinde bulunan özel karakterleri temizleyen metod. Örn: 'Yüz Bakımı' değerini 'Yuz Bakimi' olarak degistirir.
        /// </summary>
        /// <param name="text">Temizlenmesi istenen değer.</param>
        /// <returns></returns>
        public static string ToNormalize(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return (new string(chars).Normalize(NormalizationForm.FormC)).Replace("İ", "I").Replace("ı", "i");
        }

        /// <summary>
        /// Metnin tamamını küçük harfe çevirip içerisinde bulunan özel karakterleri temizleyen metod. Örn: 'Yüz Bakımı' değerini 'yuz bakimi' olarak degistirir.
        /// </summary>
        /// <param name="text">Temizlenmesi istenen değer.</param>
        /// <returns></returns>
        public static string ToLowerNormalize(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            return text.ToLower().ToNormalize();
        }

        /// <summary>
        /// Verilen değer içerisinden rakam harici karakterleri temizleyen metod.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToNumber(this string text)
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(text))
                foreach (var pLoop in text)
                    if (char.IsNumber(pLoop))
                        stringBuilder.Append(pLoop);

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Sisteme dışarıdan alınan tüm parametreleri güvenlik amacı ile temizleyen metod.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanParameter(this string text)
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (string.IsNullOrEmpty(text)) return "";

            foreach (var tLoop in text)
            {
                if (char.IsNumber(tLoop) || char.IsLetter(tLoop) || tLoop == ',' || tLoop == '.' || tLoop == '@')
                    stringBuilder.Append(tLoop);

                if (tLoop == '=' || tLoop == ':' || tLoop == '/' || tLoop == ' ' || tLoop == '$')
                    stringBuilder.Append(' ');
            }

            return stringBuilder.ToString();
        }

        public static string FirstLetterUppercase(this string text, CultureInfo cultureInfo = null)
        {
            CultureInfo _cultureInfo = cultureInfo ?? new("tr");

            return string.Join(
                " ", 
                text
                .Split(" ", StringSplitOptions.RemoveEmptyEntries)
                .Select(p => $"{p[..1].ToUpper(_cultureInfo)}{p[1..].ToLower(_cultureInfo)}"));
        }
        #endregion
    }
}
