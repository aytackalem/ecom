﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Common.Wrappers.Base;
using System.Linq;

namespace ECommerce.Application.Common.Extensions
{
    public static class ResponseExtension
    {
        #region Methods
        public static void Check(this ViewModelBase viewModelBase, params ResponseBase[] responseBases)
        {
            viewModelBase.Success = !responseBases.Any(rb => !rb.Success);
        }
        #endregion
    }
}
