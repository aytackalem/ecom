﻿namespace ECommerce.Application.Common.Constants
{
    public static class ImagePath
    {
        #region Properties
        public static string Banner => @"C:\Image\Banner\";

        public static string Product => @"C:\Image\Product\";

        public static string Category => @"C:\Image\Category\";
        #endregion
    }
}
