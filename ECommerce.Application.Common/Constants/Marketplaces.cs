﻿namespace ECommerce.Application.Common.Constants
{
    public static class Marketplaces
    {
        #region Properties
        public static string CicekSepeti => "CS";

        public static string Hepsiburada => "HB";

        public static string GittiGidiyor => "GG";

        public static string Hopi => "HO";

        public static string N11 => "N11";

        public static string N11V2 => "N11V2";

        public static string Pazarama => "PA";

        public static string Trendyol => "TY";

        public static string TrendyolEurope => "TYE";

        public static string Vodafone => "VO";

        public static string OMNI => "OMNI";

        public static string IKAS => "IK";

        public static string Morhipo => "M";

        public static string Modanisa => "MN";

        public static string PttAvm => "PTT";

        public static string Ideasoft => "IS";

        public static string Modalog => "ML";

        public static string TSoft => "TSoft";

        public static string Lcw => "LCW";

        public static string Boyner => "BY";
        #endregion
    }
}
