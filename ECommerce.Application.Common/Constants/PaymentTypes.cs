﻿namespace ECommerce.Application.Common.Constants
{
    public static class PaymentTypes
    {
        #region Properties
        public static string Havale => "C";

        public static string IndirimKuponu => "IK";

        public static string ParaPuan => "MP";

        public static string OnlineOdeme => "OO";

        public static string KapıdaNakit => "DO";

        public static string KapıdaKrediKartıNakit => "PDO";

        #endregion
    }
}
