﻿namespace ECommerce.Application.Common.Constants
{
    public static class StocktakingTypes
    {
        #region Properties
        public static string Beklemede => "BE";

        public static string DevamEdiyor => "DE";

        public static string OnayBekliyor => "OB";

        public static string Onaylanmis => "ON";

        public static string Tamamlanmis => "TA";
        #endregion
    }
}
