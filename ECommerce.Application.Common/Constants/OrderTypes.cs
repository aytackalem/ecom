﻿namespace ECommerce.Application.Common.Constants
{
    public static class OrderTypes
    {
        #region Properties
        public static string Beklemede => "BE";

        public static string Iptal => "IP";

        public static string Iade => "IA";

        public static string OnaylanmisSiparis => "OS";

        public static string KargoyaTeslimEdildi => "KTE";

        public static string Paketleniyor => "PA";

        public static string TedarikEdilemeyen => "TE";

        public static string TeslimEdildi => "TS";

        public static string Paketlendi => "PD";

        public static string Tamamlandi => "TA";

        public static string Toplaniyor => "TO";

        public static string Toplandi => "TD";
        #endregion
    }
}
