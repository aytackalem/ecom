﻿namespace ECommerce.Application.Common.Constants
{
    public static class ImageUrl
    {
        #region Properties
        public static string Banner => @"http://localhost:41491/img/upload/banner";

        public static string Product => @"http://localhost:41491/img/upload/product";

        public static string Category => @"http://localhost:41491/img/upload/category";
        #endregion
    }
}
