﻿namespace ECommerce.Application.Common.Constants
{
    public static class OrderSources
    {
        #region Properties
        public static int Pazaryeri => 1;
        
        public static int Web => 2;
        
        public static int XML => 4;

        public static int Diger => 6;

        public static int Temsilci => 7;

        public static int TemsilciInfluencer => 8;

        public static int Pazarlama => 9;

        public static int WhatsApp => 10;

        public static int Değişim => 11;
        #endregion
    }
}
