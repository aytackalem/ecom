﻿namespace ECommerce.Application.Common.Constants
{
    public static class ShipmentCompanies
    {
        #region Properties
        public static string ArasKargo => "ARS";
        public static string BorusanLojistik => "B";
        public static string HorozLojistik => "H";
        public static string HepsiJet => "HX";
        public static string Kurye => "KU";
        public static string MngKargo => "M";
        public static string PttKargo => "P";
        public static string SuratKargo => "S";
        public static string TrendyolExpress => "TYEX";
        public static string Ups => "U";
        public static string YurticiKargo => "Y";
        public static string ArtosDagitim => "ARD";
        public static string Focus => "FC";
        public static string DigerKargo => "OT";

        #endregion
    }
}
