﻿namespace ECommerce.Application.Common.ViewModels.Base
{
    public abstract class ViewModelBase
    {
        #region Properties
        public bool Success { get; set; }

        public string Message { get; set; }
        #endregion
    }
}
