﻿using ECommerce.Application.Common.Wrappers.Base;
using System;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.ExceptionHandling
{
    public static class ExceptionHandlerV2
    {
        #region Method
        public static async Task<TResponse> ResponseHandleAsync<TResponse>(Func<TResponse, Task> action, Action<TResponse> catchAction = null) where TResponse : ResponseBase, new()
        {
            TResponse response = new();
            try
            {
                await action(response);
            }
            catch (Exception e)
            {
                if (catchAction == null)
                {
                    response.Success = false;
                    response.Message = "Bilinmeyen bir hata oluştu.";
                    response.Exception = e;
                }
                else
                    catchAction(response);
            }
            return response;
        }

        public static async Task HandleAsync(Func<Task> action, Func<Exception, Task> catchAction = null)
        {
            try
            {
                await action();
            }
            catch (Exception e)
            {
                if (catchAction != null)
                    await catchAction(e);
            }
        }
        #endregion
    }
}
