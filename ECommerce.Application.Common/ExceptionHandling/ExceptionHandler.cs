﻿using ECommerce.Application.Common.Wrappers.Base;
using System;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.ExceptionHandling
{
    public static class ExceptionHandler
    {
        #region Method
        public static void Handle(Action tryAction, Action<Exception> catchAction = null)
        {
            try
            {
                tryAction();
            }
            catch (Exception exception)
            {
                if (catchAction != null)
                    catchAction(exception);
            }
        }

        public static TResponse ResultHandle<TResponse>(Action<TResponse> action, Action<TResponse, Exception> catchAction = null) where TResponse : ResponseBase, new()
        {
            TResponse response = new();
            ExceptionHandler.Handle(() =>
            {
                action(response);
            }, (e) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                if (catchAction != null)
                {
                    catchAction(response, e);
                }
            });
            return response;
        }

        public static TObject ObjectHandle<TObject>(Action<TObject> action, Action<TObject, Exception> catchAction = null) where TObject : new()
        {
            TObject obj = new();
            ExceptionHandler.Handle(() =>
            {
                action(obj);
            }, (e) =>
            {
                if (catchAction != null)
                {
                    catchAction(obj, e);
                }
            });
            return obj;
        }

        public static async Task HandleAsync(Func<Task> funcAsync, Action<Exception> catchAction)
        {
            try
            {
                await funcAsync();
            }
            catch (Exception exception)
            {
                catchAction(exception);
            }
        }

        public static async Task<TResponse> ResultHandleAsync<TResponse>(Func<TResponse, Task<TResponse>> funcAsync, Action<TResponse, Exception> catchAction) where TResponse : ResponseBase, new()
        {
            TResponse result = new TResponse();
            await ExceptionHandler.HandleAsync(async () =>
            {
                result = await funcAsync(result);
            }, (e) =>
            {
                catchAction(result, e);
            });
            return result;
        }
        #endregion
    }
}
