﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Communication
{
    public interface ISmsService
    {
        #region Methods
        Response Send(string phoneNumber, string orderTypeId, string paymentTypeId, Dictionary<string, string> parameters);

        Response Send(string phoneNumber, string message);
        #endregion
    }
}
