﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Communication
{
    public interface ISmsProvider
    {
        #region Methods
        Response Send(string phoneNumber, string message);
        #endregion
    }
}
