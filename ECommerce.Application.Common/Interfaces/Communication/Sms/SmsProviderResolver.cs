﻿namespace ECommerce.Application.Common.Interfaces.Communication
{
    public delegate ISmsProvider SmsProviderResolver(string smsProviderId);
}
