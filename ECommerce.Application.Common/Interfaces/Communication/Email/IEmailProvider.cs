﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Communication
{
    public interface IEmailProvider
    {
        #region Methods
        Response Send(List<string> emailAddress, string subject, string body, bool sendAsync = true);
        #endregion
    }
}
