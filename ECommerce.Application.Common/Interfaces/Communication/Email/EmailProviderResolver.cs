﻿namespace ECommerce.Application.Common.Interfaces.Communication
{
    public delegate IEmailProvider EmailProviderResolver(string emailProviderId);
}
