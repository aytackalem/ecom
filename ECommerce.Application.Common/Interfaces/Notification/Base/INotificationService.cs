﻿using ECommerce.Application.Common.Parameters.Notification;
using ECommerce.Application.Common.Wrappers;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Notification.Base
{
    public interface INotificationService
    {
        #region Methods
        Task<Response> PostAsync(InsertNotificationParameter parameter);

        Task<PagedResponse<Wrappers.Notification.Notification>> GetAsync(GetParameter parameter);

        Task<DataResponse<long>> GetAsync(GetCountParameter parameter);

        Task<Response> DeleteAsync(DeleteNotificationParameter parameter);
        #endregion
    }
}
