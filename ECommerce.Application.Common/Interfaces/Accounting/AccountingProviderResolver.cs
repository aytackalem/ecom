﻿namespace ECommerce.Application.Common.Interfaces.Accounting
{
    public delegate IAccountingProvider AccountingProviderResolver(string accountingProviderId);
}
