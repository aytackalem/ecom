﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Accounting
{
    public interface IAccountingProviderService
    {
        #region Methods
        /// <summary>
        /// Sipariş detay bilgilerini kullanarak fatura oluşturan fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<AccountingInfo> CreateInvoice(int orderId);

        /// <summary>
        /// Sipariş detay bilgilerini kullanarak tek bir fatura oluşturan fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds);

        /// <summary>
        /// Sipariş iade bilgilerini kullanarak gider pusulası oluşturan fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<AccountingCancel> CancelInvoice(int orderId);

        /// <summary>
        /// Sipariş detay bilgilerini kullanarak stok transfer fişi oluşturan fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<string> CreateStockTransfer(int orderId);

        /// <summary>
        /// Stok transfer fişlerindeki sipariş id bilgilerini dönen fonksiyon.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<int>> ReadStockTransfers();

        /// <summary>
        /// En son stok transfer fişinde bulunan sipariş id bilgisini dönen fonksiyon.
        /// </summary>
        /// <returns></returns>
        DataResponse<int> ReadLastStockTransfer();

        /// <summary>
        /// Stok transfer fişini silen fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Response DeleteStockTransfer(int orderId);

        /// <summary>
        /// İlgili stok kodu ve depo id bilgisi ile stok adedini dönen fonksiyon.
        /// </summary>
        /// <param name="stockCode"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        DataResponse<int> ReadStock(string stockCode, string warehouseId);

        DataResponse<bool> CreateStocktaking(int stocktakingId);
        #endregion
    }
}
