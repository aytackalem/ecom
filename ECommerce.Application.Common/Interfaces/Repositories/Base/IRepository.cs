using ECommerce.Domain.Entities.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories.Base
{
    public partial interface IRepository<TEntity, TId> where TEntity : EntityBase<TId>
    {
        #region Methods
        Boolean Create(TEntity entity);

        Task<Boolean> CreateAsync(TEntity entity, CancellationToken cancellationToken = default);

        Boolean BulkInsert(List<TEntity> entities);

        TEntity Read(TId id);

        Boolean Update(TEntity entity);

        Task<Boolean> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);

        Boolean Update(List<TEntity> entities);

        IQueryable<TEntity> DbSet();
        #endregion
    }
}