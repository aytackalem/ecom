using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductPropertyValueRepository : IRepository<Domain.Entities.PropertyValue, int>
	{
	}
}