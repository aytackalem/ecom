using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories;

public interface IWholesaleOrderTypeRepository : IRepository<WholesaleOrderType, String>
{

}