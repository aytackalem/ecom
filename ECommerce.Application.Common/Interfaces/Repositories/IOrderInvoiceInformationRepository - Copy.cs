using System;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IOrderInvoiceInformationRepository : IRepository<OrderInvoiceInformation, Int32>
	{
		
	}
}