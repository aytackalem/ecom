using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface ICategoryRepository : IRepository<Category, Int32>
	{
        Category ReadAll(Int32 id);

        void BulkInsertMapping(List<Category> categories);

    }
}