using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IUpsCityRepository : IRepository<UpsCity, Int32>
	{
		UpsCity Read(string name);
	}
}