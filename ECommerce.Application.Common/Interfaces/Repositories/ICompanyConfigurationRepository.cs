using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface ICompanyConfigurationRepository : IRepository<CompanyConfiguration, Int32>
	{
    }
}