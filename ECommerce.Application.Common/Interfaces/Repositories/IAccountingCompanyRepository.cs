using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IAccountingCompanyRepository : IRepository<Domain.Entities.AccountingCompany, int>
	{
	}
}