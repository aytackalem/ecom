using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IVariantRepository : IRepository<Variant, int>
	{
        void BulkInsertMapping(List<Variant> variants);

    }
}