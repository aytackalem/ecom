using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IAccountingRepository : IRepository<Domain.Entities.Accounting, string>
	{
	}
}