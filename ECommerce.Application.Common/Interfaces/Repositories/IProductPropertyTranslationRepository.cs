using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductPropertyTranslationRepository : IRepository<Domain.Entities.PropertyTranslation, int>
	{
	}
}