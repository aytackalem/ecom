﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public interface IUpsService
    {
        #region Methods
        /// <summary>
        /// Paketleme için gönderilen ilçe bilgileri için ups'den dönen id bilgisi
        /// </summary>
        /// <param name="name"></param>
        /// <param name="upsCityId"></param>
        /// <returns></returns>
        DataResponse<UpsAddress> Read(string cityName, string districtName);
        #endregion

    }
}
