using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationMarketplaceBulkPriceRepository : IRepository<ProductInformationMarketplaceBulkPrice, Int32>
    {
        #region Methods



        #endregion
    }
}