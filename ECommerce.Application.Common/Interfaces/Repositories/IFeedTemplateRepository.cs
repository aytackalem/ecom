using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IFeedTemplateRepository : IRepository<FeedTemplate, string>
	{	
	}
}