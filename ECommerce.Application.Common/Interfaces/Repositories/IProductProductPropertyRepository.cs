using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductProductPropertyRepository : IRepository<Domain.Entities.ProductProperty, int>
	{
	}
}