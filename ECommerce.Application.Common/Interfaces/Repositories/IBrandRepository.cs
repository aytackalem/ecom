using System;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IBrandRepository : IRepository<Brand, Int32>
	{
        void BulkInsertMapping(List<Brand> brands);

    }
}