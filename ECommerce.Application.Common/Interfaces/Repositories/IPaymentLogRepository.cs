using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IPaymentLogRepository : IRepository<PaymentLog, Int32>
	{	
	}
}