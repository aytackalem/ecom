using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IOrderPackingRepository : IRepository<OrderPacking, Int32>
	{
        #region Methods
        #endregion
    }
}