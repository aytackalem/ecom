using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Application.Common.Parameters.ProductInformations;
using ECommerce.Domain.Entities;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationMarketplaceVariantValueRepository : IRepository<ProductInformationMarketplaceVariantValue, int>
	{
        #region Methods
 
        #endregion
    }
}