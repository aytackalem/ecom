using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationTranslationBreadcrumbRepository : IRepository<Domain.Entities.ProductInformationTranslationBreadcrumb, int>
	{
	}
}