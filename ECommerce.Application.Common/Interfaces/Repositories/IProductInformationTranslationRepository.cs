using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationTranslationRepository : IRepository<Domain.Entities.ProductInformationTranslation, int>
	{
	}
}