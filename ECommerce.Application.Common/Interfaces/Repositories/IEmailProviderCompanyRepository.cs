using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IEmailProviderCompanyRepository : IRepository<Domain.Entities.EmailProviderCompany, int>
	{
	}
}