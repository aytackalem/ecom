using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IEmailProviderRepository : IRepository<Domain.Entities.EmailProvider, String>
	{
	}
}