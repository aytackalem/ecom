using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IExchangeRateRepository : IRepository<ExchangeRate, Int32>
	{
    }
}