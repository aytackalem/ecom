using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Application.Common.Parameters.ProductInformations;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IOrderRepository : IRepository<Order, Int32>
	{
        #region Methods
        void Reload(Order order);

        bool UpdateOrderType(int id, string orderTypeId);

        bool UpdateOrderType(int id, string orderTypeId, string packageNumber, string trackingCode);

        void BulkInsertMapping(List<Order> orders);

        public int DeliveredBulkCompare(List<Order> orders, string orderTypeId, string[] notOrderTypeIds = null);
        public int CanceledBulkCompare(List<Order> orders);

        #endregion
    }
}