using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IOrderPickingRepository : IRepository<OrderPicking, Int32>
	{
        #region Methods
        #endregion
    }
}