using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IOrderShipmentRepository : IRepository<OrderShipment, Int32>
	{
        #region Methods
        bool UpdateOrderType(int orderId, string trackingBase64);
        #endregion
    }
}