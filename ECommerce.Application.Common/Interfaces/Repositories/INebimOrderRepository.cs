using System;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface INebimOrderRepository : IRepository<NebimOrder, Int32>
	{
		
	}
}