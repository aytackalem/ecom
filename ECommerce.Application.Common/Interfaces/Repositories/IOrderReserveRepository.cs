using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities.Companyable;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public interface IOrderReserveRepository : IRepository<OrderReserve, Int32>
    {
    }
}