using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IStocktakingTypeRepository : IRepository<StocktakingType, string>
	{
        #region Methods

        #endregion
    }
}