using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities.Domainable;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IProductSourceDomainRepository : IRepository<ProductSourceDomain, Int32>
	{
	}
}