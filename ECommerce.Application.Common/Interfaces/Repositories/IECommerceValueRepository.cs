using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities.Companyable;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
	public partial interface IECommerceVariantValueRepository : IRepository<ECommerceVariantValue, long>
	{
		
	}
}