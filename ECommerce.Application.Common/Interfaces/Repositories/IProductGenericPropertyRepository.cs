using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;

namespace ECommerce.Application.Common.Interfaces.Repositories;

public partial interface IProductGenericPropertyRepository : IRepository<ProductGenericProperty, int>
{
}