using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationRepository : IRepository<ProductInformation, Int32>
    {
        #region Methods
        void BulkInsertMapping(List<ProductInformation> productInformations);
        #endregion
    }
}