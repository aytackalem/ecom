using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IContactRepository : IRepository<Contact, Int32>
	{
    }
}