using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IMarketplaceConfigurationRepository : IRepository<MarketplaceConfiguration, int>
    {
        Task<List<MarketplaceConfiguration>> ReadByQueueIdAsync(int queueId);
    }
}