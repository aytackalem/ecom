using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IApplicationRepository : IRepository<Domain.Entities.Application, String>
	{
	}
}