using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IMarketplaceVariantValueMappingRepository : IRepository<MarketplaceVariantValueMapping, int>
	{
		
	}
}