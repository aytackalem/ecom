using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IUpsAreaRepository : IRepository<UpsArea, Int32>
	{
		UpsArea Read(string name, int upsCityId);
	}
}