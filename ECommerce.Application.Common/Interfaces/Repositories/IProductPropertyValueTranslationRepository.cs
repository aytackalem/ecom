using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductPropertyValueTranslationRepository : IRepository<Domain.Entities.PropertyValueTranslation, int>
	{
	}
}