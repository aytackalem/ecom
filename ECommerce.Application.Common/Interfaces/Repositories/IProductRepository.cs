using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductRepository : IRepository<Product, Int32>
    {
        #region Methods
        #region Dirty Product
        Task<Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>> ReadDirtyProductsAsync(int queueId, string marketplaceId);

        Task<Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>> ReadDirtyProductsAsync(int queueId, bool opened, string marketplaceId);

        bool UpdateDirtyProduct(List<int> productInformationMarketplaceIds, bool dirtyProductInformation);
        #endregion

        void BulkInsertMapping(List<Product> products);
        #endregion
    }
}