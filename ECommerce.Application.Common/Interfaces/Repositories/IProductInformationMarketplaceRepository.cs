using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Application.Common.Parameters.ProductInformationMarketplaces;
using ECommerce.Application.Common.Parameters.ProductInformations;
using ECommerce.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IProductInformationMarketplaceRepository : IRepository<ProductInformationMarketplace, int>
    {
        #region Methods
        void BulkInsertMapping(List<ProductInformationMarketplace> productInformationMarketplaces);

        void BulkCompare(BulkCompareRequest bulkCompareRequest);

        void UpdateDirtyPrice(int id, bool dirtyPrice);

        bool UpdateDirtyPrice(List<int> productInformationMarketplaceIds, bool dirtyPrice);

        void UpdateDirtyStock(int id, bool dirtyStock);

        bool UpdateDirtyStock(List<int> productInformationMarketplaceIds, bool dirtyStock);

        void UpdateDirtyProductInformation(int id, bool dirtyProductInformation);

        int UpdateIgnoreSystemNotification(int id, bool ignoreSystemNotification);

        Task<List<DataTransferObjects.MarketplacesV2.DirtyPrice>> ReadDirtyPricesAsync(int queueId, string marketplaceId);

        Task<List<DataTransferObjects.MarketplacesV2.DirtyStock>> ReadDirtyStocksAsync(int queueId, string marketplaceId);

        void UpdateDirtyListPriceByBarcode(List<string> barcodes, int marketplaceCompanyId, bool dirtyPrice, int companyId);

        void UpdateDirtyListStockByBarcode(List<string> barcodes, int marketplaceCompanyId, bool dirtyStock, int companyId);

        void UpdateDirtyProductInformationByProductId(int productId);

        bool UpdateOpened(List<int> productInformationMarketplaceIds, bool opened);
        bool UpdateUUId(List<UpdateUUIdRequest> requests);

        bool UpdateOpenedUrl(Dictionary<int, string> productInformationMarketplaceIds, bool opened, bool? dirtyStock = null, bool? dirtyPrice = null);

        void BulkStockLastUpdate(List<ProductInformationMarketplace> productInformationMarketplaces);

        void BulkPriceLastUpdate(List<ProductInformationMarketplace> productInformationMarketplaces);

        Task PriceLogAsync(string marketplaceId, List<int> productInformationMarketplaceIds);

        Task StockLogAsync(string marketplaceId, List<int> productInformationMarketplaceIds);
        #endregion
    }
}