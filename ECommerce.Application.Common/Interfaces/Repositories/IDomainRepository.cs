using ECommerce.Application.Common.Interfaces.Repositories.Base;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IDomainRepository : IRepository<Domain.Entities.Domain, int>
	{
	}
}