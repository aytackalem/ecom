using ECommerce.Application.Common.Interfaces.Repositories.Base;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IEmailProviderConfigurationRepository : IRepository<Domain.Entities.EmailProviderConfiguration, int>
	{
	}
}