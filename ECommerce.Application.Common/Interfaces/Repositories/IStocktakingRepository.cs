using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities.Domainable;
using System;

namespace ECommerce.Application.Common.Interfaces.Repositories
{
    public partial interface IStocktakingRepository : IRepository<Stocktaking, Int32>
	{
        #region Methods
        Boolean Delete(int id);
        #endregion
    }
}