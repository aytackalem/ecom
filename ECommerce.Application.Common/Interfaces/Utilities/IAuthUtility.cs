﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Utilities
{
    public interface IAuthUtility<TUser>
    {
        #region Methods
        /// <summary>
        /// Kullanıcı giriş yapan method
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Response Login(TUser user, bool keepLoggedIn);

        /// <summary>
        /// Kullanıcı çıkış yapan method
        /// </summary>
        /// <returns></returns>
        Response Logout();

        /// <summary>
        /// Kullanıcı giriş yaptıktan sonra kullanıcı bilgileri alınınan method
        /// </summary>
        /// <returns></returns>
        DataResponse<TUser> GetLogged();

        /// <summary>
        /// Kullanıcı Şifremi unuttum'a basarak token'lı bir şekilde mail gönderilen method
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Response ForgotPassword(TUser user);
        /// <summary>
        /// Kullanıcıya ForgotPassword'den gelen tokenla birlikte gelen şifreyi değiştiren method
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Response ResetPassword(TUser user);
        /// <summary>
        /// Kullanıcı giriş yaptıkdan sonra şifresinin değiştirildiği method
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Response UpdatePassword(TUser user);

        /// <summary>
        /// Kullanıcı giriş yaptıktan sonra birden çok domaini varsa domaini değiştirdiği method
        /// </summary>
        /// <param name="domainId"></param>
        /// <returns></returns>
        Response ChangeDomain(TUser user);


        /// <summary>
        /// Kullanıcı giriş yaptıktan sonra birden çok firması varsa değiştirildiği method
        /// </summary>
        /// <param name="domainId"></param>
        /// <returns></returns>
        Response ChangeCompany(int companyId);
        #endregion
    }
}
