﻿using ECommerce.Domain.Entities;
using ECommerce.Domain.Entities.Companyable;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Application.Common.Interfaces.Context
{
    public interface IApplicationDbContext
    {
        #region Property
        DbSet<Domain.Entities.Application> Applications { get; set; }

        DbSet<Domain.Entities.Accounting> Accountings { get; set; }

        DbSet<AccountingConfiguration> AccountingConfigurations { get; set; }

        DbSet<AccountingCompany> AccountingCompanies { get; set; }

        DbSet<OrderReturnDetail> OrderReturnDetails { get; set; }

        DbSet<OrderViewing> OrderViewings { get; set; }

        DbSet<Expense> Expenses { get; set; }

        DbSet<ExpenseSource> ExpenseSources { get; set; }

        DbSet<ExchangeRate> ExchangeRates { get; set; }

        DbSet<OrderPacking> OrderPackings { get; set; }

        DbSet<CategoryTranslationBreadcrumb> CategoryTranslationBreadcrumbs { get; set; }

        DbSet<CategoryProduct> CategoryProducts { get; set; }

        DbSet<Contact> Contacts { get; set; }

        DbSet<ContactTranslation> ContactTranslations { get; set; }

        DbSet<TwoFactorCustomerUser> TwoFactorCustomerUsers { get; set; }

        DbSet<Domain.Entities.Domain> Domains { get; set; }

        DbSet<FacebookConfigration> FacebookConfigrations { get; set; }

        DbSet<FrequentlyAskedQuestion> FrequentlyAskedQuestions { get; set; }

        DbSet<FrequentlyAskedQuestionTranslation> FrequentlyAskedQuestionTranslations { get; set; }

        DbSet<GoogleConfiguration> GoogleConfigrations { get; set; }

        DbSet<LabelTranslation> LabelTranslations { get; set; }

        DbSet<OrderPicking> OrderPickings { get; set; }

        DbSet<Tenant> Tenants { get; set; }

        DbSet<Bank> Banks { get; set; }

        DbSet<Bin> Bins { get; set; }

        DbSet<Brand> Brands { get; set; }

        DbSet<Company> Companies { get; set; }

        DbSet<CompanyContact> CompanyContacts { get; set; }

        DbSet<CompanyConfiguration> CompanyConfigurations { get; set; }

        DbSet<OrderSource> OrderSources { get; set; }

        DbSet<MarketplaceBrandMapping> MarketplaceBrandMappings { get; set; }

        DbSet<BrandDiscountSetting> BrandDiscountSettings { get; set; }

        DbSet<Category> Categories { get; set; }

        DbSet<CategoryDiscountSetting> CategoryDiscountSettings { get; set; }

        DbSet<CategoryTranslation> CategoryTranslations { get; set; }

        DbSet<City> Cities { get; set; }

        DbSet<Country> Countries { get; set; }

        DbSet<Currency> Currencies { get; set; }

        DbSet<Customer> Customers { get; set; }

        DbSet<CustomerAddress> CustomerAddresses { get; set; }

        DbSet<CustomerInvoiceInformation> CustomerInvoiceInformations { get; set; }

        DbSet<CustomerContact> CustomerContacts { get; set; }

        DbSet<CustomerRole> CustomerRoles { get; set; }

        DbSet<CustomerUser> CustomerUsers { get; set; }

        DbSet<DiscountCoupon> DiscountCoupons { get; set; }

        DbSet<DiscountCouponSetting> DiscountCouponSettings { get; set; }

        DbSet<District> Districts { get; set; }

        DbSet<EntireDiscount> EntireDiscounts { get; set; }

        DbSet<EntireDiscountSetting> EntireDiscountSettings { get; set; }

        DbSet<GetXPayYSetting> GetXPayYSettings { get; set; }

        DbSet<Label> Labels { get; set; }

        DbSet<Language> Languages { get; set; }

        DbSet<Neighborhood> Neighborhoods { get; set; }

        DbSet<Order> Orders { get; set; }

        DbSet<OrderBilling> OrderBillings { get; set; }

        DbSet<OrderDeliveryAddress> OrderDeliveryAddresses { get; set; }

        DbSet<OrderDetail> OrderDetails { get; set; }

        DbSet<OrderInvoiceInformation> OrderInvoiceInformations { get; set; }

        DbSet<OrderNote> OrderNotes { get; set; }

        DbSet<OrderShipment> OrderShipments { get; set; }

        DbSet<OrderSms> OrderSmsses { get; set; }

        DbSet<OrderTechnicInformation> OrderTechnicInformations { get; set; }

        DbSet<OrderType> OrderTypes { get; set; }

        DbSet<OrderTypeTranslation> OrderTypeTranslations { get; set; }

        DbSet<Domain.Entities.Payment> Payments { get; set; }

        DbSet<PaymentType> PaymentTypes { get; set; }

        DbSet<PaymentTypeSetting> PaymentTypeSettings { get; set; }

        DbSet<Pos> Posses { get; set; }

        DbSet<PosConfiguration> PosConfigurations { get; set; }

        DbSet<Product> Products { get; set; }

        DbSet<ProductMarketplace> ProductMarketplaces { get; set; }

        DbSet<ProductInformationSubscription> ProductInformationSubscriptions { get; set; }

        DbSet<ProductInformationComment> ProductInformationComments { get; set; }

        DbSet<ProductInformation> ProductInformations { get; set; }

        DbSet<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        DbSet<ProductInformationPrice> ProductInformationPriceses { get; set; }

        DbSet<ProductLabel> ProductLabels { get; set; }

        DbSet<ProductInformationVariant> ProductInformationVariants { get; set; }

        DbSet<ShipmentCompany> ShipmentCompanies { get; set; }

        DbSet<ShoppingCartDiscountedProduct> ShoppingCartDiscountedProducts { get; set; }

        DbSet<ShoppingCartDiscountedProductSetting> ShoppingCartDiscountedProductSettings { get; set; }

        DbSet<SmsTemplate> SmsTemplates { get; set; }

        DbSet<Supplier> Suppliers { get; set; }

        DbSet<Variant> Variants { get; set; }

        DbSet<VariantValue> VariantValues { get; set; }

        DbSet<MarketplaceCompany> MarketplaceCompanies { get; set; }

        DbSet<ProductInformationMarketplaceVariantValue> ProductInformationMarketplaceVariantValues { get; set; }

        #endregion
    }
}
