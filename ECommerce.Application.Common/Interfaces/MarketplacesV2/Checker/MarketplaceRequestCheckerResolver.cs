﻿namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker
{
    public delegate Base.IMarketplaceRequestChecker MarketplaceRequestCheckerResolver(string requestType);
}
