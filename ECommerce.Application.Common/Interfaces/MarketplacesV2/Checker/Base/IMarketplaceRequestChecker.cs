﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base
{
    public interface IMarketplaceRequestChecker
    {
        #region Methods
        Task CheckAsync(MarketplaceRequestCheckerRequest request);
        #endregion
    }
}
