﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IStockPriceRequestExportable
    {
        #region Properties
        public bool StockPriceLoggable { get; }
        #endregion

        #region Methods
        void Validate(List<MarketplaceRequestUpdateStock> requests);

        void Validate(List<MarketplaceRequestUpdatePrice> requests);

        List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests);

        List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests);
        #endregion
    }
}
