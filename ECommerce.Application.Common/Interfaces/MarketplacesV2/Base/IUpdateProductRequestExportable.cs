﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IUpdateProductRequestExportable
    {
        #region Methods
        void Validate(List<MarketplaceRequestUpdateProduct> requests);

        List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests);
        #endregion
    }
}
