﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2CreateProductUntrackable : ICreateProductRequestExportable
    {
        #region Properties
        public int? WaitingSecondPerCreateProductRequest { get; }
        #endregion

        #region Methods
        Task<HttpHelperV3Response<UntrackableCreateProductProcessResponse>> SendAsync(MarketplaceRequestCreateProduct request);
        #endregion
    }
}