﻿using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Commissions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface ICommission
    {
        Task<HttpHelperV3Response<List<Commission>>> GetCommissions(DateTime startDate, DateTime endDate, Dictionary<string, string> marketplaceConfigurations);
    }
}
