﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2OrderGetable
    {
        #region Methods
        Task<List<Order>> GetOrders(MarketplaceRequestGetOrder request);
        #endregion
    }
}
