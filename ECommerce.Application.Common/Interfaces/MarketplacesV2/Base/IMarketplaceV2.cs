﻿namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2
    {
        #region Properties
        public string Id { get; }
        #endregion
    }
}
