﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2UpdateProductTrackable : IUpdateProductRequestExportable
    {
        #region Properties
        public int? WaitingSecondPerCreateProductRequest { get; }

        public int? WaitingSecondPerUpdateProductRequest { get; }
        #endregion

        #region Methods
        Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request);

        Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request);
        #endregion
    }
}