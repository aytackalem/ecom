﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2CategoryGetable
    {
        #region Methods
        Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request);
        #endregion
    }
}
