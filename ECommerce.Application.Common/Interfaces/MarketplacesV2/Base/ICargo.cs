﻿using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Shipments;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface ICargo
    {
        Task<HttpHelperV3Response<List<Cargo>>> GetCargoFees(DateTime startDate, DateTime endDate, Dictionary<string, string> marketplaceConfigurations);
    }
}
