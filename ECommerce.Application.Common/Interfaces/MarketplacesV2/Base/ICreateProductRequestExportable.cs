﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface ICreateProductRequestExportable
    {
        #region Properties
        /// <summary>
        /// Pazaryeri *upsert mantigi ile calisiyor ise bu ozellige "true" degeri atanir.
        /// 
        /// *Upsert: Update ve create sureci bir arada isleme durumu.
        /// </summary>
        public bool IsUpsert { get; }
        #endregion

        #region Methods
        void Validate(List<MarketplaceRequestCreateProduct> requests);

        List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests);
        #endregion
    }
}
