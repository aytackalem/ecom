﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Base
{
    public interface IMarketplaceV2StockPriceUntrackable : IStockPriceRequestExportable
    {
        #region Properties
        public int? WaitingSecondPerStockRequest { get; }

        public int? WaitingSecondPerPriceRequest { get; }
        #endregion

        #region Methods
        Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request);

        Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request);
        #endregion
    }
}