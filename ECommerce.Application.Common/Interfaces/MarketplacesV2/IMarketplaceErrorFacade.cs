﻿using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2
{
    public interface IMarketplaceErrorFacade
    {
        #region Methods
        Task SendNotification(MarketplaceRequestNotification notification);

        Task SendNotification(MarketplaceRequestExceptionNotification notification);

        Task SendNotification(MarketplaceRequestItemExceptionNotification notification);
        #endregion
    }

    public class MarketplaceRequestExceptionNotification
    {
        #region Properties
        public int QueueId { get; set; }

        public string MarketplaceId { get; set; }

        public string JobName { get; set; }

        public string Command { get; set; }

        public string ErrorText { get; set; }

        public string DbName { get; set; }
        #endregion
    }

    public class MarketplaceRequestItemExceptionNotification
    {
        #region Properties
        public int QueueId { get; set; }

        public string RequestId { get; set; }

        public string MarketplaceId { get; set; }

        public string JobName { get; set; }

        public string Command { get; set; }

        public string ErrorText { get; set; }

        public string DbName { get; set; }
        #endregion
    }

    public class MarketplaceRequestNotification
    {
        #region Properties
        public int QueueId { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public string RequestId { get; set; }

        public string JobName { get; set; }

        public string Command { get; set; }

        public string ErrorText { get; set; }

        public string[] Attachments { get; set; }
        #endregion
    }
}
