﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers
{
    public delegate IMarketplaceV2StockPriceUntrackable MarketplaceV2StockPriceUntrackableResolver(string marketplaceId);
}