﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers
{
    public delegate IStockPriceRequestExportable MarketplaceV2StockPriceExportableResolver(string marketplaceId);
}