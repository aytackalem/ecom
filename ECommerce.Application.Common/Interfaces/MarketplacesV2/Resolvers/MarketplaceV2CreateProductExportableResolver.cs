﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers
{
    public delegate ICreateProductRequestExportable MarketplaceV2CreateProductExportableResolver(string marketplaceId);
}