﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers
{
    public delegate IMarketplaceV2StockPriceTrackable MarketplaceV2StockPriceTrackableResolver(string marketplaceId);
}