﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers
{
    public delegate IMarketplaceV2UpdateProductUntrackable MarketplaceV2UpdateProductUntrackableResolver(string marketplaceId);
}