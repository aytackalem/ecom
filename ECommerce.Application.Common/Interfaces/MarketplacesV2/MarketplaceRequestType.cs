﻿namespace ECommerce.Application.Common.Interfaces.MarketplacesV2
{
    public enum MarketplaceRequestType
    {
        #region Members
        CreateProduct,
        UpdateProduct,
        Stock,
        Price
        #endregion
    }
}
