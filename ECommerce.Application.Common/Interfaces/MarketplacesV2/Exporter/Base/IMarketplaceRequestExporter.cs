﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base
{
    public interface IMarketplaceRequestExporter
    {
        #region Methods
        Task ExportAsync(MarketplaceRequestExporterRequest request);
        #endregion
    }
}