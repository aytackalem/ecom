﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base
{
    public interface IMarketplaceRequestWriter
    {
        #region Methods
        Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestCreateProduct> request);

        Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdateProduct> request);

        Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdatePrice> request);

        Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdateStock> request);
        #endregion
    }
}
