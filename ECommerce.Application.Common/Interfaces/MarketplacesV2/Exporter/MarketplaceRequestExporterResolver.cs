﻿namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter
{
    public delegate Base.IMarketplaceRequestExporter MarketplaceRequestExporterResolver(string requestType);
}
