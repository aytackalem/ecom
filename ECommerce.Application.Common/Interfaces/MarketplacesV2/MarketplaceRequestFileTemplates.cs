﻿namespace ECommerce.Application.Common.Interfaces.MarketplacesV2
{
    public static class MarketplaceRequestFileTemplates
    {
        #region Properties
        #region Create Product
        public static string CreateProductDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\CreateProduct";

        public static string CreateProductTemplate => @"C:\Queues\{0}\{1}\{2}\CreateProduct\{3}_{4}_{5}_{6}.json";

        public static string CreateProductBatchDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\CreateProductBatch";

        public static string CreateProductBatchTemplate => @"C:\Queues\{0}\{1}\{2}\CreateProductBatch\{3}_{4}_{5}_{6}.json";
        #endregion

        #region Update Product
        public static string UpdateProductDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateProduct";

        public static string UpdateProductTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateProduct\{3}_{4}_{5}_{6}.json";

        public static string UpdateProductBatchDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateProductBatch";

        public static string UpdateProductBatchTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateProductBatch\{3}_{4}_{5}_{6}.json";
        #endregion

        #region Update Price
        public static string UpdatePriceDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdatePrice";

        public static string UpdatePriceTemplate => @"C:\Queues\{0}\{1}\{2}\UpdatePrice\{3}_{4}_{5}_{6}.json";

        public static string UpdatePriceBatchDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdatePriceBatch";

        public static string UpdatePriceBatchTemplate => @"C:\Queues\{0}\{1}\{2}\UpdatePriceBatch\{3}_{4}_{5}_{6}.json";
        #endregion

        #region Update Stock
        public static string UpdateStockDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateStock";

        public static string UpdateStockTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateStock\{3}_{4}_{5}_{6}.json";

        public static string UpdateStockBatchDirectoryTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateStockBatch";

        public static string UpdateStockBatchTemplate => @"C:\Queues\{0}\{1}\{2}\UpdateStockBatch\{3}_{4}_{5}_{6}.json"; 
        #endregion
        #endregion
    }
}
