﻿using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base
{
    public interface IMarketplaceRequestProcessor
    {
        #region Methods
        Task ProcessAsync(MarketplaceRequestProcessorRequest request);
        #endregion
    }
}