﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base
{
    public interface IMarketplaceRequestWriter
    {
        #region Methods
        Task WriteAsync(MarketplaceRequestCreateProduct request);

        Task WriteAsync(MarketplaceRequestUpdateProduct request);

        Task WriteAsync(MarketplaceRequestUpdatePrice request);

        Task WriteAsync(MarketplaceRequestUpdateStock request);
        #endregion
    }
}
