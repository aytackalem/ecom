﻿using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base
{
    public interface IMarketplaceRequestReader
    {
        #region Methods
        Task<List<MarketplaceRequestCreateProduct>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestCreateProduct> request);

        Task<List<MarketplaceRequestUpdateProduct>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdateProduct> request);

        Task<List<MarketplaceRequestUpdatePrice>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdatePrice> request);

        Task<List<MarketplaceRequestUpdateStock>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdateStock> request);
        #endregion
    }
}
