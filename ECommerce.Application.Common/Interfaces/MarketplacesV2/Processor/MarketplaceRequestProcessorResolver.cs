﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;

namespace ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor
{
    public delegate IMarketplaceRequestProcessor MarketplaceRequestProcessorResolver(string requestType, bool trackable);
}
