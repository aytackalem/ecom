﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public abstract class TokenMarketplaceBase<TConfiguration> : MarketplaceBase<TConfiguration> where TConfiguration : new()
    {
        #region Fields
        private readonly String _cacheKey = Guid.NewGuid().ToString();

        private readonly ICacheHandler _cacheHandler;

        private readonly int _tokenExpiryMinutes;
        #endregion

        #region Constructors
        public TokenMarketplaceBase(IServiceProvider serviceProvider, ICacheHandler cacheHandler, int tokenExpiryMinutes) : base(serviceProvider)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._tokenExpiryMinutes = tokenExpiryMinutes;
            #endregion
        }
        #endregion

        #region Helper Methods
        protected DataResponse<String> GetAccessToken()
        {
            if (this._configuration == null)
                return new DataResponse<string> { Success = false };

            return this._cacheHandler.ResponseHandle<DataResponse<String>>(() =>
            {
                String accessToken = null;
                ExceptionHandler.Handle(() =>
                {
                    accessToken = this.GetAccessTokenConcreate();
                }, (exception) =>
                {
                    //TO DO: Log
                });

                return new DataResponse<String>
                {
                    Data = accessToken,
                    Success = !String.IsNullOrEmpty(accessToken)
                };
            }, this._cacheKey, this._tokenExpiryMinutes);
        }

        protected abstract String GetAccessTokenConcreate();
        #endregion
    }
}