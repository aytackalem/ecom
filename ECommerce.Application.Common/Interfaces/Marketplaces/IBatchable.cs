﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IBatchable
    {
        #region Methods
        DataResponse<bool> StockCheck(string batchId);

        DataResponse<bool> PriceCheck(string batchId);

        DataResponse<bool> CreateCheck(string batchId);

        DataResponse<bool> UpdateCheck(string batchId);
        #endregion
    }
}