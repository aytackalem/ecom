﻿namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IUsernamePasswordConfiguration
    {
        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization { get; }
        #endregion
    }
}
