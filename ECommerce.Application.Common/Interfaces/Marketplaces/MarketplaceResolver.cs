﻿namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public delegate IMarketplace MarketplaceResolver(string marketplaceId);
}