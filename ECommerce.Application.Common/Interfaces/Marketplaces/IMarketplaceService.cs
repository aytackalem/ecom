﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IMarketplaceService
    {
        #region Methods
        #region Product Methods


        DataResponse<List<MarketplacePriceStockResponse>> UpdateStock(ProductInformationMarketplace productInformationMarketplace, string marketplaceId = null);

        DataResponse<List<MarketplacePriceStockResponse>> UpdatePrice(ProductInformationMarketplace productInformationMarketplace, string marketplaceId = null);

        DataResponse<List<ProductItem>> GetProducts(string marketplaceId = null);

        DataResponse<List<MarketplaceProductStatus>> GetProductsStatus(string marketplaceId = null);

        DataResponse<bool> StockCheck(string batchId, string marketplaceId = null);

        DataResponse<bool> PriceCheck(string batchId, string marketplaceId = null);

        DataResponse<bool> ProductUpsertCheck(string batchId, string marketplaceId = null);
        #endregion

        #region Category Methods
        Response GetCategories(string marketplaceId = null);
        #endregion

        #region Order Methods
        DataResponse<OrderResponse> GetDeliveredOrders(string marketplaceId = null);

        DataResponse<OrderResponse> GetCancelledOrders(string marketplaceId = null);

        DataResponse<OrderResponse> GetReturnedOrders(string marketplaceId = null);

        /// <summary>
        /// Pazaryeri siparişlerini herhangi bir işlem yapmadan raw şeklinde dönen fonksiyon.
        /// </summary>
        /// <param name="marketplaceId">Pazaryeri id bilgisi. *Opsiyonel</param>
        /// <returns></returns>
        void RawOrders(string marketplaceId = null);

        void ProcessRawOrders(string marketplaceId = null);

        void ProcessOrderTypeUpdates(string marketplaceId = null);

        void ProcessMutualBarcodes(string marketplaceId = null);

        DataResponse<OrderResponse> GetOrders(DateTime? startDate = null, DateTime? endDate = null, string marketplaceId = null, string marketplaceOrderNumber = null);

        Response PostInvoice(string shipmentPackageId, string invoiceLink, string marketplaceId);

        DataResponse<CheckPreparingResponse> CheckPreparingOrder(OrderRequest request);

        Response OrderTypeUpdate(OrderPickingRequest request);

        DataResponse<List<KeyValue<string, string>>> GetChangableCargoCompanies(string marketplaceId, string packageNumber);

        Response ChangeCargoCompany(string marketplaceId, string marketplaceOrderNumber, string packageNumber, string cargoCompanyId);

        Response Unpack(string marketplaceId, string marketplaceOrderNumber, string packageNumber);
        #endregion

        #region Brand Methods
        DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValue(string q, string marketplaceId);
        #endregion

        List<MarketplaceCompany> GetMarketplaceCompanies();

        List<MarketplaceConfiguration> GetMarketplaceConfigurations(string marketplaceId);

        MarketplaceConfiguration GetMarketplaceConfiguration(string marketplaceId, string key);
        #endregion
    }
}