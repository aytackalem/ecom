﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using System;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IMutualBarcodable
    {
        #region Methods
        DataResponse<String> GetMutualBarcode(MutualBarcodeRequest mutualBarcodeRequest);
        #endregion
    }
}