﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IPackable
    {
        #region Methods
        Response Unpack(string packageNumber);
        #endregion
    }
}