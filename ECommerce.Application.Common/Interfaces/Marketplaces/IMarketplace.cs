﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface IMarketplace
    {
        #region Methods
        #region Product Methods
        DataResponse<string> CreateProduct(ProductRequest productRequest);

        DataResponse<string> UpdateStock(StockRequest stockRequest);

        DataResponse<string> UpdatePrice(StockRequest stockRequest);

        DataResponse<List<ProductItem>> GetProducts();

        DataResponse<List<MarketplaceProductStatus>> GetProductsStatus();
        #endregion

        #region Category Methods
        public DataResponse<List<CategoryResponse>> GetCategories(Dictionary<string, string> configurations);
        #endregion

        #region Order Methods
        /// <summary>
        /// Pazaryeri siparişlerini herhangi bir işlem yapmadan key-value şeklinde dönen fonksiyon.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DataResponse<OrderResponse> GetRawOrders(RawOrderRequest request);

        DataResponse<OrderTypeUpdateResponse> OrderTypeUpdate(OrderTypeUpdateRequest orderTypeUpdateRequest);

        DataResponse<OrderResponse> GetDeliveredOrders(OrderRequest request);

        DataResponse<OrderResponse> GetCancelledOrders(OrderRequest request);

        DataResponse<OrderResponse> GetReturnedOrders(OrderRequest orderRequest);

        DataResponse<OrderResponse> GetOrders(OrderRequest request);

        Response PostInvoice(string shipmentPackageId, string invoiceLink);

        DataResponse<CheckPreparingResponse> CheckPreparingOrder(OrderRequest request);

        Response OrderTypeUpdate(OrderPickingRequest request);
        #endregion

        #region Brand Methods
        DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValue(string q, Dictionary<string, string> configurations);
        #endregion

        #region Helper Methods
        void BindConfiguration(List<MarketplaceConfiguration> marketplaceConfigurations);
        #endregion
        #endregion
    }
}