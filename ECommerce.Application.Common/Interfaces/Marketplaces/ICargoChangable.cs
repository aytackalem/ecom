﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public interface ICargoChangable
    {
        #region Methods
        DataResponse<List<KeyValue<string, string>>> GetChangableCargoCompanies(string packageNumber);

        DataResponse<ChangeCargoCompanyResponse> ChangeCargoCompany(string packageNumber, string cargoCompanyId);
        #endregion
    }
}