﻿using System;
using System.Text;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public abstract class UsernamePasswordConfigurationBase : IUsernamePasswordConfiguration
    {
        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));
        #endregion
    }
}
