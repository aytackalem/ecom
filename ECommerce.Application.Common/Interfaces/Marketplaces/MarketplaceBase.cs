﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Marketplaces
{
    public abstract class MarketplaceBase<TConfiguration> : IMarketplace where TConfiguration : new()
    {
        #region Fields
        protected TConfiguration _configuration;

        protected readonly IServiceProvider _serviceProvider;
        #endregion

        #region Constructors
        public MarketplaceBase(IServiceProvider serviceProvider)
        {
            #region Fields
            this._serviceProvider = serviceProvider;
            #endregion
        }
        #endregion

        #region Properties
        public abstract string MarketplaceId { get; }
        #endregion

        #region Methods
        #region Product Methods
        public DataResponse<string> CreateProduct(ProductRequest productRequest)
        {
            if (this._configuration == null)
                return new DataResponse<string> { Success = false };

            return this.CreateProductConcreate(productRequest);
        }

        protected abstract DataResponse<string> CreateProductConcreate(ProductRequest productRequest);

        public DataResponse<string> UpdatePrice(StockRequest stockRequest)
        {
            if (this._configuration == null)
                return new DataResponse<string> { Success = false };

            return this.UpdatePriceConcreate(stockRequest);
        }

        protected abstract DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest);

        public DataResponse<string> UpdateStock(StockRequest stockRequest)
        {
            if (this._configuration == null)
                return new DataResponse<string> { Success = false };

            return this.UpdateStockConcreate(stockRequest);
        }

        protected abstract DataResponse<string> UpdateStockConcreate(StockRequest stockRequest);

        public DataResponse<List<ProductItem>> GetProducts()
        {
            if (this._configuration == null)
                return new DataResponse<List<ProductItem>> { Success = false };

            return this.GetProductsConcreate();
        }

        protected abstract DataResponse<List<ProductItem>> GetProductsConcreate();

        public DataResponse<List<MarketplaceProductStatus>> GetProductsStatus()
        {
            if (this._configuration == null)
                return new DataResponse<List<MarketplaceProductStatus>> { Success = false };

            return this.GetProductsStatusConcreate();
        }

        protected abstract DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate();
        #endregion

        #region Order Methods
        public DataResponse<OrderResponse> GetRawOrders(RawOrderRequest request)
        {
            if (this._configuration == null)
                return new DataResponse<OrderResponse> { Success = false };

            return this.GetRawOrdersConcreate(request);
        }

        protected abstract DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request);

        public DataResponse<OrderTypeUpdateResponse> OrderTypeUpdate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return this.OrderTypeUpdateConcreate(orderTypeUpdateRequest);
        }

        protected abstract DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest);

        public DataResponse<OrderResponse> GetDeliveredOrders(OrderRequest request)
        {
            return this.GetDeliveredOrdersConcreate(request);
        }

        protected abstract DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request);

        public DataResponse<OrderResponse> GetOrders(OrderRequest request)
        {
            return this.GetOrdersConcreate(request);
        }

        protected abstract DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request);


        public Response PostInvoice(string shipmentPackageId, string invoiceLink)
        {
            return this.PostInvoiceConcreate(shipmentPackageId, invoiceLink);
        }
        protected abstract Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink);


        public DataResponse<OrderResponse> GetReturnedOrders(OrderRequest orderRequest)
        {
            return this.GetReturnedOrdersConcreate(orderRequest);
        }

        protected abstract DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest);

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            return this.OrderTypeUpdateConcreate(request);
        }

        protected abstract Response OrderTypeUpdateConcreate(OrderPickingRequest request);

        public DataResponse<CheckPreparingResponse> CheckPreparingOrder(OrderRequest request)
        {
            return this.CheckPreparingOrderConcreate(request);
        }

        protected abstract DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request);

        public DataResponse<OrderResponse> GetCancelledOrders(OrderRequest request)
        {
            return this.GetCancelledOrdersConcreate(request);
        }

        protected abstract DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request);
        #endregion

        #region Category Methods
        public DataResponse<List<CategoryResponse>> GetCategories(Dictionary<string, string> configurations)
        {
            return this.GetCategoriesConcreate(configurations);
        }

        protected abstract DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations);
        #endregion

        #region Brand Methods
        public DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValue(string q, Dictionary<string, string> configurations)
        {
            return this.BrandReadAsKeyValueConcreate(q, configurations);
        }

        protected abstract DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations);
        #endregion
        #endregion

        #region Helper Methods
        public void BindConfiguration(List<MarketplaceConfiguration> marketplaceConfigurations)
        {
            if (this._configuration == null)
            {
                var configuration = new TConfiguration();
                this.BindConfigurationConcreate(configuration, marketplaceConfigurations);
                this._configuration = configuration;
            }
        }

        protected abstract void BindConfigurationConcreate(TConfiguration configuration, List<MarketplaceConfiguration> marketplaceConfigurations);
        #endregion
    }
}