﻿namespace ECommerce.Application.Common.Interfaces.Shipment
{
    public delegate IShipmentProvider ShipmentProviderServiceResolver(string shipmentProviderId);
}
