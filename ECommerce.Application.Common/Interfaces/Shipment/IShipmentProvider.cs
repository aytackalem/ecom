﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Shipment
{
    public interface IShipmentProvider
    {
        #region Methods
        DataResponse<ShipmentInfo> Post(List<ShipmentCompanyConfiguration> configurations, Order order, bool payor, int[] weights, int piece);

        DataResponse<ShipmentTracking> Track(List<ShipmentCompanyConfiguration> configurations, string trackingCode);
        #endregion
    }
}
