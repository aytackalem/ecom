﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Shipment
{
    public interface IShipmentProviderService
    {
        #region Methods
        DataResponse<ShipmentInfo> Post(int orderId, bool payor, int[] weights, int piece);

        DataResponse<ShipmentTracking> Track(int orderId, string trackingCode);
        #endregion
    }
}
