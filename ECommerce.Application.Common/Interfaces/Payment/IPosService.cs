﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Payment
{
    public interface IPosService : IPosProvider
    {
        #region Methods
        DataResponse<IPosProvider> Find(string creditCardNumber);
        #endregion
    }
}
