﻿using ECommerce.Application.Common.Interfaces.Payment;

namespace ECommerce.Application.Common.Interfaces.Communication
{
    public delegate IPosProvider PosProviderResolver(string posId);
}
