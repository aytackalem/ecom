﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Payment
{
    public interface IPosProvider
    {
        #region Methods
        DataResponse<Payment3DResponse> Pay3d(CreditCardPayment creditCardPayment);

        DataResponse<PaymentResponse> Check3d();
        #endregion
    }
}
