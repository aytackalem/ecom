﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplaceCatalog
{
    public interface IMarketplaceCategoryService
    {
        #region Methods
        Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId);
        #endregion
    }
}
