﻿using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantValueService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.MarketplaceCatalog
{
    public interface IMarketplaceVariantValueService
    {
        #region Methods
        Task<DataResponse<bool>> GetExsistName(SearchByNameParameters parameters);
        #endregion
    }
}
