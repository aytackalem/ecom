﻿namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface ISettingService
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        string ConnectionString { get; }

        bool IllegalSale { get; }

        /// <summary>
        /// 
        /// </summary>
        bool BarcodeWritable { get; }

        bool Waybill { get; }

        string ImagePath { get; }

        string ProductImagePath { get; }

        string ProductImageThumbPath { get; }

        string CategoryImagePath { get; }

        string ShowcaseImagePath { get; }

        string CompanyImagePath { get; }

        string ImageUrl { get; }

        string ProductImageUrl { get; }

        string ProductImageThumbUrl { get; }

        string CategoryImageUrl { get; }

        string ShowcaseImageUrl { get; }

        string CompanyImageUrl { get; }

        string DefaultLanguageId { get; }

        string DefaultCurrencyId { get; }

        int DefaultCountryId { get; }

        bool IncludeMarketplace { get; }

        bool IncludeECommerce { get; }

        bool IncludeWMS { get; }

        bool IncludeExport { get; }

        bool IncludeAccounting { get; }

        bool CreateNonexistProduct { get; }

        bool StockControl { get; }

        string UUId { get; }

        string WebUrl { get; }

        string Theme { get; }

        bool OrderDetailMultiselect { get; }

        /// <summary>
        /// Kiracı adını temsil eder.
        /// </summary>
        string Tenant { get; }
        #endregion
    }
}