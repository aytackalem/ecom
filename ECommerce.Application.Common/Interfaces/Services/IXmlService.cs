﻿using ECommerce.Application.Common.DataTransferObjects.XmlService;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface IXmlService
    {
        #region Methods
        XmlSource GetXmlSource(string requestUri, int productSourceDomainId, string productSourceId);

        DataResponse<List<ParsedXmlItem>> Parse(string text, XmlSource xmlSource);

        Response Write<T>(string path, List<T> values);
        #endregion
    }
}
