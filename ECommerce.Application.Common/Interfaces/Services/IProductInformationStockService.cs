﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface IProductInformationStockService
    {
        #region Methods
        /// <summary>
        /// İlgili ürün bilgisine ait kirli stok parametresini günceller. Bu sayede stok bilgisi güncellenecek kayıtlar arasına girer.
        /// </summary>
        /// <param name="id">Ürün bilgisi kimlik no.</param>
        /// <returns></returns>
        Response MakeDirtyStock(int id);

        /// <summary>
        /// İlgili ürün bilgisine ait stok değerini arttıran metoddur.
        /// </summary>
        /// <param name="id">Ürün bilgisi kimlik no.</param>
        /// <param name="quantity">Arttırılacak stok adedi.</param>
        /// <returns></returns>
        Response IncreaseStock(int id, int quantity);

        /// <summary>
        /// İlgili ürün bilgisine ait stok değerini azaltan metoddur.
        /// </summary>
        /// <param name="id">Ürün bilgisi kimlik no.</param>
        /// <param name="quantity">Azaltılacak stok adedi.</param>
        /// <returns></returns>
        Response DecreaseStock(int id, int quantity);
        #endregion
    }
}
