﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects.Widgets;
using ECommerce.Application.Common.Parameters.Widgets;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface IWidgetService
    {
        #region Methods
        DataResponse<List<SaleSummary>> GetFinancialSummary();

        DataResponse<Widget> Read(WitgetRequest request, string userRole);
        #endregion
    }
}
