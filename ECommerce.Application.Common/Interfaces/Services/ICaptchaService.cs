﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface ICaptchaService
    {
        #region Methods
        Response Verify<T>(CaptchaRequest<T> captchaRequest); 
        #endregion
    }
}
