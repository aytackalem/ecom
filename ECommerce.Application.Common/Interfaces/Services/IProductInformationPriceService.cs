﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface IProductInformationPriceService
    {
        #region Methods
        /// <summary>
        /// İlgili ürün bilgisine ait kirli stok parametresini günceller. Bu sayede stok bilgisi güncellenecek kayıtlar arasına girer.
        /// </summary>
        /// <param name="id">Ürün bilgisi kimlik no.</param>
        /// <returns></returns>
        Response MakeDirtyPrice(int id);
        #endregion
    }
}
