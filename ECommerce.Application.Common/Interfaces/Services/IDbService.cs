﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Services
{
    public interface IDbService
    {
        #region Methods
        Task<DataResponse<bool>> ExistAsync(string name);

        Task<DataResponse<List<string>>> GetNamesAsync();
        #endregion
    }
}
