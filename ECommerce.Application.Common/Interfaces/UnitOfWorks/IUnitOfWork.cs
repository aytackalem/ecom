﻿using ECommerce.Application.Common.Interfaces.Repositories;

namespace ECommerce.Application.Common.Interfaces.UnitOfWorks
{
    public interface IUnitOfWork
    {
        #region Properties
        IProductGenericPropertyRepository ProductGenericPropertyRepository { get; }

        IWholesaleOrderRepository WholesaleOrderRepository { get; }

        IWholesaleOrderTypeRepository WholesaleOrderTypeRepository { get; }

        IProductCategorizationRepository ProductCategorizationRepository { get; }

        IOrderPickingRepository OrderPickingRepository { get; }

        IStocktakingRepository StocktakingRepository { get; }

        IOrderReserveRepository OrderReserveRepository { get; }

        IStocktakingTypeRepository StocktakingTypeRepository { get; }

        IECommerceCategoryRepository ECommerceCategoryRepository { get; }

        IECommerceVariantRepository ECommerceVariantRepository { get; }

        IECommerceBrandRepository ECommerceBrandRepository { get; }

        IECommerceVariantValueRepository ECommerceVariantValueRepository { get; }

        IMarketplaceConfigurationRepository MarketplaceConfigurationRepository { get; }

        IPaymentLogRepository PaymentLogRepository { get; }

        ITenantPaymentRepository TenantPaymentRepository { get; }

        IFeedRepository FeedRepository { get; }

        IFeedTemplateRepository FeedTemplateRepository { get; }

        IProductMarketplaceRepository ProductMarketplaceRepository { get; }

        IMarketplaceVariantValueMappingRepository MarketplaceVariantValueMappingRepository { get; }

        IOrderViewingRepository OrderViewingRepository { get; }

        IProductInformationPriceRepository ProductInformationPriceRepository { get; }

        IExpenseRepository ExpenseRepository { get; }

        IExpenseSourceRepository ExpenseSourceRepository { get; }

        IExchangeRateRepository ExchangeRateRepository { get; }

        IOrderPackingRepository OrderPackingRepository { get; }

        ICompanyConfigurationRepository CompanyConfigurationRepository { get; }

        IDomainRepository DomainRepository { get; }

        IApplicationRepository ApplicationRepository { get; }

        IBankRepository BankRepository { get; }

        IBinRepository BinRepository { get; }

        IBrandDiscountSettingRepository BrandDiscountSettingRepository { get; }

        IBrandRepository BrandRepository { get; }

        ISupplierRepository SupplierRepository { get; }

        ICategoryRepository CategoryRepository { get; }

        ILabelRepository LabelRepository { get; }

        IVariantRepository VariantRepository { get; }

        IVariantValueRepository VariantValueRepository { get; }

        ILanguageRepository LanguageRepository { get; }

        IProductRepository ProductRepository { get; }

        IProductLabelRepository ProductLabelRepository { get; }

        ICategoryProductRepository CategoryProductRepository { get; }

        ICategoryTranslationBreadcrumbRepository CategoryTranslationBreadcrumbRepository { get; }

        ICategoryTranslationRepository CategoryTranslationRepository { get; }

        ICurrencyRepository CurrencyRepository { get; }

        IProductInformationCommentRepository ProductInformationCommentRepository { get; }

        IProductInformationRepository ProductInformationRepository { get; }

        IProductInformationContentTranslationRepository ProductInformationContentTranslationRepository { get; }

        IProductInformationCombineRepository ProductInformationCombineRepository { get; }

        IProductInformationVariantRepository ProductInformationVariantRepository { get; }

        IProductInformationSubscriptionRepository ProductInformationSubscriptionRepository { get; }

        IDiscountCouponRepository DiscountCouponRepository { get; }

        ICountryRepository CountryRepository { get; }

        IGetXPayYSettingRepository GetXPayYSettingRepository { get; }

        IEntireDiscountRepository EntireDiscountRepository { get; }

        ICategoryDiscountSettingRepository CategoryDiscountSettingRepository { get; }

        IPosRepository PosRepository { get; }

        IPosConfigurationRepository PosConfigurationRepository { get; }

        IPaymentTypeDomainRepository PaymentTypeDomainRepository { get; }

        IPaymentTypeRepository PaymentTypeRepository { get; }

        IPaymentRepository PaymentRepository { get; }

        IShoppingCartDiscountedProductRepository ShoppingCartDiscountedProductRepository { get; }

        IMarketplaceRepository MarketPlaceRepository { get; }

        IMarketPlaceCompanyRepository MarketplaceCompanyRepository { get; }

        IProductInformationMarketplaceRepository ProductInformationMarketplaceRepository { get; }

        IProductInformationMarketplaceVariantValueRepository ProductInformationMarketplaceVariantValueRepository { get; }

        ISmsProviderCompanyRepository SmsProviderCompanyRepository { get; }

        ISmsProviderConfigurationRepository SmsProviderConfigurationRepository { get; }

        ISmsTemplateRepository SmsTemplateRepository { get; }

        IEmailProviderCompanyRepository EmailProviderCompanyRepository { get; }

        IEmailProviderConfigurationRepository EmailProviderConfigurationRepository { get; }

        IAccountingCompanyConfigurationRepository AccountingCompanyConfigurationRepository { get; }

        IAccountingRepository AccountingRepository { get; }

        IAccountingCompanyRepository AccountingCompanyRepository { get; }

        IShipmentCompanyCompanyRepository ShipmentCompanyCompanyRepository { get; }

        IShipmentCompanyRepository ShipmentCompanyRepository { get; }

        IShipmentCompanyConfigurationRepository ShipmentCompanyConfigurationRepository { get; }

        IProductPropertyRepository ProductPropertyRepository { get; }

        IProductPropertyTranslationRepository ProductPropertyTranslationRepository { get; }

        IProductInformationTranslationRepository ProductInformationTranslationRepository { get; }

        IProductInformationTranslationBreadcrumbRepository ProductInformationTranslationBreadcrumbRepository { get; }

        IProductPropertyValueRepository ProductPropertyValueRepository { get; }

        IProductPropertyValueTranslationRepository ProductPropertyValueTranslationRepository { get; }

        IMoneyPointSettingRepository MoneyPointSettingRepository { get; }

        IMoneyPointRepository MoneyPointRepository { get; }

        IInformationRepository InformationRepository { get; }

        IFrequentlyAskedQuestionRepository FrequentlyAskedQuestionRepository { get; }

        IContactRepository ContactRepository { get; }

        IConfigurationRepository ConfigurationRepository { get; }

        IGoogleConfigrationRepository GoogleConfigrationRepository { get; }

        IFacebookConfigrationRepository FacebookConfigrationRepository { get; }

        IContentRepository ContentRepository { get; }

        IInformationTranslationRepository InformationTranslationRepository { get; }

        IProductInformationPhotoRepository ProductInformationPhotoRepository { get; }

        IContentTranslationRepository ContentTranslationRepository { get; }

        ICityRepository CityRepository { get; }

        INeighborhoodRepository NeighborhoodRepository { get; }

        INewBulletinRepository NewBulletinRepository { get; }

        INewBulletinInformationRepository NewBulletinInformationRepository { get; }

        IDistrictRepository DistrictRepository { get; }

        IPropertyRepository PropertyRepository { get; }

        IShowcaseRepository ShowcaseRepository { get; }

        ICustomerUserRepository CustomerUserRepository { get; }

        ITwoFactorCustomerUserRepository TwoFactorCustomerUserRepository { get; }

        IOrderRepository OrderRepository { get; }

        IOrderDetailRepository OrderDetailRepository { get; }

        IOrderBillingRepository OrderBillingRepository { get; }

        IOrderTypeRepository OrderTypeRepository { get; }

        IOrderTechnicInformationRepository OrderTechnicInformationRepository { get; }

        IOrderShipmentRepository OrderShipmentRepository { get; }

        IOrderInvoiceInformationRepository OrderInvoiceInformationRepository { get; }

        INebimOrderRepository NebimOrderRepository { get; }

        ICustomerRepository CustomerRepository { get; }

        ICustomerRoleRepository CustomerRoleRepository { get; }

        ICustomerAddressRepository CustomerAddressRepository { get; }

        ICustomerInvoiceInformationRepository CustomerInvoiceInformationRepository { get; }

        ICustomerContactRepository CustomerContactRepository { get; }

        IMarketplaceBrandMappingRepository MarketplaceBrandMappingRepository { get; }

        IMarketplaceCategoryMappingRepository MarketplaceCategoryMappingRepository { get; }

        //IMarketplaceCategoriesMarketplaceVariantRepository MarketplaceCategoriesMarketplaceVariantRepository { get; }

        //IMarketplaceCategoriesMarketplaceVariantValueRepository MarketplaceCategoriesMarketplaceVariantValueRepository { get; }

        IUpsAreaRepository UpsAreaRepository { get; }

        IUpsCityRepository UpsCityRepository { get; }

        ICompanyRepository CompanyRepository { get; }

        IOrderSourceRepository OrderSourceRepository { get; }

        ITenantRepository TenantRepository { get; }

        IProductSourceDomainRepository ProductSourceDomainRepository { get; }

        IProductInformationMarketplaceBulkPriceRepository ProductInformationMarketplaceBulkPriceRepository { get; }

        IReceiptRepository ReceiptRepository { get; }

        INebimMarketplaceCreditCardCodeRepository NebimMarketplaceCreditCardCodeRepository { get; }

        INebimMarketplaceSalesPersonnelCodeRepository NebimMarketplaceSalesPersonnelCodeRepository { get; }
        #endregion
    }
}
