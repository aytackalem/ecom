﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IDomainFinder
    {
        #region Methods
        int FindId();

        void Set(int id);
        #endregion
    }
}
