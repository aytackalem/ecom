﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IRandomHelper
    {
        #region Methods
        string GenerateNumbers(int min, int max);
        #endregion
    }
}
