﻿using ECommerce.Application.Common.Parameters.FtpHelper;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IFtpHelper
    {
        #region Methods
        public List<string> GetFolders(FtpRequest ftpRequest);

        public List<string> GetFiles(FtpRequest ftpRequest);

        public byte[] Download(FtpRequest ftpRequest);
        #endregion
    }
}
