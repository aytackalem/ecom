﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IHttpHelper
    {
        #region Methods
        TResponse PostForm<TResponse>(string request, string url, string authType, string token, string accept) where TResponse : class;

        TResponse Post<TRequest, TResponse>(TRequest request, string url, string authType, string token, bool? log, string logFileName = null) where TRequest : class where TResponse : class;

        TResponse Post<TRequest, TResponse>(TRequest request, string url, string authType, string token, Dictionary<string, string> customHeaders) where TRequest : class where TResponse : class;

        TResponse Get<TResponse>(string url, string authType, string token) where TResponse : class;

        TResponse Get<TResponse>(string url, string authType, string token, Dictionary<string, string> customHeaders) where TResponse : class;

        TResponse Put<TRequest, TResponse>(TRequest request, string url, string authType, string token) where TRequest : class where TResponse : class;

        TResponse Put<TRequest, TResponse>(TRequest request, string url, string authType, string token, Dictionary<string, string> customHeaders) where TRequest : class where TResponse : class;

        bool Put<TRequest>(TRequest request, string url, string authType, string token) where TRequest : class;

        bool Post(string url, string authType, string token);

        TResponse Post<TResponse>(string url, List<KeyValuePair<string, string>> data) where TResponse : class;

        TResponse Post<TResponse>(string url, string authType, string token) where TResponse : class;

        void Post(string url, string body, string authType, string token);
        #endregion
    }
}
