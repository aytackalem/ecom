﻿using ECommerce.Application.Common.Parameters.FtpHelper;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public class FtpHelper : IFtpHelper
    {
        #region Methods
        public List<string> GetFolders(FtpRequest ftpRequest)
        {
            var request = WebRequest.Create(ftpRequest.Path);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(ftpRequest.Username, ftpRequest.Password);

            var files = new List<string>();

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        while (!reader.EndOfStream)
                        {
                            var file = reader.ReadLine();
                            file = file.Substring(file.LastIndexOf('/') + 1);
                            if (!file.StartsWith("."))
                                files.Add(file);
                        }
                    }
                }
            }

            return files;
        }

        public List<string> GetFiles(FtpRequest ftpRequest)
        {
            var request = WebRequest.Create(ftpRequest.Path);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(ftpRequest.Username, ftpRequest.Password);

            var files = new List<string>();

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        while (!reader.EndOfStream)
                        {
                            var file = reader.ReadLine();
                            file = file.Substring(file.LastIndexOf('/') + 1);
                            if (!file.StartsWith("."))
                                files.Add(file);
                        }
                    }
                }
            }

            return files;
        }

        public byte[] Download(FtpRequest ftpRequest)
        {
            var request = new WebClient();
            request.Credentials = new NetworkCredential(ftpRequest.Username, ftpRequest.Password);
            return request.DownloadData(ftpRequest.Path);
        } 
        #endregion
    }
}
