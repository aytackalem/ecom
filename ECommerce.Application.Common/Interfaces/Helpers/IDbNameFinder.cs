﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IDbNameFinder
    {
        #region Properties
        void Set(string dbName);

        string FindName();
        #endregion
    }
}