﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ISetableDomainFinder : IDomainFinder
    {
        #region Methods
        void SetId(int id);
        #endregion
    }
}
