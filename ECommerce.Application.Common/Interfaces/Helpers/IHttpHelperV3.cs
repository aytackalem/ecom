﻿using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IHttpHelperV3
    {
        #region Methods
        Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3Request request);

        Task<HttpHelperV3Response<TContent>> SendAsync<TRequest, TContent>(HttpHelperV3Request<TRequest> request);

        Task<HttpHelperV3Response<TOkContent, TBadRequestContent>> SendAsync<TRequest, TOkContent, TBadRequestContent>(HttpHelperV3Request<TRequest> request);

        Task<HttpHelperV3Response> SendAsync<TRequest>(HttpHelperV3Request<TRequest> request);

        Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3JsonFileRequest request);

        Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3FormUrlEncodedRequest request) where TContent : class;
        #endregion
    }
}
