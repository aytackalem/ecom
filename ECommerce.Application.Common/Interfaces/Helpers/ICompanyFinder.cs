﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ICompanyFinder
    {
        #region Methods
        int FindId();

        void Set(int id);
        #endregion
    }
}
