﻿using Microsoft.AspNetCore.Http;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IImageHelper
    {
        #region Methods
        bool Save(string base64, string fileName);

        string Resize(string base64);

        string Resize(IFormFile formFile);

        //bool SaveCompressed(string base64, int imageQuality, string fileName);
        #endregion
    }
}
