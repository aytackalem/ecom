﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Helpers
{
    public interface ICookieHelper
    {
        #region Methods
        void Write(string name, List<KeyValue<string, string>> keyValues, DateTime? expires = null);

        List<KeyValue<string, string>> Read(string name);

        void Delete(string name);

        bool Exist(string name);
        #endregion
    }
}
