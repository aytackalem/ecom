﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ILocalizationHelper
    {
        #region Methods
        Currency GetCurrency();

        Language GetLanguage();
        #endregion
    }
}
