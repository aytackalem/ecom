﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ISetableTenantFinder: ITenantFinder
    {
        #region Methods
        void SetId(int id);
        #endregion
    }
}
