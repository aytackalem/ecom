﻿using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IHttpHelperV2
    {
        #region Methods
        Task<HttpResultResponse<string>> SendAsync(string requestUri, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null, string agent = null, bool? log = null, string logFileName = null);

        Task<HttpResultResponse<string>> SendJsonAsync<TRequestObject>(string requestUri, TRequestObject requestObject, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null);

        Task<HttpResultResponse<TContentObject>> SendJsonAsync<TRequestObject, TContentObject>(string requestUri, TRequestObject requestObject, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null);

        Task<HttpResultResponse<TContentObject>> SendJsonAsync<TContentObject>(string requestUri, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null);

        Task<HttpResultResponse<TContentObject>> SendJsonFileAsync<TContentObject>(string requestUri, string requestObject, string fileName, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null);

        Task<HttpResultResponse<TContentObject>> SendFormUrlEncodedAsync<TContentObject>(string requestUri, List<KeyValuePair<string, string>> formItems, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null);
        #endregion
    }
}
