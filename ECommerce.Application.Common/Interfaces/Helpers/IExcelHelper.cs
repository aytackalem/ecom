﻿using ECommerce.Application.Common.DataTransferObjects;
using System.IO;

namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface IExcelHelper
    {
        #region Methods
        MemoryStream Write(Excel excel);

        Excel Read(Stream stream);
        #endregion
    }
}
