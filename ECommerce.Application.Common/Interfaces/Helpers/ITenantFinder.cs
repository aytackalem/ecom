﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ITenantFinder
    {
        #region Methods
        int FindId();
        #endregion
    }
}
