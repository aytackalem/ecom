﻿namespace ECommerce.Application.Common.Interfaces.Helpers
{
    public interface ISetableCompanyFinder : ICompanyFinder
    {
        #region Methods
        void SetId(int id);
        #endregion
    }
}
