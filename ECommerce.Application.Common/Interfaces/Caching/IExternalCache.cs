﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Caching
{
    public interface IExternalCache
    {
        #region Methods
        List<string> GetAllKeys();

        bool ContainsKey(string key);

        void SetEntry(string key, string value);

        string GetValue(string key);

        bool RemoveEntry(params string[] args);
        #endregion
    }
}
