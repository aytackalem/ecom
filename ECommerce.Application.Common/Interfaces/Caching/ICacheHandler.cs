﻿using ECommerce.Application.Common.Wrappers.Base;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.Interfaces.Caching
{
    public interface ICacheHandler
    {
        #region Methods
        TResponse ResponseHandle<TResponse>(Func<TResponse> func, string cacheKey, int minutes = 10) where TResponse : ResponseBase, new();

        /// <summary>
        /// Data katmanından dönen entityleri cache leyen metod. Entity null değil ise cache leme işlemi yapılır. 
        /// </summary>
        /// <typeparam name="TEntity">Cache lenmesi istenen entity.</typeparam>
        /// <param name="func"></param>
        /// <param name="cacheKey">Cache in saklanırken kullanacağı anahtar kelime.</param>
        /// <param name="minutes">Cache lenecek süre.</param>
        /// <returns></returns>
        TEntity Handle<TEntity>(Func<TEntity> func, string cacheKey, int minutes = 10) where TEntity : class;

        /// <summary>
        /// Data katmanından dönen entity list leri cache leyen metod. Entity list null değil ise ve en az bir eleman içeriyor ise cache leme işlemi yapılır. 
        /// </summary>
        /// <typeparam name="TEntity">Cache lenmesi istenen entity.</typeparam>
        /// <param name="func"></param>
        /// <param name="cacheKey">Cache in saklanırken kullanacağı anahtar kelime.</param>
        /// <param name="minutes">Cache lenecek süre.</param>
        /// <returns></returns>
        List<TEntity> Handle<TEntity>(Func<List<TEntity>> func, string cacheKey, int minutes = 10) where TEntity : class;
        #endregion
    }
}
