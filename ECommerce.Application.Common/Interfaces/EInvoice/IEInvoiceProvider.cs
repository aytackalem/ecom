﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Common.Interfaces.EInvoice
{
    public interface IEInvoiceProvider
    {
        #region Methods
        /// <summary>
        /// Sipariş detay bilgilerini kullanarak fatura oluşturan fonksiyon.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<AccountingInfo> CreateInvoice(int orderId);
        #endregion
    }
}
