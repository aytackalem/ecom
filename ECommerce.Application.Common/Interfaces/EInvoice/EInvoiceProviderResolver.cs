﻿namespace ECommerce.Application.Common.Interfaces.EInvoice
{
    public delegate IEInvoiceProvider EInvoiceProviderResolver(string eInvoiceProviderId);
}
