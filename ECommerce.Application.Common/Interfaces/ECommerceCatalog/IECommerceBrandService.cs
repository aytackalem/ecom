﻿using ECommerce.Application.Common.Parameters.MarketplaceCatalog.BrandService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.BrandService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.ECommerceCatalog
{
    public interface IECommerceBrandService
    {
        #region Methods
        Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters);
        #endregion
    }
}
