﻿using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.ECommerceCatalog
{
    public interface IECommerceVariantService
    {
        #region Methods
        Task<DataResponse<GetByCategoryCodesResponse>> GetByCategoryCodesAsync(GetByCategoryCodesParameter parameters);
        #endregion
    }
}
