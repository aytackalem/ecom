﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.ECommerceCatalog
{
    public interface IECommerceCategoryService
    {
        #region Methods
        Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId);
        #endregion
    }
}
