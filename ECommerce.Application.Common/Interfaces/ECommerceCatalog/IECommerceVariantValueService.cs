﻿using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantValueService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantValueService;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.Interfaces.ECommerceCatalog
{
    public interface IECommerceVariantValueService
    {
        #region Methods
        Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters);
        #endregion
    }
}
