﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class DomainSetting
    {
        #region Properties
        /// <summary>
        /// Kiracıya ait görsellere ulaşım için kullanılacak url bilgisi.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Kiracıya ait görsellerin saklanacağı dizin bilgisi.
        /// </summary>
        public string ImagePath { get; set; }
        #endregion
    }
}
