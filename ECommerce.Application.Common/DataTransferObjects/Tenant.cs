﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Tenant
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public TenantSetting TenantSetting { get; set; }

        public List<Domain> Domains { get; set; }
        #endregion
    }
}
