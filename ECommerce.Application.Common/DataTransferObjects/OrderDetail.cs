﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    /// <summary>
    /// Sipariş detayını temsil eden sınıf.
    /// </summary>
    public class OrderDetail
    {
        #region Properties
    
        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

      
        public string CategoryName { get; set; }
        #endregion

        #region Navigation Properties
        #endregion
    }
}
