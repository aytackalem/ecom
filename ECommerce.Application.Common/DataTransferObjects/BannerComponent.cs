﻿using Newtonsoft.Json;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class BannerComponent
    {
        #region Properties
        [JsonProperty("FN")]
        public string FileName { get; set; }

        [JsonProperty("L")]
        public string Link { get; set; }
        #endregion
    }
}
