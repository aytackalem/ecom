﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Showcase
    {
        #region Properties
        [JsonProperty("I")]
        public int Id { get; set; }

        [JsonProperty("CI")]
        public int? CategoryId { get; set; }

        [JsonProperty("BI")]
        public int? BrandId { get; set; }

        [JsonProperty("LI")]
        public string LanguageId { get; set; }

        [JsonProperty("N")]
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        [JsonProperty("RCS")]
        public List<RowComponent> RowComponents { get; set; }
        #endregion
    }
}
