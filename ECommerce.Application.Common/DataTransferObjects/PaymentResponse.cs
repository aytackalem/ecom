﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class PaymentResponse
    {
        public int OrderId { get; set; }

        public int PaymentId { get; set; }

        public bool AllowFrame { get; set; } = true;
    }
}
