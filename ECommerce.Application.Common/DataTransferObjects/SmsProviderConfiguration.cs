﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class SmsProviderConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public int SmsProviderCompanyId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
