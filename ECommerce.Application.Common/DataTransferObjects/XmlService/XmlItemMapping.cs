﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class XmlItemMapping
    {
        #region Constructors
        public XmlItemMapping(string sourceNodeName, string targetPropertyName, string value, XmlItemType xmlItemType)
        {
            #region Properties
            this.SourceNodeName = sourceNodeName;
            this.TargetPropertyName = targetPropertyName;
            this.Value = value;
            this.XmlItemType = xmlItemType;
            this.ChildXmlItemMappings = new();
            this.XmlItemMappingRules = new();
            #endregion
        }

        public XmlItemMapping(string sourceNodeName, string targetPropertyName, XmlItemType xmlItemType)
        {
            #region Properties
            this.SourceNodeName = sourceNodeName;
            this.TargetPropertyName = targetPropertyName;
            this.XmlItemType = xmlItemType;
            this.ChildXmlItemMappings = new();
            this.XmlItemMappingRules = new();
            #endregion
        }

        public XmlItemMapping()
        {
            #region Properties
            this.ChildXmlItemMappings = new();
            this.XmlItemMappingRules = new();
            #endregion
        }
        #endregion

        #region Properties
        public string SourceNodeName { get; set; }

        public string SourceChildNodeName { get; set; }

        public string TargetPropertyName { get; set; }

        public string KeyNodeName { get; set; }

        public string ValueNodeName { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public string Seperator { get; set; }

        public XmlItemType SourceXmlItemType { get; set; }

        public XmlItemType XmlItemType { get; set; }
        #endregion

        #region Navigation Properties
        public List<XmlItemMappingRule> XmlItemMappingRules { get; set; }

        public List<XmlItemMapping> ChildXmlItemMappings { get; set; }
        public bool DefaultValue { get; set; }
        #endregion
    }
}
