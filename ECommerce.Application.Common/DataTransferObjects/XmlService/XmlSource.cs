﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class XmlSource
    {
        #region Properties
        public string RequestUri { get; set; }

        public string UserAgent { get; set; }

        public int ProductSourceDomainId { get; set; }

        public string ProductSourceId { get; set; }
        #endregion

        #region Navigation Properties
        public XmlMapping XmlMapping { get; set; }
        #endregion
    }
}
