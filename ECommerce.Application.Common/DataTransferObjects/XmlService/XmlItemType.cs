﻿namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public enum XmlItemType
    {
        #region Members
        MultiProperty,
        KeyValueProperty,
        Property,
        Attribute,
        Composite
        #endregion
    }
}
