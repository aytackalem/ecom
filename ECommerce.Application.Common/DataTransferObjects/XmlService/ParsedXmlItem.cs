﻿using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class ParsedXmlItem
    {
        #region Constructors
        public ParsedXmlItem()
        {
            #region Properties
            this.XmlItemMappings = new();
            #endregion
        }
        #endregion

        #region Properties
        public int ProductSourceDomainId { get; set; }
        #endregion

        #region Navigation Properties
        public List<XmlItemMapping> XmlItemMappings { get; set; }
        #endregion

        #region Methods
        public string GetXmlItemMappingValue(string targetPropertyName)
        {
            return this.XmlItemMappings.FirstOrDefault(xim => xim.TargetPropertyName == targetPropertyName)?.Value;
        }
        #endregion
    }
}
