﻿namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public enum XmlItemMappingRuleType
    {
        Replace,
        Edit,
        Concat,
        Append
    }
}
