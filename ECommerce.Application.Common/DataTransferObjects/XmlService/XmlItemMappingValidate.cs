﻿namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class XmlItemMappingValidate
    {
        public string ElementName { get; set; }

        public XmlItemMappingValidateType XmlItemMappingValidateType { get; set; }

        public string Value { get; set; }
    }
}
