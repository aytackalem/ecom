﻿namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class XmlItemMappingRule
    {
        public XmlItemMappingRuleType XmlItemMappingRuleType { get; set; }

        public string OldValueAttributeName { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string NewValueAttributeName { get; set; }

        public string Command { get; set; }

        public string SplitParameter { get; set; }
    }
}
