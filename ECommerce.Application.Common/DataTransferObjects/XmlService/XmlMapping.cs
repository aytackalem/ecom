﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects.XmlService
{
    public class XmlMapping
    {
        #region Properties
        public string ListItemNodeName { get; set; }
        #endregion

        #region Navigation Properties
        public List<XmlItemMapping> XmlItemMappings { get; set; }

        public List<XmlItemMappingValidate> XmlItemMappingValidates { get; set; }
        #endregion
    }
}
