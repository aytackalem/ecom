﻿namespace ECommerce.Application.Common.DataTransferObjects.Widgets;

public class SaleSummary
{
    #region Properties
    public string Day { get; set; }

    public int Hour { get; set; }

    public decimal SalesAmount { get; set; }

    public decimal CumulativeSalesAmount { get; set; }

    public decimal CostAmount { get; set; }

    public int OrderCount { get; set; }

    public int ProductCount { get; set; }
    #endregion
}
