﻿namespace ECommerce.Application.Common.DataTransferObjects.Widgets
{
    public class MarketplaceCount
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int Count { get; set; }

        public decimal SalesAmount { get; set; }
        #endregion
    }
}
