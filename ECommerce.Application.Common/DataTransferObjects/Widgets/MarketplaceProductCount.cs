﻿namespace ECommerce.Application.Common.DataTransferObjects.Widgets
{
    public class MarketplaceProductCount
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int Count { get; set; }
        #endregion
    }
}
