﻿namespace ECommerce.Application.Common.DataTransferObjects.Widgets
{
    public class TopSeller
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string Name { get; set; }

        public int Quantity  { get; set; }

        public decimal SalesAmount { get; set; }

        public string FileName { get; set; }

        public decimal UnitPrice { get; set; }

        public int Stock { get; set; }
        #endregion
    }
}
