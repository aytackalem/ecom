﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class EmailProviderCompany
    {
        #region Properties
        public int Id { get; set; }

        public string EmailProviderId { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<EmailProviderConfiguration> EmailProviderConfigurations { get; set; }
        #endregion
    }
}
