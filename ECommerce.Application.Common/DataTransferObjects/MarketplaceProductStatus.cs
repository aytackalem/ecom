﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class MarketplaceProductStatus
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string MarketplaceProductId { get; set; }

        public string StockCode { get; set; }

        public string Url { get; set; }

        public bool Opened { get; set; }

        public bool Locked { get; set; }
        /// <summary>
        /// True geldiği tarktirde pricedirty true gönderilmez
        /// </summary>
        public bool DirtyPriceDisabled { get; set; }
        /// <summary>
        /// True geldiği tarktirde stokdirty true gönderilmez
        /// </summary>
        public bool DirtyStockDisabled { get; set; }

        public bool OnSale { get; set; }

        public decimal ListUnitPrice { get; set; }
        
        public decimal UnitPrice { get; set; }

        public int Stock { get; set; }

        public string Barcode { get; set; }

        /// <summary>
        /// Pazaryerinden gelen kendi productId'lerini ekliyoruz
        /// </summary>
        public string UUId { get; set; }
        #endregion
    }
}
