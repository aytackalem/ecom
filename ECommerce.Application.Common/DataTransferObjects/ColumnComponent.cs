﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class ColumnComponent
    {
        #region Properties
        [JsonProperty("W")]
        public int Width { get; set; }
        #endregion

        #region Navigation Properties
        [JsonProperty("PCS")]
        public List<ProductComponent> ProductComponents { get; set; }

        [JsonProperty("BCS")]
        public List<BannerComponent> BannerComponents { get; set; }

        [JsonProperty("SCS")]
        public List<SliderComponent> SliderComponents { get; set; }
        #endregion
    }
}
