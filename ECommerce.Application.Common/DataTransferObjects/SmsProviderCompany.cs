﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class SmsProviderCompany
    {
        #region Properties
        public int Id { get; set; }
        public string SmsProviderId { get; set; }
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<SmsProviderConfiguration> SmsProviderConfigurations { get; set; }
        public SmsProvider SmsProvider { get; set; }
        #endregion
    }
}
