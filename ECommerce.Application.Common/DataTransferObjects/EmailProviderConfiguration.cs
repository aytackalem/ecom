﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class EmailProviderConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public int EmailProviderCompanyId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
