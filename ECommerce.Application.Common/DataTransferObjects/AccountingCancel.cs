﻿using System;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class AccountingCancel
    {
        #region Properties
        public string DocumentNo { get; set; }

        public string PaymentType { get; set; }

        public string PaymentSource { get; set; }
        #endregion
    }
}
