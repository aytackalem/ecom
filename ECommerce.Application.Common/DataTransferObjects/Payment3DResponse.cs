﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Payment3DResponse
    {
        public bool AllowFrame { get; set; } = true;

        public String Content { get; set; }

        public bool ThreeDRedirect { get; set; }
    }
}
