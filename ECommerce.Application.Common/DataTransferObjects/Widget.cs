﻿using ECommerce.Application.Common.DataTransferObjects.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Widget
    {
        #region Navigation Properties
        public List<MarketplaceCount> MarketplaceCounts { get; set; }

        public List<MarketplaceProductCount> MarketplaceProductCounts { get; set; }

        public Summary OrderSummary { get; set; }

        public List<TopSeller> TopSellers { get; set; }
        #endregion
    }
}
