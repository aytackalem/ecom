﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Excel
    {
        #region Navigation Properties
        public Row Header { get; set; }

        public List<Row> Rows { get; set; }
        #endregion
    }
}
