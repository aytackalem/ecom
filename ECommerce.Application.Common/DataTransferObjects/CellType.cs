﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public enum CellType
    {
        #region Members
        Number,
        String
        #endregion
    }
}
