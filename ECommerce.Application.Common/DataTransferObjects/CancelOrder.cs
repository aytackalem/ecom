﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class CancelOrder
    {
 
        
        /// <summary>
        /// iade nedeni seçilmiş ise
        /// </summary>
        public bool Returned { get; set; }

        /// <summary>
        /// Faturama numarası
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// İptal edilien ürünler
        /// </summary>
        public List<CancelOrderDetail> CancelOrderDetails  { get; set; }
    }
}
