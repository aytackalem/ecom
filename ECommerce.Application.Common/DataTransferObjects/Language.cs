﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Language
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
