﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Domain
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public DomainSetting DomainSetting { get; set; }

        public List<Company> Companies { get; set; }
        #endregion
    }
}
