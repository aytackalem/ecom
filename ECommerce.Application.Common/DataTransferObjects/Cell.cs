﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Cell
    {
        #region Properties
        public CellType CellType { get; set; }

        public object Value { get; set; }

        public string Formula { get; set; }

        public bool Merge { get; set; }

        public string ColumnName { get; set; }

        public int RowCount { get; set; }
        #endregion
    }
}
