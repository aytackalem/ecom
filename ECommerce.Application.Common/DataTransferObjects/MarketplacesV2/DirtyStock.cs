﻿using System;

namespace ECommerce.Application.Common.DataTransferObjects.MarketplacesV2
{
    public class DirtyStock
    {
        #region Properties
        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public int MarketplaceCompanyId { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string ProductUUId { get; set; }

        public string ProductInformationUUId { get; set; }

        public string SkuCode { get; set; }

        public string StockCode { get; set; }

        public string Barcode { get; set; }

        public int Stock { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public bool UpdatePrice { get; set; }

        public decimal VatRate { get; set; }

        public DateTime? LastRequestDateTime { get; set; }

        public int ProductId { get; set; }

        public int ProductInformationId { get; set; }

        public string SellerCode { get; set; }

        public string PhotoUrl { get; set; }

        public string ProductInformationName { get; set; }

        public string VariantValuesDescription { get; set; }
        #endregion
    }
}
