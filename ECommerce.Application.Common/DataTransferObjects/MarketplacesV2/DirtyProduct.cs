﻿namespace ECommerce.Application.Common.DataTransferObjects.MarketplacesV2
{
    public class DirtyProduct
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ProductMarketplaceId { get; set; }

        public string ProductMarketplaceUUId { get; set; }

        public string SellerCode { get; set; }

        public int ProductInformationId { get; set; }

        public string ProductInformationName { get; set; }

        public string VariantValuesDescription { get; set; }

        public string SubTitle { get; set; }

        public string Content { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string ProductInformationMarketplaceUUId { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public string SkuCode { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal VatRate { get; set; }

        public int Stock { get; set; }

        public int DeliveryDay { get; set; }

        public string MarketplaceCategoryCode { get; set; }

        public string MarketplaceCategoryName { get; set; }

        public string MarketplaceBrandCode { get; set; }

        public string MarketplaceBrandName { get; set; }

        public bool Opened { get; set; }
        #endregion
    }

    public class DirtyProductPhoto
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string FileName { get; set; }

        public int DisplayOrder { get; set; } 
        #endregion
    }

    public class DirtyProductMarketplaceVariantValue
    {
        #region Properties
        public int ProductInformationMarketplaceId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }
        #endregion
    }
}