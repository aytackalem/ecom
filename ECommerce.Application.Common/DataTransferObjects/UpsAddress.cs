﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class UpsAddress
    {
        #region Properties
        public int CityCode { get; set; }

        public int AreaCode { get; set; }
        #endregion

    }
}
