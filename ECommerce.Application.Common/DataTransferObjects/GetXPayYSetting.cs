﻿using System;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class GetXPayYSetting
    {
        #region Properties
        public string ApplicationId { get; set; }

        public int Id { get; set; }

        public int X { get; set; }

        public decimal XLimit { get; set; }

        public int Y { get; set; }

        public DateTime StartDate { get; set; }
        
        public string StartDateString { get; set; }

        public DateTime EndDate { get; set; }
        
        public string EndDateString { get; set; }

        public bool Active { get; set; }
        #endregion

    }
}
