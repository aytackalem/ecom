﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class CreditCardPayment
    {
        #region Properties
        public decimal Amount { get; set; }

        public int OrderId { get; set; }

        public int PaymentId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerIdentityNumber { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerIpAddress { get; set; }

        public string CustomerCityName { get; set; }

        public string CustomerCountryName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Number { get; set; }

        public byte ExpireMonth { get; set; }

        public int ExpireYear { get; set; }

        public string Cvv { get; set; } 
        #endregion
    }
}
