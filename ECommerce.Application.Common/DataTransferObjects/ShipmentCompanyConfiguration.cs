﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class ShipmentCompanyConfiguration
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
