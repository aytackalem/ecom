﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class OrderShipment
    {
        public List<OrderShipmentDetail> OrderShipmentDetails { get; set; }
        public int Piece { get; set; }
        public bool Payor { get; set; }
    }
}
