﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class ProductComponent
    {
        #region Properties
        [JsonProperty("PI")]
        public int ProductId { get; set; }

        [JsonProperty("PII")]
        public int ProductInformationId { get; set; }

        [JsonProperty("N")]
        public string Name { get; set; }

        [JsonProperty("VV")]
        public string VariantValuesDescription { get; set; }

        [JsonProperty("U")]
        public string Url { get; set; }

        [JsonProperty("SUB")]
        public string SubTitle { get; set; }

        [JsonProperty("PY")]
        public bool Payor { get; set; }

        [JsonProperty("ISL")]
        public bool IsSale { get; set; }

        [JsonProperty("ISS")]
        public bool IsStock { get; set; }

        [JsonProperty("LUP")]
        public decimal ListUnitPrice { get; set; }

        [JsonProperty("UP")]
        public decimal UnitPrice { get; set; }

        [JsonProperty("VR")]
        public decimal VatRate { get; set; }

        [JsonProperty("D")]
        public decimal Discount { get; set; }

        [JsonProperty("FN")]
        public string FileName { get; set; }

        [JsonProperty("PL")]
        public List<string> ProductLabels { get; set; }

        public bool ShoppingCartDiscount { get; set; }
        #endregion



    }
}
