﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class CompanySetting
    {
        #region Properties
        /// <summary>
        /// Stoklar kontrol edilecek mi?
        /// </summary>
        public bool StockControl { get; set; }

        /// <summary>
        /// Sistemde olmayan ve pazaryerinden gelen ürüler otomatik açılsın mı?
        /// </summary>
        public bool CreateNonexistProduct { get; set; }

        /// <summary>
        /// Varsayılan para birimine ait id bilgisi.
        /// </summary>
        public string DefaultCurrencyId { get; set; }

        /// <summary>
        /// Varsayılan görüntüleme diline ait id bilgisi.
        /// </summary>
        public string DefaultLanguageId { get; set; }

        /// <summary>
        /// Varsayılan gönderi ülkesine ait id bilgisi.
        /// </summary>
        public int DefaultCountryId { get; set; }

        /// <summary>
        /// E-ticaret sisteminin kullanacağı tema.
        /// </summary>
        public string Theme { get; set; }

        /// Chacleme için uniq Id
        /// </summary>
        public string UUId { get; set; }

        /// <summary>
        /// Company pazaryeri kullanıyor mu?
        /// </summary>
        public bool IncludeMarketplace { get; set; }

        public bool IncludeWMS { get; set; }

        /// <summary>
        /// Company e-ticaret kullanıyor mu?
        /// </summary>
        public bool IncludeECommerce { get; set; }

        /// <summary>
        /// İhracat yapılıyor mu?
        /// </summary>
        public bool IncludeExport { get; set; }

        /// Sitenin Web site Url
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// Panel sipariş sayfasında ürünlerin çoktan seçmeli olup olmayacağına karar verir.
        /// </summary>
        public bool OrderDetailMultiselect { get; set; }

        /// <summary>
        /// İrsaliye üzerinde ürün isimleri gizlenecek mi?
        /// </summary>
        public bool ShipmentSecret { get; set; }

        /// <summary>
        /// İrsaliye üzerinde yazılacak özel ürün ismi.
        /// </summary>
        public string ShipmentSecretProductName { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyor.
        /// Winka
        /// Simsar
        /// Mikra
        /// </summary>
        public bool IncludeAccounting { get; set; }
        #endregion
    }
}
