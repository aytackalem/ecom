﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Row
    {
        #region Properties
        public List<Cell> Cells { get; set; }
        #endregion
    }
}
