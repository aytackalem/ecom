﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class SliderComponent
    {
        #region Navigation Properties
        [JsonProperty("PCS")]
        public List<ProductComponent> ProductComponents { get; set; }

        [JsonProperty("BCS")]
        public List<BannerComponent> BannerComponents { get; set; }
        #endregion
    }
}
