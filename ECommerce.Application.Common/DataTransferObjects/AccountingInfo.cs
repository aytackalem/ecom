﻿using System;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class AccountingInfo
    {
        #region Properties
        public string AccountingCompanyId { get; set; }

        public string Guid { get; set; }

        public string Url { get; set; }

        public Guid AccountTransactionId { get; set; }
        #endregion
    }
}
