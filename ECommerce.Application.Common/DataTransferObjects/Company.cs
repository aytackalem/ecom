﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Company
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public string FileName { get; set; }
        #endregion

        #region Navigation Properties
        public CompanySetting CompanySetting { get; set; }
        #endregion
    }
}
