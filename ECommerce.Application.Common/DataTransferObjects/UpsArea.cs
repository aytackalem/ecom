﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class UpsArea
    {
        public int Id { get; set; }

        public int? ParentUpsAreaId { get; set; }

        public int UpsCityId { get; set; }

        public string Name { get; set; }

    }
}
