﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Neighborhood
    {
        public string Name { get; set; }

        public District District { get; set; }
    }
}
