﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class SmsProvider
    {
        #region Properties
        public string Id { get; set; }
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<SmsProviderCompany> SmsProviderCompanies { get; set; }
        #endregion
    }
}
