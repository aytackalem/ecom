﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class DiscountCoupon
    {
        #region Properties
        public int Id { get; set; }

        public string ApplicationId { get; set; }

        public string Code { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public DiscountCouponSetting DisountCouponSetting { get; set; }
        #endregion
    }
}
