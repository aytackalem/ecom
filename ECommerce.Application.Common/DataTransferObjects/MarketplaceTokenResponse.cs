﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class MarketplaceTokenResponse
    {
        #region Properties
        public string Token { get; set; }

        public string ExpirationTime { get; set; }
        #endregion
    }
}
