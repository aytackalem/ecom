﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class DiscountCouponSetting
    {
        #region Properties
        public decimal Limit { get; set; }

        public bool SingleUse { get; set; }

        public bool Used { get; set; }

        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }
        #endregion
    }
}
