﻿using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class CancelOrderDetail
    {
        /// <summary>
        /// Ürün barkodu
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// iade edilecek miktar
        /// </summary>
        public int ApprovedQuantity { get; set; }

        /// <summary>
        /// Toplam ürün adeti
        /// </summary>
        public int Quantity { get; set; }
    }
}
