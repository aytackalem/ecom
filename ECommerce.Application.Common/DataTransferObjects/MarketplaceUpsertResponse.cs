﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class MarketplaceUpsertResponse
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public string RequestId { get; set; }
        #endregion
    }
}
