﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class ShipmentTracking
    {
        #region Properties
        public bool Delivered { get; set; }
        #endregion
    }
}
