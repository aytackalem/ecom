﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class RowComponent
    {
        #region Properties
        [JsonProperty("H")]
        public string Header { get; set; }
        #endregion

        #region Navigation Properties
        [JsonProperty("CCS")]
        public List<ColumnComponent> ColumnComponents { get; set; }
        #endregion
    }
}
