﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Common.DataTransferObjects
{
    /// <summary>
    /// Siparişi temsil eden sınıf.
    /// </summary>
    public class Order
    {
        #region Properties
        public int Id { get; set; }

        /// <summary>
        /// Siparişin oluşturuğu uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Sipariş kaynağına ait id bilgisi.
        /// </summary>
        public int OrderSourceId { get; set; }

        /// <summary>
        /// Siparişlerin kdv dahil indirim hariç toplam tutarı.
        /// </summary>
        public decimal ListTotal { get; set; }

        /// <summary>
        /// Sipariş kdv dahil toplam tutarı.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Sipariş kdv dahil toplam indirimi.
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Kargo Ücreti
        /// </summary>
        public decimal CargoFee { get; set; }

        /// <summary>
        /// Kapıda ödeme ücreti
        /// </summary>
        public decimal SurchargeFee { get; set; }

        /// <summary>
        /// Sepette Indirim Tutarı
        /// </summary>
        public decimal ShoppingCartDiscount { get; set; }

        /// <summary>
        /// Kdv dahil Komisyon Ücreti
        /// </summary>
        public decimal CommissionAmount { get; set; }

        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        public decimal VatExcListTotal { get; set; }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        public decimal VatExcTotal { get; set; }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        public decimal VatExcDiscount { get; set; }

        /// <summary>
        /// İlgili para birimine ait id bilgisi.
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// İlgili dile ait id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Siparişin ait olduğu pazar yerine ait id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// İlgili sipariş tipine ait id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Terminal paketlenme tarihi
        /// </summary>
        public DateTime EstimatedPackingDate { get; set; }

        /// <summary>
        /// Siparişin ait olduğu pazar yerinin sipariş numarası
        /// </summary>
        public string MarketplaceOrderNumber { get; set; }

        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Sipariş Tarihi
        /// </summary>
        public DateTime OrderDate { get; set; }
        #endregion

        #region Navigation Properties
        public List<OrderDetail> OrderDetails { get; set; }

        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        public List<Payment> Payments { get; set; } 
        #endregion
    }
}
