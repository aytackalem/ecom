﻿namespace ECommerce.Application.Common.DataTransferObjects
{
    public class OrderDeliveryAddress
    {
        public string Recipient { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public Neighborhood Neighborhood { get; set; }
    }
}
