﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Common.DataTransferObjects
{
    public class Payment
    {
        #region Property
        /// <summary>
        /// İlgili ödeme tipine ait id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// Ödeme tutarı.
        /// </summary>
        public decimal Amount { get; set; }

       
        #endregion
    }
}
