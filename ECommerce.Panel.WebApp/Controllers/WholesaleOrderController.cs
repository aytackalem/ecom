﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager,sales")]
    public class WholesaleOrderController : Base.ControllerBase
    {
        #region Fields
        private readonly IWholesaleOrderService _service;

        private readonly IWholesaleOrderTypeService _wholesaleOrderTypeService;
        #endregion

        #region Constructors
        public WholesaleOrderController(
            IWholesaleOrderService service,
            IWholesaleOrderTypeService wholesaleOrderTypeService,
            IDomainService domainService,
            IDomainFinder domainFinder,
            ICompanyFinder companyFinder,
            INotificationService notificationService,
            ICookieHelper cookieHelper) : base(
                domainService,
                domainFinder,
                companyFinder,
                notificationService,
                cookieHelper)
        {
            #region Fields
            this._service = service;
            this._wholesaleOrderTypeService = wholesaleOrderTypeService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index() => View(new Application.Panel.ViewModels.WholesaleOrders.Index
        {
            WholesaleOrderTypes = this._wholesaleOrderTypeService.ReadAsKeyValue().Data
        });

        public JsonResult Read(PagedRequest pagedRequest) => Json(this._service.Read(pagedRequest));

        [HttpGet]
        public IActionResult Update(int id) => PartialView(new Application.Panel.ViewModels.WholesaleOrders.UpdateViewModel
        {
            WholesaleOrderTypes = this._wholesaleOrderTypeService.ReadAsKeyValue().Data,
            WholesaleOrder = this._service.Read(id).Data
        });

        [HttpPost]
        public JsonResult Update([FromBody] ECommerce.Application.Panel.ViewModels.WholesaleOrders.Update update) => Json(this._service.Update(update.Id, update.WholesaleOrderTypeId));

        public JsonResult Create([FromBody] Application.Panel.ViewModels.WholesaleOrders.Create create) => Json(this._service.Create(new Application.Panel.DataTransferObjects.WholesaleOrder
        {
            IsExport = create.IsExport,
            WholesaleOrderDetails = create
                .WholesaleOrderDetails
                .Select(wod => new Application.Panel.DataTransferObjects.WholesaleOrderDetail
                {
                    ProductInformationId = wod.ProductInformationId,
                    Quantity = wod.Quantity
                })
                .ToList()
        }));
        #endregion
    }
}