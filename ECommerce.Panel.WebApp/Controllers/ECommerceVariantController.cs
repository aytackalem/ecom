﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ECommerceVariantController : Base.ControllerBase
    {
        #region Fields
        private readonly IECommerceVariantService _ecommerceVariantService;
        #endregion

        #region Constructors
        public ECommerceVariantController(IECommerceVariantService ecommerceVariantService,  IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._ecommerceVariantService = ecommerceVariantService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<JsonResult> GetByCategoryCodes([FromBody] GetByCategoryCodesParameter parameter)
        {
            return Json(await this._ecommerceVariantService.GetByCategoryCodesAsync(parameter));
        }
        #endregion
    }
}