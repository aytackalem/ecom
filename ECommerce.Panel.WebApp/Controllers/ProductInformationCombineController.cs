﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ProductInformationCombineController : Base.ControllerBase
    {
        #region Fields



        #endregion

        #region Constructors
        public ProductInformationCombineController( IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
         
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public IActionResult IndexPartial([FromBody] List<ProductInformationCombine> productInformationCombines)
        {
            var vm = new Application.Panel.ViewModels.Products.ProductInformationCombinePartial
            {
                ProductInformationCombines = productInformationCombines,
            };
            return PartialView(vm);
        }
        #endregion
    }
}
