﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class BankController  : Base.ControllerBase
    {
        #region Fields
        private readonly IBankService _service;
        #endregion

        #region Constructors
        public BankController(IBankService service,IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult Create([FromBody] Bank dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(string id)
        {
            return PartialView(this._service.Read(id));
        }

        [HttpPost]
        public JsonResult Update([FromBody] Bank dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
