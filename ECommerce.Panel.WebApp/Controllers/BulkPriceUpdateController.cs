﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.BulkPriceUpdates;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class BulkPriceUpdateController : Base.ControllerBase
    {
        #region Fields
        private readonly IProductService _service;

        private readonly ICategoryService _categoryService;
        #endregion

        #region Constructors
        public BulkPriceUpdateController( IProductService service, ICategoryService categoryService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._categoryService = categoryService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            Application.Panel.ViewModels.BulkPriceUpdates.Index index = new()
            {
                Categories = this._categoryService.ReadAsKeyValue().Data
            };

            return View(index);
        }

        [HttpGet]
        public FileResult Download([FromQuery] FilterRequest filterRequest)
        {
            return File(this._service.Download(filterRequest).Data, "application/vnd.ms-excel", "data.xlsx");
        }

        [HttpPost]
        public JsonResult Update()
        {
            return Json(this._service.UpdateBulkPrice(Request.Form.Files[0]));
        }
        #endregion
    }
}
