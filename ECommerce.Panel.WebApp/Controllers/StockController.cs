﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager,customerservice")]
    public class StockController : Base.ControllerBase
    {
        #region Fields
        private readonly IStockService _service;

        private readonly ISettingService _settingService;

        private readonly IBrandService _brandService;

        #endregion

        #region Constructors
        public StockController( IStockService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, IBrandService brandService, ISettingService settingService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._settingService = settingService;
            this._brandService = brandService;

            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var vm = new Application.Panel.ViewModels.Stocks.Index
            {
                IncludeAccounting = _settingService.IncludeAccounting,
                Brands = this._brandService.ReadAsKeyValue().Data
            };

            return View(vm);
        }


        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.Stocks.Update
            {
                ProductInformation = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] ProductInformation dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
