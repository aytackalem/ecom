﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class PosConfigurationController  : Base.ControllerBase
    {
        #region Fields
        private readonly IPosConfigurationService _service;
        #endregion

        #region Constructors
        public PosConfigurationController(IPosConfigurationService service,IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult Create([FromBody] PosConfiguration dto)
        {
            return Json(this._service.Create(dto));
        }

        public IActionResult Read(string posId)
        {
            return PartialView(new ECommerce.Application.Panel.ViewModels.PosConfigurations.Read
            {
                PosId = posId,
                PosConfigurations = this._service.Read(posId).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] PosConfiguration dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
