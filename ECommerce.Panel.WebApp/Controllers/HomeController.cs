﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers;

[Authorize(Roles = "user,sales,customerservice,manager")]
public class HomeController : Base.ControllerBase
{
    #region Fields
    private readonly ITenantFinder _tenantFinder;

    private readonly ISettingService _settingService;

    private readonly IWidgetService _widgetService;
    #endregion

    #region Constructors
    public HomeController(IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ISettingService settingService, IWidgetService widgetService, ITenantFinder tenantFinder, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        #region Fields
        this._settingService = settingService;
        this._widgetService = widgetService;
        this._tenantFinder = tenantFinder;
        #endregion
    }
    #endregion

    #region Methods
    public IActionResult Index()
    {
        string userRole = string.Empty;

        var claim = User.Claims.FirstOrDefault(c => c.Type == "role");
        if (claim != null)
            userRole = claim.Value;

        return View(new Application.Panel.ViewModels.Homes.Index
        {
            UserRole = userRole,
            Widget = _widgetService.Read(new Application.Common.Parameters.Widgets.WitgetRequest
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date
            }, userRole).Data
        });
    }

    public JsonResult FinancialWidget()
    {
        if (User.Claims.FirstOrDefault(c => c.Type == "role").Value != "manager")
            return Json(null);

        var datas = _widgetService.GetFinancialSummary().Data;

        ChartData chartData = new()
        {
            Labels = new(),
            Series = new List<decimal>[]
            {
                //The Day Before
                new(),
                //Yesterday
                new(),
                //Today
                new()
            }
        };

        FinancialWidget financialWidget = new()
        {
            ChartData = chartData,
            OrderCount = datas.Where(d => d.Day == "T" && d.Hour <= DateTime.Now.Hour).Sum(d => d.OrderCount),
            YesterdayOrderCount = datas.Where(d => d.Day == "Y" && d.Hour <= DateTime.Now.Hour).Sum(d => d.OrderCount),
            ProductCount = datas.Where(d => d.Day == "T" && d.Hour <= DateTime.Now.Hour).Sum(d => d.ProductCount),
            YesterdayProductCount = datas.Where(d => d.Day == "Y" && d.Hour <= DateTime.Now.Hour).Sum(d => d.ProductCount)
        };

        string[] labels = new[] {
            //The Day Before
            "TDB", 
            //Yesterday
            "Y", 
            //Today
            "T"
        };

        for (int i = 0; i < 24; i++)
        {
            chartData.Labels.Add($"{i.ToString().PadLeft(2, '0')}:00-{(i + 1).ToString().PadLeft(2, '0')}:00");
            //chartData.Labels.Add($"{i.ToString().PadLeft(2, '0')}");

            for (int k = 0; k < labels.Length; k++)
            {
                var amount = datas.Where(d => d.Day == labels[k] && d.Hour <= i).Sum(d => d.SalesAmount);

                //Current Sales Amount
                if (DateTime.Now.Hour == i)
                {
                    if (k == 1)
                    {
                        financialWidget.YesterdayCurrentSalesAmount = amount;
                    }
                    else if (k == 2)
                    {
                        financialWidget.CurrentSalesAmount = amount;
                    }
                }

                chartData.Series[k].Add(amount);
            }
        }



        return Json(financialWidget);
    }
    #endregion
}

public class FinancialWidget
{
    public ChartData ChartData { get; set; }

    public decimal CurrentSalesAmount { get; set; }

    public string CurrentSalesAmountString => $"{this.CurrentSalesAmount:N2} TL";

    public decimal YesterdayCurrentSalesAmount { get; set; }

    public string YesterdayCurrentSalesAmountString => $"{this.YesterdayCurrentSalesAmount:N2} TL";

    public decimal SalesAmount
    {
        get
        {
            if (this.CurrentSalesAmount == 0 || this.YesterdayCurrentSalesAmount == 0)
            {
                return 0;
            }

            return (this.CurrentSalesAmount - this.YesterdayCurrentSalesAmount) / this.CurrentSalesAmount * 100;
        }
    }

    public string SalesAmountRate => $"% {SalesAmount:N2} {(SalesAmount < 0 ? "🥵" : "🤩")}";

    public int OrderCount { get; set; }

    public int YesterdayOrderCount { get; set; }

    public decimal OrderCountRate
    {
        get
        {
            if (this.OrderCount == 0 || this.YesterdayOrderCount == 0)
            {
                return 0;
            }

            return ((decimal)this.OrderCount - this.YesterdayOrderCount) / this.OrderCount * 100M;
        }
    }

    public string OrderCountRateString => $"% {OrderCountRate:N2}";

    public int ProductCount { get; set; }

    public int YesterdayProductCount { get; set; }

    public decimal ProductCountRate
    {
        get
        {
            if (this.ProductCount == 0 || this.YesterdayProductCount == 0)
            {
                return 0;
            }

            return ((decimal)this.ProductCount - this.YesterdayProductCount) / this.ProductCount * 100M;
        }
    }

    public string ProductCountRateString => $"% {ProductCountRate:N2}";

    public string RefreshDateTime => $"Son güncellenme zamanı: {DateTime.Now:yyyy-MM-dd HH:mm:ss}";
}

public class ChartData
{
    public List<string> Labels { get; set; }

    public List<decimal>[] Series { get; set; }
}