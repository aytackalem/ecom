﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MoneyPointSettingController : Base.ControllerBase
    {
        #region Fields
        private readonly IMoneyPointSettingService _service;
        #endregion

        #region Constructors
        public MoneyPointSettingController( IMoneyPointSettingService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View(this._service.Read());
        }

        [HttpPost]
        public JsonResult Update([FromBody] MoneyPointSetting dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
