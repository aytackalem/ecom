﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Licenses;
using ECommerce.Domain.Entities;
using ECommerce.Infrastructure.Payment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class LicenseController : Base.ControllerBase
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly LicenseIyzicoPos _licenseIyzicoPos;
        #endregion

        #region Constructors
        public LicenseController(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, LicenseIyzicoPos licenseIyzicoPos, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._licenseIyzicoPos = licenseIyzicoPos;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var tenant = this
                ._unitOfWork
                .TenantRepository
                .DbSet()
                .Include(t => t.TenantSalesAmount)
                .First(t => t.Id == this._tenantFinder.FindId());

            var vm = new Application.Panel.ViewModels.Licenses.Index
            {
                MothlySalesAmount = tenant.TenantSalesAmount.Monthly,
                HalfYearlySalesAmount = tenant.TenantSalesAmount.HalfYearly,
                YearlySalesAmount = tenant.TenantSalesAmount.Yearly,
                ExpirationDate = tenant.ExpirationDate
            };

            return View(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] CreateRequest createRequest)
        {
            var tenant = this
                ._unitOfWork
                .TenantRepository
                .DbSet()
                .Include(t => t.TenantSalesAmount)
                .Include(t => t.TenantInvoiceInformation)
                .First(t => t.Id == this._tenantFinder.FindId());

            var amount = 0M;
            switch (createRequest.Code)
            {
                case "M":
                    amount = tenant.TenantSalesAmount.Monthly;
                    break;
                case "HY":
                    amount = tenant.TenantSalesAmount.HalfYearly;
                    break;
                case "Y":
                    amount = tenant.TenantSalesAmount.Yearly;
                    break;
                default:
                    throw new Exception();
            }
            amount *= 1.18M;

            var tenantPayment = new TenantPayment()
            {
                Code = createRequest.Code,
                Amount = amount,
                TenantId = this._tenantFinder.FindId()
            };

            this._unitOfWork.TenantPaymentRepository.Create(tenantPayment);

            var pay3dResponse = this._licenseIyzicoPos.Pay3d(new Application.Common.DataTransferObjects.CreditCardPayment
            {
                FirstName = createRequest.Name,
                Number = createRequest.Number,
                ExpireMonth = byte.Parse(createRequest.Month),
                ExpireYear = int.Parse(createRequest.Year),
                Amount = amount,
                Phone = "5555555555",
                CustomerAddress = "Adres",
                CustomerCityName = "Şehir",
                CustomerCountryName = "İlçe",
                CustomerEmail = "a@a.com",
                CustomerId = this._tenantFinder.FindId(),
                Cvv = createRequest.Cvv,
                PaymentId = tenantPayment.Id,
                OrderId = tenantPayment.Id,
            });

            return Json(pay3dResponse);
        }

        public IActionResult Check3d()
        {
            var r = this._licenseIyzicoPos.Check3d();
            if (r.Success)
            {
                var tenantPayment = this
                    ._unitOfWork
                    .TenantPaymentRepository
                    .DbSet()
                    .FirstOrDefault(tp => tp.Id == r.Data.PaymentId);

                tenantPayment.Paid = true;

                this._unitOfWork.TenantPaymentRepository.Update(tenantPayment);
            }

            return View(new { Success = r.Success, Message = r.Message });
            //return Json(r);
        }
        #endregion
    }
}
