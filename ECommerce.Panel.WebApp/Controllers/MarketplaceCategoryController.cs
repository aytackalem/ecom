﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceCategoryController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplaceCategoryService _service;
        #endregion

        #region Constructors
        public MarketplaceCategoryController(IMarketplaceCategoryService service,  IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public JsonResult Autocomplete(string marketplaceId, int take, string q)
        {
            var response = this._service.ReadAsKeyValue(marketplaceId, take, q);
            return Json(response);
        }
        #endregion
    }
}
