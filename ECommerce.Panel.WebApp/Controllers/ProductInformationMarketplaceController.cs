﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.ProductInformationMarketplaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ProductInformationMarketplaceController : Base.ControllerBase
    {
        #region Fields
        private readonly ICompanyService _companyService;

        private readonly IMarketplaceService _marketplaceService;

        private readonly IProductService _service;

        private readonly IProductMarketplaceService _productMarketplaceService;

        private readonly IProductInformationMarketplaceService _productInformationMarketplaceService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceCategoryService _marketplaceCategoryService;

        private readonly IECommerceCategoryService _ecommerceCategoryService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        private readonly IECommerceVariantService _ecommerceVariantService;
        #endregion

        #region Constructors
        public ProductInformationMarketplaceController(IECommerceVariantService ecommerceVariantService, IECommerceCategoryService ecommerceCategoryService, IProductService service, IProductMarketplaceService productMarketplaceService, IProductInformationMarketplaceService productInformationMarketplaceService, IMarketplaceService marketPlaceService, ICompanyService companyService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceCategoryService marketplaceCategoryService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._ecommerceVariantService = ecommerceVariantService;
            this._ecommerceCategoryService = ecommerceCategoryService;
            this._service = service;
            this._productMarketplaceService = productMarketplaceService;
            this._productInformationMarketplaceService = productInformationMarketplaceService;
            this._marketplaceService = marketPlaceService;
            this._marketplaceCategoryService = marketplaceCategoryService;
            this._marketplaceVariantService = marketplaceVariantService;
            this._companyService = companyService;
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// Talep edilen ürün içerisindeki "ProductInformation" nesnelerinin pazaryeri tanımlarını dönen partial view.
        /// </summary>
        /// <param name="id">Ürün id bilgisi.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> IndexPartial(int id)
        {
            var companyResponse = this._companyService.Read();
            var marketplacesResponse = this._marketplaceService.Read();
            var productResponse = await this._service.ReadAsync(id);

            var productMarketplacesResponse = await this._productMarketplaceService.ReadOrGenerateAsync(productResponse.Data, marketplacesResponse.Data);

            if(productMarketplacesResponse.Success == false)
            {
                var vmPartial = new Application.Panel.ViewModels.Products.MarketplaceSettings.Partial
                {
                    Success = false,
                    Message="Lütfen Pazaryeri Ekleyiniz"
                };

                return PartialView(vmPartial);
            }

            #region Marketplace Variants
            var marketplaceCategoryCodes = productMarketplacesResponse
                    .Data
                    .GroupBy(d => d.MarketplaceId)
                    .Select(d => new GetByCategoryCodesParameter
                    {
                        MarketplaceId = d.Key,
                        CategoryCodes = d.Select(pm => pm.MarketplaceCategoryCode).ToList()
                    })
                    .ToList();
            List<Task<DataResponse<GetByCategoryCodesResponse>>> tasks = new();
            marketplaceCategoryCodes.ForEach(mcc =>
            {
                var marketplace = marketplacesResponse.Data.FirstOrDefault(m => m.Id == mcc.MarketplaceId);
                if (marketplace == null) return;

                if (marketplace.IsECommerce)
                    tasks.Add(this._ecommerceVariantService.GetByCategoryCodesAsync(mcc));
                else
                    tasks.Add(this._marketplaceVariantService.GetByCategoryCodesAsync(mcc));
            });

            var marketplaceVariants = await Task.WhenAll(tasks);
            #endregion

            var productInformationMarketplaces = await this
                ._productInformationMarketplaceService
                .ReadOrGenerateAsync(productResponse.Data.ProductInformations, productMarketplacesResponse.Data, marketplacesResponse.Data, companyResponse.Data.CompanySetting.IncludeAccounting, productResponse.Data.CategoryId);

            #region Marketplace Categories
            Stopwatch stopwatch = Stopwatch.StartNew();

            List<Task<DataResponse<GetByMarketplaceIdResponse>>> tasks2 = new();
            marketplacesResponse.Data.ForEach(mc =>
            {
                if (mc.IsECommerce == false)
                    tasks2.Add(this._marketplaceCategoryService.GetByMarketplaceIdAsync(mc.Id));
                else
                    tasks2.Add(this._ecommerceCategoryService.GetByMarketplaceIdAsync(mc.Id));
            });

            var marketplaceCategories = await Task.WhenAll(tasks2);

            stopwatch.Stop();

            if (marketplaceCategories.Any(mc => mc.Success == false))
                return PartialView(new Application.Panel.ViewModels.Products.MarketplaceSettings.Partial
                {
                    Success = false,
                    Message = marketplaceCategories.FirstOrDefault(mc => mc.Success == false).Message
                });
            #endregion

            var vm = new Application.Panel.ViewModels.Products.MarketplaceSettings.Partial
            {
                Success = true,
                IncludeAccounting = companyResponse.Data.CompanySetting.IncludeAccounting,
                Marketplaces = marketplacesResponse.Data,
                Product = productResponse.Data,
                ProductMarketplaces = productMarketplacesResponse.Data,
                ProductInformationMarketplaces = productInformationMarketplaces.Data,
                MarketplaceVariants = marketplaceVariants.Select(mv => mv.Data).ToList(),
                MarketplaceCategories = marketplaceCategories.Select(mc => mc.Data).ToList()
            };

            return PartialView(vm);
        }

        [HttpPost]
        public async Task<JsonResult> Upsert([FromBody] UpsertRequest request)
        {
            var productMarketplaceResponse = await this._productMarketplaceService.UpsertAsync(request.ProductMarketplaces);
            var productInformationMarketplaceResponse = await this._productInformationMarketplaceService.UpsertAsync(request.ProductInformationMarketplaces);

            return Json(new
            {
                productMarketplaceResponse,
                productInformationMarketplaceResponse
            });

            //return Json(this._service.UpsertProductMarketplace(product));
        }
        #endregion
    }
}
