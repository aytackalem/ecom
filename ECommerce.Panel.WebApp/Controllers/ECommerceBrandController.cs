﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.BrandService;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ECommerceBrandController : Base.ControllerBase
    {
        #region Fields
        private readonly IECommerceBrandService _ecommerceBrandService;
        #endregion

        #region Constructors
        public ECommerceBrandController(IECommerceBrandService ecommerceBrandService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder,  INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._ecommerceBrandService = ecommerceBrandService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<JsonResult> SearchByName(SearchByNameParameters searchByNameParameters)
        {
            return Json(await this._ecommerceBrandService.SearchByNameAsync(searchByNameParameters));
        }
        #endregion
    }
}
