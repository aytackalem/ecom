﻿using DocumentFormat.OpenXml.Spreadsheet;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.ViewModels.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "customerservice,manager")]
    public class ProductController : Base.ControllerBase
    {
        #region Fields
        private readonly IProductService _service;

        private readonly ISupplierService _supplierService;

        private readonly IBrandService _brandService;

        private readonly IProductCategorizationService _productCategorizationService;

        private readonly ICategoryService _categoryService;

        private readonly ILanguageService _languageService;

        private readonly ICurrencyService _currencyService;

        private readonly ILabelService _labelService;

        private readonly IVariantService _variantService;

        private readonly IPropertyService _productPropertyService;

        private readonly ISettingService _settingService;

        private readonly IProductSourceDomainService _productSourceDomainService;

        private readonly IRazorViewEngine _razorViewEngine;

        private readonly ITempDataProvider _tempDataProvider;

        private readonly IServiceProvider _serviceProvider;

        private readonly IProductGenericPropertyService _productGenericPropertyService;
        #endregion

        #region Constructors
        public ProductController(IProductService service, ISupplierService supplierService, IProductSourceDomainService productSourceDomainService, IBrandService brandService, ICategoryService categoryService, ILanguageService languageService, ICurrencyService currencyService, ILabelService labelService, IVariantService variantService, IPropertyService productPropertyService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ISettingService settingService, ICookieHelper cookieHelper, IRazorViewEngine razorViewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider, IProductCategorizationService productCategorizationService, IProductGenericPropertyService productGenericPropertyService) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            this._supplierService = supplierService;
            this._brandService = brandService;
            this._categoryService = categoryService;
            this._languageService = languageService;
            this._currencyService = currencyService;
            this._labelService = labelService;
            this._variantService = variantService;
            this._productPropertyService = productPropertyService;
            this._settingService = settingService;
            this._productSourceDomainService = productSourceDomainService;
            this._productCategorizationService = productCategorizationService;
            #endregion

            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
            _productGenericPropertyService = productGenericPropertyService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult Index()
        {
            var vm = new Application.Panel.ViewModels.Products.Index
            {
                Tenant = _settingService.Tenant,
                IncludeAccounting = _settingService.IncludeAccounting,
                Brands = this._brandService.ReadAsKeyValue().Data.OrderBy(_ => _.Value).ToList(),
                ProductSourceDomains = _productSourceDomainService.ReadAsKeyValue().Data,
                Categories = _categoryService.ReadAsKeyValue().Data.OrderBy(_ => _.Value).ToList(),
                ProductCategorizations = _productCategorizationService.Read().Data.OrderBy(_ => _).ToList(),
                Suppliers = _productGenericPropertyService.ReadAsValue("Marka").Data.OrderBy(_ => _).ToList()
            };

            return View(vm);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var vm = new Application.Panel.ViewModels.Products.Create
            {
                Languages = this._languageService.ReadAsKeyValue().Data,
                Currencies = this._currencyService.ReadAsKeyValue().Data,
                Brands = this._brandService.ReadAsKeyValue().Data,
                Categories = this._categoryService.ReadAsKeyValue().Data,
                Suppliers = this._supplierService.ReadAsKeyValue().Data,
                Labels = this._labelService.ReadAsKeyValue().Data,
                Variants = this._variantService.Read().Data,
                Properties = this._productPropertyService.Read().Data,
                IncludeMarketplace = this._settingService.IncludeMarketplace,
                IncludeWMS = this._settingService.IncludeWMS,
                IncludeECommerce = this._settingService.IncludeECommerce,
                IncludeAccounting = this._settingService.IncludeAccounting
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] Product dto)
        {
            return Json(this._service.Create(dto));
        }

        [HttpGet]
        public async Task<JsonResult> Read(PagedRequest pagedRequest)
        {
            var productsResponse = this._service.Read(pagedRequest);
            if (productsResponse.Success == false)
            {
                return Json(productsResponse);
            }

            var marketplaces = productsResponse
                .Data
                .SelectMany(x => x.ProductInformations)
                .SelectMany(x => x.ProductInformationMarketplaces)
                .GroupBy(x => x.MarketplaceId)
                .Select(pm => pm.Key)
                .ToList();

            var slicable = productsResponse
                .Data
                .Any(d => d
                    .ProductInformations
                    .Any(pi => pi
                        .ProductInformationVariants
                        .Any(piv => piv
                            .VariantValue
                            .Variant
                            .Slicer)));

            if (slicable)
            {
                SlicedReadViewModel slicedModel = new SlicedReadViewModel
                {
                    Tenant = this._settingService.Tenant,
                    Marketplaces = marketplaces,
                    Products = productsResponse.Data.Select(p => new SlicedProductViewModel
                    {
                        Id = p.Id,
                        ReturnRate = p.ReturnRate,
                        MarketplaceUrls = p
                            .ProductInformations
                            .FirstOrDefault()
                            .ProductInformationMarketplaces
                            .ToDictionary(pim => pim.MarketplaceId, pim => pim.Url),
                        Guid = Guid.NewGuid(),
                        Categorizations = p
                            .ProductInformations
                            .Select(pi => pi.Categorization)
                            .Where(pi => string.IsNullOrEmpty(pi) == false)
                            .Distinct()
                            .ToArray(),
                        MarketplaceStatuses = p
                            .ProductInformations
                            .SelectMany(pi => pi.ProductInformationMarketplaces)
                            .GroupBy(pim => pim.MarketplaceId)
                            .ToDictionary(gpim => gpim.Key, gpim => gpim.Select(x => x.Opened).Distinct().ToList()),
                        MarketplaceListPrices = p
                            .ProductInformations
                            .SelectMany(pi => pi.ProductInformationMarketplaces)
                            .GroupBy(pim => pim.MarketplaceId)
                            .ToDictionary(gpim => gpim.Key, gpim =>
                            {
                                var min = gpim.Min(x => x.ListUnitPrice);
                                var max = gpim.Max(x => x.ListUnitPrice);
                                if (min != max)
                                    return $"{min:N2} - {max:N2}";
                                else
                                    return $"{max:N2}";
                            }),
                        MarketplacePrices = p
                            .ProductInformations
                            .SelectMany(pi => pi.ProductInformationMarketplaces)
                            .GroupBy(pim => pim.MarketplaceId)
                            .ToDictionary(gpim => gpim.Key, gpim =>
                            {
                                var min = gpim.Min(x => x.UnitPrice);
                                var max = gpim.Max(x => x.UnitPrice);
                                if (min != max)
                                    return $"{min:N2} - {max:N2}";
                                else
                                    return $"{max:N2}";
                            }),
                        Photo = p
                            .ProductInformations
                            .FirstOrDefault()
                            .ProductInformationPhotos
                            .FirstOrDefault()
                            .FileName,
                        CreatedDateString = p.CreatedDateStr,
                        Name = p.Name,
                        Code = p.SellerCode,
                        Stock = p.TotalStock,
                        Models = p
                            .ProductInformations.Any(x => x
                                    .ProductInformationVariants.Any(pi => pi.VariantValue.Variant.Slicer)) ? p
                            .ProductInformations.Where(x => x
                                    .ProductInformationVariants.Any(pi => pi.VariantValue.Variant.Slicer))
                            .GroupBy(pi => pi
                                    .ProductInformationVariants
                                    .FirstOrDefault(piv => piv.VariantValue.Variant.Slicer)
                                    .VariantValue
                                    .VariantValueTranslations
                                    .FirstOrDefault()
                                    .Value)
                            .Select(gpi => new SlicedModelViewModel
                            {
                                Slicer = gpi
                                    .FirstOrDefault()
                                    .ProductInformationVariants
                                    .FirstOrDefault(piv => piv.VariantValue.Variant.Slicer)
                                    .VariantValue
                                    .Variant
                                    .VariantTranslations
                                    .FirstOrDefault()
                                    .Name,
                                ColorReturnRate = gpi.FirstOrDefault().ColorReturnRate,
                                SlicerValue = gpi.Key,
                                Categorization = gpi.FirstOrDefault().Categorization,
                                Guid = Guid.NewGuid(),
                                MarketplaceStatuses = gpi
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim => gpim.Select(x => x.Opened).Distinct().ToList()),
                                MarketplaceListPrices = gpi
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim =>
                                    {
                                        var min = gpim.Min(x => x.ListUnitPrice);
                                        var max = gpim.Max(x => x.ListUnitPrice);
                                        if (min != max)
                                            return $"{min:N2} - {max:N2}";
                                        else
                                            return $"{max:N2}";
                                    }),
                                MarketplacePrices = gpi
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim =>
                                    {
                                        var min = gpim.Min(x => x.UnitPrice);
                                        var max = gpim.Max(x => x.UnitPrice);
                                        if (min != max)
                                            return $"{min:N2} - {max:N2}";
                                        else
                                            return $"{max:N2}";
                                    }),
                                Stock = gpi.Sum(x => x.Stock),
                                Photo = gpi.FirstOrDefault().ProductInformationPhotos.FirstOrDefault().FileName,
                                Code = gpi.FirstOrDefault().SkuCode,
                                Name = gpi.FirstOrDefault()?.ProductInformationTranslations.FirstOrDefault()?.Name,
                                Variants = gpi.Select(v =>
                                {
                                    var variantable = gpi
                                            .FirstOrDefault()
                                            .ProductInformationVariants
                                            .Any(piv => piv.VariantValue.Variant.Varianter);

                                    var varianter = string.Empty;
                                    var varianterValue = string.Empty;

                                    if (variantable)
                                    {
                                        varianter = gpi
                                            .FirstOrDefault()
                                            .ProductInformationVariants
                                            .FirstOrDefault(piv => piv.VariantValue.Variant.Varianter)
                                            .VariantValue
                                            .Variant
                                            .VariantTranslations
                                            .FirstOrDefault()
                                            .Name;

                                        varianterValue = v
                                            .ProductInformationVariants
                                            ?.FirstOrDefault(piv => piv.VariantValue.Variant.VariantTranslations.Any(vt => vt.Name == varianter))
                                            ?.VariantValue
                                            ?.VariantValueTranslations
                                            ?.FirstOrDefault()
                                            ?.Value;
                                    }

                                    return new Application.Panel.ViewModels.Products.SlicedVariantViewModel
                                    {
                                        ReturnRate = v.ReturnRate,
                                        Variantable = variantable,
                                        Varianter = varianter,
                                        VarianterValue = varianterValue,
                                        Name = v
                                            .ProductInformationTranslations
                                            .FirstOrDefault()
                                            .VariantValuesDescription
                                            ?.Replace("(", "")?.Replace(")", ""),
                                        Barcode = v.Barcode,
                                        Stock = v.Stock,
                                        StockCode = v.StockCode,
                                        ProductInformationMarketplaces = v.ProductInformationMarketplaces
                                    };
                                }).ToList(),
                            }).ToList()
                            : new List<SlicedModelViewModel>
                            {
                                new SlicedModelViewModel
                            {

                                Slicer = "",
                                ColorReturnRate = 0,
                                SlicerValue = "",
                                Categorization = "",
                                Guid = Guid.NewGuid(),
                                MarketplaceStatuses = p
                                    .ProductInformations
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim => gpim.Select(x => x.Opened).Distinct().ToList()),
                                MarketplaceListPrices = p
                                    .ProductInformations
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim =>
                                    {
                                        var min = gpim.Min(x => x.ListUnitPrice);
                                        var max = gpim.Max(x => x.ListUnitPrice);
                                        if (min != max)
                                            return $"{min:N2} - {max:N2}";
                                        else
                                            return $"{max:N2}";
                                    }),
                                MarketplacePrices = p
                                    .ProductInformations
                                    .SelectMany(pi => pi.ProductInformationMarketplaces)
                                    .GroupBy(pim => pim.MarketplaceId)
                                    .ToDictionary(gpim => gpim.Key, gpim =>
                                    {
                                        var min = gpim.Min(x => x.UnitPrice);
                                        var max = gpim.Max(x => x.UnitPrice);
                                        if (min != max)
                                            return $"{min:N2} - {max:N2}";
                                        else
                                            return $"{max:N2}";
                                    }),
                                Stock = p
                                    .ProductInformations.Sum(x => x.Stock),
                                Photo = p.ProductInformations.FirstOrDefault()?.ProductInformationPhotos.FirstOrDefault()?.FileName,
                                Code = p.ProductInformations.FirstOrDefault().SkuCode,
                                Name = p.ProductInformations.FirstOrDefault()?.ProductInformationTranslations.FirstOrDefault()?.Name,
                                Variants = new List<SlicedVariantViewModel>
                                {
                                     new Application.Panel.ViewModels.Products.SlicedVariantViewModel
                                    {
                                        ReturnRate = 0,
                                        Variantable = false,
                                        Varianter = "",
                                        VarianterValue = "",
                                        Name = "",
                                        Barcode =  p.ProductInformations.FirstOrDefault().Barcode,
                                        Stock =  p.ProductInformations.FirstOrDefault().Stock,
                                        StockCode =  p.ProductInformations.FirstOrDefault().StockCode,
                                        ProductInformationMarketplaces =  p.ProductInformations.FirstOrDefault().ProductInformationMarketplaces
                                    }
                                }
                            }
                            }
                    }).ToList()
                };

                return Json(new
                {
                    Page = productsResponse.Page,
                    PageRecordsCount = productsResponse.PageRecordsCount,
                    RecordsCount = productsResponse.RecordsCount,
                    PagesCount = productsResponse.PagesCount,
                    Html = await RenderToStringAsync("Product/SlicedRead", slicedModel),
                    Sorting = pagedRequest.Sort,
                    Asc = pagedRequest.Asc
                });
            }

            ReadViewModel model = new ReadViewModel
            {
                Tenant = this._settingService.Tenant,
                Marketplaces = marketplaces,
                Products = productsResponse.Data.SelectMany(d => d.ProductInformations.Select(pi => new ProductViewModel
                {
                    Id = d.Id,
                    Name = pi.ProductInformationTranslations.FirstOrDefault()?.Name,
                    Code = d.SellerCode,
                    Barcode = pi.Barcode,
                    StockCode = pi.StockCode,
                    Stock = pi.Stock,
                    Photo = pi.ProductInformationPhotos.FirstOrDefault().FileName,
                    MarketplacePrices = pi.ProductInformationMarketplaces.ToDictionary(pim => pim.MarketplaceId, pim => $"{pim.UnitPrice:N2}"),
                    MarketplaceUrls = pi.ProductInformationMarketplaces.ToDictionary(pim => pim.MarketplaceId, pim => pim.Url),
                    MarketplaceStatuses = pi.ProductInformationMarketplaces.ToDictionary(pim => pim.MarketplaceId, pim => pim.Opened)
                })).ToList()
            };

            return Json(new
            {
                Page = productsResponse.Page,
                PageRecordsCount = productsResponse.PageRecordsCount,
                RecordsCount = productsResponse.RecordsCount,
                PagesCount = productsResponse.PagesCount,
                Html = await RenderToStringAsync("Product/Read", model),
                Sorting = pagedRequest.Sort,
                Asc = pagedRequest.Asc
            });
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var vm = new Application.Panel.ViewModels.Products.Update
            {
                Product = this._service.Read(id).Data,
                Languages = this._languageService.ReadAsKeyValue().Data,
                Currencies = this._currencyService.ReadAsKeyValue().Data,
                Brands = this._brandService.ReadAsKeyValue().Data,
                Categories = this._categoryService.ReadAsKeyValue().Data,
                Suppliers = this._supplierService.ReadAsKeyValue().Data,
                Labels = this._labelService.ReadAsKeyValue().Data,
                Variants = this._variantService.Read().Data,
                ProductProperties = this._productPropertyService.Read().Data,
                MaxBarcode = this._service.MaxBarcode().Data,
                MaxStockCode = this._service.MaxStockCode().Data,
                IncludeMarketplace = this._settingService.IncludeMarketplace,
                IncludeECommerce = this._settingService.IncludeECommerce,
                IncludeAccounting = this._settingService.IncludeAccounting,
                IncludeWMS = this._settingService.IncludeWMS
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Update([FromBody] Product dto)
        {
            return Json(this._service.Update(dto));
        }

        [HttpGet]
        public JsonResult Autocomplete(int take, string q)
        {
            return Json(this._service.ReadAsKeyValue(take, q));
        }

        [HttpPost]
        public JsonResult Delete([FromBody] Product dto)
        {

            return Json(this._service.Deleted(dto.Id));

        }

        [HttpPost]
        public JsonResult Last12MonthSales([FromBody] List<string> skuCodes)
        {
            var vm = new Last12MonthSaleViewModel
            {
                Labels = new List<string>(),
                Items = skuCodes.Select(skuCode => new Last12MonthSaleItem
                {
                    SkuCode = skuCode,
                    Series = new()
                }).ToList()
            };

            /* Generate Labels */
            var lastYear = DateTime.Now.AddMonths(-11);

            for (int i = 0; i < 12; i++)
            {
                var date = lastYear.AddMonths(i);

                var label = $"{date.Year}-{date.Month}";

                vm.Labels.Add(label);
            }

            var response = this._service.Last12MonthSales(skuCodes);

            foreach (var skuCode in skuCodes)
            {
                var categorizations = response.Data.Where(d => d.SkuCode == skuCode).Select(d => d.Categorization).Distinct().ToList();
                if (categorizations is [])
                {
                    vm.Items.FirstOrDefault(item => item.SkuCode == skuCode).Series.Add(new Last12MonthSaleItemSerie
                    {
                        Categorization = string.Empty,
                        Data = vm.Labels.Select(l => "NaN").ToList()
                    });
                }
                else
                {
                    foreach (var categorization in categorizations)
                    {
                        var series = vm.Items.FirstOrDefault(item => item.SkuCode == skuCode).Series;
                        if (series.Any(s => s.Categorization == categorization) == false)
                        {
                            series.Add(new Last12MonthSaleItemSerie
                            {
                                Categorization = categorization,
                                Data = vm.Labels.Select(l => "NaN").ToList()
                            });
                        }

                        for (int i = 0; i < vm.Labels.Count; i++)
                        {
                            var label = vm.Labels[i];
                            var data = response.Data.FirstOrDefault(d => d.SkuCode == skuCode && d.Categorization == categorization && d.Date == label);
                            if (data is not null)
                            {
                                vm.Items.FirstOrDefault(item => item.SkuCode == skuCode).Series.FirstOrDefault(serie => serie.Categorization == categorization).Data[i] = data.Quantity.ToString();
                            }
                        }
                    }
                }
            }

            return Json(vm);
        }
        #endregion

        private async Task<string> RenderToStringAsync(string viewName, object model)
        {
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }
    }
}
