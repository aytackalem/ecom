﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class StocktakingController : Base.ControllerBase
    {
        #region Fields
        private readonly IStocktakingService _service;

        private readonly IStocktakingTypeService _stocktakingTypeService;
        #endregion

        #region Constructors
        public StocktakingController(IStocktakingService service, IStocktakingTypeService stocktakingTypeService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            this._stocktakingTypeService = stocktakingTypeService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View(new Application.Panel.ViewModels.Stocktakings.Index
            {
                StocktakingTypes = this._stocktakingTypeService.ReadAsKeyValue().Data
            });
        }

        [HttpGet]
        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.Stocktakings.Create());
        }

        [HttpPost]
        public JsonResult Create([FromBody] Stocktaking dto)
        {
            return Json(this._service.Create(dto));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.Stocktakings.Update
            {
                Stocktaking = this._service.Read(id).Data,
                StocktakingTypes = this._stocktakingTypeService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] Stocktaking dto)
        {
            return Json(this._service.Update(dto));
        }

        [HttpPost]
        public JsonResult Approve([FromBody] Stocktaking dto)
        {
            return Json(this._service.Approve(dto));
        }

        [HttpPost]
        public JsonResult Delete([FromBody] Stocktaking dto)
        {
            return Json(this._service.Delete(dto));
        }
        #endregion
    }
}
