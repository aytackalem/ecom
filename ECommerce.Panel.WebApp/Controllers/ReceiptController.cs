﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers;

public class UpdateDeliveredModel
{
    public int Id { get; set; }

    public bool Delivered { get; set; }
}

public class UpdateCancelledModel
{
    public int Id { get; set; }
}

/// <summary>
/// Lafaba icin eklenmistir. Kesim foyu islemleri bu controller da yapilacaktir.
/// </summary>
[Authorize(Roles = "user,manager")]
public class ReceiptController : Base.ControllerBase
{
    #region Fields
    private readonly IReceiptService _service;

    private readonly ISupplierService _supplierService;

    private readonly IProductInformationService _productInformationService;
    #endregion

    #region Constructors
    public ReceiptController(
        IDomainService domainService,
        IDomainFinder domainFinder,
        ICompanyFinder companyFinder,
        INotificationService notificationService,
        ICookieHelper cookieHelper,
        ISupplierService supplierService,
        IProductInformationService productInformationService,
        IReceiptService service) : base(
            domainService,
            domainFinder,
            companyFinder,
            notificationService,
            cookieHelper)
    {
        this._supplierService = supplierService;
        this._productInformationService = productInformationService;
        this._service = service;
    }
    #endregion

    #region Methods
    [Authorize(Roles = "manager")]
    [HttpPost]
    public async Task<FileResult> Index(Receipt receipt)
    {
        #region Fetch Data
        var allProductInformations = this
                ._productInformationService
                .Read(receipt
                    .ReceiptItems
                    .Select(ri => ri.ProductInformationId)
                    .ToList());

        var sellerCode = allProductInformations.Data.First().Product.SellerCode;
        #endregion

        #region Sku Code Based Pagination
        var allSkuCodes = allProductInformations.Data.Select(pi => pi.SkuCode).Distinct().ToList();

        var pagesCount = Math.Ceiling(allSkuCodes.Count / 8d);
        #endregion

        PdfDocument pdfDocument = new();

        for (int p = 0; p < pagesCount; p++)
        {
            var skuCodes = allSkuCodes.Skip(p * 8).Take(8);

            var productInformations = allProductInformations.Data.Where(pi => skuCodes.Contains(pi.SkuCode)).ToList();

            #region Convert To Pivot
            var variants = productInformations
                  .SelectMany(pi => pi
                      .ProductInformationVariants
                      .Select(piv => new
                      {
                          Variant = piv.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name,
                          VariantValue = piv.VariantValue.VariantValueTranslations.FirstOrDefault().Value
                      }))
                  .Distinct()
                  .Where(x => x.Variant == "Renk" || x.Variant == "Beden")
                  .ToList();

            ReceiptPivot receiptPivot = new()
            {
                Colors = variants.Where(c => c.Variant == "Renk").Select(c => c.VariantValue).ToList(),
                Sizes = variants.Where(c => c.Variant == "Beden").Select(c => c.VariantValue).OrderBy(s => s).ToList(),
                ReceiptPivotItems = productInformations
                    .Select(pi => new ReceiptPivotItem
                    {
                        Color = pi
                            .ProductInformationVariants
                            .FirstOrDefault(piv => piv.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name == "Renk")
                            .VariantValue
                            .VariantValueTranslations
                            .FirstOrDefault()
                            .Value,
                        Size = pi
                            .ProductInformationVariants
                            .FirstOrDefault(piv => piv.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name == "Beden")
                            .VariantValue
                            .VariantValueTranslations
                            .FirstOrDefault()
                            .Value,
                        Quantity = receipt.ReceiptItems.FirstOrDefault(ri => ri.ProductInformationId == pi.Id).Quantity
                    })
                    .ToList()
            };
            #endregion

            PdfPage pdfPage = pdfDocument.AddPage();

            XGraphics xGraphics = XGraphics.FromPdfPage(pdfPage, XGraphicsUnit.Millimeter);

            XTextFormatter xTextFormatter = new(xGraphics);

            var x = 10d;
            var y = 10d;

            var pageSizeWidth = xGraphics.PageSize.Width - 20;
            var pageSizeHeight = xGraphics.PageSize.Height - 20;

            #region Header

            var headerRectangleHeight = 20d;
            var headerRectangleWidth = pageSizeWidth / 3;

            var headerFontSize = 6;

            #region Model Code
            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, headerRectangleWidth, headerRectangleHeight);
            xTextFormatter.DrawString("Model Kodu", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            xTextFormatter.DrawString(sellerCode, new XFont("Arial", headerFontSize), XBrushes.Black, new XRect(x + 2, y + 2 + headerRectangleHeight / 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            #endregion

            #region Unit Meter
            x += headerRectangleWidth;

            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, headerRectangleWidth, headerRectangleHeight);
            xTextFormatter.DrawString("Birim Kumaş Metrajı", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            xTextFormatter.DrawString($"{receipt.UnitMeter} M", new XFont("Arial", headerFontSize), XBrushes.Black, new XRect(x + 2, y + 2 + headerRectangleHeight / 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            #endregion

            #region Date
            x += headerRectangleWidth;

            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, headerRectangleWidth, headerRectangleHeight);
            xTextFormatter.DrawString("Tarih", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            xTextFormatter.DrawString(DateTime.Now.ToString("dd.MM.yyyy"), new XFont("Arial", headerFontSize), XBrushes.Black, new XRect(x + 2, y + 2 + headerRectangleHeight / 2, headerRectangleWidth - 2, (headerRectangleHeight / 2) - 2));
            #endregion

            #endregion

            #region Pivot

            x = 10d;

            var pivotFontSize = 5;

            #region Images

            var imageRectangleHeight = 15d;
            var imageRectangleWidth = 10d;

            y = headerRectangleHeight + imageRectangleHeight + 10 + 10;

            for (int i = 0; i < receiptPivot.Colors.Count; i++)
            {
                var color = receiptPivot.Colors[i];

                var productInformation = productInformations
                    .First(pi => pi
                        .ProductInformationVariants
                        .Any(piv => piv
                            .VariantValue
                            .VariantValueTranslations
                            .Any(vvt => vvt.Value == color)));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, imageRectangleWidth, imageRectangleHeight);

                try
                {
                    Stream stream = null;
                    using (HttpClient httpClient = new())
                    {
                        stream = await httpClient.GetStreamAsync(productInformation.ProductInformationPhotos[0].FileName);
                    }

                    xGraphics.DrawImage(XImage.FromStream(stream), new XRect(x, y, imageRectangleWidth, imageRectangleHeight));
                }
                catch
                {
                }

                y += imageRectangleHeight;
            }

            #endregion

            #region Colors

            x = 10 + imageRectangleWidth;

            var colorRectangleHeight = 15d;
            var colorRectangleWidth = 50d;

            y = headerRectangleHeight + colorRectangleHeight + 10 + 10;

            for (int i = 0; i < receiptPivot.Colors.Count; i++)
            {
                var color = receiptPivot.Colors[i];

                xTextFormatter.DrawString(color, new XFont("Arial", pivotFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, colorRectangleWidth - 2, colorRectangleHeight - 2));
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, colorRectangleWidth, colorRectangleHeight);

                y += colorRectangleHeight;
            }

            #endregion

            #region Sizes

            x = 10 + imageRectangleWidth + colorRectangleWidth;
            y = headerRectangleHeight + 10 + 10;

            var sizeRectangleHeight = 15d;
            var sizeRectangleWidth = 15d;

            for (int i = 0; i < receiptPivot.Sizes.Count; i++)
            {
                var size = receiptPivot.Sizes[i];

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, sizeRectangleWidth, sizeRectangleHeight);
                xTextFormatter.DrawString(
                    size,
                    new XFont("Arial", pivotFontSize, XFontStyle.Bold),
                    XBrushes.Black,
                    new XRect(x + 2, y + 2, sizeRectangleWidth - 2, sizeRectangleHeight - 2));

                x += sizeRectangleWidth;
            }

            #region Color Total Meter Header

            var colorTotalMeterHeaderWidth = 20d;

            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, colorTotalMeterHeaderWidth, sizeRectangleHeight);
            xTextFormatter.DrawString("Renk Bazlı Kumaş Metrajı", new XFont("Arial", 3), XBrushes.Black, new XRect(x + 2, y + 2, colorTotalMeterHeaderWidth - 2, sizeRectangleHeight - 2));

            #endregion

            #endregion

            #region Pivot Items

            x = 10 + imageRectangleWidth + colorRectangleWidth;
            y = headerRectangleHeight + sizeRectangleHeight + 10 + 10;

            for (int i = 0; i < receiptPivot.Colors.Count; i++)
            {
                x = 10 + imageRectangleWidth + colorRectangleWidth;

                if (i != 0)
                    y += colorRectangleHeight;

                var color = receiptPivot.Colors[i];

                for (int h = 0; h < receiptPivot.Sizes.Count; h++)
                {
                    if (h != 0)
                        x += sizeRectangleWidth;

                    var size = receiptPivot.Sizes[h];

                    var receiptPivotItem = receiptPivot.ReceiptPivotItems.FirstOrDefault(rpi => rpi.Size == size && rpi.Color == color);

                    var value = "X";

                    if (receiptPivotItem != null)
                        value = receiptPivotItem.Quantity.ToString();

                    var xBrush = value == "X" ? XBrushes.Red : XBrushes.Black;

                    xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, sizeRectangleWidth, sizeRectangleHeight);
                    xTextFormatter.DrawString(value, new XFont("Arial", pivotFontSize), xBrush, new XRect(x + 2, y + 2, sizeRectangleWidth - 2, sizeRectangleHeight - 2));
                }
            }

            #region Color Total Meter

            y = headerRectangleHeight + sizeRectangleHeight + 10 + 10;

            var colorTotalFontSize = 4;
            var colorTotalMeterWidth = 20d;

            for (int i = 0; i < receiptPivot.Colors.Count; i++)
            {
                x = 10 + imageRectangleWidth + colorRectangleWidth + (receiptPivot.Sizes.Count * sizeRectangleWidth);

                if (i != 0)
                    y += colorRectangleHeight;

                var color = receiptPivot.Colors[i];

                var colorQuantity = receiptPivot.ReceiptPivotItems.Where(rpi => rpi.Color == color).Sum(rpi => rpi.Quantity);
                var colorTotalMeter = colorQuantity * receipt.UnitMeter;
                var colorTotalMeterString = "0";
                if (colorTotalMeter > 0)
                    colorTotalMeterString = colorTotalMeter.ToString("N2");

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, colorTotalMeterWidth, sizeRectangleHeight);
                xTextFormatter.DrawString($"{colorTotalMeterString} M", new XFont("Arial", colorTotalFontSize), XBrushes.Black, new XRect(x + 2, y + 2, colorTotalMeterWidth - 2, sizeRectangleHeight - 2));
            }

            #endregion

            y += sizeRectangleHeight;

            #endregion

            #endregion

            #region Footer

            x = 10d;
            y += 10;

            var footerRectangleHeight = 20d;
            var footerRectangleWidth = xGraphics.PageSize.Width / 3;

            #region Total Quantity
            var totalQuantity = receiptPivot.ReceiptPivotItems.Sum(rpi => rpi.Quantity);

            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, footerRectangleWidth, footerRectangleHeight);
            xTextFormatter.DrawString("Toplam Ürün Adedi", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, footerRectangleWidth - 2, (footerRectangleHeight / 2) - 2));
            xTextFormatter.DrawString(totalQuantity.ToString(), new XFont("Arial", headerFontSize), XBrushes.Black, new XRect(x + 2, y + 2 + footerRectangleHeight / 2, footerRectangleWidth - 2, (footerRectangleHeight / 2) - 2));
            #endregion

            #region Total Meter
            x += footerRectangleWidth;

            var totalMeter = totalQuantity * receipt.UnitMeter;
            var totalMeterString = "0";
            if (totalMeter > 0)
                totalMeterString = totalMeter.ToString("N2");

            xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, footerRectangleWidth, footerRectangleHeight);
            xTextFormatter.DrawString("Toplam Kumaş Metrajı", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, footerRectangleWidth - 2, (footerRectangleHeight / 2) - 2));
            xTextFormatter.DrawString($"{totalMeterString} M", new XFont("Arial", headerFontSize), XBrushes.Black, new XRect(x + 2, y + 2 + footerRectangleHeight / 2, footerRectangleWidth - 2, (footerRectangleHeight / 2) - 2));
            #endregion

            y += footerRectangleHeight + 10;

            #endregion

            #region Infos

            x = 10d;

            var infoFontSize = 4;
            var infoRectangleHeight = 6d;
            var infoRectangleLabelWidth = 40d;
            var infoRectangleValueWidth = (pageSizeWidth - infoRectangleLabelWidth) - 50;

            if (string.IsNullOrEmpty(receipt.Card) == false)
            {
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, infoRectangleLabelWidth, infoRectangleHeight);
                xTextFormatter.DrawString($"Kart", new XFont("Arial", infoFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 1, y + 1, infoRectangleLabelWidth - 1, infoRectangleHeight - 1));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x + infoRectangleLabelWidth, y, infoRectangleValueWidth, infoRectangleHeight);
                xTextFormatter.DrawString(receipt.Card, new XFont("Arial", infoFontSize), XBrushes.Black, new XRect(x + infoRectangleLabelWidth + 1, y + 1, infoRectangleValueWidth - 1, infoRectangleHeight - 1));

                y += infoRectangleHeight;
            }

            if (string.IsNullOrEmpty(receipt.Ticket) == false)
            {
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, infoRectangleLabelWidth, infoRectangleHeight);
                xTextFormatter.DrawString($"Ense Etiketi", new XFont("Arial", infoFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 1, y + 1, infoRectangleLabelWidth - 1, infoRectangleHeight - 1));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x + infoRectangleLabelWidth, y, infoRectangleValueWidth, infoRectangleHeight);
                xTextFormatter.DrawString(receipt.Ticket, new XFont("Arial", infoFontSize), XBrushes.Black, new XRect(x + infoRectangleLabelWidth + 1, y + 1, infoRectangleValueWidth - 1, infoRectangleHeight - 1));

                y += infoRectangleHeight;
            }

            if (string.IsNullOrEmpty(receipt.Rubber) == false)
            {
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, infoRectangleLabelWidth, infoRectangleHeight);
                xTextFormatter.DrawString($"Şeffaf Lastik", new XFont("Arial", infoFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 1, y + 1, infoRectangleLabelWidth - 1, infoRectangleHeight - 1));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x + infoRectangleLabelWidth, y, infoRectangleValueWidth, infoRectangleHeight);
                xTextFormatter.DrawString(receipt.Rubber, new XFont("Arial", infoFontSize), XBrushes.Black, new XRect(x + infoRectangleLabelWidth + 1, y + 1, infoRectangleValueWidth - 1, infoRectangleHeight - 1));

                y += infoRectangleHeight;
            }

            if (string.IsNullOrEmpty(receipt.Wash) == false)
            {
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, infoRectangleLabelWidth, infoRectangleHeight);
                xTextFormatter.DrawString($"Yıkama Talimatı", new XFont("Arial", infoFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 1, y + 1, infoRectangleLabelWidth - 1, infoRectangleHeight - 1));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x + infoRectangleLabelWidth, y, infoRectangleValueWidth, infoRectangleHeight);
                xTextFormatter.DrawString(receipt.Wash, new XFont("Arial", infoFontSize), XBrushes.Black, new XRect(x + infoRectangleLabelWidth + 1, y + 1, infoRectangleValueWidth - 1, infoRectangleHeight - 1));

                y += infoRectangleHeight;
            }

            if (string.IsNullOrEmpty(receipt.Hanger) == false)
            {
                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, infoRectangleLabelWidth, infoRectangleHeight);
                xTextFormatter.DrawString($"Askı", new XFont("Arial", infoFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 1, y + 1, infoRectangleLabelWidth - 1, infoRectangleHeight - 1));

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x + infoRectangleLabelWidth, y, infoRectangleValueWidth, infoRectangleHeight);
                xTextFormatter.DrawString(receipt.Hanger, new XFont("Arial", infoFontSize), XBrushes.Black, new XRect(x + infoRectangleLabelWidth + 1, y + 1, infoRectangleValueWidth - 1, infoRectangleHeight - 1));

            }

            if (string.IsNullOrEmpty(receipt.Hanger) == false ||
                string.IsNullOrEmpty(receipt.Wash) == false ||
                string.IsNullOrEmpty(receipt.Rubber) == false ||
                string.IsNullOrEmpty(receipt.Ticket) == false ||
                string.IsNullOrEmpty(receipt.Card) == false)
            {
                y += 15;
            }

            #endregion

            #region Note

            x = 10d;

            if (string.IsNullOrEmpty(receipt.Note) == false)
            {
                double noteWidth = pageSizeWidth;
                double noteHeight = 30;

                xGraphics.DrawRectangle(new XPen(XColors.Black, 0.5), x, y, noteWidth, noteHeight);

                xTextFormatter.DrawString("Not", new XFont("Arial", headerFontSize, XFontStyle.Bold), XBrushes.Black, new XRect(x + 2, y + 2, pageSizeWidth - 2, noteHeight - 2));

                y += 10;

                xTextFormatter.DrawString(
                    receipt.Note,
                    new XFont("Arial", headerFontSize),
                    XBrushes.Black,
                    new XRect(x + 2, y + 2, pageSizeWidth - 2, noteHeight - 2),
                    new XStringFormat());
            }

            #endregion
        }

        MemoryStream memoryStream = new();

        pdfDocument.Save(memoryStream);

        memoryStream.Position = 0;

        return File(memoryStream, "application/pdf", $"{sellerCode}-{DateTime.Now:ddMMyyyy}.pdf");
    }

    [HttpPost("/Receipt/Insert")]
    public JsonResult Insert(Receipt receipt)
    {
        var productInformationsResponse = this
            ._productInformationService
            .Read(new List<int>
            {
                receipt.ReceiptItems.First().ProductInformationId
            });

        receipt.SellerCode = productInformationsResponse.Data.First().Product.SellerCode;

        return Json(this._service.Create(receipt));
    }

    [HttpGet("/Receipt/Update")]
    public IActionResult Update(int id)
    {
        return PartialView(this._service.ReadDetail(id));
    }

    [HttpPost("/Receipt/UpdateDelivered")]
    public JsonResult UpdateDelivered([FromBody] UpdateDeliveredModel model)
    {
        return Json(this._service.UpdateDelivered(model.Id, model.Delivered));
    }

    [HttpPost("/Receipt/UpdateCancelled")]
    public JsonResult UpdateCancelled([FromBody] UpdateCancelledModel model)
    {
        return Json(this._service.UpdateCancelled(model.Id));
    }

    [Authorize(Roles = "manager")]
    public IActionResult Index()
    {
        var viewModel = new Application.Panel.ViewModels.Receipts.Index
        {
            Suppliers = this._supplierService.ReadAsKeyValue().Data
        };
        return View(viewModel);
    }

    [Authorize(Roles = "manager")]
    public JsonResult Read(PagedRequest pagedRequest)
    {
        return Json(this._service.Read(pagedRequest));
    }
    #endregion
}