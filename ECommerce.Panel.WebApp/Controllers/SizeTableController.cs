﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers;

//[Authorize(Roles = "manager")]
public class SizeTableController : Base.ControllerBase
{
    #region Fields
    private readonly IProductService _productService;

    private readonly ApplicationDbContext _context;
    #endregion

    #region Constructors
    public SizeTableController(
        IDomainService domainService,
        IDomainFinder domainFinder,
        ICompanyFinder companyFinder,
        INotificationService notificationService,
        ICookieHelper cookieHelper,
        IProductService productService,
        ApplicationDbContext context) : base(
            domainService,
            domainFinder,
            companyFinder,
            notificationService,
            cookieHelper)
    {
        #region Fields
        this._productService = productService;
        this._context = context;
        #endregion
    }
    #endregion

    #region Actions
    [Authorize(Roles = "manager,customerservice")]
    [HttpGet]
    public IActionResult Get(int id)
    {
        var sizes = _productService.ReadAllSizes(id).Data;

        var sizeTable = this._context.SizeTables.AsNoTracking().Include(st => st.SizeTableItems).FirstOrDefault(st => st.ProductId == id);
        if (sizeTable is null)
        {
            sizeTable = new SizeTable()
            {
                ProductId = id,
                SizeTableItems = sizes.Select(s => new SizeTableItem
                {
                    Size = s
                }).ToList()
            };
        }
        else
        {
            foreach (var theSize in sizes)
            {
                if (sizeTable.SizeTableItems.Any(sti => sti.Size == theSize) == false)
                {
                    sizeTable.SizeTableItems.Add(new SizeTableItem
                    {
                        Size = theSize
                    });
                }
            }
        }

        return PartialView(sizeTable);
    }

    [Authorize(Roles = "manager")]
    [HttpPost]
    public JsonResult Post([FromBody] SizeTable sizeTable)
    {
        if (sizeTable.Id == 0)
        {
            this._context.SizeTables.Add(sizeTable);
        }
        else
        {
            this._context.SizeTables.Update(sizeTable);
        }

        this._context.SaveChanges();

        return Json(new { success = true });
    }
    #endregion
}
