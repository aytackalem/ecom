﻿using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class DistrictController : Controller
    {
        #region Fields
        private readonly IDistrictService _districtService;
        #endregion

        #region Constructors
        public DistrictController(IDistrictService districtService)
        {
            #region Fields
            this._districtService = districtService;
            #endregion
        }
        #endregion

        #region Methods
        public JsonResult Read(int cityId)
        {
            return Json(this._districtService.ReadByCityId(cityId));
        }
        #endregion
    }
}
