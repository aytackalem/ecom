﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Customers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class CustomerController : Base.ControllerBase
    {
        #region Fields
        private readonly ICustomerService _service;
        #endregion

        #region Constructors
        public CustomerController( ICustomerService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public JsonResult Search([FromBody] SearchRequest searchRequest)
        {
            return Json(this._service.Search(searchRequest));
        }

        [HttpGet]
        public JsonResult Read(int id)
        {
            return Json(this._service.Read(id));
        }
        #endregion
    }
}
