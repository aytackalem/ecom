﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class VariantValueController : Base.ControllerBase
    {
        #region Fields
        private readonly IVariantValueService _service;

        private readonly ILanguageService _languageService;
        #endregion

        #region Constructors
        public VariantValueController(IVariantValueService service, ILanguageService languageService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            this._languageService = languageService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public IActionResult IndexPartial([FromBody] ECommerce.Application.Panel.Parameters.VariantValues.IndexPartialRequest indexPartialRequest)
        {
            return PartialView(new ECommerce.Application.Panel.ViewModels.VariantValues.IndexPartial
            {
                Index = indexPartialRequest.Index,
                Languages = this._languageService.ReadAsKeyValue().Data,
                VariantValue = indexPartialRequest.VariantValue
            });
        }
        #endregion
    }
}