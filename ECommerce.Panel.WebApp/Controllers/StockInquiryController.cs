﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager,sales")]
    public class StockInquiryController : Base.ControllerBase
    {
        #region Fields
        private readonly IProductService _service;
        #endregion

        #region Constructors
        public StockInquiryController(
            IDomainService domainService,
            IDomainFinder domainFinder,
            ICompanyFinder companyFinder,
            INotificationService notificationService,
            ICookieHelper cookieHelper,
            IProductService service) : base(
                domainService,
                domainFinder,
                companyFinder,
                notificationService,
                cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Get(string modelCode)
        {
            var response = this._service.Read(modelCode);

            return PartialView(response);
        }
        #endregion
    }
}
