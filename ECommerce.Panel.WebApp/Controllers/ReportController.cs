﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Reports;
using ECommerce.Application.Panel.ViewModels.Reports;
using ECommerce.Persistence.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ReportController : Base.ControllerBase
    {
        #region Fields
        private readonly IReportService _reportService;

        private readonly ITenantFinder _tenantFinder;

        private readonly IRazorViewEngine _razorViewEngine;

        private readonly ITempDataProvider _tempDataProvider;

        private readonly IServiceProvider _serviceProvider;

        private readonly IMarketplaceService _marketplaceService;

        private readonly IProductService _productService;

        private readonly ISupplierService _supplierService;

        private readonly IReceiptService _receiptService;

        private readonly IProductCategorizationService _productCategorizationService;

        private readonly IProductGenericPropertyService _productGenericPropertyService;

        private readonly ICategoryService _categoryService;

        private readonly ApplicationDbContext _context;
        #endregion

        #region Constructors
        public ReportController(IReportService reportService, IDomainService domainService, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, IRazorViewEngine razorViewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider, ICookieHelper cookieHelper, IMarketplaceService marketplaceService, IProductService productService, ISupplierService supplierService, IReceiptService receiptService, IProductCategorizationService productCategorizationService, IProductGenericPropertyService productGenericPropertyService, ICategoryService categoryService, ApplicationDbContext context) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._reportService = reportService;
            this._tenantFinder = tenantFinder;
            this._razorViewEngine = razorViewEngine;
            this._tempDataProvider = tempDataProvider;
            this._serviceProvider = serviceProvider;
            this._marketplaceService = marketplaceService;
            _productService = productService;
            _supplierService = supplierService;
            _receiptService = receiptService;
            _productCategorizationService = productCategorizationService;
            _productGenericPropertyService = productGenericPropertyService;
            _categoryService = categoryService;
            _context = context;
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sds">(Start Date String) Başlangıç tarihi</param>
        /// <param name="eds">(End Date String) Bitiş tarihi</param>
        /// <returns></returns>
        [Authorize(Roles = "manager")]
        public IActionResult Index()
        {
            var readResponse = this._marketplaceService.Read();
            if (readResponse.Success)
                return View(readResponse.Data.ToDictionary(d => d.Id, d => d.Name));

            return View();
        }

        [Authorize(Roles = "manager")]
        public async Task<JsonResult> IndexPartials(string sds, string eds, string marketplaceId, int minQuantity, string sorting, string asc)
        {
            System.Globalization.CultureInfo culture = new("tr-TR");

            var request = new ReportRequest
            {
                StartDate = string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                EndDate = string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                TenantId = this._tenantFinder.FindId(),
                DomainId = this._domainFinder.FindId(),
                CompanyId = this._companyFinder.FindId(),
                MarketplaceId = marketplaceId,
                MinQuantity = minQuantity
            };

            var headerReport = this._reportService.ReadHeaderReport(request).Data;
            var expenseReport = this._reportService.ReadExpenseReport(request).Data;
            var headerViewModel = new Application.Panel.ViewModels.Reports.Header
            {
                TotalSaleAmount = headerReport.TotalSaleAmount,
                TotalCommissionAmount = headerReport.TotalCommissionAmount,
                TotalProductCostAmount = headerReport.TotalProductCostAmount,
                TotalShippingCostAmount = headerReport.TotalShippingCostAmount,
                TotalExpenseAmount = expenseReport.TotalExpenseAmount,
                OtherExpensesTotalAmount = expenseReport.OtherExpensesTotalAmount,
                TotalAmountProductBasedExpenses = expenseReport.TotalAmountProductBasedExpenses,
            };

            var orderReport = this._reportService.ReadDetailedOrderReport(request).Data;
            orderReport.ExpenseAmount = expenseReport.TotalExpenseAmount;
            var orderProductReports = this._reportService.ReadOrderProductReport(request).Data;

            switch (sorting)
            {
                case "Satılan Adet":
                    orderProductReports = (asc == "asc" ? orderProductReports.OrderBy(d => d.Quantity) : orderProductReports.OrderByDescending(d => d.Quantity)).ToList();
                    break;
                case "Kalan Adet":
                    orderProductReports = (asc == "asc" ? orderProductReports.OrderBy(d => d.Stock) : orderProductReports.OrderByDescending(d => d.Stock)).ToList();
                    break;
                case "Model Kodu":
                    orderProductReports = (asc == "asc" ? orderProductReports.OrderBy(d => d.SellerCode) : orderProductReports.OrderByDescending(d => d.SellerCode)).ToList();
                    break;

            }


            var indexViewModel = new Application.Panel.ViewModels.Reports.Index
            {
                StartDateString = request.StartDate.ToString("dd-MM-yyyy"),
                EndDateString = request.EndDate.ToString("dd-MM-yyyy"),
                OrderReport = orderReport,
                OrderProductReports = orderProductReports
            };

            return Json(new
            {
                OrderReport = await this.RenderToStringAsync("Report/OrderReportPartial", indexViewModel),
                OrderProductReport = await this.RenderToStringAsync("Report/OrderProductReportPartial", indexViewModel),
                Header = await this.RenderToStringAsync("Report/HeaderPartial", headerViewModel),
                Sorting = sorting,
                Asc = asc
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sds">(Start Date String) Başlangıç tarihi</param>
        /// <param name="eds">(End Date String) Bitiş tarihi</param>
        /// <returns></returns>
        [Authorize(Roles = "manager")]
        public IActionResult ProductBasedSalesReport()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        public async Task<JsonResult> ProductBasedSalesReportPartials(string sds, string eds, string sorting, string asc)
        {
            System.Globalization.CultureInfo culture = new("tr-TR");

            var request = new ReportRequest
            {
                StartDate = string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                EndDate = string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                TenantId = this._tenantFinder.FindId(),
                DomainId = this._domainFinder.FindId(),
                CompanyId = this._companyFinder.FindId()
            };

            var productBasedSalesReport = this._reportService.ReadProductBasedSalesReport(request).Data;
            switch (sorting)
            {
                case "Adet":
                    productBasedSalesReport = (asc == "asc" ? productBasedSalesReport.OrderBy(d => d.Quantity) : productBasedSalesReport.OrderByDescending(d => d.Quantity)).ToList();
                    break;
                case "Karlılık Oranı":
                    productBasedSalesReport = (asc == "asc" ? productBasedSalesReport.OrderBy(d => d.ProfitRate) : productBasedSalesReport.OrderByDescending(d => d.ProfitRate)).ToList();
                    break;
                case "Brüt Kar":
                    productBasedSalesReport = (asc == "asc" ? productBasedSalesReport.OrderBy(d => d.Profit) : productBasedSalesReport.OrderByDescending(d => d.Profit)).ToList();
                    break;
                case "Ciro":
                    productBasedSalesReport = (asc == "asc" ? productBasedSalesReport.OrderBy(d => d.TotalSalesAmount) : productBasedSalesReport.OrderByDescending(d => d.TotalSalesAmount)).ToList();
                    break;
                case "Toplam Gider":
                    productBasedSalesReport = (asc == "asc" ? productBasedSalesReport.OrderBy(d => d.ExpenseAmount) : productBasedSalesReport.OrderByDescending(d => d.ExpenseAmount)).ToList();
                    break;
            }
            var productBasedSalesReportViewModel = new Application.Panel.ViewModels.Reports.ProductBasedSalesReport
            {
                StartDateString = request.StartDate.ToString("dd-MM-yyyy"),
                EndDateString = request.EndDate.ToString("dd-MM-yyyy"),
                ProductBasedSalesReports = productBasedSalesReport
            };
            var headerReport = this._reportService.ReadHeaderReport(request).Data;
            var expenseReport = this._reportService.ReadExpenseReport(request).Data;
            var headerViewModel = new Application.Panel.ViewModels.Reports.Header
            {
                TotalSaleAmount = headerReport.TotalSaleAmount,
                TotalCommissionAmount = headerReport.TotalCommissionAmount,
                TotalProductCostAmount = headerReport.TotalProductCostAmount,
                TotalShippingCostAmount = headerReport.TotalShippingCostAmount,
                TotalExpenseAmount = expenseReport.TotalExpenseAmount,
                OtherExpensesTotalAmount = expenseReport.OtherExpensesTotalAmount,
                TotalAmountProductBasedExpenses = expenseReport.TotalAmountProductBasedExpenses,
            };

            return Json(new
            {
                ProductBasedHeader = await this.RenderToStringAsync("Report/ProductBasedHeaderPartial", productBasedSalesReportViewModel),
                ProductBasedSalesReport = await this.RenderToStringAsync("Report/ProductBasedSalesReportPartial", productBasedSalesReportViewModel),
                Header = await this.RenderToStringAsync("Report/HeaderPartial", headerViewModel),
                Sorting = sorting,
                Asc = asc
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sds">(Start Date String) Başlangıç tarihi</param>
        /// <param name="eds">(End Date String) Bitiş tarihi</param>
        /// <returns></returns>
        [Authorize(Roles = "manager")]
        public IActionResult PackagingReport()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        public IActionResult PackagingReportPartial(string sds, string eds)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("tr-TR");

            var indexRequest = new PackagingReportRequest
            {
                StartDate = string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                EndDate = string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                TenantId = this._tenantFinder.FindId(),
                DomainId = this._domainFinder.FindId(),
                CompanyId = this._companyFinder.FindId()
            };

            return PartialView(new Application.Panel.ViewModels.Reports.PackagingReport
            {
                StartDateString = indexRequest.StartDate.ToString("dd-MM-yyyy"),
                EndDateString = indexRequest.EndDate.ToString("dd-MM-yyyy"),
                Datas = this._reportService.ReadPackagingReport(indexRequest).Data
            });
        }

        [Authorize(Roles = "manager")]
        public IActionResult SoldTogetherReport()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        public IActionResult SoldTogetherReportPartial(string sds, string eds, int top)
        {
            System.Globalization.CultureInfo culture = new("tr-TR");

            var request = new SoldTogetherReportRequest
            {
                StartDate = string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                EndDate = string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                Top = top,
                TenantId = this._tenantFinder.FindId(),
                DomainId = this._domainFinder.FindId(),
                CompanyId = this._companyFinder.FindId()
            };

            return PartialView(new Application.Panel.ViewModels.Reports.SoldTogetherReport
            {
                StartDateString = request.StartDate.ToString("dd-MM-yyyy"),
                EndDateString = request.EndDate.ToString("dd-MM-yyyy"),
                Datas = this._reportService.ReadSoldTogetherReport(request).Data
            });
        }

        [Authorize(Roles = "manager")]
        public IActionResult ProductBasedStockReport(string sds, string eds, int productId, string skuCode, bool hasReceipt)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("tr-TR");

            var request = new ProductBasedStockReportRequest
            {
                StartDate = string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                EndDate = string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                ProductId = productId,
                SkuCode = skuCode
            };

            var sellerCodeData = _productService.ReadSellerCode(productId);

            var data = this._reportService.ReadProductBasedStockReport(request).Data;

            if (productId > 0)
            {
                var receiptData = this._receiptService.ProductInformationQuantity(productId);
                if (receiptData.Success)
                {
                    data.ForEach(d =>
                    {
                        if (receiptData.Data.Any(r => r.Id == d.ProductInformationId))
                        {

                            d.ReceiptDescription = string.Join(" ", receiptData.Data.Where(r => r.Id == d.ProductInformationId).Select(r => $"{r.Name} {r.Quantity}"));

                        }
                    });
                }
            }

            return PartialView(new Application.Panel.ViewModels.Reports.ProductBasedStockReport
            {
                HasReceipt = hasReceipt,
                ReceiptFileName = $"{sellerCodeData.Data}-{DateTime.Now:ddMMyyyy}.pdf",
                Datas = data
            });
        }

        [Authorize(Roles = "manager")]
        public IActionResult PickedReport()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        public IActionResult PickedReportPartial(string sds, string eds)
        {
            var culture = new System.Globalization.CultureInfo("tr-TR");

            var request = new PickedReportRequest(
                string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture));

            return PartialView(
                new Application.Panel.ViewModels.Reports.PickedReport(
                    request.StartDate.ToString("dd-MM-yyyy"),
                    request.EndDate.ToString("dd-MM-yyyy"),
                    this._reportService.ReadPickedReport(request).Data));
        }

        [Authorize(Roles = "manager")]
        public IActionResult ColorBasedSalesReport()
        {
            //var productCategorizations = this._productCategorizationService.ReadAsKeyValue();
            //var productGenericProperties = this._productGenericPropertyService.ReadAsValue("Marka");

            var readResponse = this._marketplaceService.Read();
            if (readResponse.Success)
                return View(
                    new ColorBasedSalesReport(
                        readResponse.Data.ToDictionary(d => d.Id, d => d.Name),
                        this._categoryService.ReadAsKeyValue().Data.OrderBy(_ => _.Value).ToList()));

            return View();
        }

        [Authorize(Roles = "manager")]
        public IActionResult ColorBasedSalesReportPartial(string sds, string eds, int categoryId, string? sellerCode, string? marketplaceId, string? skuCode)
        {
            var culture = new System.Globalization.CultureInfo("tr-TR");

            var request = new ColorBasedSalesReportRequest(
                string.IsNullOrEmpty(sds) ? DateTime.Now.Date : DateTime.Parse(sds, culture),
                string.IsNullOrEmpty(eds) ? DateTime.Now.Date : DateTime.Parse(eds, culture),
                sellerCode,
                marketplaceId,
                skuCode,
                categoryId);

            var data = this._reportService.ReadColorBasedSalesReport(request).Data;

            var salesPerformanceData = _reportService.ReadSalesPerformance();
            if (salesPerformanceData.Success)
            {
                foreach (var item in data.Items)
                {
                    var sp = salesPerformanceData.Data.FirstOrDefault(sp => sp.SkuCode == item.SkuCode && sp.Type == "Monthly");
                    item.MonthlyQuantity = sp == null ? 0 : sp.Quantity;

                    sp = salesPerformanceData.Data.FirstOrDefault(sp => sp.SkuCode == item.SkuCode && sp.Type == "Weekly");
                    item.WeeklyQuantity = sp == null ? 0 : sp.Quantity;

                    sp = salesPerformanceData.Data.FirstOrDefault(sp => sp.SkuCode == item.SkuCode && sp.Type == "Daily");
                    item.DailyQuantity = sp == null ? 0 : sp.Quantity;
                }
            }

            var categorizations = data
                .Items
                .GroupBy(i => i.CategorizationsString)
                .OrderBy(_ => _.Key)
                .ToDictionary(i => i.Key, i => $"{i.Count()}");

            var brands = data
                .Items
                .GroupBy(i => i.BrandsString)
                .OrderBy(_ => _.Key)
                .ToDictionary(i => i.Key, i => $"{i.Count()}");

            return PartialView(
                new Application.Panel.ViewModels.Reports.ColorBasedSalesReportPartial(
                    request.StartDate.ToString("dd-MM-yyyy"),
                    request.EndDate.ToString("dd-MM-yyyy"),
                    categorizations,
                    brands,
                    data));
        }

        [Authorize(Roles = "manager")]
        public IActionResult UnsoldProductReport()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public JsonResult UnsoldProductReportData(PagedRequest pagedRequest)
        {
            return Json(this._reportService.ReadUnsoldProduct(pagedRequest));
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult Last36(int productId, string skuCode)
        {
            var vm = new Last36ViewModel();

            vm.Years.Add(DateTime.Now.Year - 2);
            vm.Years.Add(DateTime.Now.Year - 1);
            vm.Years.Add(DateTime.Now.Year);

            for (int i = 1; i <= 12; i++)
            {
                vm.Months.Add(i);
            }

            vm.Items = _productService.Last36MonthSales(skuCode).Data;
            vm.AllItems = _productService.Last36MonthSales(productId).Data;

            return PartialView(vm);
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult StockAlert()
        {
            return View(_reportService.StockAlert().Data);
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult Return()
        {
            return View(_marketplaceService.ReadAsKeyValue().Data);
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public async Task<IActionResult> ReturnPartial(string marketplaceId, string startDate, string endDate, CancellationToken cancellationToken)
        {
            var culture = new System.Globalization.CultureInfo("tr-TR");

            var result = await _context
                .OrderBillings
                .AsNoTracking()
                .Where(ob =>
                    ob.Order.MarketplaceId == marketplaceId &&
                    ob.Order.OrderDate >= DateTime.Parse(startDate, culture) &&
                    ob.Order.OrderDate <= DateTime.Parse(endDate, culture) &&
                    string.IsNullOrEmpty(ob.ReturnDescription) == false)
                .GroupBy(ob => ob.ReturnDescription)
                .ToDictionaryAsync(gob => gob.Key, gob => gob.Count(), cancellationToken);

            return PartialView(result);
        }
        #endregion

        #region Helper Methods
        private async Task<string> RenderToStringAsync(string viewName, object model)
        {
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }
        #endregion
    }
}