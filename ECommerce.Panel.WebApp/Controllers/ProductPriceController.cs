﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class ProductPriceController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplacePriceService _service;

        private readonly IBrandService _brandService;

        private readonly ICategoryService _categoryService;

        private readonly ISettingService _settingService;

        private readonly IProductSourceDomainService _productSourceDomainService;
        #endregion

        #region Constructors
        public ProductPriceController(IMarketplacePriceService service, IProductSourceDomainService productSourceDomainService, IBrandService brandService, ICategoryService categoryService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ISettingService settingService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            this._brandService = brandService;
            this._categoryService = categoryService;
            this._settingService = settingService;
            this._productSourceDomainService = productSourceDomainService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult Index()
        {

            var refleshData = this._service.BulkDelete();


            var vm = new Application.Panel.ViewModels.Products.Index
            {
                IncludeAccounting = _settingService.IncludeAccounting,
                Brands = this._brandService.ReadAsKeyValue().Data,
                ProductSourceDomains = _productSourceDomainService.ReadAsKeyValue().Data,
                AccountingPrices = _service.AccountingPrices().Data,
                Categories = _categoryService.ReadAsKeyValue().Data
            };

            return View(vm);

        }

        [HttpGet]
        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Create()
        {
            var vm = new Application.Panel.ViewModels.ProductPrices.Create
            {
                IncludeAccounting = _settingService.IncludeAccounting,
                MarketplacePrices = this._service.MarketplaceCompanyPrices().Data,
                ActionPrices = this._service.ActionPrices().Data,
                FractionPrices = this._service.FractionPrices().Data,
                AccountingPrices = this._service.AccountingPrices().Data,
            };
            return PartialView(vm);
        }

        [HttpGet]
        public IActionResult Update()
        {
            var vm = new Application.Panel.ViewModels.ProductPrices.Update
            {

                MarketplaceUpdatePrices = _service.MarketplaceUpdatePrices().Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] ProductMarketplaceBulkPrice dto)
        {
            return Json(this._service.Create(dto));
        }

        [HttpPost]
        public JsonResult BulkUpdate()
        {
            return Json(this._service.BulkUpdate());
        }

        [HttpPost]
        public JsonResult BulkDelete()
        {
            return Json(this._service.BulkDelete());
        }

        [HttpPost]
        public JsonResult Delete([FromBody] ProductInformation productInformation)
        {
            return Json(this._service.Delete(productInformation.Id));
        }


        public JsonResult BulkAccountingUpdate([FromBody] ProductMarketplaceBulkAccountingPrice dto)
        {
            return Json(this._service.BulkAccountingUpdate(dto));

        }

        public JsonResult BulkProductMarketplaceUpdatePrice([FromBody] ProductMarketplaceBulkProductPrice dto)
        {
            return Json(this._service.BulkProductMarketplaceUpdatePrice(dto));
        }

        [HttpGet]
        public IActionResult ProductUpdatePricePartial(string search)
        {
            var vm = new Application.Panel.ViewModels.ProductPrices.ProductUpdatePrice
            {
                UpdateProductPrices = _service.ProductUpdatePrices().Data,
                MarketplaceUpdateProductPrices = _service.MarketplaceUpdateProductPrices(search).Data
            };
            return PartialView(vm);
        }

        [HttpGet]
        public IActionResult AccountingPricePartial(string search)
        {
            var vm = new Application.Panel.ViewModels.ProductPrices.AccountingPrice
            {
                AccountingPrices = _service.AccountingPrices().Data,
                MarketplaceAccountingPrices = _service.MarketplaceAccountingPrices(search).Data
            };
            return PartialView(vm);
        }


        [HttpGet]
        public FileResult ExcelDownload(string search)
        {
            return File(this._service.DownloadMarketplace(search).Data, "application/vnd.ms-excel", "data.xlsx");
        }

        [HttpPost]
        public JsonResult ExcelUpload()
        {
            return Json(this._service.UpdateBulkMarketplace(Request.Form.Files[0]));
        }


        [HttpGet]
        public IActionResult UploadExcelPartial()
        {

            return PartialView();
        }
        #endregion
    }
}
