﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class BulkMarketplaceUpdateController : Base.ControllerBase
    {
        #region Fields
        private readonly IProductService _service;

        private readonly ICategoryService _categoryService;
        #endregion

        #region Constructors
        public BulkMarketplaceUpdateController(IProductService service, ICategoryService categoryService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            this._categoryService = categoryService;
            #endregion
        }
        #endregion

        #region Methods
        public PartialViewResult Index()
        {
            Application.Panel.ViewModels.BulkMarketplaceUpdates.Index vm = new();

            return PartialView(vm);
        }

        [HttpGet]
        public FileResult Download()
        {
            return File(this._service.DownloadMarketplace().Data, "application/vnd.ms-excel", "data.xlsx");
        }

        [HttpPost]
        public JsonResult Update()
        {
            return Json(this._service.UpdateBulkMarketplace(Request.Form.Files[0]));
        }
        #endregion
    }
}
