﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.Payment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class LicenseCheck3dController : Controller
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly LicenseIyzicoPos _licenseIyzicoPos;
        #endregion

        #region Constructors
        public LicenseCheck3dController(IUnitOfWork unitOfWork, LicenseIyzicoPos licenseIyzicoPos)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._licenseIyzicoPos = licenseIyzicoPos;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var r = this._licenseIyzicoPos.Check3d();
            if (r.Success)
            {
                #region Tenant Payment
                var tenantPayment = this
                            ._unitOfWork
                            .TenantPaymentRepository
                            .DbSet()
                            .FirstOrDefault(tp => tp.Id == r.Data.PaymentId);

                tenantPayment.Paid = true;

                this._unitOfWork.TenantPaymentRepository.Update(tenantPayment);
                #endregion

                #region Tenant
                var tenant = this._unitOfWork.TenantRepository.DbSet().First(t => t.Id == tenantPayment.TenantId);

                var expirationDate = tenant.ExpirationDate;

                if (expirationDate < DateTime.Now)
                    expirationDate = DateTime.Now;

                expirationDate = tenantPayment.Code switch
                {
                    "M" => expirationDate.AddMonths(1),
                    "HY" => expirationDate.AddMonths(6),
                    "Y" => expirationDate.AddMonths(12),
                    _ => throw new Exception(),
                };

                tenant.ExpirationDate = expirationDate;

                this._unitOfWork.TenantRepository.Update(tenant); 
                #endregion
            }

            return View(new ECommerce.Application.Panel.ViewModels.LicenseCheck3ds.Index
            {
                Success = r.Success,
                Message = r.Message
            });
        }
        #endregion
    }
}
