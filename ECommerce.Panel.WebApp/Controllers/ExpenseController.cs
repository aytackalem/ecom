﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ExpenseController : Base.ControllerBase
    {
        #region Fields
        private readonly IExpenseService _service;

        private readonly IExpenseSourceService _expenseSourceService;

        private readonly IOrderSourceService _orderSourceService;

        private readonly IMarketplaceService _marketPlaceService;

        private readonly IProductInformationService _productInformationService;
        #endregion

        #region Constructors
        public ExpenseController( IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, IExpenseService service, IExpenseSourceService expenseSourceService, IOrderSourceService orderSourceService, IMarketplaceService marketPlaceService, IProductInformationService productInformationService, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._expenseSourceService = expenseSourceService;
            this._orderSourceService = orderSourceService;
            this._marketPlaceService = marketPlaceService;
            this._productInformationService = productInformationService;
            #endregion
        }
        #endregion

        #region Methods
        [Authorize(Roles = "manager")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.Expenses.Create
            {
                ExpenseSources = this._expenseSourceService.ReadAsKeyValue().Data,
                OrderSources = this._orderSourceService.ReadAsKeyValue().Data,
                Marketplaces = this._marketPlaceService.ReadAsKeyValue().Data,
                ProductInformations = this._productInformationService.ReadAsKeyValue().Data
            });
        }

        [Authorize(Roles = "manager")]
        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.Expenses.Update
            {
                Expense = this._service.Read(id).Data,
                ExpenseSources = this._expenseSourceService.ReadAsKeyValue().Data,
                OrderSources = this._orderSourceService.ReadAsKeyValue().Data,
                Marketplaces = this._marketPlaceService.ReadAsKeyValue().Data,
                ProductInformations = this._productInformationService.ReadAsKeyValue().Data
            });
        }

        [Authorize(Roles = "manager")]
        [HttpPost]
        public JsonResult Update([FromBody] Expense expense)
        {
            return Json(this._service.Update(expense));
        }

        [Authorize(Roles = "manager")]
        [HttpPost]
        public JsonResult Create([FromBody] Expense expense)
        {
            return Json(this._service.Create(expense));
        }
        #endregion
    }
}
