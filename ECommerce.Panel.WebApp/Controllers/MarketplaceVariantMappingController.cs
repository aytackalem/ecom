﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceVariantMappingController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplaceVariantMappingService _service;

        private readonly IMarketplaceCategoryMappingService _marketplaceCategoryMappingService;

        private readonly IMarketplaceCompanyService _marketplaceCompanyService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        private readonly Application.Common.Interfaces.ECommerceCatalog.IECommerceVariantService _ecommerceVariantService;

        private readonly IVariantService _variantService;
        #endregion

        #region Constructors
        public MarketplaceVariantMappingController(IMarketplaceCategoryMappingService marketplaceCategoryMappingService, IMarketplaceCompanyService marketplaceCompanyService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, Application.Common.Interfaces.ECommerceCatalog.IECommerceVariantService ecommerceVariantService,

             IVariantService variantService, IMarketplaceService marketPlaceService, IMarketplaceVariantMappingService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._marketplaceCategoryMappingService = marketplaceCategoryMappingService;
            this._ecommerceVariantService = ecommerceVariantService;
            this._marketplaceCompanyService = marketplaceCompanyService;
            this._marketplaceVariantService = marketplaceVariantService;
            this._variantService = variantService;
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> IndexPartial(int variantId)
        {
            var variantResponse = await this._variantService.ReadAsync(variantId);
            var marketplaceCompanies = await this._marketplaceCompanyService.ReadAsync();

            var marketplaceCategoryMappings = await this._marketplaceCategoryMappingService.GetMappedAsync();
            var parameters = marketplaceCategoryMappings
                .Data
                .GroupBy(mcm => mcm.MarketplaceId)
                .Select(gmcm => new GetByCategoryCodesParameter
                {
                    MarketplaceId = gmcm.Key,
                    CategoryCodes = gmcm.Select(mcm => mcm.MarketplaceCategoryCode).ToList()
                })
                .ToList();

            var tasks = new List<Task<DataResponse<GetByCategoryCodesResponse>>>();

            parameters.ForEach(parameter =>
            {
                var marketplaceId = parameter.MarketplaceId;
                if (marketplaceCompanies.Data.FirstOrDefault(d => d.MarketplaceId == marketplaceId) == null) return;

                if (marketplaceCompanies.Data.FirstOrDefault(d => d.MarketplaceId == marketplaceId).IsEcommerce)
                    tasks.Add(this._ecommerceVariantService.GetByCategoryCodesAsync(parameter));
                else
                    tasks.Add(this._marketplaceVariantService.GetByCategoryCodesAsync(parameter));
            });

            var marketplaceVariants = await Task.WhenAll(tasks);

            var vm = new Application.Panel.ViewModels.MarketplaceVariantMappings.IndexPartial
            {
                Variant = variantResponse.Data,
                Marketplaces = new()
            };

            marketplaceCompanies.Data.ForEach(d =>
            {
                var marketplaceId = d.MarketplaceId;
                var marketplace = new Application.Panel.ViewModels.MarketplaceVariantMappings.Marketplace
                {
                    Id = marketplaceId,
                    Name = d.Name,
                    IsECommerce = d.IsEcommerce,
                    Variants = new()
                };
                vm.Marketplaces.Add(marketplace);

                var mv = marketplaceVariants.FirstOrDefault(mv => mv.Data.MarketplaceId == marketplaceId);
                if (mv != null)
                {
                    marketplace.Variants = mv
                        .Data
                        .Items
                        .GroupBy(i => new
                        {
                            i.Code,
                            i.Name
                        })
                        .Select(v => new Application.Panel.ViewModels.MarketplaceVariantMappings.Variant
                        {
                            Code = v.Key.Code,
                            Name = v.Key.Name,
                            CompanyId = v.First().CompanyId,
                            AllowCustom = v.First().AllowCustom,
                            Mandatory = v.First().Mandatory,
                            Selected = variantResponse
                                .Data
                                .VariantValues
                                .Any(vv => vv.MarketplaceVariantValueMappings.Any(mvvm => mvvm.MarketplaceVariantCode == v.Key.Code && mvvm.MarketplaceId == marketplaceId))
                        })
                        .ToList();
                }
            });

            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Upsert([FromBody] Application.Panel.Parameters.MarketplaceVariantMappings.UpsertRequest upsertRequest)
        {
            return Json(this._service.Upsert(upsertRequest));
        }
        #endregion
    }
}