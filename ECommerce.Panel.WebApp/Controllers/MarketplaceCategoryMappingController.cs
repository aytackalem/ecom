﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings;
using ECommerce.Application.Panel.ViewModels.MarketplaceCategoryMappings;
using ECommerce.Persistence.Common.ECommerceCatalog;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceCategoryMappingController : Base.ControllerBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceCategoryService _marketplaceCategoryService;

        private readonly IECommerceCategoryService _ecommerceCategoryService;

        private readonly IMarketplaceCompanyService _marketplaceCompanyService;

        private readonly IMarketplaceService _marketplaceService;

        private readonly IMarketplaceCategoryMappingService _service;

        private readonly ICategoryService _categoryService;

        private readonly IECommerceVariantService _ecommerceVariantService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        #endregion

        #region Constructors
        public MarketplaceCategoryMappingController(IECommerceVariantService ecommerceVariantService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceCategoryService marketplaceCatalogService, IECommerceCategoryService ecommerceCategoryService, IMarketplaceCompanyService marketplaceCompanyService, ICategoryService categoryService, IMarketplaceService marketPlaceService, IMarketplaceCategoryMappingService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._marketplaceCategoryService = marketplaceCatalogService;
            this._ecommerceCategoryService = ecommerceCategoryService;
            this._marketplaceCompanyService = marketplaceCompanyService;
            this._marketplaceService = marketPlaceService;
            this._service = service;
            this._categoryService = categoryService;
            this._ecommerceVariantService = ecommerceVariantService;
            this._marketplaceVariantService = marketplaceVariantService;

            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId">Kategoriye ait id bilgisi.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> IndexPartial(int categoryId)
        {
            var marketplaceCompanyResponse = await this._marketplaceCompanyService.ReadAsync();
            if (marketplaceCompanyResponse.Success == false)
            {
                return PartialView(new IndexPartial
                {
                    Success = false,
                    Message = marketplaceCompanyResponse.Message
                });
            }

            List<Task<DataResponse<GetByMarketplaceIdResponse>>> categoryTasks = new();
            marketplaceCompanyResponse.Data.ForEach(mc =>
            {
                if (mc.IsEcommerce == false)
                    categoryTasks.Add(this._marketplaceCategoryService.GetByMarketplaceIdAsync(mc.MarketplaceId));
                else
                    categoryTasks.Add(this._ecommerceCategoryService.GetByMarketplaceIdAsync(mc.MarketplaceId));
            });
            var categoryResponses = await Task.WhenAll(categoryTasks);

            if (categoryResponses.Any(cr => cr.Success == false))
                return PartialView(new IndexPartial
                {
                    Success = false,
                    Message = categoryResponses.First(cr => cr.Success == false).Message
                });

            var category = this._categoryService.Read(categoryId);

            var marketplaceCategoryMappingResponse = this._service.Read(categoryId);



            #region Marketplace Variants
            var marketplaceCategoryCodes = marketplaceCategoryMappingResponse
                    .Data
                    .GroupBy(d => d.MarketplaceId)
                    .Select(d => new GetByCategoryCodesParameter
                    {
                        MarketplaceId = d.Key,
                        CategoryCodes = d.Select(pm => pm.MarketplaceCategoryCode).ToList()
                    })
                    .ToList();
            List<Task<DataResponse<GetByCategoryCodesResponse>>> tasks = new();
            marketplaceCategoryCodes.ForEach(mcc =>
            {
                var marketplace = marketplaceCompanyResponse.Data.FirstOrDefault(m => m.MarketplaceId == mcc.MarketplaceId);
                if (marketplace == null) return;

                if (marketplace.IsEcommerce)
                    tasks.Add(this._ecommerceVariantService.GetByCategoryCodesAsync(mcc));
                else
                    tasks.Add(this._marketplaceVariantService.GetByCategoryCodesAsync(mcc));
            });

            var marketplaceVariants = await Task.WhenAll(tasks);
            #endregion


            var vm = new IndexPartial
            {
                CategoryId = categoryId,
                CategoryName = category.Data.CategoryTranslations.FirstOrDefault()?.Name,
                CategoryBreadcrumb = category.Data.CategoryTranslations.FirstOrDefault()?.CategoryTranslationBreadcrumb.Html,
                Marketplaces = new(),
                MarketplaceCategoryMappings = marketplaceCategoryMappingResponse.Data,
                MarketplaceVariants = marketplaceVariants.Select(mv => mv.Data).ToList(),
                Success = true
            };

            foreach (var theCategoryResponse in categoryResponses)
            {
                var marketplaceId = theCategoryResponse.Data.MarketplaceId;
                var marketplace = new Marketplace
                {
                    Id = marketplaceId,
                    Name = marketplaceCompanyResponse.Data.First(d => d.MarketplaceId == marketplaceId).Name,
                    IsECommerce = marketplaceCompanyResponse.Data.First(d => d.MarketplaceId == marketplaceId).IsEcommerce,
                    Categories = theCategoryResponse.Data.Items.Select(c => new Application.Panel.ViewModels.MarketplaceCategoryMappings.Category
                    {
                        Code = c.Code,
                        Name = c.Name,
                        CompanyId = c.CompanyId,
                        Breadcrumb = c.Breadcrumb
                    }).ToList()
                };

                var mapping = marketplaceCategoryMappingResponse.Data.FirstOrDefault(mcm => mcm.MarketplaceId == marketplaceId);
                if (mapping != null)
                {
                    var c = marketplace.Categories.FirstOrDefault(c => c.Code == mapping.MarketplaceCategoryCode);
                    if (c != null)
                        c.Selected = true;
                }

                vm.Marketplaces.Add(marketplace);
            }

            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Upsert([FromBody] UpsertRequest upsertRequest)
        {
            return Json(this._service.Upsert(upsertRequest));
        }
        #endregion
    }
}
