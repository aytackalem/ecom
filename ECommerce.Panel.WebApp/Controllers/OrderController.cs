﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Orders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,customerservice,manager")]
    public class OrderController : Base.ControllerBase
    {
        #region Fields
        private readonly IOrderService _service;

        private readonly IProductInformationStockService _productInformationStockService;

        private readonly IShipmentCompanyService _shipmentCompanyService;

        private readonly ICountryService _countryService;

        private readonly ICityService _cityService;

        private readonly IDistrictService _districtService;

        private readonly INeighborhoodService _neighborhoodService;

        private readonly IOrderSourceService _orderSourceService;

        private readonly IPaymentTypeService _paymentTypeService;

        private readonly IOrderTypeService _orderTypeService;

        private readonly IMarketplaceService _marketPlaceService;

        private readonly IProductInformationService _productInformationService;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public OrderController(IProductInformationStockService productInformationStockService, IProductInformationService productInformationService, ISettingService settingService,  IOrderService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder,
            IShipmentCompanyService shipmentCompanyService, ICountryService countryService, IOrderSourceService orderSourceService, IPaymentTypeService paymentTypeService, IOrderTypeService orderTypeService, IMarketplaceService marketPlaceService, ICityService cityService, IDistrictService districtService, INeighborhoodService neighborhoodService, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._productInformationStockService = productInformationStockService;
            this._shipmentCompanyService = shipmentCompanyService;
            this._countryService = countryService;
            this._orderSourceService = orderSourceService;
            this._paymentTypeService = paymentTypeService;
            this._orderTypeService = orderTypeService;
            this._marketPlaceService = marketPlaceService;
            this._cityService = cityService;
            this._districtService = districtService;
            this._neighborhoodService = neighborhoodService;
            this._settingService = settingService;
            this._productInformationService = productInformationService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View(new Application.Panel.ViewModels.Orders.Index
            {
                ShipmentCompanies = this._shipmentCompanyService.ReadAsKeyValue().Data,
                BarcodeWritable = this._settingService.BarcodeWritable,
                OrderSources = _orderSourceService.ReadAsKeyValue().Data,
                OrderTypes = _orderTypeService.ReadAsKeyValue().Data,
                MarketPlaces = _marketPlaceService.ReadAsKeyValue().Data,
                OrderDetailMultiselect = this._settingService.OrderDetailMultiselect,
                ShowStock = this._settingService.StockControl
            });
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.Orders.Create
            {
                ShipmentCompanies = _shipmentCompanyService.ReadAsKeyValue().Data,
                Countries = _countryService.ReadAsKeyValue().Data,
                Citys = _cityService.ReadByCountryId(this._settingService.DefaultCountryId).Data,
                OrderSources = _orderSourceService.ReadAsKeyValue().Data,
                PaymentTypes = _paymentTypeService.ReadAsKeyValue().Data,
                OrderTypes = _orderTypeService.ReadAsKeyValue().Data,
                MarketPlaces = _marketPlaceService.ReadAsKeyValue().Data,
                OrderDetailMultiselect = this._settingService.OrderDetailMultiselect,
                IncludeExport = this._settingService.IncludeExport
            });
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            var response = this._service.Read(pagedRequest);
            return Json(response);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var order = this._service.Read(id).Data;

            var vm = new Application.Panel.ViewModels.Orders.Update
            {
                ShipmentCompanies = _shipmentCompanyService.ReadAsKeyValue().Data,
                Countries = _countryService.ReadAsKeyValue().Data,
                OrderSources = _orderSourceService.ReadAsKeyValue().Data,
                PaymentTypes = _paymentTypeService.ReadAsKeyValue().Data,
                OrderTypes = _orderTypeService.ReadAsKeyValue().Data,
                MarketPlaces = _marketPlaceService.ReadAsKeyValue().Data,
                Order = order,
                OrderDeliveryCitys = _cityService.ReadByCountryId(order.OrderDeliveryAddress.Neighborhood.District.City.Country.Id).Data,
                OrderInvoiceCitys = _cityService.ReadByCountryId(order.OrderInvoiceInformation.Neighborhood.District.City.Country.Id).Data,
                OrderDeliveryDistricts = _districtService.ReadByCityId(order.OrderDeliveryAddress.Neighborhood.District.City.Id).Data,
                OrderInvoiceDistricts = _districtService.ReadByCityId(order.OrderInvoiceInformation.Neighborhood.District.City.Id).Data,
                OrderDeliveryNeighborhoods = _neighborhoodService.ReadByDistrictId(order.OrderDeliveryAddress.Neighborhood.District.Id).Data,
                OrderInvoiceNeighborhoods = _neighborhoodService.ReadByDistrictId(order.OrderInvoiceInformation.Neighborhood.District.Id).Data,
                OrderDetailMultiselect = this._settingService.OrderDetailMultiselect,
                IncludeExport = this._settingService.IncludeExport
            };

            return PartialView(vm);

        }

        [HttpPost]
        public JsonResult Update([FromBody] Order order)
        {
            var updateResponse = this._service.Update(order);
            if (updateResponse.Success && order.OrderTypeId == OrderTypes.Iptal)
            {
                if (this._settingService.StockControl)
                {
                    order.OrderDetails.ForEach(odLoop =>
                    {
                        var stockResponse = this._productInformationStockService.IncreaseStock(odLoop.ProductInformationId, odLoop.Quantity);
                        if (!stockResponse.Success)
                        {
                            //TO DO: Alert...
                        }
                    });
                }
            }

            return Json(updateResponse);
        }

        [HttpPost]
        public JsonResult Create([FromBody] Order order)
        {
            var createResponse = this._service.Create(order);
            if (createResponse.Success)
            {
                if (this._settingService.StockControl)
                {
                    order.OrderDetails.ForEach(odLoop =>
                    {
                        var stockResponse = this._productInformationStockService.DecreaseStock(odLoop.ProductInformationId, odLoop.Quantity);
                        if (!stockResponse.Success)
                        {
                            //TO DO: Alert...
                        }
                    });
                }
            }

            return Json(createResponse);
        }

        [HttpGet]
        public IActionResult Summary()
        {
            return PartialView(this._service.Summary().Data);
        }

        [HttpGet]
        public FileResult Download([FromQuery] FilterRequest filterRequest)
        {
            return File(this._service.Download(filterRequest).Data, "application/vnd.ms-excel", "data.xlsx");
        }

        [HttpGet]
        public FileResult Print([FromQuery] string ids)
        {
            return File(this._service.Pdf(ids).Data, "application/pdf");
        }

        [HttpGet]
        public FileResult MutualBarcode([FromQuery] int id)
        {
            return File(this._service.MutualBarcode(id).Data, "image/png");
        }

        [HttpGet]
        public IActionResult Convert()
        {
            return PartialView(new Application.Panel.ViewModels.Orders.Convert());
        }

        [HttpPost]
        public JsonResult Convert([FromBody] ConvertRequest convertRequest)
        {
            return Json(this._service.Convert(convertRequest));
        }

        [HttpPost]
        public JsonResult MakeShipped([FromBody] List<int> ids)
        {
            return Json(this._service.MakeShipped(ids));
        }

        [HttpGet]
        public IActionResult Return(int id)
        {
            return PartialView(this._service.Detail(id).Data);
        }

        [HttpPost]
        public JsonResult Return([FromBody] Order order)
        {
            var returnResponse = this._service.Return(order);
            if (returnResponse.Success)
            {
                if (this._settingService.StockControl)
                {
                    order
                        .OrderReturnDetails
                        .Where(ord => ord.IsReturn)
                        .ToList()
                        .ForEach(odLoop =>
                        {
                            if (!odLoop.IsLoss)
                            {
                                var stockResponse = this
                               ._productInformationStockService
                               .IncreaseStock(odLoop.ProductInformationId, 1);
                                if (!stockResponse.Success)
                                {
                                    //TO DO: Alert...
                                }
                            }
                           
                        });
                }
            }

            return Json(returnResponse);
        }

        [HttpPost]
        public JsonResult DeletePicking([FromBody] Order order)
        {
            return Json(this._service.DeletePicking(order));
        }

        /// <summary>
        /// Faturayı sil
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteInvoice([FromBody] Order order)
        {
            return Json(this._service.DeleteInvoice(order));
        }

        /// <summary>
        /// Gider Pusulasını sil
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteReturnInvoice([FromBody] Order order)
        {
            return Json(this._service.DeleteReturnInvoice(order));
        }

        [HttpPost]
        public JsonResult EmptyPackerBarcode([FromBody] Order order)
        {
            return Json(this._service.EmptyPackerBarcode(order));
        }

        [HttpGet]
        public IActionResult ProductInformationSelection(string q)
        {
            var response = this._productInformationService.ReadAsKeyProductInformation(1000, q);
            return PartialView(response.Data);
        }
        #endregion
    }
}