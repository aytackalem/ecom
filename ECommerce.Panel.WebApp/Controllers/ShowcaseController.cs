﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ShowcaseController : Base.ControllerBase
    {
        #region Fields
        private readonly IShowcaseService _service;

        private readonly IProductService _productService;
        #endregion

        #region Constructors
        public ShowcaseController( IShowcaseService service, IProductService productService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._productService = productService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult Create([FromBody] Showcase dto)
        {
            dto.RowComponents.ForEach(rc =>
            {
                rc.ColumnComponents.ForEach(cc =>
                {
                    cc.ProductComponents.ForEach(pc =>
                    {
                        var entity = this._productService.ReadInfo(pc.ProductId).Data;
                        var productInformationPrice = entity.ProductInformations[0].ProductInformationPriceses[0];
                        var productInformationTranslation = entity.ProductInformations[0].ProductInformationTranslations[0];

                        pc.ListUnitPrice = productInformationPrice.ListUnitPrice;
                        pc.UnitPrice = productInformationPrice.UnitPrice;
                        pc.Discount = Math.Round(100 - (pc.UnitPrice * 100 / pc.ListUnitPrice));
                        pc.VatRate = productInformationPrice.VatRate;
                        pc.Name = $"{productInformationTranslation.Name} {productInformationTranslation.VariantValuesDescription}";
                        pc.Url = productInformationTranslation.Url;
                        pc.SubTitle = productInformationTranslation.SubTitle;
                        pc.Payor = productInformationTranslation.ProductInformation.Payor;
                        pc.IsSale = productInformationTranslation.ProductInformation.IsSale;
                        pc.IsStock = (productInformationTranslation.ProductInformation.Stock + productInformationTranslation.ProductInformation.VirtualStock) > 0;
                        pc.FileName = entity.ProductInformations[0].ProductInformationPhotos.FirstOrDefault()?.FileName;
                        pc.ProductLabels = entity.ProductLabels.Where(x => x.Active).OrderBy(y => y.Label.DisplayOrder).SelectMany(x => x.Label.LabelTranslations).Select(x => x.Value).ToList();

                    });

                    cc.SliderComponents.ForEach(sc =>
                    {
                        sc.ProductComponents.ForEach(pc =>
                        {
                            var entity = this._productService.ReadInfo(pc.ProductId).Data;
                            var productInformationPrice = entity.ProductInformations[0].ProductInformationPriceses[0];
                            var productInformationTranslation = entity.ProductInformations[0].ProductInformationTranslations[0];

                            pc.ListUnitPrice = productInformationPrice.ListUnitPrice;
                            pc.UnitPrice = productInformationPrice.UnitPrice;
                            pc.Discount = Math.Round(100 - (pc.UnitPrice * 100 / pc.ListUnitPrice));
                            pc.VatRate = productInformationPrice.VatRate;
                            pc.Name = $"{productInformationTranslation.Name} {productInformationTranslation.VariantValuesDescription}";
                            pc.Url = productInformationTranslation.Url;
                            pc.SubTitle = productInformationTranslation.SubTitle;
                            pc.Payor = productInformationTranslation.ProductInformation.Payor;
                            pc.IsSale = productInformationTranslation.ProductInformation.IsSale;
                            pc.IsStock = (productInformationTranslation.ProductInformation.Stock + productInformationTranslation.ProductInformation.VirtualStock) > 0;
                            pc.FileName = entity.ProductInformations[0].ProductInformationPhotos.FirstOrDefault()?.FileName;
                            pc.ProductLabels = entity.ProductLabels.Where(x => x.Active).OrderBy(y => y.Label.DisplayOrder).SelectMany(x => x.Label.LabelTranslations).Select(x => x.Value).ToList();
                        });
                    });
                });
            });

            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var vm = new Application.Panel.ViewModels.Showcases.Update
            {
                Showcase = this._service.Read(id).Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Update([FromBody] Showcase dto)
        {
            dto.RowComponents.ForEach(rc =>
            {
                rc.ColumnComponents.ForEach(cc =>
                {
                    cc.ProductComponents.ForEach(pc =>
                    {
                        var entity = this._productService.ReadInfo(pc.ProductId).Data;
                        var productInformationPrice = entity.ProductInformations[0].ProductInformationPriceses[0];
                        var productInformationTranslation = entity.ProductInformations[0].ProductInformationTranslations[0];

                        pc.ListUnitPrice = productInformationPrice.ListUnitPrice;
                        pc.UnitPrice = productInformationPrice.UnitPrice;
                        pc.Discount = Math.Round(100 - (pc.UnitPrice * 100 / pc.ListUnitPrice));
                        pc.VatRate = productInformationPrice.VatRate;
                        pc.Name = $"{productInformationTranslation.Name}";
                        pc.VariantValuesDescription= productInformationTranslation.VariantValuesDescription;
                        pc.Url = productInformationTranslation.Url;
                        pc.SubTitle = productInformationTranslation.SubTitle;
                        pc.Payor = productInformationTranslation.ProductInformation.Payor;
                        pc.IsSale = productInformationTranslation.ProductInformation.IsSale;
                        pc.IsStock = (productInformationTranslation.ProductInformation.Stock + productInformationTranslation.ProductInformation.VirtualStock) > 0;
                        pc.FileName = entity.ProductInformations[0].ProductInformationPhotos.FirstOrDefault()?.FileName;
                        pc.ProductLabels = entity.ProductLabels.Where(x => x.Active).OrderBy(y => y.Label.DisplayOrder).SelectMany(x => x.Label.LabelTranslations).Select(x => x.Value).ToList();

                    });

                    cc.SliderComponents.ForEach(sc =>
                    {
                        sc.ProductComponents.ForEach(pc =>
                        {
                            var entity = this._productService.ReadInfo(pc.ProductId).Data;
                            var productInformationPrice = entity.ProductInformations[0].ProductInformationPriceses[0];
                            var productInformationTranslation = entity.ProductInformations[0].ProductInformationTranslations[0];

                            pc.ListUnitPrice = productInformationPrice.ListUnitPrice;
                            pc.UnitPrice = productInformationPrice.UnitPrice;
                            pc.Discount = Math.Round(100 - (pc.UnitPrice * 100 / pc.ListUnitPrice));
                            pc.VatRate = productInformationPrice.VatRate;
                            pc.Name = $"{productInformationTranslation.Name}";
                            pc.VariantValuesDescription = productInformationTranslation.VariantValuesDescription;
                            pc.Url = productInformationTranslation.Url;
                            pc.SubTitle = productInformationTranslation.SubTitle;
                            pc.Payor = productInformationTranslation.ProductInformation.Payor;
                            pc.IsSale = productInformationTranslation.ProductInformation.IsSale;
                            pc.IsStock = (productInformationTranslation.ProductInformation.Stock + productInformationTranslation.ProductInformation.VirtualStock) > 0;
                            pc.FileName = entity.ProductInformations[0].ProductInformationPhotos.FirstOrDefault()?.FileName;
                            pc.ProductLabels = entity.ProductLabels.Where(x => x.Active).OrderBy(y => y.Label.DisplayOrder).SelectMany(x => x.Label.LabelTranslations).Select(x => x.Value).ToList();

                        });
                    });
                });
            });

            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
