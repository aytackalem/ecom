﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Limits;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Limits;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Limits")]
public class LimitsController : Base.ControllerBase
{
    private readonly LimitsService LimitsService;

    public LimitsController(LimitsService limitsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        LimitsService = limitsService;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var result = await LimitsService.DetailAsync();

        return View("/Themes/Org/Views/Warehouse/Limits/Index.cshtml", result);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await LimitsService.CreateAsync(command);

        return Json(result);
    }

    [HttpPut("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await LimitsService.UpdateAsync(command);

        return Json(result);
    }
}
