﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Limits.Create;

public sealed record Command(int Single, int Multiple);