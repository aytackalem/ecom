﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Limits.Update;

public sealed record Command(int Id, int Single, int Multiple);