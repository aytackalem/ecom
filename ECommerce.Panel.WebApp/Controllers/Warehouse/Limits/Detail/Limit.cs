﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Limits.Detail;

public sealed record Limit(int Id, int Single, int Multiple);