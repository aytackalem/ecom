﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Shared.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders;

public class WorkOrdersService
{
    private readonly IConfiguration Configuration;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly IHttpContextAccessor HttpContextAccessor;

    private readonly IIdentityService IdentityService;

    public WorkOrdersService(IConfiguration configuration, IDomainFinder domainFinder, ICompanyFinder companyFinder, IHttpContextAccessor httpContextAccessor, IIdentityService identityService)
    {
        Configuration = configuration;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        HttpContextAccessor = httpContextAccessor;
        IdentityService = identityService;
    }

    public async Task<Result<Detail.WorkOrder>> DetailAsync(int id)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new Result<Detail.WorkOrder>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        Result<Detail.WorkOrder> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            result = await httpClient.GetFromJsonAsync<Result<Detail.WorkOrder>>($"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/WorkOrders/{id}");
        }

        return result;
    }

    public async Task<PagedResult<Listing.WorkOrder>> ListingAsync(
        int page,
        int pageRecordsCount,
        int? id,
        int? orderId,
        string? personelName,
        int? status,
        string? createdDateTime)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new PagedResult<Listing.WorkOrder>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        PagedResult<Listing.WorkOrder> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            string url = $"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/WorkOrders?Page={page}&PageRecordsCount={pageRecordsCount}";

            if (id.HasValue)
            {
                url += $"&id={id.Value}";
            }

            if (orderId.HasValue)
            {
                url += $"&orderId={orderId.Value}";
            }

            if (string.IsNullOrEmpty(personelName) == false)
            {
                url += $"&personelName={personelName}";
            }

            if (status.HasValue)
            {
                url += $"&Status={status.Value}";
            }

            if (string.IsNullOrEmpty(createdDateTime) == false)
            {
                url += $"&CreatedDateTime={createdDateTime}";
            }

            HttpResponseMessage httpResponseMessage = null;
            httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<PagedResult<Listing.WorkOrder>>();
            }
            else
            {
                result = new PagedResult<Listing.WorkOrder>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }

    public async Task<PagedResult<KpiPicking.Item>> KpiPicking(
        int page,
        int pageRecordsCount,
        int? id,
        string? personelName)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new PagedResult<KpiPicking.Item>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        PagedResult<KpiPicking.Item> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            string url = $"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/WorkOrders/KpiPicking?Page={page}&PageRecordsCount={pageRecordsCount}";

            if (id.HasValue)
            {
                url += $"&id={id.Value}";
            }

            if (string.IsNullOrEmpty(personelName) == false)
            {
                url += $"&personelName={personelName}";
            }

            HttpResponseMessage httpResponseMessage = null;
            httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<PagedResult<KpiPicking.Item>>();
            }
            else
            {
                result = new PagedResult<KpiPicking.Item>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }

    public async Task<PagedResult<KpiPacking.Item>> KpiPacking(
        int page,
        int pageRecordsCount,
        int? id,
        string? personelName)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new PagedResult<KpiPacking.Item>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        PagedResult<KpiPacking.Item> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            string url = $"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/WorkOrders/KpiPacking?Page={page}&PageRecordsCount={pageRecordsCount}";

            if (id.HasValue)
            {
                url += $"&id={id.Value}";
            }

            if (string.IsNullOrEmpty(personelName) == false)
            {
                url += $"&personelName={personelName}";
            }

            HttpResponseMessage httpResponseMessage = null;
            httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<PagedResult<KpiPacking.Item>>();
            }
            else
            {
                result = new PagedResult<KpiPacking.Item>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }
}
