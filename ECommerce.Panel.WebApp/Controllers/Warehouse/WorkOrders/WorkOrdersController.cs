﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/WorkOrders")]
public class WorkOrdersController : Base.ControllerBase
{
    private readonly WorkOrdersService WorkOrdersService;

    public WorkOrdersController(WorkOrdersService workOrdersService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WorkOrdersService = workOrdersService;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/WorkOrders/Index.cshtml");
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        Result<Detail.WorkOrder> result = await WorkOrdersService.DetailAsync(id);

        return PartialView("/Themes/Org/Views/Warehouse/WorkOrders/Update.cshtml", result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(
        int page,
        int pageRecordsCount,
        string search)
    {
        int? id = null;
        int? orderId = null;
        string? personelName = null;
        int? status = null;
        string? createdDateTime = null;

        if (string.IsNullOrEmpty(search) == false)
        {
            var filters = search.Split('_');
            foreach (var filter in filters)
            {
                if (filter.StartsWith("Id") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    id = int.Parse(filter.Split(":")[1]);
                }
                else if (filter.StartsWith("OrderId") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    orderId = int.Parse(filter.Split(":")[1]);
                }
                else if (filter.StartsWith("PersonelName") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    personelName = filter.Split(":")[1];
                }
                else if (filter.StartsWith("Status") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    status = int.Parse(filter.Split(":")[1]);
                }
                else if (filter.StartsWith("CreatedDateTime") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    createdDateTime = filter.Split(":")[1];
                }
            }
        }

        PagedResult<Listing.WorkOrder> result = await WorkOrdersService.ListingAsync(page, pageRecordsCount, id, orderId, personelName, status, createdDateTime);

        return Json(result);
    }

    [HttpGet("KpiPicking")]
    public IActionResult KpiPicking()
    {
        return View("/Themes/Org/Views/Warehouse/WorkOrders/KpiPicking.cshtml");
    }

    [HttpGet("KpiPickingListing")]
    public async Task<JsonResult> KpiPickingListing(
        int page,
        int pageRecordsCount,
        string search)
    {
        var filter = search.Parse<KpiPicking.Search>();

        PagedResult<KpiPicking.Item> result = await WorkOrdersService.KpiPicking(page, pageRecordsCount, filter.Id, filter.PersonelName);

        return Json(result);
    }

    [HttpGet("KpiPacking")]
    public IActionResult KpiPacking()
    {
        return View("/Themes/Org/Views/Warehouse/WorkOrders/KpiPacking.cshtml");
    }

    [HttpGet("KpiPackingListing")]
    public async Task<JsonResult> KpiPackingListing(
        int page,
        int pageRecordsCount,
        string search)
    {
        var filter = search.Parse<KpiPacking.Search>();

        PagedResult<KpiPacking.Item> result = await WorkOrdersService.KpiPacking(page, pageRecordsCount, filter.Id, filter.PersonelName);

        return Json(result);
    }
}
