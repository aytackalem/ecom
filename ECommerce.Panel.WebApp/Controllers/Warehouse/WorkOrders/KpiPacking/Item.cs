﻿using System;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders.KpiPacking;

public class Item
{
    public int Id { get; set; }

    public DateTime Started { get; set; }

    public string StartedString => Started.ToString("yyyy-MM-dd HH:mm");

    public DateTime Finished { get; set; }

    public string FinishedString => Finished.ToString("yyyy-MM-dd HH:mm");

    public int Remain { get; set; }

    public string PersonelName { get; set; }
}
