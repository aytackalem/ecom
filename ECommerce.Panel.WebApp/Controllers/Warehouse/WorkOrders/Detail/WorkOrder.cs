﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders.Detail;

public class WorkOrder
{
    public int Id { get; set; }

    public WorkOrderStatus Status { get; set; }

    public int UserId { get; set; }

    public bool Single { get; set; }

    public bool Multiple => !Single;

    public List<WorkOrderItem> Items { get; set; }
}