﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders.Detail;

public enum WorkOrderStatus
{
    Pending,
    Picking,
    Picked,
    Packing,
    Packed
}