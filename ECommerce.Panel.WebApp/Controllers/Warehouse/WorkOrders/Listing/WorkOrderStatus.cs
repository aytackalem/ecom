﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders.Listing;

public enum WorkOrderStatus
{
    Pending,
    Picking,
    Picked,
    Packing,
    Packed
}