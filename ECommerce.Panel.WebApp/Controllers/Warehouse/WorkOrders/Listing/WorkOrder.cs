﻿using System;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders.Listing;

public class WorkOrder
{
    public int Id { get; set; }

    public WorkOrderStatus Status { get; set; }

    public string PersonelName { get; set; }

    public bool Single { get; set; }

    public bool Multiple => !Single;

    public DateTime CreatedDateTime { get; set; }

    public string CreatedDateTimeString => CreatedDateTime.ToString("dd-MM-yyy HH:mm");

    public DateTime? PickingDateTime { get; set; }

    public string PickingDateTimeString => PickingDateTime.HasValue ? PickingDateTime.Value.ToString("dd-MM-yyy HH:mm") : null;

    public DateTime? PickedDateTime { get; set; }

    public string PickedDateTimeString => PickedDateTime.HasValue ? PickedDateTime.Value.ToString("dd-MM-yyy HH:mm") : null;

    public TimeSpan? PickingTime { get; set; }

    public DateTime? PackingDateTime { get; set; }

    public string PackingDateTimeString => PackingDateTime.HasValue ? PackingDateTime.Value.ToString("dd-MM-yyy HH:mm") : null;

    public DateTime? PackedDateTime { get; set; }

    public string PackedDateTimeString => PackedDateTime.HasValue ? PackedDateTime.Value.ToString("dd-MM-yyy HH:mm") : null;

    public TimeSpan? PackingTime { get; set; }
}
