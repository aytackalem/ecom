﻿using System;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Print;

public class Data
{
    public List<Shelf> Shelves { get; set; }

    public int Width { get; set; }

    public int Height { get; set; }

    public int TopMargin => 2;

    public int LeftMargin => 2;

    public int BarcodeWidth
    {
        get
        {
            return Width - (LeftMargin * 2);
        }
    }

    public int BarcodeHeight
    {
        get
        {
            return Height - LabelHeight - (TopMargin * 2);
        }
    }

    public int BarcodeY => TopMargin;

    public int BarcodeX => LeftMargin;

    public int LabelHeight
    {
        get
        {
            return 10;
        }
    }

    public int LabelWidth => BarcodeWidth;

    public int LabelY => BarcodeY * 2 + BarcodeHeight;

    public int LabelX => LeftMargin;
}
