﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Print;

public class Shelf
{
    public int Id { get; set; }

    public string Name { get; set; }
}
