﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Update;

public sealed record Command(int Id, string Name, int SortNumber, int HallwayId);