﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Listing;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Listing;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Detail;
using Shared.Results;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Update;

public class ViewModel
{
    public Result<Shelf> Shelf { get; set; }

    public PagedResult<Warehouses.Listing.Warehouse> Warehouses { get; set; }

    public PagedResult<Floor> Floors { get; set; }

    public PagedResult<Hallway> Hallways { get; set; }
}
