﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Listing;

public sealed record Shelf(int Id, string Name, int SortNumber, int HallwayId);