﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Detail;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Detail;

public sealed record Shelf(int Id, string Name, int SortNumber, int HallwayId, Hallway Hallway);