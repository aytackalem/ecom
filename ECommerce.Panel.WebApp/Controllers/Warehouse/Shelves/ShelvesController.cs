﻿using DocumentFormat.OpenXml.Drawing.Charts;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Shared.Results;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Zen.Barcode;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Shelves")]
public class ShelvesController : Base.ControllerBase
{
    private readonly WarehousesService WarehousesService;

    private readonly FloorsService FloorsService;

    private readonly HallwaysService HallwaysService;

    private readonly ShelvesService ShelvesService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public ShelvesController(WarehousesService warehousesService, FloorsService floorsService, HallwaysService hallwaysService, ShelvesService shelvesService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehousesService = warehousesService;
        FloorsService = floorsService;
        HallwaysService = hallwaysService;
        ShelvesService = shelvesService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Shelves/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Shelves/Create.cshtml", warehouses);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await ShelvesService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        Result<Detail.Shelf> result = await ShelvesService.DetailAsync(id);

        var hallways = await HallwaysService.ListingAsync(0, 100, result.Data.Hallway.Floor.Id);
        var floors = await FloorsService.ListingAsync(0, 100, result.Data.Hallway.Floor.WarehouseId);
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Shelves/Update.cshtml", new Update.ViewModel
        {
            Hallways = hallways,
            Floors = floors,
            Warehouses = warehouses,
            Shelf = result
        });
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await ShelvesService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount, int? hallwayId = null)
    {
        PagedResult<Listing.Shelf> result = await ShelvesService.ListingAsync(page, pageRecordsCount, hallwayId);

        return Json(result);
    }

    [HttpGet("Print")]
    public FileResult Print(string data)
    {
        Print.Data _ = JsonSerializer.Deserialize<Print.Data>(data);

        PdfDocument pdfDocument = new();

        XFont xFont = new("Arial", 10);

        foreach (var theShelf in _.Shelves)
        {
            PdfPage pdfPage = pdfDocument.AddPage();
            pdfPage.Orientation = PdfSharp.PageOrientation.Landscape;
            pdfPage.Width = new XUnit(_.Width, XGraphicsUnit.Millimeter);
            pdfPage.Height = new XUnit(_.Height, XGraphicsUnit.Millimeter);

            XGraphics xGraphics = XGraphics.FromPdfPage(pdfPage, XGraphicsUnit.Millimeter);

            var image = BarcodeDrawFactory.Code128WithChecksum.Draw(theShelf.Id.ToString(), _.BarcodeHeight, 2);

            var memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);
            memoryStream.Position = 0;

            xGraphics.DrawImage(XImage.FromStream(memoryStream), _.BarcodeX, _.BarcodeY, _.BarcodeWidth, _.BarcodeHeight);

            xGraphics.DrawString(
                theShelf.Name,
                xFont,
                XBrushes.Black, _.LabelX, _.LabelY+7);


        }

        MemoryStream stream = new();

        pdfDocument.Save(stream);

        stream.Position = 0;

        return File(stream, "application/pdf");
    }
}
