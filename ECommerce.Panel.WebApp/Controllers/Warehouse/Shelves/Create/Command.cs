﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves.Create;

public record Command(string Name, int SortNumber, int HallwayId);