﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.Listing;

public sealed record Pivot(int Id, string Name, int WarehouseId);