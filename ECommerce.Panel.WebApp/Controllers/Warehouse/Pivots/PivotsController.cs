﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Pivots")]
public class PivotsController : Base.ControllerBase
{
    private readonly WarehousesService WarehousesService;

    private readonly FloorsService FloorsService;

    private readonly PivotsService PivotsService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public PivotsController(WarehousesService warehousesService, FloorsService floorsService, PivotsService pivotsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehousesService = warehousesService;
        FloorsService = floorsService;
        PivotsService = pivotsService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Pivots/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Pivots/Create.cshtml", warehouses);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await PivotsService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        var result = await PivotsService.DetailAsync(id);

        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Pivots/Update.cshtml", new Update.ViewModel
        {
            Warehouses = warehouses,
            Pivot = result
        });
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await PivotsService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount, int? warehouseId = null)
    {
        PagedResult<Listing.Pivot> result = await PivotsService.ListingAsync(page, pageRecordsCount, warehouseId);

        return Json(result);
    }
}
