﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.Create;

public sealed record Command(int WarehouseId, string Name);