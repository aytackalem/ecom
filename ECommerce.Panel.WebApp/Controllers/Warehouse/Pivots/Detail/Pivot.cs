﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.Detail;

public sealed record Pivot(int Id, string Name, int WarehouseId, Warehouses.Detail.Warehouse Warehouse);