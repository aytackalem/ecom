﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Listing;

public sealed record PivotShelf(int Id, int PivotShelfId, string Name);