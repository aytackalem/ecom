﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Detail;
using Shared.Results;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Update;

public class ViewModel
{
    public PagedResult<Warehouses.Listing.Warehouse> Warehouses { get; set; }

    public PagedResult<Pivots.Listing.Pivot> Pivots { get; set; }

    public Result<PivotShelf> PivotShelf { get; set; }
}
