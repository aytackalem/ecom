﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Update;

public sealed record Command(int Id, int PivotId, string Name);