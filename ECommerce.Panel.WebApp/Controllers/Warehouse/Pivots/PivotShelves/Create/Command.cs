﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Create;

public sealed record Command(int PivotId, string Name);