﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivot.PivotShelves;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Pivots/PivotShelves")]
public class PivotsController : Base.ControllerBase
{
    private readonly WarehousesService WarehousesService;

    private readonly PivotsService PivotsService;

    private readonly PivotShelvesService PivotShelvesService;

    private readonly FloorsService FloorsService;

    private readonly HallwaysService HallwaysService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public PivotsController(WarehousesService warehousesService, PivotsService pivotsService, PivotShelvesService pivotShelvesService, FloorsService floorsService, HallwaysService hallwaysService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehousesService = warehousesService;
        PivotsService = pivotsService;
        PivotShelvesService = pivotShelvesService;
        FloorsService = floorsService;
        HallwaysService = hallwaysService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Pivots/PivotShelves/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Pivots/PivotShelves/Create.cshtml", warehouses);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Pivots.PivotShelves.Create.Command command)
    {
        Result<int> result = await PivotShelvesService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        var result = await PivotShelvesService.DetailAsync(id);
        var pivots = await PivotsService.ListingAsync(0, 100, result.Data.Pivot.WarehouseId);
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Pivots/PivotShelves/Update.cshtml", new Pivots.PivotShelves.Update.ViewModel
        {
            PivotShelf = result,
            Pivots = pivots,
            Warehouses = warehouses
        });
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Pivots.PivotShelves.Update.Command command)
    {
        Result result = await PivotShelvesService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount, int? floorId = null)
    {
        PagedResult<Pivots.PivotShelves.Listing.PivotShelf> result = await PivotShelvesService.ListingAsync(page, pageRecordsCount, floorId);

        return Json(result);
    }
}
