﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves.Detail;

public sealed record PivotShelf(int Id, int PivotShelfId, string Name, Pivots.Detail.Pivot Pivot);