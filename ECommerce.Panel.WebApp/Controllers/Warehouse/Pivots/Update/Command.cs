﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.Update;

public sealed record Command(int Id, int WarehouseId, string Name);