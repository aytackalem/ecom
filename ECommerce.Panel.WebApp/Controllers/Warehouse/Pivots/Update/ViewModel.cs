﻿using Shared.Results;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.Update;

public class ViewModel
{
    public Result<Detail.Pivot> Pivot { get; set; }

    public PagedResult<Warehouses.Listing.Warehouse> Warehouses { get; set; }
}
