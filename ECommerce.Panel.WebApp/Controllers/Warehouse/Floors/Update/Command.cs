﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Update;

public sealed record Command(int Id, string Name, int SortNumber, int WarehouseId);