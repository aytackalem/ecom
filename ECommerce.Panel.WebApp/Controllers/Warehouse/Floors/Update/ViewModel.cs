﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Detail;
using Shared.Results;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Update;

public class ViewModel
{
    public PagedResult<Warehouses.Listing.Warehouse> Warehouses { get; set; }

    public Result<Floor> Floor { get; set; }
}
