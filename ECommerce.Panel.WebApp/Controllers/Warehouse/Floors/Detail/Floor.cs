﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Detail;

public sealed record Floor(int Id, string Name, int SortNumber, int WarehouseId, Warehouses.Detail.Warehouse Warehouse);