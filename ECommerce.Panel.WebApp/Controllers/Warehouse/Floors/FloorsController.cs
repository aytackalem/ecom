﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Floors")]
public class FloorsController : Base.ControllerBase
{
    private readonly WarehousesService WarehousesService;

    private readonly FloorsService FloorsService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public FloorsController(WarehousesService warehousesService, FloorsService floorsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehousesService = warehousesService;
        FloorsService = floorsService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Floors/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Floors/Create.cshtml", warehouses);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await FloorsService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);
        var result = await FloorsService.DetailAsync(id);

        return PartialView("/Themes/Org/Views/Warehouse/Floors/Update.cshtml", new Update.ViewModel
        {
            Warehouses = warehouses,
            Floor = result
        });
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await FloorsService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount, int? warehouseId = null)
    {
        PagedResult<Listing.Floor> result = await FloorsService.ListingAsync(page, pageRecordsCount, warehouseId);

        return Json(result);
    }
}
