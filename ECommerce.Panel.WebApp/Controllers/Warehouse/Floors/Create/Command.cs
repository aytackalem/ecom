﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Create;

public record Command(string Name, int SortNumber, int WarehouseId);