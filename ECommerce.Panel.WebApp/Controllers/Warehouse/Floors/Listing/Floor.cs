﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Listing;

public sealed record Floor(int Id, string Name, int SortNumber, int WarehouseId);