﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Detail;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Detail;

public sealed record Hallway(int Id, string Name, int SortNumber, int FloorId, Floor Floor);