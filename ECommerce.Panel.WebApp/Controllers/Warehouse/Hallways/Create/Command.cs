﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Create;

public record Command(string Name, int SortNumber, int FloorId);