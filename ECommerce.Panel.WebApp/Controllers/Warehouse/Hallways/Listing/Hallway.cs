﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Listing;

public sealed record Hallway(int Id, string Name, int SortNumber, int FloorId);