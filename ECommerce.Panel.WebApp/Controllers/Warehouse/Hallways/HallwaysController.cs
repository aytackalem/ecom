﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Hallways")]
public class HallwaysController : Base.ControllerBase
{
    private readonly WarehousesService WarehousesService;

    private readonly FloorsService FloorsService;

    private readonly HallwaysService HallwaysService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public HallwaysController(WarehousesService warehousesService, FloorsService floorsService, HallwaysService hallwaysService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehousesService = warehousesService;
        FloorsService = floorsService;
        HallwaysService = hallwaysService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Hallways/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var warehouses = await WarehousesService.ListingAsync(0, 100);

        return PartialView("/Themes/Org/Views/Warehouse/Hallways/Create.cshtml", warehouses);
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await HallwaysService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        var result = await HallwaysService.DetailAsync(id);

        var warehouses = await WarehousesService.ListingAsync(0, 100);
        var floors = await FloorsService.ListingAsync(0, 100, result.Data.Floor.WarehouseId);

        return PartialView("/Themes/Org/Views/Warehouse/Hallways/Update.cshtml", new Update.ViewModel
        {
            Warehouses = warehouses,
            Floors = floors,
            Hallway = result
        });
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await HallwaysService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount, int? floorId = null)
    {
        PagedResult<Listing.Hallway> result = await HallwaysService.ListingAsync(page, pageRecordsCount, floorId);

        return Json(result);
    }
}
