﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Update;

public sealed record Command(int Id, string Name, int SortNumber, int FloorId);