﻿using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors.Listing;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Detail;
using Shared.Results;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways.Update;

public class ViewModel
{
    public Result<Hallway> Hallway { get; set; }

    public PagedResult<Warehouses.Listing.Warehouse> Warehouses { get; set; }

    public PagedResult<Floor> Floors { get; set; }
}
