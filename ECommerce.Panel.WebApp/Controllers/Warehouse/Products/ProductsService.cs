﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Shared.Results;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Products;

public class ProductsService
{
    private readonly IConfiguration Configuration;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly IHttpContextAccessor HttpContextAccessor;

    private readonly IIdentityService IdentityService;

    public ProductsService(IConfiguration configuration, IDomainFinder domainFinder, ICompanyFinder companyFinder, IHttpContextAccessor httpContextAccessor, IIdentityService identityService)
    {
        Configuration = configuration;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        HttpContextAccessor = httpContextAccessor;
        IdentityService = identityService;
    }

    public async Task<PagedResult<Listing.Product>> ListingAsync(
        int page,
        int pageRecordsCount,
        string? barcode)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new PagedResult<Listing.Product>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        PagedResult<Listing.Product> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            string url = $"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/Products?Page={page}&PageRecordsCount={pageRecordsCount}";

            if (string.IsNullOrEmpty(barcode) == false)
            {
                url += $"&barcode={barcode}";
            }

            HttpResponseMessage httpResponseMessage = null;
            httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<PagedResult<Listing.Product>>();
            }
            else
            {
                result = new PagedResult<Listing.Product>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }
}
