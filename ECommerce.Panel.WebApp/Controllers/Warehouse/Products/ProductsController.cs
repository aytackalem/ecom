﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Products;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Products")]
public class ProductsController : Base.ControllerBase
{
    private readonly ProductsService ProductsService;

    public ProductsController(ProductsService productsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        ProductsService = productsService;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Products/Index.cshtml");
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(
        int page,
        int pageRecordsCount,
        string search)
    {
        string? barcode = null;

        if (string.IsNullOrEmpty(search) == false)
        {
            var filters = search.Split('_');
            foreach (var filter in filters)
            {
                if (filter.StartsWith("Barcode") && string.IsNullOrEmpty(filter.Split(":")[1]) == false)
                {
                    barcode = filter.Split(":")[1];
                }
            }
        }

        PagedResult<Listing.Product> result = await ProductsService.ListingAsync(page, pageRecordsCount, barcode);

        return Json(result);
    }
}
