﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Shared.Results;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;

public class WarehousesService
{
    private readonly IConfiguration Configuration;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly IHttpContextAccessor HttpContextAccessor;

    private readonly IIdentityService IdentityService;

    public WarehousesService(IConfiguration configuration, IDomainFinder domainFinder, ICompanyFinder companyFinder, IHttpContextAccessor httpContextAccessor, IIdentityService identityService)
    {
        Configuration = configuration;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        HttpContextAccessor = httpContextAccessor;
        IdentityService = identityService;
    }

    public async Task<Result<int>> CreateAsync(Create.Command command)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new Result<int>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        Result<int> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            var httpResponseMessage = await httpClient.PostAsJsonAsync<Create.Command>($"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/warehouses", command);
            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<Result<int>>();
            }
            else
            {
                result = new Result<int>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }

    public async Task<Result> UpdateAsync(Update.Command command)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new Result<int>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        Result result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            var httpResponseMessage = await httpClient.PutAsJsonAsync<Update.Command>($"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/warehouses", command);
            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<Result>();
            }
            else
            {
                result = new Result
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }

    public async Task<Result<Detail.Warehouse>> DetailAsync(int id)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new Result<Detail.Warehouse>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        Result<Detail.Warehouse> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            result = await httpClient.GetFromJsonAsync<Result<Detail.Warehouse>>($"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/warehouses/{id}");
        }

        return result;
    }

    public async Task<PagedResult<Listing.Warehouse>> ListingAsync(int page, int pageRecordsCount)
    {
        var identityResponse = await IdentityService.GetIdentity();

        if (identityResponse.Success == false)
        {
            return new PagedResult<Listing.Warehouse>
            {
                Failed = true,
                Messages = new string[] { "İletişim kurulamadı." }
            };
        }

        var token = identityResponse.Data.AccessToken;

        PagedResult<Listing.Warehouse> result = null;

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            HttpResponseMessage httpResponseMessage = null;
            httpResponseMessage = await httpClient.GetAsync($"{Configuration.GetValue<string>("WarehouseApiUrl")}/v1/warehouses?Page={page}&PageRecordsCount={pageRecordsCount}");

            if (httpResponseMessage.IsSuccessStatusCode || httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                result = await httpResponseMessage.Content.ReadFromJsonAsync<PagedResult<Listing.Warehouse>>();
            }
            else
            {
                result = new PagedResult<Listing.Warehouse>
                {
                    Failed = true,
                    Messages = new string[] { "İletişim kurulamadı." }
                };
            }
        }

        return result;
    }
}
