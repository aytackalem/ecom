﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses.Create;

public record Command(string Name);