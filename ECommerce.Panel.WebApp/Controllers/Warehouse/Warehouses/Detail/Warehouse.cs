﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses.Detail;

public record Warehouse(int Id, string Name);