﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses.Listing;

public record Warehouse(int Id, string Name);