﻿namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses.Update;

public sealed record Command(int Id, string Name, int SortNumber, int WarehouseId);