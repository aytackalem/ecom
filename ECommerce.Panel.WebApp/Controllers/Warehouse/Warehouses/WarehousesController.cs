﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Results;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;

[Authorize(Roles = "user,manager")]
[Route("Warehouse/Warehouses")]
public class WarehousesController : Base.ControllerBase
{
    private readonly WarehousesService WarehouseService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public WarehousesController(WarehousesService warehouseService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        WarehouseService = warehouseService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Warehouse/Warehouses/Index.cshtml");
    }

    [HttpGet("Create")]
    public IActionResult Create()
    {
        return PartialView("/Themes/Org/Views/Warehouse/Warehouses/Create.cshtml");
    }

    [HttpPost("Create")]
    public async Task<JsonResult> Post([FromBody] Create.Command command)
    {
        Result<int> result = await WarehouseService.CreateAsync(command);

        return Json(result);
    }

    [HttpGet("Update")]
    public async Task<IActionResult> Update(int id)
    {
        Result<Detail.Warehouse> result = await WarehouseService.DetailAsync(id);

        return PartialView("/Themes/Org/Views/Warehouse/Warehouses/Update.cshtml", result);
    }

    [HttpPost("Update")]
    public async Task<JsonResult> Put([FromBody] Update.Command command)
    {
        Result result = await WarehouseService.UpdateAsync(command);

        return Json(result);
    }

    [HttpGet("Listing")]
    public async Task<JsonResult> Listing(int page, int pageRecordsCount)
    {
        PagedResult<Listing.Warehouse> result = await WarehouseService.ListingAsync(page, pageRecordsCount);

        return Json(result);
    }
}
