﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class FeedController : Base.ControllerBase
    {
        #region Fields
        private readonly IFeedService _service;

        private readonly IFeedTemplateService _feedTemplateService;
        #endregion

        #region Constructors
        public FeedController(IFeedService service, IFeedTemplateService feedTemplateService,  IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._feedTemplateService = feedTemplateService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.Feeds.Create
            {
                FeedTemplates = this._feedTemplateService.ReadAsKeyValue().Data
            });
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.Feeds.Update
            {
                Feed = this._service.Read(id).Data,
                FeedTemplates = this._feedTemplateService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] Feed feed)
        {
            return Json(this._service.Update(feed));
        }

        [HttpPost]
        public JsonResult Create([FromBody] Feed feed)
        {
            return Json(this._service.Create(feed));
        }
        #endregion
    }
}
