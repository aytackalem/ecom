﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers.Base
{
    [Authorize]
    public abstract class ControllerBase : Controller
    {
        #region Fields
        protected readonly IDomainService _domainService;

        protected readonly ICompanyFinder _companyFinder;

        protected readonly IDomainFinder _domainFinder;

        protected readonly INotificationService _notificationService;

        private readonly ICookieHelper _cookieHelper;

        #endregion

        #region Constructors
        public ControllerBase(IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper)
        {
            #region Fields
            this._domainService = domainService;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._notificationService = notificationService;
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public override async void OnActionExecuting(ActionExecutingContext context)
        {


            var domains = _domainService.ReadAll();



            if (!this._cookieHelper.Exist("LoginV4"))
            {
                this._cookieHelper.Write("LoginV4", new System.Collections.Generic.List<Application.Common.DataTransferObjects.KeyValue<string, string>>
                {
                    new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="DomainId",
                        Value=domains.Data[0].Id.ToString()
                    },
                      new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="CompanyId",
                        Value=domains.Data[0].Companies[0].Id.ToString()
                    }

                }, DateTime.Now.AddYears(1));

                this._domainFinder.Set(domains.Data[0].Id);
                this._companyFinder.Set(domains.Data[0].Companies[0].Id);


            }

            #region Domain

            var domainId = this._domainFinder.FindId();
            if (domainId == 0)
            {
                context.HttpContext.Response.Redirect("/Account/Login");
                return;
            }

            var companyId = this._companyFinder.FindId();
            if (companyId == 0)
            {
                context.HttpContext.Response.Redirect("/Account/Login");
                return;
            }

            domains.Data.ForEach(x =>
            {

                x.Selected = false;
                if (x.Id == domainId) x.Selected = true;
            });

            ViewData["DomainsLayout"] = domains.Data;

            var companies = domains.Data.Where(x => x.Id == domainId).First().Companies;
            companies.ForEach(x =>
            {
                x.Selected = false;
                if (x.Id == companyId) x.Selected = true;
            });

            ViewData["CompaniesLayout"] = companies;


            var notificationCount = this._notificationService.Count();
            ViewData["NotificationCount"] = notificationCount.Data;
            #endregion

            base.OnActionExecuting(context);
        }
        #endregion
    }
}
