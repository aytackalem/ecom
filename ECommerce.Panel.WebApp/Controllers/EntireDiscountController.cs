﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class EntireDiscountController : Base.ControllerBase
    {
        #region Fields
        private readonly IEntireDiscountService _service;

        private readonly IApplicationService _applicationService;
        #endregion

        #region Constructors
        public EntireDiscountController( IEntireDiscountService service, IApplicationService applicationService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._applicationService = applicationService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var vm = new Application.Panel.ViewModels.EntireDiscounts.Create
            {
                Applications = this._applicationService.ReadAsKeyValue().Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] EntireDiscount dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var vm = new Application.Panel.ViewModels.EntireDiscounts.Update
            {
                Applications = this._applicationService.ReadAsKeyValue().Data,
                EntireDiscount = this._service.Read(id).Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Update([FromBody] EntireDiscount dto)
        {
            return Json(this._service.Update(dto));
        }

        [HttpPost]
        public JsonResult Delete([FromBody] EntireDiscount dto)
        {
            return Json(this._service.Delete(dto));
        }
        #endregion
    }
}
