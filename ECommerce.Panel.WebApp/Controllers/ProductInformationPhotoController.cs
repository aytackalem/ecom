﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ProductInformationPhotoController : Base.ControllerBase
    {
        #region Fields
        private readonly IImageHelper _imageHelper;
        #endregion

        #region Constructors
        public ProductInformationPhotoController(IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, IImageHelper imageHelper, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._imageHelper = imageHelper;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public JsonResult Index(IFormFile formFile)
        {
            return new JsonResult(new { Data = this._imageHelper.Resize(formFile) });
        }
        #endregion
    }
}
