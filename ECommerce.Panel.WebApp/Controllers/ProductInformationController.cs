﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.ProductInformations;
using ECommerce.Application.Panel.Parameters.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers;

public class UpdateCategorization
{
    public string SkuCode { get; set; }

    public string Categorization { get; set; }
}

[Authorize(Roles = "user,manager")]
public class ProductInformationController : Base.ControllerBase
{
    #region Fields
    private readonly IProductInformationService _service;

    private readonly IProductService _productservice;

    private readonly ILanguageService _languageService;

    private readonly ICurrencyService _currencyService;

    private readonly ISettingService _settingService;
    #endregion

    #region Constructors
    public ProductInformationController(IProductInformationService service,  IProductService productService, ILanguageService languageService, ICurrencyService currencyService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ISettingService settingService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
    {
        #region Fields
        this._service = service;
        this._productservice = productService;
        this._languageService = languageService;
        this._currencyService = currencyService;
        this._settingService = settingService;
        #endregion
    }
    #endregion

    #region Methods
    [HttpPost]
    public IActionResult IndexPartial([FromBody] ProductInformationPartialRequest productInformationPartialRequest)
    {
        var currenciesResponse = this._currencyService.ReadAsKeyValue();
        var languagesResponse = this._languageService.ReadAsKeyValue();
        var productInformationsResponse = this._productservice.GenerateProductInformations(productInformationPartialRequest);
        var maxBarcodeResponse = this._productservice.MaxBarcode();
        var maxStockCodeResponse = this._productservice.MaxStockCode();

        var vm = new Application.Panel.ViewModels.Products.IndexPartial();
        vm.Check(currenciesResponse, languagesResponse, productInformationsResponse, maxBarcodeResponse, maxStockCodeResponse);
        if (!vm.Success)
            return PartialView(vm);

        vm.Currencies = currenciesResponse.Data;
        vm.Languages = languagesResponse.Data;
        vm.ProductInformations = productInformationsResponse.Data;
        vm.MaxBarcode = maxBarcodeResponse.Data;
        vm.MaxStockCode = maxStockCodeResponse.Data;
        vm.IncludeECommerce = this._settingService.IncludeECommerce;

        return PartialView(vm);
    }

    [HttpPost]
    public IActionResult ConfirmPartial([FromBody] ProductInformationPartialRequest productInformationPartialRequest)
    {
        return PartialView(this._productservice.GenerateProductInformations(productInformationPartialRequest));
    }

    [HttpGet]
    public JsonResult Autocomplete(int take, string q)
    {
        var response = this._service.ReadAsKeyProductInformation(take, q);
        return Json(response);
    }

    [HttpGet]
    public IActionResult PriceUpdate(int productId)
    {
        Application.Panel.ViewModels.ProductInformations.PriceUpdate vm = new()
        {
            ProductInformations = this._service.ReadPrice(productId).Data,
            IncludeAccounting = this._settingService.IncludeAccounting
        };

        return PartialView(vm);
    }

    [HttpPost]
    public JsonResult PriceUpdate([FromBody] PriceUpdateRequest request)
    {
        return Json(this._service.UpdatePrice(request.ProductInformations));
    }

    [HttpPut]
    public JsonResult UpdateCategorization([FromBody] UpdateCategorization updateCategorization)
    {
        return Json(this._service.UpdateCategorization(updateCategorization.SkuCode, updateCategorization.Categorization));
    }
    #endregion
}