﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Accounts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly ICaptchaService _captchaService;

        private readonly IIdentityService _identityService;

        private readonly ICookieHelper _cookieHelper;

        private readonly IDomainFinder _domainFinder;

        private readonly IDomainService _domainService;
        #endregion

        #region Constructors
        public AccountController(ICaptchaService captchaService, IIdentityService identityService, ICookieHelper cookieHelper, IDomainFinder domainFinder, IDomainService domainService)
        {
            #region Fields
            this._captchaService = captchaService;
            this._identityService = identityService;
            this._cookieHelper = cookieHelper;
            this._domainFinder = domainFinder;
            this._domainService = domainService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Login()
        {
            return View(new Application.Panel.ViewModels.Accounts.Login());
        }

        [HttpPost]
        public async Task<JsonResult> Login([FromBody] CaptchaRequest<LoginRequest> captchaRequest)
        {
            if (CheckUsername(captchaRequest.Data.Username) == false || CheckPassword(captchaRequest.Data.Password) == false)
                return Json(new Application.Common.Wrappers.Response
                {
                    Success = false,
                    Message = "Kullanıcı adı veya şifre yanlış."
                });

            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            this._cookieHelper.Delete("LoginV4");

            var loginResponse = await this._identityService.LoginAsync(captchaRequest.Data.Username, captchaRequest.Data.Password, captchaRequest.Data.KeepLoggedIn);

            return Json(loginResponse);
        }

        public async Task<JsonResult> Logout()
        {
            this._cookieHelper.Delete("LoginV4");
            return Json(await this._identityService.LogoutAsync());
        }

        public JsonResult ChangeDomain([FromBody] Application.Panel.DataTransferObjects.Domain domain)
        {
            var domains = this._domainService.ReadAll();
            var _domain = domains.Data.FirstOrDefault(x => x.Id == domain.Id);

            if (domains.Success && _domain != null)
            {
                this._cookieHelper.Write("LoginV4", new System.Collections.Generic.List<Application.Common.DataTransferObjects.KeyValue<string, string>>
                {
                 new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="DomainId",
                        Value=_domain.Id.ToString()
                    },
                    new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="CompanyId",
                        Value=_domain.Companies[0].Id.ToString()
                    }}, DateTime.Now.AddYears(1));
            }


            return Json(new
            {
                success = true
            });
        }

        public JsonResult ChangeCompany([FromBody] Company company)
        {
            var domains = this._domainService.ReadAll();
            var _domain = domains.Data.FirstOrDefault(x => x.Id == this._domainFinder.FindId());

            if (domains.Success && _domain.Companies.Any(x => x.Id == company.Id))
            {
                this._cookieHelper.Write("LoginV4", new System.Collections.Generic.List<Application.Common.DataTransferObjects.KeyValue<string, string>>
                {
                 new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="DomainId",
                        Value=this._domainFinder.FindId().ToString()
                    },
                    new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="CompanyId",
                        Value=company.Id.ToString()
                    }}, DateTime.Now.AddYears(1));

            }
            return Json(new
            {
                success = true
            });
        }

        public IActionResult AccessDenied()
        {
            return View(new Application.Panel.ViewModels.Accounts.AccessDenied());
        }
        #endregion

        #region Helper Methods
        private bool CheckUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                return false;

            foreach (var theChar in username)
            {
                if (char.IsNumber(theChar) == false && char.IsLetter(theChar) == false)
                    return false;
            }

            return true;
        }

        private bool CheckPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;

            var chars = new char[] { '$', '*', '!', '-', '&', '.' };

            foreach (var theChar in password)
            {
                if (char.IsNumber(theChar) == false && char.IsLetter(theChar) == false)
                {
                    if (Array.IndexOf(chars, theChar) == -1)
                        return false;
                }
            }

            return true;
        }
        #endregion
    }
}