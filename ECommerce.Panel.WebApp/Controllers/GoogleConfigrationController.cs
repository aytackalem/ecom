﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class GoogleConfigrationController : Base.ControllerBase
    {
        #region Fields
        private readonly IGoogleConfigrationService _service;

        private readonly ILanguageService _languageService;
        #endregion

        #region Constructors
        public GoogleConfigrationController( IGoogleConfigrationService service, ILanguageService languageService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._languageService = languageService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.GoogleConfigrations.Create
            {
                Languages = this._languageService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Create([FromBody] GoogleConfigration dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.GoogleConfigrations.Update
            {
                Languages = this._languageService.ReadAsKeyValue().Data,
                GoogleConfigration = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] GoogleConfigration dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
