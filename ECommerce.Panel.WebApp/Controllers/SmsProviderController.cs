﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class SmsProviderController : Base.ControllerBase
    {
        #region Fields
        private readonly ISmsProviderCompanyService _service;
        #endregion

        #region Constructors
        public SmsProviderController( ISmsProviderCompanyService service,IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.SmsProviderCompanies.Update
            {
                SmsProviderCompany = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] SmsProviderCompany dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
