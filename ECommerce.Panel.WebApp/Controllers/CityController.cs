﻿using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class CityController : Controller
    {
        #region Fields
        private readonly ICityService _cityService;
        #endregion

        #region Constructors
        public CityController(ICityService cityService)
        {
            #region Fields
            this._cityService = cityService;
            #endregion
        }
        #endregion

        #region Methods
        public JsonResult Read(int countryId)
        {
            return Json(this._cityService.ReadByCountryId(countryId));
        }
        #endregion
    }
}
