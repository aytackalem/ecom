﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.ViewModels.ProductCategorizations;
using ECommerce.Persistence.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers;

[Authorize(Roles = "manager")]
public class ProductCategorizationController : Base.ControllerBase
{
    #region Fields
    private readonly IProductService _productService;

    private readonly ApplicationDbContext _context;
    #endregion

    #region Constructors
    public ProductCategorizationController(
        IDomainService domainService,
        IDomainFinder domainFinder,
        ICompanyFinder companyFinder,
        INotificationService notificationService,
        ICookieHelper cookieHelper,
        IProductService productService,
        ApplicationDbContext context) : base(
            domainService,
            domainFinder,
            companyFinder,
            notificationService,
            cookieHelper)
    {
        #region Fields
        this._productService = productService;
        this._context = context;
        #endregion
    }
    #endregion

    #region Actions
    [HttpGet]
    public IActionResult Get(int id)
    {
        var colors = _productService.ReadAllColors(id).Data;

        var productInformations = this
            ._context
            .ProductInformations
            .AsNoTracking()
            .Where(pi => colors.Select(c => c.Key).Contains(pi.SkuCode))
            .ToList();

        var model = new GetViewModel
        {
            Items = new List<GetItem>()
        };

        foreach (var theColor in colors)
        {
            model.Items.Add(new GetItem
            {
                Code = theColor.Key,
                Color = theColor.Value,
                Categorization = productInformations.FirstOrDefault(pi => pi.SkuCode == theColor.Key).Categorization,
                SpecialCategorization = productInformations.FirstOrDefault(pi => pi.SkuCode == theColor.Key).SpecialCategorization
            });
        }

        return PartialView(model);
    }

    [HttpGet]
    public IActionResult Log(int id)
    {
        var skuCodes = this._context.ProductInformations.Where(pi => pi.ProductId == id).Select(pi => pi.SkuCode).Distinct().ToList();

        var logs = this._context.ProductCategorizationHistories.Where(pih => skuCodes.Contains(pih.SkuCode)).OrderBy(pih => pih.SkuCode).ThenBy(pih => pih.CreatedDate).ToList();

        return PartialView(logs);
    }

    [HttpPost]
    public JsonResult Post([FromBody] GetViewModel model)
    {
        foreach (var loopItem in model.Items)
        {
            var oldValue = this
                ._context
                .ProductInformations
                .AsNoTracking()
                .Where(pi => pi.SkuCode == loopItem.Code)
                .Select(pi => pi.Categorization)
                .FirstOrDefault();

            if (oldValue != loopItem.Categorization)
            {
                this
                    ._context
                    .ProductCategorizationHistories
                    .Add(new Domain.Entities.ProductCategorizationHistory
                    {
                        SkuCode = loopItem.Code,
                        OldValue = oldValue,
                        NewValue = loopItem.Categorization,
                        CreatedDate = DateTime.UtcNow
                    });
                this._context.SaveChanges();
            }

            var _ = this
                ._context
                .ProductInformations
                .Where(pi => pi.SkuCode == loopItem.Code)
                .ExecuteUpdate(sp => sp
                    .SetProperty(pi => pi.Categorization, loopItem.Categorization)
                    .SetProperty(pi => pi.SpecialCategorization, loopItem.SpecialCategorization));
        }

        return Json(new { success = true });
    }
    #endregion
}
