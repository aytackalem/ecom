﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class PaymentController : Base.ControllerBase
    {
        #region Fields

        #endregion

        #region Constructors
        public PaymentController( IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields

            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public IActionResult IndexPartial([FromBody] List<Payment> payments)
        {
            var vm = new Application.Panel.ViewModels.Payments.PaymentPartial
            {
                Payments = payments
            };
            return PartialView(vm);
        }
        #endregion
    }
}
