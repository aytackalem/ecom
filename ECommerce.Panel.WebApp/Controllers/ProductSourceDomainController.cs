﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class ProductSourceDomainController : Base.ControllerBase
    {
        #region Fields
        private readonly IProductSourceDomainService _service;
        #endregion

        #region Constructors
        public ProductSourceDomainController(IProductSourceDomainService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.ProductSourceDomains.Update
            {
                ProductSourceDomain = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] ProductSourceDomain dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
