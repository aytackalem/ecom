﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantValueService;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ECommerceVariantValueController : Base.ControllerBase
    {
        #region Fields
        private readonly IECommerceVariantValueService _ecommerceVariantValueService;
        #endregion

        #region Constructors
        public ECommerceVariantValueController(IECommerceVariantValueService ecommerceVariantValueService,  IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._ecommerceVariantValueService = ecommerceVariantValueService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<JsonResult> SearchByName(SearchByNameParameters searchByNameParameters)
        {
            return Json(await this._ecommerceVariantValueService.SearchByNameAsync(searchByNameParameters));
        }
        #endregion
    }
}
