﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class PropertyValueController : Base.ControllerBase
    {
        #region Fields
        private readonly IPropertyValueService _service;
        #endregion

        #region Constructors
        public PropertyValueController( IPropertyValueService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult Create([FromBody] PropertyValue dto)
        {
            return Json(this._service.Create(dto));
        }

        public IActionResult Read(int propertyId)
        {
            return PartialView(new ECommerce.Application.Panel.ViewModels.PropertyValues.Read
            {
                PropertyId = propertyId,
                PropertyValues = this._service.Read(propertyId).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] PropertyValue dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
