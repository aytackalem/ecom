﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Persistence.Panel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions;

[Authorize(Roles = "user,manager")]
[Route("Xml/Definations")]
public class DefinitionsController : Base.ControllerBase
{
    private readonly IBrandService BrandService;

    private readonly DefinitionsService DefinitionsService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public DefinitionsController(IBrandService brandService, DefinitionsService definitionsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        BrandService = brandService;
        DefinitionsService = definitionsService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Xml/Definations/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        var brands = BrandService.ReadAsKeyValue();

        return PartialView("/Themes/Org/Views/Xml/Definations/Create.cshtml", new Create.ViewModel
        {
            Brands = brands.Data
        });
    }

    [HttpPost("Create")]
    public JsonResult Post([FromBody] Create.Command command)
    {
        var response = DefinitionsService.Create(command);

        return Json(response);
    }

    [HttpGet("Update")]
    public IActionResult Update(int id)
    {
        var result = DefinitionsService.Read(id);

        var brands = BrandService.ReadAsKeyValue();

        return PartialView("/Themes/Org/Views/Xml/Definations/Update.cshtml", new Update.ViewModel
        {
            XmlDefination = result.Data,
            Brands = brands.Data
        });
    }

    [HttpPost("Update")]
    public JsonResult Put([FromBody] Update.Command command)
    {
        var response = DefinitionsService.Update(command);

        return Json(response);
    }

    [HttpGet("Listing")]
    public JsonResult Listing(int page, int pageRecordsCount)
    {
        var response = DefinitionsService.Listing(page, pageRecordsCount);

        return Json(response);
    }
}
