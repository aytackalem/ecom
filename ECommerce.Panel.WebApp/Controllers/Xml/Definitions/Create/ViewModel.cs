﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Create;

public class ViewModel
{
    public List<KeyValue<int, string>> Brands { get; set; }
}
