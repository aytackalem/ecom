﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Create;

public record Command(string Name, List<string> Fields, List<int> BrandIds);