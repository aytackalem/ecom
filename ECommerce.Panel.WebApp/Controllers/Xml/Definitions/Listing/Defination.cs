﻿namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Listing;

public record Defination(int Id, string Name, string FileName);