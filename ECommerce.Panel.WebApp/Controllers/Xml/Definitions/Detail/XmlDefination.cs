﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Detail;

public record XmlDefination(int Id, string Name, string FileName, List<string> Fields, List<int> BrandIds);