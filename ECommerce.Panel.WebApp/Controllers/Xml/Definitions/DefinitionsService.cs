﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions;

public class DefinitionsService
{
    ApplicationDbContext _context;

    ITenantFinder _tenantFinder;

    IDomainFinder _domainFinder;

    public DefinitionsService(ApplicationDbContext context, IDomainFinder domainFinder, ITenantFinder tenantFinder)
    {
        _context = context;
        _domainFinder = domainFinder;
        _tenantFinder = tenantFinder;
    }

    public Response Create(Create.Command command)
    {
        return ExceptionHandler.ResultHandle<Response>((response) =>
        {
            if (string.IsNullOrEmpty(command.Name))
            {
                response.Message = "İsim boş olamaz.";
                response.Success = false;

                return;
            }

            if (command.Name.Length < 3 || command.Name.Length > 50)
            {
                response.Message = "İsim en az 3 karakter ve en fazla 50 karakter olabilir.";
                response.Success = false;

                return;
            }

            if (command.Fields is null || command.Fields.Count == 0)
            {
                response.Message = "En az 1 alan seçilmelidir.";
                response.Success = false;

                return;
            }

            if (command.BrandIds is null || command.BrandIds.Count == 0)
            {
                response.Message = "En az 1 marka seçilmelidir.";
                response.Success = false;

                return;
            }

            _context.XmlDefinations.Add(new Domain.Entities.Domainable.XmlDefination
            {
                Name = command.Name,
                FileName = $"{Guid.NewGuid()}.xml",
                Fields = command.Fields,
                DomainId = _domainFinder.FindId(),
                TenantId = _tenantFinder.FindId(),
                XmlDefinationBrands = command.BrandIds.Select(_ => new Domain.Entities.Domainable.XmlDefinationBrand
                {
                    BrandId = _,
                    DomainId = _domainFinder.FindId(),
                    TenantId = _tenantFinder.FindId()
                }).ToList()
            });

            _context.SaveChanges();

            response.Success = true;
            response.Message = "Tanımlama başarılı";
        });
    }

    public Response Update(Update.Command command)
    {
        return ExceptionHandler.ResultHandle<Response>((response) =>
        {
            if (command.Id <= 0)
            {
                response.Message = "Id sıfırdan büyük olmalıdır.";
                response.Success = false;

                return;
            }

            if (string.IsNullOrEmpty(command.Name))
            {
                response.Message = "İsim boş olamaz.";
                response.Success = false;

                return;
            }

            if (command.Name.Length < 3 || command.Name.Length > 50)
            {
                response.Message = "İsim en az 3 karakter ve en fazla 50 karakter olabilir.";
                response.Success = false;

                return;
            }

            if (command.Fields is null || command.Fields.Count == 0)
            {
                response.Message = "En az 1 alan seçilmelidir.";
                response.Success = false;

                return;
            }

            if (command.BrandIds is null || command.BrandIds.Count == 0)
            {
                response.Message = "En az 1 marka seçilmelidir.";
                response.Success = false;

                return;
            }

            var xmlDefination = _context.XmlDefinations.Include(_ => _.XmlDefinationBrands).FirstOrDefault(_ => _.Id == command.Id);

            if (xmlDefination == null)
            {
                response.Message = "Xml Tanımlaması bulunamadı.";
                response.Success = false;

                return;
            }

            xmlDefination.Name = command.Name;
            xmlDefination.Fields = command.Fields;
            xmlDefination.XmlDefinationBrands = command.BrandIds.Select(_ => new Domain.Entities.Domainable.XmlDefinationBrand
            {
                BrandId = _,
                DomainId = _domainFinder.FindId(),
                TenantId = _tenantFinder.FindId()
            }).ToList();

            _context.SaveChanges();

            response.Message = "İşlem başarılı.";
            response.Success = true;
        });
    }

    public DataResponse<Detail.XmlDefination> Read(int id)
    {
        return ExceptionHandler.ResultHandle<DataResponse<Detail.XmlDefination>>((response) =>
        {
            var xmlDefination = _context.XmlDefinations.Include(_ => _.XmlDefinationBrands).FirstOrDefault(_ => _.Id == id);

            if (xmlDefination == null)
            {
                response.Message = "Xml Tanımlaması bulunamadı.";
                response.Success = false;

                return;
            }

            response.Data = new Detail.XmlDefination(xmlDefination.Id, xmlDefination.Name, xmlDefination.FileName, xmlDefination.Fields, xmlDefination.XmlDefinationBrands.Select(_ => _.BrandId).ToList());
        });
    }

    public PagedResponse<Listing.Defination> Listing(int page, int pageRecordsCount)
    {
        return ExceptionHandler.ResultHandle<PagedResponse<Listing.Defination>>((response) =>
        {
            response.RecordsCount = _context.XmlDefinations.Where(x => x.DomainId == _domainFinder.FindId()).Count();

            if (response.RecordsCount > 0)
            {
                response.Data = _context
                    .XmlDefinations
                    .Where(x => x.DomainId == _domainFinder.FindId())
                    .Skip(page * pageRecordsCount)
                    .Take(pageRecordsCount)
                    .Select(_ => new Listing.Defination(_.Id, _.Name, _.FileName))
                    .ToList();
            }

            response.Success = true;
            response.PageRecordsCount = pageRecordsCount;
        });
    }
}
