﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Detail;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Update;

public class ViewModel
{
    public XmlDefination XmlDefination { get; set; }
    
    public List<KeyValue<int, string>> Brands { get; set; }
}
