﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Definitions.Update;

public record Command(int Id, string Name, List<string> Fields, List<int> BrandIds);
