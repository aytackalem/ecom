﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Preview;

public class ViewModel
{
    public string Preview { get; set; }

    public List<string> ElementNames { get; set; }
}
