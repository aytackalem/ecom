﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations;

[Authorize(Roles = "user,manager")]
[Route("Xml/Integrations")]
public class IntegrationsController : Base.ControllerBase
{
    private readonly IntegrationsService IntegrationsService;

    private readonly IDomainService DomainService;

    private readonly IDomainFinder DomainFinder;

    private readonly ICompanyFinder CompanyFinder;

    private readonly INotificationService NotificationService;

    private readonly ICookieHelper CookieHelper;

    public IntegrationsController(IntegrationsService integrationsService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
    {
        IntegrationsService = integrationsService;
        DomainService = domainService;
        DomainFinder = domainFinder;
        CompanyFinder = companyFinder;
        NotificationService = notificationService;
        CookieHelper = cookieHelper;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View("/Themes/Org/Views/Xml/Integrations/Index.cshtml");
    }

    [HttpGet("Create")]
    public async Task<IActionResult> Create()
    {
        return PartialView("/Themes/Org/Views/Xml/Integrations/Create.cshtml");
    }

    [HttpPost("Create")]
    public JsonResult Post([FromBody] Create.Command command)
    {
        return Json(null);
    }

    [HttpPost("Resolve")]
    public async Task<JsonResult> Post([FromBody] Resolve.Command command)
    {
        return Json(await IntegrationsService.ResolveAsync(command));
    }

    [HttpPost("Preview")]
    public async Task<JsonResult> Preview([FromBody] Resolve.Command command)
    {
        return Json(await IntegrationsService.PreviewAsync(command));
    }

    [HttpPost("Test")]
    public async Task<IActionResult> Test([FromBody] Test.Command command)
    {
        var products = await IntegrationsService.TestAsync(command);

        return PartialView("/Themes/Org/Views/Xml/Integrations/Test.cshtml", products.Data);
    }

    [HttpGet("Listing")]
    public JsonResult Listing(int page, int pageRecordsCount)
    {
        return Json(new Application.Common.Wrappers.PagedResponse<int>
        {
            Data = new System.Collections.Generic.List<int>(),
            Page = 0,
            PageRecordsCount = 10,
            RecordsCount = 0,
            Success = true
        });
    }
}
