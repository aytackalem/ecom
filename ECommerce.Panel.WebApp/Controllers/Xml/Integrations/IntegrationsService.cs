﻿using ECommerce.Application.Common.DataTransferObjects.XmlService;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Persistence.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations;

public class IntegrationsService
{
    ApplicationDbContext _context;

    ITenantFinder _tenantFinder;

    IDomainFinder _domainFinder;

    public IntegrationsService(ApplicationDbContext context, IDomainFinder domainFinder, ITenantFinder tenantFinder)
    {
        _context = context;
        _domainFinder = domainFinder;
        _tenantFinder = tenantFinder;
    }

    public async Task<DataResponse<Resolve.ViewModel>> ResolveAsync(Resolve.Command command)
    {
        return await ExceptionHandler.ResultHandleAsync<DataResponse<Resolve.ViewModel>>(async (response) =>
        {
            Resolve.ViewModel data = new()
            {
                ElementNames = new()
            };

            using (HttpClient client = new())
            using (Stream stream = await client.GetStreamAsync(command.Url))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                bool foundProduct = false;  // İlk product node'u bulundu mu?

                while (reader.Read())
                {
                    // Eğer "product" elementine geldiyseniz
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == command.ProductElementName)
                    {
                        foundProduct = true;

                        // Şimdi "product" elementinin altındaki tüm node'ları keşfet
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element) // Alt node'ları okuyoruz
                            {
                                if (data.ElementNames.Contains(reader.Name) == false)
                                {
                                    data.ElementNames.Add(reader.Name);
                                }
                            }

                            // Eğer "product" elementinin sonuna geldiyseniz, döngüyü sonlandırın
                            if (reader.NodeType == XmlNodeType.EndElement && reader.Name == command.ProductElementName)
                                break;
                        }
                        break;  // İlk "product" node'una ait bilgileri aldıktan sonra çıkıyoruz
                    }
                }
            }

            response.Data = data;
            response.Success = true;

            return response;

        }, (response, error) =>
        {
            response.Success = false;
            response.Message = "Bilinmeyen bir hata oluştu";
        });
    }

    public async Task<DataResponse<string>> PreviewAsync(Resolve.Command command)
    {
        return await ExceptionHandler.ResultHandleAsync<DataResponse<string>>(async (response) =>
        {
            StringBuilder stringBuilder = new();

            using (HttpClient client = new())
            using (Stream stream = await client.GetStreamAsync(command.Url))
            using (StreamReader reader = new(stream))
            {
                string line;
                int lineCount = 0;

                while ((line = await reader.ReadLineAsync()) != null && lineCount < 200)
                {
                    stringBuilder.AppendLine(line);

                    lineCount++;
                }
            }

            response.Data = stringBuilder.ToString();
            response.Success = true;

            return response;

        }, (response, error) =>
        {
            response.Success = false;
            response.Message = "Bilinmeyen bir hata oluştu";
        });
    }

    public async Task<DataResponse<List<Test.Product>>> TestAsync(Test.Command command)
    {
        return await ExceptionHandler.ResultHandleAsync<DataResponse<List<Test.Product>>>(async (response) =>
        {
            List<Test.Product> products = new();

            int count = 0;

            using (HttpClient client = new())
            using (Stream stream = await client.GetStreamAsync(command.Url))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == command.ProductElementName)
                    {
                        Test.Product product = new();

                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                var xmlSourceMapping = command.Mappings.FirstOrDefault(_ => _.SourceName == reader.Name);
                                if (xmlSourceMapping != null)
                                {
                                    if (xmlSourceMapping.TargetName == "Name")
                                    {
                                        if (reader.Read())
                                        {
                                            product.Name = reader.Value;
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "BrandName")
                                    {
                                        if (reader.Read())
                                        {
                                            product.BrandName = reader.Value;
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "CategoryName")
                                    {
                                        if (reader.Read())
                                        {
                                            product.CategoryName = reader.Value;
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "Description")
                                    {
                                        if (reader.Read())
                                        {
                                            product.Description = reader.Value;
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "ListUnitPrice")
                                    {
                                        if (reader.Read())
                                        {
                                            product.ListUnitPrice = decimal.Parse(reader.Value);
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "UnitPrice")
                                    {
                                        if (reader.Read())
                                        {
                                            product.UnitPrice = decimal.Parse(reader.Value);
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "Photos")
                                    {
                                        if (reader.Read())
                                        {
                                            product.Photos ??= new();
                                            product.Photos.Add(new Test.Photo
                                            {
                                                Url = reader.Value
                                            });
                                        }
                                    }
                                    else if (xmlSourceMapping.TargetName == "Variants")
                                    {
                                        var childXmlSourceMappings = (xmlSourceMapping as Test.ParentMapping).SingleMappings;

                                        Test.Variant variant = new();

                                        while (reader.Read())
                                        {
                                            if (reader.NodeType == XmlNodeType.Element)
                                            {
                                                var childXmlSourceMapping = childXmlSourceMappings.FirstOrDefault(_ => _.SourceName == reader.Name);
                                                if (childXmlSourceMapping != null)
                                                {
                                                    if (childXmlSourceMapping.TargetName == "Name")
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            variant.Name = reader.Value;
                                                        }
                                                    }
                                                    else if (childXmlSourceMapping.TargetName == "Value")
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            variant.Value = reader.Value;
                                                        }
                                                    }
                                                }
                                            }

                                            if (reader.NodeType == XmlNodeType.EndElement && reader.Name == xmlSourceMapping.SourceName)
                                                break;
                                        }

                                        product.Variants ??= new();
                                        product.Variants.Add(variant);
                                    }
                                }
                            }

                            if (reader.NodeType == XmlNodeType.EndElement && reader.Name == command.ProductElementName)
                                break;
                        }

                        products.Add(product);

                        count++;

                        if (count == 4)
                        {
                            break;
                        }
                    }
                }
            }

            response.Data = products;
            response.Success = true;

            return response;

        }, (response, error) =>
        {
            response.Success = false;
            response.Message = "Bilinmeyen bir hata oluştu";
        });
    }
}
