﻿using System.Text.Json.Serialization;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Test;

[JsonDerivedType(typeof(SingleMapping), typeDiscriminator: "single")]
[JsonDerivedType(typeof(ParentMapping), typeDiscriminator: "parent")]
public abstract class MappingBase
{
    public string SourceName { get; set; }

    public string TargetName { get; set; }
}
