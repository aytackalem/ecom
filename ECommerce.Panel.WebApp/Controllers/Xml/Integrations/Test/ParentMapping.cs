﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Test;

public class ParentMapping : MappingBase
{
    public List<SingleMapping> SingleMappings { get; set; }
}
