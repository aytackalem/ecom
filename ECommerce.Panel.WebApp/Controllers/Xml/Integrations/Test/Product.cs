﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Test;

public class Product
{
    public string Name { get; set; }

    public string BrandName { get; set; }

    public string CategoryName { get; set; }

    public string Barcode { get; set; }

    public string StockCode { get; set; }

    public int Stock { get; set; }

    public decimal ListUnitPrice { get; set; }

    public decimal UnitPrice { get; set; }

    public decimal VatRate { get; set; }

    public string Description { get; set; }

    public List<Photo> Photos { get; set; }

    public List<Variant> Variants { get; set; }
}
