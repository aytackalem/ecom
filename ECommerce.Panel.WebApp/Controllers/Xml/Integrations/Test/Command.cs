﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Test;

public class Command
{
    public string Url { get; set; }

    public string ProductElementName { get; set; }

    public List<MappingBase> Mappings { get; set; }
}
