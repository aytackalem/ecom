﻿namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Test;

public class Variant
{
    public string Name { get; set; }

    public string Value { get; set; }
}
