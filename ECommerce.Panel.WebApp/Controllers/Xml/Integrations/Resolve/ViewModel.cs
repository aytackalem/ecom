﻿using System.Collections.Generic;

namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Resolve;

public class ViewModel
{
    public List<string> ElementNames { get; set; }
}
