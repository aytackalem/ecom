﻿namespace ECommerce.Panel.WebApp.Controllers.Xml.Integrations.Resolve;

public class Command
{
    public string Url { get; set; }

    public string ProductElementName { get; set; }
}
