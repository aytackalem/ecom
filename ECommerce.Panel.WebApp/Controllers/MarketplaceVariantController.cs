﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceVariantController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplaceVariantService _marketplaceVariantService;
        #endregion

        #region Constructors
        public MarketplaceVariantController(IMarketplaceVariantService marketplaceVariantService,  IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._marketplaceVariantService = marketplaceVariantService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public JsonResult Read()
        {
            return Json(this._marketplaceVariantService.Read());
        }
        #endregion
    }
}