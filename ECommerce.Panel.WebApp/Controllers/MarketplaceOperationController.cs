﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceOperations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceOperationController : Base.ControllerBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Marketplaces.IMarketplaceService _marketplaceService;
        #endregion

        #region Constructors
        public MarketplaceOperationController( IMarketplaceCompanyService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, Application.Common.Interfaces.Marketplaces.IMarketplaceService marketplaceService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._marketplaceService = marketplaceService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult SelectionPartial(string marketplaceId, string packageNumber)
        {
            var response = this._marketplaceService.GetChangableCargoCompanies(marketplaceId, packageNumber);
            return PartialView(response);
        }

        [HttpPost]
        public JsonResult Change([FromBody] ChangeRequest changeRequest)
        {
            var response = this._marketplaceService.ChangeCargoCompany(changeRequest.MarketplaceId, changeRequest.MarketplaceOrderNumber, changeRequest.PackageNumber, changeRequest.CargoCompanyId);
            return Json(response);
        }

        [HttpPost]
        public JsonResult Unpack([FromBody] ChangeRequest changeRequest)
        {
            var response = this._marketplaceService.Unpack(changeRequest.MarketplaceId, changeRequest.MarketplaceOrderNumber, changeRequest.PackageNumber);
            return Json(response);
        }
        #endregion
    }
}
