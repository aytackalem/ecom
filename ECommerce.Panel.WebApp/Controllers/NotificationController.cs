﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class NotificationController : Base.ControllerBase
    {
        #region Fields
        private readonly IRazorViewEngine _razorViewEngine;

        private readonly IServiceProvider _serviceProvider;

        private readonly ITempDataProvider _tempDataProvider;

        private readonly INotificationService _service;

        private readonly IMarketplaceService _marketPlaceService;

        #endregion

        #region Constructors
        public NotificationController(IRazorViewEngine razorViewEngine, IServiceProvider serviceProvider, ITempDataProvider tempDataProvider, INotificationService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper, IMarketplaceService marketPlaceService) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            #region Fields
            this._razorViewEngine = razorViewEngine;
            this._serviceProvider = serviceProvider;
            this._tempDataProvider = tempDataProvider;
            this._service = service;
            this._marketPlaceService = marketPlaceService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult Index()
        {
            var vm = new ECommerce.Application.Panel.ViewModels.Notifications.Index
            {
                NotificationTypes = _service.NotificationTypes().Data,
                Marketplaces = _marketPlaceService.ReadAsKeyValue().Data
            };

            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> Read(PagedRequest pagedRequest)
        {
            return Json(await this._service.Read(pagedRequest));
        }


        public async Task<IActionResult> IndexPartial(string marketplaceRequestTypeId)
        {
            var notification = await this._service.ReadAsync(new Application.Common.Parameters.Notification.GetParameter());

            var vm = new Application.Panel.ViewModels.Notifications.Index
            {
                NotificationItems = notification.Data 
            };

            return PartialView(vm);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(string marketplaceRequestTypeId, string id)
        {
            return Json(await this._service.Delete(marketplaceRequestTypeId, id));
        }

        [HttpPost]
        public JsonResult Ignore(int productInformationMarketplaceId)
        {
            return Json(this._service.Ignore(productInformationMarketplaceId));
        }
        #endregion

        #region Helper Methods
        private async Task<string> RenderToStringAsync(string viewName, object model)
        {
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }
        #endregion
    }
}
