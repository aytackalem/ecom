﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class ShoppingCartDiscountedProductController : Base.ControllerBase
    {
        #region Fields
        private readonly IShoppingCartDiscountedProductService _service;

        private readonly IProductService _productService;

        private readonly IApplicationService _applicationService;
        #endregion

        #region Constructors
        public ShoppingCartDiscountedProductController( IShoppingCartDiscountedProductService service, IProductService productService, IApplicationService applicationService,IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._productService = productService;
            this._applicationService = applicationService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.ShoppingCartDiscountedProducts.Create
            {
                Products = this._productService.ReadAsKeyValue().Data,
                Applications = this._applicationService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Create([FromBody] ShoppingCartDiscountedProduct dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.ShoppingCartDiscountedProducts.Update
            {
                ShoppingCartDiscountedProduct = this._service.Read(id).Data,
                Products = this._productService.ReadAsKeyValue().Data,
                Applications = this._applicationService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] ShoppingCartDiscountedProduct dto)
        {
            return Json(this._service.Update(dto));
        }

        [HttpPost]
        public JsonResult Delete([FromBody] ShoppingCartDiscountedProduct dto)
        {
            return Json(this._service.Delete(dto));
        }
        #endregion
    }
}
