﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class MarketplaceBrandMappingController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplaceService _marketplaceService;

        private readonly IMarketplaceBrandMappingService _service;

        private readonly IBrandService _brandService;
        #endregion

        #region Constructors
        public MarketplaceBrandMappingController( IBrandService brandService, IMarketplaceService marketPlaceService, IMarketplaceBrandMappingService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder,  INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._marketplaceService = marketPlaceService;
            this._service = service;
            this._brandService = brandService;
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandId">Kategoriye ait id bilgisi.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> IndexPartial(int brandId)
        {
            var marketplaceResponse = this._marketplaceService.Read();
            if (marketplaceResponse.Success == false)
            {
                return PartialView(new Application.Panel.ViewModels.MarketplaceBrandMappings.IndexPartial
                {
                    Success = marketplaceResponse.Success,
                    Message = marketplaceResponse.Message
                });
            }

            var marketplaceBrandMappingResponse = await this._service.ReadOrGenerateAsync(brandId, marketplaceResponse.Data);

            var brand = _brandService.Read(brandId);

            var vm = new Application.Panel.ViewModels.MarketplaceBrandMappings.IndexPartial
            {
                BrandId = brandId,
                BrandName = brand.Data.Name,
                Marketplaces = marketplaceResponse.Data,
                MarketplaceBrandMappings = marketplaceBrandMappingResponse.Data
            };

            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Upsert([FromBody] UpsertRequest upsertRequest)
        {
            return Json(this._service.Upsert(upsertRequest));
        }
        #endregion
    }
}
