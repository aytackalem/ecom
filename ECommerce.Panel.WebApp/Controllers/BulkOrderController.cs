﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class BulkOrderController : Base.ControllerBase
    {
        #region Fields
        private readonly IOrderService _service;

        private readonly ISettingService _settingService;

        private readonly IOrderSourceService _orderSourceService;

        private readonly IOrderTypeService _orderTypeService;

        private readonly IMarketplaceService _marketPlaceService;

        private readonly ICategoryService _categoryService;

        #endregion

        public BulkOrderController(IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService, ICookieHelper cookieHelper, IOrderService service, ISettingService settingService, IOrderSourceService orderSourceService, IOrderTypeService orderTypeService, IMarketplaceService marketPlaceService, ICategoryService categoryService) : base(domainService, domainFinder, companyFinder, notificationService, cookieHelper)
        {
            this._service = service;
            this._settingService = settingService;
            this._orderSourceService = orderSourceService;
            this._orderTypeService = orderTypeService;
            this._marketPlaceService = marketPlaceService;
            this._categoryService = categoryService;
        }

        public IActionResult Index()
        {
            return View(new ECommerce.Application.Panel.ViewModels.BulkOrders.Index
            {
                OrderSources = _orderSourceService.ReadAsKeyValue().Data,
                OrderTypes = _orderTypeService.ReadAsKeyValue().Data,
                MarketPlaces = _marketPlaceService.ReadAsKeyValue().Data,
                ShowStock = this._settingService.StockControl,
                Categories = this._categoryService.ReadAsKeyValue().Data
            });
        }

        [HttpGet]
        public FileResult Print([FromQuery] string search)
        {
            return File(this._service.BulkOrderPdf(search).Data, "application/pdf");
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            var response = this._service.BulkOrderRead(pagedRequest);
            return Json(response);
        }

    }
}
