﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class CompanyController : Base.ControllerBase
    {
        #region Fields
        private readonly ICompanyService _service;
        #endregion

        #region Constructors
        public CompanyController( ICompanyService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.Companies.Create()
            {
                Company = new()
                {
                    CompanyConfigurations = new()
                    {
                        new () { Key = "FacebookUrl", Name = "Facebook Adresi" },
                        new () { Key = "InstagramUrl", Name = "Instagram Adresi" },
                        new () { Key = "YoutubeUrl", Name = "Youtube Adresi" },
                        new () { Key = "IbanBank", Name = "Havale Yapılacak Banka" },
                        new () { Key = "Iban", Name = "Iban Numarası" },
                    }
                }
            });
        }

        [HttpPost]
        public JsonResult Create([FromBody] Company dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var vm = new Application.Panel.ViewModels.Companies.Update
            {
                Company = this._service.Read(id).Data
            };

            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Update([FromBody] Company dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
