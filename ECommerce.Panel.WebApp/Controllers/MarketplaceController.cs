﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "manager")]
    public class MarketplaceController : Base.ControllerBase
    {
        #region Fields
        private readonly IMarketplaceCompanyService _service;
        #endregion

        #region Constructors
        public MarketplaceController( IMarketplaceCompanyService service, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView(new Application.Panel.ViewModels.Marketplaces.Update
            {
                MarketPlaceCompany = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] MarketplaceCompany dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
