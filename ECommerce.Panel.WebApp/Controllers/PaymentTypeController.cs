﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class PaymentTypeController  : Base.ControllerBase
    {
        #region Fields
        private readonly IPaymentTypeService _service;

        private readonly ILanguageService _languageService;
        #endregion

        #region Constructors
        public PaymentTypeController(IPaymentTypeService service, ILanguageService languageService,IDomainService domainService, IDomainFinder domainFinder,ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._languageService = languageService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView(new Application.Panel.ViewModels.PaymentTypes.Create
            {
                Languages = this._languageService.ReadAsKeyValue().Data
            });
        }

        [HttpPost]
        public JsonResult Create([FromBody] PaymentType dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(string id)
        {
            return PartialView(new Application.Panel.ViewModels.PaymentTypes.Update
            {
                Languages = this._languageService.ReadAsKeyValue().Data,
                PaymentType = this._service.Read(id).Data
            });
        }

        [HttpPost]
        public JsonResult Update([FromBody] PaymentType dto)
        {
            return Json(this._service.Update(dto));
        }
        #endregion
    }
}
