﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.Panel.WebApp.Controllers
{
    [Authorize(Roles = "user,manager")]
    public class BrandDiscountSettingController : Base.ControllerBase
    {
        #region Fields
        private readonly IBrandDiscountSettingService _service;

        private readonly IApplicationService _applicationService;

        private readonly IBrandService _brandService;
        #endregion

        #region Constructors
        public BrandDiscountSettingController( IBrandDiscountSettingService service, IApplicationService applicationService, IBrandService brandService, IDomainService domainService, IDomainFinder domainFinder, ICompanyFinder companyFinder, INotificationService notificationService,  ICookieHelper cookieHelper) : base( domainService, domainFinder, companyFinder,  notificationService,cookieHelper)
        {
            #region Fields
            this._service = service;
            this._applicationService = applicationService;
            this._brandService = brandService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var vm = new Application.Panel.ViewModels.BrandDiscountSettings.Create
            {
                Applications = this._applicationService.ReadAsKeyValue().Data,
                Brands = this._brandService.ReadAsKeyValue().Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] BrandDiscountSetting dto)
        {
            return Json(this._service.Create(dto));
        }

        public JsonResult Read(PagedRequest pagedRequest)
        {
            return Json(this._service.Read(pagedRequest));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var vm = new Application.Panel.ViewModels.BrandDiscountSettings.Update
            {
                Applications = this._applicationService.ReadAsKeyValue().Data,
                Brands = this._brandService.ReadAsKeyValue().Data,
                BrandDiscountSetting = this._service.Read(id).Data
            };
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult Update([FromBody] BrandDiscountSetting dto)
        {
            return Json(this._service.Update(dto));
        }

        [HttpPost]
        public JsonResult Delete([FromBody] BrandDiscountSetting dto)
        {
            return Json(this._service.Delete(dto));
        }
        #endregion
    }
}
