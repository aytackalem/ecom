﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;

namespace ECommerce.Panel.WebApp
{
    public class TenantViewLocationExpander : IViewLocationExpander
    {
        #region Methods
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            foreach (var vlLoop in viewLocations)
            {
                yield return $"/Themes/Org{vlLoop}";
            }
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {

        }
        #endregion
    }
}
