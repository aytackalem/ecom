﻿using System;
using System.Linq;

namespace ECommerce.Panel.WebApp.Extensions;

public static class SearchExtension
{
    public static T Parse<T>(this string search) where T : new()
    {
        T filter = new();

        if (string.IsNullOrEmpty(search))
            return filter;

        var type = typeof(T);
        var propertyInfos = type.GetProperties();

        var parameters = search.Split('_');
        foreach (var parameter in parameters)
        {
            var parameterParts = parameter.Split(':', StringSplitOptions.RemoveEmptyEntries);
            var parameterName = parameterParts[0];
            var propertyInfo = propertyInfos.FirstOrDefault(_ => _.Name == parameterName);
            if (parameterParts.Length == 2 && propertyInfo is not null)
            {
                if (propertyInfo.PropertyType == typeof(int?))
                {
                    var value = int.Parse(parameterParts[1]);
                    propertyInfo.SetValue(filter, value);
                }
                else if (propertyInfo.PropertyType == typeof(string))
                {
                    propertyInfo.SetValue(filter, parameterParts[1]);
                }
            }
        }

        return filter;
    }
}
