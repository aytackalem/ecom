﻿using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Persistence.Common.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Components
{
    public class LogoViewComponent : ViewComponent
    {
        #region Fields
        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public LogoViewComponent(ISettingService settingService)
        {
            _settingService = settingService;
        }
        #endregion

        #region Methods
        public async Task<IViewComponentResult> InvokeAsync(int? width)
        {
            return View(new string[] {
                this._settingService.CompanyImageUrl,
                width.HasValue ? width.ToString() : "120"
            });
        }
        #endregion
    }
}
