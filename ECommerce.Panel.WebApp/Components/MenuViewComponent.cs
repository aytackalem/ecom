﻿using ECommerce.Application.Common.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ECommerce.Panel.WebApp.Components
{
    public class MenuViewComponent : ViewComponent
    {
        #region Fields
        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public MenuViewComponent(ISettingService settingService)
        {
            _settingService = settingService;
        }
        #endregion

        #region Methods
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(this._settingService);
        } 
        #endregion
    }
}
