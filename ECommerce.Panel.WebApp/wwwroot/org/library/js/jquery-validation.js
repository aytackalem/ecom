﻿"use strict";

const Validation = {
    Valid: function (formId) {
        var valid = true;
        var inputs = $(`#${formId}`).find('[required]');
        for (var i = 0; i < inputs.length; i++) {

            if (inputs.eq(i).data("required-field") != undefined &&
                inputs.eq(i).data("required-field") != '') {

                if (inputs.eq(i).data(inputs.eq(i).data("required-field")) != undefined &&
                    inputs.eq(i).data(inputs.eq(i).data("required-field")) == '') {

                    valid = false;

                    if (inputs.eq(i).hasClass('is-invalid') == false) {


                        var invalidElement = $('<div>');
                        invalidElement.html("Bu alanı doldurmanız gerekli.");
                        invalidElement.addClass('error invalid-feedback');

                        inputs.eq(i).closest('.form-group').append(invalidElement);
                        inputs.eq(i).closest('.form-group').addClass("is-invalid");
                        inputs.eq(i).addClass('is-invalid');

                    }

                    inputs.eq(i).on('blur change', function () {
                        $(this).removeClass('is-invalid');
                        $(this).closest('.form-group').find('.error').remove();
                        $(this).closest('.form-group').find('.is-invalid').removeClass("is-invalid");
                    });

                }

            }
            else if (!inputs.eq(i).prop('disabled') && inputs.eq(i).val() == '') {

                valid = false;

                if (inputs.eq(i).hasClass('is-invalid') == false) {

                    var invalidElement = $('<div>');
                    invalidElement.html("Bu alanı doldurmanız gerekli.");
                    invalidElement.addClass('error invalid-feedback');

                    inputs.eq(i).closest('.form-group').append(invalidElement);
                    inputs.eq(i).closest('.form-group').addClass("is-invalid");
                    inputs.eq(i).addClass('is-invalid');

                }

                inputs.eq(i).on('blur change', function () {
                    $(this).removeClass('is-invalid');
                    $(this).closest('.form-group').find('.error').remove();
                    $(this).closest('.form-group').find('.is-invalid').removeClass("is-invalid");
                });

            }

        }
        return valid;
    }
};