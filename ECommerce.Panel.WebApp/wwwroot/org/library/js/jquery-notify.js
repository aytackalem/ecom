﻿"use strict";

const NotifyType = {
    Info: 2,
    Success: 1,
    Error: 0
};

const Notify = {
    Show: function (notifyType, message) {
        switch (notifyType) {
            case NotifyType.Info:
                toastr.info(message);
                break;
            case NotifyType.Success:
                toastr.success(message);
                break;
            case NotifyType.Error:
                toastr.error(message);
                break;
        }
    }
};