﻿"use strict";




function Modal() {




    /*
     * Helper Methods::Begin 
     */
    var _generateId = function () {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
    /*
     * Helper Methods::Begin 
     */




    /*
     * Instance::Begin 
     */
    const _modal = {
        Open: function (configuration) {
            var id = _generateId();

            var html = `<div class="modal fade ${id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">${configuration.Title}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="button" class="btn btn-primary">${configuration.ButtonText}</button>
            </div>
        </div>
    </div>
</div>`;

            $('body').append(html);

            var $modal = $(`.${id}`);

            HttpClient.Get(configuration.GetUrl, function (response) {
                $modal.modal();
                $modal.on('hidden.bs.modal', function (e) {
                    $modal.modal('dispose');
                    $modal.remove();

                    if (configuration.Closed) {
                        configuration.Closed();
                    }
                });

                $modal.find('.modal-body').eq(0).html(response);
                $modal.find('.modal-footer button').eq(1).on('click', function () {
                    var $form = $modal.find('.modal-body').find('form').eq(0);

                    var valid = ValidationV2.Valid($form);

                    if (valid) {
                        var data = SerializerV2.Serialize($form);

                        HttpClient.Post(configuration.PostUrl, data, response => {

                            if (response.success) {
                                Notify.Show(NotifyType.Success, response.message);

                                $modal.modal('hide');
                            }
                            else
                                Notify.Show(NotifyType.Error, response.message);

                        });
                    }
                    else
                        Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");
                });

                if (configuration.Opened) {
                    configuration.Opened();
                }
            });
        }
    };
    /*
     * Instance::End 
     */




    /*
     * Instance::Begin 
     */
    return _modal;
    /*
     * Instance::End 
     */




}