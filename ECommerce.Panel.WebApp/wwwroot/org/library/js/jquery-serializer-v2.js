﻿"use strict";

const SerializerV2 = (function ($) {

    return {
        Serialize: $form => {
            return $form.serializeJSON({ useIntKeysAsArrayIndex: true });
        }
    }

})(jQuery);