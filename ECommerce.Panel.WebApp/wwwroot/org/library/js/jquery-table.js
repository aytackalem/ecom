﻿const Table = {
    Initialize: function (url, pagedRequest, columns, name, menuItems, updatable, collapsable, childObjects) {
        
        $('.kt-hidden-select2').select2({
            placeholder: "Seçiniz",
            minimumResultsForSearch: Infinity
        });

        var url = `${url}?Page=${pagedRequest.Page}&PageRecordsCount=${pagedRequest.PageRecordsCount}&Search=${pagedRequest.Search}&Sort=${pagedRequest.Sort}&Active=${pagedRequest.Active}`;

        HttpClient.Get(url, function (response) {
            if (response.success) {

                let tbodyHtml = '';

                if (collapsable == true) {

                    //Parent object
                    $.each(response.data, (index, value) => {

                        var childObjectsLength = value[childObjects]['length'];
                        var objectId = ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));

                        $.each(value[childObjects], (childIndex, childValue) => {

                            let c = $.map(columns, (column, i) => {

                                if (column.Unified == true && childIndex != 0) {
                                    return '';
                                }

                                return `
<td  ${column.Padding != undefined ? ` style="padding: ${column.Padding};"` : ''} ${column.Unified == true ? `rowspan="1" class="stckTd"`  : ''} >
   <div class="stckd"> ${Table.RenderColumn(column.Render ? column.Render(childValue, value, objectId) : childValue[column.Name], column.Type)}</div>
</td>`
                            });

                            if (childIndex == 0) {
                                let mi = $.map(menuItems, (menuItem, i) => menuItem.Render(value));

                                (menuItems && menuItems.length > 0) || updatable ?
                                    c.push(`
<td>
    <span class="dropdown">
        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true" title="güncelle">
            <i class="la la-ellipsis-h"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            ${updatable ? `
            <a class="dropdown-item" href="javascript:${name}Index.OpenUpdateModal('${value["id"]}')">
                <i class="la la-edit"></i> Güncelle
            </a>` : ""}
                                ${mi.join('')}
        </div>
    </span>
</td>`) : "";
                            }
                            else {
                                c.push("<td></td>");
                            }

                            tbodyHtml += `<tr data-object-id="${objectId}" ${childIndex == 0 ? `data-child-objects-count="${childObjectsLength}"` : 'style="display: none;"'}>${c.join('')}</tr>`;

                        });

                    });
                }
                else {
                    $.each(response.data, (index, value) => {
                        let c = $.map(columns, (column, i) => `<td${column.Padding != undefined ? ` style="padding: ${column.Padding};"` : ''}>${Table.RenderColumn(column.Render ? column.Render(value) : value[column.Name], column.Type)}</td>`);
                        let mi = $.map(menuItems, (menuItem, i) => menuItem.Render(value));

                        (menuItems && menuItems.length > 0) || updatable ?
                            c.push(`<td><span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true" title="güncelle">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
${updatable ? `<a class="dropdown-item" href="javascript:${name}Index.OpenUpdateModal('${value["id"]}')"><i class="la la-edit"></i> Güncelle</a>` : ""}
                                
                                ${mi.join('')}
                            </div>
                        </span></td>`) : "";

                        tbodyHtml += `<tr>${c.join('')}</tr>`;
                    });
                }

                let paginationHtml = '';
                const pageCounts = Math.ceil(response.recordsCount / response.pageRecordsCount);
                if (pageCounts > 5) {
                    paginationHtml += `<li class="kt-pagination__link--first"><a href="javascript:${name}Index.Read(0)"><i class="fa fa-angle-double-left kt-font-brand"></i></a></li>`;
                }

                for (var i = 0; i < pageCounts; i++) {
                    let begin = 0;
                    if (pageCounts > 10) {
                        begin = response.page;

                        if (response.page > 5) {
                            begin -= 5;
                            if (pageCounts - (response.page + 1) < 5) {
                                begin -= 5 - (pageCounts - response.page);
                            }
                        }
                        else {
                            begin = 0;
                        }
                    }

                    if (begin <= i && (begin + 10) > i) {
                        paginationHtml += `<li class="${i == response.page ? "kt-pagination__link--active" : ""}"><a href="javascript:${name}Index.Read(${i})">${i + 1}</a></li>`;
                    }
                }

                if (pageCounts > 5) {
                    paginationHtml += `<li class="kt-pagination__link--last"><a href="javascript:${name}Index.Read(${pageCounts - 1})"><i class="fa fa-angle-double-right kt-font-brand"></i></a></li>`;
                }

                $('#ajax_data').html(tbodyHtml);
                $('.kt-pagination__links').html(paginationHtml);
                $('.pagination__desc').html(`Toplam ${response.recordsCount} kayıttan ${response.page * response.pageRecordsCount} ile ${(response.page * response.pageRecordsCount) + response.pageRecordsCount} arası gösterilliyor.`);

                KTApp.initTooltips()
            }
        });
    },
    RenderColumn: function (value, type) {
        switch (type) {
            case 'bool':
                return `<span class="kt-badge  kt-badge--${value ? "success" : "danger"} kt-badge--inline kt-badge--pill">${value ? "Evet" : "Hayır"}</span>`;
            case 'image':
                return `<img src="${value}" alt="${value}"/>`;
            case 'detail':
                return `<img src="${value}" alt="${value}"/>`;
            case 'check':
                return `<label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="${value}">
                            <span></span>
                        </label>`;
            case 'decimal':
                return `<span style="font-weight: 500;">${value.toLocaleString('tr-TR')}</span>`;
            default:
                return value;
        }
    },
    CheckAll: function () {
        $('.kt-checkbox [type="checkbox"]').prop('checked', true);
    },
    Collapse: function (objectId) {
        
        var childObjectsCount = $('tr[data-object-id="' + objectId + '"]').eq(0).data('child-objects-count');

        var rowSpan = $('tr[data-object-id="' + objectId + '"]').eq(0).find('[rowspan]').attr('rowspan');
        if (rowSpan == childObjectsCount) {
            $('tr[data-object-id="' + objectId + '"]').not(':eq(0)').hide();
            $('tr[data-object-id="' + objectId + '"]').eq(0).find('[rowspan]').attr('rowspan', "1");
            $(`#${objectId}-button`).attr('class', 'btn btn-xs btn-outline-brand btn-elevate btn-circle btn-icon');
            $(`#${objectId}-icon`).attr('class', 'la la-angle-down');
        }
        else {
            $('tr[data-object-id="' + objectId + '"]').show();
            $('tr[data-object-id="' + objectId + '"]').eq(0).find('[rowspan]').attr('rowspan', childObjectsCount);
            $(`#${objectId}-button`).attr('class', 'btn btn-brand btn-elevate btn-circle btn-icon');
            $(`#${objectId}-icon`).attr('class', 'la la-angle-up');
        }

    }
};