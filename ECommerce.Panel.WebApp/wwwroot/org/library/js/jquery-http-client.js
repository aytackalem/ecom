﻿"use strict";

const HttpClient = {
    Post: function (url, data, success, error, blockElement) {

        blockElement
            ? KTApp.blockPage(`#${blockElement}`, {
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            })
            : KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            });

        $.post({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (response) {
                if (success) {
                    success(response);
                }

                KTApp.unblockPage();
            },
            error: function (exception) {
                if (error) {
                    error(exception);
                }

                KTApp.unblockPage();
            }
        });
    },
    Put: function (url, data, success, error, blockElement) {

        blockElement
            ? KTApp.blockPage(`#${blockElement}`, {
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            })
            : KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            });

        $.ajax({
            method: 'PUT',
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (response) {
                if (success) {
                    success(response);
                }

                KTApp.unblockPage();
            },
            error: function (exception) {
                if (error) {
                    error(exception);
                }

                KTApp.unblockPage();
            }
        });
    },
    PostFile: function (url, data, success, error, blockElement) {

        blockElement
            ? KTApp.blockPage(`#${blockElement}`, {
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            })
            : KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            });

        $.post({
            url: url,
            data: data,
            contentType: false,
            processData: false,
            success: function (response) {
                if (success) {
                    success(response);
                }

                KTApp.unblockPage();
            },
            error: function (exception) {
                if (error) {
                    error(exception);
                }

                KTApp.unblockPage();
            }
        });
    },
    Get: function (url, success, error, blockElement) {

        blockElement
            ? KTApp.blockPage(`#${blockElement}`, {
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            })
            : KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Lütfen bekleyiniz...'
            });

        $.get({
            url: url,
            success: function (response) {
                if (success) {
                    success(response);
                }

                KTApp.unblockPage();
            },
            error: function (exception) {
                if (error) {
                    error(exception);
                }

                KTApp.unblockPage();
            }
        });
    }
};