﻿"use strict";

var swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
})

var SwalAlertModal = {
    ConfirmPopup: function (title, text, confirmButtonText, showCancelButton, cancelButtonText, confirmFunction) {
        debugger
        swalWithBootstrapButtons.fire({
            title: !title ? "Mesaj Kutusu" : title,
            text: !text ? "Örnek İçerik" : text,
            showCancelButton: showCancelButton,
            confirmButtonText: !confirmButtonText ? "Evet" : confirmButtonText,
            cancelButtonText: !cancelButtonText ? "Hayır" : cancelButtonText,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                confirmFunction();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                
            }
        })

    },

    ConfirmPopupWithId: function (title, text, confirmButtonText, showCancelButton, cancelButtonText, confirmFunction, confirmId, cancelFunction, cancelId) {

        swalWithBootstrapButtons.fire({
            title: !title ? localize['MessageBox'] : title,
            text: !text ? localize['Content'] : text,
            showCancelButton: showCancelButton,
            confirmButtonText: !confirmButtonText ? localize['Yes'] : confirmButtonText,
            cancelButtonText: !cancelButtonText ? localize['No'] : cancelButtonText,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                confirmFunction(confirmId);
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                cancelFunction(cancelId);
            }
        })

    },

}