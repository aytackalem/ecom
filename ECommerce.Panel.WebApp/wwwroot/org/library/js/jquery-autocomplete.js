﻿"use strict";

const Autocomplete = {
    Init: function ($input, url, take, params, filter, displayKey, handlebar, select, open, close) {

        var bloodhound = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: `${url}?take=${take}&q=%QUERY${params && params.length > 0 ? `&${$.map(params, p => `${p.key}=${p.value}`).join('&')}` : ''}`,
                wildcard: '%QUERY',
                filter: r => $.map(r.data, function (i) {
                    return filter(i);
                })
            }
        });

        bloodhound.initialize();

        $input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            displayKey: displayKey,
            source: bloodhound.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message" style="padding: 10px 15px; text-align: center;">',
                    'Sonuç bulunamadı.',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(handlebar ? handlebar : `<div>{{${displayKey}}}</div>`)
            }
        });

        $input.bind('typeahead:select', function (ev, suggestion) {
            if (select)
                select(suggestion);
        });

        $input.bind('typeahead:open', function (ev) {
            if (open)
                open();
        });

        $input.bind('typeahead:autocomplete', function (ev, suggestion) {
        });

        $input.bind('typeahead:cursorchange', function (ev, suggestion) {
        });

        $input.bind('typeahead:change', function (ev, suggestion) {
        });

        $input.bind('typeahead:idle', function (ev, suggestion) {
        });

        $input.bind('typeahead:close', function (ev, suggestion) {
            if (close) {
                close();
            }
        });

    },
    InitRemote: function ($input, url, params, filter, displayKey, handlebar, select, open, close) {

        var bloodhound = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: `${url}?Name=%QUERY${params && params.length > 0 ? `&${$.map(params, p => `${p.key}=${p.value}`).join('&')}` : ''}`,
                wildcard: '%QUERY',
                filter: r => $.map(r.data.I, function (i) {
                    return filter(i);
                })
            }
        });

        bloodhound.initialize();

        $input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            displayKey: displayKey,
            limit: 10,
            source: bloodhound.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message" style="padding: 10px 15px; text-align: center;">',
                    'Sonuç bulunamadı.',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(handlebar ? handlebar : `<div>{{${displayKey}}}</div>`)
            }
        });

        $input.bind('typeahead:select', function (ev, suggestion) {
            if (select)
                select(suggestion);
        });

        $input.bind('typeahead:open', function (ev) {
            if (open)
                open();
        });

        $input.bind('typeahead:autocomplete', function (ev, suggestion) {
            debugger
        });

        $input.bind('typeahead:cursorchange', function (ev, suggestion) {
        });

        $input.bind('typeahead:change', function (ev, suggestion) {

        });

        $input.bind('typeahead:idle', function (ev, suggestion) {
        });

        $input.bind('typeahead:close', function (ev, suggestion) {
            if (close) {
                close();
            }
        });

    },
    InitLocal: function ($input, data, displayKey, handlebar, select, open, close) {

        var bloodhound = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: data
        });

        bloodhound.initialize();

        $input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            displayKey: displayKey,
            source: bloodhound.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message" style="padding: 10px 15px; text-align: center;">',
                    'Sonuç bulunamadı.',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(handlebar ? handlebar : `<div>{{${displayKey}}}</div>`)
            }
        });

        $input.bind('typeahead:select', function (ev, suggestion) {
            if (select)
                select(suggestion);
        });

        $input.bind('typeahead:open', function (ev) {
            if (open)
                open();
        });

        $input.bind('typeahead:autocomplete', function (ev, suggestion) {
        });

        $input.bind('typeahead:cursorchange', function (ev, suggestion) {
        });

        $input.bind('typeahead:change', function (ev, suggestion) {
        });

        $input.bind('typeahead:idle', function (ev, suggestion) {
        });

        $input.bind('typeahead:close', function (ev, suggestion) {
            if (close)
                close();
        });

    }
};