﻿"use strict";




const DataTableColumnTypes = {
    Int: 0,
    String: 1,
    Bool: 2,
    Decimal: 3,
    Menu: 4
};




function DataTableColumn(configuration) {




    /*
     * Instance::Begin 
     */
    const _dataTableColumn = {
        Render: function (theData) {
            var dataTableRowsHtml = _configuration
                .DataTableRows
                .map((theDataTableRow, i) => {
                    return theDataTableRow.Render();
                })
                .join();

            var dataTableColumnHtml = '';
            switch (_configuration.Type) {
                case DataTableColumnTypes.Int:
                    dataTableColumnHtml = theData[_configuration.Name];
                    break;
                case DataTableColumnTypes.String:
                    dataTableColumnHtml = theData[_configuration.Name];
                    break;
                case DataTableColumnTypes.Bool:
                    dataTableColumnHtml = `<span class="kt-badge  kt-badge--${theData[_configuration.Name] ? 'success' : 'danger'} kt-badge--inline kt-badge--pill">${theData[_configuration.Name] ? 'Evet' : 'Hayır'}</span>`;
                    break;
                default:
                    dataTableColumnHtml = theData[_configuration.name];
                    break;
            }

            return `${dataTableRowsHtml}<td>${dataTableColumnHtml}</td>`;
        }
    };
    /*
     * Instance::End 
     */




    /*
     * Configuration::Begin 
     */
    let _configuration = {
        Name: '',
        Type: DataTableColumnTypes.String,
        DataTableRows: []
    };

    /* Merge Configuration */
    Object.assign(_configuration, configuration);
    /*
     * Configuration::End 
     */




    /*
     * Instance::Begin 
     */
    return _dataTableColumn;
    /*
     * Instance::End 
     */




}




function DataTableRowMenu(configuration) {




    /*
     * Instance::Begin 
     */
    const _dataTableRowMenu = {
        Render: function (theData) {
            return `<td>
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true" title="İşlemler">
                                <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
${_configuration.Items.map(theItem => `<a class="dropdown-item" href="${theItem.Click.replace('#id', theData['id'])}"><i class="${theItem.Icon}"></i> ${theItem.Name}</a>`).join('')}
                            </div>
                        </span>
                    </td>`;
        }
    };
    /*
     * Instance::End 
     */




    /*
     * Configuration::Begin 
     */
    let _configuration = {
        Items: []
    };

    /* Merge Configuration */
    Object.assign(_configuration, configuration);
    /*
     * Configuration::End 
     */




    /*
     * Instance::Begin 
     */
    return _dataTableRowMenu;
    /*
     * Instance::End 
     */




}




function DataTableRow(configuration) {




    /*
     * Instance::Begin 
     */
    const _dataTableRow = {
        Render: function (theData) {
            return `<tr>${_configuration
                .DataTableColumns
                .map((theDataTableColumn) => {
                    return theDataTableColumn.Render(theData);
                })
                .join('')}${_configuration.DataTableRowMenu.Render(theData)}</tr>`;
        }
    };
    /*
     * Instance::End 
     */




    /*
     * Configuration::Begin 
     */
    let _configuration = {
        DataTableColumns: [],
        DataTableRowMenu: null
    };

    /* Merge Configuration */
    Object.assign(_configuration, configuration);
    /*
     * Configuration::End 
     */




    /*
     * Instance::Begin 
     */
    return _dataTableRow;
    /*
     * Instance::End 
     */




}




function DataTable(configuration) {




    /*
     * Instance::Begin 
     */

    var _dataTable = {
        GetData: function () {
            _getData();
        },
        SetPage: function (page) {
            return _pagedRequest.page = page;
        }
    }

    /*
     * Instance::Begin 
     */




    /* 
     * Configuration::Begin 
     */

    var _configuration = {
        BaseUrl: '',
        Name: '',
        Pagination: {
            Click: ''
        },
        TBodyClassName: 'dt-data',
        PageRecordsCountInputClassName: 'dt-page-records-count',
        DescriptionElementClassName: 'dt-description',
        PaginationElementClassName: 'dt-pagination',
        Creatable: true,
        DataTableRow: []
    };

    /* Merge configuration */
    Object.assign(_configuration, configuration);

    var _pagedRequest = {
        page: 0,
        pageRecordsCount: function () { return $pageRecordsCountSelects.val() }
    };

    /*
     * Configuration::End 
     */




    /*
     * Fields::Begin 
     */

    var $pageRecordsCountSelects = $(`select.${_configuration.PageRecordsCountInputClassName}`);
    $pageRecordsCountSelects.change(function () {
        let $this = $(this);
        $pageRecordsCountSelects.val($this.val());

        $pageRecordsCountSelects.selectpicker('refresh');
    });
    $pageRecordsCountSelects.selectpicker();

    /*
     * Fields::Begin 
     */




    /*
     * Helper Methods::Begin 
     */
    var _readUrl = function () { return `${_configuration.BaseUrl}/Read?Page=${_pagedRequest.page}&PageRecordsCount=${_pagedRequest.pageRecordsCount()}` }

    var _getData = function () {
        HttpClient.Get(_readUrl(), response => {
            if (response.success) {
                $(`.${_configuration.TBodyClassName}`).html($.map(response.data, theData => _configuration.DataTableRow.Render(theData)).join(''));

                let paginationHtml = '';
                for (var i = 0; i < response.pagesCount; i++) {
                    paginationHtml += `<li class="${i == response.page ? 'kt-pagination__link--active' : ''}"><a href="${_configuration.Pagination.Click.replace('#index', i)}">${i + 1}</a></li>`;
                }
                $(`.${_configuration.PaginationElementClassName}`).html(paginationHtml);

                $(`.${_configuration.DescriptionElementClassName}`).html(`Toplam ${response.recordsCount} kayıttan ${response.page * response.pageRecordsCount} ile ${(response.page * response.pageRecordsCount) + response.pageRecordsCount} arası gösterilliyor.`);
            }
            else
                Notify.Show(NotifyType.Error, response.message);

        });
    }
    /*
     * Helper Methods::Begin 
     */




    /*
     * Instance::Begin 
     */
    return _dataTable;
    /*
     * Instance::Begin 
     */




}