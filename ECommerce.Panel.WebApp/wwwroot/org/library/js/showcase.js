﻿function conchangesize(e) {
    e.currentTarget.parentElement.parentElement.parentElement.parentElement.className = `col-lg-${e.currentTarget.value}`;
}

var Showcase = (function () {

    /**
     * Dynamic id starting value.
     */
    var id = 0;

    /**
     * Playground element.
     */
    var playgroundElement = null;

    /**
     * Scope.
     */
    var scope = null;

    function condragstart(e) {

        e.dataTransfer.setData("text/plain", e.target.id);

        scope = document.getElementById(e.target.id).getAttribute("scope");

        document.querySelectorAll("[type]").forEach(element => {

            if (element.getAttribute("type") != scope) {

                element.removeEventListener("drop", condrop);
                element.removeEventListener("dragover", condragover);
                element.removeEventListener("dragenter", condragenter);
                element.removeEventListener("dragleave", condragleave);

            }

            if (element.getAttribute("type") == scope) {

                element.addEventListener("drop", condrop);
                element.addEventListener("dragover", condragover);
                element.addEventListener("dragenter", condragenter);
                element.addEventListener("dragleave", condragleave);

            }

        });

    }

    function condrop(e) {

        e.preventDefault();

        if (e.target.classList.contains('sorting')) {
            e.target.classList.remove('sorting');
            return;
        }

        id -= 1;

        var dataId = e.dataTransfer.getData("text/plain");

        var appened = false;
        debugger
        e.path.forEach(path => {

            if (!appened) {

                var targetType = path.getAttribute("type");

                if (targetType == scope) {

                    var element = createelement(dataId);

                    if (element != null) {

                        if (path.id == "playground") {
                            path.append(element);
                        }
                        else if (path.querySelector("[class='row inner-playground']") != null) {
                            path.querySelector("[class='row inner-playground']").append(element);
                        }
                        else if (path.querySelector("[class='kt-portlet__body inner-playground']") != null) {
                            path.querySelector("[class='kt-portlet__body inner-playground']").append(element);
                        }

                        appened = true;

                    }

                }

            }

        });

    }

    function condragover(e) {

        e.preventDefault();

        // var currentTarget = e.currentTarget.getAttribute("type");

        // if (scope == currentTarget) {

        //     e.preventDefault();

        // }

    }

    function condragenter(e) {

        //             var currentTarget = e.currentTarget.getAttribute("type");

        //             if (scope == currentTarget) {

        //                 e.currentTarget.classList.add("hover");

        //             }

    }

    function condragleave(e) {

        // var currentTarget = e.currentTarget.getAttribute("type");

        // if (scope == currentTarget) {

        //     e.currentTarget.classList.remove("hover");

        // }

    }

    function createelement(dataId) {

        var template = `
<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title flex-box mr-2">
            <i class="{icon} {iconClass} mr-1"></i> {name}
        </h3>
        {header}
    </div>
    <button type="button" class="btn btn-brand btn-elevate btn-circle btn-icon" onclick="{deleteFunction}"><i class="flaticon2-rubbish-bin"></i></button>
</div>
{innerTemplate}`;
        var innerTemplate = '<div class="kt-portlet__body inner-playground"></div>';
        var icon = null;
        var iconClass = null;
        var name = null;
        var header = "";
        var type = null;
        var scope = null;
        var element = document.createElement("div");
        element.className = "kt-portlet";

        var childs = null;

        switch (dataId) {
            case "row":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.parentElement.remove()");

                header = '<div class="form-group pos-relative mb-0"><label class="input-abs-text">Başlık</label><input type="text" class="form-control"></div>';
                element.className = "col-lg-12";
                template = `<div class="kt-portlet">${template}</div>`;
                innerTemplate = '<div class="kt-portlet__body"><div class="row inner-playground"></div></div>'
                icon = "flaticon2-right-arrow";
                iconClass = "kt-font-warning";
                name = "Satır";
                type = "row";
                scope = "playground";

                break;
            case "column":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.parentElement.remove()");

                header = `
<select class="custom-select custom-select-sm form-control form-control-sm showcase-grid-select" onchange="conchangesize(event)">
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
  <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
  <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12" selected>12</option>
</select>`;
                element.className = "col-lg-12";
                template = `<div class="kt-portlet">${template}</div>`;
                icon = "flaticon2-arrow-1";
                iconClass = "kt-font-warning";
                name = "Sütun";
                type = "column";
                scope = "row";

                break;
            case "product":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.remove()");

                innerTemplate = '<div class="kt-portlet__body"></div>';
                icon = "flaticon2-cube";
                iconClass = "kt-font-warning";
                name = "Ürün";
                type = "product";
                scope = "column";

                childs = [];

                var formGroupElement = document.createElement("div");
                formGroupElement.className = "form-group";

                var formGroupItemElement = document.createElement("div");
                formGroupItemElement.className = "col-lg-12 typeahead pos-relative";
                

                var label = document.createElement("label");
                label.innerHTML = "Ürün";
                label.className = "input-abs-text"

                var hidden = document.createElement("input");
                hidden.type = "hidden";

                var text = document.createElement("input");
                text.type = "text";
                text.className = "form-control";
                text.placeholder = "Ara";

                var timeout = null;
                text.addEventListener("input", function (e) {

                    var input = e.currentTarget;
                    var q = e.currentTarget.value;
                    var parentElement = e.currentTarget.parentElement;

                    if (q.length > 3) {
                        if (timeout != null)
                            clearTimeout(timeout);

                        timeout = setTimeout(autocomplete, 1000, input, q, parentElement, hidden);
                    }

                });

                formGroupItemElement.appendChild(label);
                formGroupItemElement.appendChild(text);
                formGroupItemElement.appendChild(hidden);
                formGroupElement.appendChild(formGroupItemElement);

                childs.push(formGroupElement);

                break;
            case "banner":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.remove()");

                innerTemplate = '<div class="kt-portlet__body"></div>';
                icon = "flaticon2-photograph";
                iconClass = "kt-font-warning";
                name = "Banner";
                type = "banner";
                scope = "column";

                childs = [];

                var formGroupElement = document.createElement("div");
                formGroupElement.className = "form-group";

                var formGroupItemElement = document.createElement("div");
                formGroupItemElement.className = "col-lg-12";

                var label = document.createElement("label");
                label.innerHTML = "Fotoğraf";

                var fileElement = document.createElement("input");
                fileElement.type = "file";
                fileElement.className = "form-control";
                fileElement.addEventListener("change", function () {
                    var file = this.files[0];
                    var reader = new FileReader();

                    var hiddenElement = fileElement.parentElement.querySelector('[type="hidden"]');

                    reader.addEventListener("load", function () {
                        debugger;
                        hiddenElement.value = reader.result;
                        
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                });

                var hidden = document.createElement("input");
                hidden.type = "hidden";

                formGroupItemElement.appendChild(label);
                formGroupItemElement.appendChild(fileElement);
                formGroupItemElement.appendChild(hidden);
                formGroupElement.appendChild(formGroupItemElement);

                var formGroupElement2 = document.createElement("div");
                formGroupElement2.className = "form-group";

                var formGroupItemElement2 = document.createElement("div");
                formGroupItemElement2.className = "col-lg-12";

                var label2 = document.createElement("label");
                label2.innerHTML = "Link";

                var text = document.createElement("input");
                text.type = "text";
                text.className = "form-control";
                text.placeholder = "Link"

                formGroupItemElement2.appendChild(label2);
                formGroupItemElement2.appendChild(text);
                formGroupElement2.appendChild(formGroupItemElement2);

                childs.push(formGroupElement);
                childs.push(formGroupElement2);

                break;
            case "slider":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.remove()");

                innerTemplate = '<div class="kt-portlet__body"><div class="row inner-playground"></div></div>'
                icon = "flaticon2-files-and-folders";
                iconClass = "kt-font-warning";
                name = "Slider";
                type = "slider";
                scope = "column";

                break;
            case "slider-product":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.parentElement.remove()");

                element.className = "col-lg-3";
                template = `<div class="kt-portlet">${template}</div>`;
                innerTemplate = '<div class="kt-portlet__body"></div>';
                icon = "flaticon2-cube";
                iconClass = "kt-font-success";
                name = "Slider Ürün";
                type = "slider-product";
                scope = "slider";

                childs = [];

                var formGroupElement = document.createElement("div");
                formGroupElement.className = "form-group";

                var formGroupItemElement = document.createElement("div");
                formGroupItemElement.className = "col-lg-12 typeahead pos-relative";

                var label = document.createElement("label");
                label.innerHTML = "Ürün";
                label.className="input-abs-text"

                var hidden = document.createElement("input");
                hidden.type = "hidden";

                var text = document.createElement("input");
                text.type = "text";
                text.className = "form-control";
                text.placeholder = "Ara";
                var timeout = null;
                text.addEventListener("input", function (e) {
                    
                    var input = e.currentTarget;
                    var q = e.currentTarget.value;
                    var parentElement = e.currentTarget.parentElement;

                    if (q.length > 3) {
                        if (timeout != null)
                            clearTimeout(timeout);

                        timeout = setTimeout(autocomplete, 1000, input, q, parentElement, hidden);
                    }

                });

                formGroupItemElement.appendChild(label);
                formGroupItemElement.appendChild(text);
                formGroupItemElement.appendChild(hidden);
                formGroupElement.appendChild(formGroupItemElement);

                childs.push(formGroupElement);

                break;
            case "slider-banner":

                template = template.replace("{deleteFunction}", "this.parentElement.parentElement.parentElement.remove()");

                element.className = "col-lg-4";
                template = `<div class="kt-portlet">${template}</div>`;
                innerTemplate = '<div class="kt-portlet__body"></div>';
                icon = "flaticon2-photograph";
                iconClass = "kt-font-success";
                name = "Slider Banner";
                type = "slider-banner";
                scope = "slider";

                childs = [];

                var formGroupElement = document.createElement("div");
                formGroupElement.className = "form-group";

                var formGroupItemElement = document.createElement("div");
                formGroupItemElement.className = "col-lg-12";

                var label = document.createElement("label");
                label.innerHTML = "Fotoğraf";

                var fileElement = document.createElement("input");
                fileElement.type = "file";
                fileElement.className = "form-control";
                fileElement.addEventListener("change", function () {
                    var file = this.files[0];
                    var reader = new FileReader();

                    var hiddenElement = fileElement.parentElement.querySelector('[type="hidden"]');

                    reader.addEventListener("load", function () {
                        hiddenElement.value = reader.result;
                        
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                });

                var hidden = document.createElement("input");
                hidden.type = "hidden";

                formGroupItemElement.appendChild(label);
                formGroupItemElement.appendChild(fileElement);
                formGroupItemElement.appendChild(hidden);
                formGroupElement.appendChild(formGroupItemElement);

                var formGroupElement2 = document.createElement("div");
                formGroupElement2.className = "form-group";

                var formGroupItemElement2 = document.createElement("div");
                formGroupItemElement2.className = "col-lg-12";

                var label2 = document.createElement("label");
                label2.innerHTML = "Link";

                var text = document.createElement("input");
                text.type = "text";
                text.className = "form-control";
                text.placeholder = "Link"

                formGroupItemElement2.appendChild(label2);
                formGroupItemElement2.appendChild(text);
                formGroupElement2.appendChild(formGroupItemElement2);

                childs.push(formGroupElement);
                childs.push(formGroupElement2);

                break;
            default:
                return null;
        }

        template = template.replace("{icon}", icon);
        template = template.replace("{iconClass}", iconClass);
        template = template.replace("{name}", name);
        template = template.replace("{innerTemplate}", innerTemplate);
        template = template.replace("{header}", header);
        element.innerHTML = template;
        element.setAttribute("type", type);
        element.setAttribute("scope", scope);

        if (childs != null) {

            childs.forEach(child => {

                element.querySelector("[class='kt-portlet__body']").appendChild(child);

            });

        }

        /* For Sortable::Begin */
        element.setAttribute("draggable", "true");
        element.ondrag = handleDrag;
        element.ondragover = condragover;
        /* For Sortable::End */

        return element;

    }

    function createjson(element, parent) {
        var type = element.getAttribute("type");
        switch (type) {
            case "row":
                var row = {
                    header: element.children[0].children[0].querySelector('[type="text"]').value,
                    columnComponents: []
                };
                var childrens = element.children[0].children[1].children[0].children;

                for (var i = 0; i < childrens.length; i++) {
                    createjson(childrens[i], row.columnComponents);
                }

                parent.push(row);

                break;
            case "column":
                var column = {
                    width: parseInt(element.querySelector("select").value),
                    sliderComponents: [],
                    productComponents: [],
                    bannerComponents: [],
                };
                var childrens = element.children[0].children[1].children;

                for (var i = 0; i < childrens.length; i++) {
                    createjson(childrens[i], column);
                }

                parent.push(column);

                break;
            case "product":

                var productComponent = {
                    productId: parseInt(element.querySelector('[type="hidden"]').value.split('-')[0]),
                    productInformationId: parseInt(element.querySelector('[type="hidden"]').value.split('-')[1])
                };

                parent.productComponents.push(productComponent);

                break;
            case "banner":

                var bannerComponent = {
                    fileName: element.querySelector('[type="hidden"]').value,
                    link: element.querySelector('[type="text"]').value
                };

                parent.bannerComponents.push(bannerComponent);

                break;
            case "slider":

                var slider = {
                    bannerComponents: [],
                    productComponents: []
                };
                var childrens = element.children[1].children[0].children;

                for (var i = 0; i < childrens.length; i++) {
                    createjson(childrens[i], slider);
                }

                parent.sliderComponents.push(slider);

                break;
            case "slider-product":

                var sliderProduct = {
                    productId: parseInt(element.querySelector('[type="hidden"]').value.split('-')[0]),
                    productInformationId: parseInt(element.querySelector('[type="hidden"]').value.split('-')[1])
                };

                parent.productComponents.push(sliderProduct);

                break;
            case "slider-banner":

                var sliderBannerComponent = {
                    fileName: element.querySelector('[type="hidden"]').value,
                    link: element.querySelector('[type="text"]').value
                };

                parent.bannerComponents.push(sliderBannerComponent);

                break;
            default:
                return null;
        }

    }

    function handleDrag(item) {
        var selectedItem = item.target,
            list = selectedItem.parentNode,
            x = event.clientX,
            y = event.clientY;

        selectedItem.classList.add('sorting');
        var swapItem = document.elementFromPoint(x, y) === null ? selectedItem : document.elementFromPoint(x, y);

        if (list === swapItem.parentNode) {
            swapItem = swapItem !== selectedItem.nextSibling ? swapItem : swapItem.nextSibling;
            list.insertBefore(selectedItem, swapItem);
        }
    }

    function autocomplete(input, q, parentElement, hidden) {
        HttpClient.Get("/Product/Autocomplete?take=5&q=" + q, function (response) {

            if (response.success) {

                if (parentElement.querySelector('.tt-menu'))
                    parentElement.querySelector('.tt-menu').remove();

                var div = document.createElement("div");
                div.setAttribute("role", "listbox");
                div.className = "tt-menu";
                div.style = "position: absolute; top: 100%; left: 0px; z-index: 100;";

                var itemDiv = document.createElement("div");
                itemDiv.setAttribute("role", "presentation");
                itemDiv.className = "tt-dataset tt-dataset-states";

                response.data.forEach(d => {

                    var optionDiv = document.createElement("div");
                    optionDiv.setAttribute("role", "option");
                    optionDiv.className = "tt-suggestion tt-selectable";
                    optionDiv.innerHTML = d.value;
                    optionDiv.addEventListener("click", function (e) {
                        hidden.value = d.key;
                        input.value = d.value;
                        div.remove();
                    });

                    itemDiv.appendChild(optionDiv);

                });

                div.appendChild(itemDiv);

                parentElement.append(div);

                document.addEventListener("click", documentClick, false);
                document.div = div;

            }

        });
    }

    function documentClick(e) {
        e.currentTarget.div.remove();
        document.removeEventListener("click", documentClick);
    }



    var showcase = {};

    showcase.Initialize = function () {

        playgroundElement = document.querySelector("[type='playground']");
        playgroundElement.addEventListener("drop", condrop);
        playgroundElement.addEventListener("dragover", condragover);
        playgroundElement.addEventListener("dragenter", condragenter);
        playgroundElement.addEventListener("dragleave", condragleave);

        document.querySelectorAll("[scope]").forEach(element => {
            element.setAttribute("draggable", "true");
            element.addEventListener("dragstart", condragstart);
        });

        document.querySelectorAll("[not-initialize]").forEach(element => {
            element.ondrag = handleDrag;
            element.ondragover = condragover;

            var type = element.getAttribute("type");

            switch (type) {
                case "product":
                    var hidden = element.querySelector("[type='hidden']");
                    var text = element.querySelector("[type='text']");
                    var timeout = null;
                    text.addEventListener("input", function (e) {

                        var input = e.currentTarget;
                        var q = e.currentTarget.value;
                        var parentElement = e.currentTarget.parentElement;

                        if (q.length > 3) {
                            if (timeout != null)
                                clearTimeout(timeout);

                            timeout = setTimeout(autocomplete, 1000, input, q, parentElement, hidden);
                        }

                    });
                    break;
                case "slider-product":
                    var hidden = element.querySelector("[type='hidden']");
                    var text = element.querySelector("[type='text']");
                    var timeout = null;
                    text.addEventListener("input", function (e) {

                        var input = e.currentTarget;
                        var q = e.currentTarget.value;
                        var parentElement = e.currentTarget.parentElement;

                        if (q.length > 3) {
                            if (timeout != null)
                                clearTimeout(timeout);

                            timeout = setTimeout(autocomplete, 1000, input, q, parentElement, hidden);
                        }

                    });
                    break;
                case "banner":
                    var fileElement = element.querySelector("[type='file']");
                    fileElement.addEventListener("change", function () {

                        var file = this.files[0];
                        var reader = new FileReader();

                        var hiddenElement = fileElement.parentElement.querySelector('[type="hidden"]');

                        reader.addEventListener("load", function () {
                            hiddenElement.value = reader.result;
                            
                        }, false);

                        if (file) {
                            reader.readAsDataURL(file);
                        }

                    });
                    break;
                case "slider-banner":
                    var fileElement = element.querySelector("[type='file']");
                    fileElement.addEventListener("change", function () {

                        var file = this.files[0];
                        var reader = new FileReader();

                        var hiddenElement = fileElement.parentElement.querySelector('[type="hidden"]');

                        reader.addEventListener("load", function () {
                            hiddenElement.value = reader.result;
                            
                        }, false);

                        if (file) {
                            reader.readAsDataURL(file);
                        }

                    });
                    break;
            }

        });

    }

    showcase.ToJson = function () {

        var childrens = playgroundElement.children;

        var showcase = {
            id: document.getElementById("showcase_id") != null ? document.getElementById("showcase_id").value : 0,
            rowComponents: []
        };

        for (var i = 0; i < childrens.length; i++) {
            createjson(childrens[i], showcase.rowComponents);
        }

        return showcase;

    }

    showcase.Validate = function (submitFunction) {

        var showcase = Showcase.ToJson();

        var valid = true;
        var message = "";

        if (showcase.rowComponents.length == 0) {
            message = "Boş vitrin kaydedilemez.";
            valid = !valid;
        }

        if (!valid) {
            Notify.Show(NotifyType.Error, message);
            return;
        }

        showcase.rowComponents.forEach(rc => {
            if (rc.columnComponents.length == 0) {
                message = "Boş satır kaydedilemez.";
                valid = !valid;
            }
        });

        if (!valid) {
            Notify.Show(NotifyType.Error, message);
            return;
        }

        showcase.rowComponents.forEach(rc => {
            rc.columnComponents.forEach(cc => {
                if (cc.bannerComponents.length == 0 && cc.productComponents.length == 0 && cc.sliderComponents.length == 0) {
                    message = "Boş sütun kaydedilemez.";
                    valid = !valid;
                }
            });
        });

        if (!valid) {
            Notify.Show(NotifyType.Error, message);
            return;
        }

        showcase.rowComponents.forEach(rc => {
            rc.columnComponents.forEach(cc => {

                if (cc.bannerComponents.length > 0) {
                    cc.bannerComponents.forEach(bc => {
                        if (bc.fileName == "") {
                            message = "Boş banner kaydedilemez.";
                            valid = !valid;
                        }
                    });
                }

                if (cc.productComponents.length > 0) {
                    cc.productComponents.forEach(pc => {
                        if (!pc.productId) {
                            message = "Boş ürün kaydedilemez.";
                            valid = !valid;
                        }
                    });
                }

                if (cc.sliderComponents.length > 0) {
                    cc.sliderComponents.forEach(sc => {

                        if (sc.bannerComponents.length > 0) {
                            sc.bannerComponents.forEach(bc => {
                                if (bc.fileName == "") {
                                    message = "Boş banner kaydedilemez.";
                                    valid = !valid;
                                }
                            });
                        }

                        if (sc.productComponents.length > 0) {
                            sc.productComponents.forEach(pc => {
                                if (!pc.productId) {
                                    message = "Boş ürün kaydedilemez.";
                                    valid = !valid;
                                }
                            });
                        }

                    });
                }

            });
        });

        if (!valid) {
            Notify.Show(NotifyType.Error, message);
            return;
        }

        submitFunction();
    }

    return showcase;

})();