﻿const ReportIndex = {
    Initialize: function () {
        $('.ot-selectpicker').selectpicker();
        $("#ajax_data_marketplace_id").change(function () {
            ReportIndex.Refresh();
        });
        $("#ajax_data_min_quantity").change(function () {
            ReportIndex.Refresh();
        });
        $("#ajax_data_report_date").daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: false,
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {

            $("#ajax_data_report_date").val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));

            ReportIndex.Refresh();

        });
    },
    Refresh: function (sender) {
        var startDateString, endDateString, marketplaceId;
        marketplaceId = $("#ajax_data_marketplace_id").val();
        minQuantity = $("#ajax_data_min_quantity").val();
        startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        if (!marketplaceId) {
            marketplaceId = '';
        }


        var url = `/Report/IndexPartials?sds=${startDateString}&eds=${endDateString}&marketplaceId=${marketplaceId}&minQuantity=${minQuantity}`;

        if (sender != undefined) {
            var $sender = $(sender);
            if ($sender.find("i").length == 0) {
                $sender.prepend('<i style="font-size: 18px; display: inline" class="la la-sort-amount-desc"></i>');
            }
            else {

                var $i = $sender.find("i").eq(0);
                if ($i.hasClass("la la-sort-amount-desc")) {
                    $i.removeClass("la la-sort-amount-desc");
                    $i.addClass("la la-sort-amount-asc");
                }
                else {
                    $i.removeClass("la la-sort-amount-asc");
                    $i.addClass("la la-sort-amount-desc");
                }
            }

            var sorting = $sender.find("span").eq(0).html();
            var asc = $sender.find("i").eq(0).hasClass("la la-sort-amount-desc") ? "desc" : "asc";

            url = `${url}&sorting=${sorting}&asc=${asc}`
        }
        if (startDateString != '' && endDateString != '') {

            HttpClient.Get(url, function (response) {
                $("#report-partial").html(response.orderReport);
                $("#report-partial2").html(response.orderProductReport);
                $("#report-partial3").html(response.header);

                if (response.sorting != null && response.asc != null) {
                    var $sortable = $(`.sortable:contains("${response.sorting}")`);
                    $sortable.prepend(`<i style="font-size: 18px; display: inline" class="la la-sort-amount-${response.asc == "asc" ? 'asc' : 'desc'}"></i>`);
                }

                $('.sortable').click(function () {
                    ReportIndex.Refresh(this)
                });
            });

        }
    },
    OpenStockModal: function (productId, skuCode) {
        var startDateString, endDateString;

        startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        var url = `/Report/ProductBasedStockReport?sds=${startDateString}&eds=${endDateString}&ProductId=${productId}${skuCode != undefined ? `&skuCode=${skuCode}` : ""}`;

        HttpClient.Get(url, function (response) {

            $("#modal_body").html(`
<div class="row">
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="input-abs-text">Seçili Sütunlar</label>
            <select class="form-control kt-selectpicker select-active" multiple>
                <option value="0">Fotoğraf</option>
                <option value="1">Ürün</option>
                <option value="2">Model Kodu</option>
                <option value="3">Sku Kodu</option>
                <option value="4">Satılan Adet</option>
                <option value="5">Kalan Adet</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                <input type="checkbox" id="only_sold"> Satılanları Göster
                <span></span>
            </label>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                <input type="checkbox" id="only_out_of_stock"> Stoğu Bitenleri Göster
                <span></span>
            </label>
        </div>
    </div>
</div>` + response);
            $('.kt-selectpicker').selectpicker('val', ['0', '1', '2', '3', '4', '5']);
            $('.kt-selectpicker').change(function () {

                var vals = $('.kt-selectpicker').val();
                $.each($('#product_based_stock_report tr'), (index, value) => {
                    $(value).find('td').hide();
                    $.each($(value).find('td'), (index2, value2) => {
                        if (vals.indexOf(index2.toString()) > -1) {
                            $(value2).show();
                        }
                    });

                    $(value).find('th').hide();
                    $.each($(value).find('th'), (index2, value2) => {
                        if (vals.indexOf(index2.toString()) > -1) {
                            $(value2).show();
                        }
                    });
                });

            });
            $("#report_modal").modal();
            $("#modal_submit").unbind("click");
            $(".modal-title").html(`Ürün Bazlı Stok Raporu`);

            $("#only_sold").change(function () {
                ReportIndex.ApplyChecks();
            });
            $("#only_out_of_stock").change(function () {
                ReportIndex.ApplyChecks();
            });
        });
    },
    ApplyChecks: function () {
        var onlySold = $('#only_sold').prop('checked');
        var onlyOutOfStock = $('#only_out_of_stock').prop('checked');

        if (onlySold == false && onlyOutOfStock == false) {
            $('#product_based_stock_report tr').show();
        }
        else {
            $('#product_based_stock_report tr').hide();
            $('#product_based_stock_report tr').eq(0).show();

            $.each($('#product_based_stock_report tr'), (index, value) => {

                var show = false;

                if (onlySold && $(value).find('td').eq(4).text().trim() != "0") {
                    show = true;
                }

                if (onlyOutOfStock && $(value).find('td').eq(5).text().trim() == "0") {
                    show = true;
                }

                show ? $(value).show() : $(value).hide();

            });
        }
    },
    Sorting: function (columnIndex, numeric) {

        var asc = $('#product_based_stock_report thead tr th').eq(columnIndex).data('asc');
        if (asc == '') {
            asc = 'asc';
        }
        else {
            asc = asc == 'asc' ? 'desc' : 'asc';
        }

        $('#product_based_stock_report thead tr th').find('a i').remove();
        $('#product_based_stock_report thead tr th').data('asc', '');
        $('#product_based_stock_report thead tr th').eq(columnIndex).data('asc', asc);
        $('#product_based_stock_report thead tr th').eq(columnIndex).find('a').prepend(`<i style="font-size: 18px; display: inline" class="la la-sort-amount-${asc == 'asc' ? 'asc' : 'desc'}"></i>`);

        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("product_based_stock_report");
        switching = true;
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[columnIndex];
                y = rows[i + 1].getElementsByTagName("TD")[columnIndex];
                // Check if the two rows should switch place:
                if (numeric) {

                    if (asc == 'asc') {

                        if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }
                    else {

                        if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }

                }
                else {

                    if (asc == 'asc') {

                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }
                    else {

                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }

                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }

    }
};

$(document).ready(() => {
    ReportIndex.Initialize();
    ReportIndex.Refresh();
});