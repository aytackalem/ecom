﻿const ReturnIndex = {
    Initialize: function () {

        $('#ajax_data_search_marketplace_id').selectpicker();
        $('#ajax_data_search_marketplace_id').change(ReturnIndex.Refresh);

        var startDate, endDate;

        if (window.location.search == '') {
            startDate = endDate = new Date();
        }
        else {
            startDate = moment(window.location.search.split('&')[0].replace("?startDate=", ""), "DD-MM-YYYY");
            endDate = moment(window.location.search.split('&')[1].replace("endDate=", ""), "DD-MM-YYYY");
        }

        $("#ajax_data_report_date").daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: false,
            startDate: startDate,
            endDate: endDate,
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {

            $("#ajax_data_report_date").val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));

            ReturnIndex.Refresh();

        });
    },
    Refresh: function () {
        var marketplaceId, startDateString, endDateString;

        marketplaceId = $('#ajax_data_search_marketplace_id').val();
        startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        if (startDateString != '' && endDateString != '') {

            HttpClient.Get(`/Report/ReturnPartial?marketplaceId=${marketplaceId}&startDate=${startDateString}&endDate=${endDateString}`, function (response) {
                $("#ajax_table").html(response);
            });

        }
    }
};

$(document).ready(() => {
    ReturnIndex.Initialize();
    ReturnIndex.Refresh();
});