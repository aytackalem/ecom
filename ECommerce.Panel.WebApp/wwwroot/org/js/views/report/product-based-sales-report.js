﻿const ProductBasedSalesReportIndex = {
    Initialize: function () {

        var startDate, endDate;

        if (window.location.search == '') {
            startDate = endDate = new Date();
        }
        else {
            startDate = moment(window.location.search.split('&')[0].replace("?sds=", ""), "DD-MM-YYYY");
            endDate = moment(window.location.search.split('&')[1].replace("eds=", ""), "DD-MM-YYYY");
        }

        $("#ajax_data_report_date").daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: false,
            startDate: startDate,
            endDate: endDate,
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {

            $("#ajax_data_report_date").val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));

            ProductBasedSalesReportIndex.Refresh();

        });
    },
    Refresh: function (sender) {

        var startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        var endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        var url = `/Report/ProductBasedSalesReportPartials?sds=${startDateString}&eds=${endDateString}`;

        if (sender != undefined) {

            var $sender = $(sender);
            if ($sender.find("i").length == 0) {
                $sender.prepend('<i style="font-size: 18px; display: inline" class="la la-sort-amount-desc"></i>');
            }
            else {

                var $i = $sender.find("i").eq(0);
                if ($i.hasClass("la la-sort-amount-desc")) {
                    $i.removeClass("la la-sort-amount-desc");
                    $i.addClass("la la-sort-amount-asc");
                }
                else {
                    $i.removeClass("la la-sort-amount-asc");
                    $i.addClass("la la-sort-amount-desc");
                }
            }

            var sorting = $sender.find("span").eq(0).html();
            var asc = $sender.find("i").eq(0).hasClass("la la-sort-amount-desc") ? "desc" : "asc";

            url = `${url}&sorting=${sorting}&asc=${asc}`
        }

        HttpClient.Get(url, function (response) {
            $("#report-partial").html(response.productBasedSalesReport);
            $("#report-partial2").html(response.header);
            $("#report-partial3").html(response.productBasedHeader);

            if (response.sorting != null && response.asc != null) {
                var $sortable = $(`.sortable:contains("${response.sorting}")`);
                $sortable.prepend(`<i style="font-size: 18px; display: inline" class="la la-sort-amount-${response.asc == "asc" ? 'asc' : 'desc'}"></i>`);
            }


            $('.sortable').click(function () {
                ProductBasedSalesReportIndex.Refresh(this)
            });
        });

    },
    OrderSourceSelectionChanges: function () {
        $('[name="orderSourceId"]').change(function () {
            var orderSourceId = $(this).val();

            $("[name='marketplaceId']").prop('disabled', orderSourceId != "1");
            $("[name='marketplaceId']").prop('required', orderSourceId == "1");

            if (orderSourceId != "1") {

                $("[name='marketplaceId']").val('');
                $("#marketplaceId-error").remove();
            }

            $('.kt-selectpicker').selectpicker('refresh');
        });
    },
    OpenCreateModal: function (productInformationId) {

        HttpClient.Get("/Expense/Create", function (response) {
            $("#modal_body").html(response);

            $('[name="productInformationId"]').val(productInformationId);

            $("#expense_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Gider");

            $('.kt-datetimepicker').datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                autoclose: true,
                language: 'tr'
            });

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            $('.kt-selectpicker').selectpicker();

            ProductBasedSalesReportIndex.OrderSourceSelectionChanges();

            $("#modal_submit").click(function () {

                var $form = $("#expense_create_form");
                let valid = ValidationV2.Valid($form);
                if (valid) {
                    var data = Serializer.Serialize("expense_create_form");

                    if (data.productInformationId == "") data.productInformationId = null;
                    if (data.orderSourceId == "") data.orderSourceId = null;
                    if (data.marketplaceId == "") data.marketplaceId = null;

                    HttpClient.Post(
                        "/Expense/Create",
                        data,
                        function (response) {
                            if (response.success) {
                                ExpenseIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#expense_modal").modal("hide");


                                ProductBasedSalesReportIndex.Refresh();
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");

            });
        });
    }
};

$(document).ready(() => {
    ProductBasedSalesReportIndex.Initialize();
    ProductBasedSalesReportIndex.Refresh();
});