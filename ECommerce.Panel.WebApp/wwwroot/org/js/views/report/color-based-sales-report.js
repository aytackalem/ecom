﻿const ColorBasedSalesReportIndex = {
    Initialize: function () {

        $('#ajax_data_categorization').selectpicker();
        $('#ajax_data_brand').selectpicker();
        $('#ajax_data_last_3_day').selectpicker();
        $('#ajax_data_average').selectpicker();
        $('#ajax_data_marketplace_id').selectpicker();
        $('#ajax_data_category_id').selectpicker();

        //$('.kt-selectpicker').selectpicker();

        $("#ajax_data_marketplace_id, #ajax_data_category_id").change(function () {
            ColorBasedSalesReportIndex.Refresh();
        });
        $("#ajax_data_report_date").daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: false,
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {

            $("#ajax_data_report_date").val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));

            ColorBasedSalesReportIndex.Refresh();

        });

        $('#ajax_data_last_3_day').on('hide.bs.select', function () {

            $('#ajax_data_average').val('');
            $('#ajax_data_average').selectpicker("refresh");

            FilterColorBasedSalesTable();

        });

        $('#ajax_data_average').on('hide.bs.select', function () {

            $('#ajax_data_last_3_day').val('');
            $('#ajax_data_last_3_day').selectpicker("refresh");

            FilterColorBasedSalesTable();

        });

        $('#ajax_data_categorization').on('hide.bs.select', function () {

            FilterColorBasedSalesTable();

        });

        $('#ajax_data_brand').on('hide.bs.select', function () {

            FilterColorBasedSalesTable();

        });

    },
    Refresh: function () {
        var startDateString, endDateString;

        startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        if (startDateString != '' && endDateString != '') {

            var marketplaceId = $('#ajax_data_marketplace_id').val();
            var sellerCode = $('#ajax_data_search_seller_code').val();
            var skuCode = $('#ajax_data_search_sku_code').val();
            var categoryId = $('#ajax_data_category_id').val();

            HttpClient.Get(`/Report/ColorBasedSalesReportPartial?sds=${startDateString}&eds=${endDateString}&marketplaceId=${marketplaceId}&sellerCode=${sellerCode}&skuCode=${skuCode}&CategoryId=${categoryId}`, function (response) {

                $("#report-partial").html(response);
                $('[data-tooltip]').tooltip();

                const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

                const textComparer = (idx, asc) => (a, b) => ((v1, v2) =>
                    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
                )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

                const numberComparer = (idx, asc) => (a, b) => ((v1, v2) =>
                    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
                )(parseInt(getCellValue(asc ? a : b, idx)), parseInt(getCellValue(asc ? b : a, idx)));

                document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
                    var sortingType = 'text';

                    if (th.hasAttribute('sorting-type')) {
                        sortingType = th.getAttribute('sorting-type');
                    }

                    var comparer = null;
                    if (sortingType == 'text') {
                        comparer = textComparer;
                    } else if (sortingType == 'number') {
                        comparer = numberComparer;
                    }

                    const table = th.closest('table');

                    Array.from(table.querySelectorAll('tbody tr'))
                        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                        .forEach(tr => table.querySelector('tbody').appendChild(tr));
                })));

            });

        }
    },
    OpenStockModal: function (productId, skuCode, hasReceipt) {
        var startDateString, endDateString;

        startDateString = $("#ajax_data_report_date").val().split(' / ')[0];
        endDateString = $("#ajax_data_report_date").val().split(' / ')[1];

        HttpClient.Get(`/Report/ProductBasedStockReport?sds=${startDateString}&eds=${endDateString}&ProductId=${productId}${skuCode != undefined ? `&skuCode=${skuCode}` : ""}${hasReceipt != undefined ? `&hasReceipt=${hasReceipt}` : ""}`, function (response) {

            HttpClient.Get('/Supplier/Read?PageRecordsCount=50', function (r) {
                r.data.forEach(d => {
                    $('#supplier').append($('<option>', { value: d.id, text: d.name }));
                });

                $('#supplier').selectpicker();
            });

            $("#modal_body").html(`
<div class="row">
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="input-abs-text">Seçili Sütunlar</label>
            <select class="form-control kt-selectpicker select-active" multiple>
                <option value="0">Fotoğraf</option>
                <option value="1">Ürün</option>
                <option value="2">Model Kodu</option>
                <option value="3">Sku Kodu</option>
                <option value="4">Satılan Adet</option>
                <option value="5">Kalan Adet</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                <input type="checkbox" id="only_sold"> Satılanları Göster
                <span></span>
            </label>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group pos-relative">
            <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                <input type="checkbox" id="only_out_of_stock"> Stoğu Bitenleri Göster
                <span></span>
            </label>
        </div>
    </div>
</div>
<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-12">
        <button type="button" id="create-receipt" class="btn btn-brand" style="display: none;">İndir</button>
        <button type="button" id="insert-receipt" class="btn btn-brand receipt" style="display: none;">Kaydet</button>
        <button type="button" id="preview-receipt" class="btn btn-warning receipt" style="display: none;">Önizleme</button>
        <button type="button" id="btn-receipt" data-state="init" class="btn btn-info">Kesim Föyü Oluştur</button>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">

        <div class="form-group">

            <select id="supplier" class="form-control">
                <option value="">Tedarikçi Seçiniz</option>
            </select>
        
        </div>

    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Termin Süresi (Gün Bazında):</span>
                <input type="number" id="receipt-deadline-days" class="form-control" value="0" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Birim Metrajı:</span>
                <input type="number" id="receipt-unit-meter" class="form-control" value="0" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Kart:</span>
                <input type="text" id="receipt-card" class="form-control" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Ense Etiketi:</span>
                <input type="text" id="receipt-ticket" class="form-control" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Şeffaf Lastik:</span>
                <input type="text" id="receipt-rubber" class="form-control" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Yıkama Talimatı:</span>
                <input type="text" id="receipt-wash" class="form-control" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Askı:</span>
                <input type="text" id="receipt-hanger" class="form-control" />
            </div>
        </div>
    </div>
</div>
<div class="row receipt" style="display: none;">
    <div class="col-md-12">
        <div class="form-group">
            <div class="pos-relative">
                <span class="input-abs-text">Not:</span>
                <textarea id="receipt-note" class="form-control" rows="5"></textarea>
            </div>
        </div>
    </div>
</div>` + response);
            $('[data-receipt-product-information-id], #receipt-unit-meter, #receipt-deadline-days').click(function () {
                if ($(this).val() == '0') {
                    $(this).val('');
                }
            });
            $('[data-receipt-product-information-id], #receipt-unit-meter, #receipt-deadline-days').blur(function () {
                if ($(this).val() == '') {
                    $(this).val('0');
                }
            });

            $('.kt-selectpicker').selectpicker('val', ['0', '1', '2', '3', '4', '5']);
            $('.kt-selectpicker').change(function () {
                debugger
                var vals = $('.kt-selectpicker').val();
                $.each($('#product_based_stock_report tr'), (index, value) => {
                    $(value).find('td').hide();
                    $.each($(value).find('td'), (index2, value2) => {
                        if (vals.indexOf(index2.toString()) > -1) {
                            $(value2).show();
                        }
                    });

                    $(value).find('th').hide();
                    $.each($(value).find('th'), (index2, value2) => {
                        if (vals.indexOf(index2.toString()) > -1) {
                            $(value2).show();
                        }
                    });
                });

            });
            $("#report_modal").modal();
            $("#modal_submit").unbind("click");
            $(".modal-title").html(`Ürün Bazlı Stok Raporu`);
            $('#btn-receipt').click(function () {

                var state = $(this).data('state');
                if (state == 'init') {

                    $(this).attr('class', 'btn btn-danger');
                    $(this).text('Vazgeç');
                    $(this).data('state', 'create');
                    $('.receipt').show();

                }
                else if (state == 'create') {

                    $(this).attr('class', 'btn btn-info');
                    $(this).text('Kesim Föyü Oluştur');
                    $(this).data('state', 'init');
                    $('.receipt').hide();
                    $('#create-receipt').hide();

                }

            });

            $('#create-receipt').click(function () {

                var supplierId = $('#supplier').val();

                if (supplierId == '') {
                    Notify.Show(NotifyType.Error, "Lütfen 'Tedarikçi' seçiniz.");
                    return;
                }

                var receipt = {
                    supplierId: supplierId,
                    unitMeter: parseFloat($('#receipt-unit-meter').val()),
                    deadlineDays: $('#receipt-deadline-days').val(),
                    note: $('#receipt-note').val(),
                    card: $('#receipt-card').val(),
                    ticket: $('#receipt-ticket').val(),
                    wash: $('#receipt-wash').val(),
                    hanger: $('#receipt-hanger').val(),
                    rubber: $('#receipt-rubber').val(),
                    receiptItems: []
                };

                var $inputs = $('input[data-receipt-product-information-id]');
                $inputs.each((index, item) => {
                    var $input = $(item);
                    var quantity = parseInt($input.val());
                    if (quantity !== 0) {
                        receipt.receiptItems.push({
                            quantity: quantity,
                            productInformationId: $input.data('receipt-product-information-id')
                        });
                    }
                });

                if (receipt.receiptItems.length == 0) {
                    Notify.Show(NotifyType.Error, "Lütfen en az 1 'İstenilen Adet' giriniz.");
                    return;
                }

                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Lütfen bekleyiniz...'
                });

                $.ajax({
                    url: '/Receipt',
                    data: receipt,
                    method: 'POST',
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success: function (response) {
                        var blob = new Blob([response], { type: 'application/pdf' });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = $('#receipt-file-name').val();
                        a.click();

                        KTApp.unblockPage();
                    },
                    error: function () {
                        KTApp.unblockPage();
                    }
                });
            });
            $('#insert-receipt').click(function () {

                var supplierId = $('#supplier').val();

                if (supplierId == '') {
                    Notify.Show(NotifyType.Error, "Lütfen 'Tedarikçi' seçiniz.");
                    return;
                }

                var receipt = {
                    supplierId: supplierId,
                    unitMeter: parseFloat($('#receipt-unit-meter').val()),
                    deadlineDays: $('#receipt-deadline-days').val(),
                    note: $('#receipt-note').val(),
                    card: $('#receipt-card').val(),
                    ticket: $('#receipt-ticket').val(),
                    wash: $('#receipt-wash').val(),
                    hanger: $('#receipt-hanger').val(),
                    rubber: $('#receipt-rubber').val(),
                    receiptItems: []
                };

                var $inputs = $('input[data-receipt-product-information-id]');
                $inputs.each((index, item) => {
                    var $input = $(item);
                    var quantity = parseInt($input.val());
                    if (quantity !== 0) {
                        receipt.receiptItems.push({
                            quantity: quantity,
                            productInformationId: $input.data('receipt-product-information-id')
                        });
                    }
                });

                if (receipt.receiptItems.length == 0) {
                    Notify.Show(NotifyType.Error, "Lütfen en az 1 'İstenilen Adet' giriniz.");
                    return;
                }

                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Lütfen bekleyiniz...'
                });

                $.ajax({
                    url: '/Receipt/Insert',
                    data: receipt,
                    method: 'POST',
                    success: function (response) {
                        if (response.success) {

                            $('#create-receipt').show();

                            Notify.Show(NotifyType.Success, "İşlem başarılı.");
                        }
                        else {
                            Notify.Show(NotifyType.Error, "Bilinmeyen bir hata oluştu.");
                        }

                        KTApp.unblockPage();
                    },
                    error: function () {
                        KTApp.unblockPage();
                    }
                });
            });
            $('#preview-receipt').click(function () {

                var supplierId = $('#supplier').val();

                if (supplierId == '') {
                    Notify.Show(NotifyType.Error, "Lütfen 'Tedarikçi' seçiniz.");
                    return;
                }

                var receipt = {
                    supplierId: supplierId,
                    unitMeter: parseFloat($('#receipt-unit-meter').val()),
                    deadlineDays: $('#receipt-deadline-days').val(),
                    note: $('#receipt-note').val(),
                    card: $('#receipt-card').val(),
                    ticket: $('#receipt-ticket').val(),
                    wash: $('#receipt-wash').val(),
                    hanger: $('#receipt-hanger').val(),
                    rubber: $('#receipt-rubber').val(),
                    receiptItems: []
                };

                var $inputs = $('input[data-receipt-product-information-id]');
                $inputs.each((index, item) => {
                    var $input = $(item);
                    var quantity = parseInt($input.val());
                    if (quantity !== 0) {
                        receipt.receiptItems.push({
                            quantity: quantity,
                            productInformationId: $input.data('receipt-product-information-id')
                        });
                    }
                });

                if (receipt.receiptItems.length == 0) {
                    Notify.Show(NotifyType.Error, "Lütfen en az 1 'İstenilen Adet' giriniz.");
                    return;
                }

                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Lütfen bekleyiniz...'
                });

                $.ajax({
                    url: '/Receipt',
                    data: receipt,
                    method: 'POST',
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success: function (response) {
                        var blob = new Blob([response], { type: 'application/pdf' });
                        var downloadUrl = URL.createObjectURL(blob);
                        window.open(downloadUrl);

                        KTApp.unblockPage();
                    },
                    error: function () {
                        KTApp.unblockPage();
                    }
                });
            });

            $("#only_sold").change(function () {
                ColorBasedSalesReportIndex.ApplyChecks();
            });
            $("#only_out_of_stock").change(function () {
                ColorBasedSalesReportIndex.ApplyChecks();
            });
        });
    },
    ApplyChecks: function () {
        var onlySold = $('#only_sold').prop('checked');
        var onlyOutOfStock = $('#only_out_of_stock').prop('checked');

        if (onlySold == false && onlyOutOfStock == false) {
            $('#product_based_stock_report tr').show();
        }
        else {
            $('#product_based_stock_report tr').hide();
            $('#product_based_stock_report tr').eq(0).show();

            $.each($('#product_based_stock_report tr'), (index, value) => {

                var show = false;

                if (onlySold && $(value).find('td').eq(4).text().trim() != "0") {
                    show = true;
                }

                if (onlyOutOfStock && $(value).find('td').eq(5).text().trim() == "0") {
                    show = true;
                }

                show ? $(value).show() : $(value).hide();

            });
        }
    },
    OpenLast36: function (productId, skuCode) {
        HttpClient.Get(`/Report/Last36?ProductId=${productId}&SkuCode=${skuCode}`, function (response) {
            $('#last_36_modal_body').html(response);
            $('#last_36_modal').modal();
        });
    }
};

function FilterColorBasedSalesTable() {

    var lastDay = $('#ajax_data_last_3_day').val();
    var average = $('#ajax_data_average').val();

    var selectedCategorization = $('#ajax_data_categorization').val();
    selectedCategorization ??= '';
    var selectedBrand = $('#ajax_data_brand').val();
    selectedBrand ??= '';

    $('#color-based-sales-table tbody tr').each((i, v) => {
        $(v).show();
    });

    $('#color-based-sales-table tbody tr').each((i, v) => {

        if (
            (lastDay === '' || parseInt($(v).data('last-3-day')) < parseInt(lastDay)) &&
            (average === '' || parseInt($(v).data('average')) < parseInt(average)) &&
            (selectedCategorization === '' || $(v).data('categorization') == selectedCategorization) &&
            (selectedBrand === '' || $(v).data('brand') == selectedBrand)
        ) {
            $(v).show();
        }
        else {
            $(v).hide();
        }
    });

    var categorizations = [];
    var brands = [];
    var totalQuantity = 0;
    var totalStock = 0;

    $('#color-based-sales-table tbody tr').each((i, v) => {
        if ($(v).prop('style').display != 'none') {

            var categorizationText = $(v).find('td').eq(5).text().trim();
            var categorization = categorizations.find(c => c.key == categorizationText);
            if (categorization == undefined) {
                categorizations.push({ key: categorizationText, value: 1 });
            }
            else {
                categorization.value++;
            }

            var brandText = $(v).find('td').eq(6).text().trim();
            var brand = brands.find(b => b.key == brandText);
            if (brand == undefined) {
                brands.push({ key: brandText, value: 1 });
            }
            else {
                brand.value++;
            }

            totalQuantity += parseInt($(v).find('td').eq(2).text().trim());
            totalStock += parseInt($(v).find('td').eq(3).text().trim());
        }
    });

    $('#total-quantity').html(`Satış: ${totalQuantity}`);
    $('#total-stock').html(`Stok: ${totalStock}`);

    /* Categorizations */
    $('#ajax_data_categorization').html('');

    $('#ajax_data_categorization').append('<option value="">Tümü</option>');

    categorizations.sort((a, b) => a.key.localeCompare(b.key));

    for (var i = 0; i < categorizations.length; i++) {
        $('#ajax_data_categorization').append(`<option value="${categorizations[i].key}" data-content="${categorizations[i].key} <span class='kt-badge kt-badge--info kt-badge--inline kt-badge--rounded'>${categorizations[i].value}</span>">${categorizations[i].key}</option>`);
    }

    $('#ajax_data_categorization').val(selectedCategorization);

    /* Brands */
    $('#ajax_data_brand').html('');

    $('#ajax_data_brand').append('<option value="">Tümü</option>');

    brands.sort((a, b) => a.key.localeCompare(b.key));

    for (var i = 0; i < brands.length; i++) {
        $('#ajax_data_brand').append(`<option value="${brands[i].key}" data-content="${brands[i].key} <span class='kt-badge kt-badge--info kt-badge--inline kt-badge--rounded'>${brands[i].value}</span>">${brands[i].key}</option>`);
    }

    $('#ajax_data_brand').val(selectedBrand);

    /* Refresh */
    $('#ajax_data_categorization').selectpicker('refresh');
    $('#ajax_data_brand').selectpicker('refresh');
}

$(document).ready(() => {
    ColorBasedSalesReportIndex.Initialize();
    ColorBasedSalesReportIndex.Refresh();
});