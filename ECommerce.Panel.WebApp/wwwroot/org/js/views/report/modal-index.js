﻿const ModalIndex = {
    Sorting: function (columnIndex, numeric) {

        var asc = $('#product_based_stock_report thead tr th').eq(columnIndex).data('asc');
        if (asc == '') {
            asc = 'asc';
        }
        else {
            asc = asc == 'asc' ? 'desc' : 'asc';
        }

        $('#product_based_stock_report thead tr th').find('a i').remove();
        $('#product_based_stock_report thead tr th').data('asc', '');
        $('#product_based_stock_report thead tr th').eq(columnIndex).data('asc', asc);
        $('#product_based_stock_report thead tr th').eq(columnIndex).find('a').prepend(`<i style="font-size: 18px; display: inline" class="la la-sort-amount-${asc == 'asc' ? 'asc' : 'desc'}"></i>`);

        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("product_based_stock_report");
        switching = true;
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[columnIndex];
                y = rows[i + 1].getElementsByTagName("TD")[columnIndex];
                // Check if the two rows should switch place:
                if (numeric) {

                    if (asc == 'asc') {

                        if (y != undefined && y.innerHTML != undefined && parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }
                    else {

                        if (y != undefined && y.innerHTML != undefined && parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }

                }
                else {

                    if (asc == 'asc') {

                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }
                    else {

                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }

                    }

                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }

    }
};