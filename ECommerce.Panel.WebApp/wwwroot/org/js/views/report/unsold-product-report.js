﻿const UnsoldProductReportIndex = {
    Initialize: function () {

        $('.kt-selectpicker').selectpicker();
        $('.kt-hidden-select2').select2();

    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Report/UnsoldProductReportData",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `Period:${$("#ajax_data_search_period").val()}_Quantity:${$("#ajax_data_search_quantity").val()}_SkuCode:${$("#ajax_data_search_sku_code").val()}_SellerCode:${$("#ajax_data_search_seller_code").val()}_Categorization:${$("#ajax_data_search_categorization").val()}_Brand:${$("#ajax_data_search_brand").val()}`
            },
            [
                {
                    Name: "fileName",
                    Render: function (v) {

                        return `<a href="${v.fileName}" target="_blank">
                            <img class="table-product-img" src="https://imgr.helpy.com.tr/img/${v.fileName}/resize?size=150" width="50" height="75" loading="lazy" />
                        </a>`;

                    }
                },
                {
                    Name: "sellerCode",
                    Render: function (v) {
                        return `<a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.sellerCode}');Notify.Show(NotifyType.Info, '<b>${v.sellerCode}</b> kopyalandı.');">${v.sellerCode} <i class="la la-copy"></i> </a>`;
                    }
                },
                {
                    Name: "stock",
                    Type: "string"
                },
                {
                    Name: "quantity",
                    Type: "string"
                },
                {
                    Name: "skuCode",
                    Render: function (v) {
                        return `<a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.skuCode}');Notify.Show(NotifyType.Info, '<b>${v.skuCode}</b> kopyalandı.');">${v.skuCode} <i class="la la-copy"></i> </a>`;
                    }
                },
                {
                    Name: "categorization",
                    Render: function (v) {
                        return v.categorization == null ? '' : v.categorization;
                    }
                },
                {
                    Name: "brand",
                    Render: function (v) {
                        return v.brand == null ? '' : v.brand;
                    }
                },
                {
                    Name: "stockCost",
                    Render: function (v) {
                        return `<span style="font-weight: 500;">${v.stockCost.toLocaleString('tr-TR')} TL</span>`;
                    }
                },
                {
                    Name: "stockValue",
                    Render: function (v) {
                        return `<span style="font-weight: 500;">${v.stockValue.toLocaleString('tr-TR')} TL</span>`;
                    }
                },
                {
                    Name: "name",
                    Type: "string"
                }
            ],
            "UnsoldProductReport",
            [],
            false
        );
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        UnsoldProductReportIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        UnsoldProductReportIndex.Read(0);
    },
    HeaderSorting: function (sender) {
        var sorting = $(sender).data('sorting');
        $(".ajax_data_sorting").val(sorting);
        
        if (sorting.indexOf('Asc') !== -1) {
            $(sender).data('sorting', $(sender).data('sorting').replace('Asc', 'Desc'));
        }
        else {
            $(sender).data('sorting', $(sender).data('sorting').replace('Desc', 'Asc'));
        }

        UnsoldProductReportIndex.Read(0);
    }
};

$(document).ready(() => {

    UnsoldProductReportIndex.Initialize();
    UnsoldProductReportIndex.Read(0);

});