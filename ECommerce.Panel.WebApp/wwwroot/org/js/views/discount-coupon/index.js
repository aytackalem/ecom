﻿const DiscountCouponIndex = {
    Initialize: function () {
        DiscountCouponIndex.Read();

        $('.kt-selectpicker').selectpicker();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/DiscountCoupon/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "code",
                    Type: "string"
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "DiscountCoupon",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/DiscountCoupon/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#discount_coupon_modal").modal();
            $('.kt-selectpicker').selectpicker();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("İndirim Kuponu Güncelle");

            DiscountCouponIndex.ModalValidation("discount_coupon_update_form", function () {
                HttpClient.Post(
                    "/DiscountCoupon/Update",
                    Serializer.Serialize("discount_coupon_update_form"),
                    function (response) {
                        if (response.success) {
                            DiscountCouponIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#discount_coupon_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#discount_coupon_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/DiscountCoupon/Create", function (response) {
            $("#modal_body").html(response);
            $("#discount_coupon_modal").modal();
            $('.kt-selectpicker').selectpicker();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni İndirim Kuponu");

            DiscountCouponIndex.ModalValidation("discount_coupon_create_form", function () {
                HttpClient.Post(
                    "/DiscountCoupon/Create",
                    Serializer.Serialize("discount_coupon_create_form"),
                    function (response) {
                        if (response.success) {
                            DiscountCouponIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#discount_coupon_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#discount_coupon_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        DiscountCouponIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        DiscountCouponIndex.Read(0);
    },
};

$(document).ready(() => {
    DiscountCouponIndex.Initialize(0);
});