﻿const ModalSize = {
    Small: 'sm',
    Medium: 'md',
    Large: 'lg',
    Larger: 'xl'
};

const BulkOrderIndex = {
    Initialize: function () {
        
        BulkOrderIndex.Read();
        BulkOrderIndex.Summary();

        $('.kt-select2').select2({
            placeholder: "Seçiniz",
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {

                if (data.element && data.element.hasAttribute("data-html"))
                    return `<div style="text-align: center;">${data.element.getAttribute("data-html")}</div>`;
                else
                    return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });

        /* Order Types */
        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            BulkOrderIndex.Read(0);
        });


        $('#ajax_data_search_date').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: true,
            startDate: moment().add(-6, 'day'),
            endDate: new Date(),
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            $('#ajax_data_search_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
            BulkOrderIndex.Read(0);
        });
    },

    ShowStock: false,
    Summary: function () {
        HttpClient.Get(`/Order/Summary`, function (response) {
            $("#summary").html(response);

            setTimeout(BulkOrderIndex.Summary, 20000);
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        BulkOrderIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        BulkOrderIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/BulkOrder/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `OrderPrepation:${$("#ajax_data_search_order_preparation").val()}_Marketplace:${$("#ajax_data_search_marketplace").val()}_ShelfCode:${$("#ajax_data_search_shelf_code").val()}_Date:${$("#ajax_data_search_date").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_CategoryId:${$("#ajax_data_search_category").val()}_ProductCategoryPrepation:${$("#ajax_data_search_product_category_preparation").val()}`
            },
            [

                
                {
                    Name: "",
                    Type: "string",
                    Padding: 0,
                    Render: function (v) {
                        


                        return `
<table class="table" style="margin: 0;">
    <tbody>

        <tr>
            <td style="font-weight: 500;" width="25%">
                <div>${v.quantity} Adet</div>
                ${BulkOrderIndex.ShowStock ? `<div class="small">Kalan Stok ${v.productInformation.stock} Adet</div>` : ''}
            </td>
            <td width="5%">
                <a href="${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}" target="_blank">
                    <img class="table-product-img" src="https://imgr.helpy.com.tr/img/${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}/resize?size=150"/>
                </a>
            </td>
            <td width="45%">
                <div>${v.productInformation.productInformationName} ${v.productInformation.variantValuesDescription}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.product.sellerCode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.product.sellerCode}</b> kopyalandı.');">${v.productInformation.product.sellerCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.stockCode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.stockCode}</b> kopyalandı.');">${v.productInformation.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.barcode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.barcode}</b> kopyalandı.');">${v.productInformation.barcode} <i class="la la-copy"></i> </a>
                </div>
            </td>
            
        </tr>

    </tbody>
</table>`;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: (value, parentValue) => value.productInformation.shelfCode,
                    Unified: true
                }

            ],
            "OrderDetail",
            [
            ],
            false
        );
    },
    Print: function () {

        window.open(`/BulkOrder/Print?search=OrderPrepation:${$("#ajax_data_search_order_preparation").val()}_PagedShort:${$(".ajax_data_sorting :selected").val()}_Marketplace:${$("#ajax_data_search_marketplace").val()}_ShelfCode:${$("#ajax_data_search_shelf_code").val()}_Date:${$("#ajax_data_search_date").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_CategoryId:${$("#ajax_data_search_category").val()}_ProductCategoryPrepation:${$("#ajax_data_search_product_category_preparation").val()}`);


    }


}

$(document).ready(() => {
    BulkOrderIndex.Initialize();
});