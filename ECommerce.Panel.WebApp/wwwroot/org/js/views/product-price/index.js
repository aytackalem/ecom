﻿const ProductPriceIndex = {
    Initialize: function () {
 
        ProductPriceIndex.Read();

        $('.kt-select2').select2({
            placeholder: "Seçiniz",
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {

                if (data.element && data.element.hasAttribute("data-html"))
                    return `<div style="text-align: center;">${data.element.getAttribute("data-html")}</div>`;
                else
                    return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/ProductPrice/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_ProductXmlErpId:${$("#ajax_data_search_product_xmlerp_id").val() != undefined ? $("#ajax_data_search_product_xmlerp_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}`
            },
            [
                {
                    Name: "",
                    Type: "number",
                    Render: function (value) {
                        var html = '';
                        if (value.marketplaceBulkPriceOpen)
                            html = `<button type="button" onclick="ProductPriceIndex.DeleteMarketplacePrice(${value.id})" class="btn btn-danger">Çıkar</button>`;

                        return html;
                    }
                }, {
                    Name: "",
                    Type: "string",
                    Render: (value, parentValue) => value.product.productSourceDomainName,
                    Unified: true
                }, {
                    Name: "",
                    Type: "string",
                    Render: (value, parentValue) => value.product.sellerCode,
                    Unified: true
                }, {
                    Render: function (value) {
                        debugger
                        var html = `
<table class="table" style="margin: 0;">
    <tbody>
    <tr>
            <td  width="70%">
                <img class="table-product-img" height="100"  src="${value.productInformationPhotos[0].fileName}"/>
            </td>
            <td width="30%">
                <div style="font-weight: 500">${value.stock} Adet</div>
                <div>${value.productInformationTranslations[0].name}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.stockCode}');Notify.Show(NotifyType.Info, '<b>${value.stockCode}</b> kopyalandı.');">${value.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.barcode}');Notify.Show(NotifyType.Info, '<b>${value.barcode}</b> kopyalandı.');">${value.barcode} <i class="la la-copy"></i> </a>
                </div>
            </td>
</tr>
    </tbody>
</table>
`;
                        return html;

                    }
                }, {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        var logoUrl = $('#logo').attr("src");
                        var html = '';


                        html += `${value.productInformationMarketplaces.map(theProductInformationMarketplace => `
${theProductInformationMarketplace.url == null ? '' :

                                `<a target="_blank" href="${theProductInformationMarketplace.url}">`}
    <span class="kt-badge kt-badge--${theProductInformationMarketplace.active ? (theProductInformationMarketplace.opened ? 'success' : 'warning') : 'danger'} kt-badge--inline kt-badge--pill"  style="margin-top: 15px;width:45%">
        <i class="la la-${theProductInformationMarketplace.opened ? 'check' : 'close'}" style="margin-right: 5px;"></i>
        <img src="/org/img/marketplace-icons/${theProductInformationMarketplace.marketplaceId}.png" width="50" style="margin-right: 15px;" />
        <span>
   
            Liste Fiyatı: ${theProductInformationMarketplace.listUnitPrice} ₺ <br />Satış Fiyatı: ${theProductInformationMarketplace.unitPrice} ₺ 
                  
${value.includeAccounting ? `<label style="display: flex;align-items: center;">
                            <i class="la la-${theProductInformationMarketplace.accountingPrice ? 'check' : 'close'}" ></i>
                            <div class="small ml-1">Xml / Erp için <br>Fiyat Güncellensin</div>
                         </label>` : ''}
                        
 ${`<label style="display: flex;align-items: center;">
                            <i class="la la-${theProductInformationMarketplace.disabledUpdateProduct ? 'close' : 'check'}" ></i>
                            <div class="small ml-1">Ürün / Fiyat için <br>Güncelleme Yapılsın</div>
                         </label>`}  

</span>
      

    </span>

  
 
${theProductInformationMarketplace.url == null ? '' : `</a>`}`).join('')
                            }`;

                        return html;
                    }
                }, {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        var html = '';




                        value.productInformationMarketplaces.map(theProductInformationMarketplace => {

                            if (theProductInformationMarketplace.productInformationMarketplaceBulkPrice != null) {
                                debugger
                                html += `${theProductInformationMarketplace.url == null ? '' : `<a target="_blank" href="${theProductInformationMarketplace.url}">`}
    <span class="kt-badge kt-badge--${theProductInformationMarketplace.productInformationMarketplaceBulkPrice.active ? (theProductInformationMarketplace.opened ? 'success' : 'warning') : 'danger'} kt-badge--inline kt-badge--pill"  style="margin-top: 15px;width:80%">
        <i class="la la-${theProductInformationMarketplace.opened ? 'check' : 'close'}" style="margin-right: 5px;"></i>
        <img src="/org/img/marketplace-icons/${theProductInformationMarketplace.marketplaceId}.png" width="50" style="margin-right: 15px;" />
        <span>
            Liste Fiyatı: ${theProductInformationMarketplace.productInformationMarketplaceBulkPrice.listUnitPrice} ₺ <br />Satış Fiyatı: ${theProductInformationMarketplace.productInformationMarketplaceBulkPrice.unitPrice} ₺
                   ${value.includeAccounting ? ` <label style="display: flex;align-items: center;">
                            <i class="la la-${theProductInformationMarketplace.productInformationMarketplaceBulkPrice.accountingPrice ? 'check' : 'close'}" ></i>
                            <div class="small ml-1">Xml / Erp için <br>Fiyat Güncellensin</div>
                         </label>` : ''}
                   
        </span>
    </span>
${theProductInformationMarketplace.url == null ? '' : `</a>`}`;
                            }

                        });
                        return html;
                    }
                }],
            "ProductPrice",
            [],
            false,
            false,
            null
        );
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ProductPriceIndex.Read(0);
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                select: {
                    required: true
                },
                option: {
                    required: true
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },
                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenCreatePriceModal: function () {
        HttpClient.Get("/ProductPrice/Create", function (response) {

            $("#product_price_create_modal_title").html("Toplu Fiyat İşlemi Oluştur");
            $("#product_price_create_modal_body").html(response);
            $("#product_price_create_modal_submit").unbind("click");
            $("#product_price_create_modal").modal();

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });
            $('.kt-selectpicker').selectpicker();

            ProductPriceIndex.ModalValidation("product_price_create_form", function () {
                debugger
                let productPrice = Serializer.Serialize("product_price_create_form");
                productPrice.Search = `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_ProductXmlErpId:${$("#ajax_data_search_product_xmlerp_id").val() != undefined ? $("#ajax_data_search_product_xmlerp_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}`;
                debugger
                HttpClient.Post(
                    "/ProductPrice/Create",
                    productPrice,
                    function (response) {
                        if (response.success) {
                            ProductPriceIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_price_create_modal").modal("hide");
                            $("#product_price_create_modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });


            $("#product_price_create_modal_submit").click(function () {
                $("#product_price_create_form").submit();
            });

        });
    },
    OpenUpdatePriceModal: function () {

        HttpClient.Get("/ProductPrice/Update", function (response) {

            $("#product_price_update_modal_title").html("Toplu Fiyat İşlemi Güncelle");
            $("#product_price_update_modal_body").html(response);
            $("#product_price_update_modal_submit").unbind("click");
            $("#product_price_delete_modal_submit").unbind("click");
            $("#product_price_update_modal").modal();




            $("#product_price_update_modal_submit").click(function () {
                HttpClient.Post(
                    "/ProductPrice/BulkUpdate",
                    {},
                    function (response) {
                        if (response.success) {
                            ProductPriceIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_price_update_modal").modal("hide");
                            $("#product_price_update_modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#product_price_delete_modal_submit").click(function () {
                HttpClient.Post(
                    "/ProductPrice/BulkDelete",
                    {},
                    function (response) {
                        if (response.success) {
                            ProductPriceIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_price_update_modal").modal("hide");
                            $("#product_price_update_modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });
        });


    },
    OpenAccountingPriceModal: function () {
        var search = `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_ProductXmlErpId:${$("#ajax_data_search_product_xmlerp_id").val() != undefined ? $("#ajax_data_search_product_xmlerp_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}`

        HttpClient.Get(`/ProductPrice/AccountingPricePartial?search=${search}`, function (response) {

            $("#product_price_accounting_price_modal_title").html("Pazaryerinde Toplu Erp/Xml Fiyat Güncelle");
            $("#product_price_accounting_price_modal_body").html(response);
            $("#product_price_accounting_price_modal_submit").unbind("click");
            $("#product_price_accounting_price_modal").modal();
            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });
            $('.kt-selectpicker').selectpicker();




            $("#product_price_accounting_price_modal_submit").click(function () {
                if ($('.kt-checkbox-marketplace-accounting-price [type="checkbox"]:checked').length == 0) {
                    Notify.Show(NotifyType.Error, "Lütfen en az 1 pazaryeri seçiniz.");
                    return;
                }

                var ids = $('.kt-checkbox-marketplace-accounting-price [type="checkbox"]:checked').map((i, e) => e.value).get();



                HttpClient.Post(
                    "/ProductPrice/BulkAccountingUpdate",
                    {
                        marketplaceIds: ids,
                        accountingPriceId: parseInt($('#accountingPriceId').val()),
                        search: search
                    },
                    function (response) {
                        if (response.success) {
                            ProductPriceIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_price_accounting_price_modal").modal("hide");
                            $("#product_price_accounting_price_modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });


        });


    },
    OpenProductUpdatePriceModal: function () {
        var search = `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_ProductXmlErpId:${$("#ajax_data_search_product_xmlerp_id").val() != undefined ? $("#ajax_data_search_product_xmlerp_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}`

        HttpClient.Get(`/ProductPrice/ProductUpdatePricePartial?search=${search}`, function (response) {

            $("#product_price_update_price_modal_title").html("Pazaryerinde Toplu Ürün Ve Fiyat Güncellenmesin Aç/Kapat");
            $("#product_price_update_price_modal_body").html(response);
            $("#product_price_update_price_modal_submit").unbind("click");
            $("#product_price_update_price_modal").modal();
            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });
            $('.kt-selectpicker').selectpicker();




            $("#product_price_update_price_modal_submit").click(function () {
                if ($('.kt-checkbox-marketplace-update-price [type="checkbox"]:checked').length == 0) {
                    Notify.Show(NotifyType.Error, "Lütfen en az 1 pazaryeri seçiniz.");
                    return;
                }

                var ids = $('.kt-checkbox-marketplace-update-price [type="checkbox"]:checked').map((i, e) => e.value).get();



                HttpClient.Post(
                    "/ProductPrice/BulkProductMarketplaceUpdatePrice",
                    {
                        marketplaceIds: ids,
                        ProductUpdatePriceId: parseInt($('#updatePriceId').val()),
                        search: search
                    },
                    function (response) {
                        if (response.success) {
                            ProductPriceIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_price_update_price_modal").modal("hide");
                            $("#product_price_update_price_modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });


        });


    },
    AccountingPriceCheckAll: function (checked) {

        $('.kt-checkbox-marketplace-accounting-price [type="checkbox"]').prop('checked', checked);
    },
    UpdatePriceCheckAll: function (checked) {

        $('.kt-checkbox-marketplace-update-price [type="checkbox"]').prop('checked', checked);
    },
    DownloadBulkMarketplace: function () {
        var search = `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_ProductXmlErpId:${$("#ajax_data_search_product_xmlerp_id").val() != undefined ? $("#ajax_data_search_product_xmlerp_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}`

        window.open(`/ProductPrice/ExcelDownload?search=${search}`);
    },
    OpenBulkMarketplaceModal: function () {
        HttpClient.Get("/ProductPrice/UploadExcelPartial", function (response) {
            $("#bulk_marketplace_modal_title").html("Toplu Excel İşlemi Oluştur");

            $("#bulk_marketplace_modal_body").html(response);
            $("#bulk_marketplace_modal").modal();

        });
    },
    UpdateBulkMarketplace: function () {

        debugger
        if (document.getElementById("bulk-marketplace-file").files.length == 0) {

            Notify.Show(NotifyType.Error, "Lütfen indirmiş olduğunuz excel dosyasını yükleyiniz.");
            return;
        }
        var formData = new FormData();
        formData.append("file", document.getElementById("bulk-marketplace-file").files[0]);

        HttpClient.PostFile("/ProductPrice/ExcelUpload", formData, function (response) {
            if (response.success) {
                ProductPriceIndex.Read(0);

                Notify.Show(NotifyType.Success, response.message);

                $("#bulk_marketplace_modal").modal("hide");
                $("#bulk_marketplace_modal_body").html('');

            }
            else {
                Notify.Show(NotifyType.Error, response.message);
            }
        });
    },
    DeleteMarketplacePrice: function (id) {

        HttpClient.Post(
            "/ProductPrice/Delete",
            {
                id: id
            },
            function (response) {
                if (response.success) {
                    var activePage = $("#ajax_data_page").val();
                    ProductPriceIndex.Read(activePage);

                    Notify.Show(NotifyType.Success, response.message);

                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    }

};

$(document).ready(() => {
    ProductPriceIndex.Initialize(0);
});