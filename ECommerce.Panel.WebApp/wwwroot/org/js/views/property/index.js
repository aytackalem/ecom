﻿"use strict";

const PropertyIndex = {
    Initialize: function () {
        PropertyIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Property/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "string"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.propertyTranslations[0].name;
                    }
                }
            ],
            "Property",
            [{
                Render: function (value) {
                    return `<a class="dropdown-item" href="javascript:PropertyIndex.OpenPropertyValueModal('${value["id"]}')"><i class="la la-edit"></i>
                            Ürün Özellik Değerleri
                        </a>`+ `<a class="dropdown-item" href="javascript:PropertyIndex.Delete({ id: ${value["id"]}},'${value.propertyTranslations[0].name}')"><i class="la la-edit"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Property/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#property_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Ürün Özellik Güncelle");

            PropertyIndex.ModalValidation("property_update_form", function () {
                const property = {
                    id: $('[name="id"]').val(),
                    propertyTranslations: [{
                        name: $('[name="name"]').val(),
                        languageId: "TR"
                    }]
                };

                HttpClient.Post(
                    "/Property/Update",
                    property,
                    function (response) {
                        if (response.success) {
                            PropertyIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#property_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#property_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Property/Create", function (response) {
            $("#modal_body").html(response);
            $("#property_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Ürün Özellik");

            PropertyIndex.ModalValidation("property_create_form", function () {
                const property = {
                    id: $('[name="id"]').val(),
                    propertyTranslations: [{
                        name: $('[name="name"]').val(),
                        languageId: "TR"
                    }]
                };

                HttpClient.Post(
                    "/Property/Create",
                    property,
                    function (response) {
                        if (response.success) {
                            PropertyIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#property_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#property_create_form").submit();
            });
        });
    },
    OpenPropertyValueModal: function (productPropertyId) {
        HttpClient.Get(`/PropertyValue/Read?PropertyId=${productPropertyId}`, function (response) {
            $("#property_value_modal_body").html(response);
            $("#property_value_modal").modal();
        });
    },
    CreatePropertyValue: function () {
        const propertyValue = {
            propertyId: $('[name="property_id"]').val(),
            propertyValueTranslations: [{
                value: $('#new_property_value [name="value"]').val(),
                languageId: "TR"
            }]
        };

        HttpClient.Post(
            "/PropertyValue/Create",
            propertyValue,
            function (response) {
                if (response.success) {
                    PropertyIndex.OpenPropertyValueModal(propertyValue.propertyId);

                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    UpdatePropertyValue: function (id) {
        const propertyValue = {
            id: $(`[name="id_${id}"]`).val(),
            propertyId: $(`[name="property_id"]`).val(),
            propertyValueTranslations: [{
                value: $(`[name="value_${id}"]`).val(),
                languageId: "TR"
            }]
        };

        HttpClient.Post(
            "/PropertyValue/Update",
            propertyValue,
            function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    Delete: function (property, header) {


        SwalAlertModal.ConfirmPopup('Özellik Sil', `${header} silinsin mi?`, 'Evet', true, 'Hayır',
            function () {

                HttpClient.Post(
                    "/Property/Delete",
                    property,
                    function (response) {

                        if (response.success) {
                            PropertyIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                )
            }
        )



    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        PropertyIndex.Read(0);
    },
};

$(document).ready(() => {
    PropertyIndex.Initialize(0);
});