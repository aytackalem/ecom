﻿const FacebookConfigrationIndex = {
    Initialize: function () {
        FacebookConfigrationIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/FacebookConfigration/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                { Name: "id", Type: "number" },
                { Name: "name", Type: "string" },
            { Name: "active", Type: "bool" }],
            "FacebookConfigration",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var facebookConfigration = element.closest('.input-facebookConfigration');
                if (facebookConfigration.length) {
                    facebookConfigration.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/FacebookConfigration/Update?Id=${id}`, function (response) {
            debugger
            $("#modal_body").html(response);
            $("#facebookConfigration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Facebook Konfigrasyon Güncelle");
            debugger
            FacebookConfigrationIndex.ModalValidation("facebookConfigration_update_form", function () {
                const facebookConfigration = Serializer.Serialize("facebookConfigration_update_form");
                debugger
                HttpClient.Post(
                    "/FacebookConfigration/Update",
                    facebookConfigration,
                    function (response) {
                        if (response.success) {
                            FacebookConfigrationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#facebookConfigration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#facebookConfigration_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/FacebookConfigration/Create", function (response) {
            $("#modal_body").html(response);
            $("#facebookConfigration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Facebook Konfigrasyon");

            FacebookConfigrationIndex.ModalValidation("facebookConfigration_create_form", function () {
                const facebookConfigration = Serializer.Serialize("facebookConfigration_create_form");

                HttpClient.Post(
                    "/FacebookConfigration/Create",
                    facebookConfigration,
                    function (response) {
                        if (response.success) {
                            FacebookConfigrationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#facebookConfigration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#facebookConfigration_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        FacebookConfigrationIndex.Read(0);
    },
};

$(document).ready(() => {
    FacebookConfigrationIndex.Initialize(0);
});