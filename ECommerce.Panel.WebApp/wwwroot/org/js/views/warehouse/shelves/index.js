﻿const ShelvesIndex = {
    Initialize: function () {
        ShelvesIndex.Read(0);

        $('.select-picker').selectpicker();

        $('#print_submit').on('click', function () {
            debugger;
            ShelvesIndex.Print();
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Warehouse/Shelves/Listing",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val()
            },
            [{
                Name: "",
                Type: "string",
                Render: function (value) {
                    return `<div class="form-group">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="${value.id}" data-name="${value.name}">
                            <span></span>
                        </label>
                    </div>`;
                }
            }, { Name: "id", Type: "number" }, { Name: "name", Type: "string" }],
            "Shelves",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Warehouse/Shelves/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#shelf_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Raf Güncelle");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Floors/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#floor-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#floor-id').html(html);

                    $('#floor-id').selectpicker('refresh');

                    $('#hallway-id').empty();
                    $('#hallway-id').html('<option value="-1">Seçiniz</option>');

                    $('#hallway-id').selectpicker('refresh');

                });

            });

            $('#floor-id').change(function () {

                HttpClient.Get(`/Warehouse/Hallways/Listing?Page=0&PageRecordsCount=100&FloorId=${$(this).val()}`, function (response) {

                    $('#hallway-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#hallway-id').html(html);

                    $('#hallway-id').selectpicker('refresh');

                });

            });

            ShelvesIndex.ModalValidation("shelf_update_form", function () {
                HttpClient.Post(
                    "/Warehouse/Shelves/Update",
                    Serializer.Serialize("shelf_update_form"),
                    function (response) {
                        if (response.success) {
                            ShelvesIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#shelf_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#shelf_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Warehouse/Shelves/Create", function (response) {
            $("#modal_body").html(response);
            $("#shelf_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Raf");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Floors/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#floor-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#floor-id').html(html);

                    $('#floor-id').selectpicker('refresh');

                    $('#hallway-id').empty();
                    $('#hallway-id').html('<option value="-1">Seçiniz</option>');

                    $('#hallway-id').selectpicker('refresh');

                });

            });

            $('#floor-id').change(function () {

                HttpClient.Get(`/Warehouse/Hallways/Listing?Page=0&PageRecordsCount=100&FloorId=${$(this).val()}`, function (response) {

                    $('#hallway-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#hallway-id').html(html);

                    $('#hallway-id').selectpicker('refresh');

                });

            });

            ShelvesIndex.ModalValidation("shelf_create_form", function () {
                HttpClient.Post(
                    "/Warehouse/Shelves/Create",
                    Serializer.Serialize("shelf_create_form"),
                    function (response) {
                        if (response.success) {
                            ShelvesIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#shelf_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#shelf_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ShelvesIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ShelvesIndex.Read(0);
    },
    PrintModal: function () {

        if ($('.kt-checkbox [type="checkbox"]:checked').length == 0) {
            Notify.Show(NotifyType.Error, "Lütfen en az 1 raf seçiniz.");
        }
        else {
            $("#print_modal").modal();
        }

    },
    Print: function () {

        var data = {
            Shelves: [],
            Width: parseInt($('#print_width').val()),
            Height: parseInt($('#print_height').val())
        };

        data.Shelves = $('.kt-checkbox [type="checkbox"]:checked')
            .map(function (i, e) {
                return {
                    Id: parseInt(e.value),
                    Name: $(e).data('name')
                };
            })
            .get();

        window.open(`/Warehouse/Shelves/Print?Data=${JSON.stringify(data)}`);



    }
};

$(document).ready(() => {
    ShelvesIndex.Initialize();
});