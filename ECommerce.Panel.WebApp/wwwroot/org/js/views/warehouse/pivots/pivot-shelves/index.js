﻿const PivotShelvesIndex = {
    Initialize: function () {
        PivotShelvesIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Warehouse/Pivots/PivotShelves/Listing",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val()
            },
            [{ Name: "id", Type: "number" }, { Name: "name", Type: "string" }],
            "PivotShelves",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Warehouse/Pivots/PivotShelves/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#pivot_shelf_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Pivot Raf Güncelle");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Pivots/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#pivot-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#pivot-id').html(html);

                    $('#pivot-id').selectpicker('refresh');

                });

            });

            PivotShelvesIndex.ModalValidation("pivot_shelf_update_form", function () {
                debugger
                HttpClient.Post(
                    "/Warehouse/Pivots/PivotShelves/Update",
                    Serializer.Serialize("pivot_shelf_update_form"),
                    function (response) {
                        if (response.success) {
                            PivotShelvesIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#pivot_shelf_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#pivot_shelf_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Warehouse/Pivots/PivotShelves/Create", function (response) {
            $("#modal_body").html(response);
            $("#pivot_shelf_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Pivot Raf");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Pivots/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#pivot-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#pivot-id').html(html);

                    $('#pivot-id').selectpicker('refresh');

                });

            });

            PivotShelvesIndex.ModalValidation("pivot_shelf_create_form", function () {
                HttpClient.Post(
                    "/Warehouse/Pivots/PivotShelves/Create",
                    Serializer.Serialize("pivot_shelf_create_form"),
                    function (response) {
                        if (response.success) {
                            PivotShelvesIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#pivot_shelf_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#pivot_shelf_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        PivotShelvesIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        PivotShelvesIndex.Read(0);
    },
};

$(document).ready(() => {
    PivotShelvesIndex.Initialize();
});