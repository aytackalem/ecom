﻿const HallwaysIndex = {
    Initialize: function () {
        HallwaysIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Warehouse/Hallways/Listing",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val()
            },
            [{ Name: "id", Type: "number" }, { Name: "name", Type: "string" }],
            "Hallways",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Warehouse/Hallways/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#hallway_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Koridor Güncelle");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Floors/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#floor-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#floor-id').html(html);

                    $('#floor-id').selectpicker('refresh');

                });

            });

            HallwaysIndex.ModalValidation("hallway_update_form", function () {
                HttpClient.Post(
                    "/Warehouse/Hallways/Update",
                    Serializer.Serialize("hallway_update_form"),
                    function (response) {
                        if (response.success) {
                            HallwaysIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#hallway_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#hallway_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Warehouse/Hallways/Create", function (response) {
            $("#modal_body").html(response);
            $("#hallway_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Koridor");

            $('.select-picker').selectpicker();

            $('#warehouse-id').change(function () {

                HttpClient.Get(`/Warehouse/Floors/Listing?Page=0&PageRecordsCount=100&WarehouseId=${$(this).val()}`, function (response) {

                    $('#floor-id').empty();
                    var html = '<option value="-1">Seçiniz</option>';

                    for (var i = 0; i < response.data.length; i++) {
                        html += `<option value="${response.data[i].id}">${response.data[i].name}</option>`;
                    }

                    $('#floor-id').html(html);

                    $('#floor-id').selectpicker('refresh');

                });

            });

            HallwaysIndex.ModalValidation("hallway_create_form", function () {
                HttpClient.Post(
                    "/Warehouse/Hallways/Create",
                    Serializer.Serialize("hallway_create_form"),
                    function (response) {
                        if (response.success) {
                            HallwaysIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#hallway_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#hallway_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        HallwaysIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        HallwaysIndex.Read(0);
    },
};

$(document).ready(() => {
    HallwaysIndex.Initialize();
});