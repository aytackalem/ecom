﻿const LimitsIndex = {
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    Initialize: function () {
        LimitsIndex.ModalValidation("limits-form", function () {

            var data = Serializer.Serialize("limits-form");

            if (data.id == 0) {
                HttpClient.Post(
                    "/Warehouse/Limits/Create",
                    data,
                    function (response) {
                        if (response.success) {
                            $('[name="id"]').val(response.data);

                            Notify.Show(NotifyType.Success, response.messages[0]);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            }
            else {
                HttpClient.Put(
                    "/Warehouse/Limits/Update",
                    data,
                    function (response) {
                        if (response.success) {
                            Notify.Show(NotifyType.Success, response.messages[0]);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            }

        });

        $("#modal-submit").click(function () {
            $("#limits-form").submit();
        });
    }
};

$(document).ready(() => {
    LimitsIndex.Initialize();
});