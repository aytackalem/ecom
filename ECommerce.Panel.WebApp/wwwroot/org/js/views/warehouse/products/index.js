﻿const ProductsIndex = {
    Initialize: function () {
        ProductsIndex.Read(0);

        $('.select-picker').selectpicker();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        let barcode = $('#ajax_data_search_barcode').val();

        Table.Initialize(
            `/Warehouse/Products/Listing`,
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search:`Barcode:${barcode}`
            },
            [
                { Name: "id", Type: "number" },
                { Name: "shelf", Type: "string" },
                { Name: "barcode", Type: "string" },
                { Name: "stock", Type: "number" },
            ],
            "Products",
            null,
            false
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Warehouse/Products/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#workorder_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("İş Emri Güncelle");

            ProductsIndex.ModalValidation("workorder_update_form", function () {
                HttpClient.Post(
                    "/Warehouse/Products/Update",
                    Serializer.Serialize("workorder_update_form"),
                    function (response) {
                        if (response.success) {
                            ProductsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#workorder_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#workorder_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Warehouse/Products/Create", function (response) {
            $("#modal_body").html(response);
            $("#workorder_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Depo");

            ProductsIndex.ModalValidation("workorder_create_form", function () {
                HttpClient.Post(
                    "/Warehouse/Products/Create",
                    Serializer.Serialize("workorder_create_form"),
                    function (response) {
                        if (response.success) {
                            ProductsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.messages[0]);

                            $("#workorder_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.messages[0]);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#workorder_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ProductsIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ProductsIndex.Read(0);
    },
};

$(document).ready(() => {
    ProductsIndex.Initialize();
});