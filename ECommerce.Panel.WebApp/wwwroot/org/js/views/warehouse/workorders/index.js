﻿const WorkOrdersIndex = {
    Initialize: function () {
        $('.select-picker').selectpicker();

        $.fn.datepicker.dates['tr'] =
            {
            days: [
                "Pz",
                "Pt",
                "Sl",
                "Çr",
                "Pr",
                "Cm",
                "Ct"
            ],
                daysShort: [
                    "Pz",
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct"
                ],
                    daysMin: [
                        "Pz",
                        "Pt",
                        "Sl",
                        "Çr",
                        "Pr",
                        "Cm",
                        "Ct",
                    ],
                        months: [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                            monthsShort: [
                                "Ocak",
                                "Şubat",
                                "Mart",
                                "Nisan",
                                "Mayıs",
                                "Haziran",
                                "Temmuz",
                                "Ağustos",
                                "Eylül",
                                "Ekim",
                                "Kasım",
                                "Aralık"
                            ],
                                today: "Bugün"
        };

    $('#ajax_data_search_created_date_time').datepicker({
        isRTL: false,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        language: 'tr',
        autoclose: true
    });
},
    Read: function (page) {
        $("#ajax_data_page").val(page);

let id = $('#ajax_data_search_workorder_id').val();
let orderId = $('#ajax_data_search_order_id').val();
let status = $('#ajax_data_search_status_id').val();
let createdDateTime = $('#ajax_data_search_created_date_time').val();

Table.Initialize(
    `/Warehouse/WorkOrders/Listing`,
    {
        Page: $("#ajax_data_page").val(),
        PageRecordsCount: $(".ajax_data_length").val(),
        Search: `Id:${id}_OrderId:${orderId}_Status:${status}_CreatedDateTime:${createdDateTime}`
    },
    [
        { Name: "id", Type: "number" },
        {
            Name: "status", Type: "string", Render: function (value) {

                let badgeType = 'warning';
                let badgeText = 'Bekliyor';

                if (value.status == 1) {
                    badgeType = 'brand';
                    badgeText = 'Toplanıyor';
                }
                else if (value.status == 2) {
                    badgeType = 'brand';
                    badgeText = 'Toplandı';
                }
                else if (value.status == 3) {
                    badgeType = 'brand';
                    badgeText = 'Paketleniyor';
                }
                else if (value.status == 4) {
                    badgeType = 'success';
                    badgeText = 'Paketlendi';
                }

                return `<span class="kt-badge kt-badge--inline kt-badge--${badgeType}">${badgeText}</span>`;

            }
        },
        {
            Render: function (value) {
                return value.single ? "Tekli" : "Çoklu";
            }
        },
        {
            Render: function (value) {

                var html = '<div class="kt-list-timeline"><div class="kt-list-timeline__items">';

                html += '<div class="kt-list-timeline__item">';
                html += '<span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>';
                html += '<span class="kt-list-timeline__text">Oluşturuldu</span>';
                html += `<span class="kt-list-timeline__time">${value.createdDateTimeString}</span>`;
                html += '</div>';

                if (value.pickingDateTime != null) {
                    html += '<div class="kt-list-timeline__item">';
                    html += '<span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>';
                    html += '<span class="kt-list-timeline__text">Toplanıyor</span>';
                    html += `<span class="kt-list-timeline__time">${value.pickingDateTimeString}</span>`;
                    html += '</div>';
                }

                if (value.pickedDateTime != null) {
                    html += '<div class="kt-list-timeline__item">';
                    html += '<span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>';
                    html += '<span class="kt-list-timeline__text">Toplandı</span>';
                    html += `<span class="kt-list-timeline__time">${value.pickedDateTimeString}</span>`;
                    html += '</div>';
                }

                if (value.packingDateTime != null) {
                    html += '<div class="kt-list-timeline__item">';
                    html += '<span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>';
                    html += '<span class="kt-list-timeline__text">Paketleniyor</span>';
                    html += `<span class="kt-list-timeline__time">${value.packingDateTimeString}</span>`;
                    html += '</div>';
                }

                if (value.packedDateTime != null) {
                    html += '<div class="kt-list-timeline__item">';
                    html += '<span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>';
                    html += '<span class="kt-list-timeline__text">Paketlendi</span>';
                    html += `<span class="kt-list-timeline__time">${value.packedDateTimeString}</span>`;
                    html += '</div>';
                }

                html += '</div></div>';

                return html;

            }
        }
    ],
    "WorkOrders",
    null,
    true
);
    },
ModalValidation: function (formName, submitFunction) {
    $(`#${formName}`).validate({
        // define validation rules
        rules: {
            email: {
                required: true,
                email: true,
                minlength: 10
            },
            url: {
                required: true
            },
            digits: {
                required: true,
                digits: true
            },
            creditcard: {
                required: true,
                creditcard: true
            },
            phone: {
                required: true,
                phoneUS: true
            },
            option: {
                required: true
            },
            options: {
                required: true,
                minlength: 2,
                maxlength: 4
            },
            memo: {
                required: true,
                minlength: 10,
                maxlength: 100
            },

            checkbox: {
                required: true
            },
            checkboxes: {
                required: true,
                minlength: 1,
                maxlength: 2
            },
            radio: {
                required: true
            }
        },

        errorPlacement: function (error, element) {
            var group = element.closest('.input-group');
            if (group.length) {
                group.after(error.addClass('invalid-feedback'));
            } else {
                element.after(error.addClass('invalid-feedback'));
            }
        },

        //display error alert on form submit
        invalidHandler: function (event, validator) {
            var alert = $('#kt_form_1_msg');
            alert.removeClass('kt--hide').show();
            KTUtil.scrollTop();
        },

        submitHandler: function (form) {
            submitFunction();
        }
    });
},
OpenUpdateModal: function (id) {
    HttpClient.Get(`/Warehouse/WorkOrders/Update?Id=${id}`, function (response) {
        $("#modal_body").html(response);
        $("#workorder_modal").modal();
        $("#modal_submit").unbind("click");
        $("#modal_title").html("İş Emri Güncelle");

        WorkOrdersIndex.ModalValidation("workorder_update_form", function () {
            HttpClient.Post(
                "/Warehouse/WorkOrders/Update",
                Serializer.Serialize("workorder_update_form"),
                function (response) {
                    if (response.success) {
                        WorkOrdersIndex.Read(0);

                        Notify.Show(NotifyType.Success, response.messages[0]);

                        $("#workorder_modal").modal("hide");
                    }
                    else {
                        Notify.Show(NotifyType.Error, response.messages[0]);
                    }
                }
            );
        });

        $("#modal_submit").click(function () {
            $("#workorder_update_form").submit();
        });
    });
},
OpenCreateModal: function () {
    HttpClient.Get("/Warehouse/WorkOrders/Create", function (response) {
        $("#modal_body").html(response);
        $("#workorder_modal").modal();
        $("#modal_submit").unbind("click");
        $("#modal_title").html("Yeni Depo");

        WorkOrdersIndex.ModalValidation("workorder_create_form", function () {
            HttpClient.Post(
                "/Warehouse/WorkOrders/Create",
                Serializer.Serialize("workorder_create_form"),
                function (response) {
                    if (response.success) {
                        WorkOrdersIndex.Read(0);

                        Notify.Show(NotifyType.Success, response.messages[0]);

                        $("#workorder_modal").modal("hide");
                    }
                    else {
                        Notify.Show(NotifyType.Error, response.messages[0]);
                    }
                }
            );
        });

        $("#modal_submit").click(function () {
            $("#workorder_create_form").submit();
        });
    });
},
ChangeSorting: function (sender) {
    var sorting = $(sender).val();
    $(".ajax_data_sorting").val(sorting);
    WorkOrdersIndex.Read(0);
},
ChangePageRecordsCount: function (sender) {
    var pageRecordsCount = $(sender).val();
    $(".ajax_data_length").val(pageRecordsCount);
    WorkOrdersIndex.Read(0);
},
ReadKpiPicking: function (page) {
    $("#ajax_data_page").val(page);

    let id = $('#ajax_data_search_id').val();
    let personelName = $('#ajax_data_search_personel_name').val();

    Table.Initialize(
        `/Warehouse/WorkOrders/KpiPickingListing`,
        {
            Page: $("#ajax_data_page").val(),
            PageRecordsCount: $(".ajax_data_length").val(),
            Search: `Id:${id}_PersonelName:${personelName}`
        },
        [
            { Name: "id", Type: "number" },
            { Name: "startedString", Type: "string" },
            { Name: "finishedString", Type: "string" },
            {
                Render: function (value) {
                    return `${value.remain} dk`;
                } },
            { Name: "personelName", Type: "string" }
        ],
        "KpiPicking",
        null,
        false
    );
},
ReadKpiPacking: function (page) {
    $("#ajax_data_page").val(page);

    let id = $('#ajax_data_search_id').val();
    let personelName = $('#ajax_data_search_personel_name').val();

    Table.Initialize(
        `/Warehouse/WorkOrders/KpiPackingListing`,
        {
            Page: $("#ajax_data_page").val(),
            PageRecordsCount: $(".ajax_data_length").val(),
            Search: `Id:${id}_PersonelName:${personelName}`
        },
        [
            { Name: "id", Type: "number" },
            { Name: "startedString", Type: "string" },
            { Name: "finishedString", Type: "string" },
            {
                Render: function (value) {
                    return `${value.remain} dk`;
                }
            },
            { Name: "personelName", Type: "string" }
        ],
        "KpiPacking",
        null,
        false
    );
}
};

$(document).ready(() => {
    WorkOrdersIndex.Initialize();
});