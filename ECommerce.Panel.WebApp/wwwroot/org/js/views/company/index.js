﻿"use strict";

function CompanyDataTable() {
};

var _companyDataTable;

const CompanyIndex = {
    Initialize: function () {

        CompanyDataTable.prototype = new DataTable({
            BaseUrl: '/Company',
            Name: 'Pazaryeri',
            Pagination: {
                Click: 'javascript:_companyDataTable.SetPage(#index);_companyDataTable.GetData()'
            },
            DataTableRow: new DataTableRow({
                DataTableColumns: [
                    new DataTableColumn({
                        Name: 'id',
                        Type: DataTableColumnTypes.Int
                    }),
                    new DataTableColumn({
                        Name: 'name',
                        Type: DataTableColumnTypes.String
                    })],
                DataTableRowMenu: new DataTableRowMenu({
                    Items: [{
                        Icon: 'la la-edit',
                        Name: 'Güncelle',
                        Click: 'javascript:_companyDataTable.OpenUpdateModal(#id)'
                    }]
                })
            })
        });
        _companyDataTable = new CompanyDataTable();
        _companyDataTable.GetData();
        _companyDataTable.OpenUpdateModal = (id) => {
            var modal = new Modal();
            modal.Open({
                Title: 'Firma Güncelle',
                ButtonText: 'Güncelle',
                GetUrl: `/Company/Update?Id=${id}`,
                PostUrl: '/Company/Update',
                Validation: true,
                Opened: function () {
                    const preview = document.getElementById('fileNameImage-preview');
                    if (preview.src == "") {
                        preview.style.display = "none";
                    }

                    $('#select-fileNameImage').click(function () {
                        $('#fileNameImage').click();
                    });

                    $('#fileNameImage').change(function () {
                        const iconUrl = document.getElementById('fileNameImageUrl');
                        const file = document.getElementById('fileNameImage').files[0];
                        const reader = new FileReader();

                        reader.addEventListener("load", function () {
                            preview.src = reader.result;
                            preview.style.display = "";
                            iconUrl.value = reader.result;
                        }, false);

                        if (file) {
                            reader.readAsDataURL(file);
                        }
                    });
                }
            });
        }

    }
};

$(document).ready(() => {
    CompanyIndex.Initialize();
});