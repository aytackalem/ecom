﻿var _code;

const LicenseIndex = {
    Initialize: function () {
        $('.kt-select2').select2();
    },
    OpenModal: function (code, name, amount) {
        _code = code;

        $('#modal_title').html(`Online Ödeme ${name} ${amount} TL`);
        $("#online_payment_modal").modal();
    },
    Create: function () {
        var data = Serializer.Serialize("online-payment");
        data.code = _code;

        HttpClient.Post('/License/Create', data, function (response) {
            if (response.success) {
                $('#online_payment_modal .modal-body').html('');

                var iFrame = $('<iframe>');
                iFrame.width('100%');
                iFrame.height('100%');

                var div3dModalBody = $('#online_payment_modal .modal-body');
                div3dModalBody.append(iFrame);

                iFrame.attr('src', 'about:blank');
                var iFrameDocument = iFrame[0].contentWindow.document;
                iFrameDocument.open();
                iFrameDocument.write(response.data);
                iFrameDocument.close();
            }
            else {
                alert('Error');
            }
        });
    }
};

$(document).ready(() => {
    LicenseIndex.Initialize();
});