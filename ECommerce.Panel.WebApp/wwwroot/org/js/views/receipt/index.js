﻿const ModalSize = {
    Small: 'sm',
    Medium: 'md',
    Large: 'lg',
    Larger: 'xl'
};

const ReceiptIndex = {
    Initialize: function () {

        ReceiptIndex.Read();

        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            ReceiptIndex.Read(0);
        });

    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ReceiptIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ReceiptIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Receipt/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `SellerCode:${$("#ajax_data_search_seller_code").val()}_SupplierId:${$("#ajax_data_search_supplier_id").val()}_DeadlineType:${$("#ajax_data_search_deadline_type").val()}_Delivered:${$("#ajax_data_search_delivered").val()}`
            },
            [{
                Name: "id",
                Type: "number"
            },
            {
                Render: function (value) {
                    return `<a href="${value.fileName}" target="_blank">
                    <img width="50" class="table-product-img" src="https://imgr.helpy.com.tr/img/${value.fileName}/resize?size=150">
                </a>`
                }
            },
            {
                Name: "sellerCode",
                Type: "string"
            },
            {
                Name: "supplierName",
                Type: "string"
            },
            {
                Name: "createdDateString",
                Type: "string"
            },
            {
                Name: "deadlineString",
                Type: "string"
            },
            {
                Name: "deadlineRemainDays",
                Type: "number"
            },
            {
                Name: "Delivered",
                Render: function (value) {
                    return `<i style="font-size: 20px;" class="fa ${value.delivered ? 'fa-check-circle' : 'fa-times-circle'} kt-font-${value.delivered ? 'success' : 'danger'}"></i>`;
                }
            },
            {
                Name: "totalCount",
                Type: "number"
            }],
            "Receipt",
            [{
                Render: function (value) {
                    return `<a class="dropdown-item" href="javascript:ReceiptIndex.UpdateDelivered(${value["id"]}, ${value.delivered ? false : true})"><i class="fa  ${value.delivered ? 'fa-times-circle' : 'fa-check-circle'}"></i>
                            Teslim Durumunu Değiştir
                        </a>`;
                }
            },
            {
                Render: function (value) {

                    if (value.delivered) {
                        return '';
                    }

                    return `<a class="dropdown-item" href="javascript:ReceiptIndex.UpdateCancelled(${value["id"]})"><i class="la la-exclamation"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    OpenUpdateModal: function (id) {
        ReceiptIndex.SetModalSize(ModalSize.Larger);

        HttpClient.Get(`/Receipt/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#receipt_modal").modal();
            $("#modal_title").html(`${id} Nolu Kesim Föyü Detayı`);
        });
    },
    SetModalSize: function (modalSize) {
        var $modalDialog = $(".modal-dialog").eq(0);

        if ($modalDialog.hasClass(`modal-${ModalSize.Small}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Small}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Medium}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Medium}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Large}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Large}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Larger}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Larger}`);

        $modalDialog.addClass(`modal-${modalSize}`);
    },
    DownloadPdf: function () {

        var receipt = {
            unitMeter: $('#update-unit-meter').val(),
            note: $('#update-note').val(),
            card: $('#update-card').val(),
            ticket: $('#update-ticket').val(),
            wash: $('#update-wash').val(),
            hanger: $('#update-hanger').val(),
            rubber: $('#update-rubber').val(),
            receiptItems: []
        };

        var $trs = $('[data-update-product-information-id]');
        $trs.each((index, tr) => {
            var $tr = $(tr);
            var quantity = $tr.data('update-quantity');
            receipt.receiptItems.push({
                quantity: quantity,
                productInformationId: $tr.data('update-product-information-id')
            });
        });

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: 'Lütfen bekleyiniz...'
        });

        $.ajax({
            url: '/Receipt',
            data: receipt,
            method: 'POST',
            xhrFields: {
                responseType: 'blob'
            },
            success: function (response) {
                var blob = new Blob([response], { type: 'application/pdf' });
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = $('#update-file-name').val();
                a.click();

                KTApp.unblockPage();
            },
            error: function () {
                KTApp.unblockPage();
            }
        });
    },
    UpdateDelivered: function (id, delivered) {

        HttpClient.Post('/Receipt/UpdateDelivered', { Id: id, Delivered: delivered }, (response) => {
            if (response.success) {
                Notify.Show(NotifyType.Success, response.message);
                ReceiptIndex.Read();
            }
            else {
                Notify.Show(NotifyType.Error, "Hata oluştu.");
            }
        });
    },
    UpdateCancelled: function (id) {

        HttpClient.Post('/Receipt/UpdateCancelled', { Id: id }, (response) => {
            if (response.success) {
                Notify.Show(NotifyType.Success, response.message);
                ReceiptIndex.Read();
            }
            else {
                Notify.Show(NotifyType.Error, "Hata oluştu.");
            }
        });
    }
}

$(document).ready(() => {
    ReceiptIndex.Initialize();
});