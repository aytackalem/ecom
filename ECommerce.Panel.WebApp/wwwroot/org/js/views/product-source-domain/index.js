﻿"use strict";

function ProductSourceDomainDataTable() {
};

var _ProductSourceDomainDataTable;

const ProductSourceDomainIndex = (function () {

    return {
        Initialize: function () {

            ProductSourceDomainDataTable.prototype = new DataTable({
                BaseUrl: '/ProductSourceDomain',
                Name: 'Pazaryeri',
                Pagination: {
                    Click: 'javascript:_ProductSourceDomainDataTable.SetPage(#index);_ProductSourceDomainDataTable.GetData()'
                },
                DataTableRow: new DataTableRow({
                    DataTableColumns: [
                        new DataTableColumn({
                            Name: 'id',
                            Type: DataTableColumnTypes.Int
                        }),
                        new DataTableColumn({
                            Name: 'name',
                            Type: DataTableColumnTypes.String
                        }),
                        new DataTableColumn({
                            Name: 'active',
                            Type: DataTableColumnTypes.Bool
                        })],
                    DataTableRowMenu: new DataTableRowMenu({
                        Items: [{
                            Icon: 'la la-edit',
                            Name: 'Güncelle',
                            Click: 'javascript:_ProductSourceDomainDataTable.OpenUpdateModal(#id)'
                        }]
                    })
                })
            });
            _ProductSourceDomainDataTable = new ProductSourceDomainDataTable();
            _ProductSourceDomainDataTable.GetData();
            _ProductSourceDomainDataTable.OpenUpdateModal = (id) => {
                var modal = new Modal();
                modal.Open({
                    Title: 'Xml Sağlayıcı Güncelle',
                    ButtonText: 'Güncelle',
                    GetUrl: `/ProductSourceDomain/Update?Id=${id}`,
                    PostUrl: '/ProductSourceDomain/Update',
                    Validation: true,
                    Closed: function () {
                        _ProductSourceDomainDataTable.GetData();
                    }
                });
            }

        }
    }

})();

$(document).ready(() => {
    ProductSourceDomainIndex.Initialize();
});