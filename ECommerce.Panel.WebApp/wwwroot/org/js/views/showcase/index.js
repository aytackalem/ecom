﻿
const ShowcaseIndex = {
    Initialize: function () {
        ShowcaseIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Showcase/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" }, { Name: "name", Type: "string" }],
            "Showcase",
            null,
            true
        );
    },
    OpenUpdateModal: function (id) {

        HttpClient.Get("/Showcase/Update?Id=" + id, function (response) {

            $("#modal_body").html(response);
            $("#showcase_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Vitrin");

            $("#showcase_update_form").submit(function (e) {
                e.preventDefault();

                Showcase.Validate(function () {
                    HttpClient.Post(
                        "/Showcase/Update",
                        Showcase.ToJson(),
                        function (response) {
                            if (response.success) {
                                ShowcaseIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#showcase_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                });
            });

            /**
             * Initialize showcase.
             */
            Showcase.Initialize();

            $("#modal_submit").click(function () {
                $("#showcase_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Showcase/Create", function (response) {

            $("#modal_body").html(response);
            $("#showcase_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Vitrin");

            $("#showcase_create_form").submit(function (e) {
                e.preventDefault();

                Showcase.Validate(function () {
                    HttpClient.Post(
                        "/Showcase/Create",
                        Showcase.ToJson(),
                        function (response) {
                            if (response.success) {
                                ShowcaseIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#showcase_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                });
            });

            /**
             * Initialize showcase.
             */
            Showcase.Initialize();

            $("#modal_submit").click(function () {
                $("#showcase_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ShowcaseIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ShowcaseIndex.Read(0);
    },
};

$(document).ready(() => {
    ShowcaseIndex.Initialize();
});