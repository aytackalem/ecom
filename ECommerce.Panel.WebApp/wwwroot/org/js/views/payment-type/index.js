﻿"use strict";

const PaymentTypeIndex = {
    Initialize: function () {
        PaymentTypeIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/PaymentType/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "string"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.paymentTypeTranslations[0].name;
                    }
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "PaymentType",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/PaymentType/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#payment_type_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Ödeme Tipi Güncelle");

            PaymentTypeIndex.ModalValidation("payment_type_update_form", function () {
                const paymentType = Serializer.Serialize("payment_type_update_form");

                HttpClient.Post(
                    "/PaymentType/Update",
                    paymentType,
                    function (response) {
                        if (response.success) {
                            PaymentTypeIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#payment_type_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#payment_type_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/PaymentType/Create", function (response) {
            $("#modal_body").html(response);
            $("#payment_type_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Ödeme Tipi");

            PaymentTypeIndex.ModalValidation("payment_type_create_form", function () {
                const paymentType = Serializer.Serialize("payment_type_create_form");

                HttpClient.Post(
                    "/PaymentType/Create",
                    paymentType,
                    function (response) {
                        if (response.success) {
                            PaymentTypeIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#payment_type_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#payment_type_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        PaymentTypeIndex.Read(0);
    }
};

$(document).ready(() => {
    PaymentTypeIndex.Initialize(0);
});