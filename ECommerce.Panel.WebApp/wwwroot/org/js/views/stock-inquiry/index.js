﻿const StockInquiryIndex = {
    Initialize: function () {

    },
    Get: function () {

        var modelCode = $('#ajax_data_search_model_code').val();

        if (modelCode === '') {
            $('#html-container').html('');
            return;
        }

        HttpClient.Get(`/StockInquiry/Get?ModelCode=${modelCode}`, function (response) {

            $('#html-container').html(response);

        });

    },
    CreateWholesaleOrder: function () {

        var hasError = false;

        var wholesaleOrderDetails = [];
        $('[data-product-information-id]').each((i, v) => {
            if ($(v).val() != '') {

                if (parseInt($(v).val()) > parseInt($(v).data('stock'))) {
                    hasError = true;
                    return;
                }

                wholesaleOrderDetails.push({
                    productInformationId: $(v).data('product-information-id'),
                    quantity: $(v).val()
                });
            }
        });

        if (hasError) {
            Notify.Show(NotifyType.Error, "Stokları kontrol ediniz.");
            return;
        }

        if (wholesaleOrderDetails.length == 0)
            return;

        HttpClient.Post(
            "/WholesaleOrder/Create",
            {
                IsExport: $('#is-export').prop('checked'),
                WholesaleOrderDetails: wholesaleOrderDetails
            },
            function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, "Sipariş oluşturuldu.");
                    $('#html-container').html('');
                    $('#ajax_data_search_model_code').val('');
                }
                else {
                    Notify.Show(NotifyType.Error, "Sipariş oluşturulamadı.");
                }
            }
        );

    }
};

$(document).ready(() => {
    StockInquiryIndex.Initialize();
});