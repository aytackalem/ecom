﻿const LabelIndex = {
    Initialize: function () {
        LabelIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Label/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: ""
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "value",
                    Type: "string",
                    Render: function (value) {
                        
                        return value.labelTranslations[0].value;
                    }
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "Label",
            [{
                Render: function (value) {

                    return `<a class="dropdown-item" href="javascript:LabelIndex.Delete({ id: ${value["id"]}},'${value.labelTranslations[0].value}')"><i class="la la-edit"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Label/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#label_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Ürün Etiketi Güncelle");




            LabelIndex.ModalValidation("label_update_form", function () {
                HttpClient.Post(
                    "/Label/Update",
                    Serializer.Serialize("label_update_form"),
                    function (response) {
                        if (response.success) {
                            LabelIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#label_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#label_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Label/Create", function (response) {
            $("#modal_body").html(response);
            $("#label_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Ürün Etiketi");




            LabelIndex.ModalValidation("label_create_form", function () {
                HttpClient.Post(
                    "/Label/Create",
                    Serializer.Serialize("label_create_form"),
                    function (response) {
                        if (response.success) {
                            LabelIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#label_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#label_create_form").submit();
            });
        });
    },
    Delete: function (label, value) {


        SwalAlertModal.ConfirmPopup('Ürün Etiket Sil', `${value} silinsin mi?`, 'Evet', true, 'Hayır',
            function () {

                HttpClient.Post(
                    "/Label/Delete",
                    label,
                    function (response) {

                        if (response.success) {
                            LabelIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                )
            }
        )



    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        LabelIndex.Read(0);
    }
};

$(document).ready(() => {
    LabelIndex.Initialize(0);
});