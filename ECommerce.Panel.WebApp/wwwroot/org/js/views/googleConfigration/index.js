﻿const GoogleConfigrationIndex = {
    Initialize: function () {
        GoogleConfigrationIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/GoogleConfigration/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                { Name: "id", Type: "number" },
                { Name: "name", Type: "string" },
                { Name: "active", Type: "bool" }],
            "GoogleConfigration",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var googleConfigration = element.closest('.input-googleConfigration');
                if (googleConfigration.length) {
                    googleConfigration.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/GoogleConfigration/Update?Id=${id}`, function (response) {
            debugger
            $("#modal_body").html(response);
            $("#googleConfigration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Google Konfigrasyon Güncelle");
            debugger
            GoogleConfigrationIndex.ModalValidation("googleConfigration_update_form", function () {
                const googleConfigration = Serializer.Serialize("googleConfigration_update_form");
                debugger
                HttpClient.Post(
                    "/GoogleConfigration/Update",
                    googleConfigration,
                    function (response) {
                        if (response.success) {
                            GoogleConfigrationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#googleConfigration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#googleConfigration_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/GoogleConfigration/Create", function (response) {
            $("#modal_body").html(response);
            $("#googleConfigration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Google Konfigrasyon");

            GoogleConfigrationIndex.ModalValidation("googleConfigration_create_form", function () {
                const googleConfigration = Serializer.Serialize("googleConfigration_create_form");

                HttpClient.Post(
                    "/GoogleConfigration/Create",
                    googleConfigration,
                    function (response) {
                        if (response.success) {
                            GoogleConfigrationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#googleConfigration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#googleConfigration_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        GoogleConfigrationIndex.Read(0);
    },
};

$(document).ready(() => {
    GoogleConfigrationIndex.Initialize(0);
});