﻿"use strict";

const MoneyPointSettingIndex = {
    Initialize: function () {
        MoneyPointSettingIndex.Validation("money_point_setting_update_form", function () {
            HttpClient.Post(
                "/MoneyPointSetting/Update",
                Serializer.Serialize("money_point_setting_update_form"),
                function (response) {
                    if (response.success) {
                        Notify.Show(NotifyType.Success, response.message);
                    }
                    else {
                        Notify.Show(NotifyType.Error, response.message);
                    }
                }
            );
        });

        $("#modal_submit").click(function () {
            $("#money_point_setting_update_form").submit();
        });
    },
    Validation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    }
};

$(document).ready(() => {
    MoneyPointSettingIndex.Initialize();
});