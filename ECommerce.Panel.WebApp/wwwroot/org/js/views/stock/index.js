﻿const StockIndex = {
    Initialize: function () {
        $('.kt-select2').select2({
            placeholder: "Seçiniz"
        });
        StockIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);
        
        Table.Initialize(
            "/Stock/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_OnlyStockAvailable:${$("#ajax_data_search_only_stock_available").prop('checked')}`
            },
            [
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        var fileName = '';
                        if (value.productInformationPhotos.length > 0) {
                            fileName = value.productInformationPhotos[0].fileName;
                        }

                        return `<img class="table-product-img" height="100" src="${fileName}">` ;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        debugger
                        var html = `
<table class="table" style="margin: 0;">
    <tbody>
    <tr>

            <td width="30%">
                <div>${value.productInformationName}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.stockCode}');Notify.Show(NotifyType.Info, '<b>${value.stockCode}</b> kopyalandı.');">${value.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.barcode}');Notify.Show(NotifyType.Info, '<b>${value.barcode}</b> kopyalandı.');">${value.barcode} <i class="la la-copy"></i> </a>
                </div>
            </td>
</tr>
    </tbody>
</table>
`;
                        return html;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        
                        return `<input type="number" class="form-control" onfocus="StockIndex.OpenUpdateModal(${value.id})" value="${value.stock}" placeholder="Stok giriniz">`;
                    }
                },
                {
                    Name: "virtualStock",
                    Type: "string"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        return `${value.stock + value.virtualStock}`;
                    }
                },
            ],
            "Stock",
            [],
            false
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Stock/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#stock_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Stok Güncelle");

            StockIndex.ModalValidation("stock_update_form", function () {



                HttpClient.Post(
                    "/Stock/Update",
                    Serializer.Serialize("stock_update_form"),
                    function (response) {
                        if (response.success) {
                            StockIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#stock_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#stock_update_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        StockIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        StockIndex.Read(0);
    },
  

 


 

};

$(document).ready(() => {
    StockIndex.Initialize(0);
});