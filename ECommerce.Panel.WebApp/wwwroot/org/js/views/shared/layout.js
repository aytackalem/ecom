﻿const Layout = {
    Get: function (marketplaceRequestTypeId) {

        $('#notifications-search').val('');
        HttpClient.Get(`/Notification/IndexPartial?marketplaceRequestTypeId=${marketplaceRequestTypeId}`, function (response) {

            $(`#notifications-${marketplaceRequestTypeId}`).html(response);

            var vn = $('#notifications-count-vn');
            var us = $('#notifications-count-us');
            var up = $('#notifications-count-up');
            var cpr = $('#notifications-count-cpr');
            var upr = $('#notifications-count-upr');

            vn.text(vn.data('count'));
            us.text(us.data('count'));
            up.text(up.data('count'));
            cpr.text(cpr.data('count'));
            upr.text(upr.data('count'));



            $("#notifications-search").keyup(function (e) {

                vn.text(vn.data('count'));
                us.text(us.data('count'));
                up.text(up.data('count'));
                cpr.text(cpr.data('count'));
                upr.text(upr.data('count'));


                var keyword = $(this).val();

                var systemNotificationStockCount = parseInt($(`#notifications-count-${marketplaceRequestTypeId}`).data("count"));
                $.each($(`#notifications-${marketplaceRequestTypeId} .kt-notification__item`), (i, v) => {
                    if ($(v).text().indexOf(keyword) != -1) {

                        $(v).show();
                    }
                    else {
                        systemNotificationStockCount -= 1;
                        $(v).hide();
                    }
                });
                $(`#notifications-count-${marketplaceRequestTypeId}`).text(systemNotificationStockCount);

            });

        });
    },
    Delete: function (marketplaceRequestTypeId, id) {
        HttpClient.Post(`/Notification/Delete?MarketplaceRequestTypeId=${marketplaceRequestTypeId}&Id=${id}`, null, function (response) {
            var notificationsCount = $("#notifications-count").html();
            var typeNotificationsCount = $(`#notifications-count-${marketplaceRequestTypeId.toLowerCase()}`).html();

            if (response.success) {
                $(`notf-${id}`).remove();

                notificationsCount--;
                typeNotificationsCount--;

                $("#notifications-count").html(notificationsCount);
                $(`#notifications-count-${marketplaceRequestTypeId.toLowerCase()}`).html(typeNotificationsCount);

                if (notificationsCount == 0) {
                    $("#notifications-wrapper").addClass('d-none');
                }
                else if (typeNotificationsCount == 0) {
                    $(`#notifications-${marketplaceRequestTypeId.toLowerCase()}`).html(`<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
        <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
            <div class="kt-grid__item kt-grid__item--middle kt-align-center">
                Herşey yolunda!
                <br>Gösterilecek bir uyarı bulunamadı.
            </div>
        </div>
    </div>`);
                }
            }
        });
    },
    Ignore: function (validation, validationNotificationId, marketplaceRequestDetailId, productInformationMarketplaceId, marketplaceRequestTypeId) {
        HttpClient.Post(`/Notification/Ignore?ProductInformationMarketplaceId=${productInformationMarketplaceId}`, null, function (response) {
            var notificationsCount = $("#notifications-count").html();
            var typeNotificationsCount = $(`#notifications-count-${marketplaceRequestTypeId.toLowerCase()}`).html();

            if (response.success) {
                if (validation)
                    $(`#validation-${validationNotificationId}`).remove();
                else
                    $(`#notification-${marketplaceRequestDetailId}`).remove();

                notificationsCount--;
                typeNotificationsCount--;

                $("#notifications-count").html(notificationsCount);
                $(`#notifications-count-${marketplaceRequestTypeId.toLowerCase()}`).html(typeNotificationsCount);

                if (notificationsCount == 0) {
                    $("#notifications-wrapper").addClass('d-none');
                }
                else if (typeNotificationsCount == 0) {
                    $(`#notifications-${marketplaceRequestTypeId.toLowerCase()}`).html(`<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
        <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
            <div class="kt-grid__item kt-grid__item--middle kt-align-center">
                Herşey yolunda!
                <br>Gösterilecek bir uyarı bulunamadı.
            </div>
        </div>
    </div>`);
                }
            }
        });
    },
    OpenPriceUpdateModal: function (productId) {
        HttpClient.Get(`/ProductInformation/PriceUpdate?ProductId=${productId}`, function (response) {
            $("#layout_modal_body").html(response);
            $("#layout_modal").modal();
            $("#layout_modal_submit").unbind("click");
            $("#layout_modal_title").html("Fiyat Güncelle");

            $('.apply-all').click(function () {
                $('.list-unit-price').val($('.template-list-unit-price').val());
                $('.unit-price').val($('.template-unit-price').val());
            });

            $("#layout_modal_submit").click(function () {
                var valid = Validation.Valid("price_update_form");
                if (valid) {

                    var data = Serializer.Serialize("price_update_form");

                    HttpClient.Post(
                        "/ProductInformation/PriceUpdate",
                        data,
                        function (response) {
                            if (response.success) {
                                Notify.Show(NotifyType.Success, response.message);

                                $("#layout_modal").modal("hide");
                                $("#layout_modal_body").html('');
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm varyasyonları doldurunuz.");
            });
        });
    },
    OpenStockUpdateModal: function (productInformationId) {
        HttpClient.Get(`/Stock/Update?Id=${productInformationId}`, function (response) {
            $("#layout_modal_body").html(response);
            $("#layout_modal").modal();
            $("#layout_modal_submit").unbind("click");
            $("#layout_modal_title").html("Stok Güncelle");

            $("#layout_modal_submit").click(function () {
                var valid = Validation.Valid("stock_update_form");
                if (valid) {

                    var data = Serializer.Serialize("stock_update_form");

                    HttpClient.Post(
                        "/Stock/Update",
                        data,
                        function (response) {
                            if (response.success) {
                                Notify.Show(NotifyType.Success, response.message);

                                $("#layout_modal").modal("hide");
                                $("#layout_modal_body").html('');
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm varyasyonları doldurunuz.");
            });
        });
    }
};

$(document).ready(() => {
    /*Layout.Get();*/
});