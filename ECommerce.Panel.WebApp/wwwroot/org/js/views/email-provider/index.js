﻿
const EmailProviderIndex = {
    Initialize: function () {
        EmailProviderIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/EmailProvider/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: ""
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "name",
                    Type: "string"
                }
            ],
            "EmailProvider",
            [],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/EmailProvider/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#email_provider_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Email Sağlayıcısı Güncelle");

            EmailProviderIndex.ModalValidation("email_provider_update_form", function () {
                HttpClient.Post(
                    "/EmailProvider/Update",
                    Serializer.Serialize("email_provider_update_form"),
                    function (response) {
                        if (response.success) {
                            EmailProviderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#email_provider_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#email_provider_update_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        EmailProviderIndex.Read(0);
    },
};

$(document).ready(() => {
    EmailProviderIndex.Initialize(0);
});