﻿var financialWidgetInterval = null;
var financialWidgetChart = null;

const HomeIndex = {
    Initialize: function () {

    },
    Subscribe: function () {

        HomeIndex.Fetch();

    },
    Fetch: function () {

        if (financialWidgetInterval != null) {
            clearInterval(financialWidgetInterval);
        }

        HttpClient.Get('/Home/FinancialWidget', function (financialWidget) {

            document.getElementById('refresh-date-time').innerHTML = financialWidget.refreshDateTime;
            document.getElementById('current-sales-amount').innerHTML = financialWidget.currentSalesAmountString;
            document.getElementById('yesterday-current-sales-amount').innerHTML = financialWidget.yesterdayCurrentSalesAmountString;
            document.getElementById('order-count').innerHTML = financialWidget.orderCount;
            document.getElementById('product-count').innerHTML = financialWidget.productCount;
            document.getElementById('yesterday-order-count').innerHTML = financialWidget.yesterdayOrderCount;
            document.getElementById('yesterday-product-count').innerHTML = financialWidget.yesterdayProductCount;

            document.getElementById('sales-amount-rate').innerHTML = financialWidget.salesAmountRate;
            document.getElementById('sales-amount-rate').style.display = financialWidget.salesAmount == 0 ? "none" : "show";
            document.getElementById('sales-amount-rate').classList.remove("kt-font-danger");
            document.getElementById('sales-amount-rate').classList.remove("kt-font-success");
            document.getElementById('sales-amount-rate').classList.add(financialWidget.salesAmount < 0 ? "kt-font-danger" : "kt-font-success");

            document.getElementById('product-count-rate').innerHTML = financialWidget.productCountRateString;
            document.getElementById('product-count-rate').style.display = financialWidget.productCountRate == 0 ? "none" : "show";
            document.getElementById('product-count-rate').classList.remove("kt-font-danger");
            document.getElementById('product-count-rate').classList.remove("kt-font-success");
            document.getElementById('product-count-rate').classList.add(financialWidget.productCountRate < 0 ? "kt-font-danger" : "kt-font-success");

            document.getElementById('order-count-rate').innerHTML = financialWidget.orderCountRateString;
            document.getElementById('order-count-rate').style.display = financialWidget.orderCountRate == 0 ? "none" : "show";
            document.getElementById('order-count-rate').classList.remove("kt-font-danger");
            document.getElementById('order-count-rate').classList.remove("kt-font-success");
            document.getElementById('order-count-rate').classList.add(financialWidget.orderCountRate < 0 ? "kt-font-danger" : "kt-font-success");

            const ctx = document.getElementById('financial-widget-chart');

            const labels = financialWidget.chartData.labels;
            const data = {
                labels: labels,
                datasets: [
                    {
                        label: 'Önceki Gün',
                        data: financialWidget.chartData.series[0],
                        borderColor: '#1dc9b7',
                        //borderWidth: 0.5,
                        yAxisID: 'y',
                    }, {
                        label: 'Dün',
                        data: financialWidget.chartData.series[1],
                        borderColor: '#22b9ff',
                        //borderWidth: 1,
                        //yAxisID: 'y1',
                    },
                    {
                        label: 'Bugün',
                        data: financialWidget.chartData.series[2],
                        borderColor: '#ffb822',
                        //borderWidth: 3,
                        //yAxisID: 'y2',
                    }
                ]
            };

            const config = {
                type: 'line',
                data: data,
                options: {
                    responsive: true,
                    interaction: {
                        mode: 'index',
                        intersect: false,
                    },
                    plugins: {
                        legend: {
                            display: true,
                            align: 'end',
                            labels: {
                                boxWidth: 15,
                                boxHeight: 15
                            }
                        }
                    }
                },
            };

            if (financialWidgetChart != null) {
                financialWidgetChart.destroy();
            }

            financialWidgetChart = new Chart(ctx, config);

            financialWidgetInterval = setTimeout(HomeIndex.Fetch, 30000);

        });

    }
};

$(document).ready(() => {
    HomeIndex.Initialize();
});