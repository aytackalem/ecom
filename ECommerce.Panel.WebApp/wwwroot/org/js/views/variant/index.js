﻿const VariantIndex = {
    Initialize: function () {
        VariantIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Variant/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "string"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.variantTranslations[0].name;
                    }
                }
            ],
            "Variant",
            [{
                Render: function (value) {
                    return `
<a class="dropdown-item" href="javascript:VariantIndex.OpenMarketPlaceSettingsModal('${value["id"]}')"><i class="la la-edit"></i>
                            Pazaryeri Ayarları
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Variant/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#variant_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Varyasyon Güncelle");

            $("#modal_submit").click(function () {
                var valid = Validation.Valid("variant_update_form");
                if (valid) {
                    var data = Serializer.Serialize("variant_update_form");

                    HttpClient.Post(
                        "/Variant/Update",
                        data,
                        function (response) {
                            if (response.success) {
                                VariantIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#variant_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm alanları doldurunuz.");
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Variant/Create", function (response) {
            $("#modal_body").html(response);
            $("#variant_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Varyasyon");

            $("#modal_submit").click(function () {
                var valid = Validation.Valid("variant_create_form");
                if (valid) {
                    var data = Serializer.Serialize("variant_create_form");

                    HttpClient.Post(
                        "/Variant/Create",
                        data,
                        function (response) {
                            if (response.success) {
                                VariantIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#variant_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm alanları doldurunuz.");
            });
        });
    },
    OpenVariantValueModal: function (variantId) {
        HttpClient.Get(`/VariantValue/Read?VariantId=${variantId}`, function (response) {
            $("#variant_value_modal_body").html(response);
            $("#variant_value_modal").modal();
        });
    },
    OpenMarketPlaceSettingsModal: function (id) {
        HttpClient.Get(`/MarketplaceVariantMapping/IndexPartial?VariantId=${id}`, function (response) {

            $("#modal_title").html("Pazaryeri Varyasyon Ayarları");
            $("#modal_body").html(response);
            $("#modal_submit").unbind("click");

            $("#variant_modal").modal();

            $('.kt-selectpicker').selectpicker();

            $(`.marketplace-variant-container[data-marketplace-id]`).each((i, theDiv) => {
                var $theDiv = $(theDiv);
                var marketplaceId = $theDiv.data('marketplace-id');
                var isECommerce = $theDiv.data('is-ecommerce');

                $theDiv.find('.form-group').each((i, theFormGroup) => {

                    var $theFormGroup = $(theFormGroup);
                    if ($theFormGroup.data("allow-custom") == false) {

                        var $input = $theFormGroup.find("[type='text']").eq(0);

                        Autocomplete.InitRemote(
                            $input,
                            isECommerce ? "/ECommerceVariantValue/SearchByName" : "https://marketplacecatalog.helpy.com.tr/VariantValue/SearchByName",
                            [{
                                key: "MarketplaceId",
                                value: marketplaceId
                            }, {
                                key: "VariantCode",
                                value: $theFormGroup.data("marketplace-variant-code")
                            }],
                            (i) => { return { VariantValueCode: i.C, VariantValueName: i.N } },
                            "VariantValueName",
                            null,
                            (suggestion) => {
                                $theFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                                $theFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                            },
                            () => {
                                $theFormGroup.data("marketplace-variant-value-code", "");
                                $theFormGroup.data("marketplace-variant-value-name", "");
                            },
                            () => { });
                    }


                });
            });

            $("select.marketplace-variant").each((i, select) => {
                var $select = $(select);

                InitializeMarketPlaceSettings($select);
            });

            $("select.marketplace-variant").on("hidden.bs.select", function () {


                var $this = $(this);

                InitializeMarketPlaceSettings($this);
            });

            $("#modal_submit").click(function () {
                var marketplaceVariantValueMappings = [];
                $('.marketplace-variant-container').each((i, theContainer) => {
                    var $theContainer = $(theContainer);

                    var $formGroups = $theContainer.find('.form-group');
                    $formGroups.each((i, theFormGroup) => {
                        var $theFormGroup = $(theFormGroup);

                        var allowCustom = $theFormGroup.data('allow-custom');
                        var companyId = $theFormGroup.data('company-id');
                        var marketplaceVariantValueMapping = {
                            marketplaceId: $theContainer.data('marketplace-id'),
                            VariantValueId: $theContainer.data('variant-value-id'),
                            allowCustom: allowCustom,
                            mandatory: $theFormGroup.data('mandatory'),
                            companyId: companyId == "" ? null : companyId,
                            marketplaceVariantCode: $theFormGroup.data('marketplace-variant-code').toString(),
                            marketplaceVariantName: $theFormGroup.data('marketplace-variant-name')
                        };

                        if (allowCustom) {
                            marketplaceVariantValueMapping.marketplaceVariantValueName = $theFormGroup.find('[type="text"]').eq(0).val();
                            marketplaceVariantValueMapping.marketplaceVariantValueCode = $theFormGroup.find('[type="text"]').eq(0).val();
                        }
                        else {
                            marketplaceVariantValueMapping.marketplaceVariantValueName = $theFormGroup.data('marketplace-variant-value-name').toString();
                            marketplaceVariantValueMapping.marketplaceVariantValueCode = $theFormGroup.data('marketplace-variant-value-code').toString();
                        }

                        if (marketplaceVariantValueMapping.marketplaceVariantValueCode != '' &&
                            marketplaceVariantValueMapping.marketplaceVariantValueCode != undefined) {

                            marketplaceVariantValueMappings.push(marketplaceVariantValueMapping);

                        }

                    });
                });

                HttpClient.Post(
                    "/MarketplaceVariantMapping/Upsert",
                    {
                        VariantId: $('[name="variant[id]"]').val(),
                        //MarketplaceVariantMappings: marketplaceVariantMappings,
                        MarketplaceVariantValueMappings: marketplaceVariantValueMappings
                    },
                    function (response) {
                        if (response.success) {
                            Notify.Show(NotifyType.Success, response.message);

                            $("#variant_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

                //var valid = Validation.Valid("marketplace_form");
                //if (valid) {

                //}
                //else
                //    Notify.Show(NotifyType.Error, "Lütfen tüm alanları doldurunuz.");
            });
        });
    },
    CreateVariantValue: function () {
        const variantValue = {
            variantId: $('[name="variant_id"]').val(),
            variantValueTranslations: [{
                value: $('#new_variant_value [name="value"]').val(),
                languageId: "TR"
            }]
        };

        HttpClient.Post(
            "/VariantValue/Create",
            variantValue,
            function (response) {
                if (response.success) {
                    VariantIndex.OpenVariantValueModal(variantValue.variantId);

                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    UpdateVariantValue: function (id) {
        const variantValue = {
            id: $(`[name="id_${id}"]`).val(),
            variantId: $(`[name="variant_id"]`).val(),
            variantValueTranslations: [{
                value: $(`[name="value_${id}"]`).val(),
                languageId: "TR"
            }]
        };

        HttpClient.Post(
            "/VariantValue/Update",
            variantValue,
            function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        VariantIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        VariantIndex.Read(0);
    },
    AddVariantValue: function () {
        HttpClient.Post(`/VariantValue/IndexPartial`, { variantValues: null, index: $("#variant-values").find(".variant-value-container").length }
            , function (response) {
                $("#variant-values").append(response);
            });
    },
};

function InitializeMarketPlaceSettings($this) {
    var marketplace = {
        id: $this.data("marketplace-id"),
        variants: []
    };

    $this.find(":selected").each((i, theOption) => {
        let $theOption = $(theOption);

        marketplace.variants.push({
            companyId: $theOption.data("company-id"),
            code: $theOption.val(),
            name: $theOption.text(),
            allowCustom: $theOption.data("allow-custom"),
            mandatory: $theOption.data("mandatory")
        });
    });

    $(`.marketplace-variant-container[data-marketplace-id="${marketplace.id}"]`).each((i, theDiv) => {


        var $theDiv = $(theDiv);

        var isECommerce = $theDiv.data('is-ecommerce');

        /* Remove */
        var $formGroups = $theDiv.find(".form-group");
        $formGroups.each((i, formGroup) => {
            var $formGroup = $(formGroup);
            if (marketplace.variants.filter(v => v.code == $formGroup.data("marketplace-variant-code")).length == 0)
                $formGroup.remove();
        });

        /* Add */
        for (var j = 0; j < marketplace.variants.length; j++) {

            var marketplaceVariant = marketplace.variants[j];
            if ($theDiv.find(`[data-marketplace-variant-code="${marketplaceVariant.code}"]`).length == 0) {

                var html = '';
                if (marketplaceVariant.allowCustom) {
                    html += `
<div class="form-group" data-marketplace-variant-code="${marketplaceVariant.code}" data-marketplace-variant-name="${marketplaceVariant.name}" data-allow-custom="${marketplaceVariant.allowCustom}" data-mandatory="${marketplaceVariant.mandatory}" data-company-id="${marketplaceVariant.companyId}">
    <div class="kt-input-icon kt-input-icon--left">
 <label class="input-abs-text">
           <img src="/org/img/marketplace-icons/${marketplace.id}.png" height="15" />  
        ${marketplaceVariant.name}
     </label>
        <input type="text" class="form-control" placeholder="" />
        <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span><i class="la la-i-cursor"></i></span>
        </span>
    </div>
</div>`;
                }
                else {
                    html += `
<div class="form-group" data-marketplace-variant-code="${marketplaceVariant.code}" data-marketplace-variant-name="${marketplaceVariant.name}" data-allow-custom="${marketplaceVariant.allowCustom}" data-mandatory="${marketplaceVariant.mandatory}" data-company-id="${marketplaceVariant.companyId}" data-marketplace-variant-value-code="" data-marketplace-variant-value-name="">
    <div class="typeahead">
        <div class="kt-input-icon kt-input-icon--left">
             <label class="input-abs-text">
           <img src="/org/img/marketplace-icons/${marketplace.id}.png" height="15" />  
        ${marketplaceVariant.name}
     </label>
            <input type="text" class="form-control" placeholder="" />
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
    </div>
</div>`;
                }
                $theDiv.append(html);

                /* Autocomplete */
                $theDiv.find(`[data-marketplace-variant-code="${marketplaceVariant.code}"][data-allow-custom="false"]`).each((i, theFormGroup) => {
                    var $theFormGroup = $(theFormGroup);
                    var $input = $theFormGroup.find("[type='text']").eq(0);

                    Autocomplete.InitRemote(
                        $input,
                        isECommerce ? "/ECommerceVariantValue/SearchByName" : "https://marketplacecatalog.helpy.com.tr/VariantValue/SearchByName",
                        [{
                            key: "MarketplaceId",
                            value: marketplace.id
                        }, {
                            key: "VariantCode",
                            value: $theFormGroup.data("marketplace-variant-code")
                        }],
                        (i) => { return { VariantValueCode: i.C, VariantValueName: i.N } },
                        "VariantValueName",
                        null,
                        (suggestion) => {
                            $theFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                            $theFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                        },
                        () => {
                            $theFormGroup.data("marketplace-variant-value-code", "");
                            $theFormGroup.data("marketplace-variant-value-name", "");
                        },
                        () => { });
                });
            }
        }
    });
}

$(document).ready(() => {
    VariantIndex.Initialize(0);
});