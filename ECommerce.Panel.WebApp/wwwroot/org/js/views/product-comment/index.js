﻿"use strict";

const ProductCommentIndex = {
    Initialize: function () {
        ProductCommentIndex.Read();

        $('.kt-selectpicker').selectpicker();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);
        Table.Initialize(
            "/ProductComment/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: "",
                Active: $("#ajax_data_active").val() === "" ? null : $("#ajax_data_active").val() === "1" ? true : false
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "name",
                    Type: "string",
                    Render: function (value) {
                        debugger
                        return value.productInformation.productInformationTranslations[0].name;
                    }
                },
                {
                    Name: "comment",
                    Type: "string"
                },
                {
                    Name: "approved",
                    Type: "bool"
                }
            ],
            "ProductComment",
            [{
                Render: function (value) {

                    return `${value["active"] ? `<a class="dropdown-item" href="javascript:ProductCommentIndex.Update({ id: ${value["id"]}, active: ${value["active"]}, approved: !${value["approved"]}})"><i class="la la-edit"></i>
                            ${value["approved"] ? "Kaldır" : "Yayınla"}
                        </a>` : ``}
${value["approved"] ? `` : `<a class="dropdown-item" href="javascript:ProductCommentIndex.Update({ id: ${value["id"]}, active: !${value["active"]}, approved: ${value["approved"]}})"><i class="la la-edit"></i>
                            ${value["active"] ? "Pasif Et" : "Aktif Et"}
                        </a>`}`;
                }
            }],
            false
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/ProductComment/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#product_comment_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Tedarikçi Güncelle");

            ProductCommentIndex.ModalValidation("product_comment_update_form", function () {
                HttpClient.Post(
                    "/ProductComment/Update",
                    Serializer.Serialize("product_comment_update_form"),
                    function (response) {
                        if (response.success) {
                            ProductCommentIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_comment_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#product_comment_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/ProductComment/Create", function (response) {
            $("#modal_body").html(response);
            $("#product_comment_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Tedarikçi");

            ProductCommentIndex.ModalValidation("product_comment_create_form", function () {
                HttpClient.Post(
                    "/ProductComment/Create",
                    Serializer.Serialize("product_comment_create_form"),
                    function (response) {
                        if (response.success) {
                            ProductCommentIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_comment_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#product_comment_create_form").submit();
            });
        });
    },
    Update: function (productComment) {
        HttpClient.Post(
            "/ProductComment/Update",
            productComment,
            function (response) {
                if (response.success) {
                    ProductCommentIndex.Read(0);

                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ProductCommentIndex.Read(0);
    },
};

$(document).ready(() => {
    ProductCommentIndex.Initialize(0);
});