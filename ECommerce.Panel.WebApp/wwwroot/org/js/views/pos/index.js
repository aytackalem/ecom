﻿"use strict";

const PosIndex = {
    Initialize: function () {
        PosIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Pos/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "name",
                    Type: "string"
                },
                {
                    Name: "default",
                    Type: "bool"
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "Pos",
            [{
                Render: function (value) {
                    return `<a class="dropdown-item" href="javascript:PosIndex.OpenPosConfigurationModal('${value["id"]}')"><i class="la la-edit"></i>
                            Pos Konfigurasyonları
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Pos/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#pos_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Pos Güncelle");

            PosIndex.ModalValidation("pos_update_form", function () {

                const pos = Serializer.Serialize("pos_update_form");


                HttpClient.Post(
                    "/Pos/Update",
                    pos,
                    function (response) {
                        if (response.success) {
                            PosIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#pos_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#pos_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Pos/Create", function (response) {
            $("#modal_body").html(response);
            $("#pos_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Pos");

            PosIndex.ModalValidation("pos_create_form", function () {
               debugger
                const pos = Serializer.Serialize("pos_create_form");

                HttpClient.Post(
                    "/Pos/Create",
                    pos,
                    function (response) {
                        if (response.success) {
                            PosIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#pos_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#pos_create_form").submit();
            });
        });
    },
    OpenPosConfigurationModal: function (posId) {
        HttpClient.Get(`/PosConfiguration/Read?PosId=${posId}`, function (response) {
            $("#pos_configuration_modal_body").html(response);
            $("#pos_configuration_modal").modal();
        });
    },
    CreatePosConfigurationValue: function () {
        const posConfiguration = {
            posId: $('[name="posId"]').val(),
            id: $('#new_pos_configuration [name="id"]').val(),
            key: $('#new_pos_configuration [name="key"]').val(),
            value: $('#new_pos_configuration [name="value"]').val()
        };

        HttpClient.Post(
            "/PosConfiguration/Create",
            posConfiguration,
            function (response) {
                if (response.success) {
                    PosIndex.OpenPosConfigurationModal(posConfiguration.posId);

                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    UpdatePosConfigurationValue: function (id) {
        const posConfiguration = {
            posId: $('[name="posId"]').val(),
            id: $(`[name="id_${id}"]`).val(),
            key: $(`[name="key_${id}"]`).val(),
            value: $(`[name="value_${id}"]`).val()
        };

        HttpClient.Post(
            "/PosConfiguration/Update",
            posConfiguration,
            function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, response.message);
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        PosIndex.Read(0);
    },
};

$(document).ready(() => {
    PosIndex.Initialize(0);
});