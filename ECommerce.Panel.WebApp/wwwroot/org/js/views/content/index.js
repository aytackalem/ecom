﻿
const ContentIndex = {
    Initialize: function () {
        ContentIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Content/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" }, {
                Name: "", Type: "string", Render: function (value) {
                    return value.contentTranslations[0].header;
                }
            }, { Name: "active", Type: "bool" }],
            "Content",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var content = element.closest('.input-content');
                if (content.length) {
                    content.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Content/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#content_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("İçerik Güncelle");
            $('.summernote').summernote({
                height: 150
            });

            ContentIndex.ModalValidation("content_update_form", function () {
                const content = Serializer.Serialize("content_update_form");

                HttpClient.Post(
                    "/Content/Update",
                    content,
                    function (response) {
                        if (response.success) {
                            ContentIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#content_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#content_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Content/Create", function (response) {
            $("#modal_body").html(response);
            $("#content_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni İçerik");
            $('.summernote').summernote({
                height: 150
            });

            ContentIndex.ModalValidation("content_create_form", function () {
                const content = Serializer.Serialize("content_create_form");

                HttpClient.Post(
                    "/Content/Create",
                    content,
                    function (response) {
                        if (response.success) {
                            ContentIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#content_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#content_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ContentIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ContentIndex.Read(0);
    },
};

$(document).ready(() => {
    ContentIndex.Initialize(0);
});