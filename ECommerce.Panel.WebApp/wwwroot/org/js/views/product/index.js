﻿const ProductIndex = {
    Tenant: '',
    StokActive: false,
    StokAsc: '',
    Initialize: function () {

        $(document).on('show.bs.modal', '.modal', function () {
            const zIndex = 1040 + 10 * $('.modal:visible').length;
            $(this).css('z-index', zIndex);
            setTimeout(() => $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack'));
        });

        $('.ajax_data_length').select2();

        ProductIndex.Read(0);

        $('.select-picker').selectpicker();

        $('.kt-select2').select2({
            placeholder: "Seçiniz",
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {

                if (data.element && data.element.hasAttribute("data-html"))
                    return `<div style="text-align: center;">${data.element.getAttribute("data-html")}</div>`;
                else
                    return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });

        $('#size_table_submit').click(function () {

            HttpClient.Post('/SizeTable/Post', Serializer.Serialize("size_table_form"), function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, 'Kayıt başarılı.');
                    $('#size_table_modal_body').empty();
                    $('#size_table_modal').modal('hide');
                }
            });

        });

        $('#categorization_submit').click(function () {

            $('.categorization').removeAttr('disabled');

            HttpClient.Post('/ProductCategorization/Post', Serializer.Serialize("product_categorization_form"), function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, 'Kayıt başarılı.');
                    $('#categorization_modal_body').empty();
                    $('#categorization_modal').modal('hide');

                    ProductIndex.Read(0);
                }
            });

        });
    },
    Read: function (page, sender) {

        $('#ajax_table').empty();

        $("#ajax_data_page").val(page);

        var url = `/Product/Read?Page=${page}&PageRecordsCount=${$(".ajax_data_length").val()}&Search=Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}_ProductCategorization:${$("#ajax_data_search_product_categorization").length > 0 ? $("#ajax_data_search_product_categorization").val() : ''}_Supplier:${$("#ajax_data_search_supplier").length > 0 ? $("#ajax_data_search_supplier").val() : ''}_SpecialCategorization:${$("#ajax_data_search_special_categorization").length > 0 ? $("#ajax_data_search_special_categorization").val() : ''}_SizeTable:${$("#ajax_data_search_size_table").length > 0 ? $("#ajax_data_search_size_table").val() : ''}`;



        if (sender != undefined) {
            var $sender = $(sender);
            if ($sender.find("i").length == 0) {
                $sender.prepend('<i style="font-size: 18px; display: inline" class="la la-sort-amount-desc"></i>');
            }
            else {

                var $i = $sender.find("i").eq(0);
                if ($i.hasClass("la la-sort-amount-desc")) {
                    $i.removeClass("la la-sort-amount-desc");
                    $i.addClass("la la-sort-amount-asc");
                }
                else {
                    $i.removeClass("la la-sort-amount-asc");
                    $i.addClass("la la-sort-amount-desc");
                }
            }

            var sorting = $sender.find("span").eq(0).html();
            var asc = $sender.find("i").eq(0).hasClass("la la-sort-amount-desc") ? "desc" : "asc";

            url = `${url}&Sort=${sorting}&Asc=${asc}`

            ProductIndex.StokActive = true;
            ProductIndex.StokAsc = asc;
        }
        else if (ProductIndex.StokActive) {

            url = `${url}&Sort=Stok&Asc=${ProductIndex.StokAsc}`
        }

        HttpClient.Get(url, function (response) {


            $('#ajax_table').html(response.html);
            ProductIndex.ImageGallery();
            ProductIndex.Pagination(response.recordsCount, response.pageRecordsCount, response.page);


            if (response.sorting != null && response.asc != null) {
                var $sortable = $(`.sortable:contains("${response.sorting}")`);
                $sortable.prepend(`<i style="font-size: 18px; display: inline" class="la la-sort-amount-${response.asc == "asc" ? 'asc' : 'desc'}"></i>`);
            }

            $('.sortable').click(function () {
                ProductIndex.Read(0, this)
            });

        }, function () {

        });

    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                select: {
                    required: true
                },
                option: {
                    required: true
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },
                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Product/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);

            ProductIndex.FileImageUpload();

            $('.summernote').summernote({
                height: 150
            });

            $("#product_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Ürün Güncelle");
            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });
            $('.kt-selectpicker').selectpicker();

            $('.touchspin').TouchSpin({
                buttondown_class: 'btn btn-secondary',
                buttonup_class: 'btn btn-secondary'
            });

            ProductIndex.ModalValidation("product_update_form", function () {
                let product = Serializer.Serialize("product_update_form");

                product.productLabels = [];
                $('#productLabels option:selected').each((index, element) => product.productLabels.push({
                    labelId: parseInt(element.value)
                }));

                product.productProperties = [];
                $("#productProperties option:selected").each((index, element) => product.productProperties.push({
                    PropertyValueId: parseInt(element.value)
                }));

                product.multiCategoryIds = [];
                $("#multiCategoryIds option:selected").each((index, element) => product.multiCategoryIds.push(element.value));

                HttpClient.Post(
                    "/Product/Update",
                    product,
                    function (response) {
                        if (response.success) {
                            ProductIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            ProductIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {
                $("#product_update_form").submit();
            });
        });
    },
    OpenMarketPlaceSettingsModal: function (id) {
        HttpClient.Get(`/ProductInformationMarketplace/IndexPartial?Id=${id}`, function (response) {

            $("#modal_title").html("Ürün Pazaryeri Ayarları");
            $("#modal_body").html(response);
            $("#modal_submit").unbind("click");
            $("#product_modal").modal();

            //handle-barcode-stock-code-changes
            (function () {
                for (const argument of arguments) {
                    $(argument).change(function () {
                        var $this = $(this);
                        if ($(".handle-barcode-stock-code-changes").prop('checked')) {

                            $.each($(".product-information-marketplace-container"), function (index, value) {
                                var $value = $(value);
                                if ($value.find(".active").prop("checked")) {
                                    $value.find(argument).val($this.val());
                                }
                            });

                        }
                    });
                }
            })(".barcode", ".stock-code");

            //handle-prices-changes
            (function () {
                for (const argument of arguments) {
                    $(argument).change(function () {
                        var $this = $(this);
                        if ($(".handle-prices-changes").prop('checked')) {

                            $.each($(".product-information-marketplace-container"), function (index, value) {
                                var $value = $(value);
                                if ($value.find(".active").prop("checked")) {
                                    $value.find(argument).val($this.val());
                                }
                            });

                        }
                    });
                }
            })(".list-unit-price", ".unit-price");

            //handle-open-close-changes
            (function () {
                for (const argument of arguments) {
                    $(argument).change(function () {

                        var $this = $(this);
                        var marketplaceId = $this.closest(".product-marketplace-container").data("marketplace-id");

                        if ($(".handle-open-close-changes").prop('checked')) {
                            $.each($(".product-information-marketplace-container[data-marketplace-id='" + marketplaceId + "'] .active"), function (index, value) {
                                if ($(value).prop("checked") != $this.prop("checked")) {
                                    $(value).prop("checked", $this.prop("checked"));
                                    $(value).change();
                                }
                            });

                        }
                    });
                }
            })(".product-marketplace-container .active");

            $('.kt-selectpicker').selectpicker();

            $('.product-marketplace-container').each((i, theDiv) => {

                var $theDiv = $(theDiv);

                var marketplaceId = $theDiv.data('marketplace-id');
                var isECommerce = $theDiv.data('is-ecommerce');

                var $brandFormGroups = $theDiv.find(".form-group[data-marketplace-brand-autocomplete='true']");
                $brandFormGroups.each((i, formGroup) => {
                    var $formGroup = $(formGroup);
                    var $input = $formGroup.find("[type='text']").eq(0);

                    $input.focus(function () {

                        if ($input.data('autocomplete-initialized') == true)
                            return;

                        $input.data('autocomplete-initialized', true);

                        Autocomplete.InitRemote(
                            $input,
                            isECommerce ? "/ECommerceBrand/SearchByName" : "https://marketplacecatalog.helpy.com.tr/Brand/SearchByName",
                            [{
                                key: "MarketplaceId",
                                value: marketplaceId
                            }],
                            (i) => { return { Code: i.C, Name: i.N } },
                            "Name",
                            null,
                            (suggestion) => {
                                $formGroup.data("marketplace-brand-code", suggestion.Code);
                                $formGroup.data("marketplace-brand-name", suggestion.Name);

                                if ($('.handle-variant-entries').prop('checked')) {

                                    var marketplaceVariantCode = $formGroup.data("marketplace-variant-code");

                                    var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                                    var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                                    for (var i = index; i < length; i++) {
                                        var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + marketplaceVariantCode + '"]').eq(0);

                                        $foundFormGroup.find('[type="text"]').val(suggestion.VariantValueName);

                                        $foundFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                                        $foundFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                                    }

                                }

                                $input.typeahead("destroy");
                                $input.data('autocomplete-initialized', false);
                            },
                            () => {
                                $formGroup.data("marketplace-brand-code", "");
                                $formGroup.data("marketplace-brand-name", "");
                            },
                            () => { });

                        $input.closest(".form-group").find(".tt-input").focus();
                    });

                });

                var $formGroups = $theDiv.find(".form-group[data-marketplace-category-code]");
                $formGroups.each((i, formGroup) => {
                    var $formGroup = $(formGroup);
                    var $input = $formGroup.find("select").eq(0);
                    $input.change(function () {

                        var categoryCode = $input.val();
                        var categoryName = $input.find(':checked').text();

                        $formGroup.data('marketplace-category-code', categoryCode);
                        $formGroup.data('marketplace-category-name', categoryName);

                        HttpClient.Post(isECommerce ? "/ECommerceVariant/GetByCategoryCodes" : "https://marketplacecatalog.helpy.com.tr/Variant/GetByCategoryCodes", {
                            MarketplaceId: marketplaceId,
                            CategoryCodes: [categoryCode]
                        }, function (response) {

                            if (response.success) {
                                response.data.I = response.data.I.sort((a, b) => b.M - a.M);

                                var html = '';
                                for (var i = 0; i < response.data.I.length; i++) {

                                    if (response.data.I[i].AC) {
                                        html += `
<div class="form-group pos-relative" data-marketplace-variant-value-code="" data-marketplace-variant-value-name="" data-marketplace-variant-code="${response.data.I[i].C}" data-marketplace-variant-name="${response.data.I[i].N}" data-allow-custom="${response.data.I[i].AC}" data-mandatory="${response.data.I[i].M}">
    <label class="input-abs-text">
        ${response.data.I[i].N}:
        ${response.data.I[i].M ? '<span class="kt-font-danger">*</span>' : ''}
    </label>
    <div class="kt-input-icon kt-input-icon--left">
        <input type="text" class="form-control" placeholder="${response.data.I[i].N} giriniz" />
        <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span><i class="la la-i-cursor"></i></span>
        </span>
    </div>
</div>`;
                                    }
                                    else {
                                        html += `
<div class="form-group pos-relative" data-marketplace-variant-value-code="" data-marketplace-variant-value-name="" data-marketplace-variant-code="${response.data.I[i].C}" data-marketplace-variant-name="${response.data.I[i].N}" data-allow-custom="${response.data.I[i].AC}" data-mandatory="${response.data.I[i].M}">
    <label class="input-abs-text">
        ${response.data.I[i].N}:
        ${response.data.I[i].M ? '<span class="kt-font-danger">*</span>' : ''}
    </label>
    <div class="typeahead">
        <div class="kt-input-icon kt-input-icon--left">
            <input type="text" class="form-control" placeholder="${response.data.I[i].N} arayınız" />
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
    </div>
</div>`;
                                    }
                                }
                                $(`.marketplace-variant-container[data-marketplace-id="${marketplaceId}"]`).html(html);

                                InitializeMarketplaceVariantsAutocomplete(marketplaceId);

                            }

                        });

                    });
                });

                InitializeMarketplaceVariantsAutocomplete(marketplaceId);

            });

            $("#modal_submit").click(function () {
                var valid = Validation.Valid("marketplace_form");
                if (valid) {

                    var productMarketplaces = [];
                    $('.product-marketplace-container').each((i, theDiv) => {

                        var $theDiv = $(theDiv);
                        var marketplaceId = $theDiv.data('marketplace-id');

                        var $formGroups = $theDiv.find('.form-group');

                        var productMarketplace = {
                            ProductId: parseInt($('#product-id').val()),
                            MarketplaceId: marketplaceId,
                            Active: $formGroups.eq(0).find('[type="checkbox"]').eq(0).prop('checked'),
                            Id: $theDiv.data('product-marketplace-id'),
                            MarketplaceCategoryCode: "-",
                            MarketplaceCategoryName: "-",
                            MarketplaceBrandCode: "-",
                            MarketplaceBrandName: "-",
                            SellerCode: "-",
                            DeliveryDay: 0
                        };

                        if ($theDiv.data('matchable')) {
                            productMarketplace.MarketplaceCategoryCode = $formGroups.eq(1).data('marketplace-category-code').toString();
                            productMarketplace.MarketplaceCategoryName = $formGroups.eq(1).data('marketplace-category-name');
                            productMarketplace.MarketplaceBrandCode = $formGroups.eq(2).data('marketplace-brand-code').toString();
                            productMarketplace.MarketplaceBrandName = $formGroups.eq(2).data('marketplace-brand-name');
                            productMarketplace.SellerCode = $formGroups.eq(3).find('[type="text"]').val();
                            productMarketplace.DeliveryDay = parseInt($formGroups.eq(4).find('[type="text"]').val());
                        }

                        productMarketplaces.push(productMarketplace);
                    });

                    var productInformationMarketplaces = [];
                    $('.product-information-marketplace-container').each((i, theDiv) => {

                        var $theDiv = $(theDiv);
                        var marketplaceId = $theDiv.data('marketplace-id');
                        var productInformationId = parseInt($theDiv.data('product-information-id'));

                        var $formGroups = $theDiv.find('.form-group');

                        var productInformationMarketplace = {
                            ProductInformationId: productInformationId,
                            MarketplaceId: marketplaceId,
                            Id: $theDiv.data('product-information-marketplace-id'),
                            AccountingPrice: $formGroups.find('[type="checkbox"].accounting-price').prop('checked'),
                            DisabledUpdateProduct: $formGroups.find('[type="checkbox"].disabled-update-product').prop('checked'),
                            Active: $formGroups.find('[type="checkbox"].active').prop('checked'),
                            Barcode: $formGroups.find('[type="text"].barcode').val(),
                            AccountingPrice: $formGroups.find('[type="checkbox"].accounting-price').prop('checked'),
                            ListUnitPrice: parseFloat($formGroups.find('[type="text"].list-unit-price').val()),
                            UnitPrice: parseFloat($formGroups.find('[type="text"].unit-price').val()),
                            ProductInformationMarketplaceVariantValues: []
                        };

                        if ($theDiv.data('matchable')) {
                            productInformationMarketplace.StockCode = $formGroups.find('[type="text"].stock-code').val();
                        }

                        $formGroups = $theDiv.find('.marketplace-variant-container').eq(0).find('.form-group');

                        $formGroups.each((i, theFormGroup) => {
                            var $theFormGroup = $(theFormGroup);

                            var allowCustom = $theFormGroup.data("allow-custom");

                            var productInformationMarketplaceVariantValue = {
                                MarketplaceId: marketplaceId,
                                MarketplaceVariantCode: $theFormGroup.data("marketplace-variant-code").toString(),
                                MarketplaceVariantName: $theFormGroup.data("marketplace-variant-name").toString(),
                                AllowCustom: allowCustom,
                                Mandatory: $theFormGroup.data("mandatory"),
                                Varianter: $theFormGroup.data("varianter"),
                                Multiple: $theFormGroup.data("multiple")
                            };

                            if (allowCustom) {
                                productInformationMarketplaceVariantValue.MarketplaceVariantValueCode = $theFormGroup.find('[type="text"]').eq(0).val();
                                productInformationMarketplaceVariantValue.MarketplaceVariantValueName = $theFormGroup.find('[type="text"]').eq(0).val();
                            }
                            else {
                                productInformationMarketplaceVariantValue.MarketplaceVariantValueCode = $theFormGroup.data("marketplace-variant-value-code").toString();
                                productInformationMarketplaceVariantValue.MarketplaceVariantValueName = $theFormGroup.data("marketplace-variant-value-name").toString();
                            }

                            if (productInformationMarketplaceVariantValue.MarketplaceVariantValueCode != '') {
                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.push(productInformationMarketplaceVariantValue);
                            }
                        });

                        productInformationMarketplaces.push(productInformationMarketplace);
                    });

                    HttpClient.Post("/ProductInformationMarketplace/Upsert", {
                        productMarketplaces: productMarketplaces,
                        productInformationMarketplaces: productInformationMarketplaces
                    }, function (response) {

                        if (response.productInformationMarketplaceResponse.success && response.productMarketplaceResponse.success)
                            $("#product_modal").modal("hide");
                        else {
                            Notify.Show(NotifyType.Error, response.productInformationMarketplaceResponse.message);
                        }
                    });

                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm varyasyonları doldurunuz.");
            });

        });
    },
    OpenPriceUpdateModal: function (id) {
        HttpClient.Get(`/ProductInformation/PriceUpdate?ProductId=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#product_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Fiyat Güncelle");

            $('.check-all-product_marketplace_active').change(function () {
                $('.product_marketplace_active').prop('checked', $(this).prop('checked'))
            });

            $('.apply-all').click(function () {
                if (parseInt($('.template-list-unit-price').val()) > 0) {
                    $('.list-unit-price').val($('.template-list-unit-price').val());
                }

                if (parseInt($('.template-unit-price').val()) > 0) {
                    $('.unit-price').val($('.template-unit-price').val());
                }
            });

            $("#modal_submit").click(function () {
                var valid = Validation.Valid("price_update_form");
                if (valid) {

                    var data = Serializer.Serialize("price_update_form");

                    HttpClient.Post(
                        "/ProductInformation/PriceUpdate",
                        data,
                        function (response) {
                            if (response.success) {
                                Notify.Show(NotifyType.Success, response.message);

                                $("#product_modal").modal("hide");
                                $("#modal_body").html('');
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm varyasyonları doldurunuz.");
            });
        });
    },
    OpenProductDeletedModal: function (id) {

        swal.fire({
            title: 'Ürün kartı silinsin mi?',
            text: "Ürün kartını sistemden silmek istediğinize emin misiniz ?",
            type: 'warning',
            confirmButtonText: 'Ürünü Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            if (result.value) {
                HttpClient.Post(
                    "/Product/Delete",
                    {
                        id: id
                    },
                    function (response) {
                        if (response.success) {
                            Notify.Show(NotifyType.Success, response.message);
                            location.reload();

                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            }
        });

    },
    GetProductInformationVariantValues: function (id) {

        HttpClient.Get(
            `/ProductInformationMarketplace/ProductInformationVariantValues?Id=${id}`,
            function (response) {
                if (response.success) {
                    $.each(response.data, (index, value) => {

                        let $marketplaceVariantElement = $("[name=product-information-id][value=" + value.productInformationId + "]")
                            .closest('.product-information-container')
                            .find(`[data-marketplace-category-variant-id="${value.marketplaceVariantId}"]`);



                        if ($marketplaceVariantElement.length) {

                            if ($marketplaceVariantElement.hasClass("variant-value-custom") && $marketplaceVariantElement.val() == '') {
                                $marketplaceVariantElement.val(value.value);
                            }
                            else if ($marketplaceVariantElement.parent().find(".tt-input").val() == '') {
                                $marketplaceVariantElement.val(value.value);
                                $marketplaceVariantElement.data("marketplace-categories-marketplace-variant-value-id", value.marketplaceCategoriesMarketplaceVariantValueId);
                            }
                        }

                        //var color = $("[name=product-information-id][value=" + value.productInformationId + "]")
                        //    .closest('.product-information-container')
                        //    .find('[data-is-color="True"]')
                        //    .eq(0);

                        //if (color != undefined) {
                        //    color.find(`option`).filter(function () {
                        //        return $(this).html() == value.value;
                        //    }).attr('selected', true);
                        //}

                        //var gender = $("[name=product-information-id][value=" + value.productInformationId + "]")
                        //    .closest('.product-information-container')
                        //    .find('[data-is-gender="True"]')
                        //    .eq(0);

                        //if (gender != undefined) {
                        //    gender.find(`option`).filter(function () {
                        //        return $(this).html() == value.value;
                        //    }).attr('selected', true);
                        //}
                    });

                    $('.kt-selectpicker').selectpicker('refresh');
                }
            });

    },
    OpenCreateModal: function () {
        HttpClient.Get("/Product/Create", function (response) {

            ProductIndex.ProductInformationPartial("create");

            $("#modal_title").html("Yeni Ürün");
            $("#modal_body").html(response);
            $("#modal_submit").unbind("click");
            $("#product_modal").modal();

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });
            $('.kt-selectpicker').selectpicker();

            ProductIndex.ModalValidation("product_create_form", function () {
                let product = Serializer.Serialize("product_create_form");

                product.productLabels = [];
                $('#productLabels option:selected').each((index, element) => product.productLabels.push({
                    labelId: parseInt(element.value)
                }));

                product.productProperties = [];
                $("#productProperties option:selected").each((index, element) => product.productProperties.push({
                    PropertyValueId: parseInt(element.value)
                }));

                product.multiCategoryIds = [];
                $("#multiCategoryIds option:selected").each((index, element) => product.multiCategoryIds.push(element.value));

                HttpClient.Post(
                    "/Product/Create",
                    product,
                    function (response) {
                        if (response.success) {
                            ProductIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#product_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            ProductIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {


                $("#product_create_form").submit();
            });

        });
    },
    ProductInformationPartial: function (senderPartial) {

        let variants = null;

        $.each($("#variants :selected"), (index, element) => {
            if (variants === null) {
                variants = [];
            }

            let variant = null;
            variant = variants.find(v => v.id == $(element).data("variant-id"));

            if (variant === undefined) {
                variant = {
                    id: $(element).data("variant-id").toString(),
                    photoable: $(element).data("variant-photoable") == "False" ? false : true,
                    showVariant: $(element).data("variant-show-variant") == "False" ? false : true,
                    variantValues: [],
                    variantTranslations: [{ name: $(element).data("variant-id").toString() }]
                };

                variants.push(variant);
            }

            variant.variantValues.push({
                id: $(element).val(),
                variantId: variant.id,
                variant: {
                    VariantTranslations: [{ name: $(element).data("variant-name").toString() }],
                    photoable: $(element).data("variant-photoable") == "False" ? false : true,
                    showVariant: $(element).data("variant-show-variant") == "False" ? false : true
                },
                variantValueTranslations: [{
                    value: $(element).text()
                }]
            });
        });

        var data = {
            productInformations: senderPartial === "update" ? Serializer.Serialize("product_update_form").productInformations : null,
            variants: variants
        };

        if (senderPartial === "create") {
            HttpClient.Post(
                "/ProductInformation/IndexPartial",
                data,
                function (response) {

                    $("#product_informations").html(response);

                    ProductIndex.FileImageUpload();

                    $('.summernote').summernote({
                        height: 150
                    });

                    ProductIndex.Initialize(0);

                    $('.kt-selectpicker').selectpicker();

                    $('.touchspin').TouchSpin({
                        buttondown_class: 'btn btn-secondary',
                        buttonup_class: 'btn btn-secondary'
                    });

                });
        }
        else
            HttpClient.Post(
                "/ProductInformation/ConfirmPartial",
                data,
                function (response) {

                    $("#confirm_modal_title").html("Varyasyon Seçimi");
                    $("#confirm_modal_body").html(response);
                    $("#confirm_modal").modal();
                    $("#confirm_modal_submit").click(function () {

                        data.toBeDeletedProductInformations = $(".variant-selection:unchecked").map((i, v) => $(v).val()).get();

                        $("#confirm_modal_title").html("...");
                        $("#confirm_modal_body").html("");
                        $("#confirm_modal").modal("hide");
                        $("#confirm_modal_submit").unbind("click");

                        HttpClient.Post(
                            "/ProductInformation/IndexPartial",
                            data,
                            function (response) {

                                $("#product_informations").html(response);

                                ProductIndex.FileImageUpload();

                                $('.summernote').summernote({
                                    height: 150
                                });

                                ProductIndex.Initialize(0);

                                $('.kt-selectpicker').selectpicker();

                                $('.touchspin').TouchSpin({
                                    buttondown_class: 'btn btn-secondary',
                                    buttonup_class: 'btn btn-secondary'
                                });

                            });

                    });

                });

    },
    RemoveProductInformation: function (index, saved) {
        $(`[name="product_information_${index}"]`).closest('.card').remove();

        if (saved) {
            $(`[name=productInformations[${index}][active]:boolean]`).val(false);
        }
    },
    FileImageUpload: function () {

        $("a[name='photoRemove']").off("click");
        $("a[name='photoRemove']").on("click", function () {
            let $this = $(this);

            swal.fire({
                title: 'Sil!',
                text: "Ürün resmi silinsin mi?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Evet',
                cancelButtonText: 'Hayır',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $this.closest(".productInformationPhoto").remove();
                }
            });
        });

        $("input[type=file]").off("change");
        $("input[type=file]").on("change", function () {
            var productInformationIndex = $(this).data('product-information-id');
            var photoId = $(this).data('photo-id');

            const file = $(this).prop('files')[0];
            const reader = new FileReader();

            reader.addEventListener("load", function () {

                var formData = new FormData();
                formData.append("formFile", file);

                HttpClient.PostFile("/ProductInformationPhoto", formData, function (response) {

                    $(`#filename_${photoId}_${productInformationIndex}`).val(`data:image/jpeg;base64,${response.data}`);
                    $(`#fileNameImage-preview_${photoId}_${productInformationIndex}`).css('background-image', `url(data:image/jpeg;base64,${response.data})`);

                });

                //HttpClient.Post("/ProductInformationPhoto", {
                //    base64: reader.result
                //}, function (response) {

                //    $(`#filename_${photoId}_${productInformationIndex}`).val(`data:image/jpeg;base64,${response.data}`);
                //    $(`#fileNameImage-preview_${photoId}_${productInformationIndex}`).css('background-image', `url(data:image/jpeg;base64,${response.data})`);

                //});

            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        });

        $("a[name='addButton']").off("click");
        $("a[name='addButton']").on("click", function () {

            //Product Information Index
            var productInformationIndex = $(this).data('product-information-id');

            var productInformationPhotoHmtl = $('#productInformationPhoto_' + productInformationIndex);
            var j = $('[name="product_information_' + productInformationIndex + '"]').find('.productInformationPhoto').length;

            var html = "<div class=\"col-lg-12 productInformationPhoto\" id=\"productInformation-photo-item-" + j + "\">";
            html += "<div class=\"form-group row\" style=\"display:flex;align-items:center;justify-content: space-around;\">"
            html += "<label class=\"col-xl-1 col-lg-1 col-form-label\">Sıra</label>";
            html += "<input class=\"col-form-label photoİndex form-control touchspin\" type=\"number\" value=\"" + (j + 1) + "\" name=\"productInformations[" + productInformationIndex + "][ProductInformationPhotos][" + j + "][displayOrder]\" style=\"flex:0.1\">"
            html += "<div class=\"productPhoto\" style=\"flex:0.7;text-align:center;\">"
            html += "<div class=\"kt-avatar kt-avatar--outline \" id=\"kt_user_avatar_1\">"
            html += "<input type=\"hidden\" name=\"productInformations[" + productInformationIndex + "][ProductInformationPhotos][" + j + "][id]\" value=\"0\" />"
            html += "<input type=\"hidden\" id=\"filename_" + j + "_" + productInformationIndex + "\" name=\"productInformations[" + productInformationIndex + "][ProductInformationPhotos][" + j + "][fileName]\" class=\"form-control\">"
            html += "<div class=\"kt-avatar__holder photoSrc\" id=\"fileNameImage-preview_" + j + "_" + productInformationIndex + "\" \"></div>"
            html += "<label class=\"kt-avatar__upload\" data-toggle=\"kt-tooltip\" title=\"\" data-original-title=\"Change avatar\">"
            html += "<i class=\"fa fa-pen\"></i>"
            html += "<input type=\"file\" name=\"profile_avatar\" data-product-information-id=\"" + productInformationIndex + "\" data-photo-id=\"" + j + "\">"
            html += "</label>"
            html += "</div>"
            html += "</div>"
            html += "<div class=\"photoRemove \"   style=\"flex:0.2\">"
            html += "<a title=\"Sil\" name=\"photoRemove\" data-productInformation-photo-item=" + j + " class=\"btn btn-lg btn-warning btn-icon btn-icon-lg\">"
            html += "<i class=\"la la-trash\"></i>"
            html += "</a>"
            html += "</div>"
            html += "</div>"
            html += "</div>"

            productInformationPhotoHmtl.append(html);

            ProductIndex.FileImageUpload();

            $('.touchspin').TouchSpin({
                buttondown_class: 'btn btn-secondary',
                buttonup_class: 'btn btn-secondary'
            });
        });

    },
    AutoComplateInitalize: function () {

        Autocomplete.Init(
            $('[name="productId"]'),
            '/Product/Autocomplete',
            5,
            null,
            (i) => { return { key: i.key.split('-')[1], value: i.value } },
            'value',
            null,
            (suggestion) => ProductIndex.GetProductInformationCombines(suggestion.key, suggestion.value),
            () => { },
            () => { });

    },
    GetProductInformationCombines: function (id, value) {
        var productInformationExsist = $(`#product-information-combine-${id}`).length > 0;

        if (productInformationExsist) {
            var orderDetailQuantity = parseInt($(`#product-information-combine-quantity-${id}`).val()) + 1;
            $(`#product-information-combine-quantity-${id}`).val(orderDetailQuantity)
            return;
        }

        var data = ProductIndex.ParseProductInformationCombines();

        data.push({
            id: 0,
            containProductInformationId: parseInt(id),
            quantity: 1,
            name: value
        });

        ProductIndex.RefleshProductInformationCombine(data);
    },
    DeleteProductInformationCombines: function (id, value) {


        SwalAlertModal.ConfirmPopup('Ürünü listeden çıkart', `${value} ürününü listeden çıkarmak istiyor musunuz?`, 'Evet', true, 'Hayır',
            function () {

                $(`#product-information-combine-${id}`).remove();
                var data = ProductIndex.ParseProductInformationCombines();
                ProductIndex.RefleshProductInformationCombine(data);

            }
        )


    },
    ChangeProductType: function (typeId) {

        if (typeId == 'PC') {
            $("[name='product-type-pc']").show();
            $("[name='product-type-pi']").hide();
        }
        else {
            $("[name='product-type-pc']").hide();
            $("[name='product-type-pi']").show();
        }
    },
    RefleshProductInformationCombine: function (data) {

        HttpClient.Post(
            "/ProductInformationCombine/IndexPartial",
            data,
            function (response) {

                $("#product-information-combine-tbody").html(response);
                $(".combine-list-table").show();

                $('.touchspin').TouchSpin({
                    buttondown_class: 'btn btn-secondary',
                    buttonup_class: 'btn btn-secondary'
                });

            });

    },
    ParseProductInformationCombines: function () {
        return $.map($('#product-information-combine-tbody tr'), function (value) {

            var index = $(value).data("product-information-index");

            return {
                id: $(`[name='productInformations[0][productInformationCombines][${index}][id]']`).val(),
                containProductInformationId: $(`[name='productInformations[0][productInformationCombines][${index}][containProductInformationId]']`).val(),
                quantity: $(`[name='productInformations[0][productInformationCombines][${index}][quantity]']`).val(),
                name: $(value).data("product-information-name")
            }
        });
    },
    CurSelect: function () {
        var curOpt = $(".curSelect option:selected").val();
        $(".conversionUnitCost").show();
        $(".tlCost").hide();
        $(".curType").text(curOpt)

        if (curOpt == "") {
            $(".conversionUnitCost").hide();
            $(".tlCost").show();
        }
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ProductIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ProductIndex.Read(0);
    },
    ProductInformationVariantUpdate: function (updateNew) {

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: 'Lütfen bekleyiniz...'
        });

        let product = Serializer.Serialize($("#product_create_form").length > 0 ? "product_create_form" : "product_update_form");

        let variantTemplate = $('#product_informations .card').eq(0);

        let barcode = parseInt(variantTemplate.find("[name='productInformations[-1][barcode]']").val());
        let stockCode = parseInt(variantTemplate.find("[name='productInformations[-1][stockCode]']").val());
        let stock = variantTemplate.find("[name='productInformations[-1][stock]']").val();
        let shelfCode = variantTemplate.find("[name='productInformations[-1][shelfCode]']").val();
        let payor = variantTemplate.find("[name='productInformations[-1][payor]:boolean']").prop('checked');
        let isSale = variantTemplate.find("[name='productInformations[-1][isSale]:boolean']").prop('checked');

        let names = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[name]']");
        let subTitles = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[subTitle]']");
        let seoTitles = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][title]']");
        let seoMetaDescriptions = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][metaDescription]']");
        let seoMetaKeywords = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][metaKeywords]']");
        let contentDescriptions = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[ProductInformationContentTranslation][description]']");
        let contentContents = variantTemplate.find("[name^='productInformations[-1][productInformationTranslations]'][name$='[ProductInformationContentTranslation][content]']");

        let listUnitPrices = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[listUnitPrice]']");
        let unitPrices = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[unitPrice]']");
        let unitCosts = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[unitCost]']");
        let conversionCurrencyId = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[conversionCurrencyId]']");
        let conversionUnitCosts = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[conversionUnitCost]']");
        let vatRates = variantTemplate.find("[name^='productInformations[-1][productInformationPriceses]'][name$='[vatRate]']");

        $('#product_informations .card').not(".variant-template").each(function (index, value) {

            let variant = $(value);

            if (updateNew == true && variant.hasClass("new-variant") == false) {
                return;
            }
            var productInformationIndex = variant.data('index');

            let variantNames = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[name]']");
            let variantSubTitles = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[subTitle]']");
            let variantSeoTitles = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][title]']");
            let variantSeoMetaDescriptions = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][metaDescription]']");
            let variantSeoMetaKeywords = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[ProductInformationSeoTranslation][metaKeywords]']");
            let variantContentDescriptions = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[ProductInformationContentTranslation][description]']");
            let variantContentContents = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationTranslations]'][name$='[ProductInformationContentTranslation][content]']");

            names.each((index2, value2) => {
                let variantName = $(value2).val();

                if (product.productInformations[productInformationIndex]) {
                    $.each(product.productInformations[productInformationIndex].productInformationVariants, (index3, value3) => {
                        let variant = value3.variantValue.variant.variantTranslations[0].name;
                        let variantValue = value3.variantValue.variantValueTranslations[0].value;

                        if (variantName.indexOf(`#${variant.toLowerCase()}`) != -1) {
                            variantName = variantName.replace(`#${variant.toLowerCase()}`, variantValue);
                        }
                    });
                    variantNames.eq(0).val(variantName);
                }

            });
            subTitles.each((index, value) => { variantSubTitles.eq(index).val($(value).val()) });
            seoTitles.each((index, value) => { variantSeoTitles.eq(index).val($(value).val()) });
            seoMetaDescriptions.each((index, value) => { variantSeoMetaDescriptions.eq(index).val($(value).val()) });
            seoMetaKeywords.each((index, value) => { variantSeoMetaKeywords.eq(index).val($(value).val()) });
            contentDescriptions.each((index, value) => { variantContentDescriptions.eq(index).summernote('code', $(value).val()) });
            contentContents.each((index, value) => { variantContentContents.eq(index).summernote('code', $(value).val()) });

            let variantListUnitPrices = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[listUnitPrice]']");
            let variantUnitPrices = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[unitPrice]']");
            let variantUnitCosts = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[unitCost]']");
            let variantConversionCurrencyId = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[conversionCurrencyId]']");
            let variantConversionUnitCosts = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[conversionUnitCost]']");
            let variantVatRates = variant.find("[name^='productInformations[" + productInformationIndex + "][productInformationPriceses]'][name$='[vatRate]']");

            listUnitPrices.each((index, value) => { variantListUnitPrices.eq(index).val($(value).val()); });
            unitPrices.each((index, value) => { variantUnitPrices.eq(index).val($(value).val()); });
            unitCosts.each((index, value) => { variantUnitCosts.eq(index).val($(value).val()); });
            conversionCurrencyId.each((index, value) => { variantConversionCurrencyId.eq(index).val($(value).val()); });
            conversionUnitCosts.each((index, value) => { variantConversionUnitCosts.eq(index).val($(value).val()); });
            vatRates.each((index, value) => { variantVatRates.eq(index).val($(value).val()); });

            /* Update only empty */
            if ($(`[name='productInformations[${productInformationIndex}][barcode]`).val() == "") {
                $(`[name='productInformations[${productInformationIndex}][barcode]`).val((barcode + index + 1));
            }
            if ($(`[name='productInformations[${productInformationIndex}][stockCode]`).val() == "") {
                $(`[name='productInformations[${productInformationIndex}][stockCode]`).val((stockCode + index + 1));
            }
            /* Update always */

            $(`[name='productInformations[${productInformationIndex}][stock]`).val(stock);
            $(`[name='productInformations[${productInformationIndex}][shelfCode]`).val(shelfCode);
            $(`[name='productInformations[${productInformationIndex}][payor]:boolean`).prop('checked', payor);
            $(`[name='productInformations[${productInformationIndex}][isSale]:boolean`).prop('checked', isSale);

        });

        $('.kt-selectpicker').selectpicker('refresh');

        $('.summernote').summernote();

        Notify.Show(NotifyType.Success, "Varyasyon şablonu barşılı bir şekilde tümüne uygulandı.");

        KTApp.unblockPage();
    },
    ActivePassiveProductMarketplace: function ($this) {
        let marketplaceId = $this.closest("[data-check-marketplace-id]").data("check-marketplace-id");
        $(`[data-check-marketplace-id="${marketplaceId}"] .product_marketplace_active`).each((i, v) => {
            let $v = $(v);
            $v.attr("disabled", !$this.prop("checked"));

            if ($v.attr("type") == "checkbox") {
                $v.prop("checked", $this.prop("checked"));
            }
        });
    },
    ActivePassiveMarketplace: function ($this) {
        var $productInformationContainerElement = $this.closest(".marketplace-container");
        $productInformationContainerElement.find("[type='text'], select").each(function (index, value) {
            $(value).prop("disabled", !$this.prop("checked"));
        });

        $('.kt-selectpicker').selectpicker('refresh');
    },
    OpenBulkMarketplaceModal: function () {
        HttpClient.Get("/BulkMarketplaceUpdate/Index", function (response) {

            $("#bulk_marketplace_modal_body").html(response);
            $("#bulk_marketplace_modal").modal();

        });
    },

    UpdateBulkMarketplace: function () {
        var formData = new FormData();
        formData.append("file", document.getElementById("bulk-marketplace-file").files[0]);

        HttpClient.PostFile("/BulkMarketplaceUpdate/Update", formData, function (response) {
            if (response.success) {
                Notify.Show(NotifyType.Success, response.message);
            }
            else {
                Notify.Show(NotifyType.Error, response.message);
            }
        });
    },
    Collapse: function (className) {

        if (className.startsWith('P') && $(`.${className}`).eq(0).is('.hide') && $(`.${className}`).eq(0).is('.opened') == false) {

            $(`.${className}`).eq(0).addClass('opened');

            var codes = [];
            $(`.${className}`).each((i, he) => {
                codes.push($(he).data('model-code'));
            });

            if (ProductIndex.Tenant == 'Lafaba') {
                HttpClient.Post("/Product/Last12MonthSales", codes, function (response) {

                    for (var i = 0; i < response.items.length; i++) {

                        ProductIndex.Chart(`chart-${response.items[i].skuCode}`, response.labels, response.items[i].series);

                    }

                });
            }

        }

        $(`.${className}`).each((i, he) => {

            if ($(he).is('.hide'))
                $(he).removeClass('hide')
            else {
                $(he).addClass('hide');
                $(`.${className}-Child`).addClass('hide');
            }

        });

        if ($(`#${className}-Icon`).is('.la-angle-down'))
            $(`#${className}-Icon`).removeClass('la-angle-down').addClass('la-angle-up');
        else {
            $(`#${className}-Icon`).removeClass('la-angle-up').addClass('la-angle-down');
            $(`.${className} i`).removeClass('la-angle-up').addClass('la-angle-down');
        }
    },
    ImageGallery: function () {
        const galleries = document.querySelectorAll(".product-gallery");
        galleries.forEach((div, index) => {
            const images = div.querySelectorAll(".product-gallery-item");
            const offset = 30; //resimler arasındaki mesafe abi

            images.forEach((img, index) => {

                if (images.length == index + 1) {

                    img.style.opacity = 1;
                    img.style.setProperty('box-shadow', '#00000014 0px 0px 15px');

                }

                img.style.left = `${((index * offset) + 10)}px`;

                img.addEventListener("mouseover", function () {

                    images.forEach((image) => {

                        image.style.zIndex = 0;
                        image.style.opacity = 0.3;
                        image.style.removeProperty('box-shadow');

                    });

                    img.style.zIndex = 1;
                    img.style.opacity = 1;
                    img.style.setProperty('box-shadow', '#00000014 0px 0px 15px');

                });

            });
        });
    },
    Pagination: function (recordsCount, pageRecordsCount, page) {
        let paginationHtml = '';
        const pageCounts = Math.ceil(recordsCount / pageRecordsCount);
        if (pageCounts > 5) {
            paginationHtml += `<li class="kt-pagination__link--first"><a href="javascript:ProductIndex.Read(0)"><i class="fa fa-angle-double-left kt-font-brand"></i></a></li>`;
        }

        for (var i = 0; i < pageCounts; i++) {
            let begin = 0;
            if (pageCounts > 10) {
                begin = page;

                if (page > 5) {
                    begin -= 5;
                    if (pageCounts - (page + 1) < 5) {
                        begin -= 5 - (pageCounts - page);
                    }
                }
                else {
                    begin = 0;
                }
            }

            if (begin <= i && (begin + 10) > i) {
                paginationHtml += `<li class="${i == page ? "kt-pagination__link--active" : ""}"><a href="javascript:ProductIndex.Read(${i})">${i + 1}</a></li>`;
            }
        }

        if (pageCounts > 5) {
            paginationHtml += `<li class="kt-pagination__link--last"><a href="javascript:ProductIndex.Read(${pageCounts - 1})"><i class="fa fa-angle-double-right kt-font-brand"></i></a></li>`;
        }

        $('.kt-pagination__links').html(paginationHtml);
        $('.pagination__desc').html(`Toplam ${recordsCount} kayıttan ${page * pageRecordsCount} ile ${(page * pageRecordsCount) + pageRecordsCount} arası gösterilliyor.`);
    },
    Chart: function (id, labels, series) {

        const ctx = document.getElementById(id);

        const data = {
            labels: labels,
            datasets: []
        };

        for (var i = 0; i < series.length; i++) {

            var borderColor = '#5867dd';

            if (series[i].categorization.indexOf('CLEAR') != -1) {
                borderColor = '#fd27eb';
            } else if (series[i].categorization.indexOf('DISCOUNT') != -1) {
                borderColor = '#ffb822';
            } else if (series[i].categorization == 'STAR') {
                borderColor = '#1dc9b7';
            } else if (series[i].categorization.indexOf('NOS') != -1) {
                borderColor = '#22b9ff';
            }

            data.datasets.push({
                label: series[i].categorization,
                data: series[i].data,
                borderColor: borderColor,
                cubicInterpolationMode: 'monotone',
                tension: 0.4
            });

        }

        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        grid: {
                            display: false
                        },
                        ticks: {
                            display: false //this will remove only the label
                        }
                    },
                    y: {
                        grid: {
                            display: false
                        },
                        ticks: {
                            font: {
                                size: 10
                            },
                            display: true //this will remove only the label
                        }
                    }
                }
            },
        };

        let financialWidgetChart = new Chart(ctx, config);
    },
    OpenSizeTableModal: function (id) {

        HttpClient.Get(`/SizeTable/Get?Id=${id}`, function (response) {
            $('#size_table_modal_body').html(response);
            $('#size_table_modal').modal();
        });

    },
    OpenCategorizationModal: function (id) {

        HttpClient.Get(`/ProductCategorization/Get?Id=${id}`, function (response) {
            $('#categorization_modal_body').html(response);
            $('#categorization_modal').modal();
            $('.categorization').selectpicker();
            $('[data-target]').change(function () {
                $(`#${$(this).data('target')}`).attr('disabled', $(this).prop('checked') == false);
                $('.categorization').selectpicker('refresh');
            });
        });

        //$("#categorization_submit").unbind("click");
        //$("#categorization_submit").click(function () {

        //    HttpClient.Put(
        //        "/ProductInformation/UpdateCategorization",
        //        {
        //            SkuCode: $('#sku-code').val(),
        //            Categorization: $('#categorization').val()
        //        },
        //        function (response) {
        //            if (response.success) {
        //                Notify.Show(NotifyType.Success, response.message);

        //                $("#categorization_modal").modal("hide");
        //            }
        //            else {
        //                Notify.Show(NotifyType.Error, response.message);
        //            }
        //        }
        //    );

        //});
        //$("#categorization_modal").modal();

    },
    OpenCategorizationLogModal: function (id) {

        HttpClient.Get(`/ProductCategorization/Log?Id=${id}`, function (response) {
            $('#categorization_log_modal_body').html(response);
            $('#categorization_log_modal').modal();
        });

    },
    DownloadBulkMarketplace: function () { //Silinebilir kontrol edilecek BulkMarketplaceUpdate/index
        window.open(`/BulkMarketplaceUpdate/Download`);
    },
    DownloadBulkMarketplaceExcel: function () {
        var search = `Search=Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_BrandId:${$("#ajax_data_search_brand_id").val()}_ProductSourceDomainId:${$("#ajax_data_search_product_domain_source_id").val() != undefined ? $("#ajax_data_search_product_domain_source_id").val() : "-1"}_CategoryId:${$("#ajax_data_search_category_id").val()}_ProductPropertyId:${$("#ajax_data_search_product_property").val()}_ProductCategorization:${$("#ajax_data_search_product_categorization").length > 0 ? $("#ajax_data_search_product_categorization").val() : ''}_Supplier:${$("#ajax_data_search_supplier").length > 0 ? $("#ajax_data_search_supplier").val() : ''}_SpecialCategorization:${$("#ajax_data_search_special_categorization").length > 0 ? $("#ajax_data_search_special_categorization").val() : ''}_SizeTable:${$("#ajax_data_search_size_table").length > 0 ? $("#ajax_data_search_size_table").val() : ''}`

        window.open(`/ProductPrice/ExcelDownload?search=${search}`);
    },
};

function InitializeMarketplaceVariantsAutocomplete(marketplaceId) {
    $(`.marketplace-variant-container[data-marketplace-id="${marketplaceId}"]`).each((i, theDiv) => {

        var $theDiv = $(theDiv);

        var marketplaceId = $theDiv.data('marketplace-id');
        var isECommerce = $theDiv.data('is-ecommerce');

        var $formGroups = $theDiv.find(".form-group[data-allow-custom='false']");
        $formGroups.each((i, formGroup) => {
            var $formGroup = $(formGroup);
            var $input = $formGroup.find("[type='text']").eq(0);

            $input.focus(function () {

                if ($input.data('autocomplete-initialized') == true)
                    return;

                $input.data('autocomplete-initialized', true);

                Autocomplete.InitRemote(
                    $input,
                    isECommerce ? "/ECommerceVariantValue/SearchByName" : "https://marketplacecatalog.helpy.com.tr/VariantValue/SearchByName",
                    [{
                        key: "MarketplaceId",
                        value: marketplaceId
                    }, {
                        key: "VariantCode",
                        value: $formGroup.data("marketplace-variant-code")
                    }, {
                        key: "CategoryCode",
                        value: $(`.product-marketplace-container[data-marketplace-id="${marketplaceId}"]`).find('[data-marketplace-category-code]').data("marketplace-category-code")
                    }],
                    (i) => { return { VariantValueCode: i.C, VariantValueName: i.N } },
                    "VariantValueName",
                    null,
                    (suggestion) => {
                        $formGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                        $formGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);

                        if ($('.handle-variant-entries').prop('checked')) {

                            var marketplaceVariantCode = $formGroup.data("marketplace-variant-code");

                            var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                            var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                            for (var i = index; i < length; i++) {
                                var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + marketplaceVariantCode + '"]').eq(0);

                                $foundFormGroup.find('[type="text"]').val(suggestion.VariantValueName);

                                $foundFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                                $foundFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                            }

                        }

                        $input.typeahead("destroy");
                        $input.data('autocomplete-initialized', false);
                    },
                    () => {
                        $formGroup.find('[type="text"]').val('');
                        $formGroup.data("marketplace-variant-value-code", "");
                        $formGroup.data("marketplace-variant-value-name", "");
                    },
                    () => {
                        if ($formGroup.data("marketplace-variant-value-code") == '') {
                            $formGroup.find('[type="text"]').val('');
                            $formGroup.data("marketplace-variant-value-code", "");
                            $formGroup.data("marketplace-variant-value-name", "");

                            if ($input.data('autocomplete-initialized') == false)
                                return;

                            Notify.Show(NotifyType.Error, "Varyasyon seçimi yapamadınız.");
                        }
                    });

                $input.closest(".form-group").find(".tt-input").focus();
            });

        });

        var $searchButtons = $theDiv.find("button[data-full-search-marketplace-variant-code]");
        $searchButtons.each((i, button) => {
            var $button = $(button);
            var variantCode = $button.data("full-search-marketplace-variant-code");
            var variantName = $button.data("full-search-marketplace-variant-name");
            var categoryCode = $(`.product-marketplace-container[data-marketplace-id="${marketplaceId}"]`).find('[data-marketplace-category-code]').data("marketplace-category-code");

            $button.unbind('click');
            $button.click(function () {
                HttpClient.Get(`https://marketplacecatalog.helpy.com.tr/VariantValue?MarketplaceId=${marketplaceId}&VariantCode=${variantCode}&CategoryCode=${categoryCode}`, function (response) {

                    $("#variant_value_modal").modal();
                    $("#variant_value_modal_title").html(`${variantName} Değerler`);
                    $("#variant_value_modal_body").empty();
                    for (var i = 0; i < response.data.I.length; i++) {
                        var $btn = $(`<button class="btn btn-primary btn-sm" data-val="${response.data.I[i].C}" style="margin-right: 10px; margin-bottom: 10px">${response.data.I[i].N}</button>`);
                        $btn.click(function () {
                            var $input = $button.closest('.row').find(`[data-marketplace-variant-code="${variantCode}"]`);
                            $input.data("marketplace-variant-value-code", $(this).data("val"));
                            $input.data("marketplace-variant-value-name", $(this).text());
                            $input.find('[type="text"]').val($(this).text());

                            if ($('.handle-variant-entries').prop('checked')) {

                                var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                                var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                                for (var i = index; i < length; i++) {
                                    var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + variantCode + '"]').eq(0);

                                    $foundFormGroup.find('[type="text"]').val($(this).text());

                                    $foundFormGroup.data("marketplace-variant-value-code", $(this).data("val"));
                                    $foundFormGroup.data("marketplace-variant-value-name", $(this).text());
                                }

                            }

                            $("#variant_value_modal").modal("hide");
                        });
                        $("#variant_value_modal_body").append($btn);
                    }

                });
            });
        });

    });
};

$(document).ready(() => {
    ProductIndex.Initialize();
});