﻿const ModalSize = {
    Small: 'sm',
    Medium: 'md',
    Large: 'lg',
    Larger: 'xl'
};

const OrderIndex = {
    Initialize: function () {

        OrderIndex.Read();
        OrderIndex.Summary();

        $('.kt-select2').select2({
            placeholder: "Seçiniz",
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {

                if (data.element && data.element.hasAttribute("data-html"))
                    return `<div style="text-align: center;">${data.element.getAttribute("data-html")}</div>`;
                else
                    return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });

        /* Order Types */
        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            OrderIndex.Read(0);
        });

        $('#ajax_data_search_date').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: true,
            startDate: moment().add(-6, 'day'),
            endDate: new Date(),
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            $('#ajax_data_search_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
            OrderIndex.Read(0);
        });

        $('#search_button').click(function () {
            let name = $('#search_name').val();
            let surname = $('#search_surname').val();
            let phone = $('#search_phone').val();

            if (name == '' && surname == '' && phone == '') {
                Notify.Show(NotifyType.Error, "Ad, soyad veya telefon alanlarından bir tanesini doldurunuz.");
                return;
            }

            $('#search_results').html('');

            HttpClient.Post('/Customer/Search', { name: name, surname: surname, phone: phone }, (response) => {
                $.each(response.data, (i, v) => {
                    $('#search_results').append(`<div class="row kt-mt-10"><div class="col-md-9">${v.name} ${v.surname} ${v.customerContact.phone}</div><div class="col-md-3"><button type="button" class="btn btn-brand" onclick="OrderIndex.FillCustomer(${v.id})">Seç</button></div></div>`);
                });
            });
        });
    },
    OrderDetailType: '',
    ShowStock: false,
    Summary: function () {
        HttpClient.Get(`/Order/Summary`, function (response) {
            $("#summary").html(response);

            setTimeout(OrderIndex.Summary, 20000);
        });
    },
    GetIncompleteOrders: function () {
        $('#ajax_data_search_order_type').selectpicker("val", ["KTE", "TS"]);
        $('#ajax_data_search_date').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "timePicker24Hour": true,
            autoUpdateInput: true,
            startDate: moment().add(-12, 'month'),
            endDate: moment().add(-15, 'day'),
            ranges: {
                'Bugün': [moment(), moment()],
                'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Son 7 gün': [moment().subtract(6, 'days'), moment()],
                'Son 30 gün': [moment().subtract(29, 'days'), moment()],
                'Bu ay': [moment().startOf('month'), moment().endOf('month')],
                'Geçen ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " / ",
                "applyLabel": "Uygula",
                "cancelLabel": "Vazgeç",
                "fromLabel": "Dan",
                "toLabel": "a",
                "customRangeLabel": "Tarih Seç",
                "daysOfWeek": [
                    "Pt",
                    "Sl",
                    "Çr",
                    "Pr",
                    "Cm",
                    "Ct",
                    "Pz"
                ],
                "monthNames": [
                    "Ocak",
                    "Şubat",
                    "Mart",
                    "Nisan",
                    "Mayıs",
                    "Haziran",
                    "Temmuz",
                    "Ağustos",
                    "Eylül",
                    "Ekim",
                    "Kasım",
                    "Aralık"
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            $('#ajax_data_search_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
            OrderIndex.Read(0);
        });
        OrderIndex.Read(0);
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        OrderIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        OrderIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Order/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `Id:${$("#ajax_data_search_id").val()}_Name:${$("#ajax_data_search_name").val()}_Surname:${$("#ajax_data_search_surname").val()}_Marketplace:${$("#ajax_data_search_marketplace").val()}_OrderType:${$("#ajax_data_search_order_type").val()}_MarketplaceOrderNumber:${$("#ajax_data_search_marketplace_order_number").val()}_CargoTrackingNumber:${$("#ajax_data_search_cargo_tracking_number").val()}_OrderSource:${$("#ajax_data_search_order_source").val()}_PhoneNumber:${$("#ajax_data_search_phone_number").val()}_Date:${$("#ajax_data_search_date").val()}_ShipmentCompany:${$("#ajax_data_search_shipment_company").val()}_ShoppingNumber:${$("#ajax_data_search_shopping_number").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_OrderPrepation:${$("#ajax_data_search_order_preparation").val()}_Invoiced:${$("#ajax_data_search_invoiced").val()}`
            },
            [
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        var html = "";
                        if (value.orderTypeId == 'OS' && value.orderShipments.length > 0 && value.orderShipments[0].trackingCode != '')
                            html = `<label class="kt-checkbox kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="${value.id}">
                            <span></span>
                        </label>`;


                        return html;
                    }
                },
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.customer.name + ' ' + value.customer.surname
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Padding: 0,
                    Render: function (value) {
                        
                        return `
<table class="table" style="margin: 0;">
    <tbody>
${value.orderDetails.map((v, i) => `
        <tr>
            <td style="font-weight: 500;${i == 0 ? ` border-top: none;` : ''}" width="20%">
                <div>${v.quantity} Adet</div>
                ${OrderIndex.ShowStock ? `<div class="small">Kalan Stok ${v.productInformation.stock} Adet</div>` : ''}
            </td>
            <td${i == 0 ? ` style="border-top: none;"` : ''}  width="30%">
                <a href="${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}" target="_blank">
                    <img class="table-product-img" src="https://imgr.helpy.com.tr/img/${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}/resize?size=150"/>
                </a>
            </td>
            <td${i == 0 ? ` style="border-top: none;"` : ''} width="45%">
                <div>${v.productInformation.productInformationName}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.stockCode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.stockCode}</b> kopyalandı.');">${v.productInformation.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.barcode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.barcode}</b> kopyalandı.');">${v.productInformation.barcode} <i class="la la-copy"></i> </a>
                </div>



            ${v.productInformation.productInformationMarketplaces.length > 0 && v.productInformation.productInformationMarketplaces[0].url ? `<div style="margin-top: 5px">
                    <a target="_blank" style="font-weight: 500" href="${v.productInformation.productInformationMarketplaces[0].url}">Pazaryerinde Göster  <i class="la la-arrow-right"></i> </a>
                </div>` : ''}

                <div class="kt-list-timeline">
    <div class="kt-list-timeline__items">

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--danger kt-font-danger"></span>
            <span class="kt-list-timeline__text ">Başarılı</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.orderDateStr}</span>
        </div>

 ${v.type == 2 || v.type == 3 ? `
<div class="kt-list-timeline">
    <div class="kt-list-timeline__items">
        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>
            <span class="kt-list-timeline__text ">Gider Pusulası Düzenlendi ${v.returnNumber} (${v.typeName})</span>
            <span class="kt-list-timeline__time " style="width:60%">${v.returnDateStr}</span>
        </div>
    </div>
</div>
` : ''}

 ${v.type == 4 ? `
<div class="kt-list-timeline">
    <div class="kt-list-timeline__items">
        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>
            <span class="kt-list-timeline__text ">İptal</span>
            <span class="kt-list-timeline__time " style="width:60%">${v.returnDateStr}</span>
        </div>
    </div>
</div>
` : ''}

    </div>
</div>


            </div>
            </td>
            <td style="font-weight: 500;${i == 0 ? ` border-top: none;` : ''}" width="5%">${v.unitPrice.toLocaleString('tr-TR')}

            ${value.marketplaceId === 'TY' ? `<div class="small">Komisyon (%${v.marketplaceCommission.rate}):</div>${v.unitCommissionAmount.toLocaleString('tr-TR')}` : ''}
            
            </td>
        </tr>
`).join('')}
    </tbody>
</table>`;
                    }
                },
                {
                    Name: "amount",
                    Type: "number",
                    Render: function (value) {
                        return `<span style="font-weight: 500;">${value.amount.toLocaleString('tr-TR')}</span>

                        ${value.marketplaceId === 'TY' ? `<div class="small">Toplam Komisyon:</div><span style="font-weight: 500;">${value.commissionAmount.toLocaleString('tr-TR')}</span><div class="small">Kargo:</div><span style="font-weight: 500;">${value.cargoFee.toLocaleString('tr-TR')}</span><div class="small" style="cursor: pointer;" data-toggle="kt-tooltip" data-placement="right" title="Ödeme ${value.marketplaceInvoice == null ? 'yapılmadı' : 'tamamlandı'}.">Ödeme:
${value.marketplaceInvoice == null ? '<i class="kt-menu__link-icon fa fa-times kt-font-danger"></i>' : '<i class="kt-menu__link-icon fa fa-check kt-font-success"></i>'}
</div>` : ''}`;
                    }
                },
                {
                    Name: "currencyId",
                    Type: "string",
                    Render: function (value) {
                        return `<span style="font-weight: 500;">${value.currencyId}</span>`;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        var html = `
<div class="kt-list-timeline">
    <div class="kt-list-timeline__items">

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--danger kt-font-danger"></span>
            <span class="kt-list-timeline__text ">Oluşturuldu</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.orderDateStr}</span>
        </div>
${(value.orderReserve == null || value.orderReserve.number == null) ? '' : `

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>
            <span class="kt-list-timeline__text ">Stok Rezerve Edildi ${value.orderReserve.number}</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.orderReserve.createdDateStr}</span>
        </div>

`}
${value.orderPicking == null ? '' : `

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--primary kt-font-primary"></span>
            <span class="kt-list-timeline__text ">Toplandı</span>
            <span class="kt-list-timeline__time " style="width:60%"> ${value.orderPicking.createdDateStr}</span>
        </div>

`}
${value.orderPacking == null ? '' : `

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--primary kt-font-primary"></span>
            <span class="kt-list-timeline__text ">Paketlendi (${value.orderPacking.packedUsername ? value.orderPacking.packedUsername : ''})</span>
            <span class="kt-list-timeline__time " style="width:60%"> ${value.orderPacking.createdDateStr}</span>
        </div>

`}
${(value.orderBilling == null || value.orderBilling.invoiceNumber == null) ? '' : `

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>
            <span class="kt-list-timeline__text ">Faturalandırıldı ${value.orderBilling.invoiceNumber}</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.orderBilling.createdDateStr}</span>
        </div>

`}


${(value.orderBilling == null || value.orderBilling.returnNumber == null) ? '' : `

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--brand kt-font-brand"></span>
            <span class="kt-list-timeline__text ">Gider Pusulası Düzenlendi ${value.orderBilling.returnNumber}</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.orderBilling.createdDateStr}</span>
        </div>

`}

    </div>
</div>`;
                        return html;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return `
<div>${value.orderType.orderTypeName}</div>
${value.packerBarcode != null ? `<div style="margin-top: 5px"><a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.packerBarcode}');Notify.Show(NotifyType.Info, '<b>${value.packerBarcode}</b> kopyalandı.');">${value.packerBarcode} <i class="la la-copy"></i> </a></div>
` : ''}
${(value.orderTypeId == "BE" && value.payments.filter(p => p.paymentTypeId == "C" && p.paid == false).length > 0) ? "<span class=\"kt-badge kt-badge--danger kt-badge--inline kt-badge--pill\">Ödeme Bekliyor</span>" : ""}${(value.orderTypeId == "BE" && value.payments.filter(p => p.paymentTypeId == "OO" && p.paid == false).length > 0) ? "<span class=\"kt-badge kt-badge--danger kt-badge--inline kt-badge--pill\">Hatalı Sipariş</span><div class=\"small\">" + value.payments.filter(p => p.paymentTypeId == "OO" && p.paid == false && (p.paymentLog.check3dMessage != null && p.paymentLog.pay3dMessage != null)).map((v, i) => `${v.paymentLog.check3dMessage} ${v.paymentLog.pay3dMessage}`) + "</div>" : ""}`;
                    }
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.orderSource.name == "Pazaryeri" ? `<div style="text-align: center;"><img width="64" src="/org/img/marketplace-icons/${value.marketplaceId}.png"/><div style="margin-top: 5px"><a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.marketplaceOrderNumber}');Notify.Show(NotifyType.Info, '<b>${value.marketplaceOrderNumber}</b> kopyalandı.');">${value.marketplaceOrderNumber} <i class="la la-copy"></i> </a></div></div>` : value.orderSource.name;
                    }

                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        let html = value.orderShipments.map((v, i) => `<div style="text-align: center;"><img width="64" src="/org/img/shipment-company-icons/${v.shipmentCompanyId}.png"/><div style="margin-top: 5px"><a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.trackingCode}');Notify.Show(NotifyType.Info, '<b>${v.trackingCode}</b> kopyalandı.');"><span id="${value.id}-tracking-code">${v.trackingCode}</span> <i class="la la-copy"></i></a></div></div>`).join('');

                        if (value.mutualBarcode) {
                            html += `<div style="text-align: center; margin-top: 10px;font-weight: 500"><a href="javascript:window.open('/Order/MutualBarcode?Id=${value.id}')">Barkod Görüntüle <i class="la la-barcode"></i></div>`;
                        }

                        return html;

                    }
                }
            ],
            "Order",
            [
                {
                    Render: function (value) {

                        return value.packerBarcode != '' && value.packerBarcode != undefined
                            ? `<a class="dropdown-item" href="javascript:OrderIndex.EmptyPackerBarcode(${value["id"]})"><i class="la la-exclamation"></i>
                            Sepeti Sil
                        </a>`
                            : '';
                    }
                },
                {
                    Render: function (value) {

                        return value.orderPacking != null || value.packerBarcode != null
                            ? `<a class="dropdown-item" href="javascript:OrderIndex.OpenDeletePickingModal(${value["id"]})"><i class="la la-exclamation"></i>
                            Paketlemeyi Sil
                        </a>`
                            : '';
                    }
                },
                {
                    Render: function (value) {
                        if ((value.marketplaceId == "TY" || value.marketplaceId == "HB") && (value.orderTypeId == "BE" || value.orderTypeId == "OS")) {
                            return `<a class="dropdown-item" href="javascript:OrderIndex.OpenChangeCargoCompanyModal('${value["marketplaceId"]}', '${value.orderShipments.length ? value.orderShipments[0].packageNumber : ''}', '${value.marketplaceOrderNumber}')"><i class="la la-truck"></i>
                            Pazaryeri Kargo İşlemleri
                        </a>`;
                        }
                        else
                            return null;
                    }
                }, {
                    Render: function (value) {
                        if (value.marketplaceId == "HB" && value.orderTypeId == "OS") {
                            return `<a class="dropdown-item" href="javascript:OrderIndex.Unpack('${value["marketplaceId"]}', '${value.orderShipments[0].packageNumber}', '${value.marketplaceOrderNumber}')"><i class="la la-truck"></i>
                            Paket Boz
                        </a>`;
                        }
                        else
                            return null;
                    }
                },
                {
                    Render: function (value) {

                        return (value.orderBilling == null || value.orderBilling.invoiceNumber == null) ? '' :
                            `<a class="dropdown-item" href="javascript:OrderIndex.OpenDeleteInvoiceModal(${value["id"]})"><i class="la la-exclamation"></i>
                            Faturayı Sil
                        </a>`;
                    }



                },
                {
                    Render: function (value) {

                        return (value.orderBilling == null || value.orderBilling.returnNumber == null) ? '' :
                            `<a class="dropdown-item" href="javascript:OrderIndex.OpenDeleteRetunInvoiceModal(${value["id"]})"><i class="la la-exclamation"></i>
                            Gider Pusulası Sil
                        </a>`;
                    }



                }
            ],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenCustomerSearchModal: function () {
        $("#customer_search_modal").modal();
    },
    FillCustomer: function (customerId) {
        $('#search_results').html('');

        HttpClient.Get(`/Customer/Read?Id=${customerId}`, (response) => {
            if (response.success) {

                $("#customer_search_modal").modal('hide');

                /* Delivery Address */
                $('[name="orderDeliveryAddress[firstName]"]').val(response.data.orderDeliveryAddress.firstName);
                $('[name="orderDeliveryAddress[lastName]"]').val(response.data.orderDeliveryAddress.lastName);
                $('[name="orderDeliveryAddress[phoneNumber]"]').val(response.data.orderDeliveryAddress.phoneNumber);
                $('[name="orderDeliveryAddress[address]"]').val(response.data.orderDeliveryAddress.address);
                $('#country-id').val(response.data.orderDeliveryAddress.countryId);

                let cityElement = document.getElementById("city-id");
                while (cityElement.children.length > 1) {
                    cityElement.children[1].remove();
                }

                var cityOption = document.createElement("option");
                cityOption.text = response.data.orderDeliveryAddress.neighborhood.district.city.name;
                cityOption.value = response.data.orderDeliveryAddress.neighborhood.district.city.id;

                cityElement.appendChild(cityOption);

                $('#city-id').val(response.data.orderDeliveryAddress.cityId);

                let districtElement = document.getElementById("district-id");
                while (districtElement.children.length > 1) {
                    districtElement.children[1].remove();
                }

                var districtOption = document.createElement("option");
                districtOption.text = response.data.orderDeliveryAddress.neighborhood.district.name;
                districtOption.value = response.data.orderDeliveryAddress.neighborhood.district.id;

                districtElement.appendChild(districtOption);

                $('#district-id').val(response.data.orderDeliveryAddress.districtId);

                let neighborhoodElement = document.getElementById("neighborhood-id");
                while (neighborhoodElement.children.length > 1) {
                    neighborhoodElement.children[1].remove();
                }

                var neighborhoodOption = document.createElement("option");
                neighborhoodOption.text = response.data.orderDeliveryAddress.neighborhood.name;
                neighborhoodOption.value = response.data.orderDeliveryAddress.neighborhood.id;

                neighborhoodElement.appendChild(neighborhoodOption);

                $('#neighborhood-id').val(response.data.orderDeliveryAddress.neighborhoodId);

                /* Invoice Information */
                $('[name="orderInvoiceInformation[firstName]"]').val(response.data.orderInvoiceInformation.firstName);
                $('[name="orderInvoiceInformation[lastName]"]').val(response.data.orderInvoiceInformation.lastName);
                $('[name="orderInvoiceInformation[phone]"]').val(response.data.orderInvoiceInformation.phone);
                $('[name="orderInvoiceInformation[mail]"]').val(response.data.orderInvoiceInformation.mail);
                $('[name="orderInvoiceInformation[taxOffice]"]').val(response.data.orderInvoiceInformation.taxOffice);
                $('[name="orderInvoiceInformation[taxNumber]"]').val(response.data.orderInvoiceInformation.taxNumber);
                $('[name="orderInvoiceInformation[address]"]').val(response.data.orderInvoiceInformation.address);
                $('#invoice-country-id').val(response.data.orderDeliveryAddress.countryId);

                let invoiceCityElement = document.getElementById("invoice-city-id");
                while (invoiceCityElement.children.length > 1) {
                    invoiceCityElement.children[1].remove();
                }

                var invoiceCityOption = document.createElement("option");
                invoiceCityOption.text = response.data.orderInvoiceInformation.neighborhood.district.city.name;
                invoiceCityOption.value = response.data.orderInvoiceInformation.neighborhood.district.city.id;

                invoiceCityElement.appendChild(invoiceCityOption);

                $('#invoice-city-id').val(response.data.orderDeliveryAddress.cityId);

                let invoiceDistrictElement = document.getElementById("invoice-district-id");
                while (invoiceDistrictElement.children.length > 1) {
                    invoiceDistrictElement.children[1].remove();
                }

                var invoiceDistrictOption = document.createElement("option");
                invoiceDistrictOption.text = response.data.orderInvoiceInformation.neighborhood.district.name;
                invoiceDistrictOption.value = response.data.orderInvoiceInformation.neighborhood.district.id;

                invoiceDistrictElement.appendChild(invoiceDistrictOption);

                $('#invoice-district-id').val(response.data.orderDeliveryAddress.districtId);

                let invoiceNeighborhoodElement = document.getElementById("invoice-neighborhood-id");
                while (invoiceNeighborhoodElement.children.length > 1) {
                    invoiceNeighborhoodElement.children[1].remove();
                }

                var invoiceNeighborhoodOption = document.createElement("option");
                invoiceNeighborhoodOption.text = response.data.orderInvoiceInformation.neighborhood.name;
                invoiceNeighborhoodOption.value = response.data.orderInvoiceInformation.neighborhood.id;

                invoiceNeighborhoodElement.appendChild(invoiceNeighborhoodOption);

                $('#invoice-neighborhood-id').val(response.data.orderDeliveryAddress.neighborhoodId);

                $('#order_create_form .kt-select2').select2({
                    placeholder: "Seçiniz"
                });

            }
        });
    },
    OpenUpdateModal: function (id) {
        OrderIndex.SetModalSize(ModalSize.Larger);

        HttpClient.Get(`/Order/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html(`${id} Nolu Sipariş Detayı`);

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            OrderIndex.ModalValidation("order_update_form", function () {
                var data = Serializer.Serialize("order_update_form");

                HttpClient.Post(
                    "/Order/Update",
                    data,
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#order_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            OrderIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {
                $("#order_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        OrderIndex.SetModalSize(ModalSize.Larger);

        HttpClient.Get("/Order/Create", function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Sipariş");

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            OrderIndex.ModalValidation("order_create_form", function () {
                let data = Serializer.Serialize("order_create_form");

                if ($('#copy-delivery-info').prop('checked')) {
                    data.orderInvoiceInformation.firstName = data.orderDeliveryAddress.firstName;
                    data.orderInvoiceInformation.lastName = data.orderDeliveryAddress.lastName;
                    data.orderInvoiceInformation.phone = data.orderDeliveryAddress.phoneNumber;
                    data.orderInvoiceInformation.neighborhoodId = data.orderDeliveryAddress.neighborhoodId;
                    data.orderInvoiceInformation.address = data.orderDeliveryAddress.address;
                }

                HttpClient.Post(
                    "/Order/Create",
                    data,
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#order_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            OrderIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {
                $("#order_create_form").submit();
            });
        });
    },
    OpenReturnModal: function (id) {

        OrderIndex.SetModalSize(ModalSize.Medium);

        HttpClient.Get(`/Order/Return?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("İade İşlemleri");

            $('[type="checkbox"][name^="OrderReturnDetails["]').change(function () {

                var name = $(this).attr('name');
                name = name.indexOf('isLoss') != -1 ? name.replace('isLoss', 'isReturn') : name.replace('isReturn', 'isLoss');

                var checkbox = $(`[name="${name}"]`);
                if (checkbox.prop('checked')) {
                    checkbox.prop('checked', false);
                }

            });

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            OrderIndex.ModalValidation("order_return_form", function () {
                let data = Serializer.Serialize("order_return_form");

                HttpClient.Post(
                    "/Order/Return",
                    data,
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#order_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#order_return_form").submit();
            });
        });
    },
    OpenConvertModal: function () {
        OrderIndex.SetModalSize(ModalSize.Medium);

        HttpClient.Get(`/Order/Convert`, function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Sipariş Tedarik");

            Autocomplete.Init($("#productInformationId-autocomplete"), "/ProductInformation/Autocomplete", 5, null, (i) => {
                return { key: i.key, name: i.value.productInformationName, unitPrice: i.value.productInformationPriceses[0].unitPrice, photoUrl: i.value.productInformationPhotos[0].fileName }
            }, 'name', `<div><img width="50" src="{{photoUrl}}"/>{{name}}</div>`, (suggestion) => $("[name='productInformationId']").val(suggestion.key), (suggestion) => $("[name='productInformationId']").val(""));

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            OrderIndex.ModalValidation("order_convert_form", function () {
                HttpClient.Post(
                    "/Order/Convert",
                    Serializer.Serialize("order_convert_form"),
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#order_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#order_convert_form").submit();
            });
        });
    },
    GetCitys: function () {

        var element = document.getElementById("city-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var countryId = document.getElementById("country-id").value;

        HttpClient.Get("/City/Read?CountryId=" + countryId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    },
    GetDistricts: function () {

        var element = document.getElementById("district-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var cityId = document.getElementById("city-id").value;


        HttpClient.Get("/District/Read?CityId=" + cityId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });


    },
    GetNeighborhoods: function () {
        var element = document.getElementById("neighborhood-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var districtId = document.getElementById("district-id").value;


        HttpClient.Get("/Neighborhood/Read?DistrictId=" + districtId, function (response) {
            if (response.success) {


                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            }
            else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    },
    GetInvoiceCitys: function () {

        var element = document.getElementById("invoice-city-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var countryId = document.getElementById("invoice-country-id").value;

        HttpClient.Get("/City/Read?CountryId=" + countryId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    },
    GetInvoiceDistricts: function () {

        var element = document.getElementById("invoice-district-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var cityId = document.getElementById("invoice-city-id").value;


        HttpClient.Get("/District/Read?CityId=" + cityId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });


    },
    GetInvoiceNeighborhoods: function () {
        var element = document.getElementById("invoice-neighborhood-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var districtId = document.getElementById("invoice-district-id").value;


        HttpClient.Get("/Neighborhood/Read?DistrictId=" + districtId, function (response) {
            if (response.success) {


                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            }
            else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    },
    AutoComplateInitalize: function () {

        if (OrderIndex.OrderDetailType == 'Autocomplete') {
            var products = new Bloodhound({
                datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/ProductInformation/Autocomplete?take=10&q=%QUERY',
                    wildcard: '%QUERY',
                    filter: r => $.map(r.data, function (i) {
                        return {
                            key: i.key,
                            name: i.value.productInformationName,
                            unitPrice: i.value.productInformationPriceses[0].unitPrice,
                            photoUrl: i.value.productInformationPhotos[0].fileName
                        };
                    })
                }
            });

            products.initialize();

            $('[name="productId"]').typeahead(null, {
                displayKey: 'name',
                source: products.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="empty-message" style="padding: 10px 15px; text-align: center;">',
                        'Sonuç bulunamadı.',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile(`<div><img width="50" src="{{photoUrl}}"/>{{name}}</div>`)
                }
            });
            $('[name="productId"]').bind('typeahead:select', function (ev, suggestion) {
                OrderIndex.GetProductInformations(suggestion.key, suggestion.name, suggestion.photoUrl, suggestion.unitPrice)
            });
        }
        else {
            let $productId = $('[name="productId"]');
            $productId.keyup(function (e) {
                if (e.which != 13) {
                    return;
                }

                HttpClient.Get(`/Order/ProductInformationSelection?q=${$productId.val()}`, function (response) {
                    $("#product_information_selection_modal_body").html(response);
                    $("#product_information_selection_modal").modal();
                    $("#product_information_selection_modal_submit").unbind("click");
                    $("#product_information_selection_modal_title").html("Ürün Seçimi");
                });
            });

            $(".orderProductListBtn").click(function () {
                HttpClient.Get(`/Order/ProductInformationSelection?q=${$productId.val()}`, function (response) {
                    $("#product_information_selection_modal_body").html(response);
                    $("#product_information_selection_modal").modal();
                    $("#product_information_selection_modal_submit").unbind("click");
                    $("#product_information_selection_modal_title").html("Ürün Seçimi");
                });
            });
        }

    },
    GetPayments: function (id) {

        var paymentExsist = $(`#payment-tbody-${id}`).length > 0;

        if (paymentExsist) {

            return;
        }

        var data = OrderIndex.ParsePayments();

        data.push({
            id: 0,
            paymentTypeId: id,
            amount: 0,
            paid: false,
            PaymentType: {
                paymentTypeName: $('#paymentType option:selected').text()
            }
        });



        OrderIndex.RefleshPayments(data);

    },
    DeleteGetPayments: function (id, value) {


        SwalAlertModal.ConfirmPopup('Sipariş tipini sil', `${value} sipariş tipini silmek istiyor musunuz?`, 'Evet', true, 'Hayır',
            function () {

                $(`#payment-tbody-${id}`).remove();
                var data = OrderIndex.ParsePayments();
                OrderIndex.RefleshPayments(data);
            }
        )




    },
    ParsePayments: function () {
        return $.map($('#payment-tbody tr'), function (value) {
            var index = $(value).data("payment-index");
            return {
                id: $(`[name='payments[${index}][id]']`).val(),
                paymentTypeId: $(`[name='payments[${index}][paymentTypeId]']`).val(),
                amount: $(`[name='payments[${index}][amount]']`).val(),
                paid: $(`[name='payments[${index}][paid]:boolean']`).prop('checked'),
                PaymentType: {
                    paymentTypeName: $(value).data("payment-name")
                }
            }
        });
    },
    RefleshPayments: function (data) {
        HttpClient.Post(
            "/Payment/IndexPartial",
            data,
            function (response) {

                $("#payment-tbody").html(response);
            });
    },
    GetProductInformations: function (id, value, photoUrl, unitPrice) {
        var orderDetailExsist = $(`#order-detail-${id}`).length > 0;

        if (orderDetailExsist) {
            var orderDetailQuantity = parseInt($(`#order-detail-quantity-${id}`).val()) + 1;
            $(`#order-detail-quantity-${id}`).val(orderDetailQuantity);
            OrderIndex.CalculateOrderDetails();
            return;
        }

        var data = OrderIndex.ParseOrderDetails();

        data.push({
            id: 0,
            productInformationId: parseInt(id),
            quantity: 1,
            unitPrice: unitPrice,
            productInformation: {
                productInformationName: value,
                productInformationPhotos: [{ fileName: photoUrl }]
            }
        });

        OrderIndex.RefleshOrderDetails(data);
    },
    DeleteProductInformations: function (id, value) {


        SwalAlertModal.ConfirmPopup('Ürünü listeden çıkart', `${value} ürününü listeden çıkarmak istiyor musunuz?`, 'Evet', true, 'Hayır',
            function () {
                $(`#order-detail-${id}`).remove();
                var data = OrderIndex.ParseOrderDetails();
                OrderIndex.RefleshOrderDetails(data);
            }
        )
    },
    ParseOrderDetails: function () {
        return $.map($('#order-detail-tbody tr'), function (value) {

            var index = $(value).data("order-detail-index");

            return {
                id: $(`[name='orderDetails[${index}][id]']`).val(),
                productInformationId: $(`[name='orderDetails[${index}][productInformationId]']`).val(),
                quantity: $(`[name='orderDetails[${index}][quantity]']`).val(),
                unitPrice: $(`[name='orderDetails[${index}][unitPrice]']`).val(),
                productInformation: {
                    productInformationName: $(value).data("order-detail-name"),
                    productInformationPhotos: [{ fileName: $(`[name='orderDetails[${index}][id]']`).parent().parent().find('img').prop('src') }]
                }
            }
        });
    },
    RefleshOrderDetails: function (data) {
        HttpClient.Post(
            "/OrderDetail/IndexPartial",
            data,
            function (response) {
                $("#order-detail-tbody").html(response);
                OrderIndex.CalculateOrderDetails();
            });
    },
    CalculateOrderDetails: function () {
        let total = 0;
        $("#order-detail-tbody tr").each((index, value) => {
            total += $(value).find('[name$="quantity]"]').eq(0).val() * $(value).find('[name$="rice]"]').eq(0).val();
        });
        $('[name=totalAmount]').val(total.toFixed(2));
        $("#order-detail-total").html(`Ürün toplamı: ${total.toFixed(2)} TL`);
    },
    Download: function () {
        window.open(`/Order/Download?Search=` + `Id:${$("#ajax_data_search_id").val()}_Name:${$("#ajax_data_search_name").val()}_Surname:${$("#ajax_data_search_surname").val()}_Marketplace:${$("#ajax_data_search_marketplace").val()}_OrderType:${$("#ajax_data_search_order_type").val()}_MarketplaceOrderNumber:${$("#ajax_data_search_marketplace_order_number").val()}_CargoTrackingNumber:${$("#ajax_data_search_cargo_tracking_number").val()}_OrderSource:${$("#ajax_data_search_order_source").val()}_PhoneNumber:${$("#ajax_data_search_phone_number").val()}_Date:${$("#ajax_data_search_date").val()}`)
    },
    Print: function () {
        if ($('.kt-checkbox [type="checkbox"]:checked').length == 0) {
            Notify.Show(NotifyType.Error, "Lütfen en az 1 sipariş seçiniz.");
        }
        else {


            var ids = $('.kt-checkbox [type="checkbox"]:checked').map((i, e) => e.value).get();
            for (var i = 0; i < ids.length; i++) {
                if ($(`#${ids[i]}-tracking-code`).html() == '') {
                    Notify.Show(NotifyType.Error, `${ids[i]} numaralı siparişe ait kargo takip kodu olmadığından yazdırma işlemi yapılamaz.`);
                    return;
                }
            }



            window.open(`/Order/Print?Ids=` + ids.join(","));


            window.location.reload();

        }
    },
    MakeShipped: function () {
        if ($('.kt-checkbox [type="checkbox"]:checked').length == 0) {
            Notify.Show(NotifyType.Error, "Lütfen en az 1 sipariş seçiniz.");
        }
        else {
            let data = [];
            $('.kt-checkbox [type="checkbox"]:checked').each((i, v) => {
                data.push($(v).val());
            });

            HttpClient.Post("/Order/MakeShipped", data, function (response) {
                if (response.success) {
                    Notify.Show(NotifyType.Success, response.message);

                    OrderIndex.Read(0);
                }
            });

        }
    },
    CopyDeliveryInfoToInvoiceInfo: function () {
        if ($('#copy-delivery-info').prop('checked')) {
            $('#invoice-form').find('[name$="[firstName]"],[name$="[lastName]"],[name$="[phone]"],#invoice-country-id,#invoice-city-id,#invoice-district-id,#invoice-neighborhood-id,#invoice-address').val('').prop('required', false).prop('disabled', true);
        }
        else {
            $('#invoice-form').find('[name$="[firstName]"],[name$="[lastName]"],[name$="[phone]"],#invoice-country-id,#invoice-city-id,#invoice-district-id,#invoice-neighborhood-id,#invoice-address').val('').prop('required', true).prop('disabled', false);
        }
    },
    SetModalSize: function (modalSize) {
        var $modalDialog = $(".modal-dialog").eq(0);

        if ($modalDialog.hasClass(`modal-${ModalSize.Small}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Small}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Medium}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Medium}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Large}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Large}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Larger}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Larger}`);

        $modalDialog.addClass(`modal-${modalSize}`);
    },
    OpenChangeCargoCompanyModal: function (marketplaceId, packageNumber, marketplaceOrderNumber) {
        OrderIndex.SetModalSize(ModalSize.Small);

        HttpClient.Get(`/MarketplaceOperation/SelectionPartial?MarketplaceId=${marketplaceId}&PackageNumber=${packageNumber}`, function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Pazaryeri Kargo Seçimi");

            $('.kt-select2').select2();

            $("#modal_submit").click(function () {

                HttpClient.Post(
                    "/MarketplaceOperation/Change",
                    {
                        marketplaceId: marketplaceId,
                        packageNumber: packageNumber,
                        cargoCompanyId: $('[name="change-cargo-company-id"]').val(),
                        marketplaceOrderNumber: marketplaceOrderNumber
                    },
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#order_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            });
        });
    },
    Unpack: function (marketplaceId, packageNumber, marketplaceOrderNumber) {

        HttpClient.Post(
            "/MarketplaceOperation/Unpack",
            {
                marketplaceId: marketplaceId,
                packageNumber: packageNumber,
                marketplaceOrderNumber: marketplaceOrderNumber,
                cargoCompanyId: ''
            },
            function (response) {
                if (response.success) {
                    OrderIndex.Read(0);

                    Notify.Show(NotifyType.Success, response.message);

                    $("#order_modal").modal("hide");
                    $("#modal_body").html('');
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            }
        );
    },
    OpenDeletePickingModal: function (id) {

        swal.fire({
            title: 'Sipariş paketlemesi silinsin mi?',
            text: "Sipariş paketlemesini sistemden silmek istediğinize emin misiniz ?",
            type: 'warning',
            confirmButtonText: 'Paketi Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            if (result.value) {
                HttpClient.Post(
                    "/Order/DeletePicking",
                    {
                        id: id
                    },
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);
                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            }
        });

    },
    OpenDeleteInvoiceModal: function (id) {

        swal.fire({
            title: 'Sipariş faturası silinsin mi?',
            text: "Sipariş faturasını sistemden silmek istediğinize emin misiniz ?",
            type: 'warning',
            confirmButtonText: 'Fatura Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            if (result.value) {
                HttpClient.Post(
                    "/Order/DeleteInvoice",
                    {
                        id: id
                    },
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);
                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            }
        });

    },
    OpenDeleteRetunInvoiceModal: function (id) {

        swal.fire({
            title: 'Siparişin gider pusulası silinsin mi?',
            text: "Siparişin gider pusulası sistemden silmek istediğinize emin misiniz ?",
            type: 'warning',
            confirmButtonText: 'Gider Pusulası Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            if (result.value) {
                HttpClient.Post(
                    "/Order/DeleteReturnInvoice",
                    {
                        id: id
                    },
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);
                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            }
        });

    },
    EmptyPackerBarcode: function (id) {

        swal.fire({
            title: 'Sepet silinsin mi?',
            text: "Sepeti silmek istediğinize emin misiniz ?",
            type: 'warning',
            confirmButtonText: 'Sepeti Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            if (result.value) {
                HttpClient.Post(
                    "/Order/EmptyPackerBarcode",
                    {
                        id: id
                    },
                    function (response) {
                        if (response.success) {
                            OrderIndex.Read(0);
                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );

            }
        });

    }
}

$(document).ready(() => {
    OrderIndex.Initialize();
});