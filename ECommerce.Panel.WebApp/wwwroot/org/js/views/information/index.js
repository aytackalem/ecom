﻿const InformationIndex = {
    Initialize: function () {
        InformationIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Information/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{
                Name: "id",
                Type: "number"
            },
            {
                Name: "",
                Type: "string",
                Render: function (value) {
                    return value.informationTranslations[0].header;
                }
            },
            {
                Name: "active",
                Type: "bool"
            }],
            "Information",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var information = element.closest('.input-information');
                if (information.length) {
                    information.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Information/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#information_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Haber Güncelle");
            $('.summernote').summernote({
                height: 150
            });

            InformationIndex.ModalValidation("information_update_form", function () {
                const information = Serializer.Serialize("information_update_form");

                HttpClient.Post(
                    "/Information/Update",
                    information,
                    function (response) {
                        if (response.success) {
                            InformationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#information_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#information_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Information/Create", function (response) {
            $("#modal_body").html(response);
            $("#information_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Haber");
            $('.summernote').summernote({
                height: 150
            });

            InformationIndex.ModalValidation("information_create_form", function () {
                const information = Serializer.Serialize("information_create_form");

                HttpClient.Post(
                    "/Information/Create",
                    information,
                    function (response) {
                        if (response.success) {
                            InformationIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#information_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#information_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        InformationIndex.Read(0);
    },
};

$(document).ready(() => {
    InformationIndex.Initialize(0);
});