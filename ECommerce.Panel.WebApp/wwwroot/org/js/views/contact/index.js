﻿"use strict";

const ContactIndex = {
    Initialize: function () {
        ContactIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Contact/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" },
            { Name: "active", Type: "bool" }],
            "Contact",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var contact = element.closest('.input-contact');
                if (contact.length) {
                    contact.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Contact/Update?Id=${id}`, function (response) {
            debugger
            $("#modal_body").html(response);
            $("#contact_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("İletişim Güncelle");
            debugger
            ContactIndex.ModalValidation("contact_update_form", function () {
                const contact = Serializer.Serialize("contact_update_form");
                debugger
                HttpClient.Post(
                    "/Contact/Update",
                    contact,
                    function (response) {
                        if (response.success) {
                            ContactIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#contact_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#contact_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Contact/Create", function (response) {
            $("#modal_body").html(response);
            $("#contact_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni İletişim");

            ContactIndex.ModalValidation("contact_create_form", function () {
                const contact = Serializer.Serialize("contact_create_form");

                HttpClient.Post(
                    "/Contact/Create",
                    contact,
                    function (response) {
                        if (response.success) {
                            ContactIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#contact_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#contact_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ContactIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ContactIndex.Read(0);
    },
};

$(document).ready(() => {
    ContactIndex.Initialize(0);
});