﻿"use strict";

function BrandDataTable() { };

let _brandDataTable;

const BrandIndex = (function () {

    return {
        Initialize: function () {

            BrandDataTable.prototype = new DataTable({
                BaseUrl: '/Brand',
                Name: 'Marka',
                Pagination: {
                    Click: 'javascript:_brandDataTable.SetPage(#index);_brandDataTable.GetData()'
                },
                DataTableRow: new DataTableRow({
                    DataTableColumns: [
                        new DataTableColumn({
                            Name: 'id',
                            Type: DataTableColumnTypes.Int
                        }),
                        new DataTableColumn({
                            Name: 'name',
                            Type: DataTableColumnTypes.String
                        }),
                        new DataTableColumn({
                            Name: 'active',
                            Type: DataTableColumnTypes.Bool
                        })],
                    DataTableRowMenu: new DataTableRowMenu({
                        Items: [{
                            Icon: 'la la-edit',
                            Name: 'Güncelle',
                            Click: 'javascript:_brandDataTable.OpenUpdateModal(#id)'
                        }, {
                            Icon: 'la la-edit',
                            Name: 'Pazaryeri Ayarları',
                            Click: 'javascript:_brandDataTable.OpenMarketplaceConfigurationModal(#id)'
                        }]
                    })
                })
            });
            _brandDataTable = new BrandDataTable();
            _brandDataTable.GetData();
            _brandDataTable.OpenCreateModal = (id) => {
                var modal = new Modal();
                modal.Open({
                    Title: 'Yeni Marka',
                    ButtonText: 'Kaydet',
                    GetUrl: `/Brand/Create`,
                    PostUrl: '/Brand/Create',
                    Validation: true,
                    Closed: function () {
                        _brandDataTable.GetData();
                    }
                });
            };
            _brandDataTable.OpenUpdateModal = (id) => {
                var modal = new Modal();
                modal.Open({
                    Title: 'Marka Güncelle',
                    ButtonText: 'Güncelle',
                    GetUrl: `/Brand/Update?Id=${id}`,
                    PostUrl: '/Brand/Update',
                    Validation: true
                });
            };
            _brandDataTable.OpenMarketplaceConfigurationModal = (id) => {
                var modal = new Modal();
                modal.Open({
                    Title: 'Marka Pazaryeri Ayarları Güncelle',
                    ButtonText: 'Güncelle',
                    GetUrl: `/MarketplaceBrandMapping/IndexPartial?BrandId=${id}`,
                    PostUrl: '/MarketplaceBrandMapping/Upsert',
                    Validation: true,
                    Opened: function () {
                        $("[type='text'][data-brand-market-place-id]").each((i, v) => {
                            let $v = $(v);
                            let marketplaceId = $v.data("brand-market-place-id");
                            let isECommerce = $v.data("is-ecommerce");

                            Autocomplete.InitRemote(
                                $v,
                                isECommerce ? '/ECommerceBrand/SearchByName' : 'https://marketplacecatalog.helpy.com.tr/Brand/SearchByName',
                                [{ key: 'marketplaceId', value: marketplaceId }],
                                (i) => { return { BrandCode: i.C, BrandName: i.N, CompanyId: i.CI } },
                                'BrandName',
                                null,
                                (suggestion) => {
                                    $(`#marketplaceBrandCode_${marketplaceId}`).val(suggestion.BrandCode);
                                    $(`#marketplaceBrandName_${marketplaceId}`).val(suggestion.BrandName);
                                },
                                () => {
                                    $(`#marketplaceBrandCode_${marketplaceId}`).closest(".typeahead").find("[type='text']").val("");
                                    $(`#marketplaceBrandCode_${marketplaceId}`).val("");
                                    $(`#marketplaceBrandName_${marketplaceId}`).val("");
                                },
                                () => {
                                    if ($(`#marketplaceBrandCode_${marketplaceId}`).val() == '') {

                                        $(`#marketplaceBrandCode_${marketplaceId}`).closest(".typeahead").find("[type='text']").val("");
                                        $(`#marketplaceBrandCode_${marketplaceId}`).val("");
                                        $(`#marketplaceBrandName_${marketplaceId}`).val("");

                                        Notify.Show(NotifyType.Error, "Lütfen tüm markaları doldurunuz.");
                                    }
                                });

                        });
                    }
                });
            };

            $('.kt-portlet__head-wrapper').append(`<a href="javascript:_brandDataTable.OpenCreateModal()" class="btn btn-brand btn-elevate btn-icon-sm">
                    <i class="la la-plus"></i>
                    Yeni Marka
                </a>`);

        }
    }

})();

$(document).ready(() => {
    BrandIndex.Initialize();
});