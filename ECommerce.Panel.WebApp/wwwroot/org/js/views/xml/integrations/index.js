﻿const IntegrationsIndex = {
    Initialize: function () {
        IntegrationsIndex.Read(0);

        $(document).on('show.bs.modal', '.modal', function () {
            const zIndex = 1040 + 10 * $('.modal:visible').length;
            $(this).css('z-index', zIndex);
            setTimeout(() => $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack'));
        });

    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Xml/Integrations/Listing",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val()
            },
            [
                { Name: "id", Type: "number" },
                { Name: "name", Type: "string" }
            ],
            "Integrations",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Xml/Integrations/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#integration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Xml Tanımı Güncelle");

            $('.select-picker').selectpicker();

            IntegrationsIndex.ModalValidation("integration_update_form", function () {
                HttpClient.Post(
                    "/Xml/Integrations/Update",
                    Serializer.Serialize("integration_update_form"),
                    function (response) {
                        if (response.success) {
                            IntegrationsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#integration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#integration_update_form").submit();
            });
        });
    },
    Preview: function () {
        HttpClient.Post('/Xml/Integrations/Preview', {
            url: $('#xml-url').val()
        }, function (response) {
            if (response.success) {

                $('#xml-preview').html(response.data);

            }
        });
    },
    Resolve: function () {
        HttpClient.Post('/Xml/Integrations/Resolve', {
            url: $('#xml-url').val(),
            productElementName: $('#product-element-name').val()
        }, function (response) {
            if (response.success) {

                const elementNames = response.data.elementNames;

                $('select.element-names').find('option').remove();

                for (var i = 0; i < elementNames.length; i++) {

                    $('select.element-names').append($('<option>', { value: elementNames[i], text: elementNames[i] }));

                }

                $('.element-names').selectpicker('refresh');

            }
        });
    },
    Test: function () {

        let mappings = [];
        debugger
        mappings.push({
            $type: "single",
            sourceName: $('#product-name').val(),
            targetName: 'Name'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#brand-name').val(),
            targetName: 'BrandName'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#category-name').val(),
            targetName: 'CategoryName'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#barcode').val(),
            targetName: 'Barcode'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#stock').val(),
            targetName: 'Stock'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#list-unit-price').val(),
            targetName: 'ListUnitPrice'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#unit-price').val(),
            targetName: 'UnitPrice'
        });

        //mappings.push({
        //    sourceName: '',
        //    targetName: 'VatRate'
        //});

        mappings.push({
            $type: "single",
            sourceName: $('#description').val(),
            targetName: 'Description'
        });

        mappings.push({
            $type: "single",
            sourceName: $('#photo').val(),
            targetName: 'Photos'
        });

        mappings.push({
            $type: "parent",
            sourceName: $('#variant').val(),
            targetName: 'Variants',
            singleMappings: [
                {
                    $type: "single",
                    sourceName: $('#variant-name').val(),
                    targetName: 'Name'
                },
                {
                    $type: "single",
                    sourceName: $('#variant-value').val(),
                    targetName: 'Value'
                }
            ]
        });

        HttpClient.Post('/Xml/Integrations/Test', {
            url: $('#xml-url').val(),
            productElementName: $('#product-element-name').val(),
            mappings: mappings
        }, function (response) {

            $("#test_modal_body").html(response);
            $("#test_modal").modal();

            var carousel2 = $('#kt_earnings_widget .kt-widget30__body .owl-carousel');

            carousel2.owlCarousel({
                rtl: KTUtil.isRTL(),
                items: 1,
                animateIn: 'fadeIn(100)',
                loop: true
            });

        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Xml/Integrations/Create", function (response) {
            $("#modal_body").html(response);
            $("#integration_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Xml Tanımı");

            $('.element-names').selectpicker();











            IntegrationsIndex.ModalValidation("integration_create_form", function () {
                HttpClient.Post(
                    "/Xml/Integrations/Create",
                    Serializer.Serialize("integration_create_form"),
                    function (response) {

                        if (response.success) {
                            IntegrationsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#integration_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#integration_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        IntegrationsIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        IntegrationsIndex.Read(0);
    },
};

$(document).ready(() => {
    IntegrationsIndex.Initialize();
});