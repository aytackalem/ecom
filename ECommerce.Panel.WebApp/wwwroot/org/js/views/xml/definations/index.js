﻿const DefinationsIndex = {
    Initialize: function () {
        DefinationsIndex.Read(0);
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Xml/Definations/Listing",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val()
            },
            [
                { Name: "id", Type: "number" },
                { Name: "name", Type: "string" },
                { Name: "fileName", Type: "string" },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {

                        return `<a href="https://xml.helpy.tr/${value.fileName}" target="_blank" class="btn btn-primary m-2">
                                 <i class="la la-link"></i> Göster
                                </a>`;
                    }
                },

            ],
            "Definations",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Xml/Definations/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#defination_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Xml Tanımı Güncelle");

            $('.select-picker').selectpicker();

            DefinationsIndex.ModalValidation("defination_update_form", function () {
                HttpClient.Post(
                    "/Xml/Definations/Update",
                    Serializer.Serialize("defination_update_form"),
                    function (response) {
                        if (response.success) {
                            DefinationsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#defination_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#defination_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Xml/Definations/Create", function (response) {
            $("#modal_body").html(response);
            $("#defination_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Xml Tanımı");

            $('.select-picker').selectpicker();

            DefinationsIndex.ModalValidation("defination_create_form", function () {
                HttpClient.Post(
                    "/Xml/Definations/Create",
                    Serializer.Serialize("defination_create_form"),
                    function (response) {

                        if (response.success) {
                            DefinationsIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#defination_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#defination_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        DefinationsIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        DefinationsIndex.Read(0);
    },
};

$(document).ready(() => {
    DefinationsIndex.Initialize();
});