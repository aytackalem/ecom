﻿const ModalSize = {
    Small: 'sm',
    Medium: 'md',
    Large: 'lg',
    Larger: 'xl'
};

const WholesaleOrderIndex = {
    Initialize: function () {

        WholesaleOrderIndex.Read();

        /* Wholesale Order Types */
        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            WholesaleOrderIndex.Read(0);
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/WholesaleOrder/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `WholesaleOrderType:${$("#ajax_data_search_wholesale_order_type").val()}`
            },
            [{
                Name: "id",
                Type: "number"
            },
            {
                Name: "",
                Type: "number",
                Render: function (value) {
                    return value.isExport ? "Evet" : "Hayır";
                }
            },
            {
                Name: "",
                Type: "string",
                Padding: 0,
                Render: function (value) {

                    return `
<table class="table" style="margin: 0;">
    <tbody>
${value.wholesaleOrderDetails.map((v, i) => `
        <tr>
                    <td style="font-weight: 500;${i == 0 ? ` border-top: none;` : ''}" width="20%">
                <div>${v.quantity} Adet</div>
            </td>
            <td${i == 0 ? ` style="border-top: none;"` : ''}  width="30%">
                <a href="${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}" target="_blank">
                    <img class="table-product-img" src="https://imgr.helpy.com.tr/img/${(v.productInformation.productInformationPhotos == undefined ? 'https://erp.sinoz.com.tr/img/product/95/95.png' : v.productInformation.productInformationPhotos[0].fileName)}/resize?size=150"/>
                </a>
            </td>
            <td${i == 0 ? ` style="border-top: none;"` : ''} width="45%">
                <div>${v.productInformation.productInformationName}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.stockCode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.stockCode}</b> kopyalandı.');">${v.productInformation.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${v.productInformation.barcode}');Notify.Show(NotifyType.Info, '<b>${v.productInformation.barcode}</b> kopyalandı.');">${v.productInformation.barcode} <i class="la la-copy"></i> </a>
                </div>
                            
            </td>
        </tr>
`).join('')}
    </tbody>
</table>`;
                }
            },
            {
                Name: "",
                Type: "string",
                Render: function (value) {
                    var html = `
<div class="kt-list-timeline">
    <div class="kt-list-timeline__items">

        <div class="kt-list-timeline__item">
            <span class="kt-list-timeline__badge kt-list-timeline__badge--danger kt-font-danger"></span>
            <span class="kt-list-timeline__text ">Oluşturuldu</span>
            <span class="kt-list-timeline__time " style="width:60%">${value.createdDateStr}</span>
        </div>
    </div>
</div>`;
                    return html;
                }
            },
            {
                Name: "",
                Type: "string",
                Render: function (value) {
                    return `<div>${value.wholesaleOrderType.name}</div>`;
                }
            }],
            "WholesaleOrder",
            [],
            true
        );
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        WholesaleOrderIndex.Read(0);
    },
    OpenUpdateModal: function (id) {
        WholesaleOrderIndex.SetModalSize(ModalSize.Larger);

        HttpClient.Get(`/WholesaleOrder/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#order_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html(`${id} Nolu Sipariş Detayı`);
            $('.kt-selectpicker').selectpicker();
        });
    },
    SetModalSize: function (modalSize) {
        var $modalDialog = $(".modal-dialog").eq(0);

        if ($modalDialog.hasClass(`modal-${ModalSize.Small}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Small}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Medium}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Medium}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Large}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Large}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Larger}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Larger}`);

        $modalDialog.addClass(`modal-${modalSize}`);
    },
    Update: function (id) {
        HttpClient.Post(`/WholesaleOrder/Update`, { Id: id, WholesaleOrderTypeId: $('#orderTypeId').val() }, function (response) {
            if (response.success) {
                WholesaleOrderIndex.Read(0);
                $("#order_modal").modal("hide");
                $("#modal_body").html('');
                Notify.Show(NotifyType.Success, "İşlem başarılı.");
            }
        });
    }
}

$(document).ready(() => {
    WholesaleOrderIndex.Initialize();
});