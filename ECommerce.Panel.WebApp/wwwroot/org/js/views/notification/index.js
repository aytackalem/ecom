﻿const NotificationIndex = {
    Initialize: function () {
        NotificationIndex.Read();

        $('.kt-select2').select2({
            placeholder: "Seçiniz",
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {

                if (data.element && data.element.hasAttribute("data-html"))
                    return `<div style="text-align: center;">${data.element.getAttribute("data-html")}</div>`;
                else
                    return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });

        /* Order Types */
        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            NotificationIndex.Read(0);
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Notification/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: `Name:${$("#ajax_data_search_name").val()}_StockCode:${$("#ajax_data_search_stock_code").val()}_Barcode:${$("#ajax_data_search_barcode").val()}_ModelCode:${$("#ajax_data_search_model_code").val()}_MarketplaceId:${$("#ajax_data_search_marketplace").val()}_Type:${$("#ajax_data_search_type").val()}`
            },
            [
                {
                    Render: function (value) {
                        
                        var html = `<div style="text-align: center;"><img width="64" src="/org/img/marketplace-icons/${value.marketplaceId}.png"/>`;
                        return html;

                    },
                },   
                {
                    Name: "title",
                    Type: "string"
                },
                {
                    Render: function (value) {
                        
                        var html = `
<table class="table" style="margin: 0;">
    <tbody>
    <tr>
            <td  width="30%">
                <img class="table-product-img" height="100"  src="${value.photoUrl}"/>
            </td>
            <td width="50%">
                <div>${value.productInformationName}</div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.stockCode}');Notify.Show(NotifyType.Info, '<b>${value.stockCode}</b> kopyalandı.');">${value.stockCode} <i class="la la-copy"></i> </a>
                </div>
                <div style="margin-top: 5px">
                    <a style="font-weight: 500" href="javascript:navigator.clipboard.writeText('${value.barcode}');Notify.Show(NotifyType.Info, '<b>${value.barcode}</b> kopyalandı.');">${value.barcode} <i class="la la-copy"></i> </a>
                </div>
            </td>
</tr>
    </tbody>
</table>
`;
                        return html;

                    },
                },
                {
                    Name: "message",
                    Type: "string"
                }, {
                    Name: "",
                    Type: "string",
                    Render: (value, parentValue) => value.createdDateStr,
                    Unified: true
                }
            ],
            "Notification",
            [],
            false
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        NotificationIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        NotificationIndex.Read(0);
    },

};


$(document).ready(() => {
    NotificationIndex.Initialize(0);
});