﻿const ModalSize = {
    Small: 'sm',
    Medium: 'md',
    Large: 'lg',
    Larger: 'xl'
};

const StocktakingIndex = {
    Initialize: function () {

        StocktakingIndex.Read();

        /* Stocktaking Types */
        $('.ot-selectpicker').selectpicker();
        $('.ot-selectpicker').on('hidden.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            StocktakingIndex.Read(0);
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Stocktaking/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length :selected").val(),
                Sort: $(".ajax_data_sorting :selected").val(),
                Search: `StocktakingTypeId:${$("#ajax_data_search_stocktaking_type_id").val()}`
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "name",
                    Type: "string"
                },
                {
                    Name: "",
                    Type: "string",
                    Render: function (value) {
                        return value.createdDateTimeStr;
                    }
                }
            ],
            "Stocktaking",
            [
                {
                    Render: function (value) {
                        if (value.stocktakingTypeId == "BE")
                            return `<a class="dropdown-item" href="javascript:StocktakingIndex.Delete(${value["id"]})"><i class="la la-exclamation"></i>
                            Stok Sayımı Sil
                        </a>`;
                    }
                }
            ],
            true
        );
    },
    Delete: function (id) {
        SwalAlertModal.ConfirmPopup('Stok Sayımı', `Stok sayımı silinsin mi?`, 'Evet', true, 'Hayır',
            function () {

                HttpClient.Post(
                    "/Stocktaking/Delete",
                    {
                        id: parseInt(id)
                    },
                    function (response) {

                        Notify.Show(response.success ? NotifyType.Success : NotifyType.Error, response.message);

                    }
                )
            }
        )
    },
    OpenUpdateModal: function (id) {
        StocktakingIndex.SetModalSize(ModalSize.Larger);

        HttpClient.Get(`/Stocktaking/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#stocktaking_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html(`${id} Nolu Stok Sayım Detayı`);

            $('.kt-select2').selectpicker();

            var $approve = $('#stocktaking_approve');
            if ($approve.length) {
                $approve.click(function () {

                    SwalAlertModal.ConfirmPopup('Stok Sayımı', `Stok sayımı onaylansın mı?`, 'Evet', true, 'Hayır',
                        function () {

                            HttpClient.Post(
                                "/Stocktaking/Approve",
                                {
                                    id: parseInt(id)
                                },
                                function (response) {

                                    Notify.Show(response.success ? NotifyType.Success : NotifyType.Error, response.message);
                                    $("[name='stocktakingTypeId']").append("<option value='ON'>Onaylanmış</option>");
                                    $("[name='stocktakingTypeId']").val("ON");
                                    $('.kt-select2').selectpicker();
                                    $("[name='stocktakingTypeId']").prop("disabled", true);
                                    $('.kt-select2').selectpicker("refresh");
                                    $approve.remove();
                                }
                            )
                        }
                    )

                });
            }

            StocktakingIndex.ModalValidation("stocktaking_update_form", function () {
                var data = Serializer.Serialize("stocktaking_update_form");

                HttpClient.Post(
                    "/Stocktaking/Update",
                    data,
                    function (response) {
                        if (response.success) {
                            StocktakingIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#stocktaking_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#stocktaking_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        StocktakingIndex.SetModalSize(ModalSize.Medium);

        HttpClient.Get("/Stocktaking/Create", function (response) {
            $("#modal_body").html(response);
            $("#stocktaking_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Stok Sayım");

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            StocktakingIndex.ModalValidation("stocktaking_create_form", function () {
                let data = Serializer.Serialize("stocktaking_create_form");

                if ($('#copy-delivery-info').prop('checked')) {
                    data.orderInvoiceInformation.firstName = data.orderDeliveryAddress.firstName;
                    data.orderInvoiceInformation.lastName = data.orderDeliveryAddress.lastName;
                    data.orderInvoiceInformation.phone = data.orderDeliveryAddress.phoneNumber;
                    data.orderInvoiceInformation.neighborhoodId = data.orderDeliveryAddress.neighborhoodId;
                    data.orderInvoiceInformation.address = data.orderDeliveryAddress.address;
                }

                HttpClient.Post(
                    "/Stocktaking/Create",
                    data,
                    function (response) {
                        if (response.success) {
                            StocktakingIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#stocktaking_modal").modal("hide");
                            $("#modal_body").html('');
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#stocktaking_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        StocktakingIndex.Read(0);
    },
    SetModalSize: function (modalSize) {
        var $modalDialog = $(".modal-dialog").eq(0);

        if ($modalDialog.hasClass(`modal-${ModalSize.Small}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Small}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Medium}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Medium}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Large}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Large}`);

        if ($modalDialog.hasClass(`modal-${ModalSize.Larger}`) == true)
            $modalDialog.removeClass(`modal-${ModalSize.Larger}`);

        $modalDialog.addClass(`modal-${modalSize}`);
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    }
}

$(document).ready(() => {
    StocktakingIndex.Initialize();
});