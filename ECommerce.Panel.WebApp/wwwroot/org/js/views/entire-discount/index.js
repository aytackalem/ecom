﻿"use strict";

const EntireDiscountIndex = {
    Initialize: function () {
        EntireDiscountIndex.Read();

        $('.kt-selectpicker').selectpicker();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/EntireDiscount/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "startDateString",
                    Type: "string"
                },
                {
                    Name: "endDateString",
                    Type: "string"
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "EntireDiscount",
            [{
                Render: function (value) {

                    return `<a class="dropdown-item" href="javascript:EntireDiscountIndex.Delete({ id: ${value["id"]}})"><i class="la la-edit"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/EntireDiscount/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $('.kt-datetimepicker').datetimepicker({
                todayHighlight: true,
                format: 'dd/mm/yyyy',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                autoclose: true,
                language: 'tr'
            });
            $("#entire_discount_modal").modal();
            $('.kt-selectpicker').selectpicker();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Genel İndirim Güncelle");

            EntireDiscountIndex.ModalValidation("entire_discount_update_form", function () {
                HttpClient.Post(
                    "/EntireDiscount/Update",
                    Serializer.Serialize("entire_discount_update_form"),
                    function (response) {
                        if (response.success) {
                            EntireDiscountIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#entire_discount_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#entire_discount_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/EntireDiscount/Create", function (response) {
            $("#modal_body").html(response);
            $('.kt-datetimepicker').datetimepicker({
                todayHighlight: true,
                format: 'dd/mm/yyyy',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                autoclose: true,
                language: 'tr'
            });
            $("#entire_discount_modal").modal();
            $('.kt-selectpicker').selectpicker();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Genel İndirim");

            EntireDiscountIndex.ModalValidation("entire_discount_create_form", function () {
                let getXPayYSetting = Serializer.Serialize("entire_discount_create_form");
                
                HttpClient.Post(
                    "/EntireDiscount/Create",
                    getXPayYSetting,
                    function (response) {
                        if (response.success) {
                            EntireDiscountIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#entire_discount_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#entire_discount_create_form").submit();
            });
        });
    },
    Delete: function (entireDiscount) {


        SwalAlertModal.ConfirmPopup('Genel indirim', `${entireDiscount.id} nolu indirim silinsin mi?`, 'Evet', true, 'Hayır',
            function () {

                HttpClient.Post(
                    "/EntireDiscount/Delete",
                    entireDiscount,
                    function (response) {

                        if (response.success) {
                            EntireDiscountIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                )
            }
        )
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        EntireDiscountIndex.Read(0);
    },
};

$(document).ready(() => {
    EntireDiscountIndex.Initialize(0);
});