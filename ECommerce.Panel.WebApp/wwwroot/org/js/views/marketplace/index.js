﻿"use strict";

function MarketplaceDataTable() {
};

var _marketplaceDataTable;

const MarketplaceIndex = (function () {

    return {
        Initialize: function () {

            MarketplaceDataTable.prototype = new DataTable({
                BaseUrl: '/Marketplace',
                Name: 'Pazaryeri',
                Pagination: {
                    Click: 'javascript:_marketplaceDataTable.SetPage(#index);_marketplaceDataTable.GetData()'
                },
                DataTableRow: new DataTableRow({
                    DataTableColumns: [
                        new DataTableColumn({
                            Name: 'id',
                            Type: DataTableColumnTypes.Int
                        }),
                        new DataTableColumn({
                            Name: 'name',
                            Type: DataTableColumnTypes.String
                        }),
                        new DataTableColumn({
                            Name: 'active',
                            Type: DataTableColumnTypes.Bool
                        })],
                    DataTableRowMenu: new DataTableRowMenu({
                        Items: [{
                            Icon: 'la la-edit',
                            Name: 'Güncelle',
                            Click: 'javascript:_marketplaceDataTable.OpenUpdateModal(#id)'
                        }]
                    })
                })
            });
            _marketplaceDataTable = new MarketplaceDataTable();
            _marketplaceDataTable.GetData();
            _marketplaceDataTable.OpenUpdateModal = (id) => {
                var modal = new Modal();
                modal.Open({
                    Title: 'Pazaryeri Güncelle',
                    ButtonText: 'Güncelle',
                    GetUrl: `/Marketplace/Update?Id=${id}`,
                    PostUrl: '/Marketplace/Update',
                    Validation: true,
                    Closed: function () {
                        _marketplaceDataTable.GetData();
                    }
                });
            }

        }
    }

})();

$(document).ready(() => {
    MarketplaceIndex.Initialize();
});