﻿"use strict";

const FeedIndex = {
    Initialize: function () {

        $("body").on("click", ".tagify__tag__removeBtn", function () {
            $(this).parent().remove();
        });

        FeedIndex.Read(0);

    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Feed/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: `Name:${$("#ajax_data_search_name").val()}`
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "name",
                    Type: "string"
                },
                {
                    Name: "fileName",
                    Type: "string"
                }
            ],
            "Feed",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Feed/Update?Id=${id}`, function (response) {

            $("#modal_body").html(response);
            $("#feed_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Feed Detay");

            FeedIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {

                let valid = Validation.Valid("feed_update_form");
                if (valid) {

                    var data = Serializer.Serialize("feed_update_form");

                    HttpClient.Post(
                        "/Feed/Update",
                        data,
                        function (response) {
                            if (response.success) {

                                FeedIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#feed_modal").modal("hide");
                                $("#modal_body").html("");

                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );

                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");

            });

        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Feed/Create", function (response) {

            $("#modal_body").html(response);
            $("#feed_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Feed");

            FeedIndex.AutoComplateInitalize();

            $("#modal_submit").click(function () {

                let valid = Validation.Valid("feed_create_form");
                if (valid) {

                    var data = Serializer.Serialize("feed_create_form");

                    HttpClient.Post(
                        "/Feed/Create",
                        data,
                        function (response) {
                            if (response.success) {

                                FeedIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#feed_modal").modal("hide");
                                $("#modal_body").html("");

                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );

                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");

            });

        });
    },
    AutoComplateInitalize: function () {
        var products = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '/ProductInformation/Autocomplete?take=5&q=%QUERY',
                wildcard: '%QUERY',
                filter: r => $.map(r.data, function (i) {
                    return {
                        key: i.key,
                        name: i.value.productInformationName,
                        unitPrice: i.value.productInformationPriceses[0].unitPrice,
                        photoUrl: i.value.productInformationPhotos[0].fileName
                    };
                })
            }
        });

        products.initialize();

        $('#productId').typeahead(null, {
            displayKey: 'name',
            source: products.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message" style="padding: 10px 15px; text-align: center;">',
                    'Sonuç bulunamadı.',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(`<div><img width="50" src="{{photoUrl}}"/>{{name}} {{unitPrice}} TL</div>`)
            }
        });
        $('#productId').bind('typeahead:select', function (ev, suggestion) {

            var html = `
<tag contenteditable="false" spellcheck="false" class="tagify__tag " role="tag">
    <x title="" class="tagify__tag__removeBtn" role="button" aria-label="remove tag"></x>
    <div><span class="tagify__tag-text">${suggestion.name}</span></div>
    <input type="hidden" name="FeedDetails[][productInformationId]" value="${suggestion.key}"/>
</tag>`;

            $(".tagify--outside").eq(0).append(html);

        });
    }
};

$(document).ready(() => {
    FeedIndex.Initialize(0);
});

