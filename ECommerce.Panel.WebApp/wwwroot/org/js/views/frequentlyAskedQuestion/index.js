﻿

const FrequentlyAskedQuestionIndex = {
    Initialize: function () {
        FrequentlyAskedQuestionIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/FrequentlyAskedQuestion/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" }, {
                Name: "", Type: "string", Render: function (value) {
                    return value.frequentlyAskedQuestionTranslations[0].header;
                }
            },
            {
                Name: "active",
                Type: "bool"
            }],
            "FrequentlyAskedQuestion",
            [{
                Render: function (value) {

                    return `<a class="dropdown-item" href="javascript:FrequentlyAskedQuestionIndex.Delete({ id: ${value["id"]}},'${value.frequentlyAskedQuestionTranslations[0].header}')"><i class="la la-edit"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var frequentlyAskedQuestion = element.closest('.input-frequentlyAskedQuestion');
                if (frequentlyAskedQuestion.length) {
                    frequentlyAskedQuestion.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/FrequentlyAskedQuestion/Update?Id=${id}`, function (response) {
            debugger
            $("#modal_body").html(response);
            $("#frequentlyAskedQuestion_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Sıkça Sorulan Soru Güncelle");
            debugger
            FrequentlyAskedQuestionIndex.ModalValidation("frequentlyAskedQuestion_update_form", function () {
                const frequentlyAskedQuestion = Serializer.Serialize("frequentlyAskedQuestion_update_form");
                debugger
                HttpClient.Post(
                    "/FrequentlyAskedQuestion/Update",
                    frequentlyAskedQuestion,
                    function (response) {
                        if (response.success) {
                            FrequentlyAskedQuestionIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#frequentlyAskedQuestion_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#frequentlyAskedQuestion_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/FrequentlyAskedQuestion/Create", function (response) {
            $("#modal_body").html(response);
            $("#frequentlyAskedQuestion_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Sıkça Sorulan Soru");

            FrequentlyAskedQuestionIndex.ModalValidation("frequentlyAskedQuestion_create_form", function () {
                const frequentlyAskedQuestion = Serializer.Serialize("frequentlyAskedQuestion_create_form");

                HttpClient.Post(
                    "/FrequentlyAskedQuestion/Create",
                    frequentlyAskedQuestion,
                    function (response) {
                        if (response.success) {
                            FrequentlyAskedQuestionIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#frequentlyAskedQuestion_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#frequentlyAskedQuestion_create_form").submit();
            });
        });
    },
    Delete: function (frequentlyAskedQuestion, header) {


        SwalAlertModal.ConfirmPopup('Sıkça Sorunlan Sorular Sil', `${header} silinsin mi?`, 'Evet', true, 'Hayır',
            function () {
                
                HttpClient.Post(
                    "/FrequentlyAskedQuestion/Delete",
                    frequentlyAskedQuestion,
                    function (response) {

                        if (response.success) {
                            FrequentlyAskedQuestionIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                )
            }
        )



    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        FrequentlyAskedQuestionIndex.Read(0);
    },
};

$(document).ready(() => {
    FrequentlyAskedQuestionIndex.Initialize(0);
});