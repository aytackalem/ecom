﻿"use strict";

const ShoppingCartDiscountedProductIndex = {
    Initialize: function () {
        ShoppingCartDiscountedProductIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/ShoppingCartDiscountedProduct/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "",
                    Render: function (value) {
                        return value.product.productTranslations[0].name;
                    },
                    Type: "string"
                },
                {
                    Name: "",
                    Render: function (value) {
                        return value.shoppingCartDiscountedProductSetting.discountIsRate;
                    },
                    Type: "bool"
                },
                {
                    Name: "",
                    Render: function (value) {
                        return value.shoppingCartDiscountedProductSetting.discount;
                    },
                    Type: "number"
                },
                {
                    Name: "active",
                    Type: "bool"
                }
            ],
            "ShoppingCartDiscountedProduct",
            [{
                Render: function (value) {

                    return `<a class="dropdown-item" href="javascript:ShoppingCartDiscountedProductIndex.Delete({ id: ${value["id"]}})"><i class="la la-edit"></i>
                            Sil
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/ShoppingCartDiscountedProduct/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#shopping_cart_discounted_product_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Sepette İndirim Güncelle");

            ShoppingCartDiscountedProductIndex.ModalValidation("shopping_cart_discounted_product_update_form", function () {
                HttpClient.Post(
                    "/ShoppingCartDiscountedProduct/Update",
                    Serializer.Serialize("shopping_cart_discounted_product_update_form"),
                    function (response) {
                        if (response.success) {
                            ShoppingCartDiscountedProductIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#shopping_cart_discounted_product_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#shopping_cart_discounted_product_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/ShoppingCartDiscountedProduct/Create", function (response) {
            $("#modal_body").html(response);
            $("#shopping_cart_discounted_product_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Sepette İndirim");

            ShoppingCartDiscountedProductIndex.ModalValidation("shopping_cart_discounted_product_create_form", function () {
                HttpClient.Post(
                    "/ShoppingCartDiscountedProduct/Create",
                    Serializer.Serialize("shopping_cart_discounted_product_create_form"),
                    function (response) {
                        if (response.success) {
                            ShoppingCartDiscountedProductIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#shopping_cart_discounted_product_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#shopping_cart_discounted_product_create_form").submit();
            });
        });
    },
    Delete: function (shoppingCartDiscountedProduct) {


        SwalAlertModal.ConfirmPopup('Sepet indirimi', `${shoppingCartDiscountedProduct.id} nolu indirim silinsin mi?`, 'Evet', true, 'Hayır',
            function () {

                HttpClient.Post(
                    "/ShoppingCartDiscountedProduct/Delete",
                    shoppingCartDiscountedProduct,
                    function (response) {

                        if (response.success) {
                            ShoppingCartDiscountedProductIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                )
            }
        )
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        ShoppingCartDiscountedProductIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ShoppingCartDiscountedProductIndex.Read(0);
    },
};

$(document).ready(() => {
    ShoppingCartDiscountedProductIndex.Initialize(0);
});