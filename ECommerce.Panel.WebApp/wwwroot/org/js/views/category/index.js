﻿const CategoryIndex = {
    Initialize: function () {

        $(document).on('show.bs.modal', '.modal', function () {
            const zIndex = 1040 + 10 * $('.modal:visible').length;
            $(this).css('z-index', zIndex);
            setTimeout(() => $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack'));
        });

        CategoryIndex.Read();

    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Category/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" }, {
                Name: "", Type: "string", Render: function (value) {
                    return value.categoryTranslations[0].name;
                }
            }, { Name: "active", Type: "bool" }],
            "Category",
            [{
                Render: function (value) {
                    return `<a class="dropdown-item" href="javascript:CategoryIndex.OpenMarketPlaceSettingsModal(${value["id"]})"><i class="la la-edit"></i>
                            Pazaryeri Ayarları
                        </a>`;
                }
            }],
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {

        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "Value must not equal arg.");

        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },
                select: {
                    required: true
                },
                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenMarketPlaceSettingsModal: function (id) {
        HttpClient.Get(`/MarketplaceCategoryMapping/IndexPartial?categoryId=${id}`, function (response) {
            $("#modal_marketplace_body").html(response);

            $("#category_marketplace_modal").modal();
            $("#modal_marketplace_submit").unbind("click");
            $("#modal_marketplace_title").html("Pazaryeri Ayarları");
            $('.kt-selectpicker').selectpicker();
            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            CategoryIndex.ModalValidation("marketplace_form", function () {


                var marketplaceCategoryMappings = [];

                $('.category-mapping-marketplace-container').each((i, theDiv) => {
                    
                        var $theDiv = $(theDiv);
                        var value = $theDiv.find('select.marketplace-select');
                        var marketplaceId = $theDiv.data('marketplace-id');


                        var marketplaceCategoryMapping = {
                            marketplaceCategoryCode: $(value).val(),
                            marketplaceCategoryName: $(value).find(":selected").eq(0).text(),
                            companyId: $(value).find(":selected").eq(0).data("company-id"),
                            marketplaceId: marketplaceId,
                            MarketplaceCategoryVariantValueMappings: []
                        };
                        marketplaceCategoryMapping.companyId = marketplaceCategoryMapping.companyId == "" ? null : parseInt(marketplaceCategoryMapping.companyId)


                        var $formGroups = $theDiv.find('.form-group');


                        $formGroups = $theDiv.find('.marketplace-variant-container').eq(0).find('.form-group');

                        $formGroups.each((i, theFormGroup) => {
                            var $theFormGroup = $(theFormGroup);

                            var allowCustom = $theFormGroup.data("allow-custom");

                            var marketplaceCategoryVariantValueMapping = {
                                MarketplaceId: marketplaceId,
                                MarketplaceVariantCode: $theFormGroup.data("marketplace-variant-code").toString(),
                                MarketplaceVariantName: $theFormGroup.data("marketplace-variant-name").toString(),
                                AllowCustom: allowCustom,
                                Mandatory: $theFormGroup.data("mandatory"),
                                Varianter: $theFormGroup.data("varianter"),
                                Multiple: $theFormGroup.data("multiple")
                            };

                            if (allowCustom) {
                                marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode = $theFormGroup.find('[type="text"]').eq(0).val();
                                marketplaceCategoryVariantValueMapping.MarketplaceVariantValueName = $theFormGroup.find('[type="text"]').eq(0).val();
                            }
                            else {
                                marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode = $theFormGroup.data("marketplace-variant-value-code").toString();
                                marketplaceCategoryVariantValueMapping.MarketplaceVariantValueName = $theFormGroup.data("marketplace-variant-value-name").toString();
                            }

                            if (marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode != '') {
                                marketplaceCategoryMapping.MarketplaceCategoryVariantValueMappings.push(marketplaceCategoryVariantValueMapping);
                            }
                        });

                        marketplaceCategoryMappings.push(marketplaceCategoryMapping);
                    
                });


                HttpClient.Post(
                    "/MarketplaceCategoryMapping/Upsert",
                    {
                        categoryId: $("#category-id").val(),
                        marketplaceCategoryMappings: marketplaceCategoryMappings
                    },
                    function (response) {
                        if (response.success) {
                            Notify.Show(NotifyType.Success, response.message);

                            $("#category_marketplace_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $('.product-marketplace-container').each((i, theDiv) => {

                var $theDiv = $(theDiv);

                var marketplaceId = $theDiv.data('marketplace-id');
                var isECommerce = $theDiv.data('is-ecommerce');

                var $brandFormGroups = $theDiv.find(".form-group[data-marketplace-brand-autocomplete='true']");
                $brandFormGroups.each((i, formGroup) => {
                    var $formGroup = $(formGroup);
                    var $input = $formGroup.find("[type='text']").eq(0);

                    $input.focus(function () {

                        if ($input.data('autocomplete-initialized') == true)
                            return;

                        $input.data('autocomplete-initialized', true);

                        Autocomplete.InitRemote(
                            $input,
                            isECommerce ? "/ECommerceBrand/SearchByName" : "https://marketplacecatalog.helpy.com.tr/Brand/SearchByName",
                            [{
                                key: "MarketplaceId",
                                value: marketplaceId
                            }],
                            (i) => { return { Code: i.C, Name: i.N } },
                            "Name",
                            null,
                            (suggestion) => {
                                $formGroup.data("marketplace-brand-code", suggestion.Code);
                                $formGroup.data("marketplace-brand-name", suggestion.Name);

                                if ($('.handle-variant-entries').prop('checked')) {

                                    var marketplaceVariantCode = $formGroup.data("marketplace-variant-code");

                                    var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                                    var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                                    for (var i = index; i < length; i++) {
                                        var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + marketplaceVariantCode + '"]').eq(0);

                                        $foundFormGroup.find('[type="text"]').val(suggestion.VariantValueName);

                                        $foundFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                                        $foundFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                                    }

                                }

                                $input.typeahead("destroy");
                                $input.data('autocomplete-initialized', false);
                            },
                            () => {
                                $formGroup.data("marketplace-brand-code", "");
                                $formGroup.data("marketplace-brand-name", "");
                            },
                            () => { });

                        $input.closest(".form-group").find(".tt-input").focus();
                    });

                });

                var $formGroups = $theDiv.find(".form-group[data-marketplace-category-code]");
                $formGroups.each((i, formGroup) => {
                    var $formGroup = $(formGroup);
                    var $input = $formGroup.find("select").eq(0);
                    $input.change(function () {

                        var categoryCode = $input.val();
                        var categoryName = $input.find(':checked').text();

                        $formGroup.data('marketplace-category-code', categoryCode);
                        $formGroup.data('marketplace-category-name', categoryName);

                        HttpClient.Post(isECommerce ? "/ECommerceVariant/GetByCategoryCodes" : "https://marketplacecatalog.helpy.com.tr/Variant/GetByCategoryCodes", {
                            MarketplaceId: marketplaceId,
                            CategoryCodes: [categoryCode]
                        }, function (response) {

                            if (response.success) {
                                response.data.I = response.data.I.sort((a, b) => b.M - a.M);

                                var html = '';
                                for (var i = 0; i < response.data.I.length; i++) {
                                    
                                    if (response.data.I[i].AC) {
                                        html += `
                                      
<div class="form-group pos-relative" data-marketplace-variant-value-code="" data-marketplace-variant-value-name="" data-marketplace-variant-code="${response.data.I[i].C}" data-marketplace-variant-name="${response.data.I[i].N}" data-allow-custom="${response.data.I[i].AC}" data-mandatory="${response.data.I[i].M}">
    <label class="input-abs-text">
        ${response.data.I[i].N}:
        ${response.data.I[i].M ? '<span class="kt-font-danger">* Zorunlu</span>' : ''}
    </label>
    <div class="kt-input-icon kt-input-icon--left">
        <input type="text" class="form-control" placeholder="" />
        <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span><i class="la la-i-cursor"></i></span>
        </span>
    </div>
</div>



`;
                                    }
                                    else {
                                        html += `
                                         <div class="row">
                                          <div class="col-md-10">
<div class="form-group pos-relative" data-marketplace-variant-value-code="" data-marketplace-variant-value-name="" data-marketplace-variant-code="${response.data.I[i].C}" data-marketplace-variant-name="${response.data.I[i].N}" data-allow-custom="${response.data.I[i].AC}" data-mandatory="${response.data.I[i].M}">
    <label class="input-abs-text">
        ${response.data.I[i].N}:
        ${response.data.I[i].M ? '<span class="kt-font-danger">* Zorunlu</span>' : ''}
    </label>
    <div class="typeahead">
        <div class="kt-input-icon kt-input-icon--left">
            <input type="text" class="form-control" placeholder="" />
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
    </div>
</div>
</div>

    <div class="col-md-2">
               <button class="btn btn-primary btn-icon btn-sm"
                       type="button"
                       data-full-search-marketplace-variant-name="${response.data.I[i].N}"
                       data-full-search-marketplace-variant-code="${response.data.I[i].C}">
                   <i class="fa fa-search"></i>
               </button>
           </div>
               </div>

`;
                                    }
                                }
                                $(`.marketplace-variant-container[data-marketplace-id="${marketplaceId}"]`).html(html);

                                InitializeMarketplaceVariantsAutocomplete(marketplaceId);

                            }

                        });

                    });
                });

                InitializeMarketplaceVariantsAutocomplete(marketplaceId);

            });


            $("#modal_marketplace_submit").click(function () {

                $("#marketplace_form").submit();
            });
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Category/Update?Id=${id}`, function (response) {

            $("#modal_body").html(response);
            $("#category_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Kategori Güncelle");

            fileNameImage();
            $('.summernote').summernote({
                height: 150
            });

            CategoryIndex.ModalValidation("category_update_form", function () {




                const category = Serializer.Serialize("category_update_form");

                HttpClient.Post(
                    "/Category/Update",
                    category,
                    function (response) {
                        if (response.success) {
                            CategoryIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#category_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#category_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Category/Create", function (response) {
            $("#modal_body").html(response);
            $("#category_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Kategori");
            fileNameImage();
            $('.summernote').summernote({
                height: 150
            });
            CategoryIndex.ModalValidation("category_create_form", function () {
                const category = Serializer.Serialize("category_create_form");

                HttpClient.Post(
                    "/Category/Create",
                    category,
                    function (response) {
                        if (response.success) {
                            CategoryIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#category_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#category_create_form").submit();
            });
        });
    },
    ChangeSorting: function (sender) {
        var sorting = $(sender).val();
        $(".ajax_data_sorting").val(sorting);
        CategoryIndex.Read(0);
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        CategoryIndex.Read(0);
    },
};

function fileNameImage() {
    $('#select-fileNameImage').click(function () {
        $('#fileNameImage').click();
    })

    $('#fileNameImage').change(function () {
        const preview = document.getElementById('fileNameImage-preview');
        const iconUrl = document.getElementById('fileNameImageUrl');
        const file = document.getElementById('fileNameImage').files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
            iconUrl.value = reader.result;
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    });


    $("a[name='addButton']").click(function () {

        var i = $(this).data('productinformation-id');
        var productInformationPhotoHmtl = $('#productInformationPhoto_' + i);
        var j = $('.productInformationPhoto').length;



        var html = "<div class=\"col-lg-12 productInformationPhoto\" id=\"productInformation-photo-item-" + j + "\">";
        html += "<div class=\"form-group row\" style=\"display:flex;align-items:center;justify-content: space-around;\">"
        html += "<label class=\"col-xl-1 col-lg-1 col-form-label\">Sıra</label>";
        html += "<input class=\"col-form-label photoİndex form-control\" type=\"number\" value=\"" + (j + 1) + "\" name=\"productInformations[" + i + "][ProductInformationPhotos][" + j + "][displayOrder]\" style=\"flex:0.1\">"
        html += "<div class=\"productPhoto\" style=\"flex:0.7;text-align:center;\">"
        html += "<div class=\"kt-avatar kt-avatar--outline \" id=\"kt_user_avatar_1\">"
        html += "<input type=\"hidden\" name=\"productInformations[" + i + "][ProductInformationPhotos][" + j + "][id]\" value=\"0\" />"
        html += "<input type=\"hidden\" id=\"filename_" + j + "\" name=\"productInformations[" + i + "][ProductInformationPhotos][" + j + "][fileName]\" class=\"form-control\">"
        html += "<div class=\"kt-avatar__holder photoSrc\" id=\"fileNameImage-preview_" + j + "\" \"></div>"
        html += "<label class=\"kt-avatar__upload\" data-toggle=\"kt-tooltip\" title=\"\" data-original-title=\"Change avatar\">"
        html += "<i class=\"fa fa-pen\"></i>"
        html += "<input type=\"file\" name=\"profile_avatar\" data-photo-id=\"" + j + "\">"
        html += "</label>"
        html += "</div>"
        html += "</div>"
        html += "<div class=\"photoRemove \"   style=\"flex:0.2\">"
        html += "<a title=\"Sil\" name=\"photoRemove\" data-productInformation-photo-item=" + j + " class=\"btn btn-lg btn-warning btn-icon btn-icon-lg\">"
        html += "<i class=\"la la-trash\"></i>"
        html += "</a>"
        html += "</div>"
        html += "</div>"
        html += "</div>"

        productInformationPhotoHmtl.append(html);
        fileNameImage();
    })
};
function InitializeMarketplaceVariantsAutocomplete(marketplaceId) {
    $(`.marketplace-variant-container[data-marketplace-id="${marketplaceId}"]`).each((i, theDiv) => {

        var $theDiv = $(theDiv);

        var marketplaceId = $theDiv.data('marketplace-id');
        var isECommerce = $theDiv.data('is-ecommerce');

        var $formGroups = $theDiv.find(".form-group[data-allow-custom='false']");
        $formGroups.each((i, formGroup) => {
            var $formGroup = $(formGroup);
            var $input = $formGroup.find("[type='text']").eq(0);

            $input.focus(function () {

                if ($input.data('autocomplete-initialized') == true)
                    return;

                $input.data('autocomplete-initialized', true);

                Autocomplete.InitRemote(
                    $input,
                    isECommerce ? "/ECommerceVariantValue/SearchByName" : "https://marketplacecatalog.helpy.com.tr/VariantValue/SearchByName",
                    [{
                        key: "MarketplaceId",
                        value: marketplaceId
                    }, {
                        key: "VariantCode",
                        value: $formGroup.data("marketplace-variant-code")
                    }, {
                        key: "CategoryCode",
                        value: $(`.product-marketplace-container[data-marketplace-id="${marketplaceId}"]`).find('[data-marketplace-category-code]').data("marketplace-category-code")
                    }],
                    (i) => { return { VariantValueCode: i.C, VariantValueName: i.N } },
                    "VariantValueName",
                    null,
                    (suggestion) => {
                        $formGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                        $formGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);

                        if ($('.handle-variant-entries').prop('checked')) {

                            var marketplaceVariantCode = $formGroup.data("marketplace-variant-code");

                            var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                            var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                            for (var i = index; i < length; i++) {
                                var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + marketplaceVariantCode + '"]').eq(0);

                                $foundFormGroup.find('[type="text"]').val(suggestion.VariantValueName);

                                $foundFormGroup.data("marketplace-variant-value-code", suggestion.VariantValueCode);
                                $foundFormGroup.data("marketplace-variant-value-name", suggestion.VariantValueName);
                            }

                        }

                        $input.typeahead("destroy");
                        $input.data('autocomplete-initialized', false);
                    },
                    () => {
                        $formGroup.find('[type="text"]').val('');
                        $formGroup.data("marketplace-variant-value-code", "");
                        $formGroup.data("marketplace-variant-value-name", "");
                    },
                    () => {
                        if ($formGroup.data("marketplace-variant-value-code") == '') {
                            $formGroup.find('[type="text"]').val('');
                            $formGroup.data("marketplace-variant-value-code", "");
                            $formGroup.data("marketplace-variant-value-name", "");

                            if ($input.data('autocomplete-initialized') == false)
                                return;

                            Notify.Show(NotifyType.Error, "Varyasyon seçimi yapamadınız.");
                        }
                    });

                $input.closest(".form-group").find(".tt-input").focus();
            });

        });

        var $searchButtons = $theDiv.find("button[data-full-search-marketplace-variant-code]");
        $searchButtons.each((i, button) => {
            var $button = $(button);
            var variantCode = $button.data("full-search-marketplace-variant-code");
            var variantName = $button.data("full-search-marketplace-variant-name");
            var categoryCode = $(`.product-marketplace-container[data-marketplace-id="${marketplaceId}"]`).find('[data-marketplace-category-code]').data("marketplace-category-code");

            $button.unbind('click');
            $button.click(function () {
                HttpClient.Get(`https://marketplacecatalog.helpy.com.tr/VariantValue?MarketplaceId=${marketplaceId}&VariantCode=${variantCode}&CategoryCode=${categoryCode}`, function (response) {

                    $("#variant_value_modal").modal();
                    $("#variant_value_modal_title").html(`${variantName} Değerler`);
                    $("#variant_value_modal_body").empty();
                    for (var i = 0; i < response.data.I.length; i++) {
                        var $btn = $(`<button class="btn btn-primary btn-sm" data-val="${response.data.I[i].C}" style="margin-right: 10px; margin-bottom: 10px">${response.data.I[i].N}</button>`);
                        $btn.click(function () {
                            var $input = $button.closest('.row').find(`[data-marketplace-variant-code="${variantCode}"]`);
                            $input.data("marketplace-variant-value-code", $(this).data("val"));
                            $input.data("marketplace-variant-value-name", $(this).text());
                            $input.find('[type="text"]').val($(this).text());

                            if ($('.handle-variant-entries').prop('checked')) {

                                var length = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').length;
                                var index = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').index($input.closest(".marketplace-variant-container").eq(0)) + 1;

                                for (var i = index; i < length; i++) {
                                    var $foundFormGroup = $('.marketplace-variant-container[data-marketplace-id="' + marketplaceId + '"]').eq(i).find('.form-group[data-marketplace-variant-code="' + variantCode + '"]').eq(0);

                                    $foundFormGroup.find('[type="text"]').val($(this).text());

                                    $foundFormGroup.data("marketplace-variant-value-code", $(this).data("val"));
                                    $foundFormGroup.data("marketplace-variant-value-name", $(this).text());
                                }

                            }

                            $("#variant_value_modal").modal("hide");
                        });
                        $("#variant_value_modal_body").append($btn);
                    }

                });
            });
        });

    });
};

$(document).ready(() => {
    CategoryIndex.Initialize(0);
});