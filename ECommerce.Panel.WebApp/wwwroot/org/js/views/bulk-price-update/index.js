﻿
const BulkPriceUpdateIndex = {
 
    Download: function () {
        window.open(`/BulkPriceUpdate/Download?categoryId=${$("#ajax_data_search_category_id").val()}`)
    },
    Update: function () {
        var formData = new FormData();
        formData.append("file", document.getElementById("file").files[0]);

        HttpClient.PostFile("/BulkPriceUpdate/Update", formData, function (response) {
            if (response.success) {
                Notify.Show(NotifyType.Success, response.message);
            }
            else {
                Notify.Show(NotifyType.Error, response.message);
            }
        });
    }
};

$(document).ready(() => {

});