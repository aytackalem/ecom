﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

const AccountIndex = {
    Validation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    Login: function () {
        AccountIndex.Validation("login_form", function () {

            if (captchaResponse == null) {
                Notify.Show(NotifyType.Error, "Lütfen reCAPTCHA'yı doğrulayınız.");
                return;
            }

            var data = Serializer.Serialize("login_form");

            HttpClient.Post(
                "/Account/Login",
                {
                    captchaResponse: captchaResponse,
                    data: data
                },
                function (response) {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);

                    if (response.success) {
                        window.location.href = "/";
                    }
                    else {
                        Notify.Show(NotifyType.Error, response.message);
                    }
                },
                function () {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);
                },
                "kt_login_signin_submit"
            );
        });
    },
    Logout: function () {
        HttpClient.Get(
            "/Account/Logout",
            function (response) {
                if (response.success) {
                    window.location.href = "/Account/Login";
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            },
            function () { },
            "kt_login_signout"
        );
    },
    ChangeDomain: function (domainId) {

        HttpClient.Post(
            "/Account/ChangeDomain",
            {
                id: domainId
            },
            function (response) {

                if (response.success) {
                    window.location.reload();
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            });
    },
    ChangeCompany: function (comapanyId) {
        
        HttpClient.Post(
            "/Account/ChangeCompany",
            {
                id: comapanyId
            },
            function (response) {

                if (response.success) {
                    window.location.reload();
                }
                else {
                    Notify.Show(NotifyType.Error, response.message);
                }
            });
    }
};

$(document).ready(() => {

    AccountIndex.Login();

    $("#kt_login_signin_submit").click(function () {
        $("#login_form").submit();
    });
});