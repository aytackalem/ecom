﻿
const GroupIndex = {
    Initialize: function () {
        GroupIndex.Read();
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Group/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: $("#ajax_data_search").val()
            },
            [{ Name: "id", Type: "number" }, {
                Name: "", Type: "string", Render: function (value) {
                    return value.groupTranslations[0].name;
                }
            }, { Name: "active", Type: "bool" }],
            "Group",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Group/Update?Id=${id}`, function (response) {
            
            $("#modal_body").html(response);
            $("#group_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Kategori Güncelle");
            $('.summernote').summernote({
                height: 150
            });

            GroupIndex.ModalValidation("group_update_form", function () {
                const group = Serializer.Serialize("group_update_form");
                debugger
                HttpClient.Post(
                    "/Group/Update",
                    group,
                    function (response) {
                        if (response.success) {
                            GroupIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#group_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#group_update_form").submit();
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Group/Create", function (response) {
            $("#modal_body").html(response);
            $("#group_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Kategori");
            $('.summernote').summernote({
                height: 150
            });

            GroupIndex.ModalValidation("group_create_form", function () {
                const group = Serializer.Serialize("group_create_form");

                HttpClient.Post(
                    "/Group/Create",
                    group,
                    function (response) {
                        if (response.success) {
                            GroupIndex.Read(0);

                            Notify.Show(NotifyType.Success, response.message);

                            $("#group_modal").modal("hide");
                        }
                        else {
                            Notify.Show(NotifyType.Error, response.message);
                        }
                    }
                );
            });

            $("#modal_submit").click(function () {
                $("#group_create_form").submit();
            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        GroupIndex.Read(0);
    },
};

$(document).ready(() => {
    GroupIndex.Initialize(0);
});