﻿"use strict";

const ExpenseIndex = {
    Initialize: function () {
        ExpenseIndex.Read(0);

        $('.kt-select2').select2({
            placeholder: "Seçiniz"
        });
    },
    Read: function (page) {
        $("#ajax_data_page").val(page);

        Table.Initialize(
            "/Expense/Read",
            {
                Page: $("#ajax_data_page").val(),
                PageRecordsCount: $(".ajax_data_length").val(),
                Search: `Note:${$("#ajax_data_search_note").val()}`
            },
            [
                {
                    Name: "id",
                    Type: "number"
                },
                {
                    Name: "amount",
                    Type: "decimal"
                },
                {
                    Name: "expenseDateStr",
                    Type: "string"
                },
                {
                    Name: "note",
                    Type: "string"
                }
            ],
            "Expense",
            null,
            true
        );
    },
    ModalValidation: function (formName, submitFunction) {
        $(`#${formName}`).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                url: {
                    required: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true
                },
                phone: {
                    required: true,
                    phoneUS: true
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },

            errorPlacement: function (error, element) {
                var group = element.closest('.input-group');
                if (group.length) {
                    group.after(error.addClass('invalid-feedback'));
                } else {
                    element.after(error.addClass('invalid-feedback'));
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                submitFunction();
            }
        });
    },
    OpenUpdateModal: function (id) {
        HttpClient.Get(`/Expense/Update?Id=${id}`, function (response) {
            $("#modal_body").html(response);
            $("#expense_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Sipariş Detay");

            $('.kt-datetimepicker').datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                autoclose: true,
                language: 'tr'
            });

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            $('.kt-selectpicker').selectpicker();

            ExpenseIndex.OrderSourceSelectionChanges();

            $("#modal_submit").click(function () {

                let valid = Validation.Valid("expense_update_form");
                if (valid) {
                    
                    var data = Serializer.Serialize("expense_update_form");

                    if (data.productInformationId == "") data.productInformationId = null;
                    if (data.orderSourceId == "") data.orderSourceId = null;
                    if (data.marketplaceId == "") data.marketplaceId = null;

                    HttpClient.Post(
                        "/Expense/Update",
                        data,
                        function (response) {
                            if (response.success) {
                                ExpenseIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#expense_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");
            });
        });
    },
    OpenCreateModal: function () {
        HttpClient.Get("/Expense/Create", function (response) {
            $("#modal_body").html(response);
            $("#expense_modal").modal();
            $("#modal_submit").unbind("click");
            $("#modal_title").html("Yeni Sipariş");

            $('.kt-datetimepicker').datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                autoclose: true,
                language: 'tr'
            });

            $('.kt-select2').select2({
                placeholder: "Seçiniz"
            });

            $('.kt-selectpicker').selectpicker();

            ExpenseIndex.OrderSourceSelectionChanges();

            $("#modal_submit").click(function () {

                let valid = Validation.Valid("expense_create_form");
                if (valid) {
                    var data = Serializer.Serialize("expense_create_form");

                    if (data.productInformationId == "") data.productInformationId = null;
                    if (data.orderSourceId == "") data.orderSourceId = null;
                    if (data.marketplaceId == "") data.marketplaceId = null;

                    HttpClient.Post(
                        "/Expense/Create",
                        data,
                        function (response) {
                            if (response.success) {
                                ExpenseIndex.Read(0);

                                Notify.Show(NotifyType.Success, response.message);

                                $("#expense_modal").modal("hide");
                            }
                            else {
                                Notify.Show(NotifyType.Error, response.message);
                            }
                        }
                    );
                }
                else
                    Notify.Show(NotifyType.Error, "Lütfen tüm zorunlu alanları doldurunuz.");

            });
        });
    },
    ChangePageRecordsCount: function (sender) {
        var pageRecordsCount = $(sender).val();
        $(".ajax_data_length").val(pageRecordsCount);
        ExpenseIndex.Read(0);
    },
    /**
     * Sipariş kaynağı seçimlerine göre alanları düzenleyen fonksiyon.
     */
    OrderSourceSelectionChanges: function () {
        $('[name="orderSourceId"]').change(function () {
            var orderSourceId = $(this).val();

            $("[name='marketplaceId']").prop('disabled', orderSourceId != "1");
            $("[name='marketplaceId']").prop('required', orderSourceId == "1");

            if (orderSourceId != "1") {

                $("[name='marketplaceId']").val('');
                $("#marketplaceId-error").remove();
            }

            $('.kt-selectpicker').selectpicker('refresh');
        });
    }
};

$(document).ready(() => {
    ExpenseIndex.Initialize(0);
});

