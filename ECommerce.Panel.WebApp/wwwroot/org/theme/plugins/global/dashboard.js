var carousel1 = $('#kt_earnings_widget .kt-widget30__head .owl-carousel');
var carousel2 = $('#kt_earnings_widget .kt-widget30__body .owl-carousel');

carousel1.find('.carousel').each(function (index) {
    $(this).attr('data-position', index); 
});

carousel1.owlCarousel({
    rtl: KTUtil.isRTL(),
    center: true,
    loop: true,
    items: 1,
    autoPlay: true,
});

carousel2.owlCarousel({
    rtl: KTUtil.isRTL(),
    items: 1,
    animateIn: 'fadeIn(100)',
    loop: true,
    autoPlay: true,
});

$(document).on('click', '.carousel', function () {
    var index = $(this).attr('data-position');
    if (index) {
        carousel1.trigger('to.owl.carousel', index);
        carousel2.trigger('to.owl.carousel', index);
    }
});

carousel1.on('changed.owl.carousel', function () {
    var index = $(this).find('.owl-item.active.center').find('.carousel').attr('data-position');
    if (index) {
        carousel2.trigger('to.owl.carousel', index);
    }
});

carousel2.on('changed.owl.carousel', function () {
    var index = $(this).find('.owl-item.active.center').find('.carousel').attr('data-position');
    if (index) {
        carousel1.trigger('to.owl.carousel', index);
    }
});