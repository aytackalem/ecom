using ECommerce.Infrastructure;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Floors;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Hallways;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Limits;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Pivots.PivotShelves;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Products;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Shelves;
using ECommerce.Panel.WebApp.Controllers.Warehouse.Warehouses;
using ECommerce.Panel.WebApp.Controllers.Warehouse.WorkOrders;
using ECommerce.Panel.WebApp.Controllers.Xml.Definitions;
using ECommerce.Panel.WebApp.Controllers.Xml.Integrations;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Panel;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace ECommerce.Panel.WebApp
{
    public class Startup
    {
        #region Properties
        public IConfiguration _configuration { get; }
        #endregion

        #region Constructors
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region Methods
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new TenantViewLocationExpander());
            });

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services
                .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, o =>
                {
                    o.LoginPath = "/Account/Login";
                    o.ExpireTimeSpan = TimeSpan.FromDays(1);
                    o.SlidingExpiration = true;
                    o.Cookie.Name = "Panel";
                });

            services.AddHttpContextAccessor();
            services.AddPersistenceServices(_configuration);
            services.AddInfrastructureServices(_configuration);

            #region Warehouse Api Services
            services.AddScoped<FloorsService>();
            services.AddScoped<HallwaysService>();
            services.AddScoped<PivotsService>();
            services.AddScoped<PivotShelvesService>();
            services.AddScoped<ShelvesService>();
            services.AddScoped<WarehousesService>();
            services.AddScoped<LimitsService>();
            services.AddScoped<DefinitionsService>();
            services.AddScoped<IntegrationsService>();
            services.AddScoped<WorkOrdersService>();
            services.AddScoped<ProductsService>();
            #endregion
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

#if RELEASE
            app.UseHttpsRedirection();            
            app.UseHsts();
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
                context.Response.Headers.Add("X-Xss-Protection", "1; mode=block");
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Remove("server");
                context.Response.Headers.Remove("x-powered-by");              
                await next();
            });
#endif

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Strict
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        #endregion
    }
}
