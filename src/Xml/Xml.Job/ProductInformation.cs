﻿namespace Xml.Job;

public class ProductInformation
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string BrandName { get; set; }

    public string SellerCode { get; set; }

    public decimal ListUnitPrice { get; set; }

    public decimal UnitPrice { get; set; }

    public string Barcode { get; set; }

    public string FullVariantValuesDescription { get; set; }

    public int DomainId { get; set; }

    public int BrandId { get; set; }

    public int Stock { get; set; }

    public List<Photo> Photos { get; set; }
}
