﻿namespace Xml.Job;

internal class XmlDefination
{
    public int Id { get; set; }

    public string FileName { get; set; }

    public string Fields { get; set; }

    public string ImageUrl { get; set; }

    public int DomainId { get; set; }
}
