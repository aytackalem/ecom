﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using System.Xml.Serialization;
using Xml.Job;

var connectionStringTemplate = "Server=172.31.57.11;Database={0};UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;trustServerCertificate=true";

var dbNames = new List<string>();

using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, "master")))
{
    var data = await sqlConnection.QueryAsync<string>("Select [name] From master.sys.databases Where database_id > 4 And [name] Not In ('IdentityServer', 'IdentityServer.Config', 'Lafaba_ECommerce', 'Marketplace') and [name] NOT LIKE '%Warehouse%'");

    dbNames = data.ToList();
}

foreach (var dbName in dbNames)
{
    var tableExist = false;
    using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, dbName)))
    {
        var count = await sqlConnection.ExecuteScalarAsync<int>("Select Count(0) From Information_Schema.Tables Where Table_Name = 'XmlDefinations'");

        tableExist = count > 0;
    }

    if (tableExist)
    {
        Console.WriteLine($"Dbname {dbName}");

        IEnumerable<XmlDefination> xmlDefinations = null;
        using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, dbName)))
        {
            xmlDefinations = await sqlConnection.QueryAsync<XmlDefination>(
                @"Select		Xd.Id,
                                FileName, 
			                    Fields,
			                    ImageUrl,
			                    Xd.DomainId 
                 From		    XmlDefinations Xd
                 Join		    DomainSettings D ON Xd.DomainId=D.Id");
        }

       
        if (xmlDefinations.Any())
        {

            IEnumerable<ProductInformation> productInformations = null;
            using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, dbName)))
            {
                productInformations = await sqlConnection.QueryAsync<ProductInformation>(@"

Select	PN.Id,
		PIT.Name,
		P.SellerCode,
		B.Name as BrandName,
		PIP.ListUnitPrice,
		PIP.UnitPrice,
		PN.Barcode,
		PN.Stock,
        PIT.FullVariantValuesDescription,
        P.DomainId,
        P.BrandId
From	ProductInformations As PN With(NoLock)
Join	ProductInformationTranslations As PIT With(NoLock)
On		PN.Id = PIT.ProductInformationId
Join	ProductInformationPriceses As PIP With(NoLock)
On		PN.Id = PIP.ProductInformationId
Join    Products As P With(NoLock)
On		P.Id=PN.ProductId
Join    Brands As B With(NoLock)
On		B.Id=P.BrandId
Join    XmlDefinationBrands XB With(NoLock)
On		XB.BrandId=B.Id
Where   PIP.ListUnitPrice > 0
		And PIP.UnitPrice > 0
");
            }

            IEnumerable<Photo> photos = null;
            if (xmlDefinations.Any(_ => _.Fields.Contains("Photo")))
            {
                using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, dbName)))
                {
                    photos = await sqlConnection.QueryAsync<Photo>(@"Select	PIS.ProductInformationId,
		PIS.FileName
From	ProductInformationPhotos As PIS With(NoLock)");
                }
            }

            foreach (var xmlDefination in xmlDefinations)
            {
                IEnumerable<XmlDefinationBrand> xmlDefinationBrands = null;

                using (SqlConnection sqlConnection = new(string.Format(connectionStringTemplate, dbName)))
                {
                    xmlDefinationBrands = await sqlConnection.QueryAsync<XmlDefinationBrand>(@$"Select * from [dbo].[XmlDefinationBrands] Where XmlDefinationId={xmlDefination.Id}");
                }

                if (xmlDefinationBrands.Any() == false)
                {
                    continue;
                }


                var path = @"\\172.31.57.21\c$\XmlDefination";

#if DEBUG
                path = @"C:\XmlDefination";
#endif
              

                using (FileStream fileStream = new(@$"{path}\_{xmlDefination.FileName}", FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter streamWriter = new(fileStream, System.Text.Encoding.UTF8))
                    {
                        streamWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        streamWriter.WriteLine("<Products>");

                        var pins = productInformations.Where(x => x.DomainId == xmlDefination.DomainId && xmlDefinationBrands.Any(xd => xd.BrandId == x.BrandId)).ToList();

                        foreach (var productInformation in pins)
                        {
                            streamWriter.WriteLine("<Product>");
                            if (xmlDefination.Fields.IndexOf("Name") != -1)
                            {
                                streamWriter.WriteLine("<Name><![CDATA[{0}]]></Name>", productInformation.Name);
                            }

                            streamWriter.WriteLine("<ModelCode><![CDATA[{0}]]></ModelCode>", productInformation.SellerCode);
                            streamWriter.WriteLine("<BrandName><![CDATA[{0}]]></BrandName>", productInformation.BrandName);


                            if (xmlDefination.Fields.IndexOf("List Price") != -1)
                            {
                                streamWriter.WriteLine("<ListPrice>{0}</ListPrice>", productInformation.ListUnitPrice);
                            }

                            if (xmlDefination.Fields.IndexOf("Price") != -1)
                            {
                                streamWriter.WriteLine("<Price>{0}</Price>", productInformation.UnitPrice);
                            }

                            if (xmlDefination.Fields.IndexOf("Barcode") != -1)
                            {
                                streamWriter.WriteLine("<Barcode>{0}</Barcode>", productInformation.Barcode);
                            }

                            if (xmlDefination.Fields.IndexOf("Stock") != -1)
                            {
                                streamWriter.WriteLine("<Stock>{0}</Stock>", productInformation.Stock);
                            }

                            if (xmlDefination.Fields.IndexOf("Photo") != -1)
                            {
                                streamWriter.WriteLine("<Photos>");

                                if (photos.Any(_ => _.ProductInformationId == productInformation.Id))
                                {
                                    foreach (var photo in photos.Where(_ => _.ProductInformationId == productInformation.Id))
                                    {
                                        streamWriter.WriteLine("<Photo>{0}/Product/{1}</Photo>", xmlDefination.ImageUrl, photo.FileName);
                                    }
                                }

                                streamWriter.WriteLine("</Photos>");
                            }

                            if (xmlDefination.Fields.IndexOf("Variant") != -1)
                            {
                                streamWriter.WriteLine("<Variants>");

                                if (!string.IsNullOrEmpty(productInformation.FullVariantValuesDescription))
                                {

                                    var fullVariantValuesDescriptions = productInformation.FullVariantValuesDescription.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                    if (fullVariantValuesDescriptions.Length > 0)
                                    {

                                        foreach (var fLoop in fullVariantValuesDescriptions)
                                        {

                                            var fLoops = fLoop.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                                            if (fLoops.Length == 2)
                                            {
                                                streamWriter.WriteLine("<Variant>");
                                                streamWriter.WriteLine("<Name><![CDATA[{0}]]></Name>", fLoops[0]);
                                                streamWriter.WriteLine("<Value><![CDATA[{0}]]></Value>", fLoops[1]);
                                                streamWriter.WriteLine("</Variant>");

                                            }

                                        }




                                    }
                                }

                                streamWriter.WriteLine("</Variants>");



                            }
                            streamWriter.WriteLine("</Product>");
                        }
                        streamWriter.WriteLine("</Products>");
                    }



                    if (File.Exists(@$"{path}\{xmlDefination.FileName}"))
                    {
                        File.Delete(@$"{path}\{xmlDefination.FileName}");
                    }

                    File.Copy(@$"{path}\_{xmlDefination.FileName}", @$"{path}\{xmlDefination.FileName}");
                    File.Delete(@$"{path}\_{xmlDefination.FileName}");


                }

            }
        }
    }
}