using System.Threading.RateLimiting;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRateLimiter(options =>
{
    options.RejectionStatusCode = StatusCodes.Status429TooManyRequests;

    options.AddPolicy("fixed-by-ip", httpContext =>
        RateLimitPartition.GetFixedWindowLimiter(
            partitionKey: httpContext.Connection.RemoteIpAddress?.ToString(),
            factory: _ => new FixedWindowRateLimiterOptions
            {
                PermitLimit = 10,
                Window = TimeSpan.FromSeconds(30)
            }));
});

var app = builder.Build();
app.UseHttpsRedirection();
app.UseRateLimiter();
app.MapGet("/{fileName}.xml", (string fileName) =>
{
    return Results.File(@$"C:\XmlDefination\{fileName}.xml", "application/xml");
}).RequireRateLimiting("fixed-by-ip");


app.Run();