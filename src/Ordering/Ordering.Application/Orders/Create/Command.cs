﻿using MediatR;
using Shared.Results;

namespace Ordering.Application.Orders.Create;

internal record Command : IRequest<Result<int>>;