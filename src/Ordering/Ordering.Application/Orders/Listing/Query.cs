﻿using ECommerce.Domain.Entities;
using MediatR;
using Shared.Results;

namespace Ordering.Application.Orders.Listing;

internal record Query(int Page, int Size) : IRequest<PagedResult<Order>>;