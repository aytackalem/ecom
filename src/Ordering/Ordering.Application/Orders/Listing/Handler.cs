﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Ordering.Application.Orders.Listing;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Query, PagedResult<Order>>
{
    public async Task<PagedResult<Order>> Handle(Query request, CancellationToken cancellationToken)
    {
        var count = await context.Orders.CountAsync(cancellationToken);
        var orders = await context.Orders.AsNoTracking().Skip(request.Page).Take(request.Page * request.Size).ToListAsync(cancellationToken);

        return new PagedResult<Order>
        {
            Page = request.Page,
            Data = orders,
            Messages = ["Siparişler başarılı şekilde getirildi."],
            PageRecordsCount = request.Size,
            RecordsCount = count
        };
    }
}
