using Asp.Versioning.Builder;
using Asp.Versioning;
using Warehouse.Api.RouteExtensions;
using MassTransit;
using OpenTelemetry.Trace;
using OpenTelemetry.Resources;
using Warehouse.Application;
using Warehouse.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring OpenAPI at https://aka.ms/aspnet/openapi
builder.Services.AddOpenApi();
builder.Services.AddHttpContextAccessor();
builder.Services.AddApiVersioning();
builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices();
builder
    .Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
    {
#if DEBUG
        o.RequireHttpsMetadata = false;
#endif

        o.Authority = builder.Configuration["IdentityServerUrl"];
        o.Audience = "ApiResourceWarehouse";
    });
builder.Services.AddAuthorization();

//Tracing
//builder
//    .Services
//    .AddOpenTelemetry()
//    .ConfigureResource(resource => resource.AddService("Warehouse.Api"))
//    .WithTracing(tracing =>
//    {
//        tracing
//            .AddAspNetCoreInstrumentation()
//            .AddHttpClientInstrumentation()
//            //.AddSqlClientInstrumentation(o => o.SetDbStatementForText = true)
//            .AddSource(MassTransit.Logging.DiagnosticHeaders.DefaultListenerName);

//        tracing.AddOtlpExporter();
//    });

//Masstransit
//builder.Services.AddMassTransit(configure =>
//{
//    configure.UsingRabbitMq((context, configure) =>
//    {
//        configure.Host("rabbitmq", configure =>
//        {
//            configure.Username("guest");
//            configure.Password("guest");
//        });

//        configure.ConfigureEndpoints(context);
//    });
//});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.MapOpenApi();
}

//app.UseHttpsRedirection();

//Versioning
ApiVersionSet apiVersionSet = app.NewApiVersionSet()
    .HasApiVersion(new ApiVersion(1))
    .ReportApiVersions()
    .Build();

app.UseFloorRoutes(apiVersionSet);
app.UseHallwayRoutes(apiVersionSet);
app.UseLimitRoutes(apiVersionSet);
app.UsePivotRoutes(apiVersionSet);
app.UseProductRoutes(apiVersionSet);
app.UseShelfRoutes(apiVersionSet);
app.UseWarehouseRoutes(apiVersionSet);
app.UseWorkOrderRoutes(apiVersionSet);

app.UseAuthentication();
app.UseAuthorization();

app.Run();