﻿using Asp.Versioning.Builder;
using MediatR;

namespace Warehouse.Api.RouteExtensions;

public static class LimitExtension
{
    public static void UseLimitRoutes(this WebApplication app, ApiVersionSet apiVersionSet)
    {
        RouteGroupBuilder routeGroupBuilder = app
            .MapGroup("v{version:apiVersion}/Limits")
            .WithApiVersionSet(apiVersionSet)
            .RequireAuthorization();

        routeGroupBuilder
            .MapPost("", async (
                ISender sender,
                Application.Limits.Create.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Created($"/{result.Data}", result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Limits.Create")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPut("", async (
                ISender sender,
                Application.Limits.Update.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Limits.Update")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("", async (
                ISender sender,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(new Application.Limits.Detail.Query(), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Limits.Detail")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
}
