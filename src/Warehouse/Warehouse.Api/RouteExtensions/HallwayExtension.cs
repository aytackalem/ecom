﻿using Asp.Versioning.Builder;
using MediatR;
using System.Threading;

namespace Warehouse.Api.RouteExtensions;

public static class HallwayExtension
{
    public static void UseHallwayRoutes(this WebApplication app, ApiVersionSet apiVersionSet)
    {
        RouteGroupBuilder routeGroupBuilder = app
            .MapGroup("v{version:apiVersion}/Hallways")
            .WithApiVersionSet(apiVersionSet)
            .RequireAuthorization();

        routeGroupBuilder
            .MapPost("", async (
                ISender sender,
                Application.Hallways.Create.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Created($"/{result.Data}", result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Hallways.Create")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPut("", async (
                ISender sender,
                Application.Hallways.Update.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Hallways.Update")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("", async (
                ISender sender,
                CancellationToken cancellationToken,
                int page,
                int pageRecordsCount,
                int? floorId = null) =>
            {
                var result = await sender.Send(new Application.Hallways.Listing.Query(page, pageRecordsCount, floorId), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Hallways.Listing")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("/{id}", async (
                ISender sender,
                int id,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(new Application.Hallways.Detail.Query(id), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Hallways.Detail")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
}
