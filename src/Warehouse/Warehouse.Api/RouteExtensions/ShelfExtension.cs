﻿using Asp.Versioning.Builder;
using MediatR;
using System.Threading;

namespace Warehouse.Api.RouteExtensions;

public static class ShelfExtension
{
    public static void UseShelfRoutes(this WebApplication app, ApiVersionSet apiVersionSet)
    {
        RouteGroupBuilder routeGroupBuilder = app
            .MapGroup("v{version:apiVersion}/Shelves")
            .WithApiVersionSet(apiVersionSet)
            .RequireAuthorization();

        routeGroupBuilder
            .MapPost("", async (
                ISender sender,
                Warehouse.Application.Shelves.Create.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Created($"/{result.Data}", result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Shelves.Create")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPut("", async (
                ISender sender,
                Warehouse.Application.Shelves.Update.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Shelves.Update")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("", async (
                ISender sender,
                CancellationToken cancellationToken,
                int page,
                int pageRecordsCount,
                int? hallwayId = null) =>
            {
                var result = await sender.Send(new Application.Shelves.Listing.Query(
                    page, 
                    pageRecordsCount, 
                    hallwayId), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Shelves.Listing")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("/{id}", async (
                ISender sender,
                int id,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(new Application.Shelves.Detail.Query(id), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Shelves.Detail")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
}
