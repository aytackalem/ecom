﻿using Asp.Versioning.Builder;
using MediatR;

namespace Warehouse.Api.RouteExtensions;

public static class ProductExtension
{
    public static void UseProductRoutes(this WebApplication app, ApiVersionSet apiVersionSet)
    {
        RouteGroupBuilder routeGroupBuilder = app
            .MapGroup("v{version:apiVersion}/Products")
            .WithApiVersionSet(apiVersionSet)
            .RequireAuthorization();

        routeGroupBuilder
            .MapPost("", async (
                ISender sender,
                Warehouse.Application.Products.Create.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Created($"/{result.Data}", result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Products.Create")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPost("Move", async (
                ISender sender,
                Warehouse.Application.Products.Move.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Products.Move")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPut("Decrease", async (
                ISender sender,
                Warehouse.Application.Products.Decrease.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Products.Decrease")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("", async (
                ISender sender,
                CancellationToken cancellationToken,
                int page,
                int pageRecordsCount,
                string? barcode) =>
            {
                var result = await sender.Send(new Application.Products.Listing.Query(
                    page,
                    pageRecordsCount,
                    barcode));

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Products.Listing")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
}
