﻿using Asp.Versioning.Builder;
using MediatR;

namespace Warehouse.Api.RouteExtensions;

public static class FloorExtension
{
    public static void UseFloorRoutes(this WebApplication app, ApiVersionSet apiVersionSet)
    {
        RouteGroupBuilder routeGroupBuilder = app
            .MapGroup("v{version:apiVersion}/Floors")
            .WithApiVersionSet(apiVersionSet)
            .RequireAuthorization();

        routeGroupBuilder
            .MapPost("", async (
                ISender sender,
                Application.Floors.Create.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Created($"/{result.Data}", result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Floors.Create")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapPut("", async (
                ISender sender,
                Application.Floors.Update.Command command,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(command, cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Floors.Update")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("", async (
                ISender sender,
                CancellationToken cancellationToken,
                int page,
                int pageRecordsCount,
                int? warehouseId = null) =>
            {
                var result = await sender.Send(new Application.Floors.Listing.Query(page, pageRecordsCount, warehouseId), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Floors.Listing")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);

        routeGroupBuilder
            .MapGet("/{id}", async (
                ISender sender,
                int id,
                CancellationToken cancellationToken) =>
            {
                var result = await sender.Send(new Application.Floors.Detail.Query(id), cancellationToken);

                if (result.Success)
                {
                    return Results.Ok(result);
                }
                else if (result.Invalid)
                {
                    return Results.BadRequest(result);
                }

                return Results.StatusCode(500);
            })
            .WithName("Floors.Detail")
            .WithOpenApi()
            .MapToApiVersion(1)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status500InternalServerError);
    }
}
