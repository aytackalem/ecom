﻿namespace Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder;

public class OrderDetail
{
    public string Name { get; set; }

    public int Quantity { get; set; }

    public string ModelCode { get; set; }

    public string Barcode { get; set; }

    public string PhotoUrl { get; set; }
}
