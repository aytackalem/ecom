﻿namespace Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder;

public class Response
{
    public int Code { get; set; }

    public string Message { get; set; }

    public List<Order> Data { get; set; }
}
