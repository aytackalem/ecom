﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Configuration;
using System.Net.Http.Json;

namespace Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder;

public class Client(IConfiguration Configuration, IDomainFinder DomainFinder, ICompanyFinder CompanyFinder)
{
    public async Task<Response> Post(bool single, int limit, string? marketplaceId, string token, CancellationToken cancellationToken)
    {
        var orderingApiUrl = Configuration["OrderingApiUrl"];
        var orderingApiVersion = Configuration["OrderingApiVersion"];

        using (HttpClient httpClient = new())
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            httpClient.DefaultRequestHeaders.Add("DomainId", DomainFinder.FindId().ToString());
            httpClient.DefaultRequestHeaders.Add("CompanyId", CompanyFinder.FindId().ToString());

            var httpResponseMessage = await httpClient.PostAsJsonAsync($"{orderingApiUrl}/{orderingApiVersion}/Orders/ReserveForWorkOrder", new
            {
                limit,
                single,
                marketplaceId
            }, cancellationToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                return await httpResponseMessage.Content.ReadFromJsonAsync<Response>(cancellationToken);
            }
        }

        return new Response
        {
            Data = null
        };
    }
}
