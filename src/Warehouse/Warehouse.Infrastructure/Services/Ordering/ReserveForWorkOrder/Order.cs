﻿namespace Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder;

public class Order
{
    public int Id { get; set; }

    public List<OrderDetail> Details { get; set; }
}
