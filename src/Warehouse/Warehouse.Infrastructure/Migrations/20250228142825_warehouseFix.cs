﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Warehouse.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class warehouseFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDateTime",
                table: "WorkOrders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PackedDateTime",
                table: "WorkOrders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PackingDateTime",
                table: "WorkOrders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "PackingTime",
                table: "WorkOrders",
                type: "time",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PickedDateTime",
                table: "WorkOrders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PickingDateTime",
                table: "WorkOrders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "PickingTime",
                table: "WorkOrders",
                type: "time",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedDateTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PackedDateTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PackingDateTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PackingTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PickedDateTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PickingDateTime",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "PickingTime",
                table: "WorkOrders");
        }
    }
}
