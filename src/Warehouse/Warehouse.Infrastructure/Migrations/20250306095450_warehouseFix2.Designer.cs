﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Warehouse.Infrastructure.Persistence;

#nullable disable

namespace Warehouse.Infrastructure.Migrations
{
    [DbContext(typeof(WarehouseDbContext))]
    [Migration("20250306095450_warehouseFix2")]
    partial class warehouseFix2
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "9.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("Warehouse.Domain.Floors.Floor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("SortNumber")
                        .HasColumnType("int");

                    b.Property<int>("WarehouseId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("WarehouseId", "SortNumber")
                        .IsUnique();

                    b.ToTable("Floors");
                });

            modelBuilder.Entity("Warehouse.Domain.Hallways.Hallway", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("FloorId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("SortNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("FloorId", "Name")
                        .IsUnique();

                    b.HasIndex("FloorId", "SortNumber")
                        .IsUnique();

                    b.ToTable("Hallways");
                });

            modelBuilder.Entity("Warehouse.Domain.Limits.Limit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("Multiple")
                        .HasColumnType("int");

                    b.Property<int>("Single")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Limits");
                });

            modelBuilder.Entity("Warehouse.Domain.Pivots.Pivot", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("WarehouseId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("WarehouseId");

                    b.ToTable("Pivots");
                });

            modelBuilder.Entity("Warehouse.Domain.Pivots.PivotShelf", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PivotId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PivotId");

                    b.ToTable("PivotShelves");
                });

            modelBuilder.Entity("Warehouse.Domain.Products.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Barcode")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("ShelfId")
                        .HasColumnType("int");

                    b.Property<int>("Stock")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ShelfId", "Barcode")
                        .IsUnique();

                    b.ToTable("Products");
                });

            modelBuilder.Entity("Warehouse.Domain.Shelves.Shelf", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("HallwayId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("SortNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("HallwayId", "Name")
                        .IsUnique();

                    b.HasIndex("HallwayId", "SortNumber")
                        .IsUnique();

                    b.ToTable("Shelves");
                });

            modelBuilder.Entity("Warehouse.Domain.Warehouses.Warehouse", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Warehouses");
                });

            modelBuilder.Entity("Warehouse.Domain.WorkOrders.WorkOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("CreatedDateTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("PackedDateTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("PackingDateTime")
                        .HasColumnType("datetime2");

                    b.Property<TimeSpan?>("PackingTime")
                        .HasColumnType("time");

                    b.Property<string>("PersonelName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("PickedDateTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("PickingDateTime")
                        .HasColumnType("datetime2");

                    b.Property<TimeSpan?>("PickingTime")
                        .HasColumnType("time");

                    b.Property<bool>("Single")
                        .HasColumnType("bit");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("WorkOrders");
                });

            modelBuilder.Entity("Warehouse.Domain.WorkOrders.WorkOrderItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Barcode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("InvoiceNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ModelCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("OrderId")
                        .HasColumnType("int");

                    b.Property<string>("PhotoUrl")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("PivotShelfId")
                        .HasColumnType("int");

                    b.Property<int>("PuttedQuantity")
                        .HasColumnType("int");

                    b.Property<int>("Quantity")
                        .HasColumnType("int");

                    b.Property<string>("Shelf")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SortNumber")
                        .HasColumnType("int");

                    b.Property<int>("WorkOrderId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PivotShelfId");

                    b.HasIndex("WorkOrderId");

                    b.ToTable("WorkOrderItems");
                });

            modelBuilder.Entity("Warehouse.Domain.Floors.Floor", b =>
                {
                    b.HasOne("Warehouse.Domain.Warehouses.Warehouse", "Warehouse")
                        .WithMany()
                        .HasForeignKey("WarehouseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Warehouse");
                });

            modelBuilder.Entity("Warehouse.Domain.Hallways.Hallway", b =>
                {
                    b.HasOne("Warehouse.Domain.Floors.Floor", "Floor")
                        .WithMany()
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Floor");
                });

            modelBuilder.Entity("Warehouse.Domain.Pivots.Pivot", b =>
                {
                    b.HasOne("Warehouse.Domain.Warehouses.Warehouse", "Warehouse")
                        .WithMany()
                        .HasForeignKey("WarehouseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Warehouse");
                });

            modelBuilder.Entity("Warehouse.Domain.Pivots.PivotShelf", b =>
                {
                    b.HasOne("Warehouse.Domain.Pivots.Pivot", "Pivot")
                        .WithMany()
                        .HasForeignKey("PivotId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Pivot");
                });

            modelBuilder.Entity("Warehouse.Domain.Products.Product", b =>
                {
                    b.HasOne("Warehouse.Domain.Shelves.Shelf", "Shelf")
                        .WithMany()
                        .HasForeignKey("ShelfId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Shelf");
                });

            modelBuilder.Entity("Warehouse.Domain.Shelves.Shelf", b =>
                {
                    b.HasOne("Warehouse.Domain.Hallways.Hallway", "Hallway")
                        .WithMany()
                        .HasForeignKey("HallwayId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Hallway");
                });

            modelBuilder.Entity("Warehouse.Domain.WorkOrders.WorkOrderItem", b =>
                {
                    b.HasOne("Warehouse.Domain.Pivots.PivotShelf", "PivotShelf")
                        .WithMany()
                        .HasForeignKey("PivotShelfId");

                    b.HasOne("Warehouse.Domain.WorkOrders.WorkOrder", null)
                        .WithMany("Items")
                        .HasForeignKey("WorkOrderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PivotShelf");
                });

            modelBuilder.Entity("Warehouse.Domain.WorkOrders.WorkOrder", b =>
                {
                    b.Navigation("Items");
                });
#pragma warning restore 612, 618
        }
    }
}
