﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Warehouse.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class warehouseFix2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "WorkOrders");

            migrationBuilder.AddColumn<string>(
                name: "PersonelName",
                table: "WorkOrders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PersonelName",
                table: "WorkOrders");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "WorkOrders",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
