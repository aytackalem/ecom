﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.Hallways;

namespace Warehouse.Infrastructure.Persistence.Configurations;

public sealed class HallwayConfiguration : IEntityTypeConfiguration<Hallway>
{
    public void Configure(EntityTypeBuilder<Hallway> builder)
    {
        builder.HasIndex(_ => new { _.FloorId, _.Name }).IsUnique();
        builder.HasIndex(_ => new { _.FloorId, _.SortNumber }).IsUnique();
    }
}
