﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.Floors;

namespace Warehouse.Infrastructure.Persistence.Configurations;

public sealed class FloorConfiguration : IEntityTypeConfiguration<Floor>
{
    public void Configure(EntityTypeBuilder<Floor> builder)
    {
        builder.HasIndex(_ => _.Name).IsUnique();
        builder.HasIndex(_ => new { _.WarehouseId, _.SortNumber }).IsUnique();
    }
}
