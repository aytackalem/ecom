﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Warehouse.Infrastructure.Persistence.Configurations;

public sealed class WarehouseConfiguration : IEntityTypeConfiguration<Domain.Warehouses.Warehouse>
{
    public void Configure(EntityTypeBuilder<Domain.Warehouses.Warehouse> builder)
    {
        builder.HasIndex(_ => _.Name).IsUnique();
    }
}
