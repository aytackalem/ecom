﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.Shelves;

namespace Warehouse.Infrastructure.Persistence.Configurations;

public sealed class ShelfConfiguration : IEntityTypeConfiguration<Shelf>
{
    public void Configure(EntityTypeBuilder<Shelf> builder)
    {
        builder.HasIndex(_ => new { _.HallwayId, _.Name }).IsUnique();
        builder.HasIndex(_ => new { _.HallwayId, _.SortNumber }).IsUnique();
    }
}
