﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Warehouse.Domain.Floors;
using Warehouse.Domain.Hallways;
using Warehouse.Domain.Limits;
using Warehouse.Domain.Pivots;
using Warehouse.Domain.Products;
using Warehouse.Domain.Shelves;
using Warehouse.Domain.WorkOrders;
using Warehouse.Infrastructure.Persistence.Configurations;

namespace Warehouse.Infrastructure.Persistence;

public sealed class WarehouseDbContext : DbContext
{
    private readonly IConfiguration _configuration;

    private readonly IDbNameFinder _dbNameFinder;

    public WarehouseDbContext()
    {
        //_configuration = configuration;
        //_dbNameFinder = dbNameFinder;

        //Database.EnsureCreated();
    }

    public DbSet<Floor> Floors { get; set; }

    public DbSet<Hallway> Hallways { get; set; }

    public DbSet<Limit> Limits { get; set; }

    public DbSet<Pivot> Pivots { get; set; }

    public DbSet<PivotShelf> PivotShelves { get; set; }

    public DbSet<Product> Products { get; set; }

    public DbSet<Shelf> Shelves { get; set; }

    public DbSet<Domain.Warehouses.Warehouse> Warehouses { get; set; }

    public DbSet<WorkOrder> WorkOrders { get; set; }

    public DbSet<WorkOrderItem> WorkOrderItems { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //var connectionStringTemplate = _configuration.GetConnectionString("Helpy");

        //var dbName = _dbNameFinder.FindName();

        //optionsBuilder.UseSqlServer(string.Format(connectionStringTemplate, dbName));

        //base.OnConfiguring(optionsBuilder);


        var connectionString = "Server=172.31.57.11;Database=Warehouse_HelpyTest;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;trustServerCertificate=true";
        optionsBuilder.UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        new FloorConfiguration().Configure(modelBuilder.Entity<Floor>());
        new HallwayConfiguration().Configure(modelBuilder.Entity<Hallway>());
        new ProductConfiguration().Configure(modelBuilder.Entity<Product>());
        new ShelfConfiguration().Configure(modelBuilder.Entity<Domain.Shelves.Shelf>());
        new WarehouseConfiguration().Configure(modelBuilder.Entity<Domain.Warehouses.Warehouse>());

        base.OnModelCreating(modelBuilder);
    }
}
