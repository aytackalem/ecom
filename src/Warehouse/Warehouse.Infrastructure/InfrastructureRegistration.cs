﻿using Microsoft.Extensions.DependencyInjection;
using Warehouse.Infrastructure.Persistence;
using Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder;

namespace Warehouse.Infrastructure;

public static class InfrastructureRegistration
{
    public static void AddInfrastructureServices(this IServiceCollection services)
    {
        services.AddScoped<Client>();
        services.AddDbContext<WarehouseDbContext>();
    }
}
