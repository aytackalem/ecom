﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Hallways;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Hallways.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Hallway>>
{
    public async Task<PagedResult<Hallway>> Handle(Query request, CancellationToken cancellationToken)
    {
        return new PagedResult<Hallway>
        {
            RecordsCount = await context.Hallways.CountAsync(cancellationToken),
            Data = await context.Hallways.Where(_ => request.FloorId == null || _.FloorId == request.FloorId.Value).Skip(request.Page * request.PageRecordsCount).Take(request.PageRecordsCount).ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Koridorlar başarılı şekilde getirildi."]
        };
    }
}
