﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Hallways;

namespace Warehouse.Application.Hallways.Listing;

public sealed record Query(int Page, int PageRecordsCount, int? FloorId = null) : IRequest<PagedResult<Hallway>>;