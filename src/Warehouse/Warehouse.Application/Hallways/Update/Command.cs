﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Hallways.Update;

public sealed record Command(int Id, string Name, int SortNumber, int FloorId) : IRequest<Result>;