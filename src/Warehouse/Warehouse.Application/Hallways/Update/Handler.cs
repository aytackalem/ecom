﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Hallways.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var hallway = await context.Hallways.FirstOrDefaultAsync(_ => _.Id == request.Id);

        if (hallway is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Koridor bulunamadı."]
            };
        }

        hallway.Name = request.Name;
        hallway.SortNumber = request.SortNumber;
        hallway.FloorId = request.FloorId;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme başarılı."]
        };
    }
}
