﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Hallways;

namespace Warehouse.Application.Hallways.Detail;

public sealed record Query(int Id) : IRequest<Result<Hallway>>;