﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Hallways;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Hallways.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Hallway>>
{
    public async Task<Result<Hallway>> Handle(Query request, CancellationToken cancellationToken)
    {
        var shelf = await context.Hallways.Include(_=>_.Floor.Warehouse).FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (shelf is null)
        {
            return new Result<Hallway>
            {
                Failed = true,
                Messages = ["Koridor bulunamadı."]
            };
        }

        return new Result<Hallway>
        {
            Data = shelf,
            Messages = ["Koridor başarılı şekilde getirildi."]
        };
    }
}
