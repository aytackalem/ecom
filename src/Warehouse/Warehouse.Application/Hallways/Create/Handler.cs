﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Hallways;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Hallways.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var hallway = new Hallway
        {
            FloorId = request.FloorId,
            Name = request.Name,
            SortNumber = request.SortNumber
        };
        context.Hallways.Add(hallway);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = hallway.Id,
            Messages = ["Koridor başarılı şekilde kaydedildi."]
        };
    }
}
