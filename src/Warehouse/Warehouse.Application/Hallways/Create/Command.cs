﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Hallways.Create;

public record Command(string Name, int SortNumber, int FloorId) : IRequest<Result<int>>;