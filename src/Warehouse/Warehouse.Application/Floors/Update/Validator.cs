﻿using FluentValidation;

namespace Warehouse.Application.Floors.Update;

public sealed class Validator : AbstractValidator<Command>
{
    public Validator()
    {
        RuleFor(_ => _.Id).GreaterThan(0).WithMessage("Id bilgisi '0' dan büyük olmalıdır.");
        RuleFor(_ => _.WarehouseId).GreaterThan(0).WithMessage("Depo bilgisi yanlış veya eksik.");
        RuleFor(_ => _.SortNumber).GreaterThan(0).WithMessage("Sıra numarası yanlış veya eksik.");
        RuleFor(_ => _.Name)
            .Matches("^[A-Za-z0-9. ]+$").WithMessage("İsim bilgisi sadece harf, sayı, nokta ve boşluktan oluşabilir.")
            .MaximumLength(50).WithMessage("İsim bilgisi maksimum '50' karakter uzunluğunda olmalıdır.")
            .NotEmpty().WithMessage("İsim bilgisi yanlış veya eksik.")
            .NotNull().WithMessage("İsim bilgisi yanlış veya eksik.");
    }
}
