﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Floors.Update;

public sealed record Command(int Id, string Name, int SortNumber, int WarehouseId) : IRequest<Result>;
