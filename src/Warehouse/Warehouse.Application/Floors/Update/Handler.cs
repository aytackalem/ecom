﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Floors.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var floor = await context.Floors.FirstOrDefaultAsync(_ => _.Id == request.Id);

        if (floor is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Kat bulunamadı."]
            };
        }

        floor.Name = request.Name;
        floor.SortNumber = request.SortNumber;
        floor.WarehouseId = request.WarehouseId;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme başarılı."]
        };
    }
}
