﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Floors;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Floors.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Floor>>
{
    public async Task<PagedResult<Floor>> Handle(Query request, CancellationToken cancellationToken)
    {
        return new PagedResult<Floor>
        {
            RecordsCount = await context.Floors.CountAsync(cancellationToken),
            Data = await context.Floors.Where(_ => request.WarehouseId == null || _.WarehouseId == request.WarehouseId.Value).Skip(request.Page * request.PageRecordsCount).Take(request.PageRecordsCount).ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Koridorlar başarılı şekilde getirildi."]
        };
    }
}