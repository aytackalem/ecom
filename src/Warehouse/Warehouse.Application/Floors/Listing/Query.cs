﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Floors;

namespace Warehouse.Application.Floors.Listing;

public sealed record Query(int Page, int PageRecordsCount, int? WarehouseId = null) : IRequest<PagedResult<Floor>>;
