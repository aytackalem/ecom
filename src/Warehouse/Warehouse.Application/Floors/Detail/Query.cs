﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Floors;

namespace Warehouse.Application.Floors.Detail;

public sealed record Query(int Id) : IRequest<Result<Floor>>;