﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Floors;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Floors.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Floor>>
{
    public async Task<Result<Floor>> Handle(Query request, CancellationToken cancellationToken)
    {
        var floor = await context.Floors.Include(_ => _.Warehouse).FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (floor is null)
        {
            return new Result<Floor>
            {
                Failed = true,
                Messages = ["Kat bulunamadı."]
            };
        }

        return new Result<Floor>
        {
            Data = floor,
            Messages = ["Kat başarılı şekilde getirildi."]
        };
    }
}
