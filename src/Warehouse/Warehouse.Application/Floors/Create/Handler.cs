﻿using MediatR;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Floors.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var floor = new Domain.Floors.Floor
        {
            Name = request.Name,
            SortNumber = request.SortNumber,
            WarehouseId = request.WarehouseId
        };
        context.Floors.Add(floor);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = floor.Id,
            Messages = ["Kat başarılı şekilde kaydedildi."]
        };
    }
}
