﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Floors.Create;

public record Command(string Name, int SortNumber, int WarehouseId) : IRequest<Result<int>>;