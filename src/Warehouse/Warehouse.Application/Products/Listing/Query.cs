﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Products.Listing;

public sealed record Query(int Page, int PageRecordsCount, string? Barcode) : IRequest<PagedResult<Product>>;
