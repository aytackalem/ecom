﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using System.Linq.Expressions;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Products.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Product>>
{
    public async Task<PagedResult<Product>> Handle(Query request, CancellationToken cancellationToken)
    {
        Expression<Func<Domain.Products.Product, bool>> predicate = _ => string.IsNullOrEmpty(request.Barcode) || _.Barcode == request.Barcode;

        return new PagedResult<Product>
        {
            RecordsCount = await context
                .Products
                .CountAsync(predicate, cancellationToken),
            Data = await context
                .Products
                .Where(predicate)
                .Skip(request.Page * request.PageRecordsCount)
                .Take(request.PageRecordsCount)
                .Select(_ => new Product
                {
                    Id = _.Id,
                    Barcode = _.Barcode,
                    Stock = _.Stock,
                    Shelf = _.Shelf.Name
                })
                .ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Ürünler başarılı şekilde getirildi."]
        };
    }
}
