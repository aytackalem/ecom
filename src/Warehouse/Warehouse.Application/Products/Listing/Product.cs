﻿namespace Warehouse.Application.Products.Listing;

public class Product
{
    public int Id { get; set; }

    public string Barcode { get; set; }

    public int Stock { get; set; }

    public string Shelf { get; set; }
}
