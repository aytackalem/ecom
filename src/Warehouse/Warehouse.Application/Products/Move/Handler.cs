﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Products;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Products.Move;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        Product[] products = new Product[request.Items.Count];

        for (int i = 0; i < products.Length; i++)
        {
            var product = await context.Products.FirstOrDefaultAsync(_ => _.ShelfId == request.Items[i].ShelfId && _.Barcode == request.Items[i].Barcode, cancellationToken);

            if (product is null)
            {
                return new Result
                {
                    Failed = true,
                    Messages = [$"{request.Items[i].Barcode} barkodlu ürün rafta bulunamadı."]
                };
            }

            if (request.Items[i].Stock > product.Stock)
            {
                return new Result
                {
                    Failed = true,
                    Messages = [$"{request.Items[i].Barcode} barkodlu ürün için yeterli stok bulunamadı."]
                };
            }

            products[i] = product;
        }

        for (int i = 0; i < products.Length; i++)
        {
            var product = await context.Products.FirstOrDefaultAsync(_ => _.Barcode == products[i].Barcode && _.ShelfId == request.ShelfId);

            if (product is null)
            {
                product = new()
                {
                    Barcode = products[i].Barcode,
                    ShelfId = request.ShelfId,
                    Stock = request.Items[i].Stock
                };

                context.Products.Add(product);
            }
            else
            {
                product.Stock += request.Items[i].Stock;
            }

            if (products[i].Stock > request.Items[i].Stock)
            {
                products[i].Stock -= request.Items[i].Stock;
            }
            else
            {
                context.Products.Remove(products[i]);
            }

        }

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Taşıma işlemi başarılı şekilde gerçekleşti."]
        };
    }
}
