﻿using FluentValidation;

namespace Warehouse.Application.Products.Move;

public class Validator : AbstractValidator<Command>
{
    public Validator()
    {
        RuleFor(_ => _.ShelfId)
            .GreaterThan(0).WithMessage("Raf Id bilgisi '0' dan büyük olmalıdır.");
        RuleFor(_ => _.Items)
            .NotNull().WithMessage("En az bir tane taşınacak ürün olmalıdır.");

        RuleForEach(_ => _.Items).SetValidator(new ItemValidator());
    }
}

public class ItemValidator : AbstractValidator<Item>
{
    public ItemValidator()
    {
        RuleFor(_ => _.ShelfId)
            .GreaterThan(0).WithMessage("Raf Id bilgisi '0' dan büyük olmalıdır.");
        RuleFor(_ => _.Barcode)
            .Matches("^[A-Za-z0-9]+$").WithMessage("Barkod bilgisi sadece harf ve sayılardan oluşabilir.")
            .MaximumLength(25).WithMessage("Barkod maksimum '50' karakter uzunluğunda olmalıdır.")
            .NotEmpty().WithMessage("Barkod bilgisi yanlış veya eksik.")
            .NotNull().WithMessage("Barkod bilgisi yanlış veya eksik.");
        RuleFor(_ => _.Stock)
            .GreaterThan(0).WithMessage("Stok bilgisi '0' dan büyük olmalıdır.");
    }
}