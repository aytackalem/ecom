﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Products.Move;

public record Command(List<Item> Items, int ShelfId) : IRequest<Result>;

public record Item(int ShelfId, string Barcode, int Stock);