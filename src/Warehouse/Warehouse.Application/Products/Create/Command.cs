﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Products.Create;

public sealed record Command(int ShelfId, string Barcode, int Stock) : IRequest<Result<int>>;