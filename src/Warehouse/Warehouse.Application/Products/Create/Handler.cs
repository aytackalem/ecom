﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Products.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var product = await context.Products.FirstOrDefaultAsync(_ => _.ShelfId == request.ShelfId && _.Barcode == request.Barcode, cancellationToken);

        string message = string.Empty;

        if (product is null)
        {
            product = new Domain.Products.Product
            {
                Barcode = request.Barcode,
                ShelfId = request.ShelfId,
                Stock = request.Stock
            };

            context.Products.Add(product);

            message = "Ürünler rafa başarılı şekilde kaydedildi.";
        }
        else
        {
            product.Stock += request.Stock;

            message = "Rafta ürün bulundu. Stok güncellendi.";
        }

        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = product.Id,
            Messages = [message]
        };
    }
}
