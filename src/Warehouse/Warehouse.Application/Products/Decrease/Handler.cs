﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Products.Decrease;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var product = await context.Products.FirstOrDefaultAsync(_ => _.ShelfId == request.ShelfId && _.Barcode == request.Barcode, cancellationToken);

        if (product is null)
        {
            return new Result
            {
                Messages = ["Kayıt bulunamadı."]
            };
        }

        if (product.Stock < request.Stock)
        {
            return new Result
            {
                Messages = ["Yeterli stok bulunamadı."]
            };
        }

        if (product.Stock == request.Stock)
        {
            context.Products.Remove(product);
        }
        else
        {
            product.Stock -= request.Stock;
        }

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme işlemi başarılı."]
        };
    }
}
