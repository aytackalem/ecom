﻿using ECommerce.Application.Common.Interfaces.Helpers;
using FluentValidation;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;
using Shared.Behaviours;
using Shared.Finders;
using System.Reflection;
using Warehouse.Infrastructure;

namespace Warehouse.Application;

public static class ApplicationRegistration
{
    public static void AddApplicationServices(this IServiceCollection services)
    {
        var assembly = Assembly.GetExecutingAssembly();

        //services.AddScoped<PublishDomainEventsInterceptor>();
        //services.AddDbContext<WarehouseDbContext>(/*(sp, options) => options.AddInterceptors(sp.GetRequiredService<PublishDomainEventsInterceptor>())*/);
        services.AddMediatR(_ => _.RegisterServicesFromAssembly(assembly));
        //services.AddScoped<IProductInformationStockService, ProductInformationStockService>();
        services.AddScoped<IDomainFinder, DomainFinder>();
        services.AddScoped<ICompanyFinder, CompanyFinder>();
        services.AddScoped<IDbNameFinder, DbNameFinder>();
        //services.AddScoped(typeof(IRequestExceptionHandler<,,>), typeof(RequestExceptionHandler<,,>));
        //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
        //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        //services.AddTransient<HttpHelperV4>();
        services.AddValidatorsFromAssembly(assembly);
    }
}
