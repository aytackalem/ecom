﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Shelves;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Shelves.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Shelf>>
{
    public async Task<Result<Shelf>> Handle(Query request, CancellationToken cancellationToken)
    {
        var shelf = await context.Shelves.Include(_ => _.Hallway.Floor.Warehouse).FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (shelf is null)
        {
            return new Result<Shelf>
            {
                Failed = true,
                Messages = ["Raf bulunamadı."]
            };
        }

        return new Result<Shelf>
        {
            Data = shelf,
            Messages = ["Raf başarılı şekilde getirildi."]
        };
    }
}
