﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Shelves;

namespace Warehouse.Application.Shelves.Detail;

public sealed record Query(int Id) : IRequest<Result<Shelf>>;