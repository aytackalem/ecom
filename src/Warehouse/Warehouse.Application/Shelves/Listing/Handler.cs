﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using System.Linq.Expressions;
using Warehouse.Domain.Shelves;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Shelves.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Shelf>>
{
    public async Task<PagedResult<Shelf>> Handle(Query request, CancellationToken cancellationToken)
    {
        Expression<Func<Shelf, bool>> predicate = _ => request.HallwayId == null || _.HallwayId == request.HallwayId.Value;

        return new PagedResult<Shelf>
        {
            RecordsCount = await context
                .Shelves
                .CountAsync(predicate, cancellationToken),
            Data = await context
                .Shelves
                .Where(predicate)
                .Skip(request.Page * request.PageRecordsCount)
                .Take(request.PageRecordsCount)
                .ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Raflar başarılı şekilde getirildi."]
        };
    }
}
