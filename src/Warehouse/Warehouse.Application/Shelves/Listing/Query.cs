﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Shelves;

namespace Warehouse.Application.Shelves.Listing;

public sealed record Query(int Page, int PageRecordsCount, int? HallwayId = null) : IRequest<PagedResult<Shelf>>;