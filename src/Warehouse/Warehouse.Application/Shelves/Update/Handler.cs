﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Shelves.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var shelf = await context.Shelves.FirstOrDefaultAsync(_ => _.Id == request.Id);

        if (shelf == null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Raf bulunamadı."]
            };
        }

        shelf.SortNumber = request.SortNumber;
        shelf.Name = request.Name;
        shelf.HallwayId = request.HallwayId;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme başarılı."]
        };
    }
}
