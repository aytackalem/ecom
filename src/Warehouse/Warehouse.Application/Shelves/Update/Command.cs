﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Shelves.Update;

public sealed record Command(int Id, string Name, int SortNumber, int HallwayId) : IRequest<Result>;