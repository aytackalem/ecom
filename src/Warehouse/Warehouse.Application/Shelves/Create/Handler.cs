﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Shelves;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Shelves.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var shelf = new Shelf
        {
            HallwayId = request.HallwayId,
            Name = request.Name,
            SortNumber = request.SortNumber
        };
        context.Shelves.Add(shelf);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = shelf.Id,
            Messages = ["Raf başarılı şekilde kaydedildi."]
        };
    }
}
