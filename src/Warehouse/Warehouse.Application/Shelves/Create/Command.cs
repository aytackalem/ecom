﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Shelves.Create;

public record Command(string Name, int SortNumber, int HallwayId) : IRequest<Result<int>>;