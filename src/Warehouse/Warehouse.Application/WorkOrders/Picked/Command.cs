﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Picked;

public record Command(int Id) : IRequest<Result>;
