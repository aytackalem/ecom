﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Picked;

public class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var workOrder = await context.WorkOrders.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (workOrder is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri bulunamadı."]
            };
        }

        if (workOrder.Status != Domain.WorkOrders.WorkOrderStatus.Picking)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Durumu 'Toplanıyor' olmayan iş emirleri 'Toplandı' durumuna güncellenemez."]
            };
        }

        if (workOrder.PickingDateTime.HasValue == false)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri paketleniyor zamanı kaydedilememiş."]
            };
        }

        workOrder.Status = Domain.WorkOrders.WorkOrderStatus.Picked;
        workOrder.PickedDateTime = DateTime.Now;
        workOrder.PickingTime = DateTime.Now - workOrder.PickingDateTime.Value;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["İş emri başarılı şekilde güncellendi."]
        };
    }
}
