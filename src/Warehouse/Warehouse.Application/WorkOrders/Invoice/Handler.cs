﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Invoice;

public class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var affectedRows = await context
            .WorkOrderItems
            .Where(_ => _.WorkOrderId == request.Id && _.OrderId == request.OrderId)
            .ExecuteUpdateAsync(_ => _.SetProperty(_ => _.InvoiceNumber, request.InvoiceNumber), cancellationToken: cancellationToken);

        return new Result
        {
            Failed = affectedRows == 0,
            Messages = [$"Fatura {(affectedRows == 0 ? "" : "")}"]
        };
    }
}
