﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Invoice;

public record Command(int Id, int OrderId, string InvoiceNumber) : IRequest<Result>;
