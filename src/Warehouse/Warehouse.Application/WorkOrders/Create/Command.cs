﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Create;

public sealed record Command(
    bool Single, 
    string? MarketplaceId,
    int? OrderId) : IRequest<Result<int>>;