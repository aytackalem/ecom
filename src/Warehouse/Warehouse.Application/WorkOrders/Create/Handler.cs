﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Shared.Results;
using Warehouse.Domain.Products;
using Warehouse.Domain.Shelves;
using Warehouse.Domain.WorkOrders;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Create;

public sealed class Handler(WarehouseDbContext Context, IConfiguration Configuration, IMediator Mediator, Warehouse.Infrastructure.Services.Ordering.ReserveForWorkOrder.Client client, IHttpContextAccessor HttpContextAccessor) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var username = HttpContextAccessor
            .HttpContext
            .User
            .Claims
            .FirstOrDefault(c => c.Type == "name")
            .Value;

        #region Limit
        var limitResult = await Mediator.Send(new Limits.Detail.Query(), cancellationToken);

        if (limitResult is null || limitResult.Data is null)
        {
            return new Result<int>()
            {
                Failed = true,
                Messages = limitResult.Messages
            };
        }
        #endregion

        #region Reserve
        if (HttpContextAccessor.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues value) == false)
        {
            return new Result<int>()
            {
                Failed = true,
                Messages = ["Yetki hatası."]
            };
        }

        var token = value[0].Replace("Bearer ", "");

        var reserveForWorkOrderResponse = await client.Post(
            request.Single,
            request.Single ? limitResult.Data.Single : limitResult.Data.Multiple,
            request.MarketplaceId,
            token,
            cancellationToken);

        if (reserveForWorkOrderResponse.Code != 200)
        {
            return new Result<int>()
            {
                Failed = true,
                Messages = ["Siparişler alınamadı."]
            };
        }
        #endregion

        var workOrder = new WorkOrder
        {
            CreatedDateTime = DateTime.Now,
            Single = request.Single,
            Status = WorkOrderStatus.Pending,
            PersonelName = username,
            Items = reserveForWorkOrderResponse
                .Data
                .SelectMany(order => order.Details.Select(detail => new WorkOrderItem
                {
                    ModelCode = detail.ModelCode,
                    Barcode = detail.Barcode,
                    Name = detail.Name,
                    OrderId = order.Id,
                    PhotoUrl = detail.PhotoUrl,
                    Quantity = detail.Quantity
                }))
                .ToList()
        };

        #region Route

        var products = new List<Product>();

        var shelves = new List<Shelf>();

        #region Get Shelves
        foreach (var item in workOrder.Items)
        {
            if (products.Any(_ => _.Barcode == item.Barcode) == false)
            {
                products.AddRange(await Context.Products.Where(product => product.Barcode == item.Barcode).ToListAsync(cancellationToken));
            }

            foreach (var product in products)
            {
                if (shelves.Any(shelf => shelf.Id == product.ShelfId) == false)
                {
                    shelves.Add(await Context.Shelves.Include(_ => _.Hallway.Floor.Warehouse).FirstOrDefaultAsync(shelf => shelf.Id == product.ShelfId));
                }
            }
        }
        #endregion

        shelves = [.. shelves
            .OrderBy(_=>_.Hallway.Floor.SortNumber)
            .ThenBy(_=>_.Hallway.SortNumber)
            .ThenBy(_=>_.SortNumber)];

        workOrder.Items.ForEach(item =>
        {
            var product = products.First(_ => _.Barcode == item.Barcode);

            var shelfIndex = shelves.FindIndex(_ => _.Id == product.ShelfId);

            item.SortNumber = shelfIndex;
            item.Shelf = shelves[shelfIndex].Name;
        });

        #endregion

        Context.WorkOrders.Add(workOrder);

        await Context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Messages = [$"{workOrder.Id} numaralı iş emri başarılı şekilde oluşturuldu."]
        };
    }
}
