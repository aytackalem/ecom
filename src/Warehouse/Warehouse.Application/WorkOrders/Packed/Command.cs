﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Packed;

public record Command(int Id) : IRequest<Result>;
