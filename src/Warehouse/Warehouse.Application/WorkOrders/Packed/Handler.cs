﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Packed;

public class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var workOrder = await context.WorkOrders.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (workOrder is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri bulunamadı."]
            };
        }

        if (workOrder.Status != Domain.WorkOrders.WorkOrderStatus.Packing)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Durumu 'Paketleniyor' olmayan iş emirleri 'Paketlendi' durumuna güncellenemez."]
            };
        }

        if (workOrder.PackingDateTime.HasValue == false)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri paketlenme zamanı kaydedilememiş."]
            };
        }

        workOrder.Status = Domain.WorkOrders.WorkOrderStatus.Packed;
        workOrder.PackedDateTime = DateTime.Now;
        workOrder.PackingTime = DateTime.Now - workOrder.PackingDateTime.Value;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["İş emri başarılı şekilde güncellendi."]
        };
    }
}
