﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Packing;

public class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var workOrder = await context.WorkOrders.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (workOrder is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri bulunamadı."]
            };
        }

        workOrder.Status = Domain.WorkOrders.WorkOrderStatus.Packing;
        workOrder.PackingDateTime = DateTime.Now;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["İş emri başarılı şekilde güncellendi."]
        };
    }
}
