﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Packing;

public record Command(int Id) : IRequest<Result>;
