﻿namespace Warehouse.Application.WorkOrders.KpiPicking;

public sealed record Item(
    int Id,
    DateTime Started,
    DateTime Finished,
    int Remain,
    string PersonelName);