﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.Picking;

public record Command(int Id) : IRequest<Result>;
