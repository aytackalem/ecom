﻿using FluentValidation;

namespace Warehouse.Application.WorkOrders.PutTheProduct;

public class Validator : AbstractValidator<Command>
{
    public Validator()
    {
        RuleFor(_ => _.WorkOrderItemId).GreaterThan(0).WithMessage("İş emri detay bilgisi yanlış veya hatalı.");
        RuleFor(_ => _.PivotShelfId).GreaterThan(0).WithMessage("Pivot raf bilgisi yanlış veya hatalı.");
    }
}
