﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.PutTheProduct;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var workOrderItem = await context.WorkOrderItems.FirstOrDefaultAsync(_ => _.Id == request.WorkOrderItemId);

        if (workOrderItem is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["İş emri detayı bulunamadı."]
            };
        }

        workOrderItem.PivotShelfId = request.PivotShelfId;
        workOrderItem.PuttedQuantity++;

        if (workOrderItem.PuttedQuantity > workOrderItem.Quantity)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Fazla ürün okutuldu."]
            };
        }

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Ürün rafa başarılı şekilde eklendi."]
        };
    }
}
