﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.PutTheProduct;

public sealed record Command(int WorkOrderItemId, int PivotShelfId) : IRequest<Result>;