﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using System.Linq.Expressions;
using Warehouse.Domain.WorkOrders;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.KpiPacking;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Item>>
{
    public async Task<PagedResult<Item>> Handle(Query request, CancellationToken cancellationToken)
    {
        Expression<Func<WorkOrder, bool>> predicate = _ =>
            _.PackingDateTime != null &&
            _.PackedDateTime != null &&
            _.PackingTime != null &&
            (string.IsNullOrEmpty(request.PersonelName) || _.PersonelName.Contains(request.PersonelName)) &&
            (request.Id.HasValue == false || _.Id == request.Id.Value);

        var recordsCount = await context
            .WorkOrders
            .Where(predicate)
            .AsNoTracking()
            .CountAsync(cancellationToken);

        List<Item> data = [];

        if (recordsCount > 0)
        {
            data = await context
                .WorkOrders
                .Where(predicate)
                .AsNoTracking()
                .Select(_ => new Item(
                    _.Id,
                    _.PackingDateTime!.Value,
                    _.PackedDateTime!.Value,
                    (int)_.PackingTime!.Value.TotalMinutes,
                    _.PersonelName))
                .ToListAsync(cancellationToken);
        }

        return new PagedResult<Item>
        {
            RecordsCount = recordsCount,
            Data = data,
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["KPI'lar başarılı şekilde getirildi."]
        };
    }
}
