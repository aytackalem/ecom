﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.WorkOrders.KpiPacking;

public sealed record Query(int Page, int PageRecordsCount, int? Id, string? PersonelName) : IRequest<PagedResult<Item>>;