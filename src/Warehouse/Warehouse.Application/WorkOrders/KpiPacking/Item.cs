﻿namespace Warehouse.Application.WorkOrders.KpiPacking;

public sealed record Item(
    int Id,
    DateTime Started,
    DateTime Finished,
    int Remain,
    string PersonelName);