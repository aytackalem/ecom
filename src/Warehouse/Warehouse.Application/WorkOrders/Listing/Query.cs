﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.WorkOrders;

namespace Warehouse.Application.WorkOrders.Listing;

/// <summary>
/// İş emirlerini listeleyen sorgu.
/// </summary>
/// <param name="Page">Görüntülenecek sayfa.</param>
/// <param name="PageRecordsCount">Sayfada görüntülenmesi istenilen kayıt sayısı.</param>
/// <param name="CreatedDateTime">Oluşturulma tarihi.</param>
/// <param name="Id">İş emri id bilgisi.</param>
/// <param name="OrderId">İş emri içerisinde yer alan sipariş id bilgisi.</param>
/// <param name="PersonelName">İş emrini oluşturan kullanıcı bilgisi.</param>
/// <param name="Status">İş emri durum bilgisi.</param>
public sealed record Query(
    int Page, 
    int PageRecordsCount,
    string? CreatedDateTime,
    int? Id, 
    int? OrderId, 
    string? PersonelName, 
    params int[]? Status) : IRequest<PagedResult<WorkOrder>>;