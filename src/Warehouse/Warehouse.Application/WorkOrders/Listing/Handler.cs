﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using System.Linq.Expressions;
using Warehouse.Domain.WorkOrders;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<WorkOrder>>
{
    public async Task<PagedResult<WorkOrder>> Handle(Query request, CancellationToken cancellationToken)
    {
        DateTime? createdDateTime = null;

        if (string.IsNullOrEmpty(request.CreatedDateTime) == false)
        {
            createdDateTime = DateTime.Parse(request.CreatedDateTime, new System.Globalization.CultureInfo("tr-TR"));
        }

        if (createdDateTime.HasValue && createdDateTime.Value > DateTime.Now)
        {
            return new PagedResult<WorkOrder>
            {
                Failed = true,
                Messages = ["İş emirleri ileri tarihli şekilde sorgulamaz."]
            };
        }

        Expression<Func<WorkOrder, bool>> predicate = _ =>
            (string.IsNullOrEmpty(request.PersonelName) || _.PersonelName.Contains(request.PersonelName)) &&
            (request.Status!.Length == 0 || request.Status!.Select(_ => (WorkOrderStatus)_).Contains(_.Status)) &&
            (request.Id.HasValue == false || _.Id == request.Id.Value) &&
            (request.OrderId.HasValue == false || _.Items.Any(_ => _.OrderId == request.OrderId.Value)) &&
            (createdDateTime.HasValue == false || _.CreatedDateTime.Date == createdDateTime.Value.Date);

        Int32 recordsCount = await context
            .WorkOrders
            .Where(predicate)
            .CountAsync(cancellationToken);

        List<WorkOrder> data = [];

        if (recordsCount > 0)
        {
            data = await context
                .WorkOrders
                .Where(predicate)
                .Skip(request.Page * request.PageRecordsCount)
                .Take(request.PageRecordsCount)
                .ToListAsync(cancellationToken);
        }

        return new PagedResult<WorkOrder>
        {
            RecordsCount = recordsCount,
            Data = data,
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["İş emirleri başarılı şekilde getirildi."]
        };
    }
}
