﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.WorkOrders;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.WorkOrders.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<WorkOrder>>
{
    public async Task<Result<WorkOrder>> Handle(Query request, CancellationToken cancellationToken)
    {
        var workOrder = await context.WorkOrders.Include(_ => _.Items).ThenInclude(_ => _.PivotShelf).FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (workOrder is null)
        {
            return new Result<WorkOrder>
            {
                Failed = true,
                Messages = ["İş emri bulunamadı."]
            };
        }

        return new Result<WorkOrder>
        {
            Data = workOrder,
            Messages = ["İş emri başarılı şekilde getirildi."]
        };
    }
}
