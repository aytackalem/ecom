﻿using FluentValidation;

namespace Warehouse.Application.WorkOrders.Detail;

public sealed class Validator : AbstractValidator<Query>
{
    public Validator()
    {
        RuleFor(_ => _.Id).GreaterThan(0).WithMessage("Id bilgisi '0' dan büyük olmalıdır.");
    }
}
