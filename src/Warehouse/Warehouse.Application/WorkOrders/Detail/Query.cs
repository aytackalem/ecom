﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.WorkOrders;

namespace Warehouse.Application.WorkOrders.Detail;

public sealed record Query(int Id) : IRequest<Result<WorkOrder>>;