﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Pivot>>
{
    public async Task<PagedResult<Pivot>> Handle(Query request, CancellationToken cancellationToken)
    {
        return new PagedResult<Pivot>
        {
            RecordsCount = await context.Pivots.CountAsync(cancellationToken),
            Data = await context.Pivots.Where(_ => request.WarehouseId == null || _.WarehouseId == request.WarehouseId.Value).Skip(request.Page * request.PageRecordsCount).Take(request.PageRecordsCount).ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Pivotlar başarılı şekilde getirildi."]
        };
    }
}
