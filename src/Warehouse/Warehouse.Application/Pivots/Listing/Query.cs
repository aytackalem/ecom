﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;

namespace Warehouse.Application.Pivots.Listing;

public sealed record Query(int Page, int PageRecordsCount, int? WarehouseId = null) : IRequest<PagedResult<Pivot>>;