﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var pivotShelf = await context.Pivots.FirstOrDefaultAsync(_ => _.Id == request.Id);

        if (pivotShelf == null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Pivot rafı bulunamadı."]
            };
        }

        pivotShelf.Name = request.Name;
        pivotShelf.WarehouseId = request.WarehouseId;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme başarılı."]
        };
    }
}
