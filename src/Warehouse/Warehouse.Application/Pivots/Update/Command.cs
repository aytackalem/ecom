﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Pivots.Update;

public sealed record Command(int Id, int WarehouseId, string Name) : IRequest<Result>;