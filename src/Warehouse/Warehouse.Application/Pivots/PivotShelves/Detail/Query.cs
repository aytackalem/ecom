﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;

namespace Warehouse.Application.Pivots.PivotShelves.Detail;

public sealed record Query(int Id) : IRequest<Result<PivotShelf>>;