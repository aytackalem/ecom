﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.PivotShelves.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<PivotShelf>>
{
    public async Task<Result<PivotShelf>> Handle(Query request, CancellationToken cancellationToken)
    {
        var pivotShelf = await context.PivotShelves.Include(_ => _.Pivot).ThenInclude(_ => _.Warehouse).FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (pivotShelf is null)
        {
            return new Result<PivotShelf>
            {
                Failed = true,
                Messages = ["Pivot rafı bulunamadı."]
            };
        }

        return new Result<PivotShelf>
        {
            Data = pivotShelf,
            Messages = ["Pivot rafı başarılı şekilde getirildi."]
        };
    }
}