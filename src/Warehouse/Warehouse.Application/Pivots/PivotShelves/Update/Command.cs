﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Pivots.PivotShelves.Update;

public sealed record Command(int Id, int PivotId, string Name) : IRequest<Result>;