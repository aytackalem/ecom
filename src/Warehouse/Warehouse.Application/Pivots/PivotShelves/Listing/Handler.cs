﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.PivotShelves.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<PivotShelf>>
{
    public async Task<PagedResult<PivotShelf>> Handle(Query request, CancellationToken cancellationToken)
    {
        return new PagedResult<PivotShelf>
        {
            RecordsCount = await context.PivotShelves.CountAsync(cancellationToken),
            Data = await context.PivotShelves.Where(_ => request.PivotId == null || _.PivotId == request.PivotId.Value).Skip(request.Page * request.PageRecordsCount).Take(request.PageRecordsCount).ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Pivot rafları başarılı şekilde getirildi."]
        };
    }
}