﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;

namespace Warehouse.Application.Pivots.PivotShelves.Listing;

public sealed record Query(int Page, int PageRecordsCount, int? PivotId = null) : IRequest<PagedResult<PivotShelf>>;