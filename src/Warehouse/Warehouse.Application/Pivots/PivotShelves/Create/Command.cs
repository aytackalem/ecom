﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Pivots.PivotShelves.Create;

public sealed record Command(int PivotId, string Name) : IRequest<Result<int>>;