﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.PivotShelves.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var pivotShelfItem = new PivotShelf
        {
            PivotId = request.PivotId,
            Name = request.Name
        };
        context.PivotShelves.Add(pivotShelfItem);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = pivotShelfItem.Id,
            Messages = ["Pivot rafı başarılı şekilde kaydedildi."]
        };
    }
}
