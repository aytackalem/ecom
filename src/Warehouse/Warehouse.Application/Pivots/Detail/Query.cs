﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;

namespace Warehouse.Application.Pivots.Detail;

public sealed record Query(int Id) : IRequest<Result<Pivot>>;