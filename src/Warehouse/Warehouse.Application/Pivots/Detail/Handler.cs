﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Pivot>>
{
    public async Task<Result<Pivot>> Handle(Query request, CancellationToken cancellationToken)
    {
        var pivotShelf = await context.Pivots.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (pivotShelf is null)
        {
            return new Result<Pivot>
            {
                Failed = true,
                Messages = ["Pivot bulunamadı."]
            };
        }

        return new Result<Pivot>
        {
            Data = pivotShelf,
            Messages = ["Pivot başarılı şekilde getirildi."]
        };
    }
}
