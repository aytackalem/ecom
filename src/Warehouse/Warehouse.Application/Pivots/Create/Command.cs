﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Pivots.Create;

public sealed record Command(int WarehouseId, string Name) : IRequest<Result<int>>;