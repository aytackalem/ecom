﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Pivots;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Pivots.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var pivot = new Pivot
        {
            WarehouseId = request.WarehouseId,
            Name = request.Name
        };
        context.Pivots.Add(pivot);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = pivot.Id,
            Messages = ["Pivot başarılı şekilde kaydedildi."]
        };
    }
}
