﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Limits.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var limit = await context.Limits.FirstOrDefaultAsync(cancellationToken);

        if (limit is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Limitler bulunamadı."]
            };
        }

        limit.Single = request.Single;
        limit.Multiple = request.Multiple;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Limitler başarılı şekilde güncellendi."]
        };
    }
}
