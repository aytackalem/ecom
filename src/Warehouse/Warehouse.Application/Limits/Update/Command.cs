﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Limits.Update;

public sealed record Command(int Id, int Single, int Multiple) : IRequest<Result>;