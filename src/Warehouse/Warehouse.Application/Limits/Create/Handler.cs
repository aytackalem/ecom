﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Limits;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Limits.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        if (context.Limits.Any())
        {
            return new Result<int>
            {
                Failed = true,
                Messages = ["Limitler daha önce oluşturulmuştur."]
            };
        }

        var limit = new Limit
        {
            Single = request.Single,
            Multiple = request.Multiple
        };
        context.Limits.Add(limit);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = limit.Id,
            Messages = ["Limitler başarılı şekilde kaydedildi."]
        };
    }
}