﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Limits.Create;

public sealed record Command(int Single, int Multiple) : IRequest<Result<int>>;