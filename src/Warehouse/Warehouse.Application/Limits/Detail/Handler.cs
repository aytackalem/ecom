﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Domain.Limits;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Limits.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Limit>>
{
    public async Task<Result<Limit>> Handle(Query request, CancellationToken cancellationToken)
    {
        var limit = await context.Limits.FirstOrDefaultAsync(cancellationToken);

        return new Result<Limit>
        {
            Data = limit,
            Messages = [limit is null ? "Limitler tanımlanmamış." : "Limitler başarılı şekilde getirildi."]
        };
    }
}
