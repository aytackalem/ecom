﻿using MediatR;
using Shared.Results;
using Warehouse.Domain.Limits;

namespace Warehouse.Application.Limits.Detail;

public sealed record Query : IRequest<Result<Limit>>;