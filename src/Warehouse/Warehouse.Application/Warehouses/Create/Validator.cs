﻿using FluentValidation;

namespace Warehouse.Application.Warehouses.Create;

public sealed class Validator : AbstractValidator<Command>
{
    public Validator()
    {
        RuleFor(_ => _.Name)
            .Matches("^[A-Za-z0-9. ]+$").WithMessage("İsim bilgisi sadece harf, sayı, nokta ve boşluktan oluşabilir.")
            .MaximumLength(50).WithMessage("İsim bilgisi maksimum '50' karakter uzunluğunda olmalıdır.")
            .NotEmpty().WithMessage("İsim bilgisi yanlış veya eksik.")
            .NotNull().WithMessage("İsim bilgisi yanlış veya eksik.");
    }
}
