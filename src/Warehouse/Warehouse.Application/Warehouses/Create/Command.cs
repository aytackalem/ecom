﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Warehouses.Create;

public record Command(string Name) : IRequest<Result<int>>;