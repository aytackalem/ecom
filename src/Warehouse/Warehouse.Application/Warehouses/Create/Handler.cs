﻿using MediatR;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Warehouses.Create;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var warehouse = new Domain.Warehouses.Warehouse
        {
            Name = request.Name
        };
        context.Warehouses.Add(warehouse);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = warehouse.Id,
            Messages = ["Depo başarılı şekilde kaydedildi."]
        };
    }
}
