﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Warehouses.Update;

public sealed record Command(int Id, string Name) : IRequest<Result>;