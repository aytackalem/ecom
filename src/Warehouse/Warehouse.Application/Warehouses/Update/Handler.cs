﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Warehouses.Update;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var warehouse = await context.Warehouses.FirstOrDefaultAsync(_ => _.Id == request.Id);

        if (warehouse is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Depo bulunamadı."]
            };
        }

        warehouse.Name = request.Name;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Güncelleme başarılı."]
        };
    }
}
