﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Warehouses.Detail;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, Result<Domain.Warehouses.Warehouse>>
{
    public async Task<Result<Domain.Warehouses.Warehouse>> Handle(Query request, CancellationToken cancellationToken)
    {
        var warehouse = await context.Warehouses.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

        if (warehouse is null)
        {
            return new Result<Domain.Warehouses.Warehouse>
            {
                Failed = true,
                Messages = ["Depo bulunamadı."]
            };
        }

        return new Result<Domain.Warehouses.Warehouse>
        {
            Data = warehouse,
            Messages = ["Depo başarılı şekilde getirildi."]
        };
    }
}
