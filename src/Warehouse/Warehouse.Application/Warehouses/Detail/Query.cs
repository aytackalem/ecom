﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Warehouses.Detail;

public sealed record Query(int Id) : IRequest<Result<Domain.Warehouses.Warehouse>>;