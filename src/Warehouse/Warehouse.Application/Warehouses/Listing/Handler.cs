﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;
using Warehouse.Infrastructure.Persistence;

namespace Warehouse.Application.Warehouses.Listing;

public sealed class Handler(WarehouseDbContext context) : IRequestHandler<Query, PagedResult<Domain.Warehouses.Warehouse>>
{
    public async Task<PagedResult<Domain.Warehouses.Warehouse>> Handle(Query request, CancellationToken cancellationToken)
    {
        return new PagedResult<Domain.Warehouses.Warehouse>
        {
            RecordsCount = await context.Warehouses.CountAsync(cancellationToken),
            Data = await context.Warehouses.Skip(request.Page * request.PageRecordsCount).Take(request.PageRecordsCount).ToListAsync(cancellationToken),
            Page = request.Page,
            PageRecordsCount = request.PageRecordsCount,
            Messages = ["Depolar başarılı şekilde getirildi."]
        };
    }
}
