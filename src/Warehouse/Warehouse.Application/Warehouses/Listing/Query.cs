﻿using MediatR;
using Shared.Results;

namespace Warehouse.Application.Warehouses.Listing;

public sealed record Query(int Page, int PageRecordsCount) : IRequest<PagedResult<Domain.Warehouses.Warehouse>>;