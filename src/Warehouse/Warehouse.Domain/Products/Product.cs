﻿using Warehouse.Domain.Shelves;

namespace Warehouse.Domain.Products;

public class Product : DomainBase
{
    public string Barcode { get; set; }

    public int Stock { get; set; }

    public int Reserve { get; set; }

    public int ShelfId { get; set; }

    public Shelf Shelf { get; set; }
}
