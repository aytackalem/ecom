﻿using Warehouse.Domain.Hallways;

namespace Warehouse.Domain.Shelves;

public class Shelf : DomainBase
{
    public string Name { get; set; }

    public int SortNumber { get; set; }

    public int HallwayId { get; set; }

    public Hallway Hallway { get; set; }
}
