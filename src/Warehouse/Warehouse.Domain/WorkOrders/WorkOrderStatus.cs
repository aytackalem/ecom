﻿namespace Warehouse.Domain.WorkOrders;

public enum WorkOrderStatus
{
    Pending,
    Picking,
    Picked,
    Packing,
    Packed
}
