﻿namespace Warehouse.Domain.WorkOrders;

/// <summary>
/// İş emirleri.
/// </summary>
public class WorkOrder : DomainBase
{
    public WorkOrderStatus Status { get; set; }

    public string PersonelName { get; set; }

    public bool Single { get; set; }

    public bool Multiple => !Single;

    public List<WorkOrderItem> Items { get; set; }

    public DateTime CreatedDateTime { get; set; }

    /// <summary>
    /// İş emrinin toplanıyor durumuna geçtiği zaman.
    /// </summary>
    public DateTime? PickingDateTime { get; set; }

    /// <summary>
    /// İş emrinin toplandı durumuna geçtiği zaman.
    /// </summary>
    public DateTime? PickedDateTime { get; set; }

    /// <summary>
    /// İş emrinin toplanma süresi.
    /// </summary>
    public TimeSpan? PickingTime { get; set; }

    /// <summary>
    /// İş emrinin paketleniyor durumuna geçtiği zaman.
    /// </summary>
    public DateTime? PackingDateTime { get; set; }

    /// <summary>
    /// İş emrinin paketlendi durumuna geçtiği zaman.
    /// </summary>
    public DateTime? PackedDateTime { get; set; }

    /// <summary>
    /// İş emrinin paketlenme süresi.
    /// </summary>
    public TimeSpan? PackingTime { get; set; }

}
