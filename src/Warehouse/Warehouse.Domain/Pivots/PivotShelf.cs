﻿namespace Warehouse.Domain.Pivots;

public class PivotShelf : DomainBase
{
    public string Name { get; set; }

    public int PivotId { get; set; }

    public Pivot Pivot { get; set; }
}
