﻿namespace Warehouse.Domain.Pivots;

public class Pivot : DomainBase
{
    public string Name { get; set; }

    public int WarehouseId { get; set; }

    public Domain.Warehouses.Warehouse Warehouse { get; set; }
}
