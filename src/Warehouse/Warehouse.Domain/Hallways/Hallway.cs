﻿using Warehouse.Domain.Floors;

namespace Warehouse.Domain.Hallways;

public class Hallway : DomainBase
{
    public string Name { get; set; }

    public int SortNumber { get; set; }

    public int FloorId { get; set; }

    public Floor Floor { get; set; }
}
