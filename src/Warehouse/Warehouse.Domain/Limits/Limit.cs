﻿namespace Warehouse.Domain.Limits;

public class Limit : DomainBase
{
    public int Single { get; set; }

    public int Multiple { get; set; }
}
