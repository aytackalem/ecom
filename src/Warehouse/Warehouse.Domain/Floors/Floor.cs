﻿using Warehouse.Domain.Warehouses;

namespace Warehouse.Domain.Floors;

public class Floor : DomainBase
{
    public string Name { get; set; }

    public int SortNumber { get; set; }

    public int WarehouseId { get; set; }

    public Domain.Warehouses.Warehouse Warehouse { get; set; }
}
