﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;
using WarehouseDesktop.Wpf.Clients.Ordering;
using WarehouseDesktop.Wpf.Clients.Warehouse;
using WarehouseDesktop.Wpf.Identity;
using WarehouseDesktop.Wpf.ViewModels.Logins;
using WarehouseDesktop.Wpf.ViewModels.Packings;
using WarehouseDesktop.Wpf.Windows.Logins;
using WarehouseDesktop.Wpf.Windows.Packings;

namespace WarehouseDesktop.Wpf;

public partial class App : Application
{
    public IServiceProvider ServiceProvider { get; private set; }

    public IConfiguration Configuration { get; private set; }

    protected override void OnStartup(StartupEventArgs e)
    {
        IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(Environment.CurrentDirectory)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        Configuration = configurationBuilder.Build();

        var serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton<IConfiguration>(Configuration);
        serviceCollection.AddMemoryCache();
        serviceCollection.AddIdentity();
        serviceCollection.AddIdentityOptions(Configuration);
        serviceCollection.AddOrdering();
        serviceCollection.AddWarehouse();
        serviceCollection.AddSingleton<LoginWindow>();
        serviceCollection.AddSingleton<LoginViewModel>();
        serviceCollection.AddSingleton<PackingWindow>();
        serviceCollection.AddSingleton<PackingViewModel>();

        ServiceProvider = serviceCollection.BuildServiceProvider();

        var window = ServiceProvider.GetRequiredService<LoginWindow>();
        window.Show();
    }
}
