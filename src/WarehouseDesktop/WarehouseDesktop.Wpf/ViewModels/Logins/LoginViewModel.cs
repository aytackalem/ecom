﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Windows.Input;
using WarehouseDesktop.Wpf.Identity;

namespace WarehouseDesktop.Wpf.ViewModels.Logins;

public class LoginViewModel : WindowViewModelBase
{
    #region Events
    public delegate void LoggedEventHandler();

    public event LoggedEventHandler Logged;
    #endregion

    private readonly IMemoryCache _memoryCache;

    private readonly IdentityOptions _helpyIdentityOptions;

    private readonly IdentityClient _identityClient;

    public LoginViewModel(IMemoryCache memoryCache, IConfiguration configuration, IOptions<IdentityOptions> helpyIdentityOptions, IdentityClient identityClient)
    {
        this._memoryCache = memoryCache;
        this._helpyIdentityOptions = helpyIdentityOptions.Value;
        this._identityClient = identityClient;

        this.Title = configuration["Title"];
    }

    #region Property Fields
    private string _username = "helpytest";

    private string _password = "Safa.1453";
    #endregion

    #region Properties
    public string Title { get; private set; }

    public string Username
    {
        get { return _username; }
        set { _username = value; OnPropertyChanged(); }
    }

    public string Password
    {
        get { return _password; }
        set { _password = value; OnPropertyChanged(); }
    }
    #endregion

    #region Command Fields
    private ICommand _loginCommand;
    #endregion

    #region Commands
    public ICommand LoginCommand
    {
        get
        {
            if (this._loginCommand == null)
                this._loginCommand = new RelayCommand(async (obj) =>
                {
                    if (string.IsNullOrEmpty(this.Username) || string.IsNullOrEmpty(this.Password))
                    {
                        this.Message = "Kullanıcı adı veya şifre boş olamaz.";
                        return;
                    }

                    await this.CommandAsync(async () =>
                    {
                        this._helpyIdentityOptions.Username = this.Username;
                        this._helpyIdentityOptions.Password = this.Password;

                        var token = await this._identityClient.GetTokenAsync(this._helpyIdentityOptions);
                        if (token == null)
                        {
                            this.Message = "Kullanıcı adı veya şifre yanlış.";
                            return;
                        }

                        if (this.Logged == null)
                            return;

                        this._memoryCache.Set<string>("Username", this.Username);
                        this._memoryCache.Set<string>("Password", this.Password);

                        this.Logged();
                    });
                });

            return this._loginCommand;
        }
    }
    #endregion
}