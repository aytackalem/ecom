﻿using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WarehouseDesktop.Wpf.ViewModels;

public abstract class ViewModelBase : INotifyPropertyChanged
{
    #region INotifyPropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion
}