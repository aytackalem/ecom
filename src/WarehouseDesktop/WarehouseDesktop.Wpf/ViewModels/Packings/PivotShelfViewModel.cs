﻿using System.Collections.ObjectModel;

namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class PivotShelfViewModel : ViewModelBase
{
    public PivotShelfViewModel()
    {
        Items = [];
    }

    public int Id { get; set; }

    public string Name { get; set; }

    public string? InvoiceNumber
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged("Invoiced");
        }
    }

    public bool Invoiced => InvoiceNumber != null;

    public bool Packable
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Unpackable));
        }
    }

    public bool Unpackable => !this.Packable;

    public bool IsSelected
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public ObservableCollection<PuttedWorkOrderItemViewModel> Items { get; set; }

    public void AddItem(PuttedWorkOrderItemViewModel item)
    {
        var _item = Items.FirstOrDefault(_ => _.Barcode == item.Barcode);
        if (_item is null)
        {
            _item = new PuttedWorkOrderItemViewModel
            {
                //InvoiceNumber = item.InvoiceNumber,
                OrderId = item.OrderId,
                Barcode = item.Barcode,
                PhotoUrl = item.PhotoUrl,
                Quantity = item.Quantity
            };

            Items.Add(_item);
        }
        else
        {
            _item.Quantity++;
        }
    }

    public bool TryReadBarcode(string barcode, out string message)
    {
        message = string.Empty;

        var item = Items.FirstOrDefault(_ => _.Barcode == barcode);
        if (item is null)
        {
            message = "Ürün bulunamadı.";
            return false;
        }

        if (item.Quantity < (item.ReadQuantity + 1))
        {
            message = "Fazla ürün okutuldu.";
            return false;
        }

        item.ReadQuantity++;

        if (Items.Count(_ => _.Quantity == _.ReadQuantity) == Items.Count)
        {
            this.Packable = true;
        }

        return true;
    }

    public void Clear()
    {
        Items.Clear();
    }
}
