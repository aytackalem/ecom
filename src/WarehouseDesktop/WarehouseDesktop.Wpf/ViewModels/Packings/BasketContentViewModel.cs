﻿namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class BasketContentViewModel : ViewModelBase
{
    public BasketContentViewModel()
    {
        Visible = true;
    }

    public int Quantity
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();

            if (value == 0)
            {
                Visible = false;
                OnPropertyChanged("Visible");
            }
        }
    }

    public bool Visible { get; set; }

    public string PhotoUrl { get; set; }

    public string Name { get; set; }

    public string Barcode { get; set; }
}