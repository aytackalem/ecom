﻿namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class PuttedWorkOrderItemViewModel : ViewModelBase
{
    public int OrderId { get; set; }

    public string Barcode { get; set; }

    public string PhotoUrl { get; set; }

    public int Quantity
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public int ReadQuantity
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }
}
