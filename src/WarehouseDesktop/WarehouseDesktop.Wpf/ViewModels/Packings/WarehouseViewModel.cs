﻿namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class WarehouseViewModel
{
    public int Id { get; set; }

    public string Name { get; set; }
}
