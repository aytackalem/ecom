﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Collections.ObjectModel;
using System.Text.Json;
using System.Windows.Input;
using WarehouseDesktop.Wpf.Clients.Ordering;
using WarehouseDesktop.Wpf.Clients.Warehouse;
using WarehouseDesktop.Wpf.Identity;
using WarehouseDesktop.Wpf.Printing;

namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class PackingViewModel : WindowViewModelBase
{
    #region Default
    #region Services
    private readonly IdentityOptions _identityOptions;

    private readonly IdentityClient _identityClient;

    private readonly WarehouseClient _warehouseClient;

    private readonly OrderClient _orderClient;

    private readonly IServiceProvider _serviceProvider;
    #endregion

    #region Constructor
    public PackingViewModel(
        IMemoryCache memoryCache,
        IServiceProvider serviceProvider,
        IConfiguration configuration,
        IOptions<IdentityOptions> helpyIdentityOptions,
        IdentityClient identityClient,
        OrderClient orderClient,
        WarehouseClient warehouseClient)
    {
        this._serviceProvider = serviceProvider;
        this._identityOptions = helpyIdentityOptions.Value;
        this._identityClient = identityClient;
        this._orderClient = orderClient;
        this._warehouseClient = warehouseClient;

        this.Title = configuration["Title"];

        this._identityOptions.Username = memoryCache.Get<string>("Username");
        this._identityOptions.Password = memoryCache.Get<string>("Password");

        if (System.IO.File.Exists(@"C:\Warehouse\Options\Warehouse.txt") &&
            System.IO.File.Exists(@"C:\Warehouse\Options\Pivot.txt") &&
            System.IO.File.Exists(@"C:\Warehouse\Options\PivotShelves.txt"))
        {
            this.Warehouse = JsonSerializer.Deserialize<WarehouseViewModel>(System.IO.File.ReadAllText(@"C:\Warehouse\Options\Warehouse.txt"));
            this.Pivot = JsonSerializer.Deserialize<PivotViewModel>(System.IO.File.ReadAllText(@"C:\Warehouse\Options\Pivot.txt"));
            this.PivotShelves = [];

            JsonSerializer.Deserialize<List<PivotShelfViewModel>>(System.IO.File.ReadAllText(@"C:\Warehouse\Options\PivotShelves.txt")).ForEach(_ => this.PivotShelves.Add(new PivotShelfViewModel
            {
                Id = _.Id,
                Name = _.Name
            }));

            this.OptionsFound = true;
        }
        else
        {
            this.Message = "Lütfen ayarları doldurunuz.";
            this.MessageVisible = true;

            this.OptionsModalOpened = true;
        }
    }
    #endregion

    #region Properties
    public string Title { get; private set; }
    #endregion
    #endregion

    #region Options
    public bool OptionsFound
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }

    public bool OptionsModalOpened
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }

    public ICommand ToggleOptionsModalCommand => field ??= new RelayCommand(async _ =>
    {
        OptionsModalOpened = !OptionsModalOpened;
    });

    public ICommand SaveOptionsCommand => field ??= new RelayCommand(_ =>
    {
        if (Warehouse is not null && Pivot is not null && PivotShelves is not null && PivotShelves.Count > 0)
        {
            System.IO.Directory.CreateDirectory(@"C:\Warehouse\Options");
            System.IO.File.WriteAllText(@"C:\Warehouse\Options\Warehouse.txt", JsonSerializer.Serialize(Warehouse));
            System.IO.File.WriteAllText(@"C:\Warehouse\Options\Pivot.txt", JsonSerializer.Serialize(Pivot));
            System.IO.File.WriteAllText(@"C:\Warehouse\Options\PivotShelves.txt", JsonSerializer.Serialize(PivotShelves));

            this.OptionsFound = true;

            this.Message = "Kayıt başarılı.";
        }
        else
        {
            this.Message = "Lütfen tüm ayarları seçiniz.";
        }
    });

    #region Warehouses
    public ObservableCollection<WarehouseViewModel> Warehouses
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    } = [];

    public bool WarehouseModalOpened
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }

    public ICommand ToggleWarehouseModal => field ??= new RelayCommand(async _ =>
    {

        if (Warehouses.Count == 0)
        {
            this.Busy = true;

            var token = await this._identityClient.GetTokenAsync(this._identityOptions);

            if (token == null)
            {
                this.Message = "Yetki hatası.";

                this.Busy = false;

                return;
            }

            var response = await this._warehouseClient.WarehousesListingAsync(
                this._identityOptions.DomainId,
                this._identityOptions.CompanyId,
                token.AccessToken);

            if (response.Success)
            {
                this.Warehouses = [];

                response.Data.ForEach(_ =>
                {
                    this.Warehouses.Add(new WarehouseViewModel
                    {
                        Id = _.Id,
                        Name = _.Name
                    });
                });
            }
            else
            {
                this.Message = "Bilinmeyen bir hata oluştu.";

                this.Busy = false;

                return;
            }
        }

        this.Busy = false;

        WarehouseModalOpened = !WarehouseModalOpened;
    });

    public ICommand SelectWarehouseCommand => field ??= new RelayCommand(async _ =>
    {
        Warehouse = (WarehouseViewModel)_;

        this.Busy = true;

        var token = await this._identityClient.GetTokenAsync(this._identityOptions);

        if (token == null)
        {
            this.Message = "Yetki hatası.";

            this.Busy = false;

            return;
        }

        var response = await this._warehouseClient.PivotsListingAsync(
            this.Warehouse.Id,
            this._identityOptions.DomainId,
            this._identityOptions.CompanyId,
            token.AccessToken);

        if (response.Success)
        {
            this.Pivots = [];

            response.Data.ForEach(_ =>
            {
                this.Pivots.Add(new PivotViewModel
                {
                    Id = _.Id,
                    Name = _.Name
                });
            });
        }
        else
        {
            this.Message = "Bilinmeyen bir hata oluştu.";

            this.Busy = false;

            return;
        }

        this.Busy = false;

        WarehouseModalOpened = false;
    });

    public WarehouseViewModel Warehouse
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }
    #endregion

    #region Pivots
    public ObservableCollection<PivotViewModel> Pivots
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    } = [];

    public bool PivotModalOpened
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }

    public ICommand TogglePivotModal => field ??= new RelayCommand(_ =>
    {
        if (Pivots.Count == 0)
        {
            Message = "Depo seçiniz.";

            return;
        }

        PivotModalOpened = !PivotModalOpened;
    });

    public ICommand SelectPivotCommand => field ??= new RelayCommand(async _ =>
    {
        Pivot = (PivotViewModel)_;

        this.Busy = true;

        var token = await this._identityClient.GetTokenAsync(this._identityOptions);

        if (token == null)
        {
            this.Message = "Yetki hatası.";

            this.Busy = false;

            return;
        }

        var response = await this._warehouseClient.PivotShelvesListingAsync(
            this.Pivot.Id,
            this._identityOptions.DomainId,
            this._identityOptions.CompanyId,
            token.AccessToken);

        if (response.Success)
        {
            this.PivotShelves = [];

            response.Data.ForEach(_ =>
            {
                this.PivotShelves.Add(new PivotShelfViewModel
                {
                    Id = _.Id,
                    Name = _.Name
                });
            });
        }
        else
        {
            this.Message = "Bilinmeyen bir hata oluştu.";

            this.Busy = false;

            return;
        }

        this.Busy = false;

        PivotModalOpened = false;
    });

    public PivotViewModel Pivot
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }
    #endregion

    #region Pivot Shelves
    public ObservableCollection<PivotShelfViewModel> PivotShelves
    {
        get;
        set
        {
            field = value;

            OnPropertyChanged();
        }
    }
    #endregion
    #endregion

    #region Work Orders
    public int? WorkOrderId
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public WorkOrderViewModel WorkOrder
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public ICommand GetWorkOrderCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {
                if (this.WorkOrderId.HasValue == false)
                {
                    this.Message = "Lütfen geçerli bir iş emri no giriniz.";

                    return;
                }

                this.Busy = true;

                var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                if (token == null)
                {
                    this.Message = "Yetki hatası.";

                    this.Busy = false;

                    this.WorkOrderId = null;

                    return;
                }

                var workOrderDetailResponse = await this._warehouseClient.WorkOrdersDetailAsync(
                    this.WorkOrderId.Value,
                    this._identityOptions.DomainId,
                    this._identityOptions.CompanyId,
                    token.AccessToken);

                if (workOrderDetailResponse.Success == false)
                {
                    this.Message = workOrderDetailResponse.Messages[0];

                    this.Busy = false;

                    this.WorkOrderId = null;

                    return;
                }

                if (workOrderDetailResponse.Data.Multiple && this.Warehouse is null)
                {
                    this.Message = "Depo seçiniz.";

                    this.Busy = false;

                    this.WorkOrderId = null;

                    return;
                }

                if (workOrderDetailResponse.Data.Multiple && (this.Pivot is null || this.PivotShelves.Count == 0))
                {
                    this.Message = "Pivot seçiniz.";

                    this.Busy = false;

                    this.WorkOrderId = null;

                    return;
                }

                if (workOrderDetailResponse.Data.Status != Clients.Warehouse.WorkOrders.Detail.WorkOrderStatus.Picked &&
                    workOrderDetailResponse.Data.Status != Clients.Warehouse.WorkOrders.Detail.WorkOrderStatus.Packing)
                {
                    this.Message = "Sipariş durumu toplandı veya paketleniyor değil.";

                    this.Busy = false;

                    this.WorkOrderId = null;

                    return;
                }

                if (workOrderDetailResponse.Data.Status != Clients.Warehouse.WorkOrders.Detail.WorkOrderStatus.Packing)
                {
                    var packingResponse = await this._warehouseClient.Packing(
                        workOrderDetailResponse.Data.Id,
                        this._identityOptions.DomainId,
                        this._identityOptions.CompanyId,
                        token.AccessToken);

                    if (packingResponse.Success == false)
                    {
                        this.Message = "Sipariş durumu güncellenemedi.";

                        this.Busy = false;

                        this.WorkOrderId = null;

                        return;
                    }
                }

                var workOrder = new WorkOrderViewModel
                {
                    Id = this.WorkOrderId.Value,
                    Single = workOrderDetailResponse.Data.Single,
                    Status = workOrderDetailResponse.Data.Status,
                    UserId = workOrderDetailResponse.Data.UserId,
                    Items = workOrderDetailResponse
                        .Data
                        .Items
                        .Select(_ => new WorkOrderItemViewModel
                        {
                            Barcode = _.Barcode,
                            Id = _.Id,
                            Name = _.Name,
                            OrderId = _.OrderId,
                            PhotoUrl = _.PhotoUrl,
                            PivotShelfId = _.PivotShelfId,
                            InvoiceNumber = _.InvoiceNumber,
                            PuttedQuantity = _.PuttedQuantity,
                            Quantity = _.Quantity,
                            WorkOrderId = _.WorkOrderId
                        })
                        .ToList()
                };

                this.WorkOrder = workOrder;

                if (this.WorkOrder.Single)
                {
                    foreach (var item in this.WorkOrder.Items)
                    {
                        item.Invoiced = item.InvoiceNumber is not null;
                    }

                    this.SingleWorkOrderModalOpened = true;
                }

                if (this.WorkOrder.Multiple)
                {
                    this.BasketContents = [];

                    var basketContents = this
                        .WorkOrder
                        .Items
                        .GroupBy(_ => _.Barcode)
                        .Select(_ => new BasketContentViewModel
                        {
                            Barcode = _.Key,
                            Name = _.First().Name,
                            PhotoUrl = _.First().PhotoUrl,
                            Quantity = _.Sum(_ => _.Quantity)
                        });

                    foreach (var basketContent in basketContents)
                    {
                        this.BasketContents.Add(basketContent);
                    }

                    /* Eğer iş emri öğeleri içerisinde daha önce okutulmuş olanlar var ise atamalar yapılır. */
                    if (this.WorkOrder.Items.Any(_ => _.PivotShelfId.HasValue))
                    {
                        this.WorkOrder.Items.Where(_ => _.PivotShelfId.HasValue).ToList().ForEach(workOrderItem =>
                        {
                            var pivotShelf = this.PivotShelves.FirstOrDefault(_ => _.Id == workOrderItem.PivotShelfId.Value);

                            /* Okutulan ürün adreslenen rafa eklenir. */
                            pivotShelf.AddItem(new PuttedWorkOrderItemViewModel
                            {
                                //InvoiceNumber = workOrderItem.InvoiceNumber,
                                OrderId = workOrderItem.OrderId,
                                Barcode = workOrderItem.Barcode,
                                PhotoUrl = workOrderItem.PhotoUrl,
                                Quantity = 1
                            });

                            /* Eğer faturalandırma yapılmış ise fatura bilgileri adreslenmiş rafa eklenir ve paketlenebilir olarak atanır. */
                            if (workOrderItem.InvoiceNumber != null)
                            {
                                pivotShelf.InvoiceNumber = workOrderItem.InvoiceNumber;
                                pivotShelf.Packable = true;
                            }

                            /* Ürün ataması yapılır. */
                            var basketContent = BasketContents.First(_ => _.Barcode == workOrderItem.Barcode);

                            /* Ürüne ait kalan adet bilgisi azaltılır. */
                            basketContent.Quantity--;

                            /* Tüm ürünler okutulmuş ise pivot sonlandırılır. */
                            if (BasketContents.Any(_ => _.Quantity > 0) == false)
                            {
                                this.PivotEnd = true;
                            }
                        });
                    }

                    this.MultipleWorkOrderModalOpened = true;

                    /* Eğer tüm siparişler faturalanmış ise tamamla butonu aktif edilir. */
                    if (this.PivotShelves.Count(_ => _.Items.Count > 0 && _.Invoiced == false) == 0)
                    {
                        this.MultipleCompleteCommandVisible = true;
                    }
                }

                this.Busy = false;

                this.WorkOrderId = null;
            });
        }
    }

    public string Barcode
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    #region Multiple
    /// <summary>
    /// Okutulan ürünü ilgili rafa adresleyen komut.
    /// </summary>
    public ICommand FindPivotShelfCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {
                /* Okutulan ürün iş emri öğeleri içerisinden bulunur. */
                var workOrderItem = this
                    .WorkOrder
                    .Items
                    .FirstOrDefault(_ => _.Barcode == Barcode && _.Quantity > _.PuttedQuantity);

                if (workOrderItem is null)
                {
                    this.Message = "Ürün bulunamadı.";

                    this.Barcode = string.Empty;

                    return;
                }

                /* Okutulan ürününün adresleneceği rafı işaret eder. */
                PivotShelfViewModel pivotShelf = null;

                /* Okutulan ürünü işaret eder. */
                BasketContentViewModel basketContent = null;

                /* İş emri öğesinin ait olduğu sipariş daha önceden bir rafa adreslenmiş ise öncelik o rafa verilir. */
                var puttedWorkOrderItem = this
                    .WorkOrder
                    .Items
                    .FirstOrDefault(_ => _.OrderId == workOrderItem.OrderId && _.PivotShelfId.HasValue);

                /* Daha önceden adreslenmiş olan raf boş değil ise ilgili atamalar yapılır. */
                if (puttedWorkOrderItem is not null)
                {
                    /* Raf ataması yapılır. */
                    pivotShelf = this.PivotShelves.First(_ => _.Id == puttedWorkOrderItem.PivotShelfId.Value);
                }
                else if (workOrderItem.PivotShelfId.HasValue)
                {
                    /* Raf ataması yapılır. */
                    pivotShelf = this.PivotShelves.First(_ => _.Id == workOrderItem.PivotShelfId.Value);
                }
                else
                {
                    var fullPivotShelfIds = this
                        .WorkOrder
                        .Items
                        .Where(_ => _.PivotShelfId.HasValue)
                        .Select(_ => _.PivotShelfId.Value);

                    /* Raf ataması yapılır. */
                    pivotShelf = this.PivotShelves.FirstOrDefault(_ => fullPivotShelfIds.Contains(_.Id) == false);
                }

                if (pivotShelf is null)
                {
                    this.Message = "Raf bulunamadı.";

                    this.Barcode = string.Empty;

                    return;
                }

                this.Busy = true;

                var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                if (token == null)
                {
                    this.Message = "Yetki hatası.";

                    this.Busy = false;

                    return;
                }

                var putTheProductResponse = await this._warehouseClient.PutTheProduct(new Clients.Warehouse.WorkOrders.PutTheProduct.Command(workOrderItem.Id, pivotShelf.Id),
                      this._identityOptions.DomainId,
                      this._identityOptions.CompanyId,
                      token.AccessToken);

                if (putTheProductResponse.Success == false)
                {
                    this.Message = "Ürün rafa adreslenemedi.";

                    this.Busy = false;

                    return;
                }

                /* Ürün ataması yapılır. */
                basketContent = BasketContents.First(_ => _.Barcode == workOrderItem.Barcode);

                /* İş emri öğesine raf bilgisi adreslenir ve adet arttırılır. */
                workOrderItem.PivotShelfId = pivotShelf.Id;
                workOrderItem.PuttedQuantity++;

                /* Okutulan ürün adreslenen rafa eklenir. */
                pivotShelf.AddItem(new PuttedWorkOrderItemViewModel
                {
                    //InvoiceNumber = workOrderItem.InvoiceNumber,
                    OrderId = workOrderItem.OrderId,
                    Barcode = workOrderItem.Barcode,
                    PhotoUrl = workOrderItem.PhotoUrl,
                    Quantity = 1
                });

                /* Ürüne ait kalan adet bilgisi azaltılır. */
                basketContent.Quantity--;

                /* Tüm ürünler okutulmuş ise pivot sonlandırılır. */
                if (BasketContents.Any(_ => _.Quantity > 0) == false)
                {
                    this.PivotEnd = true;
                }

                /* Tüm rafların seçili durumu iptal edilir. */
                foreach (var pivotShelfLoop in PivotShelves)
                {
                    pivotShelfLoop.IsSelected = false;
                }

                /* Adreslenen raf seçili duruma getirilir. */
                pivotShelf.IsSelected = true;

                this.Busy = false;

                this.Barcode = string.Empty;
            });
        }
    }

    public ICommand ResetMultipleWorkOrderCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                this.BasketContents = [];

                var basketContents = this
                    .WorkOrder
                    .Items
                    .GroupBy(_ => _.Barcode)
                    .Select(_ => new BasketContentViewModel
                    {
                        Barcode = _.Key,
                        Name = _.First().Name,
                        PhotoUrl = _.First().PhotoUrl,
                        Quantity = _.Sum(_ => _.Quantity)
                    });

                foreach (var basketContent in basketContents)
                {
                    this.BasketContents.Add(basketContent);
                }

                foreach (var pivotShelf in this.PivotShelves)
                {
                    pivotShelf.Clear();
                }

                foreach (var item in this.WorkOrder.Items)
                {
                    item.PuttedQuantity = 0;
                }
            });
        }
    }

    public ICommand CloseMultipleWorkOrderModal
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                /* Modal'ı kapat. */
                MultipleWorkOrderModalOpened = false;

                /* İş emrini sil. */
                WorkOrder = null;

                /* Sepet içeriğini sil. */
                BasketContents = null;

                /* Pivot u sıfırla. */
                PivotEnd = false;

                /* Pivot index ini sıfırla. */
                PivotShelfIndex = 0;

                PivotShelf = null;

                foreach (var pivotShelf in PivotShelves)
                {
                    pivotShelf.IsSelected = false;
                    pivotShelf.Packable = false;
                    pivotShelf.InvoiceNumber = null;
                    pivotShelf.Clear();
                }

                this.MultipleCompleteCommandVisible = false;
            });
        }
    }

    public bool MultipleWorkOrderModalOpened
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public ObservableCollection<BasketContentViewModel> BasketContents
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public bool PivotEnd
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged("PivotContinue");
        }
    }

    public bool PivotContinue
    {
        get
        {
            return PivotEnd == false;
        }
    }

    public bool MultiplePackingModalOpened
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public ICommand StartPackingCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                this.PivotShelfIndex = 0;
                this.PivotShelf = this.PivotShelves[this.PivotShelfIndex];
                this.MultiplePackingModalOpened = true;
            });
        }
    }

    public ICommand StopPackingCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                this.MultiplePackingModalOpened = false;
            });
        }
    }

    public int PivotShelfIndex
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public PivotShelfViewModel PivotShelf
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    /// Pivotlama sonrası paketleme için kullanılan barkod okutma komutu.
    /// </summary>
    public ICommand ReadBarcodeCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {
                if (this.PivotShelf.TryReadBarcode(this.Barcode, out string message))
                {

                    this.Busy = true;

                    /* Tüm ürünler okutulmuş ve faturalandırılmamış ise faturalandırma isteği gönderilir. */
                    if (this.PivotShelf.Packable && this.PivotShelf.Invoiced == false)
                    {
                        var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                        if (token == null)
                        {
                            this.Message = "Yetki hatası.";

                            this.Busy = false;

                            return;
                        }

                        var invoiceResult = await this._warehouseClient.Invoice(
                            new Clients.Warehouse.WorkOrders.Invoice.Command(
                                this.WorkOrder.Id,
                                this.PivotShelf.Items.First().OrderId,
                                "Invoice Result"),
                            this._identityOptions.DomainId,
                            this._identityOptions.CompanyId,
                            token.AccessToken);

                        if (invoiceResult.Success == false)
                        {
                            this.Message = invoiceResult.Messages[0];

                            this.Busy = false;

                            return;
                        }

                        this.PivotShelf.InvoiceNumber = "Invoice Result";

                        PrintCommand.Execute(null);
                    }

                    if (this.PivotShelves.Count(_ => _.Items.Count > 0 && _.Invoiced == false) == 0)
                    {
                        this.MultipleCompleteCommandVisible = true;
                    }

                    this.Busy = false;

                    this.Barcode = string.Empty;

                    return;
                }

                this.Barcode = string.Empty;
                this.Message = message;
            });
        }
    }

    public ICommand NextPivotShelfCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                if (this.PivotShelfIndex == this.PivotShelves.Count - 1)
                {
                    Message = "Şuan son rafı görüntülüyorsunuz.";
                    return;
                }

                this.PivotShelfIndex++;
                this.PivotShelf = this.PivotShelves[this.PivotShelfIndex];
            });
        }
    }

    public ICommand PreviousPivotShelfCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                if (this.PivotShelfIndex == 0)
                {
                    Message = "Şuan ilk rafı görüntülüyorsunuz.";
                    return;
                }

                this.PivotShelfIndex--;
                this.PivotShelf = this.PivotShelves[this.PivotShelfIndex];
            });
        }
    }

    public bool MultipleCompleteCommandVisible
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(MultipleCompleteCommandHidden));
        }
    }

    public bool MultipleCompleteCommandHidden => !this.MultipleCompleteCommandVisible;

    public ICommand MultipleCompleteCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {

                this.Busy = true;

                var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                if (token == null)
                {
                    this.Message = "Yetki hatası.";

                    this.Busy = false;

                    return;
                }

                var packingResponse = await this._warehouseClient.Packed(
                        WorkOrder.Id,
                        this._identityOptions.DomainId,
                        this._identityOptions.CompanyId,
                        token.AccessToken);

                if (packingResponse.Success == false)
                {
                    this.Message = "Sipariş durumu güncellenemedi.";

                    this.Busy = false;

                    return;
                }

                this.Busy = false;

                CloseMultipleWorkOrderModal.Execute(null);

            });
        }
    }
    #endregion

    #region Single
    public ICommand CloseSingleWorkOrderModal
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                /* Modal'ı kapat. */
                SingleWorkOrderModalOpened = false;

                /* Seçili siparişi sil. */
                WorkOrderItem = null;

                /* İş emrini sil. */
                WorkOrder = null;

                /* Görünürlükler geri alınır. */
                CompleteCommandVisible = false;
            });
        }
    }

    public bool SingleWorkOrderModalOpened
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
        }
    }

    public bool WorkOrderItemVisible => WorkOrderItem is not null;

    public WorkOrderItemViewModel WorkOrderItem
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged("WorkOrderItemVisible");
        }
    }

    public ICommand FindWorkOrderItemCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {
                var item = WorkOrder.Items.FirstOrDefault(_ => _.Barcode == this.Barcode && _.Invoiced == false);

                this.Barcode = string.Empty;

                if (item is null)
                {
                    this.Message = "Sipariş bulunamadı.";

                    return;
                }

                WorkOrderItem = item;

                this.Busy = true;

                var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                if (token == null)
                {
                    this.Message = "Yetki hatası.";

                    this.Busy = false;

                    return;
                }

                var invoiceResult = await this._warehouseClient.Invoice(
                    new Clients.Warehouse.WorkOrders.Invoice.Command(
                        this.WorkOrder.Id,
                        this.WorkOrderItem.OrderId,
                        "Invoice Result"),
                    this._identityOptions.DomainId,
                    this._identityOptions.CompanyId,
                    token.AccessToken);

                if (invoiceResult.Success == false)
                {
                    this.Message = invoiceResult.Messages[0];

                    this.Busy = false;

                    return;
                }

                this.Busy = false;

                item.Invoiced = true;

                PrintCommand.Execute(null);

                if (WorkOrder.Items.Any(_ => _.Invoiced == false) == false)
                {
                    this.CompleteCommandVisible = true;
                }
            });
        }
    }

    public bool CompleteCommandVisible
    {
        get;
        set
        {
            field = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(CompleteCommandHidden));
        }
    }

    public bool CompleteCommandHidden => !this.CompleteCommandVisible;

    public ICommand SingleCompleteCommand
    {
        get
        {
            return field ??= new RelayCommand(async _ =>
            {

                this.Busy = true;

                var token = await this._identityClient.GetTokenAsync(this._identityOptions);

                if (token == null)
                {
                    this.Message = "Yetki hatası.";

                    this.Busy = false;

                    return;
                }

                var packingResponse = await this._warehouseClient.Packed(
                        WorkOrder.Id,
                        this._identityOptions.DomainId,
                        this._identityOptions.CompanyId,
                        token.AccessToken);

                if (packingResponse.Success == false)
                {
                    this.Message = "Sipariş durumu güncellenemedi.";

                    this.Busy = false;

                    return;
                }

                this.Busy = false;

                CloseSingleWorkOrderModal.Execute(null);

            });
        }
    }
    #endregion

    public ICommand PrintCommand
    {
        get
        {
            return field ??= new RelayCommand(_ =>
            {
                var order = JsonSerializer.Deserialize<Printing.Ordering.Order>(@"{
    ""id"": 287254,
    ""orderType"": {
      ""id"": ""OS"",
      ""name"": ""Onaylanmış Sipariş""
    },
    ""orderSource"": {
      ""id"": 1,
      ""name"": ""Pazaryeri""
    },
    ""currency"": {
      ""id"": ""TL"",
      ""name"": ""Türk Lirası""
    },
    ""marketplace"": {
      ""id"": ""TY"",
      ""name"": ""Trendyol""
    },
    ""customer"": {
      ""id"": 288613,
      ""name"": ""NARIN"",
      ""surname"": ""ŞEKER""
    },
    ""orderDeliveryAddress"": {
      ""id"": 287254,
      ""recipient"": ""narin şeker"",
      ""phoneNumber"": """",
      ""address"": ""Şabaniye mahallesi eski edemiyorduk kışla 5 sokak no 90 "",
      ""neighborhood"": {
        ""id"": 68060,
        ""name"": ""SABANIYE MAH"",
        ""district"": {
          ""id"": 937,
          ""name"": ""Edremit"",
          ""city"": {
            ""id"": 78,
            ""name"": ""Van"",
            ""country"": {
              ""id"": 1,
              ""name"": ""Türkiye""
            }
          }
        }
      }
    },
    ""orderInvoiceInformation"": {
      ""id"": 287254,
      ""firstName"": ""narin"",
      ""lastName"": ""şeker"",
      ""address"": ""Şabaniye mahallesi eski edemiyorduk kışla 5 sokak no 90 "",
      ""phone"": """",
      ""mail"": ""pf+6axao3z9@trendyolmail.com"",
      ""taxOffice"": """",
      ""taxNumber"": """",
      ""url"": null,
      ""guid"": null,
      ""number"": null,
      ""neighborhood"": {
        ""id"": 68060,
        ""name"": ""SABANIYE MAH"",
        ""district"": {
          ""id"": 937,
          ""name"": ""Edremit"",
          ""city"": {
            ""id"": 78,
            ""name"": ""Van"",
            ""country"": {
              ""id"": 1,
              ""name"": ""Türkiye""
            }
          }
        }
      }
    },
    ""orderBilling"": null,
    ""orderShipments"": [
      {
        ""id"": 287237,
        ""trackingCode"": ""7330020798602146"",
        ""trackingUrl"": null,
        ""trackingBase64"": null,
        ""packageNumber"": ""2752013757"",
        ""shipmentCompany"": {
          ""id"": ""TYEX"",
          ""name"": ""Trendyol Express""
        }
      }
    ],
    ""orderDetails"": [
      {
        ""id"": 314189,
        ""quantity"": 1,
        ""listUnitPrice"": 299.9900,
        ""unitPrice"": 299.9900,
        ""vatRate"": 0.10,
        ""vatExcUnitPrice"": 272.7182,
        ""vatExcListPrice"": 272.7182,
        ""vatExcUnitDiscount"": 0.0000,
        ""type"": 0,
        ""returnNumber"": null,
        ""returnDescription"": null,
        ""productInformation"": {
          ""id"": 61334,
          ""name"": ""Kadın Gri Balon Kol Uzun Hırka"",
          ""variantValues"": null,
          ""barcode"": ""6402187314842"",
          ""stockCode"": ""21K131127R06STD"",
          ""productInformationPhoto"": {
            ""fileName"": ""https://backoffice.helpy.com.tr/img/product/21K131127R06/21k131127r06-1.jpg""
          },
          ""product"": {
            ""id"": 11137,
            ""sellerCode"": ""21K131127""
          }
        }
      }
    ],
    ""orderNotes"": [],
    ""orderReturnDetails"": [],
    ""payments"": [
      {
        ""id"": 304094,
        ""amount"": 299.99,
        ""paymentType"": {
          ""id"": ""OO"",
          ""name"": ""Online Ödeme""
        }
      }
    ],
    ""amount"": 299.9900,
    ""cargoFee"": 0.0000,
    ""surchargeFee"": 0.0000,
    ""bulkInvoicing"": false,
    ""micro"": false,
    ""marketplaceOrderNumber"": ""9999167129"",
    ""orderDate"": ""2025-02-14T16:02:11.912"",
    ""commercial"": false,
    ""company"": {
      ""id"": 1,
      ""name"": ""Helpy"",
      ""fullName"": ""Helpy"",
      ""companyContact"": {
        ""taxNumber"": """",
        ""phoneNumber"": """",
        ""taxOffice"": """",
        ""address"": ""Helpy"",
        ""email"": "" info@Helpy.tr"",
        ""webUrl"": ""Helpy.tr""
      }
    },
    ""orderPicking"": null,
    ""orderPacking"": null
  }", new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });

                order.Company = new Printing.Ordering.Company
                {
                    CompanyContact = new Printing.Ordering.CompanyContact
                    {
                        Address = "Test Adresi",
                        PhoneNumber = "555 555 55 55",
                        Email = "test@test.com"
                    }
                };

                order.Payments = new List<Printing.Ordering.Payment>
                {
                    new Printing.Ordering.Payment
                    {
                        Amount = 1000,
                        PaymentType = new Printing.Ordering.PaymentType
                        {
                            Id = "OO",
                            Name = "Online Ödeme"
                        }
                    }
                };

                var marketplacePrint = new MarketplacePrint("Invoice", 100);
                marketplacePrint.Print(order);

                var invoice80Print = new Invoice80Print("Invoice");
                invoice80Print.Print(order);
            });
        }
    }
    #endregion
}