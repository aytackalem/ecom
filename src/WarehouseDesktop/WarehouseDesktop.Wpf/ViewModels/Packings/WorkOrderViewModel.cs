﻿using WarehouseDesktop.Wpf.Clients.Warehouse.WorkOrders.Detail;

namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class WorkOrderViewModel
{
    public int Id { get; set; }

    public WorkOrderStatus Status { get; set; }

    public int UserId { get; set; }

    public bool Single { get; set; }

    public bool Multiple => !Single;

    public List<WorkOrderItemViewModel> Items { get; set; }
}
