﻿namespace WarehouseDesktop.Wpf.ViewModels.Packings;

public class PivotViewModel
{
    public int Id { get; set; }

    public string Name { get; set; }
}
