﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WarehouseDesktop.Wpf.ViewModels;

public abstract class WindowViewModelBase : ViewModelBase
{
    [DllImport("wininet.dll")]
    private extern static bool InternetGetConnectedState(out int description, int reservedValue);

    #region Events
    public delegate void ClosedEventHandler();

    public event ClosedEventHandler Closed;
    #endregion

    private string _message;

    public string Message
    {
        get { return _message; }
        set
        {
            _message = value;

            OnPropertyChanged();

            this.MessageVisible = !string.IsNullOrEmpty(value);
        }
    }

    private bool _messageVisible = false;

    public bool MessageVisible
    {
        get { return _messageVisible; }
        set { _messageVisible = value; OnPropertyChanged(); }
    }

    private bool _busy = false;

    public bool Busy
    {
        get { return _busy; }
        set { _busy = value; OnPropertyChanged(); this.BusyVisible = value; }
    }

    private bool _busyVisible = false;

    public bool BusyVisible
    {
        get { return _busyVisible; }
        set { _busyVisible = value; OnPropertyChanged(); }
    }

    private ICommand _clearMessageCommand;

    public ICommand ClearMessageCommand
    {
        get
        {
            if (this._clearMessageCommand == null)
                this._clearMessageCommand = new RelayCommand(async (obj) =>
                {
                    this.Message = string.Empty;
                });

            return this._clearMessageCommand;
        }
    }

    private ICommand _closeApplicationCommand;

    public ICommand CloseApplicationCommand
    {
        get
        {
            if (this._closeApplicationCommand == null)
                this._closeApplicationCommand = new RelayCommand(obj =>
                {
                    if (this.Closed != null)
                        this.Closed();
                });

            return this._closeApplicationCommand;
        }
    }

    protected async Task CommandAsync(Func<Task> bodyAsync)
    {
        if (this.Busy)
            return;

        this.Busy = true;

        try
        {
            if (InternetGetConnectedState(out int description, 0) == false)
            {
                this.Message = "Lütfen internet bağlantınızı kontrol ediniz.";
                this.Busy = false;
                return;
            }
        }
        catch
        {
            this.Message = "Lütfen internet bağlantınızı kontrol ediniz.";
            this.Busy = false;
            return;
        }

        try
        {
            await bodyAsync();
        }
        catch
        {
            this.Message = "Bilinmeyen bir hata oluştu.";
            this.Busy = false;
            return;
        }

        this.Busy = false;
    }
}