﻿using System.Text.Json.Serialization;

namespace WarehouseDesktop.Wpf.Identity;

public class Token
{
    [JsonPropertyName("access_token")]
    public string AccessToken { get; set; }

    [JsonPropertyName("expires_in")]
    public int ExpireIn { get; set; }

    [JsonPropertyName("refresh_token")]
    public string RefreshToken { get; set; }

    public DateTime CreatedDateTime { get; set; }

    public bool Expired => (DateTime.Now - this.CreatedDateTime.AddSeconds(ExpireIn)).TotalSeconds < 30;
}