﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WarehouseDesktop.Wpf.Identity;

public static class ServiceRegistration
{
    public static void AddIdentity(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<IdentityClient>();
        serviceCollection.AddSingleton<IdentityClientCachingProxy>();
        serviceCollection.AddSingleton<IIdentityClient>(i => i.GetRequiredService<IdentityClientCachingProxy>());
    }

    public static void AddIdentityOptions(this IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection.Configure<IdentityOptions>(configuration.GetSection("IdentityOptions"));
    }
}