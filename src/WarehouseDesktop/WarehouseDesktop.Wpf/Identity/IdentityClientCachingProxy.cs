﻿using Microsoft.Extensions.Caching.Memory;

namespace WarehouseDesktop.Wpf.Identity;

public class IdentityClientCachingProxy : IIdentityClient
{
    private readonly IMemoryCache _memoryCache;

    private readonly IdentityClient _identityClient;

    public IdentityClientCachingProxy(IMemoryCache memoryCache, IdentityClient identityClient)
    {
        this._memoryCache = memoryCache;
        this._identityClient = identityClient;
    }

    public async Task<Token> GetTokenAsync(IdentityOptions helpyIdentityOptions)
    {
        string key = $"Token_{helpyIdentityOptions.Username}_{helpyIdentityOptions.Password}";

        if (this._memoryCache.TryGetValue(key, out Token token))
            return token;

        token = await this._identityClient.GetTokenAsync(helpyIdentityOptions);

        if (token != null)
            this._memoryCache.Set<Token>(key, token, new TimeSpan(0, 0, token.ExpireIn - 30));

        return token;
    }
}