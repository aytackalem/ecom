﻿namespace WarehouseDesktop.Wpf.Identity;

public interface IIdentityClient
{
    Task<Token> GetTokenAsync(IdentityOptions helpyIdentityOptions);
}