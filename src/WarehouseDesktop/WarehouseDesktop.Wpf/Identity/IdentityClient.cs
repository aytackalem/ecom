﻿using System.Net.Http;
using System.Net;
using System.Net.Http.Json;

namespace WarehouseDesktop.Wpf.Identity;

public class IdentityClient
{
    //Local
    //private const string _baseUrl = "http://localhost:5001";

    //Remote
    private const string _baseUrl = "https://identity.helpy.com.tr";

    public async Task<Token> GetTokenAsync(IdentityOptions helpyIdentityOptions)
    {
        Token token = null;

        using (HttpClient httpClient = new())
        {
            httpClient.BaseAddress = new Uri(_baseUrl);

            var formUrlEncodedContent = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                        new KeyValuePair<string, string>("client_id", helpyIdentityOptions.ClientId),
                        new KeyValuePair<string, string>("client_secret", helpyIdentityOptions.ClientSecret),
                        new KeyValuePair<string, string>("username", helpyIdentityOptions.Username),
                        new KeyValuePair<string, string>("password", helpyIdentityOptions.Password),
                        new KeyValuePair<string, string>("grant_type", "password")
                });

            var httpResponseMessage = await httpClient.PostAsync("/Connect/Token", formUrlEncodedContent);
            var httpContent = httpResponseMessage.Content;

            if (httpResponseMessage.StatusCode == HttpStatusCode.OK && httpContent != null)
                token = await httpResponseMessage.Content.ReadFromJsonAsync<Token>();
        }

        return token;
    }
}