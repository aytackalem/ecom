﻿namespace WarehouseDesktop.Wpf.Identity;

public class IdentityOptions
{
    public string ClientId { get; set; }

    public string ClientSecret { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public string DomainId { get; set; }

    public string CompanyId { get; set; }
}
