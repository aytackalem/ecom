﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class Currency
{
    public string Id { get; set; }
    public string Name { get; set; }
}
