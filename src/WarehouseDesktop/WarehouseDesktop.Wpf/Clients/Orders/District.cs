﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class District
{
    public int Id { get; set; }
    public string Name { get; set; }
    public City City { get; set; }
}
