﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class ProductInformation
{
    public int Id{get;set;} 
    public string Name{get;set;}
    public string VariantValues { get; set; }
    public string Barcode{get;set;} 
    public string StockCode{get;set;} 
    public ProductInformationPhoto ProductInformationPhoto{get;set;} 
    public Product Product{get;set;}
}
