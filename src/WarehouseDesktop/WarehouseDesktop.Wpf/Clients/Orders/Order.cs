﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class Order
{
    public int Id { get; set; }
    public OrderType OrderType { get; set; }
    public OrderSource OrderSource { get; set; }
    public Currency Currency { get; set; }
    public Marketplace Marketplace { get; set; }
    public Customer Customer { get; set; }
    public OrderDeliveryAddress OrderDeliveryAddress { get; set; }
    public OrderInvoiceInformation OrderInvoiceInformation { get; set; }
    public OrderBilling OrderBilling { get; set; }
    public List<OrderShipment> OrderShipments { get; set; }
    public List<OrderDetail> OrderDetails { get; set; }
    public List<OrderNote> OrderNotes { get; set; }
    public List<OrderReturnDetail> OrderReturnDetails { get; set; }
    public List<Payment> Payments { get; set; }
    public decimal Amount { get; set; }
    public decimal CargoFee { get; set; }
    public decimal SurchargeFee { get; set; }
    public bool BulkInvoicing { get; set; }
    public bool Micro { get; set; }
    public string MarketplaceOrderNumber { get; set; }
    public DateTime OrderDate { get; set; }
    public bool Commercial { get; set; }
    public Company Company { get; set; }
    public OrderPicking OrderPicking { get; set; }
    public OrderPacking OrderPacking { get; set; }
}
