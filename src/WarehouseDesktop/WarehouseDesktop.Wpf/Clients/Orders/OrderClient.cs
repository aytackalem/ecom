﻿using Helpy.Responses;
using System.Net.Http;
using System.Net.Http.Json;

namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class OrderClient
{
    //Local
    //private const string _endpoint = "http://localhost:5501/api/v1/orders";

    //Remote
    private const string _endpoint = "https://ordering.helpy.com.tr/api/v1/orders";

    public async Task<PagingResponse<Order>> FilterAsync(string packerBarcode, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<PagingResponse<Order>>($"{_endpoint}?Page=0&PageRecordsCount=1&PackerBarcode={packerBarcode}&OrderTypeId=TD");
            }
        }
        catch (Exception e)
        {
        }

        return null;
    }

    public async Task<Response<Order>> GetAsync(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<Response<Order>>($"{_endpoint}/{id}");
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response> InvoicedAsync(int id, string invoiceNumber, string accountingCompanyId, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync($"{_endpoint}/invoiced", new
                {
                    Id = id,
                    InvoiceNumber = invoiceNumber,
                    AccountingCompanyId = accountingCompanyId
                });

                var json = await httpResponseMessage.Content.ReadAsStringAsync();

                return await httpResponseMessage.Content.ReadFromJsonAsync<Response>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response> PackedAsync(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync($"{_endpoint}/packed", new
                {
                    Id = id
                });

                var json = await httpResponseMessage.Content.ReadAsStringAsync();

                return await httpResponseMessage.Content.ReadFromJsonAsync<Response>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response> ReadyForShipmentAsync(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync($"{_endpoint}/readyforshipment", new
                {
                    Id = id
                });

                var json = await httpResponseMessage.Content.ReadAsStringAsync();

                return await httpResponseMessage.Content.ReadFromJsonAsync<Response>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response> Returned(int id, string returnNumber, string returnDescription, List<OrderReturnDetail> orderReturnDetails, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync($"{_endpoint}/returned", new
                {
                    Id = id,
                    ReturnNumber = returnNumber,
                    ReturnDescription = returnDescription,
                    OrderReturnDetails = orderReturnDetails
                });
                return await httpResponseMessage.Content.ReadFromJsonAsync<Response>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response<List<string>>> TrendyolPackageNumbersAsync(string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<Response<List<string>>>($"{_endpoint}/TrendyolPackageNumbers");
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Response<List<int>>> TrendyolOrderNumbersAsync(string packageNumber, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<Response<List<int>>>($"{_endpoint}/TrendyolOrderNumbers?PackageNumber={packageNumber}");
            }
        }
        catch
        {
        }

        return null;
    }
}