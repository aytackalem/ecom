﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class OrderShipment
{
    public int Id { get; set; }
    public string TrackingCode { get; set; }
    public string TrackingUrl { get; set; }
    public string TrackingBase64 { get; set; }
    public string PackageNumber { get; set; }
    public ShipmentCompany ShipmentCompany { get; set; }
}
