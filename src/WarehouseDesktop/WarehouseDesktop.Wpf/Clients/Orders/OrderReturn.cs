﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class OrderReturn{ public string? ReturnNumber { get; set; } public string? ReturnDescription { get; set; }  public List<OrderReturnDetail> OrderReturnDetails { get; set; } }
