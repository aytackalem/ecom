﻿using Microsoft.Extensions.DependencyInjection;

namespace WarehouseDesktop.Wpf.Clients.Ordering;

public static class ServiceRegistration
{
    public static void AddOrdering(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<OrderClient>();
    }
}
