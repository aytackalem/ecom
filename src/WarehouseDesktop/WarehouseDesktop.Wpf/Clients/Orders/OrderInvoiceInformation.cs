﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class OrderInvoiceInformation { public int Id { get; set; } public string FirstName { get; set; } public string LastName { get; set; } public string Address { get; set; } public string? Phone { get; set; } public string? Mail { get; set; } public string? TaxOffice { get; set; } public string? TaxNumber { get; set; } public string? Url { get; set; } public string? Guid { get; set; } public string Number { get; set; } public Neighborhood Neighborhood { get; set; } }
