﻿namespace WarehouseDesktop.Wpf.Clients.Ordering;

public class Customer
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
}
