﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.Pivots.PivotShelves.Listing;

public class PivotShelf
{
    public int Id { get; set; }

    public string Name { get; set; }

    public int PivotId { get; set; }
}