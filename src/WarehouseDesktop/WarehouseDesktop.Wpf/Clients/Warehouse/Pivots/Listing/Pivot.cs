﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.Pivots.Listing;

public class Pivot
{
    public int Id { get; set; }

    public string Name { get; set; }
}
