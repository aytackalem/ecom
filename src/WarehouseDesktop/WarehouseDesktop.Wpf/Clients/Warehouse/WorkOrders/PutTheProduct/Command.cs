﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.WorkOrders.PutTheProduct;

public record Command(int WorkOrderItemId, int PivotShelfId);