﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.WorkOrders.Invoice;

public record Command(int Id, int OrderId, string InvoiceNumber);