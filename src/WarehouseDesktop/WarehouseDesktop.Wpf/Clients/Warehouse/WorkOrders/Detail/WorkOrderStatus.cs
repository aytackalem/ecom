﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.WorkOrders.Detail;

public enum WorkOrderStatus
{
    Pending,
    Picking,
    Picked,
    Packing,
    Packed
}
