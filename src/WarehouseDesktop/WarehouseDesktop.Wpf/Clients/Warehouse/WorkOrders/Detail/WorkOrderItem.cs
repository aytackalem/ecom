﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.WorkOrders.Detail;

public class WorkOrderItem
{
    public int Id { get; set; }

    public int WorkOrderId { get; set; }

    public int OrderId { get; set; }

    public int? PivotShelfId { get; set; }

    public string? InvoiceNumber { get; set; }

    public string Barcode { get; set; }

    public string Name { get; set; }

    public string PhotoUrl { get; set; }

    public int Quantity { get; set; }

    public int PuttedQuantity { get; set; }
}
