﻿using Microsoft.Extensions.DependencyInjection;

namespace WarehouseDesktop.Wpf.Clients.Warehouse;

public static class ServiceRegistration
{
    public static void AddWarehouse(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<WarehouseClient>();
    }
}
