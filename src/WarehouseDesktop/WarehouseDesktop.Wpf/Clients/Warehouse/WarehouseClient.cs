﻿using Shared.Results;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;

namespace WarehouseDesktop.Wpf.Clients.Warehouse;

public class WarehouseClient
{
    //Local
    //private const string _endpoint = "https://localhost:5001/v1";

    //Remote
    private const string _endpoint = "https://warehouse.helpy.tr/v1";

    public async Task<PagedResult<Warehouses.Listing.Warehouse>> WarehousesListingAsync(string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.GetAsync($"{_endpoint}/Warehouses?Page=0&PageRecordsCount=100");
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    var response = await httpResponseMessage.Content.ReadAsStringAsync();
                    return JsonSerializer.Deserialize<PagedResult<Warehouses.Listing.Warehouse>>(response, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
                }
                return null;
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<PagedResult<Pivots.Listing.Pivot>> PivotsListingAsync(int warehouseId, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<PagedResult<Pivots.Listing.Pivot>>($"{_endpoint}/Pivots?Page=0&PageRecordsCount=100&WarehouseId={warehouseId}");
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<PagedResult<Pivots.PivotShelves.Listing.PivotShelf>> PivotShelvesListingAsync(int pivotId, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<PagedResult<Pivots.PivotShelves.Listing.PivotShelf>>($"{_endpoint}/Pivots/PivotShelves?Page=0&PageRecordsCount=100&PivotId={pivotId}");
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Result<WorkOrders.Detail.WorkOrder>> WorkOrdersDetailAsync(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                return await httpClient.GetFromJsonAsync<Result<WorkOrders.Detail.WorkOrder>>($"{_endpoint}/WorkOrders/{id}");
            }
        }
        catch
        {
        }

        return new Result<WorkOrders.Detail.WorkOrder>
        {
            Failed = true,
            Messages = ["İş emri alınamadı."]
        };
    }

    public async Task<Result> Packing(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsync(requestUri: $"{_endpoint}/WorkOrders/{id}/Packing", content: null);
                return await httpResponseMessage.Content.ReadFromJsonAsync<Result>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Result> Packed(int id, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsync(requestUri: $"{_endpoint}/WorkOrders/{id}/Packed", content: null);
                return await httpResponseMessage.Content.ReadFromJsonAsync<Result>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Result> PutTheProduct(WorkOrders.PutTheProduct.Command command, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync(requestUri: $"{_endpoint}/WorkOrders/PutTheProduct", command);
                return await httpResponseMessage.Content.ReadFromJsonAsync<Result>();
            }
        }
        catch
        {
        }

        return null;
    }

    public async Task<Result> Invoice(WorkOrders.Invoice.Command command, string domainId, string companyId, string token)
    {
        try
        {
            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("DomainId", domainId);
                httpClient.DefaultRequestHeaders.Add("CompanyId", companyId);
                var httpResponseMessage = await httpClient.PatchAsJsonAsync(requestUri: $"{_endpoint}/WorkOrders/Invoice", command);
                return await httpResponseMessage.Content.ReadFromJsonAsync<Result>();
            }
        }
        catch
        {
        }

        return null;
    }
}
