﻿namespace WarehouseDesktop.Wpf.Clients.Warehouse.Warehouses.Listing;

public class Warehouse
{
    public int Id { get; set; }

    public string Name { get; set; }
}
