﻿using System.Drawing;
using System.Drawing.Printing;

namespace WarehouseDesktop.Wpf.Printing.Designer
{
    public class Text : Element
    {
        public Text(int width, int height)
        {
            Width = width; 
            Height = height;
        }

        public bool Uppercase { get; set; } = true;

        public bool AlignCenter { get; set; }

        public string Value { get; set; }

        public string FontFamily { get; set; }

        public int FontSize { get; set; }

        public bool Bold { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            e.Graphics.DrawString(Uppercase ? Value?.ToUpper() : Value, new Font(FontFamily, FontSize, Bold ? FontStyle.Bold : FontStyle.Regular), Brushes.Black, new RectangleF(x, y + 1, Width, Height - 2), new StringFormat
            {
                Alignment = AlignCenter ? StringAlignment.Center : StringAlignment.Near
            });
        }
    }
}