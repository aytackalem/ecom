﻿using System.Drawing.Printing;

namespace WarehouseDesktop.Wpf.Printing.Designer
{
    public abstract class Element
    {
        public int Height { get; set; }

        public int Width { get; set; }

        public abstract void Print(PrintPageEventArgs e, int x, int y);
    }
}