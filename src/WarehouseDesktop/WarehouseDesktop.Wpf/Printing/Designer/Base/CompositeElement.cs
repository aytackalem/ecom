﻿namespace WarehouseDesktop.Wpf.Printing.Designer
{
    public abstract class CompositeElement : Element
    {
        public CompositeElement()
        {
            Elements = new();
        }

        public List<Element> Elements { get; set; }
    }
}