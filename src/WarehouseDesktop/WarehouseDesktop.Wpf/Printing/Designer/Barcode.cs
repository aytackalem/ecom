﻿using System.Drawing;
using System.Drawing.Printing;
using Zen.Barcode;

namespace WarehouseDesktop.Wpf.Printing.Designer
{
    public class Barcode : Element
    {
        public Barcode(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public string Value { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            var image = BarcodeDrawFactory.Code128WithChecksum.Draw(Value, Height, 2);
            var bitmap = new Bitmap(image.Width, Height);

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImage(image, 0, 0);
            }

            e.Graphics.DrawImage(bitmap, new RectangleF(x, y, Width, Height));
        }
    }
}