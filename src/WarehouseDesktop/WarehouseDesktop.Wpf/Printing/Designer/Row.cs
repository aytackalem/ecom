﻿using System.Drawing.Printing;

namespace WarehouseDesktop.Wpf.Printing.Designer
{
    public class Row : CompositeElement
    {
        public Row(int width, int height, List<Element> elements)
        {
            Width = width;
            Height = height;

            elements.ForEach(e =>
            {
                if (e.Height == 0)
                    e.Height = Height;
            });

            Elements.AddRange(elements);
        }

        public int MarginTop { get; set; }

        public int MarginBottom { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            y += this.MarginTop;

            for (int i = 0; i < Elements.Count; i++)
            {
                Elements[i].Print(e, x, y);

                x += Elements[i].Width;
            }

            x = 0;
        }
    }
}