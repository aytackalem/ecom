﻿using System.Drawing.Printing;
using System.IO;
using WarehouseDesktop.Wpf.Printing.Base;
using WarehouseDesktop.Wpf.Printing.Designer;
using WarehouseDesktop.Wpf.Printing.Ordering;

namespace WarehouseDesktop.Wpf.Printing
{
    public class MarketplacePrint : PrintBase<Order>
    {
        #region Fields
        private string _companyInfo;

        private readonly string _fontFamily = "Barlow Condensed";

        private readonly int _pageHeight;
        #endregion

        #region Constructors
        public MarketplacePrint(string printerName, int pageHeight) : base(printerName)
        {
            _pageHeight = pageHeight;
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(Order order)
        {
            this._companyInfo = @$"{order.Company.Name}
{order.Company.CompanyContact.Address}
{order.Company.CompanyContact.PhoneNumber} {order.Company.CompanyContact.Email}";

            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(Order order, PrintPageEventArgs e)
        {
            int pageWidth = 90;
            int bodyFontSize = 7;
            int subHeaderFontSize = 10;
            int headerFontSize = 12;

            int row1 = 10, row2 = 25, row3 = 7, row4 = 6, row5 = 15, row6 = 6, row7 = 15, row8 = 25;
            if (_pageHeight == 100)
            {
                row1 = 10;
                row2 = 10;
                row3 = 7;
                row4 = 6;
                row5 = 10;
                row6 = 6;
                row7 = 15;
                row8 = 15;
            }

            var paymentDescription = string.Empty;
            if (order.OrderSource.Name == "Web" && order.Payments.Any(p => p.PaymentType.Id == "DO"))
                paymentDescription = $@"{order.Payments.FirstOrDefault(p => p.PaymentType.Id == "DO").PaymentType.Name}
TAHSİL EDİLECEK TUTAR: {order.Amount:N2}";

            if (order.OrderSource.Name == "Web" && order.Payments.Any(p => p.PaymentType.Id == "PDO"))
                paymentDescription = $@"{order.Payments.FirstOrDefault(p => p.PaymentType.Id == "PDO").PaymentType.Name}
TAHSİL EDİLECEK TUTAR: {order.Amount:N2}";

            Element element = null;
            if (File.Exists($@"C:\Images\{order.Company.Name}.png"))
                element = new Cell(40, 10, new Picture(34, 8)
                {
                    Src = $@"C:\Images\{order.Company.Name}.png"
                })
                {
                    Border = true,
                    AlignCenterChild = true,
                };
            else
                element = new Cell(40, 10, new Text(40, 10)
                {
                    Value = order.Company.Name,
                    FontFamily = _fontFamily,
                    FontSize = headerFontSize,
                    Bold = true,
                    AlignCenter = true
                })
                {
                    Border = true
                };

            Page page = new(new()
            {
                new Row(pageWidth, row1, new()
                {
                    new Cell(50, row1, new Text(50, row1) {
                        Value = $@"SİPARİŞ NO: {order.Id}
SİPARİŞ KAYNAĞI: {(order.Marketplace != null ? order.Marketplace.Name : order.Company.CompanyContact.WebUrl)}",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    },
                    element
                })
                {
                    MarginBottom = 5
                },
                new Row(pageWidth, row2, new()
                {
                    new Cell(pageWidth, row2, new Barcode(pageWidth, row2) {
                        Value = order.OrderShipments[0].TrackingCode
                    })
                }),
                new Row(pageWidth, row3, new()
                {
                    new Cell(pageWidth, row3, new Text(pageWidth, row3) {
                        Value = order.OrderShipments[0].TrackingCode,
                        FontFamily = _fontFamily,
                        FontSize = headerFontSize,
                        Bold = true,
                        AlignCenter = true
                    })
                })
                {
                    MarginBottom = 5
                },
                new Row(pageWidth, row4, new()
                {
                    new Cell(pageWidth, row4, new Text(pageWidth, row4) {
                        Value = "GÖNDERİCİ BİLGİLERİ",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, row5, new()
                {
                    new Cell(pageWidth, row5, new Text(pageWidth, row5) {
                        Value = $@"{order.Company.Name}
{order.Company.CompanyContact.Address}",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, row6, new()
                {
                    new Cell(pageWidth, row6, new Text(pageWidth, row6) {
                        Value = "ALICI BİLGİLERİ",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, row7, new()
                {
                    new Cell(pageWidth, row7, new Text(pageWidth, row7) {
                        Value = $@"{order.OrderDeliveryAddress.Recipient}
ADRES: {order.OrderDeliveryAddress.Address} {order.OrderDeliveryAddress.Neighborhood?.District?.Name} {order?.OrderDeliveryAddress.Neighborhood?.District?.City?.Name}
TELEFON: {order.OrderDeliveryAddress.PhoneNumber}",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, row8, new()
                {
                    new Cell(50, row8, new Text(50, row8) {
                        Value = paymentDescription,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Bold = true
                    })
                    {
                        Border = true
                    },
                    new Cell(40, row8, new Picture(40, row8) {
                        Src = @$"C:\Images\{order.OrderShipments[0].ShipmentCompany.Id}.png"
                    })
                    {
                        Border = true
                    }
                })
            })
            {
                MarginLeft = 5,
                MarginTop = 12
            };

            page.Print(e, 0, 0);
        }
        #endregion
    }
}
