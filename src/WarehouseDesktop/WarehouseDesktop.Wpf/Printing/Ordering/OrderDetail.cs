﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class OrderDetail { public int Id { get; set; } public int Quantity { get; set; } public decimal ListUnitPrice { get; set; } public decimal UnitPrice { get; set; } public decimal VatRate { get; set; } public decimal VatExcUnitPrice { get; set; } public decimal VatExcListPrice { get; set; } public decimal VatExcUnitDiscount { get; set; } public ProductInformation ProductInformation { get; set; } }
}
