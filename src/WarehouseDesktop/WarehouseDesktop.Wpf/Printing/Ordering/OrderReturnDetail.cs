﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class OrderReturnDetail
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public bool IsReturn { get; set; }
        public bool IsLoss { get; set; }
        public ProductInformation ProductInformation { get; set; }
    }
}
