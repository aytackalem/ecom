﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class OrderDeliveryAddress{ public int Id { get; set; } public string Recipient { get; set; } public string PhoneNumber { get; set; } public string Address { get; set; } public Neighborhood Neighborhood { get; set; } }
}
