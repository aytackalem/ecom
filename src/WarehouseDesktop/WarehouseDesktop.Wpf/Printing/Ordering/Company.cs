﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public CompanyContact CompanyContact { get; set; }
    }
}
