﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class Currency
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
