﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
