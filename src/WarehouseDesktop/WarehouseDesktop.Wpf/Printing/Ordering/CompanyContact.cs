﻿namespace WarehouseDesktop.Wpf.Printing.Ordering
{
    public class CompanyContact
    {
        public string TaxNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxOffice { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string WebUrl { get; set; }
    }
}
