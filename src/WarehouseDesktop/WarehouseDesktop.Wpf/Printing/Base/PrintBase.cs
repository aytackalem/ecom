﻿using System.Drawing;
using System.Drawing.Printing;

namespace WarehouseDesktop.Wpf.Printing.Base
{
    public abstract class PrintBase<T>
    {
        #region Fields
        protected readonly Brush _brushBlack;

        protected readonly Font _font7;

        protected readonly Font _font7Bold;

        protected readonly Font _font8;

        protected readonly Font _font8Bold;

        protected readonly Font _font12;

        protected readonly Font _font12Bold;

        protected string _printerName;

        protected PrintDocument _printDocument;
        #endregion

        #region Constructors
        public PrintBase(string printerName)
        {
            #region Fields
            this._brushBlack = Brushes.Black;
            this._font7 = new Font("Barlow Condensed", 7, FontStyle.Regular);
            this._font7Bold = new Font("Barlow Condensed", 7, FontStyle.Bold);
            this._font8 = new Font("Barlow Condensed", 8, FontStyle.Regular);
            this._font8Bold = new Font("Barlow Condensed", 8, FontStyle.Bold);
            this._font12 = new Font("Barlow Condensed", 12, FontStyle.Regular);
            this._font12Bold = new Font("Barlow Condensed", 12, FontStyle.Bold);
            this._printerName = printerName;
            #endregion
        }
        #endregion

        #region Methods
        public void Print(T t)
        {
            this._printDocument = new PrintDocument();

            this._printDocument.PrinterSettings.PrinterName = "Microsoft Print to PDF";
            //this._printDocument.PrinterSettings.PrinterName = this._printerName;

#if !DEBUG
             //this._printDocument.PrinterSettings.PrinterName = this._printerName;
#endif

            this._printDocument.PrintPage += (o, e) =>
            {
                e.Graphics.PageUnit = GraphicsUnit.Millimeter;
                this.Print(t, e);
            };

            this.PrintConcreate(t);
        }

        protected abstract void PrintConcreate(T t);

        protected abstract void Print(T t, PrintPageEventArgs e);
        #endregion
    }
}
