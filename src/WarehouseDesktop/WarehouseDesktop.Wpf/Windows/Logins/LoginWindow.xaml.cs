﻿using Microsoft.Extensions.DependencyInjection;
using System.Windows;
using WarehouseDesktop.Wpf.ViewModels.Logins;
using WarehouseDesktop.Wpf.Windows.Packings;

namespace WarehouseDesktop.Wpf.Windows.Logins
{
    public partial class LoginWindow : Window
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly LoginViewModel _loginViewModel;

        public LoginWindow(IServiceProvider serviceProvider, LoginViewModel loginViewModel)
        {
            InitializeComponent();

            this._serviceProvider = serviceProvider;

            this._loginViewModel = loginViewModel;
            this._loginViewModel.Logged += () =>
            {
                this.Hide();
                this._serviceProvider.GetRequiredService<PackingWindow>().Show();
            };
            this._loginViewModel.Closed += Application.Current.Shutdown;

            this.DataContext = this._loginViewModel;
        }
    }
}
