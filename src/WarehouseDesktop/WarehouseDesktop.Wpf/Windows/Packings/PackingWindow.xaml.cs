﻿using System.Windows;
using WarehouseDesktop.Wpf.ViewModels.Packings;

namespace WarehouseDesktop.Wpf.Windows.Packings
{
    public partial class PackingWindow : Window
    {
        private readonly PackingViewModel _packingViewModel;

        public PackingWindow(PackingViewModel packingViewModel)
        {
            InitializeComponent();

            this._packingViewModel = packingViewModel;
            this._packingViewModel.Closed += Application.Current.Shutdown;

            this.DataContext = this._packingViewModel;
        }
    }
}
