﻿using FluentValidation;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;
using Shared.Behaviours;
using System.Reflection;

namespace Catalog.Application;

public static class ApplicationRegistration
{
    public static void AddApplicationServices(IServiceCollection services)
    {
        var assembly = Assembly.GetExecutingAssembly();

        services.AddMediatR(_ => _.RegisterServicesFromAssembly(assembly));
        services.AddScoped(typeof(IRequestExceptionHandler<,,>), typeof(RequestExceptionHandler));
        services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour));
        services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour));
        services.AddValidatorsFromAssembly(assembly);
    }
}
