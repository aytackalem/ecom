﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Categories.Rename;

internal record Command(int Id, string Name) : IRequest<Result>;