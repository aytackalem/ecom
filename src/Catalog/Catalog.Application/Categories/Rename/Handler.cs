﻿using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Categories.Rename;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var category = await context.Categories.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken: cancellationToken);
        if (category is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Marka bulunamadı."]
            };
        }

        //category.Name = request.Name;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Kategori ismi başarılı bir şekilde güncellendi."]
        };
    }
}