﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Categories.Create;

internal record Command(string Name) : IRequest<Result<int>>;