﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Categories.Listing;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Query, PagedResult<Category>>
{
    public async Task<PagedResult<Category>> Handle(Query request, CancellationToken cancellationToken)
    {
        var count = await context.Categories.CountAsync(cancellationToken);
        var categories = await context.Categories.AsNoTracking().Skip(request.Page).Take(request.Page * request.Size).ToListAsync(cancellationToken);

        return new PagedResult<Category>
        {
            Page = request.Page,
            Data = categories,
            Messages = ["Kategoriler başarılı şekilde getirildi."],
            PageRecordsCount = request.Size,
            RecordsCount = count
        };
    }
}