﻿using ECommerce.Domain.Entities;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Categories.Listing;

internal record Query(int Page, int Size) : IRequest<PagedResult<Category>>;