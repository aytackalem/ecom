﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Variants.Listing;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Query, PagedResult<Variant>>
{
    public async Task<PagedResult<Variant>> Handle(Query request, CancellationToken cancellationToken)
    {
        var count = await context.Variants.CountAsync(cancellationToken);
        var variants = await context.Variants.AsNoTracking().Skip(request.Page).Take(request.Page * request.Size).ToListAsync(cancellationToken);

        return new PagedResult<Variant>
        {
            Page = request.Page,
            Data = variants,
            Messages = ["Varyantlar başarılı şekilde getirildi."],
            PageRecordsCount = request.Size,
            RecordsCount = count
        };
    }
}
