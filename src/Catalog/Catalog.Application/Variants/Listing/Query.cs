﻿using ECommerce.Domain.Entities;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Variants.Listing;

internal record Query(int Page, int Size) : IRequest<PagedResult<Variant>>;