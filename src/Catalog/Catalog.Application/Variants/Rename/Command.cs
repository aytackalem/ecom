﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Variants.Rename;

internal record Command(int Id, string Name) : IRequest<Result>;