﻿using FluentValidation;

namespace Catalog.Application.Variants.Rename;

internal class Validation : AbstractValidator<Command>
{
    public Validation()
    {
        RuleFor(_ => _.Id).GreaterThan(0).WithMessage("Kimlik bilgisi boş olamaz.");
        RuleFor(_ => _.Name).NotEmpty().WithMessage("İsim boş olamaz.").NotNull().WithMessage("İsim boş olamaz.");
    }
}