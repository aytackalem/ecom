﻿using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Variants.Rename;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var variant = await context.Variants.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken: cancellationToken);
        if (variant is null)
        {
            return new Result
            {
                Failed = false,
                Messages = ["Varyant bulunamadı."]
            };
        }

        //variant.Name = request.Name;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Failed = true,
            Messages = ["Varyant ismi başarılı bir şekilde güncellendi."]
        };
    }
}
