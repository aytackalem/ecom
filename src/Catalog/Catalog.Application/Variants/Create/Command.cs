﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Variants.Create;

internal record Command(string Name) : IRequest<Result<int>>;