﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Brands.Rename;

public record Command(int Id, string Name) : IRequest<Result>;