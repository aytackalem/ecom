﻿using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Brands.Rename;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var brand = await context.Brands.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken: cancellationToken);
        if (brand is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Marka bulunamadı."]
            };
        }

        brand.Name = request.Name;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Marka ismi başarılı bir şekilde güncellendi."]
        };
    }
}
