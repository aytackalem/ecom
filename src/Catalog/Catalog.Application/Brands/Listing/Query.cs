﻿using ECommerce.Domain.Entities;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Brands.Listing;

public record Query : IRequest<PagedResult<Brand>>;