﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Brands.Listing;

public class Handler(ApplicationDbContext context) : IRequestHandler<Query, PagedResult<Brand>>
{
    public Task<PagedResult<Brand>> Handle(Query request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}
