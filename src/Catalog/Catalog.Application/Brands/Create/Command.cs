﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Brands.Create;

public record Command(string Name) : IRequest<Result<int>>;