﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Brands.Create;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Command, Result<int>>
{
    public async Task<Result<int>> Handle(Command request, CancellationToken cancellationToken)
    {
        var brand = new Brand
        {
            Name = request.Name
        };
        context.Brands.Add(brand);
        await context.SaveChangesAsync(cancellationToken);

        return new Result<int>
        {
            Data = brand.Id,
            Messages = ["Marka başarılı şekilde oluşturuldu."]
        };
    }
}
