﻿using FluentValidation;

namespace Catalog.Application.Brands.Create;

internal class Validation : AbstractValidator<Command>
{
    public Validation()
    {
        RuleFor(_ => _.Name).NotEmpty().WithMessage("İsim boş olamaz.").NotNull().WithMessage("İsim boş olamaz.");
    }
}
