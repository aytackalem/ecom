﻿using ECommerce.Domain.Entities;
using MediatR;
using Shared.Results;

namespace Catalog.Application.Suppliers.Listing;

internal record Query(int Page, int Size) : IRequest<PagedResult<Supplier>>;