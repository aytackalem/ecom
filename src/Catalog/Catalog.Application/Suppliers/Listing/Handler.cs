﻿using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Suppliers.Listing;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Query, PagedResult<Supplier>>
{
    public async Task<PagedResult<Supplier>> Handle(Query request, CancellationToken cancellationToken)
    {
        var count = await context.Suppliers.CountAsync(cancellationToken);
        var suppliers = await context.Suppliers.AsNoTracking().Skip(request.Page).Take(request.Page * request.Size).ToListAsync(cancellationToken);

        return new PagedResult<Supplier>
        {
            Page = request.Page,
            Data = suppliers,
            Messages = ["Tedarikçiler başarılı şekilde getirildi."],
            PageRecordsCount = request.Size,
            RecordsCount = count
        };
    }
}
