﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Suppliers.Create;

internal record Command(string Name) : IRequest<Result<int>>;