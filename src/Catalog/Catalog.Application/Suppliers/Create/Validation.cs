﻿using FluentValidation;

namespace Catalog.Application.Suppliers.Create;

internal class Validation : AbstractValidator<Command>
{
    public Validation()
    {
        RuleFor(_ => _.Name).NotEmpty().WithMessage("İsim boş olamaz.").NotNull().WithMessage("İsim boş olamaz.");
    }
}