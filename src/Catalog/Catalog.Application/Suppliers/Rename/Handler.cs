﻿using ECommerce.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shared.Results;

namespace Catalog.Application.Suppliers.Rename;

internal class Handler(ApplicationDbContext context) : IRequestHandler<Command, Result>
{
    public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
    {
        var supplier = await context.Suppliers.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken: cancellationToken);
        if (supplier is null)
        {
            return new Result
            {
                Failed = true,
                Messages = ["Tedarikçi bulunamadı."]
            };
        }

        supplier.Name = request.Name;

        await context.SaveChangesAsync(cancellationToken);

        return new Result
        {
            Messages = ["Tedarikçi ismi başarılı bir şekilde güncellendi."]
        };
    }
}