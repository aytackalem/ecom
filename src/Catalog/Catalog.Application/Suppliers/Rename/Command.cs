﻿using MediatR;
using Shared.Results;

namespace Catalog.Application.Suppliers.Rename;

internal record Command(int Id, string Name) : IRequest<Result>;