﻿namespace Shared.Results;

public class PagedResult<T> : Result<List<T>>
{
    public int Page { get; set; }

    public int PageRecordsCount { get; set; }

    public int RecordsCount { get; set; }
}