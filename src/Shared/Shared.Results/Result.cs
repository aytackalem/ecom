﻿namespace Shared.Results;

public class Result
{
    public bool Failed { get; set; }

    public bool Invalid { get; set; }

    public bool Success => !Failed && !Invalid;

    public string[] Messages { get; set; }
}