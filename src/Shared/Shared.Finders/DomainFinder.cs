﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;

namespace Shared.Finders;

public class DomainFinder(IHttpContextAccessor HttpContextAccessor) : IDomainFinder
{
    private int _id;

    public int FindId()
    {
        if (_id == 0)
        {
            _id = int.Parse(HttpContextAccessor.HttpContext.Request.Headers["domainId"].ToString());
        }

        return _id;
    }

    public void Set(int id)
    {
        _id = id;
    }
}
