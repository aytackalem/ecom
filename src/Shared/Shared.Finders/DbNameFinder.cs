﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;

namespace Shared.Finders;

public class DbNameFinder(IHttpContextAccessor httpContextAccessor) : IDbNameFinder
{
    private string _dbName = string.Empty;

    public string FindName()
    {
        if (string.IsNullOrWhiteSpace(this._dbName))
        {
            _dbName = httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "brand").Value;
        }

        return _dbName;
    }

    public void Set(string dbName)
    {
        _dbName = dbName;
    }
}
