﻿using MediatR;
using Shared.Results;

namespace Shared.Behaviours;

public class LoggingBehaviour : IPipelineBehavior<IRequest, Result>
{
    public async Task<Result> Handle(IRequest request, RequestHandlerDelegate<Result> next, CancellationToken cancellationToken)
    {
        return await next();
    }
}
