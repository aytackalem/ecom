﻿using FluentValidation;
using MediatR;
using Shared.Results;

namespace Shared.Behaviours;

public class ValidationBehaviour(IEnumerable<IValidator<IRequest>> Validators) : IPipelineBehavior<IRequest, Result>
{
    public async Task<Result> Handle(IRequest request, RequestHandlerDelegate<Result> next, CancellationToken cancellationToken)
    {
        if (Validators.Any())
        {
            var context = new ValidationContext<IRequest>(request);

            var validationResults = await Task.WhenAll(Validators.Select(v => v.ValidateAsync(context, cancellationToken)));

            var errors = validationResults
                    .Where(_ => _.Errors.Count != 0)
                    .SelectMany(_ => _.Errors)
                    .ToList();

            if (errors.Count != 0)
            {
                return new Result
                {
                    Invalid = true,
                    Messages = errors.Select(_ => _.ErrorMessage).ToArray()
                };
            }
        }

        return await next();
    }
}
