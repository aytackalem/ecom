﻿using MediatR;
using MediatR.Pipeline;
using Shared.Results;

namespace Shared.Behaviours;

public class RequestExceptionHandler : IRequestExceptionHandler<IRequest, Result, Exception>
{
    public Task Handle(IRequest request, Exception exception, RequestExceptionHandlerState<Result> state, CancellationToken cancellationToken)
    {
        var result = new Result
        {
            Failed = true,
            Messages = ["Bilinmeyen bir hata oluştu."]
        };

        state.SetHandled(result);

        return Task.FromResult(result);
    }
}
