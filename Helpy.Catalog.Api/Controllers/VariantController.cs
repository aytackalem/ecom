using Helpy.Catalog.Api.Application.Variants.Create;
using Helpy.Catalog.Api.Application.Variants.Get;
using Helpy.Catalog.Api.Application.Variants.Put;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Catalog.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class VariantController : ControllerBase
    {
        private readonly IMediator _mediator;

        public VariantController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(PostCommand command, CancellationToken cancellationToken)
        {
            var result = await this._mediator.Send(command, cancellationToken);
            return new ObjectResult(result)
            {
                StatusCode = (int)result.Code
            };
        }

        [HttpPut]
        public async Task<IActionResult> Put(PutCommand command, CancellationToken cancellationToken)
        {
            var result = await this._mediator.Send(command, cancellationToken);
            return new ObjectResult(result)
            {
                StatusCode = (int)result.Code
            };
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetQuery query, CancellationToken cancellationToken)
        {
            var result = await this._mediator.Send(query, cancellationToken);
            return new ObjectResult(result)
            {
                StatusCode = (int)result.Code
            };
        }
    }
}