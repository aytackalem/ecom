//using Helpy.Catalog.Api.Application.Products.Create;
//using MediatR;
//using Microsoft.AspNetCore.Mvc;

//namespace Helpy.Catalog.Api.Controllers
//{
//    [ApiController]
//    [Route("[controller]")]
//    public class ProductController : ControllerBase
//    {
//        private readonly IMediator _mediator;

//        public ProductController(IMediator mediator)
//        {
//            this._mediator = mediator;
//        }

//        [HttpPost]
//        public async Task<IActionResult> Post(CreateCommand command)
//        {
//            return Ok(await this._mediator.Send(command));
//        }
//    }
//}