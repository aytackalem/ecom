//using Helpy.Catalog.Api.Application.Prices.Create;
//using MediatR;
//using Microsoft.AspNetCore.Mvc;

//namespace Helpy.Catalog.Api.Controllers
//{
//    [ApiController]
//    [Route("[controller]")]
//    public class PriceController : ControllerBase
//    {
//        private readonly IMediator _mediator;

//        public PriceController(IMediator mediator)
//        {
//            this._mediator = mediator;
//        }

//        [HttpPost]
//        public async Task<IActionResult> Post(CreateCommand command)
//        {
//            return Ok(await this._mediator.Send(command));
//        }
//    }
//}