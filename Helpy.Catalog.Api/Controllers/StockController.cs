//using Helpy.Catalog.Api.Application.Stocks.Create;
//using MediatR;
//using Microsoft.AspNetCore.Mvc;

//namespace Helpy.Catalog.Api.Controllers
//{
//    [ApiController]
//    [Route("[controller]")]
//    public class StockController : ControllerBase
//    {
//        private readonly IMediator _mediator;

//        public StockController(IMediator mediator)
//        {
//            this._mediator = mediator;
//        }

//        [HttpPost]
//        public async Task<IActionResult> Post(CreateCommand command)
//        {
//            return Ok(await this._mediator.Send(command));
//        }
//    }
//}