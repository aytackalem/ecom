﻿namespace Helpy.Catalog.Api.Application.Products
{
    public record ProductInformation(string Name, string Barcode, string StockCode, string SkuCode, string Description, List<ProductInformationVariant> ProductInformationVariants);
}