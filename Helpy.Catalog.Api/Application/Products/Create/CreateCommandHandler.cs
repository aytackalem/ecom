﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Products.Create
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateCommandHandler(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            bool success = false;

            var any = await this._unitOfWork.ProductRepository.DbSet().AnyAsync(p => p.SellerCode == request.Product.ModelCode);
            if (any)
            {
                var product = await this._unitOfWork.ProductRepository.DbSet().Include(p => p.ProductInformations).ThenInclude(pi => pi.ProductInformationVariants).FirstOrDefaultAsync(p => p.SellerCode == request.Product.ModelCode);

                foreach (var theProductInformation in request.Product.ProductInformations)
                {
                    if (!product.ProductInformations.Any(pi => pi.Barcode == theProductInformation.Barcode))
                    {
                        var productInformation = new ECommerce.Domain.Entities.ProductInformation
                        {
                            ProductInformationTranslations = new List<ECommerce.Domain.Entities.ProductInformationTranslation>
                            {
                                new ECommerce.Domain.Entities.ProductInformationTranslation
                                {
                                    LanguageId = "TR",
                                    Name = theProductInformation.Name,
                                    ProductInformationContentTranslation = new ECommerce.Domain.Entities.ProductInformationContentTranslation
                                    {
                                        Content = theProductInformation.Description,
                                        Description = theProductInformation.Name
                                    }
                                }
                            }
                        };
                        var variantValues = new List<string>();
                        var fullVariantValues = new List<string>();

                        foreach (var theProductInformationVariant in theProductInformation.ProductInformationVariants)
                        {
                            var variantValue = await this._unitOfWork.VariantValueRepository.DbSet().Include(vv => vv.Variant).FirstOrDefaultAsync(vv => vv.VariantValueTranslations.Any(vvt => vvt.Value == theProductInformationVariant.VariantValueName) && vv.Variant.VariantTranslations.Any(vt => vt.Name == theProductInformationVariant.VariantName));
                            if (variantValue != null)
                            {
                                throw new ArgumentException("Variant Value not found.");
                            }

                            if (variantValue.Variant.ShowVariant)
                            {
                                variantValues.Add(theProductInformationVariant.VariantValueName);
                            }

                            fullVariantValues.Add(theProductInformationVariant.VariantValueName);

                            productInformation.ProductInformationVariants ??= new();
                            productInformation.ProductInformationVariants.Add(new ECommerce.Domain.Entities.ProductInformationVariant
                            {
                                VariantValueId = variantValue.Id
                            });
                        }

                        productInformation.ProductInformationTranslations.FirstOrDefault().VariantValuesDescription = $"({string.Join(",", variantValues)})";
                        productInformation.ProductInformationTranslations.FirstOrDefault().FullVariantValuesDescription = $"({string.Join(",", fullVariantValues)})";

                        product.ProductInformations.Add(productInformation);
                    }
                }
            }
            else
            {
                var product = new ECommerce.Domain.Entities.Product
                {
                    ProductTranslations = new List<ECommerce.Domain.Entities.ProductTranslation>
                    {
                        new ECommerce.Domain.Entities.ProductTranslation
                        {
                            LanguageId = "TR",
                            Name = request.Product.ProductName
                        }
                    }
                };

                foreach (var theProductInformation in request.Product.ProductInformations)
                {
                    var productInformation = new ECommerce.Domain.Entities.ProductInformation
                    {
                        ProductInformationTranslations = new List<ECommerce.Domain.Entities.ProductInformationTranslation>
                            {
                                new ECommerce.Domain.Entities.ProductInformationTranslation
                                {
                                    LanguageId = "TR",
                                    Name = theProductInformation.Name,
                                    ProductInformationContentTranslation = new ECommerce.Domain.Entities.ProductInformationContentTranslation
                                    {
                                        Content = theProductInformation.Description,
                                        Description = theProductInformation.Name
                                    }
                                }
                            }
                    };
                    var variantValues = new List<string>();
                    var fullVariantValues = new List<string>();

                    foreach (var theProductInformationVariant in theProductInformation.ProductInformationVariants)
                    {
                        var variantValue = await this._unitOfWork.VariantValueRepository.DbSet().Include(vv => vv.Variant).FirstOrDefaultAsync(vv => vv.VariantValueTranslations.Any(vvt => vvt.Value == theProductInformationVariant.VariantValueName) && vv.Variant.VariantTranslations.Any(vt => vt.Name == theProductInformationVariant.VariantName));
                        if (variantValue != null)
                        {
                            throw new ArgumentException("Variant Value not found.");
                        }

                        if (variantValue.Variant.ShowVariant)
                        {
                            variantValues.Add(theProductInformationVariant.VariantValueName);
                        }

                        fullVariantValues.Add(theProductInformationVariant.VariantValueName);

                        productInformation.ProductInformationVariants ??= new();
                        productInformation.ProductInformationVariants.Add(new ECommerce.Domain.Entities.ProductInformationVariant
                        {
                            VariantValueId = variantValue.Id
                        });
                    }

                    productInformation.ProductInformationTranslations.FirstOrDefault().VariantValuesDescription = $"({string.Join(",", variantValues)})";
                    productInformation.ProductInformationTranslations.FirstOrDefault().FullVariantValuesDescription = $"({string.Join(",", fullVariantValues)})";

                    product.ProductInformations.Add(productInformation);
                }
            }

            return new Response(ResponseCodes.Ok);
        }
    }
}
