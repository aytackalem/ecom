﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Products.Create
{
    public record CreateCommand(Product Product) : IRequest<Response>;
}
