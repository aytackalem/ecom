﻿namespace Helpy.Catalog.Api.Application.Products
{
    public record ProductInformationVariant(string VariantName, string VariantValueName);
}
