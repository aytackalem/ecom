﻿namespace Helpy.Catalog.Api.Application.Products
{
    public record Product(string CategoryName, string ModelCode, string ProductName, List<ProductInformation> ProductInformations);
}
