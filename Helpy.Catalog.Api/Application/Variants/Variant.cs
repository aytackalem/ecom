﻿namespace Helpy.Catalog.Api.Application.Variants
{
    public record Variant(int Id, string Code, string Name);
}
