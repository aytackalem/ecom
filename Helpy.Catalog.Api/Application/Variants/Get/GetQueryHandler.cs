﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Variants.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, Response<List<Variant>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<List<Variant>>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            return new Response<List<Variant>>(ResponseCodes.Ok, await _unitOfWork.VariantRepository.DbSet().Select(v => new Variant(v.Id, v.Code, v.VariantTranslations.FirstOrDefault(vt => vt.LanguageId == "TR").Name)).ToListAsync(cancellationToken));
        }
    }
}
