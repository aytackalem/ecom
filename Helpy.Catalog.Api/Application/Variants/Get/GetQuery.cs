﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Variants.Get
{
    public record GetQuery : IRequest<Response<List<Variant>>>;
}
