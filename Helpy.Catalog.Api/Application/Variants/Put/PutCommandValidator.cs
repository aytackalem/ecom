﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.Variants.Put
{
    public class PutCommandValidator : AbstractValidator<PutCommand>
    {
        public PutCommandValidator()
        {
            RuleFor(cc => cc.Id).GreaterThan(0);
            RuleFor(cc => cc.Name).NotEmpty();
        }
    }
}
