﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Variants.Put
{
    public record PutCommand(int Id, string Code, string Name) : IRequest<Response>;
}
