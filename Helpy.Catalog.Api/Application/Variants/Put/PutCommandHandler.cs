﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Variants.Put
{
    public class PutCommandHandler : IRequestHandler<PutCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PutCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PutCommand request, CancellationToken cancellationToken)
        {
            var variant = await _unitOfWork
                .VariantRepository
                .DbSet()
                    .Include(i => i.VariantTranslations)
                .FirstOrDefaultAsync(v => v.Id == request.Id, cancellationToken);

            if (variant == null)
                return new Response(ResponseCodes.Bad, "Varyasyon bulunamadı.");

            variant.Code = request.Code;
            variant.VariantTranslations.ForEach(ct =>
            {
                ct.Name = request.Name;
            });

            var updated = await _unitOfWork.VariantRepository.UpdateAsync(variant, cancellationToken);

            return new Response(ResponseCodes.Ok);
        }
    }
}
