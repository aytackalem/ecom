﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.Variants.Create
{
    public class PostCommandValidator : AbstractValidator<PostCommand>
    {
        public PostCommandValidator()
        {
            RuleFor(cc => cc.Name).NotEmpty();
        }
    }
}
