﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Variants.Create
{
    public record PostCommand(string Code, string Name) : IRequest<Response<int>>;
}
