﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Variants.Create
{
    public class PostCommandHandler : IRequestHandler<PostCommand, Response<int>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<int>> Handle(PostCommand request, CancellationToken cancellationToken)
        {
            var any = await _unitOfWork
                .VariantRepository
                .DbSet()
                .AnyAsync(
                    c =>
                        c.Code == request.Code ||
                        c.VariantTranslations.Any(ct => ct.Name == request.Name),
                    cancellationToken);

            if (any)
                return new Response<int>(ResponseCodes.Bad, "Varyasyon mevcut.", default);

            ECommerce.Domain.Entities.Variant variant = new()
            {
                Code = request.Code,
                VariantTranslations = new()
                {
                    new()
                    {
                        LanguageId = "TR",
                        Name = request.Name
                    }
                }
            };

            var created = await _unitOfWork.VariantRepository.CreateAsync(variant, cancellationToken);
            if (created)
                return new Response<int>(ResponseCodes.Ok, variant.Id);

            return new Response<int>(ResponseCodes.Bad, default);
        }
    }
}
