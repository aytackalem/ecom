﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.VariantValue.Put
{
    public record PutCommand(int Id, string Code, int VariantId, string VariantCode, string Value) : IRequest<Response>;
}
