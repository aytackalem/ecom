﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.VariantValue.Put
{
    public class PutCommandHandler : IRequestHandler<PutCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PutCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PutCommand request, CancellationToken cancellationToken)
        {
            var variantValue = await _unitOfWork
                .VariantValueRepository
                .DbSet()
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (variantValue == null)
                return new Response(ResponseCodes.Bad, "Varyasyon değeri bulunamadı.");

            variantValue.Code = request.Code;
            variantValue.VariantValueTranslations.ForEach(vvt =>
            {
                vvt.Value = request.Value;
            });

            var updated = await _unitOfWork.VariantValueRepository.UpdateAsync(variantValue, cancellationToken);

            if (updated)
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Error);
        }
    }
}
