﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.VariantValue.Put
{
    public class PutCommandValidator : AbstractValidator<PutCommand>
    {
        public PutCommandValidator()
        {
            RuleFor(pc => pc.Id).GreaterThan(0);
            RuleFor(pc => pc.Value).NotEmpty();
        }
    }
}
