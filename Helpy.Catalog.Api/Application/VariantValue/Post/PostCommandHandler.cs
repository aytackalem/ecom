﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.VariantValue.Post
{
    public class PostCommandHandler : IRequestHandler<PostCommand, Response<int>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<int>> Handle(PostCommand request, CancellationToken cancellationToken)
        {
            var any = await _unitOfWork
                .VariantValueRepository
                .DbSet()
                .AnyAsync(c => c.Code == request.Code || c.VariantValueTranslations.Any(ct => ct.Value == request.Value), cancellationToken);

            if (any)
                return new Response<int>(ResponseCodes.Bad, "Varyasyon değeri mevcut.", default);

            var iQueryable = _unitOfWork.VariantRepository.DbSet();

            if (request.VariantId > 0)
                iQueryable = iQueryable.Where(v => v.Id == request.VariantId);

            if (string.IsNullOrEmpty(request.VariantCode) == false)
                iQueryable = iQueryable.Where(v => v.Code == request.VariantCode);

            var variant = await iQueryable.FirstOrDefaultAsync(cancellationToken);

            if (variant == null)
                return new Response<int>(ResponseCodes.Bad, "Varyasyon bulunamadı.", default);

            ECommerce.Domain.Entities.VariantValue variantValue = new()
            {
                Code = request.Code,
                VariantValueTranslations = new()
                {
                    new()
                    {
                        LanguageId = "TR",
                        Value = request.Value
                    }
                }
            };

            var created = await _unitOfWork.VariantValueRepository.CreateAsync(variantValue, cancellationToken);

            if (created)
                return new Response<int>(ResponseCodes.Ok, variantValue.Id);

            return new Response<int>(ResponseCodes.Bad, default);
        }
    }
}
