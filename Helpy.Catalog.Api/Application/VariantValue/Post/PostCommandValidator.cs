﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.VariantValue.Post
{
    public class PostCommandValidator : AbstractValidator<PostCommand>
    {
        public PostCommandValidator()
        {
            RuleFor(pc => pc.Value).NotEmpty();
        }
    }
}
