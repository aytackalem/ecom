﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.VariantValue.Post
{
    public record PostCommand(string Code, int VariantId, string VariantCode, string Value) : IRequest<Response<int>>;
}
