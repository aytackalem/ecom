﻿namespace Helpy.Catalog.Api.Application.VariantValue
{
    public record VariantValue(int Id, string Code, int VariantId, string Value);
}
