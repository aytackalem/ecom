﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.VariantValue.Get
{
    public class GetQueryValidator : AbstractValidator<GetQuery>
    {
        public GetQueryValidator()
        {
            RuleFor(gq => gq.VariantId).GreaterThan(0);
        }
    }
}
