﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.VariantValue.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, Response<List<VariantValue>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<List<VariantValue>>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            return new Response<List<VariantValue>>(ResponseCodes.Ok, await _unitOfWork.VariantValueRepository.DbSet().Where(vv => vv.VariantId == request.VariantId).Select(vv => new VariantValue(vv.Id, vv.Code, vv.VariantId, vv.VariantValueTranslations.FirstOrDefault(vvt => vvt.LanguageId == "TR").Value)).ToListAsync());
        }
    }
}
