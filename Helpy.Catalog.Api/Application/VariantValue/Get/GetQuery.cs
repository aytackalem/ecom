﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.VariantValue.Get
{
    public record GetQuery(int VariantId) : IRequest<Response<List<VariantValue>>>;
}
