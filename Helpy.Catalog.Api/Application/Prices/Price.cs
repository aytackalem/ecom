﻿namespace Helpy.Catalog.Api.Application.Prices
{
    public record Price(string StockCode, string Barcode, string MarketplaceId, decimal ListUnitPrice, decimal UnitPrice);
}
