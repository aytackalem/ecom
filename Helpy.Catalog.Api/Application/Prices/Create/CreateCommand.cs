﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Prices.Create
{
    public record CreateCommand(Price Price) : IRequest<Response>;
}
