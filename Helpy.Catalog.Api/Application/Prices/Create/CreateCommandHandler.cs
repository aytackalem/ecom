﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Prices.Create
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateCommandHandler(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            bool success = false;

            var productInformationMarketplace = await this._unitOfWork.ProductInformationMarketplaceRepository.DbSet().FirstOrDefaultAsync(pim => pim.MarketplaceId == request.Price.MarketplaceId && (pim.Barcode == request.Price.Barcode || pim.StockCode == request.Price.StockCode));
            if (productInformationMarketplace != null)
            {
                productInformationMarketplace.UnitPrice = request.Price.UnitPrice;
                productInformationMarketplace.ListUnitPrice = request.Price.ListUnitPrice;

                success = await this._unitOfWork.ProductInformationMarketplaceRepository.UpdateAsync(productInformationMarketplace);
            }

            return new Response(ResponseCodes.Ok);
        }
    }
}
