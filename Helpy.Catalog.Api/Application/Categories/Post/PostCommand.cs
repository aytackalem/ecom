﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Categories.Create
{
    public record PostCommand(string Code, string Name, string Information) : IRequest<Response<int>>;
}
