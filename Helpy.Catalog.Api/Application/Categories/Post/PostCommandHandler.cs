﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Categories.Create
{
    public class PostCommandHandler : IRequestHandler<PostCommand, Response<int>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostCommandHandler(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response<int>> Handle(PostCommand request, CancellationToken cancellationToken)
        {
            var any = await _unitOfWork
                .CategoryRepository
                .DbSet()
                .AnyAsync(
                    c =>
                        c.Code == request.Code ||
                        c.CategoryTranslations.Any(ct => ct.Name == request.Name),
                    cancellationToken);

            if (any)
                return new Response<int>(ResponseCodes.Bad, "Kategori mevcut.", default);

            ECommerce.Domain.Entities.Category category = new()
            {
                Code = request.Code,
                CategoryTranslations = new()
                {
                    new()
                    {
                        LanguageId = "TR",
                        Name = request.Name,
                        CategoryTranslationBreadcrumb = new()
                        {
                            Html = request.Information
                        }
                    }
                }
            };

            var created = await _unitOfWork.CategoryRepository.CreateAsync(category, cancellationToken);

            return new Response<int>(ResponseCodes.Ok, category.Id);
        }
    }
}
