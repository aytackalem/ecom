﻿using FluentValidation;
using Helpy.Catalog.Api.Application.Categories.Create;

namespace Helpy.Catalog.Api.Application.Categories.Post
{
    public class PostCommandValidator : AbstractValidator<PostCommand>
    {
        public PostCommandValidator()
        {
            RuleFor(pc => pc.Name).NotEmpty();
            RuleFor(pc => pc.Name).MaximumLength(100);
            RuleFor(pc => pc.Information).MaximumLength(500);
        }
    }
}
