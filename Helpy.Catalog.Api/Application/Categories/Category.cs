﻿namespace Helpy.Catalog.Api.Application.Categories
{
    public record Category(int Id, string Code, string Name, string Information);
}
