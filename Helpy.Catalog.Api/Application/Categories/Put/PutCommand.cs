﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Categories.Put
{
    public record PutCommand(int Id, string Code, string Name, string Information) : IRequest<Response>;
}
