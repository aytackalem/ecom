﻿using FluentValidation;

namespace Helpy.Catalog.Api.Application.Categories.Put
{
    public class PutCommandValidator : AbstractValidator<PutCommand>
    {
        public PutCommandValidator()
        {
            RuleFor(cc => cc.Id).GreaterThan(0);
            RuleFor(pc => pc.Name).NotEmpty();
        }
    }
}
