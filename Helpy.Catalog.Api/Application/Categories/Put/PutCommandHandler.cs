﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Categories.Put
{
    public class PutCommandHandler : IRequestHandler<PutCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PutCommandHandler(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PutCommand request, CancellationToken cancellationToken)
        {
            var category = await _unitOfWork
                .CategoryRepository
                .DbSet()
                    .Include(i => i.CategoryTranslations)
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (category == null)
                return new Response(ResponseCodes.Bad, "Kategori bulunamadı.");

            category.Code = request.Code;
            category.CategoryTranslations.ForEach(ct =>
            {
                ct.Name = request.Name;
                ct.CategoryTranslationBreadcrumb ??= new();
                ct.CategoryTranslationBreadcrumb.Html = request.Information;
            });

            var updated = await _unitOfWork.CategoryRepository.UpdateAsync(category, cancellationToken);

            if (updated)
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Error);
        }
    }
}
