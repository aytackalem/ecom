﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Categories.Get
{
    public class GetQuery : IRequest<Response<List<Category>>>
    {
    }
}
