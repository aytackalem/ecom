﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Categories.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, Response<List<Category>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetQueryHandler(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response<List<Category>>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            return new Response<List<Category>>(ResponseCodes.Ok, await this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                        .Include(c => c.CategoryTranslations)
                            .ThenInclude(ct => ct.CategoryTranslationBreadcrumb)
                    .Select(c => new Category(
                        c.Id,
                        c.Code,
                        c.CategoryTranslations.FirstOrDefault().Name,
                        c.CategoryTranslations.FirstOrDefault().CategoryTranslationBreadcrumb.Html))
                    .ToListAsync(cancellationToken));
        }
    }
}
