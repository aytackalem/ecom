﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Catalog.Api.Application.Stocks.Create
{
    public record CreateCommand(Stock Stock) : IRequest<Response>;
}
