﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Catalog.Api.Application.Stocks.Create
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            bool success = false;

            var productInformation = await this._unitOfWork.ProductInformationRepository.DbSet().FirstOrDefaultAsync(pi => pi.Barcode == request.Stock.Barcode || pi.StockCode == request.Stock.StockCode);
            if (productInformation != null)
            {
                productInformation.Stock = request.Stock.Quantity;

                success = this._unitOfWork.ProductInformationRepository.Update(productInformation);
            }

            return new Response(ResponseCodes.Ok);
        }
    }
}
