﻿namespace Helpy.Catalog.Api.Application.Stocks
{
    public record Stock(string StockCode, string Barcode, int Quantity);
}
