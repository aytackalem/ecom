using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using FluentValidation;
using Helpy.Behaviours;
using Helpy.Catalog.Api.Finders;
using Helpy.Middlewares;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Reflection;

namespace Helpy.Catalog.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddDbContext<ApplicationDbContext>();
            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();
            builder.Services.AddScoped<ITenantFinder, TenantFinder>();
            builder.Services.AddScoped<IDomainFinder, DomainFinder>();
            builder.Services.AddScoped<ICompanyFinder, CompanyFinder>();
            builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            builder.Services.AddScoped(typeof(IRequestExceptionHandler<,,>), typeof(ExceptionBehaviour<,,>));
            builder.Services.AddMediatR(configuration =>
            {
                configuration.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
            });
            builder.Services.AddApiVersioning(setupAction =>
            {
                setupAction.ReportApiVersions = true;
            });
            builder.Services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
                {
#if DEBUG
                    o.RequireHttpsMetadata = false;
#endif

                    o.Authority = builder.Configuration["IdentityServerUrl"];
                    o.Audience = "ApiResourceCatalog";
                });
            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseDomainIdCheck();
            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}