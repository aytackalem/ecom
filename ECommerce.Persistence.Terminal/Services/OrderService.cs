﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Services
{
    public class OrderService : IOrderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        public readonly IDomainFinder _domainFinder;

        public readonly ICompanyFinder _companyFinder;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructor
        public OrderService(IUnitOfWork unitOfWork, IDomainFinder domainFinder, ICompanyFinder companyFinder, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Method
        public DataResponse<Order> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<Order>>((response) =>
            {
                var reserveResponse = Reserve();

                if (!reserveResponse.Success)
                {
                    response.Success = false;
                    response.Message = reserveResponse.Message;
                    return;
                }

                var photos = new List<ProductInformationPhoto>()
                {
                new ProductInformationPhoto
                {
                    FileName= $"{this._settingService.ProductImageUrl}/0/no-image.jpg"
                }
                };

                var order = _unitOfWork
                .OrderRepository.
                 DbSet()
                .Where(x => x.Id == reserveResponse.Data)
                .Select(x => new Order
                {
                    Id = x.Id,
                    Customer = new Customer
                    {
                        Name = x.Customer.Name,
                        Surname = x.Customer.Surname
                    },
                    Amount = x.Total,
                    CompanyName = x.Company.Name,
                    CreatedDate = x.CreatedDate,
                    CurrencyId = x.CurrencyId,
                    CustomerId = x.CustomerId,
                    MarketPlaceId = x.MarketplaceId,
                    OrderTypeId = x.OrderTypeId,
                    OrderSourceName = x.OrderSource.Name,
                    MarketPlace = new MarketPlace
                    {
                        Id = x.Marketplace.Id,
                        Name = x.Marketplace.Name
                    },
                    OrderDetails = x.OrderDetails.Select(od => new OrderDetail
                    {

                        ProductInformationId = od.ProductInformationId,
                        OrderId = od.OrderId,
                        Quantity = od.Quantity,
                        ProductInformation = new ProductInformation
                        {
                            Barcode = od.ProductInformation.Barcode,
                            StockCode = od.ProductInformation.StockCode,
                            Payor = od.Payor,
                            ModelCode = od.ProductInformation.Product.SellerCode,
                            ShelfCode = od.ProductInformation.ShelfCode,
                            ShelfZone = od.ProductInformation.ShelfZone,
                            ProductInformationName = od.ProductInformation.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == this._settingService.DefaultLanguageId).Name,
                            ProductInformationPhotos = od.ProductInformation.ProductInformationPhotos.Count > 0 ? od.ProductInformation.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                            {
                                FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}",
                                DisplayOrder = pip.DisplayOrder
                            }).ToList()
                            : photos,
                            ProductVariants = od.ProductInformation.ProductInformationVariants.Select(pv => new ProductInformationVariant
                            {
                                VariantValue = new VariantValue
                                {
                                    Value = pv.VariantValue.VariantValueTranslations.FirstOrDefault(y => y.LanguageId == this._settingService.DefaultLanguageId).Value,
                                    Variant = new Variant
                                    {
                                        Name = pv.VariantValue.Variant.VariantTranslations.FirstOrDefault(y => y.LanguageId == this._settingService.DefaultLanguageId).Name,
                                        Photoable = pv.VariantValue.Variant.Photoable
                                    }
                                },
                                VariantValueId = pv.VariantValueId

                            }).ToList()
                        }
                    }).ToList()
                })
                .FirstOrDefault();
                if (order == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }
                response.Success = true;
                response.Data = order;
            });
        }

        public Response Skip(Order order)
        {
            return ExceptionHandler.ResultHandle<Response>((result) =>
            {


                var _order = _unitOfWork.OrderRepository.Read(order.Id);
                if (_order == null)
                {
                    result.Success = false;
                    result.Message = "Sipariş bulunamadı.";
                    return;
                }

                if (order.IsEstimatedPackingDate)
                {
                    _order.EstimatedPackingDate = DateTime.Now.AddMinutes(10);
                }

                _order.OrderTypeId = "OS";
                _unitOfWork.OrderRepository.Update(_order);
                result.Success = true;

            });
        }

        public Response Approved(Order order)
        {
            ECommerce.Domain.Entities.Order entity = null;

            return ExceptionHandler.ResultHandle<Response>((result) =>
            {

                entity = _unitOfWork.OrderRepository.Read(order.Id);
                if (entity == null)
                {
                    result.Success = false;
                    result.Message = "Sipariş bulunamadı.";
                    return;
                }


                entity.OrderTypeId = "TD";
                entity.OrderPicking = new ECommerce.Domain.Entities.OrderPicking
                {
                    TenantId = entity.TenantId,
                    DomainId = entity.DomainId,
                    CompanyId = entity.CompanyId,
                    CreatedDate = System.DateTime.Now,
                    PackerBarcode = order.PackerBarcode,
                    
                };

                this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                result.Success = true;

            });
        }

        public PagedResponse<Order> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Order>>((response) =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsQueryable();

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Id:"))
                    {
                        var value = pLoop.Replace("Id:", "");
                        if (value.Length > 0)
                        {
                            var id = int.Parse(value);
                            iQueryable = iQueryable.Where(iq => iq.Id == id);
                        }
                    }
                    else if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Customer.Name.Contains(value));
                    }
                    else if (pLoop.StartsWith("Surname:"))
                    {
                        var value = pLoop.Replace("Surname:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Customer.Surname.Contains(value));
                    }
                    else if (pLoop.StartsWith("Marketplace:"))
                    {
                        var value = pLoop.Replace("Marketplace:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.MarketplaceId == value);
                    }
                    else if (pLoop.StartsWith("ShipmentCompany:"))
                    {
                        var value = pLoop.Replace("ShipmentCompany:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.OrderShipments.Any(os => os.ShipmentCompanyId == value));
                    }
                    else if (pLoop.StartsWith("OrderType:"))
                    {
                        var value = pLoop.Replace("OrderType:", "");
                        if (value.Length > 0)
                        {
                            var values = value.Split(",", StringSplitOptions.RemoveEmptyEntries);
                            iQueryable = iQueryable.Where(iq => values.Contains(iq.OrderTypeId));
                        }
                    }
                    else if (pLoop.StartsWith("MarketplaceOrderNumber:"))
                    {
                        var value = pLoop.Replace("MarketplaceOrderNumber:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.MarketplaceOrderNumber == value);
                    }
                    else if (pLoop.StartsWith("CargoTrackingNumber:"))
                    {
                        var value = pLoop.Replace("CargoTrackingNumber:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.OrderShipments.Any(os => os.TrackingCode == value));
                    }
                    else if (pLoop.StartsWith("OrderSource:"))
                    {
                        var value = pLoop.Replace("OrderSource:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            var orderSourceId = int.Parse(value);
                            iQueryable = iQueryable.Where(iq => iq.OrderSourceId == orderSourceId);
                        }
                    }
                    else if (pLoop.StartsWith("PhoneNumber:"))
                    {
                        var value = pLoop.Replace("PhoneNumber:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            iQueryable = iQueryable.Where(iq => iq.Customer.CustomerContact.Phone == value ||
                                                                iq.OrderDeliveryAddress.PhoneNumber == value ||
                                                                iq.OrderInvoiceInformation.Phone == value);
                        }
                    }
                    else if (pLoop.StartsWith("Date:"))
                    {
                        var value = pLoop.Replace("Date:", "");
                        if (!string.IsNullOrEmpty(value))
                        {
                            var dates = value.Split(new string[] { "/" }, System.StringSplitOptions.RemoveEmptyEntries);
                            var startDate = DateTime.Parse(dates[0].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                            var endDate = DateTime.Parse(dates[1].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                            iQueryable = iQueryable.Where(iq => iq.OrderDate.Date >= startDate && iq.OrderDate.Date <= endDate.Date);
                        }
                    }
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable
                    .Count();

                if (!string.IsNullOrEmpty(pagedRequest.Sort))
                {
                    if (pagedRequest.Sort == "Id")
                        iQueryable = iQueryable.OrderByDescending(o => o.Id);
                    else if (pagedRequest.Sort == "OrderDate")
                        iQueryable = iQueryable.OrderByDescending(o => o.OrderDate);
                }

                var photos = new List<ProductInformationPhoto>()
                {
                    new ProductInformationPhoto
                    {
                    FileName= $"{this._settingService.ProductImageUrl}/0/no-image.jpg"
                    }
                };

                response.Data = iQueryable
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(o => new Order
                    {
                        Id = o.Id,
                        PackerBarcode = o.OrderPicking.PackerBarcode,
                        MarketPlaceId = o.MarketplaceId,
                        MarketplaceOrderNumber = o.MarketplaceOrderNumber,
                        Amount = o.Total,
                        CurrencyId = o.CurrencyId,
                        OrderDate = o.OrderDate,
                        CreatedDate = o.CreatedDate,
                        OrderTypeId = o.OrderTypeId,
                        ListTotal = o.ListTotal,
                        TotalQuantity = o.OrderDetails.Sum(x => x.Quantity),
                        OrderType = new OrderType
                        {
                            Name = o.OrderType.OrderTypeTranslations.FirstOrDefault(ott => ott.LanguageId == "TR").Name,
                            OrderTypeName = o.OrderType.OrderTypeTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name
                        },
                        MutualBarcode = o.OrderShipments.Any(os => !string.IsNullOrEmpty(os.TrackingBase64)),
                        OrderShipments = o
                            .OrderShipments
                            .Select(os => new OrderShipment
                            {
                                PackageNumber = os.PackageNumber,
                                ShipmentCompanyId = os.ShipmentCompanyId,
                                TrackingCode = os.TrackingCode
                            })
                            .ToList(),
                        Customer = new Customer
                        {
                            Name = o.Customer.Name,
                            Surname = o.Customer.Surname
                        },
                        OrderDetails = o
                            .OrderDetails
                            .Select(od => new OrderDetail
                            {
                                UnitPrice = od.UnitPrice,
                                Quantity = od.Quantity,
                                Payor = od.Payor,
                                VatRate = od.VatRate,
                                ProductInformation = new ProductInformation
                                {
                                    Stock = od.ProductInformation.Stock,
                                    Barcode = od.ProductInformation.Barcode,
                                    StockCode = od.ProductInformation.StockCode,
                                    ProductInformationName = $"{od
                                        .ProductInformation
                                        .ProductInformationTranslations
                                        .FirstOrDefault(pit => pit.LanguageId == "TR")
                                        .Name} {od
                                        .ProductInformation
                                        .ProductInformationTranslations
                                        .FirstOrDefault(pit => pit.LanguageId == "TR")
                                        .VariantValuesDescription}",
                                    ProductInformationPhotos = od
                                                .ProductInformation
                                                .ProductInformationPhotos
                                                .Where(pip => !pip.Deleted).Count() > 0 ? od
                                        .ProductInformation
                                        .ProductInformationPhotos
                                        .Where(pip => !pip.Deleted)
                                        .Take(1)
                                        .Select(pip => new ProductInformationPhoto
                                        {
                                            FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                        })
                                        .ToList() : photos
                                }
                            })
                            .ToList(),


                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<Application.Terminal.DataTransferObjects.Summary> Summary()
        {
            return ExceptionHandler.ResultHandle<DataResponse<Application.Terminal.DataTransferObjects.Summary>>((response) =>
            {
                var todayOrdersCount = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Count(o => o.OrderDate.Date == DateTime.Now.Date && o.OrderTypeId != "IP");

                var unpackedOrdersCount = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Count(o => o.OrderTypeId == "OS");

                var packedOrdersCount = this
                    ._unitOfWork
                    .OrderPackingRepository
                    .DbSet()
                    .Count(op => op.CreatedDate.Date == DateTime.Now.Date);

                response.Data = new Application.Terminal.DataTransferObjects.Summary
                {
                    TodayOrdersCount = todayOrdersCount,
                    PackedOrdersCount = packedOrdersCount,
                    UnpackedOrdersCount = unpackedOrdersCount
                };
                response.Success = true;
            });
        }
        #endregion

        private DataResponse<int> Reserve()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<int>>(response =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    //TO
                    var rawSql = @"
Update Top(1)	Orders
Set				OrderTypeId = 'TO'
Output			Inserted.Id
Where			OrderTypeId = 'OS'
                And DomainId = @DomainId
				And CompanyId = @CompanyId
                And EstimatedPackingDate<GETDATE()";


                    using (SqlCommand sqlCommand = new(rawSql, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                        sqlConnection.Open();

                        var id = sqlCommand.ExecuteScalar();

                        if (id == null)
                        {
                            response.Success = false;
                            response.Message = "Sipariş bulunamadı";
                        }
                        else
                        {
                            response.Success = true;
                            response.Data = System.Convert.ToInt32(id);
                        }

                        sqlConnection.Close();
                    }
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

    }
}
