﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Terminal.DataTransferObjects;
using ECommerce.Application.Terminal.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Services
{
    public class ProductService : IProductService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        public readonly IDomainFinder _domainFinder;

        public readonly ICompanyFinder _companyFinder;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructor
        public ProductService(IUnitOfWork unitOfWork, IDomainFinder domainFinder, ICompanyFinder companyFinder, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Method
        public DataResponse<ProductInformation> ReadByBarcode(string barcode)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {
                var entity = _unitOfWork.ProductInformationRepository.DbSet()
                .Include(x => x.ProductInformationTranslations)
                .Include(x => x.ProductInformationPhotos)
                .FirstOrDefault(x => x.Barcode == barcode);

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }


                var dto = new ProductInformation
                {
                    Id = entity.Id,
                    Barcode = entity.Barcode,
                    StockCode = entity.StockCode,
                    ShelfCode = entity.ShelfCode,
                    ShelfZone = entity.ShelfZone,
                    ProductInformationPhotos = entity.ProductInformationPhotos.Count > 0 ? entity.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                    {
                        FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}",
                        DisplayOrder = pip.DisplayOrder
                    }).ToList()
                            : new List<ProductInformationPhoto>(),
                    ProductInformationName = entity.ProductInformationTranslations[0].Name,
                };

                response.Data = dto;
                response.Success = true;
            });
        }

        public PagedResponse<Product> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Product>>((response) =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Where(x => !x.IsOnlyHidden);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Value:"))
                    {
                        var value = pLoop.Replace("Value:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(iq =>
                               iq.SellerCode.Contains(value)
                            || iq.ProductInformations.Any(pi =>
                                        pi.ProductInformationTranslations.Any(pit => pit.Name.Contains(value))
                                    || pi.StockCode.Contains(value)
                                    || pi.Barcode.Contains(value)));
                        }
                    }
                    else if (pLoop.StartsWith("BrandId:"))
                    {
                        var value = pLoop.Replace("BrandId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.BrandId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("OnlyStockAvailable:"))
                    {
                        var value = pLoop.Replace("OnlyStockAvailable:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.Stock > 0));
                    }
                    else if (pLoop.StartsWith("OnlyNoPhoto:"))
                    {
                        var value = pLoop.Replace("OnlyNoPhoto:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => !pi.ProductInformationPhotos.Any()));
                    }
                    else if (pLoop.StartsWith("ProductSourceDomainId:"))
                    {
                        var value = pLoop.Replace("ProductSourceDomainId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => value == "0" ? !iq.ProductSourceDomainId.HasValue : iq.ProductSourceDomainId == Convert.ToInt32(value));
                    }
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable.Count();
                response.Data = iQueryable
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Product
                    {
                        Active = b.Active,
                        Id = b.Id,
                        SellerCode = b.SellerCode,
                        TotalStock = b.ProductInformations.Sum(bi => bi.Stock),
                        VariantsCount = b.ProductInformations.Count,
                        ProductSourceDomainName = b.ProductSourceDomainId.HasValue ? b.ProductSourceDomain.Name : "Helpy",
                        ProductInformations = b
                            .ProductInformations
                            .Select(pi => new ProductInformation
                            {
                                Active = pi.Active,
                                Stock = pi.Stock,
                                StockCode = pi.StockCode,
                                Barcode = pi.Barcode,
                                ProductInformationName = pi.ProductInformationTranslations[0].Name,
                                ProductInformationUrl = this._settingService.IncludeECommerce && !string.IsNullOrEmpty(pi.ProductInformationTranslations[0].Url) ? $"{this._settingService.WebUrl}/{pi.ProductInformationTranslations[0].Url}" : null,
                                ProductInformationPriceses = pi.ProductInformationPriceses.Where(x => x.CurrencyId == "TL").Select(pip => new ProductInformationPrice
                                {
                                    ListUnitPrice = pip.ListUnitPrice,
                                    UnitPrice = pip.UnitPrice,
                                    AccountingPrice = pip.AccountingPrice,
                                    VatRate = pip.VatRate,
                                })
                                .ToList(),
                                ProductInformationPhotos = new()
                                {
                                    new()
                                    {
                                        FileName =$"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault(pip=>!pip.Deleted).FileName}"
                                    }
                                },
                                ProductInformationMarketplaces = pi
                                    .ProductInformationMarketplaces
                                    .Where(pim => pim.CompanyId == this._companyFinder.FindId())
                                    .Select(pim => new ProductInformationMarketplace
                                    {
                                        MarketplaceId = pim.MarketplaceId,
                                        Url = pim.Url,
                                        Opened = pim.Opened,
                                        ListUnitPrice = pim.ListUnitPrice,
                                        UnitPrice = pim.UnitPrice,
                                        Active = pim.Active
                                    })
                                    .ToList()
                            })
                            .ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion


    }
}
