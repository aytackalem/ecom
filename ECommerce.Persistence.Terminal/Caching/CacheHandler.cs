﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Wrappers.Base;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;

namespace ECommerce.Persistence.Terminal.Caching
{
    public class CacheHandler : ICacheHandler
    {
        #region Fields
        private readonly IMemoryCache _memoryCache;
        #endregion

        #region Constructors
        public CacheHandler(IMemoryCache memoryCache)
        {
            #region Fields
            this._memoryCache = memoryCache;
            #endregion
        }
        #endregion

        #region Methods
        public TResponse ResponseHandle<TResponse>(Func<TResponse> func, string cacheKey, int minutes = 10) where TResponse : ResponseBase, new()
        {
            if (this._memoryCache.TryGetValue(cacheKey, out TResponse response))
            {
                return response;
            }

            response = func();

            if (response.Success)
            {
                this._memoryCache.Set(cacheKey, response, new TimeSpan(0, minutes, 0));
            }

            return response;
        }

        public TEntity Handle<TEntity>(Func<TEntity> func, string cacheKey, int minutes = 10) where TEntity : class
        {
            if (this._memoryCache.TryGetValue(cacheKey, out TEntity response))
            {
                return response;
            }

            response = func();

            if (response != null)
            {
                this._memoryCache.Set(cacheKey, response, new TimeSpan(0, minutes, 0));
            }

            return response;
        }

        public List<TEntity> Handle<TEntity>(Func<List<TEntity>> func, string cacheKey, int minutes = 10) where TEntity : class
        {
            if (this._memoryCache.TryGetValue(cacheKey, out List<TEntity> response))
            {
                return response;
            }

            response = func();

            if (response != null && response.Count > 0)
            {
                this._memoryCache.Set(cacheKey, response, new TimeSpan(0, minutes, 0));
            }

            return response;
        }
        #endregion
    }
}
