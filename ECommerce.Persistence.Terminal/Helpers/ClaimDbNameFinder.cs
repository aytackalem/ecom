﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Helpers
{
    public class ClaimDbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName = string.Empty;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ClaimDbNameFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public void Set(string dbName)
        {

        }

        public string FindName()
        {

            var code = this._httpContextAccessor.HttpContext.Request.Headers["Code"].ToString();

            if (string.IsNullOrEmpty(code))
            {
                this._dbName = "Lafaba_ECommerce";
                return this._dbName;

            }

            code = code.ToLower();
            this._dbName = code;

            if (code == "lafaba")
                this._dbName = "Lafaba_ECommerce";
            else if (code == "saran"
                || code == "simge"
                || code == "caplin"
                || code == "toem"
                || code == "gozlukgozuluk"
                || code == "yesimkozmetik"
                || code == "bebbeo")
                this._dbName = "Marketplace";



            return this._dbName;
        }
        #endregion
    }
}
