﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Helpers
{
    public class ClaimDomainFinder : IDomainFinder
    {
        #region Field
        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ClaimDomainFinder(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;

        }
        #endregion

        #region Methods
        public int FindId()
        {
            var domain = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "DomainId");
            return Convert.ToInt32(domain != null ? domain.Value : 1);
        }

        public void Set(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
