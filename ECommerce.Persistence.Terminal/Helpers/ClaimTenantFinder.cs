﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Helpers
{
    public class ClaimTenantFinder : ITenantFinder
    {

        #region Field
        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ClaimTenantFinder(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region Methods
        public int FindId()
        {
            var tenant = this._httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TenantId");

            return Convert.ToInt32(tenant != null ? tenant.Value : 1);

        }
        #endregion
    }
}
