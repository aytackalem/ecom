﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Persistence.Terminal.Helpers
{
    public class ClaimCompanyFinder : ICompanyFinder
    {
        #region Field
        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ClaimCompanyFinder(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region Methods
        public int FindId()
        {

            var company = this._httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CompanyId");

            return Convert.ToInt32(company != null ? company.Value : 1);
        }

        public void Set(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
