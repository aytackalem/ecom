﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Terminal.Caching;
using ECommerce.Persistence.Terminal.Helpers;
using ECommerce.Persistence.Terminal.Services;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Persistence.Terminal
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            #region DbContext Dependencies
            //serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            //{
            //    options.UseSqlServer(configuration.GetConnectionString("Ecommerce"));
            //});
            serviceCollection.AddDbContext<ApplicationDbContext>();
            #endregion

            serviceCollection.AddTransient<IImageHelper, ImageHelper>();
            serviceCollection.AddTransient<IRandomHelper, RandomHelper>();
            serviceCollection.AddTransient<ITenantFinder, ClaimTenantFinder>();
            serviceCollection.AddTransient<IDomainFinder, ClaimDomainFinder>();
            serviceCollection.AddTransient<ICompanyFinder, ClaimCompanyFinder>();
            serviceCollection.AddTransient<IDbNameFinder, ClaimDbNameFinder>();
            serviceCollection.AddTransient<ICaptchaService, CaptchaService>();

            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddTransient<ICookieHelper, CookieHelper>();
            serviceCollection.AddTransient<IOrderService, OrderService>();
            serviceCollection.AddTransient<ISettingService, SettingService>();
            serviceCollection.AddTransient<IProductService, ProductService>();
            serviceCollection.AddTransient<IWidgetService, WidgetService>();

            serviceCollection.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();
           
        }
        #endregion
    }
}
