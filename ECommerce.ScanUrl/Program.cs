﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ECommerce.ScanUrl
{
    class RedirectLink
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public string Target { get; set; }
    }

    class ProductInformationContentTranslation
    {
        public int Id { get; set; }

        public string ExpertOpinion { get; set; }

        public string Content { get; set; }

        public string Description { get; set; }
    }

    class InformationTranslationContent
    {
        public int Id { get; set; }

        public string Html { get; set; }
    }

    class ContentTranslationContent
    {
        public int Id { get; set; }

        public string Html { get; set; }
    }

    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static void Main(string[] args)
        {
            ProductInformationContentTranslations();
            InformationTranslationContents();
            ContentTranslationContents();





            //var text = File.ReadAllText(@"C:\Users\Burak\Downloads\google-index-list-fidanistanbul.csv");
            //var links = text.Split("\r\n");

            //var redirectLinks = new List<RedirectLink>();

            //using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            //{
            //    using (SqlCommand sqlCommand = new SqlCommand("Select * From GoogleLinks Where	Target Like 'Search'", sqlConnection))
            //    {
            //        sqlConnection.Open();

            //        var sqlReader = sqlCommand.ExecuteReader();
            //        while (sqlReader.Read())
            //        {
            //            redirectLinks.Add(new RedirectLink
            //            {
            //                Id = sqlReader.GetInt32(0),
            //                Target = sqlReader.GetString(1),
            //                Url = sqlReader.GetString(2)
            //            });
            //        }
            //    }
            //}

            //foreach (var rlLoop in redirectLinks)
            //{
            //    using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            //    {
            //        using (SqlCommand sqlCommand = new SqlCommand("Update GoogleLinks Set [Target] = [Url], [Type] = 'Search', [Url] = @Url Where Id = @Id", sqlConnection))
            //        {
            //            sqlCommand.Parameters.AddWithValue("@Id", rlLoop.Id);
            //            sqlCommand.Parameters.AddWithValue("@Url", links[rlLoop.Id - 1]);

            //            sqlConnection.Open();

            //            var ar = sqlCommand.ExecuteNonQuery();

            //        }
            //    }
            //}

            //DataTable dataTable = new DataTable();
            //dataTable.Columns.Add(new DataColumn("Url", typeof(string)));

            //foreach (var lLoop in links)
            //{
            //    DataRow dataRow = dataTable.NewRow();
            //    dataRow["Url"] = lLoop;

            //    dataTable.Rows.Add(dataRow);
            //}

            //using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            //{
            //    SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
            //    sqlBulkCopy.DestinationTableName = "GoogleLinks";

            //    sqlBulkCopy.ColumnMappings.Add("Url", "Url");

            //    try
            //    {
            //        sqlConnection.Open();

            //        sqlBulkCopy.WriteToServer(dataTable);
            //    }
            //    catch (Exception e)
            //    {

            //        throw;
            //    }
            //    finally
            //    {
            //        sqlConnection.Close();
            //    }
            //}

        }

        private static void ProductInformationContentTranslations()
        {
            var list = new List<ProductInformationContentTranslation>();

            //TO DO: Read Product Information Content
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(@"
Select	Id, ExpertOpinion, Content, [Description]
From	ProductInformationContentTranslations 
Where	ExpertOpinion Like '%.fidanistanbul.com%' 
		Or Content Like '%.fidanistanbul.com%' 
		Or [Description] Like '%.fidanistanbul.com%'", sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();

                        var sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                        {
                            list.Add(new ProductInformationContentTranslation
                            {
                                Id = sqlReader.GetInt32(0),
                                ExpertOpinion = sqlReader.GetString(1),
                                Content = sqlReader.GetString(2),
                                Description = sqlReader.GetString(3)
                            });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
            }

            foreach (var lLoop in list)
            {
                lLoop.Content = lLoop.Content.Replace("\\\"", "\"");

                int si = lLoop.Content.IndexOf("href=\"", 0);
                while (si != -1)
                {
                    int ei = lLoop.Content.IndexOf("\"", (si + 6));
                    var url = lLoop.Content.Substring((si + 6), ei - (si + 6));

                    if (url.StartsWith("http"))
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand("Insert Into RedirectLinks Values (@Url, Null)", sqlConnection))
                            {
                                sqlCommand.Parameters.AddWithValue("@Url", url);

                                try
                                {
                                    sqlConnection.Open();

                                    var ar = sqlCommand.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }

                    si = lLoop.Content.IndexOf("href=\"", ei);
                }
            }

            foreach (var lLoop in list)
            {
                lLoop.Description = lLoop.Description.Replace("\\\"", "\"");

                int si = lLoop.Description.IndexOf("href=\"", 0);
                while (si != -1)
                {
                    int ei = lLoop.Description.IndexOf("\"", (si + 6));
                    var url = lLoop.Description.Substring((si + 6), ei - (si + 6));

                    if (url.StartsWith("http"))
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand("Insert Into RedirectLinks Values (@Url, Null)", sqlConnection))
                            {
                                sqlCommand.Parameters.AddWithValue("@Url", url);

                                try
                                {
                                    sqlConnection.Open();

                                    var ar = sqlCommand.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }

                    si = lLoop.Description.IndexOf("href=\"", ei);
                }
            }

            foreach (var lLoop in list)
            {
                lLoop.ExpertOpinion = lLoop.ExpertOpinion.Replace("\\\"", "\"");

                int si = lLoop.ExpertOpinion.IndexOf("href=\"", 0);
                while (si != -1)
                {
                    int ei = lLoop.ExpertOpinion.IndexOf("\"", (si + 6));
                    var url = lLoop.ExpertOpinion.Substring((si + 6), ei - (si + 6));

                    if (url.StartsWith("http"))
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand("Insert Into RedirectLinks Values (@Url, Null)", sqlConnection))
                            {
                                sqlCommand.Parameters.AddWithValue("@Url", url);

                                try
                                {
                                    sqlConnection.Open();

                                    var ar = sqlCommand.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }

                    si = lLoop.ExpertOpinion.IndexOf("href=\"", ei);
                }
            }
        }

        private static void InformationTranslationContents()
        {
            var list = new List<InformationTranslationContent>();

            //TO DO: Read Information Translation Content
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(@"
Select	Id, Html
From	InformationTranslationContents", sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();

                        var sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                        {
                            list.Add(new InformationTranslationContent
                            {
                                Id = sqlReader.GetInt32(0),
                                Html = sqlReader.GetString(1)
                            });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
            }

            foreach (var lLoop in list)
            {
                lLoop.Html = lLoop.Html.Replace("\\\"", "\"");

                int si = lLoop.Html.IndexOf("href=\"", 0);
                while (si != -1)
                {
                    int ei = lLoop.Html.IndexOf("\"", (si + 6));
                    var url = lLoop.Html.Substring((si + 6), ei - (si + 6));

                    if (url.StartsWith("http"))
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand("Insert Into RedirectLinks Values (@Url, Null)", sqlConnection))
                            {
                                sqlCommand.Parameters.AddWithValue("@Url", url);

                                try
                                {
                                    sqlConnection.Open();

                                    var ar = sqlCommand.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }

                    si = lLoop.Html.IndexOf("href=\"", ei);
                }
            }
        }

        private static void ContentTranslationContents()
        {
            var list = new List<ContentTranslationContent>();

            //TO DO: Read Content Translation Content
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(@"
Select	Id, Html
From	ContentTranslationContents", sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();

                        var sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                        {
                            list.Add(new ContentTranslationContent
                            {
                                Id = sqlReader.GetInt32(0),
                                Html = sqlReader.GetString(1)
                            });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
            }

            foreach (var lLoop in list)
            {
                lLoop.Html = lLoop.Html.Replace("\\\"", "\"");

                int si = lLoop.Html.IndexOf("href=\"", 0);
                while (si != -1)
                {
                    int ei = lLoop.Html.IndexOf("\"", (si + 6));
                    var url = lLoop.Html.Substring((si + 6), ei - (si + 6));

                    if (url.StartsWith("http"))
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                        {
                            using (SqlCommand sqlCommand = new SqlCommand("Insert Into RedirectLinks Values (@Url, Null)", sqlConnection))
                            {
                                sqlCommand.Parameters.AddWithValue("@Url", url);

                                try
                                {
                                    sqlConnection.Open();

                                    var ar = sqlCommand.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                }
                                finally
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                    }

                    si = lLoop.Html.IndexOf("href=\"", ei);
                }
            }
        }
    }
}
