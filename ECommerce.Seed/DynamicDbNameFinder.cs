﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Seed
{
    public class DynamicDbNameFinder : IDbNameFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Constructors
        public DynamicDbNameFinder()
        {
        }
        #endregion

        #region Methods
       

        public string FindName()
        {
            return "";
        }

        public void Set(string dbName)
        {

        }
        #endregion
    }
}
