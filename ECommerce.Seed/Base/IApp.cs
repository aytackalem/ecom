﻿namespace ECommerce.Seed.Base
{
    public interface IApp
    {
        void Run();
    }
}
