﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Seed
{
    public class DynamicDomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public DynamicDomainFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return 1;
        }

        public void Set(int id)
        {
        }
        #endregion
    }
}
