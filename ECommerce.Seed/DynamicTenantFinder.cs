﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Seed
{
    public class DynamicTenantFinder : ITenantFinder
    {
        #region Fields
        public int _tenantId;
        #endregion

        #region Constructors
        public DynamicTenantFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return  1;
        }
        #endregion
    }
}
