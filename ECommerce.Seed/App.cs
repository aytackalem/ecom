﻿
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using ECommerce.Domain.Entities.Companyable;
using ECommerce.Seed.Base;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Graph;
using System.Data;
using System.Globalization;
using System.Net;

namespace ECommerce.Seed
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IMarketplaceService _marketplaceService;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IConfiguration configuration, IDbNameFinder dbNameFinder, IMarketplaceService marketplaceService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            this._dbNameFinder = dbNameFinder;
            this._marketplaceService = marketplaceService;
            #endregion
        }
        #endregion

        private void RunBrandUrl()
        {
            var _brands = this._unitOfWork.BrandRepository.DbSet().ToList();
            foreach (var bLoop in _brands)
            {

                bLoop.Url = $"{bLoop.Name.GenerateUrl()}-{bLoop.Id}-b";


            }
            this._unitOfWork.BrandRepository.Update(_brands);

        }

        private void RunCategoryProduct()
        {
            var products = _unitOfWork.ProductRepository.DbSet().ToList();
            var categories = _unitOfWork.CategoryRepository.DbSet().ToList();

            var categoryProducts = new List<CategoryProduct>();

            foreach (var product in products)
            {
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, product.CategoryId, product.Id));
            }
            _unitOfWork.CategoryProductRepository.BulkInsert(categoryProducts);
        }

        private List<CategoryProduct> RecursiveCategoryProduct(List<Category> categories, int categoryId, int productId)
        {
            var categoryProducts = new List<CategoryProduct>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);

            categoryProducts.Add(new CategoryProduct
            {

                ProductId = productId,
                CategoryId = category.Id,
                CreatedDate = DateTime.Now
            });

            if (category.ParentCategoryId.HasValue)
            {
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, productId));
            }

            return categoryProducts;
        }

        private void InsertProductTranslations(List<Application.Common.Parameters.Product.ProductItem> _products)
        {
            var productInformations = _unitOfWork.ProductInformationRepository.DbSet().Include(x => x.Product).Include(x => x.ProductInformationTranslations).ToList();
            var productInformationContentTranslations = new List<ProductInformationContentTranslation>();

            foreach (var pllop in _products)
            {
                foreach (var item in pllop.StockItems)
                {
                    var productInformation = productInformations.FirstOrDefault(x => x.Barcode == item.Barcode && x.StockCode == item.StockCode && x.Product.SellerCode == pllop.SellerCode);

                    if (productInformation != null)
                    {
                        productInformationContentTranslations.Add(new ProductInformationContentTranslation
                        {
                            Id = productInformation.ProductInformationTranslations[0].Id,
                            TenantId = productInformation.TenantId,
                            DomainId = productInformation.DomainId,
                            Content = item.Description,
                            Description = "",
                            CreatedDate = DateTime.Now,
                        });
                    }

                }
            }

            for (int i = 0; i < ((productInformationContentTranslations.Count / 1000) + 1); i++)
            {
                var aa = productInformationContentTranslations.Skip(i * 1000).Take(1000).ToList();

                _unitOfWork.ProductInformationContentTranslationRepository.BulkInsert(aa);
            }






        }

        private void InsertProducts(List<Application.Common.Parameters.Product.ProductItem> _products)
        {

            List<Product> products = new();

            List<Variant> variants = new();

            List<Brand> brands = new();

            List<Category> categories = new();

            var supplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id;

            foreach (var pllop in _products)
            {
                if (!brands.Any(b => b.Name == pllop.BrandName))
                {
                    brands.Add(new Brand
                    {
                        Active = true,
                        CreatedDate = DateTime.Now,
                        DomainId = _domainFinder.FindId(),

                        TenantId = _tenantFinder.FindId(),
                        Url = String.Empty,
                        Name = pllop.BrandName
                    });
                }
                var sender = pllop.StockItems[0].Attributes.FirstOrDefault(x => x.AttributeName == "Cinsiyet");

                pllop.CategoryName = sender != null && sender.AttributeValueName != null ? $"{sender.AttributeValueName.Replace("Kadın / Kız", "Kadın")} {pllop.CategoryName}"
                    : pllop.CategoryName;




                var categoryCode = pllop.CategoryName.ReplaceChar().Replace(" ", "");

                if (!categories.Any(c => c.Code == categoryCode))
                {
                    categories.Add(new Category
                    {
                        Active = true,
                        CreatedDate = DateTime.Now,
                        FileName = String.Empty,
                        DomainId = _domainFinder.FindId(),

                        SortNumber = 1,
                        TenantId = _tenantFinder.FindId(),
                        Code = categoryCode,
                        CategoryTranslations = new()
                                {
                                    new CategoryTranslation
                                    {
                                        CreatedDate=DateTime.Now,
                                        DomainId=_domainFinder.FindId(),
                                        IconFileName=String.Empty,
                                        LanguageId="TR",

                                        TenantId=_tenantFinder.FindId(),
                                        Url=String.Empty,
                                        Name = pllop.CategoryName,
                                        CategoryContentTranslation=new CategoryContentTranslation
                                        {

                                            TenantId=_tenantFinder.FindId(),
                                            DomainId=_domainFinder.FindId(),
                                            CreatedDate=DateTime.Now,
                                            Content=pllop.CategoryName
                                        },
                                        CategorySeoTranslation=new CategorySeoTranslation
                                        {

                                            TenantId=_tenantFinder.FindId(),
                                            DomainId=_domainFinder.FindId(),
                                            CreatedDate=DateTime.Now,
                                            MetaDescription=pllop.CategoryName,
                                            MetaKeywords=pllop.CategoryName,
                                            Title=pllop.CategoryName
                                        },
                                        CategoryTranslationBreadcrumb=new CategoryTranslationBreadcrumb()
                                    }
                                }
                    });
                }

                foreach (var siLoop in pllop.StockItems)
                {
                    var elementVariants = siLoop.Attributes
                               .Select(e => new Variant
                               {
                                   CreatedDate = DateTime.Now,
                                   DomainId = _domainFinder.FindId(),

                                   Photoable = false,
                                   TenantId = _tenantFinder.FindId(),
                                   VariantTranslations = new()
                                   {
                                                new VariantTranslation
                                                {
                                                    CreatedDate=DateTime.Now,
                                                    DomainId=_domainFinder.FindId(),
                                                    LanguageId="TR",

                                                    TenantId=_tenantFinder.FindId(),
                                                    Name = e.AttributeName
                                                }
                                   },
                                   VariantValues = new()
                                   {
                                                new VariantValue
                                                {
                                                    CreatedDate= DateTime.Now,
                                                    DomainId=_domainFinder.FindId(),

                                                    TenantId=_tenantFinder.FindId(),
                                                    VariantValueTranslations = new()
                                                    {
                                                        new VariantValueTranslation
                                                        {
                                                            CreatedDate=DateTime.Now,
                                                            DomainId=_domainFinder.FindId(),
                                                            LanguageId="TR",

                                                            TenantId=_tenantFinder.FindId(),
                                                            Value = e.AttributeValueName !=null ?  CultureInfo.CurrentCulture.TextInfo.ToTitleCase(e.AttributeValueName.ReplaceChar()) : null
                                                        }
                                                    }
                                                }
                                   }
                               })
                               .GroupBy(x => new { x.VariantTranslations.FirstOrDefault().Name, x.VariantValues.FirstOrDefault().VariantValueTranslations.FirstOrDefault().Value }).Select(x => x.First())
                               .ToList();


                    if (elementVariants != null)
                    {

                        foreach (var theElementVariant in elementVariants)
                        {
                            var variantName = theElementVariant.VariantTranslations.First().Name;
                            var variant = variants.FirstOrDefault(v => v.VariantTranslations.First().Name == variantName);
                            if (variant == null)
                                variants.Add(theElementVariant);
                            else
                                theElementVariant.VariantValues.ForEach(theVariantValue =>
                                {
                                    var variantValueValue = theVariantValue.VariantValueTranslations.First().Value;
                                    if (!variant.VariantValues.Any(vv => vv.VariantValueTranslations.First().Value == variantValueValue))
                                        variant.VariantValues.Add(theVariantValue);
                                });
                        }

                    }
                }
                Product product = new()
                {
                    DomainId = _domainFinder.FindId(),
                    TenantId = _tenantFinder.FindId(),

                    CreatedDate = DateTime.Now,
                    SupplierId = supplierId,
                    AccountingProperty = false,
                    CategoryProducts = new List<CategoryProduct>(),
                    ProductTranslations = new()
                            {
                                new ProductTranslation
                                {
                                    Name=pllop.Name,

                                    LanguageId="TR",
                                    TenantId=_tenantFinder.FindId(),
                                    DomainId=_domainFinder.FindId(),
                                    CreatedDate=DateTime.Now
                                }
                            },
                    Active = true,
                    ProductMarketplaces = new List<ProductMarketplace>(),
                    Brand = new()
                    {
                        Name = pllop.BrandName,
                        Active = true
                    },
                    Category = new Category
                    {
                        Code = categoryCode,
                        CategoryTranslations = new()
                                {
                                    new CategoryTranslation
                                    {
                                        Name = pllop.CategoryName,

                                    }
                                }
                    },
                    SellerCode = pllop.SellerCode,
                    ProductInformations = pllop.StockItems.Select(x => new ProductInformation
                    {

                        Active = true,
                        Barcode = x.Barcode,
                        StockCode = x.StockCode,
                        Stock = x.Quantity,
                        TenantId = _tenantFinder.FindId(),
                        DomainId = _domainFinder.FindId(),

                        Photoable = true,
                        IsSale = true,
                        Type = "PI",
                        Payor = false,
                        SkuCode = x.StockCode,
                        CreatedDate = DateTime.Now,
                        GroupId = Guid.NewGuid().ToString(),
                        ProductInformationTranslations = new()
                                    {
                                        new ProductInformationTranslation
                                        {
                                           CreatedDate = DateTime.Now,
                                            SubTitle=String.Empty,
                                            TenantId=_tenantFinder.FindId(),
                                            DomainId=_domainFinder.FindId(),

                                            LanguageId="TR",
                                            Url=String.Empty,
                                            Name = x.Title,
                                            ProductInformationSeoTranslation=new ProductInformationSeoTranslation
                                            {
                                            TenantId=_tenantFinder.FindId(),
                                            DomainId=_domainFinder.FindId(),

                                            CreatedDate=DateTime.Now,
                                            MetaDescription="",
                                            MetaKeywords="",
                                            Title="",
                                            },
                                            ProductInformationContentTranslation=new ProductInformationContentTranslation
                                            {
                                                TenantId=_tenantFinder.FindId(),
                                                DomainId=_domainFinder.FindId(),

                                                CreatedDate=DateTime.Now,
                                                Content=x.Description,
                                                Description="",
                                                ExpertOpinion=String.Empty
                                            },
                                            ProductInformationTranslationBreadcrumb=new ProductInformationTranslationBreadcrumb(),
                                        }
                                    },
                        ProductInformationPriceses = new()
                                    {
                                        new ProductInformationPrice
                                        {
                                            CreatedDate = DateTime.Now,
                                            TenantId=_tenantFinder.FindId(),
                                            DomainId=_domainFinder.FindId(),

                                            AccountingPrice=true,
                                            CurrencyId="TL",
                                            ListUnitPrice = x.ListPrice,
                                            UnitPrice = x.SalePrice,
                                            VatRate = x.VatRate / 100
                                        }
                                    },
                        ProductInformationPhotos = x.Images
                                        .Select(e => new ProductInformationPhoto
                                        {
                                            CreatedDate = DateTime.Now,
                                            TenantId = _tenantFinder.FindId(),
                                            DomainId = _domainFinder.FindId(),

                                            FileName = e.Url
                                        })
                                        .ToList(),
                        ProductInformationVariants = x.Attributes.Where(x => !string.IsNullOrEmpty(x.AttributeValueName) && !string.IsNullOrEmpty(x.AttributeName))
                                        .Select(e => new ProductInformationVariant
                                        {


                                            CreatedDate = DateTime.Now,
                                            DomainId = _domainFinder.FindId(),
                                            TenantId = _tenantFinder.FindId(),
                                            VariantValue = new Domain.Entities.VariantValue
                                            {

                                                CreatedDate = DateTime.Now,
                                                DomainId = _domainFinder.FindId(),
                                                TenantId = _tenantFinder.FindId(),
                                                VariantValueTranslations = new List<VariantValueTranslation>
                                                {
                                                    new VariantValueTranslation
                                                    {
                                                        Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(e.AttributeValueName.ReplaceChar()),

                                                        CreatedDate=DateTime.Now,
                                                        DomainId=_domainFinder.FindId(),
                                                        LanguageId="TR",
                                                        TenantId=_tenantFinder.FindId()
                                                    }
                                                },
                                                Variant = new Variant
                                                {
                                                    VariantTranslations = new()
                                                    {
                                                        new VariantTranslation
                                                        {
                                                            Name = e.AttributeName,

                                                             CreatedDate=DateTime.Now,
                                                             DomainId=_domainFinder.FindId(),
                                                             LanguageId="TR",
                                                             TenantId=_tenantFinder.FindId()
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                        .GroupBy(x => new
                                        {
                                            x.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name,
                                            x.VariantValue.VariantValueTranslations.FirstOrDefault().Value
                                        }).Select(x => x.First())
                                        .ToList()

                    }).ToList(),

                };

                products.Add(product);

            }

            #region Brands
            var brandResponse = this.BulkRead(brands);

            var addBrands = brands
                .Where(b => !brandResponse.Data.Where(d => d.Id != 0).Select(d => d.Name).Contains(b.Name))
                .ToList();
            if (addBrands.Count > 0)
                this._unitOfWork.BrandRepository.BulkInsertMapping(addBrands);

            brandResponse = this.BulkRead(brands);
            #endregion

            #region Categories
            var categoryResponse = this.BulkRead(categories);

            var addCategories = categories
                .Where(c => !categoryResponse.Data.Where(d => d.Id != 0).Select(d => d.Code).Contains(c.Code))
                .ToList();
            if (addCategories.Count > 0)
            {
                this._unitOfWork.CategoryRepository.BulkInsertMapping(addCategories);
            }

            categoryResponse = this.BulkRead(categories);
            #endregion

            #region Variants
            variants.RemoveAll(v => v.VariantTranslations.First().Name == "Marka");
            var variantResponse = this.BulkRead(variants);

            var addVariants = variants
                .Where(v => !variantResponse.Data.Where(d => d.Id != 0).Select(x => x.VariantTranslations.First().Name).Contains(v.VariantTranslations.First().Name))
                .ToList();
            if (addVariants.Count > 0)
                this._unitOfWork.VariantRepository.BulkInsertMapping(addVariants);

            variantResponse = this.BulkRead(variants);

            variants.ForEach(theVariant =>
            {
                var variant = variantResponse
                    .Data
                    .First(d => d.VariantTranslations.First().Name == theVariant.VariantTranslations.First().Name);

                theVariant.VariantValues.ForEach(theVariantValue => theVariantValue.VariantId = variant.Id);
            });
            #endregion


            #region Variant Values
            var variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());

            var addVariantValues = new List<VariantValue>();
            variants.SelectMany(v => v.VariantValues).ToList().ForEach(theVariantValue =>
            {
                var variantValue = variantValueResponse
                    .Data
                    .FirstOrDefault(d => d.VariantId == theVariantValue.VariantId && d.VariantValueTranslations.First().Value == theVariantValue.VariantValueTranslations.First().Value);

                if (theVariantValue.VariantValueTranslations.First().Value != null)
                {
                    if (variantValue == null)
                    {
                        addVariantValues.Add(theVariantValue);
                    }
                    else if (variantValue.Id == 0)
                        addVariantValues.Add(variantValue);
                }
            });
            if (addVariantValues.Count > 0)
            {
                var groupByAddedVarinatValues = addVariantValues.GroupBy(x => new { x.VariantId, x.VariantValueTranslations[0].Value }).Select(x => x.First()).ToList();

                this._unitOfWork.VariantValueRepository.BulkInsertMapping(groupByAddedVarinatValues);
            }

            variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());
            #endregion

            #region Products (Create)
            var productResponse = this.BulkRead(products);
            products.ForEach(theProduct =>
            {
                theProduct.Brand = brandResponse.Data.First(d => d.Name == theProduct.Brand.Name);
                theProduct.BrandId = theProduct.Brand.Id;

                theProduct.Category = categoryResponse.Data.First(d => d.Code == theProduct.Category.Code);
                theProduct.CategoryId = theProduct.Category.Id;

                theProduct.CategoryProducts.Add(new CategoryProduct
                {
                    CategoryId = theProduct.Category.Id,
                    CreatedDate = DateTime.Now,
                    DomainId = _domainFinder.FindId(),
                    TenantId = _tenantFinder.FindId(),

                });

                foreach (var theProductInformation in theProduct.ProductInformations)
                {


                    if (theProductInformation.ProductInformationVariants != null)
                    {

                        theProductInformation.ProductInformationVariants.GroupBy(x => new { x.VariantValue.VariantValueTranslations.First().Value, x.VariantValue.Variant.VariantTranslations.First().Name }).Select(x => x.First()).ToList().ForEach(theProductInformationVariant =>
                        {
                            try
                            {

                                var variant = variantResponse.Data.First(d => d.VariantTranslations.First().Name == theProductInformationVariant.VariantValue.Variant.VariantTranslations.First().Name);

                                var variantValue = variantValueResponse.Data.First(d => d.VariantId == variant.Id && d.VariantValueTranslations.First().Value == theProductInformationVariant.VariantValue.VariantValueTranslations.First().Value);

                                theProductInformationVariant.VariantValueId = variantValue.Id;
                                theProductInformationVariant.VariantValue.VariantId = variant.Id;
                            }
                            catch (Exception ex)
                            {
                            }
                        });
                    }


                    var productEntity = productResponse
                   .Data
                   .FirstOrDefault(d => d.Id != 0 &&
                       d.ProductInformations.Any(pi => pi.Barcode == theProductInformation.Barcode));

                    if (productEntity != null)
                    {
                        theProduct.Id = productEntity.Id;
                        theProduct.ProductInformations.ForEach(x =>
                        {
                            x.Id = productEntity.ProductInformations[0].Id;
                            x.ProductId = productEntity.Id;
                        });

                    }
                }

            });

            if (products.Count(p => p.Id == 0) > 0)
            {
                var insertingProducts = products.Where(p => p.Id == 0).Reverse().ToList();

                this._unitOfWork.ProductRepository.BulkInsertMapping(insertingProducts);

            }
            #endregion



        }

        private void InsertMarketplaces(List<Application.Common.Parameters.Product.ProductItem> _products)
        {
            //var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().FirstOrDefault(x => x.MarketplaceId == "TY");
            //var products = _unitOfWork.ProductRepository.DbSet().Include(x => x.ProductInformations).ToList();

            //var productInformations = products.SelectMany(x => x.ProductInformations);

            //var catagories = _products.GroupBy(x => x.CategoryId).Select(x => $"TY-{x.First().CategoryId}");
            //var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
            //     .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
            //     .Include(x => x.MarketplaceVariantValue)
            //     .Where(x =>
            // x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == "TY"
            // && catagories.Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

            //var productMarketplaces = new List<ProductMarketplace>();
            //var productInformationMarketplaces = new List<ProductInformationMarketplace>();
            //foreach (var productItem in _products)
            //{



            //    foreach (var sLoop in productItem.StockItems)
            //    {
            //        var productInformation = productInformations.FirstOrDefault(x => x.Barcode == sLoop.Barcode);

            //        if (productInformation != null)
            //        {


            //            var productInformationMarketplace = new ProductInformationMarketplace
            //            {
            //                CreatedDate = DateTime.Now,
            //                DomainId = _domainFinder.FindId(),
            //                TenantId = _tenantFinder.FindId(),
            //                CompanyId = _companyFinder.FindId(),
            //                Active = true,
            //                MarketplaceCompanyId = marketplaceCompany.Id,
            //                UnitPrice = sLoop.SalePrice,
            //                Barcode = sLoop.Barcode,
            //                ListUnitPrice = sLoop.ListPrice,
            //                
            //                ProductInformationId = productInformation.Id,
            //                StockCode = sLoop.StockCode,
            //                ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()
            //            };

            //            foreach (var attLoop in sLoop.Attributes)
            //            {
            //                if (attLoop.AttributeCode != "47")
            //                {

            //                    var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValueId.Replace("TY-", "") == attLoop.AttributeValueCode));
            //                    if (mVariantValue != null)
            //                        productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
            //                        {
            //                            DomainId = _domainFinder.FindId(),
            //                            TenantId = _tenantFinder.FindId(),
            //                            CompanyId = _companyFinder.FindId(),
            //                            CreatedDate = DateTime.Now,
            //                            
            //                            MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
            //                        });

            //                }
            //                else
            //                {
            //                    var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValue.Value.ReplaceChar() == attLoop.AttributeValueName.ReplaceChar()));
            //                    if (mVariantValue != null)
            //                        productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
            //                        {
            //                            DomainId = _domainFinder.FindId(),
            //                            TenantId = _tenantFinder.FindId(),
            //                            CompanyId = _companyFinder.FindId(),
            //                            CreatedDate = DateTime.Now,
            //                            
            //                            MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
            //                        });

            //                }
            //            }
            //            productInformationMarketplaces.Add(productInformationMarketplace);

            //            productMarketplaces.Add(new ProductMarketplace
            //            {
            //                SellerCode = productItem.SellerCode,
            //                Active = true,
            //                CompanyId = _companyFinder.FindId(),
            //                CreatedDate = DateTime.Now,
            //                DomainId = _domainFinder.FindId(),
            //                TenantId = _tenantFinder.FindId(),
            //                
            //                MarketplaceBrandId = productItem.BrandId,
            //                MarketplaceCategoryId = $"TY-" + productItem.CategoryId,
            //                MarketplaceBrandName = productItem.BrandName,
            //                ProductId = productInformation.ProductId,
            //                MarketplaceCompanyId = marketplaceCompany.Id
            //            });

            //        }


            //    }

            //}
            //_unitOfWork.ProductMarketplaceRepository.BulkInsert(productMarketplaces);
            //_unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(productInformationMarketplaces);
        }

        private void HepsiburadaMercantSkuUpdate()
        {
            //var _products = this._marketplaceService.GetProducts("HB").Data;

            //var _productInformationMarketplaces = new List<ProductInformationMarketplace>();
            //var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().FirstOrDefault(x => x.MarketplaceId == "HB");
            //var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Where(x => x.MarketplaceCompanyId == marketplaceCompany.Id);
            //var stockItems = _products.SelectMany(x => x.StockItems);
            //foreach (var si in stockItems)
            //{
            //    var productInformationMarket = productInformationMarketplaces.FirstOrDefault(x => x.StockCode == si.StockCode || x.StockCode == si.StockCode.Replace("ENT-HB", "") || x.Barcode == si.StockCode.Replace("ENT-HB", ""));
            //    if (productInformationMarket != null)
            //    {
            //        productInformationMarket.StockCode = si.StockCode;
            //        _unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarket);

            //    }

            //}


        }

        private void ProductMarketplace(Tenant tLoop, Domain.Entities.Domain dLoop)
        {
            //            var companies = this
            //         ._unitOfWork
            //         .CompanyRepository
            //         .DbSet()
            //         .Include(c => c.CompanySetting)
            //         .ToList();

            //            foreach (var theCompany in companies)
            //            {
            //                ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;

            //                var _products = _unitOfWork.ProductRepository.DbSet()
            //                            .Include(x => x.ProductInformations)
            //                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
            //                             .Include(x => x.ProductInformations)
            //                             .ThenInclude(x => x.ProductInformationVariants)
            //                             .ThenInclude(x => x.VariantValue.Variant)
            //                             .Include(x => x.ProductInformations)
            //                             .ToList();

            //                var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet().Where(x => x.Active).ToList();
            //                var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet().ToList();
            //                var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformationMarketplaceVariantValues).ThenInclude(x => x.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant).ToList();

            //                var _productMarketplaces = new List<Domain.Entities.ProductMarketplace>();
            //                var _productInformationMarketplaces = new List<Domain.Entities.ProductInformationMarketplace>();
            //                var _productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();
            //                var updateProductInformationMarketplaces = new List<ProductInformationMarketplace>();

            //                foreach (var mcLoop in marketplaceCompanies)
            //                {

            //                    var marketplaceCategoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().Include(x => x.MarketplaceCategory).Where(x => x.MarketplaceCategory.MarketplaceId == mcLoop.MarketplaceId).ToList();
            //                    var marketplaceBrandMappings = _unitOfWork.MarketplaceBrandMappingRepository.DbSet().Where(x => x.MarketplaceId == mcLoop.MarketplaceId).ToList();

            //                    var marketplaceVariantMappings = _unitOfWork.MarketplaceVariantMappingRepository.DbSet().Where(x => x.MarketplaceVariant.MarketplaceId == mcLoop.MarketplaceId).ToList();
            //                    var marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().Include(x => x.MarketplaceVariantValue).Where(x => x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();
            //                    var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
            //                        .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
            //                        .Where(x =>
            //                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == mcLoop.MarketplaceId
            //                    && marketplaceCategoryMappings.Select(mc => mc.MarketplaceCategoryId).Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

            //                    foreach (var pLoop in _products)
            //                    {
            //                        Console.WriteLine($"{mcLoop.MarketplaceId} {_products.IndexOf(pLoop)}. Ürün");

            //                        var marketplaceCategory = marketplaceCategoryMappings.FirstOrDefault(x =>
            //                        x.MarketplaceCategory.MarketplaceId == mcLoop.MarketplaceId
            //                        && x.CategoryId == pLoop.CategoryId);


            //                        var anyProductMarketPlace = productMarketplaces.Any(x => x.ProductId == pLoop.Id && x.MarketplaceCompanyId == mcLoop.Id);
            //                        var marketplaceBrand = marketplaceBrandMappings.FirstOrDefault(x => x.BrandId == pLoop.BrandId);

            //                        var added = true;


            //                        if (!anyProductMarketPlace && marketplaceCategory != null && marketplaceBrand != null)
            //                        {
            //                            if (((mcLoop.MarketplaceId == "PA" || mcLoop.MarketplaceId == "TY") && string.IsNullOrEmpty(marketplaceBrand.MarketplaceBrandId)))
            //                            {
            //                                added = false;
            //                            }

            //                            if (added)
            //                                _productMarketplaces.Add(new Domain.Entities.ProductMarketplace
            //                                {
            //                                    Active = true,
            //                                    MarketplaceBrandId = marketplaceBrand.MarketplaceBrandId,
            //                                    MarketplaceBrandName = marketplaceBrand.MarketplaceBrandName,
            //                                    MarketplaceCategoryId = marketplaceCategory.MarketplaceCategoryId,
            //                                    
            //                                    TenantId = tLoop.Id,
            //                                    DomainId = dLoop.Id,
            //                                    CompanyId = theCompany.Id,
            //                                    ProductId = pLoop.Id,
            //                                    SellerCode = pLoop.SellerCode,
            //                                    CreatedDate = DateTime.Now,
            //                                    MarketplaceCompanyId = mcLoop.Id,
            //                                    UUId = Guid.NewGuid().ToString().ToLower()
            //                                });
            //                        }
            //                        foreach (var piLoop in pLoop.ProductInformations)
            //                        {
            //                            var firstProductInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.ProductInformationId == piLoop.Id && x.MarketplaceCompanyId == mcLoop.Id);


            //                            if (firstProductInformationMarketplace == null)
            //                            {

            //                                var dirty = true;
            //#if DEBUG
            //                                dirty = false;
            //#endif

            //                                var productInformationMarketplace = new Domain.Entities.ProductInformationMarketplace
            //                                {
            //                                    Active = true,
            //                                    AccountingPrice = false,
            //                                    DirtyPrice = dirty,
            //                                    DirtyStock = false,
            //                                    DirtyProductInformation = dirty,
            //                                    Barcode = piLoop.Barcode,
            //                                    ListUnitPrice = piLoop.ProductInformationPriceses.FirstOrDefault().ListUnitPrice,
            //                                    UnitPrice = piLoop.ProductInformationPriceses.FirstOrDefault().UnitPrice,
            //                                    StockCode = piLoop.StockCode,
            //                                    UUId = Guid.NewGuid().ToString().ToLower(),
            //                                    
            //                                    CreatedDate = DateTime.Now,
            //                                    TenantId = tLoop.Id,
            //                                    DomainId = dLoop.Id,
            //                                    CompanyId = theCompany.Id,
            //                                    MarketplaceCompanyId = mcLoop.Id,
            //                                    ProductInformationId = piLoop.Id,
            //                                    ProductInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>()
            //                                };

            //                                var variants = new List<string>();

            //                                foreach (var pivLoop in piLoop.ProductInformationVariants)
            //                                {

            //                                    var _marketplaceVariantMappings = marketplaceVariantMappings.Where(x => x.VariantId == pivLoop.VariantValue.VariantId).ToList();

            //                                    var _marketplaceVariantValueMappings = marketplaceVariantValueMappings.Where(x => x.VariantValueId ==
            //                                        pivLoop.VariantValueId && x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();

            //                                    MarketplaceCategoriesMarketplaceVariantValue marketplaceCategoriesMarketplaceVariantValue = null;

            //                                    if (marketplaceCategory != null && _marketplaceVariantMappings.Count > 0 && _marketplaceVariantValueMappings.Count > 0)
            //                                    {
            //                                        foreach (var mv in _marketplaceVariantValueMappings)
            //                                        {
            //                                            marketplaceCategoriesMarketplaceVariantValue = marketplaceCategoriesMarketplaceVariantValues.FirstOrDefault(x =>
            //                                     mv.MarketplaceVariantValueId == x.MarketplaceVariantValueId
            //                                    &&
            //                                      _marketplaceVariantMappings.Any(mv => mv.MarketplaceVariantId == x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId)
            //                                    &&
            //                                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId == marketplaceCategory.MarketplaceCategoryId);
            //                                            if (marketplaceCategoriesMarketplaceVariantValue != null && !variants.Any(x => x == marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId))
            //                                            {
            //                                                variants.Add(marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId);

            //                                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
            //                                                {
            //                                                    MarketplaceCategoriesMarketplaceVariantValueId = marketplaceCategoriesMarketplaceVariantValue.Id,
            //                                                    
            //                                                    CreatedDate = DateTime.Now,
            //                                                    TenantId = tLoop.Id,
            //                                                    DomainId = dLoop.Id,
            //                                                    CompanyId = theCompany.Id
            //                                                });
            //                                            }

            //                                        }






            //                                    }

            //                                }
            //                                productInformationMarketplace.ProductInformationMarketplaceVariantValues = productInformationMarketplace.ProductInformationMarketplaceVariantValues.GroupBy(x => x.MarketplaceCategoriesMarketplaceVariantValueId).Select(x => x.First()).ToList();

            //                                _productInformationMarketplaces.Add(productInformationMarketplace);

            //                            }
            //                            else
            //                            {
            //                                var variants = new List<string>();
            //                                variants.AddRange(
            //                                firstProductInformationMarketplace.ProductInformationMarketplaceVariantValues.Select(x => x.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId).ToList());

            //                                foreach (var pivLoop in piLoop.ProductInformationVariants)
            //                                {

            //                                    var _marketplaceVariantMappings = marketplaceVariantMappings.Where(x => x.VariantId == pivLoop.VariantValue.VariantId).ToList();

            //                                    var _marketplaceVariantValueMappings = marketplaceVariantValueMappings.Where(x => x.VariantValueId ==
            //                                        pivLoop.VariantValueId && x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();

            //                                    MarketplaceCategoriesMarketplaceVariantValue marketplaceCategoriesMarketplaceVariantValue = null;

            //                                    if (marketplaceCategory != null && _marketplaceVariantMappings.Count > 0 && _marketplaceVariantValueMappings.Count > 0)
            //                                    {
            //                                        foreach (var mv in _marketplaceVariantValueMappings)
            //                                        {
            //                                            marketplaceCategoriesMarketplaceVariantValue = marketplaceCategoriesMarketplaceVariantValues.FirstOrDefault(x =>
            //                                     mv.MarketplaceVariantValueId == x.MarketplaceVariantValueId
            //                                    &&
            //                                      _marketplaceVariantMappings.Any(mv => mv.MarketplaceVariantId == x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId)
            //                                    &&
            //                                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId == marketplaceCategory.MarketplaceCategoryId);


            //                                            if (marketplaceCategoriesMarketplaceVariantValue != null
            //                                                &&
            //                                                   !firstProductInformationMarketplace.ProductInformationMarketplaceVariantValues.Any(x => x.MarketplaceCategoriesMarketplaceVariantValueId == marketplaceCategoriesMarketplaceVariantValue.Id)
            //                                                && !variants.Any(x => x == marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId))
            //                                            {
            //                                                variants.Add(marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId);

            //                                                if (!updateProductInformationMarketplaces.Any(y => y.Id == firstProductInformationMarketplace.Id))
            //                                                {
            //                                                    var updateProductInformationMarketplace = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().FirstOrDefault(x => x.Id == firstProductInformationMarketplace.Id);
            //                                                    updateProductInformationMarketplace.DirtyProductInformation = true;
            //                                                    updateProductInformationMarketplaces.Add(updateProductInformationMarketplace);

            //                                                }
            //                                                _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
            //                                                {
            //                                                    ProductInformationMarketplaceId = firstProductInformationMarketplace.Id,
            //                                                    MarketplaceCategoriesMarketplaceVariantValueId = marketplaceCategoriesMarketplaceVariantValue.Id,
            //                                                    
            //                                                    CreatedDate = DateTime.Now,
            //                                                    TenantId = tLoop.Id,
            //                                                    DomainId = dLoop.Id,
            //                                                    CompanyId = theCompany.Id
            //                                                });
            //                                            }

            //                                        }


            //                                    }

            //                                }


            //                            }
            //                        }

            //                    }
            //                }

            //                try
            //                {
            //                    _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
            //                    _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);
            //                }
            //                catch (Exception exception)
            //                {

            //                }
        }

        private void InsertMarketplacesTY(List<ECommerce.Application.Common.Parameters.Product.ProductItem> _products, string marketplaceId)
        {
            //var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().FirstOrDefault(x => x.MarketplaceId == marketplaceId);
            ////var productInformations = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformation).Where(x => x.MarketplaceCompany.MarketplaceId == marketplaceId).ToList();
            //var _productInformations = _unitOfWork.ProductInformationRepository.DbSet().ToList();
            //var marketplaceCatagories = _unitOfWork.MarketplaceCategoryRepository.DbSet().Where(x => x.MarketplaceId == marketplaceId);

            //var catagories = _products.GroupBy(x => x.CategoryId).Select(x => $"{marketplaceId}-{x.First().CategoryId}");
            //var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
            //     .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
            //     .Include(x => x.MarketplaceVariantValue)
            //     .Where(x =>
            // x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == marketplaceId
            // && catagories.Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

            //var productMarketplaces = new List<ProductMarketplace>();
            //var productInformationMarketplaces = new List<ProductInformationMarketplace>();
            //foreach (var productItem in _products)
            //{

            //    ProductInformation productInformation = null;

            //    foreach (var sLoop in productItem.StockItems)
            //    {

            //        productInformation = _productInformations.FirstOrDefault(x => x.Barcode == sLoop.Barcode);

            //        if (productInformation != null)
            //        {


            //            var productInformationMarketplace = new ProductInformationMarketplace
            //            {
            //                CreatedDate = DateTime.Now,
            //                DomainId = _domainFinder.FindId(),
            //                TenantId = _tenantFinder.FindId(),
            //                CompanyId = _companyFinder.FindId(),
            //                Active = true,
            //                MarketplaceCompanyId = marketplaceCompany.Id,
            //                UnitPrice = sLoop.SalePrice,
            //                Barcode = sLoop.Barcode,
            //                ListUnitPrice = sLoop.ListPrice,
            //                
            //                ProductInformationId = productInformation.Id,
            //                StockCode = sLoop.StockCode,
            //                ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()
            //            };

            //            if (sLoop.Attributes != null)
            //                foreach (var attLoop in sLoop.Attributes)
            //                {
            //                    if (attLoop.AttributeCode != "47" && marketplaceId == "TY")
            //                    {

            //                        var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValueId.Replace("TY-", "") == attLoop.AttributeValueCode));
            //                        if (mVariantValue != null)
            //                            productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
            //                            {
            //                                DomainId = _domainFinder.FindId(),
            //                                TenantId = _tenantFinder.FindId(),
            //                                CompanyId = _companyFinder.FindId(),
            //                                CreatedDate = DateTime.Now,
            //                                
            //                                MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
            //                            });

            //                    }
            //                    else
            //                    {
            //                        var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValue.Value.ReplaceChar() == attLoop.AttributeValueName.ReplaceChar()));
            //                        if (mVariantValue != null)
            //                            productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
            //                            {
            //                                DomainId = _domainFinder.FindId(),
            //                                TenantId = _tenantFinder.FindId(),
            //                                CompanyId = _companyFinder.FindId(),
            //                                CreatedDate = DateTime.Now,
            //                                
            //                                MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
            //                            });

            //                    }
            //                }


            //            productInformationMarketplaces.Add(productInformationMarketplace);





            //        }


            //    }


            //    var _mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace($"{marketplaceId}-", "") == productItem.CategoryId));

            //    var marketplaceCatagoryId = string.Empty;

            //    if (_mVariantValue != null)
            //    {
            //        marketplaceCatagoryId = $"{marketplaceId}-" + productItem.CategoryId;

            //    }
            //    else
            //    {//Pazarama için yapıldı kategoriId göndermiyor adından buluyoruz
            //        var marketplaceCatagory = marketplaceCatagories.FirstOrDefault(x => x.Name == productItem.CategoryName);
            //        if (marketplaceCatagory != null)
            //        {
            //            marketplaceCatagoryId = marketplaceCatagory.Id;

            //        }
            //    }

            //    if (productInformation != null)
            //    {
            //        productMarketplaces.Add(new ProductMarketplace
            //        {
            //            SellerCode = productItem.SellerCode,
            //            Active = true,
            //            CompanyId = _companyFinder.FindId(),
            //            CreatedDate = DateTime.Now,
            //            DomainId = _domainFinder.FindId(),
            //            TenantId = _tenantFinder.FindId(),
            //            
            //            MarketplaceBrandId = productItem.BrandId,
            //            MarketplaceCategoryId = !string.IsNullOrEmpty(marketplaceCatagoryId) ? marketplaceCatagoryId : "",
            //            MarketplaceBrandName = productItem.BrandName,
            //            ProductId = productInformation.ProductId,
            //            MarketplaceCompanyId = marketplaceCompany.Id
            //        });
            //    }

            //}
            //_unitOfWork.ProductMarketplaceRepository.BulkInsert(productMarketplaces);
            //_unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(productInformationMarketplaces);
        }
        private void Seeding()
        {
            var accountingCompany = _unitOfWork.AccountingCompanyRepository.DbSet().Include(x => x.AccountingConfigurations).First();

            accountingCompany.AccountingConfigurations.Add(new AccountingConfiguration
            {
                Key = "SurchargeProductBarcode",
                Value = ""
            });
            accountingCompany.AccountingConfigurations.Add(new AccountingConfiguration
            {
                Key = "CargoProductBarcode",
                Value = ""
            });
            accountingCompany.AccountingConfigurations.Add(new AccountingConfiguration
            {
                Key = "InterestRateProductBarcode",
                Value = ""
            });


            _unitOfWork.AccountingCompanyRepository.Update(accountingCompany);
            return;
            if (_companyFinder.FindId() == 2)
            {
                var domain2 = new Domain.Entities.Domain
                {
                    TenantId = 1,
                    Name = "4&B",
                    DomainSetting = new DomainSetting
                    {
                        TenantId = 1,
                        ImagePath = "C:\\inetpub\\Helpy\\Erp\\wwwroot\\img\\Sunplast",
                        ImageUrl = "https://erp.helpy.com.tr/img/Sunplast",

                    }
                };

                _unitOfWork.DomainRepository.Create(domain2);

                #region Company Entities
                var company2 = new Company
                {
                    TenantId = 1,
                    DomainId = 2,
                    FullName = "4&B",
                    Name = "4&B",
                    CompanySetting = new CompanySetting
                    {
                        StockControl = true,
                        CreateNonexistProduct = true,
                        DefaultCurrencyId = "TL",
                        DefaultLanguageId = "TR",
                        DefaultCountryId = 1,
                        Theme = null,
                        UUId = null,
                        IncludeMarketplace = true,
                        IncludeECommerce = false,
                        IncludeExport = false,
                        OrderDetailMultiselect = true,
                        ShipmentSecret = false,
                        IncludeAccounting = true,
                        TenantId = 1,
                        DomainId = 2,

                    },
                    CompanyContact = new CompanyContact
                    {
                        TenantId = 1,
                        DomainId = 2,
                        WebUrl = "",
                        Address = "İstanbul",
                        PhoneNumber = "",
                        RegistrationNumber = "",
                        TaxNumber = "",
                        TaxOffice = "",
                        Email = ""
                    }
                };

                _unitOfWork.CompanyRepository.Create(company2);

                #endregion

                //var _marketPlaceCompany1 = new MarketplaceCompany
                //{
                //    Active = true,
                //    CompanyId = company2.Id,
                //    TenantId = 7,
                //    DomainId = 7,
                //    MarketplaceId = "TY",
                //    MarketplaceConfigurations = new List<MarketplaceConfiguration>
                //            {
                //                new MarketplaceConfiguration
                //                {
                //                    CompanyId = company2.Id,
                //                    TenantId = 6,
                //                    DomainId = 6,
                //                    Key="Username",
                //                    Value=""
                //                },
                //                new MarketplaceConfiguration
                //                {
                //                    CompanyId = company2.Id,
                //                   TenantId = 6,
                //                    DomainId = 6,
                //                    Key="Password",
                //                    Value=""
                //                },
                //               new MarketplaceConfiguration
                //                {
                //                    CompanyId = company2.Id,
                //                   TenantId = 6,
                //                    DomainId = 6,
                //                    Key="SupplierId",
                //                    Value=""
                //                },
                //                new MarketplaceConfiguration
                //                {
                //                    CompanyId = company2.Id,
                //                    TenantId = 6,
                //                    DomainId = 6,
                //                    Key="AverageCommission",
                //                    Value="0.22"
                //                }
                //            }

                //};

                //_unitOfWork.MarketplaceCompanyRepository.Create(_marketPlaceCompany1);

                return;
            }

            #region Seeding



            #region Tenant Entities
            var tenant = new Domain.Entities.Tenant
            {
                Name = "Mizalle",
                ContractDate = DateTime.Now,
                ExpirationDate = DateTime.Now.AddYears(1),
            };
            _unitOfWork.TenantRepository.Create(tenant);
            #endregion

            #region Domain Entities

            var domain1 = new Domain.Entities.Domain
            {
                TenantId = 1,
                Name = "Mizalle",
                DomainSetting = new DomainSetting
                {
                    TenantId = 1,
                    ImagePath = "C:\\inetpub\\Helpy\\Erp\\wwwroot\\img\\Mizalle",
                    ImageUrl = "https://erp.helpy.com.tr/img/Mizalle",

                }
            };

            _unitOfWork.DomainRepository.Create(domain1);

            #endregion



            #region Customer Entities

            var customerRole = new CustomerRole
            {
                Id = "DC",
                Name = "Default Customer",
            };
            _unitOfWork.CustomerRoleRepository.Create(customerRole);

            #endregion



            #region Company Entities
            var company1 = new Company
            {
                TenantId = 1,
                DomainId = 1,
                FullName = "Mizalle E-TİCARET İHRACAT VE İTHALAT LİMİTED ŞİRKETİ",
                Name = "Mizalle",
                CompanyContact = new CompanyContact
                {
                    DomainId = 1,
                    TenantId = 1,
                    WebUrl = "",
                    Address = "İstanbul",
                    PhoneNumber = "",
                    RegistrationNumber = "",
                    TaxNumber = "",
                    TaxOffice = "",
                    Email = ""
                },
                CompanyConfigurations = new List<CompanyConfiguration>
                {
                    new CompanyConfiguration
                     {

                         TenantId = 1,
                         DomainId = 1,
                         Name = "Iban Numarası",
                         Key = "Iban",
                         Value = ""
                     },
                     new CompanyConfiguration
                     {

                         TenantId = 1,
                         DomainId = 1,
                         Name = "Havale Yapıalcak Banka",
                         Key = "IbanBank",
                         Value = ""
                     },
                     new CompanyConfiguration
                     {

                         TenantId = 1,
                         DomainId = 1,
                         Name = "Youtube Adresi",
                         Key = "YoutubeUrl",
                         Value = ""
                     },
                         new CompanyConfiguration
                     {
                         TenantId = 1,
                         DomainId = 1,
                         Name = "Instagram Adresi",
                         Key = "InstagramUrl",
                         Value = ""
                     },
                         new CompanyConfiguration
                     {
                         TenantId = 1,
                         DomainId = 1,
                         Name = "Facebook Adresi",
                         Key = "FacebookUrl",
                         Value = ""
                     }
                     },
                CompanySetting = new CompanySetting
                {
                    StockControl = true,
                    CreateNonexistProduct = false,
                    DefaultCurrencyId = "TL",
                    DefaultLanguageId = "TR",
                    DefaultCountryId = 1,
                    Theme = "",
                    UUId = "Mizalle",
                    IncludeMarketplace = true,
                    IncludeWMS = true,
                    IncludeECommerce = false,
                    IncludeExport = false,
                    WebUrl = "",
                    OrderDetailMultiselect = true,
                    ShipmentSecret = false,
                    ShipmentSecretProductName = null,
                    IncludeAccounting = true,
                    TenantId = 1,
                    DomainId = 1

                },

            };

            _unitOfWork.CompanyRepository.Create(company1);

            #endregion

            #region Supplier Entities

            var supplier = new Supplier
            {
                CreatedDate = DateTime.Now,
                DomainId = 1,

                Name = "Mizalle",
                TenantId = 1,
            };
            _unitOfWork.SupplierRepository.Create(supplier);
            #endregion

            #region Brand Entities
            var brand = new Brand
            {
                Active = true,
                CreatedDate = DateTime.Now,
                DomainId = 1,
                Name = "Mizalle",
                TenantId = 1,
                Url = ""
            };
            _unitOfWork.BrandRepository.Create(brand);
            #endregion




            #region Payment Type Entities



            var paymentType1 = new PaymentType
            {
                Id = "C",
                Active = true,
                PaymentTypeTranslations = new List<PaymentTypeTranslation>
                {
                    new PaymentTypeTranslation
                    {
                        LanguageId = "TR",
                        Name = "HAVALE"
                    },
                },
                PaymentTypeDomains = new List<PaymentTypeDomain>
                {
                    new PaymentTypeDomain
                    {

                     Active = true,
                     TenantId = 1,
                     DomainId = 1,
                     PaymentTypeSetting= new PaymentTypeSetting
                        {
                            DiscountIsRate = false,
                            Discount = 0,
                            CostIsRate = false,
                            Cost = 0
                        }
                    }
                }
            };
            var paymentType2 = new PaymentType
            {
                Id = "IK",
                Active = true,
                PaymentTypeTranslations = new List<PaymentTypeTranslation>
              {
                new PaymentTypeTranslation
                {
                    LanguageId = "TR",
                    Name = "İndirim Kuponu"
                }
              },
                PaymentTypeDomains = new List<PaymentTypeDomain>
              {
                  new PaymentTypeDomain
                  {

                    Active = true,
                    TenantId = 1,
                    DomainId = 1,
                    PaymentTypeSetting=new PaymentTypeSetting
                    {
                        DiscountIsRate = false,
                        Discount = 0,
                        CostIsRate = false,
                        Cost = 0
                    }
                  }
              }

            };
            var paymentType4 = new PaymentType
            {
                Id = "OO",
                Active = true,
                PaymentTypeTranslations = new List<PaymentTypeTranslation>
              {
                new PaymentTypeTranslation
                {
                    LanguageId = "TR",
                    Name = "Online Ödeme"
                }
              },
                PaymentTypeDomains = new List<PaymentTypeDomain>
              {
                  new PaymentTypeDomain
                  {

                    Active = true,
                    TenantId = 1,
                    DomainId = 1,
                    PaymentTypeSetting=new PaymentTypeSetting
                    {
                        DiscountIsRate = false,
                        Discount = 0,
                        CostIsRate = false,
                        Cost = 0
                    }
                  }
              }

            };
            var paymentType5 = new PaymentType
            {
                Id = "DO",
                Active = true,
                PaymentTypeTranslations = new List<PaymentTypeTranslation>
              {
                new PaymentTypeTranslation
                {
                    LanguageId = "TR",
                    Name = "Kapıda Ödeme"
                }
              },
                PaymentTypeDomains = new List<PaymentTypeDomain>
              {
                  new PaymentTypeDomain
                  {

                    Active = true,
                    TenantId = 1,
                    DomainId = 1,
                    PaymentTypeSetting=new PaymentTypeSetting
                    {
                        DiscountIsRate = false,
                        Discount = 0,
                        CostIsRate = false,
                        Cost = 0
                    }
                  }
              }

            };
            var paymentType6 = new PaymentType
            {
                Id = "PDO",
                Active = true,
                PaymentTypeTranslations = new List<PaymentTypeTranslation>
              {
                new PaymentTypeTranslation
                {
                    LanguageId = "TR",
                    Name = "Kapıda Kredi Kartı / Nakit"
                }
              },
                PaymentTypeDomains = new List<PaymentTypeDomain>
              {
                  new PaymentTypeDomain
                  {

                    Active = true,
                    TenantId = 1,
                    DomainId = 1,
                    PaymentTypeSetting=new PaymentTypeSetting
                    {
                        DiscountIsRate = false,
                        Discount = 0,
                        CostIsRate = false,
                        Cost = 0
                    }
                  }
              }

            };

            _unitOfWork.PaymentTypeRepository.Create(paymentType1);
            _unitOfWork.PaymentTypeRepository.Create(paymentType2);
            _unitOfWork.PaymentTypeRepository.Create(paymentType4);
            _unitOfWork.PaymentTypeRepository.Create(paymentType5);
            _unitOfWork.PaymentTypeRepository.Create(paymentType6);




            #endregion

            #region Accounting Company Entities


            var accounting = new ECommerce.Domain.Entities.Accounting
            {
                Id = "NB",
                Name = "Nebim"
            };
            _unitOfWork.AccountingRepository.Create(accounting);


            var accountingCompany1 = new AccountingCompany
            {

                AccountingId = "NB",
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                AccountingConfigurations = new List<AccountingConfiguration>
                {
                 new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="ApiUrl",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="ApiPhotoUrl",//
                      Value=""
                  },
                   new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="WarehouseTransferOrders",//
                      Value=""
                  },
                    new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="OrderOfficeCode",//
                      Value=""
                  },
                     new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="OrderStoreCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="BankCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="CreditCardCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="PaymentCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="PaymentType",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="SalesPersonnelCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="NebimPOSTerminalID",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="ShippingCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="InterestDifferenceCode",//
                      Value=""
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="DeliveryCompanyCode",//
                      Value="1"
                  },
                  new AccountingConfiguration
                  {
                      TenantId=1,
                      DomainId=1,
                      CompanyId=1,
                      Key="sp_LastCodeCurrAccActive",//
                      Value="false"
                  },

                }

            };


            _unitOfWork.AccountingCompanyRepository.Create(accountingCompany1);

            #endregion

            #region Shipment Company Entities
            var shipmentCompany = new List<ShipmentCompany>
             {

                new ShipmentCompany
                {
                    Id = "ARS",
                    Name = "Aras Kargo"
                }, new ShipmentCompany
                {
                    Id = "B",
                    Name = "Borusan Lojistik"
                }, new ShipmentCompany
                {
                    Id = "H",
                    Name = "Horoz Lojistik"
                }, new ShipmentCompany
                {
                    Id = "HX",
                    Name = "HepsiJet"
                }, new ShipmentCompany
                {
                    Id = "KU",
                    Name = "Kurye"
                }, new ShipmentCompany
                {
                    Id = "M",
                    Name = "Mng Kargo"
                }, new ShipmentCompany
                {
                    Id = "P",
                    Name = "Ptt Kargo"
                }, new ShipmentCompany
                {
                    Id = "S",
                    Name = "Sürat Kargo"
                }, new ShipmentCompany
                {
                    Id = "TYEX",
                    Name = "Trendyol Express"
                }, new ShipmentCompany
                {
                    Id = "U",
                    Name = "Ups"
                }, new ShipmentCompany
                {
                    Id = "Y",
                    Name = "Yurtiçi Kargo"
                }
                , new ShipmentCompany
                {
                    Id = "MN",
                    Name = "Modanisa Kargo"
                }, new ShipmentCompany
                {
                    Id = "OT",
                    Name = "Modanisa Kargo"
                }, new ShipmentCompany
                {
                    Id = "TYFT",
                    Name = "Trendyol FT"
                }
               };
            _unitOfWork.ShipmentCompanyRepository.BulkInsert(shipmentCompany);


            #endregion

            #region Marketplace Entities


            var marketPlaceCompany1 = new MarketplaceCompany
            {
                Active = true,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "TY",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                        {
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Username",
                                Value=""
                            },
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Password",
                                Value=""
                            },
                           new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="SupplierId",
                                Value=""
                            }
                        }

            };

            var _marketPlaceCompany2 = new MarketplaceCompany
            {
                Active = true,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "CS",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                            {
                                new MarketplaceConfiguration
                                {
                              CompanyId = 1,
                              DomainId = 1,
                              TenantId = 1,
                                    Key="ApiKey",
                                    Value=""
                                }
                            }

            };

            var marketPlaceCompany3 = new MarketplaceCompany
            {
                Active = false,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "HB",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                        {
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="MerchantId",
                                Value=""
                            },
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Username",
                                Value=""
                            },
                          new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Password",
                                Value=""
                            },
                             new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="MutualBarcode",
                                Value="false"
                            }
                        }

            };

            var marketPlaceCompany4 = new MarketplaceCompany
            {
                Active = false,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "N11V2",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                        {
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="AppKey",
                                Value=""
                            },
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="AppSecret",
                                Value=""
                            },
                          new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="ShipmentTemplate",
                                Value=""
                            }
                        }

            };

            var marketPlaceCompany5 = new MarketplaceCompany
            {
                Active = false,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "TSoft",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                        {
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Url",
                                Value=""
                            },
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="User",
                                Value=""
                            },
                          new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Password",
                                Value=""
                            },
                           new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="Token",
                                Value=""
                            },
                            new MarketplaceConfiguration
                            {
                                CompanyId = 1,
                                DomainId=1,
                                TenantId=1,
                                Key="ExpirationTime",
                                Value=""
                            }
                        }

            };


            var _marketPlaceCompany6 = new MarketplaceCompany
            {
                Active = true,
                CompanyId = 1,
                DomainId = 1,
                TenantId = 1,
                MarketplaceId = "PA",
                MarketplaceConfigurations = new List<MarketplaceConfiguration>
                            {
                                new MarketplaceConfiguration
                                {
                              CompanyId = 1,
                              DomainId = 1,
                              TenantId = 1,
                                    Key="ApiKey",
                                    Value=""
                                },
                                new MarketplaceConfiguration
                                {
                                    CompanyId = 1,
                                    DomainId = 1,
                                    TenantId = 1,
                                    Key="MerchantId",
                                    Value=""
                                },
                               new MarketplaceConfiguration
                                {
                                 CompanyId = 1,
                                 DomainId = 1,
                                 TenantId = 1,
                                    Key="SecretKey",
                                    Value=""
                                }
                            }

            };




            _unitOfWork.MarketplaceCompanyRepository.Create(marketPlaceCompany1);
            _unitOfWork.MarketplaceCompanyRepository.Create(_marketPlaceCompany2);
            _unitOfWork.MarketplaceCompanyRepository.Create(marketPlaceCompany3);
            _unitOfWork.MarketplaceCompanyRepository.Create(marketPlaceCompany4);
            _unitOfWork.MarketplaceCompanyRepository.Create(marketPlaceCompany5);
            _unitOfWork.MarketplaceCompanyRepository.Create(_marketPlaceCompany6);

            #endregion







            #region Order Source Entities
            var orderSources = new List<OrderSource>
            {
                new OrderSource
                {
                    TenantId = 1,
                    Name = "Pazaryeri"
                },
                 new OrderSource
                {
                    TenantId = 1,
                    Name = "Web"
                },
            };
            _unitOfWork.OrderSourceRepository.BulkInsert(orderSources);
            #endregion


            #region Order Type Entities

            var orderType1 = new OrderType
            {
                Id = "BE",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                        new OrderTypeTranslation
                                {

                                    LanguageId = "TR",
                                    Name = "Beklemede"
                                }
                    }
            };
            var orderType2 = new OrderType
            {
                Id = "IP",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                    new OrderTypeTranslation
                        {

                            LanguageId = "TR",
                            Name = "İptal"
                        },
                    }
            };
            var orderType3 = new OrderType
            {
                Id = "OS",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                 {
                    new OrderTypeTranslation
                {

                    LanguageId = "TR",
                    Name = "Onaylanmış Sipariş"
                }
                    }
            };
            var orderType4 = new OrderType
            {
                Id = "KTE",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                  new OrderTypeTranslation
                {

                    LanguageId = "TR",
                    Name = "Kargoya Teslim Edildi"
                }
                    }
            };
            var orderType5 = new OrderType
            {
                Id = "PA",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                    new OrderTypeTranslation
                {

                    LanguageId = "TR",
                    Name = "Paketleniyor",
                }
                    }
            };
            var orderType6 = new OrderType
            {
                Id = "TE",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                   new OrderTypeTranslation
                {

                    LanguageId = "TR",
                    Name = "Tedarik Edilemeyen"
                }
                    }
            };
            var orderType7 = new OrderType
            {
                Id = "TS",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                   new OrderTypeTranslation
                        {

                            LanguageId = "TR",
                            Name = "Teslim Edildi"
                        }
                    }
            };
            var orderType8 = new OrderType
            {
                Id = "TO",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                   new OrderTypeTranslation
                        {

                            LanguageId = "TR",
                            Name = "Toplanıyor"
                        }
                    }
            };

            var orderType9 = new OrderType
            {
                Id = "TD",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                   new OrderTypeTranslation
                        {

                            LanguageId = "TR",
                            Name = "Toplandı"
                        }
                    }
            };


            var orderType10 = new OrderType
            {
                Id = "IA",
                OrderTypeTranslations = new List<OrderTypeTranslation>
                    {
                   new OrderTypeTranslation
                        {

                            LanguageId = "TR",
                            Name = "Iade"
                        }
                    }
            };

            _unitOfWork.OrderTypeRepository.Create(orderType1);
            _unitOfWork.OrderTypeRepository.Create(orderType2);
            _unitOfWork.OrderTypeRepository.Create(orderType3);
            _unitOfWork.OrderTypeRepository.Create(orderType4);
            _unitOfWork.OrderTypeRepository.Create(orderType5);
            _unitOfWork.OrderTypeRepository.Create(orderType6);
            _unitOfWork.OrderTypeRepository.Create(orderType7);
            _unitOfWork.OrderTypeRepository.Create(orderType8);
            _unitOfWork.OrderTypeRepository.Create(orderType9);
            _unitOfWork.OrderTypeRepository.Create(orderType10);
            #endregion



            #endregion
        }

        public void Run()
        {

            return;
            var marketPlacesProducts = _marketplaceService.GetProducts("TY");

            InsertProducts(marketPlacesProducts.Data);


            var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet().Where(x => x.MarketplaceId == "TY").ToList();

            var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformationMarketplaceVariantValues).ToList();

            var productInformations = _unitOfWork.ProductInformationRepository.DbSet().Include(x => x.ProductInformationTranslations).ThenInclude(x => x.ProductInformationContentTranslation).Include(x => x.ProductInformationPhotos).ToList();

            //var productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();
            var _productInformationMarketplaces = new List<ProductInformationMarketplace>();
            //var _productInformationPhotos = new List<ProductInformationPhoto>();
            //var productInformationContentTranslations = new List<ProductInformationContentTranslation>();
            var _productMarketplaces = new List<ProductMarketplace>();

            if (marketPlacesProducts.Success)
            {
                foreach (var productItem in marketPlacesProducts.Data)
                {
                    foreach (var sLoop in productItem.StockItems)
                    {
                        var productInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.Barcode == sLoop.Barcode);

                        var productInformation = productInformations.FirstOrDefault(x => x.Barcode == sLoop.Barcode);

                        if (productInformationMarketplace == null)
                        {
                            productInformationMarketplace = new ProductInformationMarketplace
                            {
                                ProductInformationId = productInformation.Id,
                                DomainId = productInformation.DomainId,
                                ListUnitPrice = sLoop.ListPrice,
                                UnitPrice = sLoop.SalePrice,
                                CompanyId = _companyFinder.FindId(),
                                AccountingPrice = false,
                                Active = sLoop.Active,
                                Barcode = sLoop.Barcode,
                                DirtyPrice = false,
                                DirtyProductInformation = false,
                                DirtyStock = false,
                                MarketplaceId = "TY",
                                StockCode = sLoop.StockCode,
                                TenantId = _tenantFinder.FindId(),
                                UUId = sLoop.UUId,
                                ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()

                            };
                        }


                        var productMarketplace = productMarketplaces.FirstOrDefault(x => x.ProductId == productInformation.ProductId);

                        if (productMarketplace != null && !_productMarketplaces.Any(p => p.ProductId == productInformation.ProductId))
                        {
                            productMarketplace.MarketplaceCategoryName = productItem.CategoryName;
                            productMarketplace.MarketplaceCategoryCode = productItem.CategoryId;
                            productMarketplace.SellerCode = productItem.SellerCode;
                            _productMarketplaces.Add(productMarketplace);
                        }
                        else
                        {
                            productMarketplace = new ProductMarketplace
                            {
                                MarketplaceCategoryName = productItem.CategoryName,
                                MarketplaceCategoryCode = productItem.CategoryId,
                                SellerCode = productItem.SellerCode,
                                MarketplaceBrandCode = productItem.BrandId,
                                MarketplaceBrandName = productItem.BrandName,
                                TenantId = _tenantFinder.FindId(),
                                ProductId = productInformation.ProductId,
                                MarketplaceId = "TY",
                                Active = true,
                                CompanyId = _companyFinder.FindId(),
                                DeliveryDay = productItem.DeliveryDay,
                                DomainId = _domainFinder.FindId(),
                                UUId = productItem.UUId
                            };
                            _productMarketplaces.Add(productMarketplace);

                        }


                        if (sLoop.Attributes != null)
                            foreach (var attLoop in sLoop.Attributes)
                            {
                                if (productInformationMarketplace.ProductInformationMarketplaceVariantValues.Any(
                                    x => x.MarketplaceVariantCode == attLoop.AttributeCode
                                    && x.MarketplaceVariantValueCode == attLoop.AttributeValueCode)) continue;

                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
                                {
                                    DomainId = _domainFinder.FindId(),
                                    TenantId = _tenantFinder.FindId(),
                                    CompanyId = _companyFinder.FindId(),
                                    AllowCustom = attLoop.AllowCustom,
                                    Mandatory = attLoop.Mandatory,
                                    MarketplaceVariantCode = attLoop.AttributeCode,
                                    MarketplaceVariantName = attLoop.AttributeName,
                                    MarketplaceVariantValueCode = attLoop.AttributeValueCode,
                                    MarketplaceVariantValueName = attLoop.AttributeValueName,
                                    ProductInformationMarketplaceId = productInformationMarketplace.Id,
                                    Multiple = attLoop.MultiValue,
                                    Varianter = attLoop.Varinatable,

                                });



                            }


                        _productInformationMarketplaces.Add(productInformationMarketplace);

                        //var productInformation = productInformations.FirstOrDefault(x => x.Id == productInformationMarketplace.ProductInformationId);

                        //productInformation.ProductInformationTranslations[0].ProductInformationContentTranslation.Description = sLoop.Description;
                        //productInformation.ProductInformationTranslations[0].ProductInformationContentTranslation.Content = sLoop.Description;


                        //productInformationContentTranslations.Add(productInformation.ProductInformationTranslations[0].ProductInformationContentTranslation);


                        //if (productInformation.ProductInformationPhotos.Count == 0)
                        //    productInformationPhotos.AddRange(sLoop.Images.Select(x => new ProductInformationPhoto
                        //    {
                        //        CreatedDate = DateTime.Now,
                        //        DisplayOrder = x.Order,
                        //        ProductInformationId = productInformationMarketplace.ProductInformationId,
                        //        TenantId = _tenantFinder.FindId(),
                        //        DomainId = _domainFinder.FindId(),
                        //        FileName = x.Url
                        //    }));


                    }
                }

                _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
                _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);


                //_unitOfWork.ProductInformationContentTranslationRepository.Update(productInformationContentTranslations);
                //_unitOfWork.ProductInformationPhotoRepository.BulkInsert(productInformationPhotos);

                //_unitOfWork.ProductInformationMarketplaceVariantValueRepository.BulkInsert(productInformationMarketplaceVariantValues);
            }



            InsertImages("\\\\172.31.57.21\\c$\\inetpub\\Helpy\\Erp\\wwwroot\\img\\Sunplast");




        }

        private void TyInvoiceSend()
        {
            var orders = _unitOfWork.OrderRepository.DbSet().Include(x => x.OrderInvoiceInformation).Where(x => x.MarketplaceId == "TY").ToList();

            var path = "C:\\MoonseaInvoice";
            var inovoices = System.IO.Directory.GetFiles("C:\\MoonseaInvoice");
            var i = 1;
            foreach (var invoice in inovoices)
            {
                var invoiceUrl = invoice.Replace($"{path}\\", "");

                var invoiceNumber = invoiceUrl.Split(new char[] { '-' }, StringSplitOptions.TrimEntries)[0];

                var order = orders.FirstOrDefault(x => x.OrderInvoiceInformation.Number == invoiceNumber);

                if (order != null)
                {
                    var marketplaceOrder = _marketplaceService.GetOrders(null, null, "TY", order.MarketplaceOrderNumber);
                    if (marketplaceOrder.Success && marketplaceOrder.Data.Orders.Count > 0)
                    {
                        _marketplaceService.PostInvoice(marketplaceOrder.Data.Orders[0].OrderPicking.MarketPlaceOrderId, $"https://erp.helpy.com.tr/MoonseaInvoice/{invoiceUrl}", "TY");
                        Console.WriteLine($"{i} numaralı {order.MarketplaceOrderNumber} sipariş gönderildi");
                    }
                }
                i++;

            }
        }


        private void InsertImages(string path)
        {
            Console.WriteLine("InsertImages");
            var _productInformationPhotos = _unitOfWork.ProductInformationPhotoRepository.DbSet().ToList();

            Dictionary<string, string> keys = new Dictionary<string, string>();

            var productPhotos = new List<Domain.Entities.ProductInformationPhoto>();

            foreach (var ppLoop in _productInformationPhotos)
            {
                if (keys.ContainsKey(ppLoop.FileName))
                {
                    ppLoop.FileName = keys[ppLoop.FileName];
                    Console.WriteLine("Cache'den geldi");
                }
                else
                {

                    var splitPhoto = ppLoop.FileName.Replace("?x-oss-process=style/resized", "").Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                    var fileName = $"{splitPhoto[splitPhoto.Length - 1]}";
                    var success = Download(path, fileName, ppLoop.ProductInformationId, ppLoop.FileName);

                    if (success)
                    {

                        keys.Add(ppLoop.FileName, $"{ppLoop.ProductInformationId}/{fileName}");

                        ppLoop.FileName = $"{ppLoop.ProductInformationId}/{fileName}";
                    }
                }

            }
            _unitOfWork.ProductInformationPhotoRepository.Update(_productInformationPhotos);


        }
        private DataResponse<List<Brand>> BulkRead(List<Brand> brands)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Brand>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theBrand in brands)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theBrand.Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkBrand (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkBrand";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(B.Id, 0) As Id
            ,T.Name
From        #TmpTableBulkBrand As T
Left Join   Brands As B With(NoLock)
On          T.Name = B.Name
            And B.DomainId = @DomainId

Drop Table  #TmpTableBulkBrand;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Brand
                        {
                            Id = dataReader.GetInt32("Id"),
                            Name = dataReader.GetString("Name")
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Category>> BulkRead(List<Category> categories)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Category>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Code", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theCategory in categories)
                {
                    var row = dataTable.NewRow();
                    row["Code"] = theCategory.Code;
                    row["Name"] = theCategory.CategoryTranslations.FirstOrDefault().Name;
                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkCategory (
    Code NVarChar(200) Not Null,
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkCategory";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(C.Id, 0) As Id
            ,T.Code,
            T.Name,
            IsNull(CT.Url,'') as Url
From        #TmpTableBulkCategory As T
Left Join   Categories As C With(NoLock)
On          T.Code = C.Code
            And C.DomainId = @DomainId
Left Join   CategoryTranslations CT ON CT.CategoryId=C.Id
            And CT.DomainId = @DomainId
Drop Table  #TmpTableBulkCategory;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Category
                        {
                            Id = dataReader.GetInt32("Id"),
                            Code = dataReader.GetString("Code"),
                            CategoryTranslations = new List<CategoryTranslation>
                            {
                                new CategoryTranslation
                                {
                                    CategoryId=dataReader.GetInt32("Id"),
                                    Name = dataReader.GetString("Name"),
                                    Url=dataReader.GetString("Url")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Product>> BulkRead(List<Product> products)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Product>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("SellerCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("StockCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));

                foreach (var theProduct in products)
                {
                    var row = dataTable.NewRow();
                    row["SellerCode"] = theProduct.SellerCode;
                    row["StockCode"] = theProduct.ProductInformations.First().StockCode;
                    row["Barcode"] = theProduct.ProductInformations.First().Barcode;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkProduct (
    SellerCode NVarChar(200) Not Null,
    StockCode NVarChar(200) Not Null,
    Barcode NVarChar(200) Not Null,
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkProduct";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(P.Id, 0) As Id
            ,IsNull(PI.Id, 0) As ProductInformationId
            ,T.SellerCode
            ,T.StockCode
            ,T.Barcode
From        #TmpTableBulkProduct As T
Left Join   Products As P With(NoLock)
On          REPLACE(T.SellerCode,' ','') = REPLACE(P.SellerCode,' ','')
            And P.DomainId = @DomainId
Left Join   ProductInformations As PI With(NoLock)
On          P.Id = PI.ProductId
            And PI.StockCode = T.StockCode
            And PI.Barcode = T.Barcode
            And PI.DomainId = @DomainId
Group by     IsNull(P.Id, 0)
            ,IsNull(PI.Id, 0)
            ,T.SellerCode
            ,T.StockCode
            ,T.Barcode
Drop Table  #TmpTableBulkProduct;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Product
                        {
                            Id = dataReader.GetInt32("Id"),
                            SellerCode = dataReader.GetString("SellerCode"),
                            ProductInformations = new()
                            {
                                new ProductInformation
                                {
                                    Id = dataReader.GetInt32("ProductInformationId"),
                                    StockCode = dataReader.GetString("StockCode"),
                                    Barcode = dataReader.GetString("Barcode")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Variant>> BulkRead(List<Variant> variants)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Variant>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theVariant in variants)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theVariant.VariantTranslations.First().Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkVariant (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkVariant";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VT.VariantId, 0) As Id
            ,T.Name
From        #TmpTableBulkVariant As T
Left Join   VariantTranslations As VT With(NoLock)
On          T.Name = VT.Name
            And VT.DomainId = @DomainId

Drop Table  #TmpTableBulkVariant;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Variant
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantTranslations = new List<VariantTranslation>
                            {
                                new VariantTranslation
                                {
                                    Name = dataReader.GetString("Name")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<VariantValue>> BulkRead(List<VariantValue> variantValues)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<VariantValue>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("VariantId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Value", typeof(string)));

                foreach (var theVariantValue in variantValues)
                {
                    var row = dataTable.NewRow();
                    row["VariantId"] = theVariantValue.VariantId;
                    row["Value"] = theVariantValue.VariantValueTranslations.First().Value;

                    if (row["Value"] != DBNull.Value)
                        dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table TmpTableBulkVariantValue (
    VariantId Int Not Null,
    Value NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "TmpTableBulkVariantValue";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VV.Id, 0) As Id
            ,IsNull(VV.VariantId, 0) As VariantId
            ,ISNULL(T.Value,VVT.Value) AS Value
From        VariantValues As VV With(NoLock)

Join		VariantValueTranslations As VVT With(NoLock)
On          VVT.VariantValueId = VV.Id
            And VVT.DomainId = @DomainId

Left Join   TmpTableBulkVariantValue As T
On          T.VariantId = VV.VariantId
            And VV.DomainId = @DomainId
			And VVT.Value = T.Value

Drop Table  TmpTableBulkVariantValue;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new VariantValue
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantId = dataReader.GetInt32("VariantId"),
                            VariantValueTranslations = new List<VariantValueTranslation>
                            {
                                new VariantValueTranslation
                                {
                                    Value = dataReader.GetString("Value")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private bool Download(string path, string fileName, int productInformationId, string url)
        {
            var success = true;


            string directory = $@"{path}\product\{productInformationId}";

            if (!System.IO.Directory.Exists(directory))
                System.IO.Directory.CreateDirectory(directory);


            string pathUrl = $@"{path}\product\{productInformationId}\{fileName.Replace("/", "\\")}";
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, pathUrl);
                    Console.WriteLine("Resim yüklendi");
                }
                catch (Exception e)
                {
                    success = false;
                }
            }

            return success;
        }

    }
}
