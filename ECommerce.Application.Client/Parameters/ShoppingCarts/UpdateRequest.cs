﻿namespace ECommerce.Application.Client.Parameters.ShoppingCarts
{
    public class UpdateRequest
    {
        #region Properties      

        public int ProductInformationId { get; set; }

        public bool ShoppingCartDiscount { get; set; }

        public int Quantity { get; set; }
        #endregion
    }
}
