﻿namespace ECommerce.Application.Client.Parameters.ShoppingCarts
{
    public class AddRequest
    {
        #region Properties

        public int ProductInformationId { get; set; }

        public bool ShoppingCartDiscount { get; set; }

        public int Quantity { get; set; }
        #endregion
    }
}
