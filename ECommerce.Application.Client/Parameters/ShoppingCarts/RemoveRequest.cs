﻿namespace ECommerce.Application.Client.Parameters.ShoppingCarts
{
    public class RemoveRequest
    {
        #region Properties      

        public int ProductInformationId { get; set; }
        #endregion
    }
}
