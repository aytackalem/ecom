﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OrderDeliveryAddress
    {
        #region Properties
        public string Recipient { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }       

        public int NeighborhoodId { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Mail { get; set; }
        #endregion

        #region Navigation Properties
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
