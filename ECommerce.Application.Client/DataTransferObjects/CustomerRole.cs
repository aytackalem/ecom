﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CustomerRole
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
