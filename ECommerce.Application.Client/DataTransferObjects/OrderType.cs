﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OrderType
    {
        #region Property
        /// <summary>
        /// Tip adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili tipe ait siparişler.
        /// </summary>
        public List<Order> Orders { get; set; }

        /// <summary>
        /// Sipariş tip dil bilgileri.
        /// </summary>
        public OrderTypeTranslation OrderTypeTranslation { get; set; }
        #endregion
    }
}
