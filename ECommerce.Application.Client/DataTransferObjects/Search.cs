﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Search
    {
        #region Properties
        public string Url { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
        public string PhotoUrl { get; set; }
        #endregion
    }
}
