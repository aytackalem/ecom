﻿using System;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationPrice
    {
        #region Properties
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal VatRate { get; set; }

        public decimal UnitCost { get; set; }

        public decimal Discount => Math.Round(100 - (this.UnitPrice * 100 / this.ListUnitPrice));


        /// <summary>
        /// UnitPrice x Quantity'den inidirmli düşmüş hali(x al y öde düşmüş hali)
        /// </summary>
        public decimal TotalUnitPrice { get; set; }
        #endregion
    }
}
