﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OldCity
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
        #endregion

    }
}
