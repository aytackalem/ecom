﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Group
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public int SortNumber { get; set; }
        #endregion

        #region Navigation Properties
        public GroupSeo GroupSeo { get; set; }

        public GroupContent GroupContent { get; set; }
        #endregion
    }
}
