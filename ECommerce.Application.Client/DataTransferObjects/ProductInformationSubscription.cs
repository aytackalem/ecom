﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationSubscription
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string PhoneNumber { get; set; }
        #endregion
    }
}
