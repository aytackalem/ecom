﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ShoppingCartDiscounted
    {


        #region Properties

        
        /// <summary>
        /// Genel indirim başlangıç tarihi.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Genel indirim bitiş tarihi.
        /// </summary>
        public DateTime EndDate { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// Sepette indirim ayarları.
        /// </summary>
        public ShoppingCartDiscountedSetting ShoppingCartDiscountedSetting { get; set; }
        #endregion

    }
}
