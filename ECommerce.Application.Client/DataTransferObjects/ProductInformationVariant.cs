﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationVariant
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public int VariantValueId { get; set; }
        #endregion


        #region Navigation Property
        /// <summary>
        /// İlgili varyasyon değeri.
        /// </summary>
        public VariantValue VariantValue { get; set; }
        #endregion
    }
}
