﻿using Newtonsoft.Json;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class LastOrderItem
    {
        #region Properties
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public string PaymentTypeId { get; set; }

        public string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public string Mail { get; set; }
        #endregion
    }
}
