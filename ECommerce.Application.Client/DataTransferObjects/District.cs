﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class District
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
        #endregion

        #region Navigation Propery
        public City City { get; set; }
        #endregion
    }
}
