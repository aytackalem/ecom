﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductContent
    {
        #region Properties
        public string Content { get; set; }
        #endregion
    }
}
