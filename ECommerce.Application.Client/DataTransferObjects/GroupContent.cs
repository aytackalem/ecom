﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class GroupContent
    {
        #region Properties
        public string Content { get; set; }
        #endregion
    }
}
