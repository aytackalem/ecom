﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CustomerAddress
    {
        #region Property
        /// <summary>
        /// Adresin id'si
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Müşterinin adresinin bulunduğu yer (Ev/iş/Dükkan)
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Alıcı adı soyadı
        /// </summary>
        public string Recipient { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Mahalle id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Açık adres veya adres tarifi.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Seçeli olduğu adresi gösterir
        /// </summary>
        public bool Default { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
