﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Country
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
