﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Company
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
        public CompanyContact CompanyContact { get; set; }
    }
}
