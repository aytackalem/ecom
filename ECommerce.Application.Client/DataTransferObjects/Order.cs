﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Order
    {
        #region Properties
        public int Id { get; set; }

        [JsonIgnore]
        public decimal ListTotal { get; set; }

        [JsonIgnore]
        public decimal Total { get; set; }

        [JsonIgnore]
        public string CurrencyId { get; set; }

        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyy");

        public int OrderTypeId { get; set; }

        /// <summary>
        /// Kullanıcının seçili olduğu adresi
        /// </summary>
        public int CustomerAddresId { get; set; }
        /// <summary>
        /// Kullanıcının seçli olduğu fatura aderesi
        /// </summary>
        public int CustomerInvoiceInformationId { get; set; }
        /// <summary>
        /// Fatura adresim farklı mı
        /// Farklı ise CustomerInvoiceInformationId alınır farklı değilse CustomerAddresId alınır
        /// </summary>
        public bool Differentaddress { get; set; }

        /// <summary>
        /// Sepette indirim Tutarı
        /// </summary>
        [JsonIgnore]
        public decimal ShoppingCartDiscount { get; set; }

        [JsonIgnore]
        public decimal Discount { get => this.ListTotal - this.Total; }


        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        [JsonIgnore]
        public decimal VatExcListTotal { get => this.OrderDetails != null ?  this.ListTotal / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        [JsonIgnore]
        public decimal VatExcTotal { get => this.OrderDetails != null ? this.Total / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        [JsonIgnore]
        public decimal VatExcDiscount { get => this.OrderDetails != null ? this.Discount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }

        #endregion

        #region Navigation Properties
        public OrderType OrderType { get; set; }

        public OrderNote OrderNote { get; set; }

        public Customer Customer { get; set; }
        [JsonIgnore]
        public List<OrderDetail> OrderDetails { get; set; }

        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        public OrderInvoiceInformation OrderInvoiceInformation { get; set; }

        public List<Payment> Payments { get; set; }
        public decimal CargoFee { get; set; }
        public decimal SurchargeFee { get; set; }
        #endregion
    }
}
