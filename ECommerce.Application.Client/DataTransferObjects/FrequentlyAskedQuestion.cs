﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class FrequentlyAskedQuestion
    {
        #region Properties

        public string Header { get; set; }

        public string Description { get; set; }

        #endregion


    }
}
