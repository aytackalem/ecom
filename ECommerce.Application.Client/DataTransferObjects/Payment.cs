﻿using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Payment
    {
        #region Properties
        public string PaymentTypeId { get; set; }

        public decimal Amount { get; set; }

        public int? DiscountCouponId { get; set; }

        [JsonIgnore]
        public bool Paid { get; set; }
        #endregion

        #region Navigation Properties
        public CreditCard CreditCard { get; set; }
        #endregion
    }

}
