﻿using Newtonsoft.Json;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ShoppingCartItem
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public bool ShoppingCartDiscount { get; set; }

        public int Quantity { get; set; }

        [JsonIgnore]
        public bool GetXPayYGift { get; set; }

        [JsonIgnore]
        public int GetXPayYCount { get; set; }
        #endregion

        #region Navigation Property
        [JsonIgnore]
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}