﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Information
    {
        #region Properties
        public int Id { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }
        #endregion

        #region Navigation Properties
        public InformationContent InformationContent { get; set; }

        public InformationSeo InformationSeo { get; set; }
        #endregion
    }
}
