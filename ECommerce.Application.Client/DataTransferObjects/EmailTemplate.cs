﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class EmailTemplate
    {
        #region Properties
        public string Template { get; set; }

        public string Subject { get; set; }
        #endregion

    }
}
