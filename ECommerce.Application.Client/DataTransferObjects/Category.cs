﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Category
    {
        #region Properties
        public int Id { get; set; }

        public int? ParentCategoryId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string IconUrl { get; set; }
        #endregion

        #region Navigation Properties
        public CategorySeo CategorySeo { get; set; }

        public CategoryContent CategoryContent { get; set; }

        public List<Category> ChildCategories { get; set; }

        public CategoryBreadcrumb CategoryBreadcrumb { get; set; }
        #endregion
    }
}
