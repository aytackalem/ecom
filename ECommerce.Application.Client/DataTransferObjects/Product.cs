﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Product
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public int CategoryId { get; set; }

        public int BrandId { get; set; }
        #endregion

        #region Navigation Properties
        public List<Category> Categories { get; set; }

        public ProductSeo ProductSeo { get; set; }

        public ProductContent ProductContent { get; set; }

        public List<ProductLabel> ProductLabels { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }

        public List<ProductProperty> ProductProperties { get; set; }

        public List<ProductTag> ProductTags { get; set; }
        #endregion
    }
}
