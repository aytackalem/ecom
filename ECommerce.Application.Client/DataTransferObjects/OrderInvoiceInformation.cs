﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OrderInvoiceInformation
    {
        #region Properties
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int NeighborhoodId { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }

        public string TaxOffice { get; set; }

        public string TaxNumber { get; set; }
        #endregion

        #region Navigation Properties
        public Neighborhood Neighborhood { get; set; }
        #endregion
    }
}
