﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        #region Properties
        public string FileName { get; set; }
        #endregion
    }
}
