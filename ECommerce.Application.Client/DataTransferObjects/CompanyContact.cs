﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CompanyContact
    {
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string WebUrl { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Facebook { get; set; }
        public string Iban { get; set; }
        public string IbanType { get; set; }
        public string IbanNameSurname { get; set; }

        /// <summary>
        /// Ücretsiz kargo limiti
        /// </summary>
        public string CargoLimit { get; set; }
        public string CargoAmount { get; set; }

    }
}
