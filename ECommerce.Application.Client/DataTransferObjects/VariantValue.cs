﻿using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class VariantValue
    {
        #region Properties
        public int Id { get; set; }

        public string Value { get; set; }

        public int ProductInformationId { get; set; }

        public bool Sale { get; set; }

        public bool Selected { get; set; }

        public string ImageUrl { get; set; }

        public bool Active { get; set; }

        public string Url { get; set; }

   
        #endregion

        public List<VariantValueTranslation> VariantValueTranslations { get; set; }
    }
}
