﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class TwoFactorCustomerUser
    {
        #region Properties      

        /// <summary>
        /// Aktif olduğu Değerde sms doğrulama modalı açılır
        /// </summary>
        public bool SmsModal { get; set; }

        /// <summary>
        /// Sms doğrulandığında aktif olur
        /// </summary>
        public bool SmsVerified { get; set; }

        /// <summary>
        /// Gönderilen sms Idsi
        /// </summary>
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// Müşterinin modalda yazdığı sms kodu
        /// </summary>
        [JsonIgnore]
        public string Code { get; set; }

        /// <summary>
        /// İlgili Müşterininin telefon numarası
        /// </summary>
        [JsonIgnore]
        public string Phone { get; set; }

        /// <summary>
        /// Sms gönderme limiti maksimum 3
        /// </summary>
        [JsonIgnore]
        public int TryCount { get; set; }

        /// <summary>
        /// Müşteririnin kullanıcı idsi
        /// </summary>
        [JsonIgnore]
        public int CustomerUserId { get; set; }
        #endregion


    }
}
