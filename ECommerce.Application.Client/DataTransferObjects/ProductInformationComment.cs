﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationComment
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string FullName { get; set; }

        public string Comment { get; set; }

        public float Rating { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyy");

        #endregion
    }
}
