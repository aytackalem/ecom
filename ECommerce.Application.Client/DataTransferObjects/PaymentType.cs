﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class PaymentType
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public PaymentTypeSetting PaymentTypeSetting { get; set; }
        #endregion
    }
}
