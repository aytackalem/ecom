﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationContent
    {
        #region Properties
        public string Description { get; set; }

        public string Content { get; set; }
    
        public string ExpertOpinion { get; set; }
        #endregion
    }
}
