﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformationBreadcrumb
    {
        #region Properties
        public string Html { get; set; }
        #endregion
    }
}
