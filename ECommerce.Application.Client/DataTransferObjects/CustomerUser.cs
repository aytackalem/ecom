﻿using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CustomerUser
    {
        #region Properties
       
        public int Id { get; set; }
       
        public string ManagerRoleId { get; set; }

        public string Username { get; set; }
       
        public string Password { get; set; }

        public string NewPassword { get; set; }

        public string Token { get; set; }

        public decimal MoneyPointAmount { get; set; }

        /// <summary>
        /// Müşteri Sms doğruladı mı?
        /// </summary>
        public bool Verified { get; set; }
        #endregion

        #region Navigation Properties
        public Customer Customer { get; set; }

        public CustomerRole CustomerRole { get; set; }
        #endregion
    }
}
