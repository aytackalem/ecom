﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OrderTypeTranslation
    {
        #region Property
        /// <summary>
        /// Tip adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Property

        #endregion
    }
}
