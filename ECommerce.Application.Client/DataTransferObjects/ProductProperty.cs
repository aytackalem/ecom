﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductProperty
    {
        #region Properties
        public int PropertyValueId { get; set; }

        public string Property { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
