﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CategoryContent
    {
        #region Properties
        public string Content { get; set; }
        #endregion
    }
}
