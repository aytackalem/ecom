﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Variant
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        public bool Photoable { get; set; }
        #endregion

        public List<VariantValue> VariantValues { get; set; }
        public bool ShowVariant { get; set; }
    }
}
