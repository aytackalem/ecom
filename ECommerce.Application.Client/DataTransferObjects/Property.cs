﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Property
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<PropertyValue> PropertyValues { get; set; }
    }
}
