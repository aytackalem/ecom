﻿using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Content
    {
        #region Properties
        public int Id { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }
        #endregion

        #region Navigation Properties
        public ContentContent ContentContent { get; set; }

        public ContentSeo ContentSeo { get; set; }
        #endregion
    }
}
