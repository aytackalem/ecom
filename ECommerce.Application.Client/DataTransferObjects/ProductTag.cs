﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductTag
    {
        #region Properties
        public int Id { get; set; }
        public string Value { get; set; }
        #endregion
    }
}
