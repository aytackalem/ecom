﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ContentSeo
    {
        #region Properties
        public string Title { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }
        #endregion

    }
}
