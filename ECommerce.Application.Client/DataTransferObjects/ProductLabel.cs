﻿using System;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductLabel
    {
        #region Properties
        public int Id { get; set; }
        public string Value { get; set; }
        #endregion
    }
}