﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CustomerUserVerification
    {
        #region Properties     

        public string Username { get; set; }

        /// <summary>
        /// Müşteriye gelen sms kodu
        /// </summary>
        public string SmsCode { get; set; }
        #endregion

        #region Navigation Properties
        public Customer Customer { get; set; }

        public CustomerRole CustomerRole { get; set; }
        #endregion
    }
}
