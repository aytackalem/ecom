﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CustomerContact
    {
        #region Properties
        public string Phone { get; set; }

        public string Mail { get; set; }

        public string TaxOffice { get; set; }

        public string TaxNumber { get; set; }
        #endregion
    }
}
