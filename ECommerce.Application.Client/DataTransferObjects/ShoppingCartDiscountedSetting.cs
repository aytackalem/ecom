﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ShoppingCartDiscountedSetting
    {
        #region Properties        

        /// <summary>
        /// Genel indirimin uygulabilir olacağı ürün alt limiti.
        /// </summary>
        public decimal Limit { get; set; }

        /// <summary>
        /// Genel indirim oran olarak mı yoksa rakam olarak mı kullanılacak?
        /// </summary>
        public bool DiscountIsRate { get; set; }

        /// <summary>
        /// Sepette indirim oran/rakam bilgisi.
        /// </summary>
        public decimal Discount { get; set; }
        #endregion

    }
}
