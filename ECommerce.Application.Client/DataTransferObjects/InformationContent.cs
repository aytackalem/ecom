﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class InformationContent
    {
        #region Properties
        public string Html { get; set; }
        #endregion

        #region Navigation Properties
        #endregion
    }
}
