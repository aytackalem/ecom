﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CategoryBreadcrumb
    {
        #region Properties
        public string Html { get; set; }
        #endregion
    }
}
