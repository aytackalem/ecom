﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    /// <summary>
    /// Müşteri üye olurkensms anlaşması var ise modal açılır. Müşteriye gelen sms doğrulanırsa üye olur.
    /// Müşterinin sms anlaşması yok ise müşteri üye olur ve modal açılmaz.
    /// </summary>
    public class CustomerUserRegister
    {
        #region Properties


        /// <summary>
        /// Aktif olduğu Değerde sms doğrulama modalı açılır
        /// </summary>
        public bool SmsModal { get; set; }


        public bool Verified { get; set; }
        #endregion


    }
}
