﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class OrderDetail
    {
        #region Property
        /// <summary>
        /// Detayin ait olduğu siparişin id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// List satış fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Birim fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Birim maliyeti.
        /// </summary>
        public decimal UnitCost { get; set; }

        /// <summary>
        /// Vergi oranı.
        /// </summary>
        public decimal VatRate { get; set; }

        /// <summary>
        /// Kargo Bedava mı?
        /// </summary>
        public bool Payor { get; set; }

        /// <summary>
        /// Sipariş detayının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Kdv dahil indirim fiyatı.
        /// </summary>
        public decimal UnitDiscount { get; set; }


        /// <summary>
        /// Kdv hariç liste satış fiyatı.
        /// </summary>
        public decimal VatExcListUnitPrice { get => this.ListUnitPrice / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim fiyatı.
        /// </summary>
        public decimal VatExcUnitPrice { get => this.UnitPrice / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim indirimi.
        /// </summary>
        public decimal VatExcUnitDiscount { get => this.UnitDiscount / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim maliyeti.
        /// </summary>
        public decimal VatExcUnitCost { get => this.UnitCost / (1 + Convert.ToDecimal(this.VatRate)); }

        #endregion

        #region Navigation Property

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
