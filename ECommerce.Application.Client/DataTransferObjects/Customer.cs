﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class Customer
    {
        #region Properties
        [JsonIgnore]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public int CustomerAddresId { get; set; }

        public int InvoiceCustomerAddresId { get; set; }

        /// <summary>
        /// Fatura adresim adresim ile aynı
        /// </summary>
        public bool SameInvoiceAddress { get; set; }
        #endregion

        #region Navigation Properties
        public CustomerUser CustomerUser { get; set; }

        public CustomerContact CustomerContact { get; set; }

        public List<CustomerAddress> CustomerAddresses { get; set; }

        public List<CustomerInvoiceInformation> CustomerInvoiceInformations { get; set; }

        public CustomerAddress DefaultCustomerAddress { get; set; }

        public CustomerInvoiceInformation DefaultCustomerInvoiceInformation { get; set; }
        #endregion
    }
}
