﻿using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Client.DataTransferObjects
{
    public class ProductInformation
    {
        public ProductInformation()
        {
            CategoryProducts = new List<ProductInformation>();
        }

        #region Fields

        private string _varinatFullName = string.Empty;
        private string _fullName = string.Empty;
        #endregion



        #region Properties
        public int Id { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public int Stock { get; set; }

        public int VirtualStock { get; set; }

        public string Name { get; set; }

        public string VariantValuesDescription { get; set; }

        public string Url { get; set; }

        public string SubTitle { get; set; }

        /// <summary>
        /// Ürün satışa açık mı.
        /// </summary>
        public bool IsSale { get; set; }

        public bool IsStock { get => (Stock + VirtualStock) > 0; }



        /// <summary>
        /// Kargo Bedava mı?
        /// </summary>
        public bool Payor { get; set; }

        /// <summary>
        /// Ürün Idsi
        /// </summary>
        public int ProductId { get; set; }
        #endregion

        #region Navigation Properties
        public Company Company { get; set; }

        public Product Product { get; set; }

        public ProductInformationSeo ProductInformationSeo { get; set; }

        public ProductInformationContent ProductInformationContent { get; set; }

        public ProductInformationPrice ProductInformationPrice { get; set; }

        public ProductInformationBreadcrumb ProductInformationBreadcrumb { get; set; }

        public List<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        public List<ProductInformationComment> ProductInformationComments { get; set; }


        public List<ProductInformation> CategoryProducts { get; set; }


        public List<ProductInformationVariant> ProductInformationVariants { get; set; }

        /// <summary>
        /// Variantların Toplu halini productinformation liste bazında verilir.
        /// isteğe bağlı Ya bu kullanılır yada VariantProductInformations kullanılır.
        /// </summary>
        public List<Variant> Variants { get; set; }


 
        #endregion





        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(_fullName))
                {
                    this._fullName = this.Name;
                    if (!string.IsNullOrEmpty(this.VariantValuesDescription))
                        this._fullName = $"{this.Name} {this.VariantValuesDescription}";
                }

                return this._fullName;
            }
        }
    }
}
