﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class PropertyValue
    {
        public int Id { get; set; }

        public int PropertyId { get; set; }

        public string Value { get; set; }
    }
}
