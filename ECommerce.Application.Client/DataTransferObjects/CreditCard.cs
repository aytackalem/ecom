﻿namespace ECommerce.Application.Client.DataTransferObjects
{
    public class CreditCard
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public byte ExpireMonth { get; set; }
        public int ExpireYear { get; set; }
        public string Cvv { get; set; }
    }

}
