﻿namespace ECommerce.Application.Client.Constants
{
    public static class ImagePath
    {
        #region Properties
        public static string Banner => @"C:\Image\Banner\";

        public static string Product => @"C:\Image\Product\";
        #endregion
    }
}
