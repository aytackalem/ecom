﻿namespace ECommerce.Application.Client.Constants
{
    public static class CacheKey
    {
        #region Properties
        public static string Categories => "Categories";

        public static string CategoryProperties => "CategoryProperties";

        public static string GetXPayYSettings => "GetXPayYSettings";

        public static string ShoppingCartDiscountedProducts => "ShoppingCartDiscountedProducts";
        public static string ShoppingCartDiscounteds => "ShoppingCartDiscounteds";

        public static string ProductinformationReadInfo => "{0}_ProductinformationReadInfo";

        public static string Properties => "Properties";

        public static string Groups => "Groups";

        public static string GroupProperties => "GroupProperties";

        public static string Contents => "Contents";

        public static string Configurations => "Configurations";

        public static string Informations => "Informations";

        public static string City => "{0}-City";

        public static string Company => "{0}-Companies";

        public static string CitiesByCountryId => "{0}-CitiesByCountryId";

        public static string DistrictsByCityId => "{0}-DistrictsByCityId";

        public static string NeighborhoodsByDistrictId => "{0}-NeighborhoodsByDistrictId";

        public static string Neighborhoods => "{0}-Neighborhoods";

        public static string GoogleConfiguration => "{0}-GoogleConfiguration";

        public static string FacebookConfiguration => "{0}-FacebookConfiguration";

        public static string CustomerUser => "{0}-{1}-CustomerUser";

        public static string PaymentTypes => "PaymentTypes";
        public static string Brands => "Brands";
        #endregion
    }
}
