﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Ads
{
    /// <summary>
    /// Google Ads kodlarını oluşturan sınıftır.
    /// </summary>
    public interface IGoogleAdsService
    {
        #region Methods
        /// <summary>
        /// Layout sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> Layout();

        /// <summary>
        /// Sipariş sonrası teşekkürler sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> Done(LastOrderItem lastOrderItem);
        #endregion
    }
}
