﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Ads
{
    /// <summary>
    /// Facebook Pixel kodlarını oluşturan sınıftır.
    /// </summary>
    public interface IFacebookPixelService
    {
        #region Methods
        /// <summary>
        /// Layout sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <param name="path">İstek yapılan url de (domain hariç tutularak) bulunan path.</param>
        /// <returns></returns>
        DataResponse<string> Layout(string path);

        /// <summary>
        /// Sipariş sonrası teşekkürler sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> Done(LastOrderItem lastOrderItem);
        #endregion
    }
}
