﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Ads
{
    /// <summary>
    /// Google Analytics kodlarını oluşturan sınıftır.
    /// </summary>
    public interface IGoogleAnalyticsService
    {
        #region Methods
        /// <summary>
        /// Layout sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> Layout();

        /// <summary>
        /// Sipariş sonrası teşekkürler sayfasına yazdırılacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> Done(LastOrderItem lastOrderItem, List<ShoppingCartItem> shoppingCartItems);
        #endregion
    }
}
