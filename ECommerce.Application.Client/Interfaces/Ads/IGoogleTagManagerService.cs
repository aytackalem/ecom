﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Ads
{
    /// <summary>
    /// Google Analytics kodlarını oluşturan sınıftır.
    /// </summary>
    public interface IGoogleTagManagerService
    {
        #region Methods
        /// <summary>
        /// Layout head etiketinden hemen sonra yazdıralacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> LayoutHead();

        /// <summary>
        /// Layout body etiketinden hemen sonra yazdıralacak olan kodu oluşturan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<string> LayoutBody();
        #endregion
    }
}
