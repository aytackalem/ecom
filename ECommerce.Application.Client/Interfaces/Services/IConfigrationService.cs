﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IConfigrationService
    {
        #region Methods

        DataResponse<Configuration> Read();
        #endregion
    }
}
