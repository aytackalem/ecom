﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IShowcaseService
    {
        #region Methods
        DataResponse<Showcase> Read(int? brandId = null, int? categoryId = null);
        #endregion
    }
}
