﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ICompanyService
    {
        #region Methods
        DataResponse<Company> Read();
        #endregion
    }
}
