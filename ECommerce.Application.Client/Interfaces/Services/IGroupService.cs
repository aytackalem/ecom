﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IGroupService
    {
        #region Methods
        /// <summary>
        /// İsim bilgilerinin yer aldığı kategori nesnesini döndüren metod.
        /// </summary>
        /// <param name="id">Grup id bilgisi.</param>
        /// <returns></returns>
        DataResponse<Group> Read(int id);

        /// <summary>
        /// Tüm grupları döndüren metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<Group>> Read();

        DataResponse<Group> ReadInfo(int id);


        DataResponse<List<Property>> ReadGroupProperties(int id);


        DataResponse<List<ProductLabel>> ReadGroupLabels(int id);
        #endregion
    }
}
