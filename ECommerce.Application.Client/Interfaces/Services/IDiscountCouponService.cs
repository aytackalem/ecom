﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IDiscountCouponService
    {
        #region Methods
        DataResponse<DiscountCoupon> Read(string code);

        Response Clear();

        Response Set(string code);
        #endregion
    }
}
