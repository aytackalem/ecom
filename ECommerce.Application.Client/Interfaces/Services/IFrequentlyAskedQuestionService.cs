﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IFrequentlyAskedQuestionService
    {
        DataResponse<List<FrequentlyAskedQuestion>> Read();
    }
}
