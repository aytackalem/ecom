﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ICategoryService
    {
        #region Methods
        /// <summary>
        /// İsim bilgilerinin yer aldığı kategori nesnesini döndüren metod.
        /// </summary>
        /// <param name="id">Kategori id bilgisi.</param>
        /// <returns></returns>
        DataResponse<Category> ReadInfo(int id);

        /// <summary>
        /// Tüm kategorileri döndüren metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<Category>> Read();

        /// <summary>
        /// İsim bilgilerinin yer aldığı kategori nesnesini döndüren metod.
        /// </summary>
        /// <param name="id">Kategori id bilgisi.</param>
        /// <returns></returns>
        DataResponse<Category> Read(int id);


        DataResponse<List<Property>> ReadCategoryProperties(int id);

        DataResponse<List<ProductLabel>> ReadCategoryLabels(int id);
        #endregion
    }
}
