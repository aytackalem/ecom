﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ICityService
    {
        #region Methods
        DataResponse<City> Read(int id);

        DataResponse<List<City>> ReadByCountryId(int countryId);
        #endregion
    }
}
