﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Client.DataTransferObjects;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ICustomerUserService
    {
        #region Methods

        DataResponse<CustomerUser> Read(int id);

        DataResponse<CustomerUser> Read(CustomerUser customerUser, bool cache = true);

        DataResponse<CustomerUserRegister> Create(CustomerUser dto);

        DataResponse<TwoFactorCustomerUser> UserVerification(CustomerUserVerification dto);

        DataResponse<CustomerUser> CreateRecovery(string username);

        DataResponse<CustomerUser> UpdatePassword(CustomerUser dto);

        Response ResetPassword(CustomerUser dto);

        Response MemberUpdate(Customer dto);
        #endregion
    }
}
