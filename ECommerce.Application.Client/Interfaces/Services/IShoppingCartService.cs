﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Parameters.ShoppingCarts;
using ECommerce.Application.Client.ViewModels.ShoppingCarts;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IShoppingCartService
    {
        #region Methods
        DataResponse<List<ShoppingCartItem>> Get();
        
        /// <summary>
        /// Ürün  Sepete eklendiğinde mapleme için kullanılır
        /// </summary>
        /// <param name="shoppingCartItems"></param>
        /// <returns></returns>
        DataResponse<List<ShoppingCartItem>> Mapping(List<ShoppingCartItem> shoppingCartItems);

        Create Add(AddRequest addRequest);
        
        Response Remove(RemoveRequest removeRequest);
        
        Response Update(UpdateRequest updateRequest);
        
        Response Clear();

        DataResponse<int> Count();
        #endregion
    }
}
