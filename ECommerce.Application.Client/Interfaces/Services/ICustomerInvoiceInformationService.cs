﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ICustomerInvoiceInformationService
    {
        DataResponse<List<CustomerInvoiceInformation>> Read(int customerUserId);

        DataResponse<CustomerInvoiceInformation> Read(int id, int customerUserId);

        Response Delete(int id, int customerUserId);

        Response Upsert(CustomerInvoiceInformation dto);

        /// <summary>
        /// Müşterinin adresini seçer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerUserId"></param>
        /// <returns></returns>
        Response Default(int id, int customerUserId);
    }
}
