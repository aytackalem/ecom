﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Orders;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface INewBulletinService
    {
        #region Methods   
        Response Create(NewBulletin newBulletin);   
        #endregion
    }
}
