﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IGetXPayYSettingService
    {
        #region Methods
        DataResponse<GetXPayYSetting> Read();
        #endregion
    }
}
