﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IEmailTemplateService
    {
        /// <summary>
        /// Şifre Resetleme maili
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        DataResponse<EmailTemplate> ResetPasswordTemplate(int customerId);

        /// <summary>
        /// Havale Maili
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        DataResponse<EmailTemplate> OrderTransferTemplate(int orderId);

        /// <summary>
        /// Yeni Sipariş Maili
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<EmailTemplate> NewOrderTemplate(int orderId);


        /// <summary>
        /// Yeni Bülten Maili
        /// </summary>
        /// <param name="informationId"></param>
        /// <returns></returns>
        DataResponse<EmailTemplate> NewBulletinTemplate(int informationId);
    }
}
