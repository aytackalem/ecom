﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IBrandService
    {
        #region Methods

        /// <summary>
        /// Tüm brandları döndüren metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<Brand>> Read();

        DataResponse<Brand> Read(int id);

        DataResponse<List<Property>> ReadBrandProperties(int id);


        DataResponse<List<ProductLabel>> ReadBrandLabels(int id);

        #endregion
    }
}
