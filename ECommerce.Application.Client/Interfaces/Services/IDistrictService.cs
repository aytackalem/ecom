﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IDistrictService
    {
        #region Methods
        DataResponse<District> Read(int id);

        DataResponse<List<District>> ReadByCityId(int cityId);
        #endregion
    }
}
