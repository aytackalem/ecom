﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IInformationService
    {
        #region Methods
        DataResponse<Information> Read(int id);

        DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue();
        #endregion
    }
}
