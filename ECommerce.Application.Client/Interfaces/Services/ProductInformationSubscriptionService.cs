﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IProductInformationSubscriptionService
    {
        #region Methods
        Response Create(ProductInformationSubscription productInformationSubscription);
        #endregion
    }
}
