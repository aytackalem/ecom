﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IShoppingCartDiscountedProductService
    {
        #region Methods
        DataResponse<List<ShoppingCartDiscountedProduct>> Read();
        #endregion
    }
}
