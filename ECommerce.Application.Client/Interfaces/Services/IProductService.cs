﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IProductService
    {
        #region Methods
        /// <summary>
        /// Ürüne ait tüm ilişkili bilgileri döndüren metod.
        /// </summary>
        /// <param name="productInformationId"></param>
        /// <returns></returns>
        DataResponse<ProductInformation> Read(int productInformationId);

        /// <summary>
        /// Ürüne ait isim, fiyat ve fotoğraf bilgilerini döndüren metod.
        /// </summary>
        /// <param name="productInformationId"></param>
        /// <returns></returns>
        DataResponse<ProductInformation> ReadInfo(int productInformationId);

        PagedResponse<ProductInformation> InfiniteReadByCategoryId(int categoryId, int page, int pageRecordsCount,int sorting);
        
        PagedResponse<ProductInformation> InfiniteReadByCategoryId(int categoryId, int page, int pageRecordsCount, int sorting, List<int> propertyValueIds);

        PagedResponse<ProductInformation> InfiniteReadByKeyword(int page, int pageRecordsCount, int sorting, string keyword);

        PagedResponse<ProductInformation> InfiniteReadByProperties(int page, int pageRecordsCount, int sorting, List<int> propertyValueIds);

        PagedResponse<ProductInformation> InfiniteReadByBrandId(int brandId, int page, int pageRecordsCount, int sorting);
        
        PagedResponse<ProductInformation> InfiniteReadByBrandId(int brandId, int page, int pageRecordsCount, int sorting, List<int> propertyValueIds);

        PagedResponse<ProductInformation> InfiniteReadByDiscount(int discount, int page, int pageRecordsCount, int sorting);

        DataResponse<ProductInformation> ReadVariants(int productInformationId);
        #endregion
    }
}
