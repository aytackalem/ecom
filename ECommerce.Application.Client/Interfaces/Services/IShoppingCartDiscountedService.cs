﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IShoppingCartDiscountedService
    {
        #region Methods
        DataResponse<List<ShoppingCartDiscounted>> Read();
        #endregion
    }
}
