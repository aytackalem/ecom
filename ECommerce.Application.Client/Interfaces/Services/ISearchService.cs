﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface ISearchService
    {
        #region Methods
        DataResponse<List<Search>> ReadInfo(string keyword);
        #endregion
    }
}
