﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Orders;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IOrderService
    {
        #region Methods
        Create Create(Order order);

        DataResponse<List<Order>> ReadByCustomerId(int customerId);

        Response Clear();

        /// <summary>
        /// Client tarafından verilmiş son siparişi çerez yardımı ile getiren metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<LastOrderItem> GetLastOrder();
        #endregion
    }
}
