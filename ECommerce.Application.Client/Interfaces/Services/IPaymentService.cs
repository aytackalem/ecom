﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Orders;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IPaymentService
    {
        #region Methods
        Response UpdateSale(int orderId);

        DataResponse<ECommerce.Application.Client.DataTransferObjects.Payment> Read(int orderId, string paymentTypeId);
        #endregion
    }
}
