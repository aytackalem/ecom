﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IProductInformationCommentService
    {
        #region Methods
        Response Create(ProductInformationComment productInformationComment);

        PagedResponse<ProductInformationComment> InfiniteRead(int productInformationId, int page, int pageRecordsCount);
        #endregion
    }
}
