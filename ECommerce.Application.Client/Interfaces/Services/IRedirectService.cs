﻿namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IRedirectService
    {
        #region Methods
        string Find(string path);
        #endregion
    }
}
