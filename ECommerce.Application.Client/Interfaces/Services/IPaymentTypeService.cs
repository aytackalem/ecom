﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface IPaymentTypeService
    {
        #region Methods
        DataResponse<List<PaymentType>> Read();
        #endregion
    }
}
