﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Client.Interfaces.Services
{
    public interface INeighborhoodService
    {
        #region Methods
        DataResponse<Neighborhood> Read(int id);

        DataResponse<List<Neighborhood>> ReadByDistrictId(int districtId);
        #endregion
    }
}
