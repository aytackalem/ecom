﻿namespace ECommerce.Application.Client.Interfaces.Helpers
{
    public interface IFileCaching
    {
        #region Methods
        public void Write<T>(string key, T t);

        public T Read<T>(string key);

        public bool Contains(string key);
        #endregion
    }
}
