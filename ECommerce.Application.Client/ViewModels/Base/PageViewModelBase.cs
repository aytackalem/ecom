﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Base
{
    public abstract class PageViewModelBase : ViewModelBase
    {
        #region Properties
        public int ShoppingCartItemsCount { get; set; }

        public bool IsLogin { get; set; }
        #endregion

        #region Navigation Properties
        public CustomerUser CustomerUser { get; set; }

        public DataTransferObjects.Company Company { get; set; }
        
        public List<Category> Categories { get; set; }

        public List<Brand> Brands { get; set; }

        public List<Group> Groups { get; set; }

        public List<ShoppingCartItem> ShoppingCartItems { get; set; }

        public List<KeyValue<string, string>> Informations { get; set; }

        public List<KeyValue<string, string>> Contents { get; set; }
        #endregion
    }
}
