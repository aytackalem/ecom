﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Accounts
{
    public class Index : PageViewModelBase
    {
        public decimal MoneyPointAmount { get; set; }

        public List<Order> Orders { get; set; }

        public Customer Customer { get; set; }

    }
}
