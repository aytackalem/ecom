﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Accounts
{
    public class CustomerAddress
    {
        public int DefaultCountryId { get; set; }

        public List<City> Cities { get; set; }

        public List<District> Districts { get; set; }

        public List<Neighborhood> Neighborhoods { get; set; }

        public DataTransferObjects.CustomerAddress Address { get; set; }
    }
}
