﻿using ECommerce.Application.Client.ViewModels.Base;

namespace ECommerce.Application.Client.ViewModels.Accounts
{
    public class ResetPassword : PageViewModelBase
    {
        #region Properties
        public string Token { get; set; }
        #endregion
    }
}
