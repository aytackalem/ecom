﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Products
{
    public class Index : PageViewModelBase
    {
        #region Properties

        #endregion

        #region Navigation Properties
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
