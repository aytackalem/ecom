﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Groups
{
    public class Index : PageViewModelBase
    {
        #region Properties
        /// <summary>
        /// Kullanıcının bulunduğu Sayfa Sayısı
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Toplam Sayfa Sayısı
        /// </summary>
        public int PagesCount { get; set; }
        /// <summary>
        /// Toplam Ürün Sayısı
        /// </summary>
        public int ProductCount { get; set; }
        /// <summary>
        /// Sayfaya Basılan Ürün Sayısı
        /// </summary>
        public int PageProductCount { get; set; }

        /// <summary>
        /// Kullanıcının bulunduğu Ürün Sayısı (20,50,100)
        /// </summary>
        public int PageRecordsCount { get; set; }

        /// <summary>
        /// Kullanıcının bulunduğu Ürün Sıralaması
        /// 0 Yok
        /// 1 Artan Fiyat
        /// 2 Azalan Fiyat
        /// 3 Alfabetik A dan Z'ye
        /// 4 Alfabetik Z dan A'ya
        /// </summary>
        public int PageSorting { get; set; }

        public List<int> PropertyValueIds { get; set; }
        #endregion

        #region Navigation Properties
        public Group Group { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }

        public List<Property> Properties { get; set; }
        #endregion
    }
}
