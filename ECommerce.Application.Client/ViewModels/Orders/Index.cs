﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Client.ViewModels.Orders
{
    public class Index : PageViewModelBase
    {
        #region Properties

        public decimal TotalUnitPrice => this.ShoppingCartItems.Sum(sci => sci.ProductInformation.ProductInformationPrice.TotalUnitPrice);
        /// <summary>
        /// Kupon indirim hesaplama
        /// </summary>
        public decimal DiscountCouponDiscount
        {
            get
            {
                var discountCouponDiscount = 0M;

                if (this.DiscountCoupon != null && this.ShoppingCartItemsCount > 0 && this.TotalUnitPrice > this.DiscountCoupon.DisountCouponSetting.Limit)
                {
                    discountCouponDiscount = this.DiscountCoupon.DisountCouponSetting.Discount;

                    if (this.DiscountCoupon.DisountCouponSetting.DiscountIsRate)
                        discountCouponDiscount = this.TotalUnitPrice * this.DiscountCoupon.DisountCouponSetting.Discount;
                }

                return discountCouponDiscount;
            }
        }

        /// <summary>
        /// Sepette inidirim hesaplama
        /// </summary>
        public decimal ShoppingCartDiscount
        {
            get
            {
                var shoppingCartDiscount = 0M;

                if (ShoppingCartDiscounteds.Count > 0)
                {

                    var shoppingCartDiscounted = ShoppingCartDiscounteds.Where(x => x.ShoppingCartDiscountedSetting.Limit <= this.TotalUnitPrice).OrderByDescending(x => x.ShoppingCartDiscountedSetting.Limit).FirstOrDefault();

                    if (shoppingCartDiscounted != null && this.ShoppingCartItemsCount > 0)
                    {
                        shoppingCartDiscount = shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;

                        if (shoppingCartDiscounted.ShoppingCartDiscountedSetting.DiscountIsRate)
                            shoppingCartDiscount = this.TotalUnitPrice * shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;
                    }
                }

                return shoppingCartDiscount;
            }
        }

        public decimal Amount => this.TotalUnitPrice - (this.DiscountCouponDiscount + this.ShoppingCartDiscount);

        public int SelectedNeighborhoodId
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].NeighborhoodId : 0;
            }
        }

        public int SelectedDisctrictId
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].Neighborhood.District.Id : 0;
            }
        }

        public int SelectedCityId
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].Neighborhood.District.City.Id : 0;
            }
        }

        public string Name
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null ? this.CustomerUserDetail.Customer.Name : "";
            }
        }

        public string Surname
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null ? this.CustomerUserDetail.Customer.Surname : "";
            }
        }

        public string Phone
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerContact != null ? this.CustomerUserDetail.Customer.CustomerContact.Phone : "";
            }
        }

        public string Email
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerContact != null ? this.CustomerUserDetail.Customer.CustomerContact.Mail : "";
            }
        }

        public string Address
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].Address : "";
            }
        }

        public int SelectedInvoiceNeighborhoodId
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail.Customer != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].NeighborhoodId : 0;
            }
        }

        public int SelectedInvoiceDisctrictId
        {
            get
            {
                return this.CustomerUserDetail != null && this.CustomerUserDetail != null && this.CustomerUserDetail.Customer.CustomerAddresses.Count > 0 ? this.CustomerUserDetail.Customer.CustomerAddresses[0].Neighborhood.District.Id : 0;
            }
        }
        /// <summary>
        /// 1 Türkiye türkiye geldiği zaman ilçe ve mahelle açık olur
        /// </summary>
        public int CountryId { get; set; }
        #endregion


        #region Navigation Properties
        public List<City> Cities { get; set; }



        public List<PaymentType> PaymentTypes { get; set; }

        public CustomerUser CustomerUserDetail { get; set; }

        public Common.DataTransferObjects.DiscountCoupon DiscountCoupon { get; set; }

        public Configuration Configuration { get; set; }

        public List<ShoppingCartDiscounted> ShoppingCartDiscounteds { get; set; }
        #endregion
    }
}
