﻿using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.ViewModels.Orders
{
    public class Create : Response
    {
        /// <summary>
        /// Kredi kartlı ile mi ödenmiş
        /// </summary>
        public bool IsOnlinePayment { get; set; }
        /// <summary>
        /// ThreeD mi?
        /// </summary>
        public bool IsThreeD { get; set; }

        /// <summary>
        /// ThreeD Redirect olacaksa 
        /// </summary>
        public bool ThreeDRedirect { get; set; }

        /// <summary>
        /// ThreeD'den döenen html 
        /// </summary>
        public string ThreeDResponse { get; set; }

        public bool AllowFrame { get; set; }
    }
}
