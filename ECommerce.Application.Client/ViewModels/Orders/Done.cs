﻿using ECommerce.Application.Client.ViewModels.Base;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Client.ViewModels.Orders
{
    public class Done : PageViewModelBase
    {
        #region Properties     
        public string PaymentTypeId { get; set; }

        public string DescriptionError { get; set; }

        public int OrderId { get; set; }

        public decimal Amount { get; set; }

        public bool OnlinePaymentPaid { get; set; }

        public bool Status { get; set; }

        public DataResponse<string> GoogleAdsDone { get; set; }

        public DataResponse<string> FacebookPixelDone { get; set; }

        public DataResponse<string> GoogleAnalyticsDone { get; set; }
        #endregion
    }
}
