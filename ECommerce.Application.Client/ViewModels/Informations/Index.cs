﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;

namespace ECommerce.Application.Client.ViewModels.Informations
{
    public class Index : PageViewModelBase
    {
        #region Properties
        public Information Information { get; set; }
        #endregion
    }
}
