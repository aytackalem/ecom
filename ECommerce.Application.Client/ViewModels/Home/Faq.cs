﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.Home
{
    public class Faq : PageViewModelBase
    {
        public List<FrequentlyAskedQuestion> FrequentlyAskedQuestions { get; set; }

    }
}
