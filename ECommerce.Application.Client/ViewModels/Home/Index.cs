﻿using ECommerce.Application.Client.ViewModels.Base;
using ECommerce.Application.Common.DataTransferObjects;

namespace ECommerce.Application.Client.ViewModels.Home
{
    public class Index : PageViewModelBase
    {
        #region Properties
        public bool OpenResetPassword { get; set; }

        public string ResetPasswordToken { get; set; }
        #endregion

        #region Navigation Properties
        public Showcase Showcase { get; set; }
        #endregion
    }
}
