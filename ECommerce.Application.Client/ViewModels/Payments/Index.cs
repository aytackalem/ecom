﻿using ECommerce.Application.Common.ViewModels.Base;

namespace ECommerce.Application.Client.ViewModels.Payments
{
    public class Index : ViewModelBase
    {
        #region Properties
        public string DoneUrl { get; set; }
        public bool AllowFrame { get; set; }
        #endregion
    }
}
