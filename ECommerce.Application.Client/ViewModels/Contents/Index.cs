﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;

namespace ECommerce.Application.Client.ViewModels.Contents
{
    public class Index : PageViewModelBase
    {
        #region Navigation Properties
        public Content Content { get; set; }
        #endregion
    }
}
