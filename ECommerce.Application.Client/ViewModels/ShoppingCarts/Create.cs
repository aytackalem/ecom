﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Client.ViewModels.ShoppingCarts
{
    public class Create : Response
    {
        public int Count { get; set; }

        public List<ShoppingCartItem> ShoppingCartItems { get; set; }
    }
}
