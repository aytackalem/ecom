﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Client.ViewModels.ShoppingCarts
{
    public class Index : PageViewModelBase
    {
        #region Constructors
        public Index()
        {
            #region Navigation Properties
            this.ShoppingCartItems = new List<ShoppingCartItem>();
            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// Toplam Liste Sepet Tutarı
        /// </summary>
        public decimal TotalListUnitPrice => this.ShoppingCartItems.Sum(sci => sci.Quantity * sci.ProductInformation.ProductInformationPrice.ListUnitPrice);

        /// <summary>
        /// Toplam Sepet Tutarı
        /// </summary>
        public decimal TotalUnitPrice => this.ShoppingCartItems.Sum(sci => sci.ProductInformation.ProductInformationPrice.TotalUnitPrice);

        public decimal DiscountCouponDiscount
        {
            get
            {
                var discountCouponDiscount = 0M;

                if (this.DiscountCoupon != null && this.ShoppingCartItemsCount > 0 && this.TotalUnitPrice > this.DiscountCoupon.DisountCouponSetting.Limit)
                {
                    discountCouponDiscount = this.DiscountCoupon.DisountCouponSetting.Discount;

                    if (this.DiscountCoupon.DisountCouponSetting.DiscountIsRate)
                        discountCouponDiscount = this.TotalUnitPrice * this.DiscountCoupon.DisountCouponSetting.Discount;
                }

                return discountCouponDiscount;
            }
        }

        public decimal ShoppingCartDiscount
        {
            get
            {
                var shoppingCartDiscount = 0M;

                if (ShoppingCartDiscounteds.Count > 0)
                {

                    var shoppingCartDiscounted = ShoppingCartDiscounteds.Where(x=> x.ShoppingCartDiscountedSetting.Limit <= this.TotalUnitPrice).OrderByDescending(x=>x.ShoppingCartDiscountedSetting.Limit).FirstOrDefault();

                    if (shoppingCartDiscounted != null && this.ShoppingCartItemsCount > 0)
                    {
                        shoppingCartDiscount = shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;

                        if (shoppingCartDiscounted.ShoppingCartDiscountedSetting.DiscountIsRate)
                            shoppingCartDiscount = this.TotalUnitPrice * shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;
                    }
                }

                return shoppingCartDiscount;
            }
        }


        public decimal Amount => this.TotalUnitPrice - (this.DiscountCouponDiscount + this.ShoppingCartDiscount);
        #endregion

        #region Navigation Properties

        public Configuration Configuration { get; set; }

        public DiscountCoupon DiscountCoupon { get; set; }

        public List<ShoppingCartDiscountedProduct> ShoppingCartDiscountedProducts { get; set; }

        public List<ShoppingCartDiscounted> ShoppingCartDiscounteds { get; set; }
        #endregion
    }
}
