﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.ViewModels.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Client.ViewModels.GeolocationContents
{
    public class Index : PageViewModelBase
    {
        #region Constructors
        public Index()
        {
            this.Cities = new();
            this.Districts = new();
            this.Neighborhoods = new();
        }
        #endregion

        #region Properties
        public string Keyword { get; set; }

        public string KeywordUrl { get; set; }

        public string Content { get; set; }
        #endregion

        #region Navigation Properties
        public List<OldCity> Cities { get; set; }

        public OldCity City { get; set; }

        public List<OldDistrict> Districts { get; set; }

        public OldDistrict District { get; set; }

        public List<OldNeighborhood> Neighborhoods { get; set; }

        public OldNeighborhood Neighborhood { get; set; }
        #endregion
    }
}
