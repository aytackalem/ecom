﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Helpers
{
    public class CookieCompanyFinder : ICompanyFinder
    {
        #region Fields
        private int? _companyId = null;

        private readonly ICookieHelper _cookieHelper;
        #endregion

        #region Constructors
        public CookieCompanyFinder(ICookieHelper cookieHelper)
        {
            #region Fields
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            string name = "LoginV4";

            if (!_companyId.HasValue && this._cookieHelper.Exist(name))
            {
                var cookie = this._cookieHelper.Read(name);
                var companyId = cookie.FirstOrDefault(x => x.Key == "CompanyId")?.Value;
                this._companyId = Convert.ToInt32(companyId);
            }

            return _companyId.Value;
        }

        public void Set(int id)
        {
            this._companyId = id;
        }
        #endregion
    }
}
