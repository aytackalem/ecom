﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Graph;
using System.Linq;

namespace ECommerce.Persistence.Panel.Helpers
{
    public class ClaimDbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName = string.Empty;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ClaimDbNameFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public void Set(string dbName)
        {
            this._dbName = dbName;         
        }

        public string FindName()
        {
            if (this._dbName == string.Empty)
            {
                var user = this._httpContextAccessor.HttpContext.User;
                if (user != null)
                {
                    var claim = user.Claims.FirstOrDefault(c => c.Type == "brand");
                    if (claim != null)
                    {
                        this._dbName = claim.Value;
                    }
                }
            }

            return this._dbName;
        }
        #endregion
    }
}
