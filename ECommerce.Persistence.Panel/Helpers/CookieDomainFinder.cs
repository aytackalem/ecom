﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Helpers
{
    public class CookieDomainFinder : IDomainFinder
    {
        #region Fields

        private int? _domainId = null;

        private readonly ICookieHelper _cookieHelper;
        #endregion

        #region Constructors
        public CookieDomainFinder(ICookieHelper cookieHelper)
        {
            #region Fields
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            string name = "LoginV4";

            if (!_domainId.HasValue && this._cookieHelper.Exist(name))
            {
                var cookie = this._cookieHelper.Read(name);
                var domainId = cookie.FirstOrDefault(x => x.Key == "DomainId")?.Value;
                this._domainId = Convert.ToInt32(domainId);                
            }
            return this._domainId.Value;
        }

        public void Set(int id)
        {
            this._domainId = id;
        }
        #endregion
    }
}
