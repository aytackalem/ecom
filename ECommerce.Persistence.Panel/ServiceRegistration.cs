﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects.Notifications;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Persistence.Common.ECommerceCatalog;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Panel.Caching;
using ECommerce.Persistence.Panel.Helpers;
using ECommerce.Persistence.Panel.Mappings;
using ECommerce.Persistence.Panel.Services;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddAutoMapper(o =>
            {
                o.AddProfile<PanelProfile>();
            });

            serviceCollection.AddTransient<INotificationService, NotificationService>();

            serviceCollection.AddSingleton<IDbService, DbService>();

            serviceCollection.AddSingleton<IIdentityService, IdentityService>();

            #region Tenant Dependencies
            serviceCollection.AddScoped<ITenantFinder, ClaimTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, CookieDomainFinder>();

            serviceCollection.AddScoped<ICompanyFinder, CookieCompanyFinder>();

            serviceCollection.AddScoped<IDbNameFinder, ClaimDbNameFinder>();
            #endregion

            #region Config And Setting Dependencies
            serviceCollection.AddTransient<ISettingService, SettingService>();
            #endregion

            #region DbContext Dependencies
     
            serviceCollection.AddDbContext<ApplicationDbContext>();
            #endregion

            serviceCollection.AddTransient<IImageHelper, ImageHelper>();

            serviceCollection.AddTransient<IProductGenericPropertyService, ProductGenericPropertyService>();

            serviceCollection.AddTransient<IWholesaleOrderService, WholesaleOrderService>();

            serviceCollection.AddTransient<IWholesaleOrderTypeService, WholesaleOrderTypeService>();

            serviceCollection.AddTransient<IReceiptService, ReceiptService>();

            serviceCollection.AddTransient<IRandomHelper, RandomHelper>();

            serviceCollection.AddTransient<IStocktakingService, StocktakingService>();

            serviceCollection.AddTransient<IStocktakingTypeService, StocktakingTypeService>();

            serviceCollection.AddTransient<IProductInformationStockService, ProductInformationStockService>();

            serviceCollection.AddTransient<IProductInformationProductService, ProductInformationProductService>();

            serviceCollection.AddTransient<IProductInformationPriceService, ProductInformationPriceService>();

            serviceCollection.AddTransient<ICaptchaService, CaptchaService>();

            serviceCollection.AddTransient<IFeedService, FeedService>();

            serviceCollection.AddTransient<IFeedTemplateService, FeedTemplateService>();

            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddTransient<IMarketplaceBrandService, MarketplaceBrandService>();

            serviceCollection.AddTransient<IExpenseService, ExpenseService>();

            serviceCollection.AddTransient<IExpenseSourceService, ExpenseSourceService>();

            serviceCollection.AddTransient<ICustomerService, CustomerService>();

            serviceCollection.AddTransient<IBrandService, BrandService>();

            serviceCollection.AddTransient<ICategoryService, CategoryService>();

            serviceCollection.AddTransient<IContentService, ContentService>();

            serviceCollection.AddTransient<IProductCategorizationService, ProductCategorizationService>();

            serviceCollection.AddTransient<IInformationService, InformationService>();

            serviceCollection.AddTransient<ILabelService, LabelService>();

            serviceCollection.AddTransient<ILanguageService, LanguageService>();

            serviceCollection.AddTransient<ISupplierService, SupplierService>();

            serviceCollection.AddTransient<IProductSourceDomainService, ProductSourceDomainService>();

            serviceCollection.AddTransient<IVariantService, VariantService>();

            serviceCollection.AddTransient<IMarketplaceVariantMappingService, MarketplaceVariantMappingService>();

            serviceCollection.AddTransient<IMarketplaceVariantValueService, MarketplaceVariantValueService>();

            serviceCollection.AddTransient<IVariantValueService, VariantValueService>();

            serviceCollection.AddTransient<IProductService, ProductService>();

            serviceCollection.AddTransient<IProductMarketplaceService, ProductMarketplaceService>();

            serviceCollection.AddTransient<IProductInformationMarketplaceService, ProductInformationMarketplaceService>();

            serviceCollection.AddTransient<ICurrencyService, CurrencyService>();

            serviceCollection.AddTransient<IProductInformationCommentService, ProductInformationCommentService>();

            serviceCollection.AddTransient<IDiscountCouponService, DiscountCouponService>();

            serviceCollection.AddTransient<IApplicationService, ApplicationService>();

            serviceCollection.AddTransient<IGetXPayYSettingService, GetXPayYSettingService>();

            serviceCollection.AddTransient<IEntireDiscountService, EntireDiscountService>();

            serviceCollection.AddTransient<IFrequentlyAskedQuestionService, FrequentlyAskedQuestionService>();

            serviceCollection.AddTransient<IContactService, ContactService>();

            serviceCollection.AddTransient<IGoogleConfigrationService, GoogleConfigrationService>();

            serviceCollection.AddTransient<IFacebookConfigrationService, FacebookConfigrationService>();

            serviceCollection.AddTransient<ICategoryDiscountSettingService, CategoryDiscountSettingService>();

            serviceCollection.AddTransient<IBrandDiscountSettingService, BrandDiscountSettingService>();

            serviceCollection.AddTransient<IBankService, BankService>();

            serviceCollection.AddTransient<IAccountingCompanyService, AccountingCompanyService>();

            serviceCollection.AddTransient<IPosService, Services.PosService>();

            serviceCollection.AddTransient<IPosConfigurationService, PosConfigurationService>();

            serviceCollection.AddTransient<IPaymentTypeService, PaymentTypeService>();

            serviceCollection.AddTransient<IShoppingCartDiscountedProductService, ShoppingCartDiscountedProductService>();

            serviceCollection.AddTransient<ISmsProviderService, Services.SmsProviderService>();

            serviceCollection.AddTransient<IEmailProviderService, EmailProviderService>();

            serviceCollection.AddTransient<IShipmentCompanyService, ShipmentCompanyService>();

            serviceCollection.AddTransient<IShipmentCompanyCompanyService, ShipmentCompanyCompanyService>();

            serviceCollection.AddTransient<ISmsProviderCompanyService, SmsProviderCompanyService>();

            serviceCollection.AddTransient<IEmailProviderCompanyService, EmailProviderCompanyService>();

            serviceCollection.AddTransient<IPropertyService, PropertyService>();

            serviceCollection.AddTransient<IPropertyValueService, PropertyValueService>();

            serviceCollection.AddTransient<IMoneyPointSettingService, MoneyPointSettingService>();

            serviceCollection.AddTransient<IShowcaseService, ShowcaseService>();

            serviceCollection.AddTransient<IOrderService, OrderService>();

            serviceCollection.AddTransient<IReportService, ReportService>();

            serviceCollection.AddTransient<IWidgetService, WidgetService>();

            serviceCollection.AddTransient<ICompanyService, CompanyService>();

            serviceCollection.AddTransient<IDomainService, DomainService>();

            serviceCollection.AddTransient<ICountryService, CountryService>();

            serviceCollection.AddTransient<IOrderSourceService, OrderSourceService>();

            serviceCollection.AddTransient<IProductInformationService, ProductInformationService>();

            serviceCollection.AddTransient<IOrderTypeService, OrderTypeService>();

            serviceCollection.AddTransient<IDistrictService, DistrictService>();

            serviceCollection.AddTransient<ICityService, CityService>();

            serviceCollection.AddTransient<INeighborhoodService, NeighborhoodService>();

            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();

            serviceCollection.AddTransient<IMarketplaceCategoryMappingService, MarketplaceCategoryMappingService>();

            serviceCollection.AddTransient<IMarketplaceBrandMappingService, MarketplaceBrandMappingService>();

            serviceCollection.AddTransient<IMarketplaceCompanyService, MarketplaceCompanyService>();

            serviceCollection.AddTransient<IStockService, StockService>();

            serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<IMarketplacePriceService, MarketplacePriceService>();

            serviceCollection.AddTransient<ICookieHelper, CookieHelper>();

            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();

            serviceCollection.AddTransient<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddTransient<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<IEmailService, EmailService>();

            serviceCollection.AddTransient<Application.Common.Interfaces.Marketplaces.IMarketplaceService, ECommerce.Persistence.Common.Marketplace.MarketplaceService>();

            serviceCollection.AddTransient<IECommerceBrandService, ECommerceBrandService>();

            serviceCollection.AddTransient<IECommerceCategoryService, ECommerceCategoryService>();

            serviceCollection.AddTransient<IECommerceVariantService, ECommerceVariantService>();

            serviceCollection.AddTransient<IECommerceVariantValueService, ECommerceVariantValueService>();

           
        }
        #endregion
    }
}
