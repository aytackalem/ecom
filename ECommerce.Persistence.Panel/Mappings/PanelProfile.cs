﻿using AutoMapper;
using ECommerce.Domain.Entities;

namespace ECommerce.Persistence.Panel.Mappings
{
    public class PanelProfile : Profile
    {
        #region Constructors
        public PanelProfile()
        {
            CreateMap<Product, Application.Panel.DataTransferObjects.Product>().ReverseMap();
            CreateMap<ProductTranslation, Application.Panel.DataTransferObjects.ProductTranslation>().ReverseMap();

            CreateMap<ProductInformation, Application.Panel.DataTransferObjects.ProductInformation>().ReverseMap();
            CreateMap<ProductInformationTranslation, Application.Panel.DataTransferObjects.ProductInformationTranslation>().ReverseMap();
            CreateMap<ProductInformationPrice, Application.Panel.DataTransferObjects.ProductInformationPrice>().ReverseMap();

            CreateMap<ProductInformationMarketplace, Application.Panel.DataTransferObjects.ProductInformationMarketplace>().ReverseMap();
            CreateMap<ProductInformationMarketplaceVariantValue, Application.Panel.DataTransferObjects.ProductInformationMarketplaceVariantValue>().ReverseMap();

            CreateMap<ProductInformationVariant, Application.Panel.DataTransferObjects.ProductInformationVariant>().ForMember(dest => dest.VariantValue, act => act.Ignore()).ReverseMap();

            CreateMap<ProductMarketplace, Application.Panel.DataTransferObjects.ProductMarketplace>().ReverseMap();

            CreateMap<VariantValue, Application.Panel.DataTransferObjects.VariantValue>().ReverseMap();

            CreateMap<MarketplaceBrandMapping, Application.Panel.DataTransferObjects.MarketplaceBrandMapping>().ReverseMap();
            CreateMap<MarketplaceCategoryMapping, Application.Panel.DataTransferObjects.MarketplaceCategoryMapping>().ReverseMap();
        }
        #endregion
    }
}
