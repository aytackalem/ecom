﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class PosService : IPosService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public PosService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Pos> Create(Pos dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Pos>>((response) =>
            {
                var hasAnyDefault = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Any(p => p.Default && dto.Default);

                if (hasAnyDefault)
                {
                    response.Success = false;
                    response.Message = "Daha önce varsayılan bir pos oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var hasAnySameName = this
                     ._unitOfWork
                     .PosRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isim ile daha önce bir pos oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var hasAnySameId = this
                     ._unitOfWork
                     .PosRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameId)
                {
                    response.Success = false;
                    response.Message = "Bu no ile daha önce bir pos oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Pos
                {
                    Id = dto.Id,
                    Name = dto.Name,
                    Active = dto.Active,
                    Default = dto.Default
                };

                var created = this
                    ._unitOfWork
                    .PosRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Pos oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Pos> Read(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Pos>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .PosRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Pos bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Pos
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Name = entity.Name,
                    Default = entity.Default
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Pos> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Pos>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Pos
                    {
                        Active = b.Active,
                        Id = b.Id,
                        Name = b.Name,
                        Default = b.Default
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Pos dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnyDefault = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Any(p => p.Default && dto.Default && p.Id != dto.Id);

                if (hasAnyDefault)
                {
                    response.Success = false;
                    response.Message = "Daha önce varsayılan bir pos oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var hasAnySameName = this
                     ._unitOfWork
                     .PosRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name && b.Id != dto.Id);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir pos oluşturulduğundan pos güncellenemez.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .PosRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Pos bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;
                entity.Active = dto.Active;
                entity.Default = dto.Default;

                var updated = this
                    ._unitOfWork
                    .PosRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Pos güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
