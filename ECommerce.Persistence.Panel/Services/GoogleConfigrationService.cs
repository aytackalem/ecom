﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class GoogleConfigrationService : IGoogleConfigrationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public GoogleConfigrationService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<GoogleConfigration> Create(GoogleConfigration dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<GoogleConfigration>>((response) =>
            {



                var entity = new Domain.Entities.GoogleConfiguration
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    Code = dto.Code,
                    Key = dto.Key,
                    Name = dto.Name
                };

                var created = this
                    ._unitOfWork.GoogleConfigrationRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;



                response.Data = dto;
                response.Success = true;
                response.Message = "Google Konfig oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<GoogleConfigration> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<GoogleConfigration>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .GoogleConfigrationRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Google Konfig bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new GoogleConfigration
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Name = entity.Name,
                    Key = entity.Key,
                    Code = entity.Code
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<GoogleConfigration> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<GoogleConfigration>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .GoogleConfigrationRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .GoogleConfigrationRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new GoogleConfigration
                    {
                        Id = c.Id,
                        Active = c.Active,
                        Code = c.Code,
                        Key = c.Key,
                        Name = c.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .GoogleConfigrationRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c.Code
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(GoogleConfigration dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .GoogleConfigrationRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"İletişim bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.Code = dto.Code;
              

                var updated = this
                    ._unitOfWork.GoogleConfigrationRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Google Konfig  güncellendi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
