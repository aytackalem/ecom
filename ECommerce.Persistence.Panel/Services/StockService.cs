﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class StockService : IStockService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;

        private readonly IProductInformationStockService _productInformationStockService;
        #endregion

        #region Constructors
        public StockService(IUnitOfWork unitOfWork, ISettingService settingService, IProductInformationStockService productInformationStockService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._productInformationStockService = productInformationStockService;
            #endregion
        }
        #endregion

        #region Methods
        public PagedResponse<ProductInformation> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {

                var iQueryable = GetProducts(pagedRequest.Search);
                if (!iQueryable.Success)
                {
                    response.Message = iQueryable.Message;
                    return;
                }


                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable.Data.Count();
                response.Data = iQueryable.Data
                  .OrderByDescending(x => x.Id)
                  .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                  .Take(pagedRequest.PageRecordsCount)
                  .Select(v => new ProductInformation
                  {
                      Id = v.Id,
                      StockCode=v.StockCode,
                      Barcode=v.Barcode,
                      Stock = v.Stock,
                      VirtualStock = v.VirtualStock,
                      ProductInformationPhotos = v.ProductInformationPhotos.OrderBy(x => x.DisplayOrder).Select(pip => new ProductInformationPhoto
                      {
                          FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                      }).ToList(),
                      ProductInformationName = $"{v.ProductInformationTranslations.FirstOrDefault(x => x.LanguageId == "TR").Name} {v.ProductInformationTranslations.FirstOrDefault(x => x.LanguageId == "TR").VariantValuesDescription}"
                  }).ToList();

                response.Success = true;
            }, (response, exception) => { });
        }


        public DataResponse<ProductInformation> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {
                var productInformation = this
                   ._unitOfWork
                   .ProductInformationRepository
                   .DbSet()
                   .Include(v => v.ProductInformationPhotos)
                   .Include(v => v.ProductInformationTranslations)
                   .FirstOrDefault(x => x.Id == id);

                response.Data = new ProductInformation
                {
                    Stock = productInformation.Stock,
                    Id = productInformation.Id,
                    VirtualStock = productInformation.VirtualStock,
                    ProductInformationPhotos = productInformation.ProductInformationPhotos.OrderBy(x => x.DisplayOrder).Select(pip => new ProductInformationPhoto
                    {
                        FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                    }).ToList(),
                    ProductInformationName = $"{productInformation.ProductInformationTranslations.FirstOrDefault(x => x.LanguageId == "TR").Name} {productInformation.ProductInformationTranslations.FirstOrDefault(x => x.LanguageId == "TR").VariantValuesDescription}"
                };

                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(ProductInformation dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                if (dto.VirtualStock < 0)
                {
                    response.Message = $"Sanal stoğu 0'dan aşağı giremezsiniz.";
                    response.Success = false;
                    return;
                }

                var product = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Include(x => x.ProductInformations)
                    .FirstOrDefault(x => x.Id == entity.ProductId);

                foreach (var pLoop in product.ProductInformations)
                {
                    if (pLoop.Id == entity.Id || dto.AllVariantStock)
                    {
                        pLoop.Stock = dto.Stock;
                        pLoop.VirtualStock = dto.VirtualStock;
                        this._productInformationStockService.MakeDirtyStock(pLoop.Id);
                    }
                }

                var updated = this
                    ._unitOfWork
                    .ProductRepository
                    .Update(product);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Stok güncellendi.";
            }, (response, exception) => { });
        }


        public DataResponse<ProductInformation> Create(ProductInformation dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {
                response.Success = true;
                response.Message = "Stok eklendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Helpers
        private DataResponse<IQueryable<Domain.Entities.ProductInformation>> GetProducts(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<IQueryable<Domain.Entities.ProductInformation>>>((response) =>
            {
                var parameters = search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(x => !x.Product.IsOnlyHidden);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(pi  => pi.ProductInformationTranslations.Any(pit => pit.Name.Contains(value)));
                        }
                    }
                    else if (pLoop.StartsWith("StockCode:"))
                    {
                        var value = pLoop.Replace("StockCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(pi => pi.StockCode.Contains(value));
                    }
                    else if (pLoop.StartsWith("Barcode:"))
                    {
                        var value = pLoop.Replace("Barcode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(pi => pi.Barcode.Contains(value));
                    }
                    else if (pLoop.StartsWith("ModelCode:"))
                    {
                        var value = pLoop.Replace("ModelCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Product.SellerCode.Contains(value));
                    }
                    else if (pLoop.StartsWith("BrandId:"))
                    {
                        var value = pLoop.Replace("BrandId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.Product.BrandId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("OnlyStockAvailable:"))
                    {
                        var value = pLoop.Replace("OnlyStockAvailable:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(pi => pi.Stock > 0);
                    }
                    else if (pLoop.StartsWith("OnlyNoPhoto:"))
                    {
                        var value = pLoop.Replace("OnlyNoPhoto:", "");
                        if (value == "true")
                            iQueryable = iQueryable.Where(pi => !pi.ProductInformationPhotos.Any());
                    }
              
                }

                response.Data = iQueryable;
                response.Success = true;
            });
        }

        #endregion
    }
}
