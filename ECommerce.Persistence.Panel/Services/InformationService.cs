﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class InformationService : IInformationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public InformationService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Information> Create(Information dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Information>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .InformationRepository
                     .DbSet()
                     .Any(c => c.InformationTranslations.Any(ct => dto.InformationTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir haber oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Information
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    InformationTranslations = dto.InformationTranslations.Select(ct => new Domain.Entities.InformationTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                        InformationTranslationContent = new Domain.Entities.InformationTranslationContent
                        {
                            Html = ct.InformationTranslationContent.Html,
                            CreatedDate = DateTime.Now,
                            
                        },
                        InformationSeoTranslation = new Domain.Entities.InformationSeoTranslation
                        {
                            CreatedDate = DateTime.Now,
                            MetaDescription = ct.InformationSeoTranslation.MetaDescription,
                            Title = ct.InformationSeoTranslation.Title,
                            MetaKeywords = ct.InformationSeoTranslation.MetaKeywords
                        }
                    }).ToList()
                };

                var created = this
                    ._unitOfWork.InformationRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;


                foreach (var informationTranslation in entity.InformationTranslations)
                {
                    informationTranslation.Url = $"/{informationTranslation.Header.GenerateUrl()}-{entity.Id}-i";
                    this._unitOfWork.InformationTranslationRepository.Update(informationTranslation);
                }

                response.Data = dto;
                response.Success = true;
                response.Message = "Haber oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Information> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Information>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .InformationRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Haber bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Information
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    InformationTranslations = entity.InformationTranslations.Select(ct => new InformationTranslation
                    {
                        Url = ct.Url,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                        InformationTranslationContent = new InformationTranslationContent
                        {

                            Html = ct.InformationTranslationContent.Html
                        },
                        InformationSeoTranslation = new InformationSeoTranslation
                        {
                            MetaDescription = ct.InformationSeoTranslation.MetaDescription,
                            Title = ct.InformationSeoTranslation.Title,
                            MetaKeywords = ct.InformationSeoTranslation.MetaKeywords
                        }
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Information> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Information>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .InformationRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .InformationRepository
                    .DbSet()
                    .Include(c => c.InformationTranslations)
                    .Where(c => string.IsNullOrEmpty(pagedRequest.Search) || c.InformationTranslations.Any(ct => ct.Header.Contains(pagedRequest.Search)))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new Information
                    {
                        Id = c.Id,
                        Active = c.Active,
                        InformationTranslations = c.InformationTranslations.Select(ct => new InformationTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Header = ct.Header,
                            Url = ct.Url
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .InformationRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c
                            .InformationTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Header
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Information dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .InformationRepository
                     .DbSet()
                     .Any(c => c.InformationTranslations.Any(ct => ct.InformationId != dto.Id && dto.InformationTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir haber oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }


                var entity = this
                    ._unitOfWork
                    .InformationRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Haber bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                foreach (var InformationTranslation in dto.InformationTranslations)
                {
                    var _InformationTranslation = entity.InformationTranslations.FirstOrDefault(x => x.LanguageId == InformationTranslation.LanguageId);

                    if (_InformationTranslation != null)
                    {
                        _InformationTranslation.Header = InformationTranslation.Header;
                        _InformationTranslation.InformationTranslationContent.Html = InformationTranslation.InformationTranslationContent.Html;
                        _InformationTranslation.InformationSeoTranslation.Title = InformationTranslation.InformationSeoTranslation.Title;
                        _InformationTranslation.InformationSeoTranslation.MetaDescription = InformationTranslation.InformationSeoTranslation.MetaDescription;
                        _InformationTranslation.InformationSeoTranslation.MetaKeywords = InformationTranslation.InformationSeoTranslation.MetaKeywords;
                    }
                }

                var updated = this
                    ._unitOfWork.InformationRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Haber güncellendi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
