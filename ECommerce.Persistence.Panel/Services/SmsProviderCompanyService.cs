﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class SmsProviderCompanyService : ISmsProviderCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public SmsProviderCompanyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<SmsProviderCompany> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<SmsProviderCompany>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Include(x => x.SmsProvider)
                    .Include(x => x.SmsProviderConfigurations)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Sms firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new SmsProviderCompany
                {
                    Id = entity.Id,
                    Name = entity.SmsProvider.Name,
                    SmsProviderConfigurations = entity.SmsProviderConfigurations.Select(x => new SmsProviderConfiguration
                    {
                        Id = x.Id,
                        Key = x.Key,
                        Value = x.Value
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<SmsProviderCompany> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<SmsProviderCompany>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new SmsProviderCompany
                    {
                        Id = b.Id,
                        Name = b.SmsProvider.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(SmsProviderCompany dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                  ._unitOfWork
                  .SmsProviderCompanyRepository
                  .DbSet()
                  .Include(x => x.SmsProviderConfigurations)
                  .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Sms firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                dto.SmsProviderConfigurations.ForEach(mc =>
                {
                    var smsProviderConfigurations = entity.SmsProviderConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                    if (smsProviderConfigurations != null)
                    {
                        smsProviderConfigurations.Value = mc.Value;
                    }
                });

                var updated = this
                    ._unitOfWork.SmsProviderCompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Sms firması güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
