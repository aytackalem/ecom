﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Company = ECommerce.Application.Panel.DataTransferObjects.Company;

namespace ECommerce.Persistence.Panel.Services
{
    public class DomainService : IDomainService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;


        private readonly ICacheHandler _cacheHandler;

        private readonly IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public DomainService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler, IDomainFinder domainFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<List<Application.Panel.DataTransferObjects.Domain>> ReadAll()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Panel.DataTransferObjects.Domain>>>((response) =>
            {

                var domains = this
                ._unitOfWork
                .DomainRepository
                .DbSet()
                .Select(x => new Application.Panel.DataTransferObjects.Domain
                {
                    Id = x.Id,
                    Name = x.Name,
                    Companies = x.Companies.Select(co => new Company
                    {
                        Id = co.Id,
                        Name = co.Name,
                        FileName = co.FileName
                    }).ToList()
                })
                .ToList();

                if (domains.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Domain bulunamadı";
                    return;
                }

                response.Data = domains;
                response.Success = true;
            }, (response, exception) => { });
        }



        public DataResponse<List<Application.Panel.DataTransferObjects.Company>> Companies()
        {
            var domainId = _domainFinder.FindId();

            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<Company>>>((response) =>
                {

                    var companies = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Select(x => new Company
                    {
                        Id = x.Id,
                        Name = x.Name,
                        FileName = x.FileName
                    })
                    .ToList();

                    if (companies.Count == 0)
                    {
                        response.Success = false;
                        response.Message = "Firma bulunamadı";
                        return;
                    }
                    response.Data = companies;
                    response.Success = true;
                }, (response, exception) => { });
            }, $"{string.Format(CacheKey.Company, domainId)}", 10);
        }
        #endregion
    }
}
