﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceCategoryService : IMarketplaceCategoryService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceCategoryService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(string marketplaceId, int take, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                //response.Data = this
                //     ._unitOfWork
                //     .MarketplaceCategoryRepository
                //     .DbSet()
                //     .Where(mc => mc.MarketplaceId == marketplaceId && mc.Name.Contains(q))
                //     .OrderByDescending(mc => mc.Name.Equals(q))
                //     .Take(take)
                //     .Select(mc => new KeyValue<string, string>
                //     {
                //         Key = mc.Id,
                //         Value = mc.Name
                //     })
                //     .ToList();
                response.Success = response.Data?.Count > 0;
            });
        }
        #endregion
    }
}