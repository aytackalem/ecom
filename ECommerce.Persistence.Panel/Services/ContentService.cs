﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class ContentService : IContentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ContentService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Content> Create(Content dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Content>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .ContentRepository
                     .DbSet()
                     .Any(c => c.ContentTranslations.Any(ct => dto.ContentTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir içerik oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Content
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    ContentTranslations = dto.ContentTranslations.Select(ct => new Domain.Entities.ContentTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                        ContentTranslationContent = new Domain.Entities.ContentTranslationContent
                        {
                            Html = ct.ContentTranslationContent.Html,
                            CreatedDate = DateTime.Now,
                            
                        },
                        ContentSeoTranslation = new Domain.Entities.ContentSeoTranslation
                        {
                            Title = ct.ContentSeoTranslation.Title,
                            MetaKeywords = ct.ContentSeoTranslation.MetaKeywords,
                            MetaDescription = ct.ContentSeoTranslation.MetaDescription,
                            CreatedDate = DateTime.Now
                        }
                    }).ToList()
                };

                var created = this
                    ._unitOfWork.ContentRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;


                foreach (var contentTranslation in entity.ContentTranslations)
                {
                    contentTranslation.Url = $"/{contentTranslation.Header.GenerateUrl()}-{entity.Id}-co";
                    this._unitOfWork.ContentTranslationRepository.Update(contentTranslation);
                }

                response.Data = dto;
                response.Success = true;
                response.Message = "İçerik oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Content> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Content>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ContentRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"İçerik bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Content
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    ContentTranslations = entity.ContentTranslations.Select(ct => new ContentTranslation
                    {

                        Url = ct.Url,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                        ContentTranslationContent = new ContentTranslationContent
                        {

                            Html = ct.ContentTranslationContent.Html
                        },
                        ContentSeoTranslation = new ContentSeoTranslation
                        {
                            Title = ct.ContentSeoTranslation.Title,
                            MetaKeywords = ct.ContentSeoTranslation.MetaKeywords,
                            MetaDescription = ct.ContentSeoTranslation.MetaDescription
                        }
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Content> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Content>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ContentRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .ContentRepository
                    .DbSet()
                    .Include(c => c.ContentTranslations)
                    .Where(c => string.IsNullOrEmpty(pagedRequest.Search) || c.ContentTranslations.Any(ct => ct.Header.Contains(pagedRequest.Search)))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new Content
                    {

                        Id = c.Id,
                        Active = c.Active,
                        ContentTranslations = c.ContentTranslations.Select(ct => new ContentTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Header = ct.Header,
                            Url = ct.Url
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ContentRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c
                            .ContentTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Header
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Content dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .ContentRepository
                     .DbSet()
                     .Any(c => c.ContentTranslations.Any(ct => ct.ContentId != dto.Id && dto.ContentTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir içerik oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }


                var entity = this
                    ._unitOfWork
                    .ContentRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"İçerik bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                foreach (var contentTranslation in dto.ContentTranslations)
                {
                    var _contentTranslation = entity.ContentTranslations.FirstOrDefault(x => x.LanguageId == contentTranslation.LanguageId);

                    if (_contentTranslation != null)
                    {
                        _contentTranslation.Header = contentTranslation.Header;
                        _contentTranslation.ContentTranslationContent.Html = contentTranslation.ContentTranslationContent.Html;
                        _contentTranslation.ContentSeoTranslation.Title = contentTranslation.ContentSeoTranslation.Title;
                        _contentTranslation.ContentSeoTranslation.MetaDescription = contentTranslation.ContentSeoTranslation.MetaDescription;
                        _contentTranslation.ContentSeoTranslation.MetaKeywords = contentTranslation.ContentSeoTranslation.MetaKeywords;
                    }
                }

                var updated = this
                    ._unitOfWork.ContentRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "İçerik güncellendi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
