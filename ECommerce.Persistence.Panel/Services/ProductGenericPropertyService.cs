﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductGenericPropertyService : IProductGenericPropertyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ProductGenericPropertyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<string>> ReadAsValue(string name)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<string>>>((response) =>
            {

                response.Data = this
                    ._unitOfWork
                    .ProductGenericPropertyRepository
                    .DbSet()
                    .Where(pgp => pgp.Name == name)
                    .GroupBy(pgp => pgp.Value)
                    .Select(gpgp => gpgp.Key)
                    .ToList();
                response.Success = true;

            }, (response, exception) => { });
        }
        #endregion
    }
}