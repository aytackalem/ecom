﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Products;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductInformationCommentService : IProductInformationCommentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ProductInformationCommentService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }

        public DataResponse<ProductInformationComment> Create(ProductInformationComment dto)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Methods
        public DataResponse<ProductInformationComment> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductInformationComment>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .DbSet()
                    .Include(pc => pc.ProductInformation)
                    .FirstOrDefault(pc => pc.Id == id);

                if (entity == null)
                {
                    response.Message = $"Ürün yorumu bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new ProductInformationComment
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Approved = entity.Approved,
                    Comment = entity.Comment,
                    ProductInformationId = entity.ProductInformationId,
                    ProductInformation = new ProductInformation
                    {
                        ProductInformationTranslations = entity
                            .ProductInformation
                            .ProductInformationTranslations
                            .Select(pt => new ProductInformationTranslation
                            {
                                Name = pt.Name
                            })
                            .ToList()
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<ProductInformationComment> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformationComment>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .DbSet()
                    .Where(pc => pagedRequest.Active == null || pc.Active == pagedRequest.Active.Value)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .DbSet()
                    .Where(pc => pagedRequest.Active == null || pc.Active == pagedRequest.Active.Value)
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Include(pc => pc.ProductInformation)
                    .ThenInclude(pc => pc.ProductInformationTranslations)
                    .Select(pc => new ProductInformationComment
                    {
                        Id = pc.Id,
                        Active = pc.Active,
                        Approved = pc.Approved,
                        Comment = pc.Comment,
                        ProductInformationId = pc.ProductInformationId,
                        ProductInformation = new ProductInformation
                        {
                            ProductInformationTranslations = pc
                            .ProductInformation
                            .ProductInformationTranslations
                            .Select(pt => new ProductInformationTranslation
                            {
                                Name = pt.Name
                            })
                            .ToList()
                        }
                    })
                     
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(ProductInformationComment dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Ürün yorumu bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.Approved = dto.Approved;

                response.Success = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .Update(entity);
                response.Message = response.Success ? "Ürün yorumu güncellendi." : "İşlem hatalı";
            }, (response, exception) => { });
        }
        #endregion
    }
}
