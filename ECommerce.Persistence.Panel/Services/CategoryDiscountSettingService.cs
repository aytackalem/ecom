﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class CategoryDiscountSettingService : ICategoryDiscountSettingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CategoryDiscountSettingService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<CategoryDiscountSetting> Create(CategoryDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CategoryDiscountSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");
                var entity = new Domain.Entities.CategoryDiscountSetting
                {
                    Active = dto.Active,
                    StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo),
                    EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo),
                    ApplicationId = dto.ApplicationId,
                    Discount = dto.Discount,
                    DiscountIsRate = dto.DiscountIsRate,
                    CreatedDate = DateTime.Now,
                    CategoryId = dto.CategoryId
                };

                var created = this
                    ._unitOfWork
                    .CategoryDiscountSettingRepository
                    .Create(entity);

                var categories = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Where(c => c.ParentCategoryId == dto.CategoryId)
                    .ToList();

                categories.ForEach(c =>
                {

                    this._unitOfWork.CategoryDiscountSettingRepository.Create(new Domain.Entities.CategoryDiscountSetting
                    {
                        Active = dto.Active,
                        StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo),
                        EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo),
                        ApplicationId = dto.ApplicationId,
                        Discount = dto.Discount,
                        DiscountIsRate = dto.DiscountIsRate,
                        CreatedDate = DateTime.Now,
                        CategoryId = c.Id,
                        ParentCategoryDiscountSettingId = entity.Id
                    });
                });

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Genel indirim oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<CategoryDiscountSetting> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CategoryDiscountSetting>>((response) =>
            {
                var entity = this.ReadEntity(id);

                if (entity == null)
                {
                    response.Message = $"Kategori indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                var dto = new CategoryDiscountSetting
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    EndDate = entity.EndDate,
                    StartDate = entity.StartDate,
                    StartDateString = entity.StartDate.ToString("dd/MM/yyy", cultureinfo),
                    EndDateString = entity.EndDate.ToString("dd/MM/yyy", cultureinfo),
                    ApplicationId = entity.ApplicationId,
                    Discount = entity.Discount,
                    DiscountIsRate = entity.DiscountIsRate,
                    CategoryId = entity.CategoryId,
                    Category = new Category
                    {
                        CategoryTranslations = entity
                            .Category
                            .CategoryTranslations
                            .Select(ct => new CategoryTranslation
                            {
                                Name = ct.Name
                            })
                            .ToList()
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<CategoryDiscountSetting> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<CategoryDiscountSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .CategoryDiscountSettingRepository
                    .DbSet()
                    .Where(cds => !cds.Deleted && !cds.ParentCategoryDiscountSettingId.HasValue)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .CategoryDiscountSettingRepository
                    .DbSet()
                    .Where(cds => !cds.Deleted && !cds.ParentCategoryDiscountSettingId.HasValue)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Include(x => x.Category)
                    .ThenInclude(c => c.CategoryTranslations)
                    .Select(x => new CategoryDiscountSetting
                    {
                        Id = x.Id,
                        Active = x.Active,
                        EndDate = x.EndDate,
                        StartDate = x.StartDate,
                        ApplicationId = x.ApplicationId,
                        Discount = x.Discount,
                        DiscountIsRate = x.DiscountIsRate,
                        CategoryId = x.CategoryId,
                        StartDateString = x.StartDate.ToString("dd/MM/yyy", cultureinfo),
                        EndDateString = x.EndDate.ToString("dd/MM/yyy", cultureinfo),
                        Category = new Category
                        {
                            CategoryTranslations = x
                            .Category
                            .CategoryTranslations
                            .Select(ct => new CategoryTranslation
                            {
                                Name = ct.Name
                            })
                            .ToList()
                        }
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(CategoryDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this.ReadEntity(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Kategori indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                entity.StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo);
                entity.EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo);
                entity.ApplicationId = dto.ApplicationId;
                entity.Active = dto.Active;
                entity.Discount = dto.Discount;
                entity.DiscountIsRate = dto.DiscountIsRate;
                //entity.CategoryId = dto.CategoryId;

                var updated = this
                    ._unitOfWork
                    .CategoryDiscountSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                var categoryDiscountSettings = this
              ._unitOfWork
              .CategoryDiscountSettingRepository
              .DbSet()
              .Where(c => c.ParentCategoryDiscountSettingId == entity.Id)
                      .ToList();


                categoryDiscountSettings.ForEach(c =>
                {
                    c.StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo);
                    c.EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo);
                    c.ApplicationId = dto.ApplicationId;
                    c.Active = dto.Active;
                    c.Discount = dto.Discount;
                    c.DiscountIsRate = dto.DiscountIsRate;
                });

                this
                  ._unitOfWork
                  .CategoryDiscountSettingRepository.Update(categoryDiscountSettings);

                response.Success = true;
                response.Message = "Kategori indirimi güncellendi.";
            }, (response, exception) => { });
        }


        public Response Delete(CategoryDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .CategoryDiscountSettingRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Kategori indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;
                entity.DeletedDateTime = DateTime.Now;

                var updated = this
                    ._unitOfWork.CategoryDiscountSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                var categoryDiscountSettings = this
                        ._unitOfWork
                        .CategoryDiscountSettingRepository
                        .DbSet()
                        .Where(c => c.ParentCategoryDiscountSettingId == entity.Id)
                         .ToList();


                categoryDiscountSettings.ForEach(c =>
                {
                    c.Deleted = true;
                    c.DeletedDateTime = DateTime.Now;
                });

                this
                  ._unitOfWork
                  .CategoryDiscountSettingRepository.Update(categoryDiscountSettings);

                response.Success = true;
                response.Message = "Kategori indirimi bulunamadı silindi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.CategoryDiscountSetting ReadEntity(int id)
        {
            return this
                ._unitOfWork
                .CategoryDiscountSettingRepository
                .DbSet()
                .Where(x => x.Id == id)
                .Include(x => x.Category)
                .ThenInclude(c => c.CategoryTranslations)
                .FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}
