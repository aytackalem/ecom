﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceVariantMappingService : IMarketplaceVariantMappingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICompanyFinder _companyFinder;

        #endregion

        #region Constructors
        public MarketplaceVariantMappingService(IUnitOfWork unitOfWork, ICompanyFinder companyFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<MarketplaceVariantValueMapping>>> GetMappedVariantValueAsync(List<int> variantValueIds)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<MarketplaceVariantValueMapping>>>(async (response) =>
            {
                response.Data = await this
                    ._unitOfWork
                    .MarketplaceVariantValueMappingRepository
                    .DbSet()
                    .Select(x => new MarketplaceVariantValueMapping
                    {
                        Id = x.Id,
                        MarketplaceId = x.MarketplaceId,
                        AllowCustom = x.AllowCustom,
                        CompanyId = x.CompanyId,
                        Mandatory = x.Mandatory,
                        MarketplaceVariantValueCode = x.MarketplaceVariantValueCode,
                        MarketplaceVariantValueName = x.MarketplaceVariantValueName,
                        VariantValueId = x.VariantValueId
                    })
                    .Where(x => variantValueIds.Contains(x.Id))
                    .ToListAsync();
                response.Success = true;
            });
        }

        public Response Upsert(Application.Panel.Parameters.MarketplaceVariantMappings.UpsertRequest upsertRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var variantValueIds = this
                    ._unitOfWork
                    .VariantValueRepository
                    .DbSet()
                    //.Include(vv => vv.MarketplaceVariantValueMappings)
                    .Where(vv => vv.VariantId == upsertRequest.VariantId)
                    .Select(vv => vv.Id)
                    .ToList();

                #region Variant Value
                //var variantValueIds = upsertRequest.MarketplaceVariantValueMappings.Select(vv => vv.VariantValueId).Distinct().ToList();
                foreach (var theVariantValueId in variantValueIds)
                {
                    var variantValue = this
                        ._unitOfWork
                        .VariantValueRepository
                        .DbSet()
                        .Include(vv => vv.MarketplaceVariantValueMappings)
                        .FirstOrDefault(vv => vv.Id == theVariantValueId);

                    var marketplaceVariantValueMappings = variantValue
         .MarketplaceVariantValueMappings
         .Where(x => x.CompanyId != this._companyFinder.FindId())
         .ToList();

                    variantValue.MarketplaceVariantValueMappings = new();


                    var _marketplaceVariantValueMappings = upsertRequest.MarketplaceVariantValueMappings.Where(x => x.VariantValueId == theVariantValueId).ToList();

                    _marketplaceVariantValueMappings.ForEach(mvvm =>
                    {
                        marketplaceVariantValueMappings.Remove(marketplaceVariantValueMappings.FirstOrDefault(x => x.MarketplaceId == mvvm.MarketplaceId && mvvm.CompanyId == null));


                        variantValue.MarketplaceVariantValueMappings.Add(new Domain.Entities.MarketplaceVariantValueMapping
                        {
                            MarketplaceId = mvvm.MarketplaceId,
                            CompanyId = mvvm.CompanyId,
                            MarketplaceVariantCode = mvvm.MarketplaceVariantCode,
                            MarketplaceVariantName = mvvm.MarketplaceVariantName,
                            MarketplaceVariantValueCode = mvvm.MarketplaceVariantValueCode,
                            MarketplaceVariantValueName = mvvm.MarketplaceVariantValueName,
                            AllowCustom = mvvm.AllowCustom,
                            Mandatory = mvvm.Mandatory
                        });
                    });

                    variantValue.MarketplaceVariantValueMappings.AddRange(marketplaceVariantValueMappings);


                    this._unitOfWork.VariantValueRepository.Update(variantValue);
                }
                #endregion

                response.Success = true;
                response.Message = "Pazaryeri varyasyon kayıtları başarılı şekilde kaydedildi.";
            });
        }
        #endregion
    }
}