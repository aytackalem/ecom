﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class PropertyValueService : IPropertyValueService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public PropertyValueService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<PropertyValue> Create(PropertyValue dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<PropertyValue>>((response) =>
            {
                var entity = new Domain.Entities.PropertyValue
                {
                    Id = dto.Id,
                    PropertyId = dto.PropertyId,
                    PropertyValueTranslations = dto
                        .PropertyValueTranslations
                        .Select(vt => new Domain.Entities.PropertyValueTranslation
                        {
                            LanguageId = vt.LanguageId,
                            CreatedDate = DateTime.Now,
                            Value = vt.Value
                        })
                        .ToList()
                };

                var created = this
                    ._unitOfWork
                    .ProductPropertyValueRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Varyasyon değeri oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<List<PropertyValue>> Read(int productPropertyId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<PropertyValue>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ProductPropertyValueRepository
                    .DbSet()
                    .Where(vv => vv.PropertyId == productPropertyId)
                    .Select(vv => new PropertyValue
                    {
                        Id = vv.Id,
                        PropertyValueTranslations = vv.PropertyValueTranslations.Select(ct => new PropertyValueTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Value = ct.Value
                        }).ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(PropertyValue dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductPropertyValueRepository
                    .DbSet()
                    .Include(ppv => ppv.PropertyValueTranslations)
                    .FirstOrDefault(ppv => ppv.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Ürün özellik değeri bulunamadı.";
                    response.Success = false;
                    return;
                }

#warning Alt yapi multi language destekliyor fakat kod tek dil icin yazilmistir.
                entity.PropertyValueTranslations[0].Value = dto.PropertyValueTranslations[0].Value;

                var updated = this
                    ._unitOfWork
                    .ProductPropertyValueRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Varyasyon değeri güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
