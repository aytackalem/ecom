﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class StocktakingService : IStocktakingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public StocktakingService(IUnitOfWork unitOfWork, IDomainFinder domainFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            _domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Stocktaking> Create(Stocktaking dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Stocktaking>>(response =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();

                var hasAnySameName = dbSet.Any(b => b.Name == dto.Name);
                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir kayıt oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Domainable.Stocktaking
                {
                    StocktakingTypeId = StocktakingTypes.Beklemede,
                    Name = dto.Name,
                    Description = dto.Description,
                    CreatedDateTime = DateTime.Now,
                    ModelCode = dto.ModelCode,
                    CheckGroup = dto.CheckGroup,
                    Manuel = dto.Manuel
                };
                var created = repository.Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Stok sayımı oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Stocktaking> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Stocktaking>>((response) =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();

                var entity = dbSet
                    .Include(s => s.StocktakingDetails)
                        .ThenInclude(sd => sd.ProductInformation)
                            .ThenInclude(sd => sd.ProductInformationTranslations.Where(pit => pit.LanguageId == "TR"))
                    .Include(s => s.StocktakingDetails)
                        .ThenInclude(sd => sd.ProductInformation)
                            .ThenInclude(sd => sd.ProductInformationPhotos)
                    .FirstOrDefault(s => s.Id == id);

                if (entity == null)
                {
                    response.Message = $"Stok sayımı bulunamadı.";
                    response.Success = false;
                    return;
                }

                var productInformationIds = entity.StocktakingDetails.Select(sd => sd.ProductInformationId).ToList();

                var collectedQuantities = this
                    ._unitOfWork
                    .OrderDetailRepository
                    .DbSet()
                    .Where(od =>
                        od.Order.OrderBilling == null &&
                        productInformationIds.Contains(od.ProductInformationId) &&
                        od.Order.OrderTypeId == OrderTypes.Toplandi)
                    .GroupBy(od => od.ProductInformationId)
                    .ToDictionary(od => od.Key, od => od.Sum(od2 => od2.Quantity));

                var packedQuantities = this
                    ._unitOfWork
                    .OrderDetailRepository
                    .DbSet()
                    .Where(od =>
                        od.Order.MarketplaceId == Marketplaces.TrendyolEurope &&
                        od.Order.OrderBilling == null &&
                        productInformationIds.Contains(od.ProductInformationId) &&
                        od.Order.OrderTypeId == OrderTypes.KargoyaTeslimEdildi)
                    .GroupBy(od => od.ProductInformationId)
                    .ToDictionary(od => od.Key, od => od.Sum(od2 => od2.Quantity));

                var domain = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(x => x.DomainSetting)
                    .FirstOrDefault(d => d.Id == this._domainFinder.FindId());

                var productPath = $"{domain.DomainSetting.ImageUrl}/product";

                var dto = new Stocktaking
                {
                    ModelCode = entity.ModelCode,
                    CheckGroup = entity.CheckGroup,
                    Manuel = entity.Manuel,
                    Description = entity.Description,
                    StocktakingTypeId = entity.StocktakingTypeId,
                    Id = entity.Id,
                    Name = entity.Name,
                    StocktakingDetails = entity
                            .StocktakingDetails
                            .OrderBy(sd=>sd.ProductInformation.Id)
                            .Select(sd => new StocktakingDetail
                            {
                                Quantity = sd.Quantity,
                                CollectedQuantity = collectedQuantities.ContainsKey(sd.ProductInformationId)
                                    ? collectedQuantities[sd.ProductInformationId]
                                    : 0,
                                PackedQuantity = packedQuantities.ContainsKey(sd.ProductInformationId)
                                    ? packedQuantities[sd.ProductInformationId]
                                    : 0,
                                ProductInformation = new ProductInformation
                                {
                                    Stock = sd.ProductInformation.Stock,
                                    ReservedStock = sd.ProductInformation.ReservedStock,
                                    Barcode = sd.ProductInformation.Barcode,
                                    ProductInformationTranslations = sd
                                        .ProductInformation
                                        .ProductInformationTranslations
                                        .Select(pit => new ProductInformationTranslation
                                        {
                                            Name = pit.Name,
                                            FullVariantValuesDescription = pit.FullVariantValuesDescription,
                                            VariantValuesDescription = pit.VariantValuesDescription
                                        })
                                        .ToList(),
                                    ProductInformationPhotos = sd
                                        .ProductInformation
                                        .ProductInformationPhotos
                                        .Select(pip => new ProductInformationPhoto
                                        {
                                            FileName = $"{productPath}/{pip.FileName}",
                                            DisplayOrder = pip.DisplayOrder
                                        })
                                        .ToList()
                                }
                            })
                            .ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Stocktaking> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Stocktaking>>((response) =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();
                var iQueryable = dbSet;

                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("StocktakingTypeId:"))
                    {
                        var value = pLoop.Replace("StocktakingTypeId:", "");
                        if (value.Length > 0)
                        {
                            var stocktakingTypeId = value;
                            iQueryable = iQueryable.Where(iq => iq.StocktakingTypeId == stocktakingTypeId);
                        }
                    }
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = dbSet.Count();
                response.Data = iQueryable
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Stocktaking
                    {
                        Id = b.Id,
                        StocktakingTypeId = b.StocktakingTypeId,
                        Name = b.Name,
                        Description = b.Description,
                        CreatedDateTime = b.CreatedDateTime,
                        ModelCode = b.ModelCode,
                        CheckGroup = b.CheckGroup
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Stocktaking dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();

                var hasAnySameName = dbSet.Any(b => b.Name == dto.Name && b.Id != dto.Id);
                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir kayıt oluşturulduğundan kayıt güncellenemez.";
                    return;
                }

                var entity = repository.Read(dto.Id);
                if (entity == null)
                {
                    response.Message = $"{dto.Name} ({dto.Id}) kaydı bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;
                entity.Description = dto.Description;
                if (entity.StocktakingTypeId != StocktakingTypes.Beklemede && entity.StocktakingTypeId != StocktakingTypes.Onaylanmis)
                    entity.StocktakingTypeId = dto.StocktakingTypeId;

                var updated = repository.Update(entity);
                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Stok sayım güncellendi.";
            }, (response, exception) => { });
        }

        public Response Approve(Stocktaking dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();

                var entity = repository.Read(dto.Id);
                if (entity == null)
                {
                    response.Message = $"Stok sayımı bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.StocktakingTypeId = "ON";

                response.Success = repository.Update(entity);
                response.Message = $"Stok sayımı {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }

        public Response Delete(Stocktaking dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var repository = this._unitOfWork.StocktakingRepository;
                var dbSet = repository.DbSet();

                var entity = repository.Read(dto.Id);
                if (entity == null)
                {
                    response.Message = $"Stok sayımı bulunamadı.";
                    response.Success = false;
                    return;
                }

                if (entity.StocktakingTypeId != "BE")
                {
                    response.Message = $"Stok sayımı silinemez.";
                    response.Success = false;
                    return;
                }

                response.Success = this._unitOfWork.StocktakingRepository.Delete(dto.Id);
                response.Message = $"Stok sayımı {(response.Success ? "silindi" : "silinemedi")}.";
            });
        }
        #endregion
    }
}
