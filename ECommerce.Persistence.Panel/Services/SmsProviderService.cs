﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class SmsProviderService : ISmsProviderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public SmsProviderService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Application.Common.DataTransferObjects.SmsProvider> Create(SmsProvider dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<SmsProvider>>((response) =>
            {
                //var hasAnySameId = this
                //     ._unitOfWork
                //     .PosRepository
                //     .DbSet()
                //     .Any(b => b.Name == dto.Name);

                //if (hasAnySameId)
                //{
                //    response.Success = false;
                //    response.Message = "Bu no ile daha önce bir sms sağlayıcı oluşturulduğundan yenisi oluşturulamaz.";
                //    return;
                //}

                //var entity = new Domain.Entities.SmsProvider
                //{
                //    Id = dto.Id,
                //    Name = dto.Name
                //};

                //var created = this
                //    ._unitOfWork
                //    .SmsProviderRepository
                //    .Create(entity);

                //if (!created)
                //{
                //    response.Success = false;
                //    return;
                //}

                //dto.Id = entity.Id;

                //response.Data = dto;
                response.Success = true;
                response.Message = "Sms sağlayıcısı oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<SmsProvider> Read(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<SmsProvider>>((response) =>
            {
                //var entity = this.ReadEntity(id);
                //if (entity == null)
                //{
                //    response.Message = $"Sms sağlayıcı bulunamadı.";
                //    response.Success = false;
                //    return;
                //}

                //var dto = new SmsProvider
                //{
                //    Id = entity.Id,
                //    Name = entity.Name,
                    //SmsProviderConfigurations = entity
                    //    .SmsProviderConfigurations
                    //    .Select(spc => new SmsProviderConfiguration
                    //    {
                    //        Id = spc.Id,
                    //        Key = spc.Key,
                    //        Value = spc.Value
                    //    })
                    //    .ToList()
                //};

                //response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<SmsProvider> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<SmsProvider>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new SmsProvider
                    {
                        Id = b.SmsProvider.Id,
                        Name = b.SmsProvider.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(SmsProvider dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this.ReadEntity(dto.Id);
                if (entity == null)
                {
                    response.Message = $"Sms sağlayıcı bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;

                //entity
                //    .SmsProviderConfigurations
                //    .AddRange(dto
                //        .SmsProviderConfigurations
                //        .Where(spc => spc.Id == 0)
                //        .Select(spc => new Domain.Entities.SmsProviderConfiguration
                //        {
                //            Key = spc.Key,
                //            Value = spc.Value
                //        }));

                //dto
                //    .SmsProviderConfigurations
                //    .Where(spc => spc.Id != 0)
                //    .ToList()
                //    .ForEach(spc =>
                //    {
                //        var smsProviderConfiguration = entity.SmsProviderConfigurations.First(spc2 => spc2.Id == spc.Id);
                //        smsProviderConfiguration.Key = spc.Key;
                //        smsProviderConfiguration.Value = spc.Value;
                //    });

                //var updated = this
                //    ._unitOfWork
                //    .SmsProviderRepository
                //    .Update(entity);

                //if (!updated)
                //{
                //    response.Success = false;
                //    return;
                //}

                response.Success = true;
                response.Message = "Sms sağlayıcı güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.SmsProvider ReadEntity(string id)
        {
            return new Domain.Entities.SmsProvider();
            //return this
            //    ._unitOfWork
            //    .SmsProviderRepository
            //    .DbSet()
            //    .Include(sp => sp.SmsProviderConfigurations)
            //    .FirstOrDefault(sp => sp.Id == id);
        }
        #endregion
    }
}
