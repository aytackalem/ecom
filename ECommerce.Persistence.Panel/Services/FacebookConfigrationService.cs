﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class FacebookConfigrationService : IFacebookConfigrationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public FacebookConfigrationService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<FacebookConfigration> Create(FacebookConfigration dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<FacebookConfigration>>((response) =>
            {



                var entity = new Domain.Entities.FacebookConfigration
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    Code = dto.Code,
                    Name = dto.Name,
                    Key = dto.Key
                };

                var created = this
                    ._unitOfWork.FacebookConfigrationRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;



                response.Data = dto;
                response.Success = true;
                response.Message = "Facebook Konfig oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<FacebookConfigration> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<FacebookConfigration>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .FacebookConfigrationRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Facebook Konfig bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new FacebookConfigration
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Code = entity.Code,
                    Key = entity.Key,
                    Name = entity.Name
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<FacebookConfigration> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<FacebookConfigration>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .FacebookConfigrationRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .FacebookConfigrationRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new FacebookConfigration
                    {
                        Id = c.Id,
                        Active = c.Active,
                        Code = c.Code,
                        Name = c.Name,
                        Key = c.Key
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .FacebookConfigrationRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c.Code
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(FacebookConfigration dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .FacebookConfigrationRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"İletişim bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.Code = dto.Code;


                var updated = this
                    ._unitOfWork.FacebookConfigrationRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Facebook Konfig  güncellendi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
