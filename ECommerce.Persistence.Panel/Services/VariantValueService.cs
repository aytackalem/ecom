﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class VariantValueService : IVariantValueService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public VariantValueService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<VariantValue> Create(VariantValue dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<VariantValue>>((response) =>
            {
                var entity = new Domain.Entities.VariantValue
                {
                    Id = dto.Id,
                    VariantId = dto.VariantId,
                    VariantValueTranslations = dto
                        .VariantValueTranslations
                        .Select(vt => new Domain.Entities.VariantValueTranslation
                        {
                            LanguageId = vt.LanguageId,
                            CreatedDate = DateTime.Now,
                            Value = vt.Value
                        })
                        .ToList()
                };

                var created = this
                    ._unitOfWork
                    .VariantValueRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Varyasyon değeri oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<List<VariantValue>> Read(int variantId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<VariantValue>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .VariantValueRepository
                    .DbSet()
                    .Where(vv => vv.VariantId == variantId)
                    .Select(vv => new VariantValue
                    {
                        Id = vv.Id,
                        VariantValueTranslations = vv.VariantValueTranslations.Select(ct => new VariantValueTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Value = ct.Value
                        }).ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(VariantValue dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .VariantValueRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Varyasyon değeri bulunamadı.";
                    response.Success = false;
                    return;
                }

#warning Alt yapi multi language destekliyor fakat kod tek dil icin yazilmistir.
                entity.VariantValueTranslations[0].Value = dto.VariantValueTranslations[0].Value;

                var updated = this
                    ._unitOfWork
                    .VariantValueRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Varyasyon değeri güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
