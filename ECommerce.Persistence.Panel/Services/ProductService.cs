﻿using AutoMapper;
using Dapper;
using DocumentFormat.OpenXml.Office2010.Excel;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Products;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Syncfusion.Pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductService : IProductService
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;

        private readonly ISettingService _settingService;

        private readonly IRandomHelper _randomHelper;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IExcelHelper _excelHelper;

        private readonly IProductInformationStockService _productInformationStockService;

        private readonly IProductInformationProductService _productInformationProductService;
        #endregion

        #region Constructors
        public ProductService(IMapper mapper, IUnitOfWork unitOfWork, IImageHelper imageHelper, ISettingService settingService, IRandomHelper randomHelper, IDomainFinder domainFinder, ICompanyFinder companyFinder, IExcelHelper excelHelper, IProductInformationStockService productInformationStockService, IProductInformationProductService productInformationProductService)
        {
            #region Fields
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            this._settingService = settingService;
            this._randomHelper = randomHelper;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._excelHelper = excelHelper;
            this._productInformationStockService = productInformationStockService;
            this._productInformationProductService = productInformationProductService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<ProductInformationVariantValue>> ReadAsKeyValueById(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductInformationVariantValue>>>((response) =>
            {
                var productEntity = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationVariants)
                    .ThenInclude(piv => piv
                        .VariantValue
                        .VariantValueTranslations
                        .Where(vvt => vvt.LanguageId == "TR"))
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationVariants)
                    //.ThenInclude(piv => piv.VariantValue.Variant.MarketplaceVariantMappings)
                    //.ThenInclude(mvm => mvm.MarketplaceVariant)
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationVariants)
                    .ThenInclude(piv => piv.VariantValue)
                    .ThenInclude(piv => piv.MarketplaceVariantValueMappings)
                    //.ThenInclude(mvvm => mvvm.MarketplaceVariantValue)
                    //.ThenInclude(x => x.MarketplaceCategoriesMarketplaceVariantValues)
                    //.ThenInclude(x => x.MarketplaceCategoriesMarketplaceVariant)
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationVariants)
                    .ThenInclude(piv => piv
                        .VariantValue
                        .Variant
                        .VariantTranslations
                        .Where(vt => vt.LanguageId == "TR"))
                    .First(p => p.Id == id);

                response.Data = new();
                response.Success = true;

                productEntity.ProductInformations.ForEach(piLoop =>
                {
                    piLoop.ProductInformationVariants.ForEach(pivLoop =>
                    {
                        pivLoop.VariantValue.MarketplaceVariantValueMappings.ForEach(mvvm =>
                        {
                            //mvvm.VariantValue.Variant.MarketplaceVariantMappings.ForEach(mvm =>
                            //{
                            //if (mvvm.MarketplaceVariantValue.MarketplaceId != mvm.MarketplaceVariant.MarketplaceId)
                            //    return;

                            //ProductInformationVariantValue productInformationVariantValue = new()
                            //{
                            //    ProductInformationId = piLoop.Id,
                            //    Value = mvvm.MarketplaceVariantValue.Value,
                            //    MarketplaceVariantValueId = mvvm.MarketplaceVariantValueId,
                            //    MarketplaceVariantId = mvm.MarketplaceVariantId,
                            //    MarketplaceCategoriesMarketplaceVariantValueId = 0
                            //};

                            //var id = mvvm
                            //        .MarketplaceVariantValue
                            //        .MarketplaceCategoriesMarketplaceVariantValues
                            //        .FirstOrDefault(y => y.MarketplaceVariantValueId == mvvm.MarketplaceVariantValueId &&
                            //                             y.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId == mvm.MarketplaceVariantId)
                            //        ?.Id;
                            //if (id.HasValue)
                            //{

                            //    productInformationVariantValue.MarketplaceCategoriesMarketplaceVariantValueId = id.Value;

                            //    response.Data.Add(productInformationVariantValue);
                            //}
                            //});
                        });
                    });
                });
            });
        }

        public DataResponse<Product> Create(Product dto)
        {
            if (dto.ProductInformations.Any(pi => pi != null && pi.ProductInformationPriceses.Any(pip => pip.ListUnitPrice < pip.UnitPrice)))
                return new DataResponse<Product> { Success = false, Message = "Satış fiyatı, liste fiyatından büyük olamaz." };

            return ExceptionHandler.ResultHandle<DataResponse<Product>>((response) =>
            {
#warning Confirm halinde yapilacak
                dto.ProductInformations = dto.ProductInformations.Where(pi => pi != null).ToList();

                var hasAnySameStockCode = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Any(p => p.ProductInformations.Any(pi => dto.ProductInformations.Select(pi2 => pi2.StockCode).Contains(pi.StockCode)));

                if (hasAnySameStockCode)
                {
                    response.Success = false;
                    response.Message = "Bu stok kodu ile daha önce bir ürün oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                //var anyProductInformationPhotos = dto
                //    .ProductInformations
                //    .FirstOrDefault(x => x.Photoable && x.ProductInformationPhotos == null);

                //if (anyProductInformationPhotos != null)
                //{
                //    response.Success = false;
                //    response.Message = $"{anyProductInformationPhotos.ProductInformationTranslations.First().Name} ürünün fotağrafı bulunamadı. Lütfen ürün fotoğrafı yükleyiniz.";
                //    return;
                //}

                if (dto.Type == "PC") //Product Kombini
                {
                    if (dto.ProductInformations.Count > 1)
                    {
                        response.Success = false;
                        response.Message = "Ürün kombinini ekleyebilmek için lütfen variant'lı ürün seçmeyiniz.";
                        return;
                    }

                    if (dto.ProductInformations.FirstOrDefault().ProductInformationCombines.Count == 0)
                    {
                        response.Success = false;
                        response.Message = "Lütfen ürün kombinine bir ürün ekleyiniz.";
                        return;
                    }
                }

                foreach (var pi in dto.ProductInformations)
                {
                    foreach (var pip in pi.ProductInformationPriceses)
                    {
                        if (pip.ListUnitPrice <= 0)
                        {
                            response.Success = false;
                            response.Message = $"{pi.ProductInformationTranslations[0].Name} ürünün liste fıyatını {pip.ListUnitPrice} giremezsiniz.";
                            return;

                        }
                        else if (pip.UnitPrice <= 0)
                        {
                            response.Success = false;
                            response.Message = $"{pi.ProductInformationTranslations[0].Name} ürünün fıyatını {pip.UnitPrice} giremezsiniz.";
                            return;

                        }


                        foreach (var productInformationPrice in pi.ProductInformationPriceses)
                        {
                            productInformationPrice.ConversionCurrencyId = productInformationPrice.ConversionCurrencyId;
                            productInformationPrice.ConversionUnitCost = productInformationPrice.ConversionUnitCost;
                            productInformationPrice.UnitPrice = productInformationPrice.UnitPrice;
                            productInformationPrice.VatRate = productInformationPrice.VatRate;
                            productInformationPrice.ListUnitPrice = productInformationPrice.ListUnitPrice;

                            if (string.IsNullOrEmpty(productInformationPrice.ConversionCurrencyId))
                            {
                                productInformationPrice.ConversionCurrencyId = null;
                                productInformationPrice.UnitCost = productInformationPrice.UnitCost;
                            }
                            else
                            {
#warning Statik kod yazıldı
                                var exchangeRate = this
                                    ._unitOfWork
                                    .ExchangeRateRepository
                                    .DbSet()
                                    .FirstOrDefault(er => er.CreatedDate.Date == DateTime.Now.Date &&
                                                          er.CurrencyId == productInformationPrice.ConversionCurrencyId);

                                productInformationPrice.UnitCost = productInformationPrice.ConversionUnitCost * exchangeRate.Rate;
                            }
                        }
                    }
                }

                var entity = new Domain.Entities.Product
                {
                    BrandId = dto.BrandId,
                    SupplierId = dto.SupplierId,
                    CategoryId = dto.CategoryId,
                    SellerCode = dto.SellerCode,
                    Active = true,
                    CreatedDate = DateTime.Now,
                    IsOnlyHidden = false,
                    AccountingProperty = dto.AccountingProperty
                };


                entity.ProductTranslations = dto.
                ProductTranslations
                .Select(pt => new Domain.Entities.ProductTranslation
                {
                    LanguageId = pt.LanguageId,
                    CreatedDate = DateTime.Now,
                    Name = pt.Name,
                })
                .ToList();


                entity.ProductInformations = dto
                    .ProductInformations
                    .Where(pi => pi != null)
                    .Select(pi => new Domain.Entities.ProductInformation
                    {
                        GroupId = pi.GroupId,
                        Photoable = pi.Photoable,
                        Barcode = pi.Barcode,
                        Stock = pi.Stock,
                        StockCode = pi.StockCode,
                        ShelfCode = pi.ShelfCode,
                        SkuCode = pi.SkuCode,
                        CreatedDate = DateTime.Now,
                        IsSale = pi.IsSale,
                        Payor = pi.Payor,
                        Type = dto.Type,
                        Active = true,
                        ProductInformationCombines = dto.Type == "PC" ? pi.ProductInformationCombines.Select(x => new Domain.Entities.ProductInformationCombine
                        {
                            ContainProductInformationId = x.ContainProductInformationId,
                            Quantity = x.Quantity
                        }).ToList() : null,
                        ProductInformationPriceses = pi
                            .ProductInformationPriceses
                            .Select(pip => new Domain.Entities.ProductInformationPrice
                            {
                                CreatedDate = DateTime.Now,
                                CurrencyId = pip.CurrencyId,
                                ListUnitPrice = pip.ListUnitPrice,
                                UnitCost = pip.UnitCost,
                                ConversionCurrencyId = pip.ConversionCurrencyId,
                                ConversionUnitCost = pip.ConversionUnitCost,
                                UnitPrice = pip.UnitPrice,
                                VatRate = pip.VatRate
                            })
                            .ToList(),
                        ProductInformationTranslations = pi
                            .ProductInformationTranslations
                            .Select(pip => new Domain.Entities.ProductInformationTranslation
                            {
                                VariantValuesDescription = pi.ProductInformationVariants?.Count(x => x.VariantValue.Variant.ShowVariant) > 0 ?
                                String.Join(
                                        ", ",
                                        pi
                                            .ProductInformationVariants
                                            .Where(x => x.VariantValue.Variant.ShowVariant)
                                            .Select(piv => piv
                                                .VariantValue
                                                .VariantValueTranslations
                                                .FirstOrDefault()?.Value.Trim())) :
                                    null,
                                FullVariantValuesDescription = pi.ProductInformationVariants?.Count() > 0 ?
                                            String.Join(
                                                 ", ",
                                                 pi.ProductInformationVariants
                                                     .Select(piv => $"{piv
                                                         .VariantValue
                                                         .Variant
                                                         .VariantTranslations.FirstOrDefault().Name.Trim()}: {piv
                                                         .VariantValue
                                                         .VariantValueTranslations.FirstOrDefault().Value.Trim()}")) :
                                    null,
                                CreatedDate = DateTime.Now,
                                LanguageId = pip.LanguageId,
                                Name = pip.Name,
                                Url = "",
                                SubTitle = pip.SubTitle == null ? "" : pip.SubTitle,
                                ProductInformationSeoTranslation = new Domain.Entities.ProductInformationSeoTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    Title = pip?.ProductInformationSeoTranslation?.Title,
                                    MetaKeywords = pip?.ProductInformationSeoTranslation?.MetaKeywords,
                                    MetaDescription = pip?.ProductInformationSeoTranslation?.MetaDescription
                                },
                                ProductInformationContentTranslation = new Domain.Entities.ProductInformationContentTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    Content = pip.ProductInformationContentTranslation.Content,
                                    Description = pip.ProductInformationContentTranslation.Description
                                }
                            })
                            .ToList(),
                        ProductInformationVariants = pi.ProductInformationVariants == null
                            ? null
                            : pi
                            .ProductInformationVariants
                            .Select(pv => new Domain.Entities.ProductInformationVariant
                            {
                                VariantValueId = pv.VariantValueId,

                            })
                            .ToList(),
                        ProductInformationPhotos = pi.Photoable && pi
                                .ProductInformationPhotos != null
                            ? pi
                                .ProductInformationPhotos
                                .Select(pip => new Domain.Entities.ProductInformationPhoto
                                {
                                    FileName = pip.FileName,
                                    DisplayOrder = pip.DisplayOrder,
                                    CreatedDate = DateTime.Now,
                                    Deleted = false
                                })
                                .ToList()
                            : null
                    })
                    .ToList();

                entity.ProductLabels = dto
                    .ProductLabels
                    .Select(pl => new Domain.Entities.ProductLabel
                    {
                        LabelId = pl.LabelId,
                        CreatedDate = DateTime.Now,
                        Active = true
                    })
                    .ToList();

                if (dto.ProductProperties?.Count > 0)
                    entity.ProductProperties = dto
                        .ProductProperties
                        .Select(ppp => new Domain.Entities.ProductProperty
                        {
                            CreatedDate = DateTime.Now,
                            PropertyValueId = ppp.PropertyValueId,
                            Active = true
                        })
                        .ToList();

                var created = this
                    ._unitOfWork
                    .ProductRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                //var companyId = this._companyFinder.FindId();
                //var company = _unitOfWork.CompanyRepository.DbSet().FirstOrDefault(x => x.Id == companyId);
                //entity.SellerCode = $"{company?.Name.ReplaceChar().Substring(0, 2).ToUpper()}{entity.Id}";



                var photoableProductInformations = entity.ProductInformations.Where(pi => pi.Photoable);
                foreach (var ppiLoop in photoableProductInformations)
                {
                    if (ppiLoop.ProductInformationPhotos != null)
                        foreach (var productInformationPhoto in ppiLoop.ProductInformationPhotos)
                        {
                            if (productInformationPhoto != null && productInformationPhoto.FileName.StartsWith("data"))
                            {
                                var directory = $@"{this._settingService.ProductImagePath}\{ppiLoop.Id}";

                                if (!Directory.Exists(directory))
                                    Directory.CreateDirectory(directory);

                                var fileName = $"{ppiLoop.ProductInformationTranslations[0].Name.GenerateUrl()}-{_randomHelper.GenerateNumbers(0, 100000)}.jpg";
                                var filePath = $@"{directory}\{fileName}";

                                //this._imageHelper.Save(productInformationPhoto.FileName, filePath);
                                this._imageHelper.Save(productInformationPhoto.FileName, filePath);

                                productInformationPhoto.FileName = $"{ppiLoop.Id}/{fileName}";

                                this
                                    ._unitOfWork
                                    .ProductInformationPhotoRepository
                                    .Update(productInformationPhoto);
                            }
                        }
                }

                if (entity.ProductInformations.Any(pi => !pi.Photoable))
                {
                    var productInformations = entity.ProductInformations.Where(pi => !pi.Photoable);
                    foreach (var piLoop in productInformations)
                    {
                        var productInformation = entity
                            .ProductInformations
                            .FirstOrDefault(pi => pi.Photoable && pi.GroupId == piLoop.GroupId);

                        piLoop.ProductInformationPhotos = productInformation
                            .ProductInformationPhotos
                            .Select(pip => new Domain.Entities.ProductInformationPhoto
                            {
                                FileName = pip.FileName,
                                DisplayOrder = pip.DisplayOrder,
                                CreatedDate = DateTime.Now,
                                Deleted = false
                            })
                            .ToList();

                        this
                            ._unitOfWork
                            .ProductInformationRepository
                            .Update(piLoop);
                    }
                }

                var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
                foreach (var productInformation in entity.ProductInformations)
                {
                    foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                    {
                        productInformationTranslation.Url = $"{productInformationTranslation.Name.GenerateUrl()}-{productInformation.Id}-p";

                        this._unitOfWork.ProductInformationTranslationRepository.Update(productInformationTranslation);

                        var html = new StringBuilder();
                        html.AppendLine("<ul>");
                        var recursiveCategory = RecursiveCategory(categories, dto.CategoryId);
                        recursiveCategory.Reverse();
                        foreach (var categoryProduct in recursiveCategory)
                        {
                            html.AppendLine(categoryProduct);
                        }
                        var productInformationTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(productInformationTranslation.Name.ToLower().ReplaceChar());
                        html.AppendLine($"<li>{productInformationTranslationName}</li>");

                        html.AppendLine("</ul>");
                        this._unitOfWork.ProductInformationTranslationBreadcrumbRepository.Create(new Domain.Entities.ProductInformationTranslationBreadcrumb
                        {
                            Id = productInformationTranslation.Id,
                            CreatedDate = DateTime.Now,
                            Html = html.ToString()
                        });

                    }
                }



                var categoryProducts = new List<Domain.Entities.CategoryProduct>();
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, dto.CategoryId, entity.Id, false, false));
                foreach (var categoryId in dto.MultiCategoryIds)
                {
                    if (categoryId == dto.CategoryId) continue;

                    categoryProducts.AddRange(RecursiveCategoryProduct(categories, categoryId, entity.Id, true, true));

                }
                categoryProducts = categoryProducts.GroupBy(x => x.CategoryId).Select(x => x.FirstOrDefault()).ToList();
                this._unitOfWork.CategoryProductRepository.BulkInsert(categoryProducts);

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Ürün oluşturuldu.";
            });
        }

        public async Task<DataResponse<Product>> ReadAsync(int id)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<Product>>(async response =>
            {

                var dbSet = this._unitOfWork.ProductRepository.DbSet();
                var product = await dbSet
                    .Include(p => p.ProductTranslations)
                    .Include(p => p.ProductInformations)
                        .ThenInclude(pi => pi.ProductInformationVariants)
                            .ThenInclude(piv => piv.VariantValue.VariantValueTranslations)
                    .Include(p => p.ProductInformations)
                        .ThenInclude(pi => pi.ProductInformationPriceses)
                    .Include(p => p.ProductInformations)
                        .ThenInclude(pi => pi.ProductInformationTranslations)
                    .FirstOrDefaultAsync(p => p.Id == id);

                response.Data = this._mapper.Map<Product>(product);
                response.Success = true;

            });
        }

        public DataResponse<Product> Read(string sellerCode)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Product>>((response) =>
            {
                var productImageUrl = this._settingService.ProductImageUrl;

                var product = _unitOfWork
                   .ProductRepository
                   .DbSet()
                   .Where(p => p.SellerCode.Contains(sellerCode))
                   .Select(entity => new Product
                   {
                       ProductInformations = entity
                           .ProductInformations
                           .Select(pi => new ProductInformation
                           {
                               Id = pi.Id,
                               Barcode = pi.Barcode,
                               Stock = pi.Stock,
                               SkuCode = pi.SkuCode,
                               ProductInformationTranslations = pi
                                   .ProductInformationTranslations
                                   .Select(pit => new ProductInformationTranslation
                                   {
                                       Id = pit.Id,
                                       LanguageId = pit.LanguageId,
                                       Name = pit.Name,
                                       VariantValuesDescription = pit.VariantValuesDescription
                                   })
                                   .ToList(),
                               ProductInformationPhotos = pi
                                    .ProductInformationPhotos
                                    .Where(x => !x.Deleted).Count() > 0 ? pi
                                    .ProductInformationPhotos
                                    .Where(x => !x.Deleted)
                                    .Select(pip => new ProductInformationPhoto
                                    {
                                        Id = pip.Id,
                                        FileName = $"{productImageUrl}/{pip.FileName}",
                                        DisplayOrder = pip.DisplayOrder
                                    }).ToList() : new(),
                               ProductInformationPriceses = pi
                                   .ProductInformationPriceses
                                   .Select(pip => new ProductInformationPrice
                                   {
                                       UnitCost = pip.UnitCost
                                   })
                                   .ToList()
                           })
                           .ToList()
                       .ToList()
                   })
                   .FirstOrDefault();

                product.ProductInformations = product.ProductInformations.OrderBy(pi => pi.ProductInformationTranslations.FirstOrDefault()?.VariantValuesDescription).ToList();

                response.Data = product;
                response.Success = true;
            });
        }

        public DataResponse<Product> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Product>>((response) =>
            {
                //Stopwatch stopwatch = Stopwatch.StartNew();
                //var entity = this.ReadEntity(id);

                //if (entity == null)
                //{
                //    response.Message = $"Ürün bulunamadı.";
                //    response.Success = false;
                //    return;
                //}
                //stopwatch.Stop();

                Stopwatch stopwatch2 = Stopwatch.StartNew();

                var _productInformationVariants = _unitOfWork.ProductInformationVariantRepository
                .DbSet()
                .Include(x => x.VariantValue.VariantValueTranslations)
                .Include(x => x.VariantValue.Variant.VariantTranslations)
                 .Where(x => x.ProductInformation.ProductId == id)
                .ToList();

                var dto = _unitOfWork
                   .ProductRepository
                   .DbSet()
                   .Select(entity => new Product
                   {
                       Id = entity.Id,
                       AccountingProperty = entity.AccountingProperty,
                       Active = entity.Active,
                       CategoryId = entity.CategoryId,
                       SellerCode = entity.SellerCode,
                       BrandId = entity.BrandId,
                       SupplierId = entity.SupplierId,
                       MultiCategoryIds = entity
                        .CategoryProducts
                        .Where(x => x.Leaf && x.Multi)
                        .Select(cp => cp.CategoryId)
                        .ToList(),
                       ProductTranslations = entity.ProductTranslations
                       .Select(x => new ProductTranslation
                       {
                           Id = x.Id,
                           LanguageId = x.LanguageId,
                           ProductId = x.ProductId,
                           Name = x.Name
                       })
                       .ToList(),
                       ProductLabels = entity
                           .ProductLabels.Where(x => x.Active)
                           .Select(pl => new ProductLabel
                           {
                               Id = pl.Id,
                               LabelId = pl.LabelId
                           })
                           .ToList(),
                       ProductInformations = entity
                           .ProductInformations
                           .Select(pi => new ProductInformation
                           {
                               GroupId = pi.GroupId,
                               Photoable = pi.Photoable,
                               Id = pi.Id,
                               Barcode = pi.Barcode,
                               Stock = pi.Stock,
                               Payor = pi.Payor,
                               IsSale = pi.IsSale,
                               Active = pi.Active,
                               StockCode = pi.StockCode,
                               ShelfCode = pi.ShelfCode,
                               SkuCode = pi.SkuCode,

                               ProductInformationPriceses = pi
                                   .ProductInformationPriceses
                                   .Select(pip => new ProductInformationPrice
                                   {
                                       Id = pip.Id,
                                       CurrencyId = pip.CurrencyId,
                                       ListUnitPrice = pip.ListUnitPrice,
                                       UnitCost = pip.UnitCost,
                                       UnitPrice = pip.UnitPrice,
                                       ConversionUnitCost = pip.ConversionUnitCost,
                                       ConversionCurrencyId = pip.ConversionCurrencyId,
                                       VatRate = pip.VatRate
                                   })
                                   .ToList(),
                               ProductInformationTranslations = pi
                                   .ProductInformationTranslations
                                   .Select(pit => new ProductInformationTranslation
                                   {
                                       Id = pit.Id,
                                       LanguageId = pit.LanguageId,
                                       Name = pit.Name,
                                       FullVariantValuesDescription = pi.ProductInformationVariants.Count() > 0 ?
                                            String.Join(
                                                 ", ",
                                                 pi.ProductInformationVariants
                                                     .Select(piv => $"{piv
                                                         .VariantValue
                                                         .Variant
                                                         .VariantTranslations.FirstOrDefault().Name.Trim()}: {piv
                                                         .VariantValue
                                                         .VariantValueTranslations.FirstOrDefault().Value.Trim()}")) :
                                    null,
                                       Url = pit.Url,
                                       SubTitle = pit.SubTitle,
                                       ProductInformationId = pit.ProductInformationId,
                                       ProductInformationContentTranslation = new ProductInformationContentTranslation
                                       {
                                           Content = pit.ProductInformationContentTranslation.Content,
                                           Description = pit.ProductInformationContentTranslation.Description
                                       },
                                       ProductInformationSeoTranslation = new ProductInformationSeoTranslation
                                       {
                                           Title = pit.ProductInformationSeoTranslation.Title,
                                           MetaDescription = pit.ProductInformationSeoTranslation.MetaDescription,
                                           MetaKeywords = pit.ProductInformationSeoTranslation.MetaKeywords
                                       },
                                       ProductInformationTranslationBreadcrumb = new ProductInformationTranslationBreadcrumb
                                       {
                                           Html = pit.ProductInformationTranslationBreadcrumb.Html
                                       }
                                   })
                                   .ToList(),
                               ProductInformationPhotos = pi
                                    .ProductInformationPhotos
                                    .Where(x => !x.Deleted)
                                    .Select(pip => new ProductInformationPhoto
                                    {
                                        Id = pip.Id,
                                        FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}",
                                        DisplayOrder = pip.DisplayOrder
                                    }).ToList(),
                               ProductInformationCombines = pi.ProductInformationCombines.Select(pic => new ProductInformationCombine
                               {
                                   Id = pic.Id,
                                   ContainProductInformationId = pic.ContainProductInformationId,
                                   Quantity = pic.Quantity,
                                   ProductInformationId = pic.ProductInformationId,
                                   Name = pic.ContainProductInformation.ProductInformationTranslations.FirstOrDefault().Name
                               }).ToList()
                           })
                           .ToList(),
                       ProductProperties = entity
                           .ProductProperties
                           .Where(pp => pp.Active)
                           .Select(ppp => new ProductProperty
                           {
                               PropertyValueId = ppp.PropertyValueId
                           })
                           .ToList()
                   })
                   .FirstOrDefault(p => p.Id == id);

                dto.ProductInformations.ForEach(x =>
                {
                    x.ProductInformationVariants = _productInformationVariants.Where(pv => pv.ProductInformationId == x.Id).Select(pv => new ProductInformationVariant
                    {

                        Id = pv.Id,
                        VariantValueId = pv.VariantValueId,
                        VariantValue = new VariantValue
                        {
                            Id = pv.VariantValue.Id,
                            VariantId = pv.VariantValue.VariantId,
                            Variant = new Variant
                            {
                                Id = pv.VariantValue.Variant.Id,
                                Photoable = pv.VariantValue.Variant.Photoable,
                                VariantTranslations = pv.VariantValue.Variant.VariantTranslations
                                                    .Select(vt => new VariantTranslation
                                                    {
                                                        Id = vt.Id,
                                                        LanguageId = vt.LanguageId,
                                                        Name = vt.Name
                                                    }).ToList()
                            },
                            VariantValueTranslations = pv
                                            .VariantValue
                                            .VariantValueTranslations
                                            .Select(vvt => new VariantValueTranslation
                                            {
                                                Id = vvt.Id,
                                                LanguageId = vvt.LanguageId,
                                                Value = vvt.Value,
                                                VariantValueId = vvt.VariantValueId
                                            })
                                            .ToList()
                        }

                    }).ToList();
                });


                stopwatch2.Stop();

                response.Data = dto;
                response.Success = true;
            });
        }

        public PagedResponse<Product> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Product>>((response) =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Where(x => !x.IsOnlyHidden);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(p => p.ProductInformations.Any(pi => pi.ProductInformationTranslations.Any(pit => pit.Name.Contains(value))));
                        }
                    }
                    else if (pLoop.StartsWith("StockCode:"))
                    {
                        var value = pLoop.Replace("StockCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.StockCode.Contains(value)));
                    }
                    else if (pLoop.StartsWith("Barcode:"))
                    {
                        var value = pLoop.Replace("Barcode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.Barcode.Contains(value)));
                    }
                    else if (pLoop.StartsWith("ModelCode:"))
                    {
                        var value = pLoop.Replace("ModelCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.SellerCode.Contains(value));
                    }
                    else if (pLoop.StartsWith("BrandId:"))
                    {
                        var value = pLoop.Replace("BrandId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.BrandId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("CategoryId:"))
                    {
                        var value = pLoop.Replace("CategoryId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.CategoryId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("SizeTable:"))
                    {
                        var value = pLoop.Replace("SizeTable:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            var any = bool.Parse(value);

                            iQueryable = iQueryable.Where(iq => iq.SizeTables.Any() == any);
                        }
                    }
                    else if (pLoop.StartsWith("ProductPropertyId:"))
                    {
                        var value = pLoop.Replace("ProductPropertyId:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            if (value == "1")
                                iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.Stock > 0));
                            else if (value == "2")
                                iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => !pi.ProductInformationPhotos.Any()));
                            else if (value == "3")
                                iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(p => p.ProductInformationMarketplaces.Any(pim => pim.DisabledUpdateProduct)));
                        }
                    }
                    else if (pLoop.StartsWith("ProductSourceDomainId:"))
                    {
                        var value = pLoop.Replace("ProductSourceDomainId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => value == "0" ? !iq.ProductSourceDomainId.HasValue : iq.ProductSourceDomainId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("ProductCategorization:"))
                    {
                        var value = pLoop.Replace("ProductCategorization:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.Categorization == value));
                    }
                    else if (pLoop.StartsWith("Supplier:"))
                    {
                        var value = pLoop.Replace("Supplier:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.ProductGenericProperties.Any(pgp => pgp.Value == value));
                    }
                    else if (pLoop.StartsWith("SpecialCategorization:"))
                    {
                        var value = pLoop.Replace("SpecialCategorization:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            iQueryable = iQueryable.Where(iq => iq.ProductInformations.Any(pi => pi.SpecialCategorization == (value == "1")));
                        }
                    }
                }

                switch (pagedRequest.Sort)
                {
                    case "Stok":
                        iQueryable = (pagedRequest.Asc == "asc" ? iQueryable.OrderBy(d => d.ProductInformations.Sum(bi => bi.Stock)) : iQueryable.OrderByDescending(d => d.ProductInformations.Sum(bi => bi.Stock)));
                        break;
                    default:
                        iQueryable = iQueryable
                    .OrderByDescending(x => x.Id);
                        break;

                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable.Count();
                response.Data = iQueryable
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Product
                    {
                        ReturnRate = b.ReturnRate,
                        Name = b.ProductTranslations.FirstOrDefault().Name,
                        CreatedDate = b.CreatedDate,
                        Active = b.Active,
                        Id = b.Id,
                        SellerCode = b.SellerCode,
                        TotalStock = b.ProductInformations.Sum(bi => bi.Stock),
                        VariantsCount = b.ProductInformations.Count,
                        ProductSourceDomainName = b.ProductSourceDomainId.HasValue ? b.ProductSourceDomain.Name : "Helpy",
                        ProductInformations = b
                            .ProductInformations
                            .Select(pi => new ProductInformation
                            {
                                ProductInformationVariants = pi.ProductInformationVariants.Select(piv => new ProductInformationVariant
                                {
                                    VariantValue = new VariantValue
                                    {
                                        VariantValueTranslations = piv.VariantValue.VariantValueTranslations.Select(vvt => new VariantValueTranslation
                                        {
                                            Value = vvt.Value
                                        }).ToList(),
                                        Variant = new Variant
                                        {
                                            Slicer = piv.VariantValue.Variant.Slicer,
                                            Varianter = piv.VariantValue.Variant.Varianter,
                                            VariantTranslations = piv.VariantValue.Variant.VariantTranslations.Select(vt => new VariantTranslation
                                            {
                                                Name = vt.Name
                                            }).ToList()
                                        }
                                    }
                                }).ToList(),
                                ReturnRate = pi.ReturnRate,
                                ColorReturnRate = pi.ColorReturnRate,
                                Categorization = pi.Categorization,
                                Active = pi.Active,
                                Stock = pi.Stock,
                                StockCode = pi.StockCode,
                                SkuCode = pi.SkuCode,
                                GroupId = pi.GroupId,
                                Barcode = pi.Barcode,
                                ProductInformationPriceses = pi.ProductInformationPriceses.Where(x => x.CurrencyId == "TL").Select(pip => new ProductInformationPrice
                                {
                                    ListUnitPrice = pip.ListUnitPrice,
                                    UnitPrice = pip.UnitPrice
                                })
                                .ToList(),
                                ProductInformationTranslations = pi
                                    .ProductInformationTranslations
                                    .Where(pit => pit.LanguageId == "TR")
                                    .Select(pit => new ProductInformationTranslation
                                    {
                                        Name = pit.Name,
                                        VariantValuesDescription = pit.VariantValuesDescription,
                                        Url = this._settingService.IncludeECommerce ? $"{this._settingService.WebUrl}/{pit.Url}" : null
                                    })
                                    .ToList(),
                                ProductInformationPhotos = new()
                                {
                                    new()
                                    {
                                        FileName = $"{this._settingService.ProductImageUrl}/{(
                                            pi.ProductInformationPhotos.Where(pip => !pip.Deleted).Count() > 0 ?
                                            pi.ProductInformationPhotos.FirstOrDefault(pip => !pip.Deleted).FileName :
                                            "default.jpg")}"
                                    }
                                },
                                ProductInformationMarketplaces = pi
                                    .ProductInformationMarketplaces
                                    .Where(pim => pim.CompanyId == this._companyFinder.FindId())
                                    .Select(pim => new ProductInformationMarketplace
                                    {
                                        MarketplaceId = pim.MarketplaceId,
                                        Url = pim.Url,
                                        Opened = pim.Opened,
                                        ListUnitPrice = pim.ListUnitPrice,
                                        UnitPrice = pim.UnitPrice,
                                        Active = pim.Active
                                    })
                                    .ToList()
                            })
                            .ToList()
                    })
                    .ToList();
                response.Success = response.RecordsCount > 0;
            }, (response, exception) => { });
        }

        public Response Update(Product dto)
        {
            if (dto.ProductInformations.Any(pi => pi != null && pi.ProductInformationPriceses.Any(pip => pip.ListUnitPrice < pip.UnitPrice)))
                return new Response { Success = false, Message = "Satış fiyatı, liste fiyatından büyük olamaz." };

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                dto.ProductInformations = dto.ProductInformations.Where(x => x != null).ToList();

                var entity = this.ReadEntity(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                var removingProductInformationIds = entity
                    .ProductInformations
                    .Where(pi => !dto.ProductInformations.Select(pi2 => pi2.Id).Contains(pi.Id))
                    .Select(pi => pi.Id).ToList();
                if (removingProductInformationIds.Count > 0)
                {
                    var usedProductInformation = this
                  ._unitOfWork
                  .OrderRepository
                  .DbSet()
                  .Any(o => o.OrderDetails.Any(od => removingProductInformationIds.Contains(od.ProductInformationId)));
                    if (usedProductInformation)
                    {
                        response.Success = false;
                        response.Message = "Silinmek istenilen kayıt ile daha önceden sipariş oluşturulduğundan silinemez.";
                        return;
                    }
                    entity.ProductInformations.RemoveAll(pi => removingProductInformationIds.Contains(pi.Id));

                }





                var hasAnySameStockCode = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Where(p => p.Id != dto.Id)
                    .Any(p => p.ProductInformations.Any(pi => dto.ProductInformations.Select(pi2 => pi2.StockCode).Contains(pi.StockCode)));

                if (hasAnySameStockCode)
                {
                    response.Success = false;
                    response.Message = "Bu stok kodu ile daha önce bir ürün oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                //var anyProductInformationPhotos = dto
                //    .ProductInformations
                //    .FirstOrDefault(x => x.Photoable && x.ProductInformationPhotos == null);

                //if (anyProductInformationPhotos != null)
                //{
                //    response.Success = false;
                //    response.Message = $"{anyProductInformationPhotos.ProductInformationTranslations.First().Name} ürünün fotağrafı bulunamadı. Lütfen ürün fotoğrafı yükleyiniz.";
                //    return;
                //}

                foreach (var pi in dto.ProductInformations)
                {
                    foreach (var pip in pi.ProductInformationPriceses)
                    {
                        if (pip.ListUnitPrice <= 0)
                        {
                            response.Success = false;
                            response.Message = $"{pi.ProductInformationTranslations[0].Name} ürünün liste fıyatını {pip.ListUnitPrice} giremezsiniz.";
                            return;

                        }
                        else if (pip.UnitPrice <= 0)
                        {
                            response.Success = false;
                            response.Message = $"{pi.ProductInformationTranslations[0].Name} ürünün fıyatını {pip.UnitPrice} giremezsiniz.";
                            return;

                        }

                    }
                }

                //entity.Active = dto.Active;
                entity.BrandId = dto.BrandId;
                entity.SupplierId = dto.SupplierId;
                entity.CategoryId = dto.CategoryId;
                entity.AccountingProperty = dto.AccountingProperty;
                entity.SellerCode = dto.SellerCode;
                var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
                var categoryProducts = RecursiveCategoryProduct(categories, dto.CategoryId, entity.Id, false, false);
                entity.CategoryProducts = new List<Domain.Entities.CategoryProduct>();
                entity.CategoryProducts.AddRange(categoryProducts);

                foreach (var categoryId in dto.MultiCategoryIds)
                {
                    if (categoryId == dto.CategoryId) continue;

                    categoryProducts = RecursiveCategoryProduct(categories, categoryId, entity.Id, true, true);
                    entity.CategoryProducts.AddRange(categoryProducts);
                }
                entity.CategoryProducts = entity.CategoryProducts.GroupBy(x => x.CategoryId).Select(x => x.FirstOrDefault()).ToList();

                dto
                    .ProductInformations
                    .Where(pi => pi != null && pi.Id > 0)
                    .ToList()
                    .ForEach(piLoop =>
                    {
                        var productInformation = entity
                            .ProductInformations
                            .FirstOrDefault(pi => pi.Id == piLoop.Id);
                        if (productInformation != null)
                        {
                            if (!piLoop.Active)
                                productInformation.Active = piLoop.Active;
                            else
                            {
                                if (productInformation.Stock != piLoop.Stock)
                                {
                                    this._productInformationStockService.MakeDirtyStock(productInformation.Id);
                                }

                                productInformation.Barcode = piLoop.Barcode;
                                productInformation.Stock = piLoop.Stock;
                                productInformation.Payor = piLoop.Payor;
                                productInformation.StockCode = piLoop.StockCode;
                                productInformation.ShelfCode = piLoop.ShelfCode;
                                productInformation.SkuCode = piLoop.SkuCode;
                                productInformation.IsSale = piLoop.IsSale;
                                productInformation.Active = piLoop.Active;
                                productInformation.GroupId = piLoop.GroupId;

                                foreach (var productInformationPhoto in productInformation.ProductInformationPhotos)
                                {
                                    if (piLoop.ProductInformationPhotos == null)
                                        productInformationPhoto.Deleted = true;
                                    else
                                    {
                                        var _productInformationPhoto = piLoop
                                            .ProductInformationPhotos
                                            .Where(x => x != null)
                                            .FirstOrDefault(x => x.Id == productInformationPhoto.Id);

                                        if (_productInformationPhoto == null)
                                            productInformationPhoto.Deleted = true;
                                        else
                                        {
                                            productInformationPhoto.DisplayOrder = _productInformationPhoto.DisplayOrder;

                                            if (_productInformationPhoto.FileName.StartsWith("data"))
                                            {
                                                var directory = $@"{this._settingService.ProductImagePath}\{productInformation.Id}";

                                                if (!Directory.Exists(directory))
                                                    Directory.CreateDirectory(directory);

                                                var fileName = $"{piLoop.ProductInformationTranslations[0].Name.GenerateUrl()}-{_randomHelper.GenerateNumbers(0, 100000)}.jpg";
                                                var filePath = $@"{this._settingService.ProductImagePath}\{productInformation.Id}\{fileName}";

                                                //var success = this._imageHelper.Save(_productInformationPhoto.FileName, filePath);
                                                var success = this._imageHelper.Save(_productInformationPhoto.FileName, filePath);
                                                if (!success)
                                                {
                                                    response.Message = "Resim Yüklenirken bir hata ile karşılaştınız. Doğru fotağraf formatında eklediğinizden emin olunuz.";
                                                    response.Success = false;
                                                    return;
                                                }

                                                productInformationPhoto.FileName = $"{productInformation.Id}/{fileName}";
                                                _productInformationPhoto.FileName = $"{productInformation.Id}/{fileName}";
                                            }
                                        }
                                    }
                                }

                                if (piLoop.ProductInformationPhotos != null)
                                    foreach (var productInformationPhoto in piLoop.ProductInformationPhotos)
                                    {
                                        if (productInformationPhoto != null && productInformationPhoto.FileName.StartsWith("data"))
                                        {
                                            var fileName = $"{piLoop.ProductInformationTranslations[0].Name.GenerateUrl()}-{_randomHelper.GenerateNumbers(0, 100000)}.jpg";
                                            var directory = $@"{this._settingService.ProductImagePath}\{productInformation.Id}";

                                            if (!Directory.Exists(directory))
                                                Directory.CreateDirectory(directory);

                                            var filePath = $@"{directory}\{fileName}";

                                            //this._imageHelper.Save(productInformationPhoto.FileName, filePath);
                                            this._imageHelper.Save(productInformationPhoto.FileName, filePath);

                                            productInformationPhoto.FileName = $"{productInformation.Id}/{fileName}";
                                            productInformation.ProductInformationPhotos.Add(new Domain.Entities.ProductInformationPhoto
                                            {
                                                FileName = $"{productInformation.Id}/{fileName}",
                                                DisplayOrder = productInformationPhoto.DisplayOrder,
                                                CreatedDate = DateTime.Now,
                                                ProductInformationId = piLoop.Id
                                            });
                                        }
                                    }

                                foreach (var productInformationPrice in productInformation.ProductInformationPriceses)
                                {
                                    var _productInformationPrice = piLoop.ProductInformationPriceses.FirstOrDefault(x => x.Id == productInformationPrice.Id);
                                    if (_productInformationPrice != null)
                                    {
                                        productInformationPrice.ConversionCurrencyId = _productInformationPrice.ConversionCurrencyId;
                                        productInformationPrice.ConversionUnitCost = _productInformationPrice.ConversionUnitCost;
                                        productInformationPrice.UnitPrice = _productInformationPrice.UnitPrice;
                                        productInformationPrice.VatRate = _productInformationPrice.VatRate;
                                        productInformationPrice.ListUnitPrice = _productInformationPrice.ListUnitPrice;

                                        if (string.IsNullOrEmpty(_productInformationPrice.ConversionCurrencyId))
                                        {
                                            productInformationPrice.ConversionCurrencyId = null;
                                            productInformationPrice.UnitCost = _productInformationPrice.UnitCost;
                                        }
                                        else
                                        {
#warning Statik kod yazıldı
                                            var exchangeRate = this
                                                ._unitOfWork
                                                .ExchangeRateRepository
                                                .DbSet()
                                                .FirstOrDefault(er => er.CreatedDate.Date == DateTime.Now.Date &&
                                                                      er.CurrencyId == _productInformationPrice.ConversionCurrencyId);

                                            productInformationPrice.UnitCost = _productInformationPrice.ConversionUnitCost * exchangeRate.Rate;
                                        }

                                    }
                                }

                                foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                                {
                                    var _productInformationTranslation = piLoop
                                        .ProductInformationTranslations
                                        .FirstOrDefault(x => x.Id == productInformationTranslation.Id);
                                    if (_productInformationTranslation != null)
                                    {
                                        if (productInformationTranslation.Name != _productInformationTranslation.Name ||
                                            productInformationTranslation.SubTitle != _productInformationTranslation?.SubTitle ||
                                            productInformationTranslation.ProductInformationContentTranslation.Description != _productInformationTranslation.ProductInformationContentTranslation?.Description)
                                        {
                                            this._productInformationProductService.MakeDirtyProductInformation(productInformation.Id);
                                        }

                                        if (productInformation.ProductInformationVariants?.Count(x => x.VariantValue.Variant.ShowVariant) > 0)
                                        {
                                            string variantValuesDescription = String.Join(
                                                ", ",
                                                productInformation
                                                    .ProductInformationVariants
                                                    .Where(x => x.VariantValue.Variant.ShowVariant)
                                                    .Select(piv => piv
                                                        .VariantValue
                                                        .VariantValueTranslations
                                                        .FirstOrDefault()?.Value.Trim()));
                                            productInformationTranslation.VariantValuesDescription = variantValuesDescription;


                                        }
                                        else
                                        {
                                            productInformationTranslation.VariantValuesDescription = null;
                                        }


                                        string fullVariantValuesDescription = String.Join(
                                            ", ",
                                            productInformation
                                                .ProductInformationVariants
                                                .Select(piv => $"{piv
                                                    .VariantValue
                                                    .Variant
                                                    .VariantTranslations.FirstOrDefault().Name.Trim()}: {piv
                                                    .VariantValue
                                                    .VariantValueTranslations.FirstOrDefault().Value.Trim()}"));
                                        productInformationTranslation.FullVariantValuesDescription = fullVariantValuesDescription;
                                        productInformationTranslation.Name = _productInformationTranslation.Name;
                                        productInformationTranslation.SubTitle = _productInformationTranslation?.SubTitle == null ?
                                            "" :
                                            _productInformationTranslation.SubTitle;

                                        productInformationTranslation.ProductInformationContentTranslation.Content = _productInformationTranslation.ProductInformationContentTranslation.Content;
                                        productInformationTranslation.ProductInformationContentTranslation.Description = _productInformationTranslation.ProductInformationContentTranslation?.Description == null ? "" : _productInformationTranslation.ProductInformationContentTranslation.Description;
                                        productInformationTranslation.ProductInformationSeoTranslation.Title = _productInformationTranslation?.ProductInformationSeoTranslation?.Title == null ? "" : _productInformationTranslation.ProductInformationSeoTranslation.Title;
                                        productInformationTranslation.ProductInformationSeoTranslation.MetaKeywords = _productInformationTranslation?.ProductInformationSeoTranslation?.MetaKeywords == null ? "" : _productInformationTranslation.ProductInformationSeoTranslation.MetaKeywords;
                                        productInformationTranslation.ProductInformationSeoTranslation.MetaDescription = _productInformationTranslation?.ProductInformationSeoTranslation?.MetaDescription == null ? "" : _productInformationTranslation.ProductInformationSeoTranslation.MetaDescription;

                                    }


                                }
                                productInformation.ProductInformationCombines = new List<Domain.Entities.ProductInformationCombine>();
                                if (piLoop.ProductInformationCombines != null && productInformation.Type == "PC") //ProductCombine
                                    productInformation.ProductInformationCombines.AddRange(piLoop.ProductInformationCombines.Select(x => new Domain.Entities.ProductInformationCombine
                                    {
                                        Id = x.Id,
                                        ContainProductInformationId = x.ContainProductInformationId,
                                        Quantity = x.Quantity
                                    }).ToList());
                            }
                        }
                    });

                //var photoableProductInformations = dto.ProductInformations.Where(pi => pi.Photoable);
                if (entity.ProductInformations.Any(pi => !pi.Photoable))
                {
                    var productInformations = entity.ProductInformations.Where(pi => !pi.Photoable);
                    foreach (var piLoop in productInformations)
                    {
                        var productInformation = entity
                            .ProductInformations
                            .FirstOrDefault(pi => pi.Photoable && pi.GroupId == piLoop.GroupId);

                        piLoop.ProductInformationPhotos = productInformation
                            .ProductInformationPhotos
                            .Select(pip => new Domain.Entities.ProductInformationPhoto
                            {
                                FileName = pip.FileName,
                                DisplayOrder = pip.DisplayOrder,
                                CreatedDate = DateTime.Now,
                                Deleted = false
                            })
                            .ToList();

                        this
                            ._unitOfWork
                            .ProductInformationRepository
                            .Update(piLoop);
                    }
                }

                entity.ProductTranslations = new List<Domain.Entities.ProductTranslation>();

                dto.ProductTranslations.ForEach(ppLoop =>
                {
                    entity.ProductTranslations.Add(new Domain.Entities.ProductTranslation
                    {
                        Name = ppLoop.Name,
                        LanguageId = ppLoop.LanguageId,
                        CreatedDate = DateTime.Now,

                    });
                });


                entity.ProductLabels = new List<Domain.Entities.ProductLabel>();

                dto.ProductLabels.ForEach(ppLoop =>
                {
                    entity.ProductLabels.Add(new Domain.Entities.ProductLabel
                    {
                        Active = true,
                        LabelId = ppLoop.LabelId,
                        CreatedDate = DateTime.Now,

                    });
                });

                entity
                    .ProductProperties = new List<Domain.Entities.ProductProperty>();
                dto
                    .ProductProperties
                    .ForEach(ppLoop =>
                    {
                        entity.ProductProperties.Add(new Domain.Entities.ProductProperty
                        {
                            Active = true,
                            CreatedDate = DateTime.Now,
                            PropertyValueId = ppLoop.PropertyValueId
                        });
                    });

                entity.ProductInformations.AddRange(dto
                    .ProductInformations
                    .Where(pi => pi != null && pi.Id == 0)
                    .Select(pi => new Domain.Entities.ProductInformation
                    {
                        Id = pi.Id,
                        Barcode = pi.Barcode,
                        Active = pi.Active,
                        Stock = pi.Stock,
                        IsSale = pi.IsSale,
                        Payor = pi.Payor,
                        StockCode = pi.StockCode,
                        ShelfCode = pi.ShelfCode,
                        SkuCode = pi.SkuCode,
                        CreatedDate = DateTime.Now,
                        GroupId = pi.GroupId,
                        Photoable = pi.Photoable,
                        ProductInformationPriceses = pi
                            .ProductInformationPriceses
                            .Select(pip => new Domain.Entities.ProductInformationPrice
                            {
                                Id = pip.Id,
                                CreatedDate = DateTime.Now,
                                CurrencyId = pip.CurrencyId,
                                ListUnitPrice = pip.ListUnitPrice,
                                UnitCost = pip.UnitCost,
                                UnitPrice = pip.UnitPrice,
                                VatRate = pip.VatRate,
                                ConversionUnitCost = pip.ConversionUnitCost,
                                ConversionCurrencyId = !string.IsNullOrEmpty(pip.ConversionCurrencyId) ? pip.ConversionCurrencyId : null
                            })
                            .ToList(),
                        ProductInformationVariants = pi
                            .ProductInformationVariants
                            .Select(pv => new Domain.Entities.ProductInformationVariant
                            {
                                Id = pv.Id,
                                VariantValueId = pv.VariantValueId,

                            })
                            .ToList(),
                        ProductInformationPhotos = pi.Photoable && pi
                                 .ProductInformationPhotos != null ? pi
                                 .ProductInformationPhotos
                                 .Select(pip => new Domain.Entities.ProductInformationPhoto
                                 {
                                     FileName = pip.FileName,
                                     DisplayOrder = pip.DisplayOrder,
                                     CreatedDate = DateTime.Now,
                                     Deleted = false
                                 }).ToList() : null,
                        ProductInformationTranslations = pi
                        .ProductInformationTranslations
                        .Select(pit => new Domain.Entities.ProductInformationTranslation
                        {
                            VariantValuesDescription = pi.ProductInformationVariants?.Count(x => x.VariantValue.Variant.ShowVariant) > 0 ?
                                $"({String.Join(
                                        ", ",
                                        pi
                                            .ProductInformationVariants
                                            .Where(x => x.VariantValue.Variant.ShowVariant)
                                            .Select(piv => piv
                                                .VariantValue
                                                .VariantValueTranslations
                                                .FirstOrDefault()?.Value.Trim()))})" :
                                    null,
                            FullVariantValuesDescription = pi.ProductInformationVariants?.Count() > 0 ?
                                            String.Join(
                                                 ", ",
                                                 pi.ProductInformationVariants
                                                     .Select(piv => $"{piv
                                                         .VariantValue
                                                         .Variant
                                                         .VariantTranslations.FirstOrDefault().Name.Trim()}: {piv
                                                         .VariantValue
                                                         .VariantValueTranslations.FirstOrDefault().Value.Trim()}")) :
                                    null,
                            LanguageId = pit.LanguageId,
                            Name = pit.Name,
                            SubTitle = pit.SubTitle == null ? "" : pit.SubTitle,
                            Url = $"",
                            ProductInformationSeoTranslation = new Domain.Entities.ProductInformationSeoTranslation
                            {
                                Title = pit?.ProductInformationSeoTranslation?.Title,
                                MetaDescription = pit?.ProductInformationSeoTranslation?.MetaDescription,
                                MetaKeywords = pit?.ProductInformationSeoTranslation?.MetaKeywords
                            },
                            ProductInformationContentTranslation = new Domain.Entities.ProductInformationContentTranslation
                            {
                                Content = pit?.ProductInformationContentTranslation?.Content,
                                Description = pit?.ProductInformationContentTranslation?.Description,
                            },
                            ProductInformationTranslationBreadcrumb = new Domain.Entities.ProductInformationTranslationBreadcrumb
                            {
                                Html = "",

                            }

                        }).ToList()
                    })
                    .ToList());

                response.Success = this
                    ._unitOfWork
                    .ProductRepository
                    .Update(entity);


                var photoableProductInformations = entity.ProductInformations.Where(pi => pi.Photoable);
                foreach (var ppiLoop in photoableProductInformations)
                {
                    if (ppiLoop.ProductInformationPhotos == null) continue;
                    foreach (var productInformationPhoto in ppiLoop.ProductInformationPhotos)
                    {
                        if (productInformationPhoto != null && productInformationPhoto.FileName.StartsWith("data"))
                        {
                            var directory = $@"{this._settingService.ProductImagePath}\{ppiLoop.Id}";

                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);

                            var fileName = $"{ppiLoop.ProductInformationTranslations[0].Name.GenerateUrl()}-{_randomHelper.GenerateNumbers(0, 100000)}.jpg";
                            var filePath = $@"{directory}\{fileName}";

                            //this._imageHelper.Save(productInformationPhoto.FileName, filePath);
                            this._imageHelper.Save(productInformationPhoto.FileName, filePath);

                            productInformationPhoto.FileName = $"{ppiLoop.Id}/{fileName}";

                            this
                                ._unitOfWork
                                .ProductInformationPhotoRepository
                                .Update(productInformationPhoto);
                        }
                    }
                }

                if (entity.ProductInformations.Any(pi => !pi.Photoable))
                {
                    var productInformations = entity.ProductInformations.Where(pi => !pi.Photoable);
                    foreach (var piLoop in productInformations)
                    {
                        var productInformation = entity
                            .ProductInformations
                            .FirstOrDefault(pi => pi.Photoable && pi.GroupId == piLoop.GroupId);

                        piLoop.ProductInformationPhotos = productInformation
                            .ProductInformationPhotos
                            .Select(pip => new Domain.Entities.ProductInformationPhoto
                            {
                                FileName = pip.FileName,
                                DisplayOrder = pip.DisplayOrder,
                                CreatedDate = DateTime.Now,
                                Deleted = false
                            })
                            .ToList();

                        this
                            ._unitOfWork
                            .ProductInformationRepository
                            .Update(piLoop);
                    }
                }

                foreach (var productInformation in entity.ProductInformations)
                {

                    foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                    {
                        if (string.IsNullOrEmpty(productInformationTranslation.Url))
                        {
                            productInformationTranslation.Url = $"{productInformationTranslation.Name.GenerateUrl()}-{productInformation.Id}-p";
                            this._unitOfWork.ProductInformationTranslationRepository.Update(productInformationTranslation);
                        }


                        var html = new StringBuilder();
                        html.AppendLine("<ul>");
                        var products = RecursiveCategory(categories, dto.CategoryId);
                        products.Reverse();
                        foreach (var pLoop in products)
                        {
                            html.AppendLine(pLoop);
                        }

                        var productInformationTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(productInformationTranslation.Name.ToLower().ReplaceChar());
                        html.AppendLine($"<li>{productInformationTranslationName}</li>");

                        html.AppendLine("</ul>");
                        var productInformationTranslationBreadcrumb = this._unitOfWork.ProductInformationTranslationBreadcrumbRepository.Read(productInformationTranslation.Id);
                        if (productInformationTranslationBreadcrumb != null)
                        {
                            productInformationTranslationBreadcrumb.Html = html.ToString();
                            this._unitOfWork.ProductInformationTranslationBreadcrumbRepository.Update(productInformationTranslationBreadcrumb);
                        }

                    }
                }

                response.Message = response.Success ? "Ürün güncellendi." : "İşlem hatalı";
            }, (response, exception) => { });
        }

        public Response Deleted(int id)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                using (conn = new SqlConnection(this._settingService.ConnectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    // Updating destination table, and dropping temp table
                    command.CommandTimeout = 300;
                    command.CommandText = @"
        Update  Products Set IsOnlyHidden=1 Where Id=@ProductId And DomainId=@DomainId
        
        Update  ProductInformations Set Barcode=NEWID(),StockCode=NEWID(),Active=0 Where ProductId=@ProductId And DomainId=@DomainId
        
        Delete  [PIM]
        From    ProductInformationMarketplaces As [PIM] 
        Join    ProductInformations As [PIN]
        On      [PIM].ProductInformationId = [PIN].Id
        Where   PIN.ProductId=@ProductId 
        And     [PIM].DomainId=@DomainId
        And     [PIM].CompanyId=@CompanyId
        
        Delete  ProductMarketplaces 
        Where   ProductId=@ProductId 
        And     DomainId=@DomainId
        And     CompanyId=@CompanyId";

                    command.Parameters.AddWithValue("@ProductId", id);
                    command.Parameters.AddWithValue("@CompanyId", _companyFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", _domainFinder.FindId());


                    command.ExecuteNonQuery();
                }

                response.Success = true;
                response.Message = "Ürün silme işlemi gerçekleştirildi.";
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn.Close();
            });
        }


        /// <summary>
        /// Gönderilen istek içerisinde bulunan varyasyon değerlerinin hesaplanması sonucunu oluşan kombinasyonları dönen metod.
        /// </summary>
        /// <param name="productInformationPartialRequest"></param>
        /// <returns></returns>
        public DataResponse<List<ProductInformation>> GenerateProductInformations(ProductInformationPartialRequest productInformationPartialRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductInformation>>>((response) =>
            {
                if (productInformationPartialRequest.Variants == null)
                    response.Data = new List<ProductInformation>
                    {
                        new ProductInformation
                        {
                            Active= true,
                            Photoable = true,
                            IsSale = true,
                            GroupId = Guid.NewGuid().ToString(),
                            ProductInformationVariants = new List<ProductInformationVariant>()
                        }
                    };
                else
                {
                    response.Data = new();

                    productInformationPartialRequest.Variants = productInformationPartialRequest
                        .Variants
                        .OrderByDescending(v => v.VariantValues.Count)
                        .ToList();

                    var productInformations = GenerateProductInformationsPrivate(productInformationPartialRequest);

                    /*
                     * Eğer kombinasyonlar içerisinde fotoğraflanabilir değere sahip varyasyonlar var ise sadece fotoğraflanabilir varyasyonlardan oluşan bir kombinasyon daha hesaplanır. Hesaplanan bu kombinasyon bize hangi varyasyonlar için kaç adet fotoğraf yükleneceği bilgisini elde etme fırsatı verir. Oluşan iki kombinasyon kıyaslanarak uygun olanlara fotoğraflanabilir değeri olarak true atanır.
                     * 
                     * Eğer kombinasyonlar içerisinde fotoğraflanabilir değere sahip varyasyon/varyasyonlar bulunamaz ise herhangi bir hesaplama yapılmadan tüm kombinasyon ögelerine fotoğraflanabilir değeri olarak true atanır.
                     */
                    if (productInformationPartialRequest.Variants.Any(v => v.Photoable))
                    {
                        var photoableProductInformations = GenerateProductInformationsPrivate(new ProductInformationPartialRequest
                        {
                            Variants = productInformationPartialRequest
                                .Variants
                                .Where(v => v.Photoable)
                                .ToList()
                        });

                        foreach (var ppiLoop in photoableProductInformations)
                        {
                            var photoableVariantValueIds = ppiLoop
                                .ProductInformationVariants
                                .Select(pv => pv.VariantValueId);

                            var foundProductInformations = productInformations
                                .Where(cpi =>
                                {
                                    return !photoableVariantValueIds
                                        .Except(cpi.ProductInformationVariants.Select(pv => pv.VariantValueId))
                                        .Any();
                                });

                            var foundProductInformation = foundProductInformations
                                .FirstOrDefault();

                            if (foundProductInformation != null)
                            {
                                var groupId = Guid.NewGuid().ToString();

                                foreach (var fpiLoop in foundProductInformations)
                                {
                                    fpiLoop.GroupId = groupId;
                                }

                                foundProductInformation.Photoable = true;
                            }
                        }
                    }
                    else
                        foreach (var piLoop in productInformations)
                            piLoop.Photoable = true;

                    if (productInformationPartialRequest.ProductInformations?.Count > 0)
                    {
                        foreach (var piLoop in productInformations)
                        {
                            if (productInformationPartialRequest.ProductInformations.Any(pi => pi.ToString() == piLoop.ToString()))
                            {
                                var existProductInformation = productInformationPartialRequest
                                    .ProductInformations
                                    .First(pi => pi.ToString() == piLoop.ToString());
                                existProductInformation.GroupId = piLoop.GroupId;
                                response.Data.Add(existProductInformation);
                            }
                            else
                                response.Data.Add(piLoop);
                        }
                    }
                    else
                        response.Data = productInformations.ToList();


                }

                response.Success = true;
                response.Message = "Kombinasyonlar başarılı şekilde oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                     ._unitOfWork
                     .ProductRepository
                     .DbSet()
                     .Select(p => new KeyValue<int, string>
                     {
                         Key = p.Id,
                         Value = p.ProductInformations.FirstOrDefault().ProductInformationTranslations.FirstOrDefault().Name
                     })
                     .ToList();
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(int take, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this
                     ._unitOfWork
                     .ProductInformationRepository
                     .DbSet()
                     .Include(pi => pi.ProductInformationTranslations)
                     .Where(pi => pi.ProductInformationTranslations.Any(pt => pt.Name.Contains(q)))
                     .Take(take)
                     .Select(pi => new KeyValue<string, string>
                     {
                         Key = $"{pi.ProductId}-{pi.Id}",
                         Value = $"{pi.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name} {(pi.ProductInformationVariants.Count == 0 ? "" : $"({string.Join(", ", pi.ProductInformationVariants.Select(piv => piv.VariantValue.VariantValueTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Value))})")}"
                     })
                     .ToList();
                response.Success = response.Data?.Count > 0;
            });
        }

        public DataResponse<Domain.Entities.Product> ReadInfo(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Domain.Entities.Product>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Include(p => p.ProductLabels)
                    .ThenInclude(x => x.Label.LabelTranslations)
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationTranslations)
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationPriceses)
                    .Include(p => p.ProductInformations)
                    .ThenInclude(pi => pi.ProductInformationPhotos)
                    .FirstOrDefault(p => p.Id == id);
                response.Success = true;
            });
        }

        public DataResponse<List<ProductInformation>> GenerateProductInformationMarketplaces(int id, List<Marketplace> marketPlaces)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductInformation>>>((response) =>
            {
                Stopwatch stopwatch2 = Stopwatch.StartNew();


                var entities = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationMarketplaces.Where(pim => pim.CompanyId == this._companyFinder.FindId()))
                    //.ThenInclude(x => x.MarketplaceCompany)
                    .Where(pi => pi.ProductId == id)
                    .ToList();

                var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository
                    .DbSet()
                   .Include(pim => pim.ProductInformationMarketplaceVariantValues)
                   //.ThenInclude(pimv => pimv.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceVariantValue)
                   //.ThenInclude(mvv => mvv.MarketplaceVariantsMarketplaceVariantValues)
                   //.Include(pim => pim.ProductInformationMarketplaceVariantValues)
                   //.ThenInclude(pimv => pimv.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant)
                   .Where(x => x.ProductInformation.ProductId == id)
                   .ToList();

                var productInformationVariants = _unitOfWork.ProductInformationVariantRepository
                    .DbSet()
                    .Include(x => x.VariantValue.VariantValueTranslations)
                    .Include(x => x.VariantValue.Variant.VariantTranslations)
                    .Where(x => x.ProductInformation.ProductId == id)
                    .ToList();

                stopwatch2.Stop();
                if (entities == null || entities.Count == 0)
                {
                    response.Message = "Ürün detay bilgileri bulunamadı.";
                    response.Success = false;
                    return;
                }

                response.Data = new();

                foreach (var eLoop in entities)
                {
                    var _productInformationMarketplaces = new List<ProductInformationMarketplace>();
                    _productInformationMarketplaces.AddRange(productInformationMarketplaces
                                            .Where(pim => pim.ProductInformationId == eLoop.Id)
                                            .Select(pim => new ProductInformationMarketplace
                                            {
                                                UUId = pim.UUId,
                                                Id = pim.Id,
                                                MarketplaceId = pim.MarketplaceId,
                                                //MarketplaceCompanyId = pim.MarketplaceCompanyId,
                                                ProductInformationId = pim.ProductInformationId,
                                                StockCode = pim.StockCode,
                                                Barcode = pim.Barcode,
                                                ListUnitPrice = pim.ListUnitPrice,
                                                UnitPrice = pim.UnitPrice,
                                                Active = pim.Active,
                                                AccountingPrice = pim.AccountingPrice,
                                                ProductInformationMarketplaceVariantValues = pim
                                                    .ProductInformationMarketplaceVariantValues
                                                    .Select(pimv => new ProductInformationMarketplaceVariantValue
                                                    {
                                                        //Id = pimv.Id,
                                                        //MarketplaceVariantId = pimv
                                                        //    .MarketplaceCategoriesMarketplaceVariantValue
                                                        //    .MarketplaceCategoriesMarketplaceVariant
                                                        //    .MarketplaceVariantId,
                                                        //MarketplaceCategoriesMarketplaceVariantValueId = pimv.MarketplaceCategoriesMarketplaceVariantValueId,
                                                        //MarketplaceVariantValue = pimv
                                                        //    .MarketplaceCategoriesMarketplaceVariantValue
                                                        //    .MarketplaceVariantValue
                                                        //    .Value
                                                    })
                                                    .ToList()
                                            })
                                            .ToList());

                    foreach (var mpLoop in marketPlaces)
                        if (!_productInformationMarketplaces.Any(d => d.MarketplaceId == mpLoop.Id))
                            _productInformationMarketplaces.Add(new ProductInformationMarketplace
                            {
                                Id = 0,
                                UUId = Guid.NewGuid().ToString().ToLower(),
                                MarketplaceId = mpLoop.Id,
                                //MarketplaceCompanyId = mpLoop.MarketplaceCompanyId,
                                ProductInformationId = eLoop.Id,
                                StockCode = eLoop.StockCode,
                                Barcode = eLoop.Barcode,

                                ListUnitPrice = eLoop.ProductInformationPriceses.FirstOrDefault(pip => pip.CurrencyId == "TL").ListUnitPrice,
                                UnitPrice = eLoop.ProductInformationPriceses.FirstOrDefault(pip => pip.CurrencyId == "TL").UnitPrice,
                                Active = false
                            });

                    var _productInformationVariants = productInformationVariants.Where(x => x.ProductInformationId == eLoop.Id).ToList();

                    response.Data.Add(new ProductInformation
                    {
                        Id = eLoop.Id,
                        ProductInformationName = $"{eLoop.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name} {(_productInformationVariants.Count == 0 ? "" : $"({string.Join(", ", _productInformationVariants.Select(piv => piv.VariantValue.VariantValueTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Value))})")}",
                        ProductInformationMarketplaces = _productInformationMarketplaces,
                        ProductInformationVariants = _productInformationVariants
                            .Select(piv => new ProductInformationVariant
                            {
                                Id = piv.Id,
                                VariantValueId = piv.VariantValueId,
                                VariantValue = new VariantValue
                                {
                                    Id = piv.VariantValue.Id,
                                    VariantId = piv.VariantValue.VariantId,
                                    Variant = new Variant
                                    {
                                        VariantTranslations = piv.VariantValue.Variant.VariantTranslations.Select(vvt => new VariantTranslation
                                        {
                                            Name = vvt.Name
                                        }).ToList()
                                    },
                                    VariantValueTranslations = piv
                                        .VariantValue
                                        .VariantValueTranslations
                                        .Select(vvt => new VariantValueTranslation
                                        {
                                            Value = vvt.Value,
                                        })
                                        .ToList()
                                }
                            })
                            .ToList()
                    });
                }

                response.Success = true;
            });
        }

        public Response UpsertProductMarketplace(Product product)
        {
            if (product.ProductInformations.Any(pi => pi.ProductInformationMarketplaces.Any(pim => pim.ListUnitPrice < pim.UnitPrice)))
                return new Response { Success = false, Message = "Satış fiyatı, liste fiyatından büyük olamaz." };

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var companyId = this._companyFinder.FindId();

                var productEntity = this
                        ._unitOfWork
                        .ProductRepository
                        .DbSet()
                        .Include(p => p.ProductMarketplaces.Where(x => x.CompanyId == companyId))
                        .Include(p => p.ProductMarketplaces.Where(x => x.CompanyId == companyId))
                        .FirstOrDefault(p => p.Id == product.Id);

                if (productEntity.ProductMarketplaces == null)
                    productEntity.ProductMarketplaces = new();

                var productMarketplaceMarketplaceIds = product.ProductMarketplaces.Select(pm => pm.MarketplaceId).ToList();
                productEntity
                    .ProductMarketplaces
                    .RemoveAll(pm => productMarketplaceMarketplaceIds.Contains(pm.MarketplaceId));
                productEntity
                    .ProductMarketplaces
                    .ForEach(pm => pm.Active = false);

                product.ProductMarketplaces.ForEach(pmLoop =>
                {
                    var marketplaceCompany = this
                        ._unitOfWork
                        .MarketplaceCompanyRepository
                        .DbSet()
                        .FirstOrDefault(mc => mc.MarketplaceId == pmLoop.MarketplaceId);

                    if (!string.IsNullOrEmpty(pmLoop.MarketplaceCategoryCode))
                        productEntity.ProductMarketplaces.Add(new Domain.Entities.ProductMarketplace
                        {
                            UUId = pmLoop.UUId,
                            Active = pmLoop.Active,
                            ProductId = product.Id,
                            SellerCode = pmLoop.SellerCode,
                            MarketplaceBrandCode = pmLoop.MarketplaceBrandCode,
                            MarketplaceBrandName = pmLoop.MarketplaceBrandName,
                            //MarketplaceCompanyId = marketplaceCompany.Id,
                            MarketplaceCategoryCode = pmLoop.MarketplaceCategoryCode,
                            DeliveryDay = pmLoop.DeliveryDay

                        });
                });

                this._unitOfWork.ProductRepository.Update(productEntity);

                product.ProductInformations.ForEach(d =>
                {
                    var entity = this
                        ._unitOfWork
                        .ProductInformationRepository
                        .DbSet()
                        .Include(pi => pi.ProductInformationMarketplaces.Where(x => x.CompanyId == companyId))
                        .FirstOrDefault(pi => pi.Id == d.Id);

                    //var marketplaceCompanyIds = d.ProductInformationMarketplaces.Select(pim => pim.MarketplaceCompanyId).ToList();
                    //var willBeDeletedProductInformationMarketplaces = entity
                    //    .ProductInformationMarketplaces
                    //    .Where(x => marketplaceCompanyIds.Contains(x.MarketplaceCompanyId))
                    //    .ToList();
                    //entity
                    //    .ProductInformationMarketplaces
                    //    .RemoveAll(x => marketplaceCompanyIds.Contains(x.MarketplaceCompanyId));
                    //entity
                    //    .ProductInformationMarketplaces
                    //    .ForEach(pim => pim.Active = false);

                    //entity.ProductInformationMarketplaces.AddRange(d
                    //    .ProductInformationMarketplaces
                    //    .Select(pim =>
                    //    {
                    //        var onSale = false;
                    //        var opened = false;
                    //        var locked = false;
                    //        var url = string.Empty;

                    //        var willBeDeletedProductInformationMarketplace = willBeDeletedProductInformationMarketplaces
                    //                .FirstOrDefault(x => x.MarketplaceCompanyId == pim.MarketplaceCompanyId);

                    //        if (willBeDeletedProductInformationMarketplace != null)
                    //        {
                    //            opened = willBeDeletedProductInformationMarketplace.Opened;
                    //            onSale = willBeDeletedProductInformationMarketplace.OnSale;
                    //            locked = willBeDeletedProductInformationMarketplace.Locked;
                    //            url = willBeDeletedProductInformationMarketplace.Url;
                    //        }

                    //        return new Domain.Entities.ProductInformationMarketplace
                    //        {
                    //            Opened = opened,
                    //            OnSale = onSale,
                    //            Locked = locked,

                    //            Url = url,
                    //            UUId = pim.UUId,
                    //            Active = pim.Active,
                    //            StockCode = pim.StockCode,
                    //            Barcode = pim.Barcode,
                    //            ListUnitPrice = pim.ListUnitPrice,
                    //            UnitPrice = pim.UnitPrice,
                    //            CreatedDate = DateTime.Now,
                    //            DirtyProductInformation = true,
                    //            DirtyPrice = pim.Id > 0 ? true : false,
                    //            ,
                    //            MarketplaceCompanyId = pim.MarketplaceCompanyId,
                    //            ProductInformationId = pim.ProductInformationId,
                    //            AccountingPrice = pim.AccountingPrice,
                    //            ProductInformationMarketplaceVariantValues = pim
                    //                    .ProductInformationMarketplaceVariantValues
                    //                    .Where(x => x.MarketplaceCategoriesMarketplaceVariantValueId > 0)
                    //                    .Select(pimcvv => new Domain.Entities.ProductInformationMarketplaceVariantValue
                    //                    {
                    //                        CreatedDate = DateTime.Now,
                    //                        //,
                    //                        //MarketplaceCategoriesMarketplaceVariantValueId = pimcvv.MarketplaceCategoriesMarketplaceVariantValueId
                    //                    })
                    //                    .ToList()
                    //        };
                    //    })
                    //    .ToList());

                    response.Success = this
                        ._unitOfWork
                        .ProductInformationRepository
                        .Update(entity);
                });

                response.Success = true;
                response.Message = "Kayıtlar başarılı bir şekilde güncellendi.";
            });
        }

        public DataResponse<MemoryStream> Download(Application.Panel.Parameters.BulkPriceUpdates.FilterRequest filterRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPriceses)
                    .Where(pi => pi.Product.CategoryProducts.Any(cp => cp.CategoryId == filterRequest.CategoryId))
                    .ToList();

                Excel excel = new()
                {
                    Header = new() { Cells = new() }
                };

                foreach (var cLoop in new[] { "Fiyat No", "Ürün Adı", "Maliyet", "Liste Fiyatı", "Fiyat", "KDV Oranı" })
                    excel.Header.Cells.Add(new() { CellType = CellType.String, Value = cLoop });

                excel.Rows = entities
                    .Select(e => new Row
                    {
                        Cells = new List<Cell>
                        {
                            new Cell { CellType = CellType.Number, Value = e.ProductInformationPriceses.FirstOrDefault().Id },
                            new Cell { CellType = CellType.String, Value = e.ProductInformationTranslations.FirstOrDefault().Name },
                            new Cell { CellType = CellType.Number, Value = e.ProductInformationPriceses.FirstOrDefault().UnitCost },
                            new Cell { CellType = CellType.Number, Value = e.ProductInformationPriceses.FirstOrDefault().ListUnitPrice },
                            new Cell { CellType = CellType.Number, Value = e.ProductInformationPriceses.FirstOrDefault().UnitPrice },
                            new Cell { CellType = CellType.Number, Value = e.ProductInformationPriceses.FirstOrDefault().VatRate }
                        }
                    })
                    .ToList();

                var memoryStream = this._excelHelper.Write(excel);
                memoryStream.Position = 0;

                response.Data = memoryStream;
            });
        }

        public DataResponse<MemoryStream> DownloadMarketplace()
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                var marketplaces = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Select(mc => new Marketplace
                    {
                        Id = mc.MarketplaceId,
                        Name = mc.Marketplace.Name
                    })
                    .ToList();

                var bulkMarketplaces = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Select(p => new BulkMarketplace
                    {
                        ProductInformationId = p.Id,
                        ProductInformationName = $"{p.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Name} {p.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").VariantValuesDescription}",
                        SellerCode = p.Product.SellerCode,
                        SkuCode = p.SkuCode,
                        StockCode = p.StockCode,
                        Barcode = p.Barcode,
                        //MarketplaceIds = p.ProductInformationMarketplaces.Select(pim => pim.MarketplaceCompany.MarketplaceId).ToList()
                    })
                    .ToList();

                Excel excel = new()
                {
                    Header = new() { Cells = new() }
                };

                var columns = new List<string> { "Id", "Ürün Adı", "Model Kodu", "Sku", "Stok Kodu", "Barcode" };

                foreach (var theMarketplace in marketplaces)
                    columns.AddRange(new[] { theMarketplace.Name });

                foreach (var cLoop in columns)
                    excel.Header.Cells.Add(new() { CellType = CellType.String, Value = cLoop });

                excel.Rows = new();

                foreach (var theBulkMarketplace in bulkMarketplaces)
                {
                    var cells = new List<Cell>
                    {
                        new Cell { CellType = CellType.Number, Value = theBulkMarketplace.ProductInformationId },
                        new Cell { CellType = CellType.String, Value = theBulkMarketplace.ProductInformationName },
                        new Cell { CellType = CellType.String, Value = theBulkMarketplace.SellerCode },
                        new Cell { CellType = CellType.String, Value = theBulkMarketplace.SkuCode },
                        new Cell { CellType = CellType.String, Value = theBulkMarketplace.StockCode },
                        new Cell { CellType = CellType.String, Value = theBulkMarketplace.Barcode }
                    };

                    //foreach (var theMarketplace in marketplaces)
                    //{
                    //    cells.AddRange(new[] {
                    //        new Cell {
                    //            CellType = CellType.String,
                    //            Value = theBulkMarketplace.MarketplaceIds.Contains(theMarketplace.Id) ? "Evet" : "Hayır"
                    //        }
                    //    });
                    //}

                    excel.Rows.Add(new() { Cells = cells });
                }

                var memoryStream = this._excelHelper.Write(excel);
                memoryStream.Position = 0;

                response.Data = memoryStream;
            });
        }

        public Response UpdateBulkPrice(IFormFile formFile)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var excel = this._excelHelper.Read(formFile.OpenReadStream());

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
                dataTable.Columns.Add(new DataColumn("UnitCost", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ListUnitPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("VatRate", typeof(decimal)));

                foreach (var rLoop in excel.Rows)
                {
                    var row = dataTable.NewRow();
                    row["Id"] = Convert.ToInt32(rLoop.Cells[0].Value);
                    row["UnitCost"] = Convert.ToDecimal(rLoop.Cells[2].Value);
                    row["ListUnitPrice"] = Convert.ToDecimal(rLoop.Cells[3].Value);
                    row["UnitPrice"] = Convert.ToDecimal(rLoop.Cells[4].Value);
                    row["VatRate"] = Convert.ToDecimal(rLoop.Cells[5].Value);

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._settingService.ConnectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    //Creating temp table on database
                    command.CommandText = @"
Create Table    #TmpTable (
    Id int Not Null, 
    UnitCost decimal(18, 2) Not Null, 
    ListUnitPrice decimal(18, 2) Not Null, 
    UnitPrice decimal(18, 2) Not Null, 
    VatRate decimal(18, 2) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    //Bulk insert into temp table
                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTable";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    // Updating destination table, and dropping temp table
                    command.CommandTimeout = 300;
                    command.CommandText = @"
Update      T 
Set         T.UnitCost = Temp.UnitCost,
            T.ListUnitPrice = Temp.ListUnitPrice,            
            T.UnitPrice = Temp.UnitPrice, 
            T.VatRate = Temp.VatRate 
From        ProductInformationPriceses T 
Inner Join  #TmpTable Temp 
On          T.Id = Temp.Id;

Drop Table  #TmpTable;";
                    command.ExecuteNonQuery();
                }

                response.Success = true;
                response.Message = "Fiyatlar başarılı bir şekilde güncellendi.";
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn.Close();
            });
        }

        public Response UpdateBulkMarketplace(IFormFile formFile)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var excel = this._excelHelper.Read(formFile.OpenReadStream());

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("MarketplaceName", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Active", typeof(bool)));

                foreach (var theRow in excel.Rows)
                {
                    int columnIndex = 6;
                    while (theRow.Cells.Count > columnIndex)
                    {
                        var row = dataTable.NewRow();
                        row["ProductInformationId"] = Convert.ToInt32(theRow.Cells[0].Value.ToString().Replace(".0", ""));
                        row["MarketplaceName"] = excel.Header.Cells[columnIndex].Value.ToString();
                        row["Active"] = theRow.Cells[columnIndex].Value.ToString() == "Evet";

                        dataTable.Rows.Add(row);

                        columnIndex++;
                    }


                }

                using (conn = new SqlConnection(this._settingService.ConnectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    //Creating temp table on database
                    command.CommandText = @"
Create Table #TmpTableBulkMarketplace (
    ProductInformationId int Not Null,
    MarketplaceName NVarChar(1000) Not Null,
    Active Bit Not Null
)";
                    var i = command.ExecuteNonQuery();

                    //Bulk insert into temp table
                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkMarketplace";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    // Updating destination table, and dropping temp table
                    command.CommandTimeout = 300;
                    command.CommandText = @"
Update	PIM
Set		PIM.Active = T.Active
From	#TmpTableBulkMarketplace As T
Join	ProductInformations As[PI]
On		T.ProductInformationId = [PI].Id
Join	ProductInformationMarketplaces As PIM
On		PIM.ProductInformationId = [PI].Id
		And PIM.CompanyId = @CompanyId
Join	MarketplaceCompanies As MC
On		PIM.MarketplaceCompanyId = MC.Id
Join	Marketplaces As M
On		MC.MarketplaceId = M.Id
        And M.Name = T.MarketplaceName

Drop Table  #TmpTableBulkMarketplace;";
                    command.Parameters.AddWithValue("@CompanyId", _companyFinder.FindId());
                    command.ExecuteNonQuery();
                }

                response.Success = true;
                response.Message = "Fiyatlar başarılı bir şekilde güncellendi.";
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                if (conn != null && conn.State != ConnectionState.Closed)
                    conn.Close();
            });
        }

        public DataResponse<long> MaxBarcode()
        {
            return ExceptionHandler.ResultHandle<DataResponse<long>>((response) =>
            {
                response.Data = _unitOfWork.ProductInformationRepository.DbSet().Max(x => Convert.ToInt64(x.Barcode));

                response.Success = true;
                response.Message = "Barkod başarılı şekilde getirildi.";
            }, (response, exception) =>
            {

                var rnd = new Random();

                response.Success = true;
                response.Data = rnd.NextInt64(10000000000, 99999999999);
            });
        }

        public DataResponse<int> MaxStockCode()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                response.Data = _unitOfWork.ProductInformationRepository.DbSet().Max(x => Convert.ToInt32(x.StockCode));

                response.Success = true;
                response.Message = "Kayıtlar başarılı bir şekilde güncellendi.";
            }, (response, exception) =>
            {

                var rnd = new Random();

                response.Success = true;
                response.Data = rnd.Next(10000, 99999);
            });
        }

        public DataResponse<string> ReadSellerCode(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                var product = _unitOfWork.ProductRepository.DbSet().FirstOrDefault(p => p.Id == id);
                if (product == null)
                    return;

                response.Data = product.SellerCode;
                response.Success = true;

            }, (response, exception) => { });
        }

        public DataResponse<List<Last12MonthSale>> Last12MonthSales(List<string> list)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Last12MonthSale>>>((response) =>
            {

                list.ForEach(l => l.CleanParameter());

                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    var query = @"
Select		PN.SkuCode,
			Cast(DatePart(Year, O.OrderDate) As NVarChar(5)) + '-' + Cast(DatePart(Month, O.OrderDate) As NVarChar(2)) As [Date],
			Sum(OD.Quantity) As Quantity
From		Orders As O With(NoLock)
Join		OrderDetails As OD With(NoLock)
On			O.Id = OD.OrderId
Join		ProductInformations As PN With(NoLock)
On			OD.ProductInformationId = PN.Id
			And PN.SkuCode In @Codes
Where		O.OrderTypeId <> 'IP'
			And O.OrderTypeId <> 'IA'
			And Cast(O.OrderDate As Date) >= DateAdd(Month, -11, DateAdd(Month, DateDiff(Month, 0, GetDate()), 0))
Group By	PN.SkuCode,
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)
Order By	PN.SkuCode,
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)";
                    query = @"
With T As (
	Select	*,
			Row_Number() Over(Partition By SkuCode Order By SkuCode, CreatedDate Desc) As RN,
			Row_Number() Over(Partition By SkuCode Order By SkuCode, CreatedDate Desc) - 1 As PRN
	From	ProductCategorizationHistories
	Where	SkuCode In @Codes
), LogCte As (
	
	Select	RN, PRN, SkuCode, OldValue, NewValue, CreatedDate As StartDate, Cast(GetDate() As Date) As EndDate
	From	T
	Where	T.PRN = 0

	Union

	Select	T2.RN, T2.PRN, T2.SkuCode, T2.OldValue, T2.NewValue, T2.CreatedDate As StartDate, DateAdd(Day, -1, T.CreatedDate)
	From	T
	Join	T As T2
	On		T2.PRN = T.RN
			And T2.SkuCode = T.SkuCode

)
Select	SkuCode,
		NewValue As Categorization,
		Cast(StartDate As Date) As StartDate,
		Cast(EndDate As Date) As EndDate
Into	#Log
From	LogCte

Select		PN.SkuCode,
			IsNull(L.Categorization, '') As Categorization,
			Cast(DatePart(Year, O.OrderDate) As NVarChar(5)) + '-' + Cast(DatePart(Month, O.OrderDate) As NVarChar(2)) As [Date],
			Sum(OD.Quantity) As Quantity
From		Orders As O With(NoLock)
Join		OrderDetails As OD With(NoLock)
On			O.Id = OD.OrderId
Join		ProductInformations As PN With(NoLock)
On			OD.ProductInformationId = PN.Id
			And PN.SkuCode In @Codes
Left Join	#Log As L
On			Cast(O.OrderDate As Date) Between L.StartDate And L.EndDate
			And L.SkuCode = PN.SkuCode
Where		O.OrderTypeId <> 'IP'
			And O.OrderTypeId <> 'IA'
			And Cast(O.OrderDate As Date) >= DateAdd(Month, -11, DateAdd(Month, DateDiff(Month, 0, GetDate()), 0))
Group By	PN.SkuCode,
			IsNull(L.Categorization, ''),
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)
Order By	PN.SkuCode,
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)

Drop Table	#Log";
                    response.Data = sqlConnection.Query<Last12MonthSale>(query, new { Codes = list }).ToList();
                }

                response.Success = true;

            }, (response, exception) =>
            {
                sqlConnection.Close();
            });
        }

        public DataResponse<List<Last12MonthSale>> Last36MonthSales(string skuCode)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Last12MonthSale>>>((response) =>
            {

                skuCode.CleanParameter();

                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    var query = @"
With T As (
	Select	*,
			Row_Number() Over(Partition By SkuCode Order By SkuCode, CreatedDate Desc) As RN,
			Row_Number() Over(Partition By SkuCode Order By SkuCode, CreatedDate Desc) - 1 As PRN
	From	ProductCategorizationHistories
	Where	SkuCode = @SkuCode
), LogCte As (
	
	Select	RN, PRN, SkuCode, OldValue, NewValue, CreatedDate As StartDate, Cast(GetDate() As Date) As EndDate
	From	T
	Where	T.PRN = 0

	Union

	Select	T2.RN, T2.PRN, T2.SkuCode, T2.OldValue, T2.NewValue, T2.CreatedDate As StartDate, DateAdd(Day, -1, T.CreatedDate)
	From	T
	Join	T As T2
	On		T2.PRN = T.RN
			And T2.SkuCode = T.SkuCode

)
Select	SkuCode,
		NewValue As Categorization,
		Cast(StartDate As Date) As StartDate,
		Cast(EndDate As Date) As EndDate
Into	#Log
From	LogCte

Select		PN.SkuCode,
			IsNull(L.Categorization, '') As Categorization,
			Cast(DatePart(Year, O.OrderDate) As NVarChar(5)) + '-' + Cast(DatePart(Month, O.OrderDate) As NVarChar(2)) As [Date],
			Sum(OD.Quantity) As Quantity
From		Orders As O With(NoLock)
Join		OrderDetails As OD With(NoLock)
On			O.Id = OD.OrderId
Join		ProductInformations As PN With(NoLock)
On			OD.ProductInformationId = PN.Id
			And PN.SkuCode = @SkuCode
Left Join	#Log As L
On			Cast(O.OrderDate As Date) Between L.StartDate And L.EndDate
			And L.SkuCode = PN.SkuCode
Where		O.OrderTypeId <> 'IP'
			And O.OrderTypeId <> 'IA'
			And Cast(O.OrderDate As Date) >= Cast(Cast(@Year As NVarChar(5)) + '-01-01' As Date)
Group By	PN.SkuCode,
			IsNull(L.Categorization, ''),
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)
Order By	PN.SkuCode,
			DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)

Drop Table	#Log";
                    response.Data = sqlConnection
                        .Query<Last12MonthSale>(query, new
                        {
                            SkuCode = skuCode,
                            Year = (DateTime.Now.Year - 2)
                        })
                        .ToList();
                }

                response.Success = true;

            }, (response, exception) =>
            {
                sqlConnection.Close();
            });
        }

        public DataResponse<List<Last12MonthSale>> Last36MonthSales(int id)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Last12MonthSale>>>((response) =>
            {
                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    var query = @"
Select		Cast(DatePart(Year, O.OrderDate) As NVarChar(5)) + '-' + Cast(DatePart(Month, O.OrderDate) As NVarChar(2)) As [Date],
			Sum(OD.Quantity) As Quantity
From		Orders As O With(NoLock)
Join		OrderDetails As OD With(NoLock)
On			O.Id = OD.OrderId
Join		ProductInformations As PN With(NoLock)
On			OD.ProductInformationId = PN.Id
			And PN.ProductId = @ProductId
Where		O.OrderTypeId <> 'IP'
			And O.OrderTypeId <> 'IA'
			And Cast(O.OrderDate As Date) >= Cast(Cast(@Year As NVarChar(5)) + '-01-01' As Date)
Group By	DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)
Order By	DatePart(Year, O.OrderDate),
			DatePart(Month, O.OrderDate)";
                    response.Data = sqlConnection
                        .Query<Last12MonthSale>(query, new
                        {
                            ProductId = id,
                            Year = (DateTime.Now.Year - 2)
                        })
                        .ToList();
                }

                response.Success = true;

            }, (response, exception) =>
            {
                sqlConnection.Close();
            });
        }

        public DataResponse<List<string>> ReadAllSizes(int id)
        {
            SqlConnection sqlConnection = null;
            return ExceptionHandler.ResultHandle<DataResponse<List<string>>>((response) =>
            {
                response.Data = [];
                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    #region Query
                    var query = @"
Select		VVT.[Value]
From		Products As P With(NoLock)
Join		ProductInformations As PN With(NoLock)
On			P.Id = PN.ProductId
Join		ProductInformationVariants As PIV With(NoLock)
On			PN.Id = PIV.ProductInformationId
Join		VariantValues As VV With(NoLock)
On			VV.Id = PIV.VariantValueId
Join		VariantValueTranslations As VVT With(NoLock)
On			VV.Id = VVT.VariantValueId
			And LanguageId = 'TR'
Join		Variants As V With(NoLock)
On			V.Id = VV.VariantId
Join		VariantTranslations As VT With(NoLock)
On			VT.VariantId = V.Id
			And VT.LanguageId = 'TR'
			And VT.[Name] = 'Beden'
Where		P.Id = @Id
Group By	VVT.[Value]";
                    #endregion

                    response.Data.AddRange(sqlConnection.Query<string>(query, new { Id = id }));
                }
                response.Success = true;
            }, (response, exception) => sqlConnection.Close());
        }

        class AllColor
        {
            public string Value { get; set; }

            public string SkuCode { get; set; }
        }

        public DataResponse<Dictionary<string, string>> ReadAllColors(int id)
        {
            SqlConnection sqlConnection = null;
            return ExceptionHandler.ResultHandle<DataResponse<Dictionary<string, string>>>((response) =>
            {
                response.Data = [];
                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    #region Query
                    var query = @"
Select		VVT.[Value],
			PN.SkuCode
From		Products As P With(NoLock)
Join		ProductInformations As PN With(NoLock)
On			P.Id = PN.ProductId
Join		ProductInformationVariants As PIV With(NoLock)
On			PN.Id = PIV.ProductInformationId
Join		VariantValues As VV With(NoLock)
On			VV.Id = PIV.VariantValueId
Join		VariantValueTranslations As VVT With(NoLock)
On			VV.Id = VVT.VariantValueId
			And LanguageId = 'TR'
Join		Variants As V With(NoLock)
On			V.Id = VV.VariantId
Join		VariantTranslations As VT With(NoLock)
On			VT.VariantId = V.Id
			And VT.LanguageId = 'TR'
			And VT.[Name] = 'Renk'
Where		P.Id = @Id
Group By	VVT.[Value],
			PN.SkuCode";
                    #endregion

                    var data = sqlConnection.Query<AllColor>(query, new { Id = id });
                    foreach (var theData in data)
                    {
                        response.Data.Add(theData.SkuCode, theData.Value);
                    }
                }
                response.Success = true;
            }, (response, exception) => sqlConnection.Close());
        }
        #endregion

        #region Private Methods
        private Domain.Entities.Product ReadEntity(int id)
        {
            var entity = this
                ._unitOfWork
                .ProductRepository
                .DbSet()
                .Include(x => x.ProductTranslations)
                .Include(p => p.CategoryProducts)
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationPriceses)
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationTranslations)
                .ThenInclude(x => x.ProductInformationSeoTranslation)
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationTranslations)
                .ThenInclude(x => x.ProductInformationTranslationBreadcrumb)
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationTranslations)
                .ThenInclude(x => x.ProductInformationContentTranslation)
                .Include(p => p.ProductInformations)
                .ThenInclude(pi => pi.ProductInformationCombines)
                .ThenInclude(pic => pic.ContainProductInformation.ProductInformationTranslations)
                .FirstOrDefault(p => p.Id == id);

            entity.ProductLabels = new List<Domain.Entities.ProductLabel>();
            entity.ProductProperties = new List<Domain.Entities.ProductProperty>();

            var productProperties = this
                  ._unitOfWork
                  .ProductPropertyRepository
                  .DbSet()
                  .Where(pp => pp.ProductId == id && pp.Active && !pp.PropertyValue.Property.Deleted)
                  .ToList();

            var productLabels = this
                ._unitOfWork
                .ProductLabelRepository
                .DbSet()
                .Where(pl => pl.Active && !pl.Label.Deleted)
                .Where(pl => pl.ProductId == id)
                .OrderBy(x => x.Label.DisplayOrder)
                .ToList();

            entity.ProductLabels.AddRange(productLabels);
            entity.ProductProperties.AddRange(productProperties);

            foreach (var piLoop in entity.ProductInformations)
            {

                var productInformationVariants = this
                      ._unitOfWork
                      .ProductInformationVariantRepository
                      .DbSet()
                      .Include(pv => pv.VariantValue)
                      .ThenInclude(vv => vv.Variant)
                      .ThenInclude(vv => vv.VariantTranslations)
                      .Include(pv => pv.VariantValue)
                      .ThenInclude(pv => pv.VariantValueTranslations)
                      .Where(x => x.ProductInformationId == piLoop.Id)
                      .ToList();
                piLoop.ProductInformationVariants = new List<Domain.Entities.ProductInformationVariant>();
                piLoop.ProductInformationVariants.AddRange(productInformationVariants);
            }

            return entity;
        }

        private ProductInformation[] GenerateProductInformationsPrivate(ProductInformationPartialRequest request)
        {
            var combinationsCount = 1;
            request.Variants.ForEach(v => combinationsCount *= v.VariantValues.Count);

            var calculatedProductInformations = new ProductInformation[combinationsCount];

            var temporaryCombinationsCount = combinationsCount;

            var index = 0;

            var parentCount = 1;

            foreach (var vLoop in request.Variants)
            {
                var itemsCount = temporaryCombinationsCount / vLoop.VariantValues.Count;

                for (int l = 0; l < parentCount; l++)
                {
                    foreach (var vvLoop in vLoop.VariantValues)
                    {
                        for (int i = 0; i < itemsCount; i++)
                        {
                            if (calculatedProductInformations[index] == null)
                                calculatedProductInformations[index] = new ProductInformation
                                {
                                    ProductInformationVariants = new List<ProductInformationVariant>()
                                };

                            calculatedProductInformations[index]
                                .ProductInformationVariants
                                .Add(new ProductInformationVariant
                                {
                                    VariantValueId = vvLoop.Id,
                                    VariantValue = vvLoop,
                                });

                            index++;

                            if (index == combinationsCount)
                                index = 0;
                        }
                    }
                }
                parentCount *= vLoop.VariantValues.Count;
                temporaryCombinationsCount = itemsCount;
            }

            if (request.ProductInformations != null)
            {
                foreach (var piLoop in request.ProductInformations)
                {
                    if (piLoop.ProductInformationVariants == null || piLoop.ProductInformationVariants.Count == 0)
                        continue;

                    var vv1 = piLoop
                            .ProductInformationVariants
                            .OrderBy(pv => pv.VariantValueId)
                            .Select(pv => pv.VariantValueId)
                            .ToList();

                    for (int i = 0; i < calculatedProductInformations.Length; i++)
                    {
                        var vv2 = calculatedProductInformations[i]
                            .ProductInformationVariants
                            .OrderBy(pv => pv.VariantValueId)
                            .Select(pv => pv.VariantValueId)
                            .ToList();

                        if (vv1.Count() == vv2.Count())
                        {
                            var found = true;
                            for (int j = 0; j < vv1.Count(); j++)
                                if (vv1[j] != vv2[j])
                                    found = false;

                            if (found)
                            {
                                calculatedProductInformations[i] = piLoop;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var cpi in calculatedProductInformations)
                {
                    cpi.IsSale = true;
                    cpi.Active = true;
                }
            }

            /*
             * Getirilmesi istenilmeyen kayıtlar silinmektedir.
             */
            if (request.ToBeDeletedProductInformations != null &&
                request.ToBeDeletedProductInformations.Count != 0)
            {
                var tempList = calculatedProductInformations.ToList();
                tempList.RemoveAll(d => request.ToBeDeletedProductInformations.Contains(String.Join(",", d.ProductInformationVariants.Select(pv => pv.VariantValueId).OrderBy(id => id))));

                calculatedProductInformations = tempList.ToArray();
            }

            return calculatedProductInformations;
        }
        #endregion

        #region Helper
        private List<Domain.Entities.CategoryProduct> RecursiveCategoryProduct(List<Domain.Entities.Category> categories, int categoryId, int productId, bool leaf, bool multi)
        {
            var categoryProducts = new List<Domain.Entities.CategoryProduct>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);

            categoryProducts.Add(new Domain.Entities.CategoryProduct
            {
                ProductId = productId,
                CategoryId = category.Id,
                CreatedDate = DateTime.Now,
                Leaf = leaf,
                Multi = multi
            });

            if (category.ParentCategoryId.HasValue)
            {
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, productId, false, multi));
            }

            return categoryProducts;
        }

        private List<string> RecursiveCategory(List<Domain.Entities.Category> categories, int categoryId)
        {
            var html = new List<string>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);
            var CategoryTranslation = category.CategoryTranslations.FirstOrDefault();

            var categoryTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CategoryTranslation.Name.ToLower().ReplaceChar());
            html.Add($"<li><a href=\"{CategoryTranslation.Url}\">{categoryTranslationName}</a> </li>");
            if (category.ParentCategoryId.HasValue)
            {
                html.AddRange(RecursiveCategory(categories, category.ParentCategoryId.Value));
            }

            return html;
        }
        #endregion
    }
}