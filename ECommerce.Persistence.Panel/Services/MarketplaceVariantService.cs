﻿using Dapper;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceVariantService : IMarketplaceVariantService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;

        protected readonly ITenantFinder _tenantFinder;

        protected readonly IDomainFinder _domainFinder;

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public MarketplaceVariantService(IUnitOfWork unitOfWork, ISettingService settingService, ITenantFinder tenantFinder,
            IDomainFinder domainFinder, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<MarketplaceVariant>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariant>>>((response) =>
            {
                //response.Data = this
                //    ._unitOfWork
                //    .MarketplaceVariantRepository
                //    .DbSet()
                //    .Select(x => new MarketplaceVariant
                //    {
                //        Id = x.Id,
                //        Name = x.Name,
                //        MarketplaceId = x.MarketplaceId,
                //        AllowCustom = x.AllowCustom,
                //        Mandatory = x.Mandatory
                //    })
                //    .ToList();

                response.Success = true;
            }, (response, exception) => { });
            }, string.Format(CacheKey.MarketplaceVariants));
        }

        public DataResponse<List<MarketplaceVariant>> ReadMappings()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariant>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Select	MV.Id,
		MV.AllowCustom,
		MV.Mandatory,
		MV.MarketplaceId,
		MV.MultiValue,
		MV.Name
From	MarketplaceCategoriesMarketplaceVariants MCV
Join    MarketplaceCategoryMappings MCP On MCP.MarketplaceCategoryId=MCV.MarketplaceCategoryId
Join	MarketplaceVariants MV On MV.Id=MCV.MarketplaceVariantId
Where	MCP.TenantId=@TenantId
		And MCP.DomainId=@DomainId
GROUP   BY MV.Id,
		MV.AllowCustom,
		MV.Mandatory,
		MV.MarketplaceId,
		MV.MultiValue,
		MV.Name
Order By MV.Name";

                    response.Data = sqlConnection
                        .Query<MarketplaceVariant>(query, new
                        {
                            TenantId = _tenantFinder.FindId(),
                            DomainId = _domainFinder.FindId()
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Variant oluşturuldu.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        public DataResponse<List<MarketplaceVariant>> Read(string marketplaceCategoryId)
        {
            throw new Exception();
//            return this._cacheHandler.ResponseHandle((System.Func<DataResponse<List<MarketplaceVariant>>>)(() =>
//            {
//                return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariant>>>((System.Action<DataResponse<List<MarketplaceVariant>>>)((response) =>
//            {
//                response.Data = Queryable.Select<Domain.Entities.Companyable.ECommerceVariant, MarketplaceVariant>(EntityFrameworkQueryableExtensions.Include<Domain.Entities.Companyable.ECommerceVariant, Domain.Entities.MarketplaceVariant>(this
//                    ._unitOfWork
//                    .MarketplaceCategoriesMarketplaceVariantRepository
//                    .DbSet()
//                    .Where(mcmv => mcmv.MarketplaceCategoryId == marketplaceCategoryId)
//, mcmv => (Domain.Entities.MarketplaceVariant)mcmv.MarketplaceVariant)
//, x => new MarketplaceVariant
//{
//    Id = x.MarketplaceVariant.Id,
//    Name = x.MarketplaceVariant.Name,
//    MarketplaceId = x.MarketplaceVariant.MarketplaceId,
//    AllowCustom = x.AllowCustom,
//    Mandatory = x.Mandatory,
//    MultiValue = x.MultiValue
//})
//                    .OrderBy(mv => !mv.Mandatory)
//                    .ThenBy(mv => mv.Name)
//                    .ToList();

//                response.Success = true;
//            }), (response, exception) => { });
//            }), string.Format(CacheKey.MarketplaceCategoryId, marketplaceCategoryId));
        }
        #endregion
    }
}