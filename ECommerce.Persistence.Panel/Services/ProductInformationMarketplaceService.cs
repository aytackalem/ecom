﻿using AutoMapper;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductInformationMarketplaceService : IProductInformationMarketplaceService
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService _marketplaceCatalogVariantValueService;
        #endregion

        #region Constructors
        public ProductInformationMarketplaceService(IMapper mapper, IUnitOfWork unitOfWork, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService marketplaceCatalogVariantValueService)
        {
            #region Fields
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;
            this._marketplaceCatalogVariantValueService = marketplaceCatalogVariantValueService;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<Response> UpsertAsync(List<ProductInformationMarketplace> productInformationMarketplaces)
        {

            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {

                var repo = this._unitOfWork.ProductInformationMarketplaceRepository;
                var dbSet = repo.DbSet();

                foreach (var theProductInformationMarketplace in productInformationMarketplaces)
                {
                    if (theProductInformationMarketplace.Id == 0)
                    {
                        repo.Create(new Domain.Entities.ProductInformationMarketplace
                        {
                            Id = theProductInformationMarketplace.Id,
                            ProductInformationId = theProductInformationMarketplace.ProductInformationId,
                            AccountingPrice = theProductInformationMarketplace.AccountingPrice,
                            Active = theProductInformationMarketplace.Active,
                            Barcode = theProductInformationMarketplace.Barcode,
                            StockCode = theProductInformationMarketplace.StockCode,
                            UnitPrice = theProductInformationMarketplace.UnitPrice,
                            ListUnitPrice = theProductInformationMarketplace.ListUnitPrice,
                            MarketplaceId = theProductInformationMarketplace.MarketplaceId,
                            UUId = Guid.NewGuid().ToString(),
                            DirtyProductInformation = true,
                            DisabledUpdateProduct = false,
                            ProductInformationMarketplaceVariantValues = theProductInformationMarketplace
                                .ProductInformationMarketplaceVariantValues
                                .Select(pimvv => new Domain.Entities.ProductInformationMarketplaceVariantValue
                                {
                                    AllowCustom = pimvv.AllowCustom,
                                    Mandatory = pimvv.Mandatory,
                                    MarketplaceVariantCode = pimvv.MarketplaceVariantCode,
                                    MarketplaceVariantName = pimvv.MarketplaceVariantName,
                                    MarketplaceVariantValueCode = pimvv.MarketplaceVariantValueCode,
                                    MarketplaceVariantValueName = pimvv.MarketplaceVariantValueName,
                                    Multiple = pimvv.Multiple,
                                    Varianter = pimvv.Varianter
                                })
                                .ToList()
                        });
                    }
                    else
                    {
                        var entity = dbSet
                            .Include(pim => pim.ProductInformationMarketplaceVariantValues)
                            .First(pim => pim.Id == theProductInformationMarketplace.Id);
                        entity.AccountingPrice = theProductInformationMarketplace.AccountingPrice;
                        entity.Active = theProductInformationMarketplace.Active;
                        entity.Barcode = theProductInformationMarketplace.Barcode;
                        entity.StockCode = theProductInformationMarketplace.StockCode;
                        entity.UnitPrice = theProductInformationMarketplace.UnitPrice;
                        entity.ListUnitPrice = theProductInformationMarketplace.ListUnitPrice;
                        entity.DirtyProductInformation = true;
                        entity.DisabledUpdateProduct = !theProductInformationMarketplace.DisabledUpdateProduct;
                        entity.ProductInformationMarketplaceVariantValues = new();
                        entity.ProductInformationMarketplaceVariantValues = theProductInformationMarketplace
                            .ProductInformationMarketplaceVariantValues
                            .Select(pimvv => new Domain.Entities.ProductInformationMarketplaceVariantValue
                            {
                                AllowCustom = pimvv.AllowCustom,
                                Mandatory = pimvv.Mandatory,
                                MarketplaceVariantCode = pimvv.MarketplaceVariantCode,
                                MarketplaceVariantName = pimvv.MarketplaceVariantName,
                                MarketplaceVariantValueCode = pimvv.MarketplaceVariantValueCode,
                                MarketplaceVariantValueName = pimvv.MarketplaceVariantValueName,
                                Multiple = pimvv.Multiple,
                                Varianter = pimvv.Varianter
                            }).ToList();

                        repo.Update(entity);
                    }
                }

                response.Success = true;
                response.Message = "İşlem başarılı.";

            });

        }

        public async Task<DataResponse<List<ProductInformationMarketplace>>> ReadOrGenerateAsync(List<ProductInformation> productInformations, List<ProductMarketplace> productMarketplaces, List<Marketplace> marketplaces, bool accountingPrice, int categoryId)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<ProductInformationMarketplace>>>(async response =>
            {
                var productInformationIds = productInformations.Select(pi => pi.Id).ToList();

                var dbSet = this._unitOfWork.ProductInformationMarketplaceRepository.DbSet();
                var productInformationMarketplaces = await dbSet
                    .Include(pim => pim.ProductInformationMarketplaceVariantValues)
                    .Where(pim => productInformationIds.Contains(pim.ProductInformationId))
                    .ToListAsync();

                foreach (var theProductInformationId in productInformationIds)
                    foreach (var theMarketplace in marketplaces)
                    {
                        var productInformation = productInformations.First(pi => pi.Id == theProductInformationId);

                        var productInformationMarketplace = productInformationMarketplaces
                            .FirstOrDefault(pim => pim.ProductInformationId == theProductInformationId &&
                                                   pim.MarketplaceId == theMarketplace.Id);
                        productInformationMarketplace ??= new Domain.Entities.ProductInformationMarketplace
                        {
                            ProductInformationId = theProductInformationId,
                            AccountingPrice = accountingPrice,
                            ListUnitPrice = productInformation.ProductInformationPriceses.First().ListUnitPrice,
                            UnitPrice = productInformation.ProductInformationPriceses.First().UnitPrice,
                            MarketplaceId = theMarketplace.Id,
                            StockCode = productInformation.StockCode,
                            Barcode = productInformation.Barcode,
                            DisabledUpdateProduct = false,
                            Active = true
                        };

                        var productMarketplace = productMarketplaces.FirstOrDefault(x => x.MarketplaceId == theMarketplace.Id);
                        if (productMarketplace == null || string.IsNullOrEmpty(productMarketplace.MarketplaceCategoryCode))
                        {
                            productInformationMarketplaces.Add(productInformationMarketplace);
                            continue;
                        }

                        var _variantValueDictionary = new Dictionary<string, bool>();

                        #region Variant Mappings
                        foreach (var theProductInformationVariant in productInformation.ProductInformationVariants)
                        {
                            var marketplaceVariantValueMappings = await this
                                ._unitOfWork
                                .MarketplaceVariantValueMappingRepository
                                .DbSet()
                                .Where(mvvm => mvvm.VariantValueId == theProductInformationVariant.VariantValueId &&
                                               mvvm.MarketplaceId == theMarketplace.Id)
                                .ToListAsync();

                            if (marketplaceVariantValueMappings.HasItem())
                            {
                                productInformationMarketplace.ProductInformationMarketplaceVariantValues ??= new();

                                foreach (var marketplaceVariantValueMapping in marketplaceVariantValueMappings)
                                {
                                    if (productInformationMarketplace
                                    .ProductInformationMarketplaceVariantValues
                                    .Any(pimvv => pimvv.MarketplaceVariantCode == marketplaceVariantValueMapping.MarketplaceVariantCode) ||
                                    string.IsNullOrEmpty(marketplaceVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(marketplaceVariantValueMapping.MarketplaceVariantValueName))
                                        continue;

                                    if (!marketplaceVariantValueMapping.AllowCustom && (theMarketplace.Id == "TY" || theMarketplace.Id == "PA"))
                                    {
                                        var code = $"{productMarketplace.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                        if (!_variantValueDictionary.ContainsKey(code))
                                        {
                                            var marketplaceCatalogVariantValueService = await this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                            {
                                                MarketplaceId = theMarketplace.Id,
                                                Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                CategoryCode = productMarketplace.MarketplaceCategoryCode,
                                                VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                            });
                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                        }

                                        if (_variantValueDictionary[code] == false) continue;
                                    }

                                    productInformationMarketplace
                                        .ProductInformationMarketplaceVariantValues
                                        .Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                        {
                                            AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                            Mandatory = marketplaceVariantValueMapping.Mandatory,
                                            Multiple = marketplaceVariantValueMapping.Multiple,
                                            Varianter = marketplaceVariantValueMapping.Varianter,
                                            MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                            MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                            MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                            MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName
                                        });
                                }
                            }

                        }

                        #endregion


                        #region Category Mappings
                        var marketplaceCategoryMapping = await this
                           ._unitOfWork
                           .MarketplaceCategoryMappingRepository
                           .DbSet()
                           .Include(x => x.MarketplaceCategoryVariantValueMappings)
                           .Where(mvvm => mvvm.CategoryId == categoryId &&
                                          mvvm.MarketplaceId == theMarketplace.Id)
                           .FirstOrDefaultAsync();

                        if (marketplaceCategoryMapping != null)
                        {
                            productInformationMarketplace.ProductInformationMarketplaceVariantValues ??= new();

                            foreach (var marketplaceCategoryVariantValueMapping in marketplaceCategoryMapping.MarketplaceCategoryVariantValueMappings)
                            {
                                if (productInformationMarketplace
                                 .ProductInformationMarketplaceVariantValues
                                 .Any(pimvv => pimvv.MarketplaceVariantCode == marketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                                 string.IsNullOrEmpty(marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(marketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                    continue;

                                if (!marketplaceCategoryVariantValueMapping.AllowCustom && (theMarketplace.Id == "TY" || theMarketplace.Id == "PA"))
                                {
                                    var code = $"{productMarketplace.MarketplaceCategoryCode}-{marketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                    if (!_variantValueDictionary.ContainsKey(code))
                                    {
                                        var marketplaceCatalogVariantValueService = await this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                        {
                                            MarketplaceId = theMarketplace.Id,
                                            Code = marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                            CategoryCode = productMarketplace.MarketplaceCategoryCode,
                                            VariantCode = marketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                        });
                                        if (!marketplaceCatalogVariantValueService.Success) continue;


                                        _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                    }

                                    if (_variantValueDictionary[code] == false) continue;
                                }



                                productInformationMarketplace
                                    .ProductInformationMarketplaceVariantValues
                                    .Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                    {
                                        AllowCustom = marketplaceCategoryVariantValueMapping.AllowCustom,
                                        Mandatory = marketplaceCategoryVariantValueMapping.Mandatory,
                                        Multiple = marketplaceCategoryVariantValueMapping.Multiple,
                                        Varianter = marketplaceCategoryVariantValueMapping.Varianter,
                                        MarketplaceVariantValueCode = marketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                        MarketplaceVariantValueName = marketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                        MarketplaceVariantCode = marketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                        MarketplaceVariantName = marketplaceCategoryVariantValueMapping.MarketplaceVariantName
                                    });
                            }



                        }

                        #endregion

                        productInformationMarketplaces.Add(productInformationMarketplace);

                    }

                response.Data = this._mapper.Map<List<ProductInformationMarketplace>>(productInformationMarketplaces);
                response.Success = true;
            });

        }
        #endregion
    }
}
