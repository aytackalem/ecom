﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class EntireDiscountService : IEntireDiscountService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public EntireDiscountService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<EntireDiscount> Create(EntireDiscount dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EntireDiscount>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");
                var entity = new Domain.Entities.EntireDiscount
                {
                    Active = dto.Active,
                    StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo),
                    EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo),
                    ApplicationId = dto.ApplicationId,
                    EntireDiscountSetting = new Domain.Entities.EntireDiscountSetting
                    {
                        Discount = dto.EntireDiscountSetting.Discount,
                        DiscountIsRate = dto.EntireDiscountSetting.DiscountIsRate,
                        Limit = dto.EntireDiscountSetting.Limit
                    },
                    CreatedDate = DateTime.Now,
                    
                };

                var created = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Genel indirim oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<EntireDiscount> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EntireDiscount>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .DbSet()
                    .Include(ed => ed.EntireDiscountSetting)
                    .FirstOrDefault(ed => ed.Id == id);

                if (entity == null)
                {
                    response.Message = $"Genel indirim bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                var dto = new EntireDiscount
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    EndDate = entity.EndDate,
                    StartDate = entity.StartDate,
                    ApplicationId = entity.ApplicationId,
                    StartDateString = entity.StartDate.ToString("dd/MM/yyy", cultureinfo),
                    EndDateString = entity.EndDate.ToString("dd/MM/yyy", cultureinfo),
                    EntireDiscountSetting = new EntireDiscountSetting
                    {
                        Discount = entity.EntireDiscountSetting.Discount,
                        DiscountIsRate = entity.EntireDiscountSetting.DiscountIsRate,
                        Id = entity.EntireDiscountSetting.Id,
                        Limit = entity.EntireDiscountSetting.Limit
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<EntireDiscount> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<EntireDiscount>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(ed => new EntireDiscount
                    {
                        Id = ed.Id,
                        Active = ed.Active,
                        EndDate = ed.EndDate,
                        StartDate = ed.StartDate,
                        ApplicationId = ed.ApplicationId,
                        StartDateString = ed.StartDate.ToString("dd/MM/yyy", cultureinfo),
                        EndDateString = ed.EndDate.ToString("dd/MM/yyy", cultureinfo)
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(EntireDiscount dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .DbSet()
                    .Include(ed => ed.EntireDiscountSetting)
                    .FirstOrDefault(ed => ed.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Genel indirim bulunamadı.";
                    response.Success = false;
                    return;
                }

                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                entity.StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo);
                entity.EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo);
                entity.ApplicationId = dto.ApplicationId;
                entity.Active = dto.Active;
                entity.EntireDiscountSetting.Discount = dto.EntireDiscountSetting.Discount;
                entity.EntireDiscountSetting.DiscountIsRate = dto.EntireDiscountSetting.DiscountIsRate;
                entity.EntireDiscountSetting.Limit = dto.EntireDiscountSetting.Limit;

                var updated = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Genel indirim güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(EntireDiscount dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .EntireDiscountRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Genel indirim bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;
                entity.DeletedDateTime = DateTime.Now;
                var updated = this
                    ._unitOfWork.EntireDiscountRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Genel indirim silindi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
