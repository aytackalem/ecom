﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class ContactService : IContactService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ContactService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Contact> Create(Contact dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Contact>>((response) =>
            {



                var entity = new Domain.Entities.Contact
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    Address = dto.Address,
                    Email = dto.Email,
                    Phone1 = dto.Phone1,
                    Phone2 = dto.Phone2,
                    Whatsapp = dto.Whatsapp,
                    ContactTranslations = dto.ContactTranslations.Select(ct => new Domain.Entities.ContactTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Description = ct.Description
                    }).ToList()
                };

                var created = this
                    ._unitOfWork.ContactRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;



                response.Data = dto;
                response.Success = true;
                response.Message = "Sıkça Sorulan Soru oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Contact> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Contact>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ContactRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Sıkça Sorulan Soru bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Contact
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Address=entity.Address,
                    Whatsapp=entity.Whatsapp,
                    Phone2=entity.Phone2,
                    Phone1=entity.Phone1,
                    Email=entity.Email,
                    ContactTranslations = entity.ContactTranslations.Select(ct => new ContactTranslation
                    {
                        Description = ct.Description,
                        LanguageId = ct.LanguageId,
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Contact> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Contact>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ContactRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .ContactRepository
                    .DbSet()
                    .Include(c => c.ContactTranslations)                   
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new Contact
                    {
                        Id = c.Id,
                        Active = c.Active,
                        Address=c.Address,
                        Email=c.Email,
                        Phone1=c.Phone1,
                        Phone2=c.Phone2,
                        Whatsapp=c.Whatsapp,
                        ContactTranslations = c.ContactTranslations.Select(ct => new ContactTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Description = ct.Description
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ContactRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c
                            .ContactTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Description
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Contact dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .ContactRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"İletişim bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.Phone1 = dto.Phone1;
                entity.Phone2 = dto.Phone2;
                entity.Address = dto.Address;
                entity.Whatsapp = dto.Whatsapp;
                entity.Email = dto.Email;

                foreach (var ContactTranslation in dto.ContactTranslations)
                {
                    var _ContactTranslation = entity.ContactTranslations.FirstOrDefault(x => x.LanguageId == ContactTranslation.LanguageId);

                    if (_ContactTranslation != null)
                    {
                        _ContactTranslation.Description = ContactTranslation.Description;
                    }
                }

                var updated = this
                    ._unitOfWork.ContactRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "İletişim  güncellendi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
