﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class CategoryService : ICategoryService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public CategoryService(IUnitOfWork unitOfWork, IImageHelper imageHelper, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Category> Create(Category dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Category>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .CategoryRepository
                     .DbSet()
                     .Any(c => c.CategoryTranslations.Any(ct => dto.CategoryTranslations.Select(ct2 => ct2.Name).Contains(ct.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir kategori oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }



                var entity = new Domain.Entities.Category
                {
                    Active = dto.Active,
                    FileName = dto.FileName,
                    ParentCategoryId = dto.ParentCategoryId == 0 ? null : dto.ParentCategoryId,
                    CategoryTranslations = dto.CategoryTranslations.Select(ct => new Domain.Entities.CategoryTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Name = ct.Name,
                        CategoryContentTranslation = new Domain.Entities.CategoryContentTranslation
                        {
                            Content = ct.CategoryContentTranslation.Content,
                            CreatedDate = DateTime.Now,

                        },
                        CategorySeoTranslation = new Domain.Entities.CategorySeoTranslation
                        {
                            CreatedDate = DateTime.Now,
                            Title = ct.CategorySeoTranslation.Title,
                            MetaKeywords = ct.CategorySeoTranslation.MetaKeywords,
                            MetaDescription = ct.CategorySeoTranslation.MetaDescription
                        }
                    }).ToList()
                };

                var created = this
                    ._unitOfWork.CategoryRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                var categoryTranslationBreadcrumbs = new List<Domain.Entities.CategoryTranslationBreadcrumb>();
                var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();

                /* Kategori ikon dosyası. */
                if (entity.FileName.StartsWith("data"))
                {
                    var directory = $@"{this._settingService.CategoryImagePath}\{entity.Id}";

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var fileName = $"{Guid.NewGuid()}.jpg";
                    var filePath = $@"{directory}\{fileName}";

                    this._imageHelper.Save(dto.FileName, filePath);

                    entity.FileName = fileName;
                    _unitOfWork.CategoryRepository.Update(entity);
                }

                foreach (var categoryTranslation in entity.CategoryTranslations)
                {
                    categoryTranslation.Url = $"{categoryTranslation.Name.GenerateUrl()}-{entity.Id}-c";
                    this._unitOfWork.CategoryTranslationRepository.Update(categoryTranslation);

                    var html = new StringBuilder();
                    html.AppendLine("<ul>");
                    var categoryProducts = RecursiveCategory(categories, entity.Id, true);
                    categoryProducts.Reverse();
                    foreach (var categoryProduct in categoryProducts)
                    {
                        html.AppendLine(categoryProduct);
                    }
                    html.AppendLine("</ul>");
                    _unitOfWork.CategoryTranslationBreadcrumbRepository.Create(new Domain.Entities.CategoryTranslationBreadcrumb
                    {
                        Id = categoryTranslation.Id,
                        CreatedDate = DateTime.Now,
                        Html = html.ToString()
                    });
                }

                response.Data = dto;
                response.Success = true;
                response.Message = "Kategori oluşturuldu.";
            });
        }

        public DataResponse<Category> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Category>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CategoryRepository
                    .ReadAll(id);

                if (entity == null)
                {
                    response.Message = $"Kategori bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Category
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    ParentCategoryId = entity.ParentCategoryId,
                    FileName = $"{this._settingService.CategoryImageUrl}/{entity.Id}/{entity.FileName}",
                    CategoryTranslations = entity.CategoryTranslations.Select(ct => new CategoryTranslation
                    {
                        Url = ct.Url,
                        LanguageId = ct.LanguageId,
                        Name = ct.Name,
                        CategoryTranslationBreadcrumb = new CategoryTranslationBreadcrumb
                        {
                            Html = ct?.CategoryTranslationBreadcrumb?.Html
                        },
                        CategorySeoTranslation = new CategorySeoTranslation
                        {
                            MetaDescription = ct.CategorySeoTranslation.MetaDescription,
                            MetaKeywords = ct.CategorySeoTranslation.MetaKeywords,
                            Title = ct.CategorySeoTranslation.Title,
                        },
                        CategoryContentTranslation = new CategoryContentTranslation
                        {
                            Content = ct.CategoryContentTranslation.Content,
                        }
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Category> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Category>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Include(c => c.CategoryTranslations)
                    .Where(c => string.IsNullOrEmpty(pagedRequest.Search) || c.CategoryTranslations.Any(ct => ct.Name.Contains(pagedRequest.Search)))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new Category
                    {
                        Active = c.Active,
                        Id = c.Id,
                        CategoryTranslations = c.CategoryTranslations.Select(ct => new CategoryTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Name = ct.Name,
                            Url = ct.Url
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<Category>> ReadByParentCategoryId(int? parentCategoryId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Category>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Include(c => c.CategoryTranslations)
                    .Where(c => c.ParentCategoryId == parentCategoryId)
                    .Select(c => new Category
                    {
                        Active = c.Active,
                        Id = c.Id,
                        CategoryTranslations = c.CategoryTranslations.Select(ct => new CategoryTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Name = ct.Name,
                            Url = ct.Url
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c
                            .CategoryTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Category dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .CategoryRepository
                     .DbSet()
                     .Any(c => c.CategoryTranslations.Any(ct => ct.CategoryId != dto.Id && dto.CategoryTranslations.Select(ct2 => ct2.Name).Contains(ct.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir kategori oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                //if (!dto.Active)
                //{
                //    var hasAnyProduct = this
                //     ._unitOfWork
                //     .CategoryRepository
                //     .DbSet()
                //     .Any(c => c.CategoryProducts.Any());

                //    if (hasAnyProduct)
                //    {
                //        response.Success = false;
                //        response.Message = "Kategoriye ait ürün bulunduğundan bu marka pasif olarak güncellenemez.";
                //        return;
                //    }
                //}

                var entity = this
                    ._unitOfWork
                    .CategoryRepository
                    .ReadAll(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Kategori bulunamadı.";
                    response.Success = false;
                    return;
                }
                /* Kategori ikon dosyası. */
                if (dto.FileName.StartsWith("data"))
                {


                    var directory = $@"{this._settingService.CategoryImagePath}\{entity.Id}";

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var fileName = $"{Guid.NewGuid()}.jpg";
                    var filePath = $@"{directory}\{fileName}";

                    this._imageHelper.Save(dto.FileName, filePath);

                    dto.FileName = fileName;
                    entity.FileName = dto.FileName;

                }

                entity.Active = dto.Active;
                entity.ParentCategoryId = dto.ParentCategoryId == 0 ? null : dto.ParentCategoryId;
                var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
                foreach (var categoryTranslation in dto.CategoryTranslations)
                {
                    var _categoryTranslation = entity.CategoryTranslations.FirstOrDefault(x => x.LanguageId == categoryTranslation.LanguageId);

                    if (_categoryTranslation != null)
                    {
                        _categoryTranslation.Name = categoryTranslation.Name;
                        //_categoryTranslation.CategoryTranslationBreadcrumb.Html = categoryTranslation.CategoryTranslationBreadcrumb.Html;
                        _categoryTranslation.CategorySeoTranslation.Title = categoryTranslation.CategorySeoTranslation.Title;
                        _categoryTranslation.CategorySeoTranslation.MetaKeywords = categoryTranslation.CategorySeoTranslation.MetaKeywords;
                        _categoryTranslation.CategorySeoTranslation.MetaDescription = categoryTranslation.CategorySeoTranslation.MetaDescription;
                        _categoryTranslation.CategoryContentTranslation.Content = categoryTranslation.CategoryContentTranslation.Content;


                        var html = new StringBuilder();
                        html.AppendLine("<ul>");
                        var categoryProducts = RecursiveCategory(categories, entity.Id, true);
                        categoryProducts.Reverse();
                        foreach (var categoryProduct in categoryProducts)
                        {
                            html.AppendLine(categoryProduct);
                        }
                        html.AppendLine("</ul>");
                        var categoryTranslationBreadcrumbRepository = _unitOfWork.CategoryTranslationBreadcrumbRepository.Read(_categoryTranslation.Id);
                        categoryTranslationBreadcrumbRepository.Html = html.ToString();

                        _unitOfWork.CategoryTranslationBreadcrumbRepository.Update(categoryTranslationBreadcrumbRepository);

                    }
                }





                var updated = this
                    ._unitOfWork.CategoryRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Kategori güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Helper
        private List<string> RecursiveCategory(List<Domain.Entities.Category> categories, int categoryId, bool first)
        {
            var html = new List<string>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);
            var CategoryTranslation = category.CategoryTranslations.FirstOrDefault();

            var categoryTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CategoryTranslation.Name.ToLower().ReplaceChar());

            if (!first)
                html.Add($"<li><a href=\"{CategoryTranslation.Url}\">{categoryTranslationName}</a> </li>");
            else
                html.Add($"<li>{categoryTranslationName}</li>");


            if (category.ParentCategoryId.HasValue)
            {
                html.AddRange(RecursiveCategory(categories, category.ParentCategoryId.Value, false));
            }

            return html;
        }
        #endregion
    }
}
