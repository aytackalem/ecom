﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class PropertyService : IPropertyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public PropertyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Property> Create(Property dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Property>>((response) =>
            {
                var exists = this
                    ._unitOfWork
                    .PropertyRepository
                    .Read(dto.Id) != null;

                if (exists)
                {
                    response.Success = false;
                    response.Message = "Bu no daha önce bir varyasyon oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var hasAnySameName = this
                     ._unitOfWork
                     .PropertyRepository
                     .DbSet()
                     .Any(b => b.ProductPropertyTranslations.Any(vt => dto.PropertyTranslations.Select(vt2 => vt2.Name).Contains(vt.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir varyasyon oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Property
                {
                    Id = dto.Id,
                    CreatedDate = DateTime.Now,
                    ProductPropertyTranslations = dto
                        .PropertyTranslations
                        .Select(vt => new Domain.Entities.PropertyTranslation
                        {
                            LanguageId = vt.LanguageId,
                            CreatedDate = DateTime.Now,
                            Name = vt.Name
                        })
                        .ToList()
                };

                var created = this
                    ._unitOfWork
                    .PropertyRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Varyasyon oluşturuldu.";
            }, (response, exception) => { });
        }

        public PagedResponse<Property> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Property>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .PropertyRepository
                    .DbSet()
                     .Where(c => !c.Deleted && (string.IsNullOrEmpty(pagedRequest.Search) || c.ProductPropertyTranslations.Any(ct => ct.Name.Contains(pagedRequest.Search))))
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .PropertyRepository
                    .DbSet()
                    .Include(v => v.ProductPropertyTranslations)
                    .Where(c => !c.Deleted && (string.IsNullOrEmpty(pagedRequest.Search) || c.ProductPropertyTranslations.Any(ct => ct.Name.Contains(pagedRequest.Search))))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(v => new Property
                    {
                        Id = v.Id,
                        PropertyTranslations = v.ProductPropertyTranslations.Select(vt => new PropertyTranslation
                        {
                            LanguageId = vt.LanguageId,
                            Name = vt.Name
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<Property>> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Property>>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .PropertyRepository
                    .DbSet()
                    .Include(v => v.ProductPropertyTranslations)
                    .Include(v => v.ProductPropertyValues)
                    .ThenInclude(vv => vv.PropertyValueTranslations)
                    .Where(c => !c.Deleted);


                response.Data = entities
                    .Select(v => new Property
                    {
                        Id = v.Id,
                        PropertyTranslations = v
                            .ProductPropertyTranslations
                            .Select(vt => new PropertyTranslation
                            {
                                Id = vt.Id,
                                LanguageId = vt.LanguageId,
                                Name = vt.Name
                            })
                            .ToList(),
                        PropertyValues = v
                            .ProductPropertyValues
                            .Select(vv => new PropertyValue
                            {
                                Id = vv.Id,
                                PropertyId = vv.PropertyId,
                                PropertyValueTranslations = vv
                                    .PropertyValueTranslations
                                    .Select(vvt => new PropertyValueTranslation
                                    {
                                        Id = vvt.Id,
                                        LanguageId = vvt.LanguageId,
                                        Value = vvt.Value,
                                        PropertyValueId = vvt.PropertyValueId
                                    })
                                    .ToList()
                            })
                            .ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<Property> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Property>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .PropertyRepository
                    .DbSet()
                    .Include(pp => pp.ProductPropertyTranslations)
                    .FirstOrDefault(pp => pp.Id == id);

                if (entity == null)
                {
                    response.Message = $"Ürün özellik bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Property
                {
                    Id = entity.Id,
                    PropertyTranslations = entity.ProductPropertyTranslations.Select(ct => new PropertyTranslation
                    {
                        LanguageId = ct.LanguageId,
                        Name = ct.Name
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Property dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .PropertyRepository
                     .DbSet()
                     .Any(c => c.ProductPropertyTranslations.Any(vt => vt.PropertyId != dto.Id && dto.PropertyTranslations.Select(vt2 => vt2.Name).Contains(vt.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir varyasyon oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .PropertyRepository
                    .DbSet()
                    .Include(pp => pp.ProductPropertyTranslations)
                    .FirstOrDefault(pp => pp.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Varyasyon bulunamadı.";
                    response.Success = false;
                    return;
                }

#warning Alt yapi multi language destekliyor fakat kod tek dil icin yazilmistir.
                entity.ProductPropertyTranslations[0].Name = dto.PropertyTranslations[0].Name;

                var updated = this
                    ._unitOfWork
                    .PropertyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Varyasyon güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(Property dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .PropertyRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Özellik bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;

                var updated = this
                    ._unitOfWork.PropertyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Özellik silindi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
