﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MoneyPointSettingService : IMoneyPointSettingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MoneyPointSettingService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<MoneyPointSetting> Create(MoneyPointSetting dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MoneyPointSetting>>((response) =>
            {
                var entity = new Domain.Entities.MoneyPointSetting
                {
                    Earning = dto.Earning,
                    EarningIsRate = dto.EarningIsRate,
                    Limit = dto.Limit
                };

                var created = this
                    ._unitOfWork
                    .MoneyPointSettingRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Marka oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<MoneyPointSetting> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<MoneyPointSetting>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .MoneyPointSettingRepository
                    .DbSet()
                    .FirstOrDefault();

                if (entity == null)
                {
                    response.Message = $"Para puan ayarları bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new MoneyPointSetting
                {
                    Id = entity.Id,
                    Earning = entity.Earning,
                    EarningIsRate = entity.EarningIsRate,
                    Limit = entity.Limit
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(MoneyPointSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .MoneyPointSettingRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Para puan ayarları bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Earning = dto.Earning;
                entity.EarningIsRate = dto.EarningIsRate;
                entity.Limit = dto.Limit;

                var updated = this
                    ._unitOfWork
                    .MoneyPointSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Para puan ayarları güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
