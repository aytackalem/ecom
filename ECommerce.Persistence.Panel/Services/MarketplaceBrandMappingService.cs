﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceBrandMappingService : IMarketplaceBrandMappingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        protected readonly ICompanyFinder _companyFinder;

        #endregion

        #region Constructors
        public MarketplaceBrandMappingService(IUnitOfWork unitOfWork, ICompanyFinder companyFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<MarketplaceBrandMapping>>> ReadOrGenerateAsync(int brandId, List<Marketplace> marketplaces)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<MarketplaceBrandMapping>>>(async response =>
            {
                response.Data = await this
                    ._unitOfWork
                    .MarketplaceBrandMappingRepository
                    .DbSet()
                    .Where(x => x.BrandId == brandId)
                    .Select(x => new MarketplaceBrandMapping
                    {
                        Id = x.Id,
                        BrandId = x.BrandId,
                        MarketplaceBrandCode = x.MarketplaceBrandCode,
                        MarketplaceBrandName = x.MarketplaceBrandName,
                        MarketplaceId = x.MarketplaceId,
                        CompanyId = x.CompanyId
                    })
                    .ToListAsync();
                response.Success = true;
            });
        }

        public Response Upsert(UpsertRequest upsertRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .BrandRepository
                    .DbSet()
                    .Include(c => c.MarketplaceBrandMappings)
                    .FirstOrDefault(c => c.Id == upsertRequest.BrandId);

                var marketplaceBrandMappings = entity
                    .MarketplaceBrandMappings
                    .Where(x => x.CompanyId != this._companyFinder.FindId())
                    .ToList();

                entity.MarketplaceBrandMappings = new List<Domain.Entities.MarketplaceBrandMapping>();

                var MarketplaceBrandMappings = upsertRequest.MarketplaceBrandMappings.Where(x => !string.IsNullOrEmpty(x.MarketplaceBrandCode) || !string.IsNullOrEmpty(x.MarketplaceBrandName)).ToList();

                MarketplaceBrandMappings.ForEach(mcm =>
                {
                    entity.MarketplaceBrandMappings.Add(new Domain.Entities.MarketplaceBrandMapping
                    {
                        BrandId = upsertRequest.BrandId,
                        MarketplaceBrandCode = mcm.MarketplaceBrandCode,
                        MarketplaceBrandName = mcm.MarketplaceBrandName,
                        MarketplaceId = mcm.MarketplaceId
                    });
                });
                entity.MarketplaceBrandMappings.AddRange(marketplaceBrandMappings);

                this
                    ._unitOfWork
                    .BrandRepository
                    .Update(entity);

                response.Success = true;
                response.Message = "Kayıtlar başarılı bir şekilde güncellendi.";
            });
        }
        #endregion
    }
}