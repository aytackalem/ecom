﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.IO.Pipelines;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class CompanyService : ICompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;

        private readonly ISettingService _settingService;

        private readonly ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public CompanyService(IUnitOfWork unitOfWork, IImageHelper imageHelper, ISettingService settingService, ICompanyFinder companyFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            this._settingService = settingService;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Company> Create(Company dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Company>>((response) =>
            {
                Domain.Entities.Company entity = new()
                {
                    Name = dto.Name,
                    FullName = dto.FullName,
                    CompanyContact = new()
                    {
                        Address = dto.CompanyContact.Address,
                        PhoneNumber = dto.CompanyContact.PhoneNumber,
                        TaxNumber = dto.CompanyContact.TaxNumber,
                        Email = dto.CompanyContact.Email,
                        RegistrationNumber = dto.CompanyContact.RegistrationNumber,
                        TaxOffice = dto.CompanyContact.TaxOffice,
                        WebUrl = dto.CompanyContact.WebUrl
                    }
                };

                dto.CompanyConfigurations.ForEach(mc =>
                {
                    var companyConfiguration = entity.CompanyConfigurations.FirstOrDefault(x => x.Id == mc.Id);

                    if (companyConfiguration != null)
                        companyConfiguration.Value = mc.Value;
                });

                if (dto.FileName.StartsWith("data"))
                {
                    var directory = $@"{this._settingService.CategoryImagePath}";

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var fileName = $"{Guid.NewGuid()}.jpg";
                    var filePath = $@"{directory}\{fileName}";

                    this._imageHelper.Save(dto.FileName, filePath);

                    dto.FileName = fileName;
                    entity.FileName = dto.FileName;
                }

                var created = this._unitOfWork.CompanyRepository.Create(entity);

                if (!created)
                {
                    dto.Id = entity.Id;

                    response.Data = dto;
                    response.Success = false;
                    response.Message = "Firma oluşturuldu.";
                    return;
                }
                else
                    response.Message = "Firma oluşturulamadı.";

            }, (response, exception) => { });
        }

        public DataResponse<Company> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Company>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(x => x.CompanyConfigurations)
                    .Include(x => x.CompanyContact)
                    .Include(x => x.CompanySetting)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Şirket bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Company
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    FullName = entity.FullName,
                    FileName = $"{this._settingService.CompanyImageUrl}/{entity.FileName}",
                    CompanyConfigurations = entity
                        .CompanyConfigurations
                        .Select(x => new CompanyConfiguration
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Key = x.Key,
                            Value = x.Value
                        })
                        .ToList(),
                    CompanyContact = new CompanyContact
                    {
                        Address = entity.CompanyContact.Address,
                        Email = entity.CompanyContact.Email,
                        PhoneNumber = entity.CompanyContact.PhoneNumber,
                        RegistrationNumber = entity.CompanyContact.RegistrationNumber,
                        TaxNumber = entity.CompanyContact.TaxNumber,
                        TaxOffice = entity.CompanyContact.TaxOffice,
                        WebUrl = entity.CompanyContact.WebUrl
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Company> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Company>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(e => new Company
                    {
                        Id = e.Id,
                        Name = e.Name,
                        FullName = e.FullName
                    }).ToList();

                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<Company> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<Company>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(x => x.CompanySetting)
                    .FirstOrDefault(x => x.Id == this._companyFinder.FindId());

                if (entity == null)
                {
                    response.Message = $"Şirket bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Company
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    FullName = entity.FullName,
                    FileName = $"{this._settingService.CompanyImageUrl}/{entity.FileName}",
                    CompanySetting = new Application.Common.DataTransferObjects.CompanySetting
                    {
                        CreateNonexistProduct = entity.CompanySetting.CreateNonexistProduct,
                        DefaultCountryId = entity.CompanySetting.DefaultCountryId,
                        DefaultCurrencyId = entity.CompanySetting.DefaultCurrencyId,
                        DefaultLanguageId = entity.CompanySetting.DefaultLanguageId,
                        IncludeAccounting = entity.CompanySetting.IncludeAccounting,
                        IncludeECommerce = entity.CompanySetting.IncludeECommerce,
                        IncludeExport = entity.CompanySetting.IncludeExport,
                        IncludeMarketplace = entity.CompanySetting.IncludeMarketplace,
                        IncludeWMS = entity.CompanySetting.IncludeWMS,
                        OrderDetailMultiselect = entity.CompanySetting.OrderDetailMultiselect,
                        ShipmentSecret = entity.CompanySetting.ShipmentSecret,
                        ShipmentSecretProductName = entity.CompanySetting.ShipmentSecretProductName,
                        StockControl = entity.CompanySetting.StockControl,
                        Theme = entity.CompanySetting.Theme,
                        UUId = entity.CompanySetting.UUId,
                        WebUrl = entity.CompanySetting.WebUrl
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Company dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(x => x.CompanyContact)
                    .Include(x => x.CompanyConfigurations)
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Şirket bulunamadı.";
                    response.Success = false;
                    return;
                }

                dto.CompanyConfigurations.ForEach(mc =>
                {
                    var companyConfiguration = entity
                        .CompanyConfigurations
                        .FirstOrDefault(x => x.Id == mc.Id);

                    if (companyConfiguration != null)
                        companyConfiguration.Value = mc.Value;
                });

                if (dto.FileName.StartsWith("data"))
                {
                    var directory = $@"{this._settingService.CompanyImagePath}";

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var fileName = $"{Guid.NewGuid()}.jpg";
                    var filePath = $@"{directory}\{fileName}";

                    this._imageHelper.Save(dto.FileName, filePath);

                    dto.FileName = fileName;
                    entity.FileName = dto.FileName;
                }

                entity.Name = dto.Name;
                entity.FullName = dto.FullName;
                entity.CompanyContact.Address = dto.CompanyContact.Address;
                entity.CompanyContact.PhoneNumber = dto.CompanyContact.PhoneNumber;
                entity.CompanyContact.TaxNumber = dto.CompanyContact.TaxNumber;
                entity.CompanyContact.Email = dto.CompanyContact.Email;
                entity.CompanyContact.RegistrationNumber = dto.CompanyContact.RegistrationNumber;
                entity.CompanyContact.TaxOffice = dto.CompanyContact.TaxOffice;
                entity.CompanyContact.WebUrl = dto.CompanyContact.WebUrl;

                var updated = this
                    ._unitOfWork
                    .CompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Şirket güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}