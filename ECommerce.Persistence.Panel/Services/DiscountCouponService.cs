﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class DiscountCouponService : IDiscountCouponService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public DiscountCouponService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<DiscountCoupon> Create(DiscountCoupon dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<DiscountCoupon>>((response) =>
            {
                var hasAnyCode = this
                     ._unitOfWork
                     .DiscountCouponRepository
                     .DbSet()
                     .Any(dc => dc.Code == dto.Code && dc.Active);

                if (hasAnyCode)
                {
                    response.Success = false;
                    response.Message = "Bu kod ile daha önce bir indirim kuponu oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.DiscountCoupon
                {
                    CreatedDate = DateTime.Now,
                    Active = dto.Active,
                    Code = dto.Code,
                    ApplicationId = dto.ApplicationId,
                    DisountCouponSetting = new Domain.Entities.DiscountCouponSetting
                    {
                        CreatedDate = DateTime.Now,
                        Discount = dto.DisountCouponSetting.Discount,
                        DiscountIsRate = dto.DisountCouponSetting.DiscountIsRate,
                        Limit = dto.DisountCouponSetting.Limit,
                        SingleUse = dto.DisountCouponSetting.SingleUse
                    }
                };

                var created = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "indirim kuponu oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<DiscountCoupon> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<DiscountCoupon>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .DbSet()
                    .Include(dc => dc.DisountCouponSetting)
                    .FirstOrDefault(dc => dc.Id == id);

                if (entity == null)
                {
                    response.Message = $"indirim kuponu bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new DiscountCoupon
                {
                    Active = entity.Active,
                    ApplicationId = entity.ApplicationId,
                    Code = entity.Code,
                    Id = entity.Id,
                    DisountCouponSetting = new DiscountCouponSetting
                    {
                        Discount = entity.DisountCouponSetting.Discount,
                        DiscountIsRate = entity.DisountCouponSetting.DiscountIsRate,
                        Limit = entity.DisountCouponSetting.Limit,
                        SingleUse = entity.DisountCouponSetting.SingleUse,
                        Used = entity.DisountCouponSetting.Used
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<DiscountCoupon> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<DiscountCoupon>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .DbSet()
                    .Where(dc => string.IsNullOrEmpty(pagedRequest.Search) || dc.Code.Contains(pagedRequest.Search))
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .DbSet()
                    .Where(dc => string.IsNullOrEmpty(pagedRequest.Search) || dc.Code.Contains(pagedRequest.Search))
                    .Include(dc => dc.DisountCouponSetting)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(dc => new DiscountCoupon
                    {
                        Active = dc.Active,
                        ApplicationId = dc.ApplicationId,
                        Code = dc.Code,
                        Id = dc.Id,
                        DisountCouponSetting = new DiscountCouponSetting
                        {
                            Discount = dc.DisountCouponSetting.Discount,
                            DiscountIsRate = dc.DisountCouponSetting.DiscountIsRate,
                            Limit = dc.DisountCouponSetting.Limit,
                            SingleUse = dc.DisountCouponSetting.SingleUse,
                            Used = dc.DisountCouponSetting.Used
                        }
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(DiscountCoupon dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .DiscountCouponRepository
                     .DbSet()
                     .Any(b => b.Code == dto.Code && b.Id != dto.Id);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu kod ile daha önce bir indirim kuponu oluşturulduğundan güncellenemez.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"{dto.Code} ({dto.Id}) indirim kuponu bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Code = dto.Code;
                entity.Active = dto.Active;
                entity.DisountCouponSetting.SingleUse = dto.DisountCouponSetting.SingleUse;
                entity.DisountCouponSetting.Used = dto.DisountCouponSetting.Used;
                entity.DisountCouponSetting.Limit = dto.DisountCouponSetting.Limit;
                entity.DisountCouponSetting.DiscountIsRate = dto.DisountCouponSetting.DiscountIsRate;
                entity.DisountCouponSetting.Discount = dto.DisountCouponSetting.Discount;
                var updated = this
                 ._unitOfWork
                 .DiscountCouponRepository
                 .Update(entity);



                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "İndirim kuponu güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
