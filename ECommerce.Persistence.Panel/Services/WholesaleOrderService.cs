﻿using DocumentFormat.OpenXml.Drawing.Charts;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services;

public class WholesaleOrderService : IWholesaleOrderService
{
    #region Fields
    private readonly IUnitOfWork _unitOfWork;

    private readonly ISettingService _settingService;
    #endregion

    #region Constructors
    public WholesaleOrderService(IUnitOfWork unitOfWork, ISettingService settingService)
    {
        #region Fields
        this._unitOfWork = unitOfWork;
        this._settingService = settingService;
        #endregion
    }
    #endregion

    #region Methods
    public DataResponse<WholesaleOrder> Read(int id)
    {
        return ExceptionHandler.ResultHandle<DataResponse<WholesaleOrder>>((response) =>
        {
            var defaultPhotos = new List<ProductInformationPhoto>()
            {
                new ProductInformationPhoto
                {
                    FileName = "/img/product/0/no-image.jpg"
                }
            };

            var wholesaleOrder = this
                ._unitOfWork
                .WholesaleOrderRepository
                .DbSet()
                .Include(wo => wo.WholesaleOrderType)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationTranslations)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationPhotos)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationVariants)
                        .ThenInclude(piv => piv.VariantValue.VariantValueTranslations)
                .FirstOrDefault(wo => wo.Id == id);

            response.Data = new WholesaleOrder
            {
                Id = id,
                IsExport = wholesaleOrder.IsExport,
                WholesaleOrderType = new WholesaleOrderType
                {
                    Name = wholesaleOrder.WholesaleOrderType.Name
                },
                CreatedDate = wholesaleOrder.CreatedDate,
                WholesaleOrderDetails = wholesaleOrder
                    .WholesaleOrderDetails
                    .Select(wod =>
                    {
                        return new WholesaleOrderDetail
                        {
                            Quantity = wod.Quantity,
                            ProductInformation = new ProductInformation
                            {
                                ProductInformationName = $"{wod
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .Name} {wod
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .VariantValuesDescription}",
                                ProductInformationPhotos = wod
                                .ProductInformation
                                .ProductInformationPhotos.Count > 0 ?
                            wod
                                .ProductInformation
                                .ProductInformationPhotos
                                .Select(pip => new ProductInformationPhoto
                                {
                                    FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                }).ToList() :
                                defaultPhotos
                            }
                        };
                    })
                    .ToList()
            };
            response.Success = true;
        });
    }

    public PagedResponse<WholesaleOrder> Read(PagedRequest pagedRequest)
    {
        return ExceptionHandler.ResultHandle<PagedResponse<WholesaleOrder>>((response) =>
        {
            var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

            var iQueryable = this
                ._unitOfWork
                .WholesaleOrderRepository
                .DbSet()
                .Include(wo => wo.WholesaleOrderType)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationTranslations)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationPhotos)
                .Include(wo => wo.WholesaleOrderDetails)
                    .ThenInclude(wod => wod.ProductInformation.ProductInformationVariants)
                        .ThenInclude(piv => piv.VariantValue.VariantValueTranslations)
                .AsQueryable();

            foreach (var pLoop in parameters)
            {
                if (pLoop.StartsWith("WholesaleOrderType:"))
                {
                    var value = pLoop.Replace("WholesaleOrderType:", "");
                    if (value.Length > 0)
                    {
                        var values = value.Split(",", StringSplitOptions.RemoveEmptyEntries);
                        iQueryable = iQueryable.Where(iq => values.Contains(iq.WholesaleOrderTypeId));
                    }
                }
            }

            response.Page = pagedRequest.Page;
            response.PageRecordsCount = pagedRequest.PageRecordsCount;
            response.RecordsCount = iQueryable.Count();

            iQueryable = iQueryable.OrderByDescending(o => o.Id);

            var defaultPhotos = new List<ProductInformationPhoto>()
            {
                new ProductInformationPhoto
                {
                    FileName = "/img/product/0/no-image.jpg"
                }
            };

            response.Data = iQueryable
                .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                .Take(pagedRequest.PageRecordsCount)
                .Select(o => new WholesaleOrder
                {
                    Id = o.Id,
                    IsExport = o.IsExport,
                    WholesaleOrderType = new WholesaleOrderType
                    {
                        Name = o.WholesaleOrderType.Name
                    },
                    CreatedDate = o.CreatedDate,
                    WholesaleOrderDetails = o
                        .WholesaleOrderDetails
                        .Select(wod => new WholesaleOrderDetail
                        {
                            Quantity = wod.Quantity,
                            ProductInformation = new ProductInformation
                            {
                                Stock = wod.ProductInformation.Stock,
                                Barcode = wod.ProductInformation.Barcode,
                                StockCode = wod.ProductInformation.StockCode,
                                ProductInformationName = $"{wod
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .Name} {wod
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .VariantValuesDescription}",
                                ProductInformationPhotos = wod
                                            .ProductInformation
                                            .ProductInformationPhotos
                                            .Where(pip => !pip.Deleted).Count() > 0 ? wod
                                    .ProductInformation
                                    .ProductInformationPhotos
                                    .Where(pip => !pip.Deleted)
                                    .Select(pip => new ProductInformationPhoto
                                    {
                                        FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                    })
                                    .ToList() : defaultPhotos
                            }
                        })
                        .ToList()
                })
                .ToList();
            response.Success = true;
        }, (response, exception) => { });
    }

    public DataResponse<int> Create(WholesaleOrder dto)
    {
        return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
        {
            var wholesaleOrder = new Domain.Entities.WholesaleOrder
            {
                IsExport = dto.IsExport,
                CreatedDate = DateTime.Now,
                WholesaleOrderTypeId = "ON",
                WholesaleOrderDetails = dto
                    .WholesaleOrderDetails
                    .Select(wod => new Domain.Entities.WholesaleOrderDetail
                    {
                        ProductInformationId = wod.ProductInformationId,
                        Quantity = wod.Quantity
                    })
                    .ToList()
            };
            this._unitOfWork.WholesaleOrderRepository.Create(wholesaleOrder);

            response.Data = wholesaleOrder.Id;
            response.Success = true;
        });
    }

    public Response Update(int id, string wholesaleOrderTypeId)
    {
        return ExceptionHandler.ResultHandle<Response>((response) =>
        {
            var wholesaleOrder = this._unitOfWork.WholesaleOrderRepository.Read(id);
            wholesaleOrder.WholesaleOrderTypeId = wholesaleOrderTypeId;

            this._unitOfWork.WholesaleOrderRepository.Update(wholesaleOrder);

            response.Success = true;
        });
    }
    #endregion
}
