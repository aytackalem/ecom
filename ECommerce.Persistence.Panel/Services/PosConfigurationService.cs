﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class PosConfigurationService : IPosConfigurationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public PosConfigurationService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<PosConfiguration> Create(PosConfiguration dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<PosConfiguration>>((response) =>
            {
                var hasAnyDefault = this
                    ._unitOfWork
                    .PosConfigurationRepository
                    .DbSet()
                    .Any(p => p.Key == dto.Key);

                if (hasAnyDefault)
                {
                    response.Success = false;
                    response.Message = "Bu anahtar daha önce kullanıldığından yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.PosConfiguration
                {
                    Key = dto.Key,
                    Value = dto.Value,
                    PosId = dto.PosId
                };

                var created = this
                    ._unitOfWork
                    .PosConfigurationRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Pos konfigurasyonu oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<List<PosConfiguration>> Read(string posId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<PosConfiguration>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .PosConfigurationRepository
                    .DbSet()
                    .Where(vv => vv.PosId == posId)
                    .Select(vv => new PosConfiguration
                    {
                        Id = vv.Id,
                        Key = vv.Key,
                        PosId = vv.PosId,
                        Value = vv.Value
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(PosConfiguration dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .PosConfigurationRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Pos konfigurasyonu değeri bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Key = dto.Key;
                entity.Value = dto.Value;

                var updated = this
                    ._unitOfWork
                    .PosConfigurationRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Pos konfigurasyonu güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
