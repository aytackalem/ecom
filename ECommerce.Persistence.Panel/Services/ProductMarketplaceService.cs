﻿using AutoMapper;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductMarketplaceService : IProductMarketplaceService
    {
        #region Fields
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;

        private readonly ISettingService _settingService;

        private readonly IRandomHelper _randomHelper;

        private readonly ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public ProductMarketplaceService(IMapper mapper, IUnitOfWork unitOfWork, IImageHelper imageHelper, ISettingService settingService, IRandomHelper randomHelper, ICompanyFinder companyFinder)
        {
            #region Fields
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            this._settingService = settingService;
            this._randomHelper = randomHelper;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<Response> UpsertAsync(List<ProductMarketplace> productMarketplaces)
        {

            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {

                var repo = this._unitOfWork.ProductMarketplaceRepository;
                var dbSet = repo.DbSet();

                foreach (var theProductMarketplace in productMarketplaces)
                {
                    if (theProductMarketplace.Id == 0)
                    {
                        repo.Create(new Domain.Entities.ProductMarketplace
                        {
                            Id = theProductMarketplace.Id,
                            ProductId = theProductMarketplace.ProductId,
                            Active = theProductMarketplace.Active,
                            MarketplaceBrandCode = theProductMarketplace.MarketplaceBrandCode,
                            MarketplaceBrandName = theProductMarketplace.MarketplaceBrandName,
                            MarketplaceCategoryCode = theProductMarketplace.MarketplaceCategoryCode,
                            MarketplaceCategoryName = theProductMarketplace.MarketplaceCategoryName,
                            MarketplaceId = theProductMarketplace.MarketplaceId,
                            SellerCode = theProductMarketplace.SellerCode,
                            UUId = Guid.NewGuid().ToString(),
                            DeliveryDay = theProductMarketplace.DeliveryDay
                        });
                    }
                    else
                    {
                        var entity = dbSet.First(pm => pm.Id == theProductMarketplace.Id);

                        entity.Active = theProductMarketplace.Active;
                        entity.MarketplaceBrandCode = theProductMarketplace.MarketplaceBrandCode;
                        entity.MarketplaceBrandName = theProductMarketplace.MarketplaceBrandName;
                        entity.MarketplaceCategoryCode = theProductMarketplace.MarketplaceCategoryCode;
                        entity.MarketplaceCategoryName = theProductMarketplace.MarketplaceCategoryName;
                        entity.SellerCode = theProductMarketplace.SellerCode;
                        entity.DeliveryDay = theProductMarketplace.DeliveryDay;

                        repo.Update(entity);
                    }
                }

                response.Success = true;
                response.Message = "İşlem başarılı.";

            });

        }

        public async Task<DataResponse<List<ProductMarketplace>>> ReadOrGenerateAsync(Product product, List<Marketplace> marketplaces)
        {

            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<ProductMarketplace>>>(async response =>
            {
                if (marketplaces == null) return;
                List<Domain.Entities.ProductMarketplace> productMarketplaces = null;

                var dbSet = this._unitOfWork.ProductMarketplaceRepository.DbSet();
                productMarketplaces = await dbSet.Where(pm => pm.ProductId == product.Id).ToListAsync();

                foreach (var theMarketplace in marketplaces)
                {
                    if (productMarketplaces.Any(pm => pm.MarketplaceId == theMarketplace.Id))
                        continue;

                    var marketplaceCategoryMapping = await this._unitOfWork.MarketplaceCategoryMappingRepository.DbSet().FirstOrDefaultAsync(mcm => mcm.CategoryId == product.CategoryId && mcm.MarketplaceId == theMarketplace.Id && (mcm.CompanyId == null || mcm.CompanyId==_companyFinder.FindId()));

                    var marketplaceBrandMapping = await this._unitOfWork.MarketplaceBrandMappingRepository.DbSet().FirstOrDefaultAsync(mbm => mbm.BrandId == product.BrandId && mbm.MarketplaceId == theMarketplace.Id);

                    productMarketplaces.Add(new Domain.Entities.ProductMarketplace
                    {
                        ProductId = product.Id,
                        MarketplaceId = theMarketplace.Id,
                        Active = true,
                        SellerCode = product.SellerCode,
                        DeliveryDay = 2,
                        MarketplaceBrandCode = marketplaceBrandMapping?.MarketplaceBrandCode,
                        MarketplaceBrandName = marketplaceBrandMapping?.MarketplaceBrandName,
                        MarketplaceCategoryCode = marketplaceCategoryMapping?.MarketplaceCategoryCode,
                        MarketplaceCategoryName = marketplaceCategoryMapping?.MarketplaceCategoryName
                    });
                }

                response.Data = this._mapper.Map<List<ProductMarketplace>>(productMarketplaces);
                response.Success = true;

            });

        }
        #endregion
    }
}