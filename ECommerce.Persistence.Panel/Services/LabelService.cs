﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class LabelService : ILabelService
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;
        #endregion

        #region Constructors
        public LabelService(IUnitOfWork unitOfWork, IImageHelper imageHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Label> Create(Label dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Label>>((response) =>
            {
                var hasAnySameName = this
                                    ._unitOfWork
                                    .LabelRepository
                                    .DbSet()
                                    .Any(c => !c.Deleted && c.LabelTranslations.Any(ct => ct.LabelId != dto.Id && dto.LabelTranslations.Select(ct2 => ct2.Value).Contains(ct.Value)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir etiket oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }


                var entity = new Domain.Entities.Label
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    Deleted = false,
                    DisplayOrder=dto.DisplayOrder,
                    LabelTranslations = dto.LabelTranslations.Select(ct => new Domain.Entities.LabelTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Value = ct.Value,
                        

                    }).ToList()
                };

                var created = this
                    ._unitOfWork
                    .LabelRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Etiket oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Label> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Label>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .LabelRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Etiket bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Label
                {
                    Id = entity.Id,
                    DisplayOrder = entity.DisplayOrder,
                    Active = entity.Active,
                    LabelTranslations = entity.LabelTranslations.Select(x => new LabelTranslation
                    {
                        Id = x.Id,
                        LanguageId = x.LanguageId,
                        Value = x.Value
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Label> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Label>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .LabelRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .LabelRepository
                    .DbSet()
                    .Include(c => c.LabelTranslations)
                    .Where(x => !x.Deleted)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(l => new Label
                    {
                        Id = l.Id,
                        Active = l.Active,
                        LabelTranslations = l.LabelTranslations.Select(lb => new LabelTranslation
                        {
                            Id = lb.Id,
                            LanguageId = lb.LanguageId,
                            Value = lb.Value
                        }).ToList()

                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .LabelRepository
                    .DbSet()
                    .Include(x => x.LabelTranslations)
                    .Where(x => !x.Deleted)
                    .Select(l => new KeyValue<int, string>
                    {
                        Key = l.Id,
                        Value = l.LabelTranslations.FirstOrDefault().Value

                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Label dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var hasAnySameName = this
                ._unitOfWork
                .LabelRepository
                .DbSet()
                .Any(c => !c.Deleted && c.LabelTranslations.Any(ct => ct.LabelId != dto.Id && dto.LabelTranslations.Select(ct2 => ct2.Value).Contains(ct.Value)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir etiket oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .LabelRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Etiket bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.DisplayOrder = dto.DisplayOrder;
                foreach (var lbLoop in entity.LabelTranslations)
                {
                    var _labelTranslation = dto.LabelTranslations.FirstOrDefault(x => x.LanguageId == lbLoop.LanguageId);
                    if (lbLoop != null)
                    {
                        lbLoop.Value = _labelTranslation.Value;

                    }
                }


                var updated = this
                    ._unitOfWork
                    .LabelRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Etiket güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(Label dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                var entity = this
                    ._unitOfWork
                    .LabelRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Ürün etiketi bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;

                var updated = this
                    ._unitOfWork.LabelRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Sıkça Sorulan Soru silindi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
