﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ExpenseService : IExpenseService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ExpenseService(IUnitOfWork unitOfWork, ISettingService settingService, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Expense> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Expense>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ExpenseRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Sipariş bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Expense
                {
                    Id = entity.Id,
                    ExpenseSourceId = entity.ExpenseSourceId,
                    Amount = entity.Amount,
                    ExpenseDate = entity.ExpenseDate,
                    Note = entity.Note,
                    MarketplaceId = entity.MarketplaceId,
                    OrderSourceId = entity.OrderSourceId,
                    ProductInformationId = entity.ProductInformationId
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Expense> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Expense>>((response) =>
            {
                var iQueryable = this
                    ._unitOfWork
                    .ExpenseRepository
                    .DbSet()
                    .AsQueryable();

                var parameters = pagedRequest.Search.Split(new string[] { "_" }, System.StringSplitOptions.RemoveEmptyEntries);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Note:"))
                    {
                        var value = pLoop.Replace("Note:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(iq => iq.Note.Contains(value));
                        }
                    }
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable
                    .Count();

                response.Data = iQueryable
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(o => new Expense
                    {
                        Id = o.Id,
                        ExpenseSourceId = o.ExpenseSourceId,
                        Amount = o.Amount,
                        ExpenseDate = o.ExpenseDate,
                        Note = o.Note
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Expense dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var expense = this
                    ._unitOfWork
                    .ExpenseRepository
                    .DbSet()
                    .FirstOrDefault(x => x.Id == dto.Id);

                expense.Amount = dto.Amount;
                expense.Note = dto.Note;
                expense.MarketplaceId = dto.MarketplaceId;
                expense.ExpenseSourceId = dto.ExpenseSourceId;
                expense.ProductInformationId = dto.ProductInformationId;
                expense.OrderSourceId = dto.OrderSourceId;
                expense.ExpenseDate = dto.ExpenseDate;

                var update = this._unitOfWork.ExpenseRepository.Update(expense);
                if (!update)
                {
                    response.Message = "Gider güncellenemedi lütfen tekrar deneyiniz.";
                    response.Success = false;
                }

                response.Message = "Gider başarılı bir şekilde güncellendi.";
                response.Success = true;
            });
        }

        public Response Create(Expense dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this
                    ._unitOfWork
                    .ExpenseRepository
                    .Create(new Domain.Entities.Expense
                    {
                        Amount = dto.Amount,
                        ExpenseDate = dto.ExpenseDate,
                        ExpenseSourceId = dto.ExpenseSourceId,
                        Note = dto.Note,
                        MarketplaceId = dto.MarketplaceId,
                        OrderSourceId = dto.OrderSourceId,
                        ProductInformationId = dto.ProductInformationId
                    });

                response.Message = "Gider başarılı bir şekilde kaydedildi.";
                response.Success = true;
            });
        }
        #endregion
    }
}
