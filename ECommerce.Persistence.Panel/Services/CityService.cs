﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class CityService : ICityService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public CityService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods     
        public DataResponse<List<City>> ReadByCountryId(int countryId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<City>>>((response) =>
            {
                var entities = this._unitOfWork.CityRepository.DbSet().Where(n => n.CountryId == countryId).OrderBy(n => n.Name).ToList();
                if (entities?.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "İl bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = entities
                        .Select(e => new City
                        {
                            Id = e.Id,
                            Name = e.Name,
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "İl başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İlçeler başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.CitysByContryId, countryId, this._dbNameFinder.FindName()));
        }
        #endregion
    }
}
