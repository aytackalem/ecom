﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class AccountingCompanyService : IAccountingCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public AccountingCompanyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<AccountingCompany> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCompany>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .Include(x => x.Accounting)
                    .Include(x => x.AccountingConfigurations)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Muhasabe firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new AccountingCompany
                {
                    Id = entity.Id,
                    Name = entity.Accounting.Name,
                    AccountingConfigurations = entity.AccountingConfigurations.Select(x => new AccountingConfiguration
                    {
                        Id = x.Id,
                        Key = x.Key,
                        Value = x.Value
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<AccountingCompany> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<AccountingCompany>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new AccountingCompany
                    {
                        Id = b.Id,
                        Name = b.Accounting.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(AccountingCompany dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                  ._unitOfWork
                  .AccountingCompanyRepository
                  .DbSet()
                  .Include(x => x.AccountingConfigurations)
                  .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Muhasebe firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                dto.AccountingConfigurations.ForEach(mc =>
                {
                    var accountingConfigurations = entity.AccountingConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                    if (accountingConfigurations != null)
                    {
                        accountingConfigurations.Value = mc.Value;
                    }
                });

                var updated = this
                    ._unitOfWork.AccountingCompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Muhasebe firması güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
