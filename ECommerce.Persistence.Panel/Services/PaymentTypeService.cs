﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class PaymentTypeService : IPaymentTypeService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public PaymentTypeService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<PaymentType> Create(PaymentType dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<PaymentType>>((response) =>
            {
                //Ödeme tipi ekleme kaldırıldı.
                //var hasAnySameName = this
                //     ._unitOfWork
                //     .PaymentTypeDomainRepository
                //     .DbSet()
                //     .Any(c => c.PaymentType.PaymentTypeTranslations.Any(ct => dto.PaymentTypeTranslations.Select(ct2 => ct2.Name).Contains(ct.Name)));

                //if (hasAnySameName)
                //{
                //    response.Success = false;
                //    response.Message = "Bu isimle daha önce bir ödeme tipi oluşturulduğundan yenisi oluşturulamaz.";
                //    return;
                //}

                //var entity = new Domain.Entities.PaymentType
                //{
                //    Id = dto.Id,
                //    Active = dto.Active,
                //    PaymentTypeTranslations = dto.PaymentTypeTranslations.Select(ct => new Domain.Entities.PaymentTypeTranslation
                //    {
                //        LanguageId = ct.LanguageId,
                //        Name = ct.Name
                //    }).ToList(),
                //    PaymentTypeDomains = new List<Domain.Entities.PaymentTypeDomain>
                //    {
                //        new Domain.Entities.PaymentTypeDomain
                //        {
                //         PaymentTypeSetting = new Domain.Entities.PaymentTypeSetting
                //            {
                //                Cost = dto.PaymentTypeSetting.Cost,
                //                Discount = dto.PaymentTypeSetting.Discount,
                //                CostIsRate = dto.PaymentTypeSetting.CostIsRate,
                //                DiscountIsRate = dto.PaymentTypeSetting.DiscountIsRate
                //            },
                //         Active=true
                //        }
                //    }

                //};

                //var created = this
                //    ._unitOfWork.PaymentTypeRepository
                //    .Create(entity);

                //if (!created)
                //{
                //    response.Success = false;
                //    return;
                //}

                //dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "ödeme tipi oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<PaymentType> Read(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<PaymentType>>((response) =>
            {
                var entity = this.ReadEntity(id);
                if (entity == null)
                {
                    response.Message = $"ödeme tipi bulunamadı.";
                    response.Success = false;
                    return;
                }

                var paymentTypeDomain = entity.PaymentTypeDomains.FirstOrDefault();


                var dto = new PaymentType
                {
                    Id = entity.Id,
                    Active = paymentTypeDomain.Active,
                    PaymentTypeSetting = new PaymentTypeSetting
                    {
                        Cost = paymentTypeDomain.PaymentTypeSetting.Cost,
                        CostIsRate = paymentTypeDomain.PaymentTypeSetting.CostIsRate,
                        Discount = paymentTypeDomain.PaymentTypeSetting.Discount,
                        DiscountIsRate = paymentTypeDomain.PaymentTypeSetting.DiscountIsRate
                    },
                    PaymentTypeTranslations = entity.PaymentTypeTranslations.Select(ct => new PaymentTypeTranslation
                    {
                        LanguageId = ct.LanguageId,
                        Name = ct.Name
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<PaymentType> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<PaymentType>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .PaymentTypeDomainRepository
                    .DbSet()
                    .Include(x => x.PaymentType)
                    .Count();


                response.Data = this
                    ._unitOfWork
                    .PaymentTypeDomainRepository
                    .DbSet()
                    .Include(c => c.PaymentType)
                    .ThenInclude(c => c.PaymentTypeTranslations)
                    .Where(c => (string.IsNullOrEmpty(pagedRequest.Search) || c.PaymentType.PaymentTypeTranslations.Any(ct => ct.Name.Contains(pagedRequest.Search))))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new PaymentType
                    {
                        Active = c.Active,
                        Id = c.PaymentType.Id,
                        PaymentTypeTranslations = c.PaymentType.PaymentTypeTranslations.Select(ct => new PaymentTypeTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Name = ct.Name
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .PaymentTypeDomainRepository
                    .DbSet()
                    .Include(c => c.PaymentType)
                    .ThenInclude(c => c.PaymentTypeTranslations)
                    .Where(x => x.Active)
                    .Select(c => new KeyValue<string, string>
                    {
                        Key = c.PaymentType.Id,
                        Value = c.PaymentType
                            .PaymentTypeTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(PaymentType dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {



                var entity = this.ReadEntity(dto.Id);
                if (entity == null)
                {
                    response.Message = $"ödeme tipi bulunamadı.";
                    response.Success = false;
                    return;
                }

                var paymentTypeDomain = entity.PaymentTypeDomains.FirstOrDefault();

                paymentTypeDomain.Active = dto.Active;
                paymentTypeDomain.PaymentTypeSetting.Cost = dto.PaymentTypeSetting.Cost;
                paymentTypeDomain.PaymentTypeSetting.CostIsRate = dto.PaymentTypeSetting.CostIsRate;
                paymentTypeDomain.PaymentTypeSetting.Discount = dto.PaymentTypeSetting.Discount;
                paymentTypeDomain.PaymentTypeSetting.DiscountIsRate = dto.PaymentTypeSetting.DiscountIsRate;

                var updated = this
                    ._unitOfWork.PaymentTypeRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "ödeme tipi güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.PaymentType ReadEntity(string id)
        {
            var paymentTypeDomain = this
                ._unitOfWork
                .PaymentTypeDomainRepository
                .DbSet()
                .Include(pt => pt.PaymentType)
                .ThenInclude(pt => pt.PaymentTypeTranslations)
                .Include(pt => pt.PaymentTypeSetting)
                .Where(pt => pt.PaymentType.Id == id)
                .FirstOrDefault();


            return paymentTypeDomain?.PaymentType;

        }
        #endregion
    }
}
