﻿using Dapper;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductInformationService : IProductInformationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;

        private readonly ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public ProductInformationService(IUnitOfWork unitOfWork, ISettingService settingService, ICompanyFinder companyFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(x => x.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                    .Select(l => new KeyValue<int, string>
                    {
                        Key = l.Id,
                        Value = l.ProductInformationTranslations.FirstOrDefault().Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, ProductInformation>>> ReadAsKeyProductInformation(int take, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, ProductInformation>>>>((response) =>
            {
                var productInformations = this
                     ._unitOfWork
                     .ProductInformationRepository
                     .DbSet()
                     .Where(pi => pi.Type != "PC" && !pi.Product.IsOnlyHidden && pi.ProductInformationTranslations.Any(pt => pt.Name.Contains(q)))
                     .Take(take)
                     .Select(pi => new KeyValue<int, ProductInformation>
                     {
                         Key = pi.Id,
                         Value = new ProductInformation
                         {
                             ProductInformationName = $"{pi.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name} {$"{pi.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == "TR").VariantValuesDescription}"}",
                             ProductInformationPriceses = new List<ProductInformationPrice>
                             {
                                 new ProductInformationPrice
                                 {
                                     UnitPrice = pi.ProductInformationPriceses.FirstOrDefault(pip=>pip.CurrencyId=="TL").UnitPrice
                                 }
                             },
                             ProductInformationPhotos = new List<ProductInformationPhoto>
                             {
                                 new ProductInformationPhoto
                                 {
                                     FileName = $"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault().FileName}",
                                 }
                             }
                         }
                     })
                     .ToList();

                response.Data = productInformations.OrderBy(x => x.Value.ProductInformationName).ToList();
                response.Success = response.Data?.Count > 0;
            });
        }

        public DataResponse<List<ProductInformation>> ReadPrice(int productId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductInformation>>>(response =>
            {
                response.Data = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(pi => pi.ProductId == productId)
                    .Select(pi => new ProductInformation
                    {
                        Id = pi.Id,
                        ProductInformationName = pi
                            .ProductInformationTranslations
                            .FirstOrDefault()
                            .Name + pi
                            .ProductInformationTranslations
                            .FirstOrDefault()
                            .VariantValuesDescription,
                        ProductInformationPhotos = new List<ProductInformationPhoto>
                        {
                            new ProductInformationPhoto
                            {
                                FileName = $"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault().FileName}"
                            }
                        },
                        ProductInformationMarketplaces = pi
                            .ProductInformationMarketplaces
                            .Where(pim => pim.CompanyId == this._companyFinder.FindId()).Select(pim => new ProductInformationMarketplace
                            {
                                Id = pim.Id,
                                ListUnitPrice = pim.ListUnitPrice,
                                UnitPrice = pim.UnitPrice,
                                MarketplaceId = pim.MarketplaceId,
                                AccountingPrice = pim.AccountingPrice
                            }).ToList(),
                        ProductInformationPriceses = new List<ProductInformationPrice>
                        {
                            pi.ProductInformationPriceses.Select(pip => new ProductInformationPrice
                            {
                                Id = pip.Id,
                                ListUnitPrice = pip.ListUnitPrice,
                                UnitPrice = pip.UnitPrice,
                                AccountingPrice = pip.AccountingPrice
                            })
                            .FirstOrDefault()
                        }
                    })
                    .ToList();
                response.Success = true;
            });
        }

        public Response UpdatePrice(List<ProductInformation> productInformations)
        {
            if (productInformations.Any(pi => pi.ProductInformationPriceses.Any(pip => pip.ListUnitPrice < pip.UnitPrice) || pi.ProductInformationMarketplaces != null && pi.ProductInformationMarketplaces.Any(pim => pim.ListUnitPrice < pim.UnitPrice)))
                return new Response { Success = false, Message = "Satış fiyatı, liste fiyatından büyük olamaz." };

            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                productInformations.ForEach(theProductInformation =>
                {
                    var productInformation = this
                        ._unitOfWork
                        .ProductInformationRepository
                        .DbSet()
                        .Include(pi => pi.ProductInformationPriceses)
                        .Include(pi => pi.ProductInformationMarketplaces)
                        .First(pi => pi.Id == theProductInformation.Id);

                    theProductInformation.ProductInformationPriceses.ForEach(theProductInformationPrice =>
                    {
                        var productInformationPrice = productInformation
                            .ProductInformationPriceses
                            .First(pip => pip.Id == theProductInformationPrice.Id);
                        productInformationPrice.ListUnitPrice = theProductInformationPrice.ListUnitPrice;
                        productInformationPrice.UnitPrice = theProductInformationPrice.UnitPrice;
                        productInformationPrice.AccountingPrice = theProductInformationPrice.AccountingPrice;
                    });

                    if (theProductInformation.ProductInformationMarketplaces != null)
                        theProductInformation.ProductInformationMarketplaces.ForEach(theProductInformationMarketplaces =>
                        {
                            var productInformationMarketplace = productInformation
                                .ProductInformationMarketplaces
                                .First(pim => pim.Id == theProductInformationMarketplaces.Id);
                            productInformationMarketplace.ListUnitPrice = theProductInformationMarketplaces.ListUnitPrice;
                            productInformationMarketplace.UnitPrice = theProductInformationMarketplaces.UnitPrice;
                            productInformationMarketplace.DirtyPrice = true;
                            productInformationMarketplace.AccountingPrice = theProductInformationMarketplaces.AccountingPrice;
                        });

                    this
                        ._unitOfWork
                        .ProductInformationRepository
                        .Update(productInformation);
                });
                response.Success = true;
                response.Message = "Fiyatlar başarılı şekilde güncellendi.";
            });
        }

        public DataResponse<List<ProductInformation>> Read(List<int> ids)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductInformation>>>(response =>
            {
                response.Data = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(pi => ids.Contains(pi.Id))
                    .Select(pi => new ProductInformation
                    {
                        Id = pi.Id,
                        SkuCode = pi.SkuCode,
                        Product = new Product
                        {
                            SellerCode = pi.Product.SellerCode
                        },
                        ProductInformationPhotos = new List<ProductInformationPhoto>
                        {
                            new ProductInformationPhoto
                            {
                                FileName = $"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault().FileName}"
                            }
                        },
                        ProductInformationVariants = pi
                            .ProductInformationVariants
                            .Select(piv => new ProductInformationVariant()
                            {
                                VariantValue = new VariantValue
                                {
                                    VariantValueTranslations = new List<VariantValueTranslation>
                                    {
                                        new VariantValueTranslation
                                        {
                                            Value = piv.VariantValue.VariantValueTranslations.FirstOrDefault().Value
                                        }
                                    },
                                    Variant = new Variant
                                    {
                                        VariantTranslations = new List<VariantTranslation>
                                        {
                                            new VariantTranslation
                                            {
                                                Name = piv.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name
                                            }
                                        }
                                    }
                                }
                            })
                            .ToList()
                    })
                    .ToList();
                response.Success = true;
            });
        }

        public Response UpdateCategorization(string skuCode, string categorization)
        {
            SqlConnection sqlConnection = null;
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                using (sqlConnection = new SqlConnection(this._settingService.ConnectionString))
                {
                    sqlConnection.Execute(@"
Update  ProductInformations
Set     Categorization = @Categorization,
        SpecialCategorization = 1
Where   SkuCode = @SkuCode", new { SkuCode = skuCode, Categorization = categorization });
                }

                response.Success = true;
                response.Message = "Güncelleme başarılı";
            }, (response, exception) =>
            {
                response.Success = false;

                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                    sqlConnection.Close();
            });
        }
        #endregion
    }
}
