﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class BankService : IBankService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public BankService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Bank> Create(Bank dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Bank>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .BankRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isim ile daha önce bir banka oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var hasAnySameId = this
                     ._unitOfWork
                     .BankRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameId)
                {
                    response.Success = false;
                    response.Message = "Bu no ile daha önce bir marka oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Bank
                {
                    Id = dto.Id,
                    Name = dto.Name,
                    Active = dto.Active,
                    PosId = dto.PosId
                };

                var created = this
                    ._unitOfWork
                    .BankRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Banka oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Bank> Read(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Bank>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .BankRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Banka bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Bank
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Name = entity.Name,
                    PosId = entity.PosId
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Bank> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Bank>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .BankRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .BankRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Bank
                    {
                        Active = b.Active,
                        Id = b.Id,
                        Name = b.Name,
                        PosId = b.PosId
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Bank dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .BankRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name && b.Id != dto.Id);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir banka oluşturulduğundan banka güncellenemez.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .BankRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Banka bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;
                entity.Active = dto.Active;
                entity.PosId = dto.PosId;

                var updated = this
                    ._unitOfWork
                    .BankRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Banka güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
