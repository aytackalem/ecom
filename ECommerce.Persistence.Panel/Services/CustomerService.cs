﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Customers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class CustomerService : ICustomerService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CustomerService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<Customer>> Search(SearchRequest searchRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Customer>>>((response) =>
            {
                var iQueryable = this
                    ._unitOfWork
                    .CustomerRepository
                    .DbSet()
                    .Include(cc => cc.CustomerContact)
                    .AsQueryable();

                if (!string.IsNullOrEmpty(searchRequest.Name))
                    iQueryable = iQueryable.Where(c => c.Name.TrimEnd().Contains(searchRequest.Name.ToUpper()) || c.Name.TrimEnd().Contains(searchRequest.Name.ToUpper() + " "));
                if (!string.IsNullOrEmpty(searchRequest.Surname))
                    iQueryable = iQueryable.Where(c => c.Surname.TrimEnd().Contains(searchRequest.Surname.ToUpper()) || c.Surname.TrimEnd().Contains(searchRequest.Surname.ToUpper() + " "));

                if (!string.IsNullOrEmpty(searchRequest.Phone))
                    iQueryable = iQueryable.Where(c => c.CustomerContact.Phone == searchRequest.Phone);

                response.Data = iQueryable                    
                    .GroupBy(x => new { x.Name, x.Surname, x.CustomerContact.Phone })
                    .Take(50)
                    .Select(c => new Customer
                    {
                        Id = c.First().Id,
                        Name = c.Key.Name,
                        Surname = c.Key.Surname,
                        CustomerContact = new() { Phone = c.Key.Phone }
                    })
                    .ToList();

                response.Success = true;
                response.Message = "Müşteriler getirildi.";

            }, (response, exception) => { });
        }

        public DataResponse<Application.Panel.DataTransferObjects.Order> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Application.Panel.DataTransferObjects.Order>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => o.CustomerId == id)
                    .Select(o => new Application.Panel.DataTransferObjects.Order
                    {
                        OrderDeliveryAddress = new Application.Panel.DataTransferObjects.OrderDeliveryAddress
                        {
                            Address = o.OrderDeliveryAddress.Address,
                            PhoneNumber = o.OrderDeliveryAddress.PhoneNumber,
                            FirstName = o.OrderDeliveryAddress.Recipient,
                            LastName = o.OrderDeliveryAddress.Recipient,
                            NeighborhoodId = o.OrderDeliveryAddress.NeighborhoodId,
                            DistrictId = o.OrderDeliveryAddress.Neighborhood.District.Id,
                            CityId = o.OrderDeliveryAddress.Neighborhood.District.City.Id,
                            CountryId = o.OrderDeliveryAddress.Neighborhood.District.City.Country.Id,
                            Neighborhood = new Neighborhood
                            {
                                Id = o.OrderDeliveryAddress.NeighborhoodId,
                                Name = o.OrderDeliveryAddress.Neighborhood.Name,
                                District = new District
                                {
                                    Id = o.OrderDeliveryAddress.Neighborhood.DistrictId,
                                    Name = o.OrderDeliveryAddress.Neighborhood.District.Name,
                                    City = new City
                                    {
                                        Id = o.OrderDeliveryAddress.Neighborhood.District.CityId,
                                        Name = o.OrderDeliveryAddress.Neighborhood.District.City.Name
                                    }
                                }
                            }
                        },
                        OrderInvoiceInformation = new OrderInvoiceInformation
                        {
                            Address = o.OrderInvoiceInformation.Address,
                            FirstName = o.OrderInvoiceInformation.FirstName,
                            LastName = o.OrderInvoiceInformation.LastName,
                            Mail = o.OrderInvoiceInformation.Mail,
                            Phone = o.OrderInvoiceInformation.Phone,
                            TaxNumber = o.OrderInvoiceInformation.TaxNumber,
                            TaxOffice = o.OrderInvoiceInformation.TaxOffice,
                            NeighborhoodId = o.OrderInvoiceInformation.NeighborhoodId,
                            DistrictId = o.OrderInvoiceInformation.Neighborhood.District.Id,
                            CityId = o.OrderInvoiceInformation.Neighborhood.District.City.Id,
                            CountryId = o.OrderInvoiceInformation.Neighborhood.District.City.Country.Id,
                            Neighborhood = new Neighborhood
                            {
                                Id = o.OrderInvoiceInformation.NeighborhoodId,
                                Name = o.OrderInvoiceInformation.Neighborhood.Name,
                                District = new District
                                {
                                    Id = o.OrderInvoiceInformation.Neighborhood.DistrictId,
                                    Name = o.OrderInvoiceInformation.Neighborhood.District.Name,
                                    City = new City
                                    {
                                        Id = o.OrderInvoiceInformation.Neighborhood.District.CityId,
                                        Name = o.OrderInvoiceInformation.Neighborhood.District.City.Name
                                    }
                                }
                            }
                        }
                    })
                    .FirstOrDefault();

                response.Success = true;
                response.Message = "Müşteriler getirildi.";

            }, (response, exception) => { });
        }
        #endregion
    }
}
