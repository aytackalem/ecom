﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Persistence.Panel.Services
{
    public class FrequentlyAskedQuestionService : IFrequentlyAskedQuestionService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public FrequentlyAskedQuestionService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<FrequentlyAskedQuestion> Create(FrequentlyAskedQuestion dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<FrequentlyAskedQuestion>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .FrequentlyAskedQuestionRepository
                     .DbSet()
                     .Any(c => c.FrequentlyAskedQuestionTranslations.Any(ct => dto.FrequentlyAskedQuestionTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir sıkça sorulan soru oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.FrequentlyAskedQuestion
                {
                    Active = dto.Active,
                    CreatedDate = DateTime.Now,
                    FrequentlyAskedQuestionTranslations = dto.FrequentlyAskedQuestionTranslations.Select(ct => new Domain.Entities.FrequentlyAskedQuestionTranslation
                    {
                        CreatedDate = DateTime.Now,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                        Description = ct.Description
                    }).ToList()
                };

                var created = this
                    ._unitOfWork.FrequentlyAskedQuestionRepository
                    .Create(entity);
                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;



                response.Data = dto;
                response.Success = true;
                response.Message = "Sıkça Sorulan Soru oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<FrequentlyAskedQuestion> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<FrequentlyAskedQuestion>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Sıkça Sorulan Soru bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new FrequentlyAskedQuestion
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    FrequentlyAskedQuestionTranslations = entity.FrequentlyAskedQuestionTranslations.Select(ct => new FrequentlyAskedQuestionTranslation
                    {
                        Description = ct.Description,
                        LanguageId = ct.LanguageId,
                        Header = ct.Header,
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<FrequentlyAskedQuestion> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<FrequentlyAskedQuestion>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .DbSet()
                    .Where(c => !c.Deleted && (string.IsNullOrEmpty(pagedRequest.Search) || c.FrequentlyAskedQuestionTranslations.Any(ct => ct.Header.Contains(pagedRequest.Search))))
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .DbSet()
                    .Include(c => c.FrequentlyAskedQuestionTranslations)
                    .Where(c => !c.Deleted && (string.IsNullOrEmpty(pagedRequest.Search) || c.FrequentlyAskedQuestionTranslations.Any(ct => ct.Header.Contains(pagedRequest.Search))))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(c => new FrequentlyAskedQuestion
                    {
                        Id = c.Id,
                        Active = c.Active,
                        FrequentlyAskedQuestionTranslations = c.FrequentlyAskedQuestionTranslations.Select(ct => new FrequentlyAskedQuestionTranslation
                        {
                            LanguageId = ct.LanguageId,
                            Header = ct.Header,
                            Description = ct.Description
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .DbSet()
                    .Select(c => new KeyValue<int, string>
                    {
                        Key = c.Id,
                        Value = c
                            .FrequentlyAskedQuestionTranslations
                            .Where(ct => ct.LanguageId == "TR")
                            .FirstOrDefault()
                            .Header
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(FrequentlyAskedQuestion dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .FrequentlyAskedQuestionRepository
                     .DbSet()
                     .Any(c => c.FrequentlyAskedQuestionTranslations.Any(ct => ct.FrequentlyAskedQuestionId != dto.Id && dto.FrequentlyAskedQuestionTranslations.Select(ct2 => ct2.Header).Contains(ct.Header)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir Sıkça Sorulan Soru oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }


                var entity = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Sıkça Sorulan Soru bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                foreach (var FrequentlyAskedQuestionTranslation in dto.FrequentlyAskedQuestionTranslations)
                {
                    var _FrequentlyAskedQuestionTranslation = entity.FrequentlyAskedQuestionTranslations.FirstOrDefault(x => x.LanguageId == FrequentlyAskedQuestionTranslation.LanguageId);

                    if (_FrequentlyAskedQuestionTranslation != null)
                    {
                        _FrequentlyAskedQuestionTranslation.Header = FrequentlyAskedQuestionTranslation.Header;
                        _FrequentlyAskedQuestionTranslation.Description = FrequentlyAskedQuestionTranslation.Description;
                    }
                }

                var updated = this
                    ._unitOfWork.FrequentlyAskedQuestionRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Sıkça Sorulan Soru güncellendi.";
            }, (response, exception) => { });
        }


        public Response Delete(FrequentlyAskedQuestion dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {



                var entity = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Sıkça Sorulan Soru bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;

                var updated = this
                    ._unitOfWork.FrequentlyAskedQuestionRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Sıkça Sorulan Soru silindi.";
            }, (response, exception) => { });
        }
        #endregion


    }
}
