﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class EmailProviderCompanyService : IEmailProviderCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public EmailProviderCompanyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<EmailProviderCompany> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailProviderCompany>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .EmailProviderCompanyRepository
                    .DbSet()
                    .Include(x => x.EmailProvider)
                    .Include(x => x.EmailProviderConfigurations)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Email firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new EmailProviderCompany
                {
                    Id = entity.Id,
                    Name = entity.EmailProvider.Name,
                    EmailProviderConfigurations = entity.EmailProviderConfigurations.Select(x => new EmailProviderConfiguration
                    {
                        Id = x.Id,
                        Key = x.Key,
                        Value = x.Value
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<EmailProviderCompany> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<EmailProviderCompany>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .EmailProviderCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .EmailProviderCompanyRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new EmailProviderCompany
                    {
                        Id = b.Id,
                        Name = b.EmailProvider.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(EmailProviderCompany dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                  ._unitOfWork
                  .EmailProviderCompanyRepository
                  .DbSet()
                  .Include(x => x.EmailProviderConfigurations)
                  .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Email firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                dto.EmailProviderConfigurations.ForEach(mc =>
                {
                    var emailProviderConfigurations = entity.EmailProviderConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                    if (emailProviderConfigurations != null)
                    {
                        emailProviderConfigurations.Value = mc.Value;
                    }
                });

                var updated = this
                    ._unitOfWork.EmailProviderCompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Email firması güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
