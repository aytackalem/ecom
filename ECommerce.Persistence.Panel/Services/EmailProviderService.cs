﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class EmailProviderService : IEmailProviderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public EmailProviderService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<EmailProvider> Create(EmailProvider dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailProvider>>((response) =>
            {
                var hasAnySameId = this
                     ._unitOfWork
                     .PosRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameId)
                {
                    response.Success = false;
                    response.Message = "Bu no ile daha önce bir sms sağlayıcı oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.EmailProvider
                {
                    Id = dto.Id,
                    Name = dto.Name
                };

                //var created = this
                //    ._unitOfWork
                //    .EmailProviderRepository
                //    .Create(entity);

                //if (!created)
                //{
                //    response.Success = false;
                //    return;
                //}

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Email sağlayıcısı oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<EmailProvider> Read(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailProvider>>((response) =>
            {
                var entity = this.ReadEntity(id);
                if (entity == null)
                {
                    response.Message = $"Email sağlayıcı bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new EmailProvider
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    //EmailProviderConfigurations = entity
                    //    .EmailProviderConfigurations
                    //    .Select(spc => new EmailProviderConfiguration
                    //    {
                    //        Id = spc.Id,
                    //        Key = spc.Key,
                    //        Value = spc.Value
                    //    })
                    //    .ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<EmailProvider> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<EmailProvider>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                //response.RecordsCount = this
                //    ._unitOfWork
                //    .EmailProviderRepository
                //    .DbSet()
                //    .Count();
                //response.Data = this
                //    ._unitOfWork
                //    .EmailProviderRepository
                //    .DbSet()
                //    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                //    .Take(pagedRequest.PageRecordsCount)
                //    .Select(b => new EmailProvider
                //    {
                //        Id = b.Id,
                //        Name = b.Name
                //    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(EmailProvider dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this.ReadEntity(dto.Id);
                if (entity == null)
                {
                    response.Message = $"Email sağlayıcı bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;

                //entity
                //    .EmailProviderConfigurations
                //    .AddRange(dto
                //        .EmailProviderConfigurations
                //        .Where(spc => spc.Id == 0)
                //        .Select(spc => new Domain.Entities.EmailProviderConfiguration
                //        {
                //            Key = spc.Key,
                //            Value = spc.Value
                //        }));

                //dto
                //    .EmailProviderConfigurations
                //    .Where(spc => spc.Id != 0)
                //    .ToList()
                //    .ForEach(spc =>
                //    {
                //        var smsProviderConfiguration = entity.EmailProviderConfigurations.First(spc2 => spc2.Id == spc.Id);
                //        smsProviderConfiguration.Key = spc.Key;
                //        smsProviderConfiguration.Value = spc.Value;
                //    });

                //var updated = this
                //    ._unitOfWork
                //    .EmailProviderRepository
                //    .Update(entity);

                //if (!updated)
                //{
                //    response.Success = false;
                //    return;
                //}

                response.Success = true;
                response.Message = "Email sağlayıcı güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.EmailProvider ReadEntity(string id)
        {
            return new Domain.Entities.EmailProvider();
            //return this
            //    ._unitOfWork
            //    .EmailProviderRepository
            //    .DbSet()
            //    .Include(sp => sp.EmailProviderConfigurations)
            //    .FirstOrDefault(sp => sp.Id == id);
        }
        #endregion
    }
}
