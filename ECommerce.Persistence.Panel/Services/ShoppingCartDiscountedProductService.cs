﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ShoppingCartDiscountedProductService : IShoppingCartDiscountedProductService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ShoppingCartDiscountedProductService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ShoppingCartDiscountedProduct> Create(ShoppingCartDiscountedProduct dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShoppingCartDiscountedProduct>>((response) =>
            {
                var entity = new Domain.Entities.ShoppingCartDiscountedProduct
                {
                    CreatedDate = DateTime.Now,
                    ApplicationId = dto.ApplicationId,
                    ProductId = dto.ProductId,
                    Active = dto.Active,
                    ShoppingCartDiscountedProductSetting = new Domain.Entities.ShoppingCartDiscountedProductSetting
                    {
                        DiscountIsRate = dto.ShoppingCartDiscountedProductSetting.DiscountIsRate,
                        Discount = dto.ShoppingCartDiscountedProductSetting.Discount
                    }
                };

                var created = this
                    ._unitOfWork
                    .ShoppingCartDiscountedProductRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Sepette indirimli ürün oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<ShoppingCartDiscountedProduct> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShoppingCartDiscountedProduct>>((response) =>
            {
                var entity = this.ReadEntity(id);
                if (entity == null)
                {
                    response.Message = $"Sepette indirimli ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new ShoppingCartDiscountedProduct
                {
                    Id = entity.Id,
                    ProductId = entity.ProductId,
                    ApplicationId = entity.ApplicationId,
                    Active = entity.Active,
                    ShoppingCartDiscountedProductSetting = new ShoppingCartDiscountedProductSetting
                    {
                        Discount = entity.ShoppingCartDiscountedProductSetting.Discount,
                        DiscountIsRate = entity.ShoppingCartDiscountedProductSetting.DiscountIsRate
                    },
                    Product = new Product
                    {

                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<ShoppingCartDiscountedProduct> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ShoppingCartDiscountedProduct>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ShoppingCartDiscountedProductRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .ShoppingCartDiscountedProductRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(x => new ShoppingCartDiscountedProduct
                    {
                        Id = x.Id,
                        ProductId = x.ProductId,
                        ApplicationId = x.ApplicationId,
                        Active = x.Active,
                        ShoppingCartDiscountedProductSetting = new ShoppingCartDiscountedProductSetting
                        {
                            Discount = x.ShoppingCartDiscountedProductSetting.Discount,
                            DiscountIsRate = x.ShoppingCartDiscountedProductSetting.DiscountIsRate
                        },
                        Product = new Product
                        {
                            Name = x.Product.ProductInformations.FirstOrDefault().ProductInformationTranslations.FirstOrDefault().Name
                        }
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(ShoppingCartDiscountedProduct dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this.ReadEntity(dto.Id);
                if (entity == null)
                {
                    response.Message = $"Sepette indirimli ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.ShoppingCartDiscountedProductSetting.Discount = dto.ShoppingCartDiscountedProductSetting.Discount;
                entity.ShoppingCartDiscountedProductSetting.DiscountIsRate = dto.ShoppingCartDiscountedProductSetting.DiscountIsRate;
                entity.Active = dto.Active;
                var updated = this
                    ._unitOfWork
                    .ShoppingCartDiscountedProductRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Sepette indirimli ürün güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(ShoppingCartDiscountedProduct dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .ShoppingCartDiscountedProductRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"x al y öde bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;
                entity.DeletedDateTime = DateTime.Now;

                var updated = this
                    ._unitOfWork.ShoppingCartDiscountedProductRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "x al y öde silindi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.ShoppingCartDiscountedProduct ReadEntity(int id)
        {
            return this
                ._unitOfWork
                .ShoppingCartDiscountedProductRepository
                .DbSet()
                .Where(x => x.Id == id)
                .Include(x => x.ShoppingCartDiscountedProductSetting)
                .Include(x => x.Product)
                .ThenInclude(x => x.ProductInformations)
                .ThenInclude(x => x.ProductInformationTranslations)
                .FirstOrDefault();
        }
        #endregion
    }
}
