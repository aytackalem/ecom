﻿using Dapper;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductCategorizationService : IProductCategorizationService
    {
        #region Fields
        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public ProductCategorizationService(ISettingService settingService)
        {
            #region Fields
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<string>> Read()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<string>>>((response) =>
            {

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    response.Data = sqlConnection.Query<string>("Select Categorization From ProductInformations Where Categorization Is Not Null Group By Categorization").ToList();
                }

                response.Success = true;

            }, (response, exception) => { });
        }
        #endregion
    }
}