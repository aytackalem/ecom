﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Security.Claims;
using System;
using System.Threading.Tasks;
using IdentityModel.Client;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Common.Helpers;

namespace ECommerce.Persistence.Panel.Services
{
    public class IdentityService : IIdentityService
    {
        #region Fields
        private readonly IConfiguration _configuration;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private Identity _identity = null;

        private const string _identityServerUrl = "https://identity.helpy.com.tr";

        private readonly ICookieHelper _cookieHelper;

        #endregion

        #region Constructors
        public IdentityService(IConfiguration configuration, IHttpContextAccessor httpContextAccessor, ICookieHelper cookieHelper)
        {
            #region Fields
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<Response> LoginAsync(string username, string password, bool keepLoggedIn)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {
                var identityServerUrl = this._configuration.GetValue<string>("IdentityServerUrl");

                HttpClient _httpClient = new();
                var disco = await _httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
                {
                    Address = identityServerUrl,
                    Policy = new DiscoveryPolicy { RequireHttps = false }
                });

                if (disco.IsError)
                {
                    throw disco.Exception;
                }

                var passwordTokenRequest = new PasswordTokenRequest
                {
                    ClientId = "PanelForUser",
                    ClientSecret = "secret",
                    UserName = username,
                    Password = password,
                    Address = disco.TokenEndpoint,

                };

                var token = await _httpClient.RequestPasswordTokenAsync(passwordTokenRequest);

                if (token.IsError)
                {
                    response.Message = "Kullanıcı adı veya şifre yanlış.";
                    return;
                }

                var userInfoRequest = new UserInfoRequest
                {
                    Token = token.AccessToken,
                    Address = disco.UserInfoEndpoint
                };

                var userInfo = await _httpClient.GetUserInfoAsync(userInfoRequest);

                if (userInfo.IsError)
                {
                    response.Message = "Bilinmeyen bir hata oluştu.";
                    return;
                }

                Identity identity = new()
                {
                    AccessToken = token.AccessToken,
                    RefreshToken = token.RefreshToken,
                    Expiry = DateTime.Now.AddSeconds(23 * 60 * 60)
                };

                this._identity = identity;

                ClaimsIdentity claimsIdentity = new(userInfo.Claims, CookieAuthenticationDefaults.AuthenticationScheme, "name", "role");
                ClaimsPrincipal claimsPrincipal = new(claimsIdentity);
                AuthenticationProperties authenticationProperties = new()
                {
                    IsPersistent = keepLoggedIn
                };
                authenticationProperties.StoreTokens(new List<AuthenticationToken>()
                {
                    new AuthenticationToken { Name = OpenIdConnectParameterNames.AccessToken, Value = token.AccessToken },
                    new AuthenticationToken { Name = OpenIdConnectParameterNames.RefreshToken, Value = token.RefreshToken },
                    new AuthenticationToken {
                        Name = OpenIdConnectParameterNames.ExpiresIn,
                        Value = DateTime.Now.AddSeconds(token.ExpiresIn).ToString("o", CultureInfo.InvariantCulture)
                    }
                });

                await _httpContextAccessor
                    .HttpContext
                    .SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, authenticationProperties);


                this._cookieHelper.Write("Token", new System.Collections.Generic.List<Application.Common.DataTransferObjects.KeyValue<string, string>>
                {
                 new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="RefreshToken",
                        Value=identity.RefreshToken.ToString()
                    },
                    new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="AccessToken",
                        Value=identity.AccessToken.ToString()
                    },
                   new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key="Expiry",
                        Value=identity.Expiry.ToString()
                    }
                }, DateTime.Now.AddYears(1));

                response.Success = true;
            });
        }

        public async Task<Response> LogoutAsync()
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {
                await this._httpContextAccessor.HttpContext.SignOutAsync();

                response.Success = true;
            });
        }

        public async Task<DataResponse<Identity>> GetIdentity()
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<Identity>>(async response =>
            {
                if (this._identity == null)
                {
                    this._identity = new Identity();
                    if (this._cookieHelper.Exist("Token"))
                    {
                        this._identity.RefreshToken = this._cookieHelper.Read("Token")[0].Value;
                        this._identity.AccessToken = this._cookieHelper.Read("Token")[1].Value;
                        this._identity.Expiry = DateTime.Parse(this._cookieHelper.Read("Token")[2].Value);
                    }
                    else
                        this._cookieHelper.Delete("LoginV4");



                    response.Success = false;
                    return;
                }

                if (this._identity.Expiry < DateTime.Now.AddSeconds(60))
                {
                    #region Refresh Token
                    using (HttpClient _httpClient = new())
                    {
                        var disco = await _httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
                        {
                            Address = _identityServerUrl,
                            Policy = new DiscoveryPolicy { RequireHttps = true }
                        });

                        if (disco.IsError)
                        {
                            response.Message = "Bilinmeyen bir hata oluştu.";
                            response.Exception = disco.Exception;
                            return;
                        }

                        var refreshTokenRequest = new RefreshTokenRequest
                        {
                            ClientId = "PanelForUser",
                            ClientSecret = "secret",
                            RefreshToken = this._identity.RefreshToken,
                            Address = disco.TokenEndpoint
                        };

                        var token = await _httpClient.RequestRefreshTokenAsync(refreshTokenRequest);

                        if (token.IsError)
                        {
                            response.Message = "Kullanıcı adı veya şifre yanlış.";
                            return;
                        }

                        var userInfoRequest = new UserInfoRequest
                        {
                            Token = token.AccessToken,
                            Address = disco.UserInfoEndpoint
                        };

                        var userInfo = await _httpClient.GetUserInfoAsync(userInfoRequest);
                        if (userInfo.IsError)
                        {
                            response.Message = "Bilinmeyen bir hata oluştu.";
                            return;
                        }

                        Identity identity = new()
                        {
                            AccessToken = token.AccessToken,
                            RefreshToken = token.RefreshToken,
                            Expiry = DateTime.Now.AddDays(token.ExpiresIn)
                        };

                        this._identity = identity;
                    }
                    #endregion
                }

                response.Data = this._identity;
                response.Success = true;

            });
        }
        #endregion
    }
}
