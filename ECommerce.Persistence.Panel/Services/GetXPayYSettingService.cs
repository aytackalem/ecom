﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class GetXPayYSettingService : IGetXPayYSettingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public GetXPayYSettingService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<GetXPayYSetting> Create(GetXPayYSetting dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<GetXPayYSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");                

                var entity = new Domain.Entities.GetXPayYSetting
                {
                    Active = dto.Active,
                    StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo),
                    EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo),
                    X = dto.X,
                    XLimit = dto.XLimit,
                    Y = dto.Y,
                    CreatedDate = DateTime.Now,
                    ApplicationId = dto.ApplicationId
                };

                var created = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Marka oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<GetXPayYSetting> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<GetXPayYSetting>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"X al y öde bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                var dto = new GetXPayYSetting
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    EndDate = entity.EndDate,
                    StartDate = entity.StartDate,
                    X = entity.X,
                    XLimit = entity.XLimit,
                    Y = entity.Y,
                    ApplicationId = entity.ApplicationId,
                    StartDateString = entity.StartDate.ToString("dd/MM/yyy", cultureinfo),
                    EndDateString = entity.EndDate.ToString("dd/MM/yyy", cultureinfo)
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<GetXPayYSetting> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<GetXPayYSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(x => new GetXPayYSetting
                    {
                        Id = x.Id,
                        Active = x.Active,
                        EndDate = x.EndDate,
                        StartDate = x.StartDate,
                        X = x.X,
                        XLimit = x.XLimit,
                        Y = x.Y,
                        ApplicationId = x.ApplicationId,
                        StartDateString = x.StartDate.ToString("dd/MM/yyy", cultureinfo),
                        EndDateString = x.EndDate.ToString("dd/MM/yyy", cultureinfo)
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(GetXPayYSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"X al y öde bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");


                entity.StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo);
                entity.EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo);
                entity.X = dto.X;
                entity.XLimit = dto.XLimit;
                entity.Y = dto.Y;
                entity.Active = dto.Active;
                entity.ApplicationId = dto.ApplicationId;

                var updated = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "X al y öde güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(GetXPayYSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"x al y öde bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;
                entity.DeletedDateTime = DateTime.Now;
                var updated = this
                    ._unitOfWork.GetXPayYSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "x al y öde silindi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
