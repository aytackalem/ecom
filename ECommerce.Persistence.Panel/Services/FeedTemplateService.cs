﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class FeedTemplateService : IFeedTemplateService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors
        public FeedTemplateService(IUnitOfWork unitOfWork, ISettingService settingService, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .FeedTemplateRepository
                    .DbSet()
                    .Select(b => new KeyValue<string, string>
                    {
                        Key = b.Id,
                        Value = b.Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}
