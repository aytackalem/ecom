﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ShipmentCompanyCompanyService : IShipmentCompanyCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ShipmentCompanyCompanyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<ShipmentCompanyCompany> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentCompanyCompany>>((response) =>
            {
                var entity = this
                            ._unitOfWork
                            .ShipmentCompanyCompanyRepository
                            .DbSet()
                            .Include(sp => sp.ShipmentCompanyConfigurations)
                            .Include(sp => sp.ShipmentCompany)
                            .FirstOrDefault(sp => sp.Id == id);

                if (entity == null)
                {
                    response.Message = $"Kargo sağlayıcı bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new ShipmentCompanyCompany
                {
                    Id = entity.Id,
                    Name = entity.ShipmentCompany.Name,
                    ShipmentCompanyConfigurations = entity
                        .ShipmentCompanyConfigurations
                        .Select(spc => new ECommerce.Application.Panel.DataTransferObjects.ShipmentCompanyConfiguration
                        {
                            Id = spc.Id,
                            Key = spc.Key,
                            Value = spc.Value
                        })
                        .ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<ShipmentCompanyCompany> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ShipmentCompanyCompany>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ShipmentCompanyCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .ShipmentCompanyCompanyRepository
                    .DbSet()
                    .Include(x => x.ShipmentCompany)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new ShipmentCompanyCompany
                    {
                        Id = b.Id,
                        Name = b.ShipmentCompany.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(ShipmentCompanyCompany dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                  ._unitOfWork
                  .ShipmentCompanyCompanyRepository
                  .DbSet()
                  .Include(x => x.ShipmentCompanyConfigurations)
                  .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Kargo firması bulunamadı.";
                    response.Success = false;
                    return;
                }

                dto.ShipmentCompanyConfigurations.ForEach(mc =>
                {
                    var shipmentCompanyConfigurations = entity.ShipmentCompanyConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                    if (shipmentCompanyConfigurations != null)
                    {
                        shipmentCompanyConfigurations.Value = mc.Value;
                    }
                });

                var updated = this
                    ._unitOfWork.ShipmentCompanyCompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Kargo firması güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods

        #endregion
    }
}
