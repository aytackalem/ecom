﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class VariantService : IVariantService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public VariantService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Variant> Create(Variant dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Variant>>((response) =>
            {
                dto.VariantValues?.RemoveAll(vv => vv == null);

                var hasAnySameName = this
                     ._unitOfWork
                     .VariantRepository
                     .DbSet()
                     .Any(b => b.VariantTranslations.Any(vt => dto.VariantTranslations.Select(vt2 => vt2.Name).Contains(vt.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir varyasyon oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Variant
                {
                    Photoable = dto.Photoable,
                    Slicer = dto.Slicer,
                    Varianter = dto.Varianter,
                    ShowVariant = dto.ShowVariant,
                    CreatedDate = DateTime.Now,
                    VariantTranslations = dto
                        .VariantTranslations
                        .Select(vt => new Domain.Entities.VariantTranslation
                        {
                            LanguageId = vt.LanguageId,
                            CreatedDate = DateTime.Now,
                            Name = vt.Name
                        })
                        .ToList(),
                    VariantValues = dto
                        .VariantValues
                        .Select(vv => new Domain.Entities.VariantValue
                        {
                            Id = vv.Id,
                            VariantValueTranslations = vv
                                .VariantValueTranslations
                                .Select(vvt => new Domain.Entities.VariantValueTranslation
                                {
                                    LanguageId = vvt.LanguageId,
                                    Value = vvt.Value
                                })
                                .ToList()
                        })
                        .ToList()
                };

                var created = this
                    ._unitOfWork
                    .VariantRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Varyasyon oluşturuldu.";
            }, (response, exception) => { });
        }

        public PagedResponse<Variant> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Variant>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .VariantRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .VariantRepository
                    .DbSet()
                    .Include(v => v.VariantTranslations)
                    .Where(c => string.IsNullOrEmpty(pagedRequest.Search) || c.VariantTranslations.Any(ct => ct.Name.Contains(pagedRequest.Search)))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(v => new Variant
                    {
                        Id = v.Id,
                        VariantTranslations = v.VariantTranslations.Select(vt => new VariantTranslation
                        {
                            LanguageId = vt.LanguageId,
                            Name = vt.Name
                        }).ToList()
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<Variant>> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Variant>>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .VariantRepository
                    .DbSet()
                    .Include(v => v.VariantTranslations)
                    .Include(v => v.VariantValues)
                    .ThenInclude(vv => vv.VariantValueTranslations);

                response.Data = entities
                    .Select(v => new Variant
                    {
                        Id = v.Id,
                        Photoable = v.Photoable,
                        Slicer = v.Slicer,
                        Varianter = v.Varianter,
                        ShowVariant = v.ShowVariant,
                        VariantTranslations = v
                            .VariantTranslations
                            .Select(vt => new VariantTranslation
                            {
                                Id = vt.Id,
                                LanguageId = vt.LanguageId,
                                Name = vt.Name
                            })
                            .ToList(),
                        VariantValues = v
                            .VariantValues
                            .Select(vv => new VariantValue
                            {
                                Id = vv.Id,
                                VariantId = vv.VariantId,
                                VariantValueTranslations = vv
                                    .VariantValueTranslations
                                    .Select(vvt => new VariantValueTranslation
                                    {
                                        Id = vvt.Id,
                                        LanguageId = vvt.LanguageId,
                                        Value = vvt.Value,
                                        VariantValueId = vvt.VariantValueId
                                    })
                                    .ToList()
                            })
                            .ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<Variant> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Variant>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .VariantRepository
                    .DbSet()
                    .Include(x => x.VariantTranslations)
                    .Include(x => x.VariantValues)
                    .ThenInclude(x => x.VariantValueTranslations)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Varyasyon bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Variant
                {
                    Id = entity.Id,
                    Photoable = entity.Photoable,
                    Slicer = entity.Slicer,
                    Varianter = entity.Varianter,
                    ShowVariant = entity.ShowVariant,
                    VariantTranslations = entity.VariantTranslations.Select(ct => new VariantTranslation
                    {
                        Id = ct.Id,
                        LanguageId = ct.LanguageId,
                        Name = ct.Name
                    }).ToList(),
                    VariantValues = entity
                            .VariantValues
                            .Select(vv => new VariantValue
                            {
                                Id = vv.Id,
                                VariantId = vv.VariantId,
                                VariantValueTranslations = vv
                                    .VariantValueTranslations
                                    .Select(vvt => new VariantValueTranslation
                                    {
                                        Id = vvt.Id,
                                        LanguageId = vvt.LanguageId,
                                        Value = vvt.Value,
                                        VariantValueId = vvt.VariantValueId
                                    })
                                    .ToList()
                            })
                            .ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public async Task<DataResponse<Variant>> ReadAsync(int id)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<Variant>>(async response =>
            {
                var entity = await this
                    ._unitOfWork
                    .VariantRepository
                    .DbSet()
                    .Include(v => v.VariantTranslations)
                    //.Include(v => v.MarketplaceVariantMappings)
                    .Include(v => v.VariantValues)
                        .ThenInclude(vv => vv.VariantValueTranslations)
                    .Include(v => v.VariantValues)
                        .ThenInclude(vv => vv.MarketplaceVariantValueMappings)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Varyasyon bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Variant
                {
                    Id = entity.Id,
                    Photoable = entity.Photoable,
                    Slicer = entity.Slicer,
                    Varianter = entity.Varianter,
                    VariantTranslations = entity
                        .VariantTranslations
                        .Select(ct => new VariantTranslation
                        {
                            Id = ct.Id,
                            LanguageId = ct.LanguageId,
                            Name = ct.Name
                        })
                        .ToList(),
                    //MarketplaceVariantMappings = entity
                    //    .MarketplaceVariantMappings
                    //    .Select(mvm => new MarketplaceVariantMapping
                    //    {
                    //        Id = mvm.Id,
                    //        MarketplaceId = mvm.MarketplaceId,
                    //        MarketplaceVariantCode = mvm.MarketplaceVariantCode,
                    //        VariantId = mvm.VariantId
                    //    })
                    //    .ToList(),
                    VariantValues = entity
                            .VariantValues
                            .Select(vv => new VariantValue
                            {
                                Id = vv.Id,
                                VariantId = vv.VariantId,
                                VariantValueTranslations = vv
                                    .VariantValueTranslations
                                    .Select(vvt => new VariantValueTranslation
                                    {
                                        Id = vvt.Id,
                                        LanguageId = vvt.LanguageId,
                                        Value = vvt.Value,
                                        VariantValueId = vvt.VariantValueId
                                    })
                                    .ToList(),
                                MarketplaceVariantValueMappings = vv.MarketplaceVariantValueMappings.Select(x => new MarketplaceVariantValueMapping
                                {
                                    Id = x.Id,
                                    VariantValueId = x.VariantValueId,
                                    MarketplaceId = x.MarketplaceId,
                                    AllowCustom = x.AllowCustom,
                                    CompanyId = x.CompanyId,
                                    Mandatory = x.Mandatory,
                                    MarketplaceVariantCode = x.MarketplaceVariantCode,
                                    MarketplaceVariantName = x.MarketplaceVariantName,
                                    MarketplaceVariantValueCode = x.MarketplaceVariantValueCode,
                                    MarketplaceVariantValueName = x.MarketplaceVariantValueName
                                }).ToList()
                            })
                            .ToList()
                };

                response.Data = dto;
                response.Success = true;
            });
        }

        public Response Update(Variant dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                #region Check Same Name
                var hasAnySameName = this
                             ._unitOfWork
                             .VariantRepository
                             .DbSet()
                             .Any(c => c.VariantTranslations.Any(vt => vt.VariantId != dto.Id && dto.VariantTranslations.Select(vt2 => vt2.Name).Contains(vt.Name)));

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir varyasyon oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }
                #endregion

                #region Null Check
                var entity = this
                            ._unitOfWork
                            .VariantRepository
                            .DbSet()
                    .Include(x => x.VariantTranslations)
                    .Include(x => x.VariantValues)
                    .ThenInclude(x => x.VariantValueTranslations)
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Varyasyon bulunamadı.";
                    response.Success = false;
                    return;
                }
                #endregion



                entity.Photoable = dto.Photoable;
                entity.Slicer = dto.Slicer;
                entity.Varianter = dto.Varianter;
                entity.ShowVariant = dto.ShowVariant;



                dto.VariantTranslations.ForEach(vt =>
                {
                    var variantTranslation = entity.VariantTranslations.FirstOrDefault(x => x.Id == vt.Id);
                    if (variantTranslation == null)
                        entity.VariantTranslations.Add(new Domain.Entities.VariantTranslation
                        {
                            LanguageId = variantTranslation.LanguageId,
                            Name = variantTranslation.Name
                        });
                    else
                        variantTranslation.Name = vt.Name;
                });

                dto.VariantValues.ForEach(vv =>
                {
                    var variantValue = entity.VariantValues.FirstOrDefault(x => x.Id == vv.Id);
                    if (variantValue == null)
                    {
                        entity.VariantValues.Add(new Domain.Entities.VariantValue
                        {
                            Id = vv.Id,
                            VariantValueTranslations = vv
                                .VariantValueTranslations
                                .Select(vvt => new Domain.Entities.VariantValueTranslation
                                {
                                    LanguageId = vvt.LanguageId,
                                    Value = vvt.Value
                                })
                                .ToList()
                        });
                    }
                    else
                    {
                        vv.VariantValueTranslations.ForEach(vvt =>
                        {
                            var variantValueTranslation = variantValue.VariantValueTranslations.FirstOrDefault(x => x.Id == vvt.Id);
                            if (variantValueTranslation == null)
                                variantValue.VariantValueTranslations.Add(new Domain.Entities.VariantValueTranslation
                                {
                                    LanguageId = vvt.LanguageId,
                                    Value = vvt.Value
                                });
                            else
                                variantValueTranslation.Value = vvt.Value;
                        });
                    }
                });

                var updated = this
                    ._unitOfWork
                    .VariantRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }


                var productInformationUpdate = ProductInformationVariantUpdate(dto.Id);
                if (!productInformationUpdate.Success)
                {
                    response.Message = "Ürün varyasyon gösterme işlemi güncellenemedi.";
                    response.Success = false;
                    return;
                }


                response.Success = true;
                response.Message = "Varyasyon güncellendi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Helper
        public Response ProductInformationVariantUpdate(int variantId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var productInformations = _unitOfWork.ProductInformationRepository.DbSet()
                .Include(x => x.ProductInformationTranslations)
                .Include(x => x.ProductInformationVariants)
                .ThenInclude(x => x.VariantValue.VariantValueTranslations)
                .Include(x => x.ProductInformationVariants)
                .ThenInclude(x => x.VariantValue.Variant)
                .ToList();

                foreach (var pi in productInformations)
                {
                    var productInformationVariantValues = pi.ProductInformationVariants.Where(x => x.VariantValue.Variant.ShowVariant).Select(x => x.VariantValue.VariantValueTranslations.FirstOrDefault().Value).ToList();
                    var variantValueNames = string.Join(", ", productInformationVariantValues);
                    pi.ProductInformationTranslations[0].VariantValuesDescription = !String.IsNullOrEmpty(variantValueNames) ? $"({variantValueNames})" : "";
                }
                _unitOfWork.ProductInformationTranslationRepository.Update(productInformations.SelectMany(x => x.ProductInformationTranslations).ToList());

                response.Success = true;
                response.Message = "Varyasyon güncellendi.";
            }, (response, exception) => { });
        }

        #endregion
    }
}
