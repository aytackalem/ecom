﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class LanguageService : ILanguageService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public LanguageService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .LanguageRepository
                    .DbSet()
                    .Where(l => l.Active)
                    .Select(l => new KeyValue<string, string>
                    {
                        Key = l.Id,
                        Value = l.Name
                    })
                    .ToList();

                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}
