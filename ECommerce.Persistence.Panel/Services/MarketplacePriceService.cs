﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using ECommerce.Application.Panel.Parameters.ProductPrices.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Syncfusion.Licensing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplacePriceService : IMarketplacePriceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;

        private readonly IExcelHelper _excelHelper;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplacePriceService(IUnitOfWork unitOfWork, IHttpHelper httpHelper, ISettingService settingService, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IConfiguration configuration, IExcelHelper excelHelper, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            this._excelHelper = excelHelper;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public PagedResponse<ProductInformation> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {

                var iQueryable = GetProductInformations(pagedRequest.Search);
                if (!iQueryable.Success)
                {
                    response.Message = iQueryable.Message;
                    return;
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable.Data.Count();
                response.Data = iQueryable.Data
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(pi => new ProductInformation
                    {
                        Product = new Product
                        {
                            CreatedDate = pi.Product.CreatedDate,
                            SellerCode = pi.Product.SellerCode,
                            ProductSourceDomainName = pi.Product.ProductSourceDomainId.HasValue ? pi.Product.ProductSourceDomain.Name : "Helpy",

                        },
                        Id = pi.Id,
                        MarketplaceBulkPriceOpen = pi
                                    .ProductInformationMarketplaces.Any(y => y.ProductInformationMarketplaceBulkPrice != null),
                        IncludeAccounting = _settingService.IncludeAccounting,
                        Active = pi.Active,
                        Stock = pi.Stock,
                        StockCode = pi.StockCode,
                        Barcode = pi.Barcode,
                        ProductInformationPriceses = pi.ProductInformationPriceses.Where(x => x.CurrencyId == "TL").Select(pip => new ProductInformationPrice
                        {
                            ListUnitPrice = pip.ListUnitPrice,
                            UnitPrice = pip.UnitPrice
                        })
                                .ToList(),
                        ProductInformationTranslations = pi
                                    .ProductInformationTranslations
                                    .Where(pit => pit.LanguageId == "TR")
                                    .Select(pit => new ProductInformationTranslation
                                    {
                                        Name = pit.Name,
                                        Url = this._settingService.IncludeECommerce ? $"{this._settingService.WebUrl}/{pit.Url}" : null
                                    })
                                    .ToList(),
                        ProductInformationPhotos = new()
                                {
                                    new()
                                    {
                                        FileName =$"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault(pip=>!pip.Deleted).FileName}"
                                    }
                                },
                        ProductInformationMarketplaces = pi
                                    .ProductInformationMarketplaces
                                    .Where(pim => pim.CompanyId == this._companyFinder.FindId())
                                    .Select(pim => new ProductInformationMarketplace
                                    {
                                        MarketplaceId = pim.MarketplaceId,
                                        Url = pim.Url,
                                        Active = pim.Active,
                                        Opened = pim.Opened,
                                        ListUnitPrice = pim.ListUnitPrice,
                                        UnitPrice = pim.UnitPrice,
                                        DisabledUpdateProduct = pim.DisabledUpdateProduct,
                                        AccountingPrice = pim.AccountingPrice,
                                        ProductInformationMarketplaceBulkPrice = pim.ProductInformationMarketplaceBulkPrice != null ? new ProductInformationMarketplaceBulkPrice
                                        {
                                            ListUnitPrice = pim.ProductInformationMarketplaceBulkPrice.ListUnitPrice,
                                            UnitPrice = pim.ProductInformationMarketplaceBulkPrice.UnitPrice,
                                            Active = pim.ProductInformationMarketplaceBulkPrice.Active,
                                            AccountingPrice = pim.ProductInformationMarketplaceBulkPrice.AccountingPrice,
                                        }
                                        : null
                                    })
                                    .ToList()
                    })
                            .ToList();
                response.Success = true;


            });
        }

        public DataResponse<List<KeyValue<string, string>>> MarketplaceCompanyPrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                var data = new List<KeyValue<string, string>>();

                var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet().Include(x => x.Marketplace).Where(x => x.Active).ToList();

                foreach (var mc in marketplaceCompanies)
                {
                    data.Add(new KeyValue<string, string>
                    {
                        Key = $"{mc.Marketplace.Id}_LP",
                        Value = $"{mc.Marketplace.Name} Liste Fiyatı"
                    });

                    data.Add(new KeyValue<string, string>
                    {
                        Key = $"{mc.Marketplace.Id}_UP",
                        Value = $"{mc.Marketplace.Name} Satış Fiyatı"
                    });
                }



                response.Data = data;
                response.Success = true;
            });
        }

        public DataResponse<List<KeyValue<int, string>>> ActionPrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {

                var data = Enum.GetValues(typeof(ProductPrice))
                   .Cast<ProductPrice>()
                   .Select(x => new KeyValue<int, string>
                   {
                       Key = (int)x,
                       Value = x.ToDescriptionString<ProductPrice>()
                   })
                   .ToList();

                response.Data = data;
                response.Success = true;
            });
        }

        public DataResponse<List<KeyValue<int, string>>> FractionPrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {

                var data = Enum.GetValues(typeof(ProductPriceFraction))
                   .Cast<ProductPriceFraction>()
                   .Select(x => new KeyValue<int, string>
                   {
                       Key = (int)x,
                       Value = x.ToDescriptionString<ProductPriceFraction>()
                   })
                   .ToList();

                response.Data = data;
                response.Success = true;
            });
        }

        public DataResponse<List<KeyValue<int, string>>> AccountingPrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {

                var data = Enum.GetValues(typeof(Application.Panel.Parameters.ProductPrices.Enums.AccountingPrice))
                        .Cast<Application.Panel.Parameters.ProductPrices.Enums.AccountingPrice>()
                        .Select(x => new KeyValue<int, string>
                        {
                            Key = (int)x,
                            Value = x.ToDescriptionString<Application.Panel.Parameters.ProductPrices.Enums.AccountingPrice>()
                        })
                        .ToList();

                response.Data = data;
                response.Success = true;
            });
        }

        public DataResponse<List<KeyValue<int, string>>> ProductUpdatePrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {

                var data = Enum.GetValues(typeof(Application.Panel.Parameters.ProductPrices.Enums.ProductUpdatePrice))
                        .Cast<Application.Panel.Parameters.ProductPrices.Enums.ProductUpdatePrice>()
                        .Select(x => new KeyValue<int, string>
                        {
                            Key = (int)x,
                            Value = x.ToDescriptionString<Application.Panel.Parameters.ProductPrices.Enums.ProductUpdatePrice>()
                        })
                        .ToList();

                response.Data = data;
                response.Success = true;
            });
        }

        public DataResponse<List<KeyValue<string, int>>> MarketplaceUpdatePrices()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, int>>>>((response) =>
            {

                var marketplacePrices = _unitOfWork.ProductInformationMarketplaceBulkPriceRepository
                     .DbSet()
                     .Include(x => x.ProductInformationMarketplace)
                     .ToList();

                response.Data = marketplacePrices
                .GroupBy(x => x.ProductInformationMarketplace.MarketplaceId)
                .Select(x => new KeyValue<string, int>
                {
                    Key = x.Key,
                    Value = x.Count()
                })
                .ToList();
                response.Success = true;
            });
        }

        public Response Delete(int id)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var deleted = DeleteBulkPrices(id);

                if (!deleted.Success)
                {
                    response.Message = "Ürün fiyat işlemleri silinirken hata ile karşılaşıldı lütfen daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }


                response.Message = "Ürün fiyat işlemi silinmiştir.";
                response.Success = true;
            });
        }

        public Response BulkDelete()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var deleted = DeleteBulkPrices();

                if (!deleted.Success)
                {
                    response.Message = "Ürün fiyat işlemleri silinirken hata ile karşılaşıldı lütfen daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }


                response.Message = "Ürün fiyat işlemleri silinmiştir.";
                response.Success = true;
            });
        }

        public Response BulkUpdate()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var count = _unitOfWork.ProductInformationMarketplaceBulkPriceRepository.DbSet().Count();

                if (count == 0)
                {
                    response.Message = "Onaylanacak ürün datası bulunamadı. Lütfen tekrar toplu fiyat işlemi yapınız.";
                    response.Success = false;
                    return;
                }


                var update = BulkMappingUpdate();

                if (!update.Success)
                {
                    response.Message = "Pazaryerine fiyat güncellemesi yapılırken bir hata ile karşılaşıldı. Lütfen tekrar deneyin";
                    response.Success = false;
                    return;
                }


                response.Message = "Pazaryerine fiyat güncellemesi yapılmıştır. Bir kaç dakika içinde pazaryerinde güncellenecektir.";
                response.Success = true;
            });
        }

        public Response BulkAccountingUpdate(ProductMarketplaceBulkAccountingPrice dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var iQueryable = GetProductInformations(dto.Search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }

                var productInformationMarketplaces = iQueryable.Data
                    .Include(x => x.ProductInformationMarketplaces)
                    .SelectMany(x => x.ProductInformationMarketplaces)
                    .Where(x => x.CompanyId == _companyFinder.FindId() && dto.MarketplaceIds.Contains(x.MarketplaceId)).ToList();

                var accountingPrice = true;
                switch (dto.AccountingPriceId)
                {
                    case AccountingPrice.UpdateNotPrice:
                        accountingPrice = false;
                        break;
                }

                productInformationMarketplaces.ForEach(x =>
                {
                    x.AccountingPrice = accountingPrice;
                });


                _unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarketplaces);

                response.Message = "Pazaryerine fiyat güncellemesi yapılmıştır.";
                response.Success = true;
            });
        }

        public Response BulkProductMarketplaceUpdatePrice(ProductMarketplaceBulkProductPrice dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var iQueryable = GetProductInformations(dto.Search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }

                var productInformationMarketplaces = iQueryable.Data
                    .Include(x => x.ProductInformationMarketplaces)
                    .SelectMany(x => x.ProductInformationMarketplaces)
                    .Where(x => x.CompanyId == _companyFinder.FindId() && dto.MarketplaceIds.Contains(x.MarketplaceId)).ToList();

                var disabledUpdateProduct = false;
                switch (dto.ProductUpdatePriceId)
                {
                    case ProductUpdatePrice.UpdateNotProductPrice:
                        disabledUpdateProduct = true;
                        break;
                }

                productInformationMarketplaces.ForEach(x =>
                {
                    x.DisabledUpdateProduct = disabledUpdateProduct;
                });


                _unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarketplaces);

                response.Message = "Pazaryerlerine ürün ve fiyat güncellemesi yapılmıştır.";
                response.Success = true;
            });
        }

        public Response Create(ProductMarketplaceBulkPrice dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var iQueryable = GetProductInformations(dto.Search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }

                var productInformations = iQueryable.Data.Include(x => x.ProductInformationMarketplaces.Where(x => x.CompanyId == _companyFinder.FindId())).ThenInclude(x => x.ProductInformationMarketplaceBulkPrice).ToList();

                if (productInformations.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "Güncelleme yapılacak bir ürün bulunamadı";
                    return;
                }

                var sourcePrice = dto.SourcePriceId.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                var updatePrice = dto.UpdatePriceId.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);


                var validSourcePrice = sourcePrice.Length > 1 && (sourcePrice[1] == "LP" || sourcePrice[1] == "UP");
                var validUpdatePrice = updatePrice.Length > 1 && (updatePrice[1] == "LP" || updatePrice[1] == "UP");

                if (!validSourcePrice || !validUpdatePrice)
                {
                    response.Success = false;
                    response.Message = "Gönderilen fiyat seçiminde bir hata ile karşılaşıldı lütfen teknik desteği arayınız.";
                    return;
                }

                var accountingPrice = true;
                switch (dto.AccountingPriceId)
                {
                    case AccountingPrice.UpdateNotPrice:
                        accountingPrice = false;
                        break;
                }


                string sourcePriceMarketplaceId = sourcePrice[0];

                string updatePriceMarketplaceId = updatePrice[0];

                var productInformationMarketplaceBulkPrices = new List<Domain.Entities.ProductInformationMarketplaceBulkPrice>();

                var dateTimeNow = DateTime.Now;

                foreach (var pinLoop in productInformations)
                {
                    var sourceProductInformationMarketplace = pinLoop.ProductInformationMarketplaces.FirstOrDefault(x => x.MarketplaceId == sourcePriceMarketplaceId);

                    var updateProductInformationMarketplace = pinLoop.ProductInformationMarketplaces.FirstOrDefault(x => x.MarketplaceId == updatePriceMarketplaceId);

                    if (sourceProductInformationMarketplace != null && updateProductInformationMarketplace != null)
                    {
                        decimal sourceProductInformationPrice = sourceProductInformationMarketplace.UnitPrice;
                        if (sourcePrice[1] == "LP")
                            sourceProductInformationPrice = sourceProductInformationMarketplace.ListUnitPrice;

                        switch (dto.ProductPriceId)
                        {
                            case ProductPrice.AddPercentagePrice:
                                sourceProductInformationPrice = sourceProductInformationPrice * (1 + (dto.PriceCalculated / 100m));
                                break;
                            case ProductPrice.LowerPercentagePrice:
                                sourceProductInformationPrice = sourceProductInformationPrice / (1 + (dto.PriceCalculated / 100m));
                                break;
                            case ProductPrice.AddFixedPrice:
                                sourceProductInformationPrice += dto.PriceCalculated;
                                break;
                            case ProductPrice.LowerFixedPrice:
                                sourceProductInformationPrice -= dto.PriceCalculated;
                                break;
                            case ProductPrice.MultiplyFixedPrice:
                                sourceProductInformationPrice = sourceProductInformationPrice * dto.PriceCalculated;
                                break;
                            case ProductPrice.SplitFixedPrice:
                                sourceProductInformationPrice = sourceProductInformationPrice / dto.PriceCalculated;
                                break;
                        }


                        switch (dto.ProductPriceFractionId)
                        {
                            case ProductPriceFraction.CeilingPrice:
                                sourceProductInformationPrice = Math.Ceiling(sourceProductInformationPrice);
                                break;
                            case ProductPriceFraction.FloorPrice:
                                sourceProductInformationPrice = Math.Floor(sourceProductInformationPrice);
                                break;
                            case ProductPriceFraction.CeilingOneSendPrice:
                                sourceProductInformationPrice = Math.Ceiling(sourceProductInformationPrice) - 0.01m;
                                break;
                            case ProductPriceFraction.FloorOneSendPrice:
                                sourceProductInformationPrice = Math.Floor(sourceProductInformationPrice) - 0.01m;
                                break;
                        }

                        sourceProductInformationPrice = sourceProductInformationPrice < 0 ? 0 : sourceProductInformationPrice;


                        var updateListUnitPrice = updateProductInformationMarketplace.ListUnitPrice;
                        var updateUnitPrice = updateProductInformationMarketplace.UnitPrice;

                        if (updateProductInformationMarketplace.ProductInformationMarketplaceBulkPrice != null)
                        {
                            updateListUnitPrice = updateProductInformationMarketplace.ProductInformationMarketplaceBulkPrice.ListUnitPrice;
                            updateUnitPrice = updateProductInformationMarketplace.ProductInformationMarketplaceBulkPrice.UnitPrice;
                        }

                        productInformationMarketplaceBulkPrices.Add(new Domain.Entities.ProductInformationMarketplaceBulkPrice
                        {
                            Id = updateProductInformationMarketplace.Id,
                            ListUnitPrice = updatePrice[1] == "LP" ? sourceProductInformationPrice : updateListUnitPrice,
                            UnitPrice = updatePrice[1] == "UP" ? sourceProductInformationPrice : updateUnitPrice,
                            AccountingPrice = accountingPrice,
                            Active = updateProductInformationMarketplace.Active,
                            TenantId = updateProductInformationMarketplace.TenantId,
                            DomainId = updateProductInformationMarketplace.DomainId,
                            CompanyId = updateProductInformationMarketplace.CompanyId
                        });

                    }

                }

                BulkMappingInsert(productInformationMarketplaceBulkPrices, updatePriceMarketplaceId);

                response.Message = "Fiyatlarınız başarılı bir şekilde eklenmiştir.";
                response.Success = true;
            });
        }

        public DataResponse<List<MarketplaceAccountingPrice>> MarketplaceAccountingPrices(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceAccountingPrice>>>((response) =>
            {

                var iQueryable = GetProductInformations(search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }

                var productInformationMarketplaces = iQueryable.Data
                .Include(x => x.ProductInformationMarketplaces)
                .SelectMany(x => x.ProductInformationMarketplaces)
                .Where(x => x.CompanyId == _companyFinder.FindId());



                response.Data = productInformationMarketplaces
                .GroupBy(x => new { x.MarketplaceId })
                .Select(x => new MarketplaceAccountingPrice
                {
                    MarketplaceId = x.Key.MarketplaceId,
                    AccountingPriceTotalCount = x.Count(),
                    OpenAccountingPriceCount = x.Count(x => x.AccountingPrice),
                    ClosedAccountingPriceCount = x.Count(x => !x.AccountingPrice),
                })
                .ToList();
                response.Success = true;
            });
        }


        public DataResponse<List<MarketplaceUpdateProductPrice>> MarketplaceUpdateProductPrices(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceUpdateProductPrice>>>((response) =>
            {

                var iQueryable = GetProductInformations(search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }

                var productInformationMarketplaces = iQueryable.Data
                .Include(x => x.ProductInformationMarketplaces)
                .SelectMany(x => x.ProductInformationMarketplaces)
                .Where(x => x.CompanyId == _companyFinder.FindId());



                response.Data = productInformationMarketplaces
                .GroupBy(x => new { x.MarketplaceId })
                .Select(x => new MarketplaceUpdateProductPrice
                {
                    MarketplaceId = x.Key.MarketplaceId,
                    UpdateProductPriceTotalCount = x.Count(),
                    OpenUpdateProductPriceCount = x.Count(x => !x.DisabledUpdateProduct),
                    ClosedUpdateProductPriceCount = x.Count(x => x.DisabledUpdateProduct),
                })
                .ToList();

                response.Success = true;
            });
        }

        public DataResponse<MemoryStream> DownloadMarketplace(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                var marketplaces = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Select(mc => new Marketplace
                    {
                        Id = mc.MarketplaceId,
                        Name = mc.Marketplace.Name
                    })
                    .ToList();



                var iQueryable = GetProductInformations(search);
                if (!iQueryable.Success)
                {
                    response.Success = false;
                    response.Message = iQueryable.Message;
                    return;
                }


                var bulkMarketplaces = iQueryable.Data
                    .Select(p => new BulkMarketplace
                    {
                        ProductInformationId = p.Id,
                        ProductInformationName = $"{p.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Name} {p.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").VariantValuesDescription}",
                        SellerCode = p.Product.SellerCode,
                        SkuCode = p.SkuCode,
                        StockCode = p.StockCode,
                        Barcode = p.Barcode,
                        Stock = p.Stock,
                        ProductInformationMarketplaces = p.ProductInformationMarketplaces.Select(pim => new ProductInformationMarketplace
                        {
                            //MarketplaceCompanyId = pim.MarketplaceCompanyId,
                            MarketplaceId = pim.MarketplaceId,
                            UnitPrice = pim.UnitPrice,
                            ListUnitPrice = pim.ListUnitPrice,
                            AccountingPrice = pim.AccountingPrice,
                            Active = pim.Active,
                        })
                        .ToList()
                    })
                    .ToList();

                Excel excel = new()
                {
                    Header = new() { Cells = new() }
                };

                var columns = new List<string> { "Id", "Ürün Adı", "Model Kodu", "Sku", "Stok Kodu", "Barcode", "Stock" };

                foreach (var theMarketplace in marketplaces)
                {
                    columns.AddRange(new[] { $"{theMarketplace.Name}_Bağlantısı" });
                    if (_settingService.IncludeAccounting)
                    {
                        columns.AddRange(new[] { $"{theMarketplace.Name}_Xml_Erp_Fiyat_Güncellensin" });

                    }
                    columns.AddRange(new[] { $"{theMarketplace.Name}_Liste_Fiyatı" });
                    columns.AddRange(new[] { $"{theMarketplace.Name}_Satış_Fiyatı" });
                }





                foreach (var cLoop in columns)
                {
                    excel.Header.Cells.Add(new() { CellType = CellType.String, Value = cLoop });
                }


                excel.Rows = new();

                foreach (var theBulkMarketplace in bulkMarketplaces)
                {
                    var cells = new List<Cell>
                    {
                        new Cell { CellType = CellType.Number, Value = theBulkMarketplace.ProductInformationId },
                        new Cell { CellType = CellType.String, Value = string.IsNullOrEmpty(theBulkMarketplace.ProductInformationName) ? "Yok" : theBulkMarketplace.ProductInformationName    },
                        new Cell { CellType = CellType.String, Value = string.IsNullOrEmpty(theBulkMarketplace.SellerCode) ? "Yok" : theBulkMarketplace.SellerCode  },
                        new Cell { CellType = CellType.String, Value = string.IsNullOrEmpty(theBulkMarketplace.SkuCode) ? "Yok" : theBulkMarketplace.SkuCode },
                        new Cell { CellType = CellType.String, Value = string.IsNullOrEmpty(theBulkMarketplace.StockCode) ? "Yok" : theBulkMarketplace.StockCode  },
                        new Cell { CellType = CellType.String, Value = string.IsNullOrEmpty(theBulkMarketplace.Barcode) ? "Yok" : theBulkMarketplace.Barcode  },
                        new Cell { CellType = CellType.Number, Value =  theBulkMarketplace.Stock  }
                    };

                    foreach (var theMarketplace in marketplaces)
                    {
                        var productInformationMarketplace = theBulkMarketplace.ProductInformationMarketplaces.FirstOrDefault(x => x.MarketplaceId == theMarketplace.Id);

                        var listUnitprice = 0m;
                        var unitprice = 0m;
                        string value = "Yok";
                        string accountingPriceValue = "Yok";

                        if (productInformationMarketplace != null)
                        {
                            listUnitprice = productInformationMarketplace.ListUnitPrice;
                            unitprice = productInformationMarketplace.UnitPrice;
                            value = productInformationMarketplace.Active ? "Evet" : "Hayır";
                            accountingPriceValue = productInformationMarketplace.AccountingPrice ? "Evet" : "Hayır";
                        }

                        cells.AddRange(new[] {
                            new Cell {
                                CellType = CellType.String,
                                Value = value
                            }
                        });

                        if (_settingService.IncludeAccounting)
                        {
                            cells.AddRange(new[] {
                                new Cell {
                                    CellType = CellType.String,
                                    Value = accountingPriceValue
                                }
                            });
                        }
                        cells.AddRange(new[] {
                            new Cell {
                                CellType = CellType.String,
                                Value = listUnitprice
                            }
                        });
                        cells.AddRange(new[] {
                            new Cell {
                                CellType = CellType.String,
                                Value = unitprice
                            }
                        });
                    }

                    excel.Rows.Add(new() { Cells = cells });
                }

                var memoryStream = this._excelHelper.Write(excel);
                memoryStream.Position = 0;

                response.Data = memoryStream;
            });
        }

        public Response UpdateBulkMarketplace(IFormFile formFile)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var excel = this._excelHelper.Read(formFile.OpenReadStream());

                var marketplaces = this
                      ._unitOfWork
                      .MarketplaceCompanyRepository
                      .DbSet()
                      .Select(mc => new Marketplace
                      {
                          Id = mc.MarketplaceId,
                          Name = mc.Marketplace.Name,
                          MarketplaceCompanyId = mc.Id
                      })
                      .ToList();

                var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository
                .DbSet()
                .Where(x => !x.ProductInformation.Product.IsOnlyHidden)
                .Select(pim => new ProductInformationMarketplace
                {
                    Id = pim.Id,
                    ProductInformationId = pim.ProductInformationId,
                    //MarketplaceCompanyId = pim.MarketplaceCompanyId,
                    MarketplaceId = pim.MarketplaceId,
                    UnitPrice = pim.UnitPrice,
                    ListUnitPrice = pim.ListUnitPrice,
                    Active = pim.Active,
                    AccountingPrice = pim.AccountingPrice,
                }).ToList();



                var productInformationMarketplaceBulkPrices = new List<Domain.Entities.ProductInformationMarketplaceBulkPrice>();
                var cells = excel.Header.Cells.Select(x => x.Value).ToList();
                var productInformationIndex = cells.IndexOf("Id");

                foreach (var row in excel.Rows)
                {
                    if (row.Cells.Count != excel.Header.Cells.Count) continue;

                    foreach (var theMarketplace in marketplaces)
                    {
                        var productInformationId = Convert.ToInt32(row.Cells[productInformationIndex].Value);
                        var productInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.ProductInformationId == productInformationId && x.MarketplaceId == theMarketplace.Id);

                        if (productInformationMarketplace == null) continue;

                        var listUnitPriceIndex = cells.IndexOf($"{theMarketplace.Name}_Liste_Fiyatı");
                        var unitPriceIndex = cells.IndexOf($"{theMarketplace.Name}_Satış_Fiyatı");
                        var activeIndex = cells.IndexOf($"{theMarketplace.Name}_Bağlantısı");
                        var accountingPriceValueIndex = cells.IndexOf($"{theMarketplace.Name}_Xml_Erp_Fiyat_Güncellensin");

                        var listUnitPrice = -1m;
                        var unitPrice = -1m;


                        bool? active =
                        activeIndex > 0 && row.Cells[activeIndex].Value.ToString().ToLower() == "evet" ? true :
                        activeIndex > 0 && row.Cells[activeIndex].Value.ToString().ToLower() == "hayır" ? false : null;

                        bool? AccountingPrice =
                       accountingPriceValueIndex > 0 && row.Cells[accountingPriceValueIndex].Value.ToString().ToLower() == "evet" ? true :
                        accountingPriceValueIndex > 0 && row.Cells[accountingPriceValueIndex].Value.ToString().ToLower() == "hayır" ? false : null;



                        decimal.TryParse(row.Cells[listUnitPriceIndex].Value.ToString().Replace(",", "."), out listUnitPrice);
                        decimal.TryParse(row.Cells[unitPriceIndex].Value.ToString().Replace(",", "."), out unitPrice);

                        if (productInformationMarketplace.ListUnitPrice != listUnitPrice
                        || productInformationMarketplace.UnitPrice != unitPrice
                        || (active.HasValue && productInformationMarketplace.Active != active.Value)
                        || (AccountingPrice.HasValue && productInformationMarketplace.AccountingPrice != AccountingPrice.Value))
                        {
                            productInformationMarketplaceBulkPrices.Add(new Domain.Entities.ProductInformationMarketplaceBulkPrice
                            {
                                Id = productInformationMarketplace.Id,
                                ListUnitPrice = listUnitPriceIndex > -1 && listUnitPrice > -1 ? listUnitPrice : productInformationMarketplace.ListUnitPrice,
                                UnitPrice = unitPriceIndex > -1 && unitPrice > -1 ? unitPrice : productInformationMarketplace.UnitPrice,
                                AccountingPrice = accountingPriceValueIndex > -1 && AccountingPrice.HasValue ? AccountingPrice.Value : productInformationMarketplace.AccountingPrice,
                                Active = activeIndex > -1 && active.HasValue ? active.Value : productInformationMarketplace.Active
                            });
                        }
                    }
                }

                BulkMappingInsert(productInformationMarketplaceBulkPrices);

                response.Message = "Fiyatlarınız başarılı bir şekilde eklenmiştir.";
                response.Success = true;


            });
        }
        #endregion

        #region Helpers

        private DataResponse<IQueryable<Domain.Entities.ProductInformation>> GetProductInformations(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<IQueryable<Domain.Entities.ProductInformation>>>((response) =>
            {
                var parameters = search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(x => !x.Product.IsOnlyHidden);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(pi => pi.ProductInformationTranslations.Any(pit => pit.Name.Contains(value)));
                        }
                    }
                    else if (pLoop.StartsWith("StockCode:"))
                    {
                        var value = pLoop.Replace("StockCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(pi => pi.StockCode.Contains(value));
                    }
                    else if (pLoop.StartsWith("Barcode:"))
                    {
                        var value = pLoop.Replace("Barcode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(pi => pi.Barcode.Contains(value));
                    }
                    else if (pLoop.StartsWith("ModelCode:"))
                    {
                        var value = pLoop.Replace("ModelCode:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Product.SellerCode.Contains(value));
                    }
                    else if (pLoop.StartsWith("CategoryId:"))
                    {
                        var value = pLoop.Replace("CategoryId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.Product.CategoryId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("BrandId:"))
                    {
                        var value = pLoop.Replace("BrandId:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.Product.BrandId == Convert.ToInt32(value));
                    }
                    else if (pLoop.StartsWith("ProductPropertyId:"))
                    {
                        var value = pLoop.Replace("ProductPropertyId:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            if (value == "1")
                                iQueryable = iQueryable.Where(pi => pi.Stock > 0);
                            else if (value == "2")
                                iQueryable = iQueryable.Where(pi => !pi.ProductInformationPhotos.Any());
                            else if (value == "3")
                                iQueryable = iQueryable.Where(iq => iq.ProductInformationMarketplaces.Any(pim => pim.DisabledUpdateProduct));
                        }
                    }

                    else if (pLoop.StartsWith("ProductSourceDomainId:"))
                    {
                        var value = pLoop.Replace("ProductSourceDomainId:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            iQueryable = iQueryable.Where(iq => value == "0" ? !iq.Product.ProductSourceDomainId.HasValue : iq.Product.ProductSourceDomainId == Convert.ToInt32(value));
                        }


                    }
                    else if (pLoop.StartsWith("ProductXmlErpId:"))
                    {
                        var value = pLoop.Replace("ProductXmlErpId:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            bool accountingPrice = false;
                            switch (Convert.ToInt32(value))
                            {
                                case ((int)AccountingPrice.UpdatePrice):
                                    accountingPrice = true;
                                    break;
                            }

                            iQueryable = iQueryable.Where(x => x.ProductInformationMarketplaces.Any(pim => pim.AccountingPrice == accountingPrice));

                        }
                    }
                }

                response.Data = iQueryable;
                response.Success = true;
            });
        }

        private Response BulkMappingInsert(List<Domain.Entities.ProductInformationMarketplaceBulkPrice> entities, string updateMarketplaceId = null)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {



                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ListUnitPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("AccountingPrice", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Active", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("TenantId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("DomainId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));



                entities.ForEach(theProductInformationMarketplaceBulkPrice =>
                {
                    var row = dataTable.NewRow();
                    row["Id"] = theProductInformationMarketplaceBulkPrice.Id;
                    row["ListUnitPrice"] = theProductInformationMarketplaceBulkPrice.ListUnitPrice;
                    row["UnitPrice"] = theProductInformationMarketplaceBulkPrice.UnitPrice;
                    row["AccountingPrice"] = theProductInformationMarketplaceBulkPrice.AccountingPrice;
                    row["Active"] = theProductInformationMarketplaceBulkPrice.Active;
                    row["TenantId"] = this._tenantFinder.FindId();
                    row["DomainId"] = this._domainFinder.FindId();
                    row["CompanyId"] = this._companyFinder.FindId();

                    dataTable.Rows.Add(row);
                });

                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                using (conn = new SqlConnection(connectionString))
                {
                    var temporaryTableName = $"ProductInformationMarketplaceBulkPrice_{Guid.NewGuid().ToString().Replace("-", "")}";

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @$"
Create Table {temporaryTableName} (
    Id              Int Not Null,
    ListUnitPrice   Decimal(18,2) Not Null,
    UnitPrice       Decimal(18,2) Not Null,
    AccountingPrice Bit Not Null,
    Active          Bit Not Null,
    TenantId        Int Not Null,
    DomainId        Int Not Null,
    CompanyId       Int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = temporaryTableName;
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @$"
DELETE ProductInformationMarketplaceBulkPrices
FROM ProductInformationMarketplaceBulkPrices PMP
Join ProductInformationMarketplaces PIM On PIM.Id=PMP.Id
Where PMP.TenantId=@TenantId 
And PMP.DomainId=@DomainId 
And PMP.CompanyId=@CompanyId 
And (PIM.MarketplaceId=@MarketplaceId OR @MarketplaceId IS NULL)


INSERT INTO [dbo].[ProductInformationMarketplaceBulkPrices] ([Id],[ListUnitPrice],[UnitPrice],[AccountingPrice],[Active],[TenantId],[DomainId],[CompanyId])
Select T.* from {temporaryTableName} T

Drop Table  {temporaryTableName};";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                    command.Parameters.AddWithValue("@MarketplaceId", string.IsNullOrEmpty(updateMarketplaceId) ? DBNull.Value : updateMarketplaceId);

                    var dataReader = command.ExecuteNonQuery();
                }


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private Response BulkMappingUpdate()
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                using (conn = new SqlConnection(connectionString))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandTimeout = 300;

                    command.CommandText = @$"
            Update ProductInformationMarketplaces
            SET
            ListUnitPrice=MBP.ListUnitPrice,
            UnitPrice=MBP.UnitPrice,
            DirtyPrice=CASE WHEN MBP.ListUnitPrice<>PIM.ListUnitPrice OR MBP.UnitPrice<>PIM.UnitPrice  THEN 1 ELSE 0 END,
            DirtyProductInformation=CASE WHEN MBP.Active=1 AND PIM.Active=0  THEN 1 ELSE 0 END,
            Active=MBP.Active,
            AccountingPrice=MBP.AccountingPrice
            From ProductInformationMarketplaces PIM
            Join ProductInformationMarketplaceBulkPrices MBP ON MBP.Id=PIM.Id
            Where PIM.TenantId=@TenantId And PIM.DomainId=@DomainId And PIM.CompanyId=@CompanyId
            
            Delete ProductInformationMarketplaceBulkPrices Where TenantId=@TenantId And DomainId=@DomainId And CompanyId=@CompanyId
            ";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                    var dataReader = command.ExecuteNonQuery();

                }


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });

        }

        private Response DeleteBulkPrices()
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                using (conn = new SqlConnection(connectionString))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandTimeout = 300;

                    command.CommandText = @$"
Delete ProductInformationMarketplaceBulkPrices Where TenantId=@TenantId And DomainId=@DomainId And CompanyId=@CompanyId";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();
                }


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private Response DeleteBulkPrices(int productInformationId)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                using (conn = new SqlConnection(connectionString))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandTimeout = 300;

                    command.CommandText = @$"
Delete PMB
From ProductInformationMarketplaceBulkPrices PMB
Join ProductInformationMarketplaces PIM ON PIM.Id= PMB.Id
Join ProductInformations PIN ON PIN.Id=PIM.ProductInformationId
Where PIN.Id=@ProductInformationId And PMB.TenantId=@TenantId And PMB.DomainId=@DomainId And PMB.CompanyId=@CompanyId";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                    command.Parameters.AddWithValue("@ProductInformationId", productInformationId);

                    var dataReader = command.ExecuteNonQuery();
                }


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }
        #endregion

    }
}