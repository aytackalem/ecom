﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class DistrictService : IDistrictService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        private readonly IDbNameFinder _dbNameFinder;

        #endregion

        #region Constructors
        public DistrictService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods     
        public DataResponse<List<District>> ReadByCityId(int cityId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<District>>>((response) =>
            {
                var entities = this._unitOfWork.DistrictRepository.DbSet().Where(n => n.CityId == cityId).OrderBy(n => n.Name).ToList();
                if (entities?.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "İlçe bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = entities
                        .Select(e => new District
                        {
                            Id = e.Id,
                            Name = e.Name,
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "İlçeler başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İlçeler başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.DistrictsByCityId, cityId, this._dbNameFinder.FindName()));
        }
        #endregion
    }
}
