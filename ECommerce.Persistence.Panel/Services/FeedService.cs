﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class FeedService : IFeedService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public FeedService(IUnitOfWork unitOfWork, ISettingService settingService, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Feed> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Feed>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .FeedRepository
                    .DbSet()
                    .Include(f => f.FeedDetails)
                    .ThenInclude(fd => fd.ProductInformation.ProductInformationTranslations.Where(fit => fit.LanguageId == "TR"))
                    .FirstOrDefault(f => f.Id == id);

                if (entity == null)
                {
                    response.Message = $"Feed bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Feed
                {
                    Id = entity.Id,
                    FeedTemplateId = entity.FeedTemplateId,
                    Name = entity.Name,
                    FileName = entity.FileName,
                    FeedDetails = entity.FeedDetails.Select(fd => new FeedDetail
                    {
                        ProductInformationId = fd.ProductInformationId,
                        ProductInformationName = fd.ProductInformation.ProductInformationTranslations[0].Name
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Feed> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Feed>>((response) =>
            {
                var iQueryable = this
                    ._unitOfWork
                    .FeedRepository
                    .DbSet()
                    .AsQueryable();

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable
                    .Count();

                response.Data = iQueryable
                    .OrderByDescending(x => x.Id)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(f => new Feed
                    {
                        Id = f.Id,
                        Name = f.Name,
                        FileName = f.FileName
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Feed dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var feedEntity = this
                    ._unitOfWork
                    .FeedRepository
                    .DbSet()
                    .Include(f => f.FeedDetails)
                    .FirstOrDefault(x => x.Id == dto.Id);

                feedEntity.Name = dto.Name;

                feedEntity.FeedDetails = new();
                feedEntity.FeedDetails = dto.FeedDetails.Select(fd => new Domain.Entities.FeedDetail { ProductInformationId = fd.ProductInformationId }).ToList();

                var update = this._unitOfWork.FeedRepository.Update(feedEntity);
                if (!update)
                {
                    response.Message = "Feed güncellenemedi lütfen tekrar deneyiniz.";
                    response.Success = false;
                }

                response.Message = "Feed başarılı bir şekilde güncellendi.";
                response.Success = true;
            });
        }

        public Response Create(Feed dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this
                    ._unitOfWork
                    .FeedRepository
                    .Create(new Domain.Entities.Feed
                    {
                        FeedTemplateId = dto.FeedTemplateId,
                        Name = dto.Name,
                        FileName = dto.Name.GenerateUrl(),
                        FeedDetails = dto.FeedDetails.Select(fd => new Domain.Entities.FeedDetail { ProductInformationId = fd.ProductInformationId }).ToList()
                    });

                response.Message = "Feed başarılı bir şekilde kaydedildi.";
                response.Success = true;
            });
        }
        #endregion
    }
}
