﻿using Dapper;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Reports;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ReportService : IReportService
    {
        #region Fields
        private readonly ISettingService _settingService;

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ReportService(ISettingService settingService, IUnitOfWork unitOfWork)
        {
            #region Fields
            this._settingService = settingService;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<HeaderReport> ReadHeaderReport(ReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<HeaderReport>>((response) =>
            {
                HeaderReport data = new();

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Select	IsNull(Sum((O.ExchangeRate * OD.UnitPrice) * OD.Quantity), 0), IsNull(Sum(OD.UnitCost * OD.Quantity), 0)
From	Orders As O With(NoLock)
Join	OrderDetails As OD
On		O.Id = OD.OrderId
Where	Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		And O.TenantId = @TenantId
		And O.DomainId = @DomainId
		And O.CompanyId = @CompanyId
        And O.OrderTypeId <> 'IP'
        And O.OrderTypeId <> 'IA'";

                    if (!string.IsNullOrEmpty(request.MarketplaceId))
                        query += $"{Environment.NewLine}And O.MarketplaceId = @MarketplaceId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);

                        if (!string.IsNullOrEmpty(request.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", request.MarketplaceId);

                        sqlConnection.Open();

                        var dataReader = sqlCommand.ExecuteReader();
                        while (dataReader.Read())
                        {
                            data.TotalSaleAmount = dataReader.GetDecimal(0);
                            data.TotalProductCostAmount = dataReader.GetDecimal(1);
                        }
                    }

                    sqlConnection.Close();

                    query = @"
Select	IsNull(Sum(O.CommissionAmount), 0), IsNull(Count(0), 0) * 25.0
From	Orders As O With(NoLock)
Where	Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		And O.TenantId = @TenantId
		And O.DomainId = @DomainId
		And O.CompanyId = @CompanyId
        And O.OrderTypeId <> 'IP'
        And O.OrderTypeId <> 'IA'";

                    if (!string.IsNullOrEmpty(request.MarketplaceId))
                        query += $"{Environment.NewLine}And O.MarketplaceId = @MarketplaceId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);

                        if (!string.IsNullOrEmpty(request.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", request.MarketplaceId);

                        sqlConnection.Open();

                        var dataReader = sqlCommand.ExecuteReader();
                        while (dataReader.Read())
                        {
                            data.TotalCommissionAmount = dataReader.GetDecimal(0);
                            data.TotalShippingCostAmount = dataReader.GetDecimal(1);
                        }
                    }

                    sqlConnection.Close();
                }

                response.Data = data;
                response.Success = true;
                response.Message = "Rapor oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<ExpenseReport> ReadExpenseReport(ReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<ExpenseReport>>((response) =>
            {
                ExpenseReport data = new();

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Select	IsNull(Sum(E.Amount), 0) As ExpenseAmount,
        IsNull(Sum(Case When E.ProductInformationId Is Not Null Then E.Amount Else 0.0 End), 0) As ProductExpenseAmount,
        IsNull(Sum(Case When E.ProductInformationId Is Null Then E.Amount Else 0.0 End), 0) As OtherExpenseAmount
From	Expenses As E With(NoLock)
Where	Cast(E.ExpenseDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		And E.TenantId = @TenantId
		And E.DomainId = @DomainId
		And E.CompanyId = @CompanyId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);

                        sqlConnection.Open();

                        var dataReader = sqlCommand.ExecuteReader();
                        while (dataReader.Read())
                        {
                            data.TotalExpenseAmount = dataReader.GetDecimal(0);
                            data.TotalAmountProductBasedExpenses = dataReader.GetDecimal(1);
                            data.OtherExpensesTotalAmount = dataReader.GetDecimal(2);
                        }
                    }
                }

                sqlConnection.Close();

                response.Data = data;
                response.Success = true;
                response.Message = "Rapor oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<OrderReport> ReadDetailedOrderReport(ReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<OrderReport>>((response) =>
            {
                OrderReport data = new()
                {
                    OrderReportItems = new()
                };

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @$"
With T As (
	Select		O.Id,
				O.OrderSourceId,
				O.MarketplaceId,
				Sum(OD.Quantity * (O.ExchangeRate * OD.UnitPrice)) Amount,
				Sum(OD.Quantity * (O.ExchangeRate * OD.VatExcUnitPrice)) VatExcAmount,
				Sum(OD.Quantity * OD.UnitCost) ProductCost,
				Sum(OD.Quantity * (O.ExchangeRate * OD.VatExcUnitCost)) VatExcProductCost,
				Sum(OD.Quantity * OD.UnitCommissionAmount) CommissionAmount
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
                And OD.TenantId = @TenantId
                And OD.DomainId = @DomainId
                And OD.CompanyId = @CompanyId
	Where		Cast(O.OrderDate As Date) Between @StartDate And @EndDate
				And O.OrderTypeId <> 'IP'
                And O.OrderTypeId <> 'IA'
                And O.TenantId = @TenantId
                And O.DomainId = @DomainId
                And O.CompanyId = @CompanyId
                {(string.IsNullOrEmpty(request.MarketplaceId) ? string.Empty : "And O.MarketplaceId = @MarketplaceId")}
	Group By	O.Id,
				O.OrderSourceId,
				O.MarketplaceId
), T2 As (
	Select		T.OrderSourceId,
				T.MarketplaceId,
				Count(0) [Count],
				Sum(Amount) Amount,
				Sum(VatExcAmount) VatExcAmount,
				Sum(ProductCost) ProductCost,
				Sum(VatExcProductCost) VatExcProductCost,
				Sum(CommissionAmount) CommissionAmount
	From		T
	Group By	T.OrderSourceId,
				T.MarketplaceId
)
Select	    T2.OrderSourceId,
		    OS.[Name] As OrderSourceName,
			T2.MarketplaceId,
			M.[Name] As MarketplaceName,
		    T2.Amount,
		    T2.VatExcAmount,
			T2.ProductCost,
		    T2.VatExcProductCost,
			T2.CommissionAmount,
		    T2.[Count]
From	    T2
Join	    OrderSources As OS With(NoLock)
On		    T2.OrderSourceId = OS.Id
            And OS.TenantId = @TenantId
Left Join	MarketPlaces As M
On			T2.MarketplaceId = M.Id";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);

                        if (!string.IsNullOrEmpty(request.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", request.MarketplaceId);

                        sqlConnection.Open();

                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlDataReader.Read())
                            {
                                data.OrderReportItems.Add(new OrderReportItem
                                {
                                    OrderSourceId = sqlDataReader.GetInt32(0).ToString(),
                                    OrderSourceName = sqlDataReader.GetString(1),
                                    MarketplaceId = sqlDataReader[2] == DBNull.Value ? null : sqlDataReader.GetString(2),
                                    MarketplaceName = sqlDataReader[3] == DBNull.Value ? null : sqlDataReader.GetString(3),
                                    Amount = sqlDataReader.GetDecimal(4),
                                    VatExcAmount = sqlDataReader.GetDecimal(5),
                                    ProductCost = sqlDataReader.GetDecimal(6),
                                    VatExcProductCost = sqlDataReader.GetDecimal(7),
                                    CommissionAmount = sqlDataReader.GetDecimal(8),
                                    Count = sqlDataReader.GetInt32(9)
                                });
                            }
                        }
                    }
                }

                sqlConnection.Close();

                response.Data = data;
                response.Success = true;
                response.Message = "Rapor oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<List<OrderProductReport>> ReadOrderProductReport(ReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<OrderProductReport>>>((response) =>
            {
                var a = this._settingService.ProductImageUrl;

                List<OrderProductReport> models = new();

                DynamicParameters dynamicParameters = new();
                dynamicParameters.Add("StartDate", request.StartDate);
                dynamicParameters.Add("EndDate", request.EndDate);
                dynamicParameters.Add("TenantId", request.TenantId);
                dynamicParameters.Add("DomainId", request.DomainId);
                dynamicParameters.Add("CompanyId", request.CompanyId);
                dynamicParameters.Add("MinQuantity", request.MinQuantity);

                if (!string.IsNullOrEmpty(request.MarketplaceId))
                    dynamicParameters.Add("MarketplaceId", request.MarketplaceId);

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    models.AddRange(sqlConnection.Query<OrderProductReport>(sql: @$"
With T As (
	Select		OD.ProductInformationId As Id,
                Sum(OD.Quantity) As Quantity
	From		Orders As O With(NoLock)

	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
                And OD.TenantId = @TenantId
                And OD.DomainId = @DomainId
                And OD.CompanyId = @CompanyId

	Where		Cast(O.OrderDate As Date) Between @StartDate And @EndDate
                And O.OrderTypeId <> 'IP'
                And O.OrderTypeId <> 'IA'
                And O.TenantId = @TenantId
                And O.DomainId = @DomainId
                And O.CompanyId = @CompanyId
                {(string.IsNullOrEmpty(request.MarketplaceId) ? string.Empty : "And O.MarketplaceId = @MarketplaceId")}

	Group By	OD.ProductInformationId
    Having      Sum(OD.Quantity) > @MinQuantity
)
Select      T.Id,
            [Name] + ' ' + ISNULL([VariantValuesDescription],'') As [Name],
            T.[Quantity],
            DS.ImageUrl + '/product/' + Min(PIP.FileName) As FileName,
            ISNULL(PIN.StockCode,'') As StockCode,
            ISNULL(PIN.Barcode,'') As Barcode,
            ISNULL(PIN.SkuCode,'') As SkuCode,
            ISNULL(P.SellerCode,'') As SellerCode,
            PIN.Stock,
            PIN.ProductId
From	    T
Join	    ProductInformationTranslations As PIT With(NoLock)
On		    T.Id = PIT.ProductInformationId
		    And PIT.LanguageId = 'TR'
            And PIT.TenantId = @TenantId
            And PIT.DomainId = @DomainId
Join		ProductInformations AS PIN  With(NoLock)
On			T.Id = PIN.Id
Join		Products AS P  With(NoLock)
On			P.Id = PIN.ProductId
Left Join   ProductInformationPhotos As PIP
On          T.Id = PIP.ProductInformationId
Join		DomainSettings DS 
On          DS.Id=PIP.DomainId
Where		P.IsOnlyHidden = 0
Group By    T.Id
		    ,[Name]
			,[VariantValuesDescription]
		    ,T.[Quantity]
			,PIN.StockCode
			,PIN.Barcode
			,PIN.SkuCode
			,P.SellerCode
            ,PIN.Stock
            ,PIN.ProductId
            ,DS.ImageUrl
Order By    T.[Quantity] Desc",
                    param: dynamicParameters));
                }

                sqlConnection.Close();



                response.Data = models;
                response.Success = true;
                response.Message = "İçerik oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<List<ProductBasedSalesReport>> ReadProductBasedSalesReport(ReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<ProductBasedSalesReport>>>((response) =>
            {
                List<ProductBasedSalesReport> models = new();

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
With C As (
	Select		O.Id,
				25.0 / Sum(OD.Quantity) As UnitCargoFee
	From		Orders As O
	Join		OrderDetails As OD
	On			O.Id = OD.OrderId
	Where		O.TenantId = @TenantId
				And O.DomainId = @DomainId
                And O.CompanyId = @CompanyId
				And O.OrderTypeId <> 'IP'
                And O.OrderTypeId <> 'IA'
				And Cast(O.OrderDate As Date) Between @StartDate And @EndDate
	Group By	O.Id
), T As (
	Select		ProductInformationId As Id,
				Sum(Quantity) As Quantity,
				Sum(Quantity * C.UnitCargoFee) / Sum(Quantity) As AvgUnitCargoFee,
				Sum(Quantity * UnitCost) / Sum(Quantity) As AvgUnitCost,
				Sum(Quantity * UnitPrice) / Sum(Quantity) As AvgUnitPrice,
				Sum(Quantity * UnitCommissionAmount) / Sum(Quantity) As AvgUnitCommissionAmount
	From		OrderDetails As OD
	Join		C
	On			OD.OrderId = C.Id
	Group By	ProductInformationId
), T2 As (
	Select	T.Id,
			PIT.[Name],
			T.Quantity,
			T.AvgUnitCargoFee,
			T.AvgUnitCost,
			T.AvgUnitPrice,
			T.AvgUnitCommissionAmount
	From	T
	Join	ProductInformationTranslations As PIT
	On		T.Id = PIT.ProductInformationId
			And PIT.LanguageId = 'TR'
), T3 As (
	Select		T2.Id,
				T2.[Name],
				T2.Quantity,
				T2.AvgUnitCargoFee,
				T2.AvgUnitCost,
				T2.AvgUnitPrice,
				T2.AvgUnitCommissionAmount,
				Max(PIP.[FileName]) As [FileName]
	From		T2
	Join		ProductInformationPhotos As PIP
	On			T2.Id = PIP.ProductInformationId
	Group By	T2.Id,
				T2.[Name],
				T2.Quantity,
				T2.AvgUnitCargoFee,
				T2.AvgUnitCost,
				T2.AvgUnitPrice,
				T2.AvgUnitCommissionAmount
)
Select		T3.Id,
			T3.[Name],
			T3.Quantity,
			T3.AvgUnitCargoFee,
			T3.AvgUnitCost,
			T3.AvgUnitPrice,
			T3.AvgUnitCommissionAmount,
			T3.[FileName],
			Sum(IsNull(E.Amount, 0)) As ExpenseAmount
From		T3
Left Join	Expenses As E With(NoLock)
On			T3.Id = E.ProductInformationId
			And Cast(E.ExpenseDate As Date) Between @StartDate And @EndDate
Group By	T3.Id,
			T3.[Name],
			T3.Quantity,
			T3.AvgUnitCargoFee,
			T3.AvgUnitCost,
			T3.AvgUnitPrice,
			T3.AvgUnitCommissionAmount,
			T3.[FileName]";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            models.Add(new ProductBasedSalesReport
                            {
                                Id = sqlDataReader.GetInt32(0),
                                Name = sqlDataReader.GetString(1),
                                Quantity = sqlDataReader.GetInt32(2),
                                AvgUnitCargoFee = sqlDataReader.GetDecimal(3),
                                AvgUnitCost = sqlDataReader.GetDecimal(4),
                                AvgUnitPrice = sqlDataReader.GetDecimal(5),
                                AvgUnitCommissionAmount = sqlDataReader.GetDecimal(6),
                                FileName = $"{this._settingService.ProductImageUrl}/{sqlDataReader.GetString(7)}",
                                ExpenseAmount = sqlDataReader.GetDecimal(8)
                            });
                        }
                    }
                }

                sqlConnection.Close();

                models.Sort((a, b) => b.ProfitRate.CompareTo(a.ProfitRate));

                response.Data = models;
                response.Success = true;
                response.Message = "İçerik oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<List<KeyValue<string, int>>> ReadPackagingReport(PackagingReportRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, int>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .OrderPackingRepository
                    .DbSet()
                    .AsNoTracking()
                    .Where(op => op.CreatedDate.Date >= request.StartDate.Date &&
                                 op.CreatedDate.Date <= request.EndDate.Date)
                    .GroupBy(op => op.PackedUsername)
                    .Select(op => new KeyValue<string, int>
                    {
                        Key = op.Key,
                        Value = op.Count()
                    }).ToList();
            });
        }

        public DataResponse<List<SoldTogetherReport>> ReadSoldTogetherReport(SoldTogetherReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<SoldTogetherReport>>>((response) =>
            {
                List<KeyValue<string, int>> datas = new();

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
With T As (
	Select		O.Id,
				OD.ProductInformationId
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	Where		O.TenantId = @TenantId
                And O.DomainId = @DomainId
                And O.CompanyId = @CompanyId
                And O.OrderSourceId = 1
				And Cast(O.OrderDate As Date) Between @StartDate And @EndDate
	Group By	O.Id,
				OD.ProductInformationId
), T2 As (
	Select		String_Agg(ProductInformationId, ',') Within Group (Order By ProductInformationId Asc) As ProductInformationIds
	From		T
	Group By	Id
	Having		Count(0) > 1
), T3 As (
	Select		ProductInformationIds,
				Count(0) As [Count],
				Row_Number() Over(Order By Count(0) Desc) As RowNumber
	From		T2
	Group By	ProductInformationIds
)
Select	*
From	T3
Where	RowNumber <= @Top";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@StartDate", request.StartDate);
                        sqlCommand.Parameters.AddWithValue("@EndDate", request.EndDate);
                        sqlCommand.Parameters.AddWithValue("@TenantId", request.TenantId);
                        sqlCommand.Parameters.AddWithValue("@DomainId", request.DomainId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", request.CompanyId);
                        sqlCommand.Parameters.AddWithValue("@Top", request.Top);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            datas.Add(new KeyValue<string, int>
                            {
                                Key = sqlDataReader.GetString(0),
                                Value = sqlDataReader.GetInt32(1)
                            });
                        }
                    }
                }

                sqlConnection.Close();

                var productInformationIds = datas.SelectMany(d => d.Key.Split(",").Select(s => int.Parse(s))).Distinct().ToList();

                var productInformations = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(pi => productInformationIds.Contains(pi.Id))
                    .Select(pi => new ProductInformation
                    {
                        Id = pi.Id,
                        //ProductInformationName = pi.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Name,
                        ProductInformationPhotos = new List<ProductInformationPhoto>
                        {
                            new ProductInformationPhoto
                            {
                                FileName = $"{this._settingService.ProductImageUrl}/{pi.ProductInformationPhotos.FirstOrDefault().FileName}"
                            }
                        },
                        ProductInformationTranslations = pi
                            .ProductInformationTranslations
                            .Select(pit => new ProductInformationTranslation
                            {
                                VariantValuesDescription = pit.VariantValuesDescription
                            })
                            .ToList()
                    })
                    .ToList();

                response.Data = datas.Select(d => new SoldTogetherReport
                {
                    Count = d.Value,
                    ProductInformations = productInformations
                        .Where(pi =>
                        {
                            return d.Key.Split(",").Select(s => int.Parse(s)).Contains(pi.Id);
                        })
                        .ToList()
                }).ToList();
                response.Success = true;
                response.Message = "İçerik oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<List<ProductBasedStockReport>> ReadProductBasedStockReport(ProductBasedStockReportRequest request)
        {
            var query = @$"
With T As (
	Select		[PI].Id,
				[PI].SkuCode,
				[PI].Barcode,
				[PI].Stock,
				[PIT].[Name],
				[PIT].[VariantValuesDescription],
                P.SellerCode,
				Min(PIP.FileName) As [FileName]
	From		ProductInformations As [PI] With(NoLock)
    Join        Products As P With(NoLock)
    On          P.Id = [PI].ProductId
	Join		ProductInformationTranslations As PIT With(NoLock)
	On			[PI].Id = PIT.ProductInformationId
				And PIT.LanguageId = 'TR'
	Left Join	ProductInformationPhotos As PIP With(NoLock)
	On			[PI].Id = PIP.ProductInformationId
	Where		ProductId = @ProductId
				{(string.IsNullOrEmpty(request.SkuCode) ? "" : "And SkuCode = @SkuCode")}
	Group By	[PI].Id,
				[PI].SkuCode,
				[PI].Barcode,
				[PI].Stock,
				[PIT].[Name],
				[PIT].[VariantValuesDescription],
                P.SellerCode
), T2 As (
	Select		[PI].Id,
				[PI].SkuCode,
				[PI].Barcode,
				[PI].Stock,
				IsNull(Sum(OD.Quantity), 0) As [Quantity]
	From		ProductInformations As [PI] With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			OD.ProductInformationId = [PI].Id
	Join		Orders As O With(NoLock)
	On			OD.OrderId = O.Id
				And O.OrderTypeId <> 'IP'
                And O.OrderTypeId <> 'IA'
				And Cast(O.OrderDate As Date) Between @StartDate And @EndDate
	Where		ProductId = @ProductId
				{(string.IsNullOrEmpty(request.SkuCode) ? "" : "And SkuCode = @SkuCode")}
	Group By	[PI].Id,
				[PI].SkuCode,
				[PI].Barcode,
				[PI].Stock
)
Select		T.Id As ProductInformationId,
			T.Barcode,
			T.SkuCode,
			T.Stock,
			T.[FileName],
			T.[Name],
			T.[VariantValuesDescription],
            T.SellerCode,
			IsNull(Sum(T2.Quantity), 0) As [SoldQuantity]
From		T
Left Join	T2
On			T.Id = T2.Id
Group By	T.Id,
			T.SkuCode,
			T.Barcode,
			T.Stock,
			T.[FileName],
			T.[Name],
			T.[VariantValuesDescription],
            T.SellerCode
Order By    T.SkuCode,
            T.[VariantValuesDescription]";

            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<ProductBasedStockReport>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    response.Data = sqlConnection
                        .Query<ProductBasedStockReport>(query, new
                        {
                            request.ProductId,
                            request.StartDate,
                            request.EndDate,
                            request.SkuCode
                        })
                        .ToList();
                }
                response.Data.ForEach(d => d.FileName = $"{this._settingService.ProductImageUrl}/{d.FileName}");
                response.Success = true;
                response.Message = "İçerik oluşturuldu.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<PickedReport> ReadPickedReport(PickedReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<PickedReport>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    response.Data = new PickedReport(sqlConnection
                        .Query<PickedItem>(@"
Select		OP.PickedUsername As Username,
			DatePart(Hour, OP.CreatedDate) As [Hour],
			Count(Distinct OP.Id) As OrderQuantity,
			Count(0) As ProductQuantity
From		OrderPickings As OP With(NoLock)
Join		OrderDetails As OD With(NoLock)
On			OP.Id = OD.OrderId
Where		OP.PickedUsername Is Not Null
			And Cast(OP.CreatedDate As Date) Between @StartDate And @EndDate
Group By	OP.PickedUsername,
			DatePart(Hour, OP.CreatedDate)", new
                        {
                            StartDate = request.StartDate.Date,
                            EndDate = request.EndDate.Date
                        })
                        .ToList());
                }

                response.Success = true;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        public DataResponse<ColorBasedSalesReport> ReadColorBasedSalesReport(ColorBasedSalesReportRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<ColorBasedSalesReport>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var skuCode = request.SkuCode == null ? "1" : request.SkuCode.CleanParameter();
                    var sellerCode = request.SellerCode == null ? "1" : request.SellerCode.CleanParameter();
                    var marketplaceId = request.MarketplaceId == null ? "1" : request.MarketplaceId.CleanParameter();
                    var categoryId = request.categoryId == 0 ? 0 : request.categoryId;

                    response.Data = new ColorBasedSalesReport(sqlConnection
                        .Query<ColorBasedSalesReportItem>(@$"

Declare
@CategoryId NVarChar(100) = '{categoryId}',
@SkuCode NVarChar(100) = '{skuCode}',
@SellerCode NVarChar(100) = '{sellerCode}', 
@MarketplaceId NVarChar(100) = '{marketplaceId}',
@StartDate DateTime = '{request.StartDate.Date.ToString("yyyy-MM-dd")}',
@EndDate DateTime = '{request.EndDate.Date.ToString("yyyy-MM-dd")}'

;With T As (


	Select		P.Id As ProductId,
				PF.SkuCode,
				PF.Categorization,
				P.SellerCode,
				PIT.[Name],
				Sum(OD.Quantity) As Quantity
	
	From		Orders As O With(NoLock)
	
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	
	Join		ProductInformations As PF With(NoLock)
	On			OD.ProductInformationId = PF.Id
                And (@SkuCode = '1' Or PF.SkuCode Like '%' + @SkuCode + '%')
	
	Join		ProductInformationTranslations As PIT With(NoLock)
	On			PF.Id = PIT.ProductInformationId
				And PIT.LanguageId = 'TR'

	Join		Products As P With(NoLock)
	On			P.Id = PF.ProductId
				And (@SellerCode = '1' Or P.SellerCode Like '%' + @SellerCode + '%')
                And (@CategoryId = 0 Or P.CategoryId = @CategoryId)

	Where		Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
				And O.OrderTypeId <> 'IP' 
				And O.OrderTypeId <> 'IA'
				And (@MarketplaceId = '1' Or O.MarketplaceId = @MarketplaceId)
	
	Group By	P.Id,
				PF.SkuCode,
				PF.Categorization,
				P.SellerCode,
				PIT.[Name]
), T1 As (
	Select		T.ProductId,
				T.SkuCode,
				T.Categorization,
				T.SellerCode,
				T.[Name],
				T.Quantity,
				Sum(PF.Stock) As Stock
	
	From		T

	Join		ProductInformations As PF With(NoLock)
	On			PF.SkuCode = T.SkuCode
				And PF.ProductId = T.ProductId
	
	Group By	T.ProductId,
				T.SkuCode,
				T.Categorization,
				T.SellerCode,
				T.[Name],
				T.Quantity
), T2 As (
	Select		T1.ProductId,
				T1.SkuCode,
				T1.Categorization,
				T1.SellerCode,
				T1.[Name],
				T1.Quantity,
				T1.Stock,
				Min(PIP.[FileName]) As [FileName],
				(
	                Select  Count(0)
	                From    ProductInformations PF2 With(NoLock)
	
	                Join    ReceiptItems RI With(NoLock)
	                On      PF2.Id = RI.ProductInformationId
	
	                Join    Receipts R With(NoLock)
	                On      RI.ReceiptId = R.Id
	                        And R.Cancelled = 0
	                        And R.Delivered = 0
	
	                Where   PF2.SkuCode = T1.SkuCode
	
	            ) As HasReceipt
	
	From		T1
	
	Join		ProductInformations As PF With(NoLock)
	On			PF.SkuCode = T1.SkuCode
				And PF.ProductId = T1.ProductId
		
	Join		ProductInformationPhotos As PIP With(NoLock)
	On			PIP.ProductInformationId = PF.Id
	
	Group By	T1.ProductId,
				T1.SkuCode,
				T1.Categorization,
				T1.SellerCode,
				T1.[Name],
				T1.Quantity,
				T1.Stock
)
Select		T2.ProductId,
			T2.SkuCode,
			T2.SellerCode,
			T2.[Name],
			T2.Quantity,
			T2.Stock,
			T2.[FileName],
			T2.HasReceipt,
			IsNull(T2.Categorization, '') CategorizationsString,
			IsNull(String_Agg(PGP.[Value], ','), '') BrandsString

From		T2

Left Join	ProductGenericProperties PGP With(NoLock)
On			PGP.ProductId = T2.ProductId
			And PGP.[Name] = 'Marka'

Group By	T2.ProductId,
			T2.SkuCode,
			T2.Categorization,
			T2.SellerCode,
			T2.[Name],
			T2.Quantity,
			T2.Stock,
			T2.[FileName],
			T2.HasReceipt

Order By    T2.SellerCode")
                        .ToList());
                }

                response.Data.Items.ForEach(i => i.FileName = $"{this._settingService.ProductImageUrl}/{i.FileName}");

                response.Success = true;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        public DataResponse<List<SalesPerformance>> ReadSalesPerformance()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<SalesPerformance>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    response.Data = sqlConnection
                        .Query<SalesPerformance>(@"
With T As (

	Select		PF.SkuCode,
				OD.Quantity,
				O.OrderDate
	
	From		Orders As O With(NoLock)
	
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	
	Join		ProductInformations As PF With(NoLock)
	On			OD.ProductInformationId = PF.Id
	
	Where		Cast(O.OrderDate As Date) Between Cast(DateAdd(Day, -29, GetDate()) As Date) And Cast(GetDate() As Date)
				And O.OrderTypeId <> 'IA'
				And O.OrderTypeId <> 'IP'

)
Select		T.SkuCode,
			Sum(T.Quantity) As Quantity,
			'Monthly' As [Type]
From		T
Group By	T.SkuCode

Union All

Select		T.SkuCode,
			Sum(T.Quantity) As Quantity,
			'Weekly' As [Type]
From		T
Where		Cast(T.OrderDate As Date) Between Cast(DateAdd(Day, -7, GetDate()) As Date) And Cast(GetDate() As Date)
Group By	T.SkuCode

Union All

Select		T.SkuCode,
			Sum(T.Quantity) As Quantity,
			'Daily' As [Type]
From		T
Where		Cast(T.OrderDate As Date) Between Cast(DateAdd(Day, -3, GetDate()) As Date) And Cast(GetDate() As Date)
Group By	T.SkuCode")
                        .ToList();
                }

                response.Success = true;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        public PagedResponse<UnsoldProduct> ReadUnsoldProduct(PagedRequest pagedRequest)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<PagedResponse<UnsoldProduct>>((response) =>
            {
                var dynamicParameters = new DynamicParameters();

                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Period:"))
                    {
                        var value = pLoop.Replace("Period:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@Period", value);
                        }
                    }
                    else if (pLoop.StartsWith("Quantity:"))
                    {
                        var value = pLoop.Replace("Quantity:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@Quantity", value);
                        }
                    }
                    else if (pLoop.StartsWith("SellerCode:"))
                    {
                        var value = pLoop.Replace("SellerCode:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@SellerCode", value);
                        }
                        else
                        {
                            dynamicParameters.Add("@SellerCode", null);
                        }
                    }
                    else if (pLoop.StartsWith("SkuCode:"))
                    {
                        var value = pLoop.Replace("SkuCode:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@SkuCode", value);
                        }
                        else
                        {
                            dynamicParameters.Add("@SkuCode", null);
                        }
                    }
                    else if (pLoop.StartsWith("Categorization:"))
                    {
                        var value = pLoop.Replace("Categorization:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@Categorization", value);
                        }
                        else
                        {
                            dynamicParameters.Add("@Categorization", null);
                        }
                    }
                    else if (pLoop.StartsWith("Brand:"))
                    {
                        var value = pLoop.Replace("Brand:", "");
                        if (value.Length > 0)
                        {
                            dynamicParameters.Add("@Brand", value);
                        }
                        else
                        {
                            dynamicParameters.Add("@Brand", null);
                        }
                    }
                }

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var orderByString = "Order By {0} {1}";
                    var paginationString = $"RowNumber Between {(pagedRequest.Page * pagedRequest.PageRecordsCount) + 1} And {(pagedRequest.Page * pagedRequest.PageRecordsCount) + pagedRequest.PageRecordsCount}";

                    switch (pagedRequest.Sort)
                    {
                        case "SkuCodeDesc":
                            orderByString = string.Format(orderByString, "SkuCode", "Desc");
                            break;
                        case "SkuCodeAsc":
                            orderByString = string.Format(orderByString, "SkuCode", "Asc");
                            break;
                        case "SellerCodeDesc":
                            orderByString = string.Format(orderByString, "SellerCode", "Desc");
                            break;
                        case "SellerCodeAsc":
                            orderByString = string.Format(orderByString, "SellerCode", "Asc");
                            break;
                        case "StockDesc":
                            orderByString = string.Format(orderByString, "Stock", "Desc");
                            break;
                        case "StockAsc":
                            orderByString = string.Format(orderByString, "Stock", "Asc");
                            break;
                        case "QuantityDesc":
                            orderByString = string.Format(orderByString, "Quantity", "Desc");
                            break;
                        default:
                            orderByString = string.Format(orderByString, "Quantity", "Asc");
                            break;
                    }

                    var gridReader = sqlConnection
                        .QueryMultiple(@$"
Select	Count(0)
From	UnsoldProducts As UP With(NoLock)
Where	[Period] = @Period
		And Quantity <= @Quantity
		And (@SellerCode Is Null Or SellerCode = @SellerCode)
		And (@SkuCode Is Null Or SkuCode =  @SkuCode)
		And (@Categorization Is Null Or Categorization = @Categorization)
        And (@Brand Is Null Or Brand = @Brand)

;With T As (
	Select		*,
				Row_Number() Over({orderByString}) As RowNumber
	From		UnsoldProducts As UP With(NoLock)
	Where		[Period] = @Period
				And Quantity <= @Quantity
				And (@SellerCode Is Null Or SellerCode = @SellerCode)
				And (@SkuCode Is Null Or SkuCode =  @SkuCode)
				And (@Categorization Is Null Or Categorization = @Categorization)
                And (@Brand Is Null Or Brand = @Brand)
)
Select	*
From	T
Where	{paginationString}", dynamicParameters);

                    var count = gridReader.ReadFirst<int>();
                    if (count > 0)
                    {
                        var productImageUrl = _settingService.ProductImageUrl;

                        response.PageRecordsCount = pagedRequest.PageRecordsCount;
                        response.Page = pagedRequest.Page;
                        response.RecordsCount = count;
                        response.Data = gridReader.Read<UnsoldProduct>().ToList();

                        response.Data.ForEach(theData => theData.FileName = $"{productImageUrl}/{theData.FileName}");
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        public DataResponse<List<StockAlert>> StockAlert()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<StockAlert>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
With T As (
	Select		PT.[Name],
				P.SellerCode,
				PN.SkuCode,
				Min(PIP.[FileName]) As [FileName],
				Sum(PN.Stock) As Stock
	From		ProductInformations As PN With(NoLock)
	Join		Products As P With(NoLock)
	On			PN.ProductId = P.Id
	Left Join	ProductInformationPhotos As PIP With(NoLock)
	On			PIP.ProductInformationId = PN.Id
				And PIP.DisplayOrder = 1
	Join		ProductTranslations As PT With(NoLock)
	On			P.Id = PT.ProductId
				And PT.LanguageId = 'TR'
	Where		PN.Stock > 0
	Group By	PT.[Name],
				P.SellerCode,
				PN.SkuCode
), T1 As (
	Select		PN.SkuCode,
				Sum(OD.Quantity) As Quantity,
				3 As [Type]
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	Join		ProductInformations As PN With(NoLock)
	On			OD.ProductInformationId = PN.Id
	Where		O.OrderTypeId <> 'IA'
				And O.OrderTypeId <> 'IP'
				And Cast(O.OrderDate As Date) Between Cast(DateAdd(Day, -3, GetDate()) As Date) And Cast(GetDate() As Date)
	Group By	PN.SkuCode
	Union
	Select		PN.SkuCode,
				Sum(OD.Quantity) As Quantity,
				7 As [Type]
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	Join		ProductInformations As PN With(NoLock)
	On			OD.ProductInformationId = PN.Id
	Where		O.OrderTypeId <> 'IA'
				And O.OrderTypeId <> 'IP'
				And Cast(O.OrderDate As Date) Between Cast(DateAdd(Day, -7, GetDate()) As Date) And Cast(GetDate() As Date)
	Group By	PN.SkuCode
	Union
	Select		PN.SkuCode,
				Sum(OD.Quantity) As Quantity,
				30 As [Type]
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	Join		ProductInformations As PN With(NoLock)
	On			OD.ProductInformationId = PN.Id
	Where		O.OrderTypeId <> 'IA'
				And O.OrderTypeId <> 'IP'
				And Cast(O.OrderDate As Date) Between Cast(DateAdd(Day, -29, GetDate()) As Date) And Cast(GetDate() As Date)
	Group By	PN.SkuCode
), T2 As (
	Select		T.*,
				((IsNull(T1_3.Quantity, 0) / 3) + (IsNull(T1_7.Quantity, 0) / 7) + (IsNull(T1_30.Quantity, 0) / 30)) / 3 As [DailyQuantity]
	From		T
	Left Join	T1 As T1_3
	On			T.SkuCode = T1_3.SkuCode
				And T1_3.[Type] = 3
	Left Join	T1 As T1_7
	On			T.SkuCode = T1_7.SkuCode
				And T1_7.[Type] = 7
	Left Join	T1 As T1_30
	On			T.SkuCode = T1_30.SkuCode
				And T1_30.[Type] = 30
)
Select		*,
			T2.Stock / T2.DailyQuantity As Remain
From		T2
Where		T2.DailyQuantity > 0
Order By    T2.Stock / T2.DailyQuantity";

                    response.Data = sqlConnection.Query<StockAlert>(query).ToList();
                    response.Success = true;
                    response.Message = "Rapor oluşturuldu.";
                }

                response.Data.ForEach(d =>
                {
                    var fileName = "default.jpg";
                    if (string.IsNullOrEmpty(d.FileName) == false)
                    {
                        fileName = d.FileName;
                    }
                    d.FileName = $"{this._settingService.ProductImageUrl}/{fileName}";
                });
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                {
                    sqlConnection.Close();
                }

            });
        }
        #endregion
    }
}
