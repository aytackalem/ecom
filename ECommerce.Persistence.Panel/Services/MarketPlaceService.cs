﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceService : IMarketplaceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<Marketplace>> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Marketplace>>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Where(mp => mp.Active)
                    .ToList();

                if (entities?.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Herhangi bir pazaryeri entegrasyonu bulunamadı.";
                    return;
                }

                response.Data = entities
                    .Select(e => new Marketplace
                    {
                        Id = e.Marketplace.Id,
                        Name = e.Marketplace.Name,
                        MarketplaceCompanyId = e.Id,
                        BrandAutocomplete = e.Marketplace.BrandAutocomplete,
                        IsECommerce = e.Marketplace.IsECommerce,
                        Matchable = e.Marketplace.Id != "TYE"
                    })
                    .ToList();

                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<Marketplace>> InfiniteReadCategories()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Marketplace>>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .ThenInclude(mp => mp.MarketplaceCategories)
                    .Where(mp => mp.Active)
                    .ToList();

                if (entities?.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Pazaryeri bulunamadı.";
                    return;
                }

                //response.Data = entities
                //    .Select(e => new Marketplace
                //    {
                //        Id = e.Marketplace.Id,
                //        Name = e.Marketplace.Name,
                //        MarketplaceCompanyId = e.Id,
                //        BrandAutocomplete = e.Marketplace.BrandAutocomplete,
                //        MarketplaceCategories = e.Marketplace
                //            .MarketplaceCategories
                //            .Select(mpc => new MarketplaceCategory
                //            {
                //                Id = mpc.Id,
                //                MarketplaceId = mpc.MarketplaceId,
                //                Name = mpc.Name,
                //            })
                //            .OrderBy(mc => mc.Name)
                //            .ToList()
                //    })
                //    .ToList();

                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Where(x => x.Active)
                    .Select(l => new KeyValue<string, string>
                    {
                        Key = l.Marketplace.Id,
                        Value = l.Marketplace.Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}