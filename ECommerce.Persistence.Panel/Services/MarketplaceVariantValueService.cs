﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceVariantValueService : IMarketplaceVariantValueService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly  IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public MarketplaceVariantValueService(IUnitOfWork unitOfWork, IDomainFinder domainFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceVariantId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariantValue>>>((response) =>
            {
                //response.Data = this
                //    ._unitOfWork
                //    .MarketplaceVariantsMarketplaceVariantValueRepository
                //    .DbSet()
                //    .Where(x => x.MarketplaceVariantId == marketplaceVariantId)
                //    .Select(x => new MarketplaceVariantValue
                //    {
                //        Id = x.MarketplaceVariantValue.Id,
                //        Value = x.MarketplaceVariantValue.Value,
                //        MarketplaceId = x.MarketplaceVariantValue.MarketplaceId
                //    })
                //    .OrderBy(x=>x.Value.Length)
                //    .ToList();

                //response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceVariantId, int take, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariantValue>>>((response) =>
            {
                //response.Data = this
                //    ._unitOfWork
                //    .MarketplaceVariantsMarketplaceVariantValueRepository
                //    .DbSet()
                //    .Where(x => x.MarketplaceVariantId == marketplaceVariantId && x.MarketplaceVariantValue.Value.Contains(q)
                //    && (!x.MarketplaceVariantValue.DomainId.HasValue || x.MarketplaceVariantValue.DomainId == this._domainFinder.FindId()))
                //    .OrderBy(x => x.MarketplaceVariantValue.Value.Length)
                //    .Take(take)
                //    .Select(x => new MarketplaceVariantValue
                //    {
                //        Id = x.MarketplaceVariantValue.Id,
                //        Value = x.MarketplaceVariantValue.Value,
                //        MarketplaceId = x.MarketplaceVariantValue.MarketplaceId
                //    })
                //    .ToList();

                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceCategoryId, string marketplaceVariantId, int take, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceVariantValue>>>((response) =>
            {
                //response.Data = this
                //    ._unitOfWork
                //    .MarketplaceCategoriesMarketplaceVariantValueRepository
                //    .DbSet()
                //    .Where(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId == marketplaceVariantId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId == marketplaceCategoryId && x.MarketplaceVariantValue.Value.Contains(q)
                //    && (!x.MarketplaceVariantValue.DomainId.HasValue || x.MarketplaceVariantValue.DomainId == this._domainFinder.FindId()))
                //    .OrderBy(x => x.MarketplaceVariantValue.Value.Length)
                //    .Take(take)
                //    .Select(x => new MarketplaceVariantValue
                //    {
                //        Id = x.MarketplaceVariantValue.Id,
                //        Value = x.MarketplaceVariantValue.Value,
                //        MarketplaceId = x.MarketplaceVariantValue.MarketplaceId,
                //        MarketplaceCategoriesMarketplaceVariantValueId = x.Id
                //    })
                //    .ToList();

                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}