﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Notification;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using ECommerce.Application.Panel.DataTransferObjects.Notifications;
using ECommerce.Application.Panel.Parameters.ProductPrices.Extensions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ECommerce.Persistence.Panel.Services
{
    public class NotificationService : Application.Panel.Interfaces.Services.INotificationService
    {
        #region Fields
        private readonly IMemoryCache _memoryCache;

        private readonly ICacheHandler _cacheHandler;

        public readonly IDbNameFinder _dbNameFinder;

        public readonly ICompanyFinder _companyFinder;

        private readonly Application.Common.Interfaces.Notification.INotificationService _notificationService;
        #endregion

        #region Constructors
        public NotificationService(ICacheHandler cacheHandler, IDbNameFinder dbNameFinder, ICompanyFinder companyFinder, Application.Common.Interfaces.Notification.INotificationService notificationService, IMemoryCache memoryCache)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._companyFinder = companyFinder;
            this._dbNameFinder = dbNameFinder;
            this._notificationService = notificationService;
            this._memoryCache = memoryCache;
            #endregion
        }
        #endregion

        public DataResponse<NotificationItemCount> Count()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<NotificationItemCount>>(async (response) =>
                {
                    var priceNotification =  this._notificationService.GetAsync(new GetCountParameter
                    {
                        CompanyId = _companyFinder.FindId(),
                        Brand = _dbNameFinder.FindName()
                    }).Result;

                    response.Data = new NotificationItemCount();
                    if (priceNotification.Success)
                    {
                        response.Data.TotalCount = priceNotification.Data;

                    }

                    response.Success = true;
                });
            }, string.Format(CacheKey.NotificationCount, _dbNameFinder.FindName(), _companyFinder.FindId()), 2);
        }

        public async Task<Response> Delete(string marketplaceRequestTypeId, string id)
        {
            return await ExceptionHandler.ResultHandleAsync<Response>(async response =>
            {

                //    var r = await this._priceNotificationService.DeleteAsync(new DeleteNotificationParameter
                //    {
                //        CompanyId = _companyFinder.FindId(),
                //        Brand = this._dbNameFinder.FindName(),
                //        Id = id
                //    });

                //    if (r.Success)
                //    {
                //        response.Success = true;
                //        response.Message = r.Message;
                //    }


                //if (response.Success)
                //{
                //    this._memoryCache.Remove(string.Format(CacheKey.Notifications, marketplaceRequestTypeId.ToLower(), _dbNameFinder.FindName(), _companyFinder.FindId()));
                //    this._memoryCache.Remove(string.Format(CacheKey.NotificationCount, _dbNameFinder.FindName(), _companyFinder.FindId()));
                //}

                return response;
            }, (r, e) =>
            {

            });
        }

        public async Task<Response> Ignore(int productInformationMarketplaceId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<PagedResponse<NotificationItem>> ReadAsync(GetParameter parameter)
        {
            return await ExceptionHandler.ResultHandleAsync<PagedResponse<NotificationItem>>(async response =>
            {
                response.Data = new List<NotificationItem>();


                var priceNotification = await this._notificationService.GetAsync(parameter);
                if (priceNotification.Success)
                {
                    response.RecordsCount = priceNotification.RecordsCount;

                    response.Data = priceNotification.Data.Select(pn => new NotificationItem
                    {
                        Id = pn.Id,
                        Barcode = pn.Barcode,
                        StockCode = pn.StockCode,
                        PhotoUrl = pn.PhotoUrl,
                        ProductInformationName = pn.ProductInformationName,
                        ProductId = pn.ProductId,
                        MarketplaceId = pn.MarketplaceId,
                        ProductInformationMarketplaceId = pn.ProductInformationMarketplaceId,
                        Message = pn.Message,
                        CreatedDate = pn.CreatedDateTime,
                        ProductInformationId = pn.ProductInformationId,
                        Title = pn.Title
                    }).ToList();
                }

                response.Success = true;
                return response;
            }, (r, e) =>
                {

                });

        }

        public DataResponse<List<KeyValue<string, string>>> NotificationTypes()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {


                var items = new List<KeyValue<string, string>>()
                {
                    new KeyValue<string, string>
                    {
                        Key="US",
                        Value="Stok"
                    },
                  new KeyValue<string, string>
                    {
                        Key="UP",
                        Value="Fiyat"
                    },
                  new KeyValue<string, string>
                    {
                        Key="CPR",
                        Value="Ürün Yükleme"
                    },
                  new KeyValue<string, string>
                    {
                        Key="UPR",
                        Value="Ürün Güncelleme"
                    }
                };

                response.Data = items;
                response.Success = true;
            });
        }

        public async Task<PagedResponse<NotificationItem>> Read(PagedRequest pagedRequest)
        {
            return await ExceptionHandler.ResultHandleAsync<PagedResponse<NotificationItem>>(async response =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                var parameter = new GetParameter();
                parameter.Page = pagedRequest.Page;
                parameter.RecordsCount = pagedRequest.PageRecordsCount;
                parameter.Brand = _dbNameFinder.FindName();
                parameter.CompanyId = _companyFinder.FindId();

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                        {
                            parameter.ProductInformationName = value;
                        }
                    }
                    else if (pLoop.StartsWith("StockCode:"))
                    {
                        var value = pLoop.Replace("StockCode:", "");
                        if (value.Length > 0)
                        {
                            parameter.Stockcode = value;
                        }
                    }
                    else if (pLoop.StartsWith("Barcode:"))
                    {
                        var value = pLoop.Replace("Barcode:", "");
                        if (value.Length > 0)
                        {
                            parameter.Barcode = value;
                        }
                    }
                    else if (pLoop.StartsWith("ModelCode:"))
                    {
                        var value = pLoop.Replace("ModelCode:", "");
                        if (value.Length > 0)
                        {
                            parameter.ModelCode = value;
                        }
                    }
                    else if (pLoop.StartsWith("MarketplaceId:"))
                    {
                        var value = pLoop.Replace("MarketplaceId:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            parameter.MarketplaceId = value;
                        }
                    }
                    else if (pLoop.StartsWith("Type:"))
                    {
                        var value = pLoop.Replace("Type:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            parameter.Type = value;
                        }
                    }
                }

                var notificationItem = await ReadAsync(parameter);
                if (notificationItem.Success)
                {

                    response.RecordsCount = notificationItem.RecordsCount;
                    response.Data = notificationItem.Data;
                }


                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;



                response.Success = true;
                return response;
            }, (response, exception) => { });
        }
    }
}
