﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ShowcaseService : IShowcaseService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IImageHelper _imageHelper;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public ShowcaseService(IUnitOfWork unitOfWork, IImageHelper imageHelper, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._imageHelper = imageHelper;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(Showcase dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                dto.RowComponents.ForEach(rc =>
                {
                    rc.ColumnComponents.ForEach(cc =>
                    {
                        cc.ProductComponents.ForEach(pc =>
                        {
                            var entity = this
                                ._unitOfWork
                                .ProductInformationRepository
                                .DbSet()
                                .Include(pi => pi.ProductInformationTranslations)
                                .Include(pi => pi.ProductInformationPriceses)
                                .Include(pi => pi.ProductInformationPhotos)
                                .Where(pi => pi.Id == pc.ProductInformationId)
                                .FirstOrDefault();

                            pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                            pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                            pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                            pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                            pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                            pc.Url = entity.ProductInformationTranslations[0].Url;
                            pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                            pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                        });

                        cc.BannerComponents.ForEach(bc =>
                        {
                            this.SaveBannerImage(bc);
                        });

                        cc.SliderComponents.ForEach(sc =>
                        {
                            sc.ProductComponents.ForEach(pc =>
                            {
                                var entity = this
                                    ._unitOfWork
                                    .ProductInformationRepository
                                    .DbSet()
                                    .Include(pi => pi.ProductInformationTranslations)
                                    .Include(pi => pi.ProductInformationPriceses)
                                    .Include(pi => pi.ProductInformationPhotos)
                                    .Where(pi => pi.Id == pc.ProductInformationId)
                                    .FirstOrDefault();

                                pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                                pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                                pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                                pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                                pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                                pc.Url = entity.ProductInformationTranslations[0].Url;
                                pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                                pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                            });

                            sc.BannerComponents.ForEach(bc =>
                            {
                                this.SaveBannerImage(bc);
                            });
                        });
                    });
                });

                response.Success = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .Create(new Domain.Entities.Showcase
                    {
                        Name = "Ana Sayfa",
                        BrandId = dto.BrandId,
                        CategoryId = dto.CategoryId,
                        CreatedDate = DateTime.Now,
                        LanguageId = "TR",
                        Json = JsonConvert.SerializeObject(dto.RowComponents, new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        })
                    });
                response.Message = "Vitrin oluşturuldu.";
            });
        }

        public DataResponse<Showcase> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Showcase>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .DbSet()
                    .Where(s => s.Id == id)
                    .Select(b => new Showcase
                    {
                        Id = b.Id,
                        Name = b.Name,
                        RowComponents = JsonConvert.DeserializeObject<List<RowComponent>>(b.Json)
                    })
                    .FirstOrDefault();
                response.Success = true;
            });
        }

        public PagedResponse<Showcase> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Showcase>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .DbSet()
                    .Where(s => string.IsNullOrEmpty(pagedRequest.Search) || s.Name.Contains(pagedRequest.Search))
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .DbSet()
                    .Where(s => string.IsNullOrEmpty(pagedRequest.Search) || s.Name.Contains(pagedRequest.Search))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Showcase
                    {
                        Id = b.Id,
                        Name = b.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .DbSet()
                    .AsNoTracking()
                    .Select(s => new KeyValue<int, string> { Key = s.Id, Value = s.Name })
                    .ToList();
                response.Success = true;
                response.Message = "Vitrin bilgileri başarılı bir şekilde getirildi.";
            });
        }

        public Response Update(Showcase dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                dto.RowComponents.ForEach(rc =>
                {
                    rc.ColumnComponents.ForEach(cc =>
                    {
                        cc.ProductComponents.ForEach(pc =>
                        {
                            var entity = this
                                ._unitOfWork
                                .ProductInformationRepository
                                .DbSet()
                                .Include(pi => pi.ProductInformationTranslations)
                                .Include(pi => pi.ProductInformationPriceses)
                                .Include(pi => pi.ProductInformationPhotos)
                                .Where(pi => pi.Id == pc.ProductInformationId)
                                .FirstOrDefault();

                            pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                            pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                            pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                            pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                            pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                            pc.Url = entity.ProductInformationTranslations[0].Url;
                            pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                            pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                        });

                        cc.BannerComponents.ForEach(bc =>
                        {
                            this.SaveBannerImage(bc);
                        });

                        cc.SliderComponents.ForEach(sc =>
                        {
                            sc.ProductComponents.ForEach(pc =>
                            {
                                var entity = this
                                    ._unitOfWork
                                    .ProductInformationRepository
                                    .DbSet()
                                    .Include(pi => pi.ProductInformationTranslations)
                                    .Include(pi => pi.ProductInformationPriceses)
                                     .Include(pi => pi.ProductInformationPhotos)
                                    .Where(pi => pi.Id == pc.ProductInformationId)
                                    .FirstOrDefault();

                                pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                                pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                                pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                                pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                                pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                                pc.Url = entity.ProductInformationTranslations[0].Url;
                                pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                                pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                            });

                            sc.BannerComponents.ForEach(bc =>
                            {
                                this.SaveBannerImage(bc);
                            });
                        });
                    });
                });

                response.Success = this
                    ._unitOfWork
                    .ShowcaseRepository
                    .Update(new Domain.Entities.Showcase
                    {
                        Id = dto.Id,
                        Name = "Ana Sayfa",
                        BrandId = dto.BrandId,
                        CategoryId = dto.CategoryId,
                        CreatedDate = DateTime.Now,
                        LanguageId = "TR",
                        Json = JsonConvert.SerializeObject(dto.RowComponents, new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        })
                    });
                response.Message = "Vitrin güncellendi.";
            });
        }
        #endregion

        #region Private Methods
        private void SaveBannerImage(BannerComponent bannerComponent)
        {
            if (bannerComponent.FileName.StartsWith("data"))
            {
                var directory = $@"{this._settingService.ShowcaseImagePath}\banner";

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                var fileName = $"{Guid.NewGuid()}.jpg";
                var filePath = $@"{directory}\{fileName}";

                this._imageHelper.Save(bannerComponent.FileName, filePath);

                bannerComponent.FileName = fileName;
            }
        }
        #endregion
    }
}
