﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class BrandService : IBrandService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public BrandService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Brand> Create(Brand dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Brand>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .BrandRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir marka oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Brand
                {
                    Name = dto.Name,
                    Active = dto.Active,
                    
                };

                var created = this
                    ._unitOfWork.BrandRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                entity.Url = $"{entity.Name.GenerateUrl()}-{entity.Id}-b";
                this._unitOfWork.BrandRepository.Update(entity);

                response.Data = dto;
                response.Success = true;
                response.Message = "Marka oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<Brand> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Brand>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .BrandRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Marka bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Brand
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Name = entity.Name,
                    Url = entity.Url
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .BrandRepository
                    .DbSet()
                    .Select(b => new KeyValue<int, string>
                    {
                        Key = b.Id,
                        Value = b.Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Brand> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Brand>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .BrandRepository
                    .DbSet()
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .BrandRepository
                    .DbSet()
                    .Where(b => string.IsNullOrEmpty(pagedRequest.Search) || b.Name.Contains(pagedRequest.Search))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Brand
                    {
                        Active = b.Active,
                        Id = b.Id,
                        Name = b.Name,
                        Url = b.Url
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Brand dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .BrandRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name && b.Id != dto.Id);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir marka oluşturulduğundan marka güncellenemez.";
                    return;
                }

                if (!dto.Active)
                {
                    var hasAnyProduct = this
                         ._unitOfWork
                         .BrandRepository
                         .DbSet()
                         .Any(b => b.Id == dto.Id && b.Products.Any());

                    if (hasAnyProduct)
                    {
                        response.Success = false;
                        response.Message = "Markaya ait ürün bulunduğundan bu marka pasif olarak güncellenemez.";
                        return;
                    }
                }

                var entity = this
                    ._unitOfWork
                    .BrandRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"{dto.Name} ({dto.Id}) markası bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;
                entity.Active = dto.Active;

                var updated = this
                    ._unitOfWork.BrandRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Marka güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}
