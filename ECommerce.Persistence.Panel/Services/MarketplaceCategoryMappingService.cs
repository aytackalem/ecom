﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceCategoryMappingService : IMarketplaceCategoryMappingService
    {
        #region Fields
        private readonly ICompanyFinder _companyFinder;

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceCategoryMappingService(ICompanyFinder companyFinder, IUnitOfWork unitOfWork)
        {
            #region Fields
            this._companyFinder = companyFinder;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<Application.Panel.DataTransferObjects.MarketplaceCategoryMapping>> Read(int categoryId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Panel.DataTransferObjects.MarketplaceCategoryMapping>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .MarketplaceCategoryMappingRepository
                    .DbSet()
                    .Include(x => x.MarketplaceCategoryVariantValueMappings)
                    .Where(mcm => mcm.CategoryId == categoryId && (mcm.CompanyId == _companyFinder.FindId() || mcm.CompanyId == null))
                    .Select(x => new Application.Panel.DataTransferObjects.MarketplaceCategoryMapping
                    {
                        Id = x.Id,
                        MarketplaceId = x.MarketplaceId,
                        CompanyId = x.CompanyId,
                        MarketplaceCategoryCode = x.MarketplaceCategoryCode,
                        MarketplaceCategoryName = x.MarketplaceCategoryName,
                        MarketplaceCategoryVariantValueMappings = x.MarketplaceCategoryVariantValueMappings.Select(mcv => new Application.Panel.DataTransferObjects.MarketplaceCategoryVariantValueMapping
                        {
                            AllowCustom = mcv.AllowCustom,
                            Mandatory = mcv.Mandatory,
                            MarketplaceId = x.MarketplaceId,
                            MarketplaceVariantCode = mcv.MarketplaceVariantCode,
                            MarketplaceVariantName = mcv.MarketplaceVariantName,
                            MarketplaceVariantValueCode = mcv.MarketplaceVariantValueCode,
                            MarketplaceVariantValueName = mcv.MarketplaceVariantValueName,
                            Multiple = mcv.Multiple,
                            Varianter = mcv.Varianter
                        }).ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public async Task<DataResponse<List<Application.Panel.DataTransferObjects.MarketplaceCategoryMapping>>> GetMappedAsync()
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<Application.Panel.DataTransferObjects.MarketplaceCategoryMapping>>>(async response =>
            {
                response.Data = await this
                    ._unitOfWork
                    .MarketplaceCategoryMappingRepository
                    .DbSet()
                    .Where(mcm => mcm.CompanyId == null || mcm.CompanyId == this._companyFinder.FindId())
                    .Select(mcm => new Application.Panel.DataTransferObjects.MarketplaceCategoryMapping
                    {
                        CompanyId = mcm.CompanyId,
                        MarketplaceId = mcm.MarketplaceId,
                        MarketplaceCategoryCode = mcm.MarketplaceCategoryCode
                    })
                    .ToListAsync();
                response.Success = true;
            });
        }

        public Response Upsert(UpsertRequest upsertRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var category = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Include(c => c.MarketplaceCategoryMappings)
                    .FirstOrDefault(c => c.Id == upsertRequest.CategoryId);

                var marketplaceBrandMappings = category
               .MarketplaceCategoryMappings
               .Where(x => x.CompanyId != this._companyFinder.FindId())
               .ToList();



                category.MarketplaceCategoryMappings = new();

                upsertRequest.MarketplaceCategoryMappings.ForEach(mcm =>
                {
                    if (string.IsNullOrEmpty(mcm.MarketplaceCategoryCode)) return;

                    marketplaceBrandMappings.Remove(marketplaceBrandMappings.FirstOrDefault(x => x.MarketplaceId == mcm.MarketplaceId && mcm.CompanyId == null));



                    category.MarketplaceCategoryMappings.Add(new Domain.Entities.MarketplaceCategoryMapping
                    {
                        CategoryId = upsertRequest.CategoryId,
                        MarketplaceId = mcm.MarketplaceId,
                        MarketplaceCategoryCode = mcm.MarketplaceCategoryCode,
                        MarketplaceCategoryName = mcm.MarketplaceCategoryName.Trim(),
                        CompanyId = mcm.CompanyId,
                        MarketplaceCategoryVariantValueMappings = mcm.MarketplaceCategoryVariantValueMappings.Select(pimvv => new Domain.Entities.MarketplaceCategoryVariantValueMapping
                        {
                            AllowCustom = pimvv.AllowCustom,
                            Mandatory = pimvv.Mandatory,
                            MarketplaceVariantCode = pimvv.MarketplaceVariantCode,
                            MarketplaceVariantName = pimvv.MarketplaceVariantName,
                            MarketplaceVariantValueCode = pimvv.MarketplaceVariantValueCode,
                            MarketplaceVariantValueName = pimvv.MarketplaceVariantValueName,
                            Multiple = pimvv.Multiple,
                            Varianter = pimvv.Varianter
                        }).ToList()
                    });
                });
                category.MarketplaceCategoryMappings.AddRange(marketplaceBrandMappings);



                this._unitOfWork.CategoryRepository.Update(category);

                response.Success = true;
                response.Message = "Kayıtlar başarılı bir şekilde güncellendi.";
            });
        }
        #endregion
    }
}