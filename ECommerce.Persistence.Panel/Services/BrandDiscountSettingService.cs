﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class BrandDiscountSettingService : IBrandDiscountSettingService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public BrandDiscountSettingService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<BrandDiscountSetting> Create(BrandDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<BrandDiscountSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");
                var entity = new Domain.Entities.BrandDiscountSetting
                {
                    Active = dto.Active,
                    StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo),
                    EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo),
                    ApplicationId = dto.ApplicationId,
                    Discount = dto.Discount,
                    DiscountIsRate = dto.DiscountIsRate,
                    CreatedDate = DateTime.Now,
                    BrandId = dto.BrandId
                };

                var created = this
                    ._unitOfWork
                    .BrandDiscountSettingRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Success = true;
                response.Message = "Marka indirimi oluşturuldu.";
            }, (response, exception) => { });
        }

        public DataResponse<BrandDiscountSetting> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<BrandDiscountSetting>>((response) =>
            {
                var entity = this.ReadEntity(id);

                if (entity == null)
                {
                    response.Message = $"Marka indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                var dto = new BrandDiscountSetting
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    EndDate = entity.EndDate,
                    StartDate = entity.StartDate,
                    StartDateString = entity.StartDate.ToString("dd/MM/yyy", cultureinfo),
                    EndDateString = entity.EndDate.ToString("dd/MM/yyy", cultureinfo),
                    ApplicationId = entity.ApplicationId,
                    Discount = entity.Discount,
                    DiscountIsRate = entity.DiscountIsRate,
                    BrandId = entity.BrandId,
                    Brand = new Brand
                    {
                        Name = entity.Brand.Name
                    }
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<BrandDiscountSetting> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<BrandDiscountSetting>>((response) =>
            {
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .BrandDiscountSettingRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .BrandDiscountSettingRepository
                    .DbSet()
                    .Where(x => !x.Deleted)
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Include(x => x.Brand)
                    .Select(x => new BrandDiscountSetting
                    {
                        Id = x.Id,
                        Active = x.Active,
                        EndDate = x.EndDate,
                        StartDate = x.StartDate,
                        ApplicationId = x.ApplicationId,
                        Discount = x.Discount,
                        DiscountIsRate = x.DiscountIsRate,
                        BrandId = x.BrandId,
                        StartDateString = x.StartDate.ToString("dd/MM/yyy", cultureinfo),
                        EndDateString = x.EndDate.ToString("dd/MM/yyy", cultureinfo),
                        Brand = new Brand
                        {
                            Name = x.Brand.Name
                        }
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(BrandDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this.ReadEntity(dto.Id);

                if (entity == null)
                {
                    response.Message = $"Marka indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }
                var cultureinfo = new System.Globalization.CultureInfo("en-US");

                entity.StartDate = DateTime.ParseExact(dto.StartDateString, "dd/MM/yyyy", cultureinfo);
                entity.EndDate = DateTime.ParseExact(dto.EndDateString, "dd/MM/yyyy", cultureinfo);
                entity.ApplicationId = dto.ApplicationId;
                entity.Active = dto.Active;
                entity.Discount = dto.Discount;
                entity.DiscountIsRate = dto.DiscountIsRate;
                entity.BrandId = dto.BrandId;

                var updated = this
                    ._unitOfWork
                    .BrandDiscountSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Marka indirimi güncellendi.";
            }, (response, exception) => { });
        }

        public Response Delete(BrandDiscountSetting dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var entity = this
                    ._unitOfWork
                    .BrandDiscountSettingRepository
                    .Read(dto.Id);


                if (entity == null)
                {
                    response.Message = $"Marka indirimi bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Deleted = true;
                entity.DeletedDateTime = DateTime.Now;
                var updated = this
                    ._unitOfWork.BrandDiscountSettingRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Marka indirimi silindi.";
            }, (response, exception) => { });
        }
        #endregion

        #region Private Methods
        private Domain.Entities.BrandDiscountSetting ReadEntity(int id)
        {
            return this
                ._unitOfWork
                .BrandDiscountSettingRepository
                .DbSet()
                .Where(x => x.Id == id)
                .Include(x => x.Brand)
                .FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}
