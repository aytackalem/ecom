﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceBrandService : IMarketplaceBrandService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpHelper _httpHelper;

        private readonly Application.Common.Interfaces.Marketplaces.IMarketplaceService _marketplaceService;
        #endregion

        #region Constructors
        public MarketplaceBrandService(IUnitOfWork unitOfWork, IHttpHelper httpHelper, Application.Common.Interfaces.Marketplaces.IMarketplaceService marketplaceService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            this._marketplaceService = marketplaceService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(string marketplaceId, string q)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {

                var brandResponse = this._marketplaceService.BrandReadAsKeyValue(q, marketplaceId);
                if (brandResponse.Success)
                {
                    response.Data = brandResponse.Data;
                    response.Success = true;
                }
            });
        }
        #endregion
    }
}