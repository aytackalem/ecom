﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Linq;
using System;
using DocumentFormat.OpenXml.Office2010.Excel;
using ECommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using ECommerce.Application.Common.Interfaces.Services;
using System.IO.Pipelines;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;

namespace ECommerce.Persistence.Panel.Services
{
    public class ReceiptService : IReceiptService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public ReceiptService(IUnitOfWork unitOfWork, ISettingService settingService)
        {
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
        }
        #endregion

        #region Methods
        public PagedResponse<Application.Panel.DataTransferObjects.Receipt> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Application.Panel.DataTransferObjects.Receipt>>((response) =>
            {
                var parameters = pagedRequest.Search?.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .ReceiptRepository
                    .DbSet()
                    .Where(r => r.Cancelled == false)
                    .AsQueryable();

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("SellerCode:"))
                    {
                        var value = pLoop.Replace("SellerCode:", "");
                        if (value.Length > 0)
                        {
                            iQueryable = iQueryable.Where(iq => iq.SellerCode.Contains(value));
                        }
                    }
                    else if (pLoop.StartsWith("SupplierId:"))
                    {
                        var value = pLoop.Replace("SupplierId:", "");
                        if (value.Length > 0)
                        {
                            var supplierId = int.Parse(value);
                            iQueryable = iQueryable.Where(iq => iq.SupplierId == supplierId);
                        }
                    }
                    else if (pLoop.StartsWith("Delivered:"))
                    {
                        var value = pLoop.Replace("Delivered:", "");
                        if (value.Length > 0)
                        {
                            var delivered = value == "1";
                            iQueryable = iQueryable.Where(iq => iq.Delivered == delivered);
                        }
                    }
                    else if (pLoop.StartsWith("DeadlineType:"))
                    {
                        var value = pLoop.Replace("DeadlineType:", "");
                        if (value.Length > 0)
                        {
                            if (value == "15")
                            {
                                iQueryable = iQueryable.Where(iq => iq.Deadline <= DateTime.Now.AddDays(15).Date);
                            }
                            else if (value == "7")
                            {
                                iQueryable = iQueryable.Where(iq => iq.Deadline <= DateTime.Now.AddDays(7).Date);
                            }
                            else if (value == "3")
                            {
                                iQueryable = iQueryable.Where(iq => iq.Deadline <= DateTime.Now.AddDays(3).Date);
                            }
                            else if (value == "-1")
                            {
                                iQueryable = iQueryable.Where(iq => iq.Deadline <= DateTime.Now.Date);
                            }
                        }
                    }
                }

                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable.Count();

                if (!string.IsNullOrEmpty(pagedRequest.Sort))
                {
                    if (pagedRequest.Sort == "Id")
                        iQueryable = iQueryable.OrderByDescending(o => o.Id);
                    else if (pagedRequest.Sort == "CreatedDateAsc")
                        iQueryable = iQueryable.OrderBy(o => o.CreatedDate);
                    else if (pagedRequest.Sort == "CreatedDateDesc")
                        iQueryable = iQueryable.OrderByDescending(o => o.CreatedDate);
                    else if (pagedRequest.Sort == "DeadlineAsc")
                        iQueryable = iQueryable.OrderBy(o => o.CreatedDate);
                    else if (pagedRequest.Sort == "DeadlineDesc")
                        iQueryable = iQueryable.OrderByDescending(o => o.CreatedDate);
                }

                response.Data = iQueryable
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(o => new Application.Panel.DataTransferObjects.Receipt
                    {
                        Id = o.Id,
                        SellerCode = o.SellerCode,
                        SupplierName = o.Supplier.Name,
                        Deadline = o.Deadline,
                        CreatedDate = o.CreatedDate,
                        TotalCount = o.ReceiptItems.Sum(ri => ri.Quantity),
                        Delivered = o.Delivered,
                        FileName = $"{this._settingService.ImageUrl}/product/{o.ReceiptItems.FirstOrDefault().ProductInformation.ProductInformationPhotos.FirstOrDefault().FileName}"
                    })
                    .ToList();
                response.Success = true;
            });
        }

        public DataResponse<Application.Panel.DataTransferObjects.Receipt> Create(Application.Panel.DataTransferObjects.Receipt dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Application.Panel.DataTransferObjects.Receipt>>((response) =>
            {
                var receipt = new Domain.Entities.Receipt
                {
                    Card = dto.Card,
                    CreatedDate = DateTime.Now,
                    Deadline = DateTime.Now.AddDays(dto.DeadlineDays).Date,
                    Hanger = dto.Hanger,
                    Note = dto.Note,
                    Rubber = dto.Rubber,
                    SellerCode = dto.SellerCode,
                    SupplierId = dto.SupplierId,
                    Ticket = dto.Ticket,
                    Wash = dto.Wash,
                    UnitMeter = dto.UnitMeter,
                    ReceiptItems = dto.ReceiptItems.Select(ri => new Domain.Entities.ReceiptItem
                    {
                        ProductInformationId = ri.ProductInformationId,
                        Quantity = ri.Quantity
                    }).ToList()
                };

                response.Success = this._unitOfWork.ReceiptRepository.Create(receipt);
            });
        }

        public Response Update(Application.Panel.DataTransferObjects.Receipt dto)
        {
            throw new NotImplementedException();
        }

        public DataResponse<Application.Panel.DataTransferObjects.Receipt> ReadDetail(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Application.Panel.DataTransferObjects.Receipt>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .ReceiptRepository
                    .DbSet()
                    .Where(r => r.Cancelled == false && r.Id == id)
                    .Select(r => new Application.Panel.DataTransferObjects.Receipt
                    {
                        Id = r.Id,
                        Card = r.Card,
                        CreatedDate = r.CreatedDate,
                        Deadline = r.Deadline,
                        Hanger = r.Hanger,
                        Note = r.Note,
                        Rubber = r.Rubber,
                        SellerCode = r.SellerCode,
                        SupplierId = r.SupplierId,
                        Ticket = r.Ticket,
                        UnitMeter = r.UnitMeter,
                        Wash = r.Wash,
                        SupplierName = r.Supplier.Name,
                        ReceiptItems = r.ReceiptItems.Select(ri => new Application.Panel.DataTransferObjects.ReceiptItem
                        {
                            ProductInformationId = ri.ProductInformationId,
                            Quantity = ri.Quantity,
                            ProductInformationName = $"{ri.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name} {ri.ProductInformation.ProductInformationTranslations.FirstOrDefault().VariantValuesDescription}",
                            ProductInformationPhoto = $"{this._settingService.ProductImageUrl}/{ri.ProductInformation.ProductInformationPhotos.FirstOrDefault().FileName}"
                        }).ToList()
                    })
                    .FirstOrDefault();
                response.Success = true;
            });
        }

        public Response UpdateDelivered(int id, bool delivered)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var receipt = this
                    ._unitOfWork
                    .ReceiptRepository
                    .DbSet()
                    .FirstOrDefault(r => r.Id == id);

                receipt.Delivered = delivered;

                response.Success = this
                    ._unitOfWork
                    .ReceiptRepository
                    .Update(receipt);
                response.Message = "Teslim edildi durumu bşarılı bir şekilde güncellendi.";
            });
        }

        public Response UpdateCancelled(int id)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var receipt = this
                    ._unitOfWork
                    .ReceiptRepository
                    .DbSet()
                    .FirstOrDefault(r => r.Id == id);

                receipt.Cancelled = true;

                response.Success = this
                    ._unitOfWork
                    .ReceiptRepository
                    .Update(receipt);
                response.Message = "Kayıt bşarılı bir şekilde silindi.";
            });
        }

        public DataResponse<List<ReceiptProductInformationQuantity>> ProductInformationQuantity(int productId)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<ReceiptProductInformationQuantity>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    response.Data = sqlConnection
                        .Query<ReceiptProductInformationQuantity>(@"
Select		PF.Id,
			S.Name,
			Sum(RI.Quantity) As Quantity
		
From		ProductInformations As PF With(NoLock)

Join		ReceiptItems As RI With(NoLock)
On			PF.Id = RI.ProductInformationId

Join		Receipts As R With(NoLock)
On			RI.ReceiptId = R.Id
            And R.Cancelled = 0
            And R.Delivered = 0

Join		Suppliers As S With(NoLock)
On			R.SupplierId = S.Id

Where		PF.ProductId = @ProductId

Group By	PF.Id,
			S.Name", new
                        {
                            ProductId = productId
                        })
                        .ToList();
                }

                response.Success = true;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }
        #endregion
    }
}
