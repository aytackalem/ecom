﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class ProductSourceDomainService : IProductSourceDomainService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ProductSourceDomainService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {


                var productSourceDomains = this
                   ._unitOfWork
                   .ProductSourceDomainRepository
                   .DbSet()
                   .Select(b => new KeyValue<int, string>
                   {
                       Key = b.Id,
                       Value = b.Name
                   })
                   .ToList();

                productSourceDomains.Add(new KeyValue<int, string>
                {
                    Key = 0,
                    Value = "Helpy"
                });
                productSourceDomains.Reverse();
                response.Data = productSourceDomains;
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<ProductSourceDomain> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductSourceDomain>>((response) =>
            {
                var entity = this
                   ._unitOfWork
                   .ProductSourceDomainRepository
                   .DbSet()
                   .Include(psc => psc.ProductSourceConfigurations)
                   .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Xml sağlayıcısı bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new ProductSourceDomain
                {
                    Id = entity.Id,
                    Active = entity.Active,
                    Name = entity.Name,
                    ProductSourceConfigurations = entity.ProductSourceConfigurations.Select(psc => new ProductSourceConfiguration
                    {
                        Id=psc.Id,
                        Key = psc.Key,
                        Value = psc.Value,
                        Name = psc.Name
                    }).ToList(),
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<ProductSourceDomain> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductSourceDomain>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .ProductSourceDomainRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .ProductSourceDomainRepository
                    .DbSet()
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new ProductSourceDomain
                    {
                        Active = b.Active,
                        Id = b.Id,
                        Name = b.Name
                    }).ToList();


                response.Success = true;
            }, (response, exception) => { });
        }


        public Response Update(ProductSourceDomain dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductSourceDomainRepository
                    .DbSet()
                    .Include(x => x.ProductSourceConfigurations)
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Xml sağlayıcısı bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.Name = dto.Name;
                if (dto.ProductSourceConfigurations != null)
                    dto.ProductSourceConfigurations.ForEach(mc =>
                    {
                        var marketplaceConfiguration = entity.ProductSourceConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                        if (marketplaceConfiguration != null)
                        {
                            marketplaceConfiguration.Value = mc.Value;
                        }
                    });

                var updated = this
                    ._unitOfWork.ProductSourceDomainRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Xml sağlayıcısı güncellendi.";
            }, (response, exception) => { });
        }

        #endregion
    }
}
