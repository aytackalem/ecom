﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects.Widgets;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Application.Panel.Parameters.Orders;
using ECommerce.Application.Panel.Parameters.ProductPrices.Extensions;
using ECommerce.Application.Panel.ViewModels.Orders;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Zen.Barcode;

namespace ECommerce.Persistence.Panel.Services
{
    public class OrderService : IOrderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISettingService _settingService;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IExcelHelper _excelHelper;
        #endregion

        #region Constructors
        public OrderService(ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IUnitOfWork unitOfWork, ISettingService settingService, IHttpContextAccessor httpContextAccessor, IExcelHelper excelHelper)
        {
            #region Fields
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._httpContextAccessor = httpContextAccessor;
            this._excelHelper = excelHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Order> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Order>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .OrderRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Sipariş bulunamadı.";
                    response.Success = false;
                    return;
                }

                var recipient = entity.OrderDeliveryAddress.Recipient.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                var photos = new List<ProductInformationPhoto>()
                {
                new ProductInformationPhoto
                {
                    FileName="/img/product/0/no-image.jpg"
            }
                };

                var dto = new Order
                {
                    Id = entity.Id,
                    MarketplaceId = entity.MarketplaceId,
                    Amount = entity.Total,
                    CurrencyId = entity.CurrencyId,
                    CreatedDate = entity.CreatedDate,
                    OrderDate = entity.OrderDate,
                    OrderTypeId = entity.OrderTypeId,
                    OrderSourceId = entity.OrderSourceId,
                    CargoFee = entity.CargoFee,
                    CommissionAmount = entity.CommissionAmount,
                    ApplicationId = entity.ApplicationId,
                    Discount = entity.Discount,
                    CustomerId = entity.CustomerId,
                    OrderSource = new OrderSource
                    {
                        Id = entity.OrderSourceId,
                        Name = entity.OrderSource.Name
                    },
                    OrderType = new OrderType
                    {

                        Name = entity.OrderType.OrderTypeTranslations.FirstOrDefault(ott => ott.LanguageId == "TR").Name,
                        OrderTypeName = entity.OrderType.OrderTypeTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name
                    },
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        FirstName = recipient.Length > 0 ? recipient[0] : "",
                        LastName = recipient.Length > 1 ? recipient[1] : "",
                        NeighborhoodId = entity.OrderDeliveryAddress.NeighborhoodId,
                        Address = entity.OrderDeliveryAddress.Address,
                        PhoneNumber = entity.OrderDeliveryAddress.PhoneNumber,
                        Neighborhood = new Neighborhood
                        {
                            Id = entity.OrderDeliveryAddress.NeighborhoodId,
                            Name = entity.OrderDeliveryAddress.Neighborhood.Name,
                            District = new District
                            {
                                Id = entity.OrderDeliveryAddress.Neighborhood.DistrictId,
                                Name = entity.OrderDeliveryAddress.Neighborhood.District.Name,
                                City = new City
                                {
                                    Id = entity.OrderDeliveryAddress.Neighborhood.District.CityId,
                                    Name = entity.OrderDeliveryAddress.Neighborhood.District.City.Name,
                                    Country = new Country
                                    {
                                        Id = entity.OrderDeliveryAddress.Neighborhood.District.City.CountryId,
                                        Name = entity.OrderDeliveryAddress.Neighborhood.District.City.Country.Name
                                    }
                                }
                            }
                        }
                    },
                    OrderInvoiceInformation = new OrderInvoiceInformation
                    {
                        Address = entity.OrderInvoiceInformation.Address,
                        Mail = entity.OrderInvoiceInformation.Mail,
                        Phone = entity.OrderInvoiceInformation.Phone,
                        TaxNumber = entity.OrderInvoiceInformation.TaxNumber,
                        TaxOffice = entity.OrderInvoiceInformation.TaxOffice,
                        FirstName = entity.OrderInvoiceInformation.FirstName,
                        LastName = entity.OrderInvoiceInformation.LastName,
                        NeighborhoodId = entity.OrderInvoiceInformation.NeighborhoodId,
                        Neighborhood = new Neighborhood
                        {
                            Id = entity.OrderInvoiceInformation.NeighborhoodId,
                            Name = entity.OrderInvoiceInformation.Neighborhood.Name,
                            District = new District
                            {
                                Id = entity.OrderInvoiceInformation.Neighborhood.DistrictId,
                                Name = entity.OrderInvoiceInformation.Neighborhood.District.Name,
                                City = new City
                                {
                                    Id = entity.OrderInvoiceInformation.Neighborhood.District.CityId,
                                    Name = entity.OrderInvoiceInformation.Neighborhood.District.City.Name,
                                    Country = new Country
                                    {
                                        Id = entity.OrderInvoiceInformation.Neighborhood.District.City.CountryId,
                                        Name = entity.OrderInvoiceInformation.Neighborhood.District.City.Country.Name
                                    }
                                }
                            }
                        }
                    },
                    Customer = new Customer
                    {
                        Name = entity.Customer.Name,
                        Surname = entity.Customer.Surname,
                        CustomerContact = new CustomerContact
                        {
                            Phone = entity.Customer.CustomerContact.Phone,
                            Mail = entity.Customer.CustomerContact.Mail,
                            TaxNumber = entity.Customer.CustomerContact.TaxNumber,
                            TaxOffice = entity.Customer.CustomerContact.TaxOffice,

                        }
                    },
                    OrderNotes = entity.OrderNotes.Select(c => new OrderNote
                    {
                        Note = c.Note
                    }).ToList(),
                    OrderShipments = entity.OrderShipments.Select(x => new OrderShipment
                    {
                        TrackingUrl = x.TrackingUrl,
                        TrackingCode = x.TrackingCode,
                        Payor = x.Payor,
                        ShipmentCompanyId = x.ShipmentCompanyId,
                        ShipmentCompany = new ShipmentCompany
                        {
                            Name = x.ShipmentCompany.Name
                        }
                    }).ToList(),
                    OrderDetails = entity.OrderDetails.Select(x => new OrderDetail
                    {
                        Id = x.Id,
                        UnitPrice = x.UnitPrice,
                        ListUnitPrice = x.ListUnitPrice,
                        Quantity = x.Quantity,
                        Payor = x.Payor,
                        UnitCost = x.UnitCost,
                        VatRate = x.VatRate,
                        ProductInformationId = x.ProductInformationId,
                        //TypeName = x.Type.ToDescriptionString(),
                        ReturnDate = x.ReturnDate,
                        ReturnDescription = x.ReturnDescription,
                        ReturnNumber = x.ReturnNumber,
                        ProductInformation = new ProductInformation
                        {
                            ProductInformationName = $"{x.ProductInformation.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name} {(x.ProductInformation.ProductInformationVariants?.Count == 0 ? "" : $"({string.Join(",", x.ProductInformation.ProductInformationVariants.Select(piv => piv.VariantValue.VariantValueTranslations.FirstOrDefault(pit => pit.LanguageId == "TR").Value))})")}",
                            ProductInformationPhotos =
                            x
                                .ProductInformation
                                .ProductInformationPhotos.Count > 0 ?
                            x
                                .ProductInformation
                                .ProductInformationPhotos
                                .Select(pip => new ProductInformationPhoto
                                {
                                    FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                }).ToList() :
                                photos
                        }
                    }).ToList(),
                    Payments = entity.Payments.Select(x => new Payment
                    {
                        Id = x.Id,
                        Amount = x.Amount,
                        PaymentTypeId = x.PaymentTypeId,
                        PaymentType = new PaymentType
                        {
                            PaymentTypeName = x.PaymentType.PaymentTypeTranslations.FirstOrDefault(y => y.LanguageId == this._settingService.DefaultLanguageId).Name
                        },
                        Paid = x.Paid
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<Application.Common.DataTransferObjects.KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ECommerce.Application.Common.DataTransferObjects.KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Select(b => new ECommerce.Application.Common.DataTransferObjects.KeyValue<int, string>
                    {
                        Key = b.Id,
                        Value = b.Id.ToString()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Order> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Order>>((response) =>
            {
                var parameters = pagedRequest.Search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

                var iQueryable = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsQueryable();

                var anyParameterDate = true;

                    foreach (var pLoop in parameters)
                    {
                        if (pLoop.StartsWith("Id:"))
                        {
                            var value = pLoop.Replace("Id:", "");
                            if (value.Length > 0)
                            {
                                anyParameterDate = false;
                                var id = int.Parse(value);
                                iQueryable = iQueryable.Where(iq => iq.Id == id);
                            }
                        }
                        else if (pLoop.StartsWith("Name:"))
                        {
                            var value = pLoop.Replace("Name:", "");
                            if (value.Length > 0)
                            {
                                anyParameterDate = false;
                                iQueryable = iQueryable.Where(iq => iq.Customer.Name.TrimEnd().Contains(value.ToUpper()) || iq.Customer.Name.TrimEnd().Contains(value.ToUpper() + " "));
                            }

                        }
                        else if (pLoop.StartsWith("Surname:"))
                        {
                            var value = pLoop.Replace("Surname:", "");
                            if (value.Length > 0)
                            {
                                anyParameterDate = false;
                                iQueryable = iQueryable.Where(iq => iq.Customer.Surname.TrimEnd().Contains(value.ToUpper()) || iq.Customer.Surname.TrimEnd().Contains(value.ToUpper() + " "));
                            }

                        }
                        else if (pLoop.StartsWith("Marketplace:"))
                        {
                            var value = pLoop.Replace("Marketplace:", "");
                            if (value.Length > 0 && value != "-1")
                                iQueryable = iQueryable.Where(iq => iq.MarketplaceId == value);
                        }
                        else if (pLoop.StartsWith("ShipmentCompany:"))
                        {
                            var value = pLoop.Replace("ShipmentCompany:", "");
                            if (value.Length > 0 && value != "-1")
                                iQueryable = iQueryable.Where(iq => iq.OrderShipments.Any(os => os.ShipmentCompanyId == value));
                        }
                        else if (pLoop.StartsWith("OrderType:"))
                        {
                            var value = pLoop.Replace("OrderType:", "");
                            if (value.Length > 0)
                            {
                                var values = value.Split(",", StringSplitOptions.RemoveEmptyEntries);
                                iQueryable = iQueryable.Where(iq => values.Any(x => x == iq.OrderTypeId));
                            }
                        }
                        else if (pLoop.StartsWith("MarketplaceOrderNumber:"))
                        {
                            var value = pLoop.Replace("MarketplaceOrderNumber:", "");
                            if (value.Length > 0 && value != "-1")
                            {
                                anyParameterDate = false;
                                iQueryable = iQueryable.Where(iq => iq.MarketplaceOrderNumber == value);

                            }
                        }
                        else if (pLoop.StartsWith("CargoTrackingNumber:"))
                        {
                            var value = pLoop.Replace("CargoTrackingNumber:", "");
                            if (value.Length > 0 && value != "-1")
                            {
                                anyParameterDate = false;
                                iQueryable = iQueryable.Where(iq => iq.OrderShipments.Any(os => os.TrackingCode == value));

                            }
                        }
                        else if (pLoop.StartsWith("OrderSource:"))
                        {
                            var value = pLoop.Replace("OrderSource:", "");
                            if (value.Length > 0 && value != "-1")
                            {
                                var orderSourceId = int.Parse(value);
                                iQueryable = iQueryable.Where(iq => iq.OrderSourceId == orderSourceId);
                            }
                        }
                        else if (pLoop.StartsWith("PhoneNumber:"))
                        {
                            var value = pLoop.Replace("PhoneNumber:", "");
                            if (value.Length > 0 && value != "-1")
                            {
                                anyParameterDate = false;
                                iQueryable = iQueryable.Where(iq => iq.Customer.CustomerContact.Phone == value ||
                                                                    iq.OrderDeliveryAddress.PhoneNumber == value ||
                                                                    iq.OrderInvoiceInformation.Phone == value);
                            }
                        }
                        else if (pLoop.StartsWith("Date:"))
                        {
                            var value = pLoop.Replace("Date:", "");
                            if (!string.IsNullOrEmpty(value) && anyParameterDate)
                            {
                                var dates = value.Split(new string[] { "/" }, System.StringSplitOptions.RemoveEmptyEntries);
                                var startDate = DateTime.Parse(dates[0].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                                var endDate = DateTime.Parse(dates[1].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                                iQueryable = iQueryable.Where(iq => iq.OrderDate.Date >= startDate && iq.OrderDate.Date <= endDate.Date);
                            }
                        }
                        else if (pLoop.StartsWith("ShoppingNumber"))
                        {
                            var value = pLoop.Replace("ShoppingNumber:", "");
                            if (!string.IsNullOrEmpty(value) && value != "-1")
                            {
                                iQueryable = iQueryable.Where(iq => iq.OrderPicking.PackerBarcode == value);
                            }
                        }
                        else if (pLoop.StartsWith("StockCode:"))
                        {
                            var value = pLoop.Replace("StockCode:", "");
                            if (value.Length > 0)
                                iQueryable = iQueryable.Where(iq => iq.OrderDetails.Any(od => od.ProductInformation.StockCode.Contains(value)));
                        }
                        else if (pLoop.StartsWith("Barcode:"))
                        {
                            var value = pLoop.Replace("Barcode:", "");
                            if (value.Length > 0)
                                iQueryable = iQueryable.Where(iq => iq.OrderDetails.Any(od => od.ProductInformation.Barcode.Contains(value)));
                        }
                        else if (pLoop.StartsWith("ModelCode:"))
                        {
                            var value = pLoop.Replace("ModelCode:", "");
                            if (value.Length > 0)
                                iQueryable = iQueryable.Where(iq => iq.OrderDetails.Any(od => od.ProductInformation.Product.SellerCode.Contains(value)));
                        }
                        else if (pLoop.StartsWith("OrderPrepation:"))
                        {
                            var value = pLoop.Replace("OrderPrepation:", "");
                            if (value.Length > 0 && value != "-1")
                            {
                                if (value == "1") //Tekli
                                {
                                    iQueryable = iQueryable.Where(x => x.OrderDetails.Count(y => y.Quantity == 1) == 1);
                                }
                                else if (value == "2") //Çoklu
                                {
                                    iQueryable = iQueryable.Where(x => x.OrderDetails.Count(y => y.Quantity > 1) == 1 || x.OrderDetails.Count > 1);
                                }

                            }
                        }
                        else if (pLoop.StartsWith("Invoiced:"))
                        {
                            var value = pLoop.Replace("Invoiced:", "");
                            if (value == "true" || value == "false")
                            {
                                var invoiced = bool.Parse(value);
                                if (invoiced)
                                    iQueryable = iQueryable.Where(iq => iq.OrderBilling != null);
                                else
                                    iQueryable = iQueryable.Where(iq => iq.OrderBilling == null);
                            }
                        }
                    }
               
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = iQueryable
                    .Count();

                if (!string.IsNullOrEmpty(pagedRequest.Sort))
                {
                    if (pagedRequest.Sort == "Id")
                        iQueryable = iQueryable.OrderByDescending(o => o.Id);
                    else if (pagedRequest.Sort == "OrderDateAsc")
                        iQueryable = iQueryable.OrderBy(o => o.OrderDate);
                    else if (pagedRequest.Sort == "OrderDateDesc")
                        iQueryable = iQueryable.OrderByDescending(o => o.OrderDate);
                }


                var photos = new List<ProductInformationPhoto>()
                {
                    new ProductInformationPhoto
                    {
                        FileName="/img/product/0/no-image.jpg"
                    }
                };

                response.Data = iQueryable
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(o => new Order
                    {
                        Id = o.Id,
                        PackerBarcode = o.OrderPicking.PackerBarcode,
                        MarketplaceId = o.MarketplaceId,
                        MarketplaceOrderNumber = o.MarketplaceOrderNumber,
                        Amount = o.Total,
                        CargoFee = o.CargoFee,
                        CommissionAmount = o.CommissionAmount,
                        CurrencyId = o.CurrencyId,
                        OrderDate = o.OrderDate,
                        CreatedDate = o.CreatedDate,
                        OrderTypeId = o.OrderTypeId,
                        ListTotal = o.ListTotal,
                        OrderType = new OrderType
                        {
                            Name = o.OrderType.OrderTypeTranslations.FirstOrDefault(ott => ott.LanguageId == "TR").Name,
                            OrderTypeName = o.OrderType.OrderTypeTranslations.FirstOrDefault(y => y.LanguageId == "TR").Name
                        },
                        MutualBarcode = o.OrderShipments.Any(os => !string.IsNullOrEmpty(os.TrackingBase64)),
                        OrderShipments = o
                            .OrderShipments
                            .Select(os => new OrderShipment
                            {
                                PackageNumber = os.PackageNumber,
                                ShipmentCompanyId = os.ShipmentCompanyId,
                                TrackingCode = os.TrackingCode
                            })
                            .ToList(),
                        Customer = new Customer
                        {
                            Name = o.Customer.Name,
                            Surname = o.Customer.Surname
                        },
                        OrderSourceId = o.OrderSourceId,
                        OrderSource = new OrderSource
                        {
                            Id = o.OrderSourceId,
                            Name = o.OrderSource.Name
                        },
                        OrderDetails = o
                            .OrderDetails
                            .Select(od => new OrderDetail
                            {
                                MarketplaceCommission = new MarketplaceCommission
                                {
                                    Rate = od.MarketplaceCommissions.Count > 0 ? od.MarketplaceCommissions.FirstOrDefault().Rate : 0M
                                },
                                UnitCommissionAmount = od.UnitCommissionAmount,
                                UnitPrice = od.UnitPrice,
                                Quantity = od.Quantity,
                                Payor = od.Payor,
                                VatRate = od.VatRate,
                                Type = (int)od.Type,
                                //TypeName = od.Type.ToDescriptionString(),
                                ReturnDate = od.ReturnDate,
                                ReturnDescription = od.ReturnDescription,
                                ReturnNumber = od.ReturnNumber,
                                ProductInformation = new ProductInformation
                                {
                                    Stock = od.ProductInformation.Stock,
                                    Barcode = od.ProductInformation.Barcode,
                                    StockCode = od.ProductInformation.StockCode,
                                    ProductInformationMarketplaces = od.ProductInformation.ProductInformationMarketplaces
                                    .Where(pim => pim.Opened && pim.MarketplaceId == o.MarketplaceId && pim.CompanyId == o.CompanyId).Select(pim => new ProductInformationMarketplace
                                    {
                                        Url = pim.Url,
                                    }).ToList(),
                                    ProductInformationName = $"{od
                                        .ProductInformation
                                        .ProductInformationTranslations
                                        .FirstOrDefault(pit => pit.LanguageId == "TR")
                                        .Name} {od
                                        .ProductInformation
                                        .ProductInformationTranslations
                                        .FirstOrDefault(pit => pit.LanguageId == "TR")
                                        .VariantValuesDescription}",
                                    ProductInformationPhotos = od
                                                .ProductInformation
                                                .ProductInformationPhotos
                                                .Where(pip => !pip.Deleted).Count() > 0 ? od
                                        .ProductInformation
                                        .ProductInformationPhotos
                                        .Where(pip => !pip.Deleted)
                                        .Select(pip => new ProductInformationPhoto
                                        {
                                            FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                        })
                                        .ToList() : photos
                                }
                            })
                            .ToList(),
                        OrderReturnDetails = o
                            .OrderReturnDetails
                            .Select(ord => new OrderReturnDetail
                            {
                                Id = ord.Id
                            })
                            .ToList(),
                        OrderPacking = o.OrderPacking == null ? null : new OrderPacking
                        {
                            CreatedDate = o.OrderPacking.CreatedDate,
                            PackedUsername = o.OrderPacking.PackedUsername
                        },
                        OrderPicking = o.OrderPicking == null ? null : new OrderPicking
                        {
                            CreatedDate = o.OrderPicking.CreatedDate,
                        },
                        OrderReserve = o.OrderReserve == null ? null : new OrderReserve
                        {
                            Number = o.OrderReserve.Number,
                            CreatedDate = o.OrderReserve.CreatedDate
                        },
                        OrderBilling = o.OrderBilling == null ? null : new OrderBilling
                        {
                            CreatedDate = o.OrderBilling.CreatedDate,
                            InvoiceNumber = o.OrderBilling.InvoiceNumber,
                            ReturnDescription = o.OrderBilling.ReturnDescription,
                            ReturnNumber = o.OrderBilling.ReturnNumber
                        },
                        MarketplaceInvoice = o.MarketplaceInvoice == null ? null : new MarketplaceInvoice
                        {
                            Number = o.MarketplaceInvoice.Number
                        },
                        Payments = o.Payments.Select(p => new Payment
                        {
                            PaymentTypeId = p.PaymentTypeId,
                            Paid = p.Paid,
                            PaymentType = new PaymentType
                            {
                                PaymentTypeName = p.PaymentType.PaymentTypeTranslations.FirstOrDefault(ptt => ptt.LanguageId == "TR").Name
                            },
                            PaymentLog = new PaymentLog
                            {
                                Pay3dMessage = p.PaymentLog.Pay3dMessage,
                                Check3dMessage = p.PaymentLog.Check3dMessage
                            }
                        }).ToList()
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }


        public PagedResponse<OrderDetail> BulkOrderRead(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<OrderDetail>>((response) =>
            {

                var orderDetails = GetBulkOrders(pagedRequest.Search, pagedRequest.Sort);







                response.Page = pagedRequest.Page;
                response.PageRecordsCount = orderDetails.Count;
                response.RecordsCount = orderDetails.Count;





                response.Data = orderDetails;
                response.Success = true;
            }, (response, exception) => { });
        }


        public Response Update(Order dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var order = this
                                ._unitOfWork
                                .OrderRepository
                                .DbSet()
                                .Include(x => x.OrderDeliveryAddress)
                                .Include(x => x.OrderInvoiceInformation)
                                .Include(x => x.OrderShipments)
                                .Include(x => x.OrderDetails)
                                .Include(x => x.Payments)
                                .Include(x => x.OrderNotes)
                                .FirstOrDefault(x => x.Id == dto.Id);


                var cargoFeeRatioAmount = dto.CargoFee;
                if (cargoFeeRatioAmount > 0)
                {
                    cargoFeeRatioAmount = cargoFeeRatioAmount / dto.OrderDetails.Sum(x => x.Quantity);
                }

                var commissionRatioAmount = dto.CommissionAmount;
                if (commissionRatioAmount > 0)
                {
                    commissionRatioAmount = commissionRatioAmount / dto.OrderDetails.Sum(x => x.Quantity);
                }

                foreach (var odLoop in dto.OrderDetails)
                {

                    var productInformation = this._unitOfWork.ProductInformationRepository
                    .DbSet()
                    .Include(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == this._settingService.DefaultCurrencyId))
                    .FirstOrDefault(x => x.Id == odLoop.ProductInformationId);
                    if (odLoop.Id == 0)
                    {
                        odLoop.UnitCost = productInformation.ProductInformationPriceses.FirstOrDefault().UnitCost;
                        odLoop.ListUnitPrice = productInformation.ProductInformationPriceses.FirstOrDefault().ListUnitPrice;
                        odLoop.VatRate = productInformation.ProductInformationPriceses.FirstOrDefault().VatRate;
                        odLoop.UnitDiscount = odLoop.ListUnitPrice - odLoop.UnitPrice;
                        odLoop.UnitCargoFee = cargoFeeRatioAmount;
                        odLoop.UnitCommissionAmount = commissionRatioAmount;
                    }
                    else
                    {
                        var orderDeail = order.OrderDetails.FirstOrDefault(x => x.Id == odLoop.Id);
                        odLoop.UnitCost = orderDeail.UnitCost;
                        odLoop.ListUnitPrice = orderDeail.ListUnitPrice;
                        odLoop.VatRate = orderDeail.VatRate;
                        odLoop.UnitDiscount = orderDeail.UnitDiscount;
                        odLoop.UnitCargoFee = orderDeail.UnitCargoFee;
                        odLoop.UnitCommissionAmount = odLoop.UnitCommissionAmount;
                    }
                }

                dto.Discount = dto.OrderDetails.Sum(x => x.ListUnitPrice * x.Quantity) - dto.TotalAmount;

                dto.ListTotal = dto.OrderDetails.Sum(x => x.ListUnitPrice * x.Quantity);

                order.MarketplaceId = String.IsNullOrEmpty(dto.MarketplaceId) ? null : dto.MarketplaceId;
                order.OrderTypeId = dto.OrderTypeId;
                order.CommissionAmount = dto.CommissionAmount;
                order.CargoFee = dto.CargoFee;
                order.Discount = dto.Discount;
                order.ListTotal = dto.ListTotal;
                order.VatExcDiscount = dto.VatExcDiscount;
                order.VatExcListTotal = dto.VatExcListTotal;
                order.VatExcTotal = dto.VatExcTotal;
                order.Total = dto.TotalAmount;
                order.OrderSourceId = dto.OrderSourceId;
                order.OrderDeliveryAddress.Recipient = $"{dto.OrderDeliveryAddress.FirstName} {dto.OrderDeliveryAddress.LastName}";
                order.OrderDeliveryAddress.Address = dto.OrderDeliveryAddress.Address;
                order.OrderDeliveryAddress.NeighborhoodId = dto.OrderDeliveryAddress.NeighborhoodId;
                order.OrderDeliveryAddress.PhoneNumber = dto.OrderDeliveryAddress.PhoneNumber;

                order.OrderInvoiceInformation.NeighborhoodId = dto.OrderInvoiceInformation.NeighborhoodId;
                order.OrderInvoiceInformation.Address = dto.OrderInvoiceInformation.Address;
                order.OrderInvoiceInformation.Mail = dto.OrderInvoiceInformation.Mail;
                order.OrderInvoiceInformation.Phone = dto.OrderInvoiceInformation.Phone;
                order.OrderInvoiceInformation.TaxNumber = dto.OrderInvoiceInformation.TaxNumber;
                order.OrderInvoiceInformation.TaxOffice = dto.OrderInvoiceInformation.TaxOffice;
                order.OrderInvoiceInformation.FirstName = dto.OrderInvoiceInformation.FirstName;
                order.OrderInvoiceInformation.LastName = dto.OrderInvoiceInformation.LastName;
                order.OrderShipments[0].ShipmentCompanyId = dto.OrderShipments[0].ShipmentCompanyId;
                order.OrderShipments[0].TrackingCode = dto.OrderShipments[0].TrackingCode;
                order.OrderShipments[0].TrackingUrl = dto.OrderShipments[0].TrackingUrl;

                order.OrderDetails = new List<Domain.Entities.OrderDetail>();
                order.OrderDetails = dto.OrderDetails.Select(x => new Domain.Entities.OrderDetail
                {
                    Id = x.Id,
                    ListUnitPrice = x.ListUnitPrice,
                    ProductInformationId = x.ProductInformationId,
                    Quantity = x.Quantity,
                    UnitCost = x.UnitCost,
                    UnitPrice = x.UnitPrice,
                    VatRate = x.VatRate,
                    UnitCargoFee = x.UnitCargoFee,
                    UnitCommissionAmount = x.UnitCommissionAmount,
                    UnitDiscount = x.UnitDiscount,
                    VatExcListUnitPrice = x.VatExcListUnitPrice,
                    VatExcUnitPrice = x.VatExcUnitPrice,
                    VatExcUnitCost = x.VatExcUnitCost,
                    VatExcUnitDiscount = x.VatExcUnitDiscount,
                    Payor = x.Payor,
                    CreatedDate = DateTime.Now
                }).ToList();

                order.Payments = new List<Domain.Entities.Payment>();
                order.Payments = dto.Payments?.Select(x => new Domain.Entities.Payment
                {
                    Amount = x.Amount,
                    Paid = x.Paid,
                    PaymentTypeId = x.PaymentTypeId
                }).ToList();

                order.OrderNotes = dto.OrderNotes.Select(on => new Domain.Entities.OrderNote { Note = on.Note }).ToList();

                _unitOfWork.OrderRepository.Update(order);

                if (dto.OrderTypeId == OrderTypes.Iptal)
                {
                    DeletePicking(dto);
                }

                response.Message = "Sipariş tipi başarılı bir şekilde güncellendi.";
                response.Success = true;
            });
        }

        public Response Create(Order dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var eCustomer = new Domain.Entities.Customer
                {
                    Name = dto.OrderDeliveryAddress.FirstName.ToUpperInvariant(),
                    Surname = dto.OrderDeliveryAddress.LastName.ToUpperInvariant(),
                    CreatedDate = DateTime.Now,
                    CustomerContact = new Domain.Entities.CustomerContact
                    {
                        Phone = dto.OrderDeliveryAddress.PhoneNumber,
                        Mail = String.Empty,
                        TaxNumber = null,
                        TaxOffice = null,
                        CreatedDate = DateTime.Now
                    },
                };
                _unitOfWork.CustomerRepository.Create(eCustomer);

                var cargoFeeRatioAmount = dto.CargoFee;
                if (cargoFeeRatioAmount > 0)
                {
                    cargoFeeRatioAmount = cargoFeeRatioAmount / dto.OrderDetails.Sum(x => x.Quantity);
                }

                var commissionRatioAmount = dto.CommissionAmount;
                if (commissionRatioAmount > 0)
                {
                    commissionRatioAmount = commissionRatioAmount / dto.OrderDetails.Sum(x => x.Quantity);
                }

                foreach (var odLoop in dto.OrderDetails)
                {
                    var productInformation = this._unitOfWork.ProductInformationRepository
                    .DbSet()
                    .Include(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == this._settingService.DefaultCurrencyId))
                    .FirstOrDefault(x => x.Id == odLoop.ProductInformationId);


                    odLoop.UnitCost = productInformation.ProductInformationPriceses.FirstOrDefault().UnitCost;
                    odLoop.ListUnitPrice = productInformation.ProductInformationPriceses.FirstOrDefault().ListUnitPrice;
                    odLoop.VatRate = productInformation.ProductInformationPriceses.FirstOrDefault().VatRate;
                    odLoop.UnitDiscount = odLoop.ListUnitPrice - odLoop.UnitPrice;
                    odLoop.UnitCargoFee = cargoFeeRatioAmount;
                    odLoop.UnitCommissionAmount = commissionRatioAmount;
                }

                dto.Discount = dto.OrderDetails.Sum(x => x.ListUnitPrice * x.Quantity) - dto.TotalAmount;
                dto.ListTotal = dto.OrderDetails.Sum(x => x.ListUnitPrice * x.Quantity);

                var eOrder = new Domain.Entities.Order
                {
                    CustomerId = eCustomer.Id,
                    CurrencyId = this._settingService.DefaultCurrencyId,
                    ApplicationId = null,
                    MarketplaceId = String.IsNullOrEmpty(dto.MarketplaceId) ? null : dto.MarketplaceId,
                    LanguageId = this._settingService.DefaultLanguageId,
                    OrderTypeId = dto.OrderTypeId,
                    Total = dto.TotalAmount,
                    CommissionAmount = dto.CommissionAmount,
                    CargoFee = dto.CargoFee,
                    Discount = dto.Discount,
                    ListTotal = dto.ListTotal,
                    VatExcDiscount = dto.VatExcDiscount,
                    VatExcListTotal = dto.VatExcListTotal,
                    VatExcTotal = dto.VatExcTotal,
                    CreatedDate = DateTime.Now,
                    OrderSourceId = dto.OrderSourceId,
                    OrderDate = DateTime.Now,
                    EstimatedPackingDate = DateTime.Now,
                    OrderDeliveryAddress = new Domain.Entities.OrderDeliveryAddress
                    {
                        Recipient = $"{dto.OrderDeliveryAddress.FirstName} {dto.OrderDeliveryAddress.LastName}".ToUpperInvariant(),
                        CreatedDate = DateTime.Now,
                        Address = dto.OrderDeliveryAddress.Address,
                        NeighborhoodId = dto.OrderDeliveryAddress.NeighborhoodId,
                        PhoneNumber = dto.OrderDeliveryAddress.PhoneNumber
                    },
                    OrderInvoiceInformation = new Domain.Entities.OrderInvoiceInformation
                    {
                        CreatedDate = DateTime.Now,
                        NeighborhoodId = dto.OrderInvoiceInformation.NeighborhoodId,
                        Address = dto.OrderInvoiceInformation.Address,
                        Mail = dto.OrderInvoiceInformation.Mail,
                        Phone = dto.OrderInvoiceInformation.Phone,
                        TaxNumber = dto.OrderInvoiceInformation.TaxNumber,
                        TaxOffice = dto.OrderInvoiceInformation.TaxOffice,
                        FirstName = dto.OrderInvoiceInformation.FirstName.ToUpperInvariant(),
                        LastName = dto.OrderInvoiceInformation.LastName.ToUpperInvariant()
                    },
                    OrderShipments = dto.OrderShipments.Select(x => new Domain.Entities.OrderShipment
                    {
                        ShipmentCompanyId = x.ShipmentCompanyId,
                        TrackingCode = x.TrackingCode,
                        TrackingUrl = x.TrackingUrl

                    }).ToList(),
                    OrderTechnicInformation = new Domain.Entities.OrderTechnicInformation
                    {
                        Browser = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString(),
                        IpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                        Platform = "",
                    },
                    OrderDetails = dto.OrderDetails.Select(x => new Domain.Entities.OrderDetail
                    {
                        ListUnitPrice = x.ListUnitPrice,
                        ProductInformationId = x.ProductInformationId,
                        Quantity = x.Quantity,
                        UnitCost = x.UnitCost,
                        UnitPrice = x.UnitPrice,
                        VatRate = x.VatRate,
                        UnitCargoFee = x.UnitCargoFee,
                        UnitCommissionAmount = x.UnitCommissionAmount,
                        UnitDiscount = x.UnitDiscount,
                        VatExcListUnitPrice = x.VatExcListUnitPrice,
                        VatExcUnitPrice = x.VatExcUnitPrice,
                        VatExcUnitCost = x.VatExcUnitCost,
                        VatExcUnitDiscount = x.VatExcUnitDiscount,
                        Payor = x.Payor,
                        CreatedDate = DateTime.Now
                    }).ToList(),
                    Payments = dto.Payments != null ? dto.Payments.Select(x => new Domain.Entities.Payment
                    {
                        Amount = x.Amount,
                        Paid = x.Paid,
                        PaymentTypeId = x.PaymentTypeId
                    }).ToList() : new(),
                    OrderNotes = dto.OrderNotes.Select(on => new Domain.Entities.OrderNote { Note = on.Note }).ToList(),
                    Application = null,
                    Currency = null,
                    Customer = null,
                    Language = null,
                    Marketplace = null
                };
                _unitOfWork.OrderRepository.Create(eOrder);

                response.Message = "Yeni sipariş başarılı bir şekilde eklendi.";
                response.Success = true;
            });
        }

        public DataResponse<Summary> Summary()
        {
            return ExceptionHandler.ResultHandle<DataResponse<Summary>>((response) =>
            {
                var todayOrdersCount = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Count(o => o.OrderDate.Date == DateTime.Now.Date && o.OrderTypeId != "IP");

                var unpackedOrdersCount = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Count(o => o.OrderTypeId == "OS");

                var packedOrdersCount = this
                    ._unitOfWork
                    .OrderPackingRepository
                    .DbSet()
                    .Count(op => op.CreatedDate.Date == DateTime.Now.Date);

                response.Data = new Summary
                {
                    TodayOrdersCount = todayOrdersCount,
                    PackedOrdersCount = packedOrdersCount,
                    UnpackedOrdersCount = unpackedOrdersCount
                };
                response.Success = true;
            });
        }

        public DataResponse<MemoryStream> Download(FilterRequest filterRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                var iQueryable = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet();

                var parameters = filterRequest.Search.Split(new string[] { "_" }, System.StringSplitOptions.RemoveEmptyEntries);

                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("Id:"))
                    {
                        var value = pLoop.Replace("Id:", "");
                        if (value.Length > 0)
                        {
                            var id = int.Parse(value);
                            iQueryable = iQueryable.Where(iq => iq.Id == id);
                        }
                    }
                    else if (pLoop.StartsWith("Name:"))
                    {
                        var value = pLoop.Replace("Name:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Customer.Name.Contains(value));
                    }
                    else if (pLoop.StartsWith("Surname:"))
                    {
                        var value = pLoop.Replace("Surname:", "");
                        if (value.Length > 0)
                            iQueryable = iQueryable.Where(iq => iq.Customer.Surname.Contains(value));
                    }
                    else if (pLoop.StartsWith("Marketplace:"))
                    {
                        var value = pLoop.Replace("Marketplace:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.MarketplaceId == value);
                    }
                    else if (pLoop.StartsWith("OrderType:"))
                    {
                        var value = pLoop.Replace("OrderType:", "");
                        if (value.Length > 0)
                        {
                            var values = value.Split(",", StringSplitOptions.RemoveEmptyEntries);
                            iQueryable = iQueryable.Where(iq => values.Contains(iq.OrderTypeId));
                        }
                    }
                    else if (pLoop.StartsWith("MarketplaceOrderNumber:"))
                    {
                        var value = pLoop.Replace("MarketplaceOrderNumber:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.MarketplaceOrderNumber == value);
                    }
                    else if (pLoop.StartsWith("CargoTrackingNumber:"))
                    {
                        var value = pLoop.Replace("CargoTrackingNumber:", "");
                        if (value.Length > 0 && value != "-1")
                            iQueryable = iQueryable.Where(iq => iq.OrderShipments.Any(os => os.TrackingCode == value));
                    }
                    else if (pLoop.StartsWith("OrderSource:"))
                    {
                        var value = pLoop.Replace("OrderSource:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            var orderSourceId = int.Parse(value);
                            iQueryable = iQueryable.Where(iq => iq.OrderSourceId == orderSourceId);
                        }
                    }
                    else if (pLoop.StartsWith("PhoneNumber:"))
                    {
                        var value = pLoop.Replace("PhoneNumber:", "");
                        if (value.Length > 0 && value != "-1")
                        {
                            iQueryable = iQueryable.Where(iq => iq.Customer.CustomerContact.Phone == value ||
                                                                iq.OrderDeliveryAddress.PhoneNumber == value ||
                                                                iq.OrderInvoiceInformation.Phone == value);
                        }
                    }
                    else if (pLoop.StartsWith("Date:"))
                    {
                        var value = pLoop.Replace("Date:", "");
                        if (!string.IsNullOrEmpty(value))
                        {
                            var dates = value.Split(new string[] { "/" }, System.StringSplitOptions.RemoveEmptyEntries);
                            var startDate = DateTime.Parse(dates[0].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                            var endDate = DateTime.Parse(dates[1].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                            iQueryable = iQueryable.Where(iq => iq.OrderDate.Date >= startDate && iq.OrderDate.Date <= endDate.Date);
                        }
                    }
                }

                var excel = new Application.Common.DataTransferObjects.Excel
                {
                    Header = new Application.Common.DataTransferObjects.Row
                    {
                        Cells = new List<Application.Common.DataTransferObjects.Cell>()
                    },
                    Rows = new List<Application.Common.DataTransferObjects.Row>()
                };

                foreach (var theHeader in new[] { "Sipariş No",
                                                  "Pazaryeri",
                                                  "Pazaryeri Sipariş No",
                                                  "Kargo Firması",
                                                  "Ad",
                                                  "Soyad",
                                                  "Ürün",
                                                  "Adet",
                                                  "Ürün Birim Fiyat",
                                                  "Ürün Tutarı",
                                                  "Toplam" })
                {
                    excel.Header.Cells.Add(new Application.Common.DataTransferObjects.Cell
                    {
                        CellType = Application.Common.DataTransferObjects.CellType.String,
                        Value = theHeader
                    });
                }

                iQueryable
                    .Include(o => o.Customer)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderDetails)
                    .ThenInclude(o => o.ProductInformation.ProductInformationTranslations)
                    .ToList()
                    .ForEach(theOrder =>
                    {
                        bool firstLine = true;
                        theOrder.OrderDetails.ForEach(theOrderDetail =>
                        {
                            bool merge = firstLine && theOrder.OrderDetails.Count > 1;

                            excel.Rows.Add(new Application.Common.DataTransferObjects.Row
                            {
                                Cells = new List<Application.Common.DataTransferObjects.Cell>
                                {
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.Number,
                                        Value = firstLine ? theOrder.Id : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "A" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = firstLine ? theOrder.Marketplace?.Name : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "B" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = firstLine ? theOrder.MarketplaceOrderNumber : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "C" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "D" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = firstLine ? theOrder.Customer.Name : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "E" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = firstLine ? theOrder.Customer.Surname : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "F" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count: 0
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.String,
                                        Value = theOrderDetail.ProductInformation.ProductInformationTranslations[0].Name
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.Number,
                                        Value = theOrderDetail.Quantity
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.Number,
                                        Value = theOrderDetail.UnitPrice
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.Number,
                                        Value = theOrderDetail.Quantity * theOrderDetail.UnitPrice
                                    },
                                    new Application.Common.DataTransferObjects.Cell
                                    {
                                        CellType = Application.Common.DataTransferObjects.CellType.Number,
                                        Value = firstLine ? theOrder.Total : string.Empty,
                                        Merge = merge,
                                        ColumnName = merge ? "K" : null,
                                        RowCount = merge ? theOrder.OrderDetails.Count : 0
                                    }
                                }
                            });

                            firstLine = false;
                        });
                    });

                var memoryStream = this._excelHelper.Write(excel);

                memoryStream.Position = 0;

                response.Data = memoryStream;
            });
        }

        public DataResponse<MemoryStream> Pdf(string ids)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                #region Check Parameters
                if (string.IsNullOrEmpty(ids))
                {
                    response.Success = false;
                    response.Message = "Lütfen en az 1 tane sipariş seçiniz.";
                    return;
                }

                var idList = new List<int>();
                var idsParts = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
                foreach (var ipLoop in idsParts)
                    if (int.TryParse(ipLoop, out int id))
                        idList.Add(id);

                if (idList.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Lütfen en az 1 tane sipariş seçiniz.";
                    return;
                }
                #endregion

                var company = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(c => c.CompanyContact)
                    .FirstOrDefault(x => x.Id == this._companyFinder.FindId());

                var orders = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => idList.Contains(o.Id))
                    .Include(o => o.Payments)
                    .ThenInclude(p => p.PaymentType.PaymentTypeTranslations.Where(pt => pt.LanguageId == "TR"))
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
                    .Include(o => o.Customer)
                    .Include(o => o.OrderNotes)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(os => os.ShipmentCompany)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations.Where(pt => pt.LanguageId == "TR"))
                     .Include(o => o.OrderDetails)
                     .ThenInclude(od => od.ProductInformation.Product)
                    .ToList();

                PdfDocument pdfDocument = new();
                XFont xFont12 = new("Arial", 12);
                XFont xFont14 = new("Arial", 14);
                XFont xFont18 = new("Arial", 18);
                XFont xFont14Bold = new("Arial", 14, XFontStyle.Bold);
                XFont xFont16Bold = new("Arial", 16, XFontStyle.Bold);
                XFont xFont18Bold = new("Arial", 18, XFontStyle.Bold);
                XFont xFont20Bold = new("Arial", 20, XFontStyle.Bold);
                XFont xFont24 = new("Arial", 24);

                orders.ForEach(oLoop =>
                {

                    var y = 10;

                    PdfPage pdfPage = pdfDocument.AddPage();
                    pdfPage.Orientation = PdfSharp.PageOrientation.Landscape;
                    pdfPage.Size = PdfSharp.PageSize.A5;

                    XGraphics xGraphics = XGraphics.FromPdfPage(pdfPage);

                    //if (string.IsNullOrEmpty(oLoop.OrderShipments.FirstOrDefault().TrackingBase64))
                    //{
                    var lineCount = 0;



                    #region Order
                    if (oLoop.Marketplace == null && oLoop.Payments.Count > 0)
                    {

                        var paymentType = oLoop.Payments[0].PaymentType.PaymentTypeTranslations[0].Name;
                        lineCount = (int)Math.Ceiling(paymentType.Length / 20M);
                        for (int i = 0; i < lineCount; i++)
                        {
                            xGraphics.DrawString(
                                string.Join("", paymentType.Skip(i * 20).Take(20)),
                                xFont18,
                                XBrushes.Black,
                                new XRect(180, y, 75, 20),
                                XStringFormat.Center);

                            y += 20;
                        }
                        var total = oLoop.Payments[0].Amount;


                        xGraphics.DrawString(
                                $"{total:N2} TL",
                                xFont18,
                                XBrushes.Black,
                                new XRect(180, y, 75, 20),
                                XStringFormat.Center);

                    }



                    y -= 20 * lineCount;

                    if (oLoop.Marketplace != null)
                    {
                        xGraphics.DrawImage(XImage.FromFile($@"{Environment.CurrentDirectory}\wwwroot\org\img\marketplace-icons\{oLoop.MarketplaceId}.png"), new XRect(10, y, 75, 30));

                        y += 30;
                    }

                    var xImage = XImage.FromFile($@"{Environment.CurrentDirectory}\wwwroot\org\img\shipment-company-icons\{oLoop.OrderShipments[0].ShipmentCompanyId}.png");
                    xGraphics.DrawImage(xImage, new XRect(10, y, 75, 30));

                    y += 30;

                    xGraphics.DrawString($"#{oLoop.Id} {(string.IsNullOrEmpty(oLoop.MarketplaceOrderNumber) ? "" : $"- #{oLoop.MarketplaceOrderNumber}")}", xFont20Bold, XBrushes.Black, new XRect(10, y, 200, 20), XStringFormat.TopLeft);
                    #endregion

                    #region Barcode
                    y = 5;
                    xGraphics.DrawString($"{oLoop.OrderDate.ToString("dd-MM-yyy")} ", xFont14Bold, XBrushes.Black, new XRect(pdfPage.Width - 40, y, 0, 5), XStringFormat.TopCenter);

                    y = 25;

                    var trackingCode = oLoop.OrderShipments.FirstOrDefault().TrackingCode;

                    if ((oLoop.OrderSourceId == OrderSources.Web || oLoop.OrderSourceId == OrderSources.WhatsApp || oLoop.OrderSourceId == OrderSources.Değişim) && string.IsNullOrEmpty(oLoop.MarketplaceId))// Focus kargosuna özel yapıldı değiştiriecek caplin
                    {
                        trackingCode = oLoop.Id.ToString().PadLeft(8, '0');
                    }

                    var barcodeImage = BarcodeDrawFactory
                                    .Code128WithChecksum
                                    .Draw(trackingCode, 60, 2);

                    MemoryStream memoryStream = new();
                    barcodeImage.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    memoryStream.Position = 0;

                    xGraphics.DrawImage(XImage.FromStream(memoryStream), new XRect(pdfPage.Width - barcodeImage.Width - 10, y, barcodeImage.Width, barcodeImage.Height));

                    y += barcodeImage.Height;

                    xGraphics.DrawString(trackingCode, xFont14, XBrushes.Black, new XRect(pdfPage.Width - barcodeImage.Width - 10, y, barcodeImage.Width, 14), XStringFormat.Center);

                    y += 25;
                    #endregion

                    #region Rectangle
                    xGraphics.DrawLine(XPens.Black, 10, y, pdfPage.Width - 10, y);

                    xGraphics.DrawLine(XPens.Black, 10, y, 10, y + 300);

                    xGraphics.DrawLine(XPens.Black, pdfPage.Width - 10, y, pdfPage.Width - 10, y + 300);

                    y += 300;

                    xGraphics.DrawLine(XPens.Black, 10, y, pdfPage.Width - 10, y);
                    #endregion

                    y -= 280;

                    xGraphics.DrawString($"GÖNDERİCİ BİLGİLERİ", xFont18, XBrushes.Black, new XRect(20, y, pdfPage.Width / 2, 18), XStringFormat.TopLeft);

                    xGraphics.DrawString($"ALICI BİLGİLERİ", xFont18, XBrushes.Black, new XRect(pdfPage.Width / 2, y, pdfPage.Width / 2, 18), XStringFormat.TopLeft);

                    y += 18;

                    var companyFullAddress = $"{company.Name} {company.CompanyContact.Address} {company.CompanyContact.PhoneNumber}";

                    lineCount = (int)Math.Ceiling(companyFullAddress.Length / 30M);
                    for (int i = 0; i < lineCount; i++)
                    {
                        xGraphics.DrawString(
                            string.Join("", companyFullAddress.Skip(i * 30).Take(30)),
                            xFont12,
                            XBrushes.Black,
                            new XRect(20, y, pdfPage.Width / 2, 18),
                            XStringFormat.TopLeft);

                        y += 14;
                    }

                    y -= (lineCount * 14);

                    xGraphics.DrawString($"{oLoop.Customer.Name} {oLoop.Customer.Surname}", xFont12, XBrushes.Black, new XRect(pdfPage.Width / 2, y, pdfPage.Width / 2, 14), XStringFormat.TopLeft);

                    y += 14;

                    var fullAddress = $"{oLoop.OrderDeliveryAddress.Address} {oLoop.OrderDeliveryAddress.Neighborhood.Name} {oLoop.OrderDeliveryAddress.Neighborhood.District.Name} {oLoop.OrderDeliveryAddress.Neighborhood.District.City.Name} {oLoop.OrderDeliveryAddress.PhoneNumber}";

                    lineCount = (int)Math.Ceiling(fullAddress.Length / 40M);
                    for (int i = 0; i < lineCount; i++)
                    {
                        xGraphics.DrawString(
                            string.Join("", fullAddress.Skip(i * 40).Take(40)),
                            xFont12,
                            XBrushes.Black,
                            new XRect(pdfPage.Width / 2, y, (pdfPage.Width / 2) - 50, 42),
                            XStringFormat.TopLeft);

                        y += 14;
                    }

                    y += 50;

                    xGraphics.DrawString($"ÜRÜN BİLGİLERİ", xFont18, XBrushes.Black, new XRect(20, y, pdfPage.Width / 2, 18), XStringFormat.TopLeft);

                    y += 20;

                    oLoop.OrderDetails.ForEach(odLoop =>
                    {
                        var sellerCode = !string.IsNullOrEmpty(odLoop.ProductInformation.Product.SellerCode) ? odLoop.ProductInformation.Product.SellerCode : "";
                        var productName = $"{sellerCode} {odLoop.ProductInformation.ProductInformationTranslations[0].Name}";


                        //if (productName.Length > 75)
                        //{
                        //    productName = productName.Substring(0, 75);
                        //}

                        if (!string.IsNullOrEmpty(odLoop.ProductInformation.ProductInformationTranslations[0].VariantValuesDescription))
                            productName = $"{productName} {odLoop.ProductInformation.ProductInformationTranslations[0].VariantValuesDescription}";

                        if (!string.IsNullOrEmpty(odLoop.ProductInformation.ShelfCode))
                            productName = $"{productName} ({odLoop.ProductInformation.ShelfCode})";


                        productName = $"{odLoop.Quantity} Adet {productName}".ToUpper(new System.Globalization.CultureInfo("tr-TR"));

                        var productlineCount = (int)Math.Ceiling(productName.Length / 65M);
                        for (int i = 0; i < productlineCount; i++)
                        {
                            xGraphics.DrawString(
                                string.Join("", productName.Skip(i * 65).Take(65)),
                                xFont14Bold,
                                XBrushes.Black,
                                new XRect(20, y, pdfPage.Width - 65, pdfPage.Height),
                                XStringFormat.TopLeft);

                            y += 14;
                        }
                        //y += 14;
                        //xGraphics.DrawString(, xFont12, XBrushes.Black, new XRect(20, y, pdfPage.Width, pdfPage.Height), XStringFormat.TopLeft);
                    });

                    if (oLoop.OrderNotes != null && oLoop.OrderNotes.Count > 0 && !string.IsNullOrEmpty(oLoop.OrderNotes[0].Note))
                    {
                        y += 15;

                        lineCount = (int)Math.Ceiling(oLoop.OrderNotes.FirstOrDefault().Note.Length / 100M);
                        for (int i = 0; i < lineCount; i++)
                        {
                            y += 15;

                            xGraphics.DrawString(
                                $"{(i == 0 ? "Not:" : "")} {string.Join("", oLoop.OrderNotes.FirstOrDefault().Note.Skip(i * 100).Take(100))}",
                                xFont12,
                                XBrushes.Black,
                                new XRect(20, y, pdfPage.Width - 40, pdfPage.Height),
                                XStringFormat.TopLeft);
                        }

                    }
                    //}
                    //else
                    //{
                    //    using (MemoryStream memoryStream = new(
                    //        System.Convert.FromBase64String(
                    //            oLoop.OrderShipments.FirstOrDefault().TrackingBase64)))
                    //    {
                    //        memoryStream.Position = 0;

                    //        using (var bitmap = System.Drawing.Image.FromStream(memoryStream))
                    //        {
                    //            bitmap.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);

                    //            using (MemoryStream memoryStream1 = new())
                    //            {
                    //                bitmap.Save(memoryStream1, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //                var xImage = XImage.FromStream(memoryStream1);

                    //                xGraphics.DrawImage(xImage, 10, 10, xImage.Width / 2.5, xImage.Height / 2.5);
                    //            }
                    //        }
                    //    }
                    //}
                });

                MemoryStream stream = new();

                pdfDocument.Save(stream);

                stream.Position = 0;

                response.Data = stream;

                MakeShipped(idList);
            });
        }

        public DataResponse<MemoryStream> BulkOrderPdf(string search)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
                PdfDocument pdfDocument = new PdfDocument();
                pdfDocument.Info.Title = "Sipariş Toplama";
                var parameters = search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                var pagedShort = "";
                foreach (var pLoop in parameters)
                {
                    if (pLoop.StartsWith("PagedShort:"))
                    {
                        var value = pLoop.Replace("PagedShort:", "");
                        if (value.Length > 0 && value != "-1")
                            pagedShort = value;
                    }
                }

                var orderDetails = GetBulkOrders(search, pagedShort);

                var productlineCount = (int)Math.Ceiling(orderDetails.Count / 40m);


                for (int j = 0; j < productlineCount; j++)
                {
                    // Page Options
                    PdfPage pdfPage = pdfDocument.AddPage();

                    pdfPage.Size = PdfSharp.PageSize.A4;

                    // Get an XGraphics object for drawing
                    XGraphics graph = XGraphics.FromPdfPage(pdfPage);

                    // Text format
                    XStringFormat format = new XStringFormat();
                    format.LineAlignment = XLineAlignment.Near;
                    format.Alignment = XStringAlignment.Near;
                    var tf = new XTextFormatter(graph);

                    XFont fontParagraph = new XFont("Verdana", 8, XFontStyle.Regular);

                    // Row elements
                    int el1_width = 120;
                    int el2_width = 200;
                    int el3_width = 320;

                    // page structure options
                    double lineHeight = 20;
                    int marginLeft = 20;
                    int marginTop = 0;

                    int el_height = 30;
                    int rect_height = 17;

                    int interLine_X_1 = 2;
                    int interLine_X_2 = 4;
                    int interLine_X_3 = 6;

                    int offSetX_1 = el1_width;
                    int offSetX_2 = el1_width + el2_width;
                    int offSetX_3 = el1_width + el3_width;

                    XSolidBrush rect_style1 = new XSolidBrush(XColors.LightGray);
                    XSolidBrush rect_style2 = new XSolidBrush(XColors.White);
                    XSolidBrush rect_style3 = new XSolidBrush(XColors.Red);

                    var odLoop = orderDetails.Skip(j * 40).Take(40).ToList();
                    odLoop.ForEach(oLoop =>
                    {

                        var index = odLoop.IndexOf(oLoop);

                        double dist_Y = lineHeight * (index + 1);
                        double dist_Y2 = dist_Y - 2;

                        // header della G
                        //if (index == 0)
                        //{
                        //    graph.DrawRectangle(rect_style2, marginLeft, marginTop, (pdfPage.Width + 10) - 2 * marginLeft, rect_height);

                        //    tf.DrawString("Adet", fontParagraph, XBrushes.Black,
                        //                  new XRect(marginLeft + 20, marginTop, el1_width, el_height), format);

                        //    tf.DrawString("Model", fontParagraph, XBrushes.Black,
                        //                  new XRect(marginLeft + offSetX_1 + 30 * interLine_X_1, marginTop, el2_width, el_height), format);

                        //    tf.DrawString("Varyant", fontParagraph, XBrushes.Black,
                        //                  new XRect(marginLeft + offSetX_2 + 6 * interLine_X_2, marginTop, el1_width, el_height), format);

                        //    tf.DrawString("Raf", fontParagraph, XBrushes.Black,
                        //                  new XRect(marginLeft + offSetX_3 + 6 * interLine_X_3, marginTop, el1_width, el_height), format);

                        //}

                        //ELEMENT 1 - SMALL 80
                        graph.DrawRectangle(rect_style1, marginLeft, marginTop + dist_Y2, el1_width, rect_height);
                        tf.DrawString(
                           $"{oLoop.Quantity.ToString()} ADET",
                            fontParagraph,
                            XBrushes.Black,
                            new XRect(marginLeft + 6, marginTop + dist_Y, el1_width, el_height),
                            format);

                        //ELEMENT 2 - BIG 380
                        graph.DrawRectangle(rect_style1, marginLeft + offSetX_1 + interLine_X_1, dist_Y2 + marginTop, el2_width, rect_height);
                        tf.DrawString(
                            oLoop.ProductInformation.Product.SellerCode.ToUpper(),
                            fontParagraph,
                            XBrushes.Black,
                            new XRect(marginLeft + offSetX_1 + 4 * interLine_X_1, marginTop + dist_Y, el2_width, el_height),
                            format);


                        //ELEMENT 3 - SMALL 80

                        graph.DrawRectangle(rect_style1, marginLeft + offSetX_2 + interLine_X_2, dist_Y2 + marginTop, el1_width, rect_height);
                        tf.DrawString(
                           oLoop.ProductInformation.VariantValuesDescription == null ? "" : oLoop.ProductInformation.VariantValuesDescription,
                            fontParagraph,
                            XBrushes.Black,
                            new XRect(marginLeft + offSetX_2 + 2 * interLine_X_2, marginTop + dist_Y, el1_width, el_height),
                            format);


                        graph.DrawRectangle(rect_style1, marginLeft + offSetX_3 + interLine_X_3, dist_Y2 + marginTop, el1_width, rect_height);
                        tf.DrawString(
                           !string.IsNullOrEmpty(oLoop.ProductInformation.ShelfCode) ? oLoop.ProductInformation.ShelfCode : "",
                            fontParagraph,
                            XBrushes.Black,
                            new XRect(marginLeft + offSetX_3 + 2 * interLine_X_3, marginTop + dist_Y, el1_width, el_height),
                            format);




                        //double dist_Y = lineHeight * (i + 1);
                        //double dist_Y2 = dist_Y - 2;






                    });





                }


                MemoryStream stream = new();

                pdfDocument.Save(stream);

                stream.Position = 0;

                response.Data = stream;

            });
        }

        public DataResponse<MemoryStream> MutualBarcode(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MemoryStream>>((response) =>
            {
#warning Tek barkod oldugu varsayılmıştır

                var base64 = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => o.Id == id)
                    .Select(o => o.OrderShipments.FirstOrDefault().TrackingBase64)
                    .FirstOrDefault();

                MemoryStream memoryStream = new(System.Convert.FromBase64String(base64));
                memoryStream.Position = 0;

                response.Data = memoryStream;
            });
        }

        public DataResponse<int> Convert(ConvertRequest convertRequest)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Update  Orders 
Set     OrderTypeId = @TargetOrderTypeId
Where   Id In (
    Select  O.Id 
    From    Orders As O 
    Join    OrderDetails As OD 
    On      O.Id = OD.OrderId 
            And OD.ProductInformationId = @ProductInformationId 
    Where   O.OrderTypeId = @SoureOrderTypeId
)";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@ProductInformationId", convertRequest.ProductInformationId);
                        sqlCommand.Parameters.AddWithValue("@TargetOrderTypeId", convertRequest.Available ? "OS" : "TE");
                        sqlCommand.Parameters.AddWithValue("@SoureOrderTypeId", convertRequest.Available ? "TE" : "OS");

                        sqlConnection.Open();

                        response.Data = sqlCommand.ExecuteNonQuery();
                    }
                }

                sqlConnection.Close();

                response.Success = true;
                response.Message = $"{response.Data} sipariş güncellendi.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public Response MakeShipped(List<int> ids)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = $@"
Update  Orders 
Set     OrderTypeId = 'KTE'
Where   TenantId = @TenantId
        And DomainId = @DomainId
        And CompanyId = @CompanyId
        And Id In (${string.Join(",", ids)})";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                        sqlConnection.Open();

                        response.Success = sqlCommand.ExecuteNonQuery() == ids.Count;
                        response.Message = $"Siparişler başarılı bir şekilde güncellendi.";
                    }
                }

                sqlConnection.Close();
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public DataResponse<Order> Detail(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Order>>((response) =>
            {
                var dto = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => o.Id == id)
                    .Select(o => new Order
                    {
                        Id = o.Id,
                        MarketPlace = o.Marketplace != null
                            ? new Marketplace
                            {
                                Name = o.Marketplace.Name
                            }
                            : null,
                        MarketplaceOrderNumber = o.MarketplaceOrderNumber,
                        Customer = new Customer
                        {
                            Name = o.Customer.Name,
                            Surname = o.Customer.Surname,
                            CustomerContact = new CustomerContact
                            {
                                Mail = o.Customer.CustomerContact.Mail,
                                Phone = o.Customer.CustomerContact.Phone
                            }
                        },
                        OrderDetails = o.OrderDetails.Select(od => new OrderDetail
                        {
                            Id = od.Id,
                            Payor = od.Payor,
                            ProductInformationId = od.ProductInformationId,
                            Quantity = od.Quantity,
                            ProductInformation = new ProductInformation
                            {
                                ProductInformationName = $"{od
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .Name} {od
                                    .ProductInformation
                                    .ProductInformationTranslations
                                    .FirstOrDefault(pit => pit.LanguageId == "TR")
                                    .VariantValuesDescription}",

                                ProductInformationPhotos = new List<ProductInformationPhoto>
                                {
                                    new ProductInformationPhoto
                                    {
                                        FileName = $"{this._settingService.ProductImageUrl}/{od
                                            .ProductInformation
                                            .ProductInformationPhotos
                                            .FirstOrDefault()
                                            .FileName}"
                                    }
                                }
                            }
                        })
                        .ToList(),
                        OrderReturnDetails = o.OrderReturnDetails.Select(od => new OrderReturnDetail
                        {
                            Id = od.Id,
                            ProductInformationId = od.ProductInformationId,
                            Quantity = od.Quantity,
                            IsLoss = od.IsLoss,
                            IsReturn = od.IsReturn
                        })
                        .ToList()
                    })
                    .FirstOrDefault();

                if (dto == null)
                {
                    response.Message = $"Sipariş bulunamadı.";
                    response.Success = false;
                    return;
                }

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Return(Order dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => o.Id == dto.Id)
                    .Include(o => o.OrderReturnDetails)
                    .Include(o => o.OrderBilling)
                    .FirstOrDefault();

                if (entity.OrderTypeId == "IP")
                {
                    response.Success = false;
                    response.Message = "İptal sipariş iade edilemez.";
                    return;
                }

                if (this._settingService.IllegalSale)
                {
                    entity.OrderBilling ??= new();
                }
                else
                {
                    if (entity.OrderBilling == null)
                    {
                        response.Success = false;
                        response.Message = "Faturası kesilmeyen sipariş iade edilemez.";
                        return;
                    }
                }

                entity.OrderBilling.CreatedDate = dto.CreatedDate;
                entity.OrderBilling.ReturnDescription = dto.OrderBilling.ReturnDescription;
                entity.OrderReturnDetails = dto
                    .OrderReturnDetails
                    .Where(ord => ord != null && (ord.IsReturn || ord.IsLoss))
                    .GroupBy(ord => new { dto.Id, ord.ProductInformationId, ord.IsReturn, ord.IsLoss })
                    .Select(gord => new Domain.Entities.OrderReturnDetail
                    {
                        OrderId = dto.Id,
                        ProductInformationId = gord.Key.ProductInformationId,
                        Quantity = gord.Count(),
                        IsLoss = gord.Key.IsLoss,
                        IsReturn = gord.Key.IsReturn
                    })
                    .ToList();

                entity.OrderTypeId = "ID";

                response.Success = this
                  ._unitOfWork
                  .OrderRepository
                  .Update(entity);
                response.Message = "İade başarılı bir şekilde kaydedildi.";
            });
        }

        public Response DeletePicking(Order order)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var count = 0;
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Delete OrderPickings where Id=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId

Delete OrderPackings where Id=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", order.Id);
                        sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                        sqlConnection.Open();

                        count = sqlCommand.ExecuteNonQuery();
                    }
                }

                sqlConnection.Close();

                response.Success = true;
                response.Message = $"{count} sipariş güncellendi.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }


        public Response DeleteInvoice(Order order)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var count = 0;
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
update OrderInvoiceInformations set [Url]=null,[Guid]=null,[Number]=Number where Id=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId

update OrderBillings  set InvoiceNumber=null where Id=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId";


                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", order.Id);
                        sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                        sqlConnection.Open();

                        count = sqlCommand.ExecuteNonQuery();
                    }
                }

                sqlConnection.Close();

                response.Success = true;
                response.Message = $"{count} sipariş güncellendi.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public Response DeleteReturnInvoice(Order order)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var count = 0;
                using (sqlConnection = new(this._settingService.ConnectionString))
                {

                    var query = @"
update OrderBillings  set ReturnNumber=null,ReturnDescription=null where Id=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId

Delete OrderReturnDetails where OrderId=@Id And TenantId=@TenantId and DomainId=@DomainId and CompanyId=@CompanyId";


                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", order.Id);
                        sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                        sqlConnection.Open();

                        count = sqlCommand.ExecuteNonQuery();
                    }
                }

                sqlConnection.Close();

                response.Success = true;
                response.Message = $"{count} sipariş'in gider pusulası silindi.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }

        public Response EmptyPackerBarcode(Order order)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var count = 0;
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Update OrderPickings Set PackerBarcode = Null Where Id = @Id And TenantId = @TenantId and DomainId = @DomainId and CompanyId = @CompanyId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", order.Id);
                        sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                        sqlConnection.Open();

                        count = sqlCommand.ExecuteNonQuery();
                    }
                }

                sqlConnection.Close();

                response.Success = true;
                response.Message = $"Sepet silindi.";
            }, (response, exception) =>
            {

                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

            });
        }


        private List<OrderDetail> GetBulkOrders(string search, string pagedShort)
        {

            var parameters = search.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);




            var photos = new List<ProductInformationPhoto>()
                {
                    new ProductInformationPhoto
                    {
                        FileName="/org/img/product/0/no-image.jpg"
                    }
                };

            var iQueryable = this
                 ._unitOfWork
                .OrderRepository
                .DbSet()
               .Where(x => x.OrderTypeId == "OS");




            foreach (var pLoop in parameters)
            {

                if (pLoop.StartsWith("Marketplace:"))
                {
                    var value = pLoop.Replace("Marketplace:", "");
                    if (value.Length > 0 && value != "-1")
                        iQueryable = iQueryable.Where(iq => iq.MarketplaceId == value);
                }
                else if (pLoop.StartsWith("OrderPrepation:"))
                {
                    var value = pLoop.Replace("OrderPrepation:", "");
                    if (value.Length > 0 && value != "-1")
                    {

                        if (value == "1") //Tekli
                        {
                            iQueryable = iQueryable.Where(x => x.OrderDetails.Count(y => y.Quantity == 1) == 1);
                        }
                        else if (value == "2") //Çoklu
                        {
                            iQueryable = iQueryable.Where(x => x.OrderDetails.Count(y => y.Quantity > 1) == 1 || x.OrderDetails.Count > 1);
                        }
                    }
                }
                else if (pLoop.StartsWith("Date:"))
                {
                    var value = pLoop.Replace("Date:", "");
                    if (!string.IsNullOrEmpty(value))
                    {
                        var dates = value.Split(new string[] { "/" }, System.StringSplitOptions.RemoveEmptyEntries);
                        var startDate = DateTime.Parse(dates[0].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                        var endDate = DateTime.Parse(dates[1].Trim(), new System.Globalization.CultureInfo("tr-TR"));
                        iQueryable = iQueryable.Where(iq => iq.OrderDate.Date >= startDate && iq.OrderDate.Date <= endDate.Date);
                    }
                }

                else if (pLoop.StartsWith("ProductCategoryPrepation:"))
                {
                    var value = pLoop.Replace("ProductCategoryPrepation:", "");
                    if (value.Length > 0 && value != "-1")
                    {
                        if (value == "1") //Aynı Kategori Ürün
                        {
                            iQueryable = iQueryable.Where(x => x.OrderDetails.GroupBy(g => g.ProductInformation.Product.CategoryId).Count() == 1);
                        }
                        else if (value == "2") //Farklı Kategori Ürün
                        {
                            iQueryable = iQueryable.Where(x => x.OrderDetails.GroupBy(g => g.ProductInformation.Product.CategoryId).Count() > 1);
                        }
                    }
                }

            }





            var iQueryableOrderDetails = iQueryable.SelectMany(x => x.OrderDetails)
                   .GroupBy(x => x.ProductInformationId)
                      .Select(x => new OrderDetail
                      {
                          Order = new Order
                          {
                              OrderDate = x.OrderBy(od => od.Order.OrderDate).First().Order.OrderDate,
                          },
                          ProductInformationId = x.Key,
                          ProductInformation = new ProductInformation
                          {
                              Barcode = x.First().ProductInformation.Barcode,
                              StockCode = x.First().ProductInformation.StockCode,
                              Stock = x.First().ProductInformation.Stock,
                              ShelfCode = x.First().ProductInformation.ShelfCode,
                              Product = new Product
                              {
                                  SellerCode = x.First().ProductInformation.Product.SellerCode,
                                  CategoryId = x.First().ProductInformation.Product.CategoryId,
                              },
                              ProductInformationName = $"{x.First()
                                          .ProductInformation
                                          .ProductInformationTranslations
                                          .FirstOrDefault(pit => pit.LanguageId == "TR")
                                          .Name} ",
                              VariantValuesDescription =
                                  x.First()
                                          .ProductInformation
                                          .ProductInformationTranslations
                                          .FirstOrDefault(pit => pit.LanguageId == "TR")
                                          .VariantValuesDescription,
                              ProductInformationPhotos = x.First()
                                                  .ProductInformation
                                                  .ProductInformationPhotos
                                                  .Where(pip => !pip.Deleted).Count() > 0 ? x.First()
                                          .ProductInformation
                                          .ProductInformationPhotos
                                          .Where(pip => !pip.Deleted)
                                          .Select(pip => new ProductInformationPhoto
                                          {
                                              FileName = $"{this._settingService.ProductImageUrl}/{pip.FileName}"
                                          })
                                          .ToList() : photos
                          },
                          Quantity = x.Sum(y => y.Quantity)

                      });



            foreach (var pLoop in parameters)
            {
                if (pLoop.StartsWith("ShelfCode:"))
                {
                    var value = pLoop.Replace("ShelfCode:", "");
                    if (value.Length > 0)
                        iQueryableOrderDetails = iQueryableOrderDetails.Where(iq => iq.ProductInformation.ShelfCode.Contains(value));
                }
                else if (pLoop.StartsWith("StockCode:"))
                {
                    var value = pLoop.Replace("StockCode:", "");
                    if (value.Length > 0)
                        iQueryableOrderDetails = iQueryableOrderDetails.Where(iq => iq.ProductInformation.StockCode.Contains(value));
                }
                else if (pLoop.StartsWith("Barcode:"))
                {
                    var value = pLoop.Replace("Barcode:", "");
                    if (value.Length > 0)
                        iQueryableOrderDetails = iQueryableOrderDetails.Where(iq => iq.ProductInformation.Barcode.Contains(value));
                }
                else if (pLoop.StartsWith("ModelCode:"))
                {
                    var value = pLoop.Replace("ModelCode:", "");
                    if (value.Length > 0)
                        iQueryableOrderDetails = iQueryableOrderDetails.Where(iq => iq.ProductInformation.Product.SellerCode.Contains(value));
                }
                else if (pLoop.StartsWith("CategoryId:"))
                {
                    var value = pLoop.Replace("CategoryId:", "");
                    if (value != "-1")
                        iQueryableOrderDetails = iQueryableOrderDetails.Where(iq => iq.ProductInformation.Product.CategoryId == System.Convert.ToInt32(value));
                }
            }

            if (!string.IsNullOrEmpty(pagedShort))
            {
                if (pagedShort == "OrderDateAsc")
                    iQueryableOrderDetails = iQueryableOrderDetails.OrderBy(o => o.Order.OrderDate).ThenByDescending(x => x.Quantity);
                else if (pagedShort == "OrderDateDesc")
                    iQueryableOrderDetails = iQueryableOrderDetails.OrderByDescending(o => o.Order.OrderDate).ThenByDescending(x => x.Quantity);
                else if (pagedShort == "SellerCodeAsc")
                    iQueryableOrderDetails = iQueryableOrderDetails.OrderBy(o => o.ProductInformation.Product.SellerCode).ThenByDescending(x => x.Quantity);
                else if (pagedShort == "SellerCodeDesc")
                    iQueryableOrderDetails = iQueryableOrderDetails.OrderByDescending(o => o.ProductInformation.Product.SellerCode).ThenByDescending(x => x.Quantity);

            }




            var orderDetails = iQueryableOrderDetails.ToList();

            return orderDetails;
        }
        #endregion
    }
}