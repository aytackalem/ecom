﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Panel.Services
{
    public class MarketplaceCompanyService : IMarketplaceCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceCompanyService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<MarketplaceCompany>>> ReadAsync()
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<MarketplaceCompany>>>(async response =>
            {
                var entities = await this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Include(x => x.MarketplaceConfigurations)
                    .ToListAsync();

                if (entities.Count == 0)
                {
                    response.Message = $"Herhangi bir pazaryeri entegrasyonu bulunamadı.";
                    response.Success = false;
                    return;
                }

                response.Data = entities
                    .Select(e => new MarketplaceCompany
                    {
                        Id = e.Id,
                        MarketplaceId = e.MarketplaceId,
                        Name = e.Marketplace.Name,
                        Active = e.Active,
                        CreateProduct = e.CreateProduct,
                        ReadOrder = e.ReadOrder,
                        UpdatePrice = e.UpdatePrice,
                        UpdateStock = e.UpdateStock,
                        IsEcommerce = e.Marketplace.IsECommerce
                    })
                    .ToList();
                response.Success = true;
            });
        }

        public DataResponse<MarketplaceCompany> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<MarketplaceCompany>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Include(x => x.MarketplaceConfigurations)
                    .FirstOrDefault(x => x.Id == id);

                if (entity == null)
                {
                    response.Message = $"Pazaryeri bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new MarketplaceCompany
                {
                    Id = entity.Id,
                    MarketplaceId = entity.MarketplaceId,
                    Name = entity.Marketplace.Name,
                    Active = entity.Active,
                    CreateProduct = entity.CreateProduct,
                    ReadOrder = entity.ReadOrder,
                    UpdatePrice = entity.UpdatePrice,
                    UpdateStock = entity.UpdateStock,
                    IsEcommerce = entity.Marketplace.IsECommerce,
                    MarketplaceConfigurations = entity.MarketplaceConfigurations.Select(x => new MarketplaceConfiguration
                    {
                        Id = x.Id,
                        Key = x.Key,
                        Value = x.Value
                    }).ToList()
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<MarketplaceCompany> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<MarketplaceCompany>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Count();

                response.Data = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Where(c => (string.IsNullOrEmpty(pagedRequest.Search) || c.Marketplace.Name.Contains(pagedRequest.Search)))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(e => new MarketplaceCompany
                    {
                        Id = e.Id,
                        MarketplaceId = e.MarketplaceId,
                        Name = e.Marketplace.Name,
                        Active = e.Active,
                        CreateProduct = e.CreateProduct,
                        ReadOrder = e.ReadOrder,
                        UpdatePrice = e.UpdatePrice,
                        UpdateStock = e.UpdateStock,
                        IsEcommerce = e.Marketplace.IsECommerce
                    }).ToList();

                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(MarketplaceCompany dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.MarketplaceConfigurations)
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (entity == null)
                {
                    response.Message = $"Pazaryeri bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Active = dto.Active;
                entity.UpdatePrice = dto.UpdatePrice;
                entity.UpdateStock = dto.UpdateStock;
                entity.ReadOrder = dto.ReadOrder;
                entity.CreateProduct = dto.CreateProduct;
                if (dto.MarketplaceConfigurations != null)
                    dto.MarketplaceConfigurations.ForEach(mc =>
                    {
                        var marketplaceConfiguration = entity.MarketplaceConfigurations.FirstOrDefault(x => x.Id == mc.Id);
                        if (marketplaceConfiguration != null)
                        {
                            marketplaceConfiguration.Value = mc.Value;
                        }
                    });

                var updated = this
                    ._unitOfWork.MarketplaceCompanyRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Message = "Pazaryeri güncellendi.";
            }, (response, exception) => { });
        }
        #endregion
    }
}