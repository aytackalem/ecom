﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Panel.Services
{
    public class SupplierService : ISupplierService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public SupplierService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Supplier> Create(Supplier dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Supplier>>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .SupplierRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir tedarikçi oluşturulduğundan yenisi oluşturulamaz.";
                    return;
                }

                var entity = new Domain.Entities.Supplier
                {
                    Name = dto.Name,
                    
                };

                var created = this
                    ._unitOfWork.SupplierRepository
                    .Create(entity);

                if (!created)
                {
                    response.Success = false;
                    return;
                }

                dto.Id = entity.Id;

                response.Data = dto;
                response.Message = "Tedarikçi oluşturuldu.";
                response.Success = true;
            }, (response, exception) => { });
        }

        public PagedResponse<Supplier> Read(PagedRequest pagedRequest)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<Supplier>>((response) =>
            {
                response.Page = pagedRequest.Page;
                response.PageRecordsCount = pagedRequest.PageRecordsCount;
                response.RecordsCount = this
                    ._unitOfWork
                    .SupplierRepository
                    .DbSet()
                    .Where(s => string.IsNullOrEmpty(pagedRequest.Search) || s.Name.Contains(pagedRequest.Search))
                    .Count();
                response.Data = this
                    ._unitOfWork
                    .SupplierRepository
                    .DbSet()
                    .Where(s => string.IsNullOrEmpty(pagedRequest.Search) || s.Name.Contains(pagedRequest.Search))
                    .Skip(pagedRequest.PageRecordsCount * pagedRequest.Page)
                    .Take(pagedRequest.PageRecordsCount)
                    .Select(b => new Supplier
                    {
                        Id = b.Id,
                        Name = b.Name
                    }).ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<Supplier> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Supplier>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .SupplierRepository
                    .Read(id);

                if (entity == null)
                {
                    response.Message = $"Tedarikçi bulunamadı.";
                    response.Success = false;
                    return;
                }

                var dto = new Supplier
                {
                    Id = entity.Id,
                    Name = entity.Name
                };

                response.Data = dto;
                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<List<KeyValue<int, string>>> ReadAsKeyValue()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<int, string>>>>((response) =>
            {
                response.Data = this
                    ._unitOfWork
                    .SupplierRepository
                    .DbSet()
                    .Select(b => new KeyValue<int, string>
                    {
                        Key = b.Id,
                        Value = b.Name
                    })
                    .ToList();
                response.Success = true;
            }, (response, exception) => { });
        }

        public Response Update(Supplier dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var hasAnySameName = this
                     ._unitOfWork
                     .SupplierRepository
                     .DbSet()
                     .Any(b => b.Name == dto.Name && b.Id != dto.Id);

                if (hasAnySameName)
                {
                    response.Success = false;
                    response.Message = "Bu isimle daha önce bir tedaarikçi oluşturulduğundan tedaarikçi güncellenemez.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .SupplierRepository
                    .Read(dto.Id);

                if (entity == null)
                {
                    response.Message = $"{dto.Name} ({dto.Id}) tedarikçisi bulunamadı.";
                    response.Success = false;
                    return;
                }

                entity.Name = dto.Name;

                var updated = this
                    ._unitOfWork.SupplierRepository
                    .Update(entity);

                if (!updated)
                {
                    response.Success = false;
                    return;
                }

                response.Message = "Tedarikçi güncellendi.";
                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}
