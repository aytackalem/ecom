﻿using Helpy.Shared.Response.Base;

namespace Helpy.Shared.Response
{
    public class DataResponse<T> : ResponseBase
    {
        #region Properties
        public T Data { get; set; }
        #endregion
    }
}