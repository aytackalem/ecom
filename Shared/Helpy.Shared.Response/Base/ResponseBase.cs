﻿namespace Helpy.Shared.Response.Base
{
    public abstract class ResponseBase
    {
        #region Properties
        public int Code { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }
        #endregion
    }
}
