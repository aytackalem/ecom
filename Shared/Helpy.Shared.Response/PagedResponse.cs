﻿using Helpy.Shared.Response.Base;

namespace Helpy.Shared.Response
{
    public class PagedResponse<T> : ResponseBase
    {
        #region Properties
        public List<T> Data { get; set; }

        public int Count => this.Data?.Count ?? 0;

        public int Page { get; set; }

        /// <summary>
        /// Bir sayfada goruntulenecek adet bilgisi.
        /// </summary>
        public int PageRecordsCount { get; set; }

        /// <summary>
        /// Ilgili kayitlara iliskin tum adet bilgisi.
        /// </summary>
        public long RecordsCount { get; set; }

        public int PagesCount => this.RecordsCount == 0 ? 0 : Convert.ToInt32(Math.Ceiling(this.RecordsCount / (decimal)this.PageRecordsCount));
        #endregion
    }
}