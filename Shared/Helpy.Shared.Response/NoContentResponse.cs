﻿using Helpy.Shared.Response.Base;

namespace Helpy.Shared.Response
{
    public class NoContentResponse : ResponseBase
    {

    }
}