﻿using Helpy.Shared.Response.Base;

namespace Helpy.Shared.ExceptionHandling
{
    public static class ExceptionHandler
    {
        #region Methods
        public static TResponse Handle<TResponse>(Action<TResponse> action, Action<TResponse> catchAction = null) where TResponse : ResponseBase, new()
        {
            TResponse response = new();
            try
            {
                action(response);
            }
            catch (Exception e)
            {
                if (catchAction == null)
                {
                    response.Success = false;
                    response.Message = "Bilinmeyen bir hata oluştu.";
                    response.Exception = e;
                }
                else
                    catchAction(response);
            }
            return response;
        }

        public static async Task<TResponse> HandleAsync<TResponse>(Func<TResponse, Task> action, Action<TResponse> catchAction = null) where TResponse : ResponseBase, new()
        {
            TResponse response = new();
            try
            {
                await action(response);
            }
            catch (Exception e)
            {
                if (catchAction == null)
                {
                    response.Success = false;
                    response.Message = "Bilinmeyen bir hata oluştu.";
                    response.Exception = e;
                }
                else
                    catchAction(response);
            }
            return response;
        }

        public static async Task<TResponse> HandleTryAsync<TResponse>(Func<TResponse, Task> action, int tryCount, int tryWaitSeconds, Action<TResponse> catchAction = null) where TResponse : ResponseBase, new()
        {
            TResponse response = new();
            for (int i = 0; i < tryCount; i++)
            {
                Console.WriteLine($"Try {i}");

                if (i > 0)
                    Thread.Sleep(tryWaitSeconds * 1000);

                try
                {
                    await action(response);

                    if (response.Success)
                    {
                        Console.WriteLine($"Try {i} success");
                        break;
                    }
                }
                catch (Exception e)
                {
                    if (catchAction == null)
                    {
                        response.Success = false;
                        response.Message = "Bilinmeyen bir hata oluştu.";
                        response.Exception = e;
                    }
                    else
                        catchAction(response);
                }
            }
            return response;
        }
        #endregion
    }
}
