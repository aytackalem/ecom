﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;

namespace ECommerce.Job.MarketplaceExporter
{
    public class Exporter
    {
        #region Fields
        private readonly MarketplaceRequestExporterResolver _marketplaceRequestExporterResolver;
        #endregion

        #region Constructors
        public Exporter(MarketplaceRequestExporterResolver marketplaceRequestExporterResolver)
        {
            #region Fields
            this._marketplaceRequestExporterResolver = marketplaceRequestExporterResolver;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(string[] args)
        {
            var queueId = int.Parse(args[0]);
            var requestType = args[1];
            var marketplaceId = args[2];

            var marketplaceRequestExporter = this._marketplaceRequestExporterResolver(requestType);
            await marketplaceRequestExporter.ExportAsync(new MarketplaceRequestExporterRequest
            {
                QueueId = queueId,
                MarketplaceId = marketplaceId
            });
        }
        #endregion
    }
}