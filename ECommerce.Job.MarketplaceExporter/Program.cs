﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Job.MarketplaceExporter.Helpers;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.MarketplacesV2;
using ECommerce.Persistence.Common.MarketplacesV2.Exporter;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.MarketplaceExporter
{
    internal class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IConfiguration>(p => configuration);

            serviceCollection.AddSingleton<IDbService, DbService>();

            serviceCollection.AddDbContext<ApplicationDbContext>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ITenantFinder>(tf => null);

            serviceCollection.AddScoped<IDomainFinder>(df => null);

            serviceCollection.AddScoped<ICompanyFinder>(cf => null);

            serviceCollection.AddScoped<IDbNameFinder, DbNameFinder>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddScoped<IMarketplaceErrorFacade, MarketplaceErrorFacade>();

            #region Marketplace Request Exporter Injections
            serviceCollection.AddTransient<MarketplaceRequestExporterCreateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestExporterUpdateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestExporterUpdatePrice>();

            serviceCollection.AddTransient<MarketplaceRequestExporterUpdateStock>();

            serviceCollection.AddTransient<MarketplaceRequestExporterResolver>(serviceProvider => requestType =>
            {
                IMarketplaceRequestExporter p = requestType switch
                {
                    "CreateProduct" => serviceProvider.GetService<MarketplaceRequestExporterCreateProduct>(),
                    "UpdateProduct" => serviceProvider.GetService<MarketplaceRequestExporterUpdateProduct>(),
                    "UpdatePrice" => serviceProvider.GetService<MarketplaceRequestExporterUpdatePrice>(),
                    "UpdateStock" => serviceProvider.GetService<MarketplaceRequestExporterUpdateStock>(),
                    _ => null
                };
                return p;
            });
            #endregion

            serviceCollection.AddTransient<IMarketplaceRequestWriter, MarketplaceRequestWriter>();

            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddScoped<Exporter>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            #region Checks
#if DEBUG
            args = new string[] { "1", "UpdatePrice", "BY" };
#endif

            if (args.Length != 3)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Please check jos argumants.", new string[] { });
                return;
            }

            if (int.TryParse(args[0], out int i) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "UpdatePrice", "UpdateStock", "CreateProduct", "UpdateProduct" }.Contains(args[1]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "CS", "HB", "M", "TY", "TYE", "PA", "N11", "PTT", "IK", "IS", "MN", "ML", "N11V2", "TSoft", "LCW", "BY" }.Contains(args[2]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Checker", "Please check jos argumants.", new string[] { });
                return;
            }
            #endregion

            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);

            var dbNames = new List<string>();

            using (var scope = serviceProvider.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = await dbService.GetNamesAsync();
                if (getNamesResponse.Success == false)
                {
                    SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Db names not fetched.");
                    return;
                }

                dbNames = getNamesResponse.Data;
            }

            if (dbNames.HasItem())
                foreach (var theDbName in dbNames)
                {
#if DEBUG
                    if (theDbName != "Mizalle")
                        continue;
#endif

                    using (var scope = serviceProvider.CreateScope())
                    {
                        var dbNameFinder = scope.ServiceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(theDbName);


                        await scope.ServiceProvider.GetRequiredService<Exporter>().RunAsync(args);
                    }
                }
        }
        #endregion
    }
}