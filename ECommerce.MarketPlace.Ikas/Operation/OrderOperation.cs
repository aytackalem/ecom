﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Base;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Ikas.Common;
using Newtonsoft.Json;

namespace ECommerce.MarketPlace.Ikas.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private Token _token = null;

        private readonly string _tokenUrl = "https://entegre.myikas.com/api/admin/oauth/token";

        private readonly string _graphQlUrl = "https://api.myikas.com/api/v1/admin/graphql";
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> Get(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                GetToken(request);

                var page = 0;
                var orders = new List<Order>();
                OrderPaginationResponse orderPaginationResponse = null;

                do
                {
                    var queryObject = new QueryObject
                    {
                        Query = @$"
{{listOrder(status: {{ eq: CREATED }}, page: {page}){{
        count,
        data{{
            id,
            customerId
            customer{{
                id
                firstName
                lastName
            }}
            orderPackages{{
                id
                orderPackageNumber
            }}
            orderLineItems{{
                id,
                finalPrice,
                originalOrderLineItemId,
                variant{{
                    name,
                    barcodeList
                    sku
                    variantValues{{
                        variantTypeId
                        variantTypeName
                        variantValueId
                        variantValueName
                    }}
                }}
            }}
        }},
        hasNext,
        limit,
        page
    }}
}}
"
                    };

                    orderPaginationResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListOrderReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this._token.AccessToken).Data.ListOrder;

                    orders.AddRange(orderPaginationResponse.Data);

                    page++;
                } while (orderPaginationResponse != null && orderPaginationResponse.Page < page);

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @$"
            mutation {{
                updateOrderPackageStatus(input: {{
                    orderId: {""},
                    packages: [{""}]
                }})
            }}"
                };

                var responseString = _httpHelper.Post<QueryObject, dynamic>(
                    queryObject,
                    this._graphQlUrl,
                    "Bearer",
                    this._token.AccessToken);
            });
        }
        #endregion

        #region Helper Methods
        private void GetToken(IConfigurableRequest request)
        {
            var clientId = request.Configurations["ClientId"];
            var clientSecret = request.Configurations["ClientSecret"];

#if DEBUG
            //clientId = "18a5b1e2-6ab1-4cf8-8d32-b73eff7714c5";
            //clientSecret = "s_1yp8LwVYjxO9Fu2tyeFIM7f12295156bceae46dfbc15cf2a4ae2bcb3";
#endif

            this._token ??= this
                    ._httpHelper
                    .Post<Token>(
                        _tokenUrl,
                        new List<KeyValuePair<string, string>> {
                            new KeyValuePair<string, string>("grant_type", "client_credentials"),
                            new KeyValuePair<string, string>("client_id", clientId),
                            new KeyValuePair<string, string>("client_secret", clientSecret)
                        });
        }
        #endregion
    }
}