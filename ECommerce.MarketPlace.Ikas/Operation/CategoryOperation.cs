﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request.Base;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Ikas.Common;

namespace ECommerce.MarketPlace.Ikas.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private Token _token = null;

        private readonly string _tokenUrl = "https://entegre.myikas.com/api/admin/oauth/token";

        private readonly string _graphQlUrl = "https://api.myikas.com/api/v1/admin/graphql";
        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                GetToken(configurations);

                var queryObject = new QueryObject
                {
                    Query = @"
{
    listCategory{
        id
        name
    }
}"
                };

                var listCategoryResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListCategoryReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this._token.AccessToken).Data.ListCategory;

                queryObject = new QueryObject
                {
                    Query = @"
{
    listVariantType{
        id
        name,
        values{
            id
            name
        }
    }
}"
                };

                var listVariantTypeResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListVariantTypeReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this._token.AccessToken).Data.ListVariantType;

                response.Data = new();

                listCategoryResponse.ForEach(cLoop =>
                {
                    response.Data.Add(new CategoryResponse
                    {
                        Code = cLoop.Id,
                        Name = cLoop.Name,
                        CategoryAttributes = listVariantTypeResponse
                            .Select(v => new CategoryAttributeResponse
                            {
                                Code = v.Id,
                                Name = v.Name,
                                Mandatory = true,
                                AllowCustom = false,
                                MultiValue = false,
                                CategoryCode = cLoop.Id,
                                CategoryAttributesValues = v
                                    .Values
                                    .Select(vv => new CategoryAttributeValueResponse
                                    {
                                        Code = vv.Id,
                                        Name = vv.Name,
                                        VarinatCode = v.Id
                                    })
                                    .ToList()
                            })
                            .ToList()
                    });
                });

                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private void GetToken(Dictionary<string, string> configurations)
        {
            var clientId = configurations["ClientId"];
            var clientSecret = configurations["ClientSecret"];

#if DEBUG
            //clientId = "18a5b1e2-6ab1-4cf8-8d32-b73eff7714c5";
            //clientSecret = "s_1yp8LwVYjxO9Fu2tyeFIM7f12295156bceae46dfbc15cf2a4ae2bcb3";
#endif

            this._token ??= this
                    ._httpHelper
                    .Post<Token>(
                        _tokenUrl,
                        new List<KeyValuePair<string, string>> {
                            new KeyValuePair<string, string>("grant_type", "client_credentials"),
                            new KeyValuePair<string, string>("client_id", clientId),
                            new KeyValuePair<string, string>("client_secret", clientSecret)
                        });
        }
        #endregion
    }
}
