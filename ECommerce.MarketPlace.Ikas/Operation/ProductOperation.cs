﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Base;
using ECommerce.MarketPlace.Ikas.Common;
using Microsoft.VisualBasic;
using Newtonsoft.Json;

namespace ECommerce.MarketPlace.Ikas.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;

        private Token _token = null;

        private readonly string _tokenUrl = "https://entegre.myikas.com/api/admin/oauth/token";

        private readonly string _graphQlUrl = "https://api.myikas.com/api/v1/admin/graphql";

        private readonly string _imageUploadUrl = "https://api.myikas.com/api/v1/admin/product/upload/image";
        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                GetToken(productRequest);




                List<string> productVariantTypes = new();
                var groupedProductVariantTypes = productRequest
                     .ProductItem
                     .StockItems
                     .SelectMany(si => si.Attributes.OrderByDescending(x => x.AttributeName.ToLower() == "renk").Select(a => new
                     {
                         variantTypeId = a.AttributeCode,
                         variantValueId = a.AttributeValueCode
                     }))
                     .GroupBy(n => n.variantTypeId);

                int order = 1;
                foreach (var variantType in groupedProductVariantTypes)
                {
                    productVariantTypes.Add($@"{{
    order: {order},
    variantTypeId: ""{variantType.Key}"",
    variantValueIds: [{string.Join(",", variantType.GroupBy(x => x.variantValueId).Select(x => $"\"{x.Key}\""))}]
}}");
                    order++;
                }

                var queryObject = new QueryObject
                {
                    Query = @$"
mutation {{
    saveProduct(input: {{
        id: ""{productRequest.ProductItem.UUId.ToLower()}"",
        name: ""{productRequest.ProductItem.Name}"",
        type: PHYSICAL,
        brandId: ""{productRequest.ProductItem.BrandId}"",
        categoryIds: [""{productRequest.ProductItem.CategoryId}""],
        groupVariantsByVariantTypeId:""{groupedProductVariantTypes.FirstOrDefault()?.Key}""
        description: ""{productRequest.ProductItem.StockItems.FirstOrDefault()?.Description}"",
        productVariantTypes:[{string.Join(",", productVariantTypes)}],
        variants:[{string.Join(",", productRequest.ProductItem.StockItems.Select(si => @$"
{{
    id: ""{si.UUId.ToLower()}"",
    isActive: true,
    barcodeList: [""{si.Barcode}""],
    sku: ""{si.StockCode}"",
    variantValueIds: [{string.Join(",", si.Attributes.Select(a => $@"{{
        variantTypeId: ""{a.AttributeCode}""
        variantValueId: ""{a.AttributeValueCode}""
    }}"))}],
    prices:[{{
        sellPrice: {si.ListPrice},
        {(si.SalePrice < si.ListPrice ? $"discountPrice: {si.SalePrice}" : "")}
    }}]
}}"))}]
    }}) {{
        id,
        variants {{
            id,
        images{{
        imageId
              }}
        }}
    }}
}}"
                };

                var product = _httpHelper
                    .Post<QueryObject, SaveProductResponse>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this._token.AccessToken);

            foreach (var theVariant in product.data.saveProduct.variants)
            {
                foreach (var theImage in productRequest.ProductItem.StockItems[product.data.saveProduct.variants.IndexOf(theVariant)].Images)
                {
                    var body = $@"
{{
   ""productImage"": {{
        ""variantIds"": [""{theVariant.Id}""],
        ""url"": ""{theImage.Url}"",
        ""order"": ""{theImage.Order}"",
        ""isMain"": {(productRequest.ProductItem.StockItems[product.data.saveProduct.variants.IndexOf(theVariant)].Images.IndexOf(theImage) == 0 ? "true" : "false")}
    }}
}}";
                    _httpHelper
                        .Post(
                        this._imageUploadUrl,
                        body,
                        "Bearer",
                        this._token.AccessToken);
                }
                }
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @$"
            mutation {{
                saveVariantPrices(input: {{
                    variantPriceInputs: [{{
                        productId: ""{stockRequest.UUId}""
                        price: [{{
                            sellPrice: {stockRequest.PriceStock.ListPrice},
                            discountPrice: {stockRequest.PriceStock.SalePrice}
                        }}],
                        variantId: ""{stockRequest.PriceStock.UUId}""    
                    }}]
                }})
            }}"
                };

                var responseString = _httpHelper.Post<QueryObject, dynamic>(
                    queryObject,
                    this._graphQlUrl,
                    "Bearer",
                    this._token.AccessToken);
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @$"
            mutation {{
                saveProductStockLocations(input: {{
                    productStockLocationInputs: [{{
                        productId: ""{stockRequest.UUId}""
                        stockCount: {stockRequest.PriceStock.Quantity}
                        stockLocationId: ""8e47e405-c7d2-4639-b2bf-cb95cb268737""
                        variantId: ""{stockRequest.PriceStock.UUId}""    
                    }}]
                }})
            }}"
                };

                var responseString = _httpHelper.Post<QueryObject, dynamic>(
                    queryObject,
                    this._graphQlUrl,
                    "Bearer",
                    this._token.AccessToken);
            });
        }
        #endregion

        #region Helper Methods
        private void GetToken(IConfigurableRequest request)
        {
            var clientId = request.Configurations["ClientId"];
            var clientSecret = request.Configurations["ClientSecret"];

#if DEBUG
            //clientId = "18a5b1e2-6ab1-4cf8-8d32-b73eff7714c5";
            //clientSecret = "s_1yp8LwVYjxO9Fu2tyeFIM7f12295156bceae46dfbc15cf2a4ae2bcb3";
#endif

            this._token ??= this
                    ._httpHelper
                    .Post<Token>(
                        _tokenUrl,
                        new List<KeyValuePair<string, string>> {
                            new KeyValuePair<string, string>("grant_type", "client_credentials"),
                            new KeyValuePair<string, string>("client_id", clientId),
                            new KeyValuePair<string, string>("client_secret", clientSecret)
                        });
        }
        #endregion
    }
}