﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderLineItem
    {
        #region Properties
        public string Id { get; set; }

        public string OriginalOrderLineItemId { get; set; }

        public decimal FinalPrice { get; set; }
        #endregion

        #region Navigation Properties
        public OrderLineVariant Variant { get; set; }
        #endregion
    }
}
