﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderPackage
    {
        #region Properties
        public string Id { get; set; }

        public string orderPackageNumber { get; set; }
        #endregion
    }
}
