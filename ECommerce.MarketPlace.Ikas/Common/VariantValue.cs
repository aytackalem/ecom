﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class VariantValue
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
