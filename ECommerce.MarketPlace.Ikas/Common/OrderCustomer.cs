﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderCustomer
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }
    }
}
