﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderLineVariant
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public List<string> BarcodeList { get; set; }
        #endregion

        #region Navigation Properties
        public List<OrderLineVariantVariantValue> VariantValues { get; set; }
        #endregion
    }
}
