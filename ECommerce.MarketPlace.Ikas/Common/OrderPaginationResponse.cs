﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderPaginationResponse
    {
        #region Properties
        public int Count { get; set; }

        public bool HasNext { get; set; }

        public int Limit { get; set; }

        public int Page { get; set; }
        #endregion

        #region Navigation Properties
        public List<Order> Data { get; set; }
        #endregion
    }
}
