﻿using Newtonsoft.Json;

namespace ECommerce.MarketPlace.Ikas.Common
{
    public class QueryObject
    {
        #region Properties
        [JsonProperty("query")]
        public string Query { get; set; }
        #endregion
    }
}
