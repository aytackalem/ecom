﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class ListCategoryReponse
    {
        #region Properties
        public List<Category> ListCategory { get; set; }
        #endregion
    }
}
