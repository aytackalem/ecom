﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class Order
    {
        #region Properties
        public string Id { get; set; }

        public string CustomerId { get; set; }
        #endregion

        #region Navigation Properties
        public OrderCustomer Customer { get; set; }

        public List<OrderLineItem> OrderLineItems { get; set; }

        public List<OrderPackage> OrderPackages { get; set; }
        #endregion
    }
}
