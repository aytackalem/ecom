﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class ListOrderReponse
    {
        #region Properties
        public OrderPaginationResponse ListOrder { get; set; }
        #endregion
    }
}
