﻿namespace ECommerce.MarketPlace.Ikas.Common
{
    public class OrderLineVariantVariantValue
    {
        #region Properties
        public string VariantTypeId { get; set; }

        public string VariantTypeName { get; set; }

        public string VariantValueId { get; set; }

        public string VariantValueName { get; set; }
        #endregion
    }
}
