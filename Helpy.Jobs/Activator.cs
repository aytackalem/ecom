﻿namespace Helpy.Jobs
{
    public class Activator : Hangfire.JobActivator
    {
        private readonly IServiceProvider _serviceProvider;

        public Activator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public override object ActivateJob(Type jobType)
        {
            return _serviceProvider.GetService(jobType);
        }
    }
}
