using Hangfire;
using Helpy.Jobs.Common;
using MassTransit;

namespace Helpy.Jobs
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddMassTransit(c => {
                c.UsingRabbitMq((context, configure) =>
                {
                    configure.Host(builder.Configuration.GetConnectionString("RabbitMq"), "/", host =>
                    {
                        host.Username("guest");
                        host.Password("guest");
                    });
                });
            });

            builder.Services.AddControllers();

            var connectionString = builder.Configuration.GetConnectionString("Hangfire");
            builder.Services.AddHangfire(c => c.UseSqlServerStorage(connectionString));
            builder.Services.AddHangfireServer();

            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();
            builder.Services.AddScoped<IJob, Job>();

            var app = builder.Build();

            GlobalConfiguration.Configuration.UseColouredConsoleLogProvider();

            GlobalConfiguration.Configuration.UseActivator(new Activator(app.Services.GetRequiredService<IServiceProvider>()));

            var recurringJobManager = app.Services.GetRequiredService<IRecurringJobManager>();
            recurringJobManager
                .AddOrUpdate<IJob>(
                    "Job",
                    ii => ii.Run(),
                    Cron.Minutely());

            // Configure the HTTP request pipeline.

            app.UseHangfireDashboard();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}