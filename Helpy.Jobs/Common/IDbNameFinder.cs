﻿using Dapper;
using Microsoft.Data.SqlClient;

namespace Helpy.Jobs.Common
{
    public interface IDbNameFinder
    {
        List<string> Get();
    }

    public class DbNameFinder : IDbNameFinder
    {
        private readonly IConfiguration _configuration;

        public DbNameFinder(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<string> Get()
        {
            List<string> list = new();

            using (var conn = new SqlConnection(string.Format(_configuration.GetConnectionString("Helpy"), "master")))
            {
                var query = @"Select [name] From master.sys.databases Where database_id > 4 And [name] Not In ('IdentityServer', 'IdentityServer.Config', 'Lafaba_ECommerce', 'Marketplace')";

                list.AddRange(conn.Query<string>(query));
            }

            return list;
        }
    }
}
