﻿using Helpy.Jobs.Messages;
using MassTransit;

namespace Helpy.Jobs.Common
{
    public interface IJob
    {
        void Run();
    }

    public class Job : IJob
    {
        private readonly ISendEndpointProvider sendEndpointProvider;

        private readonly IDbNameFinder _nameFinder;

        private readonly IConfiguration _configuration;

        public Job(IDbNameFinder nameFinder, IConfiguration configuration, ISendEndpointProvider sendEndpointProvider)
        {
            _nameFinder = nameFinder;
            _configuration = configuration;
            this.sendEndpointProvider = sendEndpointProvider;
        }

        public void Run()
        {
            Console.WriteLine(_configuration.GetConnectionString("Helpy"));

            var list = _nameFinder.Get();
            foreach (var item in list)
            {
                var send = this.sendEndpointProvider.GetSendEndpoint(new("queue:Job")).Result;
                send.Send<JobMessage>(new()
                {
                    Name = item
                }).Wait();

                Console.WriteLine(item);
            }
        }
    }
}
