﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using N11ServiceReference;

namespace ECommerce.N11Fetch
{
    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static async Task Main(string[] args)
        {
            //TO DO: Get Products
            var products = await GetProductsAsync();

            //TO DO: Insert Products
            BulkProducts(products);

            //TO DO: Insert Categories
            BulkCategories(products);

            //TO DO: Insert Category Attributes
            BulkCategoryAttributes(products);

            //TO DO: Insert Category Attribute Values
            BulkCategoryAttributeValues(products);

            //TO DO: Insert Product Category Attribute Values
            BulkProductCategoryAttributeValue(products);
        }

        #region Products
        static List<string> ReadStockCodes()
        {
            var list = new List<string>();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand("Select StockCode From ProductInformationMarketplaces Where MarketPlaceId = 'N11'", sqlConnection))
                {
                    sqlConnection.Open();

                    var sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        list.Add(sqlReader.GetString(0));
                    }
                }
            }

            return list;
        }

        static async Task<List<Product>> GetProductsAsync()
        {
            var client = new ProductServicePortClient();

            var products = new List<Product>();

            var stockCodes = ReadStockCodes();

            foreach (var scLoop in stockCodes)
            {
                var response = await client.GetProductBySellerCodeAsync(new GetProductBySellerCodeRequest
                {
                    auth = new Authentication
                    {
                        appKey = "b0db755c-40ed-49e6-aee6-46f6b52c5002",
                        appSecret = "ZYxoDH8ZxFPh27ZB"
                    },
                    sellerCode = scLoop
                });

                if (response.GetProductBySellerCodeResponse.result.status == "success")
                    products.Add(response.GetProductBySellerCodeResponse.product);
            }

            return products;
        }

        static void BulkProducts(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("CategoryId", typeof(int));
            dataTable.Columns.Add("ProductSellerCode", typeof(string));
            dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("Subtitle", typeof(string));

            products
                .Select(p => new
                {
                    Id = p.id,
                    CategoryId = p.category.id,
                    ProductSellerCode = p.productSellerCode,
                    Title = p.title,
                    Subtitle = p.subtitle
                })
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.Id;
                    dataRow[1] = p.CategoryId;
                    dataRow[2] = p.ProductSellerCode;
                    dataRow[3] = p.Title;
                    dataRow[4] = p.Subtitle;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "N11Products";

                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                sqlBulkCopy.ColumnMappings.Add("CategoryId", "CategoryId");
                sqlBulkCopy.ColumnMappings.Add("ProductSellerCode", "ProductSellerCode");
                sqlBulkCopy.ColumnMappings.Add("Title", "Title");
                sqlBulkCopy.ColumnMappings.Add("Subtitle", "Subtitle");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
        #endregion

        static void BulkCategories(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .Select(p => new
                {
                    Id = p.category.id,
                    Name = p.category.name
                })
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.Id;
                    dataRow[1] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "N11Categories";

                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkCategoryAttributes(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("CategoryId", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .SelectMany(p => p.attributes.Select(a => new
                {
                    CategoryId = p.category.id,
                    Name = a.name
                }))
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.CategoryId;
                    dataRow[1] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "N11CategoryAttributes";

                sqlBulkCopy.ColumnMappings.Add("CategoryId", "CategoryId");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkCategoryAttributeValues(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("CategoryAttributeName", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .SelectMany(p => p.attributes.Select(a =>
                {
                    return new
                    {
                        CategoryAttributeName = a.name,
                        Name = a.value
                    };

                }))
                .Distinct()
                .ToList()
                .ForEach(a =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = a.CategoryAttributeName;
                    dataRow[1] = a.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "N11CategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeName", "CategoryAttributeName");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkProductCategoryAttributeValue(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("ProductSellerCode", typeof(string));
            dataTable.Columns.Add("CategoryAttributeValueName", typeof(string));

            products
                .SelectMany(p => p.attributes.Select(a => new
                {
                    ProductSellerCode = p.productSellerCode,
                    CategoryAttributeValueName = a.value
                }))
                .Distinct()
                .ToList()
                .ForEach(a =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = a.ProductSellerCode;
                    dataRow[1] = a.CategoryAttributeValueName;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "N11ProductCategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("ProductSellerCode", "ProductSellerCode");
                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeValueName", "CategoryAttributeValueName");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static async Task<N11CategoryServiceReference.CategoryProductAttributeData[]> FetchCategoryAttributesAsync(long categoryId)
        {
            var client = new N11CategoryServiceReference.CategoryServicePortClient();
            var response = await client.GetCategoryAttributesIdAsync(new N11CategoryServiceReference.GetCategoryAttributesIdRequest
            {
                auth = new N11CategoryServiceReference.Authentication
                {
                    appKey = "b0db755c-40ed-49e6-aee6-46f6b52c5002",
                    appSecret = "ZYxoDH8ZxFPh27ZB"
                },
                categoryId = categoryId
            });
            return response.GetCategoryAttributesIdResponse.categoryProductAttributeList;
        }

        static async Task<List<N11CategoryServiceReference.CategoryProductAttributeValueData>> FetchCategoryAttributeValuesAsync(long categoryProductAttributeId)
        {
            var client = new N11CategoryServiceReference.CategoryServicePortClient();

            var list = new List<N11CategoryServiceReference.CategoryProductAttributeValueData>();


            int page = 0;
            int pageSize = 0;

            do
            {
                var response = await client.GetCategoryAttributeValueAsync(new N11CategoryServiceReference.GetCategoryAttributeValueRequest
                {
                    auth = new N11CategoryServiceReference.Authentication
                    {
                        appKey = "b0db755c-40ed-49e6-aee6-46f6b52c5002",
                        appSecret = "ZYxoDH8ZxFPh27ZB"
                    },
                    categoryProductAttributeId = categoryProductAttributeId,
                    pagingData = new N11CategoryServiceReference.RequestPagingData
                    {
                        currentPage = 0,
                        pageSize = 100
                    }
                });

                if (response.GetCategoryAttributeValueResponse.result.status != "success")
                {

                }

                if (response.GetCategoryAttributeValueResponse.pagingData == null)
                {

                }

                pageSize = response.GetCategoryAttributeValueResponse.pagingData.pageSize.Value;

                page++;

                list.AddRange(response.GetCategoryAttributeValueResponse.categoryProductAttributeValueList);

            } while (page < pageSize);


            return list;
        }
    }
}