﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Shipment.Common;

namespace ECommerce.Shipment.Base
{
    public interface IShipment
    {
        #region Methods
        /// <summary>
        /// Aras kargo ile gönderilecek siparişi kaydeden fonksiyon.
        /// </summary>
        /// <param name="order">İlgili sipariş.</param>
        /// <param name="weight">Gönderiye ait desi bilgisi.</param>
        /// <param name="piece">Gönderinin kaç parça halinde gönderileceği bilgisi.</param>
        /// <returns></returns>
        DataResponse<ShipmentInfo> Post(Order order, bool payor, int[] weights, int piece);
        #endregion
    }
}
