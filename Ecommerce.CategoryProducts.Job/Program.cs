﻿using Ecommerce.CategoryProducts.Job.Base;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Marketplace;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.IO;

namespace Ecommerce.CategoryProducts.Job
{
    class Program
    {
        static void Main(string[] args)
        {

            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();

           

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("ECommerce"));
            });


            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddMemoryCache();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<IRandomHelper, RandomHelper>();
            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinder>();
            serviceCollection.AddScoped<IDbNameFinder, AppConfigDbNameFinder>();
            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();
            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();
            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();
            serviceCollection.AddScoped<IExcelHelper, OpenXmlExcelHelper>();
            serviceCollection.AddInfrastructureServices(configuration);
            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();
            serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();
            serviceCollection.AddSingleton<IConfiguration>(configuration);

            return serviceCollection.BuildServiceProvider();
        }
    }
}
