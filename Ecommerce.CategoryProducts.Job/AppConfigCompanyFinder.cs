﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace Ecommerce.CategoryProducts.Job
{
    public class AppConfigCompanyFinder : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Constructors
        public AppConfigCompanyFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _companyId;
        }

        public void Set(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
