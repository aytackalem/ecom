﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace Ecommerce.CategoryProducts.Job
{
    public class AppConfigTenantFinder : ITenantFinder
    {
        #region Fields
        public int _tenantId;
        #endregion

        #region Constructors
        public AppConfigTenantFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _tenantId;
        }
        #endregion
    }
}
