﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace Ecommerce.CategoryProducts.Job
{
    public class AppConfigDomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public AppConfigDomainFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }

        public void Set(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
