﻿using Ecommerce.CategoryProducts.Job.Base;
using Ecommerce.CategoryProducts.Job.Model;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using ECommerce.Domain.Entities.Companyable;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Ecommerce.CategoryProducts.Job
{
    public class App : IApp
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRandomHelper _randomHelper;

        public readonly IHttpHelper _httpHelper;

        private readonly IMarketplaceService _marketplaceService;

        private readonly IExcelHelper _excelHelper;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IConfiguration _configuration;

        public App(IUnitOfWork unitOfWork, IRandomHelper randomHelper, IHttpHelper httpHelper, IMarketplaceService marketplaceService,
            IExcelHelper excelHelper, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder,
            IConfiguration configuration)
        {
            #region Field
            this._unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            this._marketplaceService = marketplaceService;
            this._excelHelper = excelHelper;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            #endregion
        }
        public void Run()
        {
            ((AppConfigTenantFinder)this._tenantFinder)._tenantId = 4;
            ((AppConfigDomainFinder)this._domainFinder)._domainId = 4;
            ((AppConfigCompanyFinder)this._companyFinder)._companyId = 8;

            RunBrandUrl();

            //var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Where(x => x.MarketplaceId == "PTT").ToList();
            //var status = _marketplaceService.GetProductsStatus("PTT");

            //foreach (var pit in productInformationMarketplaces)
            //{
            //    var data = status.Data.FirstOrDefault(x => x.StockCode.Contains(pit.StockCode));
            //    if (data != null)
            //    {
            //        pit.StockCode = data.StockCode;
            //    }

            //}

            //_unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarketplaces);

        }

        #region ProductPhoto

        private void ProductTransfer()
        {
            //((AppConfigTenantFinder)this._tenantFinder)._tenantId = 5;
            //((AppConfigDomainFinder)this._domainFinder)._domainId = 5;
            //((AppConfigCompanyFinder)this._companyFinder)._companyId = 9;


            //var _products = _unitOfWork.ProductRepository.DbSet()
            //      .Include(x => x.ProductInformations)
            //      .ThenInclude(x => x.ProductInformationMarketplaces.Where(x => x.CompanyId == this._companyFinder.FindId()))
            //      .ThenInclude(x => x.ProductInformationMarketplaceVariantValues)
            //      .Include(x => x.ProductInformations)
            //      .ThenInclude(x => x.ProductInformationMarketplaces.Where(x => x.CompanyId == this._companyFinder.FindId()))
            //      .ThenInclude(x => x.MarketplaceCompany)
            //      .Include(x => x.ProductInformations)
            //      .ThenInclude(x => x.ProductInformationPhotos)
            //      .Include(x => x.ProductMarketplaces.Where(x => x.CompanyId == this._companyFinder.FindId()))
            //      .Include(x => x.ProductTranslations)
            //      .Include(x => x.ProductInformations)
            //      .ThenInclude(x => x.ProductInformationTranslations)
            //      .ThenInclude(x => x.ProductInformationContentTranslation)
            //      .Include(x => x.Brand)
            //      .Include(x => x.ProductInformations)
            //      .ThenInclude(x => x.ProductInformationPriceses)
            //      .Where(x => x.BrandId == 257)
            //      .ToList();

            //List<Product> products = new();


            //List<Brand> brands = new();


            //var categoryId = 1583;
            //((AppConfigTenantFinder)this._tenantFinder)._tenantId = 2;
            //((AppConfigDomainFinder)this._domainFinder)._domainId = 2;
            //((AppConfigCompanyFinder)this._companyFinder)._companyId = 2;
            //var supplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id;
            //var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet().ToList();

            //foreach (var pllop in _products)
            //{
            //    if (!brands.Any(b => b.Name == pllop.Brand.Name))
            //    {
            //        brands.Add(new Brand
            //        {
            //            Active = true,
            //            CreatedDate = DateTime.Now,
            //            DomainId = _domainFinder.FindId(),
            //            ManagerUserId = 1,
            //            TenantId = _tenantFinder.FindId(),
            //            Url = String.Empty,
            //            Name = pllop.Brand.Name
            //        });
            //    }


            //    Product product = new()
            //    {
            //        DomainId = _domainFinder.FindId(),
            //        TenantId = _tenantFinder.FindId(),
            //        ManagerUserId = 1,
            //        DeliveryDay = 1,
            //        CategoryId = categoryId,
            //        CreatedDate = DateTime.Now,
            //        SupplierId = supplierId,
            //        AccountingProperty = false,
            //        GroupId = null,
            //        CategoryProducts = new List<CategoryProduct>(),
            //        ProductTranslations = new()
            //                {
            //                    new ProductTranslation
            //                    {
            //                        Name=pllop.ProductTranslations[0].Name,
            //                        ManagerUserId=1,
            //                        LanguageId="TR",
            //                        TenantId=_tenantFinder.FindId(),
            //                        DomainId=_domainFinder.FindId(),
            //                        CreatedDate=DateTime.Now
            //                    }
            //                },
            //        Active = true,
            //        ProductMarketplaces = pllop.ProductMarketplaces.Where(pm => marketplaceCompanies.Select(mc => mc.MarketplaceId).Contains(pm.MarketplaceCompany.MarketplaceId)).Select(x => new ECommerce.Domain.Entities.ProductMarketplace
            //        {

            //            Active = x.Active,
            //            UUId = Guid.NewGuid().ToString(),
            //            TenantId = this._tenantFinder.FindId(),
            //            DomainId = _domainFinder.FindId(),
            //            CompanyId = this._companyFinder.FindId(),
            //            CreatedDate = DateTime.Now,
            //            DeliveryDay = x.MarketplaceCompany.MarketplaceId == "N11" ? 3 : 1,
            //            ManagerUserId = 1,
            //            MarketplaceBrandId = x.MarketplaceBrandId,
            //            MarketplaceBrandName = x.MarketplaceBrandName,
            //            MarketplaceCategoryId = x.MarketplaceCategoryId,
            //            MarketplaceCompanyId = marketplaceCompanies.FirstOrDefault(y => y.MarketplaceId == x.MarketplaceCompany.MarketplaceId).Id,
            //            SellerCode = $"TO-{x.SellerCode}"
            //        })
            //        .ToList(),
            //        Brand = new()
            //        {
            //            Name = pllop.Brand.Name,
            //            Active = true
            //        },
            //        SellerCode = $"TO-{pllop.SellerCode}",
            //        ProductInformations = pllop.ProductInformations.Select(x => new ProductInformation
            //        {

            //            Active = true,
            //            Barcode = x.Barcode,
            //            StockCode = x.StockCode,
            //            Stock = x.Stock,
            //            TenantId = _tenantFinder.FindId(),
            //            DomainId = _domainFinder.FindId(),
            //            ManagerUserId = 1,
            //            Photoable = true,
            //            IsSale = true,
            //            Type = "PI",
            //            Payor = false,
            //            SkuCode = x.StockCode,
            //            CreatedDate = DateTime.Now,
            //            GroupId = Guid.NewGuid().ToString(),
            //            ProductInformationTranslations = new()
            //                        {
            //                            new ProductInformationTranslation
            //                            {
            //                               CreatedDate = DateTime.Now,
            //                                SubTitle=String.Empty,
            //                                TenantId=_tenantFinder.FindId(),
            //                                DomainId=_domainFinder.FindId(),
            //                                ManagerUserId=1,
            //                                LanguageId="TR",
            //                                Url=String.Empty,
            //                                Name = x.ProductInformationTranslations[0].Name,
            //                                ProductInformationSeoTranslation=new ProductInformationSeoTranslation
            //                                {
            //                                TenantId=_tenantFinder.FindId(),
            //                                DomainId=_domainFinder.FindId(),
            //                                ManagerUserId=1,
            //                                CreatedDate=DateTime.Now,
            //                                MetaDescription="",
            //                                MetaKeywords="",
            //                                Title="",
            //                                },
            //                                ProductInformationContentTranslation=new ProductInformationContentTranslation
            //                                {
            //                                    TenantId=_tenantFinder.FindId(),
            //                                    DomainId=_domainFinder.FindId(),
            //                                    ManagerUserId=1,
            //                                    CreatedDate=DateTime.Now,
            //                                    Content=x.ProductInformationTranslations[0].ProductInformationContentTranslation.Content,
            //                                    Description="",
            //                                    ExpertOpinion=String.Empty
            //                                },
            //                                ProductInformationTranslationBreadcrumb=new ProductInformationTranslationBreadcrumb(),
            //                            }
            //                        },
            //            ProductInformationPriceses = new()
            //                        {
            //                            new ProductInformationPrice
            //                            {
            //                                CreatedDate = DateTime.Now,
            //                                TenantId=_tenantFinder.FindId(),
            //                                DomainId=_domainFinder.FindId(),
            //                                ManagerUserId=1,
            //                                AccountingPrice=false,
            //                                CurrencyId="TL",
            //                                ListUnitPrice = x.ProductInformationPriceses[0].ListUnitPrice * 1.30m,
            //                                UnitPrice = x.ProductInformationPriceses[0].UnitPrice * 1.30m,
            //                                VatRate = x.ProductInformationPriceses[0].VatRate
            //                            }
            //                        },
            //            ProductInformationPhotos = x.ProductInformationPhotos
            //                            .Select(e => new ProductInformationPhoto
            //                            {
            //                                CreatedDate = DateTime.Now,
            //                                TenantId = _tenantFinder.FindId(),
            //                                DomainId = _domainFinder.FindId(),
            //                                ManagerUserId = 1,
            //                                FileName = $"https://erp.helpy.com.tr/img/SimgeSaat/product/{e.FileName}"
            //                            })
            //                            .ToList(),
            //            ProductInformationVariants = new List<ProductInformationVariant>(),
            //            ProductInformationMarketplaces = x.ProductInformationMarketplaces.Where(pm => marketplaceCompanies.Select(mc => mc.MarketplaceId).Contains(pm.MarketplaceCompany.MarketplaceId)).Select(x => new ProductInformationMarketplace
            //            {
            //                TenantId = this._tenantFinder.FindId(),
            //                DomainId = _domainFinder.FindId(),
            //                CompanyId = this._companyFinder.FindId(),
            //                AccountingPrice = false,
            //                Active = true,
            //                CreatedDate = DateTime.Now,
            //                DirtyPrice = false,
            //                DirtyStock = false,
            //                DirtyProductInformation = false,
            //                IgnoreSystemNotification = false,
            //                ManagerUserId = 1,
            //                MarketplaceCompanyId = marketplaceCompanies.FirstOrDefault(y => y.MarketplaceId == x.MarketplaceCompany.MarketplaceId).Id,
            //                Locked = false,
            //                OnSale = false,
            //                Opened = false,
            //                UUId = Guid.NewGuid().ToString(),
            //                ListUnitPrice = x.ListUnitPrice * 1.30m,
            //                UnitPrice = x.UnitPrice * 1.30m,
            //                Barcode = x.Barcode.Length > 5 ? $"53445{x.Barcode.Substring(5, x.Barcode.Length - 5)}" : x.Barcode,
            //                StockCode = $"TO-{x.StockCode}",
            //                ProductInformationMarketplaceVariantValues = x.ProductInformationMarketplaceVariantValues.Select(x => new ProductInformationMarketplaceVariantValue
            //                {
            //                    TenantId = this._tenantFinder.FindId(),
            //                    DomainId = this._domainFinder.FindId(),
            //                    CompanyId = this._companyFinder.FindId(),
            //                    CreatedDate = DateTime.Now,
            //                    ManagerUserId = 1,
            //                    MarketplaceCategoriesMarketplaceVariantValueId = x.MarketplaceCategoriesMarketplaceVariantValueId
            //                })
            //                .ToList()
            //            })
            //            .ToList()

            //        }).ToList(),

            //    };
            //    products.Add(product);
            //}

            //#region Brands
            //var brandResponse = this.BulkRead(brands);

            //var addBrands = brands
            //    .Where(b => !brandResponse.Data.Where(d => d.Id != 0).Select(d => d.Name).Contains(b.Name))
            //    .ToList();
            //if (addBrands.Count > 0)
            //    this._unitOfWork.BrandRepository.BulkInsertMapping(addBrands);

            //brandResponse = this.BulkRead(brands);
            //#endregion

            //#region Products (Create)
            //var productResponse = this.BulkRead(products);
            //products.ForEach(theProduct =>
            //{
            //    theProduct.Brand = brandResponse.Data.First(d => d.Name == theProduct.Brand.Name);
            //    theProduct.BrandId = theProduct.Brand.Id;

            //    foreach (var theProductInformation in theProduct.ProductInformations)
            //    {

            //        var productEntity = productResponse
            //       .Data
            //       .FirstOrDefault(d => d.Id != 0 &&
            //           d.ProductInformations.Any(pi => pi.Barcode == theProductInformation.Barcode));

            //        if (productEntity != null)
            //        {
            //            theProduct.Id = productEntity.Id;
            //            theProduct.ProductInformations.ForEach(x =>
            //            {
            //                x.Id = productEntity.ProductInformations[0].Id;
            //                x.ProductId = productEntity.Id;
            //            });
            //        }
            //    }

            //});

            //if (products.Count(p => p.Id == 0) > 0)
            //{
            //    var insertingProducts = products.Where(p => p.Id == 0).Reverse().ToList();

            //    this._unitOfWork.ProductRepository.BulkInsertMapping(insertingProducts);

            //}
            //#endregion


            //InsertImages("C:\\inetpub\\Helpy\\Erp\\wwwroot\\img");

        }


        private void Phototable()
        {
            var produts = _unitOfWork.ProductRepository.DbSet().Include(x => x.ProductInformations).Where(x => !x.IsOnlyHidden).ToList();
            var productInformations = new List<ProductInformation>();
            foreach (var pLoop in produts)
            {

                var groupProductInformations = pLoop.ProductInformations.Where(x => !string.IsNullOrEmpty(x.SkuCode)).GroupBy(x => x.SkuCode).Select(x =>
                  new { key = x.Key, ProductInformations = x.Select(y => y).ToList() }).ToList();

                foreach (var groupPi in groupProductInformations)
                {
                    foreach (var piLoop in groupPi.ProductInformations)
                    {
                        piLoop.GroupId = groupPi.key;
                        piLoop.Photoable = groupPi.ProductInformations.IndexOf(piLoop) == 0;

                        productInformations.Add(piLoop);
                    }

                }
            }

            _unitOfWork.ProductInformationRepository.Update(productInformations);
        }

        private void InsertImages(string path)
        {
            Console.WriteLine("InsertImages");
            var _productInformationPhotos = _unitOfWork.ProductInformationPhotoRepository.DbSet().Where(x => x.FileName.Contains("https://erp.helpy.com.tr/img/SimgeSaat/product")).ToList();

            Dictionary<string, string> keys = new Dictionary<string, string>();

            var productPhotos = new List<ProductInformationPhoto>();

            foreach (var ppLoop in _productInformationPhotos)
            {
                if (keys.ContainsKey(ppLoop.FileName))
                {
                    ppLoop.FileName = keys[ppLoop.FileName];
                    Console.WriteLine("Cache'den geldi");
                }
                else
                {

                    var splitPhoto = ppLoop.FileName.Replace("https://erp.helpy.com.tr/img/SimgeSaat/product", "").Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                    var fileName = $"{splitPhoto[splitPhoto.Length - 1]}";
                    var success = Download(path, fileName, ppLoop.ProductInformationId, ppLoop.FileName);

                    if (success)
                    {
                        keys.Add(ppLoop.FileName, $"{ppLoop.ProductInformationId}/{fileName}");
                        ppLoop.FileName = $"{ppLoop.ProductInformationId}/{fileName}";
                    }
                }

            }
            _unitOfWork.ProductInformationPhotoRepository.Update(_productInformationPhotos);


        }

        //private void InsertProducts(List<ECommerce.Application.Common.Parameters.Product.ProductItem> _products)
        //{

        //    List<Product> products = new();

        //    List<Variant> variants = new();

        //    List<Brand> brands = new();

        //    List<Category> categories = new();

        //    var supplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id;




        //    foreach (var pllop in _products)
        //    {
        //        if (!brands.Any(b => b.Name == pllop.BrandName))
        //        {
        //            brands.Add(new Brand
        //            {
        //                Active = true,
        //                CreatedDate = DateTime.Now,
        //                DomainId = _domainFinder.FindId(),
        //                ManagerUserId = 1,
        //                TenantId = _tenantFinder.FindId(),
        //                Url = String.Empty,
        //                Name = pllop.BrandName
        //            });
        //        }
        //        var sender = pllop.StockItems[0].Attributes.FirstOrDefault(x => x.AttributeName == "Cinsiyet");

        //        pllop.CategoryName = sender != null && sender.AttributeValueName != null ? $"{sender.AttributeValueName.Replace("Kadın / Kız", "Kadın")} {pllop.CategoryName}"
        //            : pllop.CategoryName;




        //        var categoryCode = pllop.CategoryName.ReplaceChar().Replace(" ", "");

        //        if (!categories.Any(c => c.Code == categoryCode))
        //        {
        //            categories.Add(new Category
        //            {
        //                Active = true,
        //                CreatedDate = DateTime.Now,
        //                FileName = String.Empty,
        //                DomainId = _domainFinder.FindId(),
        //                ManagerUserId = 1,
        //                SortNumber = 1,
        //                TenantId = _tenantFinder.FindId(),
        //                Code = categoryCode,
        //                CategoryTranslations = new()
        //                        {
        //                            new CategoryTranslation
        //                            {
        //                                CreatedDate=DateTime.Now,
        //                                DomainId=_domainFinder.FindId(),
        //                                IconFileName=String.Empty,
        //                                LanguageId="TR",
        //                                ManagerUserId=1,
        //                                TenantId=_tenantFinder.FindId(),
        //                                Url=String.Empty,
        //                                Name = pllop.CategoryName,
        //                                CategoryContentTranslation=new CategoryContentTranslation
        //                                {
        //                                    ManagerUserId=1,
        //                                    TenantId=_tenantFinder.FindId(),
        //                                    DomainId=_domainFinder.FindId(),
        //                                    CreatedDate=DateTime.Now,
        //                                    Content=pllop.CategoryName
        //                                },
        //                                CategorySeoTranslation=new CategorySeoTranslation
        //                                {
        //                                    ManagerUserId=1,
        //                                    TenantId=_tenantFinder.FindId(),
        //                                    DomainId=_domainFinder.FindId(),
        //                                    CreatedDate=DateTime.Now,
        //                                    MetaDescription=pllop.CategoryName,
        //                                    MetaKeywords=pllop.CategoryName,
        //                                    Title=pllop.CategoryName
        //                                },
        //                                CategoryTranslationBreadcrumb=new CategoryTranslationBreadcrumb()
        //                            }
        //                        }
        //            });
        //        }

        //        foreach (var siLoop in pllop.StockItems)
        //        {
        //            var elementVariants = siLoop.Attributes
        //                       .Select(e => new Variant
        //                       {
        //                           CreatedDate = DateTime.Now,
        //                           DomainId = _domainFinder.FindId(),
        //                           ManagerUserId = 1,
        //                           Photoable = false,
        //                           TenantId = _tenantFinder.FindId(),
        //                           VariantTranslations = new()
        //                           {
        //                                        new VariantTranslation
        //                                        {
        //                                            CreatedDate=DateTime.Now,
        //                                            DomainId=_domainFinder.FindId(),
        //                                            LanguageId="TR",
        //                                            ManagerUserId=1,
        //                                            TenantId=_tenantFinder.FindId(),
        //                                            Name = e.AttributeName
        //                                        }
        //                           },
        //                           VariantValues = new()
        //                           {
        //                                        new VariantValue
        //                                        {
        //                                            CreatedDate= DateTime.Now,
        //                                            DomainId=_domainFinder.FindId(),
        //                                            ManagerUserId=1,
        //                                            TenantId=_tenantFinder.FindId(),
        //                                            VariantValueTranslations = new()
        //                                            {
        //                                                new VariantValueTranslation
        //                                                {
        //                                                    CreatedDate=DateTime.Now,
        //                                                    DomainId=_domainFinder.FindId(),
        //                                                    LanguageId="TR",
        //                                                    ManagerUserId=1,
        //                                                    TenantId=_tenantFinder.FindId(),
        //                                                    Value = e.AttributeValueName !=null ?  CultureInfo.CurrentCulture.TextInfo.ToTitleCase(e.AttributeValueName.ReplaceChar()) : null
        //                                                }
        //                                            }
        //                                        }
        //                           }
        //                       })
        //                       .GroupBy(x => new { x.VariantTranslations.FirstOrDefault().Name, x.VariantValues.FirstOrDefault().VariantValueTranslations.FirstOrDefault().Value }).Select(x => x.First())
        //                       .ToList();


        //            if (elementVariants != null)
        //            {

        //                foreach (var theElementVariant in elementVariants)
        //                {
        //                    var variantName = theElementVariant.VariantTranslations.First().Name;
        //                    var variant = variants.FirstOrDefault(v => v.VariantTranslations.First().Name == variantName);
        //                    if (variant == null)
        //                        variants.Add(theElementVariant);
        //                    else
        //                        theElementVariant.VariantValues.ForEach(theVariantValue =>
        //                        {
        //                            var variantValueValue = theVariantValue.VariantValueTranslations.First().Value;
        //                            if (!variant.VariantValues.Any(vv => vv.VariantValueTranslations.First().Value == variantValueValue))
        //                                variant.VariantValues.Add(theVariantValue);
        //                        });
        //                }

        //            }
        //        }
        //        Product product = new()
        //        {
        //            DomainId = _domainFinder.FindId(),
        //            TenantId = _tenantFinder.FindId(),
        //            ManagerUserId = 1,
        //            DeliveryDay = 1,
        //            CreatedDate = DateTime.Now,
        //            SupplierId = supplierId,
        //            AccountingProperty = false,
        //            GroupId = null,
        //            CategoryProducts = new List<CategoryProduct>(),
        //            ProductTranslations = new()
        //                    {
        //                        new ProductTranslation
        //                        {
        //                            Name=pllop.Name,
        //                            ManagerUserId=1,
        //                            LanguageId="TR",
        //                            TenantId=_tenantFinder.FindId(),
        //                            DomainId=_domainFinder.FindId(),
        //                            CreatedDate=DateTime.Now
        //                        }
        //                    },
        //            Active = true,
        //            ProductMarketplaces = new List<ProductMarketplace>(),
        //            Brand = new()
        //            {
        //                Name = pllop.BrandName,
        //                Active = true
        //            },
        //            Category = new Category
        //            {
        //                Code = categoryCode,
        //                CategoryTranslations = new()
        //                        {
        //                            new CategoryTranslation
        //                            {
        //                                Name = pllop.CategoryName,

        //                            }
        //                        }
        //            },
        //            SellerCode = pllop.SellerCode,
        //            ProductInformations = pllop.StockItems.Select(x => new ProductInformation
        //            {

        //                Active = true,
        //                Barcode = x.Barcode,
        //                StockCode = x.StockCode,
        //                Stock = x.Quantity,
        //                TenantId = _tenantFinder.FindId(),
        //                DomainId = _domainFinder.FindId(),
        //                ManagerUserId = 1,
        //                Photoable = true,
        //                IsSale = true,
        //                Type = "PI",
        //                Payor = false,
        //                SkuCode = x.StockCode,
        //                CreatedDate = DateTime.Now,
        //                GroupId = Guid.NewGuid().ToString(),
        //                ProductInformationTranslations = new()
        //                            {
        //                                new ProductInformationTranslation
        //                                {
        //                                   CreatedDate = DateTime.Now,
        //                                    SubTitle=String.Empty,
        //                                    TenantId=_tenantFinder.FindId(),
        //                                    DomainId=_domainFinder.FindId(),
        //                                    ManagerUserId=1,
        //                                    LanguageId="TR",
        //                                    Url=String.Empty,
        //                                    Name = x.Title,
        //                                    ProductInformationSeoTranslation=new ProductInformationSeoTranslation
        //                                    {
        //                                    TenantId=_tenantFinder.FindId(),
        //                                    DomainId=_domainFinder.FindId(),
        //                                    ManagerUserId=1,
        //                                    CreatedDate=DateTime.Now,
        //                                    MetaDescription="",
        //                                    MetaKeywords="",
        //                                    Title="",
        //                                    },
        //                                    ProductInformationContentTranslation=new ProductInformationContentTranslation
        //                                    {
        //                                        TenantId=_tenantFinder.FindId(),
        //                                        DomainId=_domainFinder.FindId(),
        //                                        ManagerUserId=1,
        //                                        CreatedDate=DateTime.Now,
        //                                        Content=x.Description,
        //                                        Description="",
        //                                        ExpertOpinion=String.Empty
        //                                    },
        //                                    ProductInformationTranslationBreadcrumb=new ProductInformationTranslationBreadcrumb(),
        //                                }
        //                            },
        //                ProductInformationPriceses = new()
        //                            {
        //                                new ProductInformationPrice
        //                                {
        //                                    CreatedDate = DateTime.Now,
        //                                    TenantId=_tenantFinder.FindId(),
        //                                    DomainId=_domainFinder.FindId(),
        //                                    ManagerUserId=1,
        //                                    AccountingPrice=true,
        //                                    CurrencyId="TL",
        //                                    ListUnitPrice = x.ListPrice,
        //                                    UnitPrice = x.SalePrice,
        //                                    VatRate = x.VatRate / 100
        //                                }
        //                            },
        //                ProductInformationPhotos = x.Images
        //                                .Select(e => new ProductInformationPhoto
        //                                {
        //                                    CreatedDate = DateTime.Now,
        //                                    TenantId = _tenantFinder.FindId(),
        //                                    DomainId = _domainFinder.FindId(),
        //                                    ManagerUserId = 1,
        //                                    FileName = e.Url
        //                                })
        //                                .ToList(),
        //                ProductInformationVariants = x.Attributes.Where(x => !string.IsNullOrEmpty(x.AttributeValueName) && !string.IsNullOrEmpty(x.AttributeName))
        //                                .Select(e => new ProductInformationVariant
        //                                {

        //                                    ManagerUserId = 1,
        //                                    CreatedDate = DateTime.Now,
        //                                    DomainId = _domainFinder.FindId(),
        //                                    TenantId = _tenantFinder.FindId(),
        //                                    VariantValue = new VariantValue
        //                                    {
        //                                        ManagerUserId = 1,
        //                                        CreatedDate = DateTime.Now,
        //                                        DomainId = _domainFinder.FindId(),
        //                                        TenantId = _tenantFinder.FindId(),
        //                                        VariantValueTranslations = new List<VariantValueTranslation>
        //                                        {
        //                                            new VariantValueTranslation
        //                                            {
        //                                                Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(e.AttributeValueName.ReplaceChar()),
        //                                                ManagerUserId = 1,
        //                                                CreatedDate=DateTime.Now,
        //                                                DomainId=_domainFinder.FindId(),
        //                                                LanguageId="TR",
        //                                                TenantId=_tenantFinder.FindId()
        //                                            }
        //                                        },
        //                                        Variant = new Variant
        //                                        {
        //                                            VariantTranslations = new()
        //                                            {
        //                                                new VariantTranslation
        //                                                {
        //                                                    Name = e.AttributeName,
        //                                                     ManagerUserId = 1,
        //                                                     CreatedDate=DateTime.Now,
        //                                                     DomainId=_domainFinder.FindId(),
        //                                                     LanguageId="TR",
        //                                                     TenantId=_tenantFinder.FindId()
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                })
        //                                .GroupBy(x => new
        //                                {
        //                                    x.VariantValue.Variant.VariantTranslations.FirstOrDefault().Name,
        //                                    x.VariantValue.VariantValueTranslations.FirstOrDefault().Value
        //                                }).Select(x => x.First())
        //                                .ToList()

        //            }).ToList(),

        //        };

        //        products.Add(product);

        //    }

        //    #region Brands
        //    var brandResponse = this.BulkRead(brands);

        //    var addBrands = brands
        //        .Where(b => !brandResponse.Data.Where(d => d.Id != 0).Select(d => d.Name).Contains(b.Name))
        //        .ToList();
        //    if (addBrands.Count > 0)
        //        this._unitOfWork.BrandRepository.BulkInsertMapping(addBrands);

        //    brandResponse = this.BulkRead(brands);
        //    #endregion

        //    #region Categories
        //    var categoryResponse = this.BulkRead(categories);

        //    var addCategories = categories
        //        .Where(c => !categoryResponse.Data.Where(d => d.Id != 0).Select(d => d.Code).Contains(c.Code))
        //        .ToList();
        //    if (addCategories.Count > 0)
        //    {
        //        this._unitOfWork.CategoryRepository.BulkInsertMapping(addCategories);
        //    }

        //    categoryResponse = this.BulkRead(categories);
        //    #endregion

        //    #region Variants
        //    variants.RemoveAll(v => v.VariantTranslations.First().Name == "Marka");
        //    var variantResponse = this.BulkRead(variants);

        //    var addVariants = variants
        //        .Where(v => !variantResponse.Data.Where(d => d.Id != 0).Select(x => x.VariantTranslations.First().Name).Contains(v.VariantTranslations.First().Name))
        //        .ToList();
        //    if (addVariants.Count > 0)
        //        this._unitOfWork.VariantRepository.BulkInsertMapping(addVariants);

        //    variantResponse = this.BulkRead(variants);

        //    variants.ForEach(theVariant =>
        //    {
        //        var variant = variantResponse
        //            .Data
        //            .First(d => d.VariantTranslations.First().Name == theVariant.VariantTranslations.First().Name);

        //        theVariant.VariantValues.ForEach(theVariantValue => theVariantValue.VariantId = variant.Id);
        //    });
        //    #endregion


        //    #region Variant Values
        //    var variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());

        //    var addVariantValues = new List<VariantValue>();
        //    variants.SelectMany(v => v.VariantValues).ToList().ForEach(theVariantValue =>
        //    {
        //        var variantValue = variantValueResponse
        //            .Data
        //            .FirstOrDefault(d => d.VariantId == theVariantValue.VariantId && d.VariantValueTranslations.First().Value == theVariantValue.VariantValueTranslations.First().Value);

        //        if (theVariantValue.VariantValueTranslations.First().Value != null)
        //        {
        //            if (variantValue == null)
        //            {
        //                addVariantValues.Add(theVariantValue);
        //            }
        //            else if (variantValue.Id == 0)
        //                addVariantValues.Add(variantValue);
        //        }
        //    });
        //    if (addVariantValues.Count > 0)
        //    {
        //        var groupByAddedVarinatValues = addVariantValues.GroupBy(x => new { x.VariantId, x.VariantValueTranslations[0].Value }).Select(x => x.First()).ToList();

        //        this._unitOfWork.VariantValueRepository.BulkInsertMapping(groupByAddedVarinatValues);
        //    }

        //    variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());
        //    #endregion

        //    #region Products (Create)
        //    var productResponse = this.BulkRead(products);
        //    products.ForEach(theProduct =>
        //    {
        //        theProduct.Brand = brandResponse.Data.First(d => d.Name == theProduct.Brand.Name);
        //        theProduct.BrandId = theProduct.Brand.Id;

        //        theProduct.Category = categoryResponse.Data.First(d => d.Code == theProduct.Category.Code);
        //        theProduct.CategoryId = theProduct.Category.Id;

        //        theProduct.CategoryProducts.Add(new CategoryProduct
        //        {
        //            CategoryId = theProduct.Category.Id,
        //            CreatedDate = DateTime.Now,
        //            DomainId = _domainFinder.FindId(),
        //            TenantId = _tenantFinder.FindId(),
        //            ManagerUserId = 1
        //        });

        //        foreach (var theProductInformation in theProduct.ProductInformations)
        //        {


        //            if (theProductInformation.ProductInformationVariants != null)
        //            {

        //                theProductInformation.ProductInformationVariants.GroupBy(x => new { x.VariantValue.VariantValueTranslations.First().Value, x.VariantValue.Variant.VariantTranslations.First().Name }).Select(x => x.First()).ToList().ForEach(theProductInformationVariant =>
        //                {
        //                    try
        //                    {

        //                        var variant = variantResponse.Data.First(d => d.VariantTranslations.First().Name == theProductInformationVariant.VariantValue.Variant.VariantTranslations.First().Name);

        //                        var variantValue = variantValueResponse.Data.First(d => d.VariantId == variant.Id && d.VariantValueTranslations.First().Value == theProductInformationVariant.VariantValue.VariantValueTranslations.First().Value);

        //                        theProductInformationVariant.VariantValueId = variantValue.Id;
        //                        theProductInformationVariant.VariantValue.VariantId = variant.Id;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                    }
        //                });
        //            }


        //            var productEntity = productResponse
        //           .Data
        //           .FirstOrDefault(d => d.Id != 0 &&
        //               d.ProductInformations.Any(pi => pi.Barcode == theProductInformation.Barcode));

        //            if (productEntity != null)
        //            {
        //                theProduct.Id = productEntity.Id;
        //                theProduct.ProductInformations.ForEach(x =>
        //                {
        //                    x.Id = productEntity.ProductInformations[0].Id;
        //                    x.ProductId = productEntity.Id;
        //                });

        //            }
        //        }

        //    });

        //    if (products.Count(p => p.Id == 0) > 0)
        //    {
        //        var insertingProducts = products.Where(p => p.Id == 0).Reverse().ToList();

        //        this._unitOfWork.ProductRepository.BulkInsertMapping(insertingProducts);

        //    }
        //    #endregion



        //}

        //private void InsertMarketplacesTY(List<ECommerce.Application.Common.Parameters.Product.ProductItem> _products, string marketplaceId)
        //{
        //    var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().FirstOrDefault(x => x.MarketplaceId == marketplaceId);
        //    //var productInformations = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformation).Where(x => x.MarketplaceCompany.MarketplaceId == marketplaceId).ToList();
        //    var _productInformations = _unitOfWork.ProductInformationRepository.DbSet().ToList();
        //    var marketplaceCatagories = _unitOfWork.MarketplaceCategoryRepository.DbSet().Where(x => x.MarketplaceId == marketplaceId);

        //    var catagories = _products.GroupBy(x => x.CategoryId).Select(x => $"{marketplaceId}-{x.First().CategoryId}");
        //    var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
        //         .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
        //         .Include(x => x.MarketplaceVariantValue)
        //         .Where(x =>
        //     x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == marketplaceId
        //     && catagories.Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

        //    var productMarketplaces = new List<ProductMarketplace>();
        //    var productInformationMarketplaces = new List<ProductInformationMarketplace>();
        //    foreach (var productItem in _products)
        //    {

        //        ProductInformation productInformation = null;

        //        foreach (var sLoop in productItem.StockItems)
        //        {

        //            productInformation = _productInformations.FirstOrDefault(x => x.Barcode == sLoop.Barcode);

        //            if (productInformation != null)
        //            {


        //                var productInformationMarketplace = new ProductInformationMarketplace
        //                {
        //                    CreatedDate = DateTime.Now,
        //                    DomainId = _domainFinder.FindId(),
        //                    TenantId = _tenantFinder.FindId(),
        //                    CompanyId = _companyFinder.FindId(),
        //                    Active = true,
        //                    MarketplaceCompanyId = marketplaceCompany.Id,
        //                    UnitPrice = sLoop.SalePrice,
        //                    Barcode = sLoop.Barcode,
        //                    ListUnitPrice = sLoop.ListPrice,
        //                    ManagerUserId = 1,
        //                    ProductInformationId = productInformation.Id,
        //                    StockCode = sLoop.StockCode,
        //                    ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()
        //                };

        //                if (sLoop.Attributes != null)
        //                    foreach (var attLoop in sLoop.Attributes)
        //                    {
        //                        if (attLoop.AttributeCode != "47" && marketplaceId == "TY")
        //                        {

        //                            var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValueId.Replace("TY-", "") == attLoop.AttributeValueCode));
        //                            if (mVariantValue != null)
        //                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
        //                                {
        //                                    DomainId = _domainFinder.FindId(),
        //                                    TenantId = _tenantFinder.FindId(),
        //                                    CompanyId = _companyFinder.FindId(),
        //                                    CreatedDate = DateTime.Now,
        //                                    ManagerUserId = 1,
        //                                    MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
        //                                });

        //                        }
        //                        else
        //                        {
        //                            var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace("TY-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId.Replace("TY-", "") == attLoop.AttributeCode && x.MarketplaceVariantValue.Value.ReplaceChar() == attLoop.AttributeValueName.ReplaceChar()));
        //                            if (mVariantValue != null)
        //                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
        //                                {
        //                                    DomainId = _domainFinder.FindId(),
        //                                    TenantId = _tenantFinder.FindId(),
        //                                    CompanyId = _companyFinder.FindId(),
        //                                    CreatedDate = DateTime.Now,
        //                                    ManagerUserId = 1,
        //                                    MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
        //                                });

        //                        }
        //                    }


        //                productInformationMarketplaces.Add(productInformationMarketplace);





        //            }


        //        }


        //        var _mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace($"{marketplaceId}-", "") == productItem.CategoryId));

        //        var marketplaceCatagoryId = string.Empty;

        //        if (_mVariantValue != null)
        //        {
        //            marketplaceCatagoryId = $"{marketplaceId}-" + productItem.CategoryId;

        //        }
        //        else
        //        {//Pazarama için yapıldı kategoriId göndermiyor adından buluyoruz
        //            var marketplaceCatagory = marketplaceCatagories.FirstOrDefault(x => x.Name == productItem.CategoryName);
        //            if (marketplaceCatagory != null)
        //            {
        //                marketplaceCatagoryId = marketplaceCatagory.Id;

        //            }
        //        }

        //        if (productInformation != null)
        //        {
        //            productMarketplaces.Add(new ProductMarketplace
        //            {
        //                SellerCode = productItem.SellerCode,
        //                Active = true,
        //                CompanyId = _companyFinder.FindId(),
        //                CreatedDate = DateTime.Now,
        //                DomainId = _domainFinder.FindId(),
        //                TenantId = _tenantFinder.FindId(),
        //                ManagerUserId = 1,
        //                MarketplaceBrandId = productItem.BrandId,
        //                MarketplaceCategoryId = !string.IsNullOrEmpty(marketplaceCatagoryId) ? marketplaceCatagoryId : "",
        //                MarketplaceBrandName = productItem.BrandName,
        //                ProductId = productInformation.ProductId,
        //                MarketplaceCompanyId = marketplaceCompany.Id
        //            });
        //        }

        //    }
        //    _unitOfWork.ProductMarketplaceRepository.BulkInsert(productMarketplaces);
        //    _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(productInformationMarketplaces);
        //}


        //        private void InsertMarketplacesN11AndHB(List<ECommerce.Application.Common.Parameters.Product.ProductItem> _products, string marketplaceId)
        //        {
        //            var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().FirstOrDefault(x => x.MarketplaceId == marketplaceId);
        //            var productInformations = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformation).Where(x => x.MarketplaceCompany.MarketplaceId == marketplaceId).ToList();

        //            var catagories = _products.GroupBy(x => x.CategoryId).Select(x => $"{marketplaceId}-{x.First().CategoryId}");
        //            var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
        //                 .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
        //                 .ThenInclude(x => x.MarketplaceVariant)
        //                 .Include(x => x.MarketplaceVariantValue)
        //                 .Where(x =>
        //             x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == marketplaceId
        //             && catagories.Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

        //            var productMarketplaces = new List<ProductMarketplace>();
        //            var productInformationMarketplaces = new List<ProductInformationMarketplace>();
        //            foreach (var productItem in _products)
        //            {



        //                foreach (var sLoop in productItem.StockItems)
        //                {
        //                    var productInformation = productInformations.FirstOrDefault(x => x.StockCode == sLoop.StockCode);
        //                    if (productInformation != null)
        //                    {


        //                        var productInformationMarketplace = new ProductInformationMarketplace
        //                        {
        //                            CreatedDate = DateTime.Now,
        //                            DomainId = _domainFinder.FindId(),
        //                            TenantId = _tenantFinder.FindId(),
        //                            CompanyId = _companyFinder.FindId(),
        //                            Active = true,
        //                            MarketplaceCompanyId = marketplaceCompany.Id,
        //                            UnitPrice = sLoop.SalePrice,
        //                            Barcode = sLoop.Barcode,
        //                            ListUnitPrice = sLoop.ListPrice,
        //                            ManagerUserId = 1,
        //                            ProductInformationId = productInformation.ProductInformationId,
        //                            StockCode = sLoop.StockCode,
        //                            ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()
        //                        };

        //                        if (sLoop.Attributes != null)
        //                            foreach (var attLoop in sLoop.Attributes)
        //                            {

        //                                var mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace($"{marketplaceId}-", "") == productItem.CategoryId && x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.Name == attLoop.AttributeName
        //                                 && x.MarketplaceVariantValue.Value == attLoop.AttributeValueName));

        //                                if (mVariantValue != null)
        //                                    productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
        //                                    {
        //                                        DomainId = _domainFinder.FindId(),
        //                                        TenantId = _tenantFinder.FindId(),
        //                                        CompanyId = _companyFinder.FindId(),
        //                                        CreatedDate = DateTime.Now,
        //                                        ManagerUserId = 1,
        //                                        MarketplaceCategoriesMarketplaceVariantValueId = mVariantValue.Id
        //                                    });


        //                            }


        //                        productInformationMarketplaces.Add(productInformationMarketplace);



        //                        var _mVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x => x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId.Replace($"{marketplaceId}-", "") == productItem.CategoryId));


        //                        productMarketplaces.Add(new ProductMarketplace
        //                        {
        //                            SellerCode = productItem.SellerCode,
        //                            Active = true,
        //                            CompanyId = _companyFinder.FindId(),
        //                            CreatedDate = DateTime.Now,
        //                            DomainId = _domainFinder.FindId(),
        //                            TenantId = _tenantFinder.FindId(),
        //                            ManagerUserId = 1,
        //                            MarketplaceBrandId = productItem.BrandId,
        //                            MarketplaceCategoryId = _mVariantValue != null ? $"{marketplaceId}-" + productItem.CategoryId : null,
        //                            MarketplaceBrandName = productItem.BrandName,
        //                            ProductId = productInformation.ProductInformation.ProductId,
        //                            MarketplaceCompanyId = marketplaceCompany.Id
        //                        });

        //                    }


        //                }

        //            }
        //            _unitOfWork.ProductMarketplaceRepository.BulkInsert(productMarketplaces);
        //            _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(productInformationMarketplaces);
        //        }

        //        private void ProductMarketplace(Tenant tLoop, Domain dLoop)
        //        {
        //            var companies = this
        //         ._unitOfWork
        //         .CompanyRepository
        //         .DbSet()
        //         .Include(c => c.CompanySetting)
        //         .ToList();

        //            foreach (var theCompany in companies)
        //            {

        //                var _products = _unitOfWork.ProductRepository.DbSet()
        //                            .Include(x => x.ProductInformations)
        //                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
        //                             .Include(x => x.ProductInformations)
        //                             .ThenInclude(x => x.ProductInformationVariants)
        //                             .ThenInclude(x => x.VariantValue.Variant)
        //                             .Include(x => x.ProductInformations)
        //                             .ToList();

        //                var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet().Where(x => x.Active).ToList();
        //                var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet().ToList();
        //                var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Include(x => x.ProductInformationMarketplaceVariantValues).ThenInclude(x => x.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant).ToList();

        //                var _productMarketplaces = new List<ProductMarketplace>();
        //                var _productInformationMarketplaces = new List<ProductInformationMarketplace>();
        //                var _productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();
        //                var updateProductInformationMarketplaces = new List<ProductInformationMarketplace>();

        //                foreach (var mcLoop in marketplaceCompanies)
        //                {
        //                    var marketplaceCategoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().Include(x => x.MarketplaceCategory).Where(x => x.MarketplaceCategory.MarketplaceId == mcLoop.MarketplaceId).ToList();
        //                    var marketplaceBrandMappings = _unitOfWork.MarketplaceBrandMappingRepository.DbSet().Where(x => x.MarketplaceId == mcLoop.MarketplaceId).ToList();

        //                    var marketplaceVariantMappings = _unitOfWork.MarketplaceVariantMappingRepository.DbSet().Where(x => x.MarketplaceVariant.MarketplaceId == mcLoop.MarketplaceId).ToList();
        //                    var marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().Include(x => x.MarketplaceVariantValue).Where(x => x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();
        //                    var marketplaceCategoriesMarketplaceVariantValues = _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.DbSet()
        //                        .Include(x => x.MarketplaceCategoriesMarketplaceVariant)
        //                        .Where(x =>
        //                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariant.MarketplaceId == mcLoop.MarketplaceId
        //                    && marketplaceCategoryMappings.Select(mc => mc.MarketplaceCategoryId).Contains(x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId)).ToList();

        //                    foreach (var pLoop in _products)
        //                    {
        //                        Console.WriteLine($"{mcLoop.MarketplaceId} {_products.IndexOf(pLoop)}. Ürün");

        //                        var marketplaceCategory = marketplaceCategoryMappings.FirstOrDefault(x =>
        //                        x.MarketplaceCategory.MarketplaceId == mcLoop.MarketplaceId
        //                        && x.CategoryId == pLoop.CategoryId);


        //                        var anyProductMarketPlace = productMarketplaces.Any(x => x.ProductId == pLoop.Id && x.MarketplaceCompanyId == mcLoop.Id);
        //                        var marketplaceBrand = marketplaceBrandMappings.FirstOrDefault(x => x.BrandId == pLoop.BrandId);

        //                        var added = true;


        //                        if (!anyProductMarketPlace && marketplaceCategory != null && marketplaceBrand != null)
        //                        {
        //                            if (((mcLoop.MarketplaceId == "PA" || mcLoop.MarketplaceId == "TY") && string.IsNullOrEmpty(marketplaceBrand.MarketplaceBrandId)))
        //                            {
        //                                added = false;
        //                            }

        //                            if (added)
        //                                _productMarketplaces.Add(new ProductMarketplace
        //                                {
        //                                    Active = true,
        //                                    MarketplaceBrandId = marketplaceBrand.MarketplaceBrandId,
        //                                    MarketplaceBrandName = marketplaceBrand.MarketplaceBrandName,
        //                                    MarketplaceCategoryId = marketplaceCategory.MarketplaceCategoryId,
        //                                    ManagerUserId = 1,
        //                                    TenantId = tLoop.Id,
        //                                    DomainId = dLoop.Id,
        //                                    CompanyId = theCompany.Id,
        //                                    ProductId = pLoop.Id,
        //                                    SellerCode = pLoop.SellerCode,
        //                                    CreatedDate = DateTime.Now,
        //                                    MarketplaceCompanyId = mcLoop.Id,
        //                                    UUId = Guid.NewGuid().ToString().ToLower()
        //                                });
        //                        }
        //                        foreach (var piLoop in pLoop.ProductInformations)
        //                        {
        //                            var firstProductInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.ProductInformationId == piLoop.Id && x.MarketplaceCompanyId == mcLoop.Id);


        //                            if (firstProductInformationMarketplace == null)
        //                            {

        //                                var dirty = true;
        //#if DEBUG
        //                                dirty = true;
        //#endif

        //                                var productInformationMarketplace = new ProductInformationMarketplace
        //                                {
        //                                    Active = true,
        //                                    AccountingPrice = true,
        //                                    DirtyPrice = dirty,
        //                                    DirtyStock = false,
        //                                    DirtyProductInformation = dirty,
        //                                    Barcode = piLoop.Barcode,
        //                                    ListUnitPrice = piLoop.ProductInformationPriceses.FirstOrDefault().ListUnitPrice,
        //                                    UnitPrice = piLoop.ProductInformationPriceses.FirstOrDefault().UnitPrice,
        //                                    StockCode = piLoop.StockCode,
        //                                    UUId = Guid.NewGuid().ToString().ToLower(),
        //                                    ManagerUserId = 1,
        //                                    CreatedDate = DateTime.Now,
        //                                    TenantId = tLoop.Id,
        //                                    DomainId = dLoop.Id,
        //                                    CompanyId = theCompany.Id,
        //                                    MarketplaceCompanyId = mcLoop.Id,
        //                                    ProductInformationId = piLoop.Id,
        //                                    ProductInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>()
        //                                };

        //                                var variants = new List<string>();

        //                                foreach (var pivLoop in piLoop.ProductInformationVariants)
        //                                {

        //                                    var _marketplaceVariantMappings = marketplaceVariantMappings.Where(x => x.VariantId == pivLoop.VariantValue.VariantId).ToList();

        //                                    var _marketplaceVariantValueMappings = marketplaceVariantValueMappings.Where(x => x.VariantValueId ==
        //                                        pivLoop.VariantValueId && x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();

        //                                    MarketplaceCategoriesMarketplaceVariantValue marketplaceCategoriesMarketplaceVariantValue = null;

        //                                    if (marketplaceCategory != null && _marketplaceVariantMappings.Count > 0 && _marketplaceVariantValueMappings.Count > 0)
        //                                    {
        //                                        foreach (var mv in _marketplaceVariantValueMappings)
        //                                        {
        //                                            marketplaceCategoriesMarketplaceVariantValue = marketplaceCategoriesMarketplaceVariantValues.FirstOrDefault(x =>
        //                                     mv.MarketplaceVariantValueId == x.MarketplaceVariantValueId
        //                                    &&
        //                                      _marketplaceVariantMappings.Any(mv => mv.MarketplaceVariantId == x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId)
        //                                    &&
        //                                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId == marketplaceCategory.MarketplaceCategoryId);
        //                                            if (marketplaceCategoriesMarketplaceVariantValue != null && !variants.Any(x => x == marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId))
        //                                            {
        //                                                variants.Add(marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId);

        //                                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
        //                                                {
        //                                                    MarketplaceCategoriesMarketplaceVariantValueId = marketplaceCategoriesMarketplaceVariantValue.Id,
        //                                                    ManagerUserId = 1,
        //                                                    CreatedDate = DateTime.Now,
        //                                                    TenantId = tLoop.Id,
        //                                                    DomainId = dLoop.Id,
        //                                                    CompanyId = theCompany.Id
        //                                                });
        //                                            }

        //                                        }






        //                                    }

        //                                }
        //                                productInformationMarketplace.ProductInformationMarketplaceVariantValues = productInformationMarketplace.ProductInformationMarketplaceVariantValues.GroupBy(x => x.MarketplaceCategoriesMarketplaceVariantValueId).Select(x => x.First()).ToList();

        //                                _productInformationMarketplaces.Add(productInformationMarketplace);

        //                            }
        //                            else
        //                            {
        //                                var variants = new List<string>();
        //                                variants.AddRange(
        //                                firstProductInformationMarketplace.ProductInformationMarketplaceVariantValues.Select(x => x.MarketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId).ToList());

        //                                foreach (var pivLoop in piLoop.ProductInformationVariants)
        //                                {

        //                                    var _marketplaceVariantMappings = marketplaceVariantMappings.Where(x => x.VariantId == pivLoop.VariantValue.VariantId).ToList();

        //                                    var _marketplaceVariantValueMappings = marketplaceVariantValueMappings.Where(x => x.VariantValueId ==
        //                                        pivLoop.VariantValueId && x.MarketplaceVariantValue.MarketplaceId == mcLoop.MarketplaceId).ToList();

        //                                    MarketplaceCategoriesMarketplaceVariantValue marketplaceCategoriesMarketplaceVariantValue = null;

        //                                    if (marketplaceCategory != null && _marketplaceVariantMappings.Count > 0 && _marketplaceVariantValueMappings.Count > 0)
        //                                    {
        //                                        foreach (var mv in _marketplaceVariantValueMappings)
        //                                        {
        //                                            foreach (var mvm in _marketplaceVariantMappings)
        //                                            {
        //                                                marketplaceCategoriesMarketplaceVariantValue = Enumerable.FirstOrDefault<MarketplaceCategoriesMarketplaceVariantValue>(marketplaceCategoriesMarketplaceVariantValues, (Func<MarketplaceCategoriesMarketplaceVariantValue, bool>)(x =>
        //                                     mv.MarketplaceVariantValueId == x.MarketplaceVariantValueId
        //                                    &&
        //                                     mvm.MarketplaceVariantId == x.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId
        //                                    &&
        //                                    x.MarketplaceCategoriesMarketplaceVariant.MarketplaceCategoryId == marketplaceCategory.MarketplaceCategoryId));


        //                                                if (marketplaceCategoriesMarketplaceVariantValue != null
        //                                                    &&
        //                                                       !firstProductInformationMarketplace.ProductInformationMarketplaceVariantValues.Any(x => x.MarketplaceCategoriesMarketplaceVariantValueId == marketplaceCategoriesMarketplaceVariantValue.Id)
        //                                                    && !variants.Any(x => x == marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId))
        //                                                {
        //                                                    variants.Add(marketplaceCategoriesMarketplaceVariantValue.MarketplaceCategoriesMarketplaceVariant.MarketplaceVariantId);

        //                                                    if (!updateProductInformationMarketplaces.Any(y => y.Id == firstProductInformationMarketplace.Id))
        //                                                    {
        //                                                        var updateProductInformationMarketplace = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().FirstOrDefault(x => x.Id == firstProductInformationMarketplace.Id);
        //                                                        updateProductInformationMarketplace.DirtyProductInformation = true;
        //                                                        updateProductInformationMarketplaces.Add(updateProductInformationMarketplace);

        //                                                    }
        //                                                    _productInformationMarketplaceVariantValues.Add(new ProductInformationMarketplaceVariantValue
        //                                                    {
        //                                                        ProductInformationMarketplaceId = firstProductInformationMarketplace.Id,
        //                                                        MarketplaceCategoriesMarketplaceVariantValueId = marketplaceCategoriesMarketplaceVariantValue.Id,
        //                                                        ManagerUserId = 1,
        //                                                        CreatedDate = DateTime.Now,
        //                                                        TenantId = tLoop.Id,
        //                                                        DomainId = dLoop.Id,
        //                                                        CompanyId = theCompany.Id
        //                                                    });
        //                                                }

        //                                            }
        //                                        }




        //                                    }

        //                                }


        //                            }
        //                        }

        //                    }
        //                }

        //                try
        //                {
        //                    _unitOfWork.ProductInformationMarketplaceRepository.Update(updateProductInformationMarketplaces);
        //                    _unitOfWork.ProductInformationMarketplaceVariantValueRepository.BulkInsert(_productInformationMarketplaceVariantValues);
        //                    _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
        //                    _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);
        //                }
        //                catch (Exception exception)
        //                {
        //                    var error = $"ProductMarketplace Company {dLoop.Name} {exception} {exception.InnerException}";
        //                }
        //            }
        //        }


        private void MarketplaceMatch()
        {
            //var marketPlaceId = "TY";

            //var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().Include(x => x.MarketplaceConfigurations).FirstOrDefault(x => x.Marketplace.Id == marketPlaceId);

            //var _marketPlaces = _marketplaceService.GetProducts(marketPlaceId);
            //var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Where(x => x.MarketplaceCompanyId == marketplaceCompany.Id).ToList();

            //var stockItems = _marketPlaces.Data.SelectMany(x => x.StockItems).ToList();

            //foreach (var pLoop in productInformationMarketplaces)
            //{
            //    var stokItem = stockItems.FirstOrDefault(x => x.Barcode == pLoop.Barcode);

            //    if (stokItem != null)
            //    {
            //        if (marketPlaceId == "TY")
            //        {
            //            var url = $"https://www.trendyol.com/{stokItem.BrandName.ToLower()}/{stokItem.Title.GenerateUrl()}-p-{stokItem.MarketplaceProductId}?merchantId={marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(x => x.Key == "SupplierId").Value}";
            //            pLoop.Url = url;
            //            pLoop.Opened = true;
            //        }
            //        else if (marketPlaceId == "HB")
            //        {
            //            var url = $"https://www.hepsiburada.com/{stokItem.Title.GenerateUrl()}-p-{stokItem.MarketplaceProductId}?magaza=AA";
            //            pLoop.Url = url;
            //        }
            //    }
            //}

            //_unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarketplaces);


        }
        private void N11Match()
        {
            //var getProducts = _marketplaceService.GetProducts("N11");
            //var stockItems = getProducts.Data.SelectMany(x => x.StockItems).Where(x => x.StockCode != null).ToList();

            //var productInformationMarketplaces = _unitOfWork.ProductInformationMarketplaceRepository.DbSet().Where(x => x.MarketplaceCompanyId == 19).ToList();

            //foreach (var piLoop in productInformationMarketplaces)
            //{
            //    var stockItem = stockItems.FirstOrDefault(x => x.StockCode.Contains(piLoop.StockCode));

            //    if (stockItem != null)
            //        piLoop.StockCode = stockItem.StockCode;
            //    else
            //    {

            //    }
            //}

            //_unitOfWork.ProductInformationMarketplaceRepository.Update(productInformationMarketplaces);


        }


        private void VariantValuesNames()
        {
            var productInformations = _unitOfWork.ProductInformationRepository.DbSet().Include(x => x.ProductInformationTranslations).Include(x => x.ProductInformationVariants).ThenInclude(x => x.VariantValue.VariantValueTranslations)
                .Where(x => x.ProductInformationTranslations.Any(y => y.VariantValuesDescription == null))
                .ToList();

            foreach (var pi in productInformations)
            {
                var productInformationVariantValues = pi.ProductInformationVariants.Select(x => x.VariantValue.VariantValueTranslations.FirstOrDefault().Value).ToList();
                var variantValueNames = string.Join(", ", productInformationVariantValues);
                pi.ProductInformationTranslations[0].VariantValuesDescription = $"({variantValueNames})";
            }
            _unitOfWork.ProductInformationTranslationRepository.Update(productInformations.SelectMany(x => x.ProductInformationTranslations).ToList());
        }
        private void RunArtosCity()
        {
            var districts = new List<District>();

            for (int i = 1; i <= 81; i++)
            {
                Console.WriteLine(i);

                using HttpClient client = new HttpClient();

                HttpResponseMessage res = client.GetAsync($"http://webpostman.artosdagitim.com:9999/restapi/client/county/{i}?popup=1").Result;
                HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseString))
                {
                    var districtResponse = responseString.Replace(" ", "").Replace("\n", "").Replace("\"", " ");

                    var beginIndex = districtResponse.IndexOf("result :{");
                    var endIndex = districtResponse.IndexOf(@"}}");
                    var district = districtResponse.Substring(beginIndex, endIndex - beginIndex).Replace("result :{", "").Replace(" ", "").Split(new string[] { ",", ":" }, StringSplitOptions.None);

                    for (int j = 0; j < district.Length; j += 2)
                    {

                        var neighborhoods = new List<Neighborhood>();


                        using HttpClient client1 = new HttpClient();

                        HttpResponseMessage res1 = client.GetAsync($"http://webpostman.artosdagitim.com:9999/restapi/client/district/{i}/{district[j]}?popup=1").Result;
                        HttpContent content1 = res1.Content;
                        string responseString1 = content1.ReadAsStringAsync().Result;

                        var neighborhoodResponse = responseString1.Replace(" ", "").Replace("\n", "").Replace("\"", " ");

                        var beginIndex1 = neighborhoodResponse.IndexOf("result :{");
                        var endIndex1 = neighborhoodResponse.IndexOf(@"}}");

                        if (beginIndex1 != -1 && endIndex1 != -1)
                        {
                            var neighborhood = neighborhoodResponse.Substring(beginIndex1, endIndex1 - beginIndex1).Replace("result :{", "").Replace(" ", "").Split(new string[] { ",", ":" }, StringSplitOptions.None);

                            for (int n = 0; n < neighborhood.Length; n += 2)
                            {
                                neighborhoods.Add(new Neighborhood
                                {
                                    Active = true,
                                    Name = neighborhood[n + 1]
                                });

                            }
                        }
                        else
                        {
                            neighborhoods.Add(new Neighborhood
                            {
                                Active = true,
                                Name = district[j + 1]
                            });
                        }




                        districts.Add(new District
                        {
                            Active = true,
                            CityId = i,
                            Name = district[j + 1],
                            Neighborhoods = neighborhoods
                        });
                    }


                }


            }

            this._unitOfWork.DistrictRepository.BulkInsertMapping(districts);
        }
        public class ArtosCity
        {
            public string result { get; set; }
        }
        private void RunProductPhotos()
        {

            #region Bulkinsert
            //var productInformationPhotos = this._unitOfWork.ProductInformationPhotoRepository.DbSet().ToList();

            //var insertProductInformationPhotos = new List<ProductInformationPhoto>();
            //foreach (var productInformationPhoto in productInformationPhotos)
            //{
            //    var photos = productInformationPhoto.FileName.Split(new string[] { "~" }, StringSplitOptions.RemoveEmptyEntries);

            //    for (int i = 0; i < photos.Length; i++)
            //    {
            //        insertProductInformationPhotos.Add(new ProductInformationPhoto
            //        {
            //            CreatedDate = DateTime.Now,
            //            DisplayOrder = i,
            //            FileName = photos[i],
            //            ProductInformationId = productInformationPhoto.ProductInformationId,
            //            ManagerUserId = 1,
            //            Deleted = false
            //        });
            //    }
            //    //var fileName = $"{Guid.NewGuid()}.jpg";


            //}

            //this._unitOfWork.ProductInformationPhotoRepository.BulkInsert(insertProductInformationPhotos);
            #endregion


            //var productinformations = this._unitOfWork.ProductInformationRepository.DbSet().Include(x => x.ProductInformationTranslations).Include(c => c.ProductInformationPhotos.Where(x => !x.Deleted));
            //var updateProductInformationPhotos = new List<ProductInformationPhoto>();

            //foreach (var productinformation in productinformations)
            //{
            //    foreach (var productInformationPhoto in productinformation.ProductInformationPhotos)
            //    {
            //        var fileName = $"{productinformation.ProductInformationTranslations[0].Name.GenerateUrl()}-{_randomHelper.GenerateNumbers(0, 100000)}.jpg";


            //        var success = Download(fileName, productInformationPhoto.ProductInformationId, productInformationPhoto.FileName);
            //        if (success)
            //        {
            //            productInformationPhoto.FileName = fileName;
            //            updateProductInformationPhotos.Add(productInformationPhoto);

            //        }
            //    }
            //}

            //this._unitOfWork.ProductInformationPhotoRepository.Update(updateProductInformationPhotos);



        }
        #endregion

        /// <summary>
        /// Haberler tablosu url
        /// </summary>
        #region RunInformationUrl
        private void RunInformationUrl()
        {
            var informationTranslations = new List<InformationTranslation>();
            var informations = this._unitOfWork.InformationRepository.DbSet().Include(x => x.InformationTranslations).ToList();
            foreach (var information in informations)
            {
                foreach (var informationTranslation in information.InformationTranslations)
                {
                    informationTranslation.Url = $"{informationTranslation.Header.GenerateUrl()}-{information.Id}-i";
                    informationTranslations.Add(informationTranslation);
                }
            }
            this._unitOfWork.InformationTranslationRepository.Update(informationTranslations);
        }
        #endregion

        #region RunInformationUrl
        private void RunContentUrl()
        {
            var contentTranslations = new List<ContentTranslation>();
            var contens = this._unitOfWork.ContentRepository.DbSet().Include(x => x.ContentTranslations).ToList();
            foreach (var content in contens)
            {
                foreach (var contentTranslation in content.ContentTranslations)
                {
                    contentTranslation.Url = $"{contentTranslation.Header.GenerateUrl()}-{content.Id}-co";
                    contentTranslations.Add(contentTranslation);
                }
            }
            this._unitOfWork.ContentTranslationRepository.Update(contentTranslations);
        }
        #endregion

        #region RunProductUrl
        private void RunGroupUrl()
        {
            //var groupTranslations = new List<GroupTranslation>();
            //var groups = this._unitOfWork.GroupRepository.DbSet().Include(x => x.GroupTranslations).ToList();
            //foreach (var group in groups)
            //{
            //    foreach (var groupTranslation in group.GroupTranslations)
            //    {
            //        groupTranslation.Url = $"{groupTranslation.Name.GenerateUrl()}-{group.Id}-g";
            //        groupTranslations.Add(groupTranslation);
            //    }
            //}
            //this._unitOfWork.GroupTranslationRepository.Update(groupTranslations);

        }

        private void RunBrandUrl()
        {
            var _brands = this._unitOfWork.BrandRepository.DbSet().ToList();
            foreach (var bLoop in _brands)
            {

                bLoop.Url = $"{bLoop.Name.GenerateUrl()}-{bLoop.Id}-b";


            }
            this._unitOfWork.BrandRepository.Update(_brands);

        }


        private void RunProductUrl()
        {
            var productInformationTranslations = new List<ProductInformationTranslation>();
            var products = this._unitOfWork.ProductRepository.DbSet().Include(x => x.ProductInformations).ThenInclude(x => x.ProductInformationTranslations).ToList();
            foreach (var product in products)
            {
                foreach (var productInformation in product.ProductInformations)
                {
                    foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                    {
                        if (string.IsNullOrEmpty(productInformationTranslation.Url))
                        {
                            productInformationTranslation.Url = $"{productInformationTranslation.Name.GenerateUrl()}-{productInformation.Id}-p";
                            productInformationTranslations.Add(productInformationTranslation);
                            _unitOfWork.ProductInformationTranslationRepository.Update(productInformationTranslation);
                        }

                    }

                }
            }

            //this._unitOfWork.ProductInformationTranslationRepository.Update(productInformationTranslations);


        }
        #endregion

        #region RunCategoryUrl
        private void RunCategoryUrl()
        {
            var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
            foreach (var category in categories)
            {
                foreach (var categoryTranslation in category.CategoryTranslations)
                {
                    categoryTranslation.Url = $"{categoryTranslation.Name.GenerateUrl()}-{category.Id}-c";


                    this._unitOfWork.CategoryTranslationRepository.Update(categoryTranslation);
                }
            }
        }
        #endregion

        #region RunProductInformationTranslationBreadcrumb
        private void RunProductInformationTranslationBreadcrumb()
        {

            //var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();

            //var products = this._unitOfWork.ProductRepository.DbSet().Include(x => x.ProductInformations).ThenInclude(y => y.ProductInformationTranslations).ToList();
            //var productInformationTranslationBreadcrumbs = new List<ProductInformationTranslationBreadcrumb>();

            //foreach (var product in products)
            //{
            //    foreach (var productInformation in product.ProductInformations)
            //    {
            //        foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
            //        {
            //            var html = new StringBuilder();
            //            html.AppendLine("<ul>");
            //            var categoryProducts = RecursiveCategoryProduct(categories, product.CategoryId, false);
            //            categoryProducts.Reverse();
            //            foreach (var categoryProduct in categoryProducts)
            //            {
            //                html.AppendLine(categoryProduct);
            //            }

            //            var productInformationTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(productInformationTranslation.Name.ToLower().ReplaceChar());
            //            html.AppendLine($"<li>{productInformationTranslationName}</li>");

            //            html.AppendLine("</ul>");
            //            productInformationTranslationBreadcrumbs.Add(new ProductInformationTranslationBreadcrumb
            //            {
            //                Id = productInformationTranslation.Id,
            //                CreatedDate = DateTime.Now,
            //                ManagerUserId = 1,
            //                Html = html.ToString()
            //            });
            //        }

            //    }
            //}
            //_unitOfWork.ProductInformationTranslationBreadcrumbRepository.BulkInsert(productInformationTranslationBreadcrumbs);
        }
        #endregion

        #region RunCategoryTranslationBreadcrumb
        private void RunCategoryTranslationBreadcrumb()
        {

            //var categories = this._unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
            //var CategoryTranslationBreadcrumbs = new List<CategoryTranslationBreadcrumb>();

            //foreach (var category in categories)
            //{
            //    foreach (var categoryTranslation in category.CategoryTranslations)
            //    {
            //        var html = new StringBuilder();
            //        html.AppendLine("<ul>");
            //        var categoryProducts = RecursiveCategoryProduct(categories, category.Id, true);
            //        categoryProducts.Reverse();
            //        foreach (var categoryProduct in categoryProducts)
            //        {
            //            html.AppendLine(categoryProduct);
            //        }
            //        html.AppendLine("</ul>");
            //        CategoryTranslationBreadcrumbs.Add(new CategoryTranslationBreadcrumb
            //        {
            //            Id = categoryTranslation.Id,
            //            CreatedDate = DateTime.Now,
            //            ManagerUserId = 1,
            //            Html = html.ToString()
            //        });
            //    }
            //}
            //_unitOfWork.CategoryTranslationBreadcrumbRepository.BulkInsert(CategoryTranslationBreadcrumbs);
        }

        private List<string> RecursiveCategoryProduct(List<Category> categories, int categoryId, bool first)
        {
            var html = new List<string>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);
            var CategoryTranslation = category.CategoryTranslations.FirstOrDefault();

            var categoryTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CategoryTranslation.Name.ToLower().ReplaceChar());

            if (!first)
                html.Add($"<li><a href=\"{CategoryTranslation.Url}\">{categoryTranslationName}</a> </li>");
            else
                html.Add($"<li>{categoryTranslationName}</li>");


            if (category.ParentCategoryId.HasValue)
            {
                html.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, false));
            }

            return html;
        }
        #endregion

        #region RunCategoryProduct
        private void RunKartlarCategoryProduct()
        {
            //            SqlConnection sqlConnection = null;
            //            var models = new List<Urun>();
            //            using (sqlConnection = new("Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;"))
            //            {
            //                var query = @"Select P.Id as ProductId,U.katlar from Products P 
            //Inner Join [fistanbul_shop].dbo.urun U ON P.ProductId=U.id
            //where U.katlar <>''";

            //                using (SqlCommand sqlCommand = new(query, sqlConnection))
            //                {
            //                    sqlConnection.Open();

            //                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //                    while (sqlDataReader.Read())
            //                    {
            //                        var id = sqlDataReader.GetInt32(0);
            //                        var katlar = sqlDataReader.GetString(1);
            //                        models.Add(new Urun
            //                        {
            //                            ProductId = id,
            //                            Katlar = katlar
            //                        });
            //                    }
            //                }
            //            }
            //            sqlConnection.Close();
            //            var categories = _unitOfWork.CategoryRepository.DbSet().ToList();


            //            var categoryProducts = new List<CategoryProduct>();

            //            foreach (var mLoop in models)
            //            {
            //                var _categories = mLoop.Katlar.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            //                foreach (var cLoop in _categories)
            //                {
            //                    var category = categories.FirstOrDefault(x => x.Id == Convert.ToInt32(cLoop));
            //                    categoryProducts.Add(new CategoryProduct
            //                    {
            //                        ManagerUserId = 1,
            //                        ProductId = mLoop.ProductId,
            //                        CategoryId = category.Id,
            //                        CreatedDate = DateTime.Now
            //                    });
            //                }

            //            }
            //            _unitOfWork.CategoryProductRepository.BulkInsert(categoryProducts);







        }

        private void RunCategoryProduct()
        {
            var products = _unitOfWork.ProductRepository.DbSet().ToList();
            var categories = _unitOfWork.CategoryRepository.DbSet().ToList();

            var categoryProducts = new List<CategoryProduct>();

            foreach (var product in products)
            {
                categoryProducts.AddRange(RecursiveCategoryProduct(categories, product.CategoryId, product.Id));
            }
            _unitOfWork.CategoryProductRepository.BulkInsert(categoryProducts);
        }

        private List<CategoryProduct> RecursiveCategoryProduct(List<Category> categories, int categoryId, int productId)
        {
            var categoryProducts = new List<CategoryProduct>();

            //var category = categories.FirstOrDefault(x => x.Id == categoryId);

            //categoryProducts.Add(new CategoryProduct
            //{
            //    ManagerUserId = 1,
            //    ProductId = productId,
            //    CategoryId = category.Id,
            //    CreatedDate = DateTime.Now
            //});

            //if (category.ParentCategoryId.HasValue)
            //{
            //    categoryProducts.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, productId));
            //}

            return categoryProducts;
        }
        #endregion

        private bool Download(string path, string fileName, int productInformationId, string url)
        {
            var success = true;


            string directory = $@"{path}\product\{productInformationId}";

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            string pathUrl = $@"{path}\product\{productInformationId}\{fileName.Replace("/", "\\")}";
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, pathUrl);
                    Console.WriteLine("Resim yüklendi");
                }
                catch (Exception e)
                {
                    success = false;
                }
            }

            return success;
        }


        #region Helper
        private DataResponse<List<Brand>> BulkRead(List<Brand> brands)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Brand>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theBrand in brands)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theBrand.Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._configuration.GetConnectionString("Ecommerce")))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkBrand (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkBrand";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(B.Id, 0) As Id
            ,T.Name
From        #TmpTableBulkBrand As T
Left Join   Brands As B With(NoLock)
On          T.Name = B.Name
            And B.DomainId = @DomainId

Drop Table  #TmpTableBulkBrand;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Brand
                        {
                            Id = dataReader.GetInt32("Id"),
                            Name = dataReader.GetString("Name")
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Category>> BulkRead(List<Category> categories)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Category>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Code", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theCategory in categories)
                {
                    var row = dataTable.NewRow();
                    row["Code"] = theCategory.Code;
                    row["Name"] = theCategory.CategoryTranslations.FirstOrDefault().Name;
                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._configuration.GetConnectionString("Ecommerce")))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkCategory (
    Code NVarChar(200) Not Null,
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkCategory";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(C.Id, 0) As Id
            ,T.Code,
            T.Name,
            IsNull(CT.Url,'') as Url
From        #TmpTableBulkCategory As T
Left Join   Categories As C With(NoLock)
On          T.Code = C.Code
            And C.DomainId = @DomainId
Left Join   CategoryTranslations CT ON CT.CategoryId=C.Id
            And CT.DomainId = @DomainId
Drop Table  #TmpTableBulkCategory;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Category
                        {
                            Id = dataReader.GetInt32("Id"),
                            Code = dataReader.GetString("Code"),
                            CategoryTranslations = new List<CategoryTranslation>
                            {
                                new CategoryTranslation
                                {
                                    CategoryId=dataReader.GetInt32("Id"),
                                    Name = dataReader.GetString("Name"),
                                    Url=dataReader.GetString("Url")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Product>> BulkRead(List<Product> products)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Product>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("SellerCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("StockCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));

                foreach (var theProduct in products)
                {
                    var row = dataTable.NewRow();
                    row["SellerCode"] = theProduct.SellerCode;
                    row["StockCode"] = theProduct.ProductInformations.First().StockCode;
                    row["Barcode"] = theProduct.ProductInformations.First().Barcode;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._configuration.GetConnectionString("Ecommerce")))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkProduct (
    SellerCode NVarChar(200) Not Null,
    StockCode NVarChar(200) Not Null,
    Barcode NVarChar(200) Not Null,
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkProduct";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(P.Id, 0) As Id
            ,IsNull(PI.Id, 0) As ProductInformationId
            ,T.SellerCode
            ,T.StockCode
            ,T.Barcode
From        #TmpTableBulkProduct As T
Left Join   Products As P With(NoLock)
On          REPLACE(T.SellerCode,' ','') = REPLACE(P.SellerCode,' ','')
            And P.DomainId = @DomainId
Left Join   ProductInformations As PI With(NoLock)
On          P.Id = PI.ProductId
            And PI.StockCode = T.StockCode
            And PI.Barcode = T.Barcode
            And PI.DomainId = @DomainId
Group by     IsNull(P.Id, 0)
            ,IsNull(PI.Id, 0)
            ,T.SellerCode
            ,T.StockCode
            ,T.Barcode
Drop Table  #TmpTableBulkProduct;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Product
                        {
                            Id = dataReader.GetInt32("Id"),
                            SellerCode = dataReader.GetString("SellerCode"),
                            ProductInformations = new()
                            {
                                new ProductInformation
                                {
                                    Id = dataReader.GetInt32("ProductInformationId"),
                                    StockCode = dataReader.GetString("StockCode"),
                                    Barcode = dataReader.GetString("Barcode")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Variant>> BulkRead(List<Variant> variants)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Variant>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theVariant in variants)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theVariant.VariantTranslations.First().Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._configuration.GetConnectionString("Ecommerce")))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkVariant (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkVariant";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VT.VariantId, 0) As Id
            ,T.Name
From        #TmpTableBulkVariant As T
Left Join   VariantTranslations As VT With(NoLock)
On          T.Name = VT.Name
            And VT.DomainId = @DomainId

Drop Table  #TmpTableBulkVariant;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Variant
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantTranslations = new List<VariantTranslation>
                            {
                                new VariantTranslation
                                {
                                    Name = dataReader.GetString("Name")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<VariantValue>> BulkRead(List<VariantValue> variantValues)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<VariantValue>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("VariantId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Value", typeof(string)));

                foreach (var theVariantValue in variantValues)
                {
                    var row = dataTable.NewRow();
                    row["VariantId"] = theVariantValue.VariantId;
                    row["Value"] = theVariantValue.VariantValueTranslations.First().Value;

                    if (row["Value"] != DBNull.Value)
                        dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(this._configuration.GetConnectionString("Ecommerce")))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table TmpTableBulkVariantValue (
    VariantId Int Not Null,
    Value NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "TmpTableBulkVariantValue";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VV.Id, 0) As Id
            ,IsNull(VV.VariantId, 0) As VariantId
            ,ISNULL(T.Value,VVT.Value) AS Value
From        VariantValues As VV With(NoLock)

Join		VariantValueTranslations As VVT With(NoLock)
On          VVT.VariantValueId = VV.Id
            And VVT.DomainId = @DomainId

Left Join   TmpTableBulkVariantValue As T
On          T.VariantId = VV.VariantId
            And VV.DomainId = @DomainId
			And VVT.Value = T.Value

Drop Table  TmpTableBulkVariantValue;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new VariantValue
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantId = dataReader.GetInt32("VariantId"),
                            VariantValueTranslations = new List<VariantValueTranslation>
                            {
                                new VariantValueTranslation
                                {
                                    Value = dataReader.GetString("Value")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }



        #endregion
    }
}
