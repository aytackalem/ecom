﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Fidanistanbul.Common.Response;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ECommerce.MarketPlace.Fidanistanbul.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields

        #endregion

        #region Constructors
        public OrderOperation()
        {

        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
             {
                 response.Data = new OrderResponse { Orders = new List<Order>() };
                 var _mySqlOrders = GetOrders();

                 if (_mySqlOrders.Success)
                 {

                     var _orders = _mySqlOrders.Data.GroupBy(x => x.Id).Select(x => x.ToList()).ToList();
                     foreach (var pOrder in _orders)
                     {

                         var _order = pOrder.First();
                         var orderDeliveryAddress = CleanCargoTable(_order.CargoName);
                         var orderInvoiceAddress = CleanInvoiceTable(_order.InvoiceName);
                         var totalAmount = _order.Amount;
                         foreach (var pLoop in pOrder)
                         {
                             if (pLoop.UnitPrice == 0)
                             {
                                 pLoop.UnitPrice = pLoop.ListPrice;
                             }
                         }

                         var diffranceAmount = (totalAmount - pOrder.Sum(x => x.UnitPrice * x.Quantity)) / pOrder.Count;
                         orderDeliveryAddress.Email = _order.Mail;
                         orderInvoiceAddress.Email = _order.Mail;
                         //_order.Description = _order.Description + " " + "#il:aydın##ilçe:efeler##mahalle:#";

                         var selectedAdress = _order.Description.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
                         foreach (var sLoop in selectedAdress)
                         {
                             var selectedAdres = sLoop.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                             if (selectedAdres.Length == 2)
                             {
                                 switch (selectedAdres[0].ReplaceChar())
                                 {
                                     case "il":
                                         orderDeliveryAddress.City = selectedAdres[1];
                                         break;
                                     case "ilce":
                                         orderDeliveryAddress.District = selectedAdres[1];
                                         break;
                                     case "mahalle":
                                         orderDeliveryAddress.Neighborhood = $"{selectedAdres[1]} mah";
                                         break;
                                 }
                             }

                         }

                         //eğer ilçe yoksa siparişi içeriye almıyoruz çünkü kargo entegrsayonu yanlış çalışabilir
                         if (string.IsNullOrEmpty(orderDeliveryAddress.District))
                         {
                             continue;
                         }

                         Order order = new Order();
                         order.CurrencyId = "TL";
                         order.TotalAmount = totalAmount;
                         order.ShippingCost = 0;
                         order.Source = "FI";
                         order.OrderNote = _order.OrderNote;
                         order.OrderCode = _order.Id.ToString();
                         order.OrderDate = DateTime.Now;
                         order.OrderShipment = new OrderShipment
                         {
                             TrackingCode = "",
                             ShipmentTypeId = "PND",
                             TrackingUrl = "",
                             ShipmentCompanyId = "ARS",
                             Payor = totalAmount >= 500 // kargo tutarı 500 tl'den yukarıda ise kargo ücretsiz
                         };
                         order.Customer = new Customer()
                         {
                             FirstName = orderDeliveryAddress.FirstName,
                             LastName = orderDeliveryAddress.LastName,
                             IdentityNumber = "",
                             Mail = orderDeliveryAddress.Email,
                             TaxNumber = "",
                             Phone = orderDeliveryAddress.Phone
                         };
                         order.OrderDeliveryAddress = orderDeliveryAddress;
                         order.OrderInvoiceAddress = orderInvoiceAddress;
                         order.Payments = new List<Payment>();
                         order.Payments.Add(new Payment()
                         {
                             CurrencyId = "TRY",
                             PaymentTypeId = "OO",
                             Amount = totalAmount,
                             Date = DateTime.Now
                         });

                         #region Order Details
                         order.OrderDetails = new List<OrderDetail>();
                         foreach (var orderDetail in pOrder)
                         {
                             order.OrderDetails.Add(new OrderDetail()
                             {
                                 TaxRate = 0.18,
                                 UnitPrice = orderDetail.UnitPrice + (diffranceAmount / orderDetail.Quantity),
                                 ListPrice = orderDetail.ListPrice,
                                 Product = new Product()
                                 {
                                     Barcode = orderDetail.Barcode.ToUpper(),
                                     Name = orderDetail.ProductName,
                                     StockCode = orderDetail.ProductCode,

                                 }, //SIPARIS TUTARI 500 TL'DEN YUKSEKSE BUTUN URUNLER UCRETSIZ KARGO VE URUN ETIKETINDE UCRETSIZ KARGO VARISE UCRETSIZ KARGO OLUR
                                 Payor = totalAmount >= 500 || orderDetail.Payor,
                                 Quantity = orderDetail.Quantity
                             });
                         }
                         #endregion

                         response.Data.Orders.Add(order);
                     }
                 }

                 response.Success = true;


             }, (response, exception) =>
             {
                 response.Success = false;
                 response.Message = "";
             });
        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();
                response.Data.IsCancel = false;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Fidanistanbul ile bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }
        #endregion

        private DataResponse<List<MySqlOrderResponse>> GetOrders()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MySqlOrderResponse>>>((response) =>
            {
                var _mySqlOrderResponse = new List<MySqlOrderResponse>();


                using (var mySqlConnection = new MySqlConnection($"Server=94.138.210.114;Uid=data_pooling;Pwd=Aw799ep!;database=fistanbul_shop"))
                {
                    string commandText = @"
Select		O.id,
       	 	O.geneltutar,
            UI.ad,
            UI.gsm,
            US.mail,
            UI.ulke,
            UI.il,
            UI.adres1,
            UI.adres2,
            OD.id,
            OD.urun_adet,
            U.ad,
            U.id AS kod,                             
		    U.fiyat,
            U.i_fiyat,
            O.info,
		    O.fatura,
		    O.kargo,
            O.Durum,
            IFNULL(OU.ozl_id, 0)
 From       siparis AS O
 INNER JOIN siparis_urun OD ON O.id = OD.siparis_id
 INNER JOIN urun U ON U.id = OD.urun_id
 LEFT JOIN  user US ON US.id = O.user_id
 LEFT JOIN  user_info UI ON UI.user_id = US.id
 LEFT JOIN  ozl_urun OU ON OU.urun_id=U.id AND OU.ozl_id=1            
 Where    	O.siparis_id=164865815776
ORDER BY    O.id desc";

                    //O.id=79230 O.siparis_id=164915741427 DATEDIFF(CURDATE(),O.date)<3
                    using (var mySqlCommand = new MySqlCommand(commandText, mySqlConnection))
                    {
                        try
                        {
                            mySqlConnection.Open();

                            mySqlCommand.CommandTimeout = 180;
                            var reader = mySqlCommand.ExecuteReader();
                            while (reader.Read())
                            {
                                _mySqlOrderResponse.Add(new Common.Response.MySqlOrderResponse
                                {
                                    Id = reader.GetInt32(0),
                                    Amount = Convert.ToDecimal(reader.GetString(1).Replace(".", "").Replace(",", ".")),
                                    Name = reader.GetString(2),
                                    Phone = reader.GetString(3),
                                    Mail = reader.GetString(4),
                                    Country = reader.GetString(5),
                                    City = reader.GetString(6),
                                    Address1 = reader.GetString(7),
                                    Address2 = reader.GetString(8),
                                    OrderDetailId = reader.GetInt32(9),
                                    Quantity = reader.GetInt32(10),
                                    ProductName = reader.GetString(11),
                                    Barcode = reader.GetInt32(12).ToString(),
                                    ProductCode = reader.GetInt32(12).ToString(),
                                    ListPrice = Convert.ToDecimal(reader.GetDouble(13)),
                                    UnitPrice = Convert.ToDecimal(reader.GetDouble(14)),
                                    OrderNote = reader.GetString(15),
                                    InvoiceName = reader.GetString(16),
                                    CargoName = reader.GetString(17),
                                    Description = reader.GetString(18),
                                    Payor = reader.GetInt32(19) > 0
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        finally { mySqlConnection.Close(); }
                    }
                }

                response.Data = _mySqlOrderResponse;
                response.Success = true;


            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Fidanistanbul ile bağlantı kurulamıyor.";
                });


        }

        private OrderInvoiceAddress CleanInvoiceTable(string value)
        {
            var orderInvoiceAddress = new OrderInvoiceAddress();
            var array = value
                .Trim()
                .Replace("<td>Ad Soyad</td>", "")
                .Replace("<td>Telefon</td>", "")
                .Replace("<td>Adres</td>", "")
                .Replace("<td>Şehir</td>", "")
                .Replace("<td>Vergi Dairesi</td>", "")
                .Replace("<td>Vergi Numarası</td>", "")
                .Replace("<td>Tc Kimlik No / Vergi No</td>", "")
                .Replace("<td>:</td>", "")
                .Replace(@"<table width=""100%"" border=""0"">", "")
                .Replace("</table>", "")
                .Split(new string[] { "<tr>" }, StringSplitOptions.RemoveEmptyEntries);


            string name = array[1].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            string surname = string.Empty;

            var nameSurname = name.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (nameSurname.Length == 2)
            {
                name = nameSurname[0];
                surname = nameSurname[1];
            }

            orderInvoiceAddress.FirstName = name;
            orderInvoiceAddress.LastName = surname;
            orderInvoiceAddress.Phone = array[2].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderInvoiceAddress.Address = array[3].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderInvoiceAddress.City = array[4].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderInvoiceAddress.TaxOffice = array[5].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderInvoiceAddress.TaxNumber = array[6].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderInvoiceAddress.District = "";
            orderInvoiceAddress.Neighborhood = "";
            return orderInvoiceAddress;
        }

        private OrderDeliveryAddress CleanCargoTable(string value)
        {
            var orderDeliveryAddress = new OrderDeliveryAddress();
            var array = value
                .Trim()
                .Replace("<td>Ad Soyad</td>", "")
                .Replace("<td>Telefon</td>", "")
                .Replace("<td>Adres</td>", "")
                .Replace("<td>Şehir</td>", "")
                .Replace("<td>Vergi Dairesi</td>", "")
                .Replace("<td>Vergi Numarası</td>", "")
                .Replace("<td>:</td>", "")
                .Replace(@"<table width=""100%"" border=""0"">", "")
                .Replace("</table>", "")
                .Split(new string[] { "<tr>" }, StringSplitOptions.RemoveEmptyEntries);

            string name = array[1].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", ""); ;
            string surname = string.Empty;

            var nameSurname = name.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (nameSurname.Length == 2)
            {
                name = nameSurname[0];
                surname = nameSurname[1];
            }

            orderDeliveryAddress.FirstName = name;
            orderDeliveryAddress.LastName = surname;
            orderDeliveryAddress.Phone = array[2].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderDeliveryAddress.Address = array[3].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            orderDeliveryAddress.City = array[4].Replace("<td>", "").Replace("</td>", "").Replace("</tr>", "").Replace("\r", "").Replace("\n", "");
            return orderDeliveryAddress;
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
    }
}
