﻿using System.Security.Cryptography;
using System.Text;

namespace Helpy.Shared.Crypto
{
    public static class CryptoHelper
    {
        #region Fields
        private static readonly string _key = "95434895434821219543489543482121";

        private static readonly string _iv = "3316481528215329";
        #endregion

        #region Methods
        public static string Decrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            byte[] aryKey = Byte8(_key);
            byte[] aryIV = Byte8(_iv);
            RijndaelManaged cp = new();
            MemoryStream ms = new(Convert.FromBase64String(value));
            CryptoStream cs = new(ms, cp.CreateDecryptor(aryKey, aryIV), CryptoStreamMode.Read);
            StreamReader reader = new(cs);
            string result = reader.ReadToEnd();
            reader.Dispose();
            cs.Dispose();
            ms.Dispose();

            return result;
        }

        public static string Encrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            byte[] aryKey = Byte8(_key);
            byte[] aryIV = Byte8(_iv);
            RijndaelManaged dec = new();
            dec.Mode = CipherMode.CBC;
            MemoryStream ms = new();
            CryptoStream cs = new(ms, dec.CreateEncryptor(aryKey, aryIV), CryptoStreamMode.Write);
            StreamWriter writer = new(cs);
            writer.Write(value);
            writer.Flush();
            cs.FlushFinalBlock();
            writer.Flush();
            value = Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
            writer.Dispose();
            cs.Dispose();
            ms.Dispose();

            return value;
        }
        #endregion

        #region Helper Methods
        public static byte[] ByteDonustur(string deger)
        {
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            return ByteConverter.GetBytes(deger);
        }

        public static byte[] Byte8(string value)
        {
            char[] arrayChar = value.ToCharArray();
            byte[] arrayByte = new byte[arrayChar.Length];
            for (int i = 0; i < arrayByte.Length; i++)
            {
                arrayByte[i] = Convert.ToByte(arrayChar[i]);
            }
            return arrayByte;
        }
        #endregion
    }
}
