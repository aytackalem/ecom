﻿namespace Helpy.Shared.Crypto.Extensions
{
    public static class CryptoExtensions
    {
        public static Dictionary<string, string> Decrypt(this Dictionary<string, string> dictionary)
        {
            var encryptDictionary = new Dictionary<string, string>();
            foreach (var key in dictionary.Keys)
            {
                var value = dictionary[key];
                encryptDictionary.Add(CryptoHelper.Decrypt(key), CryptoHelper.Decrypt(value));
            }
            return encryptDictionary;
        }
    }
}
