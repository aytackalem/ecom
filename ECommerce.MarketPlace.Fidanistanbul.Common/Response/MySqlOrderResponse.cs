﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Fidanistanbul.Common.Response
{
    public class MySqlOrderResponse
    {
        public int Id { get; set; }      

        public Decimal Amount { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public int OrderDetailId { get; set; }

        public int Quantity { get; set; }

        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public string Barcode { get; set; }

        public decimal ListPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public string OrderNote { get; set; }

        public string InvoiceName { get; set; }

        public string CargoName { get; set; }

        public string Description { get; set; }
        /// <summary>
        /// Ürün etiketinin içinde ürün kargo ücretsiz mi
        /// </summary>
        public bool Payor { get; set; }

    }
}
