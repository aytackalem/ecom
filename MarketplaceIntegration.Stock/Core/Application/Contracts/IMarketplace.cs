﻿using MarketplaceIntegration.Stock.Core.Application.Parameters;

namespace MarketplaceIntegration.Stock.Core.Application.Contracts
{
    public interface IMarketplace
    {
        Task SendAsync(StockRequest stockRequest);
    }
}
