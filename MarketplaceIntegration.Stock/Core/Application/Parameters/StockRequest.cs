﻿namespace MarketplaceIntegration.Stock.Core.Application.Parameters
{
    public sealed class StockRequest
    {
        public StockRequestHeader Header { get; set; }

        public List<StockRequestItem> Items { get; set; }
    }
}
