﻿namespace MarketplaceIntegration.Stock.Core.Application.Parameters
{
    public sealed class StockRequestHeader
    {
        public string MarketplaceId { get; set; }

        public Dictionary<string, string> Configurations { get; set; }
    }
}
