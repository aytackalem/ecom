﻿namespace MarketplaceIntegration.Stock.Core.Application.Parameters
{
    public sealed class StockRequestItem
    {
        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public int Stock { get; set; }
    }
}
