﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Trendyol;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Trendyol.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Trendyol.Responses;
using MassTransit;
using Notification.Shared;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class TrendyolConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _baseUrl = "https://api.trendyol.com/sapigw";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public TrendyolConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            TrendyolConfiguration trendyolConfiguration = context.Message.Header.Configurations.Decrypt();

            var request = new StockRequest
            {
                Items = context
                    .Message
                    .Items
                    .Select(i => new StockRequestItem
                    {
                        Barcode = i.Barcode,
                        Quantity = i.Stock
                    })
                    .ToList()
            };

            var response = await this._httpHelper.SendAsync<StockRequest, StockResponse, StockBadRequestResponse>(new HttpRequest<StockRequest>
            {
                RequestUri = $"{_baseUrl}/suppliers/{trendyolConfiguration.SupplierId}/products/price-and-inventory",
                Request = request,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Basic,
                Authorization = trendyolConfiguration.Authorization
            });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var sendEndpointProvider = scope.ServiceProvider.GetRequiredService<ISendEndpointProvider>();
                        var sendEndpoint = await sendEndpointProvider.GetSendEndpoint(new Uri($"queue:stock-notification"));
                        foreach (var theItem in context.Message.Items)
                        {
                            await sendEndpoint.Send<StockNotification>(new StockNotification
                            {
                                Title = "Hata",
                                Message = "Stok Güncellenemedi",
                                Barcode = theItem.Barcode,
                                StockCode = theItem.StockCode,
                                CreatedDateTime = DateTime.Now,
                                MarketplaceId = "TY"
                            });
                        }
                    }
                }
            }
            else
            {
                //FATAL ERROR
            }
        }
    }
}
