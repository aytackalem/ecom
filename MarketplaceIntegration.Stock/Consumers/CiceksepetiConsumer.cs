﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Ciceksepeti;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Ciceksepeti.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Ciceksepeti.Responses;
using MassTransit;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class CiceksepetiConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _url = "https://apis.ciceksepeti.com/api/v1/Products/price-and-stock";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public CiceksepetiConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            CiceksepetiConfiguration ciceksepetiConfiguration = context.Message.Header.Configurations.Decrypt();

            var request = new StockRequest
            {
                Items = context
                    .Message
                    .Items
                    .Select(i => new StockRequestItem
                    {
                        StockCode = i.StockCode,
                        StockQuantity = i.Stock
                    })
                    .ToList()
            };

            var response = await this
                ._httpHelper
                .SendAsync<StockRequest, StockResponse, StockBadRequestResponse>(new HttpRequest<StockRequest>
                {
                    RequestUri = _url,
                    HttpMethod = HttpMethod.Put,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfiguration.ApiKey,
                    Request = request
                });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }
    }
}
