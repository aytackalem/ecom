﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Modanisa;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modanisa.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modanisa.Responses;
using MassTransit;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class ModanisaConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _baseUrl = "https://marketplace.modanisa.com/api/marketplace";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public ModanisaConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            ModanisaConfiguration modanisaConfiguration = context.Message.Header.Configurations.Decrypt();

            var request = new StockRequest
            {
                Products = context
                    .Message
                    .Items
                    .Select(i => new StockRequestItem
                    {
                        barcode = i.Barcode,
                        quantity = i.Stock
                    })
                    .ToList()
            };
            var response = await this._httpHelper.SendAsync<StockRequest, StockResponse, StockBadRequestResponse>(new HttpRequest<StockRequest>
            {
                RequestUri = $"{_baseUrl}/updateProductStock",
                Request = request,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Basic,
                Authorization = modanisaConfiguration.Authorization
            });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }
    }
}
