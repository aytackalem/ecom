﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Modalog;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Responses;
using MassTransit;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class ModalogConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _baseUrl = "https://merchant-api.modalog.com";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public ModalogConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            ModalogConfiguration modalogConfiguration = context.Message.Header.Configurations.Decrypt();
            String token = await this.GetToken(modalogConfiguration);

            var request = context
                .Message
                .Items
                .Select(theItem => new StockRequest
                {
                    Barcode = theItem.Barcode,
                    Stock = theItem.Stock
                })
                .ToList();
            var response = await this._httpHelper.SendAsync<List<StockRequest>, StockResponse, StockBadRequestResponse>(new HttpRequest<List<StockRequest>>
            {
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token,
                HttpMethod = HttpMethod.Put,
                RequestUri = $"{_baseUrl}/ProductStockUpdate",
                Request = request
            });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }

        private async Task<string> GetToken(ModalogConfiguration modalogConfiguration)
        {
            var tokenResponse = await _httpHelper.SendAsync<TokenRequest, TokenResponse>(new HttpRequest<TokenRequest>
            {
                RequestUri = $"{_baseUrl}/Auth/Login",
                Request = new TokenRequest
                {
                    Email = modalogConfiguration.Email,
                    Password = modalogConfiguration.Password
                },
                HttpMethod = HttpMethod.Post
            });
            return tokenResponse.Content.data.token;
        }
    }
}
