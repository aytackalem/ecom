﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Hepsiburada;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Hepsiburada.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Hepsiburada.Responses;
using MassTransit;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class HepsiburadaConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _url = "https://listing-external.hepsiburada.com/listings/merchantid/{0}/stock-uploads";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public HepsiburadaConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            HepsiburadaConfiguration hepsiburadaConfiguration = context.Message.Header.Configurations.Decrypt();

            var request = context
                .Message
                .Items
                .Select(i => new StockRequest
                {
                    AvailableStock = i.Stock,
                    MerchantSku = i.StockCode
                })
                .ToList();

            var response = await _httpHelper.SendAsync<List<StockRequest>, StockResponse, StockBadRequestResponse>(new HttpRequest<List<StockRequest>>
            {
                RequestUri = string.Format(_url, hepsiburadaConfiguration.Authorization),
                Request = request,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Basic,
                Authorization = hepsiburadaConfiguration.Authorization
            });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }
    }
}
