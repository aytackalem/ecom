﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Pazarama;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Pazarama.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Pazarama.Responses;
using MassTransit;
using System.Text;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class PazaramaConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _baseUrl = "https://isortagimapi.pazarama.com";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public PazaramaConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            PazaramaConfiguration pazaramaConfiguration = context.Message.Header.Configurations.Decrypt();
            String token = await this.GetToken(pazaramaConfiguration);

            var request = new StockRequest
            {
                Items = context.Message
                    .Items
                    .Select(dpi => new StockRequestItem
                    {
                        Code = dpi.Barcode,
                        StockCount = dpi.Stock
                    })
                    .ToList()
            };

            var response = await this._httpHelper.SendAsync<StockRequest, StockResponse, StockBadRequestResponse>(
                new HttpRequest<StockRequest>
                {
                    RequestUri = $"{_baseUrl}product/updateStock",
                    Request = request,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token
                });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }

        private async Task<string> GetToken(PazaramaConfiguration pazaramaConfiguration)
        {
            var requestUri = $"{_baseUrl}connect/token";
            var token = Convert.ToBase64String(
                Encoding.ASCII.GetBytes($"{pazaramaConfiguration.ApiKey}:{pazaramaConfiguration.SecretKey}"));
            var tokenResponse = await this._httpHelper.SendAsync<TokenResponse>(
                new HttpFormUrlEncodedRequest
                {
                    RequestUri = requestUri,
                    Data = new List<KeyValuePair<string, string>> {
                        new KeyValuePair<string,string>( "grant_type", pazaramaConfiguration.GrantType ),
                        new KeyValuePair<string,string>( "scope", pazaramaConfiguration.Scope)
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = token
                });
            return tokenResponse.Content.data.accessToken;
        }
    }
}
