﻿using Helpy.Shared.Crypto.Extensions;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Morhipo;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Morhipo.Requests;
using MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Morhipo.Responses;
using MassTransit;

namespace MarketplaceIntegration.Stock.Consumers
{
    public class MorhipoConsumer : IConsumer<Core.Application.Parameters.StockRequest>
    {
        private const string _baseUrl = "https://mpapi.morhipo.com/mpstore/listing/SaveStockByBarcode";

        private readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        public MorhipoConsumer(IHttpHelper httpHelper, IServiceProvider serviceProvider)
        {
            _httpHelper = httpHelper;
            _serviceProvider = serviceProvider;
        }

        public async Task Consume(ConsumeContext<Core.Application.Parameters.StockRequest> context)
        {
            MorhipoConfiguration morhipoConfiguration = context.Message.Header.Configurations.Decrypt();

            var request = new StockRequest
            {
                products = context
                    .Message
                    .Items
                    .Select(i => new StockRequestItem
                    {
                        b = i.Barcode,
                        s = i.Stock
                    })
                    .ToList()
            };
            var response = await this._httpHelper.SendAsync<StockRequest, StockResponse, StockBadRequestResponse>(new HttpRequest<StockRequest>
            {
                Request = request,
                RequestUri = _baseUrl,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Basic,
                Authorization = morhipoConfiguration.Authorization
            });

            if (response.Success)
            {
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {

                }
                else if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {

                }
            }
            else
            {
                //FATAL ERROR
            }
        }
    }
}
