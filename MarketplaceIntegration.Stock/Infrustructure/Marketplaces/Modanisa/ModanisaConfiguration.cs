﻿using System.Text;

namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Modanisa
{
    public class ModanisaConfiguration
    {
        #region Members
        public static implicit operator ModanisaConfiguration(Dictionary<string, string> configurations)
        {
            return new ModanisaConfiguration
            {
                Username = configurations["Username"],
                Password = configurations["Password"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));
        #endregion
    }
}
