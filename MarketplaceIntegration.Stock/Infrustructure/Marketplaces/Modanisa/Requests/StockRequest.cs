﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modanisa.Requests
{
    public class StockRequest
    {
        public List<StockRequestItem> Products { get; set; }
    }
}
