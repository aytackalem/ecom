﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modanisa.Requests
{
    public class StockRequestItem
    {
        public string variant_id { get; set; }

        public int quantity { get; set; }

        public string barcode { get; set; }
    }
}
