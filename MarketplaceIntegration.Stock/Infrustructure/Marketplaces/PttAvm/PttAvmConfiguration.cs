﻿using System.Collections.Generic;

namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.PttAvm
{
    public class PttAvmConfiguration
    {
        #region Members
        public static implicit operator PttAvmConfiguration(Dictionary<string, string> configurations)
        {
            return new PttAvmConfiguration
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                ShopId = int.Parse(configurations["ShopId"])
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public int ShopId { get; set; }
        #endregion
    }
}
