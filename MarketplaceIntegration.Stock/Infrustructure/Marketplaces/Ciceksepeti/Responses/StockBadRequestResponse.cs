﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Ciceksepeti.Responses
{
    public sealed class StockBadRequestResponse
    {
        public string Message { get; set; }
    }
}
