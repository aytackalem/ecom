﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Ciceksepeti.Requests
{
    public class StockRequest
    {
        [JsonProperty("items")]
        public List<StockRequestItem> Items { get; set; }
    }
}
