﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Ciceksepeti.Requests
{
    public class StockRequestItem
    {
        public string StockCode { get; set; }

        public int StockQuantity { get; set; }
    }
}
