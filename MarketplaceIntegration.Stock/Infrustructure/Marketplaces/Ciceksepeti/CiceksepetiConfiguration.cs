﻿namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Ciceksepeti
{
    public class CiceksepetiConfiguration
    {
        #region Members
        public static implicit operator CiceksepetiConfiguration(Dictionary<string, string> configurations)
        {
            return new CiceksepetiConfiguration
            {
                ApiKey = configurations["ApiKey"]
            };
        }
        #endregion

        #region Properties
        public string ApiKey { get; set; }
        #endregion
    }
}
