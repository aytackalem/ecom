﻿using System.Text;

namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Trendyol
{
    public class TrendyolConfiguration
    {
        #region Members
        public static implicit operator TrendyolConfiguration(Dictionary<string, string> configurations)
        {
            return new TrendyolConfiguration
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                SupplierId = configurations["SupplierId"]
            };
        } 
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));

        public string SupplierId { get; set; }
        #endregion
    }
}
