﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Trendyol.Requests
{
    public class StockRequest
    {
        [JsonProperty("items")]
        public List<StockRequestItem> Items { get; set; }
    }
}
