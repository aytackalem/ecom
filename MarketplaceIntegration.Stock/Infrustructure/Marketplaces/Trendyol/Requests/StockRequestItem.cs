﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Trendyol.Requests
{
    public class StockRequestItem
    {
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
    }
}
