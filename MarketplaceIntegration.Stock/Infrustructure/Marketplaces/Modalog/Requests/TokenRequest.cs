﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Requests
{
    public class TokenRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
