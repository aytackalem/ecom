﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Requests
{
    public class StockRequest
    {
        public string Barcode { get; set; }
        
        public int Stock { get; set; }
    }
}
