﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Responses
{
    public class Data
    {
        #region Properties
        public string token { get; set; }

        public DateTime expiration { get; set; }
        #endregion
    }

    public class TokenResponse
    {
        #region Properties
        public bool success { get; set; }

        public string message { get; set; }

        public Data data { get; set; }

        public int rowCount { get; set; }

        public int currentPageIndex { get; set; }

        public int pageSize { get; set; }

        public int pageCount { get; set; }
        #endregion
    }
}
