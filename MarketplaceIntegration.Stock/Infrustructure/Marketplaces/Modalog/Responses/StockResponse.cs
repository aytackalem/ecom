﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Modalog.Responses
{
    public class StockResponse
    {
        public bool success { get; set; }
        public object message { get; set; }
        public UpdateStockResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class UpdateStockResponseItem
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string barcode { get; set; }
    }
}
