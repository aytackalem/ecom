﻿namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.Pazarama
{
    public class PazaramaConfiguration
    {
        #region Members
        public static implicit operator PazaramaConfiguration(Dictionary<string, string> configurations)
        {
            return new PazaramaConfiguration
            {
                GrantType = "client_credentials",
                ApiKey = configurations["ApiKey"],
                MerchantId = configurations["MerchantId"],
                SecretKey = configurations["SecretKey"],
                Scope = "merchantgatewayapi.fullaccess"
            };
        }
        #endregion

        #region Properties
        public string GrantType { get; internal set; }

        public string ApiKey { get; internal set; }

        public string MerchantId { get; internal set; }

        public string SecretKey { get; internal set; }

        public string Scope { get; internal set; }
        #endregion
    }
}
