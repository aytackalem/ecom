﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Pazarama.Requests
{
    public class StockRequestItem
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("stockCount")]
        public int StockCount { get; set; }
    }
}
