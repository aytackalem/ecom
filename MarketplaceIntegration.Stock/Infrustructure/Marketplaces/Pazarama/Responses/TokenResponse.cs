﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Pazarama.Responses
{
    public class TokenResponse
    {
        #region Properties
        public bool success { get; set; }

        public string messageCode { get; set; }

        public string message { get; set; }

        public string userMessage { get; set; }

        public TokenData data { get; set; }
        #endregion
    }

    public class TokenData
    {
        #region Properties
        public string accessToken { get; set; }

        public object refreshToken { get; set; }

        public int expiresIn { get; set; }

        public string tokenType { get; set; }

        public string scope { get; set; }
        #endregion
    }
}
