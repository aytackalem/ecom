﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Morhipo.Requests
{
    public class StockRequest
    {
        public List<StockRequestItem> products { get; set; }
    }
}
