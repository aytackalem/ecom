﻿namespace MarketplaceIntegration.Stock.Infrastructure.Marketplaces.N11
{
    public class N11Configuration
    {
        #region Members
        public static implicit operator N11Configuration(Dictionary<string, string> configurations)
        {
            return new N11Configuration
            {
                AppSecret = configurations["AppSecret"],
                AppKey = configurations["AppKey"],
                ShipmentTemplate = configurations["ShipmentTemplate"]
            };
        }
        #endregion

        #region Properties
        public string AppSecret { get; internal set; }

        public string AppKey { get; internal set; }

        public string ShipmentTemplate { get; internal set; }
        #endregion
    }
}
