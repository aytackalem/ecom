﻿namespace MarketplaceIntegration.Stock.Infrustructure.Marketplaces.Hepsiburada.Requests
{
    public sealed class StockRequest
    {
        public string HepsiburadaSku { get; set; }

        public string MerchantSku { get; set; }

        public int AvailableStock { get; set; }
    }
}
