using Helpy.Shared.Http.Extensions;
using MarketplaceIntegration.Stock.Consumers;
using MassTransit;

namespace MarketplaceIntegration.Stock
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddHttpHelper(ServiceLifetime.Scoped);

            builder.Services.AddMassTransit(mt =>
            {
                mt.AddConsumer<CiceksepetiConsumer>();
                mt.AddConsumer<HepsiburadaConsumer>();
                mt.AddConsumer<ModalogConsumer>();
                mt.AddConsumer<ModanisaConsumer>();
                mt.AddConsumer<MorhipoConsumer>();
                mt.AddConsumer<N11Consumer>();
                mt.AddConsumer<PazaramaConsumer>();
                mt.AddConsumer<PttAvmConsumer>();
                mt.AddConsumer<TrendyolConsumer>();

                mt.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(builder.Configuration["RabbitMqUrl"], "/", host =>
                    {
                        host.Username("guest");
                        host.Password("guest");
                    });

                    configurator.ReceiveEndpoint("CS-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<CiceksepetiConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("HB-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<HepsiburadaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("ML-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<ModalogConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("MN-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<ModanisaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("M-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<MorhipoConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("N11-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<N11Consumer>(context);
                    });

                    configurator.ReceiveEndpoint("PA-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<PazaramaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("PTT-stock", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<PttAvmConsumer>(context);
                    });
                });
            });

            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}