using MarketplaceIntegration.Stock.Core.Application.Parameters;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace MarketplaceIntegration.Stock.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StockController : ControllerBase
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;

        public StockController(ISendEndpointProvider sendEndpointProvider)
        {
            this._sendEndpointProvider = sendEndpointProvider;
        }

        [HttpPost]
        public async Task<IActionResult> Post(StockRequest stockRequest)
        {
            var sendEndpoint = await this._sendEndpointProvider.GetSendEndpoint(new Uri($"queue:{stockRequest.Header.MarketplaceId}-stock"));
            await sendEndpoint.Send<StockRequest>(stockRequest);

            return Ok();
        }
    }
}