﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ECommerce.UrlReplacer
{
    class Url
    {
        public string Old { get; set; }

        public string New { get; set; }
    }

    class ProductInformationContentTranslation
    {
        public int Id { get; set; }

        public string ExpertOpinion { get; set; }

        public string Content { get; set; }

        public string Description { get; set; }
    }

    class InformationTranslationContent
    {
        public int Id { get; set; }

        public string Html { get; set; }
    }

    class ContentTranslationContent
    {
        public int Id { get; set; }

        public string Html { get; set; }
    }

    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static void Main(string[] args)
        {
            var urls = new List<Url>();

            //TO DO: Read Urls
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(@"
With T As (
	Select	[Url], [Target] + IsNull([QS], '') As [Target] 
	From	GoogleLinks With(NoLock) 
	Where	[Target] Is Not Null And Id <> 18047

	Union

	Select	[Url], [Target] 
	From	RedirectLinks With(NoLock) 
	Where	[Target] Is Not Null And [Url] Not Like '/%'
)
Select * From T Group By [Url], [Target] Order By Len([Url]) Desc", sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();

                        var sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                        {
                            urls.Add(new Url
                            {
                                Old = sqlReader.GetString(0),
                                New = sqlReader.GetString(1)
                            });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }
            }

            //RunProductInformationContentTranslation(urls);
            RunInformationTranslationContent(urls);
            RunContentTranslationContent(urls);
        }

        private static void RunProductInformationContentTranslation(List<Url> urls)
        {
            Int32 id = 0;

            ProductInformationContentTranslation productInformationContentTranslation = null;

            while (true)
            {
                productInformationContentTranslation = null;

                //TO DO: Read Product Information Content Translation
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Select Top 1 Id, ExpertOpinion, Content, Description From ProductInformationContentTranslations Where Id > @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);

                        try
                        {
                            sqlConnection.Open();

                            var sqlReader = sqlCommand.ExecuteReader();
                            while (sqlReader.Read())
                            {
                                productInformationContentTranslation = new ProductInformationContentTranslation
                                {
                                    Id = sqlReader.GetInt32(0),
                                    ExpertOpinion = sqlReader.GetString(1),
                                    Content = sqlReader.GetString(2),
                                    Description = sqlReader.GetString(3)
                                };
                            }
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                if (productInformationContentTranslation == null)
                    break;

                productInformationContentTranslation.ExpertOpinion = productInformationContentTranslation.ExpertOpinion.Replace("http://https://", "https://");
                productInformationContentTranslation.Content = productInformationContentTranslation.Content.Replace("http://https://", "https://");
                productInformationContentTranslation.Description = productInformationContentTranslation.Description.Replace("http://https://", "https://");

                //TO DO: Replace Expert Opinion, Content, Description
                foreach (var uLoop in urls)
                {
                    productInformationContentTranslation.ExpertOpinion = productInformationContentTranslation.ExpertOpinion.Replace(uLoop.Old, uLoop.New);
                    productInformationContentTranslation.Content = productInformationContentTranslation.Content.Replace(uLoop.Old, uLoop.New);
                    productInformationContentTranslation.Description = productInformationContentTranslation.Description.Replace(uLoop.Old, uLoop.New);
                }

                //TO DO: Update Product Information Content Translation
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update ProductInformationContentTranslations Set ExpertOpinion = @ExpertOpinion, Content = @Content, [Description] = @Description Where Id = @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", productInformationContentTranslation.Id);
                        sqlCommand.Parameters.AddWithValue("@ExpertOpinion", productInformationContentTranslation.ExpertOpinion);
                        sqlCommand.Parameters.AddWithValue("@Content", productInformationContentTranslation.Content);
                        sqlCommand.Parameters.AddWithValue("@Description", productInformationContentTranslation.Description);

                        try
                        {
                            sqlConnection.Open();

                            var ar = sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                id = productInformationContentTranslation.Id;

                Console.Clear();
                Console.WriteLine($"Product Information Content Translation {id}");
            }
        }

        private static void RunInformationTranslationContent(List<Url> urls)
        {
            Int32 id = 0;

            InformationTranslationContent informationTranslationContent = null;

            while (true)
            {
                informationTranslationContent = null;

                //TO DO: Read Information Translation Content
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Select Top 1 Id, Html From InformationTranslationContents Where Id > @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);

                        try
                        {
                            sqlConnection.Open();

                            var sqlReader = sqlCommand.ExecuteReader();
                            while (sqlReader.Read())
                            {
                                informationTranslationContent = new InformationTranslationContent
                                {
                                    Id = sqlReader.GetInt32(0),
                                    Html = sqlReader.GetString(1)
                                };
                            }
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                if (informationTranslationContent == null)
                    break;

                informationTranslationContent.Html = informationTranslationContent.Html.Replace("http://https://", "https://");

                //TO DO: Replace Html
                foreach (var uLoop in urls)
                {
                    informationTranslationContent.Html = informationTranslationContent.Html.Replace(uLoop.Old, uLoop.New);
                }

                //TO DO: Update Product Information Content Translation
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update InformationTranslationContents Set Html = @Html Where Id = @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", informationTranslationContent.Id);
                        sqlCommand.Parameters.AddWithValue("@Html", informationTranslationContent.Html);

                        try
                        {
                            sqlConnection.Open();

                            var ar = sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                id = informationTranslationContent.Id;

                Console.Clear();
                Console.WriteLine($"Information Translation Content {id}");
            }
        }

        private static void RunContentTranslationContent(List<Url> urls)
        {
            Int32 id = 0;

            ContentTranslationContent contentTranslationContent = null;

            while (true)
            {
                contentTranslationContent = null;

                //TO DO: Read Information Translation Content
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Select Top 1 Id, Html From ContentTranslationContents Where Id > @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);

                        try
                        {
                            sqlConnection.Open();

                            var sqlReader = sqlCommand.ExecuteReader();
                            while (sqlReader.Read())
                            {
                                contentTranslationContent = new ContentTranslationContent
                                {
                                    Id = sqlReader.GetInt32(0),
                                    Html = sqlReader.GetString(1)
                                };
                            }
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                if (contentTranslationContent == null)
                    break;

                contentTranslationContent.Html = contentTranslationContent.Html.Replace("http://https://", "https://");

                //TO DO: Replace Html
                foreach (var uLoop in urls)
                {
                    contentTranslationContent.Html = contentTranslationContent.Html.Replace(uLoop.Old, uLoop.New);
                }

                //TO DO: Update Content Translation Contents
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update ContentTranslationContents Set Html = @Html Where Id = @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", contentTranslationContent.Id);
                        sqlCommand.Parameters.AddWithValue("@Html", contentTranslationContent.Html);

                        try
                        {
                            sqlConnection.Open();

                            var ar = sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }
                    }
                }

                id = contentTranslationContent.Id;

                Console.Clear();
                Console.WriteLine($"Content Translation Content {id}");
            }
        }
    }
}
