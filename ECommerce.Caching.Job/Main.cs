﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Caching.Job
{
    public class Main : IMain
    {
        #region Fields
        private readonly IDbService _dbService;

        private readonly IServiceProvider _serviceProvider;
        #endregion

        #region Constructors
        public Main(IDbService dbService, IServiceProvider serviceProvider)
        {
            #region Fields
            this._dbService = dbService;
            this._serviceProvider = serviceProvider;
            #endregion
        }
        #endregion

        #region Methods
        public async Task Run(string[] args)
        {
            var getNameResponse = await this._dbService.GetNamesAsync();
            if (getNameResponse.Success == false)
            {
                //TO DO: Log
            }
            var dbNames = getNameResponse.Data;
            if (dbNames.HasItem())
                foreach (var dbName in dbNames)
                {
                    using (var scope = this._serviceProvider.CreateScope())
                    {
                        var dbNameFinder = scope.ServiceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(dbName);

                        var tenantFinder = (DynamicTenantFinder)scope.ServiceProvider.GetRequiredService<ITenantFinder>();
                        var domainFinder = (DynamicDomainFinder)scope.ServiceProvider.GetRequiredService<IDomainFinder>();
                        var companyFinder = (DynamicCompanyFinder)scope.ServiceProvider.GetRequiredService<ICompanyFinder>();
                        var _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                        var tenants = await _unitOfWork.TenantRepository.DbSet().Include(t => t.TenantSetting).ToListAsync();
                        foreach (var theTenant in tenants)
                        {
                            tenantFinder._tenantId = theTenant.Id;

                            var domains = await _unitOfWork.DomainRepository.DbSet().Include(d => d.DomainSetting).ToListAsync();
                            foreach (var theDomain in domains)
                            {
                                domainFinder._domainId = theDomain.Id;

                                var companies = await _unitOfWork.CompanyRepository.DbSet().Include(c => c.CompanySetting).Where(x=>x.CompanySetting.IncludeECommerce).ToListAsync();
                                foreach (var theCompany in companies)
                                {
                                    companyFinder._companyId = theCompany.Id;
                                    using (var appScope = this._serviceProvider.CreateScope())
                                    {
                                        

                                        var appV2 = appScope.ServiceProvider.GetRequiredService<IApp>();
                                        appV2.Run(theCompany.CompanySetting.WebUrl,args);

                                    }
                                }
                            }
                        }
                    }

                }
        }
        #endregion
    }
}