﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace ECommerce.Caching.Job
{
    public class App : IApp
    {
        private readonly IConfiguration _configuration;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IDbNameFinder _dbNameFinder;


        public App(IConfiguration configuration, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IDbNameFinder dbNameFinder)
        {
            this._configuration = configuration;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._dbNameFinder = dbNameFinder;
        }
        public void Run(string webUrl, string[] args)
        {
       

            string type = "Categories";

#if !DEBUG
        type = args[0];
#endif


            var connectionString = string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName());


            var urls = new List<string>();
            urls.Add("");

            if (type == "Products")
                using (SqlConnection sqlConnection = new(connectionString))
                {
                    var query = @"
                    Select	[PIT].[Url]
                    From	Products As P With(NoLock)
                    Join	ProductInformations As [PI] With(NoLock)
                    On		P.Id = [PI].ProductId
                    Join	ProductInformationTranslations As [PIT] With(NoLock)
                    On		[PI].Id = [PIT].ProductInformationId
                    Where	P.Active = 1
                            And P.TenantId=@TenantId
							And P.DomainId=@DomainId
                            And P.IsOnlyHidden=0";

                    try
                    {
                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlConnection.Open();
                            sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                            sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                urls.Add(sqlDataReader.GetString(0));
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

            if (type == "Categories")
                using (SqlConnection sqlConnection = new(connectionString))
                {
                    var query = @"
                        Select		CT.[Url], Count(0)
                        From		Categories As C
                        Left Join	CategoryProducts AS CP
                        On			C.Id = CP.CategoryId
                        Join		CategoryTranslations As CT
                        On			C.Id = CT.CategoryId
                        Where		C.Active = 1
                        			And C.TenantId=@TenantId
                        			And C.DomainId=@DomainId
                        Group By	CT.[Url], CT.[CreatedDate]
                        Order By	Count(0) Desc";

                    try
                    {
                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlConnection.Open();
                            sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                            sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                var url = sqlDataReader.GetString(0);

                                urls.Add(url);

                           
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }


            if (type == "Informations")
                using (SqlConnection sqlConnection = new(connectionString))
                {
                    var query = @"Select [Url] From InformationTranslations With(NoLock)       
                                            Where TenantId=@TenantId
                        			        And DomainId=@DomainId";

                    try
                    {
                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlConnection.Open();
                            sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                            sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                urls.Add(sqlDataReader.GetString(0));
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

            if (type == "Contents")
                using (SqlConnection sqlConnection = new(connectionString))
                {
                    var query = @"Select [Url] From ContentTranslations With(NoLock) 
                                            Where  TenantId=@TenantId
                        			        And DomainId=@DomainId";

                    try
                    {
                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlConnection.Open();
                            sqlCommand.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                            sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                urls.Add(sqlDataReader.GetString(0));
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

            foreach (var url in urls)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        var queryType = !url.Contains("?") ? "?" : "&";

                        var _url = $"{webUrl}/{url}{queryType}caching=false";
                        Console.WriteLine($"{_url} Eklendi.");
                        webClient.DownloadString(_url);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex}");

                }
            }
        }

    }
}
