﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Caching.Job
{
    public interface IApp
    {
        #region Methods
        void Run(string webUrl,string[] args);
        #endregion
    }
}
