﻿using System.Threading.Tasks;

namespace ECommerce.Caching.Job
{
    public interface IMain
    {
        #region Methods
        Task Run(string[] args); 
        #endregion
    }
}
