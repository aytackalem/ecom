﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Caching.Job
{
    public class DynamicDomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public DynamicDomainFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }

        public void Set(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
