﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Caching.Job
{
    class Program
    {

        public Program(IConfiguration configuration)
        {
        }

        static async Task Main(string[] args)
        {

#if DEBUG
            
#else
            var type = args[0];
#endif
            await ConfigureServiceCollection(GetConfiguration()).GetRequiredService<IMain>()?.Run(args);



        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;

            ServiceCollection serviceCollection = new();



            serviceCollection.AddScoped<IMain, Main>();

            serviceCollection.AddScoped<IApp, App>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddScoped<IDbService, DbService>();

            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();

            serviceCollection.AddDbContext<ApplicationDbContext>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            return serviceCollection.BuildServiceProvider();
        }

        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }
    }
}
