﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Terminal.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Terminal.Interfaces
{
    public interface IOrderService
    {
        #region Method
        /// <summary>
        /// Siparişler Terminalde gösterilir
        /// </summary>
        /// <returns></returns>
        DataResponse<Order> Get();
        /// <summary>
        /// Ürün terminalde hazılanırken kullanıcı atla derse güncelleme yapılır
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Response Skip(Order order);

        /// <summary>
        /// Ürün terminalde onaylanırsa güncelleme yapılır
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Response Approved(Order order);

        /// <summary>
        /// Sipariş listesi mobilde gösterilir.
        /// </summary>
        /// <param name="pagedRequest"></param>
        /// <returns></returns>
        PagedResponse<Order> Read(PagedRequest pagedRequest);

        DataResponse<Summary> Summary();
        #endregion
    }
}
