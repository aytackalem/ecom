﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Terminal.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Terminal.Interfaces
{
    public interface IProductService
    {
        #region Method

        DataResponse<ProductInformation> ReadByBarcode(string barcode);

        PagedResponse<Product> Read(PagedRequest pagedRequest);
        #endregion
    }
}
