﻿using ECommerce.Application.Common.DataTransferObjects.Widgets;
using System.Collections.Generic;

namespace ECommerce.Application.Terminal.ViewModels.Homes
{
    public class Index
    {
        #region Navigation Properties
        public List<MarketplaceCount> MarketplaceCounts { get; set; }

        public List<MarketplaceProductCount> MarketplaceProductCounts { get; set; }

        public FinancialSummary FinancialSummary { get; set; }

        public OrderSummary OrderSummary { get; set; }

        public List<TopSeller> TopSellers { get; set; }
        #endregion
    }
}
