﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Terminal.Constants
{
    public static class CacheKey
    {
        #region Properties
        public static string Authentication => "Authentication-{0}-{1}";
        #endregion
    }
}
