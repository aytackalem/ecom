﻿namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        #region Property


        /// <summary>
        /// Dosya adı.
        /// </summary>
        public string FileName { get; set; }


        /// <summary>
        /// Ürün Sırası
        /// </summary>
        public int DisplayOrder { get; set; }

        #endregion


    }
}
