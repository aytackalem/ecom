﻿using System.Collections.Generic;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class VariantTranslation
    {
        #region Properties
        public int Id { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
