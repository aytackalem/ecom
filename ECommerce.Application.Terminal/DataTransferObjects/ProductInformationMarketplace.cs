﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class ProductInformationMarketplace
    {
        public string MarketplaceId { get; set; }
        public string Url { get; set; }
        public bool Opened { get; set; }
        public decimal ListUnitPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public bool Active { get; set; }
    }
}
