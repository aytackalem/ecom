﻿using System.Collections.Generic;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class ProductInformationVariant
    {
        #region Properties
        public int VariantValueId { get; set; }
        #endregion

        public VariantValue VariantValue { get; set; }
    }
}
