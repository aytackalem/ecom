﻿using System.Collections.Generic;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class MarketPlace
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

    }
}
