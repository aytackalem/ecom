﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class Customer
    {
        #region Property
        /// <summary>
        /// Müşteri adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Müşteri soyadı.
        /// </summary>
        public string Surname { get; set; }
        #endregion
    }
}
