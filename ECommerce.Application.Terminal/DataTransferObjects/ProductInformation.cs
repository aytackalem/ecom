﻿using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class ProductInformation
    {
        #region Properties
        public int Id { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public string ModelCode { get; set; }

        public bool Payor { get; set; }

        public string ProductInformationName { get; set; }

        public string ShelfZone { get; set; }

        public string ShelfCode { get; set; }

        public int Stock { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public List<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        public List<ProductInformationVariant> ProductVariants { get; set; }

        public List<ProductInformationPrice> ProductInformationPriceses { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }
        public string ProductInformationUrl { get; set; }
        #endregion


    }
}
