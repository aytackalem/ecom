﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class ManagerUser
    {
        #region Properties
        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        public int TenantId { get; set; }

        public string ManagerRoleId { get; set; }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        #endregion

        #region Navigation Properties
        public List<Domain> Domains { get; set; }
        #endregion
    }
}
