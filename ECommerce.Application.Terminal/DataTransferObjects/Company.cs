﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Terminal
{
    public class Company
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Logo { get; set; }
        #endregion
    }
}
