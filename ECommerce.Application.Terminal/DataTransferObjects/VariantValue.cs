﻿using System.Collections.Generic;

namespace ECommerce.Application.Terminal.DataTransferObjects
{
    public class VariantValue
    {
        #region Properties
        public string VariantId { get; set; }

        public string Value { get; set; }
        #endregion

        #region Navigation Properties
        public Variant Variant { get; set; }
        #endregion
    }
}
