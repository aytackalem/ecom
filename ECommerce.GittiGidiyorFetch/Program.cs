﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ECommerce.GittiGidiyorFetch
{
    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static string _apiKey = "Wya5SJfnd2TvFD2v48fh9CmPqmzjzrxK";

        static string _username = "meyvefidancim";

        static string _password = "BY9vJqz3VCNCHB9SC7DfmJD5qgcBdZBW";

        static string _secretKey = "99k5cRn9Mk2PgjtD";

        static void Main(string[] args)
        {
            var products = GetProducts();

            BulkProducts(products);

            var categories = GetCategories(products.Select(p => p.ProductData.CategoryCode).Distinct().ToArray());

            BulkCategories(products, categories);

            BulkCategoryAttributes(products);

            BulkCategoryAttributeValues(products);

            BulkProductCategoryAttributeValue(products);
        }

        #region Products
        static List<Product.Product> GetProducts()
        {
            List<Product.Product> products = new();

            int page = 0;
            int pageSize = 30;
            int pagesCount = 0;

            do
            {

                var productResponse = PostProducts(page, pageSize);

                products.AddRange(productResponse.Products.Product);

                if (pagesCount == 0)
                    pagesCount = Convert.ToInt32(Math.Ceiling(productResponse.ProductCount / (pageSize * 1M)));

                page++;

                Console.WriteLine(page);

            } while (page < pagesCount);

            return products;
        }

        static Product.Return PostProducts(int page, int pageSize)
        {
            var time = TimeLong().ToString();

            var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <prod:getProducts>
         <apiKey>{_apiKey}</apiKey>
         <sign>{Md5Hash(time)}</sign>
         <time>{time}</time>
         <startOffSet>{page * pageSize}</startOffSet>
         <rowCount>{pageSize}</rowCount>
         <status>A</status>
         <withData>true</withData>
         <lang>tr</lang>
      </prod:getProducts>
   </soapenv:Body>
</soapenv:Envelope>";

            var response = Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;

            Product.Return @return = null;

            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Product.Return));
                @return = (Product.Return)xmlSerializer.Deserialize(stringReader);
            }

            return @return;
        }

        static ProductInsert.Return InsertProduct()
        {
            var time = TimeLong().ToString();

            var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <prod:insertAndActivateProduct>
         <apiKey>{_apiKey}</apiKey>
         <sign>{Md5Hash(time)}</sign>
         <time>{time}</time>
         <itemId>?</itemId>
         <product>
            <!--Optional:-->
            <categoryCode>?</categoryCode>
            <!--Optional:-->
            <storeCategoryId></storeCategoryId>
            <!--Optional:-->
            <title>?</title>
            <!--Optional:-->
            <subtitle>?</subtitle>
            <!--Optional:-->
            <specs>
               <!--Zero or more repetitions:-->
               <spec name=""?"" value=""?"" type=""?"" required=""?""/>
            </specs>
            <!--Optional:-->
            <photos>
               <!--Zero or more repetitions:-->
               <photo photoId=""?"">
                  <!--Optional:-->
                  <url>?</url>
                  <!--Optional:-->
                  <base64>?</base64>
               </photo>
            </photos>
            <!--Optional:-->
            <pageTemplate>?</pageTemplate>
            <!--Optional:-->
            <description>?</description>
            <!--Optional:-->
            <startDate>?</startDate>
            <!--Optional:-->
            <catalogId>?</catalogId>
            <!--Optional:-->
            <newCatalogId>?</newCatalogId>
            <!--Optional:-->
            <catalogDetail>?</catalogDetail>
            <!--Optional:-->
            <catalogFilter>?</catalogFilter>
            <!--Optional:-->
            <format>?</format>
            <!--Optional:-->
            <startPrice>?</startPrice>
            <!--Optional:-->
            <buyNowPrice>?</buyNowPrice>
            <!--Optional:-->
            <netEarning>?</netEarning>
            <!--Optional:-->
            <listingDays>?</listingDays>
            <!--Optional:-->
            <productCount>?</productCount>
            <!--Optional:-->
            <cargoDetail>
               <!--Optional:-->
               <city>?</city>
               <!--Optional:-->
               <cargoCompanies>
                  <!--Zero or more repetitions:-->
                  <cargoCompany>?</cargoCompany>
               </cargoCompanies>
               <!--Optional:-->
               <shippingPayment>?</shippingPayment>
               <!--Optional:-->
               <cargoDescription>?</cargoDescription>
               <!--Optional:-->
               <shippingWhere>?</shippingWhere>
               <!--Optional:-->
               <shippingFeePaymentType>?</shippingFeePaymentType>
               <!--Optional:-->
               <cargoCompanyDetails>
                  <!--Zero or more repetitions:-->
                  <cargoCompanyDetail>
                     <!--Optional:-->
                     <name>?</name>
                     <!--Optional:-->
                     <value>?</value>
                     <!--Optional:-->
                     <cityPrice>?</cityPrice>
                     <!--Optional:-->
                     <countryPrice>?</countryPrice>
                  </cargoCompanyDetail>
               </cargoCompanyDetails>
               <!--Optional:-->
               <shippingTime>
                  <!--Optional:-->
                  <days>?</days>
                  <!--Optional:-->
                  <beforeTime>?</beforeTime>
               </shippingTime>
               <!--Optional:-->
               <productPackageSize>
                  <!--Optional:-->
                  <width>?</width>
                  <!--Optional:-->
                  <height>?</height>
                  <!--Optional:-->
                  <depth>?</depth>
                  <!--Optional:-->
                  <weight>?</weight>
                  <!--Optional:-->
                  <desi>?</desi>
               </productPackageSize>
            </cargoDetail>
            <!--Optional:-->
            <affiliateOption>?</affiliateOption>
            <!--Optional:-->
            <boldOption>?</boldOption>
            <!--Optional:-->
            <catalogOption>?</catalogOption>
            <!--Optional:-->
            <vitrineOption>?</vitrineOption>
            <!--Optional:-->
            <variantGroups>
               <!--Zero or more repetitions:-->
               <variantGroup nameId=""?"" valueId=""?"" alias=""?"">
                  <!--Optional:-->
                  <variants>
                     <!--Zero or more repetitions:-->
                     <variant variantId=""?"" operation=""?"">
                        <!--Optional:-->
                        <variantSpecs>
                           <!--Zero or more repetitions:-->
                           <variantSpec nameId=""?"" name=""?"" valueId=""?"" value=""?"" orderNumber=""?"" specDataOrderNumber=""?""/>
                        </variantSpecs>
                        <!--Optional:-->
                        <quantity>?</quantity>
                        <!--Optional:-->
                        <stockCode>?</stockCode>
                        <!--Optional:-->
                        <soldCount>?</soldCount>
                        <!--Optional:-->
                        <newCatalogId>?</newCatalogId>
                     </variant>
                  </variants>
                  <!--Optional:-->
                  <photos>
                     <!--Zero or more repetitions:-->
                     <photo photoId=""?"">
                        <!--Optional:-->
                        <url>?</url>
                        <!--Optional:-->
                        <base64>?</base64>
                     </photo>
                  </photos>
               </variantGroup>
            </variantGroups>
            <!--Optional:-->
            <auctionProfilePercentage>?</auctionProfilePercentage>
            <!--Optional:-->
            <marketPrice>?</marketPrice>
            <!--Optional:-->
            <globalTradeItemNo>?</globalTradeItemNo>
            <!--Optional:-->
            <manufacturerPartNo>?</manufacturerPartNo>
            <!--Optional:-->
            <sameDayDeliveryTypes>
               <!--Zero or more repetitions:-->
               <sameDayDeliveryType>
                  <!--Optional:-->
                  <lastReceivingTime>?</lastReceivingTime>
                  <!--Optional:-->
                  <shippingFirmId>?</shippingFirmId>
                  <!--Optional:-->
                  <deliveryOption>?</deliveryOption>
               </sameDayDeliveryType>
            </sameDayDeliveryTypes>
         </product>
         <forceToSpecEntry>?</forceToSpecEntry>
         <nextDateOption>?</nextDateOption>
         <lang>tr</lang>
      </prod:insertAndActivateProduct>
   </soapenv:Body>
</soapenv:Envelope>";

            var response = Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;

            ProductInsert.Return @return = null;

            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ProductInsert.Return));
                @return = (ProductInsert.Return)xmlSerializer.Deserialize(stringReader);
            }

            return @return;
        }

        static UpdateStock.Return UpdateStock()
        {
            var time = TimeLong().ToString();

            var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <prod:updateStock>
         <apiKey>{_apiKey}</apiKey>
         <sign>{Md5Hash(time)}</sign>
         <time>{time}</time>
         <productId>?</productId>
         <itemId>?</itemId>
         <stock>?</stock>
         <cancelBid>?</cancelBid>
         <lang>tr</lang>
      </prod:updateStock>
   </soapenv:Body>
</soapenv:Envelope>";

            var response = Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;

            UpdateStock.Return @return = null;

            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpdateStock.Return));
                @return = (UpdateStock.Return)xmlSerializer.Deserialize(stringReader);
            }

            return @return;
        }

        static UpdatePrice.Return UpdatePrice()
        {
            var time = TimeLong().ToString();

            var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <prod:updatePrice>
         <apiKey>{_apiKey}</apiKey>
         <sign>{Md5Hash(time)}</sign>
         <time>{time}</time>
         <productId>?</productId>
         <itemId>?</itemId>
         <price>?</price>
         <cancelBid>?</cancelBid>
         <lang>tr</lang>
      </prod:updatePrice>
   </soapenv:Body>
</soapenv:Envelope>";

            var response = Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;

            UpdatePrice.Return @return = null;

            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(UpdatePrice.Return));
                @return = (UpdatePrice.Return)xmlSerializer.Deserialize(stringReader);
            }

            return @return;
        }
        #endregion

        #region Categories
        static List<Category.Category> GetCategories(string[] categoryCodes)
        {
            List<Category.Category> categories = new();

            foreach (var ccLoop in categoryCodes)
            {
                var categoryResponse = PostCategory(ccLoop);
                categories.Add(categoryResponse.Categories.Category);
            }

            return categories;
        }

        static Category.Return PostCategory(string categoryCode)
        {
            var time = TimeLong().ToString();

            var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:cat=""http://category.anonymous.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <cat:getCategoriesByCodes>
         <categoryCodes>
            <item>{categoryCode}</item>
         </categoryCodes>
         <withSpecs>true</withSpecs>
         <withDeepest>true</withDeepest>
         <withCatalog>true</withCatalog>
         <lang>tr</lang>
      </cat:getCategoriesByCodes>
   </soapenv:Body>
</soapenv:Envelope>";

            var response = Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/CategoryService?wsdl");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;

            Category.Return @return = null;

            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Category.Return));
                @return = (Category.Return)xmlSerializer.Deserialize(stringReader);
            }

            return @return;
        }
        #endregion

        static string Post(string data, string address)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
            webRequest.ContentType = "text/xml";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_username}:{_password}")));

            using (var stream = webRequest.GetRequestStream())
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(data);
                }
            }

            var response = string.Empty;

            using (var streamReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }

            return response;
        }

        static long TimeLong()
        {
            TimeSpan span = (TimeSpan)(DateTime.UtcNow - new DateTime(1970, 1, 1));
            return (span.Ticks / 0x2710L);
        }

        static string Md5Hash(string time)
        {
            var input = _apiKey + _secretKey + time;

            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        static void BulkProducts(List<Product.Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("ProductId", typeof(int));
            dataTable.Columns.Add("CategoryCode", typeof(string));
            dataTable.Columns.Add("Title", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Format", typeof(string));
            dataTable.Columns.Add("ListingDays", typeof(int));
            dataTable.Columns.Add("PageTemplate", typeof(int));

            products
                .Select(p => new
                {
                    ProductId = p.ProductId,
                    CategoryCode = p.ProductData.CategoryCode,
                    Title = p.ProductData.Title,
                    Description = p.ProductData.Description,
                    Format = p.ProductData.Format,
                    ListingDays = p.ProductData.ListingDays,
                    PageTemplate = p.ProductData.PageTemplate
                })
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.ProductId;
                    dataRow[1] = p.CategoryCode;
                    dataRow[2] = p.Title;
                    dataRow[3] = p.Description;
                    dataRow[4] = p.Format;
                    dataRow[4] = p.ListingDays;
                    dataRow[4] = p.PageTemplate;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "GGProducts";

                sqlBulkCopy.ColumnMappings.Add("ProductId", "ProductId");
                sqlBulkCopy.ColumnMappings.Add("CategoryCode", "CategoryCode");
                sqlBulkCopy.ColumnMappings.Add("Title", "Title");
                sqlBulkCopy.ColumnMappings.Add("Description", "Description");
                sqlBulkCopy.ColumnMappings.Add("Format", "Format");
                sqlBulkCopy.ColumnMappings.Add("ListingDays", "ListingDays");
                sqlBulkCopy.ColumnMappings.Add("PageTemplate", "PageTemplate");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkCategories(List<Product.Product> products, List<Category.Category> categories)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Code", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .Select(p => new
                {
                    Code = p.ProductData.CategoryCode,
                    Name = categories.First(c => c.CategoryCode == p.ProductData.CategoryCode).CategoryName
                })
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.Code;
                    dataRow[1] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "GGCategories";

                sqlBulkCopy.ColumnMappings.Add("Code", "Code");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkCategoryAttributes(List<Product.Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("CategoryCode", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .Where(p => p.ProductData.Specs != null)
                .SelectMany(p => p.ProductData.Specs.Spec.Select(a => new
                {
                    CategoryCode = p.ProductData.CategoryCode,
                    Name = a.Name
                }))
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.CategoryCode;
                    dataRow[1] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "GGCategoryAttributes";

                sqlBulkCopy.ColumnMappings.Add("CategoryCode", "CategoryCode");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkCategoryAttributeValues(List<Product.Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Value", typeof(string));

            products
                .Where(p => p.ProductData.Specs != null)
                .SelectMany(p => p.ProductData.Specs.Spec.Select(a => new
                {
                    Name = a.Name,
                    Value = a.Value
                }))
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.Name;
                    dataRow[1] = p.Value;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "GGCategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("Name", "Name");
                sqlBulkCopy.ColumnMappings.Add("Value", "Value");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void BulkProductCategoryAttributeValue(List<Product.Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("ProductId", typeof(int));
            dataTable.Columns.Add("CategoryAttributeValueName", typeof(string));

            products
                .Where(p => p.ProductData.Specs != null)
                .SelectMany(p => p.ProductData.Specs.Spec.Select(a => new
                {
                    ProductId = p.ProductId,
                    CategoryAttributeValueName = a.Value
                }))
                .Distinct()
                .ToList()
                .ForEach(a =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = a.ProductId;
                    dataRow[1] = a.CategoryAttributeValueName;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "GGProductCategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("ProductId", "ProductId");
                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeValueName", "CategoryAttributeValueName");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
