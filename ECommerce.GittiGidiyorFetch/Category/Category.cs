﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ECommerce.GittiGidiyorFetch.Category
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Return));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Return)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "shippingTimes")]
    public class ShippingTimes
    {

        [XmlElement(ElementName = "shippingTime")]
        public List<string> ShippingTime { get; set; }
    }

    [XmlRoot(ElementName = "values")]
    public class Values
    {

        [XmlElement(ElementName = "value")]
        public List<string> Value { get; set; }
    }

    [XmlRoot(ElementName = "spec")]
    public class Spec
    {

        [XmlElement(ElementName = "values")]
        public Values Values { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "required")]
        public bool Required { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "specs")]
    public class Specs
    {

        [XmlElement(ElementName = "spec")]
        public List<Spec> Spec { get; set; }
    }

    [XmlRoot(ElementName = "category")]
    public class Category
    {

        [XmlElement(ElementName = "categoryCode")]
        public string CategoryCode { get; set; }

        [XmlElement(ElementName = "categoryName")]
        public string CategoryName { get; set; }

        [XmlElement(ElementName = "shippingTimes")]
        public ShippingTimes ShippingTimes { get; set; }

        [XmlElement(ElementName = "specs")]
        public Specs Specs { get; set; }

        [XmlAttribute(AttributeName = "hasCatalog")]
        public bool HasCatalog { get; set; }

        [XmlAttribute(AttributeName = "deepest")]
        public bool Deepest { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "categories")]
    public class Categories
    {

        [XmlElement(ElementName = "category")]
        public Category Category { get; set; }
    }

    [XmlRoot(ElementName = "return")]
    public class Return
    {

        [XmlElement(ElementName = "ackCode")]
        public string AckCode { get; set; }

        [XmlElement(ElementName = "responseTime")]
        public string ResponseTime { get; set; }

        [XmlElement(ElementName = "timeElapsed")]
        public string TimeElapsed { get; set; }

        [XmlElement(ElementName = "categoryCount")]
        public int CategoryCount { get; set; }

        [XmlElement(ElementName = "categories")]
        public Categories Categories { get; set; }
    }


}
