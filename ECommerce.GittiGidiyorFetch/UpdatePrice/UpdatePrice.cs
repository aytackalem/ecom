﻿using System;
using System.Xml.Serialization;

namespace ECommerce.GittiGidiyorFetch.UpdatePrice
{
    [XmlRoot(ElementName = "return")]
	public class Return
	{

		[XmlElement(ElementName = "ackCode")]
		public string AckCode { get; set; }

		[XmlElement(ElementName = "responseTime")]
		public DateTime ResponseTime { get; set; }

		[XmlElement(ElementName = "timeElapsed")]
		public string TimeElapsed { get; set; }

		[XmlElement(ElementName = "productId")]
		public int ProductId { get; set; }

		[XmlElement(ElementName = "result")]
		public string Result { get; set; }
	}
}
