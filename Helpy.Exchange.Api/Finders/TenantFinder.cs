﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace Helpy.Exchange.Api.Finders
{
    public class TenantFinder : ITenantFinder
    {
        #region Fields
        private int _tenantId = 0;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public TenantFinder(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            if (this._tenantId == 0)
            {
                var user = this._httpContextAccessor.HttpContext.User;
                if (user != null)
                {
                    var claim = user.Claims.FirstOrDefault(c => c.Type == "tenantId");
                    if (claim != null)
                    {
                        this._tenantId = int.Parse(claim.Value);
                    }
                }
            }

            return this._tenantId;
        }
        #endregion
    }
}
