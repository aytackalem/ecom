﻿using Asp.Versioning;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Helpy.Exchange.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion(1.0)]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class RatesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public RatesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string currencyId, string date)
        {
            try
            {
                var createdDate = DateTime.Parse(date, new CultureInfo("tr-TR"));

                var exchangeRate = await _unitOfWork
                    .ExchangeRateRepository
                    .DbSet()
                    .FirstOrDefaultAsync(er =>
                        er.CurrencyId == currencyId &&
                        er.CreatedDate == createdDate.Date);

                return Ok(new
                {
                    Code = 200,
                    Data = new { Rate = exchangeRate.Rate }
                });
            }
            catch (Exception)
            {

                return BadRequest(new
                {
                    Code = 200,
                    Message = "Bilinmeyen bir hata oluştu."
                });
            }
        }
    }
}