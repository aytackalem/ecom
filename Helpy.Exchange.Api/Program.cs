using Asp.Versioning;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Exchange.Api.Finders;
using Helpy.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Helpy.Exchange.Api
{
#if DEBUG
    public class CustomHeaderSwaggerAttribute : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "DomainId",
                In = ParameterLocation.Header,
                Required = true,
                Schema = new OpenApiSchema
                {
                    Type = "integer"
                }
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "CompanyId",
                In = ParameterLocation.Header,
                Required = true,
                Schema = new OpenApiSchema
                {
                    Type = "integer"
                }
            });
        }
    }
#endif

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

#if DEBUG
            //builder.Services.AddSwaggerGen(sa =>
            //{
            //    sa.OperationFilter<CustomHeaderSwaggerAttribute>();
            //});
#endif
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddDbContext<ApplicationDbContext>();
            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();
            builder.Services.AddScoped<ITenantFinder, TenantFinder>();
            builder.Services.AddScoped<IDomainFinder, DomainFinder>();
            builder.Services.AddScoped<ICompanyFinder, CompanyFinder>();
            builder.Services.AddApiVersioning(options =>
            {
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
            builder.Services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
                {
#if DEBUG
                    o.RequireHttpsMetadata = false;
#endif

                    o.Authority = builder.Configuration["IdentityServerUrl"];
                    o.Audience = "ApiResourceExchange";
                });
            builder.Services.AddControllers();

            var app = builder.Build();

#if DEBUG
            //app.UseSwagger();
            //app.UseSwaggerUI();
#endif

            app.UseDomainIdCheck();
            app.UseAuthentication();
            app.UseAuthorization();

#if !DEBUG
            app.UseHttpsRedirection();
            app.UseHsts();
#endif

            app.MapControllers();

            app.Run();
        }
    }
}