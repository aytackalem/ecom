﻿namespace ECommerce.Application.Panel.Constants
{
    public static class CacheKey
    {
        public static string Setting => "Setting-{0}-{1}-{2}-{3}";

        public static string RolePages => "{0}-RolePages";

        public static string Domains => "Domains";
        
        public static string Company => "{0}-Domains_Company";
        
        public static string NeighborhoodsByDistrictId => "{0}-{1}-NeighborhoodsByDistrictId";
        
        public static string DistrictsByCityId => "{0}-{1}-DistrictsByCityId";
        
        public static string CitysByContryId => "{0}-{1}-CitysByContryId";

        public static string MarketplaceCategoryId => "{0}-MarketplaceCategoryId";

        public static string MarketplaceVariants => "MarketplaceVariants";

        public static string NotificationCount => "{0}-{1}-NotificationCount";
        public static string Notifications => "{0}-{1}-{2}-{3}-{4}-Notifications";
    }
}
