﻿using ECommerce.Application.Panel.DataTransferObjects;

namespace ECommerce.Application.Panel.ViewModels.Companies
{
    public class Create
    {
        #region Properties
        public Company Company { get; set; }
        #endregion
    }
}
