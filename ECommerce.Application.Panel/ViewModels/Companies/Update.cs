﻿using ECommerce.Application.Panel.DataTransferObjects;

namespace ECommerce.Application.Panel.ViewModels.Companies
{
    public class Update
    {
        #region Properties
        public Company Company { get; set; }
        #endregion
    }
}
