﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Payments
{
    public class PaymentPartial
    {
        #region Properties
        public List<Payment> Payments { get; set; }
        #endregion
    }
}
