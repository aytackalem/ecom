﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductInformationExpenses
{
    public class Create
    {
        #region Properties
        public List<KeyValue<int, string>> ExpenseSources { get; set; }
        #endregion
    }
}