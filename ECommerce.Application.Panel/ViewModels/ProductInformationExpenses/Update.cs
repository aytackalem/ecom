﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductInformationExpenses
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.ProductInformationExpense ProductInformationExpense { get; set; }

        public List<KeyValue<int, string>> ExpenseSources { get; set; }
        #endregion
    }
}
