﻿using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.ViewModels.Orders
{
    public class Update
    {
        #region Properties
        public bool OrderDetailMultiselect { get; set; }
      
        public List<Common.DataTransferObjects.KeyValue<string, string>> ShipmentCompanies { get; set; }

        public List<Common.DataTransferObjects.KeyValue<int, string>> Countries { get; set; }

        public List<Common.DataTransferObjects.KeyValue<int, string>> OrderSources { get; set; }

        public List<Common.DataTransferObjects.KeyValue<string, string>> OrderTypes { get; set; }

        public List<Common.DataTransferObjects.KeyValue<string, string>> PaymentTypes { get; set; }

        public List<Common.DataTransferObjects.KeyValue<string, string>> MarketPlaces { get; set; }

        public Order Order { get; set; }

        public List<City> OrderDeliveryCitys { get; set; }

        public List<District> OrderDeliveryDistricts { get; set; }

        public List<Neighborhood> OrderDeliveryNeighborhoods { get; set; }

        public List<City> OrderInvoiceCitys { get; set; }

        public List<District> OrderInvoiceDistricts { get; set; }

        public List<Neighborhood> OrderInvoiceNeighborhoods { get; set; }
        public bool IncludeExport { get; set; }
        #endregion
    }
}
