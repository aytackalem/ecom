﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Orders
{
    public class OrderDetailPartial
    {
        #region Properties

        public List<ECommerce.Application.Panel.DataTransferObjects.OrderDetail> OrderDetails { get; set; }
        #endregion
    }
}
