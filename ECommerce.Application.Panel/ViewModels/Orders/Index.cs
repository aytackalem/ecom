﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Orders
{
    public class Index
    {
        #region Properties
        public bool BarcodeWritable { get; set; }

        public bool OrderDetailMultiselect { get; set; }

        public bool ShowStock { get; set; }

        public List<KeyValue<int, string>> OrderSources { get; set; }

        public List<KeyValue<string, string>> OrderTypes { get; set; }

        public List<KeyValue<string, string>> MarketPlaces { get; set; }

        public List<KeyValue<string, string>> ShipmentCompanies { get; set; }
        #endregion
    }
}
