﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Orders
{
    public class Create
    {
        #region Properties
        public bool OrderDetailMultiselect { get; set; }

        public List<KeyValue<string, string>> ShipmentCompanies { get; set; }

        public List<KeyValue<int, string>> Countries { get; set; }

        public List<ECommerce.Application.Panel.DataTransferObjects.City> Citys { get; set; }

        public List<KeyValue<int, string>> OrderSources { get; set; }

        public List<KeyValue<string, string>> OrderTypes { get; set; }

        public List<KeyValue<string, string>> PaymentTypes { get; set; }

        public List<KeyValue<string, string>> MarketPlaces { get; set; }
        public bool IncludeExport { get; set; }
        #endregion
    }
}
