﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Variants
{
    public class Update
    {
        #region Properties
        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion

        #region Navigation Properties
        public Variant Variant { get; set; }
        #endregion
    }
}
