﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Contacts
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.Contact Contact { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
