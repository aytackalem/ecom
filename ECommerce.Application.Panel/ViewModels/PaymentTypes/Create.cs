﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.PaymentTypes
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}