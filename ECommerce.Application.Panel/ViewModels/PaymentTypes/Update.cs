﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.PaymentTypes
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.PaymentType PaymentType { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
