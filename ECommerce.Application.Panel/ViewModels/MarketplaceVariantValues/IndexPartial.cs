﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.MarketplaceVariantValues
{
    public class IndexPartial
    {
        #region Properties
        public int VariantValueId { get; set; }

        public string VV { get; set; }

        public List<MarketplaceVariant> MarketplaceVariants { get; set; }

        public List<MarketplaceVariantValueMapping> MarketplaceVariantValueMappings { get; set; }
        #endregion

        #region Navigation Properties
        public Marketplace Marketplace { get; set; }
        #endregion
    }
}
