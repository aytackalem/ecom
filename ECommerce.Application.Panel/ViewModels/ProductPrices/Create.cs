﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductPrices
{
    public class Create
    {
        #region Properties
        public bool IncludeAccounting { get; set; }

        public List<KeyValue<string, string>> MarketplacePrices { get; set; }

        public List<KeyValue<int, string>> ActionPrices { get; set; }

        public List<KeyValue<int, string>> FractionPrices { get; set; }

        /// <summary>
        /// Erp/Xml için Fiyat güncellensin mi datasını döner.
        /// </summary>
        /// <returns></returns>
        public List<KeyValue<int, string>> AccountingPrices { get; set; }
        #endregion
    }
}
