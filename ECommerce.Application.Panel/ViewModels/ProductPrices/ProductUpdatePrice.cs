﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductPrices
{
    public class ProductUpdatePrice
    {
        #region Properties
        public List<MarketplaceUpdateProductPrice> MarketplaceUpdateProductPrices { get; set; }


        /// <summary>
        /// Erp/Xml için Fiyat güncellensin mi datasını döner.
        /// </summary>
        /// <returns></returns>
        public List<KeyValue<int, string>> UpdateProductPrices { get; set; }
        #endregion
    }
}
