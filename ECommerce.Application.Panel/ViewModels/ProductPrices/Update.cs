﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductPrices
{
    public class Update
    {
        #region Properties
        public List<KeyValue<string, int>> MarketplaceUpdatePrices { get; set; }
        #endregion
    }
}
