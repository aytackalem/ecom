﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ManagerUsers
{
    public class Create
    {
        #region Properties
        public List<KeyValue<int, string>> ManagerRoles { get; set; }
        #endregion
    }
}