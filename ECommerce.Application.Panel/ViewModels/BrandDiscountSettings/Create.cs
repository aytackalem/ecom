﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.BrandDiscountSettings
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Applications { get; set; }

        public List<KeyValue<int, string>> Brands { get; set; }
        #endregion
    }
}