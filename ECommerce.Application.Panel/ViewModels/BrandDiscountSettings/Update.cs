﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.BrandDiscountSettings
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.BrandDiscountSetting BrandDiscountSetting { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }

        public List<KeyValue<int, string>> Brands { get; set; }
        #endregion
    }
}
