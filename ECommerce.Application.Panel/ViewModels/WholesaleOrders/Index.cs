﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.WholesaleOrders;

public class Index
{
    public List<KeyValue<string, string>> WholesaleOrderTypes { get; set; }
}
