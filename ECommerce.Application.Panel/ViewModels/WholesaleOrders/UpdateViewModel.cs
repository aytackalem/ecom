﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.WholesaleOrders;

public class UpdateViewModel
{
    public List<KeyValue<string, string>> WholesaleOrderTypes { get; set; }

    public WholesaleOrder WholesaleOrder { get; set; }
}
