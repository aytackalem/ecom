﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.WholesaleOrders;

public class Create
{
    public bool IsExport { get; set; }
    public List<WholesaleOrderDetail> WholesaleOrderDetails { get; set; }
}
