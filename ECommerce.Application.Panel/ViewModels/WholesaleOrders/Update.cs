﻿namespace ECommerce.Application.Panel.ViewModels.WholesaleOrders;

public class Update
{
    public int Id { get; set; }

    public string WholesaleOrderTypeId { get; set; }
}
