﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductCategorizations;

public class GetViewModel
{
    public List<GetItem> Items { get; set; }
}

public class GetItem
{
    public string Color { get; set; }

    public string Code { get; set; }

    public string Categorization { get; set; }

    public bool SpecialCategorization { get; set; }
}