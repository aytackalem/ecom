﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.DiscountCoupons
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}