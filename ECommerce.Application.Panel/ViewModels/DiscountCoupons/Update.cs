﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.DiscountCoupons
{
    public class Update
    {
        #region Properties
        public DiscountCoupon DiscountCoupon { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}
