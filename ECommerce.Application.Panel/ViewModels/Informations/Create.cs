﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Informations
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}