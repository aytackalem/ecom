﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports;

public class Last36ViewModel
{
    public Last36ViewModel()
    {
        Years = new();
        Months = new();
    }

    public List<int> Years { get; set; }

    public List<int> Months { get; set; }

    public List<Last12MonthSale> Items { get; set; }

    public List<Last12MonthSale> AllItems { get; set; }
}