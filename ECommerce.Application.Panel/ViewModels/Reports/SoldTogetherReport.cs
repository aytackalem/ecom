﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public class SoldTogetherReport
    {
        #region Properties
        public string StartDateString { get; set; }

        public string EndDateString { get; set; }
        #endregion

        #region Navigation Properties
        public List<DataTransferObjects.SoldTogetherReport> Datas { get; set; }
        #endregion
    }
}
