﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public class ProductBasedStockReport
    {
        public string ReceiptFileName { get; set; }

        #region Navigation Properties
        public List<DataTransferObjects.ProductBasedStockReport> Datas { get; set; }
        public bool HasReceipt { get; set; }
        #endregion
    }
}
