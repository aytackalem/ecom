﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public class PackagingReport
    {
        #region Properties
        public string StartDateString { get; set; }

        public string EndDateString { get; set; }
        #endregion

        #region Navigation Properties
        public List<KeyValue<string, int>> Datas { get; set; }
        #endregion
    }
}
