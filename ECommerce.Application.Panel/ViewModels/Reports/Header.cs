﻿namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public class Header
    {
        #region Properties
        /// <summary>
        /// Toplam satış tutarı.
        /// </summary>
        public decimal TotalSaleAmount { get; set; }

        /// <summary>
        /// Toplam ürün maliyet tutarı.
        /// </summary>
        public decimal TotalProductCostAmount { get; set; }

        /// <summary>
        /// Toplam komisyon tutarı.
        /// </summary>
        public decimal TotalCommissionAmount { get; set; }

        /// <summary>
        /// Toplam kargo maliyet tutarı.
        /// </summary>
        public decimal TotalShippingCostAmount { get; set; }

        /// <summary>
        /// Toplam gider tutarı.
        /// </summary>
        public decimal TotalExpenseAmount { get; set; }

        /// <summary>
        /// Ürün bazlı giderler toplam tutarı.
        /// </summary>
        public decimal TotalAmountProductBasedExpenses { get; set; }

        /// <summary>
        /// Diğer giderler toplam tutarı.
        /// </summary>
        public decimal OtherExpensesTotalAmount { get; set; }
        #endregion

        #region Readonly Properties
        public decimal TotalCostAmount => this.TotalProductCostAmount + this.TotalShippingCostAmount + this.TotalCommissionAmount;

        public decimal ProfitAmount => this.TotalSaleAmount - this.TotalCostAmount - this.TotalExpenseAmount;
        #endregion
    }
}
