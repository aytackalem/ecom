﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public record ColorBasedSalesReportPartial(
        string StartDateString, 
        string EndDateString, 
        Dictionary<string, string> Categorizations,
        Dictionary<string, string> Brands,
        DataTransferObjects.ColorBasedSalesReport Datas);
}
