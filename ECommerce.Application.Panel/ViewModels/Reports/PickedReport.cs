﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public record PickedReport(string StartDateString, string EndDateString, DataTransferObjects.PickedReport Datas);
}
