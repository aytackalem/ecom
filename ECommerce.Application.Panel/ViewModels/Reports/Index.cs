﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports
{
    public class Index
    {
        #region Properties
        public string StartDateString { get; set; }
        
        public string EndDateString { get; set; }
        #endregion

        #region Navigation Properties
        public OrderReport OrderReport { get; set; }

        public List<OrderProductReport> OrderProductReports { get; set; }
        #endregion
    }
}
