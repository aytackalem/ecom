﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Reports;

public record ColorBasedSalesReport(Dictionary<string, string> Marketplaces/*, Dictionary<string, string> ProductCategorizations,*/ /*List<string> Brands*/, List<KeyValue<int, string>> Categories);