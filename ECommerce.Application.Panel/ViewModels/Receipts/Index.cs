﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Receipts
{
    public class Index
    {
        public List<KeyValue<int, string>> Suppliers { get; set; }
    }
}
