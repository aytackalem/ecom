﻿using ECommerce.Application.Common.DataTransferObjects;

namespace ECommerce.Application.Panel.ViewModels.Homes
{
    public class Index
    {
        #region Properties
        public string UserRole { get; set; }
        #endregion

        #region Navigation Properties
        public Widget Widget { get; set; }
        #endregion
    }
}
