﻿using System;

namespace ECommerce.Application.Panel.ViewModels.LicenseCheck3ds
{
    public class Index
    {
        #region Properties
        public bool Success { get; set; }

        public string Message { get; set; }
        #endregion
    }
}
