﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.BulkPriceUpdates
{
    public class Index
    {
        #region Properties
        public List<KeyValue<int, string>> Categories { get; set; }
        #endregion
    }
}
