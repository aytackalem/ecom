﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects.Notifications;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Notifications
{
    public class Index
    {
        #region Properties
        public List<KeyValue<string, string>> Marketplaces { get; set; }

        public List<KeyValue<string, string>> NotificationTypes { get; set; }

        public List<NotificationItem> NotificationItems { get; set; }
        #endregion
    }
}
