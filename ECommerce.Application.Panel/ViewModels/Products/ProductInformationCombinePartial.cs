﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class ProductInformationCombinePartial
    {
        #region Properties

        public List<ProductInformationCombine> ProductInformationCombines { get; set; }
        #endregion
    }
}
