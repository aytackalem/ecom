﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class Index
    {
        /// <summary>
        /// Online olan mağaza hesabı. Örn: Lafaba.
        /// </summary>
        public string Tenant { get; set; }

        public bool IncludeAccounting { get; set; }

        public List<KeyValue<int, string>> Brands { get; set; }

        public List<KeyValue<int, string>> Categories { get; set; }

        public List<KeyValue<int, string>> ProductSourceDomains { get; set; }

        public List<KeyValue<int, string>> AccountingPrices { get; set; }

        public List<string> ProductCategorizations { get; set; }

        public List<string> Suppliers { get; set; }
    }
}
