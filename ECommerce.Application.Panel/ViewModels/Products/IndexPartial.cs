﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class IndexPartial : ViewModelBase
    {
        #region Constructors
        public IndexPartial()
        {
            this.ProductInformations = new();
        }
        #endregion

        #region Properties
        public int Index { get; set; }

        public bool Accordion => this.ProductInformations?.Count > 1;

        public List<KeyValue<string, string>> Currencies { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }

        public long MaxBarcode { get; set; }

        public int MaxStockCode { get; set; }

        public bool IncludeECommerce { get; set; }
        #endregion
    }
}
