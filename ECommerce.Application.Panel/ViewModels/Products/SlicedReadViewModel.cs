﻿using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.ViewModels.Products;

public class SlicedReadViewModel
{
    public bool Success { get; set; }

    public string Message { get; set; }

    public string Tenant { get; set; }
    public List<string> Marketplaces { get; set; }
    public List<SlicedProductViewModel> Products { get; set; }
    public int Page { get; set; }
    public int PageRecordsCount { get; set; }
    public int RecordsCount { get; set; }
    public int PagesCount { get; set; }
}

public class SlicedProductViewModel
{
    public string Name { get; set; }

    public float ReturnRate { get; set; }

    public int Stock { get; set; }

    public int ModelsCount => Models.Count;

    public List<SlicedModelViewModel> Models { get; set; }
    public string CreatedDateString { get; set; }
    public string Photo { get; set; }
    public string Code { get; set; }
    public Dictionary<string, string> MarketplacePrices { get; set; }
    public Dictionary<string, string> MarketplaceListPrices { get; set; }
    public Dictionary<string, List<bool>> MarketplaceStatuses { get; set; }
    public Guid Guid { get; set; }
    public int Id { get; set; }
    public string[] Categorizations { get; set; }
    public Dictionary<string, string> MarketplaceUrls { get; set; }
}

public class SlicedModelViewModel
{
    public string Name { get; set; }
    public int VariantsCount => Variants.Count;
    public List<SlicedVariantViewModel> Variants { get; set; }
    public string Photo { get; set; }
    public string Code { get; set; }
    public int Stock { get; set; }
    public Dictionary<string, string> MarketplacePrices { get; set; }
    public Dictionary<string, string> MarketplaceListPrices { get; set; }
    public Dictionary<string, List<bool>> MarketplaceStatuses { get; set; }
    public Guid Guid { get; set; }
    public string Categorization { get; set; }
    public string Slicer { get; set; }
    public string SlicerValue { get; set; }
    public float ReturnRate { get; set; }

    public float ColorReturnRate { get; set; }
}

public class SlicedVariantViewModel
{
    public string Varianter { get; set; }

    public string Name { get; set; }

    public string StockCode { get; set; }

    public string Barcode { get; set; }
    public int Stock { get; set; }
    public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }
    public string VarianterValue { get; set; }
    public bool Variantable { get; set; }
    public float ReturnRate { get; set; }
}