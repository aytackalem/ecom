﻿using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products;

public class ReadViewModel
{
    public bool Success { get; set; }

    public string Message { get; set; }

    public string Tenant { get; set; }

    public List<string> Marketplaces { get; set; }
    
    public List<ProductViewModel> Products { get; set; }
    
    public int Page { get; set; }
    
    public int PageRecordsCount { get; set; }
    
    public int RecordsCount { get; set; }
    
    public int PagesCount { get; set; }
}

public class ProductViewModel
{
    public string Name { get; set; }

    public int Stock { get; set; }

    public string StockCode { get; set; }

    public string Barcode { get; set; }

    public string CreatedDateString { get; set; }
    
    public string Photo { get; set; }
    
    public string Code { get; set; }
    
    public Dictionary<string, string> MarketplacePrices { get; set; }
    
    public Dictionary<string, bool> MarketplaceStatuses { get; set; }
    
    public Guid Guid { get; set; }
    
    public int Id { get; set; }
    
    public string[] Categorizations { get; set; }
    
    public Dictionary<string, string> MarketplaceUrls { get; set; }
}