﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class ProductInformationPricePartial
    {
        #region Properties
        public int ProductInformationIndex { get; set; }

        public int Index { get; set; }

        public List<KeyValue<string, string>> Currencies { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }

        public ProductInformationPrice ProductInformationPrice { get; set; }
        #endregion
    }
}
