﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products;

public class Last12MonthSaleViewModel
{
    public List<string> Labels { get; set; }

    public List<Last12MonthSaleItem> Items { get; set; }
}

public class Last12MonthSaleItem
{
    public string SkuCode { get; set; }

    public List<Last12MonthSaleItemSerie> Series { get; set; }
}

public class Last12MonthSaleItemSerie
{
    public string Categorization { get; set; }

    public List<string> Data { get; set; }
}