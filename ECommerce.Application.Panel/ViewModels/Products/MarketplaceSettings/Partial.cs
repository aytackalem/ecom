﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products.MarketplaceSettings
{
    public class Partial : ViewModelBase
    {
        #region Properties
        public bool IncludeAccounting { get; set; }
        #endregion

        #region Navigation Properties
        public List<Marketplace> Marketplaces { get; set; }

        public Product Product { get; set; }

        public List<ProductMarketplace> ProductMarketplaces { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

        public List<GetByCategoryCodesResponse> MarketplaceVariants { get; set; }

        public List<GetByMarketplaceIdResponse> MarketplaceCategories { get; set; }
        #endregion
    }
}
