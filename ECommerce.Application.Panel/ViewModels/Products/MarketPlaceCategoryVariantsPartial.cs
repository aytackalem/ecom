﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class MarketPlaceCategoryVariantsPartial : ViewModelBase
    {
        #region Properties
        public string MarketplaceId { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceVariant> MarketplaceVariants { get; set; }
        #endregion
    }
}
