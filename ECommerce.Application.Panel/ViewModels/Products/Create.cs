﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Currencies { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }

        public List<KeyValue<int, string>> Categories { get; set; }

        public List<KeyValue<int, string>> Groups { get; set; }

        public List<KeyValue<int, string>> Brands { get; set; }

        public List<KeyValue<int, string>> Suppliers { get; set; }

        public List<KeyValue<int, string>> Labels { get; set; }

        public List<Variant> Variants { get; set; }

        public List<Property> Properties { get; set; }

        public bool IncludeECommerce { get; set; }

        public bool IncludeAccounting { get; set; }

        public bool IncludeMarketplace { get; set; }

        public bool IncludeWMS { get; set; }
        #endregion
    }
}
