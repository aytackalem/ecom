﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Products
{
    public class ProductInformationTranslationPartial
    {
        #region Properties
        public int Index { get; set; }

        public int ProductInformationIndex { get; set; }

        public List<KeyValue<string, string>> Currencies { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }

        public ProductInformationTranslation ProductInformationTranslation { get; set; }

        public bool IncludeECommerce { get; set; }
        #endregion
    }
}
