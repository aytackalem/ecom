﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.ViewModels.EmailProviderCompanies
{
    public class Update
    {
        #region Properties
        public EmailProviderCompany EmailProviderCompany { get; set; }
        #endregion
    }
}
