﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Contents
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.Content Content { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
