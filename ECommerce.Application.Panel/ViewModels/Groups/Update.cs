﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Groups
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.Group Group { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
