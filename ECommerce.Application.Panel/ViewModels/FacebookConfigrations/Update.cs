﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.FacebookConfigrations
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.FacebookConfigration FacebookConfigration { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
