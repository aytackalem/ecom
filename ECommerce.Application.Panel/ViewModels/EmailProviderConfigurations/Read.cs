﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.EmailProviderConfigurations
{
    public class Read
    {
        #region Properties
        public int EmailProviderCompanyId { get; set; }

        public List<EmailProviderConfiguration> EmailProviderConfigurations { get; set; }
        #endregion
    }
}
