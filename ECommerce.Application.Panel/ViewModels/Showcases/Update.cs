﻿using ECommerce.Application.Common.DataTransferObjects;

namespace ECommerce.Application.Panel.ViewModels.Showcases
{
    public class Update
    {
        public Showcase Showcase { get; set; }
    }
}
