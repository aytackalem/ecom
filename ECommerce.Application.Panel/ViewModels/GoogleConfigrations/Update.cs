﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.GoogleConfigrations
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.GoogleConfigration GoogleConfigration { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
