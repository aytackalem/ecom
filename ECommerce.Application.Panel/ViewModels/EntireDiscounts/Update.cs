﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.EntireDiscounts
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.EntireDiscount EntireDiscount { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}
