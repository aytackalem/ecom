﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels
{
    public class DataTableViewModel
    {
        #region Properties
        public string Name { get; set; }

        public string JsName { get; set; }

        public List<string> Columns { get; set; }
        #endregion
    }
}
