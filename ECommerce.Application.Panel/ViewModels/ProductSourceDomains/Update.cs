﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductSourceDomains
{
    public class Update
    {
        #region Properties
        public ProductSourceDomain ProductSourceDomain { get; set; }
        #endregion
    }
}
