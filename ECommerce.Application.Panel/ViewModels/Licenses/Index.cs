﻿using System;

namespace ECommerce.Application.Panel.ViewModels.Licenses
{
    public class Index
    {
        #region Properties
        public decimal MothlySalesAmount { get; set; }

        public decimal HalfYearlySalesListAmount => MothlySalesAmount * 6;

        public decimal HalfYearlySalesAmount { get; set; }

        public decimal YearlySalesListAmount => MothlySalesAmount * 12;

        public decimal YearlySalesAmount { get; set; }

        public DateTime ExpirationDate { get; set; }

        public int RemainDays => (ExpirationDate - DateTime.Now).Days;
        #endregion
    }
}
