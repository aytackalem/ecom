﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.FrequentlyAskedQuestions
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.FrequentlyAskedQuestion FrequentlyAskedQuestion { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
