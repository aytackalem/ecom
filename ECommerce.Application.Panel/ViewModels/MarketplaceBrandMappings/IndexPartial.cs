﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.MarketplaceBrandMappings
{
    public class IndexPartial : ViewModelBase
    {
        #region Properties
        public int BrandId { get; set; }

        public string BrandName { get; set; }
        #endregion

        #region Navigation Properties
        public List<Marketplace> Marketplaces { get; set; }

        public List<MarketplaceBrandMapping> MarketplaceBrandMappings { get; set; }
        #endregion
    }
}
