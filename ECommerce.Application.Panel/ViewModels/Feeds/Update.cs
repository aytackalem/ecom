﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Feeds
{
    public class Update
    {
        #region Navigation Properties
        public DataTransferObjects.Feed Feed { get; set; }

        public List<KeyValue<string, string>> FeedTemplates { get; set; }
        #endregion
    }
}
