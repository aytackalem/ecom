﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Feeds
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> FeedTemplates { get; set; }
        #endregion
    }
}