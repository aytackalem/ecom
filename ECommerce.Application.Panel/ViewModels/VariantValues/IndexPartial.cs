﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.VariantValues
{
    public class IndexPartial
    {
        #region Properties
        public int Index { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion

        #region Navigation Properties
        public VariantValue VariantValue { get; set; }
        #endregion
    }
}
