﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Categories
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.Category Category { get; set; }

        public List<KeyValue<int, string>> Categories { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
