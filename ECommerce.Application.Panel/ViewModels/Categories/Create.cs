﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Categories
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Languages { get; set; }
        public List<KeyValue<int, string>> Categories { get; set; }
        #endregion
    }
}