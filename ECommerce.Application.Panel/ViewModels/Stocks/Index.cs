﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.ViewModels.Stocks
{
    public class Index
    {
        public bool IncludeAccounting { get; set; }

        public List<KeyValue<int, string>> Brands { get; set; }
    }
}
