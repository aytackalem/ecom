﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.ViewModels.Stocks
{
    public class Update
    {
        #region Properties
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
