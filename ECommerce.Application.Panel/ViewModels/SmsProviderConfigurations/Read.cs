﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.SmsProviderConfigurations
{
    public class Read
    {
        #region Properties
        public int SmsProviderCompanyId { get; set; }

        public List<SmsProviderConfiguration> SmsProviderConfigurations { get; set; }
        #endregion
    }
}
