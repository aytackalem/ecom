﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ProductInformations
{
    public class PriceUpdate : ViewModelBase
    {
        #region Properties
        public bool IncludeAccounting { get; set; }
        #endregion

        #region Navigation Properties
        public List<ProductInformation> ProductInformations { get; set; }
        #endregion
    }
}
