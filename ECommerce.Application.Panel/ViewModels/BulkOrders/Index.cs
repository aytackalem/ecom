﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.BulkOrders
{
    public class Index
    {
        #region Properties
        public bool ShowStock { get; set; }

        public List<KeyValue<int, string>> OrderSources { get; set; }

        public List<KeyValue<string, string>> OrderTypes { get; set; }

        public List<KeyValue<string, string>> MarketPlaces { get; set; }

        public List<KeyValue<int, string>> Categories { get; set; }

        #endregion
    }
}
