﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.GetXPayYSettings
{
    public class Update
    {
        #region Properties
        public GetXPayYSetting GetXPayYSetting { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}
