﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.GetXPayYSettings
{
    public class Create
    {
        #region Properties
        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}