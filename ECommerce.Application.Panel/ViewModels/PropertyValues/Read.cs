﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.PropertyValues
{
    public class Read
    {
        #region Properties
        public int PropertyId { get; set; }

        public List<PropertyValue> PropertyValues { get; set; }
        #endregion
    }
}
