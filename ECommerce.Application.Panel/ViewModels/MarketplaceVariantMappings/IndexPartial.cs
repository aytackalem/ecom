﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.ViewModels.MarketplaceVariantMappings
{
    public class IndexPartial
    {
        #region Fields

        #endregion

        #region Constructors
        public IndexPartial()
        {
            #region Properties
            this.Marketplaces = new();
            #endregion
        }
        #endregion

        #region Properties

        #endregion

        #region Navigation Properties
        public DataTransferObjects.Variant Variant { get; set; }

        public List<Marketplace> Marketplaces { get; set; }
        #endregion

        #region Methods

        #endregion
    }
}
