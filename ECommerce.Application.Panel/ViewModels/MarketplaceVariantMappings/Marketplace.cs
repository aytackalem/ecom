﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.MarketplaceVariantMappings
{
    public class Marketplace
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsECommerce { get; set; }
        #endregion

        #region Navigation Properties
        public List<Variant> Variants { get; set; }
        #endregion
    }
}
