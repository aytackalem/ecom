﻿namespace ECommerce.Application.Panel.ViewModels.MarketplaceVariantMappings
{
    public class Variant
    {
        #region Properties
        public string Code { get; set; }

        public string Name { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public int? CompanyId { get; set; }

        public bool Selected { get; set; }
        #endregion
    }
}
