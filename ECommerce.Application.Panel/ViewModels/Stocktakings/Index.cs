﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Stocktakings
{
    public class Index
    {
        #region Properties
        public List<KeyValue<string, string>> StocktakingTypes { get; set; }
        #endregion
    }
}
