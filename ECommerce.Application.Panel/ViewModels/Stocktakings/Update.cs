﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Stocktakings
{
    public class Update
    {
        #region Properties
        public Stocktaking Stocktaking { get; set; }

        public List<KeyValue<string, string>> StocktakingTypes { get; set; }
        #endregion
    }
}
