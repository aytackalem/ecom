﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Marketplaces
{
    public class Update
    {
        #region Properties
        public MarketplaceCompany MarketPlaceCompany { get; set; }

        #endregion
    }
}
