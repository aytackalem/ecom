﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.PosConfigurations
{
    public class Read
    {
        #region Properties
        public string PosId { get; set; }

        public List<PosConfiguration> PosConfigurations { get; set; }
        #endregion
    }
}
