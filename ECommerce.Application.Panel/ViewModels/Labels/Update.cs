﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Labels
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.Label Label { get; set; }

        public List<KeyValue<string, string>> Languages { get; set; }
        #endregion
    }
}
