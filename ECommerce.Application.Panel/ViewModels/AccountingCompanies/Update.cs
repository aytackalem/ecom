﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.AccountingCompanies
{
    public class Update
    {
        #region Properties
        public AccountingCompany AccountingCompany { get; set; }
        #endregion
    }
}
