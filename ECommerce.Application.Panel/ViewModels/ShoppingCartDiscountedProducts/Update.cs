﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ShoppingCartDiscountedProducts
{
    public class Update
    {
        #region Properties
        public DataTransferObjects.ShoppingCartDiscountedProduct ShoppingCartDiscountedProduct { get; set; }

        public List<KeyValue<int, string>> Products { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}
