﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ShoppingCartDiscountedProducts
{
    public class Create
    {
        #region Properties
        public List<KeyValue<int, string>> Products { get; set; }

        public List<KeyValue<string, string>> Applications { get; set; }
        #endregion
    }
}