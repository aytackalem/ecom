﻿namespace ECommerce.Application.Panel.ViewModels.MarketplaceCategoryMappings
{
    public class Category
    {
        #region Properties
        public string Code { get; set; }

        public string Name { get; set; }

        public int? CompanyId { get; set; }

        public bool Selected { get; set; }

        public string Breadcrumb { get; set; }
        #endregion
    }
}
