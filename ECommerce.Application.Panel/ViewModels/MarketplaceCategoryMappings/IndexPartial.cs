﻿using ECommerce.Application.Common.ViewModels.Base;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.MarketplaceCategoryMappings
{
    public class IndexPartial : ViewModelBase
    {
        #region Properties
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
        #endregion

        #region Navigation Properties
        public List<Marketplace> Marketplaces { get; set; }

        public List<MarketplaceCategoryMapping> MarketplaceCategoryMappings { get; set; }
        public string CategoryBreadcrumb { get; set; }

        public List<GetByCategoryCodesResponse> MarketplaceVariants { get; set; }

        #endregion
    }
}
