﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Expenses
{
    public class Create
    {
        #region Properties
        public List<KeyValue<int, string>> ExpenseSources { get; set; }
        
        public List<KeyValue<int, string>> OrderSources { get; set; }
        
        public List<KeyValue<string, string>> Marketplaces { get; set; }
        
        public List<KeyValue<int, string>> ProductInformations { get; set; }
        #endregion
    }
}