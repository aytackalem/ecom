﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.Expenses
{
    public class Update
    {
        #region Navigation Properties
        public DataTransferObjects.Expense Expense { get; set; }

        public List<KeyValue<int, string>> ExpenseSources { get; set; }
        
        public List<KeyValue<int, string>> OrderSources { get; set; }
        
        public List<KeyValue<string, string>> Marketplaces { get; set; }
        
        public List<KeyValue<int, string>> ProductInformations { get; set; }
        #endregion
    }
}
