﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.ViewModels.ShipmentCompanyConfigurations
{
    public class Read
    {
        #region Properties
        public int ShipmentCompanyCompanyId { get; set; }

        public List<ShipmentCompanyConfiguration> ShipmentCompanyConfigurations { get; set; }
        #endregion
    }
}
