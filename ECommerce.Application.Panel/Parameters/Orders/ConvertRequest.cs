﻿namespace ECommerce.Application.Panel.Parameters.Orders
{
    public class ConvertRequest
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public bool Available { get; set; }
        #endregion
    }
}
