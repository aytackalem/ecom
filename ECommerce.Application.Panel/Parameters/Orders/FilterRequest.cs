﻿namespace ECommerce.Application.Panel.Parameters.Orders
{
    public class FilterRequest
    {
        #region Properties
        public string Search { get; set; }
        #endregion
    }
}
