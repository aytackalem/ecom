﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.MarketplaceVariantMappings
{
    public class UpsertRequest
    {
        public int VariantId { get; set; }

        #region Navigation Properties
        //public List<MarketplaceVariantMapping> MarketplaceVariantMappings { get; set; }

        public List<MarketplaceVariantValueMapping> MarketplaceVariantValueMappings { get; set; }
        #endregion
    }
}
