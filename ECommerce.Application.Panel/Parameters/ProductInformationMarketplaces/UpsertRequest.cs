﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.ProductInformationMarketplaces
{
    public class UpsertRequest
    {
        #region Navigation Properties
        public List<ProductMarketplace> ProductMarketplaces { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }
        #endregion
    }
}
