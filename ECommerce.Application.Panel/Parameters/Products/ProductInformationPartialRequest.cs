﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.Products
{
    public class ProductInformationPartialRequest
    {
        #region Properties
        public List<ProductInformation> ProductInformations { get; set; }

        public List<Variant> Variants { get; set; }

        public List<string> ToBeDeletedProductInformations { get; set; }
        #endregion
    }
}
