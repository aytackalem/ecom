﻿namespace ECommerce.Application.Panel.Parameters.Customers
{
    public class SearchRequest
    {
        #region Properties
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }
        #endregion
    }
}
