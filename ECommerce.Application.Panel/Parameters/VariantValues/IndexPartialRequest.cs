﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.VariantValues
{
    public class IndexPartialRequest
    {
        #region Properties
        public int Index { get; set; }
        #endregion

        #region Navigation Properties
        public VariantValue VariantValue { get; set; }
        #endregion
    }
}
