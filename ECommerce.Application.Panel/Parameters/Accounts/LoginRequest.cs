﻿using ECommerce.Application.Panel.DataTransferObjects;

namespace ECommerce.Application.Panel.Parameters.Accounts
{
    public class LoginRequest
    {
        #region Properties
        public bool KeepLoggedIn { get; set; }

        public string Brand { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
