﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings
{
    public class UpsertRequest
    {
        #region Properties
        public int BrandId { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceBrandMapping> MarketplaceBrandMappings { get; set; }
        #endregion
    }
}
