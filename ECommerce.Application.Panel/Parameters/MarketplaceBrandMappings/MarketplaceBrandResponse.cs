﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings
{
    public class MarketplaceBrandResponse
    {
        public int id { get; set; }
        public string name { get; set; }
    }


}
