﻿namespace ECommerce.Application.Panel.Parameters.ProductInformationPhotos
{
    public class IndexRequest
    {
        #region Properties
        public string Base64 { get; set; }
        #endregion
    }
}
