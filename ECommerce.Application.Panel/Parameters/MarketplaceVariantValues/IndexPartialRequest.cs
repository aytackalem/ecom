﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.MarketplaceVariantValues
{
    public class IndexPartialRequest
    {
        #region Properties
        public int VariantValueId { get; set; }

        public List<MarketplaceVariant> MarketplaceVariants { get; set; }
        #endregion

        #region Navigation Properties
        public Marketplace Marketplace { get; set; }
        #endregion
    }
}
