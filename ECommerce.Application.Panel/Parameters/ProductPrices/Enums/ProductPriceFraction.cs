﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Parameters.ProductPrices.Enums
{
    /// <summary>
    /// Toplu fiyat güncellemede küsürat işlemlerini gösterir
    /// </summary>
    public enum ProductPriceFraction
    {
        [Description("Yuvarlama")]
        RealPrice = 1,
        [Description("Yukarı Yuvarla")]
        CeilingPrice = 2,
        [Description("Aşağı Yuvarla")]
        FloorPrice = 3,
        [Description("Yukarı Yuvarla 1 Kuruş Düşür (#.99)")]
        CeilingOneSendPrice = 4,
        [Description("Aşağı Yuvarla 1 Kuruş Düşür (#.99)")]
        FloorOneSendPrice = 5,
      

    }
}
