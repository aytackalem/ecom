﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Parameters.ProductPrices.Enums
{
    /// <summary>
    /// Toplu fiyat güncellemede aksiyon alınacak işlemleri gösterir
    /// </summary>
    public enum ProductPrice
    {
        [Description("Yüzde (%) Fiyat Ekle")]
        AddPercentagePrice = 1,
        [Description("Yüzde (%) Fiyat Çıkar")]
        LowerPercentagePrice = 2,
        [Description("Sabit Fiyat Ekle")]
        AddFixedPrice = 3,
        [Description("Sabit Fiyat Çıkar")]
        LowerFixedPrice = 4,
        [Description("Sabit Fiyat ile Çarp")]
        MultiplyFixedPrice = 5,
        [Description("Sabit Fiyat ile Böl")]
        SplitFixedPrice = 6

    }
}
