﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Parameters.ProductPrices.Enums
{
    /// <summary>
    /// Xml/Erp'den gelen ürünler ekranda select'de gösterilir.
    /// </summary>
    public enum AccountingPrice
    {
        [Description("Xml/Erp Fiyat Güncellenmesin")]
        UpdateNotPrice = 1,
        [Description("Xml/Erp Fiyat Güncellensin")]
        UpdatePrice = 2,

    

    }
}
