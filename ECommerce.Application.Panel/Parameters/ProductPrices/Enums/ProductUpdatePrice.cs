﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Parameters.ProductPrices.Enums
{
    /// <summary>
    /// Xml/Erp'den gelen ürünler ekranda select'de gösterilir.
    /// </summary>
    public enum ProductUpdatePrice
    {
        [Description("Pazaryerinde Fiyat ve Ürün Güncellensin")]
        UpdateProductPrice = 1,
        [Description("Pazaryerinde Fiyat ve Ürün Güncellenmesin")]
        UpdateNotProductPrice = 2,

    

    }
}
