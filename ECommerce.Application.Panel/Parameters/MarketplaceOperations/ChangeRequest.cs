﻿namespace ECommerce.Application.Panel.Parameters.MarketplaceOperations
{
    public class ChangeRequest
    {
        #region Properties
        public string MarketplaceOrderNumber { get; set; }

        public string MarketplaceId { get; set; }

        public string PackageNumber { get; set; }

        public string CargoCompanyId { get; set; }
        #endregion
    }
}
