﻿using System;

namespace ECommerce.Application.Panel.Parameters.Reports
{
    public record ColorBasedSalesReportRequest(DateTime StartDate, DateTime EndDate, string? SellerCode, string? MarketplaceId, string? SkuCode, int categoryId);
}
