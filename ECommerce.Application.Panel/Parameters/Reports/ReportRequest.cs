﻿using System;

namespace ECommerce.Application.Panel.Parameters.Reports
{
    public class ReportRequest
    {
        #region Properties
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public int MinQuantity { get; set; }
        #endregion
    }
}
