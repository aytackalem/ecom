﻿using System;

namespace ECommerce.Application.Panel.Parameters.Reports
{
    public class PackagingReportRequest
    {
        #region Properties
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TenantId { get; set; }

        public int DomainId { get; set; }

        public int CompanyId { get; set; }
        #endregion
    }
}
