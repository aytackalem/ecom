﻿using System;

namespace ECommerce.Application.Panel.Parameters.Reports
{
    public class ProductBasedStockReportRequest
    {
        #region Properties
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int ProductId { get; set; }

        public string SkuCode { get; set; }
        #endregion
    }
}
