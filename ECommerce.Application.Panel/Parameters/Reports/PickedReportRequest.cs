﻿using System;

namespace ECommerce.Application.Panel.Parameters.Reports
{
    public record PickedReportRequest(DateTime StartDate, DateTime EndDate);
}
