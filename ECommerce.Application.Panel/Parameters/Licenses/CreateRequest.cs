﻿namespace ECommerce.Application.Panel.Parameters.Licenses
{
    public class CreateRequest
    {
        #region Properties
        public string Name { get; set; }

        public string Number { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }

        public string Cvv { get; set; }

        public string Code { get; set; }
        #endregion
    }
}
