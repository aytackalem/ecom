﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings
{
    public class UpsertRequest
    {
        #region Properties
        public int CategoryId { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceCategoryMapping> MarketplaceCategoryMappings { get; set; }
        #endregion
    }
}
