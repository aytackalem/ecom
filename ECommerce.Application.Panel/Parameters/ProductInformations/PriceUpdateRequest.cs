﻿using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Parameters.ProductInformations
{
    public class PriceUpdateRequest
    {
        #region Properties
        public List<ProductInformation> ProductInformations { get; set; }
        #endregion
    }
}
