﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ReceiptPivotItem
    {
        public string Color { get; set; }

        public string Size { get; set; }

        public int Quantity { get; set; }
    }
}
