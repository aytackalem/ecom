﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Payment
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ödeme tipine ait id bilgisi.
        /// </summary>
        public string PaymentTypeId { get; set; }

        /// <summary>
        /// İndirim kuponu kullanılmış ise indirim kuponu id bilgisi.
        /// </summary>
        public int? DiscountCouponId { get; set; }

        /// <summary>
        /// Ödeme tutarı.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Ödeme bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        public bool Paid { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ödeme tipi.
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// İndirim kuponu kullanılmış ise ilgili indirim kuponu.
        /// </summary>
        public DiscountCoupon DiscountCoupon { get; set; }
        
        public PaymentLog PaymentLog { get; set; }
        #endregion
    }
}
