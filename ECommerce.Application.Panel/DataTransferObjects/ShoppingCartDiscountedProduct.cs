﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ShoppingCartDiscountedProductSetting
    {
        #region Properties
        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }
        #endregion
    }
}
