﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Marketplace
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public int MarketplaceCompanyId { get; set; }

        public bool BrandAutocomplete { get; set; }

        public bool IsECommerce { get; set; }

        public bool Matchable { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceVariant> MarketplaceVariants { get; set; }

        public List<MarketplaceCategory> MarketplaceCategories { get; set; }
        #endregion
    }
}
