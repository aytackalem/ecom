﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class VariantValue
    {
        #region Properties
        public int Id { get; set; }

        public int VariantId { get; set; }
        #endregion

        #region Navigation Properties
        public Variant Variant { get; set; }

        public List<VariantValueTranslation> VariantValueTranslations { get; set; }

        public List<MarketplaceVariantValueMapping> MarketplaceVariantValueMappings { get; set; }
        #endregion
    }
}
