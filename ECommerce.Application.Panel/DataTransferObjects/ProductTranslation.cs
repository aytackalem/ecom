﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductTranslation
    {
        #region Properties
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public string LanguageId { get; set; }
        #endregion
    }
}
