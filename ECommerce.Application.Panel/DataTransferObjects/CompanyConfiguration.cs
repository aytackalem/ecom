﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CompanyConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }
        public string Value { get; set; }
        #endregion
    }
}
