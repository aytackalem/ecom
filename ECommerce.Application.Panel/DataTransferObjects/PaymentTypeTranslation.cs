﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PaymentTypeTranslation
    {
        #region Properties
        public short Id { get; set; }

        public string PaymentTypeId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
