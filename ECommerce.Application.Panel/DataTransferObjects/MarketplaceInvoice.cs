﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceInvoice
    {
        public string Number { get; set; }
    }
}
