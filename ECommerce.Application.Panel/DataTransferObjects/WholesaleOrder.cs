﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects;

public class WholesaleOrder
{
    public int Id { get; set; }

    public DateTime CreatedDate { get; set; }

    public string CreatedDateStr => this.CreatedDate.ToString("dd-MM-yyyy HH:mm:ss");

    public WholesaleOrderType WholesaleOrderType { get; set; }

    public List<WholesaleOrderDetail> WholesaleOrderDetails { get; set; }

    public bool IsExport { get; set; }
}
