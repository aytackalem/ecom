﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class LabelTranslation
    {
        public int Id { get; set; }

        public string LanguageId { get; set; }

        public string Value { get; set; }

    }
}