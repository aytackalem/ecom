﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ContentSeoTranslation
    {
        /// <summary>
        /// Başlık.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama.
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Anahtar kelimeler.
        /// </summary>
        public string MetaKeywords { get; set; }

    }
}