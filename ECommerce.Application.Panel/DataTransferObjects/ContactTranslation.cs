﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ContactTranslation
    {

        public string LanguageId { get; set; }

        public string Description { get; set; }

        public int ContactId { get; set; }


    }
}