﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceVariantValue
    {
        #region Properties
        public string Id { get; set; }

        public string Value { get; set; }

        public string MarketplaceId { get; set; }
        public int MarketplaceCategoriesMarketplaceVariantValueId { get; set; }
        public List<string> MarketplaceVariantIds { get; set; }
        #endregion
    }
}
