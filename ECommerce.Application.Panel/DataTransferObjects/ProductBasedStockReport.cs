﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductBasedStockReport
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string Barcode { get; set; }

        public string SkuCode { get; set; }

        public int Stock { get; set; }

        public string FileName { get; set; }

        public string Name { get; set; }

        public string VariantValuesDescription { get; set; }

        public int SoldQuantity { get; set; }

        public string SellerCode { get; set; }

        public string ReceiptDescription { get; set; }
        #endregion
    }
}
