﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Contact
    {
        #region Properties

        public int Id { get; set; }

        public string Address { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Whatsapp { get; set; }

        public string Email { get; set; }

        public bool Active { get; set; }

        public List<ContactTranslation> ContactTranslations { get; set; }
        #endregion
    }
}