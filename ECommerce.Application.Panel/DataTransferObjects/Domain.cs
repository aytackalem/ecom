﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Domain
    {
        #region Property
        [JsonIgnore]
        public int Id { get; set; }
        /// <summary>
        /// Şirket adı.
        /// </summary>
        public string Name { get; set; }

        public bool Selected { get; set; }
        #endregion

        #region Navigation Properties
        public List<Company> Companies { get; set; }
        #endregion

    }
}
