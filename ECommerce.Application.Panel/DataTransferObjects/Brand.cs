﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Brand
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public bool Active { get; set; }
        #endregion
    }
}
