﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class Last12MonthSale
{
    public string SkuCode { get; set; }

    public string Categorization { get; set; }

    public string Date { get; set; }

    public int Quantity { get; set; }
}
