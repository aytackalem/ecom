﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ColorBasedSalesReportItem
    {
        public int ProductId { get; set; }

        public int ProductInformationId { get; set; }

        public string SkuCode { get; set; }

        public string SellerCode { get; set; }

        public string Name { get; set; }

        public string DisplayName => Name.Length > 40 ? $"{Name[..40]}..." : Name;

        public string FileName { get; set; }

        public int Quantity { get; set; }

        public int Stock { get; set; }

        public int MonthlyQuantity { get; set; }

        public double MonthlyQuantityAverage
        {
            get
            {
                if (MonthlyQuantity == 0)
                    return 0d;

                return MonthlyQuantity / 30d;
            }
        }

        public int MonthlyStock => Stock == 0 || MonthlyQuantityAverage == 0 ? 0 : (int)Math.Ceiling(Stock / MonthlyQuantityAverage);

        public int WeeklyQuantity { get; set; }

        public double WeeklyQuantityAverage
        {
            get
            {
                if (WeeklyQuantity == 0)
                    return 0d;

                return WeeklyQuantity / 7d;
            }
        }

        public bool WeeklyIncrement => WeeklyQuantityAverage > MonthlyQuantityAverage;

        public int WeeklyStock => Stock == 0 || WeeklyQuantityAverage == 0 ? 0 : (int)Math.Ceiling(Stock / WeeklyQuantityAverage);

        public int DailyQuantity { get; set; }

        public double DailyQuantityAverage
        {
            get
            {
                if (DailyQuantity == 0)
                    return 0d;

                return DailyQuantity / 3d;
            }
        }

        public bool DailyIncrement => DailyQuantityAverage > WeeklyQuantityAverage;

        public int DailyStock => Stock == 0 || DailyQuantityAverage == 0 ? 0 : (int)Math.Ceiling(Stock / DailyQuantityAverage);

        public int AverageQuantity
        {
            get
            {
                var quantity = MonthlyQuantity + WeeklyQuantity + DailyQuantity;
                if (quantity == 0)
                    return 0;

                return quantity / 3;
            }
        }

        public double AverageQuantityAverage
        {
            get
            {
                var average = MonthlyQuantityAverage + WeeklyQuantityAverage + DailyQuantityAverage;
                if (average == 0)
                    return 0;

                return average / 3;
            }
        }

        public int AverageStock => Stock == 0 || AverageQuantityAverage == 0 ? 0 : (int)Math.Ceiling(Stock / AverageQuantityAverage);

        public bool HasReceipt { get; set; }

        public string CategorizationsString { get; set; }

        public string BrandsString { get; set; }

        public List<string> Categorizations
        {
            get
            {
                var list = new List<string>();

                if (string.IsNullOrEmpty(this.CategorizationsString) == true)
                    return list;

                list.AddRange(this.CategorizationsString.Split(','));

                return list;
            }
        }

        public List<string> Brands
        {
            get
            {
                var list = new List<string>();

                if (string.IsNullOrEmpty(this.BrandsString) == true)
                    return list;

                list.AddRange(this.BrandsString.Split(','));

                return list;
            }
        }
    }
}
