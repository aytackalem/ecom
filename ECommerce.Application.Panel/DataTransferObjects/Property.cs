﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Property
    {
        #region Properties
        public int Id { get; set; }
        #endregion

        #region Navigation Properties
        public List<PropertyTranslation> PropertyTranslations { get; set; }

        public List<ProductProperty> ProductProperties { get; set; }

        public List<PropertyValue> PropertyValues { get; set; }
        #endregion
    }
}
