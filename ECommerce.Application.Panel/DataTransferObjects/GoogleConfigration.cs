﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class GoogleConfigration
    {
        #region Property

        public int Id { get; set; }

        /// <summary>
        /// Facebook code.
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Marka aktif mi?
        /// </summary>
        public bool Active { get; set; }


        public string Key { get; set; }


        public string Name { get; set; }
        #endregion
    }
}