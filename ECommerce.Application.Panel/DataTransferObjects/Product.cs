﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Product
    {
        #region Properties
        public int Id { get; set; }
        //ProductInformationVariantValue
        public int BrandId { get; set; }

        public int SupplierId { get; set; }

        public int CategoryId { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// Ürün tip bilgisini temsil eden özellik. PI = Product Information, PIP = Product Information Package, PIC = Product Information Combine. 
        /// </summary>
        public string Type { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Ürüne ait product information ların toplam stoğu.
        /// </summary>
        public int TotalStock { get; set; }

        /// <summary>
        /// Model kodu.
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Ürüne ait product information sayısı.
        /// </summary>
        public int VariantsCount { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa isteiği ürünün özellikleri erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingProperty { get; set; }

        /// <summary>
        /// Ürün kaynağının nereden geldiğini belirtir.
        /// </summary>
        public string ProductSourceDomainName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyy");
        #endregion

        #region Navigation Properties
        public Category Category { get; set; }

        public List<ProductMarketplace> ProductMarketplaces { get; set; }

        public List<int> MultiCategoryIds { get; set; }

        public List<ProductLabel> ProductLabels { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }

        public List<ProductProperty> ProductProperties { get; set; }

        public List<ProductTranslation> ProductTranslations { get; set; }
        public float ReturnRate { get; set; }
        #endregion
    }
}
