﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ExpenseReport
    {
        #region Properties
        /// <summary>
        /// Toplam gider tutarı.
        /// </summary>
        public decimal TotalExpenseAmount { get; set; }

        /// <summary>
        /// Ürün bazlı giderler toplam tutarı.
        /// </summary>
        public decimal TotalAmountProductBasedExpenses { get; set; }

        /// <summary>
        /// Diğer giderler toplam tutarı.
        /// </summary>
        public decimal OtherExpensesTotalAmount { get; set; }
        #endregion
    }
}
