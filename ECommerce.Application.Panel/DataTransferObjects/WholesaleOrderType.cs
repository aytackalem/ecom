﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class WholesaleOrderType
{
    public string Name { get; set; }
}
