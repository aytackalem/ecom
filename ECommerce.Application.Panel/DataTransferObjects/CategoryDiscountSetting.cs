﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CategoryDiscountSetting
    {
        #region Properties
        public int Id { get; set; }

        public string ApplicationId { get; set; }

        public int CategoryId { get; set; }

        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }

        public DateTime StartDate { get; set; }

        public string StartDateString { get; set; }

        public DateTime EndDate { get; set; }

        public string EndDateString { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public Category Category { get; set; }
        #endregion
    }
}
