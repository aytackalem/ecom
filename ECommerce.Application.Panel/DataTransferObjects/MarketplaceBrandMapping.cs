﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceBrandMapping
    {
        #region Properties
        public int Id { get; set; }

        /// <summary>
        /// Pazaryeri id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// Sistem kategori id bilgisi.
        /// </summary>
        public int BrandId { get; set; }

        /// <summary>
        /// Pazaryerine ait marka id bilgisi
        /// </summary>
        public string MarketplaceBrandCode { get; set; }

        /// <summary>
        /// Pazaryerine ait marka adı
        /// </summary>
        public string MarketplaceBrandName { get; set; }
        
        public int? CompanyId { get; set; }
        #endregion
    }
}
