﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderType
    {
        #region Property
        /// <summary>
        /// Tip adı.
        /// </summary>
        public string Name { get; set; }

        public string OrderTypeName { get; set; }
        #endregion

        #region Navigation Property


        /// <summary>
        /// Sipariş tip dil bilgileri.
        /// </summary>
        public List<OrderTypeTranslation> OrderTypeTranslations { get; set; }
        #endregion
    }
}
