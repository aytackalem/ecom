﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class FrequentlyAskedQuestionTranslation
    {
        public string Header { get; set; }

        public string Description { get; set; }

        public string LanguageId { get; set; }

        public int FrequentlyAskedQuestionId { get; set; }

    }
}