﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PropertyTranslation
    {
        #region Properties
        public int Id { get; set; }

        public int PropertyId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
