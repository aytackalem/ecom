﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// Ürünü oluşturan kullanıcı id bilgisi.
        /// </summary>
        public int ManagerUserId { get; set; }

        /// <summary>
        /// İlgili ürün detayına ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Dosya adı.
        /// </summary>
        public string FileName { get; set; }


        /// <summary>
        /// Ürün Sırası
        /// </summary>
        public int DisplayOrder { get; set; }

        #endregion


    }
}
