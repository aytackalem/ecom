﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ReceiptItem
    {
        public int ProductInformationId { get; set; }

        public string ProductInformationName { get; set; }

        public int Quantity { get; set; }
        public string ProductInformationPhoto { get; set; }
    }
}
