﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class GroupTranslation
    {
        #region Properties
        public int GroupId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
        #endregion

        public GroupSeoTranslation GroupSeoTranslation { get; set; }

        public GroupContentTranslation GroupContentTranslation { get; set; }
    }
}