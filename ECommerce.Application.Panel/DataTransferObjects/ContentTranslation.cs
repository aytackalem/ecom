﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ContentTranslation
    {

        public string LanguageId { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }

        public int ContentId { get; set; }

        public ContentTranslationContent ContentTranslationContent { get; set; }

        public ContentSeoTranslation ContentSeoTranslation { get; set; }


    }
}