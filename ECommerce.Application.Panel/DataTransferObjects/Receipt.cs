﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects;

public class Receipt
{
    /// <summary>
    /// Kesim föyü id bilgisi.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Tedarikçi id bilgisi.
    /// </summary>
    public int SupplierId { get; set; }

    /// <summary>
    /// Tedarikçi adı.
    /// </summary>
    public string SupplierName { get; set; }

    /// <summary>
    /// Birim metre değeri.
    /// </summary>
    public double UnitMeter { get; set; }

    /// <summary>
    /// Not.
    /// </summary>
    public string Note { get; set; }

    /// <summary>
    /// Askı.
    /// </summary>
    public string Hanger { get; set; }

    /// <summary>
    /// Yıkama talimatı.
    /// </summary>
    public string Wash { get; set; }

    /// <summary>
    /// Şeffaf lastik.
    /// </summary>
    public string Rubber { get; set; }

    /// <summary>
    /// Ense etiketi.
    /// </summary>
    public string Ticket { get; set; }

    /// <summary>
    /// Kart.
    /// </summary>
    public string Card { get; set; }

    /// <summary>
    /// Model kodu.
    /// </summary>
    public string SellerCode { get; set; }

    /// <summary>
    /// Termin süresi.
    /// </summary>
    public DateTime Deadline { get; set; }

    public int TotalCount { get; set; }

    public int DeadlineRemainDays => (int)Math.Ceiling((this.Deadline - DateTime.Now).TotalDays);

    public string DeadlineString => this.Deadline.ToString("dd-MM-yyy");

    public int DeadlineDays { get; set; }

    public DateTime CreatedDate { get; set; }

    public string CreatedDateString => this.CreatedDate.ToString("dd-MM-yyy");

    public List<ReceiptItem> ReceiptItems { get; set; }

    public bool Delivered { get; set; }

    public string FileName { get; set; }
}
