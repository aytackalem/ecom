﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Pos
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public bool Default { get; set; }
        #endregion
    }
}
