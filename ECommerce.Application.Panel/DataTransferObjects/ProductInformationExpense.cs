﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationExpense
    {
        #region Property
        public int Id { get; set; }

        public int ExpenseSourceId { get; set; }

        public string ProductInformationName { get; set; }

        public int ProductInformationId { get; set; }

        public decimal Amount { get; set; }

        public DateTime ProductInformationExpenseDate { get; set; }

        public string ProductInformationExpenseDateStr => ProductInformationExpenseDate.ToString("dd-MM-yyy HH:MM");

        public string Note { get; set; }
        #endregion
    }
}
