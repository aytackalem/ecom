﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationTranslationBreadcrumb
    {
        #region Properties
        /// <summary>
        /// Breadcrumb html.
        /// </summary>
        public string Html { get; set; }
        #endregion

    }
}
