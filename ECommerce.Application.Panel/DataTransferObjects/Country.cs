﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Country
    {

        #region Property
        public int Id { get; set; }
        /// <summary>
        /// Ülke adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ülke aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        #endregion
    }
}
