﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    /// <summary>
    /// Birlikte satın alınan ürünlere ilişkin raporu temsil eden sınıf.
    /// </summary>
    public class SoldTogetherReport
    {
        #region Properties
        public int Count { get; set; }
        #endregion

        #region Navigation Properties
        public List<ProductInformation> ProductInformations { get; set; }
        #endregion
    }
}
