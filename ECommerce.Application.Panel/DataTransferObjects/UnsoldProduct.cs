﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class UnsoldProduct
{
    public string SellerCode { get; set; }

    public string SkuCode { get; set; }

    public string Name { get; set; }

    public int Stock { get; set; }

    public decimal StockCost { get; set; }

    public decimal StockValue { get; set; }

    public string Categorization { get; set; }

    public string FileName { get; set; }

    public int Quantity { get; set; }

    public string Period { get; set; }

    public string Brand { get; set; }
}