﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Group
    {
        #region Properties
        public int Id { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public List<GroupTranslation> GroupTranslations { get; set; }
        #endregion
    }
}