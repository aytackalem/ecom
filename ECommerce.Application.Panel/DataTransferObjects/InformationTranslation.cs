﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class InformationTranslation
    {

        public string LanguageId { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }

        public int InformationId { get; set; }

        public InformationTranslationContent InformationTranslationContent { get; set; }

        public InformationSeoTranslation InformationSeoTranslation { get; set; }

    }
}