﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Expense
    {
        #region Property
        public int Id { get; set; }

        public int ExpenseSourceId { get; set; }

        public int? OrderSourceId { get; set; }

        public string MarketplaceId { get; set; }

        public int? ProductInformationId { get; set; }

        public decimal Amount { get; set; }

        public DateTime ExpenseDate { get; set; }

        public string ExpenseDateStr => ExpenseDate.ToString("yyyy-MM-dd");

        public string Note { get; set; }
        #endregion
    }
}
