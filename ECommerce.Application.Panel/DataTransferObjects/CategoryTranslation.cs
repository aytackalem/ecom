﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CategoryTranslation
    {
        #region Properties
        public int CategoryId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
        #endregion

        #region Navigation Property
        public CategoryContentTranslation CategoryContentTranslation { get; set; }

        public CategorySeoTranslation CategorySeoTranslation { get; set; }

        public CategoryTranslationBreadcrumb CategoryTranslationBreadcrumb { get; set; }
        #endregion
    }
}