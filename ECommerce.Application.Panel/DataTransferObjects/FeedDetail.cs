﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class FeedDetail
    {
        #region Property
        public int ProductInformationId { get; set; }

        public string ProductInformationName { get; set; }
        #endregion
    }
}
