﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ExpenseSource
    {
        #region Property
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
