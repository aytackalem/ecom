﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class HeaderReport
    {
        #region Properties
        /// <summary>
        /// Toplam satış tutarı.
        /// </summary>
        public decimal TotalSaleAmount { get; set; }

        /// <summary>
        /// Toplam ürün maliyet tutarı.
        /// </summary>
        public decimal TotalProductCostAmount { get; set; }

        /// <summary>
        /// Toplam komisyon tutarı.
        /// </summary>
        public decimal TotalCommissionAmount { get; set; }

        /// <summary>
        /// Toplam kargo maliyet tutarı.
        /// </summary>
        public decimal TotalShippingCostAmount { get; set; }
        #endregion

        #region Navigation Properties
        public ExpenseReport ExpenseReport { get; set; }
        #endregion
    }
}
