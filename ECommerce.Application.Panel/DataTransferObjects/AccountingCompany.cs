﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class AccountingCompany
    {
        #region Properties
        public int Id { get; set; }

        public string AccountingId { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Muhasebe firması yapılandırmaları.
        /// </summary>
        public List<AccountingConfiguration> AccountingConfigurations { get; set; }

        public Accounting Accounting { get; set; }
        #endregion
    }
}
