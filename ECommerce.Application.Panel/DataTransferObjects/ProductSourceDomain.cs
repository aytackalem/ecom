﻿using ECommerce.Domain.Entities.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductSourceDomain : DomainEntityBase<int>, IActivable
    {
        #region Properties
        public string ProductSourceId { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties

        public List<ProductSourceConfiguration> ProductSourceConfigurations { get; set; }
        #endregion
    }
}