﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    /// <summary>
    /// Sipariş kaynağı bazlı raporu temsil eden sınıf.
    /// </summary>
    public class OrderReportItem
    {
        #region Properties
        public string OrderSourceId { get; set; }

        public string OrderSourceName { get; set; }

        public string MarketplaceId { get; set; }

        public string MarketplaceName { get; set; }

        public int Count { get; set; }

        #region Vat Excluded
        public decimal VatExcAmount { get; set; }

        public decimal VatExcProductCost { get; set; }

        public decimal VatExcCargoFeeCost => this.Count * 21.1864M;
        #endregion

        #region Vat Included
        public decimal Amount { get; set; }

        public decimal ProductCost { get; set; }

        public decimal CargoFeeCost => this.Count * 25M;
        #endregion

        /// <summary>
        /// Komisyon tutarı.
        /// </summary>
        public decimal CommissionAmount { get; set; }

        #region Readonly
        /// <summary>
        /// Maliyet.
        /// </summary>
        public decimal Cost => this.ProductCost + this.CargoFeeCost + this.CommissionAmount;

        /// <summary>
        /// Maliyet vergi hariç.
        /// </summary>
        public decimal VatExcCost => this.VatExcProductCost + this.VatExcCargoFeeCost + this.CommissionAmount;

        /// <summary>
        /// Brüt kar.
        /// </summary>
        public decimal Profit => this.Amount - this.Cost;

        /// <summary>
        /// Brüt kar vergi hariç.
        /// </summary>
        public decimal VatExcProfit => this.VatExcAmount - this.VatExcCost;
        #endregion
        #endregion
    }
}
