﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderShipment
    {
        #region Property
        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public string ShipmentCompanyId { get; set; }

        /// <summary>
        /// Kargo takip numarası.
        /// </summary>
        public string TrackingCode { get; set; }


        /// <summary>
        /// Kargo takip numarası.
        /// </summary>
        public string TrackingUrl { get; set; }

        /// <summary>
        /// Sipariş kargo bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        public bool Payor { get; set; }

        public string PackageNumber { get; set; }
        #endregion

        #region Navigation Property


        /// <summary>
        /// İlgili kargo firması.
        /// </summary>
        public ShipmentCompany ShipmentCompany { get; set; }
        #endregion
    }
}
