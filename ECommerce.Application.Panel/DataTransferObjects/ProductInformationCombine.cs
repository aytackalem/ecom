﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    /// <summary>
    /// Ürün detay birden fazla ürün detayın birleişiminden oluştuğunda kullanıcak olan sınıf.
    /// </summary>
    public class ProductInformationCombine
    {
        #region Properties
        public int Id { get; set; }

        /// <summary>
        /// Ürün detay id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Ürün detayı oluşturan ilişkili ürün detay id bilgisi.
        /// </summary>
        public int ContainProductInformationId { get; set; }

        /// <summary>
        /// Ürün detayının kaç adet satılacağı bilgisi.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Ürün İsmi
        /// </summary>
        public string Name { get; set; }
        #endregion


    }
}
