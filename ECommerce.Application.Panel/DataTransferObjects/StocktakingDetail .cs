﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class StocktakingDetail
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public int Quantity { get; set; }

        /// <summary>
        /// Bu deger ilgili urune ait "Toplandi" statusundeki adetleri belirtir.
        /// </summary>
        public int CollectedQuantity { get; set; }

        public int PackedQuantity { get; set; }

        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
