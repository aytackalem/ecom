﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderDeliveryAddress
    {
        #region Property
        /// <summary>
        /// Müşteri ismi
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Müşteri soyadu
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Telefon Numarası
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// İlgili mahalleye ait id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Açık adres.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Sipariş teslimat adresinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int CountryId { get; set; }
        #endregion
    }
}
