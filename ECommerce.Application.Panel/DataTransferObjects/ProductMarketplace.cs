﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductMarketplace
    {
        #region Properties
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string MarketplaceBrandCode { get; set; }

        public string MarketplaceBrandName { get; set; }

        public string MarketplaceCategoryCode { get; set; }

        public string MarketplaceCategoryName { get; set; }

        public string SellerCode { get; set; }

        public string MarketplaceId { get; set; }

        public bool Active { get; set; }

        public string UUId { get; set; }

        public int DeliveryDay { get; set; }
        #endregion

        #region Navigation Properties

        #endregion
    }
}
