﻿using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    /// <summary>
    /// Sipariş kaynağı bazlı raporu temsil eden sınıf.
    /// </summary>
    public class OrderReport
    {
        #region Properties
        #region Vat Excluded
        public decimal TotalVatExcAmount => this.OrderReportItems.Sum(ori => ori.VatExcAmount);

        public decimal TotalVatExcProductCost => this.OrderReportItems.Sum(ori => ori.VatExcProductCost);

        public decimal TotalVatExcCargoFeeCost => this.OrderReportItems.Sum(ori => ori.VatExcCargoFeeCost);

        public decimal TotalVatExcCost => this.OrderReportItems.Sum(ori => ori.VatExcCost);

        public decimal TotalVatExcProfit => this.OrderReportItems.Sum(ori => ori.VatExcProfit);
        #endregion

        #region Vat Included
        public decimal TotalAmount => this.OrderReportItems.Sum(ori => ori.Amount);

        public decimal TotalProductCost => this.OrderReportItems.Sum(ori => ori.ProductCost);

        public decimal TotalCargoFeeCost => this.OrderReportItems.Sum(ori => ori.CargoFeeCost);

        public decimal TotalCost => this.OrderReportItems.Sum(ori => ori.Cost);

        public decimal Profit => this.OrderReportItems.Sum(ori => ori.Profit);
        #endregion

        public decimal TotalCommissionAmount => this.OrderReportItems.Sum(ori => ori.CommissionAmount);

        public int TotalCount => this.OrderReportItems.Sum(ori => ori.Count);

        public decimal ExpenseAmount { get; set; }
        #endregion

        #region Navigation Properties
        public List<OrderReportItem> OrderReportItems { get; set; }
        #endregion
    }
}
