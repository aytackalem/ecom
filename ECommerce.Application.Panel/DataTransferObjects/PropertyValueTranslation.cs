﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PropertyValueTranslation
    {
        #region Properties
        public int Id { get; set; }

        public string LanguageId { get; set; }

        public string Value { get; set; }

        public int PropertyValueId { get; set; }
        #endregion
    }
}
