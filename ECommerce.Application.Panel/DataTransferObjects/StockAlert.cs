﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class StockAlert
{
    public string Name { get; set; }

    public string SellerCode { get; set; }

    public string SkuCode { get; set; }

    public string FileName { get; set; }

    public int Stock { get; set; }

    public int DailyQuantity { get; set; }

    public int Remain { get; set; }
}
