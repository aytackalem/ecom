﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class District
    {

        #region Property

        public int Id { get; set; }

        /// <summary>
        /// İlçe adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// İlçenin ait olduğu şehre ait id bilgisi.
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// İlçe aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlçenin ait olduğu şehir.
        /// </summary>
        public City City { get; set; }


        #endregion
    }
}
