﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CustomerAddress
    {
        #region Properties
        public int Id { get; set; }

        public int NeighborhoodId { get; set; }

        public int DistrictId { get; set; }
        
        public int CityId { get; set; }
        
        public int CountryId { get; set; }
        
        public string Address { get; set; }
        
        public string Phone { get; set; }
        #endregion
    }
}
