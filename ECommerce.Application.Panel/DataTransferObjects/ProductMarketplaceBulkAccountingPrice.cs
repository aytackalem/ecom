﻿using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductMarketplaceBulkAccountingPrice
    {
        #region Properties
       
        /// <summary>
        /// Gelen değere göre pazaryeri veya web sitesinde toplu erp/xml açılsın.
        /// </summary>
        public AccountingPrice AccountingPriceId { get; set; }


        public List<string> MarketplaceIds { get; set; }


        public string Search { get; set; }
        #endregion

    }
}
