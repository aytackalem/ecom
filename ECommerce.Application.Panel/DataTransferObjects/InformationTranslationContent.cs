﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class InformationTranslationContent
    {
        public string Html { get; set; }

    }
}