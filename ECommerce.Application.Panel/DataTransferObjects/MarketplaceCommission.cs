﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceCommission
    {
        public decimal Rate { get; set; }
    }
}
