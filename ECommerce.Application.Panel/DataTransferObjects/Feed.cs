﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Feed
    {
        #region Property
        public int Id { get; set; }

        public string FeedTemplateId { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }
        #endregion

        #region Navigation Properties
        public List<FeedDetail> FeedDetails { get; set; }
        #endregion
    }
}
