﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CompanyContact
    {
        #region Properties
        /// <summary>
        /// Şirket adresi.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Şirket telefonu.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Web site adresi
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// Mersis No
        /// </summary>
        public string RegistrationNumber { get; set; }
        #endregion
    }
}
