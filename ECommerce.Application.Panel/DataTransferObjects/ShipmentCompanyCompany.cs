﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ShipmentCompanyCompany
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<ShipmentCompanyConfiguration> ShipmentCompanyConfigurations { get; set; }

        #endregion
    }
}
