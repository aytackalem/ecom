﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class SalesPerformance
    {
        public string SkuCode { get; set; }

        public int Quantity { get; set; }

        public string Type { get; set; }
    }
}
