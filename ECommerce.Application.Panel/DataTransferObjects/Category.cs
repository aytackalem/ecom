﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Category
    {
        #region Properties
        public int Id { get; set; }

        public int? ParentCategoryId { get; set; }

        public string FileName { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryTranslation> CategoryTranslations { get; set; }
        #endregion
    }
}