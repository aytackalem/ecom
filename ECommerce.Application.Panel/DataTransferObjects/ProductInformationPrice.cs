﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationPrice
    {
        #region Properties
        public int Id { get; set; }

        public string CurrencyId { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal UnitCost { get; set; }

        public decimal ConversionUnitCost { get; set; }

        public string ConversionCurrencyId { get; set; }

        public decimal VatRate { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa istediği ürünün fiyatını erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingPrice { get; set; }
        #endregion
    }
}
