﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public record ColorBasedSalesReport(List<ColorBasedSalesReportItem> Items);
}
