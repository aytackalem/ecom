﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationMarketplaceBulkPrice
    {
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Erp/Xml için ürün fiyat güncellemesi açık mı?
        /// </summary>
        public bool AccountingPrice { get; set; }

        /// <summary>
        /// İlgili ürünün pazaryerine gönderilip/gönderilmeyeceği bilgisini tutan özellik.
        /// </summary>
        public bool Active { get; set; }
    }
}
