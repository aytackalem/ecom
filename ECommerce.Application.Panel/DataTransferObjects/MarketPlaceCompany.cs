﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceCompany
    {
        #region Properties
        public int Id { get; set; }

        public string MarketplaceId { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public bool CreateProduct { get; set; }

        public bool ReadOrder { get; set; }

        public bool UpdatePrice { get; set; }

        public bool UpdateStock { get; set; }
        
        public bool IsEcommerce { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceConfiguration> MarketplaceConfigurations { get; set; }
        #endregion
    }
}
