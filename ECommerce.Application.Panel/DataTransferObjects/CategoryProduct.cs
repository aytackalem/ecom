﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CategoryProduct
    {
        #region Properties
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public int ProductId { get; set; }
        #endregion
    }
}
