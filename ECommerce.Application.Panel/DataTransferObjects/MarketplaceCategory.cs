﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceCategory
    {
        #region Properties
        public string Id { get; set; }

        public string MarketplaceId { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceVariant> MarketplaceVariants { get; set; }
        #endregion
    }
}
