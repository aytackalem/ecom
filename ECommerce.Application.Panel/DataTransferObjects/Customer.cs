﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Customer
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// Müşteri adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Müşteri soyadı.
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Müşteri kaydının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Müşteri iletişim bilgileri.
        /// </summary>
        public CustomerContact CustomerContact { get; set; }

        /// <summary>
        /// Müşteriye kayıtlı adresler. Örn: Ev, iş gibi.
        /// </summary>
        public List<CustomerAddress> CustomerAddresses { get; set; }

        /// <summary>
        /// Müşteriye ait notlar. Bu notlar müşteriye gösterilmek için değil müşteri hakkında bilgi toplamak için kullanılmaktadır.
        /// </summary>
        public List<CustomerNote> CustomerNotes { get; set; }

        /// <summary>
        /// Müşteri kullanıcı bilgileri.
        /// </summary>
        public CustomerUser CustomerUser { get; set; }
        #endregion
    }
}
