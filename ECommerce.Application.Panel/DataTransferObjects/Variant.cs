﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Variant
    {
        #region Properties
        public int Id { get; set; }

        public bool Photoable { get; set; }

        public bool Slicer { get; set; }

        public bool Varianter { get; set; }

        public bool ShowVariant { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantTranslation> VariantTranslations { get; set; }

        public List<VariantValue> VariantValues { get; set; }

        public List<MarketplaceVariantMapping> MarketplaceVariantMappings { get; set; }
        #endregion
    }
}
