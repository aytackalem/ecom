﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationContentTranslation
    {
        #region Properties
        /// <summary>
        /// Ürünye ait içerik.
        /// </summary>
        public string Content { get; set; }


        /// <summary>
        /// Ürünye ait içerik.
        /// </summary>
        public string Description { get; set; }
        #endregion

    }
}
