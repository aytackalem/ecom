﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Content
    {
        #region Properties

        public int Id { get; set; }
        public bool Active { get; set; }
        public List<ContentTranslation> ContentTranslations { get; set; }
        #endregion
    }
}