﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CategoryTranslationBreadcrumb
    {
        #region Properties
        /// <summary>
        /// Breadcrumb html.
        /// </summary>
        public string Html { get; set; }
        #endregion

    }
}