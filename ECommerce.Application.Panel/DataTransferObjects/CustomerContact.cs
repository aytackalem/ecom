﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class CustomerContact
    {
        #region Property
        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// E-posta.
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Vergi dairesi.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Vergi numarası.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Müşteri iletişim bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }
        #endregion

    }
}
