﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Stocktaking
    {
        #region Properties
        public int Id { get; set; }

        public string StocktakingTypeId { get; set; }

        public string? ModelCode { get; set; }

        public bool CheckGroup { get; set; }

        public bool Manuel { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string CreatedDateTimeStr => CreatedDateTime.ToString("dd-MM-yyy HH:mm");

        public List<StocktakingDetail> StocktakingDetails { get; set; }
        #endregion
    }
}
