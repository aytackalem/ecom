﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductBasedSalesReport
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public string FileName { get; set; }

        public decimal AvgUnitCargoFee { get; set; }

        public decimal AvgUnitCost { get; set; }

        public decimal AvgUnitPrice { get; set; }

        public decimal AvgUnitCommissionAmount { get; set; }

        public decimal ExpenseAmount { get; set; }
        #endregion

        #region Readonly Properties
        public decimal TotalProductCost => this.Quantity * this.AvgUnitCost;

        public decimal TotalSalesAmount => this.Quantity * this.AvgUnitPrice;

        public decimal TotalCommissionAmount => this.Quantity * this.AvgUnitCommissionAmount;

        public decimal TotalCargoFee => this.Quantity * this.AvgUnitCargoFee;

        public decimal TotalCost => this.TotalProductCost + this.TotalCommissionAmount + this.TotalCargoFee + this.ExpenseAmount;

        public decimal Profit => this.TotalSalesAmount - this.TotalCost;

        public decimal ProfitRate => this.Profit <= 0 ? 0 : this.Profit / this.TotalSalesAmount * 100M;
        #endregion
    }
}
