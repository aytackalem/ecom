﻿using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceAccountingPrice
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int AccountingPriceTotalCount { get; set; }

        public int OpenAccountingPriceCount { get; set; }

        public int ClosedAccountingPriceCount { get; set; }
        #endregion

    }
}
