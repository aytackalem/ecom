﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class WholesaleOrderDetail
{
    public int ProductInformationId { get; set; }

    public ProductInformation ProductInformation { get; set; }

    public int Quantity { get; set; }
}
