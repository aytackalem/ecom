﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationVariantValue
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string Value { get; set; }

        public string MarketplaceVariantId { get; set; }

        public string MarketplaceVariantValueId { get; set; }

        public int MarketplaceCategoriesMarketplaceVariantValueId { get; set; }
        #endregion

        #region Navigation Properties

        #endregion
    }
}
