﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ShoppingCartDiscountedProduct
    {
        #region Properties
        public int Id { get; set; }

        public string ApplicationId { get; set; }

        public int ProductId { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public ShoppingCartDiscountedProductSetting ShoppingCartDiscountedProductSetting { get; set; }

        public Product Product { get; set; }
        #endregion
    }
}
