﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderProductReport
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public string FileName { get; set; }

        public string StockCode { get; set; }

        public string Barcode { get; set; }

        public string SkuCode { get; set; }

        public string SellerCode { get; set; }

        public int Stock { get; set; }

        public int ProductId { get; set; }
    }
}
