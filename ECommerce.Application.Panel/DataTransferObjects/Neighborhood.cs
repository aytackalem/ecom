﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Neighborhood
    {
        #region Property
        public int Id { get; set; }

        /// <summary>
        /// Mahalle adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mahallenin ait olduğu ilçeye ait id bilgisi.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Mahalle aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Mahallenin ait olduğu ilçe.
        /// </summary>
        public District District { get; set; }
        #endregion
    }
}
