﻿using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductMarketplaceBulkPrice
    {
        #region Properties
        /// <summary>
        /// Ekrandan gelen değere göre pazaryerini veya web sitesinde güncellenecek parametreyi gösterir.
        /// TY_LP Trendyol Liste Fiyatı
        /// TY_UN Trendyol Satış Fiyatı
        /// </summary>
        public string UpdatePriceId { get; set; }


        /// <summary>
        /// Ekrandan gelen değere göre kaynak pazaryerini veya web sitesini gösterir.
        /// TY_LP Trendyol Liste Fiyatı
        /// TY_UN Trendyol Satış Fiyatı
        /// </summary>
        public string SourcePriceId { get; set; }

        /// <summary>
        /// Gelen değere göre pazaryeri veya web sitesinde oransal hesaplama ve normal hesaplama yapılır.
        /// </summary>
        public ProductPrice ProductPriceId { get; set; }

        /// <summary>
        /// Gelen değere göre pazaryeri veya web sitesinde oransal hesaplama ve normal hesaplama yapılır.
        /// </summary>
        public ProductPriceFraction ProductPriceFractionId { get; set; }

        /// <summary>
        /// Gelen değere göre pazaryeri veya web sitesinde toplu erp/xml açılsın.
        /// </summary>
        public AccountingPrice AccountingPriceId { get; set; }

        /// <summary>
        /// Gelen değere göre pazaryeri veya web sitesinde hesaplama yapılır.
        /// </summary>
        public decimal PriceCalculated { get; set; }


        public string Search { get; set; }
        #endregion

    }
}
