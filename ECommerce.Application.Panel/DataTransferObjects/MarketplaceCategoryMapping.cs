﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceCategoryMapping
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int? CompanyId { get; set; }

        public string MarketplaceCategoryName { get; set; }

        public string MarketplaceCategoryCode { get; set; }
        #endregion

        public List<MarketplaceCategoryVariantValueMapping> MarketplaceCategoryVariantValueMappings { get; set; }
        public int Id { get; set; }
    }
}
