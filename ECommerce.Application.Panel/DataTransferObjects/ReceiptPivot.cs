﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ReceiptPivot
    {
        public List<string> Colors { get; set; }

        public List<string> Sizes { get; set; }

        public List<ReceiptPivotItem> ReceiptPivotItems { get; set; }
    }
}
