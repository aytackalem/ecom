﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationComment
    {
        #region Properties
        public int Id { get; set; }

        public int ProductInformationId { get; set; }

        public string Comment { get; set; }

        public bool Active { get; set; }

        public bool Approved { get; set; }
        #endregion

        #region Navigation Properties
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
