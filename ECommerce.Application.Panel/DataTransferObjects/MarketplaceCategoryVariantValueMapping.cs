﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceCategoryVariantValueMapping
    {
        #region Properties
        //public int Id { get; set; }

        public string MarketplaceId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }

        public bool Varianter { get; set; }

        public bool Multiple { get; set; }
        #endregion

    }
}
