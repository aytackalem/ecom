﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public record PickedReport(List<PickedItem> PickedItems);
}
