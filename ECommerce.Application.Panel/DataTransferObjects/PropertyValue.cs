﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PropertyValue
    {
        #region Properties
        public int Id { get; set; }

        public int PropertyId { get; set; }
        #endregion

        #region Navigation Properties
        public List<PropertyValueTranslation> PropertyValueTranslations { get; set; }
        #endregion
    }
}
