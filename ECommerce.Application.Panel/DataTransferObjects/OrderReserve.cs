﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderReserve
    {
        #region Properties
        public string Number { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyyy HH:mm:ss");
        #endregion
    }
}
