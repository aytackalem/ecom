﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductProperty
    {
        #region Properties
        public int ProductId { get; set; }

        public int PropertyValueId { get; set; }
        #endregion
    }
}
