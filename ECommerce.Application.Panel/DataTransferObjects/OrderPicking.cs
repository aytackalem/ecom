﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderPacking
    {
        #region Property

        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyyy HH:mm:ss");

        /// <summary>
        /// Siparişi kimin paketlediği
        /// </summary>
        public string PackedUsername { get; set; }
        #endregion
    }
}
