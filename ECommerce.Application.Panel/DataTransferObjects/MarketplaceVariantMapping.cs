﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceVariantMapping
    {
        #region Properties
        public int Id { get; set; }

        public int VariantId { get; set; }

        public int? CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }
        #endregion
    }
}
