﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationMarketplace
    {
        #region Properties
        public int Id { get; set; }

        public int ProductInformationId { get; set; }

        public string MarketplaceId { get; set; }

        //public int MarketplaceCompanyId { get; set; }

        public string StockCode { get; set; }

        public string Barcode { get; set; }

        public bool Active { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Company'nin bütün sistemi Erp sistemi ile mi çalışıyorsa isteiği ürünün fiyatını erp'den değişime açıp katabilen özellik.
        /// </summary>
        public bool AccountingPrice { get; set; }

        public string Url { get; set; }
        
        public string UUId { get; set; }
        
        public bool Opened { get; set; }

        /// <summary>
        /// Ürün fiyat ve ürün bilgilerinin güncellenmesini engelleyen ozellik. Stok guncellemesini etkilemez.
        /// </summary>
        public bool DisabledUpdateProduct { get; set; }
        #endregion

        #region Navigation Properties
        public ProductInformationMarketplaceBulkPrice ProductInformationMarketplaceBulkPrice { get; set; }

        public List<ProductInformationMarketplaceVariantValue> ProductInformationMarketplaceVariantValues { get; set; }    
        #endregion
    }
}
