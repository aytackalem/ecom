﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PosConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public string PosId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
