﻿namespace ECommerce.Application.Panel.DataTransferObjects;

public class ReceiptProductInformationQuantity
{
    public int Id { get; set; }

    public string Name { get; set; }

    public int Quantity { get; set; }
}
