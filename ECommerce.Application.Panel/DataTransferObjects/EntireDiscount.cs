﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class EntireDiscount
    {
        #region Properties
        public int Id { get; set; }

        public string ApplicationId { get; set; }

        public DateTime StartDate { get; set; }

        public string StartDateString { get; set; }

        public DateTime EndDate { get; set; }

        public string EndDateString { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public EntireDiscountSetting EntireDiscountSetting { get; set; }
        #endregion
    }
}
