﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderReturnDetail
    {
        #region Property
        public int Id { get; set; }
        /// <summary>
        /// Detayin ait olduğu siparişin id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Geri gönderilen ürün iade alınabilir mi?
        /// </summary>
        public bool IsReturn { get; set; }

        /// <summary>
        /// Geri gönderilen ürün zayi mi?
        /// </summary>
        public bool IsLoss { get; set; }

        /// <summary>
        /// Sipariş detayının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }
        #endregion
    }
}
