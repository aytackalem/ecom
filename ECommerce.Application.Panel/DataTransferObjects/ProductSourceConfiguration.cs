﻿using ECommerce.Domain.Entities.Base;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductSourceConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public int ProductSourceDomainId { get; set; }

        /// <summary>
        /// Anahtar adı kullanıcı ekranda görür
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Anahtar key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Değer.
        /// </summary>
        public string Value { get; set; }
        #endregion

     
    }
}