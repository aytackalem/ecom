﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderBilling
    {
        #region Properties
        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyyy HH:mm:ss");

        public string ReturnDescription { get; set; }

        public string ReturnNumber { get; set; }

        public string InvoiceNumber { get; set; }
        #endregion
    }
}
