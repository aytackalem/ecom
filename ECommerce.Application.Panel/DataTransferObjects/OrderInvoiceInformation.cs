﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderInvoiceInformation
    {
        #region Property
        /// <summary>
        /// Müşteri ismi
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Müşteri soyadu
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// İlgili mahalleye ait id bilgisi.
        /// </summary>
        public int NeighborhoodId { get; set; }

        /// <summary>
        /// Açık adres.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Telefon.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// E-posta.
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Vergi dairesi.
        /// </summary>
        public string TaxOffice { get; set; }

        /// <summary>
        /// Vergi numarası.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Sipariş fatura bilgisinin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property

        /// <summary>
        /// İlgili mahalle.
        /// </summary>
        public Neighborhood Neighborhood { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int CountryId { get; set; }
        #endregion
    }
}
