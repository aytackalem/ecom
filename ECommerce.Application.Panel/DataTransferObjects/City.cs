﻿using ECommerce.Application.Common.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class City
    {

        #region Property
        public int Id { get; set; }

        /// <summary>
        /// Şehir adı.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ait olduğu ülkenin id bilgisi.
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Şehir aktif mi?
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Ait olduğu ülke.
        /// </summary>
        public Country Country { get; set; }

        #endregion
    }
}
