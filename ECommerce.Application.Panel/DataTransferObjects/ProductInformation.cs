﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformation
    {
        #region Fields
        private string _productInformationName = string.Empty;

        private string _fullName = string.Empty;
        #endregion

        #region Properties
        public int Id { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        /// <summary>
        /// Urune ait satilabilir stok bilgisi.
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Urune ait rezerve edilmis stok bilgisi.
        /// </summary>
        public int ReservedStock { get; set; }

        public int VirtualStock { get; set; }

        public bool IsSale { get; set; }

        public bool Payor { get; set; }

        public string ProductInformationName { get; set; }

        public bool DirtyStock { get; set; }

        public bool DirtyProductInformation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Photoable { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string GroupId { get; set; }

        /// <summary>
        /// Stok ekranında seçiliyse tüm variantlara aynı stoğu günceller
        /// </summary>
        public bool AllVariantStock { get; set; }

        public bool Active { get; set; }

        /// <summary>
        /// Companysetting tablosunda IncludeAccounting'in açık kapalı olduğunu gösterir.
        /// </summary>
        public bool IncludeAccounting { get; set; }

        /// <summary>
        /// Toplu fiyat güncelleme ekranında çıkar butonunu gösterilmesi için yapıldı
        /// </summary>
        public bool MarketplaceBulkPriceOpen { get; set; }

        /// <summary>
        /// Ürünün bulunduğu raf bilgisi.
        /// </summary>
        public string ShelfCode { get; set; }
        public string SkuCode { get; set; }

        public string VariantValuesDescription { get; set; }

        public string Categorization { get; set; }
        #endregion

        #region Navigation Properties
        public List<ProductInformationCombine> ProductInformationCombines { get; set; }

        public List<ProductInformationPrice> ProductInformationPriceses { get; set; }

        public List<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        public List<ProductInformationTranslation> ProductInformationTranslations { get; set; }

        public List<ProductInformationVariant> ProductInformationVariants { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

        public Product Product { get; set; }
        public float ReturnRate { get; set; }
        public float ColorReturnRate { get; set; }
        #endregion

        public override string ToString()
        {
            if (this.ProductInformationVariants?.Count > 0)
                return string.Join(", ", this.ProductInformationVariants.GroupBy(x => x.VariantValueId).Select(pv =>
                $"{pv.First().VariantValue.Variant.VariantTranslations[0].Name.Trim()}: {pv.First().VariantValue.VariantValueTranslations[0].Value.Trim()}"
                ));
            else
                return string.Empty;
        }
    }
}
