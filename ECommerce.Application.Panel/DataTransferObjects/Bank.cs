﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Bank
    {
        #region Properties
        public string Id { get; set; }

        public string PosId { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
        #endregion
    }
}
