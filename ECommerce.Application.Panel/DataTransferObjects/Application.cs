﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Application
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
