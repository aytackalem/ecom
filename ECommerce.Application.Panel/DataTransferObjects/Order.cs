﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Order
    {
        #region Properties
        public int Id { get; set; }


        /// <summary>
        /// Siparişin oluşturuğu uygulamaya ait id bilgisi.
        /// </summary>
        public string ApplicationId { get; set; }


        /// <summary>
        /// İlgili para birim toplamı.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// İlgili müşteriye ait id bilgisi.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// İlgili para birimine ait id bilgisi.
        /// </summary>
        public string CurrencyId { get; set; }


        /// <summary>
        /// Siparişin ait olduğu pazar yerine ait id bilgisi.
        /// </summary>
        public string MarketplaceId { get; set; }

        /// <summary>
        /// İlgili sipariş tipine ait id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Siparişin oluşturulduğu tarih.
        /// </summary>
        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyyy HH:mm:ss");

        /// <summary>
        /// Siparişin gerçek tarihi.
        /// </summary>
        public DateTime OrderDate { get; set; }


        /// <summary>
        /// Siparişin gerçek tarihi.
        /// </summary>
        public string OrderDateStr => OrderDate.ToString("dd-MM-yyyy HH:mm:ss");

        public int OrderSourceId { get; set; }

        public decimal CargoFee { get; set; }

        public decimal CommissionAmount { get; set; }
        public decimal ListTotal { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Discount { get; set; }


        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        public decimal VatExcListTotal { get => this.OrderDetails?.Count > 0 ? this.ListTotal / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        public decimal VatExcTotal { get => this.OrderDetails?.Count > 0 ? this.TotalAmount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        public decimal VatExcDiscount { get => this.OrderDetails?.Count > 0 ? this.Discount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.VatRate)) / this.OrderDetails.Count)) : 0; }


        public string MarketplaceOrderNumber { get; set; }

        public bool MutualBarcode { get; set; }

        public string PackerBarcode { get; set; }

        public MarketplaceInvoice MarketplaceInvoice { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// Sipariş kargo bilgileri.
        /// </summary>
        public List<OrderShipment> OrderShipments { get; set; }

        /// <summary>
        /// Sipariş teslimat bilgileri.
        /// </summary>
        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        /// <summary>
        /// Sipariş fatura bilgileri.
        /// </summary>
        public OrderInvoiceInformation OrderInvoiceInformation { get; set; }

        /// <summary>
        /// Siparişe ait notlar.
        /// </summary>
        public List<OrderNote> OrderNotes { get; set; }

        /// <summary>
        /// Siparişe ait ödemeler.
        /// </summary>
        public List<Payment> Payments { get; set; }

        /// <summary>
        /// Siparişe ait detaylar.
        /// </summary>
        public List<OrderDetail> OrderDetails { get; set; }

        /// <summary>
        /// Siparişe ait iade detayları.
        /// </summary>
        public List<OrderReturnDetail> OrderReturnDetails { get; set; }


        /// <summary>
        /// İlgili müşteri.
        /// </summary>
        public Customer Customer { get; set; }
      

        /// <summary>
        /// İlgili sipariş tipi.
        /// </summary>
        public OrderType OrderType { get; set; }


        public Marketplace MarketPlace { get; set; }

        public OrderSource OrderSource { get; set; }

        public OrderPacking OrderPacking { get; set; }

        public OrderPicking OrderPicking { get; set; }

        public OrderBilling OrderBilling { get; set; }

        public OrderReserve OrderReserve { get; set; }
        #endregion
    }
}
