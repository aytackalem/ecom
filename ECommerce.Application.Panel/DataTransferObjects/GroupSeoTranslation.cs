﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class GroupSeoTranslation
    {
        #region Properties
        /// <summary>
        /// Başlık.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama.
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Anahtar kelimeler.
        /// </summary>
        public string MetaKeywords { get; set; }
        #endregion

    }
}