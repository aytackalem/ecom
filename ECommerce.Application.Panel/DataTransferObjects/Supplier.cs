﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Supplier
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
