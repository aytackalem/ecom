﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Identity
    {
        #region Properties
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime Expiry { get; set; }
        #endregion
    }
}
