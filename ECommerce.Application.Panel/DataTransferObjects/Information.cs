﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Information
    {
        #region Properties

        public int Id { get; set; }
        public bool Active { get; set; }
        public List<InformationTranslation> InformationTranslations { get; set; }
        #endregion
    }
}