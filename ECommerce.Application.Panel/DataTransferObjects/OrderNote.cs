﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderNote
    {
        #region Property
        /// <summary>
        /// İlgili siparişe ait id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Not.
        /// </summary>
      
        public string Note { get; set; }

        /// <summary>
        /// Sipariş notunun oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}
