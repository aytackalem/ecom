﻿using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects.Notifications
{
    public class NotificationItem
    {
        #region Properties
        public string Id { get; set; }

        public string MarketplaceId { get; set; }

        public string Title { get; set; }

        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ProductInformationMarketplaceId { get; set; }

        public string Message { get; set; }

        public string PhotoUrl { get; set; }

        public int ProductId { get; set; }

        public int ProductInformationId { get; set; }

        public string ProductInformationName { get; set; }

        public string CreatedDateStr => CreatedDate.ToString("dd-MM-yyy HH:mm");

        #endregion
    }
}
