﻿namespace ECommerce.Application.Panel.DataTransferObjects.Notifications
{
    public class NotificationItemCount
    {
        #region Properties
        public long TotalCount { get; set; }
        #endregion
    }
}
