﻿namespace ECommerce.Application.Panel.DataTransferObjects.Notifications
{
    public class NotificationCount
    {
        #region Properties
        public string MarketplaceRequestTypeId { get; set; }
        public int MarketplaceRequestCount { get; set; }
        #endregion
    }
}
