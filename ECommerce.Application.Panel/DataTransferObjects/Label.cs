﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Label
    {
        #region Properties
        public int Id { get; set; }

        public int DisplayOrder { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Property
        public List<LabelTranslation> LabelTranslations { get; set; }
        #endregion

    }
}
