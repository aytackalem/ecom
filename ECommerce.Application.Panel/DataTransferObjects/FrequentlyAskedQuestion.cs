﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class FrequentlyAskedQuestion
    {
        #region Properties

        public int Id { get; set; }
        public bool Active { get; set; }
        public List<FrequentlyAskedQuestionTranslation> FrequentlyAskedQuestionTranslations { get; set; }
        #endregion
    }
}