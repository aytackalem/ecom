﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class GroupContentTranslation
    {
        #region Properties
        /// <summary>
        /// Kategoriye ait içerik.
        /// </summary>
        public string Content { get; set; }
        #endregion
    }
}