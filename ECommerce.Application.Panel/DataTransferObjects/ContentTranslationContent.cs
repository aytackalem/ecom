﻿using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ContentTranslationContent
    {
        public string Html { get; set; }

    }
}