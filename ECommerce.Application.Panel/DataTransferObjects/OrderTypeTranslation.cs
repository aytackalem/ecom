﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderTypeTranslation
    {
        #region Property
        /// <summary>
        /// Sipariş tip id bilgisi.
        /// </summary>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// İlgili dil id bilgisi.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Sipariş tip adı.
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş tipi.
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// İlgili dil.
        /// </summary>
        public Language Language { get; set; }
        #endregion
    }
}
