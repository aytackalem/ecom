﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Accounting
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        /// <summary>
        /// Muhasebe firması yapılandırmaları.
        /// </summary>
        public List<AccountingCompany> AccountingCompanies { get; set; }
        #endregion
    }
}
