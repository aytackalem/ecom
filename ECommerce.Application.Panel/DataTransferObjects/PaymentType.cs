﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PaymentType
    {
        #region Properties
        public string Id { get; set; }

        public bool Active { get; set; }

        public string PaymentTypeName { get; set; }
        #endregion

        #region Navigation Properties
        public List<PaymentTypeTranslation> PaymentTypeTranslations { get; set; }

        public PaymentTypeSetting PaymentTypeSetting { get; set; }
        #endregion
    }
}
