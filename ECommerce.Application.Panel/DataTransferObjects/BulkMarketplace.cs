﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class BulkMarketplace
    {
        #region Properties
        public int ProductInformationId { get; set; }

        public string ProductInformationName { get; set; }

        public string SellerCode { get; set; }

        public string SkuCode { get; set; }
        
        public string StockCode { get; set; }

        public string Barcode { get; set; }

        public int Stock { get; set; }

        public List<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }
        #endregion
    }



}
