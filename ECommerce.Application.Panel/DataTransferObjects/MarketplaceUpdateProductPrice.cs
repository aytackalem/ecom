﻿using ECommerce.Application.Panel.Parameters.ProductPrices.Enums;
using System;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceUpdateProductPrice
    {
        #region Properties
        public string MarketplaceId { get; set; }

        public int UpdateProductPriceTotalCount { get; set; }

        public int OpenUpdateProductPriceCount { get; set; }

        public int ClosedUpdateProductPriceCount { get; set; }
        #endregion

    }
}
