﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ShipmentCompanyConfiguration
    {
        #region Properties
        public int Id { get; set; }

        public int ShipmentCompanyCompanyId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
