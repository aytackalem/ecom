﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public record PickedItem(string Username, int Hour, int OrderQuantity, int ProductQuantity);
}
