﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceVariantValueMapping
    {
        #region Properties
        public int Id { get; set; }

        public int? CompanyId { get; set; }

        public string MarketplaceId { get; set; }

        public int VariantValueId { get; set; }

        public string MarketplaceVariantCode { get; set; }

        public string MarketplaceVariantName { get; set; }

        public string MarketplaceVariantValueCode { get; set; }

        public string MarketplaceVariantValueName { get; set; }

        public bool AllowCustom { get; set; }

        public bool Mandatory { get; set; }
        #endregion
    }
}
