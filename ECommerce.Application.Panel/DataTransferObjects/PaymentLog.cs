﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PaymentLog
    {
        #region Properties
        public string Pay3dMessage { get; set; }

        public string Check3dMessage { get; set; }
        #endregion
    }
}
