﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class PaymentTypeSetting
    {
        #region Properties
        public string Id { get; set; }

        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }

        public bool CostIsRate { get; set; }

        public decimal Cost { get; set; }
        #endregion
    }
}
