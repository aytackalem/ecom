﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductLabel
    {
        #region Properties
        public int Id { get; set; }

        public int LabelId { get; set; }
        #endregion
    }
}
