﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class ProductInformationTranslation
    {
        #region Properties
        public int Id { get; set; }

        public int ProductInformationId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string SubTitle { get; set; }

        public string VariantValuesDescription { get; set; }

        public string FullVariantValuesDescription { get; set; }
        #endregion

        public ProductInformation ProductInformation { get; set; }

        public ProductInformationSeoTranslation ProductInformationSeoTranslation { get; set; }

        public ProductInformationContentTranslation ProductInformationContentTranslation { get; set; }

        public ProductInformationTranslationBreadcrumb ProductInformationTranslationBreadcrumb { get; set; }
    }
}
