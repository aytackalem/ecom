﻿using ECommerce.Application.Common.DataTransferObjects;
using System;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class OrderDetail
    {
        #region Property
        public int Id { get; set; }
        /// <summary>
        /// Detayin ait olduğu siparişin id bilgisi.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// İlgili ürüne ait id bilgisi.
        /// </summary>
        public int ProductInformationId { get; set; }

        /// <summary>
        /// Adet.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// List satış fiyatı.
        /// </summary>
        public decimal ListUnitPrice { get; set; }

        /// <summary>
        /// Birim fiyatı.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Birim maliyeti.
        /// </summary>
        public decimal UnitCost { get; set; }

        /// <summary>
        /// Vergi oranı.
        /// </summary>
        public decimal VatRate { get; set; }

        public bool Payor { get; set; }

        /// <summary>
        /// İndirim Tutarı
        /// </summary>
        public decimal UnitDiscount { get; set; }


        /// <summary>
        /// İndirim Tutarı
        /// </summary>
        public decimal UnitCargoFee { get; set; }


        /// <summary>
        /// Komisyon Tutarı
        /// </summary>
        public decimal UnitCommissionAmount { get; set; }


        /// <summary>
        /// Kdv hariç liste satış fiyatı.
        /// </summary>
        public decimal VatExcListUnitPrice { get => this.ListUnitPrice / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim fiyatı.
        /// </summary>
        public decimal VatExcUnitPrice { get => this.UnitPrice / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim indirimi.
        /// </summary>
        public decimal VatExcUnitDiscount { get => this.UnitDiscount / (1 + Convert.ToDecimal(this.VatRate)); }

        /// <summary>
        /// Kdv hariç birim maliyeti.
        /// </summary>
        public decimal VatExcUnitCost { get => this.UnitCost / (1 + Convert.ToDecimal(this.VatRate)); }


        /// <summary>
        /// Sipariş detayının oluşturulduğu tarih.
        /// </summary>
        public DateTime CreatedDate { get; set; }


        public string TypeName { get; set; }
        public int Type { get; set; }

        /// <summary>
        /// İade Fatura Numarası
        /// </summary>
        public string ReturnNumber { get; set; }


        /// <summary>
        /// İade Fatura sebebi
        /// </summary>
        public string ReturnDescription { get; set; }

        public DateTime? ReturnDate { get; set; }

        public string ReturnDateStr => ReturnDate.HasValue ? ReturnDate.Value.ToString("dd-MM-yyyy HH:mm:ss") :"";

        #endregion

        #region Navigation Property
        /// <summary>
        /// İlgili sipariş.
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// İlgili ürün.
        /// </summary>
        public ProductInformation ProductInformation { get; set; }

        public MarketplaceCommission MarketplaceCommission { get; set; }
        #endregion
    }
}
