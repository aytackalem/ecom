﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class EntireDiscountSetting
    {
        #region Properties
        public int Id { get; set; }

        public decimal Limit { get; set; }

        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }
        #endregion
    }
}
