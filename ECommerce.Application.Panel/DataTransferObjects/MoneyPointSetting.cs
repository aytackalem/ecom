﻿namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MoneyPointSetting
    {
        #region Properties
        public int Id { get; set; }

        public decimal Limit { get; set; }

        public bool EarningIsRate { get; set; }

        public decimal Earning { get; set; }
        #endregion
    }
}
