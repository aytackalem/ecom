﻿using ECommerce.Application.Common.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class Company
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public bool Selected { get; set; }

        /// <summary>
        /// Ikon dosya adı.
        /// </summary>
        public string FileName { get; set; }
        #endregion

        #region Navigation Properties
        public CompanySetting CompanySetting { get; set; }

        public CompanyContact CompanyContact { get; set; }

        public List<CompanyConfiguration> CompanyConfigurations { get; set; }
        #endregion
    }
}
