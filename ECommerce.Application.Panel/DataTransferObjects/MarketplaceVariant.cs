﻿using System.Collections.Generic;

namespace ECommerce.Application.Panel.DataTransferObjects
{
    public class MarketplaceVariant
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public string MarketplaceId { get; set; }

        public bool Mandatory { get; set; }

        public bool AllowCustom { get; set; }

        public bool MultiValue { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceVariantValue> MarketplaceVariantValues { get; set; }
        #endregion
    }
}
