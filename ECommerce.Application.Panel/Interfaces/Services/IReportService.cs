﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Parameters.Reports;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IReportService
    {
        #region Methods
        DataResponse<HeaderReport> ReadHeaderReport(ReportRequest request);

        DataResponse<ExpenseReport> ReadExpenseReport(ReportRequest request);

        DataResponse<OrderReport> ReadDetailedOrderReport(ReportRequest request);

        DataResponse<List<OrderProductReport>> ReadOrderProductReport(ReportRequest request);

        DataResponse<List<ProductBasedSalesReport>> ReadProductBasedSalesReport(ReportRequest request);

        DataResponse<List<KeyValue<string, int>>> ReadPackagingReport(PackagingReportRequest request);

        DataResponse<List<SoldTogetherReport>> ReadSoldTogetherReport(SoldTogetherReportRequest request);

        DataResponse<List<ProductBasedStockReport>> ReadProductBasedStockReport(ProductBasedStockReportRequest request);

        DataResponse<PickedReport> ReadPickedReport(PickedReportRequest request);

        DataResponse<ColorBasedSalesReport> ReadColorBasedSalesReport(ColorBasedSalesReportRequest request);

        DataResponse<List<SalesPerformance>> ReadSalesPerformance();

        PagedResponse<UnsoldProduct> ReadUnsoldProduct(PagedRequest pagedRequest);

        DataResponse<List<StockAlert>> StockAlert();
        #endregion
    }
}
