﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Notification;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects.Notifications;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface INotificationService
    {
        #region Methods
        Task<PagedResponse<NotificationItem>> ReadAsync(GetParameter parameter);

        Task<Response> Delete(string marketplaceRequestTypeId, string id);

        Task<Response> Ignore(int productInformationMarketplaceId);

        Task<PagedResponse<NotificationItem>> Read(PagedRequest pagedRequest);

        DataResponse<List<KeyValue<string, string>>> NotificationTypes();

        DataResponse<NotificationItemCount> Count();
        #endregion
    }
}
