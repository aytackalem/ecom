﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceCategoryService
    {
        #region Methods
        DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(string marketplaceId, int take, string q);
        #endregion
    }
}
