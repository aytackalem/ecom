﻿using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ILanguageService : IKeyValueReadableService<string, string>
    {
    }
}
