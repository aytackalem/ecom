﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductInformationMarketplaceService
    {
        #region Methods
        Task<Response> UpsertAsync(List<ProductInformationMarketplace> productInformationMarketplaces);

        Task<DataResponse<List<ProductInformationMarketplace>>> ReadOrGenerateAsync(List<ProductInformation> productInformations, List<ProductMarketplace> productMarketplaces, List<Marketplace> marketplaces, bool accountingPrice, int categoryId);
        #endregion
    }
}
