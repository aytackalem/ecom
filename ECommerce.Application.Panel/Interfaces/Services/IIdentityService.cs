﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IIdentityService
    {
        Task<Response> LoginAsync(string username, string password, bool keepLoggedIn);

        Task<Response> LogoutAsync();

        Task<DataResponse<Identity>> GetIdentity();
    }
}
