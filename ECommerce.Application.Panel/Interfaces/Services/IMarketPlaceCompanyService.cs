﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceCompanyService : IReadableService<int, MarketplaceCompany>
    {
        #region Methods
        Task<DataResponse<List<MarketplaceCompany>>> ReadAsync();

        Response Update(MarketplaceCompany dto);
        #endregion
    }
}
