﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductSourceDomainService : IKeyValueReadableService<int, string>, IReadableService<int, ProductSourceDomain>
    {
        Response Update(ProductSourceDomain dto);
    }
}
