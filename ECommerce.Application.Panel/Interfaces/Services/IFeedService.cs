﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IFeedService : IReadableService<int, Feed>
    {
        #region Methods
        Response Update(Feed dto);

        Response Create(Feed dto);
        #endregion
    }
}
