﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceService : IKeyValueReadableService<string, string>
    {
        #region Methods
        DataResponse<List<Marketplace>> Read();

        DataResponse<List<Marketplace>> InfiniteReadCategories();
        #endregion
    }
}
