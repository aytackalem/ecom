﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IFrequentlyAskedQuestionService : ICreatableService<FrequentlyAskedQuestion>, IReadableService<int, FrequentlyAskedQuestion>, IKeyValueReadableService<int, string>
    {
        #region Methods

        Response Delete(FrequentlyAskedQuestion dto);
        #endregion
    }
}
