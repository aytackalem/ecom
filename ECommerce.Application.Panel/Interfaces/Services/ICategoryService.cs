﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ICategoryService : ICreatableService<Category>, IReadableService<int, Category>, IKeyValueReadableService<int, string>
    {
        #region Methods
        DataResponse<List<Category>> ReadByParentCategoryId(int? parentCategoryId);
        #endregion
    }
}
