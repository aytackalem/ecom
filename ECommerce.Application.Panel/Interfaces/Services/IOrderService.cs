﻿using ECommerce.Application.Common.DataTransferObjects.Widgets;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using ECommerce.Application.Panel.Parameters.Orders;
using ECommerce.Application.Panel.ViewModels.Orders;
using System.Collections.Generic;
using System.IO;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IOrderService : IReadableService<int, Order>, IKeyValueReadableService<int, string>
    {
        #region Methods
        Response Update(Order dto);

        DataResponse<Summary> Summary();

        Response Create(Order dto);

        DataResponse<MemoryStream> Download(Parameters.Orders.FilterRequest filterRequest);

        DataResponse<MemoryStream> Pdf(string ids);
        DataResponse<MemoryStream> BulkOrderPdf(string search);

        DataResponse<MemoryStream> MutualBarcode(int id);

        DataResponse<int> Convert(ConvertRequest convertRequest);

        Response MakeShipped(List<int> ids);

        /// <summary>
        /// Müşteri adı, satın alınan ve iade edilen ürünlere ilişkin detayları dönen metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<Order> Detail(int id);

        Response Return(Order order);

        /// <summary>
        /// Siparişin içindeki sepet ve paketleme silinir.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Response DeletePicking(Order order);

        Response DeleteInvoice(Order order);

        Response DeleteReturnInvoice(Order order);

        Response EmptyPackerBarcode(Order order);

        PagedResponse<OrderDetail> BulkOrderRead(PagedRequest pagedRequest);
        #endregion
    }
}
