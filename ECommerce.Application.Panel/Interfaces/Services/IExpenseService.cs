﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IExpenseService : IReadableService<int, Expense>
    {
        #region Methods
        Response Update(Expense dto);

        Response Create(Expense dto);
        #endregion
    }
}
