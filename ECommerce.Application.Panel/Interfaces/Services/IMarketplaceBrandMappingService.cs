﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Parameters.MarketplaceBrandMappings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceBrandMappingService
    {
        #region Methods
        Task<DataResponse<List<MarketplaceBrandMapping>>> ReadOrGenerateAsync(int brandId, List<Marketplace> marketplaces);

        Response Upsert(UpsertRequest upsertRequest);
        #endregion
    }
}
