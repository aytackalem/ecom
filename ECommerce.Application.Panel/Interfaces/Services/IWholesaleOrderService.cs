﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services;

public interface IWholesaleOrderService : IReadableService<int, WholesaleOrder>
{
    DataResponse<int> Create(WholesaleOrder wholesaleOrder);

    Response Update(int id, string wholesaleOrderTypeId);
}
