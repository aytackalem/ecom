﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IBrandDiscountSettingService : ICreatableService<BrandDiscountSetting>, IReadableService<int, BrandDiscountSetting>
    {
        Response Delete(BrandDiscountSetting dto);
    }
}
