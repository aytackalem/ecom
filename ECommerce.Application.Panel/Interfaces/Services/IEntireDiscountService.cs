﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IEntireDiscountService : ICreatableService<EntireDiscount>, IReadableService<int, EntireDiscount>
    {
        Response Delete(EntireDiscount dto);
    }
}
