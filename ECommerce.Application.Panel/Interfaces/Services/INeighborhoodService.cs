﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface INeighborhoodService
    {
        #region Methods

        DataResponse<List<Neighborhood>> ReadByDistrictId(int districtId);
        #endregion
    }
}
