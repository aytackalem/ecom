﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ISmsProviderCompanyService :  IReadableService<int, SmsProviderCompany>
    {
        #region Methods
        Response Update(SmsProviderCompany dto);
        #endregion
    }
}
