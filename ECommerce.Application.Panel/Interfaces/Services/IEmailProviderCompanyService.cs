﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IEmailProviderCompanyService :  IReadableService<int, EmailProviderCompany>
    {
        #region Methods
        Response Update(EmailProviderCompany dto);
        #endregion
    }
}
