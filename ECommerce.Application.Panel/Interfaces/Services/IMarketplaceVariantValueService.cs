﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceVariantValueService
    {
        #region Methods
        DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceVariantId);

        DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceVariantId, int take, string q);

        DataResponse<List<MarketplaceVariantValue>> Read(string marketplaceCategoryId, string marketplaceVariantId, int take, string q);
        #endregion
    }
}
