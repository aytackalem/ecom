﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceVariantMappingService
    {
        #region Methods
        public Task<DataResponse<List<MarketplaceVariantValueMapping>>> GetMappedVariantValueAsync(List<int> variantValueIds);

        Response Upsert(Parameters.MarketplaceVariantMappings.UpsertRequest upsertRequest);
        #endregion
    }
}
