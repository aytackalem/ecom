﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceBrandService
    {
        #region Methods
        DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(string marketplaceId, string q);
        #endregion
    }
}
