﻿using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IBankService : ICreatableService<Bank>, IReadableService<string, Bank>
    {
    }
}
