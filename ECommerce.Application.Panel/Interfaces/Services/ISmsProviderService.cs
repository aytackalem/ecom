﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ISmsProviderService : ICreatableService<SmsProvider>, IReadableService<string, SmsProvider>
    {
    }
}
