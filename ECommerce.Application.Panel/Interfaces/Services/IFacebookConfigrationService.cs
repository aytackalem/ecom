﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IFacebookConfigrationService : ICreatableService<FacebookConfigration>, IReadableService<int, FacebookConfigration>, IKeyValueReadableService<int, string>
    {
        #region Methods
        #endregion
    }
}
