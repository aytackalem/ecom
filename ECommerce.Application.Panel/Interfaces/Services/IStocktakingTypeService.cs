﻿using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IStocktakingTypeService : IKeyValueReadableService<string, string>
    {
    }
}
