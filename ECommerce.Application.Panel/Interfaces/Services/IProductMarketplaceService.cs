﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductMarketplaceService
    {
        #region Methods
        Task<Response> UpsertAsync(List<ProductMarketplace> productMarketplaces);

        Task<DataResponse<List<ProductMarketplace>>> ReadOrGenerateAsync(Product product, List<Marketplace> marketplaces);
        #endregion
    }
}
