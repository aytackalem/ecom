﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplacePriceService
    {

        #region Methods
        PagedResponse<ProductInformation> Read(PagedRequest pagedRequest);

        /// <summary>
        /// Müşterinin çalıştığı pazaryerlini döner
        /// </summary>
        /// <returns></returns>
        DataResponse<List<KeyValue<string, string>>> MarketplaceCompanyPrices();

        /// <summary>
        /// Enumdan gelen aksiyon alınacak değerleri döner.
        /// Yüzde (%) Fiyat Ekle
        /// Yüzde (%) Fiyat Çıkar
        /// Sabit Fiyat Ekle
        /// </summary>
        /// <returns></returns>
        DataResponse<List<KeyValue<int, string>>> ActionPrices();

        /// <summary>
        /// Enumdan gelen fiyat küsüratlarını döner.
        /// Yuvarlama
        /// Yukarı Yuvarla
        /// Aşağı Yuvarla
        /// </summary>
        /// <returns></returns>
        DataResponse<List<KeyValue<int, string>>> FractionPrices();

        /// <summary>
        /// ProductInformationMarketplaceBulkPrice Tablosuna kayıt atılır. 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Response Create(ProductMarketplaceBulkPrice dto);

        /// <summary>
        /// ProductInformationMarketplaceBulkPrice kayıt atılan data taşınarak ProductInformationMarketplaces tablosunda güncelleme yapılır.
        /// </summary>
        /// <returns></returns>
        Response BulkUpdate();

        Response BulkDelete();

        Response Delete(int id);

        DataResponse<List<KeyValue<string, int>>> MarketplaceUpdatePrices();


        DataResponse<List<KeyValue<int, string>>> AccountingPrices();

        /// <summary>
        /// Erp/Xml için Fiyat güncellensin mi datasını döner.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<MarketplaceAccountingPrice>> MarketplaceAccountingPrices(string search);

        /// <summary>
        /// ProductInformationMarketplaces tablosunda AccountingPrice kolununa bulk update yapılır.
        /// </summary>
        /// <returns></returns>
        Response BulkAccountingUpdate(ProductMarketplaceBulkAccountingPrice dto);

        /// <summary>
        /// ProductInformationMarketplaces tablosunda DisabledUpdateProduct kolununa bulk update yapılır.
        /// </summary>
        /// <returns></returns>
        Response BulkProductMarketplaceUpdatePrice(ProductMarketplaceBulkProductPrice dto);

        DataResponse<List<KeyValue<int, string>>> ProductUpdatePrices();

        DataResponse<List<MarketplaceUpdateProductPrice>> MarketplaceUpdateProductPrices(string search);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        DataResponse<MemoryStream> DownloadMarketplace(string search);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        Response UpdateBulkMarketplace(IFormFile formFile);
        #endregion
    }
}
