﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IPropertyValueService : ICreatableService<PropertyValue>
    {
        #region Methods
        DataResponse<List<PropertyValue>> Read(int productPropertyId);
        #endregion
    }
}
