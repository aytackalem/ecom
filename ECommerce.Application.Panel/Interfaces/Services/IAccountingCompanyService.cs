﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IAccountingCompanyService: IReadableService<int, AccountingCompany>
    {
        Response Update(AccountingCompany dto);
    }
}
