﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ICityService
    {
        #region Methods
        DataResponse<List<City>> ReadByCountryId(int countryId);
        #endregion
    }
}
