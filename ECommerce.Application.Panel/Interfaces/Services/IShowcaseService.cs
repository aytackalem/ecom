﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IShowcaseService : IReadableService<int, Showcase>, IKeyValueReadableService<int, string>
    {
        Response Create(Showcase dto);

        Response Update(Showcase dto);
    }
}
