﻿using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ISupplierService : ICreatableService<Supplier>, IReadableService<int, Supplier>, IKeyValueReadableService<int, string>
    {
    }
}
