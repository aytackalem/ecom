﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IShipmentCompanyCompanyService : IReadableService<int, ShipmentCompanyCompany>
    {
        #region Methods
        Response Update(ShipmentCompanyCompany dto);
        #endregion
    }
}
