﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Parameters.Customers;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ICustomerService
    {
        #region Methods
        DataResponse<List<Customer>> Search(SearchRequest searchRequest);

        DataResponse<Order> Read(int id);
        #endregion
    }
}
