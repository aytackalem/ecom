﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ICategoryDiscountSettingService : ICreatableService<CategoryDiscountSetting>, IReadableService<int, CategoryDiscountSetting>
    {
        Response Delete(CategoryDiscountSetting dto);
    }
}
