﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IVariantValueService : ICreatableService<VariantValue>
    {
        #region Methods
        DataResponse<List<VariantValue>> Read(int variantId);
        #endregion
    }
}
