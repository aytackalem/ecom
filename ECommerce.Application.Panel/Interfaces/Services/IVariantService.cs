﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IVariantService : ICreatableService<Variant>, IReadableService<int, Variant>
    {
        #region Methods
        DataResponse<List<Variant>> Read();

        Task<DataResponse<Variant>> ReadAsync(int id);
        #endregion
    }
}
