﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IGetXPayYSettingService : ICreatableService<GetXPayYSetting>, IReadableService<int, GetXPayYSetting>
    {
        Response Delete(GetXPayYSetting dto);
    }
}
