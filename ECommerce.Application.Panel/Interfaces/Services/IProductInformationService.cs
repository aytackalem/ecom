﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductInformationService : IKeyValueReadableService<int, string>
    {
        #region Methods
        DataResponse<List<KeyValue<int, ProductInformation>>> ReadAsKeyProductInformation(int take, string q);

        DataResponse<List<ProductInformation>> ReadPrice(int productId);

        Response UpdatePrice(List<ProductInformation> productInformations);

        DataResponse<List<ProductInformation>> Read(List<int> ids);

        Response UpdateCategorization(string sku, string categorization);
        #endregion
    }
}
