﻿using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IBrandService : ICreatableService<Brand>, IReadableService<int, Brand>, IKeyValueReadableService<int, string>
    {
    }
}
