﻿using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Panel.Interfaces.Services.Base
{
    public interface ICreatableService<TEntity>
    {
        #region Methods
        DataResponse<TEntity> Create(TEntity dto);

        Response Update(TEntity dto);
        #endregion
    }
}
