﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services.Base
{
    public interface IKeyValueReadableService<TKey, TValue>
    {
        #region Methods
        DataResponse<List<KeyValue<TKey, TValue>>> ReadAsKeyValue();
        #endregion
    }
}
