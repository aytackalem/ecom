﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Application.Panel.Interfaces.Services.Base
{
    public interface IReadableService<TKey, TEntity>
    {
        #region Methods
        DataResponse<TEntity> Read(TKey id);

        PagedResponse<TEntity> Read(PagedRequest pagedRequest);
        #endregion
    }
}
