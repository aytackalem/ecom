﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IPosConfigurationService : ICreatableService<PosConfiguration>
    {
        #region Methods
        DataResponse<List<PosConfiguration>> Read(string posId);
        #endregion
    }
}
