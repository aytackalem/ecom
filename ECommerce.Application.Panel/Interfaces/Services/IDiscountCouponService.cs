﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IDiscountCouponService : ICreatableService<DiscountCoupon>, IReadableService<int, DiscountCoupon>
    {
    }
}
