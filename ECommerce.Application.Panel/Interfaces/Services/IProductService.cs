﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using ECommerce.Application.Panel.Parameters.Products;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductService : ICreatableService<Product>, IReadableService<int, Product>, IKeyValueReadableService<int, string>
    {
        #region Methods
        /// <summary>
        /// Ürün, ürün dil bilgisi, ürün detay, ürün detay dil bilgisi, ürün detay varyasyon, varyasyon ve varyasyon dil bilgilerini içerir.
        /// </summary>
        /// <param name="id">Ürüne ait id bilgisi.</param>
        /// <returns></returns>
        Task<DataResponse<Product>> ReadAsync(int id);

        DataResponse<List<ProductInformationVariantValue>> ReadAsKeyValueById(int id);

        /// <summary>
        /// Aldığı ürün varyasyon ve varyasyon değerleri ile birlikte kombinasyonu hazırlayan metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<ProductInformation>> GenerateProductInformations(ProductInformationPartialRequest productInformationPartialRequest);

        /// <summary>
        /// İlgili ürün içerisindeki pazaryeri kayıtlarını dönen metod.
        /// </summary>
        /// <param name="id">Ürüne ait id bilgisi.</param>
        /// <param name="marketPlaces"></param>
        /// <returns></returns>
        DataResponse<List<ProductInformation>> GenerateProductInformationMarketplaces(int id, List<Marketplace> marketPlaces);

        DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue(int take, string q);

        /// <summary>
        /// Vitrin (Showcase) için hazırlanmış, kullanıcılara gösterilecek ürün bilgilerini dönen metod. Fiyat, isim, etiket ve fotoğraf bilgilerini içerir.
        /// </summary>
        /// <param name="id">Ürün id bilgisi.</param>
        DataResponse<Domain.Entities.Product> ReadInfo(int id);

        /// <summary>
        /// Ürünlere ait pazaryeri ayarlarını kaydeden metod.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        Response UpsertProductMarketplace(Product product);

        DataResponse<MemoryStream> Download(Parameters.BulkPriceUpdates.FilterRequest filterRequest);

        DataResponse<MemoryStream> DownloadMarketplace();

        Response UpdateBulkPrice(IFormFile formFile);

        Response UpdateBulkMarketplace(IFormFile formFile);

        DataResponse<long> MaxBarcode();

        DataResponse<int> MaxStockCode();

        /// <summary>
        /// Ürünleri product tabloundan hidden olarak günceller ve productinformationmarketplace tablosundan kayıtlarını siler
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response Deleted(int id);

        DataResponse<string> ReadSellerCode(int id);

        DataResponse<Product> Read(string sellerCode);

        DataResponse<List<Last12MonthSale>> Last12MonthSales(List<string> list);

        DataResponse<List<Last12MonthSale>> Last36MonthSales(string skuCode);

        DataResponse<List<Last12MonthSale>> Last36MonthSales(int id);

        /// <summary>
        /// Ürün içerisindeki tüm bedenleri getirir.
        /// </summary>
        /// <returns></returns>
        DataResponse<List<string>> ReadAllSizes(int id);

        /// <summary>
        /// Ürün içerisindeki tüm renkleri kodları ile birlikte getirir. Key değeri renk kodunu temsil eder.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DataResponse<Dictionary<string,string>> ReadAllColors(int id);
        #endregion
    }
}
