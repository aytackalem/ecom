﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IShoppingCartDiscountedProductService : ICreatableService<ShoppingCartDiscountedProduct>, IReadableService<int, ShoppingCartDiscountedProduct>
    {
        Response Delete(ShoppingCartDiscountedProduct dto);
    }
}
