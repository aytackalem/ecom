﻿using ECommerce.Application.Panel.Interfaces.Services.Base;
using System;

namespace ECommerce.Application.Panel.Interfaces.Services;

public interface IWholesaleOrderTypeService : IKeyValueReadableService<String, String>
{
}
