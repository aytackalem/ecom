﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Parameters.MarketplaceCategoryMappings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceCategoryMappingService
    {
        #region Methods
        public DataResponse<List<MarketplaceCategoryMapping>> Read(int categoryId);

        public Task<DataResponse<List<MarketplaceCategoryMapping>>> GetMappedAsync();

        Response Upsert(UpsertRequest upsertRequest);
        #endregion
    }
}
