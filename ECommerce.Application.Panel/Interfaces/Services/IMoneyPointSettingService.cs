﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMoneyPointSettingService : ICreatableService<MoneyPointSetting>
    {
        #region Methods
        DataResponse<MoneyPointSetting> Read();
        #endregion
    }
}
