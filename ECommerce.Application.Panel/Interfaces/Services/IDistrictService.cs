﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IDistrictService
    {
        #region Methods
        DataResponse<List<District>> ReadByCityId(int cityId);
        #endregion
    }
}
