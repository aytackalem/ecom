﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ICompanyService : ICreatableService<Company>, IReadableService<int, Company>
    {
        #region Methods
        Response Update(Company dto);

        DataResponse<Company> Read();
        #endregion
    }
}
