﻿using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IReceiptService : ICreatableService<Receipt>
    {
        PagedResponse<Receipt> Read(PagedRequest pagedRequest);

        DataResponse<Receipt> ReadDetail(int id);

        Response UpdateDelivered(int id, bool delivered);

        Response UpdateCancelled(int id);

        DataResponse<List<ReceiptProductInformationQuantity>> ProductInformationQuantity(int productId);
    }
}
