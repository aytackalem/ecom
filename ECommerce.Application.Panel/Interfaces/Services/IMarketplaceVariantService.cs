﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IMarketplaceVariantService
    {
        #region Methods
        public DataResponse<List<MarketplaceVariant>> Read();

        public DataResponse<List<MarketplaceVariant>> ReadMappings();

        public DataResponse<List<MarketplaceVariant>> Read(string marketplaceCategoryId);
        #endregion
    }
}
