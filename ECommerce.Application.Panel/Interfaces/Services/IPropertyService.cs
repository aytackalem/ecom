﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IPropertyService : ICreatableService<Property>, IReadableService<int, Property>
    {
        #region Methods
        DataResponse<List<Property>> Read();

        Response Delete(Property dto);
        #endregion
    }
}
