﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface ILabelService : ICreatableService<Label>, IReadableService<int, Label>, IKeyValueReadableService<int, string>
    {
        Response Delete(Label dto);
    }
}
