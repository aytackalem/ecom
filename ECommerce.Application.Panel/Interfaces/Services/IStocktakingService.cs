﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Application.Panel.Interfaces.Services.Base;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IStocktakingService : ICreatableService<Stocktaking>, IReadableService<int, Stocktaking>
    {
        Response Approve(Stocktaking dto);

        Response Delete(Stocktaking dto);
    }
}
