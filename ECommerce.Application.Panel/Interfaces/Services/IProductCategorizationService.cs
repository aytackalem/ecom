﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IProductCategorizationService
    {
        DataResponse<List<string>> Read();
    }
}
