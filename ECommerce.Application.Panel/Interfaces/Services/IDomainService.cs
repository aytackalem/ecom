﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services
{
    public interface IDomainService
    {
        #region Methods
        DataResponse<List<Application.Panel.DataTransferObjects.Domain>> ReadAll();


        DataResponse<List<Company>> Companies();
        #endregion
    }
}
