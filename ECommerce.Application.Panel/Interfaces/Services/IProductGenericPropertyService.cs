﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Panel.Interfaces.Services;

public interface IProductGenericPropertyService
{
    #region Methods
    DataResponse<List<string>> ReadAsValue(string name);
    #endregion
}
