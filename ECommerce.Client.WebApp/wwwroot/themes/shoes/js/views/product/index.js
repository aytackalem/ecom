﻿var ProductIndex = (function (context) {
    var productIndex = {};

    productIndex.ProductInformationCommentCreate = function (productInformationId) {
        debugger;
        var commentValid = context.Validator.Validate("commentForm", "warning", "invalid-message");

        context.Network.PostJson("/ProductInformationComment/Create", {
            productInformationId: productInformationId,
            fullName: document.getElementById("name").value,
            comment: document.getElementById("comment").value,
        }, function (response) {

            if (response.success && commentValid) {
                $("#preloader-active").fadeOut("slow");
                $("#preloader-active").fadeOut("slow", function () {
                    $('#commentForm')[0].reset();
                    swal({
                        title: "Başarılı",
                        text: "Yorumunuz başarılı şekilde iletilmiştir.",
                        button: "Tamam"
                    });
                });
            } else {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: response.message,
                        button: "Tamam"
                    });
                });
            }



        });
    };

    productIndex.ProductInformationSubscriptionCreate = function (productInformationId) {

        var phoneNumber = $(`#stockModal-${productInformationId}`).find('[name="stockPhone"]').eq(0).val();

        if (phoneNumber == "") {
            document.querySelector(".invalid-message").style.display = "block";
            document.querySelector(".stock-icon-text").style.display = "none"
        } else {
            document.querySelector(".invalid-message").style.display = "none";
            document.querySelector(".stock-icon-text").style.display = "block"
            context.Network.PostJson("/ProductInformationSubscription/Create", {
                productInformationId: productInformationId,
                phoneNumber: phoneNumber,
            }, function (response) {

                if (response.success) {
                    $("#preloader-active").fadeOut("slow");

                    $(`#stockModal-${productInformationId}`).find('[name="stockPhone"]').eq(0).val('');

                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Başarılı",
                            text: "Ürün tekrar stoklarımıza ulaştığında sms aracılığıyla bilgilendirileceksiniz.",
                            button: "Tamam"
                        });
                    });

                    $(`#stockModal-${productInformationId}`).modal('hide');
                } else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }

            });
        }

    };

    productIndex.GetVariants = function (productInformationId) {

   context.Network.Get("/Product/IndexPartial?ProductInformationId=" + productInformationId, function (response) {
       $('#productPartial').html(response);
       $('.product-image-slider').slick({
           slidesToShow: 1,
           slidesToScroll: 1,
           dots: true,
           prevArrow: false,
           nextArrow: false,
           fade: false,
           asNavFor: '.slider-nav-thumbnails',
       });
       $('.slider-nav-thumbnails').slick({
           slidesToShow: 4,
           slidesToScroll: 1,
           asNavFor: '.product-image-slider',
           dots: false,
           prevArrow: false,
           nextArrow: false,
           focusOnSelect: true,

       });
       ///*Zoom*/
       if ($(window).width() > 969) {
           $('.pd-img-figure').on('mouseover', function () {
               $(this).children('.product-img--main__image').css({ 'transform': 'scale(' + $(this).attr('data-scale') + ')' });
           })
               .on('mouseout', function () {
                   $(this).children('.product-img--main__image').css({ 'transform': 'scale(1)' });
               })
               .on('mousemove', function (e) {
                   $(this).children('.product-img--main__image').css({ 'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 + '%' });
               }).each(function () {
                   $(this).append('<div class="product-img--main__image"></div>').children('.product-img--main__image').css({ 'background-image': 'url(' + $(this).attr('data-image') + ')' });
               });
       } else {
            $("#lightgallery").lightGallery();
       }
       ///*Zoom*/
    
          
       // slick light gallery
       $('.product-image-slider').lightGallery({
           selector: '.slick-slide:not(.slick-cloned) .lightgallery',

       });

        });

 
    };

    return productIndex;
})(Context);
