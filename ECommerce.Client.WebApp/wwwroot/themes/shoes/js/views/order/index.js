﻿var OrderIndex = (function (context) {
    var orderIndex = {};

    var _tabNamePrefix = null;

    var _tabsCount = null;

    orderIndex.Initialize = function (tabNamePrefix, tabsCount) {
        _tabNamePrefix = tabNamePrefix;
        _tabsCount = tabsCount;

        context.Tab.Initialize(_tabNamePrefix, _tabsCount);
    }

    orderIndex.TabNavigate = function (tabIndex, isLogin) {
        window.scrollTo(0, 0);
        if (tabIndex == 1) {
            
            if (isLogin) {
                if (!$('#default-customer-address-id').val()) {
                    swal({
                        title: "Uyarı",
                        text: "Ödeme adımına geçmek için adres bilgisi seçmeniz gerekmektedir.",
                        button: "Tamam"
                    });
                    return;
                }

                if (document.getElementById("differentaddress").checked && !$('#default-customer-invoice-address-id').val()) {

                    swal({
                        title: "Uyarı",
                        text: "Ödeme adımına geçmek için fatura adres bilgisi seçmeniz gerekmektedir.",
                        button: "Tamam"
                    });
                    return;
                }

                context.Tab.Navigate(tabIndex);
            }
            else {
                var valid = context.Validator.Validate("customer-information-container", "warning", "invalid-message");
                var invoValid = context.Validator.Validate("ship-invoice", "warning", "invalid-message")
                if (!document.getElementById("differentaddress").checked) {
                    if (valid) {
                        context.Tab.Navigate(tabIndex);
                    }
                } else {
                    if (valid && invoValid) {
                        context.Tab.Navigate(tabIndex);
                    }
                }
            }





        }

    };

    orderIndex.Create = function (isLogin) {
        

        var paymentTypeId = "";
        var paymentTypeElements = document.getElementsByName("payment-type");
        for (var i = 0; i < paymentTypeElements.length; i++) {
            var paymentTypeElement = paymentTypeElements[i];
            if (paymentTypeElement.checked) {
                paymentTypeId = paymentTypeElement.value;
                break;
            }
        }
        var valid = context.Validator.Validate("customer-information-container", "warning", "invalid-message")
            && (!document.getElementById("differentaddress").checked
                || (document.getElementById("differentaddress").checked
                    && context.Validator.Validate("customer-information-container", "warning", "invalid-message")))
            && (paymentTypeId !== "OO" || (paymentTypeId === "OO" && context.Validator.Validate("credit-card-container", "warning", "invalid-message")));

        if (valid) {

            var payments = [];
            var paymentTypeId = null;
            var elements = context.Selector.SelectByName("payment-type");
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                if (element.checked) {
                    paymentTypeId = element.value;
                }
            }

            var creditCard = null;
            if (paymentTypeId == "OO") {
                creditCard = {
                    Name: $('#cc-name').val(),
                    Number: $('#cc-number').val(),
                    ExpireMonth: $('#cc-expire-month').val(),
                    ExpireYear: $('#cc-expire-year').val(),
                    Cvv: $('#cc-cvv').val()
                };
            }
            payments.push({
                PaymentTypeId: paymentTypeId,
                CreditCard: creditCard
            });
            if ($('#moneyPointCheck').prop('checked')) {
                payments.push({
                    PaymentTypeId: "MP",
                    Amount: parseFloat($('#moneyPoint-amount').val())
                });
            }

            var differentaddress = document.getElementById("differentaddress").checked;

            var customerAddresId = 0;
            var customerInvoiceInformationId = 0;
            var orderDeliveryAddress = {};
            var orderInvoiceInformation = {};
            if (isLogin) {
                customerAddresId = $('#default-customer-address-id').val();

                if (differentaddress) {
                    customerInvoiceInformationId = $('#default-customer-invoice-address-id').val();
                }
            }
            else {
                orderDeliveryAddress = {
                    Recipient: `${context.Selector.SelectById("name").value} ${context.Selector.SelectById("surname").value}`,
                    FirstName: context.Selector.SelectById("name").value,
                    LastName: context.Selector.SelectById("surname").value,
                    NeighborhoodId: context.Selector.SelectById("neighborhood-id").value,
                    Address: context.Selector.SelectById("billing_address").value,
                    PhoneNumber: context.Selector.SelectById("phone").value,
                    Mail: context.Selector.SelectById("mail").value
                };

                if (differentaddress) {

                    orderInvoiceInformation = {
                        FirstName: context.Selector.SelectById("invoice-firstName").value,
                        LastName: context.Selector.SelectById("invoice-lastName").value,
                        NeighborhoodId: context.Selector.SelectById("invoice-neighborhood-id").value,
                        Address: context.Selector.SelectById("invoice-address").value,
                        Mail: context.Selector.SelectById("invoice-mail").value,
                        Phone: context.Selector.SelectById("invoice-phone").value,
                        TaxOffice: context.Selector.SelectById("invoice-taxOffice").value,
                        TaxNumber: context.Selector.SelectById("invoice-taxNumber").value
                    };
                }
            }

            

            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Order/Create", {
                    Payments: payments,
                    OrderNote: {
                        Note: context.Selector.SelectById("order-note").value
                    },
                    Differentaddress: differentaddress,
                    CustomerAddresId: customerAddresId,
                    CustomerInvoiceInformationId: customerInvoiceInformationId,
                    OrderDeliveryAddress: orderDeliveryAddress,
                    OrderInvoiceInformation: orderInvoiceInformation
                }, function (response) {
                    
                    if (response.success) {
                        $("#preloader-active").fadeOut("slow", function () {

                            if (response.isOnlinePayment) {
                                if (response.isThreeD) {
                                    
                                    var iFrame = $('<iframe>');

                                    var div3d = $('#div-3d');
                                    var div3dModalBody = $('#div-3d .modal-body');
                                    div3dModalBody.append(iFrame);

                                    debugger;

                                    if (!response.allowFrame) {
                                        window.document.write(response.threeDResponse);
                                    }
                                    else if (response.threeDRedirect) {
                                        iFrame.attr('src', response.threeDResponse);
                                        //window.location.href = response.threeDResponse;
                                    }
                                    else {
                                        iFrame.attr('src', 'about:blank');
                                        var iFrameDocument = iFrame[0].contentWindow.document;
                                        iFrameDocument.open();
                                        iFrameDocument.write(response.threeDResponse);
                                        iFrameDocument.close();
                                        //document.write(response.threeDResponse);
                                    }

                                    div3d.modal('show');
                                    div3d.on('hidden.bs.modal', function () {

                                    });

                                    return;
                                }
                            }

                            window.location.href = '/Order/Done';

                        });
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });


        }

    };

    orderIndex.CheckMoneyPoint = function () {

        var moneyPointAmount = parseFloat($('#moneyPoint-amount').val());
        var totalMoneyAmount = parseFloat($('#total-moneyAmount').val());


        if (isNaN(moneyPointAmount) || (moneyPointAmount > totalMoneyAmount)) {
            swal({
                title: "Uyarı",
                text: `Maksimum bonus tutarı ${totalMoneyAmount} TL olmalıdır.`,
                button: "Tamam"
            });
            $('#moneyPoint-amount').val(totalMoneyAmount);
        }

        OrderIndex.UseMoneyPoint();


    }

    orderIndex.UseMoneyPoint = function () {
        
        var totalAmount = parseFloat($('#total-amount').data('totalamount'));
        if ($('#moneyPointCheck').prop('checked')) {
            var moneyPointAmount = parseFloat($('#moneyPoint-amount').val());
            if (moneyPointAmount >= totalAmount) {
                totalAmount = moneyPointAmount;
            }

            totalAmount -= moneyPointAmount;
            $('#moneyPoint-useAmount').text(`- ${moneyPointAmount.toFixed(2)} ₺`);
            $('#moneyPoint-use-div').removeClass('display-none');

        }
        else {
            $('#moneyPoint-use-div').addClass('display-none');
        }
        $('#total-amount').text(`${totalAmount.toFixed(2)} ₺`)

    }




    return orderIndex;
})(Context);

