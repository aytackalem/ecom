﻿


var CustomerAddress = (function (context) {
    var customerAddress = {};

    customerAddress.Get = function () {
        debugger
        context.Network.Get(`/Account/CustomerAddressesPartial`, function (response) {
            debugger
            $("#customer-address").html(response);

        });
    };

    customerAddress.OpenUpdateModal = function (id) {

        context.Network.Get(`/Account/CustomerAddressDetailPartial?id=${id}`, function (response) {

            $(".modal_addres_body").html(response);
            $("#addres-update-modal").modal();
        });
    };

    customerAddress.Upsert = function () {

        var customerAddressValid = context.Validator.Validate("customer-address-form", "warning", "invalid-message");
        var customerAddressId = document.getElementById("customer-address-id").value;
        var customerAddressTitle = document.getElementById("customer-address-title").value;
        var customerAddressPhoneNumber = document.getElementById("customer-address-phone").value;
        var customerAddressRecipient = document.getElementById("customer-address-recipient").value;
        var customerAddressAddres = document.getElementById("customer-address-address").value;
        var neighborhoodId = context.Selector.SelectById("neighborhood-id").value;
        if (customerAddressValid) {

            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/CustomerAddressUpsert", {
                    id: customerAddressId,
                    recipient: customerAddressRecipient,
                    title: customerAddressTitle,
                    phone: customerAddressPhoneNumber,
                    NeighborhoodId: neighborhoodId,
                    address: customerAddressAddres
                }, function (response) {

                    if (response.success) {
                        customerAddress.Get();
                        $("#preloader-active").fadeOut("slow", function () {

                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                            $(`.addres-update-modal`).modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                        });

                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    customerAddress.Delete = function (id) {
        swal("Uyarı", "Adresi silmek istiyor musunuz?", {
            buttons: {
                confirmButtonText: "Evet",
                cancel: "Hayır"

            },
        }).then(
            function (isConfirm) {
                if (isConfirm) {

                    $("#preloader-active").fadeIn("slow", function () {
                        context.Network.PostJson("/Account/CustomerAddressRemove", {
                            id: id
                        }, function (response) {

                            if (response.success) {
                                customerAddress.Get();
                                $("#preloader-active").fadeOut("slow", function () {

                                    swal({
                                        title: "Başarılı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });

                            }
                            else {
                                $("#preloader-active").fadeOut("slow", function () {
                                    swal({
                                        title: "Uyarı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });
                            }
                        });
                    });

                }

            },
        );
    };

    customerAddress.Default = function (id) {

        if (id == $('#default-customer-address-id').val()) {
            return;
        }

        $("#preloader-active").fadeIn("slow", function () {
            context.Network.PostJson("/Account/CustomerAddressDefault", {
                id: id
            }, function (response) {

                if (response.success) {
                    //customerAddress.Get();
                    $("#preloader-active").fadeOut("slow", function () {


                        var title = $(`#customer-adress-title_${id}`).text();
                        var address = $(`#customer-adress_adress_${id}`).text();

                        $('#default-customer-address-id').val(id);
                        $('#default-customer-address-title').text(title);
                        $('#default-customer-address-address').text(address);
                        $('.activeCustomerAdressIcon').hide();
                        $('#activeCustomerAdressIcon_' + id).css("display", "block")

                        swal({
                            title: "Başarılı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });

                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            });
        });



    };

    customerAddress.GetDistricts = function (countryId) {

        var element = context.Selector.SelectById("district-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var cityId = context.Selector.SelectById("city-id").value;


        context.Network.GetJson("/District/Read?CityId=" + cityId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }

                if (countryId && countryId > 1) {
                    $('#district-id').find('option:eq(1)').prop('selected', true);
                    customerAddress.GetNeighborhoods(countryId);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });


    };

    customerAddress.GetNeighborhoods = function (countryId) {

        var element = context.Selector.SelectById("neighborhood-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var districtId = context.Selector.SelectById("district-id").value;


        context.Network.GetJson("/Neighborhood/Read?DistrictId=" + districtId, function (response) {
            if (response.success) {


                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
                if (countryId && countryId > 1) {
                    $('#neighborhood-id').find('option:eq(1)').prop('selected', true);
                }
            }
            else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    };

    return customerAddress;
})(Context);



