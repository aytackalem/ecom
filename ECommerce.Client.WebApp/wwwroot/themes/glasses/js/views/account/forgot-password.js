﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var AccountForgotPassword = (function (context) {
    var accountForgotPassword = {};

    accountForgotPassword.Open = function () {
        context.Network.Get("/Account/ForgotPassword", function (response) {
            context.Modal.Show("", response);
        });
    };

    accountForgotPassword.Submit = function () {




        if (context.Validator.Validate("forgot-password-form", "warning", "invalid-message")) {
            if (captchaResponse == null) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Lütfen captcha'yı doğrulayınız.",
                        button: "Tamam"
                    });
                });
                return;
            }
            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/ForgotPassword", {
                    captchaResponse: captchaResponse,
                    data: {
                        username: context.Selector.SelectByName("username")[0].value
                    }
                }, function (response) {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);

                    if (response.success) {

                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            }).then((result) => {

                                window.location.href = "/";
                            })

                        });

                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                }, function () {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);
                });
            });
        }

    };

    return accountForgotPassword;
})(Context);