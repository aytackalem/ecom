﻿
var CategoryIndex = (function (context) {
    var categoryIndex = {};
    var _url = null;

    window.scrollTo(0, 145);
    categoryIndex.Initialize = function (url) {
        _url = url;
    }

    categoryIndex.Filter = function (pageItems) {
        var propertyValueIds = [];
        debugger
        var elements = context.Selector.SelectByName("property-value");
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (element.checked) {
                propertyValueIds.push(element.value);
            }
        }
        var propertyValueUrl = "";
        if (propertyValueIds.length > 0) {
            propertyValueUrl = "/" + propertyValueIds.join("_") + "-pv";
        }
     

        window.location.href = _url + propertyValueUrl +"?" + pageItems;
    };

    return categoryIndex;
})(Context);