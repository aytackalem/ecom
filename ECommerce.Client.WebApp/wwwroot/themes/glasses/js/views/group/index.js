﻿"use strict";

var GroupIndex = (function (context) {
    var groupIndex = {};

    var _url = null;

    groupIndex.Initialize = function (url) {
        _url = url;
    }

    groupIndex.Filter = function (pageItems) {
        var propertyValueIds = [];
        var elements = context.Selector.SelectByName("property-value");
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (element.checked) {
                propertyValueIds.push(element.value);
            }
        }
        var propertyValueUrl = "";
        if (propertyValueIds.length > 0) {
            propertyValueUrl = "/" + propertyValueIds.join("_") + "-pv"
        }

        window.location.href = _url + propertyValueUrl + "?" + pageItems;
    };

    return groupIndex;
})(Context);