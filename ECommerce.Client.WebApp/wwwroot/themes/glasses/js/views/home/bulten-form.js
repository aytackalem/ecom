﻿
var BultenSave = (function (context) {
    var bultenSave = {};

    bultenSave.save = function (e) {

        var bultenValid = context.Validator.Validate("bultenForm", "warning", "invalid-message");
        var bultenMailInput = document.getElementById("bulten-mail-input").value
        var bultenPattern = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
        var kvkkCheck = document.getElementById("bulten-kvkk").checked

        if (bultenValid && bultenPattern.test(bultenMailInput)) {
            e.preventDefault();
            debugger
            if (kvkkCheck == false) {
                swal({
                    title: "Uyarı",
                    text: "Kvkk onaylayınız",
                    button: "Tamam"
                });
            } else {
                $("#preloader-active").fadeIn("slow", function () {
                    context.Network.PostJson("/Home/NewBulletin", {
                        mail: bultenMailInput,

                    }, function (response) {
                        if (response.success) {
                            $("#preloader-active").fadeOut("slow", function () {
                                $('#bultenForm')[0].reset();
                                swal({
                                    title: "Başarılı",
                                    text: response.message,
                                    button: "Tamam"
                                });
                            });
                        }
                        else {
                            $("#preloader-active").fadeOut("slow", function () {
                                swal({
                                    title: "Uyarı",
                                    text: response.message,
                                    button: "Tamam"
                                });
                            });
                        }
                    });
                });
            }
          
        } 
    };

    return bultenSave;
})(Context);