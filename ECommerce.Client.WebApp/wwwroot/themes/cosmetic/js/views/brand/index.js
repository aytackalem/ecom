﻿"use strict";

var BrandIndex = (function (context) {
    var brandIndex = {};

    var _url = null;

    brandIndex.Initialize = function (url) {
        _url = url;
    }

    brandIndex.Filter = function (pageItems) {
        var propertyValueIds = [];
        var elements = context.Selector.SelectByName("property-value");
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (element.checked) {
                propertyValueIds.push(element.value);
            }
        }
        var propertyValueUrl = "";
        if (propertyValueIds.length > 0) {
            propertyValueUrl = "/" + propertyValueIds.join("_") + "-pv"
        }
        window.location.href = _url + propertyValueUrl + "?" + pageItems;
    };

    return brandIndex;
})(Context);