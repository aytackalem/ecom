﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var AccountLogin = (function (context) {
    var accountLogin = {};
    accountLogin.Open = function () {
        context.Network.Get("/Account/Login", function (response) {
            context.Modal.Show("", response);
        });
    };
    accountLogin.Login = function () {

 

        var loginValid = context.Validator.Validate("login-form", "warning", "invalid-message");
        if (loginValid) {
            if (captchaResponse == null) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Lütfen captcha'yı doğrulayınız.",
                        button: "Tamam"
                    });
                });
                return;
            }
            var mailPattern = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
            var registerMailResponse;
            mailPattern.test($("#username").val()) ? registerMailResponse = true : registerMailResponse = false;
            if (registerMailResponse == false) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Lütfen mail adresinizi doğru giriniz.",
                        button: "Tamam"
                    });
                });
                return;
            }

            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/Login", {
                    captchaResponse: captchaResponse,
                    data: {
                        username: context.Selector.SelectByName("username")[0].value,
                        password: context.Selector.SelectByName("password")[0].value
                    }
                }, function (response) {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);

                    if (response.success) {
                        window.location.href = '/Account';
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                }, function () {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);
                });

            });
        }

    };

    return accountLogin;
})(Context);