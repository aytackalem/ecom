﻿


var CustomerAddress = (function (context) {
    var customerAddress = {};

    customerAddress.Get = function () {

        context.Network.Get(`/Account/CustomerAddressesPartial`, function (response) {
       
            $("#customer-address").html(response);

        });
    };

    customerAddress.OpenUpdateModal = function (id) {

        context.Network.Get(`/Account/CustomerAddressDetailPartial?id=${id}`, function (response) {

            $(".modal_addres_body").html(response);
            $("#addres-update-modal").modal();
        });
    };

    customerAddress.Upsert = function () {

        var customerAddressValid = context.Validator.Validate("customer-address-form", "warning", "invalid-message");
        var customerAddressId = document.getElementById("customer-address-id").value;
        var customerAddressTitle = document.getElementById("customer-address-title").value;
        var customerAddressPhoneNumber = document.getElementById("customer-address-phone").value;
        var customerAddressRecipient = document.getElementById("customer-address-recipient").value;
        var customerAddressAddres = document.getElementById("customer-address-address").value;
        var neighborhoodId = context.Selector.SelectById("neighborhood-id").value;
        if (customerAddressValid) {

            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/CustomerAddressUpsert", {
                    id: customerAddressId,
                    recipient: customerAddressRecipient,
                    title: customerAddressTitle,
                    phone: customerAddressPhoneNumber,
                    NeighborhoodId: neighborhoodId,
                    address: customerAddressAddres
                }, function (response) {

                    if (response.success) {
                        customerAddress.Get();
                        $("#preloader-active").fadeOut("slow", function () {

                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                            $(`.addres-update-modal`).modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                        });

                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    customerAddress.Delete = function (id) {
        swal("Uyarı", "Adresi silmek istiyor musunuz?", {
            buttons: {
                confirmButtonText: "Evet",
                cancel: "Hayır"

            },
        }).then(
            function (isConfirm) {
                if (isConfirm) {

                    $("#preloader-active").fadeIn("slow", function () {
                        context.Network.PostJson("/Account/CustomerAddressRemove", {
                            id: id
                        }, function (response) {

                            if (response.success) {
                                customerAddress.Get();
                                $("#preloader-active").fadeOut("slow", function () {

                                    swal({
                                        title: "Başarılı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });

                            }
                            else {
                                $("#preloader-active").fadeOut("slow", function () {
                                    swal({
                                        title: "Uyarı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });
                            }
                        });
                    });

                }

            },
        );
    };

    customerAddress.Default = function (id) {

        if (id == $('#default-customer-address-id').val()) {
            return;
        }

        $("#preloader-active").fadeIn("slow", function () {
            context.Network.PostJson("/Account/CustomerAddressDefault", {
                id: id
            }, function (response) {

                if (response.success) {
                    //customerAddress.Get();
                    $("#preloader-active").fadeOut("slow", function () {


                        var title = $(`#customer-adress-title_${id}`).text();
                        var address = $(`#customer-adress_adress_${id}`).text();

                        $('#default-customer-address-id').val(id);
                        $('#default-customer-address-title').text(title);
                        $('#default-customer-address-address').text(address);
                        $('.activeCustomerAdressIcon').hide();
                        $('#activeCustomerAdressIcon_' + id).css("display", "block")

                        swal({
                            title: "Başarılı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });

                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            });
        });



    };

    customerAddress.GetDistricts = function () {

        var element = context.Selector.SelectById("district-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var cityId = context.Selector.SelectById("city-id").value;


        context.Network.GetJson("/District/Read?CityId=" + cityId, function (response) {
            if (response.success) {

                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            } else {
                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });


    };

    customerAddress.GetNeighborhoods = function () {
        var element = context.Selector.SelectById("neighborhood-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var districtId = context.Selector.SelectById("district-id").value;


        context.Network.GetJson("/Neighborhood/Read?DistrictId=" + districtId, function (response) {
            if (response.success) {


                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            }
            else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    };

    customerAddress.Phonecontrol = function () {
             const customerOrderAdres = document.getElementById("customer-address-phone");
            const customerInvoicePhone = document.getElementById("customer-invoice-address-phone");

            if (customerInvoicePhone != undefined) {
                customerInvoicePhone.addEventListener('input', handleInput)
            }

            if (customerOrderAdres != undefined) {

                customerOrderAdres.addEventListener('input', handleInput)
            }
            function handleInput(e) {

                var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
                e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
                if (e.target.value[0] == "0") {
                    this.value = "";
                }
            }
    }

    return customerAddress;
})(Context);



