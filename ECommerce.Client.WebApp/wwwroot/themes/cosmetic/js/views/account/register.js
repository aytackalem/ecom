﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var AccountRegister = (function (context) {
    var accountRegister = {};

    accountRegister.Open = function () {
        context.Network.Get("/Account/Register", function (response) {
            context.Modal.Show("", response);
        });
    };

    accountRegister.Register = function (event) {

        var mailPattern = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
        var registerMailResponse;
        mailPattern.test($(".registerMail").val()) ? registerMailResponse = true : registerMailResponse = false;
        
      


        var registerValid = context.Validator.Validate("register-form", "warning", "invalid-message");
        var registerContract = document.getElementById("registerPrivacy").checked;
        var password = document.getElementById("rpassword").value;
        var passwordAgain = document.getElementById("rpasswordAgain").value;
        if ($(event).attr('currentTarget').id == "modal-trymail-form-button") { //Tekrar mail gönderimi yapılması için mailcode boş göndermemiz yeterli olacaktır.
            $('#modalMailCode').val('');
        }
        debugger
        if (captchaResponse == null) {
            $("#preloader-active").fadeOut("slow", function () {
                swal({
                    title: "Uyarı",
                    text: "Lütfen captcha'yı doğrulayınız.",
                    button: "Tamam"
                });
            });
            return;
        }

        if (registerMailResponse == false) {
            $("#preloader-active").fadeOut("slow", function () {
                swal({
                    title: "Uyarı",
                    text: "Lütfen mail adresinizi doğru giriniz.",
                    button: "Tamam"
                });
            });
            return;
        }

        if (registerValid && registerContract) {
            if (password != passwordAgain) {
                swal({
                    title: "Uyarı",
                    text: "Şifreler aynı değil.",
                    button: "Tamam",
                    timer: 4000,
                });
            } else {


                $("#preloader-active").fadeIn("slow", function () {
                    context.Network.PostJson("/Account/Register", {
                        captchaResponse: captchaResponse,
                        data: {
                            mailCode: context.Selector.SelectByName("modalMailCode")[0].value,
                            username: context.Selector.SelectByName("username")[0].value,
                            password: context.Selector.SelectByName("password")[0].value,
                            Customer: {
                                name: context.Selector.SelectByName("customerName")[0].value,
                                surname: context.Selector.SelectByName("customerSurname")[0].value,
                                customerContact: {
                                    phone: context.Selector.SelectByName("customerContactPhone")[0].value,
                                    mail: context.Selector.SelectByName("username")[0].value
                                }
                            }
                        }
                    }, function (response) {


                        if (response.success) {
                            debugger
                            if (response.data.verified) {
                                $("#preloader-active").fadeOut("slow", function () {
                                    window.location.href = '/Account/Login'
                                    return;
                                });
                            }
                            else if (response.data.mailModal) {
                                $("#preloader-active").fadeOut("slow", function () {
                                    $('#mailModal').modal('show');                                    
                                });
                                return
                            }
                        }
                        else {
                            $("#preloader-active").fadeOut("slow", function () {
                                captchaResponse = null;
                                grecaptcha.reset(captchaId);
                                $('#modalMailCode').val('');
                                swal({
                                    title: "Uyarı",
                                    text: response.message,
                                    button: "Tamam"
                                });
                            });
                        }
                    }, function () {
                        captchaResponse = null;
                        grecaptcha.reset(captchaId);
                    });
                });

            }

        } else if (!registerContract) {
            swal({
                title: "Uyarı",
                text: "Üyelik sözleşmesini okuyup, onaylayınız.",
                button: "Tamam",
                timer: 4000,
            });
        }
    };

    accountRegister.CheckVerification = function (event) {



        var modalTryMailButton = document.getElementById("modal-trymail-form-button");
        var modalMailButton = document.getElementById("modal-mail-form-button");

        if ($(event).attr('currentTarget').id == "modal-trymail-form-button") { //Tekrar mail gönderimi yapılması için mailcode boş göndermemiz yeterli olacaktır.
            $('#modalMailCode').val('');
        }

        if ($(event).attr('currentTarget').id == "modal-mail-form-button") {
            context.Validator.Validate("modal-mail-form", "warning", "invalid-message");
            if ($('#modalMailCode').val() == '')
                return;
        }


        $(modalTryMailButton).prop('disabled', true);
        $(modalMailButton).prop('disabled', true);
        $("#preloader-active").fadeIn("slow", function () {
            context.Network.PostJson("/Account/UserVerification", {

                mailCode: context.Selector.SelectByName("modalMailCode")[0].value,
                username: context.Selector.SelectByName("username")[0].value

            }, function (response) {


                $(modalTryMailButton).prop('disabled', false);
                $(modalMailButton).prop('disabled', false);

                if (response.success) {

                    if (response.data.mailVerified) {
                        $("#preloader-active").fadeOut("slow", function () {
                            window.location.href = '/Account/Login'
                            return;
                        });
                    }
                    else if (response.data.mailModal) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#mailModal').modal('show');
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                        return
                    }
                    else if (!response.data.mailModal) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#mailModal').modal('hide');
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                        return
                    }
                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        $('#modalMailCode').val('');
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            }, function () {
                captchaResponse = null;
                grecaptcha.reset(captchaId);
            });
        });




    };

    return accountRegister;
})(Context);



