﻿var AccountLogout = (function (context) {
    var accountLogout = {};

    accountLogout.Open = function (token) {
        context.Network.Get("/Account/Logout?rpd=" + token, function (response) {
            context.Modal.Show("", response);
        });
    };

    accountLogout.Close = function () {

        context.Network.PostJson("/Account/Logout", null, function (response) {
            debugger
            if (response.success) {
                window.location.href = '/';
            }
        });

    };

    return accountLogout;
})(Context);