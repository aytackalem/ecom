﻿var AccountMember = (function (context) {
    var accountMember = {};
    accountMember.Submit = function () {

        var loginValid = context.Validator.Validate("member-update-form", "warning", "invalid-message");


        if (loginValid) {
            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/MemberUpdate", {
                    name: context.Selector.SelectByName("customer-name")[0].value,
                    surname: context.Selector.SelectByName("customer-surname")[0].value,
                    customerContact: {
                        phone: context.Selector.SelectByName("customer-customercustomerAddress-phone")[0].value
                    }
                }, function (response) {
                    if (response.success) {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };
    accountMember.ChangePassword = function () {
        var loginValid = context.Validator.Validate("member-change-password-form", "warning", "invalid-message");
        if (loginValid) {
            var newPassword = context.Selector.SelectByName("customer-new-password")[0].value;
            var newPasswordAgain = context.Selector.SelectByName("customer-new-password-again")[0].value;
            if (newPassword != newPasswordAgain) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Şifreler eşleşmemektedir lütfen kontrol ediniz",
                        button: "Tamam"
                    });
                });
                return;
            }
            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/MemberChangePassword", {
                    newPassword: newPassword
                }, function (response) {
                    if (response.success) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#member-change-password-form')[0].reset();
                            $('#member-account-Modal').modal('hide');
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });

        }
    };



    return accountMember;
})(Context);