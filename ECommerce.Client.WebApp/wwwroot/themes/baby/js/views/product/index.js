﻿var ProductIndex = (function (context) {
    var productIndex = {};
    productIndex.ProductInformationCommentCreate = function (productInformationId) {
        debugger;
        var commentValid = context.Validator.Validate("commentForm", "warning", "invalid-message");
        if (commentValid) {
            debugger
            var rateStar = parseInt($('#rate-star').val());
            if (rateStar < 1) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Ürün puanlaması 0'dan büyük olamamlıdır. Lütfen bir ürün puanlaması giriniz.",
                        button: "Tamam"
                    });
                });
                return;
            }

            context.Network.PostJson("/ProductInformationComment/Create", {
                productInformationId: productInformationId,
                fullName: document.getElementById("name").value,
                comment: document.getElementById("comment").value,
                rating: rateStar
            }, function (response) {

                if (response.success) {
                    $("#preloader-active").fadeOut("slow");
                    $("#preloader-active").fadeOut("slow", function () {
                        $('#commentForm')[0].reset();
                        swal({
                            title: "Başarılı",
                            text: "Yorumunuz başarılı şekilde iletilmiştir.",
                            button: "Tamam"
                        });
                    });
                } else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }



            });
        }
    };
    productIndex.ProductInformationSubscriptionCreate = function (productInformationId) {

        var phoneNumber = $(`#stockModal-${productInformationId}`).find('[name="stockPhone"]').eq(0).val();

        if (phoneNumber == "") {
            document.querySelector(".invalid-message").style.display = "block";
            document.querySelector(".stock-icon-text").style.display = "none"
        } else {
            document.querySelector(".invalid-message").style.display = "none";
            document.querySelector(".stock-icon-text").style.display = "block"
            context.Network.PostJson("/ProductInformationSubscription/Create", {
                productInformationId: productInformationId,
                phoneNumber: phoneNumber,
            }, function (response) {

                if (response.success) {
                    $("#preloader-active").fadeOut("slow");

                    $(`#stockModal-${productInformationId}`).find('[name="stockPhone"]').eq(0).val('');

                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Başarılı",
                            text: "Ürün tekrar stoklarımıza ulaştığında sms aracılığıyla bilgilendirileceksiniz.",
                            button: "Tamam"
                        });
                    });

                    $(`#stockModal-${productInformationId}`).modal('hide');
                } else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }

            });
        }

    };
    productIndex.GetVariants = function (productInformationId) {
        debugger
        context.Network.Get("/Product/IndexPartial?ProductInformationId=" + productInformationId, function (response) {
            $('#productPartial').html(response);
            $('.product-image-slider').slick({
                dots: false,
                arrows: false

            });
        });


    };
    productIndex.RateStar = function (rate) {
        debugger
        $('#rate-star').val(rate);
    };

    return productIndex;
})(Context);
