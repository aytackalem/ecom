﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var ContactSave = (function (context) {
    var contactSave = {};

    contactSave.save = function () {

       

        var contactValid = context.Validator.Validate("contact-form", "warning", "invalid-message");
        var contactNameInput = document.getElementById("contact-name-input").value;
        var contactMailInput = document.getElementById("contact-mail-input").value;
        var contactPhoneNumberInput = document.getElementById("contact-phoneNumber-input").value;
        var contactSubjectInput = document.getElementById("contact-subject-input").value;
        var contactMessageInput = document.getElementById("contact-message-input").value;
        if (contactValid) {
            if (captchaResponse == null) {
                $("#preloader-active").fadeOut("slow", function () {
                    swal({
                        title: "Uyarı",
                        text: "Lütfen captcha'yı doğrulayınız.",
                        button: "Tamam"
                    });
                });
                return;
            }
            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Home/Contact", {
                    captchaResponse: captchaResponse,
                    data: {
                        name: contactNameInput,
                        phoneNumber: contactPhoneNumberInput,
                        mail: contactMailInput,
                        subject: contactSubjectInput,
                        message: contactMessageInput
                    }

                }, function (response) {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);

                    if (response.success) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#contact-form')[0].reset();
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    return contactSave;
})(Context);