(function ($) {
    ("use strict");
/*     Page loading*/
    $(window).on("load", function () {
        $("#preloader-active").delay(450).fadeOut("slow");
        $("body").delay(450).css({
            overflow: "visible"
        });
        $("#onloadModal").modal("show");
    });

    //Category Search NavBar
    $('.searchBar-categorySelect').change(function () {
        window.location = $(':selected', this).attr('value')
    });
    //Category Search NavBar

    //ScrollUp 
    $.scrollUp({
        scrollText: '<i class="fi-rs-arrow-small-up"></i>',
        easingType: "linear",
        scrollSpeed: 900,
        animation: "fade"
    });


    //sidebar sticky
    if ($(".sticky-sidebar").length) {
        $(".sticky-sidebar").theiaStickySidebar();
    }

    // Slider Range JS
    if ($("#slider-range").length) {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [130, 250],
            slide: function (event, ui) {
                $("#amount").val(ui.values[0] + "₺ -" + ui.values[1] + " ₺");
            }
        });
        $("#amount").val($("#slider-range").slider("values", 0) + "₺ -" + $("#slider-range").slider("values", 1) + "₺");
    }

    /*slider 1*/
    $(".single-hero-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        loop: true,
        dots: true,
        arrows: true,
        prevArrow: '<span class="slider-btn slider-prev"><i class="fi-rs-angle-left"></i></span>',
        nextArrow: '<span class="slider-btn slider-next"><i class="fi-rs-angle-right"></i></span>',
        appendArrows: ".hero-slider-arrow",
        autoplay: true
    });

    /*Category Slider*/
    $(".categorySlider-columns").each(function (key, item) {
        var id = $(this).attr("id");
        var sliderID = "#" + id;
        var appendArrowsClassName = "#" + id + "-arrows";

        $(sliderID).slick({
            dots: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            prevArrow: '<span class="slider-btn slider-prev"><i class="fi-rs-arrow-small-left"></i></span>',
            nextArrow: '<span class="slider-btn slider-next"><i class="fi-rs-arrow-small-right"></i></span>',
            appendArrows: appendArrowsClassName,
        });
    });

    /*Product Slider*/
    $(".productSlider-columns").each(function (key, item) {
        var id = $(this).attr("id");
        var sliderID = "#" + id;
        var appendArrowsClassName = "#" + id + "-arrows";

        $(sliderID).slick({
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 3,
            loop: true,
            autoplay: false,
            autoplaySpeed:1500,
            prevArrow: '<span class="slider-btn slider-prev"><i class="fi-rs-arrow-small-left"></i></span>',
            nextArrow: '<span class="slider-btn slider-next"><i class="fi-rs-arrow-small-right"></i></span>',
            appendArrows: appendArrowsClassName,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        margin:0,
                    }
                }
            ],

        });
    });
    /*Product Slider*/

    /*Fix Bootstrap 5 tab & slick slider*/

    $('button[data-bs-toggle="tab"]').on("shown.bs.tab", function (e) {
        $(".productSlider-columns").slick("setPosition");
    });

    /*------ Timer Countdown ----*/

    $("[data-countdown]").each(function () {
        var $this = $(this),
            finalDate = $(this).data("countdown");
        $this.countdown(finalDate, function (event) {
            $(this).html(event.strftime("" + '<span class="countdown-section"><span class="countdown-amount hover-up">%D</span><span class="countdown-period"> Gün </span></span>' + '<span class="countdown-section"><span class="countdown-amount hover-up">%H</span><span class="countdown-period"> Saat </span></span>' + '<span class="countdown-section"><span class="countdown-amount hover-up">%M</span><span class="countdown-period"> Dak </span></span>' + '<span class="countdown-section"><span class="countdown-amount hover-up">%S</span><span class="countdown-period"> Sn </span></span>'));
        });
    });

    /*Category toggle function*/
    var searchToggle = $(".categories-button-active");
    searchToggle.on("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            $(this).siblings(".categories-dropdown-active-large").removeClass("open");
        } else {
            $(this).addClass("open");
            $(this).siblings(".categories-dropdown-active-large").addClass("open");
        }
    });

    /* Price range filter */
    var sliderrange = $("#slider-range");
    var amountprice = $("#amount");
    $(function () {
        sliderrange.slider({
            range: true,
            min: 16,
            max: 400,
            values: [0, 300],
            slide: function (event, ui) {
                amountprice.val(ui.values[0] + "₺ - " + ui.values[1] + "₺");
            }
        });
        amountprice.val(sliderrange.slider("values", 0) + "₺ - " + sliderrange.slider("values", 1) + "₺");
    });

    /*Sort active sıralama*/
    if ($(".sort-by-product-area").length) {
        var $body = $("body"),
            $cartWrap = $(".sort-by-product-area"),
            $cartContent = $cartWrap.find(".sort-by-dropdown");
        $cartWrap.on("click", ".sort-by-product-wrap", function (e) {
            e.preventDefault();
            var $this = $(this);
            if (!$this.parent().hasClass("show")) {
                $this.siblings(".sort-by-dropdown").addClass("show").parent().addClass("show");
            } else {
                $this.siblings(".sort-by-dropdown").removeClass("show").parent().removeClass("show");
            }
        });
        /*Close When Click Outside*/
        $body.on("click", function (e) {
            var $target = e.target;
            if (!$($target).is(".sort-by-product-area") && !$($target).parents().is(".sort-by-product-area") && $cartWrap.hasClass("show")) {
                $cartWrap.removeClass("show");
                $cartContent.removeClass("show");
            }
        });
    }

    /*Shop filter active  */
    $(".shop-filter-toogle").on("click", function (e) {
        e.preventDefault();
        $(".shop-product-fillter-header").slideToggle();
    });
    var shopFiltericon = $(".shop-filter-toogle");
    shopFiltericon.on("click", function () {
        $(".shop-filter-toogle").toggleClass("active");
    });


    /*Select active*/
    $(".select-active").select2();

    /*Checkout paymentMethod function*/
    paymentMethodChanged();
    function paymentMethodChanged() {
        var $order_review = $(".payment-method");
        $order_review.on("click", 'input[name="payment_method"]', function () {
            var selectedClass = "payment-selected";
            var parent = $(this).parents(".sin-payment").first();
            parent.addClass(selectedClass).siblings().removeClass(selectedClass);
        });
    }

    /*---- CounterUp ----*/
    //$(".count").counterUp({
    //    delay: 10,
    //    time: 2000
    //});

    /*SidebarSearch */
    function sidebarSearch() {
        var searchTrigger = $(".search-active"),
            endTriggersearch = $(".search-close"),
            container = $(".main-search-active");

        searchTrigger.on("click", function (e) {
            e.preventDefault();
            container.addClass("search-visible");
        });

        endTriggersearch.on("click", function () {
            container.removeClass("search-visible");
        });
    }
    sidebarSearch();

    /*Sidebar menu Active*/
    function mobileHeaderActive() {
 
        var navbarTrigger = $(".burger-icon"),
            endTrigger = $(".mobile-menu-close"),
            container = $(".mobile-header-active"),
            wrapper4 = $("body"),
            searchMobInput = $(".searchInputMob");

        wrapper4.prepend('<div class="body-overlay"></div>');

        navbarTrigger.on("click", function (e) {
            e.preventDefault();
            container.addClass("sidebar-visible");
            wrapper4.addClass("mobile-menu-active");
        });
       
        endTrigger.on("click", function () {
            container.removeClass("sidebar-visible");
            wrapper4.removeClass("mobile-menu-active");
        });

        $(".body-overlay").on("click", function () {
            container.removeClass("sidebar-visible");
            wrapper4.removeClass("mobile-menu-active");
        });
    }
    mobileHeaderActive();

    /*Mobile menu active*/
    var $offCanvasNav = $(".mobile-menu"),
        $offCanvasNavSubMenu = $offCanvasNav.find(".dropdown");
        $offCanvasNavSubMenuHead = $offCanvasNav.find(".dropdown-head");
    /*Add Toggle Button With Off Canvas Sub Menu*/

    $offCanvasNavSubMenu.parent().prepend('<span class="menu-expand "><i class="fi-rs-angle-small-down"></i></span>');
    $offCanvasNavSubMenuHead.parent().prepend('<span class="menu-expand head-expand"><i class="fi-rs-angle-small-up"></i></span>');
    $(".head-expand").next().remove();
 
    /*Close Off Canvas Sub Menu*/
    $offCanvasNavSubMenu.slideUp();
    $offCanvasNavSubMenuHead.slideDown();

    /*Category Sub Menu Toggle*/
    $offCanvasNav.on("click", "li a, li .menu-expand", function (e) {
        var $this = $(this);
        if (
            $this
                .parent()
                .attr("class")
                .match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/) &&
            ($this.attr("href") === "#" || $this.hasClass("menu-expand"))
        ) {
            e.preventDefault();
            if ($this.siblings("ul:visible").length) {
                $this.parent("li").removeClass("active");
                $this.siblings("ul").slideUp();
            } else {
                $this.parent("li").addClass("active");
                $this.closest("li").siblings("li").removeClass("active").find("li").removeClass("active");
                $this.closest("li").siblings("li").find("ul:visible").slideUp();
                $this.siblings("ul").slideDown();
            }
        }
    });

    /*--- language currency active ----*/
    $(".mobile-language-active").on("click", function (e) {
        e.preventDefault();
        $(".lang-dropdown-active").slideToggle(900);
    });

    /*--- categories-button-active-2 ----*/
    $(".categories-button-active-2").on("click", function (e) {
        e.preventDefault();
        $(".categori-dropdown-active-small").slideToggle(900);
    });

    /*--- Mobile demo active ----*/
    var demo = $(".tm-demo-options-wrapper");
    $(".view-demo-btn-active").on("click", function (e) {
        e.preventDefault();
        demo.toggleClass("demo-open");
    });


    /*-----Modal----*/

    $(".modal").on("shown.bs.modal", function (e) {
        $(".product-image-slider").slick("setPosition");
        $(".slider-nav-thumbnails").slick("setPosition");

    
    });

    /*Accordion*/
    var accordions = document.querySelectorAll(".accordion");
    accordions.forEach((acc) =>
        acc.addEventListener("click", () => {
            acc.classList.toggle("activeAcc");
            var panel = acc.nextElementSibling;
            panel.classList.toggle("activeP");
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        })
    );
    /*Accordion*/


    /*Payment Change Setting*/
    $(document).on('change', '.payment-type', function () {
  
        var paymentTypeCode = $(this).data('code');
        var cardInfo = $('.order-wizard-card-information');
        var bankInfo = $('.order-wizard-bank-information');
 
        switch (paymentTypeCode) {
            case 'OO':
                cardInfo.show();
                bankInfo.hide();
                break;
     
            case 'C':
                cardInfo.hide();
                bankInfo.show();

                break;
            default:
                cardInfo.hide();
                bankInfo.hide();
        }


    });
    /*Payment Change Setting*/

    /*Pagination*/
    $(document).ready(function () {
        var pageOneElement = $(".page-link").eq(0).attr("href");
        var url = window.location.href.split("?");
        if (url != pageOneElement) {
            $(".page-link").eq(0).addClass("page-link-active");
        }
        $(".page-link").each(function (e) {
            var activeItem = $(this).attr("href").split("?");
            if (url[1] == activeItem[1]) {
                $(".page-link").eq(0).removeClass("page-link-active");
                $(this).addClass("page-link-active");
            } 
        });
    })
    /*Pagination*/
    /*Mail Mask*/
    $(function () {
     
        var mailPattern = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
        $('.mail').on('keyup', function () {

            mailPattern.test($(this).val()) ? $('.invalid-message-mail').hide() : $('.invalid-message-mail').show();
        });

    });
    /*Mail Mask*/

    $(document).ready(function () {
        /*Product Spot Load More*/
        $(".pd-category-name").prepend("<span class='loadMore'>Devamını Oku</span>")
        $(".loadMore").click(function () {
            $(".product-spot").css({ "max-height": "fit-content", "overflow": "auto" });
            $(this).hide();
        })
        /*Product Spot Load More*/


        /*Copy Clipboard*/
        var iban = document.querySelector(".ibanNos");
        let copyButton = document.getElementById("ibanCopy");
        if (copyButton) {
            copyButton.addEventListener("click", () => {
                navigator.clipboard.writeText(iban.value).then(() => {
                    swal("Başarılı", `${iban.value} kopyalandı`, "success", {
                        button: "Tamam",
                        timer: 3000,
                    });
                })
            });
        }
   
        /*Copy Clipboard*/

    });

    /*Payment Type Money Point*/
    $("#moneyPointCheck").change(function () {
        if (this.checked) {
            $(".moneyPointInput").removeAttr("disabled");
        }
        if (!this.checked) {
            $(".moneyPointInput").prop("disabled", true);
        }
      
    });
    /*Payment Type Money Point*/


    /*Overlay Element*/
      $('.btn-filter-close').click(function () {
            $('.filter-overlay').fadeOut();
        });
      $('.filter-category-btn').click(function () {
      $('.filter-overlay').fadeIn();
      });

    $('.btn-search-close').click(function () {
        $('.search-overlay').fadeOut();
    });
    $(".header-mob-search-icon").click(function () {
        $('.search-overlay').fadeIn();
    });
    /*Overlay Element*/

    /*Order Adress Settings*/
    $(".registered-address-detail").hide();
    $(".registered-address-head").click(function () {
        $(".registered-address-detail").toggle();
    })

    $(".registered-invoice-address-head").click(function () {
        $(".registered-invoice-address-detail").toggle();
    })
    /*Order Adres Settings*/

    /*Customer Adres Style*/

    $.each($('.customerAdress'), function (index, span) {
        var str = $(span).text().trim();
        var n = str.length;
        if (n > 30)
            $(span).text(str.substr(0, 30) + '...');

    });

    /*Customer Adres Style*/
})(jQuery);

