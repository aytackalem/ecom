﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var AccountRegister = (function (context) {
    var accountRegister = {};

    accountRegister.Open = function () {
        context.Network.Get("/Account/Register", function (response) {
            context.Modal.Show("", response);
        });
    };

    accountRegister.Register = function (event) {




        var registerValid = context.Validator.Validate("register-form", "warning", "invalid-message");
        var registerContract = document.getElementById("registerPrivacy").checked;
        var password = document.getElementById("rpassword").value;
        var passwordAgain = document.getElementById("rpasswordAgain").value;
        if ($(event).attr('currentTarget').id == "modal-trysms-form-button") { //Tekrar sms gönderimi yapılması için smscode boş göndermemiz yeterli olacaktır.
            $('#modalSmsCode').val('');
        }


        if (captchaResponse == null) {
            $("#preloader-active").fadeOut("slow", function () {
                swal({
                    title: "Uyarı",
                    text: "Lütfen captcha'yı doğrulayınız.",
                    button: "Tamam"
                });
            });
            return;
        }

        if (registerValid && registerContract) {
            if (password != passwordAgain) {
                swal({
                    title: "Uyarı",
                    text: "Şifreler aynı değil.",
                    button: "Tamam",
                    timer: 4000,
                });
            } else {


                $("#preloader-active").fadeIn("slow", function () {
                    context.Network.PostJson("/Account/Register", {
                        captchaResponse: captchaResponse,
                        data: {
                            smsCode: context.Selector.SelectByName("modalSmsCode")[0].value,
                            username: context.Selector.SelectByName("username")[0].value,
                            password: context.Selector.SelectByName("password")[0].value,
                            Customer: {
                                name: context.Selector.SelectByName("customerName")[0].value,
                                surname: context.Selector.SelectByName("customerSurname")[0].value,
                                customerContact: {
                                    phone: context.Selector.SelectByName("customerContactPhone")[0].value,
                                    mail: context.Selector.SelectByName("customerContactMail")[0].value
                                }
                            }
                        }
                    }, function (response) {


                        if (response.success) {
                            if (response.data.verified) {
                                $("#preloader-active").fadeOut("slow", function () {
                                    window.location.href = '/Account/Login'
                                    return;
                                });
                            }
                            else if (response.data.smsModal) {
                                $("#preloader-active").fadeOut("slow", function () {
                                    $('#smsModal').modal('show');                                    
                                });
                                return
                            }
                        }
                        else {
                            $("#preloader-active").fadeOut("slow", function () {
                                captchaResponse = null;
                                grecaptcha.reset(captchaId);
                                $('#modalSmsCode').val('');
                                swal({
                                    title: "Uyarı",
                                    text: response.message,
                                    button: "Tamam"
                                });
                            });
                        }
                    }, function () {
                        captchaResponse = null;
                        grecaptcha.reset(captchaId);
                    });
                });

            }

        } else if (!registerContract) {
            swal({
                title: "Uyarı",
                text: "Üyelik sözleşmesini okuyup, onaylayınız.",
                button: "Tamam",
                timer: 4000,
            });
        }
    };

    accountRegister.CheckVerification = function (event) {



        var modalTrySmsButton = document.getElementById("modal-trysms-form-button");
        var modalSmsButton = document.getElementById("modal-sms-form-button");

        if ($(event).attr('currentTarget').id == "modal-trysms-form-button") { //Tekrar sms gönderimi yapılması için smscode boş göndermemiz yeterli olacaktır.
            $('#modalSmsCode').val('');
        }

        if ($(event).attr('currentTarget').id == "modal-sms-form-button") {
            context.Validator.Validate("modal-sms-form", "warning", "invalid-message");
            if ($('#modalSmsCode').val() == '')
                return;
        }


        $(modalTrySmsButton).prop('disabled', true);
        $(modalSmsButton).prop('disabled', true);
        $("#preloader-active").fadeIn("slow", function () {
            context.Network.PostJson("/Account/UserVerification", {

                smsCode: context.Selector.SelectByName("modalSmsCode")[0].value,
                username: context.Selector.SelectByName("username")[0].value

            }, function (response) {


                $(modalTrySmsButton).prop('disabled', false);
                $(modalSmsButton).prop('disabled', false);

                if (response.success) {

                    if (response.data.smsVerified) {
                        $("#preloader-active").fadeOut("slow", function () {
                            window.location.href = '/Account/Login'
                            return;
                        });
                    }
                    else if (response.data.smsModal) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#smsModal').modal('show');
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                        return
                    }
                    else if (!response.data.smsModal) {
                        $("#preloader-active").fadeOut("slow", function () {
                            $('#smsModal').modal('hide');
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                        return
                    }
                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        $('#modalSmsCode').val('');
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            }, function () {
                captchaResponse = null;
                grecaptcha.reset(captchaId);
            });
        });




    };

    return accountRegister;
})(Context);



