﻿var FindIndex = (function (context) {
    var findIndex = {};
    findIndex.Filter = function (pageItems,success) {
        var propertyValueIds = [];
        var elements = context.Selector.SelectByName("property-value");
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (element.checked) {
                propertyValueIds.push(element.value);
            }
        }
        var propertyValueUrl = "";
        if (propertyValueIds.length > 0) {
            propertyValueUrl = "/fidan-bul/" + propertyValueIds.join("_") + "-pv";
            window.location.href = propertyValueUrl + "?" + pageItems;
        } else {
            swal({
                title: "Uyarı",
                text: "Filtrelemek için en az 1 seçenekte bulunmalısınız",
                button: "Tamam",
                timer: 4000,
            });
        }
       
      

    };

    return findIndex;
})(Context);