﻿
var ShoppingCartIndex = (function (context) {
    var shoppingCartIndex = {};
    var _update = function (productInformationId, quantityElement, quantity, shoppingCartDiscount) {

        //quantityElement.innerHTML = quantity.toString();

        $("#preloader-active").fadeIn("slow", function () {
            context.ShoppingCart.Update(productInformationId, quantity, shoppingCartDiscount, function (response) {
                if (response.success) {
                    window.location.reload();
                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            });
        });
    }

    shoppingCartIndex.InitializeItem = function (productInformationId, shoppingCartDiscount) {
        var shoppingCartItemContainerElement = context.Selector.SelectById("shopping-cart-item-" + productInformationId);

        var shoppingCartItemElement = shoppingCartItemContainerElement.querySelector(".detail-qty");

        var quantityElement = shoppingCartItemElement.querySelector(".qty-val");
        quantityElement.addEventListener("change", function (e) {
            var quantity = parseInt(quantityElement.value);

            _update(productInformationId, quantityElement, quantity, shoppingCartDiscount);
        });

        var upElement = shoppingCartItemElement.querySelector(".qty-up");
        upElement.addEventListener("click", function (e) {
            var quantity = parseInt(quantityElement.value);

            quantity += 1;

            _update(productInformationId, quantityElement, quantity, shoppingCartDiscount);
        });

        var downElement = shoppingCartItemElement.querySelector(".qty-down");
        if (downElement) {
            downElement.addEventListener("click", function (e) {
                var quantity = parseInt(quantityElement.value);
                quantity -= 1;
                _update(productInformationId, quantityElement, quantity, shoppingCartDiscount);

            });
        }
    };

    shoppingCartIndex.Remove = function (productInformationId) {
        swal({
            title: "Uyarı",
            text: "Ürünü Silmek İstiyor musunuz?",
            buttons: ["Hayır", "Evet"]
        }).then((Delete) => {
            if (Delete) {
                $("#preloader-active").fadeIn("slow", function () {
                    context.ShoppingCart.Remove(productInformationId, function (response) {
                        if (response.success) {
                            window.location.reload();
                        }
                        else {
                            $("#preloader-active").fadeOut("slow", function () {
                                swal({
                                    title: "Uyarı",
                                    text: response.message,
                                    button: "Tamam"
                                });
                            });
                        }
                    });
                });
            } else {
                return false
            }
        });
    };

    shoppingCartIndex.DiscountCoupon = function () {
        if (context.Validator.Validate("discount-coupon-code-container", "warning", "invalid-message")) {
            $("#preloader-active").fadeIn("slow", function () {

                context.Network.PostJson("/DiscountCoupon/Read", {
                    code: document.getElementById("discount-coupon-code").value
                }, function (response) {
                    if (response.success) {
                        window.location.reload();
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    shoppingCartIndex.DiscountCouponClear = function () {
        if (context.Validator.Validate("discount-coupon-code-container", "warning", "invalid-message")) {
            $("#preloader-active").fadeIn("slow", function () {
                var url = "/DiscountCoupon/Clear"

                context.Network.GetJson(url, function (response) {
                    if (response.success) {
                        window.location.reload();
                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    return shoppingCartIndex;
})(Context);