(function ($) {

    /*Product Detail*/
    var productDetails = function () {
        $('.product-image-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            prevArrow: false,
            nextArrow: false,
            fade: false,
            asNavFor: '.slider-nav-thumbnails',
        });

        $('.slider-nav-thumbnails').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.product-image-slider',
            dots: false,
            prevArrow: false,
            nextArrow: false,
            focusOnSelect: true,
     
        });

        // anti aktif thumbn remove class 
        $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

        // active class 
        $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');


   
 
        //Filtre color - size - varyant(vb)
        //$('.list-filter').each(function () {
        //    $(this).find('a').on('click', function (event) {
        //        event.preventDefault();
        //        $(this).parent().siblings().removeClass('active');
        //        $(this).parent().toggleClass('active');
        //        $(this).parents('.attr-detail').find('.current-size').text($(this).text());
        //        $(this).parents('.attr-detail').find('.current-color').text($(this).attr('data-color'));
        //    });
        //});
        //quantity
        //$('.detail-qty').each(function () {
        //    var qtyval = parseInt($(this).find('.qty-val').text(), 10);
        //    $('.qty-up').on('click', function (event) {
        //        event.preventDefault();
        //        qtyval = qtyval + 1;
        //        $(this).prev().text(qtyval);
        //    });
        //    $('.qty-down').on('click', function (event) {
        //        event.preventDefault();
        //        qtyval = qtyval - 1;
        //        if (qtyval > 1) {
        //            $(this).next().text(qtyval);
        //        } else {
        //            qtyval = 1;
        //            $(this).next().text(qtyval);
        //        }
        //    });
        //});
        //silmeyin 
        $('.dropdown-menu .cart_list').on('click', function (event) {
            event.stopPropagation();
        });
    };

    //Load functions 
    $(document).ready(function () {
        productDetails();
    });

})(jQuery);