﻿


var CustomerInvoiceInformation = (function (context) {
    var customerInvoiceInformation = {};

    customerInvoiceInformation.Get = function () {
        debugger
        context.Network.Get(`/Account/CustomerInvoiceInformationsPartial`, function (response) {
            debugger
            $("#customer-invoice-address").html(response);

        });
    };

    customerInvoiceInformation.OpenUpdateModal = function (id) {

        context.Network.Get(`/Account/CustomerInvoiceInformationDetailPartial?id=${id}`, function (response) {

            $(".modal_invoice_addres_body").html(response);
            $("#addres-invoice-update-modal").modal();
        });
    };

    customerInvoiceInformation.Upsert = function () {

        var customerAddressValid = context.Validator.Validate("customer-invoice-address-form", "warning", "invalid-message");
        var customerAddressId = document.getElementById("customer-invoice-address-id").value;
        var customerAddressTitle = document.getElementById("customer-invoice-address-title").value;
        var customerAddressPhoneNumber = document.getElementById("customer-invoice-address-phone").value;
        var customerAddressFirstName = document.getElementById("customer-invoice-address-firstname").value;
        var customerAddressLastName = document.getElementById("customer-invoice-address-lastname").value;
        var customerAddressMail = document.getElementById("customer-invoice-address-mail").value;
        var customerAddressTaxOffice = document.getElementById("customer-invoice-taxOffice").value;
        var customerAddressTaxNumber = document.getElementById("customer-invoice-taxNumber").value;
        var customerAddressAddres = document.getElementById("customer-invoice-address-addres").value;
        var neighborhoodId = context.Selector.SelectById("invoice-neighborhood-id").value;
        if (customerAddressValid) {

            $("#preloader-active").fadeIn("slow", function () {
                context.Network.PostJson("/Account/CustomerInvoiceInformationUpsert", {
                    id: customerAddressId,
                    firstName: customerAddressFirstName,
                    lastName: customerAddressLastName,
                    title: customerAddressTitle,
                    mail: customerAddressMail,
                    taxOffice: customerAddressTaxOffice,
                    taxNumber: customerAddressTaxNumber,
                    phone: customerAddressPhoneNumber,
                    NeighborhoodId: neighborhoodId,
                    address: customerAddressAddres
                }, function (response) {

                    if (response.success) {
                        customerInvoiceInformation.Get();
                        $("#preloader-active").fadeOut("slow", function () {

                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            });
                            $(`.addres-invoice-update-modal`).modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                        });

                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    customerInvoiceInformation.Delete = function (id) {
        swal("Uyarı", "Adresi silmek istiyor musunuz?", {
            buttons: {
                confirmButtonText: "Evet",
                cancel: "Hayır"

            },
        }).then(
            function (isConfirm) {
                if (isConfirm) {

                    $("#preloader-active").fadeIn("slow", function () {
                        context.Network.PostJson("/Account/CustomerInvoiceInformationRemove", {
                            id: id
                        }, function (response) {

                            if (response.success) {
                                customerInvoiceInformation.Get();
                                $("#preloader-active").fadeOut("slow", function () {

                                    swal({
                                        title: "Başarılı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });

                            }
                            else {
                                $("#preloader-active").fadeOut("slow", function () {
                                    swal({
                                        title: "Uyarı",
                                        text: response.message,
                                        button: "Tamam"
                                    });
                                });
                            }
                        });
                    });

                }

            },
        );
    };

    customerInvoiceInformation.Default = function (id) {

        if (id == $('#default-customer-invoice-address-id').val()) {
            return;
        }

        $("#preloader-active").fadeIn("slow", function () {
            context.Network.PostJson("/Account/CustomerInvoiceInformationDefault", {
                id: id
            }, function (response) {

                if (response.success) {
                    //customerInvoiceInformation.Get();
                    $("#preloader-active").fadeOut("slow", function () {


                        var title = $(`#customer-invoice-adress-title_${id}`).text();
                        var address = $(`#customer-invoice-adress_adress_${id}`).text();

                        $('#default-customer-invoice-address-id').val(id);
                        $('#default-customer-invoice-address-title').text(title);
                        $('#default-customer-invoice-address-address').text(address);
                        $('.activeCustomerInvoiceAdressIcon').hide();
                        $('#activeCustomerInvoiceAdressIcon_' + id).css("display", "block")

                        swal({
                            title: "Başarılı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });

                }
                else {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: response.message,
                            button: "Tamam"
                        });
                    });
                }
            });
        });

    };

    customerInvoiceInformation.GetInvoiceDistricts = function () {
        debugger
        var element = context.Selector.SelectById("invoice-district-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var cityId = context.Selector.SelectById("invoice-city-id").value;


        context.Network.GetJson("/District/Read?CityId=" + cityId, function (response) {
            if (response.success) {
                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;
                    element.appendChild(option);
                }
            } else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });


    };

    customerInvoiceInformation.GetInvoiceNeighborhoods = function () {
        debugger
        var element = context.Selector.SelectById("invoice-neighborhood-id");
        while (element.children.length > 1) {
            element.children[1].remove();
        }
        var districtId = context.Selector.SelectById("invoice-district-id").value;
        context.Network.GetJson("/Neighborhood/Read?DistrictId=" + districtId, function (response) {
            if (response.success) {
                for (var i = 0; i < response.data.length; i++) {
                    var option = document.createElement("option");
                    option.text = response.data[i].name;
                    option.value = response.data[i].id;

                    element.appendChild(option);
                }
            }
            else {

                swal({
                    title: "Uyarı",
                    text: response.message,
                    button: "Tamam"
                });

            }
        });

    };

    customerInvoiceInformation.Phonecontrol = function () {
        const customerOrderAdres = document.getElementById("customer-address-phone");
        const customerInvoicePhone = document.getElementById("customer-invoice-address-phone");

        if (customerInvoicePhone != undefined) {
            customerInvoicePhone.addEventListener('input', handleInput)
        }

        if (customerOrderAdres != undefined) {

            customerOrderAdres.addEventListener('input', handleInput)
        }
        function handleInput(e) {

            var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
            e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
            if (e.target.value[0] == "0") {
                this.value = "";
            }
        }
    }
    customerInvoiceInformation.MailControl = function () {
        var mailPattern = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
  

            mailPattern.test($("#customer-invoice-address-mail").val()) ? $('.invalid-message-mail').hide() : $('.invalid-message-mail').show();
        
    }

    return customerInvoiceInformation;
})(Context);



