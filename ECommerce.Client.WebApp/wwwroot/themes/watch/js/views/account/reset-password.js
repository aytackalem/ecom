﻿var captchaId = null;
var captchaResponse = null;

function loadCaptcha() {
    captchaId = grecaptcha.render('g-recaptcha', {
        'sitekey': '6LdHTp4eAAAAAE8hPindNp_z01QYFuQjpMDzv2Jk',
        'callback': function (response) {
            captchaResponse = response;
        }
    });
}

var AccountResetPassword = (function (context) {
    var accountResetPassword = {};

    accountResetPassword.Open = function (token) {
        context.Network.Get("/Account/ResetPassword?rpd=" + token, function (response) {
            context.Modal.Show("", response);
        });
    };

    accountResetPassword.Submit = function () {

        if (context.Validator.Validate("reset-password-form", "warning", "invalid-message")) {
            $("#preloader-active").fadeIn("slow", function () {

                var newPassword = context.Selector.SelectByName("new-password")[0].value;
                var newPasswordAgain = context.Selector.SelectByName("new-password-again")[0].value;

                if (newPassword != newPasswordAgain) {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: "Şifreler eşleşmemektedir lütfen kontrol ediniz",
                            button: "Tamam"
                        });
                    });
                    return;
                }

                if (captchaResponse == null) {
                    $("#preloader-active").fadeOut("slow", function () {
                        swal({
                            title: "Uyarı",
                            text: "Lütfen captcha'yı doğrulayınız.",
                            button: "Tamam"
                        });
                    });
                    return;
                }


                context.Network.PostJson("/Account/ResetPassword", {
                    captchaResponse: captchaResponse,
                    data: {
                        token: context.Selector.SelectByName("token")[0].value,
                        password: context.Selector.SelectByName("new-password")[0].value
                    }
                }, function (response) {
                    captchaResponse = null;
                    grecaptcha.reset(captchaId);
                    if (response.success) {

                        $("#preloader-active").fadeOut("slow", function () {
                            $('#reset-password-form')[0].reset();
                            swal({
                                title: "Başarılı",
                                text: response.message,
                                button: "Tamam"
                            }).then((result) => {

                                window.location.href = "/";
                            })

                        });


                    }
                    else {
                        $("#preloader-active").fadeOut("slow", function () {
                            swal({
                                title: "Uyarı",
                                text: response.message,
                                button: "Tamam"
                            });
                        });
                    }
                });
            });
        }
    };

    return accountResetPassword;
})(Context);