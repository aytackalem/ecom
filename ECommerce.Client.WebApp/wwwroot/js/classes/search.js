﻿var Search = (function (selector, network) {
    var search = {};
    var busy = false;
    var timeout = null;
    search.Initialize = function (elementId) {
        debugger
        if (elementId.term.length > 3 && !busy) {
            if (timeout != null) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
                busy = true;
                var start = new Date();
                network.GetJson("/Search/Read?q=" + e.target.value, function (response) {
                    debugger
                    busy = false;
                });
            }, 1500);
        }

    };
    $(function () {

        var searchSource = [];
        if ($(window).width() <= 869) {
            var search = $(".searchInputMob")
        } else {
            var search = $(".searchInput")
        }
 
  

        $(search).autocomplete({
            source: function (request, response) {
                debugger;
                if (request.term.length > 3 && !busy) {
                    if (timeout != null) {
                        clearTimeout(timeout);
                    }
                    timeout = setTimeout(function () {
                        Network.GetJson("/Search/Read?q=" + request.term, function (autocompleteModel) {
                            busy = false;
                            searchSource = [];
                            $.each(autocompleteModel.data, function (index,item) {

                                searchSource.push({
                                    url: `/${item.url}`,
                                    value: item.name,
                                    img: item.photoUrl,
                                    type: item.type
                                });

                            });

                            response(searchSource);


                        });
                    }, 500);
                }
            },
            focus: function (event, ui) {
      
                return false;
            },
            select: function (event, ui) {
                window.location.href = ui.item.url;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var inner_html = `<a href="${item.url}"><div class="list_item_container ui-item-li"> <img class="ui-image" src="${item.img}"/> <span>${item.value}</span> </div> </a>`  
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append(inner_html)
                .appendTo(ul);
        };
 



        $(search).on('keyup', function (e) {
            if (e.keyCode == 13) {
            window.location.href = "/Search?q=" +  $(this).val();
        }                                   
    });
        $('.txtSearch').on('click', function (ui) {

            if ($(window).width() <= 869) {
                var searchInput = $(".searchInputMob").val()
            } else {
                var searchInput = $(".searchInput").val()
            }

            if (searchInput == "") {
                window.location.href = "/discount";
        
            } else {
                window.location.href = "/Search?q=" + searchInput;
            }
         
            
       
    });

  

    });

    return search;
})(Selector, Network);



