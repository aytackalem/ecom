﻿var Selector = (function () {
    var selector = {};

    selector.SelectById = function (id) {
        return document.getElementById(id);
    };

    selector.SelectByName = function (name) {
        return document.getElementsByName(name);
    };

    selector.SelectByClassName = function (className) {
        return document.getElementsByClassName(className);
    };

    return selector;
})();