﻿var Network = (function () {
    var network = {};

    function request(success, error) {
        var xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.onload = function () {
            if (success) {
                success(this.responseText);
            }
        };
        xmlHttpRequest.onerror = function () {
            if (error) {
                error();
            }
        }

        return xmlHttpRequest;
    }

    network.Get = function (url, success, error) {
        var xmlHttpRequest = request(success, error);
        var token = $("input[name='AntiTokenWebField']").val();

        xmlHttpRequest.open("GET", url);

        if (token) {
            xmlHttpRequest.setRequestHeader("Anti-Token-Web", token);
        }
        xmlHttpRequest.send();
    };

    network.GetJson = function (url, success, error) {

        var xmlHttpRequest = request(function (response) {
            if (success) {
                success(JSON.parse(response));
            }
        }, error);
        var token = $("input[name='AntiTokenWebField']").val();

        xmlHttpRequest.open("GET", url);
        xmlHttpRequest.setRequestHeader("Content-type", "application/json");
        if (token) {
            xmlHttpRequest.setRequestHeader("Anti-Token-Web", token);
        }
        xmlHttpRequest.send();
    };

    network.Post = function (url, data, success, error) {
        var xmlHttpRequest = request(success, error);

        xmlHttpRequest.open("POST", url);
        var token = $("input[name='AntiTokenWebField']").val();

        if (token) {
            xmlHttpRequest.setRequestHeader("Anti-Token-Web", token);
        }
        xmlHttpRequest.send(data);
    };

    network.PostJson = function (url, data, success, error) {
 
        var xmlHttpRequest = request(function (response) {
            if (success) {
                success(JSON.parse(response));
            }
        }, error);

        var token = $("input[name='AntiTokenWebField']").val();
        xmlHttpRequest.open("POST", url);
        xmlHttpRequest.setRequestHeader("Content-type", "application/json");
        if (token) {
            xmlHttpRequest.setRequestHeader("Anti-Token-Web", token);
        }
        xmlHttpRequest.send(JSON.stringify(data));
    };

    return network;
})();