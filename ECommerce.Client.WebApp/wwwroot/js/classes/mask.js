
/*Phone Mask*/
const phoneMask = document.getElementById("phone")
const invoicePhone = document.getElementById("invoice-phone");
if (phoneMask) {
    phoneMask.addEventListener('input', handleInput)
}
if (invoicePhone) {
    invoicePhone.addEventListener('input', handleInput)
}

function handleInput(e) {
    
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    if (e.target.value[0] == "0" || e.target.value[0] == "9" || (e.target.value[0] == "(" && (e.target.value[1] == "0" || e.target.value[1] == "9"))) {
        this.value = "";
    }
}
/*Phone Mask*/

/*Credit Card*/
/*Number*/

const cardInput = document.getElementById('cc-number');
cardInput.addEventListener('keyup', handleChange);
cardInput.addEventListener('change', handleChange);
function handleChange(event) {
    event.target.value = event.target.value
        .replace(/\D/g, '')
        .substring(0, 16)
        .split('')
        .reduce((acc, cur, idx) => {
            const isEveryFourth = (idx + 1) % 4 === 0 && idx > 0 && idx < 16;
            const spacer = isEveryFourth ? '-' : '';
            return acc + cur + spacer;
        }, '')
        .replace(/([-]*)$/g, '');

}
/*Number*/

/*Credit Card*/