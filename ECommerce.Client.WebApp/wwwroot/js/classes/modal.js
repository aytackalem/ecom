﻿"use strict";

var Modal = (function (selector) {
    var modal = {};

    var modalId = 0;

    modal.Show = function (title, message) {
        modalId++;

        var modalElement = document.createElement("div");
        modalElement.id = "ecommerce-modal-" + modalId;
        modalElement.className = "ecommerce-modal";
        modalElement.innerHTML = '<button onclick="Context.Modal.Close(' + modalId + ')" class="close-button" aria-label="close" tabindex="0">✕</button><div><h1>' + title + '</h1>' + message + '</div>';
        document.body.appendChild(modalElement);
    };

    modal.Close = function (modalId) {
        var modalElement = selector.SelectById("ecommerce-modal-" + modalId);
        document.body.removeChild(modalElement);
    };

    return modal;
})(Selector);