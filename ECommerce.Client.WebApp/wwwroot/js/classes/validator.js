﻿
var Validator = (function () {
    var validator = {};

    validator.Validate = function (elementId, invalidClassName, tooltipClassName) {
        var valid = true;
        var containerElement = document.getElementById(elementId);
        var elementTags = ["input", "select","textarea"];
        for (var et = 0; et < elementTags.length; et++) {
            var elementTag = elementTags[et];
            var inputElements = containerElement.getElementsByTagName(elementTag);
            for (var i = 0; i < inputElements.length; i++) {
                var inputElement = inputElements[i];
                if (inputElement.hasAttribute("required")) {

                    if (elementTag == "select" || elementTag == "input" || elementTag == "textarea")

                        var value = elementTag == "input" || elementTag == "textarea"
                        ? inputElement.value
                        : inputElement.options[inputElement.selectedIndex].value;
                    if (value === "") {
                        var warningElement = inputElement.parentElement.querySelector('.' + tooltipClassName);
                        if (warningElement != null)
                            warningElement.style.display = "inherit";

                        inputElement.classList.add(invalidClassName);

                        valid = false;
                    }
                    else {
                        var warningElement = inputElement.parentElement.querySelector('.' + tooltipClassName);
                        if (warningElement != null)
                            warningElement.style.display = "none";

                        inputElement.classList.remove(invalidClassName);
                    }

                }
            }
        }

        return valid;
    };

    return validator;
})();