﻿var Tab = (function (selector) {
    var _tab = {};

    var _tabNamePrefix = null;

    var _tabsCount = null;

    _tab.Initialize = function (tabNamePrefix, tabsCount) {
        _tabNamePrefix = tabNamePrefix;
        _tabsCount = tabsCount;
    }

    _tab.Navigate = function (tabIndex) {

        if (_tabNamePrefix == null) {
            console.log("Tab name prefix not set.");
            return;
        }

        if (_tabsCount == null) {
            console.log("Tabs count not set.");
            return;
        }

        for (var i = 0; i < _tabsCount; i++) {
            var element = selector.SelectById(_tabNamePrefix + "-" + i);

            if (element) {
                element.style.display = i == tabIndex ? "inherit" : "none";
            }
        }
    };

    return _tab;
})(Selector);