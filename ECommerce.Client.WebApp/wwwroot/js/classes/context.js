﻿var Context = (function (network, selector, validator, modal, shoppingCart, tab) {
    var context = {};
    context.Network = network;
    context.Selector = selector;
    context.Validator = validator;
    context.Modal = modal;
    context.ShoppingCart = shoppingCart;
    context.Tab = tab;

    return context;
})(Network, Selector, Validator, Modal, ShoppingCart, Tab);