﻿"use strict";

var ShoppingCart = (function (network, modal) {

    var shoppingCart = {};
    var cartUrl = document.getElementById("cartUrl").href;
    shoppingCart.Add = function (productInformationId, quantity, success) {
        network.PostJson("/ShoppingCart/Add", { productInformationId: productInformationId, quantity: quantity }, function (response) {
            debugger
            swal("Başarılı", "Ürün sepetinize eklendi", {
                buttons: {
                    cancel: "Alışverişe Devam Et",
                    confirmButtonText: "Sepete Git"
                },
            }).then(
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = cartUrl;
                        return;
                    }

                    if (response.success) {
                        var html = "";
                        var amount = 0;
                        $.each(response.shoppingCartItems, function (index, value) {
                            html += "<li>";
                            html += "    <div class=\"shopping-cart-img\">";
                            html += "        <a href=" + value.productInformation.url + "><img alt=" + value.productInformation.fullName + " src=" + value.productInformation.productInformationPhotos[0].fileName + "></a>";
                            html += "   </div>";
                            html += "    <div class=\"shopping-cart-title\">";
                            html += "        <h4><a href=" + value.productInformation.url + ">" + value.productInformation.fullName + "</a></h4>";
                            if (value.getXPayYGift) {
                                html += "<h3><span class=\"color-orange\">" + value.getXPayYCount + " ürün hediye edildi</span> </h3>";

                            }

                            if (value.productInformation.productInformationPrice.totalUnitPrice > 0) {
                                html += "        <h3><span>" + value.quantity + " Adet </span>" + value.productInformation.productInformationPrice.totalUnitPrice + " TL</h3>";
                            }

                            html += "    </div>";
                            html += "  </li>";
                            amount += value.productInformation.productInformationPrice.totalUnitPrice;
                        });
                        debugger
                        $('.shoppingCartProductAmount').text(amount + ' TL');
                        $('.basketIconCount').text(response.count);
                        $('.shoppingCartProduct').html(html);

                    }
                    else {
                        swal("Hata", response.message, "warning", {
                            button: "Tamam",
                        });
                    }

                    if (success) {
                        success(response);
                    }


                },
            );

        });
    };

    shoppingCart.AddShoppingCartDiscount = function (productInformationId, quantity, success) {
        network.PostJson("/ShoppingCart/Add", { productInformationId: productInformationId, quantity: quantity, shoppingCartDiscount: true }, function (response) {

            debugger
            swal("Başarılı", "Ürün sepetinize eklendi", "success", {
                button: "Tamam",
            });
            window.location.reload();
            if ($(window).width() > 768) {
                $('#basketIcon').text(response.count);
            } else {
                $('#basketIconMob').text(response.count);
            }
            if (success) {
                success(response);
            }

        });
    };

    shoppingCart.Remove = function (productInformationId, success) {
        network.PostJson("/ShoppingCart/Remove", {

            productInformationId: productInformationId
        }, function (response) {
            success(response);

        });
    };

    shoppingCart.Update = function (productInformationId, quantity, shoppingCartDiscount, success) {
        network.PostJson("/ShoppingCart/Update", {

            productInformationId: productInformationId,
            quantity: quantity,
            shoppingCartDiscount: shoppingCartDiscount
        }, function (response) {

            if (success)
                success(response);

        });
    };

    shoppingCart.Clear = function () {
        network.GetJson("/ShoppingCart/Clear", function (response) {
            if (response.success) {
                modal.Show("", response.message);
            }
        });
    };

    shoppingCart.Count = function () {
        network.GetJson("/ShoppingCart/Count", function (response) {
            if (response.success) {
                modal.Show("", response.message);
            }
        });
    };

    return shoppingCart;
})(Network, Modal);