using ECommerce.Infrastructure;
using ECommerce.Persistence.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
//using ECommerce.Client.WebApp.Middleware;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace ECommerce.Client.WebApp
{
    public class Startup
    {
        #region Properties
        public IConfiguration _configuration { get; }
        #endregion

        #region Constructors
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region Methods
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }).AddRazorRuntimeCompilation();

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new TenantViewLocationExpander(_configuration));
            });

            services.AddAntiforgery(opiton =>
            {
                opiton.FormFieldName = "AntiTokenWebField";
                opiton.HeaderName = "Anti-Token-Web";
            });

            services.AddHttpContextAccessor();

            services.AddPersistenceServices(_configuration);

            services.AddInfrastructureServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseMiddleware<RedirectMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var x = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(x.Value);

#if RELEASE
            app.UseHttpsRedirection();            
            app.UseHsts();
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
                context.Response.Headers.Add("X-Xss-Protection", "1; mode=block");
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Remove("server");
                context.Response.Headers.Remove("x-powered-by");              
                await next();
            });
#endif

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                #region Routes
                endpoints.MapControllerRoute(
                            name: "brand",
                            pattern: "{url}-{id}-b",
                            defaults: new { controller = "Brand", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "brandFilter",
                    pattern: "{url}-{id}-b/{pv}-pv",
                    defaults: new { controller = "Brand", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "category",
                    pattern: "{url}-{id}-c",
                    defaults: new { controller = "Category", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "categoryFilter",
                    pattern: "{url}-{id}-c/{pv}-pv",
                    defaults: new { controller = "Category", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "group",
                    pattern: "{url}-{id}-g",
                    defaults: new { controller = "Group", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "groupFilter",
                    pattern: "{url}-{id}-g/{pv}-pv",
                    defaults: new { controller = "Group", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "content",
                    pattern: "{url}-{id}-co",
                    defaults: new { controller = "Content", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "information",
                    pattern: "{url}-{id}-i",
                    defaults: new { controller = "Information", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "product",
                    pattern: "{url}-{productInformationId}-p",
                    defaults: new { controller = "Product", action = "Index" });

                endpoints.MapControllerRoute(
                      name: "discount",
                      pattern: "discount",
                      defaults: new { controller = "Home", action = "Discount" });

                endpoints.MapControllerRoute(
                    name: "find",
                    pattern: "fidan-bul",
                    defaults: new { controller = "Home", action = "Find" });

                endpoints.MapControllerRoute(
                    name: "findFilter",
                    pattern: "fidan-bul/{pv}-pv",
                    defaults: new { controller = "Home", action = "Find" });

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                #endregion

                #region Multilanguage Routes
                endpoints.MapControllerRoute(
                    name: "ml-brand",
                    pattern: "{l}/{url}-{id}-b",
                    defaults: new { controller = "Brand", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-brandFilter",
                    pattern: "{l}/{url}-{id}-b/{pv}-pv",
                    defaults: new { controller = "Brand", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "ml-category",
                    pattern: "{l}/{url}-{id}-c",
                    defaults: new { controller = "Category", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-categoryFilter",
                    pattern: "{l}/{url}-{id}-c/{pv}-pv",
                    defaults: new { controller = "Category", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "ml-group",
                    pattern: "{l}/{url}-{id}-g",
                    defaults: new { controller = "Group", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-groupFilter",
                    pattern: "{l}/{url}-{id}-g/{pv}-pv",
                    defaults: new { controller = "Group", action = "IndexFilter" });

                endpoints.MapControllerRoute(
                    name: "ml-content",
                    pattern: "{l}/{url}-{id}-co",
                    defaults: new { controller = "Content", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-geolocationContent",
                    pattern: "{l}/{url}-gc",
                    defaults: new { controller = "GeolocationContent", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-geolocationContent",
                    pattern: "{l}/{url}-{cityId}-gcc",
                    defaults: new { controller = "GeolocationContent", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-geolocationContent",
                    pattern: "{l}/{url}-{cityId}-{districtId}-gcd",
                    defaults: new { controller = "GeolocationContent", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-geolocationContent",
                    pattern: "{l}/{url}-{cityId}-{districtId}-{neighborhoodId}-gcn",
                    defaults: new { controller = "GeolocationContent", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-information",
                    pattern: "{l}/{url}-{id}-i",
                    defaults: new { controller = "Information", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "ml-product",
                    pattern: "{l}/{url}-{productInformationId}-p",
                    defaults: new { controller = "Product", action = "Index" });

                endpoints.MapControllerRoute(
                      name: "ml-discount",
                      pattern: "{l}/discount",
                      defaults: new { controller = "Home", action = "Discount" });

                endpoints.MapControllerRoute(
                    name: "ml-find",
                    pattern: "{l}/fidan-bul",
                    defaults: new { controller = "Home", action = "Find" });

                endpoints.MapControllerRoute(
                    name: "ml-findFilter",
                    pattern: "{l}/fidan-bul/{pv}-pv",
                    defaults: new { controller = "Home", action = "Find" });

                endpoints.MapControllerRoute(
                    name: "ml-default",
                    pattern: "{l}/{controller=Home}/{action=Index}/{id?}");
                #endregion
            });
        }
        #endregion
    }
}
