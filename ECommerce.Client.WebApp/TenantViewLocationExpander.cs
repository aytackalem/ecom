﻿using ECommerce.Application.Common.Interfaces.Services;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace ECommerce.Client.WebApp
{
    public class TenantViewLocationExpander : IViewLocationExpander
    {
        #region Fields
        protected readonly IConfiguration _configuration;

        #endregion

        #region Constructors
        public TenantViewLocationExpander(IConfiguration configuration)
        {
            #region Fields
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            var theme = !string.IsNullOrEmpty(this._configuration.GetSection("Theme").Value) ? this._configuration.GetSection("Theme").Value : "";

            foreach (var vlLoop in viewLocations)
            {
                yield return $"/Themes/{theme}{vlLoop}";
            }
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
        }

        //private const string THEME_KEY = "theme", TENANT_KEY = "tenant";

        //public void PopulateValues(ViewLocationExpanderContext context)
        //{
        //    context.Values[THEME_KEY]
        //        = context.ActionContext.HttpContext.GetTenant<AppTenant>()?.Theme;

        //    context.Values[TENANT_KEY]
        //        = context.ActionContext.HttpContext.GetTenant<AppTenant>()?.Name.Replace(" ", "-");
        //}

        //public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        //{
        //    string theme = null;
        //    if (context.Values.TryGetValue(THEME_KEY, out theme))
        //    {
        //        IEnumerable<string> themeLocations = new[]
        //        {
        //        $"/Themes/{theme}/{{1}}/{{0}}.cshtml",
        //        $"/Themes/{theme}/Shared/{{0}}.cshtml"
        //    };

        //        string tenant;
        //        if (context.Values.TryGetValue(TENANT_KEY, out tenant))
        //        {
        //            themeLocations = ExpandTenantLocations(tenant, themeLocations);
        //        }

        //        viewLocations = themeLocations.Concat(viewLocations);
        //    }


        //    return viewLocations;
        //}

        //private IEnumerable<string> ExpandTenantLocations(string tenant, IEnumerable<string> defaultLocations)
        //{
        //    foreach (var location in defaultLocations)
        //    {
        //        yield return location.Replace("{0}", $"{{0}}_{tenant}");
        //        yield return location;
        //    }
        //}
        #endregion
    }
}
