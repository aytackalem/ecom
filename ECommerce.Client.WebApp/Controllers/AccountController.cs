﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Accounts;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ECommerce.Client.WebApp.Controllers
{
    public class AccountController : Base.ControllerBase
    {
        #region Fields
        private readonly IAuthUtility<CustomerUser> _utility;

        private readonly ICustomerUserService _customerUserService;

        private readonly IOrderService _orderService;

        private readonly ICustomerAddressService _customerAddressService;

        private readonly ICustomerInvoiceInformationService _customerInvoiceInformationService;

        private readonly ICaptchaService _captchaService;

        private readonly ICityService _cityService;

        private readonly IDistrictService _districtService;

        private readonly INeighborhoodService _neighborhoodService;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public AccountController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, IAuthUtility<CustomerUser> utility, ICustomerUserService customerUserService, IShoppingCartService shoppingCartService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IOrderService orderService,
            IProductService productService, IBrandService brandService, ICustomerAddressService customerAddressService, ICaptchaService captchaService, ICityService cityService, INeighborhoodService neighborhoodService, IDistrictService districtService, ICustomerInvoiceInformationService customerInvoiceInformationService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._utility = utility;
            this._customerUserService = customerUserService;
            this._orderService = orderService;
            this._captchaService = captchaService;
            this._customerAddressService = customerAddressService;
            this._cityService = cityService;
            this._districtService = districtService;
            this._neighborhoodService = neighborhoodService;
            this._customerInvoiceInformationService = customerInvoiceInformationService;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var vm = new Application.Client.ViewModels.Accounts.Index();

            var logged = this._utility.GetLogged();
            if (!logged.Success)
            {
                return new RedirectResult("/Account/Login");
            }



            var customerUserResponse = this._customerUserService.Read(logged.Data.Id);
            if (customerUserResponse.Success)
            {
                vm.Customer = customerUserResponse.Data.Customer;
                vm.MoneyPointAmount = customerUserResponse.Data.MoneyPointAmount;

            }


            var orders = _orderService.ReadByCustomerId(logged.Data.Id);
            if (orders.Success)
            {
                vm.Orders = orders.Data;
            }


            return View(vm);
        }

        public IActionResult Register()
        {
            var vm = new Register();

            return View(vm);
        }

        [HttpGet]
        public IActionResult Login()
        {
            var vm = new Login();

            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                return new RedirectResult("/Account");
            }

            return View(vm);
        }

        public IActionResult ForgotPassword()
        {
            var vm = new ForgotPassword();
            return View(vm);
        }

        public IActionResult ResetPassword(string rpd)
        {
            return View(new ResetPassword { Token = rpd });
        }

        #region CustomerAddresses
        public IActionResult CustomerAddressesPartial()
        {
            var logged = this._utility.GetLogged();
            if (!logged.Success)
            {
                return new RedirectResult("/Account/Login");
            }

            var customerAddreses = new List<Application.Client.DataTransferObjects.CustomerAddress>();

            var customerAddress = this._customerAddressService.Read(logged.Data.Id);
            if (customerAddress.Success)
            {
                customerAddreses = customerAddress.Data;
            }



            return PartialView(customerAddreses);
        }

        public IActionResult CustomerAddressDetailPartial(int id)
        {
            var vm = new Application.Client.ViewModels.Accounts.CustomerAddress
            {
                Cities = new System.Collections.Generic.List<City>(),
                Districts = new System.Collections.Generic.List<District>(),
                Neighborhoods = new System.Collections.Generic.List<Neighborhood>(),
                Address = new Application.Client.DataTransferObjects.CustomerAddress()
            };

            var countryId = this._settingService.DefaultCountryId;

            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                var customerAddress = this._customerAddressService.Read(id, logged.Data.Id);
                if (customerAddress.Success)
                {
                    countryId = customerAddress.Data.Neighborhood.District.City.Country.Id;
                    vm.Districts = this._districtService.ReadByCityId(customerAddress.Data.Neighborhood.District.City.Id).Data;
                    vm.Neighborhoods = this._neighborhoodService.ReadByDistrictId(customerAddress.Data.Neighborhood.District.Id).Data;
                    vm.Address = customerAddress.Data;
                }
            }

            vm.DefaultCountryId = countryId;
            vm.Cities = this._cityService.ReadByCountryId(countryId).Data;

            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult CustomerAddressUpsert([FromBody] Application.Client.DataTransferObjects.CustomerAddress customerAddress)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerAddress.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }
            return Json(this._customerAddressService.Upsert(customerAddress));

        }

        [HttpPost]
        public JsonResult CustomerAddressRemove([FromBody] Application.Client.DataTransferObjects.CustomerAddress customerAddress)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerAddress.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }

            return Json(this._customerAddressService.Delete(customerAddress.Id, customerAddress.CustomerId));
        }

        [HttpPost]
        public JsonResult CustomerAddressDefault([FromBody] Application.Client.DataTransferObjects.CustomerAddress customerAddress)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerAddress.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }

            return Json(this._customerAddressService.Default(customerAddress.Id, customerAddress.CustomerId));
        }
        #endregion


        #region CustomerInvoiceInformations
        public IActionResult CustomerInvoiceInformationsPartial()
        {
            var logged = this._utility.GetLogged();
            if (!logged.Success)
            {
                return new RedirectResult("/Account/Login");
            }

            var customerAddreses = new List<Application.Client.DataTransferObjects.CustomerInvoiceInformation>();

            var customerInvoiceInformation = this._customerInvoiceInformationService.Read(logged.Data.Id);
            if (customerInvoiceInformation.Success)
            {
                customerAddreses = customerInvoiceInformation.Data;
            }



            return PartialView(customerAddreses);
        }

        public IActionResult CustomerInvoiceInformationDetailPartial(int id)
        {
            var vm = new Application.Client.ViewModels.Accounts.CustomerInvoiceInformation
            {
                Cities = new System.Collections.Generic.List<City>(),
                Districts = new System.Collections.Generic.List<District>(),
                Neighborhoods = new System.Collections.Generic.List<Neighborhood>(),
                Address = new Application.Client.DataTransferObjects.CustomerInvoiceInformation()
            };

            var countryId = this._settingService.DefaultCountryId;

            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                var customerInvoiceInformation = this._customerInvoiceInformationService.Read(id, logged.Data.Id);
                if (customerInvoiceInformation.Success)
                {
                    countryId = customerInvoiceInformation.Data.Neighborhood.District.City.Country.Id;
                    vm.Districts = this._districtService.ReadByCityId(customerInvoiceInformation.Data.Neighborhood.District.City.Id).Data;
                    vm.Neighborhoods = this._neighborhoodService.ReadByDistrictId(customerInvoiceInformation.Data.Neighborhood.District.Id).Data;
                    vm.Address = customerInvoiceInformation.Data;
                }
            }
            vm.Cities = this._cityService.ReadByCountryId(countryId).Data;
            vm.DefaultCountryId = countryId;
            return PartialView(vm);
        }

        [HttpPost]
        public JsonResult CustomerInvoiceInformationUpsert([FromBody] Application.Client.DataTransferObjects.CustomerInvoiceInformation customerInvoiceInformation)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerInvoiceInformation.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }
            return Json(this._customerInvoiceInformationService.Upsert(customerInvoiceInformation));

        }

        [HttpPost]
        public JsonResult CustomerInvoiceInformationRemove([FromBody] Application.Client.DataTransferObjects.CustomerInvoiceInformation customerInvoiceInformation)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerInvoiceInformation.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }

            return Json(this._customerInvoiceInformationService.Delete(customerInvoiceInformation.Id, customerInvoiceInformation.CustomerId));
        }

        [HttpPost]
        public JsonResult CustomerInvoiceInformationDefault([FromBody] Application.Client.DataTransferObjects.CustomerInvoiceInformation customerInvoiceInformation)
        {
            var logged = this._utility.GetLogged();
            if (logged.Success)
            {
                customerInvoiceInformation.CustomerId = logged.Data.Id;
            }
            else
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bilgileri bulunamadı" });

            }

            return Json(this._customerInvoiceInformationService.Default(customerInvoiceInformation.Id, customerInvoiceInformation.CustomerId));
        }
        #endregion

        /// <summary>
        /// Müşteri üye olurken captcha ile doğrulama yapar.
        /// Eğer Sms yoksa modal açılmaz müşteri direk üye olur.
        /// </summary>
        /// <param name="captchaRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Register([FromBody] CaptchaRequest<CustomerUser> captchaRequest)
        {
            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            return Json(this._customerUserService.Create(captchaRequest.Data));
        }

        /// <summary>
        /// Müşteri üye olurken sms açıksa modal açılır telefona gelen sms ile doğrulama yapar.       
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UserVerification([FromBody] CustomerUserVerification customerUserVerification)
        {
            return Json(this._customerUserService.UserVerification(customerUserVerification));
        }

        /// <summary>
        /// Müşteri Login olurken kullanılır
        /// </summary>
        /// <param name="captchaRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Login([FromBody] CaptchaRequest<CustomerUser> captchaRequest)
        {
            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            return Json(this._utility.Login(captchaRequest.Data, false));
        }

        /// <summary>
        /// Müşteri şifremi unuttuma bastıktan sonra kullanıcı adını sisteme girer.
        /// Müşteriye mail gönderilir.
        /// </summary>
        /// <param name="captchaRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ForgotPassword([FromBody] CaptchaRequest<CustomerUser> captchaRequest)
        {
            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            return Json(this._utility.ForgotPassword(captchaRequest.Data));
        }

        /// <summary>
        /// Müşteri Şifremi unuttum dedikten sonra mail'den gelen url ile şifreyi değiştirme için buraya istek atar.
        /// </summary>
        /// <param name="captchaRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ResetPassword([FromBody] CaptchaRequest<CustomerUser> captchaRequest)
        {
            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            return Json(this._utility.ResetPassword(captchaRequest.Data));
        }

        /// <summary>
        /// Müşteri login'ise şifresini değiştirmek için kullanılır.
        /// </summary>
        /// <param name="customerUser"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MemberUpdate([FromBody] Customer customer)
        {

            var logged = this._utility.GetLogged();
            if (!logged.Success)
            {
                return Json(new Response { Success = false, Message = "Kullanıcı bulunamadı" });
            }
            customer.Id = logged.Data.Id;

            return Json(this._customerUserService.MemberUpdate(customer));
        }


        /// <summary>
        /// Müşteri login'ise şifresini değiştirmek için kullanılır.
        /// </summary>
        /// <param name="customerUser"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MemberChangePassword([FromBody] CustomerUser customerUser)
        {
            var accountResponse = this._utility.GetLogged();
            if (!accountResponse.Success)
                return Json(accountResponse);


            customerUser.Id = accountResponse.Data.Id;
            return Json(this._utility.UpdatePassword(customerUser));
        }

        /// <summary>
        /// Müşteri çıkış yapar
        /// </summary>
        /// <returns></returns>
        public JsonResult Logout()
        {
            return Json(this._utility.Logout());
        }
        #endregion
    }
}
