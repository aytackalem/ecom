﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Brands;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class BrandController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<ControllerBase> _logger;

        private readonly IBrandService _brandService;

        private readonly IFileCaching _fileCaching;
        #endregion

        #region Constructors
        public BrandController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<ControllerBase> logger, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, ICategoryService categoryService,  IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IFileCaching fileCaching, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._brandService = brandService;
            this._fileCaching = fileCaching;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index(int id, string url, int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Application.Client.ViewModels.Brands.Index { Success = true };

            #region Check Parameters
            if (p < 0 || (pc != 20 && pc != 50) || st < 0)
                return Redirect("/Home/Error");
            #endregion

            #region Caching
            var key = $"{id}-{p}-{pc}-{st}-b";
            if (this._fileCaching.Contains(key) && (!Request.QueryString.HasValue || (Request.QueryString.HasValue && !Request.QueryString.Value.Contains("caching=false"))))
            {
                vm = this._fileCaching.Read<Application.Client.ViewModels.Brands.Index>(key);

                #region Check Url
                if ($"{url}-{id}-b" != vm.Brand.Url)
                    return RedirectPermanent($"/{vm.Brand.Url}");
                #endregion

                return View(vm);
            }
            #endregion

            var brandResponse = this._brandService.Read(id);
            if (!brandResponse.Success)
                return Redirect("/Home/Error");

            #region Check Url
            if ($"{url}-{id}-b" != brandResponse.Data.Url)
                return RedirectPermanent($"/{brandResponse.Data.Url}");
            #endregion

            var productResponse = this._productService.InfiniteReadByBrandId(id, p, pc, st);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            #region Check Parameters
            if (p != 0 && p > (productResponse.PagesCount - 1))
                return Redirect("/Home/Error");
            #endregion

            var brandPropertiesResponse = this._brandService.ReadBrandProperties(id);
            if (!brandPropertiesResponse.Success)
            {
                vm.Message = brandPropertiesResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Properties = brandPropertiesResponse.Data;
            vm.Brand = brandResponse.Data;
            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;

            #region Caching
            if (vm.Success && Request.QueryString.HasValue && Request.QueryString.Value.Contains("caching=false"))
                this._fileCaching.Write(key, vm);
            #endregion

            return View(vm);
        }

        public IActionResult IndexFilter(string pv, int id, string url, int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Application.Client.ViewModels.Brands.Index { Success = true };

            var brandResponse = this._brandService.Read(id);
            if (!brandResponse.Success)
            {
                vm.Message = brandResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            var brandPropertiesResponse = this._brandService.ReadBrandProperties(id);
            if (!brandPropertiesResponse.Success)
            {
                vm.Message = brandPropertiesResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            List<int> propertyValueIds = new();
            if (!string.IsNullOrEmpty(pv))
            {
                propertyValueIds = pv
                    .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => int.Parse(x))
                    .ToList();
                vm.PropertyValueIds = propertyValueIds;
            }

            var productResponse = this._productService.InfiniteReadByBrandId(id, p, pc, st, propertyValueIds);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }


            vm.Properties = brandPropertiesResponse.Data;
            vm.Brand = brandResponse.Data;
            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;

            return View(vm);
        }
        #endregion
    }
}
