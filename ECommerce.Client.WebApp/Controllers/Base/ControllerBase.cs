﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Base;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers.Base
{
    public class ControllerBase : Controller
    {
        #region Fields
        protected readonly IGoogleTagManagerService _googleTagManagerService;

        protected readonly IGoogleAnalyticsService _googleAnalyticsService;

        protected readonly IFacebookPixelService _facebookPixelService;

        protected readonly IGoogleAdsService _googleAdsService;

        protected readonly ICategoryService _categoryService;

        protected readonly IGroupService _groupService;

        protected readonly IShoppingCartService _shoppingCartService;

        protected readonly IInformationService _informationService;

        protected readonly IContentService _contentService;

        protected readonly IProductService _productService;

        protected readonly IBrandService _brandService;

        protected readonly IAuthUtility<CustomerUser> _authUtility;

        protected readonly ICompanyService _companyService;

        protected readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public ControllerBase(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ICategoryService categoryService,  IShoppingCartService shoppingCartService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IProductService productService, IBrandService brandService,ICompanyService companyService,
            ISettingService settingService)
        {
            #region Fields
            this._googleTagManagerService = googleTagManagerService;
            this._googleAnalyticsService = googleAnalyticsService;
            this._googleAdsService = googleAdsService;
            this._facebookPixelService = facebookPixelService;
            this._categoryService = categoryService;
            this._shoppingCartService = shoppingCartService;
            this._informationService = informationService;
            this._contentService = contentService;
            this._authUtility = authUtility;
            this._productService = productService;
            this._companyService = companyService;
            this._brandService = brandService;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (ViewData.Model != null && ViewData.Model is PageViewModelBase)
            {
                var vm = ViewData.Model as PageViewModelBase;

                #region Google Ads Layout
                var response1 = this._googleAdsService.Layout();
                if (response1.Success)
                    ViewData["GoogleAdsLayout"] = response1.Data;
                #endregion

                #region Google Analytics Layout
                var response2 = this._googleAnalyticsService.Layout();
                if (response2.Success)
                    ViewData["GoogleAnalyticsLayout"] = response2.Data;
                #endregion

                #region Google Tag Manager Layout
                var response3 = this._googleTagManagerService.LayoutHead();
                if (response3.Success)
                    ViewData["GoogleTagManagerLayoutHead"] = response3.Data;

                var response4 = this._googleTagManagerService.LayoutBody();
                if (response4.Success)
                    ViewData["GoogleTagManagerLayoutBody"] = response4.Data;
                #endregion

                #region Facebook Pixel Layout
                var response5 = this._facebookPixelService.Layout(Request.Path);
                if (response5.Success)
                    ViewData["FacebookPixelLayout"] = response5.Data;
                #endregion

                #region Read Categories
                var categoryResponse = this._categoryService.Read();
                if (!categoryResponse.Success)
                {
                    vm.Success = categoryResponse.Success;
                    vm.Message = categoryResponse.Message;
                }
                else
                    vm.Categories = categoryResponse.Data;
                #endregion

                #region Read Categories
                var brandResponse = this._brandService.Read();
                if (!categoryResponse.Success)
                {
                    vm.Success = brandResponse.Success;
                    vm.Message = brandResponse.Message;
                }
                else
                    vm.Brands = brandResponse.Data;
                #endregion

                #region Read Groups
                //var groupResponse = this._groupService.Read();
                //if (!groupResponse.Success)
                //{
                //    vm.Success = groupResponse.Success;
                //    vm.Message = groupResponse.Message;
                //}
                //else
                //    vm.Groups = groupResponse.Data;
                #endregion

                #region Shopping Cart Count
                var shoppingCartCountResponse = this._shoppingCartService.Count();
                if (!shoppingCartCountResponse.Success)
                {
                    vm.Success = shoppingCartCountResponse.Success;
                    vm.Message = shoppingCartCountResponse.Message;
                }
                else
                    vm.ShoppingCartItemsCount = shoppingCartCountResponse.Data;
                #endregion

                #region Shopping Cart Items
                vm.ShoppingCartItems = new List<ShoppingCartItem>();
                var shoppingCartResponse = this._shoppingCartService.Get();
                if (shoppingCartResponse.Success)
                {
                    vm.ShoppingCartItems.AddRange(shoppingCartResponse.Data);
                }
                #endregion

                #region Read Informations
                var informationResponse = this._informationService.ReadAsKeyValue();
                if (!informationResponse.Success)
                {
                    vm.Success = informationResponse.Success;
                    vm.Message = informationResponse.Message;
                }
                else
                    vm.Informations = informationResponse.Data;
                #endregion

                #region Read Contents
                var contentResponse = this._contentService.ReadAsKeyValue();
                if (!contentResponse.Success)
                {
                    vm.Success = contentResponse.Success;
                    vm.Message = contentResponse.Message;
                }
                else
                    vm.Contents = contentResponse.Data;
                #endregion


                var company = _companyService.Read();
                if (!company.Success)
                {
                    vm.Success = company.Success;
                    vm.Message = company.Message;
                }
                else
                    vm.Company = company.Data;


                var logged = _authUtility.GetLogged();
                if (logged.Success)
                {
                    vm.IsLogin = true;
                    vm.CustomerUser = new CustomerUser
                    {
                        Id = 0,
                        Customer = logged.Data.Customer,
                        CustomerRole = null,
                        ManagerRoleId = "",
                        MoneyPointAmount = logged.Data.MoneyPointAmount
                    };
                }

                ViewData["Url"] = company.Data.CompanyContact.WebUrl + context.HttpContext.Request.Path.Value;

            }


            base.OnActionExecuted(context);
        }
        #endregion
    }
}
