﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class ProductInformationSubscriptionController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<ControllerBase> _logger;

        private readonly IProductInformationSubscriptionService _productInformationSubscriptionService;
        #endregion

        #region Constructors
        public ProductInformationSubscriptionController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<ProductInformationCommentController> logger, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, IProductInformationSubscriptionService productInformationSubscriptionService, ICategoryService categoryService,  IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._productInformationSubscriptionService = productInformationSubscriptionService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public JsonResult Create([FromBody] ProductInformationSubscription productInformationSubscription)
        {
            return Json(this._productInformationSubscriptionService.Create(productInformationSubscription));
        }



        #endregion
    }
}
