﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class OrderController : Base.ControllerBase
    {
        #region Fields
        private readonly IOrderService _orderService;

        private readonly ICityService _cityService;

        private readonly IPaymentService _paymentService;

        private readonly IPaymentTypeService _paymentTypeService;

        private readonly ICustomerUserService _customerUserService;

        private readonly INeighborhoodService _neighborhoodService;

        private readonly IDistrictService _districtService;

        private readonly ISmsService _smsService;

        private readonly IEmailService _emailService;

        private readonly IEmailTemplateService _emailTemplateService;

        private readonly ICookieHelper _cookieHelper;

        private readonly IDiscountCouponService _discountCouponService;

        private readonly IShoppingCartDiscountedService _shoppingCartDiscountedService;

        private readonly Configuration _configuration;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public OrderController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, IOrderService orderService, ICityService cityService, IPaymentService paymentService, IPaymentTypeService paymentTypeService, ILogger<OrderController> logger, IShoppingCartService shoppingCartService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IProductService productService, IBrandService brandService, ICustomerUserService customerUserService,
            INeighborhoodService neighborhoodService, IDistrictService districtService, IShoppingCartDiscountedService shoppingCartDiscountedService, ISmsService smsService, ICookieHelper cookieHelper, IDiscountCouponService discountCouponService, IConfigrationService configrationService, IEmailTemplateService emailTemplateService,
            IEmailService emailService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._orderService = orderService;
            this._cityService = cityService;
            this._paymentService = paymentService;
            this._paymentTypeService = paymentTypeService;
            this._customerUserService = customerUserService;
            this._neighborhoodService = neighborhoodService;
            this._districtService = districtService;
            this._smsService = smsService;
            this._cookieHelper = cookieHelper;
            this._discountCouponService = discountCouponService;
            this._shoppingCartDiscountedService = shoppingCartDiscountedService;
            this._configuration = configrationService.Read().Data;
            this._emailTemplateService = emailTemplateService;
            this._emailService = emailService;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var vm = new Application.Client.ViewModels.Orders.Index
            {
                Success = true
            };

            var shoppingCartCountResponse = base._shoppingCartService.Count();
            if (!shoppingCartCountResponse.Success)
            {
                vm.Success = shoppingCartCountResponse.Success;
                vm.Message = shoppingCartCountResponse.Message;

                return View(vm);
            }

            if (shoppingCartCountResponse.Data == 0)
                return new RedirectResult("/ShoppingCart");


            var paymentTypeResponse = this._paymentTypeService.Read();
            if (!paymentTypeResponse.Success)
            {
                vm.Message = paymentTypeResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.PaymentTypes = paymentTypeResponse.Data.Where(x => x.Id != "MP" && x.Id != "IK").ToList();

            var countryId = this._settingService.DefaultCountryId;
            vm.CountryId = countryId;
            var cityResponse = this._cityService.ReadByCountryId(countryId);
            if (!cityResponse.Success)
            {
                vm.Message = cityResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Cities = cityResponse.Data;


            var shoppingCartDiscountedResponse = this._shoppingCartDiscountedService.Read();
            if (shoppingCartDiscountedResponse.Success)
            {
                vm.ShoppingCartDiscounteds = shoppingCartDiscountedResponse.Data;
            }



            if (this._cookieHelper.Exist("DiscountCoupon"))
            {
                var cookie = this._cookieHelper.Read("DiscountCoupon");
                if (cookie != null)
                {
                    var discountCouponCode = cookie[0].Value;
                    var dicountCouponResponse = this._discountCouponService.Read(discountCouponCode);
                    if (dicountCouponResponse.Success)
                    {
                        vm.DiscountCoupon = dicountCouponResponse.Data;
                    }
                }
            }


            vm.Configuration = this._configuration;

            var logged = _authUtility.GetLogged();
            if (logged.Success)
            {
                var customerUserDetail = this._customerUserService.Read(logged.Data.Id);
                if (customerUserDetail.Success)
                {
                    vm.CustomerUserDetail = customerUserDetail.Data;
                    if (customerUserDetail.Data.Customer.CustomerAddresses.Count > 0)
                    {
                        customerUserDetail.Data.Customer.DefaultCustomerAddress = customerUserDetail.Data.Customer.CustomerAddresses.FirstOrDefault(dc => dc.Default);
                    }

                    if (customerUserDetail.Data.Customer.CustomerInvoiceInformations.Count > 0)
                    {
                        customerUserDetail.Data.Customer.DefaultCustomerInvoiceInformation = customerUserDetail.Data.Customer.CustomerInvoiceInformations.FirstOrDefault(dc => dc.Default);
                    }
                }

            }


            return View(vm);
        }

        /// <summary>
        /// Sipariş sonrası gösterilecek olan sayfadır.
        /// </summary>
        /// <returns></returns>
        [IgnoreAntiforgeryToken]
        [AllowAnonymous]
        public IActionResult Done(string status, string description)
        {
            var vm = new Application.Client.ViewModels.Orders.Done
            {
                Success = true,
                Status = status == "success",
                DescriptionError = description,
                OnlinePaymentPaid = false
            };

            var result = this._orderService.GetLastOrder();
            if (result.Success)
            {
                //var shoppingCartItemsResponse = this._shoppingCartService.Get();

                //#region Google Ads Done
                //vm.GoogleAdsDone = this._googleAdsService.Done(result.Data);
                //#endregion

                //#region Google Analytics Done
                //vm.GoogleAnalyticsDone = this._googleAnalyticsService.Done(result.Data, shoppingCartItemsResponse.Data);
                //#endregion

                //#region Facebook Pixel Done
                //vm.FacebookPixelDone = this._facebookPixelService.Done(result.Data);
                //#endregion

                var paid = false;

                vm.PaymentTypeId = result.Data.PaymentTypeId;
                vm.Amount = result.Data.Amount;
                vm.OrderId = result.Data.Id;


                var smsText = string.Empty;
                EmailTemplate emailTemplate = null;
                Dictionary<string, string> parameters = new();
                parameters.Add("#CustomerName", result.Data.CustomerName.ToUpper());
                parameters.Add("#Iban", this._configuration.Iban);
                parameters.Add("#OrderId", result.Data.Id.ToString());
                parameters.Add("#Company", this._configuration.Company);
                parameters.Add("#Amount", result.Data.Amount.ToString());
                parameters.Add("#PhoneNumber", this._configuration.PhoneNumber);


                if (result.Data.PaymentTypeId == PaymentTypes.Havale)
                {
                    paid = true;

                    //smsText = $"Sevgili #CustomerName, #OrderId'nolu siparişinizi aldık. #Amount TL Tutarındaki ödemeni #Company adına #Iban iban numarasına yapabilirsiniz. Ödeme açıklamasına Sipariş No'sunu yazmayı unutmayınız. Banka ödemesi bizlere ulaştığı an siparişini işleme alacağız. Bizlere #PhoneNumber no'lu numaradan ulaşabilirsiniz.";

                    //var orderTransferHtml = this._emailTemplateService.OrderTransferTemplate(result.Data.Id);
                    //if (orderTransferHtml.Success)
                    //{
                    //    emailTemplate = orderTransferHtml.Data;
                    //}
                }
                else if (result.Data.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit || result.Data.PaymentTypeId == PaymentTypes.KapıdaNakit)
                {
                    paid = true;


                }
                else if (result.Data.PaymentTypeId == PaymentTypes.OnlineOdeme)
                {
                    var payment = _paymentService.Read(result.Data.Id, result.Data.PaymentTypeId);
                    if (payment.Success && payment.Data.Paid)
                    {
                        paid = true;
                        vm.OnlinePaymentPaid = true;
                        //smsText = $"Sevgili #CustomerName, #OrderId'nolu siparişinizi aldık. Doğaya Kattığın Değer İçin Sonsuz Teşekkürler. Bizlere #PhoneNumber no'lu numaramızdan ulaşabilirsiniz.";

                        //var orderTransferHtml = this._emailTemplateService.NewOrderTemplate(result.Data.Id);
                        //if (orderTransferHtml.Success)
                        //{
                        //    emailTemplate = orderTransferHtml.Data;
                        //}
                    }
                }

                if (paid)
                {
                    this._smsService.Send(result.Data.PhoneNumber, null, result.Data.PaymentTypeId, parameters);
                }

                this._orderService.Clear();
                this._shoppingCartService.Clear();
                this._discountCouponService.Clear();
            }

            return View(vm);
        }

        [HttpPost]
        public JsonResult Create([FromBody] Order order)
        {
            var shoppingCartResponse = this._shoppingCartService.Get();
            if (!shoppingCartResponse.Success)
            {
                return Json(new Response
                {
                    Success = false,
                    Message = "Sepetinizde ürün bulunmamaktadır"
                });
            }


            var logged = _authUtility.GetLogged();
            if (logged.Success)
            {
                var customerUserDetail = this._customerUserService.Read(logged.Data.Id);
                order.Customer = new Customer();
                order.Customer.CustomerUser = customerUserDetail.Data;
            }


            var totalUnitPrice = shoppingCartResponse.Data.Sum(sci => sci.ProductInformation.ProductInformationPrice.TotalUnitPrice);
            var listTotalUnitPrice = shoppingCartResponse.Data.Sum(sci => sci.ProductInformation.ProductInformationPrice.ListUnitPrice * sci.Quantity);



            order.OrderDetails = new List<OrderDetail>();


            foreach (var dLoop in shoppingCartResponse.Data)
            {
                /*
                   * Kargo Ücretsiz mi ? 
                 */
                var orderPayor = totalUnitPrice >= _configuration.CargoLimit || dLoop.ProductInformation.Payor;

                /*
                     * X al Y Öde için TotalUnitPrice'ı Quantity bölüyoruz sepet tutarını tutturmak için
                */
                var unitPrice = dLoop.ProductInformation.ProductInformationPrice.TotalUnitPrice > 0 ? dLoop.ProductInformation.ProductInformationPrice.TotalUnitPrice / dLoop.Quantity : 0;


                order.OrderDetails.Add(new OrderDetail
                {
                    ProductInformationId = dLoop.ProductInformationId,
                    Quantity = dLoop.Quantity,
                    UnitDiscount = dLoop.ProductInformation.ProductInformationPrice.ListUnitPrice - unitPrice,
                    ListUnitPrice = dLoop.ProductInformation.ProductInformationPrice.ListUnitPrice,
                    UnitCost = dLoop.ProductInformation.ProductInformationPrice.UnitCost,
                    UnitPrice = unitPrice,
                    VatRate = dLoop.ProductInformation.ProductInformationPrice.VatRate,
                    Payor = orderPayor
                });
            }


            /*Septte İndirim*/
            var shoppingCartDiscount = 0M;
            var shoppingCartDiscountedResponse = this._shoppingCartDiscountedService.Read();
            if (shoppingCartDiscountedResponse.Success)
            {

                if (shoppingCartDiscountedResponse.Data.Count > 0)
                {

                    var shoppingCartDiscounted = shoppingCartDiscountedResponse.Data.Where(x => x.ShoppingCartDiscountedSetting.Limit <= totalUnitPrice).OrderByDescending(x => x.ShoppingCartDiscountedSetting.Limit).FirstOrDefault();

                    if (shoppingCartDiscounted != null && shoppingCartResponse.Data.Count > 0)
                    {
                        shoppingCartDiscount = shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;

                        if (shoppingCartDiscounted.ShoppingCartDiscountedSetting.DiscountIsRate)
                            shoppingCartDiscount = totalUnitPrice * shoppingCartDiscounted.ShoppingCartDiscountedSetting.Discount;
                    }
                }
            }






            order.ShoppingCartDiscount = shoppingCartDiscount;
            order.Total = totalUnitPrice - shoppingCartDiscount;
            order.ListTotal = listTotalUnitPrice;
            var cargoFee = totalUnitPrice >= _configuration.CargoLimit ? 0 : _configuration.CargoAmount;


            order.Total += cargoFee;
            order.ListTotal += cargoFee;
            order.CargoFee = cargoFee;
            order.SurchargeFee = 0;

            /*
                  * Eğer müşteri indirim kuponu kullanmışsa payment tablosuna ekliyoruz.
             */
            if (this._cookieHelper.Exist("DiscountCoupon"))
            {
                var cookie = this._cookieHelper.Read("DiscountCoupon");
                if (cookie != null)
                {
                    var discountCouponCode = cookie[0].Value;
                    var dicountCouponResponse = this._discountCouponService.Read(discountCouponCode);
                    if (dicountCouponResponse.Success)
                    {

                        var discountCouponDiscount = 0M;

                        if (dicountCouponResponse != null && totalUnitPrice > dicountCouponResponse.Data.DisountCouponSetting.Limit)
                        {
                            discountCouponDiscount = dicountCouponResponse.Data.DisountCouponSetting.Discount;

                            if (dicountCouponResponse.Data.DisountCouponSetting.DiscountIsRate)
                                discountCouponDiscount = totalUnitPrice * dicountCouponResponse.Data.DisountCouponSetting.Discount;


                            order.Payments.Add(new Payment
                            {
                                Amount = discountCouponDiscount,
                                PaymentTypeId = "IK",
                                DiscountCouponId = dicountCouponResponse.Data.Id
                            });
                        }


                    }
                }
            }



            return Json(this._orderService.Create(order));
        }
        #endregion
    }
}