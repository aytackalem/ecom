﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.Parameters.ShoppingCarts;
using ECommerce.Application.Client.ViewModels.ShoppingCarts;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class ShoppingCartController : Base.ControllerBase
    {
        #region Fields
        private readonly IDiscountCouponService _discountCouponService;

        private readonly IGetXPayYSettingService _getXPayYSettingService;

        private readonly IShoppingCartDiscountedProductService _shoppingCartDiscountedProductService;

        private readonly IShoppingCartDiscountedService _shoppingCartDiscountedService;

        private readonly IConfigrationService _configrationService;

        private readonly ICookieHelper _cookieHelper;
        #endregion

        #region Constructors
        public ShoppingCartController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, IShoppingCartService shoppingCartService, ICategoryService categoryService, IProductService productService, IBrandService brandService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, ICookieHelper cookieHelper, IDiscountCouponService discountCouponService, IGetXPayYSettingService getXPayYSettingService, IShoppingCartDiscountedService shoppingCartDiscountedService, IShoppingCartDiscountedProductService shoppingCartDiscountedProductService, IConfigrationService configrationService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._discountCouponService = discountCouponService;
            this._cookieHelper = cookieHelper;
            this._getXPayYSettingService = getXPayYSettingService;
            this._shoppingCartDiscountedService = shoppingCartDiscountedService;
            this._shoppingCartDiscountedProductService = shoppingCartDiscountedProductService;
            this._configrationService = configrationService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var vm = new Index
            {
                Success = true,
                Message = "Sepet ekranı başarılı bir şekilde getirildi."
            };



            var shoppingCartDiscountedResponse = this._shoppingCartDiscountedService.Read();
            if (shoppingCartDiscountedResponse.Success)
            {
                vm.ShoppingCartDiscounteds = shoppingCartDiscountedResponse.Data;
            }


            var shoppingCartDiscountedProductResponse = this._shoppingCartDiscountedProductService.Read();
            if (shoppingCartDiscountedProductResponse.Success)
            {
                /*
                   * Sepette aynı ürün varsa indirimli ürünün içinden kaldırılır. (Şuan kapatıldı)
                   * Şimdilik aynı ürün varsa sepette gösterelecek 
                */

                //var productInformationsIds = vm.ShoppingCartItems.Where(x => !x.ShoppingCartDiscount).Select(y => y.ProductInformationId);
                //vm.ShoppingCartDiscountedProducts = shoppingCartDiscountedProductResponse.Data.Where(x => !productInformationsIds.Contains(x.ProductInformation.Id)).ToList();

                vm.ShoppingCartDiscountedProducts = shoppingCartDiscountedProductResponse.Data;
            }



            var configrationService = this._configrationService.Read();
            if (configrationService.Success)
            {
                vm.Configuration = configrationService.Data;
            }



            if (this._cookieHelper.Exist("DiscountCoupon"))
            {
                var cookie = this._cookieHelper.Read("DiscountCoupon");
                if (cookie != null)
                {
                    var discountCouponCode = cookie[0].Value;
                    var dicountCouponResponse = this._discountCouponService.Read(discountCouponCode);
                    if (dicountCouponResponse.Success)
                    {
                        vm.DiscountCoupon = dicountCouponResponse.Data;
                    }
                }
            }

            return View(vm);
        }


        public JsonResult Add([FromBody] AddRequest addRequest)
        {
            var productResponse = this._productService.ReadInfo(addRequest.ProductInformationId);
            if (!productResponse.Success)
                return new JsonResult(new Create
                {
                    Success = false,
                    Message = "Ürün bulunamadı."
                });

            var shoppingCart = this._shoppingCartService.Add(addRequest);

            var result = this._shoppingCartService.Mapping(shoppingCart.ShoppingCartItems);
            if (result.Success)
            {
                shoppingCart.ShoppingCartItems = result.Data;
            }

            return Json(shoppingCart);
        }

        public JsonResult Remove([FromBody] RemoveRequest removeRequest)
        {
            return Json(this._shoppingCartService.Remove(removeRequest));
        }

        public JsonResult Update([FromBody] UpdateRequest updateRequest)
        {
            return Json(this._shoppingCartService.Update(updateRequest));
        }

        public JsonResult Clear()
        {
            return Json(this._shoppingCartService.Clear());
        }

        public JsonResult Count()
        {
            return Json(this._shoppingCartService.Count());
        }
        #endregion
    }
}
