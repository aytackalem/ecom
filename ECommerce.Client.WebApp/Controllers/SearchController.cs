﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class SearchController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<SearchController> _logger;

        private readonly ISearchService _searchService;


        #endregion

        #region Constructors
        public SearchController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<SearchController> logger, ISearchService searchService, IProductService productService, IBrandService brandService, ICategoryService categoryService,  IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IShoppingCartService shoppingCartService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService,  shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._searchService = searchService;
            #endregion
        }
        #endregion

        #region Methods
        public JsonResult Read(string q)
        {
            return Json(this._searchService.ReadInfo(q));
        }

        public IActionResult Index(string q, int p = 0, int pc = 20, int st = 0)
        {
            if (string.IsNullOrEmpty(q))
                return View(new Application.Client.ViewModels.Searches.Index
                {
                    Success = false,
                    Message = "Lütfen bir değer giriniz."
                });

            var vm = new Application.Client.ViewModels.Searches.Index
            {
                Keyword = q,
                Success = true,
                Message = "Ürünler başarılı bir şekilde getirildi."
            };

            var productResponse = base._productService.InfiniteReadByKeyword(p, pc, st, q);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;

            return View(vm);
        }
        #endregion
    }
}