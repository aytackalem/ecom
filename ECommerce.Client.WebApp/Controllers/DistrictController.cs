﻿using ECommerce.Application.Client.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class DistrictController : Controller
    {
        #region Fields
        private readonly ILogger<ControllerBase> _logger;

        private readonly IDistrictService _districtService;
        #endregion

        #region Constructors
        public DistrictController(ILogger<DistrictController> logger, IDistrictService districtService)
        {
            #region Fields
            this._logger = logger;
            this._districtService = districtService;
            #endregion
        }
        #endregion

        #region Methods
        public JsonResult Read(int cityId)
        {
            return Json(this._districtService.ReadByCityId(cityId));
        }
        #endregion
    }
}
