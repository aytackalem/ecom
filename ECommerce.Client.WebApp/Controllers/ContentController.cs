﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class ContentController : Base.ControllerBase
    {
        #region Fields
        private readonly IFileCaching _fileCaching;
        #endregion

        #region Constructors
        public ContentController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<ContentController> logger, IShoppingCartService shoppingCartService, IContentService contentService, ICategoryService categoryService, IInformationService informationService, IAuthUtility<CustomerUser> authUtility, IProductService productService, IBrandService brandService, IFileCaching fileCaching, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._fileCaching = fileCaching;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index(int id, string url)
        {
            var vm = new Application.Client.ViewModels.Contents.Index
            {
                Success = true,
                Message = "İçerik ekranı başarılı bir şekilde getirildi."
            };

            #region Caching
            var key = $"{id}-co";
            if (this._fileCaching.Contains(key) && (!Request.QueryString.HasValue || (Request.QueryString.HasValue && !Request.QueryString.Value.Contains("caching=false"))))
            {
                vm = this._fileCaching.Read<Application.Client.ViewModels.Contents.Index>(key);

                return View(vm);
            }
            #endregion

            var contentResponse = base._contentService.Read(id);
            if (!contentResponse.Success)
            {
                vm.Message = contentResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Content = contentResponse.Data;

            #region Caching
            if (vm.Success && Request.QueryString.HasValue && Request.QueryString.Value.Contains("caching=false"))
                this._fileCaching.Write(key, vm);
            #endregion

            return View(vm);
        }
        #endregion
    }
}
