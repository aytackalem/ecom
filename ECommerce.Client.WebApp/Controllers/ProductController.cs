﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Products;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class ProductController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<ProductController> _logger;

        private readonly IFileCaching _fileCaching;
        #endregion

        #region Constructors
        public ProductController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<ProductController> logger, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IFileCaching fileCaching, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            this._logger = logger;
            this._fileCaching = fileCaching;
        }
        #endregion

        #region Methods
        public IActionResult Index(string url, int productInformationId)
        {
            var vm = new Index
            {
                Success = true,
                Message = "Ürün ekranı başarılı bir şekilde getirildi."
            };

            #region Caching
            var key = $"{productInformationId}-p";
            if (this._fileCaching.Contains(key) && (!Request.QueryString.HasValue || (Request.QueryString.HasValue && !Request.QueryString.Value.Contains("caching=false"))))
            {
                vm = this._fileCaching.Read<Index>(key);

                return View(vm);
            }
            #endregion

            var productInformationResponse = this._productService.Read(productInformationId);
            if (!productInformationResponse.Success)
            {
                vm.Message = productInformationResponse.Message;
                vm.Success = false;

                return View(vm);
            }


            productInformationResponse.Data.Company = _companyService.Read().Data;


            vm.ProductInformation = productInformationResponse.Data;

            var infiniteReadResponse = this._productService.InfiniteReadByCategoryId(vm.ProductInformation.Product.CategoryId, 0, 15, 1);
            if (infiniteReadResponse.Success)
                vm.ProductInformation.CategoryProducts.AddRange(infiniteReadResponse.Data.Where(x => x.Id != productInformationId));

            #region Caching
            if (vm.Success && Request.QueryString.HasValue && Request.QueryString.Value.Contains("caching=false"))
                this._fileCaching.Write(key, vm);
            #endregion

            return View(vm);
        }


        public IActionResult IndexPartial(int productInformationId)
        {
            var product = this._productService.Read(productInformationId).Data;
            product.Company = _companyService.Read().Data;

            return PartialView(product);
        }
        #endregion
    }
}
