﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class ProductInformationCommentController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<ControllerBase> _logger;

        private readonly IProductInformationCommentService _productInformationCommentService;
        #endregion

        #region Constructors
        public ProductInformationCommentController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<ProductInformationCommentController> logger, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, IProductInformationCommentService productInformationCommentService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._productInformationCommentService = productInformationCommentService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public JsonResult Create([FromBody] ProductInformationComment productInformationComment)
        {
            return Json(this._productInformationCommentService.Create(productInformationComment));
        }

        [HttpGet]
        public JsonResult InfiniteRead(int productInformationId, int p = 0)
        {
            return Json(this._productInformationCommentService.InfiniteRead(productInformationId, p, 20));
        }
        #endregion
    }
}
