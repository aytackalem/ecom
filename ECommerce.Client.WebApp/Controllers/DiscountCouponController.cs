﻿using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace ECommerce.Client.WebApp.Controllers
{
    public class DiscountCouponController : Controller
    {
        #region Fields
        private readonly ILogger<ControllerBase> _logger;

        private readonly IDiscountCouponService _discountCouponService;

        private readonly ICookieHelper _cookieHelper;
        #endregion

        #region Constructors
        public DiscountCouponController(ILogger<DiscountCouponController> logger, IDiscountCouponService discountCouponService, ICookieHelper cookieHelper)
        {
            #region Fields
            this._logger = logger;
            this._discountCouponService = discountCouponService;
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public JsonResult Read([FromBody] DiscountCoupon discountCoupon)
        {
            var response = this._discountCouponService.Read(discountCoupon.Code);

            if (response.Success)
            {
                this._discountCouponService.Set(discountCoupon.Code);
            }

            return Json(response);
        }

        public JsonResult Clear()
        {
            return Json(this._discountCouponService.Clear());
        }

        #endregion
    }
}
