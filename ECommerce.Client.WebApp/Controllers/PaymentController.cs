﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Brands;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class PaymentController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<PaymentController> _logger;

        private readonly IPosService _posService;

        private readonly IPaymentService _paymentService;
        #endregion

        #region Constructors
        public PaymentController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<PaymentController> logger, IPosService posService, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IPaymentService paymentService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._posService = posService;
            this._paymentService = paymentService;
            #endregion
        }
        #endregion

        #region Methods
        [IgnoreAntiforgeryToken]
        public IActionResult Index(string status, string description, bool allowFrame)
        {
            if(!allowFrame)
            {
                return new RedirectResult($"/Order/Done?Status={status}&Description={description}");
            }

            return View(new Application.Client.ViewModels.Payments.Index
            {
                AllowFrame = allowFrame,
                Success = status.ToLower() == "success" ? true : false,
                Message = description.CleanParameter(),
                DoneUrl = "/Order/Done"
            });
        }

        [IgnoreAntiforgeryToken]
        public RedirectResult Check3d()
        {
            var posResult = this._posService.Check3d();
            if (posResult.Success)
            {
                this._paymentService.UpdateSale(posResult.Data.OrderId);

                return new RedirectResult($"/Payment?Status=success&allowFrame={posResult.Data.AllowFrame}");
            }

            return new RedirectResult($"/Payment?Status=fail&Description={posResult.Message}&allowFrame={posResult.Data.AllowFrame}");
        }
        #endregion
    }
}
