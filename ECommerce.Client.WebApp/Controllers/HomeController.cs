﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Home;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using ECommerce.Application.Common.Parameters;
using ECommerce.Client.WebApp.Localize;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class HomeController : Base.ControllerBase
    {
        #region Fields
        private readonly IStringLocalizer<Resource> _resource;

        private readonly IShowcaseService _showcaseService;

        private readonly IFrequentlyAskedQuestionService _frequentlyAskedQuestionService;

        private readonly IFileCaching _fileCaching;

        private readonly IPropertyService _propertyService;

        private readonly INewBulletinService _newBulletinService;

        private readonly IContactFormService _contactFormService;

        private readonly ICaptchaService _captchaService;
        #endregion

        #region Constructors
        public HomeController(IStringLocalizer<Resource> resource, IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<HomeController> logger, IShoppingCartService shoppingCartService, IShowcaseService showcaseService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IFrequentlyAskedQuestionService frequentlyAskedQuestionService, IProductService productService, IBrandService brandService, IFileCaching fileCaching, IPropertyService propertyService, INewBulletinService newBulletinService, IContactFormService contactFormService, ICaptchaService captchaService, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._resource = resource;
            this._showcaseService = showcaseService;
            this._frequentlyAskedQuestionService = frequentlyAskedQuestionService;
            this._fileCaching = fileCaching;
            this._propertyService = propertyService;
            this._newBulletinService = newBulletinService;
            this._contactFormService = contactFormService;
            this._captchaService = captchaService;
            #endregion
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            var vm = new Application.Client.ViewModels.Home.Index
            {
                Success = true,
                Message = this._resource["Success"] //"Anasayfa başarılı bir şekilde getirildi."
            };

            #region Caching
            var key = "h";
            if (this._fileCaching.Contains(key) && (!Request.QueryString.HasValue || (Request.QueryString.HasValue && !Request.QueryString.Value.Contains("caching=false"))))
            {
                vm = this._fileCaching.Read<Application.Client.ViewModels.Home.Index>(key);

                return View(vm);
            }
            #endregion

            var showcaseResponse = this._showcaseService.Read();

            if (!showcaseResponse.Success)
            {
                vm.Message = showcaseResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Showcase = showcaseResponse.Data;

            #region Caching
            if (vm.Success && Request.QueryString.HasValue && Request.QueryString.Value.Contains("caching=false"))
                this._fileCaching.Write(key, vm);
            #endregion

            return View(vm);
        }

        public IActionResult About()
        {
            var vm = new About();

            return View(vm);
        }


        public IActionResult Contact()
        {
            var vm = new Contact();

            return View(vm);
        }

        public IActionResult Error()
        {
            var vm = new Error();

            return View(vm);
        }

        public IActionResult Faq(int id)
        {
            var vm = new Faq();
            var frequentlyAskedQuestionResponse = _frequentlyAskedQuestionService.Read();
            if (!frequentlyAskedQuestionResponse.Success)
            {
                vm.Message = frequentlyAskedQuestionResponse.Message;
                vm.Success = false;

                return View(vm);
            }
            vm.FrequentlyAskedQuestions = frequentlyAskedQuestionResponse.Data;
            return View(vm);
        }

        public IActionResult RegisterContract()
        {
            var vm = new RegisterContract();

            return View(vm);
        }
        public IActionResult DeliveryAndReturns()
        {
            var vm = new DeliveryAndReturns();

            return View(vm);
        }
        public IActionResult DistanceSalesAgreement()
        {
            var vm = new DistanceSalesAgreement();

            return View(vm);
        }
        public IActionResult Reset()
        {
            var vm = new Reset();

            return View(vm);
        }

        public IActionResult Kvkk()
        {
            var vm = new Kvkk();

            return View(vm);
        }

        public IActionResult PaymentInfo()
        {
            var vm = new PaymentInfo();

            return View(vm);
        }

        public IActionResult Find(string pv, int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Find
            {
                ProductInformations = new List<ProductInformation>(),
                DiscountedProductInformations = new List<ProductInformation>(),
                PropertyValueIds = new List<int>(),
                Success = true,
                Message = "Detaylı arama sayfası başarılı bir şekilde getirildi."
            };

            var propertyResponse = this._propertyService.Read();
            if (!propertyResponse.Success)
            {
                vm.Message = propertyResponse.Message;
                vm.Success = false;

                return View(vm);
            }
            vm.Properties = propertyResponse.Data;

            if (!string.IsNullOrEmpty(pv))
            {
                List<int> propertyValueIds = new();

                propertyValueIds = pv
                    .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => int.Parse(x))
                    .ToList();
                vm.PropertyValueIds = propertyValueIds;

                var productResponse = this._productService.InfiniteReadByProperties(p, pc, st, propertyValueIds);
                if (!productResponse.Success)
                {
                    vm.Message = productResponse.Message;
                    vm.Success = false;

                    return View(vm);
                }
                vm.ProductInformations = productResponse.Data;
                vm.Page = productResponse.Page;
                vm.PagesCount = productResponse.PagesCount;
                vm.ProductCount = productResponse.RecordsCount;
                vm.PageProductCount = productResponse.Data.Count;
                vm.PageRecordsCount = productResponse.PageRecordsCount;
                vm.PageSorting = st;
            }
            else
            {
                vm.Page = p;
                vm.PageRecordsCount = pc;
                vm.PageSorting = st;
            }

            if (!string.IsNullOrEmpty(pv) && vm.ProductCount == 0)
            {
                var productResponse = base._productService.InfiniteReadByDiscount(50, 0, 10, 0);

                if (!productResponse.Success)
                {
                    vm.Message = productResponse.Message;
                    vm.Success = false;

                    return View(vm);
                }

                vm.DiscountedProductInformations = productResponse.Data;
            }

            return View(vm);
        }

        public IActionResult Discount(int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Application.Client.ViewModels.Categories.Index();

            var productResponse = base._productService.InfiniteReadByDiscount(50, p, pc, st);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;

            return View(vm);
        }

        public IActionResult SalesAssociate()
        {
            var vm = new SalesAssociate();

            return View(vm);
        }

        public IActionResult WholeSale()
        {
            var vm = new WholeSale();

            return View(vm);
        }

        public IActionResult ConsumerRights()
        {
            var vm = new ConsumerRights();

            return View(vm);
        }

        public IActionResult ShoppingProcedure()
        {

            var vm = new ShoppingProcedure();

            return View(vm);
        }

        public IActionResult ReturnExchange()
        {

            var vm = new ReturnExchange();

            return View(vm);
        }

        [HttpPost]
        public JsonResult NewBulletin([FromBody] NewBulletin newBulletin)
        {
            return Json(this._newBulletinService.Create(newBulletin));
        }

        [HttpPost]
        public JsonResult Contact([FromBody] CaptchaRequest<ContactForm> captchaRequest)
        {
            var verifyResponse = this._captchaService.Verify(captchaRequest);
            if (!verifyResponse.Success)
                return Json(verifyResponse);

            return Json(this._contactFormService.Create(captchaRequest.Data));
        }
        #endregion
    }
}
