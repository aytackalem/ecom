﻿using ECommerce.Application.Client.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ECommerce.Client.WebApp.Controllers
{
    public class NeighborhoodController : Controller
    {
        #region Fields
        private readonly ILogger<NeighborhoodController> _logger;

        private readonly INeighborhoodService _neighborhoodService;
        #endregion

        #region Constructors
        public NeighborhoodController(ILogger<NeighborhoodController> logger, INeighborhoodService neighborhoodService)
        {
            #region Fields
            this._logger = logger;
            this._neighborhoodService = neighborhoodService;
            #endregion
        }
        #endregion

        #region Methods
        public JsonResult Read(int districtId)
        {
            return Json(this._neighborhoodService.ReadByDistrictId(districtId));
        }
        #endregion
    }
}