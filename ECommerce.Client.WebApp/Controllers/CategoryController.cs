﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Categories;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Client.WebApp.Controllers
{
    public class CategoryController : Base.ControllerBase
    {
        #region Fields
        private readonly ILogger<CategoryController> _logger;

        private readonly IFileCaching _fileCaching;
        #endregion

        #region Constructors
        public CategoryController(IGoogleTagManagerService googleTagManagerService, IGoogleAnalyticsService googleAnalyticsService, IFacebookPixelService facebookPixelService, IGoogleAdsService googleAdsService, ILogger<CategoryController> logger, IShoppingCartService shoppingCartService, IProductService productService, IBrandService brandService, ICategoryService categoryService, IInformationService informationService, IContentService contentService, IAuthUtility<CustomerUser> authUtility, IFileCaching fileCaching, ICompanyService companyService, ISettingService settingService) : base(googleTagManagerService, googleAnalyticsService, facebookPixelService, googleAdsService, categoryService, shoppingCartService, informationService, contentService, authUtility, productService, brandService, companyService, settingService)
        {
            #region Fields
            this._logger = logger;
            this._fileCaching = fileCaching;
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// Kategori ana sayfa.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="url"></param>
        /// <param name="p">Geçerli sayfa. (page)</param>
        /// <param name="pc">Sayfada gösterilecek kayıt sayısı. (pageRecordsCount)</param>
        /// <param name="st">Sıralama kriteri. (sorting)</param>
        /// <returns></returns>
        public IActionResult Index(int id, string url, int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Application.Client.ViewModels.Categories.Index
            {
                Success = true,
                Message = "Kategori ekranı başarılı bir şekilde getirildi."
            };

            #region Check Parameters
            if (p < 0 || (pc != 20 && pc != 50) || st < 0)
                return Redirect("/Home/Error");
            #endregion

            #region Caching
            var key = $"{id}-{p}-{pc}-{st}-c";
            if (this._fileCaching.Contains(key) && (!Request.QueryString.HasValue || (Request.QueryString.HasValue && !Request.QueryString.Value.Contains("caching=false"))))
            {
                vm = this._fileCaching.Read<Application.Client.ViewModels.Categories.Index>(key);

                #region Check Url
                if ($"{url}-{id}-c" != vm.Category.Url)
                    return RedirectPermanent($"/{vm.Category.Url}");
                #endregion

                return View(vm);
            }
            #endregion

            var productResponse = this._productService.InfiniteReadByCategoryId(id, p, pc, st);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            #region Check Parameters
            if (p != 0 && p > (productResponse.PagesCount - 1))
                return Redirect("/Home/Error");
            #endregion

            var categoryResponse = base._categoryService.Read(id);
            if (!categoryResponse.Success)
                return Redirect("/Home/Error");

            #region Check Url
            if ($"{url}-{id}-c" != categoryResponse.Data.Url)
                return RedirectPermanent($"/{categoryResponse.Data.Url}");
            #endregion

            var categoryPropertiesResponse = base._categoryService.ReadCategoryProperties(id);
            if (!categoryPropertiesResponse.Success)
            {
                vm.Message = categoryPropertiesResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Properties = categoryPropertiesResponse.Data;
            vm.Category = categoryResponse.Data;
            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;

            #region Caching
            if (vm.Success && Request.QueryString.HasValue && Request.QueryString.Value.Contains("caching=false"))
                this._fileCaching.Write(key, vm);
            #endregion

            return View(vm);
        }

        public IActionResult IndexFilter(string pv, int id, string url, int p = 0, int pc = 20, int st = 0)
        {
            var vm = new Application.Client.ViewModels.Categories.Index
            {
                Success = true,
                Message = "Kategori ekranı başarılı bir şekilde getirildi."
            };

            var categoryResponse = base._categoryService.Read(id);
            if (!categoryResponse.Success)
            {
                vm.Message = categoryResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            var categoryPropertiesResponse = base._categoryService.ReadCategoryProperties(id);
            if (!categoryPropertiesResponse.Success)
            {
                vm.Message = categoryPropertiesResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            List<int> propertyValueIds = new();
            if (!string.IsNullOrEmpty(pv))
            {
                propertyValueIds = pv
                    .Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => int.Parse(x))
                    .ToList();
                vm.PropertyValueIds = propertyValueIds;
            }



            var productResponse = this._productService.InfiniteReadByCategoryId(id, p, pc, st, propertyValueIds);
            if (!productResponse.Success)
            {
                vm.Message = productResponse.Message;
                vm.Success = false;

                return View(vm);
            }

            vm.Properties = categoryPropertiesResponse.Data;
            vm.Category = categoryResponse.Data;
            vm.ProductInformations = productResponse.Data;
            vm.Page = productResponse.Page;
            vm.PagesCount = productResponse.PagesCount;
            vm.ProductCount = productResponse.RecordsCount;
            vm.PageProductCount = productResponse.Data.Count;
            vm.PageRecordsCount = productResponse.PageRecordsCount;
            vm.PageSorting = st;


            return View(vm);
        }
        #endregion
    }
}
