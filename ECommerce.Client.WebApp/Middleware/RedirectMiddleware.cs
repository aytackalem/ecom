﻿//using ECommerce.Application.Client.Interfaces.Services;
//using Microsoft.AspNetCore.Http;
//using System.Threading.Tasks;

//namespace ECommerce.Client.WebApp.Middleware
//{
//    public class RedirectMiddleware
//    {
//        #region Fields
//        private readonly RequestDelegate _next;

//        private readonly IRedirectService _redirectService;
//        #endregion

//        #region Constructors
//        public RedirectMiddleware(RequestDelegate next, IRedirectService redirectService)
//        {
//            #region Fields
//            this._next = next;
//            this._redirectService = redirectService;
//            #endregion
//        }
//        #endregion

//        #region Methods
//        public async Task InvokeAsync(HttpContext context)
//        {
//            var redirect = true;

//            var path = context.Request.Path.Value;
//            if (path != "/" && path.EndsWith("/"))
//                path = path.TrimEnd('/');

//            var queryString = string.Empty;
//            if (context.Request.QueryString.HasValue)
//                queryString = context.Request.QueryString.Value;

//            /* Check project url */
//            if (path.EndsWith(".map") ||
//                path.EndsWith(".jpg") ||
//                path.EndsWith(".png") ||
//                path.EndsWith(".svg") ||
//                path.EndsWith(".js") ||
//                path.EndsWith(".css") ||
//                path.EndsWith(".eot") ||
//                path.EndsWith(".woff") ||
//                path.EndsWith(".woff2") ||
//                path.EndsWith(".ico") ||
//                path.EndsWith(".gif") ||
//                path.EndsWith("-p") ||
//                path.EndsWith("-pc") ||
//                path.EndsWith("-c") ||
//                path.EndsWith("-co") ||
//                path.EndsWith("-b") ||
//                path.EndsWith("-pv") ||
//                path.EndsWith("-g") ||
//                path.EndsWith("-gc") ||
//                path.EndsWith("-gcc") ||
//                path.EndsWith("-gcd") ||
//                path.EndsWith("-gcn") ||
//                path.EndsWith("-i") ||
//                path.ToLowerInvariant() == "/discount" ||
//                path.ToLowerInvariant() == "/fidan-bul" ||
//                path.ToLowerInvariant() == "/home/newbulletin" ||
//                path.ToLowerInvariant() == "/home/about" ||
//                path.ToLowerInvariant() == "/home/paymentinfo" ||
//                path.ToLowerInvariant() == "/home/contact" ||
//                path.ToLowerInvariant() == "/home/faq" ||
//                path.ToLowerInvariant() == "/home/find" ||
//                path.ToLowerInvariant() == "/home/registercontract" ||
//                path.ToLowerInvariant() == "/home/reset" ||
//                path.ToLowerInvariant() == "/home/kvkk" ||
//                path.ToLowerInvariant() == "/home/salesassociate" ||
//                path.ToLowerInvariant() == "/home/error" ||
//                path.ToLowerInvariant() == "/home/discount" ||
//                path.ToLowerInvariant() == "/home/wholesale" ||
//                path.ToLowerInvariant() == "/home/consumerrights" ||
//                path.ToLowerInvariant() == "/home/find" ||
//                path.ToLowerInvariant() == "/home/returnexchange" ||
//                path.ToLowerInvariant() == "/account" ||
//                path.ToLowerInvariant() == "/account/register" ||
//                path.ToLowerInvariant() == "/account/userverification" ||
//                path.ToLowerInvariant() == "/account/login" ||
//                path.ToLowerInvariant() == "/account/logout" ||
//                path.ToLowerInvariant() == "/account/forgotpassword" ||
//                path.ToLowerInvariant() == "/account/resetpassword" ||
//                path.ToLowerInvariant() == "/account/memberchangepassword" ||
//                path.ToLowerInvariant() == "/search" ||
//                path.ToLowerInvariant() == "/search/read" ||
//                path.ToLowerInvariant() == "/shoppingcart" ||
//                path.ToLowerInvariant() == "/shoppingcart/add" ||
//                path.ToLowerInvariant() == "/shoppingcart/remove" ||
//                path.ToLowerInvariant() == "/shoppingcart/update" ||
//                path.ToLowerInvariant() == "/shoppingcart/clear" ||
//                path.ToLowerInvariant() == "/shoppingcart/count" ||
//                path.ToLowerInvariant() == "/productinformationsubscription/create" ||
//                path.ToLowerInvariant() == "/productinformationcomment/create" ||
//                path.ToLowerInvariant() == "/productinformationcomment/infiniteread" ||
//                path.ToLowerInvariant() == "/payment" ||
//                path.ToLowerInvariant() == "/payment/check3d" ||
//                path.ToLowerInvariant() == "/order" ||
//                path.ToLowerInvariant() == "/order/create" ||
//                path.ToLowerInvariant() == "/order/done" ||
//                path.ToLowerInvariant() == "/neighborhood/read" ||
//                path.ToLowerInvariant() == "/district/read" ||
//                path.ToLowerInvariant() == "/discountcoupon/read" ||
//                path.ToLowerInvariant() == "/discountcoupon/clear" ||
//                path.ToLowerInvariant() == "/account/memberupdate" ||
//                path.ToLowerInvariant() == "/product/variants" ||
//                path.ToLowerInvariant() == "/product/indexpartial" ||
//                path.ToLowerInvariant() == "/account/customeraddressespartial" ||
//                path.ToLowerInvariant() == "/account/customeraddressdetailpartial" ||
//                path.ToLowerInvariant() == "/account/customeraddressupsert" ||
//                path.ToLowerInvariant() == "/account/customeraddressremove" ||
//                path.ToLowerInvariant() == "/account/customeraddressdefault" ||
//                path.ToLowerInvariant() == "/account/customerinvoiceinformationspartial" ||
//                path.ToLowerInvariant() == "/account/customerinvoiceinformationdetailpartial" ||
//                path.ToLowerInvariant() == "/account/customerinvoiceinformationupsert" ||
//                path.ToLowerInvariant() == "/account/customerinvoiceinformationremove" ||
//                path.ToLowerInvariant() == "/account/customerinvoiceinformationdefault" ||
//                (path == "/" && string.IsNullOrEmpty(queryString)) ||
//                (path == "/" && !string.IsNullOrEmpty(queryString) && queryString.Contains("caching")))
//                redirect = false;

//            if (redirect)
//            {
//                var redirectPath = this._redirectService.Find(path + queryString);
//                context.Response.Redirect(redirectPath, true);
//                return;
//            }
//            else
//                await _next(context);
//        }
//        #endregion
//    }
//}
