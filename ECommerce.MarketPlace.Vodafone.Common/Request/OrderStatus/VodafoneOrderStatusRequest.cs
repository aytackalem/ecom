﻿namespace ECommerce.MarketPlace.Vodafone.Common.Request.OrderStatus
{
    public class VodafoneOrderStatusRequest
    {
        public string newStatus { get; set; }
        public string orderId { get; set; }
        public string reasonCode { get; set; }
        public string reasonDescription { get; set; }
    }
}
