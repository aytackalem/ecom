﻿namespace ECommerce.MarketPlace.Vodafone.Common.Request.Shipment
{
    public class VodafoneShipmentRequest
    {
        #region Properties
        public List<string> orderIds { get; set; }
        #endregion
    }
}
