﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Shipment
{
    public class CustomerContactInfoList
    {
        public string fullName { get; set; }
        public string town { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public object postCode { get; set; }
        public string address { get; set; }
        public string type { get; set; }
    }
}
