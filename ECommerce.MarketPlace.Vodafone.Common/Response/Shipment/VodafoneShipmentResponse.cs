﻿using ECommerce.MarketPlace.Vodafone.Response.Base;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Shipment
{
    public class VodafoneShipmentResponse : ResponseBase
    {
        public List<string> shipmentRefNoList { get; set; }
        public List<CustomerContactInfoList> customerContactInfoList { get; set; }
        public List<string> shipmentCompanyList { get; set; }
        public List<object> failedPackagesList { get; set; }
    }
}
