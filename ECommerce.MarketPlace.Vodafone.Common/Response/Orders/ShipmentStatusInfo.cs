﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class ShipmentStatusInfo
    {
        public string status { get; set; }
        public string statusCode { get; set; }
        public string statusDesc { get; set; }
        public Date date { get; set; }
    }
}
