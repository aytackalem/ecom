﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class ShipmentInfo
    {
        public string refNo { get; set; }
        public CreatedDate createdDate { get; set; }
        public string shipmentCompany { get; set; }
        public List<ShipmentStatusInfo> shipmentStatusInfo { get; set; }
        public string recipient { get; set; }
        public bool customerShipping { get; set; }
        public DeadlineToShip deadlineToShip { get; set; }
    }
}
