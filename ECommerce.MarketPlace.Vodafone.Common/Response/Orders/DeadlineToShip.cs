﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class DeadlineToShip
    {
        public string value { get; set; }
    }
}
