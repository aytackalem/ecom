﻿namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class CreatedDate
    {
        #region Properties
        public string Value { get; set; } 
        #endregion
    }
}
