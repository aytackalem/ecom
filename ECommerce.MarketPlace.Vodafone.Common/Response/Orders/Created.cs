﻿namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class Created
    {
        #region Properties
        public string Value { get; set; } 
        #endregion
    }
}
