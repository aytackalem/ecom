﻿namespace ECommerce.MarketPlace.Vodafone.Common.Response.Orders
{
    public class VodafoneOrder
    {
        public string id { get; set; }
        public string shoppingCartId { get; set; }
        public string dealerId { get; set; }
        public string offeringId { get; set; }
        public string offeringName { get; set; }
        public string status { get; set; }
        public bool isPacked { get; set; }
        public string lastShipmentRefNo { get; set; }
        public List<ShipmentInfo> shipmentInfo { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public Created created { get; set; }
        public Updated updated { get; set; }
        public string barcode { get; set; }
        public decimal listPrice { get; set; }
        public decimal salePrice { get; set; }
        public string cargoCompanyId { get; set; }
        public string stockCode { get; set; }
        public string deliveryDuration { get; set; }
        public string vatRate { get; set; }
        public int quantity { get; set; }
        public object createDate { get; set; }
        public object updateDate { get; set; }
        public decimal discount { get; set; }
        public string cargoPaymentFlag { get; set; }
        public bool isDigital { get; set; }
        public object statusHistory { get; set; }
        public List<CustomerContactInfo> customerContactInfoList { get; set; }
    }
}
