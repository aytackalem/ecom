﻿using ECommerce.MarketPlace.Vodafone.Response.Base;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Token
{
    public class TokenResponse : ResponseBase
    {
        public string username { get; set; }
        public string token { get; set; }
        public string expireDate { get; set; }
    }
}
