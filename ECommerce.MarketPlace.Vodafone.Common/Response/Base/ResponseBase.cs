﻿namespace ECommerce.MarketPlace.Vodafone.Response.Base
{
    public abstract class ResponseBase
    {
        public ResultObj Result { get; set; }
    }
}
