﻿using ECommerce.MarketPlace.Vodafone.Response.Base;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.Category
{
    public class VodafoneCategoryResponse : ResponseBase
    {
        #region Properties
        public VodafoneGetCategories GetCategories { get; set; }
        #endregion
    }
}
