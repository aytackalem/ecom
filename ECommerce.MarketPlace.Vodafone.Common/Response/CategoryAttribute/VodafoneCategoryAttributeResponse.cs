﻿using ECommerce.MarketPlace.Vodafone.Response.Base;

namespace ECommerce.MarketPlace.Vodafone.Common.Response.CategoryAttribute
{
    public class VodafoneCategoryAttributeResponse : ResponseBase
    {
        #region Properties
        public List<VodafoneExtensionList> ExtensionList { get; set; }
        #endregion
    }
}
