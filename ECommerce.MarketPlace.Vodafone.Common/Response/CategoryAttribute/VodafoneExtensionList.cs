﻿namespace ECommerce.MarketPlace.Vodafone.Common.Response.CategoryAttribute
{
    public class VodafoneExtensionList
    {
        #region Properties
        public List<VodafoneParameter> Parameters { get; set; }
        #endregion
    }
}
