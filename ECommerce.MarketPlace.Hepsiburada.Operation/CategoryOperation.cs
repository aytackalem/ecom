﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Hepsiburada.Common.Category;
using ECommerce.MarketPlace.Hepsiburada.Common.Product;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hepsiburada.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private readonly IMemoryCache _memoryCache;

        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper, IMemoryCache memoryCache)
        {
            #region Fields
            _httpHelper = httpHelper;
            _memoryCache = memoryCache;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {


                var page = 0;

                var categories = new List<HepsiburadaCategory>();

                HepsiburadaCategoryPage categoryPage;

                do
                {
                    categoryPage = this._httpHelper.Get<HepsiburadaCategoryPage>(
                        $"https://mpop.hepsiburada.com/product/api/categories/get-all-categories?status=ACTIVE&available=true&page={page}&size=7500&version=1",
                        "Bearer",
                        this.GetToken(configurations).id_token);

                    categories.AddRange(categoryPage.Data);

                    page++;
                } while (!categoryPage.Last);

                categories = categories.Where(x => x.Leaf && x.Available && x.Status == "ACTIVE").ToList();


                List<string> newCategories = new List<string>();
                newCategories.Add("erkek");
                newCategories.Add("kadın");
                newCategories.Add("elbise");
                newCategories.Add("giyim");
                newCategories.Add("aksesuar");
                newCategories.Add("ayakkabı");
                newCategories.Add("kozmetik");
                newCategories.Add("krem");
                newCategories.Add("sneakers");
                newCategories.Add("giyim / ayakkabı");
                newCategories.Add("saatler");
                newCategories.Add("saat/gözlük/aksesuar");
                newCategories.Add("takı");
                newCategories.Add("bijuteri");




                var _categories = categories.Where(x => x.Paths.Any(y => newCategories.Contains(y.ToLower()))).ToList();

                foreach (var cLoop in _categories)
                {

                Console.WriteLine($"{_categories.IndexOf(cLoop)} {cLoop.DisplayName}");
                    var categoryAttributePage = this._httpHelper.Get<HepsiburadaCategoryAttributePage>(
                             $"https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attributes",
                             "Bearer",
                             this.GetToken(configurations).id_token);
                    if (categoryAttributePage.Success)
                    {
                        foreach (var atLoop in categoryAttributePage.Data.Attributes)
                        {
                            if (atLoop.Type == "string") continue;

                            var attributeValuePage = 0;
                            HepsiburadaCategoryAttributeValuePage categoryAttributeValuePage;

                            #region Get Attribute Value
                            do
                            {
                                categoryAttributeValuePage = this._httpHelper.Get<HepsiburadaCategoryAttributeValuePage>(
                                   $" https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attribute/{atLoop.Id}/values?page={attributeValuePage}&size=1000&version=4",
                                   "Bearer",
                                   this.GetToken(configurations).id_token);
                                if (categoryAttributeValuePage.Success)
                                {
                                    atLoop.CategoryAttributeValues.AddRange(categoryAttributeValuePage.Data);
                                }

                                attributeValuePage++;
                            } while (!categoryAttributeValuePage.Last && categoryAttributeValuePage.Success);

                            #endregion

                        }


                        cLoop.Attributes.AddRange(categoryAttributePage.Data.Attributes);
                    }
                }

                var data = _categories.Select(x => new CategoryResponse
                {
                    Code = x.CategoryId.ToString(),
                    Name = x.DisplayName,
                    ParentCategoryCode = x.ParentCategoryId.ToString(),
                    CategoryAttributes = x.Attributes.Select(ca => new CategoryAttributeResponse
                    {
                        CategoryCode = x.CategoryId.ToString(),
                        Code = ca.Id.ToString(),
                        Name = ca.Name,
                        Mandatory = ca.Mandatory,
                        AllowCustom = ca.Type == "string",
                        MultiValue = ca.MultiValue,
                        CategoryAttributesValues = ca.CategoryAttributeValues.Select(av => new CategoryAttributeValueResponse
                        {
                            VarinatCode = ca.Id.ToString(),
                            Code = av.Value.Replace(" ","-"),
                            Name = av.Value
                        }).ToList()
                    }).ToList()
                }).ToList();


                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        private Token GetToken(Dictionary<string, string> configurations)
        {
            var key = "HepsiburadaProductToken";

            if (!this._memoryCache.TryGetValue(key, out Token token))
            {
                token = this._httpHelper.Post<Authenticate, Token>(
                    new Authenticate
                    {
                        username = "sinozkozmetik_dev",
                        password = "aoUlyaxJDWnzca!"
                    },
                    $"https://mpop.hepsiburada.com/api/authenticate",
                    string.Empty,
                    string.Empty);

                this._memoryCache.Set(key, token, new TimeSpan(0, 29, 0));
            }

            return token;
        }

    }
}
