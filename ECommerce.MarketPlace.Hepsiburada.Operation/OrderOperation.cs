﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Hepsiburada.Common.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ECommerce.MarketPlace.Hepsiburada.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        private readonly ProviderOperation _providerOperation;
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            #region Fields
            _providerOperation = new ProviderOperation(httpHelper);
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };
                List<Item> items = new();

                int page = 0;
                int pageCount = 0;
                int offset = 100;

                if (string.IsNullOrEmpty(orderRequest.MarketPlaceOrderCode))
                {
                    do
                    {
                        var providerData = _providerOperation.GetOrders(orderRequest.Configurations, offset * page);

                        if (providerData == null || providerData.Items == null)
                            break;

                        items.AddRange(providerData.Items);

                        pageCount = providerData.PageCount;

                        page++;
                    } while (page < pageCount);
                }
                else
                {
                    var providerData = _providerOperation.GetOrder(orderRequest.Configurations, orderRequest.MarketPlaceOrderCode);
                    if (providerData != null || providerData.Items != null)
                        items.AddRange(providerData.Items);
                }

                if (items.Count > 0)
                {
                    var groupedOrders = items.GroupBy(i => i.OrderNumber).ToList();

                    foreach (var groupedOrder in groupedOrders)
                    {
                        var sameCustomer = items.Where(i => i.OrderNumber == groupedOrder.Key);
                        if (sameCustomer == null) continue;

                        var package = Package(sameCustomer, orderRequest);
                        var cargo = CargoPage(package.PackageNumber, orderRequest);

                        if (orderRequest.Configurations.ContainsKey("MutualBarcodeActive") &&
                            orderRequest.Configurations["MutualBarcodeActive"] == "True")
                        {
                            //if (cargo.CargoCompany.ToLower() == "hepsijet")
                            //{
                            //    MutualBarcode mutualBarcode = null;
                            //    for (int i = 0; i < 3; i++)
                            //    {
                            //        try
                            //        {
                            //            Thread.Sleep(1000);

                            //            mutualBarcode = MutualBarcode(package.PackageNumber, orderRequest);

                            //            if (mutualBarcode != null &&
                            //                mutualBarcode.Data != null &&
                            //                mutualBarcode.Data.Count > 0 &&
                            //                mutualBarcode.Code == "100")
                            //            {
                            //                orderResponse.Orders.Add(MapOrder(sameCustomer, cargo.Barcode, mutualBarcode.Data[0], package.PackageNumber));
                            //                break;
                            //            }
                            //        }
                            //        catch
                            //        {
                            //        }
                            //    }

                            //    if (mutualBarcode == null ||
                            //        mutualBarcode.Data == null ||
                            //        mutualBarcode.Data.Count == 0 ||
                            //        mutualBarcode.Code != "100")
                            //        orderResponse.Orders.Add(MapOrder(sameCustomer, cargo.Barcode, null, package.PackageNumber));
                            //}
                            //else
                            //{
                            //    orderResponse.Orders.Add(MapOrder(sameCustomer, cargo.Barcode, null, package.PackageNumber));
                            //}
                        }
                        else
                            orderResponse.Orders.Add(MapOrder(sameCustomer, cargo.Barcode, package.PackageNumber));
                    }
                }

                response.Data = orderResponse;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var providerData = _providerOperation.GetOrder(orderRequest.Configurations, orderRequest.MarketPlaceOrderCode).Items.Any(x => x.Status.Contains("Cancelled"));
                if (providerData)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Hepsiburda siparişiniz iptal statüsündedir. Siparişin devam edilemesi hepsiburada panelinden kontrol edilmesi gerekiyor.";
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                int page = 0;
                int pageCount = 0;
                int offset = 50;

                do
                {
                    var data = this._providerOperation.GetDelivered(orderRequest.Configurations, offset * page);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order { OrderCode = i.OrderNumber }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                int page = 0;
                int pageCount = 0;
                int offset = 50;

                do
                {
                    var data = this._providerOperation.GetCancelled(orderRequest.Configurations, offset * page);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order { OrderCode = i.OrderNumber }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });

        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                int page = 0;
                int pageCount = 0;
                int offset = 50;

                do
                {
                    var data = this._providerOperation.GetReturned(orderRequest.Configurations, offset * page);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order { OrderCode = i.OrderNumber }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }
        #endregion

        #region Helpers
        private Package Package(IEnumerable<Item> items, OrderRequest orderRequest)
        {
            var packageRequest = new PackagePage
            {
                LineItemRequests = items.Select(sc => new LineItemRequest
                {
                    Id = sc.Id,
                    Quantity = sc.Quantity.ToString()
                }).ToList()
            };
            Package package = _providerOperation.CreatePackage(orderRequest.Configurations, packageRequest);
            return package;
        }

        private CargoPage CargoPage(string packageNumber, OrderRequest orderRequest)
        {
            CargoPage cargo = _providerOperation.SetCargo(orderRequest.Configurations, packageNumber);
            return cargo;
        }

        private MutualBarcode MutualBarcode(string packageNumber, OrderRequest orderRequest)
        {
            MutualBarcode mutualBarcode = _providerOperation.MutualBarcode(orderRequest.Configurations, packageNumber);
            return mutualBarcode;
        }

        private Order MapOrder(IEnumerable<Item> items, string cargoBarcode, /*string base64,*/ string packageNumber)
        {
            var customerData = items.FirstOrDefault();
            Order order = new()
            {
                ListTotalAmount = Convert.ToDecimal(items.Sum(x => x.TotalPrice.Amount)) + Convert.ToDecimal(items.Sum(x => x.HbDiscount.TotalPrice.Amount)),
                Discount = Convert.ToDecimal(items.Sum(x => x.HbDiscount.TotalPrice.Amount)),
                OrderCode = customerData.OrderNumber,
                Source = "HB",
                TotalAmount = Convert.ToDecimal(items.Sum(x => x.TotalPrice.Amount)),
                CurrencyId = "TL",
                OrderTypeId = "OS",
                OrderDate = customerData.OrderDate,
                OrderSourceId = OrderSources.Pazaryeri,
                MarketPlaceId = Marketplaces.Hepsiburada
            };
            var customerName = customerData.CustomerName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var firstName = customerName[0];
            var lastName = "";
            if (customerName.Length > 1)
            {
                lastName = customerName[1];
            }

            customerName = customerData.Invoice?.Address.Name.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var invoiceFirstName = "";
            var invoiceLastName = "";
            if (customerName.Length > 1)
            {
                invoiceFirstName = customerName[0];
                invoiceLastName = customerName[1];
            }

            order.Customer = new Customer()
            {
                FirstName = firstName,
                LastName = lastName,
                IdentityNumber = customerData.Invoice?.TurkishIdentityNumber,
                Phone = customerData.ShippingAddress?.PhoneNumber
            };
            order.OrderDeliveryAddress = new OrderDeliveryAddress()
            {
                Country = customerData.ShippingAddress.CountryCode == "TR" ? "Türkiye" : customerData.ShippingAddress.CountryCode,
                City = customerData.ShippingAddress.City,
                District = customerData.ShippingAddress.Town,
                Phone = customerData.ShippingAddress.PhoneNumber,
                Email = customerData.ShippingAddress.Email,
                Neighborhood = $"{customerData.ShippingAddress.District}",
                Address = customerData.ShippingAddress.Address,
                FirstName = firstName,
                LastName = lastName
            };
            order.OrderInvoiceAddress = new OrderInvoiceAddress()
            {
                Country = customerData.Invoice?.Address?.CountryCode == "TR" ? "Türkiye" : customerData.Invoice?.Address?.CountryCode,
                City = customerData.Invoice?.Address?.City,
                District = customerData.Invoice?.Address?.Town,
                Neighborhood = $"{customerData.Invoice?.Address?.District}",
                Address = customerData.Invoice?.Address?.address,
                Phone = customerData.Invoice?.Address?.PhoneNumber,
                Email = customerData.Invoice?.Address?.Email,
                TaxOffice = customerData.Invoice?.TaxOffice?.ToString(),
                TaxNumber = customerData.Invoice?.TaxNumber,
                FirstName = invoiceFirstName,
                LastName = invoiceLastName
            };
            order.Payments = new List<Payment>
            {
                new Payment()
                {
                    Amount = Decimal.Parse(items.Sum(tp => tp.TotalPrice.Amount).ToString()),
                    Date = customerData.OrderDate,
                    PaymentTypeId = "OO",
                    CurrencyId = "TL"
                }
            };

            string shipmentId = "";

            switch (customerData.CargoCompanyModel.ShortName)
            {
                case "AR":
                    shipmentId = ShipmentCompanies.ArasKargo;
                    break;
                case "YK":
                    shipmentId = ShipmentCompanies.YurtiçiKargo;
                    break;
                case "PK":
                    shipmentId = ShipmentCompanies.PttKargo;
                    break;
                case "MK":
                    shipmentId = ShipmentCompanies.MngKargo;
                    break;
                case "SK":
                    shipmentId = ShipmentCompanies.SüratKargo;
                    break;
                case "HX":
                    shipmentId = ShipmentCompanies.HepsiJet;
                    break;
            }

            order.OrderShipment = new OrderShipment()
            {
                TrackingCode = cargoBarcode,
                TrackingUrl = customerData.CargoCompanyModel?.TrackingUrl?.Replace("{0}", cargoBarcode),
                //TrackingBase64 = base64,
                ShipmentCompanyId = shipmentId, //mapleme yapılacak
                ShipmentTypeId = "3",
                Payor = false,
                PackageNumber = packageNumber
            };

            #region Order Details
            order.OrderDetails = new List<OrderDetail>();
            foreach (var orderDetail in items)
            {

                decimal discountPrice = 0;

                if (orderDetail.HbDiscount != null)
                {
                    discountPrice = Convert.ToDecimal(orderDetail.HbDiscount.UnitPrice.Amount);

                }

                order.OrderDetails.Add(new OrderDetail()
                {
                    Product = new Product()
                    {
                        Barcode = orderDetail.MerchantSKU,
                        StockCode = orderDetail.Sku,
                        Name = orderDetail.Name
                    },
                    Quantity = orderDetail.Quantity,
                    UnitPrice = Convert.ToDecimal(orderDetail.UnitPrice.Amount),
                    ListPrice = Convert.ToDecimal(orderDetail.UnitPrice.Amount) + discountPrice,
                    TaxRate = orderDetail.VatRate / 100,
                    UnitDiscount = discountPrice,
                    UnitCommissionAmount = Convert.ToDecimal(orderDetail.Commission.amount)

                });
            }
            #endregion
            return order;
        }
        #endregion
    }
}