﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Stock;
using ECommerce.MarketPlace.Hepsiburada.Common.Product;
using ECommerce.MarketPlace.Hepsiburada.Common.ProductFilter;
using ECommerce.MarketPlace.Hepsiburada.Common.Stock;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.MarketPlace.Hepsiburada.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private readonly IMemoryCache _memoryCache;
        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper, IMemoryCache memoryCache)
        {
            this._httpHelper = httpHelper;
            this._memoryCache = memoryCache;

        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                var products = new List<HepsiburadaProduct>();



                foreach (var siLoop in productRequest.ProductItem.StockItems)
                {


                    var filter = Filter(new ProductFilterRequest
                    {
                        Stockcode = siLoop.StockCode,
                        Configurations = productRequest.Configurations
                    });

                    //filter.Success true demek ürün sistemde var demek
                    //Ürün yükleme yapılır Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz 
                    if (!filter.Success && siLoop.Active)
                    {
                        continue;
                    }

                    var product = new HepsiburadaProduct
                    {
                        CategoryId = int.Parse(productRequest.ProductItem.CategoryId),
                        Merchant = productRequest.Configurations["MerchantId"],
                        Attributes = new HepsiburadaProductAttribute
                        {
                            MerchantSku = siLoop.StockCode,
                            VaryantGroupID = productRequest.ProductItem.SellerCode,
                            Barcode = siLoop.Barcode,
                            UrunAdi = siLoop.Title,
                            UrunAciklamasi = siLoop.Description,
                            Marka = productRequest.ProductItem.BrandName,
                            GarantiSuresi = 0,
                            Kg = productRequest.ProductItem.DimensionalWeight.ToString(),
                            Tax_vat_rate = (siLoop.VatRate * 100).ToString("0"),
                            Price = siLoop.SalePrice.ToString().Replace(".", ","),
                            Stock = siLoop.Quantity.ToString()
                        }
                    };

                    #region Images
                    siLoop.Images = siLoop.Images.OrderBy(i => i.Order).ToList();
                    for (int i = 0; i < (siLoop.Images.Count >= 5 ? 5 : siLoop.Images.Count); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                product.Attributes.Image1 = siLoop.Images[i].Url;
                                break;
                            case 1:
                                product.Attributes.Image2 = siLoop.Images[i].Url;
                                break;
                            case 2:
                                product.Attributes.Image3 = siLoop.Images[i].Url;
                                break;
                            case 3:
                                product.Attributes.Image4 = siLoop.Images[i].Url;
                                break;
                            case 4:
                                product.Attributes.Image5 = siLoop.Images[i].Url;
                                break;
                        }
                    }
                    #endregion

                    if (siLoop.Attributes.Any(a => a.AttributeName == "Beden"))
                        product.Attributes.Ebatlar_variant_property = siLoop.Attributes.FirstOrDefault(a => a.AttributeName == "Beden").AttributeValueName;

                    if (siLoop.Attributes.Any(a => a.AttributeName == "Renk"))
                        product.Attributes.Renk_variant_property = siLoop.Attributes.FirstOrDefault(a => a.AttributeName == "Renk").AttributeValueName;

                    products.Add(product);
                }

                this._httpHelper.PostForm<List<HepsiburadaProduct>, HepsiburadaProductResponse>(
                    products,
                    $"https://mpop.hepsiburada.com/product/api/products/import",
                    "Bearer",
                    this.GetToken(productRequest.Configurations).id_token,
                    "application/json");

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest updatePriceAndStock)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var culture = new CultureInfo("en-US");

                var merchantid = updatePriceAndStock.Configurations["MerchantId"];

                var stockRequest = new List<HepsiburadaStockRequest>();
                stockRequest.Add(new HepsiburadaStockRequest
                {
                    HepsiburadaSku = null, //Şimdilik hepsiburada kodu olduğu için bunu açtık.Normalde MerchantSku açık olması gerekiyor.
                    MerchantSku = updatePriceAndStock.PriceStock.StockCode, //updatePriceAndStock.PriceStock.StockCode, 
                    AvailableStock = updatePriceAndStock.PriceStock.Quantity
                });

                var stockResponse = this._httpHelper.Post<List<HepsiburadaStockRequest>, HepsiburadaStockResponse>(
                   stockRequest,
                   $"https://listing-external.hepsiburada.com/listings/merchantid/{merchantid}/stock-uploads",
                   "Basic",
                  ProviderOperation.GetBasicAuth(updatePriceAndStock.Configurations["Username"], updatePriceAndStock.Configurations["Password"]));

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }


        public Response UpdatePrice(StockRequest updatePriceAndStock)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var culture = new CultureInfo("en-US");

                var merchantid = updatePriceAndStock.Configurations["MerchantId"];

                if (updatePriceAndStock.PriceStock.SalePrice.HasValue)
                {


                    var priceRequest = new List<HepsiburadaPriceRequest>();
                    priceRequest.Add(new HepsiburadaPriceRequest()
                    {
                        HepsiburadaSku = null,
                        MerchantSku = updatePriceAndStock.PriceStock.StockCode,
                        Price = Convert.ToDouble(updatePriceAndStock.PriceStock.SalePrice.ToString().Replace(",", "."), culture)
                    });
                    var priceResponse = this._httpHelper.Post<List<HepsiburadaPriceRequest>, HepsiburadaPriceResponse>(
                       priceRequest,
                       $"https://listing-external.hepsiburada.com/listings/merchantid/{merchantid}/price-uploads",
                       "Basic",
                       ProviderOperation.GetBasicAuth(updatePriceAndStock.Configurations["Username"], updatePriceAndStock.Configurations["Password"]));
                }



                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        #endregion


        #region Helper Method
        private Token GetToken(Dictionary<string, string> configurations)
        {
            var key = "HepsiburadaProductToken";

            if (!this._memoryCache.TryGetValue(key, out Token token))
            {
                token = this._httpHelper.Post<Authenticate, Token>(
                    new Authenticate
                    {
                        username = configurations["Username"],
                        password = configurations["Password"]
                    },
                    $"https://mpop.hepsiburada.com/api/authenticate",
                    string.Empty,
                    string.Empty);

                this._memoryCache.Set(key, token, new TimeSpan(0, 29, 0));
            }

            return token;
        }

        private DataResponse<HepsiburadaProductFilterResponse> Filter(ProductFilterRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<HepsiburadaProductFilterResponse>>((response) =>
            {

                var merchantid = productRequest.Configurations["MerchantId"];




                var filterResponse = this._httpHelper.Get<HepsiburadaProductFilterResponse>(
                   $"https://listing-external.hepsiburada.com/listings/merchantid/{merchantid}?offset=0&limit=1&merchantskulist={productRequest.Stockcode}",
                   "Basic",
                  ProviderOperation.GetBasicAuth(productRequest.Configurations["Username"], productRequest.Configurations["Password"]));
                if (filterResponse == null)
                {
                    response.Success = false;
                    response.Message = "Ürün bulunamadı";
                    return;
                }

                response.Data = filterResponse;
                response.Success = true;


            });
        }

        #endregion
    }
}
