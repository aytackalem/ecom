﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.MarketPlace.Hepsiburada.Common.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Hepsiburada.Operation
{
    public class ProviderOperation
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public ProviderOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
        }
        #endregion

        #region Methods
        public OrderPage GetOrders(Dictionary<string, string> configuration, int skip = 0, int take = 100)
        {
            string url = $"https://oms-external.hepsiburada.com/orders/merchantid/{configuration["MerchantId"]}?offset={skip}&limit={take}";
            var orderResult = _httpHelper.Get<OrderPage>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (orderResult == null) return new OrderPage();
            return orderResult;
        }

        public OrderPage GetOrder(Dictionary<string, string> configuration, string orderNumber)
        {
            string url = $"https://oms-external.hepsiburada.com/orders/merchantid/{configuration["MerchantId"]}/ordernumber/{orderNumber}";
            var orderResult = _httpHelper.Get<OrderPage>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (orderResult == null) return new OrderPage();

            return orderResult;
        }

        public OrderPage GetDelivered(Dictionary<string, string> configuration, int skip = 0, int take = 50)
        {
            string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{configuration["MerchantId"]}/delivered?offset={skip}&limit={take}";
            var orderResult = _httpHelper.Get<OrderPage>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (orderResult == null) return new OrderPage();
            return orderResult;
        }

        public OrderPage GetCancelled(Dictionary<string, string> configuration, int skip = 0, int take = 50)
        {
            string url = $"https://oms-external.hepsiburada.com/orders/merchantid/{configuration["MerchantId"]}/cancelled?offset={skip}&limit={take}";
            var orderResult = _httpHelper.Get<OrderPage>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (orderResult == null) return new OrderPage();
            return orderResult;
        }

        public OrderPage GetReturned(Dictionary<string, string> configuration, int skip = 0, int take = 50)
        {
            string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{configuration["MerchantId"]}/undelivered?offset={skip}&limit={take}";
            var orderResult = _httpHelper.Get<OrderPage>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (orderResult == null) return new OrderPage();
            return orderResult;
        }

        public Package CreatePackage(Dictionary<string, string> configuration, PackagePage lineItems)
        {
            string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{configuration["MerchantId"]}";
            var merchantPackages = _httpHelper.Post<PackagePage, Package>(lineItems, url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (merchantPackages == null) return new Package();
            return merchantPackages;
        }

        public CargoPage SetCargo(Dictionary<string, string> configuration, string packageNumber)
        {
            string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{configuration["MerchantId"]}/packagenumber/{packageNumber}";
            var lineItems = _httpHelper.Get<CargoPage[]>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            if (lineItems == null) return new CargoPage();
            return lineItems[0];
        }

        public MutualBarcode MutualBarcode(Dictionary<string, string> configuration, string packageNumber)
        {
            string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{configuration["MerchantId"]}/packagenumber/{packageNumber}/labels?format=png";
            var mutualBarcode = _httpHelper.Get<MutualBarcode>(url, "Basic", GetBasicAuth(configuration["Username"], configuration["Password"]));
            return mutualBarcode;
        }

        public static string GetBasicAuth(string username, string password)
        {
            string txt = username + ":" + password;
            byte[] encodedBytes = System.Text.Encoding.UTF8.GetBytes(txt);
            return Convert.ToBase64String(encodedBytes);
        }
        #endregion
    }
}


