﻿using DocumentFormat.OpenXml.Bibliography;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects;
using ECommerce.Persistence.Common.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECommerce.Accounting.Nebim
{
    public class NebimProviderService : IAccountingProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        public readonly IHttpHelper _httpHelper;

        public readonly ICacheHandler _cacheHandler;

        public readonly IDbNameFinder _dbNameFinder;
        #endregion,

        #region Constructors
        public NebimProviderService(IUnitOfWork unitOfWork, IHttpHelper httpHelper, ICacheHandler cacheHandler, IDbNameFinder dbNameFinder)
        {
            _unitOfWork = unitOfWork;
            _httpHelper = httpHelper;
            _cacheHandler = cacheHandler;
            _dbNameFinder = dbNameFinder;
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId, Dictionary<string, string> configurations)
        {
            StringBuilder stringBuilder = new();

            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                response.Data = new();

                stringBuilder.Append("Sipariş bulunuyor.");

                #region Read Order
                var order = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Customer)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(x => x.ProductInformation.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.OrderShipments)
                    .Include(o => o.Payments)
                        .ThenInclude(x => x.PaymentType)
                        .ThenInclude(x => x.PaymentTypeTranslations)
                    .Include(x => x.Marketplace)
                    .Include(x => x.Company)
                    .FirstOrDefault(x => x.Id == orderId);

                var orderNotFound = order == null;
                if (orderNotFound)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }
                #endregion

                stringBuilder.Append("Sipariş bulundu.");



                response.Success = true;
                response.Data = new AccountingInfo
                {
                    Guid = "",
                    AccountingCompanyId = "NB",
                    Url = null
                };
            }, (response, e) =>
            {

                response.Message = $"Bilinmeyen bir hata oluştu. {stringBuilder}";
            });
        }

        public DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                response.Success = true;
                response.Data = new AccountingInfo
                {
                    Guid = "",
                    AccountingCompanyId = "NB",
                    Url = null
                };
            }, (response, e) =>
            {
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        public DataResponse<AccountingCancel> CancelInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCancel>>((response) =>
            {
                response.Data = new();


                response.Data = new AccountingCancel
                {
                    PaymentType = "",
                    PaymentSource = "",
                    DocumentNo = ""
                };
                response.Success = true;
            }, (response, e) =>
            {
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        #region Stock Transfer
        public DataResponse<string> CreateStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                #region Read And Check Order From Helpy
                var order = _unitOfWork
                            .OrderRepository
                            .DbSet()
                            .Include(x => x.OrderDeliveryAddress.Neighborhood.District.City.Country)
                            .Include(x => x.OrderInvoiceInformation.Neighborhood.District.City.Country)
                            .Include(x => x.OrderShipments)
                            .Include(x => x.Marketplace)
                            .Include(o => o.OrderDetails)
                                .ThenInclude(x => x.ProductInformation)
                                .Include(x => x.Payments)
                            .FirstOrDefault(x => x.Id == orderId);

                if (order == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }
                #endregion

                var apiUrl = configurations["ApiUrl"];

                var login = GetLogin(apiUrl);

                if (login != null)
                {

                    var currAccCode = "";
                    var currAccCodeActive = bool.Parse(configurations["sp_LastCodeCurrAccActive"]);

                    if (currAccCodeActive)
                    {
                        var nebimProc = new NebimProc
                        {
                            ProcName = "sp_LastCodeCurrAcc",
                            Parameters = new List<NebimProcParameter>
                    {
                        new NebimProcParameter
                        {
                            Name="CompanyCode",
                            Value="1"
                        },
                         new NebimProcParameter
                        {
                            Name="CurrAccTypeCode",
                            Value="4"
                        },
                          new NebimProcParameter
                        {
                            Name="OfficeCode",
                            Value="1"
                        },
                           new NebimProcParameter
                        {
                            Name="StoreCode",
                            Value="0"
                        }
                    }
                        };

                        var currAccResponse = _httpHelper.Post<NebimProc, List<NebimProcCurrAcc>>(nebimProc, $"{apiUrl}/(S({login.SessionID}))/IntegratorService/RunProc", null, null, null, null);

                        currAccCode = currAccResponse != null ? currAccResponse[0].CurrAccCode : "";
                    }

                    var findOrderDeliveryAddress = FindDistrict(apiUrl, login.SessionID, order.OrderDeliveryAddress.Neighborhood.District.City.Country.Name, order.OrderDeliveryAddress.Neighborhood.District.City.Name, order.OrderDeliveryAddress.Neighborhood.District.Name);

                    if (order.Micro)
                    {
                        findOrderDeliveryAddress = FindDistrict(apiUrl, login.SessionID, order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name, order.OrderInvoiceInformation.Neighborhood.District.City.Name, order.OrderInvoiceInformation.Neighborhood.District.Name);
                    }

                    var findOrderInvoiceAddress = FindDistrict(apiUrl, login.SessionID, order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name, order.OrderInvoiceInformation.Neighborhood.District.City.Name, order.OrderInvoiceInformation.Neighborhood.District.Name);


                    var taxOfficeCode = string.Empty;
                    if (order.Commercial && !string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxOffice))
                    {
                        var findNebimTaxOfficeCode = FindNebimTaxOfficeCodes(apiUrl, login.SessionID, order.OrderInvoiceInformation.TaxOffice);

                        if (findNebimTaxOfficeCode.Success == false)
                        {
                            return;
                        }

                        taxOfficeCode = findNebimTaxOfficeCode.Data.TaxOfficeCode;
                    }

                    if (findOrderDeliveryAddress.Success)
                    {

                        var _recipient = order.OrderDeliveryAddress.Recipient.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);

                        var firstName = _recipient[0];
                        var lastName = _recipient[1];

                        if (order.Micro)
                        {
                            firstName = order.OrderInvoiceInformation.FirstName;
                            lastName = order.OrderInvoiceInformation.LastName;
                            order.OrderDeliveryAddress.Recipient = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}";
                        }

                        var nebimCustomer = new NebimCustomerRequest
                        {
                            ModelType = 3,
                            CurrAccCode = currAccCode,
                            IsSubjectToEInvoice = false,
                            CurrAccDescription = order.OrderDeliveryAddress.Recipient,
                            FirstName = firstName,
                            LastName = lastName,
                            IdentityNum = order.Commercial ? order.OrderInvoiceInformation.TaxNumber : "11111111111",
                            IsIndividualAcc = true,
                            Communications = new List<NebimCommunication>
                            {
                                new NebimCommunication
                                {
                                    CommAddress= order.OrderInvoiceInformation.Mail,
                                    CommunicationTypeCode= "3",
                                    CanSendAdvert= true
                                },
                                new NebimCommunication
                                {
                                    CommAddress= order.OrderInvoiceInformation.Phone,
                                    CommunicationTypeCode= "7",
                                    CanSendAdvert= true
                                }
                            },
                            PostalAddresses = new List<NebimCustomerPostalAddress>
                            {
                                new NebimCustomerPostalAddress
                                {
                                  //PostalAddressID= null,
                                  AddressTypeCode = "1",
                                  Address = order.OrderDeliveryAddress.Address,
                                  CountryCode = findOrderDeliveryAddress.Data.CountryCode,
                                  CityCode = findOrderDeliveryAddress.Data.CityCode,
                                  DistrictCode = findOrderDeliveryAddress.Data.DistrictCode,
                                  StateCode = findOrderDeliveryAddress.Data.StateCode,
                                  ZipCode = ""
                                },
                                new NebimCustomerPostalAddress
                                {
                                  //PostalAddressID= null,
                                  AddressTypeCode = "2",
                                  Address = order.OrderInvoiceInformation.Address,
                                  CountryCode = findOrderInvoiceAddress.Data.CountryCode,
                                  CityCode = findOrderInvoiceAddress.Data.CityCode,
                                  DistrictCode = findOrderInvoiceAddress.Data.DistrictCode,
                                  StateCode = findOrderInvoiceAddress.Data.StateCode,
                                  ZipCode = ""
                                }
                            }

                        };

                        var nebimCustomerResponse = _httpHelper.Post<NebimCustomerRequest, NebimCustomerResponse>(nebimCustomer, $"{apiUrl}/(S({login.SessionID}))/IntegratorService/POST", null, null, true, $"{_dbNameFinder.FindName()}_NebimCustomer_{order.Id}.txt");

                        currAccCode = nebimCustomerResponse != null ? nebimCustomerResponse.CurrAccCode : "";

                        if (string.IsNullOrEmpty(currAccCode)) return;


                        var documentNumber = $"{order.MarketplaceId}{order.MarketplaceOrderNumber}".Replace("TSoft", "");

                        #region SalesPersonCode
                        var salesPersonCode = string.Empty;
                        var salesUrl = string.Empty;

                        var nebimMarketplaceSalesPersonnelCode = _unitOfWork
                              .NebimMarketplaceSalesPersonnelCodeRepository
                              .DbSet()
                              .FirstOrDefault(ncc => ncc.MarketplaceId == order.MarketplaceId && ncc.Micro == order.Micro);


                        if (nebimMarketplaceSalesPersonnelCode != null)
                        {
                            salesPersonCode = nebimMarketplaceSalesPersonnelCode.Value;
                            salesUrl = nebimMarketplaceSalesPersonnelCode.SalesUrl;
                        }

                        #endregion

                        var payment = order.Payments.FirstOrDefault();

                        #region Nebim Payment
                        var paymentType = 2;//2 Kredi Kartı
                        var code = string.Empty;
                        var creditCardTypeCode = string.Empty;

                        if (payment.PaymentTypeId == PaymentTypes.OnlineOdeme || payment.PaymentTypeId == PaymentTypes.KapıdaNakit || payment.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit)
                        {
                            paymentType = 2;
                            code = string.Empty; //Her zaman boş gönderilir

                            #region CreditCardCodes
                            var nebimMarketplaceCreditCardCode = _unitOfWork
                             .NebimMarketplaceCreditCardCodeRepository
                             .DbSet()
                             .FirstOrDefault(ncc => ncc.MarketplaceId == order.MarketplaceId && ncc.BankId == payment.BankId && ncc.Micro == order.Micro &&
                             ncc.PaymentTypeId == payment.PaymentTypeId
                             );

                            if (nebimMarketplaceCreditCardCode != null)
                            {
                                creditCardTypeCode = nebimMarketplaceCreditCardCode.Value;
                            }
                            #endregion
                        }
                        else if (payment.PaymentTypeId == "C") //HAVALE
                        {
                            paymentType = 4;
                            code = configurations["BankCode"];//Code alanına Mağazanın Nakit Kasa Kodu 
                            creditCardTypeCode = string.Empty;//Her zaman boş gönderilir
                        }

                        #endregion

                        #region OrdersViaInternetInfo
                        var paymentTypeDescription = "KREDIKARTI/BANKAKARTI";
                        int paymentTypeCode = 1;

                        if (order.OrderSourceId == OrderSources.Pazaryeri)
                        {
                            paymentTypeDescription = "ODEMEARACISI";
                            paymentTypeCode = 4;
                        }
                        else if (payment.PaymentTypeId == PaymentTypes.OnlineOdeme)
                        {
                            paymentTypeDescription = "KREDIKARTI/BANKAKARTI";
                            paymentTypeCode = 1;
                        }
                        else if (payment.PaymentTypeId == PaymentTypes.Havale)
                        {
                            paymentTypeDescription = "EFT/HAVALE";
                            paymentTypeCode = 2;
                        }
                        else if (payment.PaymentTypeId == PaymentTypes.KapıdaNakit || payment.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit)
                        {
                            paymentTypeDescription = "KAPIDAODEME";
                            paymentTypeCode = 3;
                        }
                        #endregion

                        if (order.Micro) //sadece trendyol siparişlerinde olur
                        {
                            var nebimMicroOrder = new NebimMicroOrderRequest
                            {
                                ModelType = 228,
                                CustomerTypeCode = 4,
                                TaxTypeCode = 6,
                                DocumentNumber = documentNumber,
                                CustomerCode = currAccCode,
                                WarehouseCode = configurations["WarehouseTransferOrders"],
                                IncotermCode1 = configurations["IncotermCode1"],
                                IncotermCode2 = configurations["IncotermCode2"],
                                PaymentMethodCode = configurations["PaymentMethodCode"],
                                DocCurrencyCode = "TRY",
                                BillingPostalAddressID = nebimCustomerResponse.CurrAccDefault.BillingAddressID,
                                ShippingPostalAddressID = nebimCustomerResponse.CurrAccDefault.ShippingAddressID,
                                CompanyCode = 1,
                                POSTerminalID = Convert.ToInt32(configurations["NebimPOSTerminalID"]),
                                InternalDescription = order.OrderShipments.FirstOrDefault()?.TrackingCode,
                                ShipmentMethodCode = configurations["ShippingCode"],
                                OfficeCode = configurations["OrderOfficeCode"],//ayarlar
                                StoreCode = configurations["OrderStoreCode"],//ayarlar
                                StoreWarehouseCode = configurations["WarehouseTransferOrders"],//ayarlar
                                DeliveryCompanyCode = configurations["DeliveryCompanyCode"],
                                OrderDate = order.OrderDate,
                                IsCompleted = true,
                                IsSalesViaInternet = true,
                                Description = $"{order.Marketplace.Name} - Mikro Ihracat Sipariş",
                                Lines = order.OrderDetails.Select(x => new Line
                                {
                                    UsedBarcode = x.ProductInformation.Barcode,
                                    LineDescription = string.Empty,
                                    PriceVI = Convert.ToDouble(x.UnitPrice),
                                    Qty1 = x.Quantity,
                                    LDisRate1 = 0,
                                    SalesPersonCode = salesPersonCode,
                                }).ToList(),
                                OrdersViaInternetInfo = new OrdersViaInternetInfo
                                {
                                    SalesURL = salesUrl,
                                    PaymentTypeCode = paymentTypeCode,
                                    PaymentTypeDescription = paymentTypeDescription,
                                    PaymentAgent = "",
                                    PaymentDate = DateTime.Now,
                                    SendDate = DateTime.Now
                                },
                                Payments = order.Payments.Select(x => new Infrastructure.Accounting.Nebim.DataTransferObjects.Payment
                                {
                                    PaymentType = paymentType,
                                    InstallmentCount = 1,
                                    AmountVI = Convert.ToDouble(order.Total),
                                    Code = code,
                                    CreditCardTypeCode = creditCardTypeCode,
                                    DocCurrencyCode = "TRY",
                                }).ToList(),
                                PostalAddress = new NebimPostalAddress
                                {
                                    Address = order.OrderInvoiceInformation.Address,
                                    CountryCode = findOrderInvoiceAddress.Data.CountryCode,
                                    CityCode = findOrderInvoiceAddress.Data.CityCode,
                                    StateCode = findOrderInvoiceAddress.Data.StateCode,
                                    DistrictCode = findOrderInvoiceAddress.Data.DistrictCode,
                                    TaxOfficeCode = order.Commercial ? taxOfficeCode : string.Empty,
                                    TaxNumber = order.Commercial ? order.OrderInvoiceInformation.TaxNumber : string.Empty,
                                    CompanyName = order.Commercial ? $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}" : string.Empty,
                                    FirstName = order.Commercial == false ? order.OrderInvoiceInformation.FirstName : string.Empty,
                                    LastName = order.Commercial == false ? order.OrderInvoiceInformation.LastName : string.Empty,
                                    IdentityNum = order.Commercial == false ? "11111111111" : string.Empty
                                }
                            };

                            var nebimOrderResponse = _httpHelper.Post<NebimMicroOrderRequest, NebimOrderResponse>(nebimMicroOrder, $"{apiUrl}/(S({login.SessionID}))/IntegratorService/POST", null, null, true, $"{_dbNameFinder.FindName()}_NebimOrder_{order.Id}.txt");


                            var nebimOrderEntity = new Domain.Entities.NebimOrder
                            {
                                Id = order.Id,
                                OrderNumber = nebimOrderResponse.OrderNumber,
                                CurrAccCode = currAccCode,
                                CreatedDate = DateTime.Now,
                                DocumentNumber = documentNumber,
                                HeaderId = nebimOrderResponse.HeaderID,
                                NebimOrderDetails = new List<NebimOrderDetail>(),
                                Micro = true

                            };

                            foreach (var item in nebimOrderResponse.Lines)
                            {
                                var orderDetail = order.OrderDetails.FirstOrDefault(x => x.ProductInformation.Barcode == item.UsedBarcode);


                                nebimOrderEntity.NebimOrderDetails.Add(new Domain.Entities.NebimOrderDetail
                                {
                                    Id = orderDetail.Id,
                                    LineId = item.LineID,
                                    UnitPrice = orderDetail.UnitPrice,
                                    Qty1 = orderDetail.Quantity,
                                    VatRate = orderDetail.VatRate,
                                    UsedBarcode = orderDetail.ProductInformation.Barcode
                                });
                            }

                            _unitOfWork.NebimOrderRepository.Create(nebimOrderEntity);
                        }
                        else
                        {
                            var nebimOrder = new NebimOrderRequest
                            {
                                ModelType = 6,
                                CustomerCode = currAccCode,
                                InternalDescription = order.OrderShipments.FirstOrDefault()?.TrackingCode,
                                OfficeCode = configurations["OrderOfficeCode"],//ayarlar
                                StoreCode = configurations["OrderStoreCode"],//ayarlar
                                StoreWarehouseCode = configurations["WarehouseTransferOrders"],//ayarlar
                                PosTerminalID = Convert.ToInt32(configurations["NebimPOSTerminalID"]),//ayarlar
                                DeliveryCompanyCode = configurations["DeliveryCompanyCode"],
                                ShipmentMethodCode = Convert.ToInt32(configurations["ShippingCode"]),
                                OrderDate = order.OrderDate,
                                IsCompleted = true,
                                IsSalesViaInternet = true,
                                DocumentNumber = documentNumber,
                                Description = $"ST{order.Id}",
                                Lines = order.OrderDetails.Select(x => new Line
                                {
                                    UsedBarcode = x.ProductInformation.Barcode,
                                    LineDescription = string.Empty,
                                    PriceVI = Convert.ToDouble(x.UnitPrice),
                                    Qty1 = x.Quantity,
                                    LDisRate1 = 0,
                                    SalesPersonCode = salesPersonCode,
                                }).ToList(),
                                OrdersViaInternetInfo = new OrdersViaInternetInfo
                                {
                                    SalesURL = salesUrl,
                                    PaymentTypeCode = paymentTypeCode,
                                    PaymentTypeDescription = paymentTypeDescription,
                                    PaymentAgent = "",
                                    PaymentDate = DateTime.Now,
                                    SendDate = DateTime.Now
                                },
                                Discounts = new List<Discount>(),
                                Payments = order.Payments.Select(x => new Infrastructure.Accounting.Nebim.DataTransferObjects.Payment
                                {
                                    PaymentType = paymentType,
                                    Code = code,
                                    CreditCardTypeCode = creditCardTypeCode,
                                    InstallmentCount = 1,
                                    CurrencyCode = "TRY",
                                    AmountVI = Convert.ToDouble(order.Total),
                                }).ToList(),
                                PostalAddress = new NebimPostalAddress
                                {
                                    Address = order.OrderInvoiceInformation.Address,
                                    CountryCode = findOrderInvoiceAddress.Data.CountryCode,
                                    CityCode = findOrderInvoiceAddress.Data.CityCode,
                                    StateCode = findOrderInvoiceAddress.Data.StateCode,
                                    DistrictCode = findOrderInvoiceAddress.Data.DistrictCode,
                                    TaxOfficeCode = order.Commercial ? taxOfficeCode : string.Empty,
                                    TaxNumber = order.Commercial ? order.OrderInvoiceInformation.TaxNumber : string.Empty,
                                    CompanyName = order.Commercial ? $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}" : string.Empty,
                                    FirstName = order.Commercial == false ? order.OrderInvoiceInformation.FirstName : string.Empty,
                                    LastName = order.Commercial == false ? order.OrderInvoiceInformation.LastName : string.Empty,
                                    IdentityNum = order.Commercial == false ? "11111111111" : string.Empty
                                }
                            };

                            if (order.CargoFee > 0 && !string.IsNullOrEmpty(configurations["CargoProductBarcode"]))
                            {
                                nebimOrder.Lines.Add(new Line
                                {
                                    UsedBarcode = configurations["CargoProductBarcode"],
                                    LineDescription = string.Empty,
                                    PriceVI = Convert.ToDouble(order.CargoFee),
                                    Qty1 = 1,
                                    LDisRate1 = 0,
                                    SalesPersonCode = salesPersonCode,
                                });
                            }

                            if (order.SurchargeFee > 0 && !string.IsNullOrEmpty(configurations["SurchargeProductBarcode"]))
                            {
                                nebimOrder.Lines.Add(new Line
                                {
                                    UsedBarcode = configurations["SurchargeProductBarcode"],
                                    LineDescription = string.Empty,
                                    PriceVI = Convert.ToDouble(order.SurchargeFee),
                                    Qty1 = 1,
                                    LDisRate1 = 0,
                                    SalesPersonCode = salesPersonCode,
                                });
                            }

                            var nebimOrderResponse = _httpHelper.Post<NebimOrderRequest, NebimOrderResponse>(nebimOrder, $"{apiUrl}/(S({login.SessionID}))/IntegratorService/POST", null, null, true, $"{_dbNameFinder.FindName()}_NebimOrder_{order.Id}.txt");

                            if (nebimOrderResponse == null)
                            {
                                Console.WriteLine($"{_dbNameFinder.FindName()} Mizalle stok transfer kesilemedi. OrderId:{order.Id}");
                                return;
                            }

                            var nebimOrderEntity = new Domain.Entities.NebimOrder
                            {
                                Id = order.Id,
                                OrderNumber = nebimOrderResponse.OrderNumber,
                                CurrAccCode = currAccCode,
                                CreatedDate = DateTime.Now,
                                DocumentNumber = documentNumber,
                                HeaderId = nebimOrderResponse.HeaderID,
                                NebimOrderDetails = new List<NebimOrderDetail>()

                            };

                            foreach (var item in nebimOrderResponse.Lines)
                            {
                                var orderDetail = order.OrderDetails.FirstOrDefault(x => x.ProductInformation.Barcode == item.UsedBarcode);

                                if (orderDetail == null) continue;
                                nebimOrderEntity.NebimOrderDetails.Add(new Domain.Entities.NebimOrderDetail
                                {
                                    Id = orderDetail.Id,
                                    LineId = item.LineID,
                                    UnitPrice = orderDetail.UnitPrice,
                                    Qty1 = orderDetail.Quantity,
                                    VatRate = orderDetail.VatRate,
                                    UsedBarcode = orderDetail.ProductInformation.Barcode
                                });
                            }

                            _unitOfWork.NebimOrderRepository.Create(nebimOrderEntity);
                        }
                    }


                }



                response.Data = "";
                response.Success = true;
            }, (response, e) =>
            {
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        public DataResponse<List<int>> ReadStockTransfers()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<int>>>((response) =>
            {
                var nebimOrders = _unitOfWork.NebimOrderRepository.DbSet().Where(x => x.Cancelled == false && DateTime.Now.AddDays(-15) <= x.CreatedDate).OrderByDescending(x => x.Id)
                .Select(x => x.Id).ToList();

                response.Data = nebimOrders;
                response.Success = true;
            });
        }

        public Response DeleteStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var nebimOrder = _unitOfWork.NebimOrderRepository.DbSet().FirstOrDefault(x => x.Id == orderId);

                var apiUrl = configurations["ApiUrl"];

                var login = GetLogin(apiUrl);

                if (login != null)
                {
                    var cancelled = GetNebimCancelled(apiUrl, login.SessionID, nebimOrder.HeaderId);

                    if (cancelled.Success)
                    {
                        var orderTrans = GetNebimDeleteOrderTransfer(apiUrl, login.SessionID, nebimOrder.HeaderId);
                        if (orderTrans.Success)
                        {
                            nebimOrder.Cancelled = true;
                            _unitOfWork.NebimOrderRepository.Update(nebimOrder);
                        }
                    }


                }



            }, (response, e) =>
            {
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }
        #endregion

        public DataResponse<int> ReadLastStockTransfer()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                var nebimOrder = _unitOfWork.NebimOrderRepository.DbSet().OrderByDescending(x => x.Id).FirstOrDefault();

                response.Data = nebimOrder != null ? nebimOrder.Id : 0;
                response.Success = true;
            });
        }

        public DataResponse<int> ReadStock(string stockCode, string warehouseId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
            }, (response, exception) =>
            {

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        public DataResponse<bool> CreateStocktaking(int stocktakingId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
            }, (response, exception) =>
            {

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        private NebimConnect GetLogin(string apiUrl)
        {
            return this._httpHelper.Get<NebimConnect>($"{apiUrl}/IntegratorService/connect", null, null);

        }

        private DataResponse<List<NebimAddress>> GetNebimAddress(string apiUrl, string sessionID)
        {

            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<NebimAddress>>>((response) =>
                {

                    var nebimProc = new NebimProc
                    {
                        ProcName = "[dbo].[usp_GetAddressList_HELPY]",
                        Parameters = new List<NebimProcParameter>()
                    };
                    response.Data = _httpHelper.Post<NebimProc, List<NebimAddress>>(nebimProc, $"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc", null, null, null, null);
                    if (response.Data == null) return;

                    response.Success = true;
                    response.Message = "";

                });

            }, $"{apiUrl}_usp_GetAddressList_HELPY", 30);
        }

        private DataResponse<List<NebimTaxOfficeCodes>> GetTaxOfficeCodes(string apiUrl, string sessionID)
        {

            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<NebimTaxOfficeCodes>>>((response) =>
                {

                    var nebimProc = new NebimProc
                    {
                        ProcName = "[dbo].[sp_TaxOfficeCode_HELPY]",
                        Parameters = new List<NebimProcParameter>()
                    };
                    response.Data = _httpHelper.Post<NebimProc, List<NebimTaxOfficeCodes>>(nebimProc, $"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc", null, null, null, null);
                    if (response.Data == null) return;

                    response.Success = true;
                    response.Message = "";

                });

            }, $"{apiUrl}_sp_TaxOfficeCode_HELPY", 30);
        }

        private DataResponse<List<string>> GetNebimCancelled(string apiUrl, string sessionID, string HeaderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<string>>>((response) =>
            {

                var nebimProc = new NebimProc
                {
                    ProcName = "spIptalSiparis_HELPY",
                    Parameters = new List<NebimProcParameter>
                    {
                            new NebimProcParameter
                            {
                                Name="SipNo",
                                Value =HeaderId
                            }
                    }
                };
                response.Data = _httpHelper.Post<NebimProc, List<string>>(nebimProc, $"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc", null, null, null, null);
                if (response.Data == null) return;

                response.Success = true;
                response.Message = "";

            });


        }

        private DataResponse<List<string>> GetNebimDeleteOrderTransfer(string apiUrl, string sessionID, string HeaderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<string>>>((response) =>
            {

                var nebimProc = new NebimProc
                {
                    ProcName = "sp_DeleteOrderTrans",
                    Parameters = new List<NebimProcParameter>
                    {
                        new NebimProcParameter
                        {
                               Name="ApplicationCode",
                                Value ="Order"
                        },
                            new NebimProcParameter
                            {
                                Name="OrderHeaderID",
                                Value =HeaderId
                            },
                              new NebimProcParameter
                        {
                               Name="OnlyIntegratedRecords",
                                Value ="0"
                        }
                    }
                };
                response.Data = _httpHelper.Post<NebimProc, List<string>>(nebimProc, $"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc", null, null, null, null);
                if (response.Data == null) return;

                response.Success = true;
                response.Message = "";

            });


        }

        private DataResponse<NebimTaxOfficeCodes> FindNebimTaxOfficeCodes(string apiUrl, string sessionID, string taxOffice)
        {
            return ExceptionHandler.ResultHandle<DataResponse<NebimTaxOfficeCodes>>((response) =>
            {
                var officeCodes = GetTaxOfficeCodes(apiUrl, sessionID);

                if (officeCodes.Success)
                {
                    

                    var officeCode = officeCodes.Data.FirstOrDefault(x => x.TaxOfficeDescription.ReplaceChar().Replace("vergi dairesi", "").TrimEnd() == taxOffice.ReplaceChar().Replace("vergi dairesi", "").TrimEnd());

                    if (officeCode != null)
                    {
                        response.Data = officeCode;
                        response.Success = true;
                    }

                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }
        private DataResponse<NebimAddress> FindDistrict(string apiUrl, string sessionID, string countryCode, string cityName, string districtName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<NebimAddress>>((response) =>
            {
                var nebimAddress = GetNebimAddress(apiUrl, sessionID);

                if (nebimAddress.Success)
                {
                    #region Find Address

                    var country = nebimAddress.Data.FirstOrDefault(x => x.CountryCode.ReplaceChar() == countryCode.ReplaceChar());

                    if (country == null)
                    {
                        country = nebimAddress.Data.FirstOrDefault(x => x.CountryCode == "TR");
                    }

                    cityName = cityName.Trim();


                    var city = nebimAddress.Data.Where(x => x.CountryCode == country.CountryCode).FirstOrDefault(x => x.CityDescription.ReplaceChar() == cityName.ReplaceChar());

                    if (city == null)
                    {
                        city = nebimAddress.Data.FirstOrDefault();
                    }

                    var district = nebimAddress.Data.Where(x => x.CityCode == city.CityCode).FirstOrDefault(x => x.DistrictDescription.ReplaceChar() == districtName.ReplaceChar());


                    if (district == null)
                    {
                        district = nebimAddress.Data.FirstOrDefault(x => x.CityCode == city.CityCode);
                    }



                    response.Data = district;
                    response.Success = true;
                    #endregion
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }
        #endregion
    }
}