﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
  
    public class NebimProcParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class NebimProc
    {
        public string ProcName { get; set; }
        public List<NebimProcParameter> Parameters { get; set; }
    }

}
