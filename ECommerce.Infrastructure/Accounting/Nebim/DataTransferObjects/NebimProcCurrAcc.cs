﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
    public class NebimProcCurrAcc
    {
        public string CurrAccCode { get; set; }
    }
}
