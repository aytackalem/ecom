﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
 
    public class NebimCommunication
    {
        public string CommAddress { get; set; }
        public string CommunicationTypeCode { get; set; }
        public bool CanSendAdvert { get; set; }
    }

    public class NebimCustomerPostalAddress
    {
        //public object PostalAddressID { get; set; }
        public string AddressTypeCode { get; set; }
        public string Address { get; set; }
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public string DistrictCode { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
    }

    public class NebimCustomerRequest
    {
        public int ModelType { get; set; }
        public string CurrAccCode { get; set; }
        public bool IsSubjectToEInvoice { get; set; }
        public string CurrAccDescription { get; set; }
        public bool IsIndividualAcc { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityNum { get; set; }
        public List<NebimCommunication> Communications { get; set; }
        public List<NebimCustomerPostalAddress> PostalAddresses { get; set; }
    }
}
