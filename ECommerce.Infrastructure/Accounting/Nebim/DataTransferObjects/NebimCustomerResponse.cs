﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{

    public class Communication
    {
        public bool CanSendAdvert { get; set; }
        public string CommAddress { get; set; }
        public string CommunicationID { get; set; }
        public string CommunicationTypeCode { get; set; }
        public string ContactID { get; set; }
        public string FormNumber { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsConfirmed { get; set; }
        public string SubCurrAccID { get; set; }
    }

    public class CurrAccDefault
    {
        public string BillingAddressID { get; set; }
        public string BusinessMobileID { get; set; }
        public string CommunicationID { get; set; }
        public string ContactID { get; set; }
        public string EArchieveEMailCommunicationID { get; set; }
        public string EArchieveMobileCommunicationID { get; set; }
        public string GuidedSalesNotificationEmailID { get; set; }
        public string GuidedSalesNotificationPhoneID { get; set; }
        public string HomePhoneID { get; set; }
        public string OfficePhoneID { get; set; }
        public string PersonalMobileID { get; set; }
        public string PostalAddressID { get; set; }
        public string ShippingAddressID { get; set; }
        public string SubCurrAccID { get; set; }
    }

    public class CurrAccExtendedProperties
    {
        public string MarketPlaceCode { get; set; }
    }

    public class CurrAccPersonalInfo
    {
        public bool AvaibleToTravelforTheBusiness { get; set; }
        public bool AvaibleToWorkOnAssignmentAbroad { get; set; }
        public bool BenefitByAgi { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string BloodTypeCode { get; set; }
        public string CurrencyCode { get; set; }
        public string DrivingLicenceType { get; set; }
        public string DrivingLicenceTypeNum { get; set; }
        public string FatherName { get; set; }
        public int GenderCode { get; set; }
        public string HandicapTypeCode { get; set; }
        public string HighSchool { get; set; }
        public DateTime HighSchoolFinishedDate { get; set; }
        public string IdentityCardNum { get; set; }
        public bool IsEducationInProgress { get; set; }
        public bool IsMarried { get; set; }
        public bool IsSmoker { get; set; }
        public string MaidenName { get; set; }
        public string MaladyTypeCode { get; set; }
        public DateTime MarriedDate { get; set; }
        public string MilitaryExcuseReason { get; set; }
        public int MilitaryServiceDelayYear { get; set; }
        public DateTime MilitaryServiceFinishedDate { get; set; }
        public string MilitaryServiceStatusCode { get; set; }
        public int MonthlyIncome { get; set; }
        public string MotherName { get; set; }
        public string Nationality { get; set; }
        public DateTime PassportIssueDate { get; set; }
        public string PassportNum { get; set; }
        public string PersonalInfoID { get; set; }
        public string PrimarySchool { get; set; }
        public DateTime PrimarySchoolFinishedDate { get; set; }
        public string RecidivistTypeCode { get; set; }
        public string RegisteredCityCode { get; set; }
        public string RegisteredDistrictCode { get; set; }
        public int RegisteredFamilyNum { get; set; }
        public string RegisteredFileNum { get; set; }
        public int RegisteredNum { get; set; }
        public int RegisteredRecordNum { get; set; }
        public string RegisteredTown { get; set; }
        public string SGKRecourseLastName { get; set; }
        public string SocialInsuranceNumber { get; set; }
    }

    public class CustomerVerificationPassword
    {
        public int CompanyCode { get; set; }
        public string OfficeCode { get; set; }
        public string Password { get; set; }
        public int PasswordExpiryPeriod { get; set; }
        public DateTime PasswordLastUpdatedDate { get; set; }
        public bool PasswordNeverExpires { get; set; }
        public int PosTerminalID { get; set; }
        public string StoreCode { get; set; }
    }

    public class PostalAddress
    {
        public string Address { get; set; }
        public string AddressID { get; set; }
        public string AddressTypeCode { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNum { get; set; }
        public string CityCode { get; set; }
        public string ContactID { get; set; }
        public string CountryCode { get; set; }
        public string DistrictCode { get; set; }
        public int DoorNum { get; set; }
        public string DrivingDirections { get; set; }
        public int FloorNum { get; set; }
        public bool IsBlocked { get; set; }
        public string PostalAddressID { get; set; }
        public int QuarterCode { get; set; }
        public string QuarterName { get; set; }
        public string SiteName { get; set; }
        public string StateCode { get; set; }
        public int StreetCode { get; set; }
        public string StreetName { get; set; }
        public string SubCurrAccID { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOfficeCode { get; set; }
        public string ZipCode { get; set; }
    }

    public class PostalAddressesWithContact
    {
        public string Address { get; set; }
        public string AddressID { get; set; }
        public string AddressTypeCode { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNum { get; set; }
        public string CityCode { get; set; }
        public string ContactID { get; set; }
        public string CountryCode { get; set; }
        public string DistrictCode { get; set; }
        public int DoorNum { get; set; }
        public string DrivingDirections { get; set; }
        public int FloorNum { get; set; }
        public bool IsBlocked { get; set; }
        public string PostalAddressID { get; set; }
        public int QuarterCode { get; set; }
        public string QuarterName { get; set; }
        public string SiteName { get; set; }
        public string StateCode { get; set; }
        public int StreetCode { get; set; }
        public string StreetName { get; set; }
        public string SubCurrAccID { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOfficeCode { get; set; }
        public string ZipCode { get; set; }
    }

    public class NebimCustomerResponse
    {
        //public int ModelType { get; set; }
        //public DateTime AccountClosingDate { get; set; }
        //public DateTime AccountOpeningDate { get; set; }
        //public DateTime AgreementDate { get; set; }
        //public List<Communication> Communications { get; set; }
        //public int CreditLimit { get; set; }
        public string CurrAccCode { get; set; }
        public CurrAccDefault CurrAccDefault { get; set; }
        //public CurrAccExtendedProperties CurrAccExtendedProperties { get; set; }
        //public CurrAccPersonalInfo CurrAccPersonalInfo { get; set; }
        //public string CurrencyCode { get; set; }
        //public bool CustomerASNNumberIsRequiredForShipments { get; set; }
        //public string CustomerDiscountGrCode { get; set; }
        //public string CustomerPaymentPlanGrCode { get; set; }
        //public CustomerVerificationPassword CustomerVerificationPassword { get; set; }
        //public string DataLanguageCode { get; set; }
        //public string DueDateFormulaCode { get; set; }
        //public string EInvoiceConfirmationRuleCode { get; set; }
        //public string EInvoiceConfirmationRuleID { get; set; }
        //public string FirstName { get; set; }
        //public string IdentityNum { get; set; }
        //public bool IsBlocked { get; set; }
        //public bool IsIndividualAcc { get; set; }
        //public bool IsSubjectToEInvoice { get; set; }
        //public bool IsSubjectToEShipment { get; set; }
        //public bool IsUserConfirmationRequired { get; set; }
        //public string LastName { get; set; }
        //public string MarketPlaceCode { get; set; }
        //public string OfficeCode { get; set; }
        //public string Patronym { get; set; }
        //public int PaymentTerm { get; set; }
        //public List<PostalAddress> PostalAddresses { get; set; }
        public List<PostalAddressesWithContact> PostalAddressesWithContacts { get; set; }
        //public string PromotionGroupCode { get; set; }
        //public string RetailSalePriceGroupCode { get; set; }
        //public string TaxNumber { get; set; }
        //public string TaxOfficeCode { get; set; }
        //public string TitleCode { get; set; }
        //public bool UseBankAccOnStore { get; set; }
        //public string WholesalePriceGroupCode { get; set; }
    }


}
