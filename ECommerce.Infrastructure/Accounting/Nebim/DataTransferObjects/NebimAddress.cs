﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
    public class NebimAddress
    {
        public string CountryCode { get; set; }
        public string CountryDescription { get; set; }
        public string StateCode { get; set; }
        public string StateDescription { get; set; }
        public string CityCode { get; set; }
        public string CityDescription { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictDescription { get; set; }
    }
}
