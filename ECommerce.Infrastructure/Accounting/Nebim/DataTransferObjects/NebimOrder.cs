﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
    public class Discount
    {
        public int DiscountTypeCode { get; set; }
        public int Value { get; set; }
        public string DiscountReasonCode { get; set; }
        public bool IsPercentage { get; set; }
    }

    public class Line
    {
        public string SalesPersonCode { get; set; }
        public string UsedBarcode { get; set; }
        public int Qty1 { get; set; }
        public int LDisRate1 { get; set; }
        public double PriceVI { get; set; }
        public string LineDescription { get; set; }
    }

    public class OrdersViaInternetInfo
    {
        public string SalesURL { get; set; }
        public int PaymentTypeCode { get; set; }
        public string PaymentTypeDescription { get; set; }
        public string PaymentAgent { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime SendDate { get; set; }
    }

    public class Payment
    {
        public int PaymentType { get; set; }
        public string Code { get; set; }
        public string CreditCardTypeCode { get; set; }
        public int InstallmentCount { get; set; }
        public string CurrencyCode { get; set; }
        public string DocCurrencyCode { get; set; }
        public double AmountVI { get; set; }
    }

    public class NebimOrderRequest
    {
        public int ModelType { get; set; }
        public string CustomerCode { get; set; }
        public string InternalDescription { get; set; }
        public string OfficeCode { get; set; }
        public string StoreCode { get; set; }
        public string StoreWarehouseCode { get; set; }
        public string DeliveryCompanyCode { get; set; }
        public int PosTerminalID { get; set; }
        public int ShipmentMethodCode { get; set; }
        public DateTime OrderDate { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsSalesViaInternet { get; set; }
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        public List<Line> Lines { get; set; }
        public OrdersViaInternetInfo OrdersViaInternetInfo { get; set; }

        public NebimPostalAddress PostalAddress { get; set; }

        public List<Discount> Discounts { get; set; }
        public List<Payment> Payments { get; set; }
    }


    public class NebimPostalAddress
    {
        public string Address { get; set; }
        public string CityCode { get; set; }
        public string StateCode { get; set; }
        public string CompanyName { get; set; }
        public string CountryCode { get; set; }
        public string DistrictCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityNum { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOfficeCode { get; set; }
    }

    public class NebimMicroOrderRequest
    {
        public int ModelType { get; set; }
        public int CompanyCode { get; set; }
        public string CustomerCode { get; set; }
        public string DocumentNumber { get; set; }
        public int CustomerTypeCode { get; set; }
        public int TaxTypeCode { get; set; }
        public string DeliveryCompanyCode { get; set; }
        public string Description { get; set; }
        public string InternalDescription { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsSalesViaInternet { get; set; }
        public int POSTerminalID { get; set; }
        public string DocCurrencyCode { get; set; }
        public string ShipmentMethodCode { get; set; }
        public string StoreCode { get; set; }
        public string StoreWarehouseCode { get; set; }
        public string WarehouseCode { get; set; }
        public string IncotermCode1 { get; set; }
        public string IncotermCode2 { get; set; }
        public string PaymentMethodCode { get; set; }
        public string ShippingPostalAddressID { get; set; }
        public string BillingPostalAddressID { get; set; }
        public List<Line> Lines { get; set; }
        public string OfficeCode { get; set; }
        public DateTime OrderDate { get; set; }
        public OrdersViaInternetInfo OrdersViaInternetInfo { get; set; }
        public List<Payment> Payments { get; set; }
        public NebimPostalAddress PostalAddress { get; set; }
    }
}
