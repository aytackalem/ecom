﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Nebim.DataTransferObjects
{
   // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class NebimOrderLine
    {
        //public int ActualPrice { get; set; }
        //public string ActualPriceCurrencyCode { get; set; }
        //public int ActualPriceExchangeRate { get; set; }
        //public bool AllowNotExistReturnBatchCode { get; set; }
        //public double Amount { get; set; }
        //public double AmountVI { get; set; }
        //public string BaseCustomerCode { get; set; }
        //public int BaseCustomerTypeCode { get; set; }
        //public string BaseOrderNumber { get; set; }
        //public string BaseProcessCode { get; set; }
        //public string BaseStoreCode { get; set; }
        //public string BaseSubCurrAccID { get; set; }
        //public string BatchCode { get; set; }
        //public DateTime ClosedDate { get; set; }
        //public string ColorCode { get; set; }
        //public string CostCenterCode { get; set; }
        //public string CurrencyCode { get; set; }
        //public DateTime DeliveryDate { get; set; }
        //public bool DisableBeforeDoSave { get; set; }
        //public bool IsBarcodeRollNumber { get; set; }
        //public bool IsTaxIncludedPrice { get; set; }
        //public string ITAtt01 { get; set; }
        //public string ITAtt02 { get; set; }
        //public string ITAtt03 { get; set; }
        //public string ITAtt04 { get; set; }
        //public string ITAtt05 { get; set; }
        //public string ItemCode { get; set; }
        //public string ItemDim1Code { get; set; }
        //public string ItemDim2Code { get; set; }
        //public string ItemDim3Code { get; set; }
        //public int ItemTypeCode { get; set; }
        //public double LDiscount1 { get; set; }
        //public int LDiscount2 { get; set; }
        //public int LDiscount3 { get; set; }
        //public int LDiscount4 { get; set; }
        //public int LDiscount5 { get; set; }
        //public double LDiscountVI1 { get; set; }
        //public int LDiscountVI2 { get; set; }
        //public int LDiscountVI3 { get; set; }
        //public int LDiscountVI4 { get; set; }
        //public int LDiscountVI5 { get; set; }
        //public int LDisRate1 { get; set; }
        //public int LDisRate2 { get; set; }
        //public int LDisRate3 { get; set; }
        //public int LDisRate4 { get; set; }
        //public int LDisRate5 { get; set; }
        //public double LineAmount { get; set; }
        //public string LineDescription { get; set; }
        //public int MinPrice { get; set; }
        //public bool MinPriceTaxIncluded { get; set; }
        //public double NetAmount { get; set; }
        //public string OrderCancelReasonCode { get; set; }
        //public int OrderLineBOMID { get; set; }
        //public int OrderLineSumID { get; set; }
        //public NebimOrderOrderOpticalProductLine OrderOpticalProductLine { get; set; }


        //public bool OTAttChanged { get; set; }
        //public string PaymentPlanCode { get; set; }
        //public int PCTAmount { get; set; }
        //public string PCTCode { get; set; }
        //public int PCTRate { get; set; }
        //public DateTime PlannedDateOfLading { get; set; }
        //public double Price { get; set; }
        //public string PriceListLineID { get; set; }
        //public double PriceVI { get; set; }

        //public int Qty2 { get; set; }
        //public string RelationCurrencyCode { get; set; }
        //public string SalespersonCode { get; set; }
        //public int SortOrder { get; set; }
        //public string SupportRequestHeaderID { get; set; }
        //public string SupportRequestNumber { get; set; }
        //public double TaxBase { get; set; }
        //public int TDiscount1 { get; set; }
        //public int TDiscount2 { get; set; }
        //public int TDiscount3 { get; set; }
        //public int TDiscount4 { get; set; }
        //public double TDiscount5 { get; set; }
        //public int TDiscountVI1 { get; set; }
        //public int TDiscountVI2 { get; set; }
        //public int TDiscountVI3 { get; set; }
        //public int TDiscountVI4 { get; set; }
        //public double TDiscountVI5 { get; set; }
        //public double VatAmount { get; set; }
        //public string VatCode { get; set; }
        //public int VatDeducation { get; set; }
        //public int VatRate { get; set; }

        public string LineID { get; set; }

        public string UsedBarcode { get; set; }

        public int Qty1 { get; set; }
    }

    public class NebimOrderOrderHeaderExtension
    {
        public string InsuranceAgencyCode { get; set; }
        public bool IsInstantReserve { get; set; }
    }

    public class NebimOrderOrderOpticalProductLine
    {
        public int Addition { get; set; }
        public int Axis { get; set; }
        public int BaseCurveRadius { get; set; }
        public int ContributionAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string DistanceReading { get; set; }
        public int EffectiveDiameter { get; set; }
        public int ExchangeRate { get; set; }
        public string LeftRightFrame { get; set; }
        public string OpticalSutCode { get; set; }
        public string OrderOpticalProductID { get; set; }
        public string OrderOpticalProductLineID { get; set; }
        public int Prism { get; set; }
        public string ProtocolNumber { get; set; }
        public int PupillaryDistance { get; set; }
        public int SegmentHeight { get; set; }
        public bool UsePolished { get; set; }
    }

    public class NebimOrderOrdersViaInternetInfo
    {
        public string PaymentAgent { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PaymentTypeCode { get; set; }
        public string PaymentTypeDescription { get; set; }
        public string SalesURL { get; set; }
        public DateTime SendDate { get; set; }
        public NebimOrderSendTime SendTime { get; set; }
    }

    public class NebimOrderOrderTime
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public int Milliseconds { get; set; }
        public long Ticks { get; set; }
        public int Days { get; set; }
        public double TotalDays { get; set; }
        public double TotalHours { get; set; }
        public int TotalMilliseconds { get; set; }
        public double TotalMinutes { get; set; }
        public double TotalSeconds { get; set; }
    }

    public class NebimOrderPostalAddress
    {
        public string Address { get; set; }
        public int AddressID { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNum { get; set; }
        public string CityCode { get; set; }
        public string CompanyName { get; set; }
        public string CountryCode { get; set; }
        public string DistrictCode { get; set; }
        public int DoorNum { get; set; }
        public string FirstName { get; set; }
        public int FloorNum { get; set; }
        public string IdentityNum { get; set; }
        public string LastName { get; set; }
        public int QuarterCode { get; set; }
        public string QuarterName { get; set; }
        public string SiteName { get; set; }
        public string StateCode { get; set; }
        public int StreetCode { get; set; }
        public string StreetName { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOfficeCode { get; set; }
        public string ZipCode { get; set; }
    }

    public class NebimOrderResponse
    {
        //public int ModelType { get; set; }
        //public bool AllowValidateEmptyPrice { get; set; }
        //public string ApplicationCode { get; set; }
        //public string ApplicationID { get; set; }
        //public bool AutoGenerateUniqueNumber { get; set; }
        //public DateTime AverageDueDate { get; set; }
        //public string BillingPostalAddressID { get; set; }
        //public bool BlockCheckOutProcess { get; set; }
        //public int CompanyCode { get; set; }
        //public string ContactID { get; set; }
        //public DateTime CreditableConfirmedDate { get; set; }
        //public string CreditableConfirmedUser { get; set; }
        //public string CustomerCode { get; set; }
        //public string DeliveryCompanyCode { get; set; }
        //public string Description { get; set; }
        //public bool DisableInsertDefaultAttributes { get; set; }
        //public int DiscountReasonCode { get; set; }
        //public string DocCurrencyCode { get; set; }
        //public string DocumentNumber { get; set; }
        //public string DOVCode { get; set; }
        //public int ExchangeRate { get; set; }
        //public int ExchangeTypeCode { get; set; }
        //public string ExportFileNumber { get; set; }
        //public string GLTypeCode { get; set; }
        //public string GuarantorContactID { get; set; }
        //public string GuarantorContactID2 { get; set; }
        public string HeaderID { get; set; }
        //public string ImportFileNumber { get; set; }
        //public string InternalDescription { get; set; }
        //public bool IsCompleted { get; set; }
        //public bool IsCreditableConfirmed { get; set; }
        //public bool IsCreditSale { get; set; }
        //public bool IsInstantForOpenOrder { get; set; }
        //public bool IsLocked { get; set; }
        //public bool IsPrinted { get; set; }
        //public bool IsProposalBased { get; set; }
        //public bool IsSalesViaInternet { get; set; }
        //public bool IsSuspended { get; set; }
        //public bool IsTaxIncluded { get; set; }
        //public int ItemBalanceProcessFlowCode { get; set; }
        public List<NebimOrderLine> Lines { get; set; }
        //public string LocalCurrencyCode { get; set; }
        //public string OfficeCode { get; set; }
        //public DateTime OrderDate { get; set; }
        //public NebimOrderOrderHeaderExtension OrderHeaderExtension { get; set; }
        public string OrderNumber { get; set; }
        //public NebimOrderOrdersViaInternetInfo OrdersViaInternetInfo { get; set; }
        //public NebimOrderOrderTime OrderTime { get; set; }
        //public string PaymentPlanCode { get; set; }
        //public int PaymentTerm { get; set; }
        //public NebimOrderPostalAddress PostalAddress { get; set; }
        //public int POSTerminalID { get; set; }
        //public string RoundsmanCode { get; set; }
        //public bool SetDefaultLotForItem { get; set; }
        //public string ShipmentMethodCode { get; set; }
        //public string ShippingPostalAddressID { get; set; }
        //public bool ShouldSaveITAttributes { get; set; }
        //public string StoreCode { get; set; }
        //public string StoreWarehouseCode { get; set; }
        //public bool SuppressItemDiscount { get; set; }
        //public int SurplusOrderQtyToleranceRate { get; set; }
        //public int TaxExemptionCode { get; set; }
        //public int TaxTypeCode { get; set; }
        //public int TDisRate1 { get; set; }
        //public int TDisRate2 { get; set; }
        //public int TDisRate3 { get; set; }
        //public int TDisRate4 { get; set; }
        //public int TDisRate5 { get; set; }
        //public bool UserLocked { get; set; }
        //public string WarehouseCode { get; set; }
        //public string WithHoldingTaxTypeCode { get; set; }
    }

    public class NebimOrderSendTime
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public int Milliseconds { get; set; }
        public int Ticks { get; set; }
        public int Days { get; set; }
        public int TotalDays { get; set; }
        public int TotalHours { get; set; }
        public int TotalMilliseconds { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalSeconds { get; set; }
    }


}
