﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	[Table("OdemeBaslik")]
	public partial class OdemeBaslik
	{
		#region Properties
		public Int32 Id { get; set; }

		public Byte Durum { get; set; }

		public Int16 KasaId { get; set; }

		public Int16 KullaniciId { get; set; }

		public Byte BelgeTipi { get; set; }

		public String BelgeNo { get; set; }

		public DateTime Tarih { get; set; }

		public Int32 CariId { get; set; }

		public Decimal Kur { get; set; }

		public Int32 OzelKod1 { get; set; }

		public Int32 OzelKod2 { get; set; }

		public DateTime OlusturmaTarihi { get; set; }

		public Int32 GenelKod1 { get; set; }

		public Int32 GenelKod2 { get; set; }

		public String Aciklama { get; set; }
		#endregion

		#region Navigation Properties
		#endregion
	}
}
