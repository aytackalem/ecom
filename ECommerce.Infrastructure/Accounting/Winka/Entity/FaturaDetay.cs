using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	[Table("FaturaDetay")]
	public partial class FaturaDetay
	{
		#region Properties
		public Int32 Id { get; set; }

		public Int32 BaslikId { get; set; }

		public Byte OdemeTipi { get; set; }

		public DateTime OdemeTarihi { get; set; }

		public String OdemeAciklamasi { get; set; }

		public Int32 TasiyiciId { get; set; }

		public DateTime SevkTarihi { get; set; }

		public Int32 WebAdresi { get; set; }
		#endregion

		#region Navigation Properties
		#endregion
	}
}