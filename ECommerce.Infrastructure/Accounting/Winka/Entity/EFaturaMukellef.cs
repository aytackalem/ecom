﻿using Dapper.Contrib.Extensions;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("EFaturaMukellef")]
    public class EFaturaMukellef
    {
        public Int32 Id { get; set; }

        public string Adi { get; set; }

        public string VergiNo { get; set; }
    }
}
