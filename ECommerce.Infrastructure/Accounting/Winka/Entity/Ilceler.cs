﻿using Dapper.Contrib.Extensions;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("Ilceler")]
    public class Ilceler
    {
        public Int32 Id { get; set; }

        public Int32 IlId { get; set; }

        public Int32 IlceId { get; set; }

        public string Adi { get; set; }
    }
}
