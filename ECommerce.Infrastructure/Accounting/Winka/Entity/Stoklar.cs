using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	public partial class Stoklar
	{
		#region Property
		public Int32 Id { get; set; }

		public String StokKodu { get; set; }

		public String StokAdi { get; set; }

		public Byte StokTipi { get; set; }

		public Int32 AnaBirim { get; set; }

		public Int32 VarsayilanBirim { get; set; }

		public Int32 SiparisBirimi { get; set; }

		public Boolean Durum { get; set; }

		public Int32 OzelKod1 { get; set; }

		public Int32 OzelKod2 { get; set; }

		public Int32 OzelKod3 { get; set; }

		public Int32 OzelKod4 { get; set; }

		public Int32 OzelKod5 { get; set; }

		public Int32 OzelKod6 { get; set; }

		public Decimal KritikSeviye { get; set; }

		public Decimal OptimumSeviye { get; set; }

		public Boolean HemenTeslim { get; set; }

		public DateTime Tarih { get; set; }

		public Int16 KurulumSuresi { get; set; }

		public Decimal Prim { get; set; }

		public Byte Taksit { get; set; }

		public Decimal AlisFiyati { get; set; }

		public String AlisPB { get; set; }

		public String Tanimi { get; set; }

		public Int32 Grubu { get; set; }

		public Int32 HesapId { get; set; }

		public Decimal SatisIskonto { get; set; }

		public Decimal AlisIskonto { get; set; }

		public Int32 OzelKod0 { get; set; }

		public String Seri { get; set; }
		#endregion
	}
}