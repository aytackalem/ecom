using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	public partial class Barkodlar
	{
		#region Property
		public Int32 Id { get; set; }

		public Int32 BirimId { get; set; }

		public Byte Std { get; set; }

		public String Barkodu { get; set; }
		#endregion
	}
}