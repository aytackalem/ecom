using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("Cariler")]
    public class Cariler
    {
        #region Properties
        public Int32 Id { get; set; }

        public Byte Durum { get; set; } = 0;

        public Int32 AnaHesapId { get; set; } = 0;

        public String Kodu { get; set; }

        public String Adi { get; set; }

        public String Unvani { get; set; }

        public String Yetkili { get; set; } = string.Empty;

        public DateTime? DogumTarihi { get; set; }

        public Byte Tipi { get; set; } = 4;

        public Int16 SubeId { get; set; } = 1;

        public String ParaBirimi { get; set; } = "TL";

        public String Aciklama { get; set; } = string.Empty;

        public Int32 MahalleId { get; set; } = 0;

        public String FaturaAdresi { get; set; }

        public String SevkAdresi { get; set; }

        public String Telefon1 { get; set; }

        public String Telefon2 { get; set; } = string.Empty;

        public String Faks { get; set; } = string.Empty;

        public String GSM { get; set; }

        public String EMail { get; set; }

        public String WebAdresi { get; set; } = string.Empty;

        public String VergiDairesi { get; set; }

        public String VergiNumarasi { get; set; }

        public Decimal KrediLimiti { get; set; } = 0;

        public Decimal AcikHesapLimiti { get; set; } = 0;

        public Int16 Opsiyon { get; set; } = 0;

        public Decimal Iskonto { get; set; } = 0;

        public Decimal GecikmeFaizi { get; set; } = 0;

        public Decimal AylikVade { get; set; } = 0;

        public Boolean SatisYapilmasin { get; set; } = false;

        public Boolean TahsilatYapilmasin { get; set; }

        public Int32 OzelKod1 { get; set; } = 0;

        public Int32 OzelKod2 { get; set; } = 0;

        public Int32 OzelKod3 { get; set; } = 0;

        public DateTime Tarih { get; set; }

        public DateTime? OrtalamaDevirTarihi { get; set; }

        public Byte ArtiPuan { get; set; } = 0;

        public Boolean OtomasyonDisi { get; set; } = false;

        public Int32 BorcHesapId { get; set; } = 0;

        public Int32 AlacakHesapId { get; set; } = 0;

        public Byte SatisFiyati { get; set; } = 0;

        public String IsYeri { get; set; } = string.Empty;

        public Int32 OzelKod0 { get; set; } = 0;

        public Int32 IlceId { get; set; }

        public Byte EFaturaSenaryo { get; set; } = 0;

        public Int32 GiderTipi { get; set; } = 0;

        public Int32 AdresId { get; set; } = 0;

        public Int16 UlkeId { get; set; }
        #endregion
    }
}