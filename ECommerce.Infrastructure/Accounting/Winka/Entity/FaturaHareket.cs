using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("FaturaHareket")]
    public partial class FaturaHareket
    {
        #region Property
        public Int32 Id { get; set; }

        public Byte Durum { get; set; }

        public Int32 BaslikId { get; set; }

        public Int32 BirimId { get; set; }

        public Int32 BarkodId { get; set; }

        public String StokAdi { get; set; }

        public Decimal Fiyat { get; set; }

        public String ParaBirimi { get; set; }

        public Decimal Kur { get; set; }

        public Decimal Miktar { get; set; }

        public Decimal KDV { get; set; }

        public Decimal OTV { get; set; }

        public Decimal Iskonto1 { get; set; }

        public Decimal Iskonto2 { get; set; }

        public Decimal Iskonto3 { get; set; }

        public Decimal Iskonto4 { get; set; }

        public DateTime? Termin { get; set; }

        public Byte Opsiyon { get; set; }

        public Int16 Personel { get; set; }

        public Decimal TransferMiktari { get; set; }

        public Int16 SevkDeposu { get; set; }

        public Decimal Prim { get; set; }

        public Byte SatisSekli { get; set; }

        public Int32 ProtokolId { get; set; }

        public String Not1 { get; set; }

        public String Not2 { get; set; }

        public String Not3 { get; set; }

        public String Not4 { get; set; }

        public Int32 Kod1 { get; set; }

        public Int32 Kod2 { get; set; }

        public Int32 Kod3 { get; set; }

        public Int32 Kod4 { get; set; }

        public int? MuafiyetKodu { get; set; } = null;
        #endregion
    }
}