﻿using Dapper.Contrib.Extensions;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("Iller")]
    public class Iller
    {
        public Int32 Id { get; set; }

        public Int32 IlId { get; set; }

        public string Adi { get; set; }
    }
}
