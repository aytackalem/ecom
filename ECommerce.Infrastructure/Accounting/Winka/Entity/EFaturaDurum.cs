﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	[Table("EFaturaDurum")]
	public partial class EFaturaDurum
	{
		#region Properties
		public Int32 Id { get; set; }

		public Byte Durum { get; set; }

		public Int32 BaslikId { get; set; }

		public Byte FaturaDurumu { get; set; }

		public Int32 GIB_Durum { get; set; }

		public Byte Senaryo { get; set; }

		public Byte Tipi { get; set; }

		public Guid ETTN { get; set; }

		public Guid ZarfId { get; set; }

		public DateTime? Portal { get; set; }

		public DateTime? Gonderim { get; set; }

		public DateTime? Alinma { get; set; }

		public DateTime? Yanit { get; set; }

		public String Aciklama { get; set; }

		public String GIBAciklama { get; set; }

		public String FaturaDurumAciklama { get; set; }
		#endregion

		#region Navigation Properties
		#endregion
	}
}
