using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
	[Table("FaturaBaslik")]
	public class FaturaBaslik
	{
		#region Properties
		public Int32 Id { get; set; }

		public Byte Durum { get; set; } = 0;

		public Int16 KasaId { get; set; }

		public Byte BelgeTipi { get; set; }

		public Boolean KDVDahil { get; set; }

		public DateTime Tarih { get; set; }

		public Int16 SubeId { get; set; }

		public Int32 CariId { get; set; }

		public String BelgeNo { get; set; }

		public Decimal Kur { get; set; }

		public String Aciklama { get; set; }

		public Int16 KullaniciId { get; set; }

		public Decimal Iskonto1 { get; set; }= 0;

		public Decimal Iskonto2 { get; set; } = 0;

		public Int32 OzelKod1 { get; set; } = 0;

		public Int32 OzelKod2 { get; set; } = 0;

		public DateTime OlusturmaTarihi { get; set; }

		public Byte Kapali { get; set; } = 0;

		public Int32 OzelKod3 { get; set; } = 0;

		public Int32 OzelKod4 { get; set; } = 0;

		public Int32 GenelKod1 { get; set; } = 0;

		public Int32 GenelKod2 { get; set; } = 0;

		public Int32 CariAdresId { get; set; } = 0;

		public Byte GonderimSekli { get; set; }
		#endregion
	}
}