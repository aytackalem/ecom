﻿using Dapper.Contrib.Extensions;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Entity
{
    [Table("Ulkeler")]
    public class Ulkeler
    {
        public short Id { get; set; }

        public string Kodu { get; set; }

        public string Adi { get; set; }
    }
}
