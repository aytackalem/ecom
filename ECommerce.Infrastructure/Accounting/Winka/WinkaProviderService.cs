﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Accounting.Winka.DataTransferObjects;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository;
using ECommerce.Infrastructure.Accounting.Winka.UnitOfWork;
using ECommerce.Infrastructure.Accounting.Winka.UnitOfWork.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ECommerce.Accounting.Winka
{
    public class WinkaProviderService : IAccountingProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private IWinkaUnitOfWork _winkaUnitOfWork;

        private readonly Dictionary<string, int> _webAddress = new Dictionary<string, int>
        {
            { "HB", 8827 },
            { "N11", 8826 },
            { "TY", 10031 },
            { "Web", 8545},
            { "PA",12638 },
            { "MN",31260 },
            { "TYE",33498 }

        };

        private readonly Dictionary<string, int> _cargoCurrent = new Dictionary<string, int>
        {
            { "S", 33084 },
            { "U", 18308},
            { "MNGHB", 28987 },
            { "M" , 27886 },
            {"TYEX",40099 },
            {"TYFT",441558 },
            {"HX",27886 }
        };

        private readonly Dictionary<string, IdValue> _paymentType = new Dictionary<string, IdValue>
        {
           { "", new IdValue { Id = 0, Name = "Yok" } },
           { "DO", new IdValue { Id = 10, Name = "Kapıda Ödeme" } },
           { "PDO", new IdValue { Id = 10, Name = "Kapıda Ödeme" } },
           { "C", new IdValue { Id = 30, Name = "Havale"} },
           { "OO", new IdValue { Id = 48, Name = "Kredi Kartı" } }
        };

        private readonly Dictionary<int, IdValue> _paymentTypeReturn = new Dictionary<int, IdValue>
        {
           { 0, new IdValue { Id = 0, Name = "Yok" } },

           { 1005, new IdValue { Id = 1,Name = "Ödeme Aracısı" } },
           { 1008, new IdValue { Id = 1,Name = "Ödeme Aracısı" } },
           { 1004, new IdValue { Id = 1,Name = "Ödeme Aracısı" } },
           { 1006, new IdValue { Id = 10, Name = "Kapıda Ödeme" } },
           { 1007, new IdValue { Id = 10, Name = "Kapıda Ödeme" } },
           { 1000, new IdValue { Id = 30, Name = "Havale"} },
           { 1001, new IdValue { Id = 48, Name = "Kredi Kartı" } }
        };
        #endregion,

        #region Constructors
        public WinkaProviderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId, Dictionary<string, string> configurations)
        {
            StringBuilder stringBuilder = new();

            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                response.Data = new();

                stringBuilder.Append("Sipariş bulunuyor.");

                #region Read Order
                var order = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Customer)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(x => x.ProductInformation.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.OrderShipments)
                    .Include(o => o.Payments)
                        .ThenInclude(x => x.PaymentType)
                        .ThenInclude(x => x.PaymentTypeTranslations)
                    .Include(x => x.Marketplace)
                    .Include(x => x.Company)
                    .FirstOrDefault(x => x.Id == orderId);

                var orderNotFound = order == null;
                if (orderNotFound)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }
                #endregion

                stringBuilder.Append("Sipariş bulundu.");

                var winkaConnectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);

                stringBuilder.Append("Transaction basliyor.");

                _winkaUnitOfWork.BeginTransaction();

                stringBuilder.Append("Transaction basladi.");

                var carilerRepository = _winkaUnitOfWork.CarilerRepository;
                var faturaBaslikRepository = _winkaUnitOfWork.FaturaBaslikRepository;
                var faturaDetayRepository = _winkaUnitOfWork.FaturaDetayRepository;
                var odemeBaslikRepository = _winkaUnitOfWork.OdemeBaslikRepository;
                var eFaturaDurumRepository = _winkaUnitOfWork.EFaturaDurumRepository;
                var eFaturaMukellefRepository = _winkaUnitOfWork.EFaturaMukellefRepository;

                #region Check Duplicate
                var documentNumber = faturaBaslikRepository.ReadByOrderId(orderId);
                var documentFound = !string.IsNullOrEmpty(documentNumber);
                if (documentFound)
                {
                    response.Success = true;

                    response.Data = new AccountingInfo
                    {
                        Guid = documentNumber,
                        AccountingCompanyId = "W",
                        Url = null
                    };

                    _winkaUnitOfWork.RollbackTransaction();

                    return;
                }
                #endregion

                EFaturaMukellef eFaturaMukellef = null;
                var commercial = order.Commercial;
                var eInvoicePayer = false;
                if (commercial)
                {
                    eFaturaMukellef = eFaturaMukellefRepository.ReadByTaxNumber(order.OrderInvoiceInformation.TaxNumber);
                    eInvoicePayer = eFaturaMukellef != null;
                }

                stringBuilder.Append("Fatura kaydediliyor.");

                #region Cariler
                var customerId = $"{configurations["CurrentSerie"]}{orderId}{order.Customer.Id}";
                var cariler = _winkaUnitOfWork.CarilerRepository.Read(customerId.ToString());
                if (cariler == null)
                {
                    var winkaUlkeRepository = _winkaUnitOfWork.UlkelerRepository;

                    short ulkeId = 0;
                    int ilceId = 0;

                    var winkaUlke = winkaUlkeRepository
                        .Find(order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name);
                    var winkaUlkeNotFound = winkaUlke == null;
                    if (winkaUlkeNotFound)
                    {
                        response.Success = false;
                        response.Message = "Ülke bulunamadı. Lütfen kontrol ediniz.";

                        _winkaUnitOfWork.RollbackTransaction();

                        return;
                    }

                    ulkeId = winkaUlke.Id;

                    var isTurkey = order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name.Trim() == "Türkiye";
                    if (isTurkey)
                    {
                        var cityName = order.OrderInvoiceInformation.Neighborhood.District.City.Name;

                        if (cityName == "Afyon")
                        {
                            cityName = "AFYONKARAHİSAR";
                        }
                        else if (cityName == "Mersin")
                        {
                            cityName = "MERSİN(İÇEL)";
                        }


                        var winkaIlRepository = _winkaUnitOfWork.IllerRepository;
                        var winkaIl = winkaIlRepository
                            .Find(cityName);
                        var winkaIlNotFound = winkaIl == null;
                        if (winkaIlNotFound)
                        {
                            response.Success = false;
                            response.Message = "İl bulunamadı. Lütfen kontrol ediniz.";

                            _winkaUnitOfWork.RollbackTransaction();

                            return;
                        }

                        var winkaIlceRepository = _winkaUnitOfWork.IlcelerRepository;
                        var winkaIlce = winkaIlceRepository
                            .Find(winkaIl.Id, order.OrderInvoiceInformation.Neighborhood.District.Name);
                        var winkaIlceNotFound = winkaIlce == null;

                        if (winkaIlceNotFound == false)
                        {
                            ilceId = winkaIlce.Id;
                        }


                    }

                    string adi, unvani;
                    unvani = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}";
                    adi = unvani.Length > 30
                        ? unvani[..30]
                        : unvani;
                    if (eInvoicePayer)
                    {
                        unvani = eFaturaMukellef.Adi;
                        adi = eFaturaMukellef.Adi.Length > 30
                            ? eFaturaMukellef.Adi[..30]
                            : eFaturaMukellef.Adi;
                    }

                    cariler = new Cariler
                    {
                        Kodu = customerId.ToString(),
                        Adi = adi.ToUpper(new CultureInfo("tr-TR")),
                        Unvani = unvani.ToUpper(new CultureInfo("tr-TR")),
                        FaturaAdresi = order.OrderInvoiceInformation.Address,
                        SevkAdresi = order.OrderDeliveryAddress.Address,
                        Telefon1 = order.OrderDeliveryAddress.PhoneNumber,
                        GSM = order.OrderDeliveryAddress.PhoneNumber,
                        EMail = order.OrderInvoiceInformation.Mail,

                        VergiDairesi = commercial
                            ? order.OrderInvoiceInformation.TaxOffice
                            : string.Empty,
                        VergiNumarasi = commercial
                            ? order.OrderInvoiceInformation.TaxNumber
                            : "11111111111",

                        Tarih = DateTime.Now,
                        IlceId = ilceId,
                        UlkeId = ulkeId,
                        EFaturaSenaryo = (byte)(eInvoicePayer ? 1 : 0)
                    };
                    _winkaUnitOfWork.CarilerRepository.Create(cariler);
                }
                #endregion

                #region FaturaBaslik
                var serie = configurations[eInvoicePayer ? "EInvoiceSerie" : "EArchiveSerie"];

                if (string.IsNullOrEmpty(serie))
                {
                    response.Message = "Konfigurasyon hatası.";

                    _winkaUnitOfWork.RollbackTransaction();

                    return;
                }

                var belgeNo = _winkaUnitOfWork.FaturaBaslikRepository.MaxInvoiceBelgeNo(serie);
                string invoiceNumber = $"{serie}{DateTime.Now.Year}{belgeNo.PadLeft(9, '0')}";

                var faturaBaslik = new FaturaBaslik
                {
                    KasaId = Convert.ToInt16(configurations["InvoiceDealerNumber"]),
                    BelgeTipi = 23,
                    KDVDahil = false,
                    Tarih = DateTime.Now,
                    SubeId = Convert.ToInt16(configurations["InvoiceBranchNumber"]),
                    CariId = cariler.Id,
                    BelgeNo = invoiceNumber,//Bulanacak
                    Kur = 1,
                    Aciklama = $"{order.Id}",
                    KullaniciId = 1,
                    OlusturmaTarihi = DateTime.Now,
                    GonderimSekli = (byte)(eInvoicePayer ? 1 : 3)
                };
                _winkaUnitOfWork.FaturaBaslikRepository.Create(faturaBaslik);
                #endregion

                #region FaturaDetay
                if (faturaBaslik.Id > 0)
                {
                    var payment = order.Payments.FirstOrDefault();
                    var paymentType = new IdValue();
                    if (payment != null)
                    {
                        paymentType = _paymentType.ContainsKey(payment.PaymentTypeId)
                            ? _paymentType.Single(pt => pt.Key == payment.PaymentTypeId).Value
                            : new IdValue
                            {
                                Id = 0,
                                Name = "Yok"
                            };
                    }

                    if (order.OrderSourceId == 1) //PAZARYERI
                        paymentType = new IdValue
                        {
                            Id = 1,
                            Name = "Ödeme Aracısı"
                        };

                    var shipment = order.OrderShipments.FirstOrDefault();

                    var source = "Web";
                    if (!string.IsNullOrEmpty(order.MarketplaceId) && order.OrderSourceId == 1) //pazaryeri ise
                    {
                        source = order.MarketplaceId;
                    }

                    var tasiyiciId = 0;
                    var webAdresi = 0;
                    if (order.Company.Name == "Saade")
                    {
                        tasiyiciId = 2217;
                        webAdresi = 65;
                    }
                    else
                    {
                        tasiyiciId = _cargoCurrent.ContainsKey(shipment.ShipmentCompanyId)
                            ? _cargoCurrent.Single(cc => cc.Key == shipment.ShipmentCompanyId).Value
                            : 0;
                        webAdresi = _webAddress.ContainsKey(source)
                            ? _webAddress.Single(wa => wa.Key == source).Value
                            : 0;
                    }

                    _winkaUnitOfWork.FaturaDetayRepository.Create(new FaturaDetay
                    {
                        BaslikId = faturaBaslik.Id,
                        OdemeTipi = paymentType.Id,//bulunacak
                        OdemeTarihi = DateTime.Now,
                        OdemeAciklamasi = payment != null
                            ? payment.PaymentType.PaymentTypeTranslations.FirstOrDefault().Name
                            : order.Marketplace.Name,
                        TasiyiciId = tasiyiciId, //HANGI KARGO FIRMASI ILE TASINDI
                        SevkTarihi = DateTime.Now,
                        WebAdresi = webAdresi //SIPARIS HANGI WEB SITESINDEN VERILDI
                    });

                    foreach (var orderDetail in order.OrderDetails)
                    {
                        var stoklar = _winkaUnitOfWork.StoklarRepository.ReadByStockCode(orderDetail.ProductInformation.StockCode);
                        var barkodlar = _winkaUnitOfWork.BarkodlarRepository.ReadByUnitId(stoklar.AnaBirim);
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = stoklar.AnaBirim,
                            BarkodId = barkodlar.Id,
                            Miktar = Convert.ToDecimal(orderDetail.Quantity),
                            StokAdi = stoklar.StokAdi,
                            Fiyat = orderDetail.VatExcUnitPrice,
                            ParaBirimi = order.CurrencyId,
                            Kur = 1,
                            //Mikro ihracat icin KDV 0 atiliyor!
                            KDV = order.Micro
                                ? 0M
                                : Convert.ToDecimal(orderDetail.VatRate * 100),
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = (byte)(commercial ? 3 : 0),
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                            //Mikro ihracat icin 301 kodu atiliyor!
                            MuafiyetKodu = order.Micro
                                ? 301
                                : null
                        });
                    }

                    if (order.SurchargeFee > 0)
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = 16533,
                            BarkodId = 0,
                            Miktar = 1,
                            StokAdi = "Kapıda ödeme hizmet bedeli",
                            Fiyat = order.SurchargeFee / 1.20m,
                            ParaBirimi = order.CurrencyId,
                            Kur = 1,
                            KDV = 20,
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = 0,
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                        });

                    if (order.CargoFee > 0)
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = 5750,
                            BarkodId = 0,
                            Miktar = 1,
                            StokAdi = "Kargo Bedeli",
                            Fiyat = order.CargoFee / 1.20m,
                            ParaBirimi = order.CurrencyId,
                            Kur = 1,
                            KDV = 20,
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = 0,
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                        });
                }

                var odemeBaslik = new OdemeBaslik
                {
                    Durum = 0,
                    KasaId = 1,
                    KullaniciId = 1,
                    BelgeTipi = 101,
                    BelgeNo = belgeNo,
                    Tarih = DateTime.Now,
                    CariId = cariler.Id,
                    Kur = 1,
                    OzelKod1 = 0,
                    OzelKod2 = 2,
                    OlusturmaTarihi = DateTime.Now,
                    GenelKod1 = 0,
                    GenelKod2 = 0,
                    Aciklama = string.Empty
                };
                _winkaUnitOfWork.OdemeBaslikRepository.Create(odemeBaslik);

                byte tipi = order.Micro ? (byte)3 : (byte)0;

                if (odemeBaslik.Id > 0)
                {
                    _winkaUnitOfWork.EFaturaDurumRepository.Create(new EFaturaDurum
                    {
                        Durum = 0,
                        BaslikId = faturaBaslik.Id,
                        FaturaDurumu = 0,
                        GIB_Durum = 0,
                        Senaryo = (byte)(eInvoicePayer ? 1 : 3),
                        Tipi = tipi,
                        Portal = null,
                        Gonderim = null,
                        Alinma = null,
                        Yanit = null,
                        Aciklama = string.Empty,
                        GIBAciklama = string.Empty,
                        FaturaDurumAciklama = string.Empty
                    });
                }
                #endregion

                stringBuilder.Append("Fatura kaydedildi.");

                stringBuilder.Append("Transfer fisleri siliniyor.");

                _winkaUnitOfWork.FaturaBaslikRepository.DeleteStockTransfer(orderId);
                _winkaUnitOfWork.FaturaHareketRepository.DeleteStockTransfer(orderId);

                stringBuilder.Append("Transfer fisleri silindi.");

                stringBuilder.Append("Transaction commit ediliyor.");

                _winkaUnitOfWork.CommitTransaction();

                stringBuilder.Append("Transaction commit edildi.");

                response.Success = true;
                response.Data = new AccountingInfo
                {
                    Guid = invoiceNumber,
                    AccountingCompanyId = "W",
                    Url = null
                };
            }, (response, e) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();

                response.Message = $"Bilinmeyen bir hata oluştu. {stringBuilder}";
            });
        }

        public DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                /*
                 * SIPARIS NUMARALARI KONTROL EDILIYOR.
                 */
                if (orderIds.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Sipariş numarası gönderilmedi.";
                    return;
                }

                orderIds.OrderBy(x => x);

                /*
                 * SIPARISLER HELPY DEN CEKILIYOR.
                 */
                var orders = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Customer)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(x => x.ProductInformation.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.OrderShipments)
                    .Include(o => o.Payments)
                        .ThenInclude(x => x.PaymentType.PaymentTypeTranslations)
                    .Include(x => x.Marketplace)
                    .Where(x => orderIds.Contains(x.Id))
                    .ToList();

                var order = orders.FirstOrDefault();

                var winkaConnectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);
                _winkaUnitOfWork.BeginTransaction();

                /*
                 * SIPARISLERE ILISKIN FATURALAR KONTROL EDILIYOR.
                 */
                foreach (var orderId in orderIds)
                {
                    var faturaBaslikInvoiceNumber = _winkaUnitOfWork.FaturaBaslikRepository.ReadByOrderId(orderId);
                    var invoiceFound = !string.IsNullOrEmpty(faturaBaslikInvoiceNumber);
                    if (invoiceFound)
                    {
                        response.Success = false;
                        response.Message = $"Helpye ait {orderId} numaralı sipariş winkada {faturaBaslikInvoiceNumber} faturada daha önce kesilmiştir. Lütfen bu sipariş numarasını sistemden çıkartınız veya faturayı siliniz.";

                        _winkaUnitOfWork.RollbackTransaction();

                        return;
                    }
                }

                /*
                 * STOK TRANSFER FISLERI SILINIYOR.
                 */
                foreach (var orderId in orderIds)
                {
                    var faturaBaslikdeleteStockTransfer = this._winkaUnitOfWork.FaturaBaslikRepository.DeleteStockTransfer(orderId);
                    var faturaHarketdeleteStockTransfer = this._winkaUnitOfWork.FaturaHareketRepository.DeleteStockTransfer(orderId);
                }

                var source = "Web";
                if (!string.IsNullOrEmpty(order.MarketplaceId) && order.OrderSourceId == 1) //pazaryeri ise
                {
                    source = order.MarketplaceId;
                }

                /*
                 * CARI OLUSTURULUYOR.
                 */
                #region Cariler
                var customerId = $"{configurations["CurrentSerie"]}{source}{order.Customer.Id}";
                if (order.MarketplaceId == "MN")
                {
                    customerId = "ININFLN7662";
                }
                else if (order.MarketplaceId == "TYE")
                {
                    customerId = "ININFLN7696";
                }

                var cariler = _winkaUnitOfWork.CarilerRepository.Read(customerId.ToString());
                if (cariler == null)
                {
                    string invoiceNameSurname = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}";

                    var nameSurname = invoiceNameSurname.Length >
                    30 ? invoiceNameSurname.Substring(0, 30) : invoiceNameSurname;

                    cariler = new Cariler
                    {
                        Durum = 0,
                        AnaHesapId = 0,
                        Kodu = customerId.ToString(),
                        Adi = nameSurname.ToUpper(new CultureInfo("tr-TR")),
                        Unvani = invoiceNameSurname.ToUpper(new CultureInfo("tr-TR")),
                        Yetkili = "",
                        Tipi = 4,
                        SubeId = 1,
                        ParaBirimi = "TL",
                        Aciklama = "",
                        MahalleId = 0,
                        FaturaAdresi = order.OrderInvoiceInformation.Address,
                        SevkAdresi = order.OrderDeliveryAddress.Address,
                        Telefon1 = order.OrderDeliveryAddress.PhoneNumber,
                        Telefon2 = "",
                        Faks = "",
                        GSM = order.OrderDeliveryAddress.PhoneNumber,
                        EMail = order.OrderInvoiceInformation.Mail,
                        WebAdresi = "",
                        VergiDairesi = "",
                        VergiNumarasi = !String.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) ? order.OrderInvoiceInformation.TaxNumber : "11111111111",
                        KrediLimiti = 0,
                        AcikHesapLimiti = 0,
                        Opsiyon = 0,
                        Iskonto = 0,
                        GecikmeFaizi = 0,
                        AylikVade = 0,
                        SatisYapilmasin = false,
                        OzelKod1 = 0,
                        OzelKod2 = 0,
                        OzelKod3 = 0,
                        Tarih = DateTime.Now,
                        OrtalamaDevirTarihi = null,
                        ArtiPuan = 0,
                        OtomasyonDisi = false,
                        BorcHesapId = 0,
                        AlacakHesapId = 0,
                        SatisFiyati = 0,
                        IsYeri = "",
                        OzelKod0 = 0,
                        IlceId = 1,// bulanacak
                        EFaturaSenaryo = 0,
                        GiderTipi = 0,
                        AdresId = 0,
                        UlkeId = 792, // bulanacak
                    };
                    _winkaUnitOfWork.CarilerRepository.Create(cariler);
                }
                #endregion

                /*
                 * FATURA BASLIK OLUSTURULUYOR.
                 */
                #region FaturaBaslik
                var invoiceStart = configurations["EArchiveSerie"];
                byte gonderimSekli = 3;
                byte senaryo = 3;
                var aciklama = order.Id.ToString();
                short kasaId = 3;
                decimal kur = 1;
                int genelKod1 = 0;

                if (order.MarketplaceId == "MN")
                {
                    invoiceStart = "FAT";
                    gonderimSekli = 1;
                    senaryo = 1;
                }
                else if (order.MarketplaceId == "TYE")
                {
                    var exchangeRate = _unitOfWork.ExchangeRateRepository.DbSet().OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.CurrencyId == order.CurrencyId);

                    invoiceStart = "FAT";
                    gonderimSekli = 1;
                    senaryo = 2;
                    kasaId = 1;
                    kur = exchangeRate.Rate;
                    genelKod1 = 84;


                    if (order.OrderShipments.Count > 0)
                    {
                        aciklama = $"TAKİP NO: {order.OrderShipments[0].TrackingCode} -  BUTİK ID: {order.OrderShipments[0].PackageNumber} - TOPLAM ÜRÜN ADETİ: {orderIds.Count} - KUR: {kur.ToString().Replace(".", ",")}";
                    }
                }

                var belgeNo = _winkaUnitOfWork.FaturaBaslikRepository.MaxInvoiceBelgeNo(invoiceStart);

                string invoiceNumber = $"{invoiceStart}{DateTime.Now.Year}{belgeNo.PadLeft(9, '0')}";

                var faturaBaslik = new FaturaBaslik
                {
                    Durum = 0,
                    KasaId = kasaId,
                    SubeId = 3,
                    BelgeTipi = 23,
                    KDVDahil = false,
                    Tarih = DateTime.Now,
                    CariId = cariler.Id,
                    BelgeNo = invoiceNumber,//Bulanacak
                    Kur = kur,
                    Aciklama = aciklama,
                    KullaniciId = 1,
                    Iskonto1 = 0,
                    Iskonto2 = 0,
                    OzelKod1 = 0,
                    OzelKod2 = 0,
                    OlusturmaTarihi = DateTime.Now,
                    Kapali = 0,
                    OzelKod3 = 0,
                    OzelKod4 = 0,
                    GenelKod1 = genelKod1,
                    GenelKod2 = 0,
                    CariAdresId = 0,
                    GonderimSekli = gonderimSekli
                };
                _winkaUnitOfWork.FaturaBaslikRepository.Create(faturaBaslik);
                #endregion

                #region FaturaDetay
                if (faturaBaslik.Id > 0)
                {
                    var payment = order.Payments.FirstOrDefault();
                    var paymentType = new IdValue();
                    if (payment != null)
                    {
                        paymentType = _paymentType.ContainsKey(payment.PaymentTypeId) ? _paymentType.Single(pt => pt.Key == payment.PaymentTypeId).Value
                            : new IdValue { Id = 0, Name = "Yok" };
                    }
                    if (order.OrderSourceId == 1)
                    {
                        paymentType = new IdValue { Id = 1, Name = "Ödeme Aracısı" };

                    }


                    var shipment = order.OrderShipments.FirstOrDefault();



                    _winkaUnitOfWork.FaturaDetayRepository.Create(new FaturaDetay
                    {
                        BaslikId = faturaBaslik.Id,
                        OdemeTipi = paymentType.Id,//bulunacak
                        OdemeTarihi = DateTime.Now,
                        OdemeAciklamasi = payment != null ? payment.PaymentType.PaymentTypeTranslations.FirstOrDefault().Name : order.Marketplace.Name,
                        TasiyiciId = _cargoCurrent.ContainsKey(shipment.ShipmentCompanyId) ? _cargoCurrent.Single(cc => cc.Key == shipment.ShipmentCompanyId).Value : 0, //HANGI KARGO FIRMASI ILE TASINDI
                        SevkTarihi = DateTime.Now,
                        WebAdresi = _webAddress.ContainsKey(source) ? _webAddress.Single(wa => wa.Key == source).Value : 0 //SIPARIS HANGI WEB SITESINDEN VERILDI
                    });


                    var orderDetails = orders.SelectMany(od => od.OrderDetails).ToList();

                    var paraBirimi = order.CurrencyId;

                    if (paraBirimi == "EUR")
                        paraBirimi = "€";


                    foreach (var orderDetail in orderDetails)
                    {
                        var stoklar = _winkaUnitOfWork.StoklarRepository.ReadByStockCode(orderDetail.ProductInformation.StockCode);
                        var barkodlar = _winkaUnitOfWork.BarkodlarRepository.ReadByUnitId(stoklar.AnaBirim);
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = stoklar.AnaBirim,
                            BarkodId = barkodlar.Id,
                            Miktar = Convert.ToDecimal(orderDetail.Quantity),
                            StokAdi = stoklar.StokAdi,
                            Fiyat = orderDetail.VatExcUnitPrice,
                            ParaBirimi = paraBirimi,
                            Kur = kur,
                            KDV = Convert.ToDecimal(orderDetail.VatRate * 100),
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = 0,
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                        });
                    }

                    if (order.SurchargeFee > 0)
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = 16533,
                            BarkodId = 0,
                            Miktar = 1,
                            StokAdi = "Kapıda ödeme hizmet bedeli",
                            Fiyat = order.SurchargeFee / 1.20m,
                            ParaBirimi = order.CurrencyId,
                            Kur = 1,
                            KDV = 20,
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = 0,
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                        });

                    if (order.CargoFee > 0)
                        _winkaUnitOfWork.FaturaHareketRepository.Create(new FaturaHareket
                        {
                            Durum = 0,
                            BaslikId = faturaBaslik.Id,
                            BirimId = 5750,
                            BarkodId = 0,
                            Miktar = 1,
                            StokAdi = "Kargo Bedeli",
                            Fiyat = order.CargoFee / 1.20m,
                            ParaBirimi = order.CurrencyId,
                            Kur = 1,
                            KDV = 20,
                            OTV = 0,
                            Iskonto1 = 0,
                            Iskonto2 = 0,
                            Iskonto3 = 0,
                            Iskonto4 = 0,
                            Termin = null,
                            Opsiyon = 0,
                            Personel = 0,
                            TransferMiktari = 0,
                            SevkDeposu = 0,
                            Prim = 0,
                            SatisSekli = 0,
                            ProtokolId = 0,
                            Not1 = "",
                            Not2 = "",
                            Not3 = "",
                            Not4 = "",
                            Kod1 = 0,
                            Kod2 = 0,
                            Kod3 = 0,
                            Kod4 = 0,
                        });
                }

                var odemeBaslik = new OdemeBaslik
                {
                    Durum = 0,
                    KasaId = 1,
                    KullaniciId = 1,
                    BelgeTipi = 101,
                    BelgeNo = belgeNo,
                    Tarih = DateTime.Now,
                    CariId = cariler.Id,
                    Kur = kur,
                    OzelKod1 = 0,
                    OzelKod2 = 2,
                    OlusturmaTarihi = DateTime.Now,
                    GenelKod1 = 0,
                    GenelKod2 = 0,
                    Aciklama = string.Empty
                };
                _winkaUnitOfWork.OdemeBaslikRepository.Create(odemeBaslik);

                if (odemeBaslik.Id > 0)
                {
                    _winkaUnitOfWork.EFaturaDurumRepository.Create(new EFaturaDurum
                    {
                        Durum = 0,
                        BaslikId = faturaBaslik.Id,
                        FaturaDurumu = 0,
                        GIB_Durum = 0,
                        Senaryo = senaryo,
                        Tipi = 0,
                        Portal = null,
                        Gonderim = null,
                        Alinma = null,
                        Yanit = null,
                        Aciklama = string.Empty,
                        GIBAciklama = string.Empty,
                        FaturaDurumAciklama = string.Empty
                    });
                }
                #endregion

                _winkaUnitOfWork.CommitTransaction();

                response.Success = true;
                response.Data = new AccountingInfo
                {
                    Guid = invoiceNumber,
                    AccountingCompanyId = "W",
                    Url = null
                };
            }, (response, e) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        public DataResponse<AccountingCancel> CancelInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCancel>>((response) =>
            {
                response.Data = new();

                #region Read Order
                var order = this
                            ._unitOfWork
                            .OrderRepository
                            .DbSet()
                            .AsNoTracking()
                            .Include(o => o.Customer)
                            .Include(o => o.OrderDetails)
                                .ThenInclude(x => x.ProductInformation.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                            .Include(o => o.OrderReturnDetails)
                                .ThenInclude(ord => ord.ProductInformation)
                            .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                            .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                            .Include(o => o.OrderShipments)
                            .ThenInclude(o => o.ShipmentCompany)
                            .Include(o => o.Payments)
                            .Include(o => o.OrderBilling)
                            .Include(x => x.Company)
                            .FirstOrDefault(x => x.Id == orderId);

                var orderNotFound = order == null;
                if (orderNotFound)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }

                var invoiceNotFound = order.OrderBilling == null || string.IsNullOrEmpty(order.OrderBilling.InvoiceNumber);
                if (invoiceNotFound)
                {
                    response.Message = "Helpy sisteminde siparişe ilişkin fatura bulunamadı.";
                    response.Success = false;
                    return;
                }

                var orderReturnDetailNotFound = order.OrderReturnDetails?.Count == 0;
                if (orderReturnDetailNotFound)
                {
                    response.Message = "İade ürünü bulunamadı.";
                    response.Success = false;
                    return;
                }
                #endregion

                var winkaConnectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);
                _winkaUnitOfWork.BeginTransaction();

                var faturaBaslikRepository = _winkaUnitOfWork.FaturaBaslikRepository;
                var faturaDetayRepository = _winkaUnitOfWork.FaturaDetayRepository;
                var faturaHareketRepository = _winkaUnitOfWork.FaturaHareketRepository;

                var faturaBaslik = faturaBaslikRepository.ReadByBelgeNo(order.OrderBilling.InvoiceNumber);
                var faturaHarekets = faturaHareketRepository.ReadByBaslikId(faturaBaslik.Id);
                var faturaDetay = faturaDetayRepository.ReadByBaslikId(faturaBaslik.Id);
                var detay = faturaDetay.FirstOrDefault();
                var belgeNo = faturaBaslikRepository.MaxCancelBelgeNo() ?? "1";
                var odemeTipi = _paymentTypeReturn.Values.FirstOrDefault(x => x.Id == detay.OdemeTipi)?.Name;
                var webAdresi = _webAddress.ContainsValue(detay.WebAdresi)
                        ? _webAddress.FirstOrDefault(x => x.Value == detay.WebAdresi).Key
                        : "";
                var kargoKodu = _cargoCurrent.ContainsValue(detay.TasiyiciId)
                        ? _cargoCurrent.FirstOrDefault(x => x.Value == detay.TasiyiciId).Key
                        : "";

                if (order.Company.Name == "Saade")
                {
                    odemeTipi = "Ödeme Aracısı";
                    kargoKodu = order.OrderShipments.FirstOrDefault().ShipmentCompany.Name;
                    webAdresi = "www.trendyol.com";
                }

                #region Check Duplicate
                var documentNumber = faturaBaslikRepository.FindExpenseSlipNumber(order.OrderBilling.InvoiceNumber);
                var documentFound = !string.IsNullOrEmpty(documentNumber);
                if (documentFound)
                {
                    response.Success = true;

                    response.Data.PaymentType = odemeTipi;
                    response.Data.PaymentSource = webAdresi;
                    response.Data.DocumentNo = documentNumber;

                    _winkaUnitOfWork.RollbackTransaction();

                    return;
                }
                #endregion

                FaturaBaslik existingDocument = null;

                var hasReturnNumber = !string.IsNullOrEmpty(order.OrderBilling.ReturnNumber);
                if (hasReturnNumber)
                {
                    existingDocument = faturaBaslikRepository.ReadByBelgeNo(order.OrderBilling.ReturnNumber);
                }

                var hasReturnDocument = existingDocument != null;
                if (hasReturnDocument)
                {
                    response.Success = true;

                    response.Data.PaymentType = odemeTipi;
                    response.Data.PaymentSource = webAdresi;
                    response.Data.DocumentNo = existingDocument.BelgeNo;

                    _winkaUnitOfWork.RollbackTransaction();

                    return;
                }

                if (hasReturnNumber && !hasReturnDocument)
                {
                    response.Success = false;

                    response.Data.DocumentNo = order.OrderBilling.ReturnNumber;
                    response.Message = "Helpy sisteminde iade faturası oluşmutur ancak Winka programında oluşmamıştır. Lütfen kontrol ediniz.";

                    _winkaUnitOfWork.RollbackTransaction();

                    return;
                }

                faturaBaslik.KasaId = 5;
                faturaBaslik.SubeId = 5;

                if (order.Company.Name == "Saade")
                {
                    faturaBaslik.KasaId = 1;
                    faturaBaslik.SubeId = 1;
                }

                faturaBaslik.GonderimSekli = 0;
                faturaBaslik.BelgeTipi = 29;
                faturaBaslik.Aciklama = $"{faturaBaslik.BelgeNo} numaralı faturanın iadesidir. Kargo: {kargoKodu} - Web:{webAdresi} - Ödeme Tipi: {odemeTipi} - Ödeme Açıkla: {detay.OdemeAciklamasi}";

                faturaBaslik.BelgeNo = $"{configurations["ReturnSerie"]}{belgeNo.PadLeft(6, '0')}";
                faturaBaslik.Tarih = DateTime.Now;
                faturaBaslik.OlusturmaTarihi = DateTime.Now;

                if (!string.IsNullOrEmpty(order.OrderBilling.ReturnDescription) && orderReturnDetailNotFound)
                    while (faturaHarekets.Any(fh => fh.BarkodId == 0))
                        faturaHarekets.Remove(faturaHarekets.First(fh => fh.BarkodId == 0));

                order.OrderReturnDetails.ForEach(ordLoop =>
                {
                    var barcodes = _winkaUnitOfWork.BarkodlarRepository.ReadByBarcode(ordLoop.ProductInformation.Barcode);
                    if (barcodes == null)
                    {
                        response.Success = false;
                        response.Message = $"{ordLoop.ProductInformation.Barcode} borkodlu ürün winkada bulunamadı lütfen kontrol ediniz.";
                        return;
                    }
                    var faturaHareket = faturaHarekets.FirstOrDefault(fd => fd.BarkodId == barcodes.Id);

                    faturaHareket.Miktar = ordLoop.Quantity;
                });

                foreach (var ordLoop in order.OrderDetails)
                {
                    var exist = order.OrderReturnDetails.Any(x => x.ProductInformationId == ordLoop.ProductInformationId);
                    if (exist) continue;

                    var barcodes = _winkaUnitOfWork.BarkodlarRepository.ReadByBarcode(ordLoop.ProductInformation.Barcode);
                    if (barcodes == null)
                    {
                        response.Success = false;
                        response.Message = $"{ordLoop.ProductInformation.Barcode} borkodlu ürün Winka programında bulunamadı. Lütfen kontrol ediniz.";

                        _winkaUnitOfWork.RollbackTransaction();

                        return;
                    }
                    var faturaHareket = faturaHarekets.FirstOrDefault(fd => fd.BarkodId == barcodes.Id);
                    faturaHarekets.Remove(faturaHareket);
                }

                _winkaUnitOfWork.FaturaBaslikRepository.Create(faturaBaslik);

                faturaHarekets.ForEach(fh =>
                {
                    fh.BaslikId = faturaBaslik.Id;
                    _winkaUnitOfWork.FaturaHareketRepository.Create(fh);
                });

                _winkaUnitOfWork.CommitTransaction();

                response.Data = new AccountingCancel
                {
                    PaymentType = odemeTipi,
                    PaymentSource = webAdresi,
                    DocumentNo = faturaBaslik.BelgeNo
                };
                response.Success = true;
            }, (response, e) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        #region Stock Transfer
        public DataResponse<string> CreateStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                #region Read And Check Order From Helpy
                var order = _unitOfWork
                            .OrderRepository
                            .DbSet()
                            .Include(o => o.OrderDetails)
                                .ThenInclude(x => x.ProductInformation)
                            .FirstOrDefault(x => x.Id == orderId);

                if (order == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }
                #endregion

                /*
                 * Aciklama mukerrer kontrolu icin anahtar gorevi gorur.
                 */
                String aciklama = $"ST{order.Id}";

                var winkaConnectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);
                _winkaUnitOfWork.BeginTransaction();

                var faturaBaslikRepository = this._winkaUnitOfWork.FaturaBaslikRepository;
                var faturaHareketRepository = this._winkaUnitOfWork.FaturaHareketRepository;
                var stoklarRepository = this._winkaUnitOfWork.StoklarRepository;
                var barkodlarRepository = this._winkaUnitOfWork.BarkodlarRepository;

                #region Check Duplicate
                var varolanFaturaBaslik = faturaBaslikRepository.ReadByAciklama(aciklama);
                var mukerrerBulundu = varolanFaturaBaslik is not null;
                if (mukerrerBulundu)
                {
                    if (order.MarketplaceId == "TYE")
                    {
                        var commitRequired = false;

                        var varolanFaturaHareketler = faturaHareketRepository.ReadByBaslikId(varolanFaturaBaslik.Id);

                        foreach (var orderDetail in order.OrderDetails)
                        {
                            var stoklar = stoklarRepository.ReadByStockCode(orderDetail.ProductInformation.StockCode);
                            var barkodlar = barkodlarRepository.ReadByUnitId(stoklar.AnaBirim);

                            var varolanFaturaHareket = varolanFaturaHareketler.FirstOrDefault(_ => _.BarkodId == barkodlar.Id);
                            if (varolanFaturaHareket.TransferMiktari != orderDetail.Quantity)
                            {
                                varolanFaturaHareket.Miktar = orderDetail.Quantity;
                                varolanFaturaHareket.TransferMiktari = orderDetail.Quantity;

                                faturaHareketRepository.Update(varolanFaturaHareket);

                                commitRequired = true;
                            }
                        }

                        if (commitRequired)
                        {
                            _winkaUnitOfWork.CommitTransaction();
                        }
                        else
                        {
                            _winkaUnitOfWork.RollbackTransaction();
                        }
                    }
                    else
                    {
                        _winkaUnitOfWork.RollbackTransaction();
                    }

                    response.Data = varolanFaturaBaslik.BelgeNo;
                    response.Success = true;
                    return;
                }
                #endregion

                String belgeSeri = configurations["StockSerie"];
                String belgeSira = faturaBaslikRepository.MaxStockTransferBelgeSira();

                if (string.IsNullOrEmpty(belgeSira))
                    belgeSira = "1";

                String belgeNo = $"{belgeSeri}{belgeSira}";

                FaturaBaslik faturaBaslik = new()
                {
                    Durum = 0,
                    KasaId = Convert.ToInt16(configurations["InvoiceDealerNumber"]),
                    BelgeTipi = 41,
                    KDVDahil = false,
                    Tarih = DateTime.Now,
                    SubeId = Convert.ToInt16(configurations["InvoiceBranchNumber"]),
                    CariId = 6,
                    BelgeNo = belgeNo,
                    Kur = 1,
                    Aciklama = aciklama,
                    KullaniciId = 1,
                    Iskonto1 = 0,
                    Iskonto2 = 0,
                    OzelKod1 = 0,
                    OzelKod2 = 0,
                    OlusturmaTarihi = DateTime.Now,
                    Kapali = 0,
                    OzelKod3 = 0,
                    OzelKod4 = 0,
                    GenelKod1 = 0,
                    GenelKod2 = 0,
                    CariAdresId = 0,
                    GonderimSekli = 0
                };
                this._winkaUnitOfWork.FaturaBaslikRepository.Create(faturaBaslik);

                foreach (var orderDetail in order.OrderDetails)
                {
                    var stoklar = stoklarRepository.ReadByStockCode(orderDetail.ProductInformation.StockCode);
                    var barkodlar = barkodlarRepository.ReadByUnitId(stoklar.AnaBirim);

                    FaturaHareket faturaHareket = new()
                    {
                        Durum = 0,
                        BaslikId = faturaBaslik.Id,
                        BirimId = stoklar.AnaBirim,
                        BarkodId = barkodlar.Id,
                        Miktar = Convert.ToDecimal(orderDetail.Quantity),
                        StokAdi = stoklar.StokAdi,
                        Fiyat = orderDetail.VatExcUnitPrice,
                        ParaBirimi = order.CurrencyId,
                        Kur = 1,
                        KDV = Convert.ToDecimal(orderDetail.VatRate * 100),
                        OTV = 0,
                        Iskonto1 = 0,
                        Iskonto2 = 0,
                        Iskonto3 = 0,
                        Iskonto4 = 0,
                        Termin = null,
                        Opsiyon = 0,
                        Personel = 0,
                        TransferMiktari = Convert.ToDecimal(orderDetail.Quantity),
                        SevkDeposu = 3,
                        Prim = 0,
                        SatisSekli = 0,
                        ProtokolId = 0,
                        Not1 = $"ST{order.Id}",
                        Not2 = "",
                        Not3 = "",
                        Not4 = "",
                        Kod1 = 0,
                        Kod2 = 0,
                        Kod3 = 0,
                        Kod4 = 0
                    };
                    faturaHareketRepository.Create(faturaHareket);
                }

                _winkaUnitOfWork.CommitTransaction();

                response.Data = belgeNo;
                response.Success = true;
            }, (response, e) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        public DataResponse<List<int>> ReadStockTransfers()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<int>>>((response) =>
            {
                var ccountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();
                var configurations = ccountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);
                _winkaUnitOfWork = new WinkaUnitOfWork(configurations["ConnectionString"]);

                response.Data = this._winkaUnitOfWork.FaturaBaslikRepository.ReadStockTransferOrderIds();
                response.Success = true;
            });
        }

        public Response DeleteStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var winkaConnectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);
                _winkaUnitOfWork.BeginTransaction();

                response.Success = this._winkaUnitOfWork.FaturaBaslikRepository.DeleteStockTransfer(orderId) &&
                                   this._winkaUnitOfWork.FaturaHareketRepository.DeleteStockTransfer(orderId);

                _winkaUnitOfWork.CommitTransaction();
            }, (response, e) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }
        #endregion

        public DataResponse<int> ReadLastStockTransfer()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                var ccountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();
                var configurations = ccountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);
                var connectionString = configurations["ConnectionString"];
                _winkaUnitOfWork = new WinkaUnitOfWork(connectionString);


                response.Data = this._winkaUnitOfWork.FaturaBaslikRepository.ReadLastStockTransferOrderId();
                response.Success = true;
            });
        }

        public DataResponse<int> ReadStock(string stockCode, string warehouseId)
        {
            throw new NotImplementedException();
        }

        public DataResponse<bool> CreateStocktaking(int stocktakingId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                StringBuilder stringBuilder = new();

                #region Check Configurations
                if (!configurations.ContainsKey("InternetWarehouseId") || !byte.TryParse(configurations["InternetWarehouseId"], out byte internetWarehouseId))
                {
                    response.Message = "İnternet depo kodu eksik.";
                    return;
                }

                if (!configurations.ContainsKey("ReservedWarehouseId") || !byte.TryParse(configurations["ReservedWarehouseId"], out byte reservedWarehouseId))
                {
                    response.Message = "Rezerve depo kodu eksik.";
                    return;
                }

                if (!configurations.ContainsKey("ConnectionString"))
                {
                    response.Message = "Bağlantı bilgileri eksik.";
                    return;
                }
                #endregion

                #region Check Data
                if (!_unitOfWork.StocktakingRepository.DbSet().Any(s => s.Id == stocktakingId))
                {
                    response.Message = "Stok sayımı bulunamadı.";

                    return;
                }
                #endregion

                var stocktaking = _unitOfWork
                    .StocktakingRepository
                    .DbSet()
                    .Include(s => s.StocktakingDetails)
                        .ThenInclude(sd => sd.ProductInformation)
                            .ThenInclude(pi => pi.ProductInformationTranslations)
                    .FirstOrDefault(s => s.Id == stocktakingId);

                var winkaDocumentDetails = new List<FaturaHareket>();

                foreach (var theStocktakingDetail in stocktaking.StocktakingDetails)
                {
                    var difference = 0;
                    var collectedQuantity = _unitOfWork
                        .OrderDetailRepository
                        .DbSet()
                        .Where(od =>
                            od.Order.OrderBilling == null &&
                            od.Order.OrderTypeId == OrderTypes.Toplandi &&
                            od.ProductInformationId == theStocktakingDetail.ProductInformationId)
                        .Sum(od => od.Quantity);
                    var packedQuantities = this
                        ._unitOfWork
                        .OrderDetailRepository
                        .DbSet()
                        .Where(od =>
                            od.Order.MarketplaceId == Marketplaces.TrendyolEurope &&
                            od.Order.OrderBilling == null &&
                            od.ProductInformationId == theStocktakingDetail.ProductInformationId &&
                            od.Order.OrderTypeId == OrderTypes.KargoyaTeslimEdildi)
                        .GroupBy(od => od.ProductInformationId)
                        .ToDictionary(od => od.Key, od => od.Sum(od2 => od2.Quantity));
                    var stock = theStocktakingDetail.ProductInformation.Stock;
                    var reservedStock = theStocktakingDetail.ProductInformation.ReservedStock;

                    if (reservedStock < collectedQuantity)
                        reservedStock = collectedQuantity;

                    difference = theStocktakingDetail.Quantity - (stock + (reservedStock - collectedQuantity));

                    var winkaBarkodlar = _winkaUnitOfWork.BarkodlarRepository.FirstOrDefault(theStocktakingDetail.ProductInformation.Barcode);
                    if (winkaBarkodlar == null)
                    {
                        response.Message = "Hatalı ürün.";
                        return;
                    }

                    var winkaDocumentDetail = new FaturaHareket
                    {
                        Durum = 0,
                        BirimId = winkaBarkodlar.BirimId,
                        BarkodId = winkaBarkodlar.Id,
                        StokAdi = theStocktakingDetail.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name,
                        Fiyat = 0,
                        ParaBirimi = "TL",
                        Kur = 1,
                        Miktar = difference,
                        KDV = 0,
                        OTV = 0,
                        Iskonto1 = 0,
                        Iskonto2 = 0,
                        Iskonto3 = 0,
                        Iskonto4 = 0,
                        Termin = null,
                        Opsiyon = 0,
                        Personel = 0,
                        TransferMiktari = difference,
                        SevkDeposu = internetWarehouseId,
                        Prim = 0,
                        SatisSekli = 0,
                        ProtokolId = 0,
                        Not1 = string.Empty,
                        Not2 = string.Empty,
                        Not3 = string.Empty,
                        Not4 = string.Empty,
                        Kod1 = 0,
                        Kod2 = 0,
                        Kod3 = 0,
                        Kod4 = 0,
                        MuafiyetKodu = 0,
                    };

                    winkaDocumentDetails.Add(winkaDocumentDetail);
                }

                string winkaConnectionString = configurations["ConnectionString"];

                _winkaUnitOfWork = new WinkaUnitOfWork(winkaConnectionString);

                winkaDocumentDetails.RemoveAll(wdd => wdd.Miktar == 0);

                _winkaUnitOfWork.BeginTransaction();

                /* Stok Sayim Giris */
                if (winkaDocumentDetails.Any(wdd => wdd.Miktar > 0))
                {
                    var winkaDocument = new FaturaBaslik
                    {
                        Durum = 0,
                        KasaId = 1,
                        BelgeTipi = 34,
                        KDVDahil = false,
                        Tarih = DateTime.Now,
                        SubeId = internetWarehouseId,
                        CariId = 0,
                        Kur = 1,
                        Aciklama = stocktaking.Name.Length > 500 ? stocktaking.Name[..500] : stocktaking.Name,
                        KullaniciId = 0,
                        Iskonto1 = 0,
                        Iskonto2 = 0,
                        OzelKod1 = 0,
                        OzelKod2 = 0,
                        OlusturmaTarihi = DateTime.Now,
                        Kapali = 0,
                        OzelKod3 = 0,
                        OzelKod4 = 0,
                        GenelKod1 = 0,
                        GenelKod2 = 0,
                        CariAdresId = 0,
                        GonderimSekli = 0
                    };

                    var belgeNo = _winkaUnitOfWork.FaturaBaslikRepository.MaxInvoiceBelgeNo("IN");

                    winkaDocument.BelgeNo = belgeNo;

                    _winkaUnitOfWork.FaturaBaslikRepository.Create(winkaDocument);

                    var documentDetails = winkaDocumentDetails.Where(wdd => wdd.Miktar > 0).ToList();

                    foreach (var thedocumentDetail in documentDetails)
                    {
                        thedocumentDetail.BaslikId = winkaDocument.Id;
                        thedocumentDetail.Miktar = Math.Abs(thedocumentDetail.Miktar);
                        thedocumentDetail.TransferMiktari = Math.Abs(thedocumentDetail.TransferMiktari);

                        _winkaUnitOfWork.FaturaHareketRepository.Create(thedocumentDetail);
                    }

                    stringBuilder.AppendLine($"Stok sayım giriş {belgeNo} nolu belge ile kaydedildi.");
                }

                /* Stok Sayim Cikis */
                if (winkaDocumentDetails.Any(wdd => wdd.Miktar < 0))
                {
                    var winkaDocument = new FaturaBaslik
                    {
                        Durum = 0,
                        KasaId = 1,
                        BelgeTipi = 35,
                        KDVDahil = false,
                        Tarih = DateTime.Now,
                        SubeId = internetWarehouseId,
                        CariId = 0,
                        Kur = 1,
                        Aciklama = stocktaking.Name.Length > 500 ? stocktaking.Name[..500] : stocktaking.Name,
                        KullaniciId = 0,
                        Iskonto1 = 0,
                        Iskonto2 = 0,
                        OzelKod1 = 0,
                        OzelKod2 = 0,
                        OlusturmaTarihi = DateTime.Now,
                        Kapali = 0,
                        OzelKod3 = 0,
                        OzelKod4 = 0,
                        GenelKod1 = 0,
                        GenelKod2 = 0,
                        CariAdresId = 0,
                        GonderimSekli = 0
                    };

                    var belgeNo = _winkaUnitOfWork.FaturaBaslikRepository.MaxInvoiceBelgeNo("IN");

                    winkaDocument.BelgeNo = belgeNo;

                    _winkaUnitOfWork.FaturaBaslikRepository.Create(winkaDocument);

                    var documentDetails = winkaDocumentDetails.Where(wdd => wdd.Miktar < 0).ToList();

                    foreach (var thedocumentDetail in documentDetails)
                    {
                        thedocumentDetail.BaslikId = winkaDocument.Id;
                        thedocumentDetail.Miktar = Math.Abs(thedocumentDetail.Miktar);
                        thedocumentDetail.TransferMiktari = Math.Abs(thedocumentDetail.TransferMiktari);

                        _winkaUnitOfWork.FaturaHareketRepository.Create(thedocumentDetail);
                    }

                    stringBuilder.AppendLine($"Stok sayım çıkış {belgeNo} nolu belge ile kaydedildi.");
                }

                _winkaUnitOfWork.CommitTransaction();
            }, (response, exception) =>
            {
                _winkaUnitOfWork?.RollbackTransaction();

                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }
        #endregion
    }
}