using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class FaturaDetayRepository : RepositoryBase<Infrastructure.Accounting.Winka.Entity.FaturaDetay, Int32>, IFaturaDetayRepository
	{
		#region Constructors
        public FaturaDetayRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

		#region Methods
		
		#endregion
	}
}