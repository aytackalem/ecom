using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class StoklarRepository : RepositoryBase<Stoklar, Int32>, IStoklarRepository
	{
		#region Constructor
        public StoklarRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

		#region Method
		
		#endregion
	}
}