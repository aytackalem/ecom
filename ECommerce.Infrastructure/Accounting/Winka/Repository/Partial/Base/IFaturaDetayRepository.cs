﻿using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IFaturaDetayRepository : IRepository<FaturaDetay, Int32>
    {
        #region Methods
        List<FaturaDetay> ReadByBaslikId(Int32 BaslikId);
        #endregion
    }
}
