using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
	public partial interface IFaturaBaslikRepository : IRepository<FaturaBaslik, Int32>
	{
		#region Method
		string ReadRowNumber(string seri,string invoiceStart);

		string ReadByOrderId(int orderId);

        FaturaBaslik ReadByAciklama(string aciklama);

        string FindExpenseSlipNumber(string invoiceNumber);

        string MaxInvoiceBelgeNo(string invoiceStart);

		string MaxStockTransferBelgeSira();

        bool DeleteStockTransfer(int orderId);

        string MaxCancelBelgeNo();

		FaturaBaslik ReadByBelgeNo(string belgeNo);

		List<int> ReadStockTransferOrderIds();

		int ReadLastStockTransferOrderId();
        #endregion
    }
}