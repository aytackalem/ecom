﻿using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IFaturaHareketRepository : IRepository<FaturaHareket, Int32>
    {
        #region Methods
        List<FaturaHareket> ReadByBaslikId(Int32 BaslikId);

        bool DeleteStockTransfer(int orderId);
        #endregion
    }
}
