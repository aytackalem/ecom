using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
	public partial interface IStoklarRepository : IRepository<Stoklar, Int32>
	{
		#region Method
		Stoklar ReadByStockCode(string stockCode);
		#endregion
	}
}