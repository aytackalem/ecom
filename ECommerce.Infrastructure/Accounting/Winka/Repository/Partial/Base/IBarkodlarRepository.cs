using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IBarkodlarRepository : IRepository<Barkodlar, Int32>
    {
        #region Method
        Barkodlar ReadByUnitId(int unitId);

        Barkodlar ReadByBarcode(string barcode);
        #endregion
    }
}