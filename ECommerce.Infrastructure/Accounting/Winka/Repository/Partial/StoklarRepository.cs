using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class StoklarRepository : RepositoryBase<Stoklar, Int32>, IStoklarRepository
    {
        #region Method
        public Stoklar ReadByStockCode(string stockCode)
        {
          
                return _dbConnection.QueryFirstOrDefault<Stoklar>(sql: "Select * From Stoklar WHERE StokKodu = @StockCode", param: new { StockCode = stockCode }, transaction: _dbTransaction);
           
        }
        #endregion
    }
}