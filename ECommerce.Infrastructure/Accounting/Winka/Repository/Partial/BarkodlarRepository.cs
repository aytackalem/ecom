using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class BarkodlarRepository : RepositoryBase<Barkodlar, Int32>, IBarkodlarRepository
    {
        #region Method
        public Barkodlar ReadByUnitId(int unitId)
        {

            return _dbConnection.QueryFirstOrDefault<Barkodlar>(sql: "Select * from [dbo].[Barkodlar] where BirimId = @UnitId", param: new { UnitId = unitId }, transaction: _dbTransaction);


        }

        public Barkodlar ReadByBarcode(string barcode)
        {


            return _dbConnection.QueryFirstOrDefault<Barkodlar>(sql: "Select * from [dbo].[Barkodlar] where Barkodu=@Barcode", param: new { Barcode = barcode }, transaction: _dbTransaction);


        }

        #endregion
    }
}