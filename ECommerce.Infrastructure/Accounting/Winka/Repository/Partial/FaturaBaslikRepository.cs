using Dapper;
using DocumentFormat.OpenXml.Presentation;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class FaturaBaslikRepository : RepositoryBase<FaturaBaslik, Int32>, IFaturaBaslikRepository
    {
        #region Method
        public string ReadRowNumber(string serie, string invoiceStart)
        {

            return _dbConnection.QueryFirst<string>(sql: $@"
Select Top 1 Cast(Replace(BelgeNo, '{invoiceStart}' + Cast(DatePart(Year, GetDate()) As VarChar(4)), '') As Int) + 1 as BelgeNo 
                    From         [dbo].[FaturaBaslik] 
                    Where        Durum = 0 
                    And          DatePart(Year, Tarih) = DatePart(Year, GetDate()) 
                    And          DatePart(Year, OlusturmaTarihi) = DatePart(Year, GetDate()) 
                    And          BelgeNo Like '{invoiceStart}' + CAST(DatePart(Year, GetDate()) as NCHAR(4)) + '%'
                    And          Len(BelgeNo) > 12 And BelgeNo NOT Like '%XML' 
                    Order By     BelgeNo Desc", param: new { Seri = serie }, transaction: _dbTransaction);

        }

        public string ReadByOrderId(int orderId)
        {

            return _dbConnection.Query<string>(sql: $"Select Top 1 BelgeNo From FaturaBaslik Where Aciklama=@Aciklama And Cast(Tarih as date) >= @Date and Durum=0 and KullaniciId=1", param:
                new { Aciklama = orderId.ToString(), Date = DateTime.Now.AddMonths(-5).ToString("yyy-MM-dd") }, transaction: _dbTransaction).FirstOrDefault();

        }

        public FaturaBaslik ReadByAciklama(string aciklama)
        {
            return _dbConnection
                .QueryFirstOrDefault<FaturaBaslik>(
                    sql: $"Select * From FaturaBaslik Where Aciklama=@Aciklama",
                    param: new
                    {
                        Aciklama = aciklama
                    },
                    transaction: _dbTransaction);

        }

        public string FindExpenseSlipNumber(string invoiceNumber)
        {

            return _dbConnection
                .QueryFirstOrDefault<string>(
                    sql: @"
Select Top 1    BelgeNo
From            [dbo].[FaturaBaslik] With(NoLock) 
Where           [Aciklama] Like @InvoiceNumber + '%'",
                    param: new
                    {
                        InvoiceNumber = invoiceNumber
                    },
                    transaction: _dbTransaction);

        }

        public string MaxInvoiceBelgeNo(string invoiceStart)
        {


            return _dbConnection.Query<string>(sql: $@"
                    Select Top 1 Cast(Replace(BelgeNo, '{invoiceStart}' + Cast(DatePart(Year, GetDate()) As VarChar(4)), '') As Int) + 1 as BelgeNo 
                    From [dbo].[FaturaBaslik]
                    Where        Durum = 0
                    And          DatePart(Year, Tarih) = DatePart(Year, GetDate())
                    And          DatePart(Year, OlusturmaTarihi) = DatePart(Year, GetDate())
                    And          BelgeNo Like '{invoiceStart}' + CAST(DatePart(Year, GetDate()) as NCHAR(4)) + '%'
                    And          Len(BelgeNo) > 12 And BelgeNo NOT Like '%XML'
                    Order By     BelgeNo Desc", param:
                new { }, transaction: _dbTransaction).FirstOrDefault();

        }

        public string MaxStockTransferBelgeSira()
        {
            return _dbConnection.Query<string>(sql: @"
Select Top 1    Cast(Replace(Replace(BelgeNo, 'IN', ''), 'N', '') As Int) + 1 
From            FaturaBaslik 
Where           BelgeTipi = 41 
                And BelgeNo Like 'IN%' 
Order By        Cast(Replace(Replace(BelgeNo, 'IN', ''), 'N', '') As Int) Desc", param:
                new { }, transaction: _dbTransaction).FirstOrDefault();

        }

        public bool DeleteStockTransfer(int orderId)
        {
            return _dbConnection.Execute(sql: @"
Delete
From            FaturaBaslik 
Where           BelgeTipi = 41 
                And Aciklama = @Aciklama
", param: new { Aciklama = $"ST{orderId}" }, transaction: _dbTransaction) > 0;
        }

        public string MaxCancelBelgeNo()
        {

            return _dbConnection.Query<string>(sql: "Select Top 1 Cast(Replace(Replace(BelgeNo, 'GP', ''), 'N', '') As Int) + 1 From FaturaBaslik Where BelgeTipi = 29 And BelgeNo Like 'GP%' Order By Cast(Replace(Replace(BelgeNo, 'GP', ''), 'N', '') As Int) Desc", param:
                new { }, transaction: _dbTransaction).FirstOrDefault();

        }

        public FaturaBaslik ReadByBelgeNo(string belgeNo)
        {

            return _dbConnection.QueryFirstOrDefault<FaturaBaslik>(sql: "Select * From FaturaBaslik With(NoLock) Where BelgeNo = @BelgeNo order by 1 desc", transaction: _dbTransaction, param: new { BelgeNo = belgeNo });

        }

        public List<int> ReadStockTransferOrderIds()
        {
            return _dbConnection.Query<int>(sql: @"
Select  Cast(Replace(Replace(Aciklama, 'ST', ''), 'N', '') As Int)
From    FaturaBaslik
Where   BelgeTipi = 41 
        And BelgeNo Like 'IN%'
And Aciklama LIKE 'ST%'", transaction: _dbTransaction).ToList();
        }

        public int ReadLastStockTransferOrderId()
        {
            return _dbConnection.QueryFirstOrDefault<int>(sql: @"
Select      Cast(Replace(Replace(Aciklama, 'ST', ''), 'N', '') As Int)
From        FaturaBaslik
Where       BelgeTipi = 41
            And BelgeNo Like 'IN%'
            And Aciklama LIKE 'ST%'
Order By    Id Desc", transaction: _dbTransaction);
        }
        #endregion
    }
}