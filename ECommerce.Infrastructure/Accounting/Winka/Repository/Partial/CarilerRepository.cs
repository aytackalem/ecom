using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class CarilerRepository : RepositoryBase<Cariler, Int32>, ICarilerRepository
    {
        #region Method
        public Cariler Read(string kodu)
        {
          
                return _dbConnection.QueryFirstOrDefault<Cariler>(sql: "Select * From [Cariler] Where Kodu LIKE ''+@kodu+'%' Order by 1 desc", param: new { Kodu = kodu }, transaction: _dbTransaction);
          
        }
        #endregion
    }
}