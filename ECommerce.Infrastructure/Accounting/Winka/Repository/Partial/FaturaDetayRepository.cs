﻿using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public  partial class FaturaDetayRepository : RepositoryBase<FaturaDetay, Int32>, IFaturaDetayRepository
    {
        #region Methods
        public List<FaturaDetay> ReadByBaslikId(Int32 BaslikId)
        {
          
                return _dbConnection.Query<FaturaDetay>(sql: "Select * From FaturaDetay With(NoLock) Where BaslikId = @BaslikId", transaction: _dbTransaction, param: new { BaslikId = BaslikId }).ToList();
           
        }
        #endregion
    }
}
