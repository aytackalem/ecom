﻿using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public  partial class FaturaHareketRepository : RepositoryBase<FaturaHareket, Int32>, IFaturaHareketRepository
    {
        #region Methods
        public List<FaturaHareket> ReadByBaslikId(Int32 BaslikId)
        {
                return _dbConnection.Query<FaturaHareket>(sql: "Select * From FaturaHareket With(NoLock) Where BaslikId = @BaslikId", transaction: _dbTransaction, param: new { BaslikId = BaslikId }).ToList();
           
        }

        public bool DeleteStockTransfer(int orderId)
        {
            return _dbConnection.Execute(sql: @"
Delete
From            FaturaHareket
Where           Not1 = @Not1
", param: new { Not1 = $"ST{orderId}" }, transaction: _dbTransaction) > 0;
        }
        #endregion
    }
}
