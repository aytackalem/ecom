using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class FaturaBaslikRepository : RepositoryBase<FaturaBaslik, Int32>, IFaturaBaslikRepository
	{
		#region Constructor
        public FaturaBaslikRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

		#region Method
	
		#endregion
	}
}