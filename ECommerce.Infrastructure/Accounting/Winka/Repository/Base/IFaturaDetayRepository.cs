using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
	public partial interface IFaturaDetayRepository : IRepository<FaturaDetay, Int32>
	{

	}
}