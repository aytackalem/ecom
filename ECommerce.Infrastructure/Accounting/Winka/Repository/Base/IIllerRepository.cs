using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IIllerRepository : IRepository<Iller, Int32>
	{
		public Iller Find(string adi);
	}
}