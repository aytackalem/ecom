using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IIlcelerRepository : IRepository<Ilceler, Int32>
    {
        public Ilceler Find(Int32 ilId, string adi);
    }
}