using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IBarkodlarRepository : IRepository<Barkodlar, Int32>
    {
        /// <summary>
        /// Barkodlar tablosundan eslesen ilk kaydi donduren metod.
        /// </summary>
        /// <param name="barcode">Urune ait barkod bilgisi.</param>
        /// <returns></returns>
        public Barkodlar FirstOrDefault(string barcode);
    }
}