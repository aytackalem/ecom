using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IFaturaHareketRepository : IRepository<FaturaHareket, Int32>
	{
		
	}
}