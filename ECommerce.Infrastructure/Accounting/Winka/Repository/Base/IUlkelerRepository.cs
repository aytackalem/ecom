using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public partial interface IUlkelerRepository : IRepository<Ulkeler, Int32>
	{
		public Ulkeler Find(string adi);
	}
}