﻿using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public interface IEFaturaMukellefRepository : IRepository<EFaturaMukellef, Int32>
    {
        #region Methods
        EFaturaMukellef ReadByTaxNumber(string taxNumber);

        bool Exists(string vergiNo);
        #endregion
    }
}
