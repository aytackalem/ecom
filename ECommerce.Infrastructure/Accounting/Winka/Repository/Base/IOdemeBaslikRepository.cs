using ECommerce.Infrastructure.Accounting.Winka.Entity;
using System;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository.Base
{
    public interface IOdemeBaslikRepository : IRepository<OdemeBaslik, Int32>
	{

	}
}