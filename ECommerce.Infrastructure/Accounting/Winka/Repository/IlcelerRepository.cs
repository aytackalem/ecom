﻿using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class IlcelerRepository : RepositoryBase<Ilceler, Int32>, IIlcelerRepository
    {
        #region Constructor
        public IlcelerRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Method
        public Ilceler Find(int ilId, string adi)
        {
            return _dbConnection.QueryFirstOrDefault<Ilceler>(sql: "Select Top 1 * From vtmaster.dbo.Ilceler Where IlId = @IlId And (Replace(Replace(Replace(Upper(Adi), 'İ', 'I'), 'Ğ', 'G'),'Ş','S') = Replace(Replace(Replace(Upper(@Adi), 'İ', 'I'), 'Ğ', 'G'),'Ş','S') OR Adi IS NULL) Order By Adi Desc", param: new { IlId = ilId, Adi = adi }, transaction: _dbTransaction);
        }
        #endregion
    }
}