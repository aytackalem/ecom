﻿using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class UlkelerRepository : RepositoryBase<Ulkeler, Int16>, IUlkelerRepository
    {
        #region Constructor
        public UlkelerRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Method
        public Ulkeler Find(string adi)
        {
            return _dbConnection.QueryFirstOrDefault<Ulkeler>(sql: "Select * From vtmaster.dbo.Ulkeler Where Replace(Replace(Upper(Adi), 'İ', 'I'), 'Ğ', 'G') = Replace(Replace(Upper(@Adi), 'İ', 'I'), 'Ğ', 'G')", param: new { Adi = adi }, transaction: _dbTransaction);
        }
        #endregion
    }
}