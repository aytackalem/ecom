using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class FaturaHareketRepository : RepositoryBase<Infrastructure.Accounting.Winka.Entity.FaturaHareket, Int32>, IFaturaHareketRepository
	{
		#region Constructors
        public FaturaHareketRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

		#region Methods
		
		#endregion
	}
}