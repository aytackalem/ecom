using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class OdemeBaslikRepository : RepositoryBase<Infrastructure.Accounting.Winka.Entity.OdemeBaslik, Int32>, IOdemeBaslikRepository
	{
		#region Constructors
		public OdemeBaslikRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Methods
	
		#endregion
	}
}