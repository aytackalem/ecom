using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class CarilerRepository : RepositoryBase<Infrastructure.Accounting.Winka.Entity.Cariler, Int32>, ICarilerRepository
	{
		#region Constructor
        public CarilerRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

		#region Method
	

	
		#endregion
	}
}