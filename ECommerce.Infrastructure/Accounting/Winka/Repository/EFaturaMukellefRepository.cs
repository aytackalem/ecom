using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class EFaturaMukellefRepository : RepositoryBase<EFaturaMukellef, Int32>, IEFaturaMukellefRepository
    {
        #region Constructors
        public EFaturaMukellefRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Methods
        public EFaturaMukellef ReadByTaxNumber(string taxNumber)
        {
            return _dbConnection
                .QueryFirstOrDefault<EFaturaMukellef>(
                sql: "Select * From EFaturaMukellef Where VergiNo = @VergiNo",
                param: new
                {
                    VergiNo = taxNumber
                },
                transaction: _dbTransaction);
        }

        public bool Exists(string vergiNo)
        {
            return _dbConnection.QueryFirst<bool>(sql: "Select Case Count(0) When 0 Then 0 Else 1 End From EFaturaMukellef Where VergiNo = @VergiNo", param: new { VergiNo = vergiNo }, transaction: _dbTransaction);
        }
        #endregion
    }
}