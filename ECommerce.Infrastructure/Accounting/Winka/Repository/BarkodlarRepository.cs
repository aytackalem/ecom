using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class BarkodlarRepository : RepositoryBase<Barkodlar, Int32>, IBarkodlarRepository
    {
        #region Constructor
        public BarkodlarRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Method
        public Barkodlar FirstOrDefault(string barcode)
        {
            return _dbConnection.QueryFirstOrDefault<Barkodlar>(sql: "Select * From Barkodlar With(NoLock) Where Barkodu = @Barkodu", param: new { Barkodu = barcode }, transaction: _dbTransaction);
        }
        #endregion
    }
}