﻿using Dapper;
using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class IllerRepository : RepositoryBase<Iller, Int32>, IIllerRepository
    {
        #region Constructor
        public IllerRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Method
        public Iller Find(string adi)
        {
            return _dbConnection.QueryFirstOrDefault<Iller>(sql: "Select * From vtmaster.dbo.Iller Where Replace(Replace(Replace(Upper(Adi), 'İ', 'I'), 'Ğ', 'G'),'Ş','S') = Replace(Replace(Replace(Upper(@Adi), 'İ', 'I'), 'Ğ', 'G'),'Ş','S')", param: new { Adi = adi }, transaction: _dbTransaction);
        }
        #endregion
    }
}