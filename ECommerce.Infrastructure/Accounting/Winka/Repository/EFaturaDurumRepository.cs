using ECommerce.Infrastructure.Accounting.Winka.Entity;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Winka.Repository
{
    public partial class EFaturaDurumRepository : RepositoryBase<EFaturaDurum, Int32>, IEFaturaDurumRepository
	{
		#region Constructors
		public EFaturaDurumRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Methods
	
		#endregion
	}
}