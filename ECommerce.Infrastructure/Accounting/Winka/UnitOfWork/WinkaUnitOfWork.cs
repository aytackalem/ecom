using ECommerce.Infrastructure.Accounting.Winka.Repository;
using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using ECommerce.Infrastructure.Accounting.Winka.UnitOfWork.Base;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ECommerce.Infrastructure.Accounting.Winka.UnitOfWork
{
    public class WinkaUnitOfWork : IWinkaUnitOfWork
    {
        #region Field
        protected readonly IDbConnection _dbConnection;

        protected IDbTransaction _dbTransaction;

        private IUlkelerRepository _UlkelerRepository;

        private IIllerRepository _IllerRepository;

        private IIlcelerRepository _IlcelerRepository;

        private IEFaturaMukellefRepository _EFaturaMukellefRepository;

        private IFaturaDetayRepository _FaturaDetayRepository;

        private IFaturaBaslikRepository _FaturaBaslikRepository;

        private IFaturaHareketRepository _FaturaHareketRepository;

        private ICarilerRepository _CarilerRepository;

        private IStoklarRepository _StoklarRepository;

        private IBarkodlarRepository _BarkodlarRepository;

        private IOdemeBaslikRepository _OdemeBaslikRepository;

        private IEFaturaDurumRepository _EFaturaDurumRepository;
        #endregion

        #region Constructor
        public WinkaUnitOfWork(string connectionString)
        {
#if !DEBUG

            //_dbConnection = new SqlConnection(@"Server=81.213.109.81;Database=CodeDB_2;UId=Helpy;Pwd=3297723azazX;");
            //_dbConnection = new SqlConnection(@"Server=212.156.173.197;Database=CodeDB_1;UId=winka;Pwd=*mEgA1453*!;");
#else
            //_dbConnection = new SqlConnection(@"Server=192.168.1.2;Database=CodeDB_2;UId=Helpy;Pwd=3297723azazX;");
            _dbConnection = new SqlConnection(@"Server=192.168.1.2;Database=CodeDB_1;UId=winka;Pwd=*mEgA1453*!;");
#endif

            _dbConnection = new SqlConnection(connectionString);


        }
        #endregion

        #region Property
        public IUlkelerRepository UlkelerRepository
        {
            get
            {
                if (_UlkelerRepository == null)
                    _UlkelerRepository = new UlkelerRepository(_dbConnection, _dbTransaction);

                return _UlkelerRepository;
            }
        }

        public IIllerRepository IllerRepository
        {
            get
            {
                if (_IllerRepository == null)
                    _IllerRepository = new IllerRepository(_dbConnection, _dbTransaction);

                return _IllerRepository;
            }
        }

        public IIlcelerRepository IlcelerRepository
        {
            get
            {
                if (_IlcelerRepository == null)
                    _IlcelerRepository = new IlcelerRepository(_dbConnection, _dbTransaction);

                return _IlcelerRepository;
            }
        }

        public IEFaturaMukellefRepository EFaturaMukellefRepository
        {
            get
            {
                if (_EFaturaMukellefRepository == null)
                    _EFaturaMukellefRepository = new EFaturaMukellefRepository(_dbConnection, _dbTransaction);

                return _EFaturaMukellefRepository;
            }
        }

        public IEFaturaDurumRepository EFaturaDurumRepository
        {
            get
            {
                if (_EFaturaDurumRepository == null)
                    _EFaturaDurumRepository = new EFaturaDurumRepository(_dbConnection, _dbTransaction);

                return _EFaturaDurumRepository;
            }
        }

        public IOdemeBaslikRepository OdemeBaslikRepository
        {
            get
            {
                if (_OdemeBaslikRepository == null)
                    _OdemeBaslikRepository = new OdemeBaslikRepository(_dbConnection, _dbTransaction);

                return _OdemeBaslikRepository;
            }
        }

        public IFaturaDetayRepository FaturaDetayRepository
        {
            get
            {
                if (_FaturaDetayRepository == null)
                    _FaturaDetayRepository = new FaturaDetayRepository(_dbConnection, _dbTransaction);

                return _FaturaDetayRepository;
            }
        }

        public IFaturaBaslikRepository FaturaBaslikRepository
        {
            get
            {
                if (_FaturaBaslikRepository == null)
                    _FaturaBaslikRepository = new FaturaBaslikRepository(_dbConnection, _dbTransaction);

                return _FaturaBaslikRepository;
            }
        }

        public IFaturaHareketRepository FaturaHareketRepository
        {
            get
            {
                if (_FaturaHareketRepository == null)
                    _FaturaHareketRepository = new FaturaHareketRepository(_dbConnection, _dbTransaction);

                return _FaturaHareketRepository;
            }
        }

        public ICarilerRepository CarilerRepository
        {
            get
            {
                if (_CarilerRepository == null)
                    _CarilerRepository = new CarilerRepository(_dbConnection, _dbTransaction);

                return _CarilerRepository;
            }
        }

        public IStoklarRepository StoklarRepository
        {
            get
            {
                if (_StoklarRepository == null)
                    _StoklarRepository = new StoklarRepository(_dbConnection, _dbTransaction);

                return _StoklarRepository;
            }
        }

        public IBarkodlarRepository BarkodlarRepository
        {
            get
            {
                if (_BarkodlarRepository == null)
                    _BarkodlarRepository = new BarkodlarRepository(_dbConnection, _dbTransaction);

                return _BarkodlarRepository;
            }
        }
        #endregion

        #region Method
        public void BeginTransaction()
        {
            if (_dbConnection.State != ConnectionState.Open)
                _dbConnection.Open();

            _dbTransaction = _dbConnection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (_dbTransaction == null)
                return;

            _dbTransaction.Commit();
        }

        public void RollbackTransaction()
        {
            if (_dbTransaction == null)
                return;

            _dbTransaction.Rollback();
        }
        #endregion

        #region IDisposable
        bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                if (_FaturaDetayRepository != null)
                    _FaturaDetayRepository = null;

                if (_FaturaBaslikRepository != null)
                    _FaturaBaslikRepository = null;

                if (_FaturaHareketRepository != null)
                    _FaturaHareketRepository = null;

                if (_CarilerRepository != null)
                    _CarilerRepository = null;

                if (_OdemeBaslikRepository != null)
                    _OdemeBaslikRepository = null;

                if (_BarkodlarRepository != null)
                    _BarkodlarRepository = null;

                if (_StoklarRepository!= null)
                    _StoklarRepository = null;
            }

            disposed = true;
        }
        #endregion

        #region Destructor
        ~WinkaUnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}