using ECommerce.Infrastructure.Accounting.Winka.Repository.Base;
using System;


namespace ECommerce.Infrastructure.Accounting.Winka.UnitOfWork.Base
{
    public interface IWinkaUnitOfWork : IDisposable
    {
        #region Property
        IUlkelerRepository UlkelerRepository { get; }

        IIllerRepository IllerRepository { get; }

        IIlcelerRepository IlcelerRepository { get; }

        IEFaturaMukellefRepository EFaturaMukellefRepository { get; }

        IFaturaBaslikRepository FaturaBaslikRepository { get; }

        IFaturaDetayRepository FaturaDetayRepository { get; }

        IFaturaHareketRepository FaturaHareketRepository { get; }

        ICarilerRepository CarilerRepository { get; }

        IStoklarRepository StoklarRepository { get; }

        IBarkodlarRepository BarkodlarRepository { get; }

        IOdemeBaslikRepository OdemeBaslikRepository { get; }

        IEFaturaDurumRepository EFaturaDurumRepository { get; }
        #endregion


        #region Method
        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();
        #endregion
    }
}