﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Winka.DataTransferObjects
{
    public class IdValue
    {
        #region Properties
        public byte Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
