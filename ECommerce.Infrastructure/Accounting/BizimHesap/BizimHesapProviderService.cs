﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.BizimHesap
{
    public class BizimHesapProviderService : IAccountingProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public BizimHesapProviderService(IUnitOfWork unitOfWork, IHttpHelper httpHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            #endregion            
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                var url = "";
                var firmId = "";

                var accountingCompanyConfigurations = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == "BH")
                    .ToList();

                var order = _unitOfWork
                     .OrderRepository
                     .DbSet()
                     .Include(o => o.Customer)
                     .Include(o => o.OrderDetails)
                     .ThenInclude(x => x.ProductInformation.ProductInformationTranslations.Where(x => x.LanguageId == "TR"))
                     .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                     .Include(o => o.OrderDetails)
                     .ThenInclude(x => x.ProductInformation.ProductInformationMarketplaces)
                     //.ThenInclude(x => x.MarketplaceCompany)
                     .FirstOrDefault(x => x.Id == orderId);


                url = accountingCompanyConfigurations.Find(x => x.Key == "Url")?.Value;
                firmId = accountingCompanyConfigurations.Find(x => x.Key == "FirmId")?.Value;

                if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(firmId))
                {
                    response.Success = false;
                    response.Message = "Muhasebe firması ayarları okunamadı.";

                    return;
                }

                var gross = 0M;
                var discount = 0M;
                var net = 0M;
                var tax = 0M;
                var total = 0M;


                var taxExcShipmentFee = 0M;
                var taxAmountShipmentFee = 0M;
                if (order.CargoFee > 0)
                {
                    taxExcShipmentFee = order.CargoFee / 1.20M;
                    taxAmountShipmentFee = order.CargoFee - taxExcShipmentFee;
                }

                var invoice = new Invoice
                {
                    firmId = firmId,
                    invoiceType = 3,
                    dates = new Dates
                    {
                        invoiceDate = DateTime.Now,
                        dueDate = DateTime.Now,
                        deliveryDate = DateTime.Now
                    },
                    customer = new Customer
                    {
                        customerId = order.Customer.Id.ToString(),
                        title = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}",
                        taxOffice = order.OrderInvoiceInformation.TaxOffice,
                        taxNo = order.OrderInvoiceInformation.TaxNumber,
                        email = order.OrderInvoiceInformation.Mail,
                        address = order.OrderInvoiceInformation.Address,
                        phone = order.OrderInvoiceInformation.Phone
                    },
                    details = new System.Collections.Generic.List<Detail>()
                };

                order.OrderDetails.ForEach(od =>
                {
                    var listUnitPrice = od.ListUnitPrice - od.ListUnitPrice * od.VatRate;
                    var unitPrice = od.UnitPrice - od.UnitPrice * od.VatRate;

                    Detail detail = null;

                    if (!string.IsNullOrEmpty(order.MarketplaceId))
                        detail = new Detail
                        {
                            //productId = od.Product.ProductInformation.Id,
                            productName = od.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name,
                            note = "",
                            barcode = od
                                .ProductInformation
                                .ProductInformationMarketplaces
                                .FirstOrDefault(pim => pim.MarketplaceId == order.MarketplaceId && pim.CompanyId == order.CompanyId)
                                .StockCode,
                            taxRate = (od.VatRate * 100).ToString(CultureInfo.InvariantCulture),
                            quantity = od.Quantity,
                            unitPrice = listUnitPrice.ToString(CultureInfo.InvariantCulture),
                            grossPrice = (listUnitPrice * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            discount = ((listUnitPrice - unitPrice) * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            net = (unitPrice * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            tax = ((od.UnitPrice * od.Quantity) * od.VatRate).ToString(CultureInfo.InvariantCulture)
                        };
                    else
                        detail = new Detail
                        {
                            //productId = od.Product.ProductInformation.Id,
                            productName = od.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name,
                            note = "",
                            barcode = od.ProductInformation.ProductInformationTranslations.FirstOrDefault().ProductInformation.Barcode,
                            taxRate = (od.VatRate * 100).ToString(CultureInfo.InvariantCulture),
                            quantity = od.Quantity,
                            unitPrice = listUnitPrice.ToString(CultureInfo.InvariantCulture),
                            grossPrice = (listUnitPrice * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            discount = ((listUnitPrice - unitPrice) * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            net = (unitPrice * od.Quantity).ToString(CultureInfo.InvariantCulture),
                            tax = ((od.UnitPrice * od.Quantity) * od.VatRate).ToString(CultureInfo.InvariantCulture)
                        };
                    gross += listUnitPrice * od.Quantity;
                    discount += (listUnitPrice - unitPrice) * od.Quantity;
                    net += unitPrice * od.Quantity;
                    tax += (od.UnitPrice * od.Quantity) * od.VatRate;
                    total += (unitPrice * od.Quantity) + ((od.UnitPrice * od.Quantity) * od.VatRate);

                    detail.total = (net + tax).ToString(CultureInfo.InvariantCulture);

                    invoice.details.Add(detail);
                });

                invoice.amounts = new Amounts
                {
                    currency = "TL",
                    gross = (gross + taxExcShipmentFee).ToString(CultureInfo.InvariantCulture),
                    discount = discount.ToString(CultureInfo.InvariantCulture),
                    net = (net + taxExcShipmentFee).ToString(CultureInfo.InvariantCulture),
                    tax = (tax + taxAmountShipmentFee).ToString(CultureInfo.InvariantCulture),
                    total = (total + order.CargoFee).ToString(CultureInfo.InvariantCulture)
                };

                if (order.CargoFee > 0M)
                {
                    invoice.details.Add(new Detail
                    {
                        productName = "Kargo Ücreti",
                        note = "",
                        barcode = null,
                        taxRate = "20",
                        grossPrice = taxExcShipmentFee.ToString(CultureInfo.InvariantCulture),
                        unitPrice = taxExcShipmentFee.ToString(CultureInfo.InvariantCulture),
                        quantity = 1,
                        discount = 0M.ToString(CultureInfo.InvariantCulture),
                        net = taxExcShipmentFee.ToString(CultureInfo.InvariantCulture),
                        tax = taxAmountShipmentFee.ToString(CultureInfo.InvariantCulture),
                        total = order.CargoFee.ToString(CultureInfo.InvariantCulture)
                    });
                }

                var invoiceInfo = this._httpHelper.Post<Invoice, InvoiceInfo>(invoice, url, null, null, null, null);
                if (string.IsNullOrEmpty(invoiceInfo.Error))
                {
                    response.Data = new AccountingInfo
                    {
                        AccountingCompanyId = "BH",
                        Guid = invoiceInfo.Guid,
                        Url = invoiceInfo.Url
                    };
                    response.Success = true;
                    response.Message = "Fatura başarılı bir şekilde gönderildi.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "Fatura gönderilirken bir hata oluştu." + " " + Newtonsoft.Json.JsonConvert.SerializeObject(invoice);
                }
            });
        }

        public DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds, Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }

        public DataResponse<AccountingCancel> CancelInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCancel>>((response) =>
            {
                response.Success = true;
            });

        }

        public DataResponse<string> CreateStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;
            });
        }

        public DataResponse<List<int>> ReadStockTransfers()
        {
            throw new NotImplementedException();
        }

        public Response DeleteStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Methods
        public DataResponse<int> ReadLastStockTransfer()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                response.Success = true;
            });
        }

        public DataResponse<int> ReadStock(string stockCode, string warehouseId)
        {
            throw new NotImplementedException();
        }

        public DataResponse<bool> CreateStocktaking(int stocktakingId, Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
