﻿namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class Amounts
    {
        #region Properties
        public string currency { get; set; }

        /// <summary>
        /// Herhangi bir indirim olmadan siparişin KDV hariç toplam tutarı
        /// </summary>
        public string gross { get; set; }

        /// <summary>
        /// İndirim tutarı
        /// </summary>
        public string discount { get; set; }

        /// <summary>
        /// İndirim sonrası tutar
        /// </summary>
        public string net { get; set; }

        /// <summary>
        /// KDV tutarı
        /// </summary>
        public string tax { get; set; }

        /// <summary>
        /// KDV dahil ödenecek tutar
        /// </summary>
        public string total { get; set; } 
        #endregion
    }
}
