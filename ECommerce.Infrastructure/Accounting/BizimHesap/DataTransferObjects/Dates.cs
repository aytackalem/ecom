﻿using System;

namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class Dates
    {
        #region Properties
        public DateTime invoiceDate { get; set; }
        
        public DateTime dueDate { get; set; }
        
        public DateTime deliveryDate { get; set; } 
        #endregion
    }
}
