﻿namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class Customer
    {
        #region Properties
        public string customerId { get; set; }
        
        public string title { get; set; }
        
        public string taxOffice { get; set; }
        
        public string taxNo { get; set; }
        
        public string email { get; set; }
        
        public string phone { get; set; }
        
        public string address { get; set; } 
        #endregion
    }
}
