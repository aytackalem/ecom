﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class Invoice
    {
        #region Properties
        public string firmId { get; set; }
        
        public string invoiceNo { get; set; }
        
        public int invoiceType { get; set; }
        
        public string note { get; set; }
        
        public Dates dates { get; set; }
        
        public Customer customer { get; set; }
        
        public Amounts amounts { get; set; }
        
        public List<Detail> details { get; set; } 
        #endregion
    }
}
