﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class Detail
    {
        #region Properties
        public int? productId { get; set; }
        
        public string productName { get; set; }
        
        public string note { get; set; }
        
        public string barcode { get; set; }
        
        public string taxRate { get; set; }
        
        public int quantity { get; set; }
        
        public string unitPrice { get; set; }
        
        /// <summary>
        /// Herhangi bir indirim olmadan siparişin KDV hariç adet ile çarpılmış toplam tutarı
        /// </summary>
        public string grossPrice { get; set; }
        
        /// <summary>
        /// İndirim tutarı
        /// </summary>
        public string discount { get; set; }
        
        /// <summary>
        /// İndirim sonrası tutar
        /// </summary>
        public string net { get; set; }
        
        /// <summary>
        /// KDV tutarı
        /// </summary>
        public string tax { get; set; }
        
        /// <summary>
        /// KDV dahil ödenecek tutar
        /// </summary>
        public string total { get; set; }
        #endregion
    }
}
