﻿namespace ECommerce.Infrastructure.Accounting.BizimHesap.DataTransferObjects
{
    public class InvoiceInfo
    {
        #region Properties
        public string Error { get; set; }

        public string Guid { get; set; }

        public string Url { get; set; }
        #endregion
    }
}
