using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Mikro.Entities
{
    [Table("CARI_HESAP_HAREKETLERI")]
	public partial class CARI_HESAP_HAREKETLERI
	{
		#region Property
		public Guid cha_Guid { get; set; }

		public Int16 cha_DBCno { get; set; }

		public Int32 cha_SpecRecNo { get; set; }

		public Boolean cha_iptal { get; set; }

		public Int16 cha_fileid { get; set; }

		public Boolean cha_hidden { get; set; }

		public Boolean cha_kilitli { get; set; }

		public Boolean cha_degisti { get; set; }

		public Int32 cha_CheckSum { get; set; }

		public Int16 cha_create_user { get; set; }

		public DateTime cha_create_date { get; set; }

		public Int16 cha_lastup_user { get; set; }

		public DateTime cha_lastup_date { get; set; }

		public String cha_special1 { get; set; }

		public String cha_special2 { get; set; }

		public String cha_special3 { get; set; }

		public Int32 cha_firmano { get; set; }

		public Int32 cha_subeno { get; set; }

		public Byte cha_evrak_tip { get; set; }

		public String cha_evrakno_seri { get; set; }

		public Int32 cha_evrakno_sira { get; set; }

		public Int32 cha_satir_no { get; set; }

		public DateTime cha_tarihi { get; set; }

		public Byte cha_tip { get; set; }

		public Byte cha_cinsi { get; set; }

		public Byte cha_normal_Iade { get; set; }

		public Byte cha_tpoz { get; set; }

		public Byte cha_ticaret_turu { get; set; }

		public String cha_belge_no { get; set; }

		public DateTime cha_belge_tarih { get; set; }

		public String cha_aciklama { get; set; }

		public String cha_satici_kodu { get; set; }

		public String cha_EXIMkodu { get; set; }

		public String cha_projekodu { get; set; }

		public String cha_yat_tes_kodu { get; set; }

		public Byte cha_cari_cins { get; set; }

		public String cha_kod { get; set; }

		public String cha_ciro_cari_kodu { get; set; }

		public Byte cha_d_cins { get; set; }

		public Double cha_d_kur { get; set; }

		public Double cha_altd_kur { get; set; }

		public Byte cha_grupno { get; set; }

		public String cha_srmrkkodu { get; set; }

		public Byte cha_kasa_hizmet { get; set; }

		public String cha_kasa_hizkod { get; set; }

		public Byte cha_karsidcinsi { get; set; }

		public Double cha_karsid_kur { get; set; }

		public Byte cha_karsidgrupno { get; set; }

		public String cha_karsisrmrkkodu { get; set; }

		public Double cha_miktari { get; set; }

		public Double cha_meblag { get; set; }

		public Double cha_aratoplam { get; set; }

		public Int32 cha_vade { get; set; }

		public Double cha_Vade_Farki_Yuz { get; set; }

		public Double cha_ft_iskonto1 { get; set; }

		public Double cha_ft_iskonto2 { get; set; }

		public Double cha_ft_iskonto3 { get; set; }

		public Double cha_ft_iskonto4 { get; set; }

		public Double cha_ft_iskonto5 { get; set; }

		public Double cha_ft_iskonto6 { get; set; }

		public Double cha_ft_masraf1 { get; set; }

		public Double cha_ft_masraf2 { get; set; }

		public Double cha_ft_masraf3 { get; set; }

		public Double cha_ft_masraf4 { get; set; }

		public Byte cha_isk_mas1 { get; set; }

		public Byte cha_isk_mas2 { get; set; }

		public Byte cha_isk_mas3 { get; set; }

		public Byte cha_isk_mas4 { get; set; }

		public Byte cha_isk_mas5 { get; set; }

		public Byte cha_isk_mas6 { get; set; }

		public Byte cha_isk_mas7 { get; set; }

		public Byte cha_isk_mas8 { get; set; }

		public Byte cha_isk_mas9 { get; set; }

		public Byte cha_isk_mas10 { get; set; }

		public Boolean cha_sat_iskmas1 { get; set; }

		public Boolean cha_sat_iskmas2 { get; set; }

		public Boolean cha_sat_iskmas3 { get; set; }

		public Boolean cha_sat_iskmas4 { get; set; }

		public Boolean cha_sat_iskmas5 { get; set; }

		public Boolean cha_sat_iskmas6 { get; set; }

		public Boolean cha_sat_iskmas7 { get; set; }

		public Boolean cha_sat_iskmas8 { get; set; }

		public Boolean cha_sat_iskmas9 { get; set; }

		public Boolean cha_sat_iskmas10 { get; set; }

		public Double cha_yuvarlama { get; set; }

		public Byte cha_StFonPntr { get; set; }

		public Double cha_stopaj { get; set; }

		public Double cha_savsandesfonu { get; set; }

		public Double cha_avansmak_damgapul { get; set; }

		public Byte cha_vergipntr { get; set; }

		public Double cha_vergi1 { get; set; }

		public Double cha_vergi2 { get; set; }

		public Double cha_vergi3 { get; set; }

		public Double cha_vergi4 { get; set; }

		public Double cha_vergi5 { get; set; }

		public Double cha_vergi6 { get; set; }

		public Double cha_vergi7 { get; set; }

		public Double cha_vergi8 { get; set; }

		public Double cha_vergi9 { get; set; }

		public Double cha_vergi10 { get; set; }

		public Boolean cha_vergisiz_fl { get; set; }

		public Double cha_otvtutari { get; set; }

		public Boolean cha_otvvergisiz_fl { get; set; }

		public Byte cha_oiv_pntr { get; set; }

		public Double cha_oivtutari { get; set; }

		public Double cha_oiv_vergi { get; set; }

		public Boolean cha_oivergisiz_fl { get; set; }

		public DateTime cha_fis_tarih { get; set; }

		public Int32 cha_fis_sirano { get; set; }

		public String cha_trefno { get; set; }

		public Byte cha_sntck_poz { get; set; }

		public DateTime cha_reftarihi { get; set; }

		public Byte cha_istisnakodu { get; set; }

		public Byte cha_pos_hareketi { get; set; }

		public Byte cha_meblag_ana_doviz_icin_gecersiz_fl { get; set; }

		public Byte cha_meblag_alt_doviz_icin_gecersiz_fl { get; set; }

		public Byte cha_meblag_orj_doviz_icin_gecersiz_fl { get; set; }

		public Guid cha_sip_uid { get; set; }

		public Guid cha_kirahar_uid { get; set; }

		public DateTime cha_vardiya_tarihi { get; set; }

		public Byte cha_vardiya_no { get; set; }

		public Byte cha_vardiya_evrak_ti { get; set; }

		public Byte cha_ebelge_turu { get; set; }

		public Double cha_tevkifat_toplam { get; set; }

		public Double cha_ilave_edilecek_kdv1 { get; set; }

		public Double cha_ilave_edilecek_kdv2 { get; set; }

		public Double cha_ilave_edilecek_kdv3 { get; set; }

		public Double cha_ilave_edilecek_kdv4 { get; set; }

		public Double cha_ilave_edilecek_kdv5 { get; set; }

		public Double cha_ilave_edilecek_kdv6 { get; set; }

		public Double cha_ilave_edilecek_kdv7 { get; set; }

		public Double cha_ilave_edilecek_kdv8 { get; set; }

		public Double cha_ilave_edilecek_kdv9 { get; set; }

		public Double cha_ilave_edilecek_kdv10 { get; set; }

		public Byte cha_e_islem_turu { get; set; }

		public Byte cha_fatura_belge_turu { get; set; }

		public String cha_diger_belge_adi { get; set; }

		public String cha_uuid { get; set; }

		public Int32 cha_adres_no { get; set; }

		public Double cha_vergifon_toplam { get; set; }

		public DateTime cha_ilk_belge_tarihi { get; set; }

		public Double cha_ilk_belge_doviz_kuru { get; set; }

		public String cha_HareketGrupKodu1 { get; set; }

		public String cha_HareketGrupKodu2 { get; set; }

		public String cha_HareketGrupKodu3 { get; set; }

		public String cha_ebelgeno_seri { get; set; }

		public Int32 cha_ebelgeno_sira { get; set; }

		public String cha_hubid { get; set; }

		public String cha_hubglbid { get; set; }

		public String cha_disyazilimid { get; set; }
		#endregion
	}
}