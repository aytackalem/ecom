using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Mikro.Entities
{
    [Table("CARI_HESAP_HAREKETLERI_EK")]
	public partial class CARI_HESAP_HAREKETLERI_EK
	{
		#region Property
		public Guid chaek_Guid { get; set; }

		public Int16 chaek_DBCno { get; set; }

		public Int32 chaek_SpecRecNo { get; set; }

		public Boolean chaek_iptal { get; set; }

		public Int16 chaek_fileid { get; set; }

		public Boolean chaek_hidden { get; set; }

		public Boolean chaek_kilitli { get; set; }

		public Boolean chaek_degisti { get; set; }

		public Int32 chaek_CheckSum { get; set; }

		public Int16 chaek_create_user { get; set; }

		public DateTime chaek_create_date { get; set; }

		public Int16 chaek_lastup_user { get; set; }

		public DateTime chaek_lastup_date { get; set; }

		public String chaek_special1 { get; set; }

		public String chaek_special2 { get; set; }

		public String chaek_special3 { get; set; }

		public Guid chaek_related_uid { get; set; }

		public Double cha_mustahsil_borsa { get; set; }

		public Double cha_mustahsil_bagkur { get; set; }

		public Double cha_mustahsil_diger { get; set; }

		public Double cha_HalMSDF { get; set; }

		public Double cha_HalHamaliye { get; set; }

		public Double cha_HalStopaj { get; set; }

		public Double cha_HalKomisyonu { get; set; }

		public Double cha_HalRusum { get; set; }

		public Double cha_HalNavlunTut { get; set; }

		public Double cha_HalRehinFuture { get; set; }

		public Double cha_HalKomisyon { get; set; }

		public Double cha_HalRehinSandikmiktari { get; set; }

		public Double cha_HalSandikVrMiktar { get; set; }

		public Double cha_HalSandikTutari { get; set; }

		public Double cha_HalSandikKDVTutari { get; set; }

		public Double cha_HalrehinSandikTutari { get; set; }

		public Double cha_HalHamaliyeKdv { get; set; }

		public Boolean cha_HalHamaliyeVergisiz_fl { get; set; }

		public Double cha_HalRusumKdv { get; set; }

		public Double cha_HalDiger { get; set; }

		public Double cha_HalDigerKdv { get; set; }

		public Boolean cha_HalDigerVergisiz_fl { get; set; }

		public Boolean cha_HalrusumVergisiz_fl { get; set; }

		public Boolean cha_Halrusumsuz_fl { get; set; }

		public Guid cha_sozlesme_uid { get; set; }

		public Guid cha_ciroprim_uid { get; set; }

		public Guid cha_bakimhar_uid { get; set; }

		public Guid cha_avanstalep_uid { get; set; }

		public Guid cha_gidkatsoz_uid { get; set; }

		public Byte cha_Tevkifat_turu { get; set; }

		public Double cha_tevkifat1Yok { get; set; }

		public Double cha_tevkifat131 { get; set; }

		public Double cha_tevkifat191 { get; set; }

		public Double cha_tevkifat121 { get; set; }

		public Double cha_tevkifat132 { get; set; }

		public Double cha_tevkifat161 { get; set; }

		public Double cha_tevkifat145 { get; set; }

		public Double cha_tevkifat1Tam { get; set; }

		public Double cha_tevkifat1102 { get; set; }

		public Double cha_tevkifat1105 { get; set; }

		public Double cha_tevkifat1107 { get; set; }

		public Double cha_tevkifat2Yok { get; set; }

		public Double cha_tevkifat231 { get; set; }

		public Double cha_tevkifat291 { get; set; }

		public Double cha_tevkifat221 { get; set; }

		public Double cha_tevkifat232 { get; set; }

		public Double cha_tevkifat261 { get; set; }

		public Double cha_tevkifat245 { get; set; }

		public Double cha_tevkifat2Tam { get; set; }

		public Double cha_tevkifat2102 { get; set; }

		public Double cha_tevkifat2105 { get; set; }

		public Double cha_tevkifat2107 { get; set; }

		public Double cha_tevkifat3Yok { get; set; }

		public Double cha_tevkifat331 { get; set; }

		public Double cha_tevkifat391 { get; set; }

		public Double cha_tevkifat321 { get; set; }

		public Double cha_tevkifat332 { get; set; }

		public Double cha_tevkifat361 { get; set; }

		public Double cha_tevkifat345 { get; set; }

		public Double cha_tevkifat3Tam { get; set; }

		public Double cha_tevkifat3102 { get; set; }

		public Double cha_tevkifat3105 { get; set; }

		public Double cha_tevkifat3107 { get; set; }

		public Double cha_tevkifat4Yok { get; set; }

		public Double cha_tevkifat431 { get; set; }

		public Double cha_tevkifat491 { get; set; }

		public Double cha_tevkifat421 { get; set; }

		public Double cha_tevkifat432 { get; set; }

		public Double cha_tevkifat461 { get; set; }

		public Double cha_tevkifat445 { get; set; }

		public Double cha_tevkifat4Tam { get; set; }

		public Double cha_tevkifat4102 { get; set; }

		public Double cha_tevkifat4105 { get; set; }

		public Double cha_tevkifat4107 { get; set; }

		public Double cha_tevkifat5Yok { get; set; }

		public Double cha_tevkifat531 { get; set; }

		public Double cha_tevkifat591 { get; set; }

		public Double cha_tevkifat521 { get; set; }

		public Double cha_tevkifat532 { get; set; }

		public Double cha_tevkifat561 { get; set; }

		public Double cha_tevkifat545 { get; set; }

		public Double cha_tevkifat5Tam { get; set; }

		public Double cha_tevkifat5102 { get; set; }

		public Double cha_tevkifat5105 { get; set; }

		public Double cha_tevkifat5107 { get; set; }

		public Double cha_tevkifat6Yok { get; set; }

		public Double cha_tevkifat631 { get; set; }

		public Double cha_tevkifat691 { get; set; }

		public Double cha_tevkifat621 { get; set; }

		public Double cha_tevkifat632 { get; set; }

		public Double cha_tevkifat661 { get; set; }

		public Double cha_tevkifat645 { get; set; }

		public Double cha_tevkifat6Tam { get; set; }

		public Double cha_tevkifat6102 { get; set; }

		public Double cha_tevkifat6105 { get; set; }

		public Double cha_tevkifat6107 { get; set; }

		public Double cha_tevkifat7Yok { get; set; }

		public Double cha_tevkifat731 { get; set; }

		public Double cha_tevkifat791 { get; set; }

		public Double cha_tevkifat721 { get; set; }

		public Double cha_tevkifat732 { get; set; }

		public Double cha_tevkifat761 { get; set; }

		public Double cha_tevkifat745 { get; set; }

		public Double cha_tevkifat7Tam { get; set; }

		public Double cha_tevkifat7102 { get; set; }

		public Double cha_tevkifat7105 { get; set; }

		public Double cha_tevkifat7107 { get; set; }

		public Double cha_tevkifat8Yok { get; set; }

		public Double cha_tevkifat831 { get; set; }

		public Double cha_tevkifat891 { get; set; }

		public Double cha_tevkifat821 { get; set; }

		public Double cha_tevkifat832 { get; set; }

		public Double cha_tevkifat861 { get; set; }

		public Double cha_tevkifat845 { get; set; }

		public Double cha_tevkifat8Tam { get; set; }

		public Double cha_tevkifat8102 { get; set; }

		public Double cha_tevkifat8105 { get; set; }

		public Double cha_tevkifat8107 { get; set; }

		public Double cha_tevkifat9Yok { get; set; }

		public Double cha_tevkifat931 { get; set; }

		public Double cha_tevkifat991 { get; set; }

		public Double cha_tevkifat921 { get; set; }

		public Double cha_tevkifat932 { get; set; }

		public Double cha_tevkifat961 { get; set; }

		public Double cha_tevkifat945 { get; set; }

		public Double cha_tevkifat9Tam { get; set; }

		public Double cha_tevkifat9102 { get; set; }

		public Double cha_tevkifat9105 { get; set; }

		public Double cha_tevkifat9107 { get; set; }

		public Double cha_tevkifat10Yok { get; set; }

		public Double cha_tevkifat1031 { get; set; }

		public Double cha_tevkifat1091 { get; set; }

		public Double cha_tevkifat1021 { get; set; }

		public Double cha_tevkifat1032 { get; set; }

		public Double cha_tevkifat1061 { get; set; }

		public Double cha_tevkifat1045 { get; set; }

		public Double cha_tevkifat10Tam { get; set; }

		public Double cha_tevkifat10102 { get; set; }

		public Double cha_tevkifat10105 { get; set; }

		public Double cha_tevkifat10107 { get; set; }

		public String cha_Istisna1 { get; set; }

		public String cha_Istisna2 { get; set; }

		public String cha_Istisna3 { get; set; }

		public String cha_Istisna4 { get; set; }

		public String cha_Istisna5 { get; set; }

		public String cha_Istisna6 { get; set; }

		public String cha_Istisna7 { get; set; }

		public String cha_Istisna8 { get; set; }

		public String cha_Istisna9 { get; set; }

		public String cha_Istisna10 { get; set; }

		public Byte cha_otv_tevkifat_turu { get; set; }

		public Double cha_otv_tevkifat_tam { get; set; }

		public Guid cha_servishar_uid { get; set; }

		public String cha_ozel_matrah_kodu { get; set; }

		public Guid cha_periyodik_uid { get; set; }

		public Int32 cha_periyodik_donem_no { get; set; }

		public Int16 cha_vergifonid_1 { get; set; }

		public Double cha_vergifontutari_1 { get; set; }

		public Int16 cha_vergifonid_2 { get; set; }

		public Double cha_vergifontutari_2 { get; set; }

		public Int16 cha_vergifonid_3 { get; set; }

		public Double cha_vergifontutari_3 { get; set; }

		public Int16 cha_vergifonid_4 { get; set; }

		public Double cha_vergifontutari_4 { get; set; }

		public Int16 cha_vergifonid_5 { get; set; }

		public Double cha_vergifontutari_5 { get; set; }

		public String cha_yolcuberaber_kodu { get; set; }

		public String cha_yetkiliaracikurumkodu { get; set; }

		public Guid cha_icraodeme_uid { get; set; }

		public String cha_EArsiv_unvani_ad { get; set; }

		public String cha_EArsiv_unvani_soyad { get; set; }

		public String cha_EArsiv_daire_adi { get; set; }

		public String cha_EArsiv_Vkn { get; set; }

		public String cha_EArsiv_ulke { get; set; }

		public String cha_EArsiv_Il { get; set; }

		public String cha_EArsiv_tel_ulke_kod { get; set; }

		public String cha_EArsiv_tel_bolge_kod { get; set; }

		public String cha_EArsiv_tel_no { get; set; }

		public String cha_EArsiv_mail { get; set; }

		public String cha_EArsiv_cadde { get; set; }

		public String cha_EArsiv_sokak { get; set; }

		public String cha_EArsiv_Ilce { get; set; }

		public String cha_EArsiv_Ceptel { get; set; }

		public Double cha_kkegmatrahi { get; set; }

		public Double cha_kkegvergi { get; set; }

		public Double cha_otv_vergi1 { get; set; }

		public Double cha_otv_vergi2 { get; set; }

		public Double cha_otv_vergi3 { get; set; }

		public Double cha_otv_vergi4 { get; set; }

		public Double cha_otv_vergi5 { get; set; }

		public Double cha_otv_vergi6 { get; set; }

		public Double cha_otv_vergi7 { get; set; }

		public Double cha_otv_vergi8 { get; set; }

		public Double cha_otv_vergi9 { get; set; }

		public Double cha_otv_vergi10 { get; set; }
		#endregion
	}
}