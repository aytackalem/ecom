using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Mikro.Entities
{
    [Table("STOK_HAREKETLERI")]
	public partial class STOK_HAREKETLERI
	{
		#region Property
		public Guid sth_Guid { get; set; }

		public Int16 sth_DBCno { get; set; }

		public Int32 sth_SpecRECno { get; set; }

		public Boolean sth_iptal { get; set; }

		public Int16 sth_fileid { get; set; }

		public Boolean sth_hidden { get; set; }

		public Boolean sth_kilitli { get; set; }

		public Boolean sth_degisti { get; set; }

		public Int32 sth_checksum { get; set; }

		public Int16 sth_create_user { get; set; }

		public DateTime sth_create_date { get; set; }

		public Int16 sth_lastup_user { get; set; }

		public DateTime sth_lastup_date { get; set; }

		public String sth_special1 { get; set; }

		public String sth_special2 { get; set; }

		public String sth_special3 { get; set; }

		public Int32 sth_firmano { get; set; }

		public Int32 sth_subeno { get; set; }

		public DateTime sth_tarih { get; set; }

		public Byte sth_tip { get; set; }

		public Byte sth_cins { get; set; }

		public Byte sth_normal_iade { get; set; }

		public Byte sth_evraktip { get; set; }

		public String sth_evrakno_seri { get; set; }

		public Int32 sth_evrakno_sira { get; set; }

		public Int32 sth_satirno { get; set; }

		public String sth_belge_no { get; set; }

		public DateTime sth_belge_tarih { get; set; }

		public String sth_stok_kod { get; set; }

		public Byte sth_isk_mas1 { get; set; }

		public Byte sth_isk_mas2 { get; set; }

		public Byte sth_isk_mas3 { get; set; }

		public Byte sth_isk_mas4 { get; set; }

		public Byte sth_isk_mas5 { get; set; }

		public Byte sth_isk_mas6 { get; set; }

		public Byte sth_isk_mas7 { get; set; }

		public Byte sth_isk_mas8 { get; set; }

		public Byte sth_isk_mas9 { get; set; }

		public Byte sth_isk_mas10 { get; set; }

		public Boolean sth_sat_iskmas1 { get; set; }

		public Boolean sth_sat_iskmas2 { get; set; }

		public Boolean sth_sat_iskmas3 { get; set; }

		public Boolean sth_sat_iskmas4 { get; set; }

		public Boolean sth_sat_iskmas5 { get; set; }

		public Boolean sth_sat_iskmas6 { get; set; }

		public Boolean sth_sat_iskmas7 { get; set; }

		public Boolean sth_sat_iskmas8 { get; set; }

		public Boolean sth_sat_iskmas9 { get; set; }

		public Boolean sth_sat_iskmas10 { get; set; }

		public Byte sth_pos_satis { get; set; }

		public Boolean sth_promosyon_fl { get; set; }

		public Byte sth_cari_cinsi { get; set; }

		public String sth_cari_kodu { get; set; }

		public Byte sth_cari_grup_no { get; set; }

		public String sth_isemri_gider_kodu { get; set; }

		public String sth_plasiyer_kodu { get; set; }

		public Byte sth_har_doviz_cinsi { get; set; }

		public Double sth_har_doviz_kuru { get; set; }

		public Double sth_alt_doviz_kuru { get; set; }

		public Byte sth_stok_doviz_cinsi { get; set; }

		public Double sth_stok_doviz_kuru { get; set; }

		public Double sth_miktar { get; set; }

		public Double sth_miktar2 { get; set; }

		public Byte sth_birim_pntr { get; set; }

		public Double sth_tutar { get; set; }

		public Double sth_iskonto1 { get; set; }

		public Double sth_iskonto2 { get; set; }

		public Double sth_iskonto3 { get; set; }

		public Double sth_iskonto4 { get; set; }

		public Double sth_iskonto5 { get; set; }

		public Double sth_iskonto6 { get; set; }

		public Double sth_masraf1 { get; set; }

		public Double sth_masraf2 { get; set; }

		public Double sth_masraf3 { get; set; }

		public Double sth_masraf4 { get; set; }

		public Byte sth_vergi_pntr { get; set; }

		public Double sth_vergi { get; set; }

		public Byte sth_masraf_vergi_pntr { get; set; }

		public Double sth_masraf_vergi { get; set; }

		public Double sth_netagirlik { get; set; }

		public Int32 sth_odeme_op { get; set; }

		public String sth_aciklama { get; set; }

		public Guid sth_sip_uid { get; set; }

		public Guid sth_fat_uid { get; set; }

		public Int32 sth_giris_depo_no { get; set; }

		public Int32 sth_cikis_depo_no { get; set; }

		public DateTime sth_malkbl_sevk_tarihi { get; set; }

		public String sth_cari_srm_merkezi { get; set; }

		public String sth_stok_srm_merkezi { get; set; }

		public DateTime sth_fis_tarihi { get; set; }

		public Int32 sth_fis_sirano { get; set; }

		public Boolean sth_vergisiz_fl { get; set; }

		public Double sth_maliyet_ana { get; set; }

		public Double sth_maliyet_alternatif { get; set; }

		public Double sth_maliyet_orjinal { get; set; }

		public Int32 sth_adres_no { get; set; }

		public String sth_parti_kodu { get; set; }

		public Int32 sth_lot_no { get; set; }

		public Guid sth_kons_uid { get; set; }

		public String sth_proje_kodu { get; set; }

		public String sth_exim_kodu { get; set; }

		public Byte sth_otv_pntr { get; set; }

		public Double sth_otv_vergi { get; set; }

		public Double sth_brutagirlik { get; set; }

		public Byte sth_disticaret_turu { get; set; }

		public Double sth_otvtutari { get; set; }

		public Boolean sth_otvvergisiz_fl { get; set; }

		public Byte sth_oiv_pntr { get; set; }

		public Double sth_oiv_vergi { get; set; }

		public Boolean sth_oivvergisiz_fl { get; set; }

		public Int32 sth_fiyat_liste_no { get; set; }

		public Double sth_oivtutari { get; set; }

		public Byte sth_Tevkifat_turu { get; set; }

		public Int32 sth_nakliyedeposu { get; set; }

		public Byte sth_nakliyedurumu { get; set; }

		public Guid sth_yetkili_uid { get; set; }

		public Boolean sth_taxfree_fl { get; set; }

		public Double sth_ilave_edilecek_kdv { get; set; }

		public String sth_ismerkezi_kodu { get; set; }

		public String sth_HareketGrupKodu1 { get; set; }

		public String sth_HareketGrupKodu2 { get; set; }

		public String sth_HareketGrupKodu3 { get; set; }

		public Double sth_Olcu1 { get; set; }

		public Double sth_Olcu2 { get; set; }

		public Double sth_Olcu3 { get; set; }

		public Double sth_Olcu4 { get; set; }

		public Double sth_Olcu5 { get; set; }

		public Byte sth_FormulMiktarNo { get; set; }

		public Double sth_FormulMiktar { get; set; }

		public Byte sth_eirs_senaryo { get; set; }

		public Byte sth_eirs_tipi { get; set; }

        public DateTime? sth_teslim_tarihi { get; set; }

        public Boolean? sth_matbu_fl { get; set; }

        public byte? sth_satis_fiyat_doviz_cinsi { get; set; }

        public byte? sth_satis_fiyat_doviz_kuru { get; set; }
        #endregion
    }
}