using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Infrastructure.Accounting.Mikro.Entities
{
	[Table("STOKLAR")]
	public partial class STOKLAR
	{
		#region Property
		public Guid sto_Guid { get; set; }

		public Int16 sto_DBCno { get; set; }

		public Int32 sto_SpecRECno { get; set; }

		public Boolean sto_iptal { get; set; }

		public Int16 sto_fileid { get; set; }

		public Boolean sto_hidden { get; set; }

		public Boolean sto_kilitli { get; set; }

		public Boolean sto_degisti { get; set; }

		public Int32 sto_checksum { get; set; }

		public Int16 sto_create_user { get; set; }

		public DateTime sto_create_date { get; set; }

		public Int16 sto_lastup_user { get; set; }

		public DateTime sto_lastup_date { get; set; }

		public String sto_special1 { get; set; }

		public String sto_special2 { get; set; }

		public String sto_special3 { get; set; }

		public String sto_kod { get; set; }

		public String sto_isim { get; set; }

		public String sto_kisa_ismi { get; set; }

		public String sto_yabanci_isim { get; set; }

		public String sto_sat_cari_kod { get; set; }

		public Byte sto_cins { get; set; }

		public Byte sto_doviz_cinsi { get; set; }

		public Byte sto_detay_takip { get; set; }

		public String sto_birim1_ad { get; set; }

		public Double sto_birim1_katsayi { get; set; }

		public Double sto_birim1_agirlik { get; set; }

		public Double sto_birim1_en { get; set; }

		public Double sto_birim1_boy { get; set; }

		public Double sto_birim1_yukseklik { get; set; }

		public Double sto_birim1_dara { get; set; }

		public String sto_birim2_ad { get; set; }

		public Double sto_birim2_katsayi { get; set; }

		public Double sto_birim2_agirlik { get; set; }

		public Double sto_birim2_en { get; set; }

		public Double sto_birim2_boy { get; set; }

		public Double sto_birim2_yukseklik { get; set; }

		public Double sto_birim2_dara { get; set; }

		public String sto_birim3_ad { get; set; }

		public Double sto_birim3_katsayi { get; set; }

		public Double sto_birim3_agirlik { get; set; }

		public Double sto_birim3_en { get; set; }

		public Double sto_birim3_boy { get; set; }

		public Double sto_birim3_yukseklik { get; set; }

		public Double sto_birim3_dara { get; set; }

		public String sto_birim4_ad { get; set; }

		public Double sto_birim4_katsayi { get; set; }

		public Double sto_birim4_agirlik { get; set; }

		public Double sto_birim4_en { get; set; }

		public Double sto_birim4_boy { get; set; }

		public Double sto_birim4_yukseklik { get; set; }

		public Double sto_birim4_dara { get; set; }

		public String sto_muh_kod { get; set; }

		public String sto_muh_Iade_kod { get; set; }

		public String sto_muh_sat_muh_kod { get; set; }

		public String sto_muh_satIadmuhkod { get; set; }

		public String sto_muh_sat_isk_kod { get; set; }

		public String sto_muh_aIiskmuhkod { get; set; }

		public String sto_muh_satmalmuhkod { get; set; }

		public String sto_yurtdisi_satmuhk { get; set; }

		public String sto_ilavemasmuhkod { get; set; }

		public String sto_yatirimtesmuhkod { get; set; }

		public String sto_depsatmuhkod { get; set; }

		public String sto_depsatmalmuhkod { get; set; }

		public String sto_bagortsatmuhkod { get; set; }

		public String sto_bagortsatIadmuhkod { get; set; }

		public String sto_bagortsatIskmuhkod { get; set; }

		public String sto_satfiyfarkmuhkod { get; set; }

		public String sto_yurtdisisatmalmuhkod { get; set; }

		public String sto_bagortsatmalmuhkod { get; set; }

		public String sto_sifirbedsatmalmuhkod { get; set; }

		public String sto_ihrackayitlisatismuhkod { get; set; }

		public String sto_ihrackayitlisatismaliyetimuhkod { get; set; }

		public Double sto_karorani { get; set; }

		public Double sto_min_stok { get; set; }

		public Double sto_siparis_stok { get; set; }

		public Double sto_max_stok { get; set; }

		public Byte sto_ver_sip_birim { get; set; }

		public Byte sto_al_sip_birim { get; set; }

		public Int16 sto_siparis_sure { get; set; }

		public Byte sto_perakende_vergi { get; set; }

		public Byte sto_toptan_vergi { get; set; }

		public String sto_yer_kod { get; set; }

		public Byte sto_elk_etk_tipi { get; set; }

		public Byte sto_raf_etiketli { get; set; }

		public Byte sto_etiket_bas { get; set; }

		public Byte sto_satis_dursun { get; set; }

		public Byte sto_siparis_dursun { get; set; }

		public Byte sto_malkabul_dursun { get; set; }

		public Boolean sto_malkabul_gun1 { get; set; }

		public Boolean sto_malkabul_gun2 { get; set; }

		public Boolean sto_malkabul_gun3 { get; set; }

		public Boolean sto_malkabul_gun4 { get; set; }

		public Boolean sto_malkabul_gun5 { get; set; }

		public Boolean sto_malkabul_gun6 { get; set; }

		public Boolean sto_malkabul_gun7 { get; set; }

		public Boolean sto_siparis_gun1 { get; set; }

		public Boolean sto_siparis_gun2 { get; set; }

		public Boolean sto_siparis_gun3 { get; set; }

		public Boolean sto_siparis_gun4 { get; set; }

		public Boolean sto_siparis_gun5 { get; set; }

		public Boolean sto_siparis_gun6 { get; set; }

		public Boolean sto_siparis_gun7 { get; set; }

		public Boolean sto_iskon_yapilamaz { get; set; }

		public Boolean sto_tasfiyede { get; set; }

		public Int16 sto_alt_grup_no { get; set; }

		public String sto_kategori_kodu { get; set; }

		public String sto_urun_sorkod { get; set; }

		public String sto_altgrup_kod { get; set; }

		public String sto_anagrup_kod { get; set; }

		public String sto_uretici_kodu { get; set; }

		public String sto_sektor_kodu { get; set; }

		public String sto_reyon_kodu { get; set; }

		public String sto_muhgrup_kodu { get; set; }

		public String sto_ambalaj_kodu { get; set; }

		public String sto_marka_kodu { get; set; }

		public String sto_beden_kodu { get; set; }

		public String sto_renk_kodu { get; set; }

		public String sto_model_kodu { get; set; }

		public String sto_sezon_kodu { get; set; }

		public String sto_hammadde_kodu { get; set; }

		public String sto_prim_kodu { get; set; }

		public String sto_kalkon_kodu { get; set; }

		public String sto_paket_kodu { get; set; }

		public String sto_pozisyonbayrak_kodu { get; set; }

		public String sto_mkod_artik { get; set; }

		public Boolean sto_kasa_tarti_fl { get; set; }

		public Boolean sto_bedenli_takip { get; set; }

		public Boolean sto_renkDetayli { get; set; }

		public Boolean sto_miktarondalikli_fl { get; set; }

		public Boolean sto_pasif_fl { get; set; }

		public Boolean sto_eksiyedusebilir_fl { get; set; }

		public String sto_GtipNo { get; set; }

		public Double sto_puan { get; set; }

		public String sto_komisyon_hzmkodu { get; set; }

		public Double sto_komisyon_orani { get; set; }

		public Byte sto_otvuygulama { get; set; }

		public Double sto_otvtutar { get; set; }

		public Byte sto_otvliste { get; set; }

		public Byte sto_otvbirimi { get; set; }

		public Double sto_prim_orani { get; set; }

		public Int16 sto_garanti_sure { get; set; }

		public Byte sto_garanti_sure_tipi { get; set; }

		public Double sto_iplik_Ne_no { get; set; }

		public Double sto_standartmaliyet { get; set; }

		public Double sto_kanban_kasa_miktari { get; set; }

		public Byte sto_oivuygulama { get; set; }

		public Boolean sto_zraporu_stoku_fl { get; set; }

		public Double sto_maxiskonto_orani { get; set; }

		public Boolean sto_detay_takibinde_depo_kontrolu_fl { get; set; }

		public String sto_tamamlayici_kodu { get; set; }

		public Byte sto_oto_barkod_acma_sekli { get; set; }

		public String sto_oto_barkod_kod_yapisi { get; set; }

		public Double sto_KasaIskontoOrani { get; set; }

		public Double sto_KasaIskontoTutari { get; set; }

		public Double sto_gelirpayi { get; set; }

		public Double sto_oivtutar { get; set; }

		public Byte sto_oivturu { get; set; }

		public String sto_giderkodu { get; set; }

		public Byte sto_oivvergipntr { get; set; }

		public Byte sto_Tevkifat_turu { get; set; }

		public Boolean sto_SKT_fl { get; set; }

		public Int16 sto_terazi_SKT { get; set; }

		public Int16 sto_RafOmru { get; set; }

		public Boolean sto_KasadaTaksitlenebilir_fl { get; set; }

		public String sto_ufrsfark_kod { get; set; }

		public String sto_iade_ufrsfark_kod { get; set; }

		public String sto_yurticisat_ufrsfark_kod { get; set; }

		public String sto_satiade_ufrsfark_kod { get; set; }

		public String sto_satisk_ufrsfark_kod { get; set; }

		public String sto_alisk_ufrsfark_kod { get; set; }

		public String sto_satmal_ufrsfark_kod { get; set; }

		public String sto_yurtdisisat_ufrsfark_kod { get; set; }

		public String sto_ilavemas_ufrsfark_kod { get; set; }

		public String sto_yatirimtes_ufrsfark_kod { get; set; }

		public String sto_depsat_ufrsfark_kod { get; set; }

		public String sto_depsatmal_ufrsfark_kod { get; set; }

		public String sto_bagortsat_ufrsfark_kod { get; set; }

		public String sto_bagortsatiade_ufrsfark_kod { get; set; }

		public String sto_bagortsatisk_ufrsfark_kod { get; set; }

		public String sto_satfiyfark_ufrsfark_kod { get; set; }

		public String sto_yurtdisisatmal_ufrsfark_kod { get; set; }

		public String sto_bagortsatmal_ufrsfark_kod { get; set; }

		public String sto_sifirbedsatmal_ufrsfark_kod { get; set; }

		public String sto_uretimmaliyet_ufrsfark_kod { get; set; }

		public String sto_uretimkapasite_ufrsfark_kod { get; set; }

		public String sto_degerdusuklugu_ufrs_kod { get; set; }

		public Double sto_halrusumyudesi { get; set; }

		public Boolean sto_webe_gonderilecek_fl { get; set; }

		public Int16 sto_min_stok_belirleme_gun { get; set; }

		public Int16 sto_sip_stok_belirleme_gun { get; set; }

		public Int16 sto_max_stok_belirleme_gun { get; set; }

		public Boolean sto_sev_bel_opr_degerlendime_fl { get; set; }

		public Byte sto_otv_tevkifat_turu { get; set; }

		public Byte sto_kay_plan_degerlendir { get; set; }

		public Boolean sto_CRM_sistemine_aktar_fl { get; set; }

		public Int32 sto_plu_no { get; set; }

		public Byte sto_yerli_yabanci { get; set; }

		public String sto_mensei { get; set; }

		public Boolean sto_oto_parti_lot_kod_fl { get; set; }

		public String sto_efat_sinif_kodu { get; set; }

		public String sto_efat_sinif_listesi { get; set; }

		public String sto_efat_sinif_versiyonu { get; set; }

		public Boolean sto_utssisteminegonderilsin_fl { get; set; }

		public Boolean sto_posetbeyannamekonusu_fl { get; set; }

		public Int16 sto_STT_oncesi_kaldirma { get; set; }

		public Int16 sto_toplam_rafomru { get; set; }

		public Boolean sto_fiyat_kasada_belirlenir_fl { get; set; }

		public Byte sto_franchise_siparis_dursun { get; set; }

		public String sto_GEKAP { get; set; }

		public Byte sto_GEKAP_birim { get; set; }

		public String sto_resim_url { get; set; }

		public String sto_GEKAP_depozitoonaykodu { get; set; }
		#endregion
	}
}