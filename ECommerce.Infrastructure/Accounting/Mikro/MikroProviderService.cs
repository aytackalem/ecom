﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Base;
using ECommerce.Domain.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Mikro
{
    public class MikroProviderService : IAccountingProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private List<ResponsibilityCenter> _responsibilityCenter = new List<ResponsibilityCenter>
        {
            new ResponsibilityCenter{
                Id="CS",
                Name="CICEK SEPETI",
                AccountNumber="120.01.040",
                DocumentDescription="NOT: Bu faturanın tahsilatı Çiçek Sepeti İnternet Hizmetleri A.Ş. Vergi No:2530759503 hesabına devredilmiştir.",
                IsBrokerage=true
            },
             new ResponsibilityCenter{
                Id="GG",
                Name="GITTIGIDIYOR",
                AccountNumber="120.01.038",
                DocumentDescription="NOT: Bu faturanin tutari Gitti Gidiyor Bilgi Tek. San.Tic. A.S. Vergi No:3960458851 hesabina devredilmistir.",
                IsBrokerage=true
            },
            new ResponsibilityCenter{
                Id="HB",
                Name="HEPSIBURADA",
                AccountNumber="120.01.004",
                DocumentDescription="NOT: Bu faturanin tutari D-Market Elektronik Hiz. A.S. Vergi No:2650179910 hesabina devredilmistir.",
                IsBrokerage=true
            },
             new ResponsibilityCenter{
                Id="MP",
                Name="MORHIPO",
                AccountNumber="120.01.037",
                DocumentDescription="NOT: Bu faturanin tutari Boyner Büyük Magazacilik A.S. Vergi No:5200043963 hesabina devredilmistir.",
                IsBrokerage=true
            },
              new ResponsibilityCenter{
                Id="N11",
                Name="N11",
                AccountNumber="120.01.003",
                DocumentDescription="NOT: Bu faturanin tutari Dogus Planet Elekt. Tic.Hiz.A.S. Vergi No:3090345332 hesabina devredilmistir.",
                IsBrokerage=true
            },
               new ResponsibilityCenter{
                Id="TY",
                Name="TRENDYOL",
                AccountNumber="120.01.006",
                DocumentDescription="NOT: Bu faturanin tutari Dsm Grup Danismanlik Iletisim Ve Satis Tic. A.s. Vergi No:3130557669 hesabina devredilmistir.",
                IsBrokerage=true
            },
               new ResponsibilityCenter{
                Id="PA",
                Name="PAZARAMA",
                AccountNumber="120.01.127",
                DocumentDescription="NOT: Bu faturanin tutari TOPKAPI DANISMANLIK ELEKTRONIK HIZMETLER PAZARLAMA VE TICARET ANONIM SIRKETI Vergi No:8540156705 hesabina devredilmistir.",
                IsBrokerage=true
            },
               new ResponsibilityCenter{
                Id="VO",
                Name="VODAFONE",
                AccountNumber="120.01.131",
                DocumentDescription="NOT: Bu faturanin tutari VODAFONE DAĞITIM SERVİS VE İÇERİK A.Ş. Vergi No:9250401254 hesabina devredilmistir.",
                IsBrokerage=true
            },
               new ResponsibilityCenter{
                Id="MS",
                Name="MERKEZ SATISLAR",
                AccountNumber="120.01.005",
                DocumentDescription="",
                IsBrokerage=false
            }
        };
        #endregion

        #region Constructors
        public MikroProviderService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                var transactionDate = DateTime.Now.Date;

                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                            ._unitOfWork
                            .AccountingCompanyConfigurationRepository
                            .DbSet()
                            .Where(x => x.AccountingCompany.AccountingId == "M")
                            .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                var order = _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Customer)
                    .Include(o => o.Company)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(x => x.ProductInformation)
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .FirstOrDefault(x => x.Id == orderId);

                var cariKod = $"120.{accountCustomerPrefix}.{order.Customer.Id + 10000000}";
                var cariVergiDaireKodu = "998";
                var chaEvraknoSeri = eArchiveInvoiceSerie;
                var cari_vdaire_adi = "NİHAİ TÜKETİCİ";
                var cari_vdaire_no = "33333333333";

                if (!string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) &&
                    !string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxOffice))
                {
                    cariKod = $"120.{accountCompanyPrefix}.{order.Customer.Id + 10000000}";
                    cariVergiDaireKodu = "999";
                    chaEvraknoSeri = eInvoiceInvoiceSerie;
                    cari_vdaire_adi = order.OrderInvoiceInformation.TaxOffice;
                    cari_vdaire_no = order.OrderInvoiceInformation.TaxNumber;
                }

                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    mikroUnitOfWork.BeginTransaction();

                    #region Cari_Hesaplar
                    var taxInfo = GetTaxInfo(order);

                    //CARI_HESAPLAR
                    CARI_HESAPLAR cariHesaplar = null;

                    /*
                     * if: Vergi numarası girmiş müşteriler için vergi numarasından sorgulama yapılır.
                     * else: Cari kod ile sorgulama yapılır.
                     */
                    if (!string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) &&
                        !string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxOffice))
                        cariHesaplar = mikroUnitOfWork.CARI_HESAPLARRepository.ReadByTaxNumber(order.OrderInvoiceInformation.TaxNumber);
                    else
                        cariHesaplar = mikroUnitOfWork.CARI_HESAPLARRepository.ReadByCariKod(cariKod);

                    if (cariHesaplar == null)
                    {
                        cariHesaplar =
                        new CARI_HESAPLAR
                        {
                            cari_Guid = Guid.NewGuid(),
                            cari_fileid = 31,
                            cari_create_user = 3,
                            cari_create_date = transactionDate,
                            cari_lastup_user = 3,
                            cari_lastup_date = transactionDate,
                            cari_kod = cariKod,
                            cari_unvan1 = order.OrderInvoiceInformation.FirstName,
                            cari_unvan2 = order.OrderInvoiceInformation.LastName,
                            cari_muh_kod = "120.01.#SOM#",
                            cari_doviz_cinsi1 = 255,
                            cari_doviz_cinsi2 = 255,
                            cari_vade_fark_yuz = 25,
                            cari_KurHesapSekli = 1,
                            cari_satis_fk = 1,
                            cari_fatura_adres_no = 1,
                            cari_sevk_adres_no = 1,
                            cari_EftHesapNum = 1,
                            cari_kaydagiristarihi = transactionDate,
                            cari_TeminatMekAlacakMuhKodu = "910",
                            cari_TeminatMekBorcMuhKodu = "912",
                            cari_VerilenDepozitoTeminatMuhKodu = "226",
                            cari_AlinanDepozitoTeminatMuhKodu = "326",
                            cari_efatura_baslangic_tarihi = new DateTime(1899, 12, 31),
                            cari_eirsaliye_baslangic_tarihi = new DateTime(1899, 12, 31),
                            //cari_vdaire_adi = order.OrderInvoiceInformation.TaxOffice,
                            //cari_vdaire_no = order.OrderInvoiceInformation.TaxNumber,
                            //cari_vergidairekodu = cariVergiDaireKodu,
                            cari_vdaire_adi = cari_vdaire_adi,
                            cari_vdaire_no = cari_vdaire_no,
                            cari_vergidairekodu = cariVergiDaireKodu,
                            cari_efatura_fl = false,
                            cari_TeminatMekAlacakMuhKodu1 = string.Empty,
                            cari_TeminatMekAlacakMuhKodu2 = string.Empty,
                            cari_TeminatMekBorcMuhKodu1 = string.Empty,
                            cari_TeminatMekBorcMuhKodu2 = string.Empty,
                            cari_VergiKimlikNo = string.Empty,
                            cari_special1 = string.Empty,
                            cari_special2 = string.Empty,
                            cari_special3 = string.Empty,
                            cari_muh_kod1 = string.Empty,
                            cari_muh_kod2 = string.Empty,
                            cari_sicil_no = string.Empty,
                            cari_banka_tcmb_kod1 = string.Empty,
                            cari_banka_tcmb_subekod1 = string.Empty,
                            cari_banka_tcmb_ilkod1 = string.Empty,
                            cari_banka_hesapno1 = string.Empty,
                            cari_banka_swiftkodu1 = string.Empty,
                            cari_banka_tcmb_kod2 = string.Empty,
                            cari_banka_tcmb_subekod2 = string.Empty,
                            cari_banka_tcmb_ilkod2 = string.Empty,
                            cari_banka_hesapno2 = string.Empty,
                            cari_banka_swiftkodu2 = string.Empty,
                            cari_banka_tcmb_kod3 = string.Empty,
                            cari_banka_tcmb_subekod3 = string.Empty,
                            cari_banka_tcmb_ilkod3 = string.Empty,
                            cari_banka_hesapno3 = string.Empty,
                            cari_banka_swiftkodu3 = string.Empty,
                            cari_banka_tcmb_kod4 = string.Empty,
                            cari_banka_tcmb_subekod4 = string.Empty,
                            cari_banka_tcmb_ilkod4 = string.Empty,
                            cari_banka_hesapno4 = string.Empty,
                            cari_banka_swiftkodu4 = string.Empty,
                            cari_banka_tcmb_kod5 = string.Empty,
                            cari_banka_tcmb_subekod5 = string.Empty,
                            cari_banka_tcmb_ilkod5 = string.Empty,
                            cari_banka_hesapno5 = string.Empty,
                            cari_banka_swiftkodu5 = string.Empty,
                            cari_banka_tcmb_kod6 = string.Empty,
                            cari_banka_tcmb_subekod6 = string.Empty,
                            cari_banka_tcmb_ilkod6 = string.Empty,
                            cari_banka_hesapno6 = string.Empty,
                            cari_banka_swiftkodu6 = string.Empty,
                            cari_banka_tcmb_kod7 = string.Empty,
                            cari_banka_tcmb_subekod7 = string.Empty,
                            cari_banka_tcmb_ilkod7 = string.Empty,
                            cari_banka_hesapno7 = string.Empty,
                            cari_banka_swiftkodu7 = string.Empty,
                            cari_banka_tcmb_kod8 = string.Empty,
                            cari_banka_tcmb_subekod8 = string.Empty,
                            cari_banka_tcmb_ilkod8 = string.Empty,
                            cari_banka_hesapno8 = string.Empty,
                            cari_banka_swiftkodu8 = string.Empty,
                            cari_banka_tcmb_kod9 = string.Empty,
                            cari_banka_tcmb_subekod9 = string.Empty,
                            cari_banka_tcmb_ilkod9 = string.Empty,
                            cari_banka_hesapno9 = string.Empty,
                            cari_banka_swiftkodu9 = string.Empty,
                            cari_banka_tcmb_kod10 = string.Empty,
                            cari_banka_tcmb_subekod10 = string.Empty,
                            cari_banka_tcmb_ilkod10 = string.Empty,
                            cari_banka_hesapno10 = string.Empty,
                            cari_banka_swiftkodu10 = string.Empty,
                            cari_Ana_cari_kodu = string.Empty,
                            cari_satis_isk_kod = string.Empty,
                            cari_sektor_kodu = string.Empty,
                            cari_bolge_kodu = string.Empty,
                            cari_grup_kodu = string.Empty,
                            cari_temsilci_kodu = string.Empty,
                            cari_muhartikeli = string.Empty,
                            cari_wwwadresi = string.Empty,
                            cari_EMail = order.OrderInvoiceInformation.Mail,
                            cari_CepTel = string.Empty,
                            cari_Portal_PW = string.Empty,
                            cari_kampanyakodu = string.Empty,
                            cari_ufrs_fark_muh_kod = string.Empty,
                            cari_ufrs_fark_muh_kod1 = string.Empty,
                            cari_ufrs_fark_muh_kod2 = string.Empty,
                            cari_mutabakat_mail_adresi = string.Empty,
                            cari_mersis_no = string.Empty,
                            cari_VerilenDepozitoTeminatMuhKodu1 = string.Empty,
                            cari_VerilenDepozitoTeminatMuhKodu2 = string.Empty,
                            cari_AlinanDepozitoTeminatMuhKodu1 = string.Empty,
                            cari_AlinanDepozitoTeminatMuhKodu2 = string.Empty,
                            cari_KEP_adresi = string.Empty,
                            cari_istasyon_cari_kodu = string.Empty,
                            cari_efatura_xslt_dosya = string.Empty,
                            cari_Perakende_fl = false
                        };
                        mikroUnitOfWork.CARI_HESAPLARRepository.Create(cariHesaplar);
                        //if (mikroUnitOfWork.CARI_HESAPLARRepository.Create(cariHesaplar))
                        //{
                        //    response.Success = false;
                        //    response.Message = "Cari Hesaplar not inserted.";
                        //    mikroUnitOfWork.RollbackTransaction();
                        //    return;
                        //}
                    }
                    #endregion

                    #region Cari Hesap Adresleri
                    var address = GetAddress(order.OrderInvoiceInformation);

                    var adr_adress_number = mikroUnitOfWork.CARI_HESAP_ADRESLERIRepository.GetLastAdrAdresNo(cariKod);
                    adr_adress_number++;

                    //CARI_HESAP_ADRESLERI
                    var cariHesapAdresleri = new CARI_HESAP_ADRESLERI
                    {
                        adr_Guid = Guid.NewGuid(),
                        adr_create_user = 3,
                        adr_create_date = transactionDate,
                        adr_lastup_user = 3,
                        adr_lastup_date = transactionDate,
                        adr_cari_kod = cariKod,
                        adr_ulke = order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name,
                        adr_il = order.OrderInvoiceInformation.Neighborhood.District.City.Name,
                        adr_Semt = order.OrderInvoiceInformation.Neighborhood.District.Name,
                        adr_cadde = address.Lines[0],
                        adr_mahalle = "",//order.OrderInvoiceInformation.Neighborhood.Name,
                        adr_sokak = address.Lines[2],
                        adr_tel_no1 = order.OrderInvoiceInformation.Phone,
                        adr_adres_no = adr_adress_number
                    };

                    if (cariHesapAdresleri.adr_tel_no1.StartsWith("90"))
                        cariHesapAdresleri.adr_tel_no1 = cariHesapAdresleri.adr_tel_no1.TrimStart('9').TrimStart('0');

                    if (cariHesapAdresleri.adr_tel_no1.StartsWith("0"))
                        cariHesapAdresleri.adr_tel_no1 = cariHesapAdresleri.adr_tel_no1.TrimStart('0');

                    if (cariHesapAdresleri.adr_tel_no1.Length > 10)
                        cariHesapAdresleri.adr_tel_no1 = "";

                    mikroUnitOfWork.CARI_HESAP_ADRESLERIRepository.Create(cariHesapAdresleri);
                    //if (mikroUnitOfWork.CARI_HESAP_ADRESLERIRepository.Create(cariHesapAdresleri))
                    //{
                    //    response.Success = false;
                    //    response.Message = "Cari Hesap Adresleri not inserted.";
                    //    mikroUnitOfWork.RollbackTransaction();
                    //    return;
                    //}
                    #endregion

                    var chaEvraknoSira = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira(chaEvraknoSeri);
                    chaEvraknoSira++;

                    var sign = $"{accountCustomerPrefix}{order.Id}";
                    var isSign = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.ReadBySign(sign);
                    if (isSign)
                    {
                        response.Success = false;
                        response.Message = "Fatura daha önce kesilmiş.";

                        mikroUnitOfWork.RollbackTransaction();

                        return;
                    }

                    #region Brokerage
                    /*
                     * Sirkete ait sanal poslar icin cha_cari_cins degeri 2 olarak belirlenir. Bu durum muhasebesel olarak "Bankadan Kapanacak" statusune denk gelir. PayU gibi araci kurumlar icin bu durum gecerli degildir.
                     */
                    ResponsibilityCenter responsibilityCenter = null;
                    if (order.MarketplaceId != null)
                        responsibilityCenter = _responsibilityCenter.FirstOrDefault(x => x.Id == order.MarketplaceId);
                    else
                        responsibilityCenter = _responsibilityCenter.FirstOrDefault(x => x.Id == "MS");

                    byte cha_cari_cins = 0;
                    byte cha_tpoz = 0;
                    byte cha_grupno = 0;
                    string cha_kod = cariKod;
                    //if (!responsibilityCenter.IsBrokerage)
                    //{
                    //if (order.ResponsibilityCenter.PaymentTypeId == "OO" ||
                    //    order.ResponsibilityCenter.PaymentTypeId == "KKK")
                    //{
                    //    cha_cari_cins = 2;
                    //    cha_tpoz = 1;
                    //    cha_grupno = 1;
                    //    cha_kod = order.ResponsibilityCenter.AccountNumber;
                    //}
                    //else if (order.ResponsibilityCenter.PaymentTypeId == "KNO")
                    //{
                    //    cha_cari_cins = 4;
                    //    cha_tpoz = 1;
                    //    cha_kod = order.ResponsibilityCenter.AccountNumber;
                    //}
                    //}
                    #endregion

                    var chaekRelatedUid = Guid.NewGuid();

                    #region Abroad Invoice
                    byte cha_cinsi = 6;
                    byte cha_ticaret_turu = 0;
                    bool abroadInvoice = Convert.ToInt32(order.OrderInvoiceInformation.Neighborhood.District.City.Country.Id) > 1;
                    string cha_EXIMkodu = string.Empty;
                    if (abroadInvoice)
                    {
                        cha_cinsi = 29;
                        cha_ticaret_turu = 3;
                        cha_EXIMkodu = cariKod;

                        var currentAccountTransactionAppendix = new CARI_HESAP_HAREKETLERI_EK
                        {
                            chaek_Guid = Guid.NewGuid(),
                            chaek_fileid = 575,
                            chaek_create_user = 1,
                            chaek_create_date = DateTime.Now,
                            chaek_lastup_user = 1,
                            chaek_lastup_date = DateTime.Now,
                            chaek_special1 = string.Empty,
                            chaek_special2 = string.Empty,
                            chaek_special3 = string.Empty,
                            chaek_related_uid = chaekRelatedUid,
                            cha_Istisna1 = "301",
                            cha_Istisna2 = "301",
                            cha_Istisna3 = "301",
                            cha_Istisna4 = "301",
                            cha_Istisna5 = "301",
                            cha_Istisna6 = "301",
                            cha_Istisna7 = "301",
                            cha_Istisna8 = "301",
                            cha_Istisna9 = "301",
                            cha_Istisna10 = "301",
                            cha_ozel_matrah_kodu = string.Empty,
                            cha_yolcuberaber_kodu = string.Empty,
                            cha_yetkiliaracikurumkodu = string.Empty,
                            cha_EArsiv_unvani_ad = string.Empty,
                            cha_EArsiv_unvani_soyad = string.Empty,
                            cha_EArsiv_daire_adi = string.Empty,
                            cha_EArsiv_Vkn = string.Empty,
                            cha_EArsiv_ulke = string.Empty,
                            cha_EArsiv_Il = string.Empty,
                            cha_EArsiv_tel_ulke_kod = string.Empty,
                            cha_EArsiv_tel_bolge_kod = string.Empty,
                            cha_EArsiv_tel_no = string.Empty,
                            cha_EArsiv_mail = string.Empty,
                            cha_EArsiv_cadde = string.Empty,
                            cha_EArsiv_sokak = string.Empty,
                            cha_EArsiv_Ilce = string.Empty,
                            cha_EArsiv_Ceptel = string.Empty
                        };
                        mikroUnitOfWork.CARI_HESAP_HAREKETLERI_EKRepository.Create(currentAccountTransactionAppendix);
                        //if (mikroUnitOfWork.CARI_HESAP_HAREKETLERI_EKRepository.Create(currentAccountTransactionAppendix))
                        //{
                        //    response.Success = false;
                        //    response.Message = "Cari Hesap Adresleri EK not inserted.";
                        //    mikroUnitOfWork.RollbackTransaction();
                        //    return;
                        //}
                    }
                    #endregion

                    var cariHesapHareketleri = new CARI_HESAP_HAREKETLERI
                    {
                        cha_Guid = chaekRelatedUid,
                        cha_fileid = 51,
                        cha_create_date = DateTime.Now,
                        cha_lastup_date = DateTime.Now,
                        cha_special1 = string.Empty,
                        cha_special2 = string.Empty,
                        cha_special3 = string.Empty,
                        cha_evrak_tip = 63, //egk_evr_tip Evrak Aciklamalari Icin
                        cha_evrakno_seri = chaEvraknoSeri,
                        cha_evrakno_sira = chaEvraknoSira,
                        cha_tarihi = transactionDate,
                        cha_tpoz = cha_tpoz,
                        cha_belge_no = string.Empty,
                        cha_belge_tarih = transactionDate,
                        cha_aciklama = cariHesaplar.cari_unvan1,
                        cha_satici_kodu = string.Empty,
                        cha_EXIMkodu = cha_EXIMkodu,
                        cha_projekodu = responsibilityCenter.Name,
                        cha_yat_tes_kodu = string.Empty,
                        cha_cari_cins = cha_cari_cins,
                        cha_kod = cariKod,
                        cha_ciro_cari_kodu = cariKod,
                        cha_d_kur = 1,
                        cha_altd_kur = 5,
                        cha_grupno = cha_grupno,
                        cha_srmrkkodu = responsibilityCenter.Name,
                        cha_karsid_kur = 1,
                        cha_karsisrmrkkodu = string.Empty,
                        cha_fis_tarih = transactionDate,
                        cha_fis_sirano = 0,
                        cha_reftarihi = new DateTime(1899, 12, 30),
                        cha_vardiya_tarihi = new DateTime(1899, 12, 30),
                        cha_uuid = Guid.NewGuid().ToString(),
                        cha_adres_no = 1,
                        cha_ilk_belge_tarihi = new DateTime(1899, 12, 30),
                        cha_HareketGrupKodu1 = string.Empty,
                        cha_HareketGrupKodu2 = sign,
                        cha_HareketGrupKodu3 = string.Empty,
                        cha_kasa_hizkod = string.Empty,
                        cha_vade = cariHesaplar.cari_odemeplan_no,
                        cha_cinsi = cha_cinsi,
                        cha_ticaret_turu = cha_ticaret_turu,
                        cha_vergisiz_fl = abroadInvoice,
                        cha_oivergisiz_fl = abroadInvoice,
                        cha_otvvergisiz_fl = abroadInvoice
                    };
                    #region Stok Haraketleri

                    #region Get Price Infos
                    var priceInfos = new List<PriceInfo>();
                    foreach (var orderDetail in order.OrderDetails)
                    {
                        var priceInfo = mikroUnitOfWork.STOKLARRepository.ReadByStokKod(orderDetail.ProductInformation.StockCode);
                        if ((priceInfo.VatType == 4 && orderDetail.VatRate != 0.18M) || (priceInfo.VatType == 3 && orderDetail.VatRate != 0.08M))
                        {
                            response.Success = false;
                            response.Message = "Vergi oranı hatası.";

                            mikroUnitOfWork.RollbackTransaction();

                            return;
                        }
                        priceInfos.Add(priceInfo);

                    }
                    #endregion

                    //STOK_HAREKETLERI
                    var stokHareketleri = new List<STOK_HAREKETLERI>();
                    var sthRowNumber = 0;
                    foreach (var orderDetail in order.OrderDetails)
                    {
                        var priceInfo = priceInfos.First(pi => pi.StokKod == orderDetail.ProductInformation.StockCode);

                        var sip_amount = orderDetail.VatExcUnitPrice * orderDetail.Quantity;
                        var sip_vergi = (orderDetail.UnitPrice - orderDetail.VatExcUnitPrice) * orderDetail.Quantity;
                        byte sth_vergi_pntr = priceInfo.VatType;
                        byte sth_disticaret_turu = 0;

                        /*Yurt dışı siparişler için vergi hesaplanmıyor*/
                        if (abroadInvoice)
                        {
                            sth_disticaret_turu = 3;
                            sip_vergi = 0;
                            sth_vergi_pntr = 0;
                            sip_amount = orderDetail.UnitPrice * orderDetail.Quantity;

                        }

                        stokHareketleri.Add(new STOK_HAREKETLERI
                        {
                            sth_Guid = Guid.NewGuid(),
                            sth_DBCno = 0,
                            sth_SpecRECno = 0,
                            sth_iptal = false,
                            sth_fileid = 16,
                            sth_hidden = false,
                            sth_kilitli = false,
                            sth_degisti = false,
                            sth_checksum = 0,
                            sth_create_user = 3,
                            sth_create_date = DateTime.Now,
                            sth_lastup_user = 2,
                            sth_lastup_date = DateTime.Now,
                            sth_special1 = string.Empty,
                            sth_special2 = string.Empty,
                            sth_special3 = string.Empty,
                            sth_firmano = 0,
                            sth_subeno = 0,
                            sth_tarih = transactionDate,
                            sth_tip = 1,
                            sth_cins = 0,
                            sth_normal_iade = 0,
                            sth_evraktip = 4,
                            sth_evrakno_seri = chaEvraknoSeri,
                            sth_evrakno_sira = chaEvraknoSira,
                            sth_satirno = sthRowNumber,
                            sth_belge_no = string.Empty,
                            sth_belge_tarih = transactionDate,
                            sth_stok_kod = orderDetail.ProductInformation.StockCode,
                            sth_isk_mas1 = 0,
                            sth_isk_mas2 = 1,
                            sth_isk_mas3 = 1,
                            sth_isk_mas4 = 1,
                            sth_isk_mas5 = 1,
                            sth_isk_mas6 = 1,
                            sth_isk_mas7 = 1,
                            sth_isk_mas8 = 1,
                            sth_isk_mas9 = 1,
                            sth_isk_mas10 = 1,
                            sth_sat_iskmas1 = false,
                            sth_sat_iskmas2 = false,
                            sth_sat_iskmas3 = false,
                            sth_sat_iskmas4 = false,
                            sth_sat_iskmas5 = false,
                            sth_sat_iskmas6 = false,
                            sth_sat_iskmas7 = false,
                            sth_sat_iskmas8 = false,
                            sth_sat_iskmas9 = false,
                            sth_sat_iskmas10 = false,
                            sth_pos_satis = 0,
                            sth_promosyon_fl = false,
                            sth_cari_cinsi = 0,
                            sth_cari_kodu = cariKod,
                            sth_cari_grup_no = 0,
                            sth_isemri_gider_kodu = string.Empty,
                            sth_plasiyer_kodu = string.Empty,
                            sth_har_doviz_cinsi = 0,
                            sth_har_doviz_kuru = 1,
                            sth_alt_doviz_kuru = 5,
                            sth_stok_doviz_cinsi = 0,
                            sth_stok_doviz_kuru = 1,
                            sth_miktar = orderDetail.Quantity,
                            sth_miktar2 = orderDetail.Quantity,
                            sth_birim_pntr = 1,
                            sth_tutar = (double)orderDetail.VatExcListUnitPrice * orderDetail.Quantity,
                            sth_iskonto1 = (double)orderDetail.VatExcUnitDiscount * orderDetail.Quantity,
                            sth_iskonto2 = 0,
                            sth_iskonto3 = 0,
                            sth_iskonto4 = 0,
                            sth_iskonto5 = 0,
                            sth_iskonto6 = 0,
                            sth_masraf1 = 0,
                            sth_masraf2 = 0,
                            sth_masraf3 = 0,
                            sth_masraf4 = 0,
                            sth_vergi_pntr = sth_vergi_pntr,
                            sth_vergi = (double)sip_vergi,
                            sth_masraf_vergi_pntr = 0,
                            sth_masraf_vergi = 0,
                            sth_netagirlik = 0,
                            sth_aciklama = order.MarketplaceOrderNumber,
                            sth_fat_uid = cariHesapHareketleri.cha_Guid,
                            sth_giris_depo_no = int.Parse(entryWarehouseNo),
                            sth_cikis_depo_no = int.Parse(exitWarehouseNo),
                            sth_malkbl_sevk_tarihi = transactionDate,
                            sth_cari_srm_merkezi = responsibilityCenter.Name,
                            sth_stok_srm_merkezi = responsibilityCenter.Name,
                            sth_fis_tarihi = DateTime.Now,
                            sth_maliyet_ana = 0,
                            sth_maliyet_alternatif = 0,
                            sth_maliyet_orjinal = 0,
                            sth_parti_kodu = string.Empty,
                            sth_lot_no = 0,
                            sth_proje_kodu = responsibilityCenter.Name,
                            sth_exim_kodu = cha_EXIMkodu,
                            sth_otv_pntr = 0,
                            sth_otv_vergi = 0,
                            sth_brutagirlik = 0,
                            sth_otvtutari = 0,
                            sth_oiv_pntr = 0,
                            sth_oiv_vergi = 0,
                            sth_fiyat_liste_no = 1,
                            sth_oivtutari = 0,
                            sth_Tevkifat_turu = 0,
                            sth_nakliyedeposu = 0,
                            sth_nakliyedurumu = 0,
                            sth_taxfree_fl = false,
                            sth_ilave_edilecek_kdv = 0,
                            sth_ismerkezi_kodu = string.Empty,
                            sth_HareketGrupKodu1 = string.Empty,
                            sth_HareketGrupKodu2 = string.Empty,
                            sth_HareketGrupKodu3 = string.Empty,
                            sth_Olcu1 = 0,
                            sth_Olcu2 = 0,
                            sth_Olcu3 = 0,
                            sth_Olcu4 = 0,
                            sth_Olcu5 = 0,
                            sth_FormulMiktarNo = 0,
                            sth_FormulMiktar = 0,
                            sth_eirs_senaryo = 0,
                            sth_eirs_tipi = 0,
                            sth_odeme_op = cariHesaplar.cari_odemeplan_no,
                            sth_disticaret_turu = sth_disticaret_turu,
                            sth_vergisiz_fl = abroadInvoice,
                            sth_otvvergisiz_fl = abroadInvoice,
                            sth_oivvergisiz_fl = abroadInvoice
                        });
                        sthRowNumber++;
                    }

                    #endregion

                    /* %8 Kdv */
                    cariHesapHareketleri.cha_vergi3 = stokHareketleri.Where(st => st.sth_vergi_pntr == 3).Sum(sh => sh.sth_vergi);
                    /* %18 Kdv */
                    cariHesapHareketleri.cha_vergi4 = stokHareketleri.Where(st => st.sth_vergi_pntr == 4).Sum(sh => sh.sth_vergi);
                    cariHesapHareketleri.cha_aratoplam = stokHareketleri.Sum(sh => sh.sth_tutar);
                    cariHesapHareketleri.cha_meblag = stokHareketleri.Sum(sh => (sh.sth_tutar - sh.sth_iskonto1) + sh.sth_vergi);
                    cariHesapHareketleri.cha_ft_iskonto1 = stokHareketleri.Sum(sh => sh.sth_iskonto1);

                    #region Insert
                    mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(cariHesapHareketleri);
                    //if (mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(cariHesapHareketleri))
                    //{
                    //    response.Success = false;
                    //    response.Message = "Cari Hesap Hareketleri not inserted.";
                    //    mikroUnitOfWork.RollbackTransaction();
                    //    return;
                    //}


                    foreach (var stokTransaction in stokHareketleri)
                    {
                        stokTransaction.sth_fat_uid = cariHesapHareketleri.cha_Guid;

                        mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(stokTransaction);
                        //if (mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(stokTransaction))
                        //{
                        //    response.Success = false;
                        //    response.Message = "Stok hareketleri not inserted.";
                        //    mikroUnitOfWork.RollbackTransaction();
                        //    return;
                        //}
                    }
                    #endregion

                    #region Evrak Açıklamaları
                    //EVRAK_ACIKLAMALARI
                    var documentDescription = GetDocumentDescription(order.Total, responsibilityCenter, order.MarketplaceId,
                         order.Company);

                    var evrakAciklamalari = new EVRAK_ACIKLAMALARI
                    {
                        egk_Guid = Guid.NewGuid(),
                        egk_evr_tip = 63,
                        egk_fileid = 66,
                        egk_create_user = 1,
                        egk_create_date = transactionDate,
                        egk_lastup_user = 1,
                        egk_lastup_date = transactionDate,
                        egk_dosyano = 51,
                        egk_evr_seri = chaEvraknoSeri,
                        egk_evr_sira = chaEvraknoSira,
                        egk_tesaltarihi = new DateTime(1899, 12, 30),
                        egk_evracik1 = documentDescription.Lines[0],
                        egk_evracik2 = documentDescription.Lines[1],
                        egk_evracik3 = documentDescription.Lines[2],
                        egk_evracik4 = documentDescription.Lines[3],
                        egk_evracik5 = documentDescription.Lines[4],
                        egk_evracik6 = documentDescription.Lines[5],
                        egk_evracik7 = documentDescription.Lines[6],
                        egk_evracik8 = documentDescription.Lines[7],
                        egk_evracik9 = documentDescription.Lines[8],
                        egk_evracik10 = documentDescription.Lines[9],
                        egk_kargokodu = string.Empty,
                        egk_kargono = string.Empty,
                        egk_tesalkisi = string.Empty,
                        egk_special1 = string.Empty,
                        egk_special2 = string.Empty,
                        egk_special3 = string.Empty,
                        egk_evr_ustkod = string.Empty
                    };
                    mikroUnitOfWork.EVRAK_ACIKLAMALARIRepository.Create(evrakAciklamalari);
                    //if (mikroUnitOfWork.EVRAK_ACIKLAMALARIRepository.Create(evrakAciklamalari))
                    //{
                    //    response.Success = false;
                    //    response.Message = "Evrak acıklamaları not inserted.";
                    //    mikroUnitOfWork.RollbackTransaction();
                    //    return;
                    //}

                    #endregion

                    #region Current Account Transaction / Cari Hesap Hareketleri
                    /*
                     * Aracilik hizmeti yapilan durumlarda virman islemi gerceklestirilir.
                     */
                    if (responsibilityCenter.IsBrokerage)
                    {
                        var cha_document_number2 = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira("ZX", 33);
                        cha_document_number2++;

                        var zx_One_Cari_hesap_haraketleri = new CARI_HESAP_HAREKETLERI
                        {
                            cha_Guid = Guid.NewGuid(),
                            cha_cari_cins = 0,
                            cha_belge_no = sign,
                            cha_kasa_hizmet = 0,
                            cha_kasa_hizkod = String.Empty,
                            cha_evrakno_seri = "ZX",
                            cha_iptal = false,
                            cha_fileid = 51,
                            cha_hidden = false,
                            cha_kilitli = false,
                            cha_degisti = false,
                            cha_create_user = 1,
                            cha_create_date = transactionDate,
                            cha_lastup_user = 1,
                            cha_lastup_date = transactionDate,
                            cha_special1 = string.Empty,
                            cha_special2 = string.Empty,
                            cha_special3 = string.Empty,
                            cha_evrak_tip = 33,
                            cha_evrakno_sira = cha_document_number2,
                            cha_satir_no = 0,
                            cha_tarihi = transactionDate,
                            cha_cinsi = 5,
                            cha_belge_tarih = transactionDate,
                            cha_aciklama = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}".Length > 40 ? $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}".Substring(0, 40) : $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}",
                            cha_satici_kodu = string.Empty,
                            cha_EXIMkodu = string.Empty,
                            cha_projekodu = string.Empty,
                            cha_yat_tes_kodu = string.Empty,
                            cha_kod = responsibilityCenter.AccountNumber,
                            cha_ciro_cari_kodu = string.Empty,
                            cha_d_kur = 1,
                            cha_altd_kur = 5,
                            cha_srmrkkodu = responsibilityCenter.Name,
                            cha_karsid_kur = 1,
                            cha_karsisrmrkkodu = string.Empty,
                            cha_miktari = 0,
                            cha_meblag = Convert.ToDouble(order.Total),
                            cha_fis_tarih = new DateTime(1900, 01, 01),
                            cha_trefno = string.Empty,
                            cha_reftarihi = new DateTime(1900, 01, 01),
                            cha_vardiya_tarihi = new DateTime(1900, 01, 01),
                            cha_diger_belge_adi = string.Empty,
                            cha_uuid = string.Empty,
                            cha_ilk_belge_tarihi = new DateTime(1900, 01, 01),
                            cha_HareketGrupKodu1 = string.Empty,
                            cha_HareketGrupKodu2 = string.Empty,
                            cha_HareketGrupKodu3 = string.Empty,
                        };


                        mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(zx_One_Cari_hesap_haraketleri);
                        //if (mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(zx_One_Cari_hesap_haraketleri))
                        //{
                        //    response.Success = false;
                        //    response.Message = "Zx Cari Hesap Hareketleri not inserted.";
                        //    mikroUnitOfWork.RollbackTransaction();
                        //    return;
                        //}

                        var zx_Two_Cari_hesap_haraketleri = new CARI_HESAP_HAREKETLERI
                        {
                            cha_Guid = Guid.NewGuid(),
                            cha_belge_no = sign,
                            cha_kasa_hizmet = 0,
                            cha_kasa_hizkod = String.Empty,
                            cha_evrakno_seri = "ZX",
                            cha_iptal = false,
                            cha_fileid = 51,
                            cha_tip = 1,
                            cha_hidden = false,
                            cha_kilitli = false,
                            cha_degisti = false,
                            cha_create_user = 1,
                            cha_create_date = transactionDate,
                            cha_lastup_user = 1,
                            cha_lastup_date = transactionDate,
                            cha_special1 = string.Empty,
                            cha_special2 = string.Empty,
                            cha_special3 = string.Empty,
                            cha_evrak_tip = 33,
                            cha_evrakno_sira = cha_document_number2,
                            cha_satir_no = 1,
                            cha_tarihi = transactionDate,
                            cha_cinsi = 5,
                            cha_belge_tarih = transactionDate,
                            cha_aciklama = string.Empty,
                            cha_satici_kodu = string.Empty,
                            cha_EXIMkodu = string.Empty,
                            cha_projekodu = string.Empty,
                            cha_yat_tes_kodu = string.Empty,
                            cha_kod = cariKod,
                            cha_ciro_cari_kodu = string.Empty,
                            cha_d_kur = 1,
                            cha_altd_kur = 5,
                            cha_srmrkkodu = responsibilityCenter.Name,
                            cha_karsid_kur = 1,
                            cha_karsisrmrkkodu = string.Empty,
                            cha_miktari = 0,
                            cha_meblag = Convert.ToDouble(order.Total),
                            cha_fis_tarih = new DateTime(1900, 01, 01),
                            cha_trefno = string.Empty,
                            cha_reftarihi = new DateTime(1900, 01, 01),
                            cha_vardiya_tarihi = new DateTime(1900, 01, 01),
                            cha_diger_belge_adi = string.Empty,
                            cha_uuid = string.Empty,
                            cha_ilk_belge_tarihi = new DateTime(1900, 01, 01),
                            cha_HareketGrupKodu1 = string.Empty,
                            cha_HareketGrupKodu2 = string.Empty,
                            cha_HareketGrupKodu3 = string.Empty,
                        };
                        mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(zx_Two_Cari_hesap_haraketleri);
                        //if (mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(zx_Two_Cari_hesap_haraketleri))
                        //{
                        //    response.Success = false;
                        //    response.Message = "Zx Cari Hesap Hareketleri not inserted.";
                        //    mikroUnitOfWork.RollbackTransaction();
                        //    return;
                        //}
                    }
                    #endregion

                    mikroUnitOfWork.CommitTransaction();

                    response.Data = new AccountingInfo
                    {
                        AccountingCompanyId = "M",
                        AccountTransactionId = chaekRelatedUid,
                        Guid = $"{chaEvraknoSeri}{chaEvraknoSira}",
                        Url = null
                    };
                }

                response.Success = true;
            });
        }

        public DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds, Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }

        public DataResponse<AccountingCancel> CancelInvoice(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCancel>>((response) =>
            {
                var transactionDate = DateTime.Now;

                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                            ._unitOfWork
                            .AccountingCompanyConfigurationRepository
                            .DbSet()
                            .Where(x => x.AccountingCompany.AccountingId == "M")
                            .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                var entity = _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.OrderBilling)
                    .Include(o => o.Customer)
                    .Include(o => o.Company)
                    .Include(o => o.OrderReturnDetails)
                    .ThenInclude(x => x.ProductInformation)
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .FirstOrDefault(x => x.Id == orderId);

                var cariKod = $"120.{accountCustomerPrefix}.{entity.Customer.Id + 10000000}";
                var cariVergiDaireKodu = "998";
                var chaEvraknoSeri = eArchiveInvoiceSerie;

                if (!string.IsNullOrEmpty(entity.OrderInvoiceInformation.TaxNumber) &&
                    !string.IsNullOrEmpty(entity.OrderInvoiceInformation.TaxOffice))
                {
                    cariKod = $"120.{accountCompanyPrefix}.{entity.Customer.Id + 10000000}";
                    cariVergiDaireKodu = "999";
                    chaEvraknoSeri = eInvoiceInvoiceSerie;
                }

                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    mikroUnitOfWork.BeginTransaction();

                    var currentAccountTransaction = mikroUnitOfWork
                        .CARI_HESAP_HAREKETLERIRepository
                        .ReadBySeriSira(entity.OrderBilling.InvoiceNumber);
                    var stockTransactions = mikroUnitOfWork
                        .STOK_HAREKETLERIRepository
                        .ReadBySth_fat_uid(currentAccountTransaction.cha_Guid);
                    var cha_document_number = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira("A", 61) + 1;

                    var accountTransactionId = Guid.NewGuid();
                    currentAccountTransaction.cha_Guid = accountTransactionId;
                    currentAccountTransaction.cha_evrakno_seri = "A";
                    currentAccountTransaction.cha_evrak_tip = 61;
                    currentAccountTransaction.cha_cinsi = 26;
                    currentAccountTransaction.cha_tip = 1;
                    currentAccountTransaction.cha_fatura_belge_turu = 11;
                    currentAccountTransaction.cha_normal_Iade = 1;
                    currentAccountTransaction.cha_create_date = transactionDate;
                    currentAccountTransaction.cha_lastup_date = transactionDate;
                    currentAccountTransaction.cha_belge_tarih = transactionDate;
                    currentAccountTransaction.cha_tarihi = transactionDate;
                    currentAccountTransaction.cha_evrakno_sira = cha_document_number;
                    currentAccountTransaction.cha_diger_belge_adi = "Stok gider pusulası";
                    currentAccountTransaction.cha_create_user = 1;
                    currentAccountTransaction.cha_lastup_user = 1;
                    currentAccountTransaction.cha_kilitli = false;
                    currentAccountTransaction.cha_belge_no = string.Empty;
                    currentAccountTransaction.cha_fis_tarih = new DateTime(1899, 12, 30);
                    currentAccountTransaction.cha_fis_sirano = 0;

                    #region StockTransaction
                    //Tam Iade
                    if (stockTransactions.Where(x => x.sth_stok_kod != "STKARGO" && x.sth_stok_kod != "STOB").Sum(x => x.sth_miktar) == entity.OrderReturnDetails.Where(ord => ord.IsReturn).Sum(ord => ord.Quantity))
                    {
                        foreach (var stLoop in stockTransactions)
                        {
                            stLoop.sth_Guid = Guid.NewGuid();
                            stLoop.sth_create_date = transactionDate;
                            stLoop.sth_lastup_date = transactionDate;
                            stLoop.sth_tarih = transactionDate;
                            stLoop.sth_normal_iade = 1;
                            stLoop.sth_evraktip = 16;
                            stLoop.sth_evrakno_seri = "A";
                            stLoop.sth_aciklama = $"Stok urun iade {accountCompanyPrefix}{entity.Id}";
                            stLoop.sth_evrakno_sira = cha_document_number;
                            stLoop.sth_belge_tarih = transactionDate;
                            stLoop.sth_fat_uid = currentAccountTransaction.cha_Guid;
                            stLoop.sth_fis_tarihi = new DateTime(1899, 12, 30);
                            stLoop.sth_belge_no = string.Empty;
                            stLoop.sth_fis_sirano = 0;
                            stLoop.sth_kilitli = false;
                            stLoop.sth_cins = 17;
                            stLoop.sth_tip = 0;
                            stLoop.sth_giris_depo_no = int.Parse(entryWarehouseNo);
                            stLoop.sth_cikis_depo_no = int.Parse(entryWarehouseNo);

                            if (!Create(() => mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(stLoop), response, "Stok Hareketleri", mikroUnitOfWork))
                                return;
                        }
                    }
                    else //Parçalı İade
                    {
                        #region Stoks Transactions
                        var sth_iskonto1 = 0d;
                        var sth_iskonto2 = 0d;
                        var sth_iskonto3 = 0d;
                        var sth_iskonto4 = 0d;
                        var sth_iskonto5 = 0d;
                        var sth_iskonto6 = 0d;

                        var sth_vergi1 = 0d;
                        var sth_vergi2 = 0d;
                        var sth_vergi3 = 0d;
                        var sth_vergi4 = 0d;
                        var sth_vergi5 = 0d;
                        var sth_vergi6 = 0d;
                        var sth_vergi7 = 0d;
                        var sth_vergi8 = 0d;
                        var sth_vergi9 = 0d;
                        var sth_vergi10 = 0d;

                        var sth_tutar = 0d;

                        foreach (var ordLoop in entity.OrderReturnDetails)
                        {
                            var stockTransaction = stockTransactions
                                .FirstOrDefault(x => x.sth_stok_kod == ordLoop.ProductInformation.StockCode);

                            if (stockTransaction == null) continue;

                            var percent = ordLoop.Quantity / stockTransaction.sth_miktar;

                            stockTransaction.sth_miktar = ordLoop.Quantity;
                            stockTransaction.sth_Guid = Guid.NewGuid();
                            stockTransaction.sth_create_date = transactionDate;
                            stockTransaction.sth_lastup_date = transactionDate;
                            stockTransaction.sth_tarih = transactionDate;
                            stockTransaction.sth_normal_iade = 1;
                            stockTransaction.sth_evraktip = 16;
                            stockTransaction.sth_evrakno_seri = "A";
                            stockTransaction.sth_aciklama = $"Stok urun iade {accountCompanyPrefix}{entity.Id}";
                            stockTransaction.sth_evrakno_sira = cha_document_number;
                            stockTransaction.sth_belge_tarih = transactionDate;
                            stockTransaction.sth_fat_uid = currentAccountTransaction.cha_Guid;
                            stockTransaction.sth_fis_tarihi = new DateTime(1899, 12, 30);
                            stockTransaction.sth_belge_no = string.Empty;
                            stockTransaction.sth_fis_sirano = 0;
                            stockTransaction.sth_kilitli = false;
                            stockTransaction.sth_cins = 17;
                            stockTransaction.sth_tip = 0;
                            stockTransaction.sth_satirno = entity.OrderReturnDetails.IndexOf(ordLoop);

                            stockTransaction.sth_iskonto1 = stockTransaction.sth_iskonto1 * percent;
                            stockTransaction.sth_iskonto2 = stockTransaction.sth_iskonto2 * percent;
                            stockTransaction.sth_iskonto3 = stockTransaction.sth_iskonto3 * percent;
                            stockTransaction.sth_iskonto4 = stockTransaction.sth_iskonto4 * percent;
                            stockTransaction.sth_iskonto5 = stockTransaction.sth_iskonto5 * percent;
                            stockTransaction.sth_iskonto6 = stockTransaction.sth_iskonto6 * percent;
                            stockTransaction.sth_vergi = stockTransaction.sth_vergi * percent;
                            stockTransaction.sth_tutar = stockTransaction.sth_tutar * percent;
                            stockTransaction.sth_giris_depo_no = Convert.ToInt32(entryWarehouseNo);
                            stockTransaction.sth_cikis_depo_no = Convert.ToInt32(entryWarehouseNo);

                            if (ordLoop.IsLoss)
                            {
                                stockTransaction.sth_giris_depo_no = 9997;
                                stockTransaction.sth_cikis_depo_no = 9997;
                            }

                            if (!Create(() => mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(stockTransaction), response, "Stok Hareketleri", mikroUnitOfWork))
                                return;

                            #region sth_Iskonto
                            sth_iskonto1 += stockTransaction.sth_iskonto1;
                            sth_iskonto2 += stockTransaction.sth_iskonto2;
                            sth_iskonto3 += stockTransaction.sth_iskonto3;
                            sth_iskonto4 += stockTransaction.sth_iskonto4;
                            sth_iskonto5 += stockTransaction.sth_iskonto5;
                            sth_iskonto6 += stockTransaction.sth_iskonto6;
                            #endregion

                            #region sth_vergi_pntr                              
                            switch (stockTransaction.sth_vergi_pntr)
                            {
                                case 1:
                                    sth_vergi1 += stockTransaction.sth_vergi;
                                    break;
                                case 2:
                                    sth_vergi2 += stockTransaction.sth_vergi;
                                    break;
                                case 3:
                                    sth_vergi3 += stockTransaction.sth_vergi;
                                    break;
                                case 4:
                                    sth_vergi4 += stockTransaction.sth_vergi;
                                    break;
                                case 5:
                                    sth_vergi5 += stockTransaction.sth_vergi;
                                    break;
                                case 6:
                                    sth_vergi6 += stockTransaction.sth_vergi;
                                    break;
                                case 7:
                                    sth_vergi7 += stockTransaction.sth_vergi;
                                    break;
                                case 8:
                                    sth_vergi8 += stockTransaction.sth_vergi;
                                    break;
                                case 9:
                                    sth_vergi9 += stockTransaction.sth_vergi;
                                    break;
                                case 10:
                                    sth_vergi10 += stockTransaction.sth_vergi;
                                    break;
                            }
                            #endregion

                            #region sth_tutar
                            sth_tutar += stockTransaction.sth_tutar;
                            #endregion
                        }

                        #region cha_ft_iskonto
                        currentAccountTransaction.cha_ft_iskonto1 = sth_iskonto1;
                        currentAccountTransaction.cha_ft_iskonto2 = sth_iskonto2;
                        currentAccountTransaction.cha_ft_iskonto3 = sth_iskonto3;
                        currentAccountTransaction.cha_ft_iskonto4 = sth_iskonto4;
                        currentAccountTransaction.cha_ft_iskonto5 = sth_iskonto5;
                        currentAccountTransaction.cha_ft_iskonto6 = sth_iskonto6;
                        #endregion

                        #region cha_vergi
                        currentAccountTransaction.cha_vergi1 = sth_vergi1;
                        currentAccountTransaction.cha_vergi2 = sth_vergi2;
                        currentAccountTransaction.cha_vergi3 = sth_vergi3;
                        currentAccountTransaction.cha_vergi4 = sth_vergi4;
                        currentAccountTransaction.cha_vergi5 = sth_vergi5;
                        currentAccountTransaction.cha_vergi6 = sth_vergi6;
                        currentAccountTransaction.cha_vergi7 = sth_vergi7;
                        currentAccountTransaction.cha_vergi8 = sth_vergi8;
                        currentAccountTransaction.cha_vergi9 = sth_vergi9;
                        currentAccountTransaction.cha_vergi10 = sth_vergi10;
                        #endregion

                        #region total
                        var sth_vergi = sth_vergi1 + sth_vergi2 + sth_vergi3 + sth_vergi4 + sth_vergi5 + sth_vergi6 + sth_vergi7 + sth_vergi8 + sth_vergi9 + sth_vergi10;
                        var sth_iskonto = sth_iskonto1 + sth_iskonto2 + sth_iskonto3 + sth_iskonto4 + sth_iskonto5 + sth_iskonto6;
                        currentAccountTransaction.cha_aratoplam = sth_tutar;
                        currentAccountTransaction.cha_meblag = sth_tutar - sth_iskonto + sth_vergi;
                        #endregion

                        #endregion
                    }
                    #endregion

                    #region Current Account Transaction / Cari Hesap Hareketleri
                    if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(currentAccountTransaction), response, "Cari Hesap Hareketleri", mikroUnitOfWork))
                        return;
                    #endregion

                    #region Current Account Transaction ZX Insert
                    var currentAccountTransaction2 = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Read("ZX", 33, $"{accountCustomerPrefix}{orderId}");

                    if (currentAccountTransaction2.Count > 0)
                    {
                        var cha_document_number2 = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira("ZX", 33);
                        cha_document_number2++;

                        foreach (var cLopp in currentAccountTransaction2)
                        {
                            cLopp.cha_Guid = Guid.NewGuid();
                            cLopp.cha_meblag = currentAccountTransaction.cha_meblag;
                            cLopp.cha_evrakno_sira = cha_document_number2;
                            cLopp.cha_tip = (byte)(cLopp.cha_tip == 1 ? 0 : 1);
                            cLopp.cha_create_date = transactionDate;
                            cLopp.cha_lastup_date = transactionDate;
                            cLopp.cha_belge_tarih = transactionDate;
                            cLopp.cha_tarihi = transactionDate;
                            if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(cLopp), response, "Cari Hesap Hareketleri ZX", mikroUnitOfWork))
                                return;
                        }
                    }
                    #endregion

                    mikroUnitOfWork.CommitTransaction();

                    response.Data = new AccountingCancel
                    {
                        DocumentNo = cha_document_number.ToString()
                    };
                }

                response.Success = true;
            });
        }

        public DataResponse<string> CreateStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == "M")
                    .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                var order = _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.OrderDetails)
                        .ThenInclude(x => x.ProductInformation)
                    .FirstOrDefault(x => x.Id == orderId);

                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    var sth_evrakno_sira = mikroUnitOfWork.STOK_HAREKETLERIRepository.LastStockTransferEvrakNoSira();
                    sth_evrakno_sira++;

                    var orderDetails = order.OrderDetails.GroupBy(od => od.ProductInformationId).Select(od => new Application.Client.DataTransferObjects.OrderDetail
                    {
                        ProductInformationId = od.Key,
                        Quantity = od.Sum(x => x.Quantity),
                        VatRate = od.First().VatRate,
                        ProductInformation = new Application.Client.DataTransferObjects.ProductInformation
                        {
                            StockCode = od.First().ProductInformation.StockCode
                        }
                    }).ToList();

                    var sth_satirno = 0;
                    foreach (var odLoop in orderDetails)
                    {
                        mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(new STOK_HAREKETLERI
                        {
                            sth_Guid = Guid.NewGuid(),
                            sth_DBCno = 0,
                            sth_SpecRECno = 0,
                            sth_iptal = false,
                            sth_fileid = 16,
                            sth_hidden = false,
                            sth_kilitli = false,
                            sth_degisti = false,
                            sth_checksum = 0,
                            sth_create_user = 3,
                            sth_create_date = DateTime.Now,
                            sth_lastup_user = 2,
                            sth_lastup_date = DateTime.Now,
                            sth_special1 = "SST", /* System Stock Transfer */
                            sth_special2 = string.Empty,
                            sth_special3 = string.Empty,
                            sth_firmano = 0,
                            sth_subeno = 0,
                            sth_tarih = DateTime.Now.Date,
                            sth_tip = 2,
                            sth_cins = 6,
                            sth_normal_iade = 0,
                            sth_evraktip = 2,
                            sth_evrakno_seri = String.Empty,
                            sth_evrakno_sira = sth_evrakno_sira,
                            sth_satirno = sth_satirno,
                            sth_belge_no = string.Empty,
                            sth_belge_tarih = DateTime.Now.Date,
                            sth_stok_kod = odLoop.ProductInformation.StockCode,
                            sth_isk_mas1 = 0,
                            sth_isk_mas2 = 1,
                            sth_isk_mas3 = 1,
                            sth_isk_mas4 = 1,
                            sth_isk_mas5 = 1,
                            sth_isk_mas6 = 1,
                            sth_isk_mas7 = 1,
                            sth_isk_mas8 = 1,
                            sth_isk_mas9 = 1,
                            sth_isk_mas10 = 1,
                            sth_sat_iskmas1 = false,
                            sth_sat_iskmas2 = false,
                            sth_sat_iskmas3 = false,
                            sth_sat_iskmas4 = false,
                            sth_sat_iskmas5 = false,
                            sth_sat_iskmas6 = false,
                            sth_sat_iskmas7 = false,
                            sth_sat_iskmas8 = false,
                            sth_sat_iskmas9 = false,
                            sth_sat_iskmas10 = false,
                            sth_pos_satis = 0,
                            sth_promosyon_fl = false,
                            sth_cari_cinsi = 0,
                            sth_cari_kodu = String.Empty,
                            sth_cari_grup_no = 0,
                            sth_isemri_gider_kodu = string.Empty,
                            sth_plasiyer_kodu = string.Empty,
                            sth_har_doviz_cinsi = 0,
                            sth_har_doviz_kuru = 1,
                            sth_alt_doviz_kuru = 5,
                            sth_stok_doviz_cinsi = 0,
                            sth_stok_doviz_kuru = 1,
                            sth_miktar = (double)odLoop.Quantity,
                            sth_miktar2 = (double)odLoop.Quantity,
                            sth_birim_pntr = 1,
                            sth_tutar = 0,
                            sth_iskonto1 = 0,
                            sth_iskonto2 = 0,
                            sth_iskonto3 = 0,
                            sth_iskonto4 = 0,
                            sth_iskonto5 = 0,
                            sth_iskonto6 = 0,
                            sth_masraf1 = 0,
                            sth_masraf2 = 0,
                            sth_masraf3 = 0,
                            sth_masraf4 = 0,
                            sth_vergi_pntr = 4,
                            sth_vergi = 0,
                            sth_masraf_vergi_pntr = 0,
                            sth_masraf_vergi = 0,
                            sth_netagirlik = 0,
                            sth_aciklama = order.Id.ToString(),
                            sth_fat_uid = Guid.Empty,
                            sth_giris_depo_no = 6,
                            sth_cikis_depo_no = int.Parse(entryWarehouseNo),
                            sth_malkbl_sevk_tarihi = DateTime.Now.Date,
                            sth_cari_srm_merkezi = String.Empty,
                            sth_stok_srm_merkezi = String.Empty,
                            sth_fis_tarihi = DateTime.Now,
                            sth_maliyet_ana = 0,
                            sth_maliyet_alternatif = 0,
                            sth_maliyet_orjinal = 0,
                            sth_parti_kodu = string.Empty,
                            sth_lot_no = 0,
                            sth_proje_kodu = String.Empty,
                            sth_exim_kodu = String.Empty,
                            sth_otv_pntr = 0,
                            sth_otv_vergi = 0,
                            sth_brutagirlik = 0,
                            sth_otvtutari = 0,
                            sth_oiv_pntr = 0,
                            sth_oiv_vergi = 0,
                            sth_fiyat_liste_no = 1,
                            sth_oivtutari = 0,
                            sth_Tevkifat_turu = 0,
                            sth_nakliyedeposu = 0,
                            sth_nakliyedurumu = 0,
                            sth_taxfree_fl = false,
                            sth_ilave_edilecek_kdv = 0,
                            sth_ismerkezi_kodu = string.Empty,
                            sth_HareketGrupKodu1 = string.Empty,
                            sth_HareketGrupKodu2 = string.Empty,
                            sth_HareketGrupKodu3 = string.Empty,
                            sth_Olcu1 = 0,
                            sth_Olcu2 = 0,
                            sth_Olcu3 = 0,
                            sth_Olcu4 = 0,
                            sth_Olcu5 = 0,
                            sth_FormulMiktarNo = 0,
                            sth_FormulMiktar = 0,
                            sth_eirs_senaryo = 0,
                            sth_eirs_tipi = 0,
                            sth_odeme_op = 0,
                            sth_disticaret_turu = 0,
                            sth_vergisiz_fl = false,
                            sth_otvvergisiz_fl = false,
                            sth_oivvergisiz_fl = false
                        });
                        sth_satirno++;
                    }
                };

                response.Success = true;
            });
        }

        public DataResponse<List<int>> ReadStockTransfers()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<int>>>((response) =>
            {
                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == "M")
                    .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                List<int> ints = null;
                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    ints = mikroUnitOfWork.STOK_HAREKETLERIRepository.WaitingStockTransfers();
                }

                response.Data = ints;
                response.Success = true;
            });
        }

        public Response DeleteStockTransfer(int orderId, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == "M")
                    .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    var affectedRows = mikroUnitOfWork.STOK_HAREKETLERIRepository.RemoveStockTransfer(orderId);
                    response.Success = affectedRows > 0;
                }
            });
        }

        public DataResponse<int> ReadLastStockTransfer()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == "M")
                    .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                int orderId = 0;
                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    orderId = mikroUnitOfWork.STOK_HAREKETLERIRepository.LastStockTransferOrderId();
                };

                response.Success = true;
                response.Data = orderId;
            });
        }

        public DataResponse<int> ReadStock(string stockCode, string warehouseId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Read Accounting Company Configurations
                var accountingCompanyConfigurations = this
                            ._unitOfWork
                            .AccountingCompanyConfigurationRepository
                            .DbSet()
                            .Where(x => x.AccountingCompany.AccountingId == "M")
                            .ToList();

                var connectionString = accountingCompanyConfigurations.First(x => x.Key == "ConnectionString").Value;
                var accountCustomerPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCustomerPrefix").Value;
                var accountCompanyPrefix = accountingCompanyConfigurations.First(x => x.Key == "AccountCompanyPrefix").Value;
                var eArchiveInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EArchiveInvoiceSerie").Value;
                var eInvoiceInvoiceSerie = accountingCompanyConfigurations.First(x => x.Key == "EInvoiceInvoiceSerie").Value;
                var entryWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "EntryWarehouseNo").Value;
                var exitWarehouseNo = accountingCompanyConfigurations.First(x => x.Key == "ExitWarehouseNo").Value;
                #endregion

                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(connectionString))
                {
                    response.Data = mikroUnitOfWork.STOKLARRepository.StockMovenmentsCount(stockCode, warehouseId);
                }
            });
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Adrese göre tc kimlik belirleyen method
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private TaxInfo GetTaxInfo(ECommerce.Domain.Entities.Order order)
        {
            if (order.OrderInvoiceInformation.Address != null && order.OrderInvoiceInformation.Neighborhood.District.City.Country.Id != 1)
                return new TaxInfo
                {
                    Number = "2222222222",
                    OfficeName = "NİHAİ TÜKETİCİ",
                    OfficeCode = "998"
                };

            if (!string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) && !string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxOffice))
                return new TaxInfo
                {
                    Number = order.OrderInvoiceInformation.TaxNumber,
                    OfficeName = order.OrderInvoiceInformation.TaxOffice,
                    OfficeCode = string.Empty
                };

            return new TaxInfo
            {
                Number = "33333333333",
                OfficeName = "NİHAİ TÜKETİCİ",
                OfficeCode = "999"
            };
        }

        private Address GetAddress(OrderInvoiceInformation invoiceAddress)
        {
            invoiceAddress.Address = invoiceAddress.Address.Trim().Replace("\n", "");

            if (invoiceAddress.Address.Length > 175)
                invoiceAddress.Address = invoiceAddress.Address.Substring(0, 175);

            var address = new Address
            {
                Lines = new string[]
                {
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty
                }
            };

            int take = 40;
            for (int i = 0; i < 4; i++)
            {
                if (i == 3)
                    take = 25;

                address.Lines[i] = new String(invoiceAddress.Address.Skip(i * 40).Take(take).ToArray());
            }

            return address;
        }

        public static EvrakAciklamalari GetDocumentDescription(decimal amount, ResponsibilityCenter responsibilityCenter, string marketPlaceId, ECommerce.Domain.Entities.Company company)
        {
            var evrakAciklamalari = new EvrakAciklamalari
            {
                Lines = new string[]
                {
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty,
                    String.Empty
                }
            };
            var lineIndex = 0;

            if (amount == 0.001M)
            {
                evrakAciklamalari.Lines[lineIndex] = "NOT: ÜRÜN HEDİYE VE PROMOSYON AMAÇLI GÖNDERİLMİŞTİR.";
                lineIndex++;
            }

            if (!string.IsNullOrEmpty(responsibilityCenter.DocumentDescription))
            {
                if (responsibilityCenter.DocumentDescription.Length > 127)
                {
#warning Loop
                    evrakAciklamalari.Lines[lineIndex] = responsibilityCenter.DocumentDescription.Replace("#FullName#", company.FullName).Substring(0, 127);
                    lineIndex++;
                    evrakAciklamalari.Lines[lineIndex] = responsibilityCenter.DocumentDescription.Replace("#FullName#", company.FullName).Substring(127, responsibilityCenter.DocumentDescription.Replace("#FullName#", company.FullName).Length - 127);
                    lineIndex++;
                }
                else
                {
                    evrakAciklamalari.Lines[lineIndex] = responsibilityCenter.DocumentDescription.Replace("#FullName#", company.FullName);
                    lineIndex++;
                }
            }

            if (!string.IsNullOrEmpty(marketPlaceId))
            {
                switch (marketPlaceId)
                {
                    case "GG":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.gittigidiyor.com sitesi üzerinden yapılmıştır.";
                        break;
                    case "HB":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.hepsiburada.com sitesi üzerinden yapılmıştır.";
                        break;
                    case "MP":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.morhipo.com sitesi üzerinden yapılmıştır.";
                        break;
                    case "N11":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.n11.com sitesi üzerinden yapılmıştır.";
                        break;
                    case "TY":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.trendyol.com sitesi üzerinden yapılmıştır.";
                        break;
                    case "CS":
                        evrakAciklamalari.Lines[lineIndex] = "Bu satış www.ciceksepeti.com sitesi üzerinden yapılmıştır.";
                        break;
                }
            }

            return evrakAciklamalari;
        }

        public bool Create(Func<bool> func, ResponseBase resultBase, string entityName, UnitOfWork.UnitOfWork unitOfWork)
        {
            var result = func();
            if (!result)
            {
                resultBase.Success = false;
                resultBase.Message = $"{entityName} kaydedilemedi.";

                unitOfWork.RollbackTransaction();
            }
            return result;
        }

        public DataResponse<bool> CreateStocktaking(int stocktakingId, Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}