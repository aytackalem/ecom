using System;
using System.Linq;
using System.Collections.Generic;
using ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects;
using Dapper;
using System.Data;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories
{
    public partial class STOKLARRepository : RepositoryBase<STOKLAR, String>, ISTOKLARRepository
    {
        #region Constructors
        public STOKLARRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Methods
        public PriceInfo ReadByStokKod(string stokKod)
        {
            var sql = $@"
WITH T AS (
	SELECT	'{stokKod}' AS StokKod,
            (SELECT sto_isim FROM [STOKLAR] WHERE sto_kod = '{stokKod}') AS [Name], 
			(SELECT sfiyat_fiyati FROM [STOK_SATIS_FIYAT_LISTELERI] WHERE sfiyat_stokkod = '{stokKod}') AS TaxExcludedUnitPrice, 
			(SELECT sfiyat_fiyati * 
                CASE (SELECT sto_perakende_vergi FROM [STOKLAR] WHERE sto_kod = '{stokKod}')
                    WHEN 3
                    THEN 0.08
                    ELSE 0.18 END 
                FROM [STOK_SATIS_FIYAT_LISTELERI] WHERE sfiyat_stokkod = '{stokKod}') AS Vat,
            (SELECT sto_perakende_vergi FROM [STOKLAR] WHERE sto_kod = '{stokKod}') AS VatType
)
SELECT T.*, CEILING(T.TaxExcludedUnitPrice + T.Vat) AS TaxIncludedUnitPrice FROM T";

            return QueryFirstOrDefault<PriceInfo>(sql: sql);
        }

        public int StockMovenmentsCount(string stokKod, string depokod)
        {
            return _dbConnection.QueryFirstOrDefault<int>(sql: $"Select dbo.fn_DepolardakiMiktar(@sto_kod,@depo_kod,getdate())", transaction: _dbTransaction, param: new { sto_kod = stokKod, depo_kod = depokod });
        }

        public int StockMovenmentsCountV2(string stokKod, string depokod)
        {
            return _dbConnection.QueryFirstOrDefault<int>(sql: $"Select	dbo.fn_DepodakiMiktar(@sto_kod,@depo_kod,getdate())", transaction: _dbTransaction, param: new { sto_kod = stokKod, depo_kod = depokod });
        }

        public List<Stock> ReadBarcodes()
        {
            return _dbConnection.Query<Stock>(sql:
  $@"Select      ST.sto_Guid,
                 ST.sto_kod,
                 ST.sto_isim,
                 MIN(BAR.bar_kodu) as bar_kodu
     From        [STOKLAR] ST With(NoLock)
     Inner join  [BARKOD_TANIMLARI] BAR With(NoLock)  ON ST.sto_kod = bar.bar_stokkodu
     Group by ST.sto_Guid,ST.sto_kod,ST.sto_isim
     ", transaction: _dbTransaction).ToList();

       
        }

        public List<Stock> ReadBarcodesV2()
        {
            return _dbConnection.Query<Stock>(sql:
  $@"Select      ST.sto_Guid,
                 ST.sto_kod,
                 ST.sto_isim,
                 BAR.bar_kodu as bar_kodu
     From        [STOKLAR] ST With(NoLock)
     Inner join  [BARKOD_TANIMLARI] BAR With(NoLock)  ON ST.sto_kod = bar.bar_stokkodu
     Where sto_kod='ST9211'
     ", transaction: _dbTransaction).ToList();


       

        }

        public List<STOKLAR> Read()
        {
            return _dbConnection.Query<STOKLAR>(sql: $"Select * From [STOKLAR] With(NoLock)", transaction: _dbTransaction).ToList();
        }

        public STOKLAR Read(string sto_kod)
        {
            return _dbConnection.QueryFirstOrDefault<STOKLAR>(sql: $"Select * From [STOKLAR] With(NoLock) Where [sto_kod] = @sto_kod", transaction: _dbTransaction, param: new { sto_kod = sto_kod });
        }
        #endregion
    }
}