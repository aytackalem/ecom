using Dapper;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories
{
    public partial class STOK_HAREKETLERIRepository : RepositoryBase<STOK_HAREKETLERI, Guid>, ISTOK_HAREKETLERIRepository
    {
        #region Constructors
        public STOK_HAREKETLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Methods
        public List<STOK_HAREKETLERI> Read()
        {
            return _dbConnection.Query<STOK_HAREKETLERI>(sql: $"Select * From [STOK_HAREKETLERI] With(NoLock)", transaction: _dbTransaction).ToList();
        }

        public STOK_HAREKETLERI Read(Guid sth_Guid)
        {
            return _dbConnection.QueryFirstOrDefault<STOK_HAREKETLERI>(sql: $"Select * From [STOK_HAREKETLERI] With(NoLock) Where [sth_Guid] = @sth_Guid", transaction: _dbTransaction, param: new { sth_Guid = sth_Guid });
        }

        public int LastStockTransferEvrakNoSira()
        {
            var obj = _dbConnection.ExecuteScalar<int?>(sql: $"Select Max(sth_evrakno_sira) From STOK_HAREKETLERI Where sth_tip = 2 And sth_cins = 6 And sth_evraktip = 2", transaction: _dbTransaction);
            return obj == null || obj.ToString() == "" ? 0 : Convert.ToInt32(obj);
        }

        public int LastStockTransferOrderId()
        {
            var obj = _dbConnection.ExecuteScalar(sql: $"Select Max(Parse(sth_aciklama As Int)) From STOK_HAREKETLERI Where sth_tip = 2 And sth_cins = 6 And sth_evraktip = 2 And sth_cikis_depo_no = 1 And sth_giris_depo_no = 6 And sth_special1 = 'SST'", transaction: _dbTransaction);

            return obj == null || obj.ToString() == "" ? 0 : Convert.ToInt32(obj);
        }

        public int RemoveStockTransfer(int orderId)
        {
            return _dbConnection.Execute(sql: $"Delete From STOK_HAREKETLERI Where sth_tip = 2 And sth_cins = 6 And sth_evraktip = 2 And sth_cikis_depo_no = 1 And sth_giris_depo_no = 6 And sth_special1 = 'SST' And sth_aciklama = @sth_aciklama", transaction: _dbTransaction, param: new { sth_aciklama = orderId.ToString() });
        }

        public List<int> WaitingStockTransfers()
        {
            return _dbConnection.Query<int>(sql: @"
Select		sth_aciklama 
From		STOK_HAREKETLERI With(NoLock)
Where		sth_tip = 2 
			And sth_cins = 6 
			And sth_evraktip = 2 
			And sth_cikis_depo_no = 1 
			And sth_giris_depo_no = 6 
			And sth_special1 = 'SST'
Group By	sth_aciklama
Order By	sth_aciklama").ToList();
        }

        public List<STOK_HAREKETLERI> ReadBySth_fat_uid(Guid sth_fat_uid)
        {
            return _dbConnection.Query<STOK_HAREKETLERI>(sql: $"Select * From [STOK_HAREKETLERI] With(NoLock) Where [sth_fat_uid] = @sth_fat_uid", transaction: _dbTransaction, param: new { sth_fat_uid = sth_fat_uid }).ToList();
        }
        #endregion
    }
}