using Dapper;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories
{
    public partial class CARI_HESAP_ADRESLERIRepository : RepositoryBase<CARI_HESAP_ADRESLERI, Guid>, ICARI_HESAP_ADRESLERIRepository
    {
        #region Constructor
        public CARI_HESAP_ADRESLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }
        #endregion

        #region Method
        public List<CARI_HESAP_ADRESLERI> Read()
        {
            return _dbConnection.Query<CARI_HESAP_ADRESLERI>(sql: $"Select * From [CARI_HESAP_ADRESLERI] With(NoLock)", transaction: _dbTransaction).ToList();
        }

        public CARI_HESAP_ADRESLERI Read(Guid adr_Guid)
        {
            return _dbConnection.QueryFirstOrDefault<CARI_HESAP_ADRESLERI>(sql: $"Select * From [CARI_HESAP_ADRESLERI] With(NoLock) Where [adr_Guid] = @adr_Guid", transaction: _dbTransaction, param: new { adr_Guid = adr_Guid });
        }

        public int GetLastAdrAdresNo(string adrCariKod)
        {
            return QueryFirstOrDefault<int>(sql: $"Select IsNull(Max([adr_adres_no]), 0) From [CARI_HESAP_ADRESLERI] Where [adr_cari_kod] = @adr_cari_kod", param: new { adr_cari_kod = adrCariKod });
        }
        #endregion
    }
}