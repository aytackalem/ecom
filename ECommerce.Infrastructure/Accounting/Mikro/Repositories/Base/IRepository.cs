using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
	public interface IRepository<TEntity, TId> where TEntity : class
	{
		#region Method
		Boolean Create(TEntity entity);

		Boolean Update(TEntity entity);
		#endregion
	}
}