using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public partial interface ICARI_HESAPLARRepository : IRepository<CARI_HESAPLAR, Guid>
	{
		#region Method
		List<CARI_HESAPLAR> Read();

		CARI_HESAPLAR Read(Guid cari_Guid);

		CARI_HESAPLAR ReadByCariKod(string cariKod);

		CARI_HESAPLAR ReadByTaxNumber(string taxnumber);
		#endregion
	}
}