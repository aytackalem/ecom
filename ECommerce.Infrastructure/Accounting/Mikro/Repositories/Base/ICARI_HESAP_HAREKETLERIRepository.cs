using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public partial interface ICARI_HESAP_HAREKETLERIRepository : IRepository<CARI_HESAP_HAREKETLERI, Guid>
	{
		#region Methods
		List<CARI_HESAP_HAREKETLERI> Read();

		CARI_HESAP_HAREKETLERI Read(Guid cha_Guid);

		int GetEvrakNoSira(string cha_evrakno_seri, byte cha_evrak_tip = 63);

		bool ReadBySign(string sign);

		CARI_HESAP_HAREKETLERI ReadBySeriSira(string cha_evrakno_seri_sira);

		List<CARI_HESAP_HAREKETLERI> Read(string cha_evrakno_seri, byte cha_evrak_tip, string cha_belge_no);
		#endregion
	}
}