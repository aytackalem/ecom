using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public interface ICARI_HESAP_HAREKETLERI_EKRepository : IRepository<CARI_HESAP_HAREKETLERI_EK, Guid>
	{
		#region Method
		List<CARI_HESAP_HAREKETLERI_EK> Read();

		CARI_HESAP_HAREKETLERI_EK Read(Guid chaek_Guid);
		#endregion
	}
}