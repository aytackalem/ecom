using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public partial interface ICARI_HESAP_ADRESLERIRepository : IRepository<CARI_HESAP_ADRESLERI, Guid>
	{
		#region Method
		List<CARI_HESAP_ADRESLERI> Read();

		CARI_HESAP_ADRESLERI Read(Guid adr_Guid);

		int GetLastAdrAdresNo(string adrCariKod);
		#endregion
	}
}