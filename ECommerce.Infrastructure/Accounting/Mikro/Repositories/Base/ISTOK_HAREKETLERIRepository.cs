using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public partial interface ISTOK_HAREKETLERIRepository : IRepository<STOK_HAREKETLERI, Guid>
	{
		#region Method
		List<STOK_HAREKETLERI> Read();

		STOK_HAREKETLERI Read(Guid sth_Guid);

		/// <summary>
		/// Stok transfer evraklar�nda kullan�lan en son evrak numaras�n� d�nen metod.
		/// </summary>
		/// <returns></returns>
		int LastStockTransferEvrakNoSira();

		/// <summary>
		/// Stok transfer evraklar�nda kullan�lan en son sipari� id bilgisini d�nen metod.
		/// </summary>
		/// <returns></returns>
		int LastStockTransferOrderId();

		int RemoveStockTransfer(int orderId);

		List<int> WaitingStockTransfers();

		List<STOK_HAREKETLERI> ReadBySth_fat_uid(Guid sth_fat_uid);
		#endregion
	}
}