using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
    public interface IEVRAK_ACIKLAMALARIRepository : IRepository<EVRAK_ACIKLAMALARI, Guid>
	{
		#region Method
		List<EVRAK_ACIKLAMALARI> Read();

		EVRAK_ACIKLAMALARI Read(Guid egk_Guid);

		#endregion
	}
}