﻿using ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base
{
	public partial interface ISTOKLARRepository : IRepository<STOKLAR, String>
	{
		#region Method
		List<STOKLAR> Read();

		STOKLAR Read(String sto_kod);

		PriceInfo ReadByStokKod(string stokKod);

		int StockMovenmentsCount(string stokKod, string depokod);

		int StockMovenmentsCountV2(string stokKod, string depokod);

		List<Stock> ReadBarcodes();

		List<Stock> ReadBarcodesV2();
		#endregion
	}
}
