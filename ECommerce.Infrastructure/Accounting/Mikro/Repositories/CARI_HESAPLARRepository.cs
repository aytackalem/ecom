using Dapper;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories
{
    public partial class CARI_HESAPLARRepository : RepositoryBase<CARI_HESAPLAR, Guid>, ICARI_HESAPLARRepository
	{
		#region Constructors
		public CARI_HESAPLARRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Methods
		public List<CARI_HESAPLAR> Read()
		{
			return _dbConnection.Query<CARI_HESAPLAR>(sql: $"Select * From [CARI_HESAPLAR] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public CARI_HESAPLAR Read(Guid cari_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<CARI_HESAPLAR>(sql: $"Select * From [CARI_HESAPLAR] With(NoLock) Where [cari_Guid] = @cari_Guid", transaction: _dbTransaction, param: new { cari_Guid = cari_Guid });
		}

		public CARI_HESAPLAR ReadByCariKod(string cariKod)
		{
			return QueryFirstOrDefault<CARI_HESAPLAR>(sql: "Select * From [CARI_HESAPLAR] With(NoLock) Where [cari_kod] = @cari_kod", param: new { cari_kod = cariKod });
		}

		public CARI_HESAPLAR ReadByTaxNumber(string taxNumber)
		{
			return QueryFirstOrDefault<CARI_HESAPLAR>(sql: "Select * From [CARI_HESAPLAR] With(NoLock) Where [cari_vdaire_no] = @cari_vdaire_no And cari_kod LIKE '120.%' and cari_muh_kod<>'120.01.#SOM#'", param: new { cari_vdaire_no = taxNumber });
		}
		#endregion
	}
}