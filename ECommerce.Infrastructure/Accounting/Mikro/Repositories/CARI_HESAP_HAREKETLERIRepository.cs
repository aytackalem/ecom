using Dapper;
using ECommerce.Infrastructure.Accounting.Mikro.Entities;
using ECommerce.Infrastructure.Accounting.Mikro.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Infrastructure.Accounting.Mikro.Repositories
{
    public partial class CARI_HESAP_HAREKETLERIRepository : RepositoryBase<CARI_HESAP_HAREKETLERI, Guid>, ICARI_HESAP_HAREKETLERIRepository
	{
		#region Constructor
		public CARI_HESAP_HAREKETLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<CARI_HESAP_HAREKETLERI> Read()
		{
			return _dbConnection.Query<CARI_HESAP_HAREKETLERI>(sql: $"Select * From [CARI_HESAP_HAREKETLERI] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public CARI_HESAP_HAREKETLERI Read(Guid cha_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<CARI_HESAP_HAREKETLERI>(sql: $"Select * From [CARI_HESAP_HAREKETLERI] With(NoLock) Where [cha_Guid] = @cha_Guid", transaction: _dbTransaction, param: new { cha_Guid = cha_Guid });
		}

        public CARI_HESAP_HAREKETLERI ReadBySeriSira(string cha_evrakno_seri_sira)
        {
            return _dbConnection.QueryFirstOrDefault<CARI_HESAP_HAREKETLERI>(sql: "Select * From [CARI_HESAP_HAREKETLERI] With(NoLock) Where [cha_evrakno_seri] + Cast([cha_evrakno_sira] As NVarChar(100)) = @cha_evrakno_seri_sira", transaction: _dbTransaction, param: new { cha_evrakno_seri_sira });
        }

        public int GetEvrakNoSira(string cha_evrakno_seri, byte cha_evrak_tip = 63)
        {
            return QueryFirstOrDefault<int>(sql: @"
            Select  IsNull(Max([cha_evrakno_sira]), 0) 
            From    [CARI_HESAP_HAREKETLERI] 
            Where   cha_evrakno_seri = @cha_evrakno_seri And 
                    cha_evrak_tip = @cha_evrak_tip", param: new
            {
                cha_evrakno_seri,
                cha_evrak_tip
            });
        }

        public List<CARI_HESAP_HAREKETLERI> Read(string cha_evrakno_seri, byte cha_evrak_tip, string cha_belge_no)
        {
            return Query<CARI_HESAP_HAREKETLERI>(sql: @"
            Select  * 
            From    [CARI_HESAP_HAREKETLERI] With(NoLock)
            Where   cha_evrakno_seri = @cha_evrakno_seri And 
                    cha_evrak_tip = @cha_evrak_tip and
                    cha_belge_no = @cha_belge_no",
                    param: new
                    {
                        cha_evrakno_seri,
                        cha_evrak_tip,
                        cha_belge_no
                    }).ToList();
        }

        public bool ReadBySignReturn(string sign)
        {
            return QueryFirstOrDefault<bool>(sql: @"
            Select  Case Count(0) When 0 Then 0 Else 1 End
            From    [CARI_HESAP_HAREKETLERI] 
            Where   cha_HareketGrupKodu2 = @sign and cha_evrak_tip=61", param: new
            {
                sign
            });
        }

        public bool ReadBySign(string sign)
        {
            return QueryFirstOrDefault<bool>(sql: @"
            Select  Case Count(0) When 0 Then 0 Else 1 End
            From    [CARI_HESAP_HAREKETLERI] 
            Where   cha_HareketGrupKodu2 = @sign", param: new
            {
                sign
            });
        }
        public CARI_HESAP_HAREKETLERI Read(string sign)
        {
            return QueryFirstOrDefault<CARI_HESAP_HAREKETLERI>(sql: @"
            Select  *
            From    [CARI_HESAP_HAREKETLERI] 
            Where   cha_HareketGrupKodu2 = @sign", param: new
            {
                sign
            });
        }

        public CARI_HESAP_HAREKETLERI ReadReturn(string sign)
        {
            return QueryFirstOrDefault<CARI_HESAP_HAREKETLERI>(sql: @"
            Select  *
            From    [CARI_HESAP_HAREKETLERI] 
            Where   cha_HareketGrupKodu2 = @sign and cha_evrak_tip=61", param: new
            {
                sign
            });
        }


      

        #endregion
    }
}