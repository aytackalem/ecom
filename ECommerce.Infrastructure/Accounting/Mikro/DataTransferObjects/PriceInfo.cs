﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{

    public class PriceInfo
    {
        #region Properties
        public string StokKod { get; set; }

        public string Name { get; set; }

        public float TaxExcludedUnitPrice { get; set; }

        public float Vat { get; set; }

        public float TaxIncludedUnitPrice { get; set; }

        public byte VatType { get; set; }
        #endregion
    }
}
