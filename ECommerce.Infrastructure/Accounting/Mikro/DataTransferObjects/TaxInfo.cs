﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{
    public class TaxInfo
    {
        #region Properties
        public string Number { get; set; }

        public string OfficeName { get; set; }

        public string OfficeCode { get; set; }
        #endregion
    }
}
