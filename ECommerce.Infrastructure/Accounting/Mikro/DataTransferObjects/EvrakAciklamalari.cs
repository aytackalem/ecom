﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{
    public class EvrakAciklamalari
    {
        #region Property
        public string[] Lines { get; set; }
        #endregion
    }
}
