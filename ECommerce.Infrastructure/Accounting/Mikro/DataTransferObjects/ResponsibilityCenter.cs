﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{
    public class ResponsibilityCenter
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }

        public string AccountNumber { get; set; }

        public string DocumentDescription { get; set; }

        public bool IsBrokerage { get; set; }
        #endregion
    }
}
