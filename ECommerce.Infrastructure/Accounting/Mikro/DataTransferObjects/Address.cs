﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{
    public class Address
    {
        #region Properties
        public string[] Lines { get; set; }
        #endregion
    }
}
