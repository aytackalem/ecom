﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects
{
    public class Stock
    {
        public Guid sto_Guid { get; set; }

        public string sto_kod { get; set; }

        public string sto_isim { get; set; }

        public string bar_kodu { get; set; }
    }
}
