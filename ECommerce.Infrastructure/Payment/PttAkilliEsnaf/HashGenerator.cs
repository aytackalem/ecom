﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ECommerce.Infrastructure.Payment.PttAkilliEsnaf
{
    public sealed class HashGenerator
    {
 

        public static String GenerateHash(String apiPass, String clientId, String apiUser,String rnd,String timeSpan)
        {

       
            var hashString = apiPass + clientId + apiUser + rnd + timeSpan;
            System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(hashString);
            byte[] hashingbytes = sha.ComputeHash(bytes);
            var hash = Convert.ToBase64String(hashingbytes);
            return hash;
        }
    }
}
