﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Request
{
    public class CreatePaymentRequest
    {
        public string ThreeDSessionId { get; set; }
        public string CardHolderName { get; set; }
        public string CardNo { get; set; }
        public string ExpireDate { get; set; }
        public string Cvv { get; set; }

    }
}
