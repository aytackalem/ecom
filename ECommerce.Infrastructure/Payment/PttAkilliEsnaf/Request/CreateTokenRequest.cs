﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Request
{
    public class CreateTokenRequest
    {
        [JsonProperty("clientId")]
        public int ClientId { get; set; }

        [JsonProperty("apiUser")]
        public string ApiUser { get; set; }

        [JsonProperty("rnd")]
        public string Rnd { get; set; }

        [JsonProperty("timeSpan")]
        public string TimeSpan { get; set; }
        
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("echo")]
        public string Echo { get; set; }

        [JsonProperty("extraParameters")]
        public string ExtraParameters { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("currency")]
        public int Currency { get; set; }

        [JsonProperty("installmentCount")]
        public int InstallmentCount { get; set; }
    }
}
