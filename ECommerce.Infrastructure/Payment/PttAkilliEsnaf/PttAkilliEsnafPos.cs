﻿using DocumentFormat.OpenXml.Office2016.Excel;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada;
using ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope.Stock;
using ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope.StockBatch;
using ECommerce.Infrastructure.Payment.Iyzico.Model;
using ECommerce.Infrastructure.Payment.PttAkilliEsnaf;
using ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Model;
using ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Request;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;

namespace ECommerce.Infrastructure.Payment
{
    public class PttAkilliEsnafPosProvider : IPosProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly string _baseUrl = "https://aeo.ptt.gov.tr/api/Payment";

        #region Fields
        public readonly IHttpHelperV3 _httpHelperV3;
        public readonly IHttpHelperV2 _httpHelperV2;
        #endregion
        #endregion

        #region Constructors
        public PttAkilliEsnafPosProvider(IUnitOfWork unitOfWork, IHttpHelperV3 httpHelperV3, IHttpHelperV2 httpHelperV2, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpHelperV2 = httpHelperV2;
            this._httpHelperV3 = httpHelperV3;
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Payment3DResponse> Pay3d(CreditCardPayment creditCardPayment)
        {
            var posConfigurations = this._unitOfWork.PosConfigurationRepository.DbSet().Where(pc => pc.PosId == "PTT").ToList();

            //            ClientId 1000000032
            //ApiUser Entegrasyon_01
            //ApiPass gkk4l2*TY112

            var randomGenerator = new Random();
            var rnd = randomGenerator.Next(1, 1000000).ToString();
            var timeSpan = DateTime.Now.ToString("yyyyMMddHHmmss");

            var apiUser = posConfigurations.FirstOrDefault(pc => pc.Key == "apiUser")?.Value;
            var apiPass = posConfigurations.FirstOrDefault(pc => pc.Key == "apiPass")?.Value;
            var callbackUrl = posConfigurations.FirstOrDefault(pc => pc.Key == "callbackUrl")?.Value;
            var clientId = 0;
            int.TryParse(posConfigurations.FirstOrDefault(pc => pc.Key == "clientId")?.Value, out clientId);


            var hash = HashGenerator.GenerateHash(apiPass, clientId.ToString(), apiUser, rnd, timeSpan);


            var amount = creditCardPayment.Amount.ToString("0.00").Replace(".", "").Replace(",", "");

            CreateTokenRequest tokenRequest = new()
            {
                OrderId = $"{creditCardPayment.OrderId}",
                Amount = Convert.ToInt32(amount),
                Currency = 949,
                ApiUser = apiUser,
                ClientId = clientId,
                CallbackUrl = callbackUrl,
                Hash = hash,
                Rnd = rnd,

                TimeSpan = timeSpan,
                ExtraParameters = creditCardPayment.PaymentId.ToString()

            };

#if DEBUG
            tokenRequest.CallbackUrl = "https://localhost:44392/Payment/Check3d";
#endif

            var createTokenResponse = this._httpHelperV3.SendAsync<CreateTokenRequest, ThreeDPayment>(
                 new HttpHelperV3Request<CreateTokenRequest>
                 {
                     RequestUri = $"{_baseUrl}/threeDPayment",
                     Request = tokenRequest,
                     HttpMethod = HttpMethod.Post
                 });

            createTokenResponse.Wait();


            if (createTokenResponse.Result.Content.Code == 0)
            {
                var httpHelperV3Response = _httpHelperV3.SendAsync<String>(new HttpHelperV3FormUrlEncodedRequest
                {
                    RequestUri = $"{_baseUrl}/ProcessCardForm",
                    HttpMethod = HttpMethod.Post,
                    Data = new()
                    {
                        new KeyValuePair<string, string>("ThreeDSessionId",createTokenResponse.Result.Content.ThreeDSessionId),
                        new KeyValuePair<string, string>("CardHolderName", $"{creditCardPayment.FirstName} {creditCardPayment.LastName}"),
                        new KeyValuePair<string, string>("CardNo", creditCardPayment.Number),
                        new KeyValuePair<string, string>("ExpireDate",$"{creditCardPayment.ExpireMonth.ToString().PadLeft(2,'0')}{creditCardPayment.ExpireYear.ToString().Replace("20","")}"),
                        new KeyValuePair<string, string>("Cvv", creditCardPayment.Cvv)
                    }
                }).Result;

                return new DataResponse<Payment3DResponse>()
                {

                    Data = new Payment3DResponse
                    {
                        AllowFrame = false,
                        Content = httpHelperV3Response.Content,
                        ThreeDRedirect = false
                    },
                    Success = httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK
                };
            }
            else
            {
                return new DataResponse<Payment3DResponse> { Success = false, Message = "PTT sanal pos'da geçiçi yoğunluktan dolayı işlem yapılamıyor. Lütfen daha sonra tekrar deneyiniz." };
            }

        }

        public DataResponse<PaymentResponse> Check3d()
        {

            var formOrderId = this._httpContextAccessor.HttpContext.Request.Form["orderId"];
            int orderId = 0;
            int.TryParse(formOrderId, out orderId);
            var posConfigurations = this._unitOfWork.PosConfigurationRepository.DbSet().Where(pc => pc.PosId == "PTT").ToList();
            var payment = this._unitOfWork.PaymentRepository.DbSet().FirstOrDefault(x => x.OrderId == orderId && x.PaymentTypeId == "OO");
            var randomGenerator = new Random();
            var rnd = randomGenerator.Next(1, 1000000).ToString();
            var timeSpan = DateTime.Now.ToString("yyyyMMddHHmmss");

            var apiUser = posConfigurations.FirstOrDefault(pc => pc.Key == "apiUser")?.Value;
            var apiPass = posConfigurations.FirstOrDefault(pc => pc.Key == "apiPass")?.Value;
            var callbackUrl = posConfigurations.FirstOrDefault(pc => pc.Key == "callbackUrl")?.Value;
            var clientId = 0;
            int.TryParse(posConfigurations.FirstOrDefault(pc => pc.Key == "clientId")?.Value, out clientId);

            var hash = HashGenerator.GenerateHash(apiPass, clientId.ToString(), apiUser, rnd, timeSpan);

            if (payment == null)
            {
                return new DataResponse<PaymentResponse>
                {
                    Success = false,
                    Message = "Lütfen Kart Bilgilerinizi Kontrol Ediniz.",
                    Data = new PaymentResponse
                    {
                        OrderId = orderId,
                        PaymentId = 0
                    }
                };
            }


            CreateInquiryRequest request = new()
            {
                ApiUser = apiUser,
                ClientId = clientId,
                Hash = hash,
                Rnd = rnd,
                TimeSpan = timeSpan,
                OrderId = orderId.ToString()
            };

            var createTokenResponse = this._httpHelperV3.SendAsync<CreateInquiryRequest, Inquiry>(
                 new HttpHelperV3Request<CreateInquiryRequest>
                 {
                     RequestUri = $"{_baseUrl}/inquiry",
                     Request = request,
                     HttpMethod = HttpMethod.Post
                 }).Result;


            if (createTokenResponse.Content != null &&
                !string.IsNullOrEmpty(createTokenResponse.Content.BankResponseCode)
                && createTokenResponse.Content.BankResponseCode == "00")
            {
                return new DataResponse<PaymentResponse>
                {
                    Success = true,
                    Message = "Başarılı",
                    Data = new PaymentResponse
                    {
                        AllowFrame = false,
                        OrderId = orderId,
                        PaymentId = payment.Id
                    }
                };
            }


            return new DataResponse<PaymentResponse>
            {
                Success = false,
                Message = "Lütfen Kart Bilgilerinizi Kontrol Ediniz.",
                Data = new PaymentResponse
                {
                    AllowFrame = false,
                    OrderId = orderId,
                    PaymentId = payment.Id
                }
            };
        }

        public DataResponse<IPosProvider> Find(string creditCardNumber)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
