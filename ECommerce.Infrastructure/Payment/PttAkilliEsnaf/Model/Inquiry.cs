﻿using ECommerce.Infrastructure.Payment.Iyzico.Request;
using Newtonsoft.Json;
using System;

namespace ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Model
{
    public class Inquiry
    {
        public int TransactionType { get; set; }
        public string CreateDate { get; set; }
        public string OrderId { get; set; }
        public string BankResponseCode { get; set; }
        public string BankResponseMessage { get; set; }
        public string AuthCode { get; set; }
        public string HostReferenceNumber { get; set; }
        public int Amount { get; set; }
        public int Currency { get; set; }
        public int InstallmentCount { get; set; }
        public int ClientId { get; set; }
        public string CardNo { get; set; }
        public int RequestStatus { get; set; }
        public int RefundedAmount { get; set; }
        public int PostAuthedAmount { get; set; }
        public int TransactionId { get; set; }
        public object CommissionStatus { get; set; }
        public int NetAmount { get; set; }
        public int MerchantCommissionAmount { get; set; }
        public object MerchantCommissionRate { get; set; }
        public int CardBankId { get; set; }
        public int CardTypeId { get; set; }
        public int ValorDate { get; set; }
        public int TransactionDate { get; set; }
        public int BankValorDate { get; set; }
        public object ExtraParameters { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
