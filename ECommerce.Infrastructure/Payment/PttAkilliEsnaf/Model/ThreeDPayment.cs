﻿using ECommerce.Infrastructure.Payment.Iyzico.Request;
using Newtonsoft.Json;
using System;

namespace ECommerce.Infrastructure.Payment.PttAkilliEsnaf.Model
{
    public class ThreeDPayment
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("threeDSessionId")]
        public string ThreeDSessionId { get; set; }

        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }
    }
}
