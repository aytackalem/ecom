﻿//using ECommerce.Application.Common.Interfaces.Payment;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Linq;
//using System.Net;
//using System.Security.Cryptography;
//using System.Text;
//using System.Xml.Linq;

//namespace ECommerce.Infrastructure.Payment
//{
//    public class PayUPos : IPos
//    {
//        #region Field
//        private readonly PayUPosConfiguration _configuration;

//        private StringDictionary _messages;
//        #endregion

//        #region Constructor
//        public PayUPos(IPosLogRepository posLogRepository, IPaymentLogRepository paymentLogRepository, IPayUPosConfigurationRepository payUPosConfigurationRepository) : base(posLogRepository, paymentLogRepository, "PU")
//        {
//            #region Field
//            _configuration = payUPosConfigurationRepository.Read();

//            _messages = new StringDictionary
//            {
//                { "3DS_ENROLLED", Resources.Payu.Payu3dRespons },
//                { "INVALID_PAYMENT_INFO", Resources.Payu.CardErrorResponse },
//                { "GWERROR_51", Resources.Payu.YetersizBakiyeCard },
//                { "GWERROR_-9", Resources.Payu.CardLostDateTime },
//                { "GWERROR_93", Resources.Payu.YasalİslemCardResponse }
//            };
//            #endregion
//        }
//        #endregion

//        #region Method
//        public Response Pay(Parameters parameters, CreditCard creditCard)
//        {
//            #region Parameters
//            var data = new NameValueCollection
//            {
//                { "BACK_REF", "" },
//                { "BILL_COUNTRYCODE", "TR" },
//                { "BILL_EMAIL", string.IsNullOrEmpty(parameters.CustomerEmail) ? "a@a.com" : parameters.CustomerEmail },
//                { "BILL_FNAME", parameters.CustomerName },
//                { "BILL_LNAME", parameters.CustomerName },
//                { "BILL_PHONE", parameters.CustomerPhoneNumber },
//                { "CC_CVV", creditCard.Cvv },
//                { "CC_NUMBER", creditCard.Number },
//                { "CLIENT_IP", parameters.CustomerIpAddress },
//                { "EXP_MONTH", creditCard.ExpireMonth.ToString().PadLeft(2, '0') },
//                { "EXP_YEAR", creditCard.ExpireYear.ToString() },
//                { "MERCHANT", _configuration.Merchant },
//                { "ORDER_DATE", string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.UtcNow) },
//                { "ORDER_PCODE[0]", "COSMETIC" },
//                { "ORDER_PINFO[0]", "COSMETIC" },
//                { "ORDER_PNAME[0]", "COSMETIC" },
//                { "ORDER_PRICE[0]", parameters.Amount.ToString(new System.Globalization.CultureInfo("en-us")) },
//                { "ORDER_QTY[0]", "1" },
//                { "ORDER_REF", parameters.OrderId.ToString() },
//                { "PAY_METHOD", "CCVISAMC" },
//                { "PRICES_CURRENCY", (parameters.CurrencyId == 1 ? "TRY" : "USD") },
//                { "SELECTED_INSTALLMENTS_NUMBER", parameters.Installment.ToString() }
//            };
//            #endregion

//            #region PosLog
//            var posLog = new PosLog
//            {
//                OrderId = parameters.OrderId,
//                CustomerIpAddress = parameters.CustomerIpAddress,
//                Is3d = false,
//                HasException = false,
//                Exception = string.Empty,
//                HasError = false,
//                Error = string.Empty,
//                ReturnMessage = string.Empty,
//                Timestamp = DateTime.Now,
//                PosCode = Code,
//                Request = string.Empty,
//                PaymentId = parameters.PaymentId
//            };
//            #endregion

//            #region PaymentLog
//            var paymentLog = new PaymentLog
//            {
//                OrderId = parameters.OrderId,
//                PaymentId = parameters.PaymentId,
//                PosResponseCode = string.Empty,
//                PosResponseMessage = string.Empty,
//                PosCode = Code
//            };
//            #endregion

//            var deger = "";
//            foreach (var val in data)
//            {
//                var value = val as string;
//                var byteCount = Encoding.UTF8.GetByteCount(data.Get(value));
//                deger += byteCount + data.Get(value);
//            }

//            var signatureKey = _configuration.SignatureKey;
//            var hash = HashWithSignature(deger, signatureKey);

//            data.Add("ORDER_HASH", hash);

//            var response = new Response();
//            try
//            {
//                var payuResponse = POSTFormPayu(_configuration.Url, data);
//                var status = string.Empty;
//                var returnCode = string.Empty;

//                #region PosLog
//                posLog.Response = payuResponse;
//                posLog.Response3d = "";
//                #endregion

//                if (!string.IsNullOrEmpty(payuResponse))
//                {
//                    XDocument xDocument = XDocument.Parse(payuResponse);

//                    status = xDocument.Element("EPAYMENT").Element("STATUS").Value;
//                    returnCode = xDocument.Element("EPAYMENT").Element("RETURN_CODE").Value;

//                    if (!(status == "SUCCESS" && returnCode != "3DS_ENROLLED"))
//                    {
//                        //response.Message = xDocument.Element("EPAYMENT").Element("RETURN_MESSAGE").Value;
//                        //response.Message = _messages[returnCode];
//                        response.Message = "Ödeme Onaylanmadı.";
//                        response.Code = Codes.TryAgain;

//                        #region PosLog
//                        posLog.HasError = true;
//                        posLog.Error = xDocument.Element("EPAYMENT").Element("RETURN_MESSAGE").Value;
//                        #endregion

//                        #region PaymentLog
//                        paymentLog.PosResponseCode = returnCode;
//                        paymentLog.PosResponseMessage = _messages[returnCode];
//                        #endregion


//                    }
//                    else
//                    {
//                        response.Code = Codes.TransactionApproved;
//                        response.Succeed = true;

//                        #region PosLog
//                        posLog.ReturnMessage = xDocument.Element("EPAYMENT").Element("RETURN_MESSAGE").Value;
//                        #endregion

//                        #region PaymentLog
//                        paymentLog.PosResponseCode = returnCode;
//                        paymentLog.PosResponseMessage = posLog.ReturnMessage;
//                        #endregion
//                    }
//                }
//                else
//                {
//                    response.Succeed = false;
//                    response.Code = Codes.TryAgain;
//                    response.Message = _messages[returnCode];
//                }
//            }
//            catch (Exception e)
//            {
//                response.Succeed = false;
//                response.Code = Codes.TryAgain;

//                #region PosLog Exception Logging
//                posLog.HasException = true;
//                posLog.Exception = e.Message;
//                #endregion

//                #region PaymentLog
//                paymentLog.PosResponseCode = "Code_Exception";
//                paymentLog.PosResponseMessage = e.Message;
//                #endregion
//            }

//            #region Save PosLog
//            var privateFields = new string[] { "CC_NUMBER", "CC_CVV" };
//            posLog.Request = string.Join("&", data.AllKeys.Select(key => $"{key}={(privateFields.Contains(key) ? "*" : data[key])}"));

//            _posLogRepository.Create(posLog);
//            #endregion

//            #region Save PaymentLog
//            try
//            {
//                _paymentLogRepository.Create(paymentLog);
//            }
//            catch (Exception ex)
//            {
//            }
//            #endregion

//            return response;
//        }

//        public Response3d Pay3d(Parameters parameters, CreditCard creditCard)
//        {
//#if DEBUG
//            creditCard.Number = "5571135571135575";
//            creditCard.Cvv = "000";
//            creditCard.ExpireMonth = 12;
//            creditCard.ExpireYear = 2022;
//#endif

//            #region Parameter
//            NameValueCollection data = new NameValueCollection();
//            string backref = GetReturnUrl(parameters);

//            data.Add("BACK_REF", backref);
//            data.Add("BILL_COUNTRYCODE", "TR");
//            data.Add("BILL_EMAIL", string.IsNullOrEmpty(parameters.CustomerEmail) ? "a@a.com" : parameters.CustomerEmail);
//            data.Add("BILL_FNAME", parameters.CustomerName);
//            data.Add("BILL_LNAME", parameters.CustomerName);
//            data.Add("BILL_PHONE", parameters.CustomerPhoneNumber);

//            data.Add("CC_CVV", creditCard.Cvv);
//            data.Add("CC_NUMBER", creditCard.Number);
//            data.Add("CLIENT_IP", parameters.CustomerIpAddress);
//            data.Add("EXP_MONTH", creditCard.ExpireMonth.ToString());
//            data.Add("EXP_YEAR", creditCard.ExpireYear.ToString());

//            data.Add("MERCHANT", _configuration.SecureMerchant);
//            data.Add("ORDER_DATE", string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.UtcNow));
//            data.Add("ORDER_PCODE[0]", "Cosmetic");
//            data.Add("ORDER_PINFO[0]", "Cosmetic");
//            data.Add("ORDER_PNAME[0]", "Cosmetic");
//            data.Add("ORDER_PRICE[0]", parameters.Amount.ToString(new System.Globalization.CultureInfo("en-us")));
//            data.Add("ORDER_QTY[0]", "1");

//            data.Add("ORDER_REF", parameters.OrderId.ToString());
//            data.Add("PAY_METHOD", "CCVISAMC");
//            data.Add("PRICES_CURRENCY", (parameters.CurrencyId == 1 ? "TRY" : "USD"));
//            data.Add("SELECTED_INSTALLMENTS_NUMBER", parameters.Installment.ToString());
//            #endregion

//            #region PosLog
//            var posLog = new PosLog
//            {
//                OrderId = parameters.OrderId,
//                CustomerIpAddress = parameters.CustomerIpAddress,
//                Is3d = true,
//                HasException = false,
//                Exception = string.Empty,
//                HasError = false,
//                Error = string.Empty,
//                ReturnMessage = string.Empty,
//                Timestamp = DateTime.Now,
//                PosCode = Code,
//                PaymentId = parameters.PaymentId
//            };
//            #endregion

//            var deger = "";
//            foreach (var val in data)
//            {
//                var value = val as string;
//                var byteCount = Encoding.UTF8.GetByteCount(data.Get(value));
//                deger += byteCount + data.Get(value);
//            }

//            var hash = HashWithSignature(deger, _configuration.SecureSignatureKey);

//            data.Add("ORDER_HASH", hash);

//            var response3d = new Response3d();

//            try
//            {
//                var payuResponse = POSTFormPayu(_configuration.SecureUrl, data);

//                #region PosLog
//                posLog.Response = payuResponse;
//                posLog.Response3d = "";
//                #endregion

//                if (!string.IsNullOrEmpty(payuResponse))
//                {
//                    XDocument xDocument = XDocument.Parse(payuResponse);

//                    response3d.Succeed = false;
//                    response3d.RequireRedirect = true;

//                    if (xDocument.Element("EPAYMENT").Element("STATUS").Value == "SUCCESS")
//                    {
//                        response3d.Response = xDocument.Element("EPAYMENT").Element("URL_3DS").Value;
//                        response3d.Succeed = true;
//                    }
//                    else
//                    {
//                        #region PosLog
//                        posLog.HasError = true;
//                        posLog.Error = xDocument.Element("EPAYMENT").Element("RETURN_MESSAGE").Value;
//                        #endregion
//                    }
//                }
//                else
//                {
//                    response3d.Succeed = false;
//                    response3d.Message = "Ödeme sırasında hata oluştu. Lütfen tekrar deneyiniz.";
//                }
//            }
//            catch (Exception e)
//            {
//                response3d.Succeed = false;
//                response3d.Message = "Ödeme sırasında hata oluştu. Lütfen tekrar deneyiniz.";

//                #region PosLog Exception Logging
//                posLog.HasException = true;
//                posLog.Exception = e.Message;
//                #endregion
//            }

//            #region PosLog
//            var privateFields = new string[] { "CC_NUMBER", "CC_CVV" };
//            posLog.Request = string.Join("&", data.AllKeys.Select(key => $"{key}={(privateFields.Contains(key) ? "*" : data[key])}"));

//            _posLogRepository.Create(posLog);
//            #endregion

//            return response3d;
//        }

//        public Check3d Check3d(HttpRequestBase httpRequestBase)
//        {
//            // Params
//            var orderId = int.Parse(httpRequestBase["OrderId"]);
//            var status = string.Empty;
//            var returnMessage = string.Empty;

//            // Method result
//            var result = false;

//            try
//            {
//                // Params
//                status = httpRequestBase.Form["STATUS"];
//                returnMessage = httpRequestBase.Form["RETURN_MESSAGE"];

//                // Method result
//                result = status.ToLower() == "success";

//                // Pos Log
//                //_posLogRepository.Update(
//                //    orderId,
//                //    $"Status: {status}. Return Message: {returnMessage}.",
//                //    returnMessage);
//            }
//            catch (Exception e)
//            {
//                // Pos Log
//                //_posLogRepository.Update(
//                //    orderId,
//                //    true,
//                //    e.Message);
//            }

//            return new Check3d
//            {
//                Status = result
//            };
//        }
//        #endregion

//        #region PayU Helper Method
//        public class StringString
//        {
//            public string Text1 { get; set; }
//            public string Text2 { get; set; }
//        }

//        public string HashWithSignature(string hashString, string signature)
//        {
//            var binaryHash = new HMACMD5(Encoding.UTF8.GetBytes(signature))
//                .ComputeHash(Encoding.UTF8.GetBytes(hashString));

//            var hash = BitConverter.ToString(binaryHash)
//                .Replace("-", string.Empty)
//                    .ToLowerInvariant();

//            return hash;
//        }

//        private string POSTFormPayu(string url, NameValueCollection data)
//        {
//            var result = new List<StringString>();

//            var webClient = new WebClient();
//            try
//            {
//                string request = System.Text.Encoding.UTF8.GetString(webClient.UploadValues(url, data)).Trim();
//                return request;
//            }
//            catch (WebException ex)
//            {
//            }

//            return "";
//        }

//        private static string ComputeHash(string input, HashAlgorithm algorithm)
//        {
//            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
//            byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
//            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
//        }
//        #endregion
//    }
//}