﻿using System;

namespace ECommerce.Infrastructure.Payment.Iyzico
{
    public interface RequestStringConvertible
    {
        String ToPKIRequestString();
    }
}
