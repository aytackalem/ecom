﻿namespace ECommerce.Infrastructure.Payment.Iyzico.Model
{
    public enum BasketItemType
    {
        PHYSICAL,
        VIRTUAL
    }
}
