﻿namespace ECommerce.Infrastructure.Payment.Iyzico.Model
{
    public enum PaymentGroup
    {
        PRODUCT,
        LISTING,
        SUBSCRIPTION
    }
}
