﻿using System;

namespace ECommerce.Infrastructure.Payment.Iyzico
{
    public class IyzipayConstants
    {
        public static readonly String CLIENT_VERSION = "iyzipay-dotnet-2.1.39";
    }
}
