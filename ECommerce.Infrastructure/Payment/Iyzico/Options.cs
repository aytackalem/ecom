﻿using System;

namespace ECommerce.Infrastructure.Payment.Iyzico
{
    public class Options
    {
        public String ApiKey { get; set; }
        public String SecretKey { get; set; }
        public String BaseUrl { get; set; }
    }
}
