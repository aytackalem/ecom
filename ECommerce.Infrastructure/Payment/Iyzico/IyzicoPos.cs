﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Payment.Iyzico;
using ECommerce.Infrastructure.Payment.Iyzico.Model;
using ECommerce.Infrastructure.Payment.Iyzico.Request;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Payment
{
    public class IyzicoPosProvider : IPosProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly string _baseUrl = "https://api.iyzipay.com";
        #endregion

        #region Constructors
        public IyzicoPosProvider(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Payment3DResponse> Pay3d(CreditCardPayment creditCardPayment)
        {
            var posConfigurations = this._unitOfWork.PosConfigurationRepository.DbSet().Where(pc => pc.PosId == "IY").ToList();

            Options options = new Options
            {
                ApiKey = posConfigurations.FirstOrDefault(pc => pc.Key == "apiKey")?.Value,
                SecretKey = posConfigurations.FirstOrDefault(pc => pc.Key == "secretKey")?.Value,
                BaseUrl = _baseUrl,
            };

            CreatePaymentRequest request = new()
            {
                Locale = Locale.TR.ToString(),
                ConversationId = $"{creditCardPayment.OrderId}_{creditCardPayment.PaymentId}",
                Price = creditCardPayment.Amount.ToString("#.##").Replace(",", "."),
                PaidPrice = creditCardPayment.Amount.ToString("#.##").Replace(",", "."),
                Currency = Iyzico.Model.Currency.TRY.ToString(),
                Installment = 1,
                BasketId = Guid.NewGuid().ToString(),
                PaymentChannel = PaymentChannel.WEB.ToString(),
                PaymentGroup = PaymentGroup.PRODUCT.ToString(),
                CallbackUrl = posConfigurations.FirstOrDefault(pc => pc.Key == "callbackUrl")?.Value
            };

#if DEBUG
            request.CallbackUrl = "https://localhost:44392/Payment/Check3d";
#endif

            PaymentCard paymentCard = new PaymentCard();
            //#if DEBUG
            //            paymentCard.CardHolderName = "John Doe";
            //            paymentCard.CardNumber = "5528790000000008";
            //            paymentCard.ExpireMonth = "12";
            //            paymentCard.ExpireYear = "2030";
            //            paymentCard.Cvc = "123";
            //#else
            paymentCard.CardHolderName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}";
            paymentCard.CardNumber = creditCardPayment.Number;
            paymentCard.ExpireMonth = creditCardPayment.ExpireMonth.ToString();
            paymentCard.ExpireYear = creditCardPayment.ExpireYear.ToString();
            paymentCard.Cvc = creditCardPayment.Cvv;
            //#endif
            paymentCard.RegisterCard = 0;
            request.PaymentCard = paymentCard;

            Buyer buyer = new Buyer();
            buyer.Id = Guid.NewGuid().ToString();
            buyer.Name = creditCardPayment.FirstName;
            buyer.Surname = creditCardPayment.LastName;
            buyer.GsmNumber = creditCardPayment.Phone;
            buyer.Email = creditCardPayment.CustomerEmail;
            buyer.IdentityNumber = "11111111111";
            buyer.LastLoginDate = "2015-10-05 12:43:35";
            buyer.RegistrationDate = "2013-04-21 15:12:09";
            buyer.RegistrationAddress = creditCardPayment.CustomerAddress;
            buyer.Ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            buyer.City = creditCardPayment.CustomerCityName;
            buyer.Country = creditCardPayment.CustomerCountryName;
            buyer.ZipCode = "34000";
            request.Buyer = buyer;

            Address shippingAddress = new Address();
            shippingAddress.ContactName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}";
            shippingAddress.City = creditCardPayment.CustomerCityName;
            shippingAddress.Country = creditCardPayment.CustomerCountryName;
            shippingAddress.Description = creditCardPayment.CustomerAddress;
            shippingAddress.ZipCode = "34000";
            request.ShippingAddress = shippingAddress;

            Address billingAddress = new Address();
            billingAddress.ContactName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}";
            billingAddress.City = creditCardPayment.CustomerCityName;
            billingAddress.Country = creditCardPayment.CustomerCountryName;
            billingAddress.Description = creditCardPayment.CustomerAddress;
            billingAddress.ZipCode = "34000";
            request.BillingAddress = billingAddress;

            List<BasketItem> basketItems = new();
            BasketItem firstBasketItem = new()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Ürün",
                Category1 = "Kategori 1",
                Category2 = "Kategori 2",
                ItemType = BasketItemType.PHYSICAL.ToString(),
                Price = creditCardPayment.Amount.ToString("#.##").Replace(",", ".")
            };
            basketItems.Add(firstBasketItem);
            request.BasketItems = basketItems;


            ThreedsInitialize threedsInitialize = ThreedsInitialize.Create(request, options);

            if (threedsInitialize.Status == "success")
            {
                return new DataResponse<Payment3DResponse>
                {
                    Success = true,
                    Data = new Payment3DResponse
                    {
                        ThreeDRedirect = false,
                        Content = threedsInitialize.HtmlContent
                    }
                };
            }
            else
            {
                return new DataResponse<Payment3DResponse> { Success = false, Message = threedsInitialize.ErrorMessage };
            }


        }

        public DataResponse<PaymentResponse> Check3d()
        {
            var paymentResponse = new DataResponse<PaymentResponse>();
            var posConfigurations = this._unitOfWork.PosConfigurationRepository.DbSet().Where(pc => pc.PosId == "IY").ToList();

            if (posConfigurations.Count == 0)
            {
                paymentResponse.Message = "Konfigirasyon Bulunamadı";
                return paymentResponse;
            }



            var ids = this._httpContextAccessor.HttpContext.Request.Form["conversationId"].ToString().Split("_");

            int orderId = 0;
            int paymentId = 0;
            int.TryParse(ids[0], out orderId);
            int.TryParse(ids[1], out paymentId);

            Options options = new Options
            {
                ApiKey = posConfigurations.FirstOrDefault(pc => pc.Key == "apiKey")?.Value,
                SecretKey = posConfigurations.FirstOrDefault(pc => pc.Key == "secretKey")?.Value,
                BaseUrl = _baseUrl,
            };

            if (this._httpContextAccessor.HttpContext.Request.Form["mdStatus"] != "1")
            {
                var message = "";
                switch (this._httpContextAccessor.HttpContext.Request.Form["mdStatus"])
                {
                    case "0":
                        message = "3-D Secure imzası geçersiz veya doğrulama";
                        break;
                    case "2":
                        message = "Kart sahibi veya bankası sisteme kayıtlı değil";
                        break;
                    case "3":
                        message = "Kartın bankası sisteme kayıtlı değil";
                        break;
                    case "4":
                        message = "Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmiş";
                        break;
                    case "5":
                        message = "Doğrulama yapılamıyor";
                        break;
                    case "6":
                        message = "3-D Secure hatası";
                        break;
                    case "7":
                        message = "Sistem hatası";
                        break;
                    case "8":
                        message = "Bilinmeyen kart no";
                        break;
                }

                return new DataResponse<PaymentResponse>
                {
                    Data = new PaymentResponse
                    {
                        OrderId = orderId,
                        PaymentId = paymentId
                    },
                    Success = false,
                    Message = message
                };
            }

            CreateThreedsPaymentRequest request = new CreateThreedsPaymentRequest();
            request.Locale = Locale.TR.ToString();
            request.ConversationId = this._httpContextAccessor.HttpContext.Request.Form["conversationId"];
            request.PaymentId = this._httpContextAccessor.HttpContext.Request.Form["paymentId"];
            request.ConversationData = this._httpContextAccessor.HttpContext.Request.Form["conversationData"];

            ThreedsPayment threedsPayment = ThreedsPayment.Create(request, options);

            if (threedsPayment.Status == "success")
            {
                return new DataResponse<PaymentResponse>
                {
                    Success = true,
                    Message = "Başarılı",
                    Data = new PaymentResponse
                    {
                        OrderId = orderId,
                        PaymentId = paymentId
                    }
                };

            }

            return new DataResponse<PaymentResponse>
            {
                Success = false,
                Message = threedsPayment.ErrorMessage,
                Data = new PaymentResponse
                {
                    OrderId = orderId,
                    PaymentId = paymentId
                }
            };
        }

        public DataResponse<IPosProvider> Find(string creditCardNumber)
        {
            return new DataResponse<IPosProvider>();
        }
        #endregion
    }
}
