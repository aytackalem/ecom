﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Payment.Iyzico;
using ECommerce.Infrastructure.Payment.Iyzico.Model;
using ECommerce.Infrastructure.Payment.Iyzico.Request;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Payment
{
    public class LicenseIyzicoPos
    {
        #region Fields
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly string _baseUrl = "https://api.iyzipay.com";

        private readonly string _apiKey = "JxboxFyv92TMelZ5iI9tdFLE0GBInwlE";

        private readonly string _secretKey = "GuAwHywVEXHl9vHOGReekaJDoALZuBZC";
        #endregion

        #region Constructors
        public LicenseIyzicoPos(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<string> Pay3d(CreditCardPayment creditCardPayment)
        {
            Options options = new()
            {
                ApiKey = _apiKey,
                SecretKey = _secretKey,
                BaseUrl = _baseUrl,
            };

            CreatePaymentRequest request = new()
            {
                Locale = Locale.TR.ToString(),
                ConversationId = $"{creditCardPayment.OrderId}_{creditCardPayment.PaymentId}",
                Price = creditCardPayment.Amount.ToString("#.##").Replace(",", "."),
                PaidPrice = creditCardPayment.Amount.ToString("#.##").Replace(",", "."),
                Currency = Iyzico.Model.Currency.TRY.ToString(),
                Installment = 1,
                BasketId = Guid.NewGuid().ToString(),
                PaymentChannel = PaymentChannel.WEB.ToString(),
                PaymentGroup = PaymentGroup.PRODUCT.ToString(),
                CallbackUrl = "https://erp.helpy.com.tr/LicenceCheck3d"
            };

#if DEBUG
            request.CallbackUrl = "http://localhost:41491/LicenseCheck3d";
#endif

            PaymentCard paymentCard = new();
            paymentCard.CardHolderName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}";
            paymentCard.CardNumber = creditCardPayment.Number;
            paymentCard.ExpireMonth = creditCardPayment.ExpireMonth.ToString();
            paymentCard.ExpireYear = creditCardPayment.ExpireYear.ToString();
            paymentCard.Cvc = creditCardPayment.Cvv;
            paymentCard.RegisterCard = 0;
            request.PaymentCard = paymentCard;

            Buyer buyer = new()
            {
                Id = Guid.NewGuid().ToString(),
                Name = creditCardPayment.FirstName,
                Surname = creditCardPayment.FirstName,
                GsmNumber = creditCardPayment.Phone,
                Email = creditCardPayment.CustomerEmail,
                IdentityNumber = "11111111111",
                LastLoginDate = "2015-10-05 12:43:35",
                RegistrationDate = "2013-04-21 15:12:09",
                RegistrationAddress = creditCardPayment.CustomerAddress,
                Ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                City = creditCardPayment.CustomerCityName,
                Country = creditCardPayment.CustomerCountryName,
                ZipCode = "34000"
            };
            request.Buyer = buyer;

            Address shippingAddress = new()
            {
                ContactName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}",
                City = creditCardPayment.CustomerCityName,
                Country = creditCardPayment.CustomerCountryName,
                Description = creditCardPayment.CustomerAddress,
                ZipCode = "34000"
            };
            request.ShippingAddress = shippingAddress;

            Address billingAddress = new()
            {
                ContactName = $"{creditCardPayment.FirstName} {creditCardPayment.LastName}",
                City = creditCardPayment.CustomerCityName,
                Country = creditCardPayment.CustomerCountryName,
                Description = creditCardPayment.CustomerAddress,
                ZipCode = "34000"
            };
            request.BillingAddress = billingAddress;

            List<BasketItem> basketItems = new();
            BasketItem firstBasketItem = new()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Lisans",
                Category1 = "Kategori 1",
                Category2 = "Kategori 2",
                ItemType = BasketItemType.VIRTUAL.ToString(),
                Price = creditCardPayment.Amount.ToString("#.##").Replace(",", ".")
            };
            basketItems.Add(firstBasketItem);
            request.BasketItems = basketItems;

            ThreedsInitialize threedsInitialize = ThreedsInitialize.Create(request, options);

            if (threedsInitialize.Status == "success")
                return new DataResponse<string> { Success = true, Data = threedsInitialize.HtmlContent };
            else
                return new DataResponse<string> { Success = false, Message = threedsInitialize.ErrorMessage };
        }

        public DataResponse<PaymentResponse> Check3d()
        {
            var ids = this._httpContextAccessor.HttpContext.Request.Form["conversationId"].ToString().Split("_");

            int orderId = 0;
            int paymentId = 0;
            int.TryParse(ids[0], out orderId);
            int.TryParse(ids[1], out paymentId);

            Options options = new Options
            {
                ApiKey = _apiKey,
                SecretKey = _secretKey,
                BaseUrl = _baseUrl,
            };

            if (this._httpContextAccessor.HttpContext.Request.Form["mdStatus"] != "1")
            {
                var message = "";
                switch (this._httpContextAccessor.HttpContext.Request.Form["mdStatus"])
                {
                    case "0":
                        message = "3-D Secure imzası geçersiz veya doğrulama";
                        break;
                    case "2":
                        message = "Kart sahibi veya bankası sisteme kayıtlı değil";
                        break;
                    case "3":
                        message = "Kartın bankası sisteme kayıtlı değil";
                        break;
                    case "4":
                        message = "Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmiş";
                        break;
                    case "5":
                        message = "Doğrulama yapılamıyor";
                        break;
                    case "6":
                        message = "3-D Secure hatası";
                        break;
                    case "7":
                        message = "Sistem hatası";
                        break;
                    case "8":
                        message = "Bilinmeyen kart no";
                        break;
                }

                return new DataResponse<PaymentResponse>
                {
                    Data = new PaymentResponse
                    {
                        OrderId = orderId,
                        PaymentId = paymentId
                    },
                    Success = false,
                    Message = message
                };
            }

            CreateThreedsPaymentRequest request = new();
            request.Locale = Locale.TR.ToString();
            request.ConversationId = this._httpContextAccessor.HttpContext.Request.Form["conversationId"];
            request.PaymentId = this._httpContextAccessor.HttpContext.Request.Form["paymentId"];
            request.ConversationData = this._httpContextAccessor.HttpContext.Request.Form["conversationData"];

            ThreedsPayment threedsPayment = ThreedsPayment.Create(request, options);

            if (threedsPayment.Status == "success")
            {
                return new DataResponse<PaymentResponse>
                {
                    Success = true,
                    Message = "Başarılı",
                    Data = new PaymentResponse
                    {
                        OrderId = orderId,
                        PaymentId = paymentId
                    }
                };

            }

            return new DataResponse<PaymentResponse>
            {
                Success = false,
                Message = threedsPayment.ErrorMessage,
                Data = new PaymentResponse
                {
                    OrderId = orderId,
                    PaymentId = paymentId
                }
            };
        }

        public DataResponse<IPosProvider> Find(string creditCardNumber)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
