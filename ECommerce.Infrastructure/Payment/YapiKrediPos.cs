﻿//using ECommerce.Application.Common.Interfaces.Payment;
//using FarkKozmetik.Client.Data.Repositories.Base;
//using FarkKozmetik.Client.DataTransferObject;
//using FarkKozmetik.Client.Entity;
//using System;
//using System.Collections.Specialized;
//using System.IO;
//using System.Net;
//using System.Security.Cryptography;
//using System.Text;
//using System.Web;
//using System.Xml;
//using System.Xml.Serialization;

//namespace ECommerce.Infrastructure.Payment
//{
//    public class YapiKrediPos : IPos
//    {
//        #region Field
//        private readonly YapiKrediPosConfiguration _configuration;

//        private StringDictionary _messages;
//        #endregion

//        #region Constructors
//        public YapiKrediPos(IPosLogRepository posLogRepository, IPaymentLogRepository paymentLogRepository, IYapiKrediPosConfigurationRepository yapiKrediPosConfigurationRepository) : base(posLogRepository, paymentLogRepository, "YK")
//        {
//            #region Field
//            _configuration = yapiKrediPosConfigurationRepository.Read();

//            _messages = new StringDictionary
//            {
//                { "0001", Resources.YapiKredi.CCNotPermitted },
//                { "0004", Resources.YapiKredi.CCBlocked },
//                { "0005", Resources.YapiKredi.CCIncorrectOrUnavailable },
//                { "0007", Resources.YapiKredi.CCStatusError },
//                { "0014", Resources.YapiKredi.CCNotExists },
//                { "0041", Resources.YapiKredi.CCLost },
//                { "0051", Resources.YapiKredi.CCInsufficientLimit },
//                { "0054", Resources.YapiKredi.CCExpired },
//                { "0057", Resources.YapiKredi.CCOnlineNotPermitted },
//                { "0062", Resources.YapiKredi.CCRestricted }
//            };
//            #endregion
//        }
//        #endregion

//        #region Method
//        public Response Pay(Parameters parameters, CreditCard creditCard)
//        {
//#if DEBUG
//            creditCard.Number = "4506347027911094";
//            creditCard.Cvv = "000";
//            creditCard.ExpireMonth = 12;
//            creditCard.ExpireYear = 2024;
//#endif

//            String currencyCode = parameters.CurrencyId == 1 ? "TL" : "USD";

//            PosnetRequest posnetRequest = new PosnetRequest
//            {
//                Mid = _configuration.MerchantId,
//                Tid = _configuration.TerminalId,
//                TranDateRequired = "1",
//                Sale = new Sale
//                {
//                    Amount = parameters.Amount.ToString("N2").Replace(",", "").Replace(".", ""),
//                    Ccno = creditCard.Number,
//                    CurrencyCode = currencyCode,
//                    Cvc = creditCard.Cvv,
//                    ExpDate = $"{creditCard.ExpireYear.ToString().Substring(2, 2)}{creditCard.ExpireMonth.ToString().PadLeft(2, '0')}",
//                    OrderID = $"{_configuration.OrderIdPrefix}{parameters.OrderId.ToString().PadLeft(22, '0')}",
//                    Installment = "00"
//                }
//            };
//            PosnetResponse posnetResponse = Pay(posnetRequest);

//            var posLog = new PosLog
//            {
//                OrderId = parameters.OrderId,
//                CustomerIpAddress = parameters.CustomerIpAddress,
//                Is3d = false,
//                HasException = false,
//                Exception = string.Empty,
//                HasError = false,
//                Error = string.Empty,
//                ReturnMessage = string.Empty,
//                Timestamp = DateTime.Now,
//                PosCode = Code,
//                Request = string.Empty,
//                PaymentId = parameters.PaymentId
//            };

//            if (posnetResponse.Approved == "1")
//                return new Response
//                {
//                    Succeed = true
//                };
//            else
//                return new Response
//                {
//                    Succeed = false,
//                    Code = Codes.TryAgain,
//                    Message = _messages[posnetResponse.AuthCode]
//                };
//        }

//        private PosnetResponse Pay(PosnetRequest posnetRequest)
//        {
//            String data = Serialize<PosnetRequest>(posnetRequest);

//            WebRequest webRequest = WebRequest.Create(String.Format($"{_configuration.Url}?xmldata={{0}}", data));
//            webRequest.Method = "POST";
//            webRequest.ContentType = "application/xwww-form-urlencoded; charset=utf-8";
//            webRequest.Headers.Add("X-MERCHANT-ID", _configuration.MerchantId);
//            webRequest.Headers.Add("X-TERMINAL-ID", _configuration.TerminalId);
//            webRequest.Headers.Add("X-POSNET-ID", _configuration.PosnetId);

//            String xml = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();

//            return Deserialize<PosnetResponse>(xml);
//        }

//        public Response3d Pay3d(Parameters parameters, CreditCard creditCard)
//        {
////#if DEBUG
////            creditCard.Number = "4506347027911094";
////            creditCard.Cvv = "000";
////            creditCard.ExpireMonth = 12;
////            creditCard.ExpireYear = 2024;
////#endif

//            String currencyCode = parameters.CurrencyId == 1 ? "TL" : "USD";

//            Posnet3dRequest posnet3DRequest = new Posnet3dRequest
//            {
//                Mid = _configuration.MerchantId,
//                Tid = _configuration.TerminalId,
//                OosRequestData = new OosRequestData
//                {
//                    Amount = parameters.Amount.ToString("N2").Replace(",", "").Replace(".", ""),
//                    CardHolderName = creditCard.Name,
//                    Ccno = creditCard.Number,
//                    CurrencyCode = currencyCode,
//                    Cvc = creditCard.Cvv,
//                    ExpDate = $"{creditCard.ExpireYear.ToString().Substring(2, 2)}{creditCard.ExpireMonth.ToString().PadLeft(2, '0')}",
//                    Installment = "00",
//                    TranType = "Sale",
//                    Posnetid = _configuration.PosnetId,
//                    XID = $"{_configuration.OrderIdPrefix}{parameters.OrderId.ToString().PadLeft(18, '0')}"
//                }
//            };

//            var returnUrl = GetReturnUrl(parameters);

//            String html;
//            Posnet3dResponse posnet3DResponse = Pay3d(posnet3DRequest, parameters, returnUrl, out html);

//            return new Response3d
//            {
//                RequireRedirect = false,
//                Response = html,
//                Succeed = posnet3DResponse.Approved == "1"
//            };
//        }

//        private Posnet3dResponse Pay3d(Posnet3dRequest posnet3dRequest, Parameters parameters, string returnUrl, out string html)
//        {
//            String data = Serialize<Posnet3dRequest>(posnet3dRequest);

//            WebRequest webRequest = WebRequest.Create(String.Format($"{_configuration.Url}?xmldata={{0}}", data));
//            webRequest.Method = "POST";
//            webRequest.ContentType = "application/xwww-form-urlencoded; charset=utf-8";
//            webRequest.Headers.Add("X-MERCHANT-ID", _configuration.MerchantId);
//            webRequest.Headers.Add("X-TERMINAL-ID", _configuration.TerminalId);
//            webRequest.Headers.Add("X-POSNET-ID", _configuration.PosnetId);

//            String xml = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();
//            Posnet3dResponse posnet3DResponse = Deserialize<Posnet3dResponse>(xml);

//            html = @"
//<!DOCTYPE html>

//<html lang=""en"" xmlns=""http://www.w3.org/1999/xhtml"">
//    <head>
//        <meta charset=""utf-8"" />
//        <title></title>
//        <script type=""text/javascript"" src=""https://" + parameters.Domain + @"/Content/Js/posnet.js""></script>
//        <script type=""text/javascript"">
//            function submitFormEx(Form, OpenNewWindowFlag, WindowName) {
//                submitForm(Form, OpenNewWindowFlag, WindowName)
//                Form.submit();
//            }
//        </script>
//</head>
//<body>
//    <form name=""formName"" method=""post"" action=""{url}"" target=""YKBWindow"">
//        <input name=""mid"" type=""hidden"" id=""mid"" value=""{mid}"" />
//        <input name=""posnetID"" type=""hidden"" id=""PosnetID"" value=""{pid}"" />
//        <input name=""posnetData"" type=""hidden"" id=""posnetData"" value=""{data1}"" />
//        <input name=""posnetData2"" type=""hidden"" id=""posnetData2"" value=""{data2}"" />
//        <input name=""digest"" type=""hidden"" id=""sign"" value=""{sign}"" />
//        <input name=""merchantReturnURL"" type=""hidden"" id="" merchantReturnURL"" value=""{returnUrl}"" />
//        <input name=""lang"" type=""hidden"" id=""lang"" value=""tr"" />
//        <input name=""url"" type=""hidden"" id=""url"" value=""https://www." + parameters.Domain + @""" />
//        <input name=""openANewWindow"" type=""hidden"" id=""openANewWindow"" value=""0"" />

//        <input name=""orderId"" type=""hidden"" id=""orderId"" value=""{orderId}"" />
//        <input name=""orderGuid"" type=""hidden"" id=""orderGuid"" value=""{orderGuid}"" />
//        <input name=""ourPaymentId"" type=""hidden"" id=""ourPaymentId"" value=""{ourPaymentId}"" />
//        <input name=""PosCode"" type=""hidden"" id=""PosCode"" value=""YK"" />
//    </form>
//    <script type=""text/javascript"">
//        submitFormEx(formName, 0, 'YKBWindow')
//    </script>
//</body>
//</html>";
//            html = html.Replace("{url}", _configuration.SecureUrl);
//            html = html.Replace("{mid}", posnet3dRequest.Mid);
//            html = html.Replace("{pid}", posnet3dRequest.OosRequestData.Posnetid);
//            html = html.Replace("{data1}", posnet3DResponse.OosRequestDataResponse == null ? "" : posnet3DResponse.OosRequestDataResponse.Data1);
//            html = html.Replace("{data2}", posnet3DResponse.OosRequestDataResponse == null ? "" : posnet3DResponse.OosRequestDataResponse.Data2);
//            html = html.Replace("{sign}", posnet3DResponse.OosRequestDataResponse == null ? "" : posnet3DResponse.OosRequestDataResponse.Sign);
//            html = html.Replace("{returnUrl}", returnUrl);

//            return posnet3DResponse;
//        }

//        public Check3d Check3d(HttpRequestBase httpRequestBase)
//        {
//            string MerchantPacket, BankPacket, Sign, CCPrefix, TranType, Amount, Xid, MerchantId;
//            MerchantPacket = httpRequestBase.Form["MerchantPacket"];
//            BankPacket = httpRequestBase.Form["BankPacket"];
//            Sign = httpRequestBase.Form["Sign"];
//            CCPrefix = httpRequestBase.Form["CCPrefix"];
//            TranType = httpRequestBase.Form["TranType"];
//            Amount = httpRequestBase.Form["Amount"];
//            Xid = httpRequestBase.Form["Xid"];
//            MerchantId = httpRequestBase.Form["MerchantId"];

//            String hash = Hash(_configuration.EncryptionKey + ';' + _configuration.TerminalId);
//            String mac = Hash(Xid + ';' + Amount + ';' + "TL" + ';' + _configuration.MerchantId + ';' + hash);

//            var response = ResolveMerchant(new PosnetResolveMerchantRequest
//            {
//                Mid = _configuration.MerchantId,
//                Tid = _configuration.TerminalId,
//                OosResolveMerchantData = new OosResolveMerchantData
//                {
//                    BankData = BankPacket,
//                    MerchantData = MerchantPacket,
//                    Mac = mac,
//                    Sign = Sign
//                }
//            });

//            String bankMac = null;
//            if (response != null && response.OosResolveMerchantDataResponse != null)
//            {
//                bankMac = Hash(response.OosResolveMerchantDataResponse.MdStatus + ';' + Xid + ';' + Amount + ';' + "TL" + ';' + _configuration.MerchantId + ';' + Hash(_configuration.EncryptionKey + ';' + _configuration.TerminalId));

//                if (response.OosResolveMerchantDataResponse.Mac != bankMac)
//                    return new Check3d
//                    {
//                        Status = false
//                    };
//            }

//            var response2 = TranData(new PosnetTranDataRequest
//            {
//                Mid = _configuration.MerchantId,
//                Tid = _configuration.TerminalId,
//                OosTranData = new OosTranData
//                {
//                    BankData = BankPacket,
//                    Mac = mac,
//                    WpAmount = Amount
//                }
//            });

//            return new Check3d
//            {
//                Status = response2.Approved == "1"
//            };
//        }

//        private PosnetResolveMerchantResponse ResolveMerchant(PosnetResolveMerchantRequest posnetResolveMerchantRequest)
//        {
//            String data = Serialize<PosnetResolveMerchantRequest>(posnetResolveMerchantRequest);

//            WebRequest webRequest = WebRequest.Create(String.Format($"{_configuration.Url}?xmldata={{0}}", data));
//            webRequest.Method = "POST";
//            webRequest.ContentType = "application/xwww-form-urlencoded; charset=utf-8";
//            webRequest.Headers.Add("X-MERCHANT-ID", _configuration.MerchantId);
//            webRequest.Headers.Add("X-TERMINAL-ID", _configuration.TerminalId);
//            webRequest.Headers.Add("X-POSNET-ID", _configuration.PosnetId);

//            string xml = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();

//            return Deserialize<PosnetResolveMerchantResponse>(xml);
//        }

//        private PosnetTranDataResponse TranData(PosnetTranDataRequest posnetTranDataRequest)
//        {
//            String data = Serialize<PosnetTranDataRequest>(posnetTranDataRequest);

//            WebRequest webRequest = WebRequest.Create(String.Format($"{_configuration.Url}?xmldata={{0}}", data));
//            webRequest.Method = "POST";
//            webRequest.ContentType = "application/xwww-form-urlencoded; charset=utf-8";
//            webRequest.Headers.Add("X-MERCHANT-ID", _configuration.MerchantId);
//            webRequest.Headers.Add("X-TERMINAL-ID", _configuration.TerminalId);
//            webRequest.Headers.Add("X-POSNET-ID", _configuration.PosnetId);

//            String xml = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();

//            return Deserialize<PosnetTranDataResponse>(xml);
//        }
//        #endregion

//        #region Helper Methods
//        private String Serialize<T>(T t)
//        {
//            String data;

//            using (MemoryStream memoryStream = new MemoryStream())
//            {
//                using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream, new XmlWriterSettings
//                {
//                    Encoding = Encoding.GetEncoding("ISO-8859-9")
//                }))
//                {
//                    XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces();
//                    xmlSerializerNamespaces.Add("", "");

//                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
//                    xmlSerializer.Serialize(xmlWriter, t, xmlSerializerNamespaces);
//                    xmlWriter.Close();
//                    xmlWriter.Flush();

//                    memoryStream.Seek(0, SeekOrigin.Begin);
//                    using (StreamReader streamReader = new StreamReader(memoryStream))
//                    {
//                        data = streamReader.ReadToEnd();
//                    }
//                }
//            }

//            return data;
//        }

//        private T Deserialize<T>(StreamReader streamReader)
//        {
//            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
//            return (T)xmlSerializer.Deserialize(streamReader);
//        }

//        private T Deserialize<T>(String xml)
//        {
//            using (StringReader stringReader = new StringReader(xml))
//            {
//                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
//                return (T)xmlSerializer.Deserialize(stringReader);
//            }
//        }

//        private string Hash(string originalString)
//        {
//            using (SHA256 sha256Hash = SHA256.Create())
//            {
//                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(originalString));
//                return Convert.ToBase64String(bytes);
//            }
//        }
//        #endregion
//    }
//}