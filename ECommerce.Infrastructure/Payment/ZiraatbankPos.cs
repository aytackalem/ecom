﻿//using ECommerce.Application.Common.Interfaces.Payment;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Globalization;
//using System.Net.Http;
//using System.Security.Cryptography;
//using System.Text;
//using System.Web;
//using System.Xml;

//namespace ECommerce.Infrastructure.Payment
//{
//    public class ZiraatbankPos : IPos
//    {
//        #region Field
//        private readonly ZiraatbankPosConfiguration _configuration;

//        private StringDictionary _messages;
//        #endregion

//        #region Constructor
//        public ZiraatbankPos(IPosLogRepository posLogRepository, IPaymentLogRepository paymentLogRepository, IZiraatbankPosConfigurationRepository ziraatbankPosConfigurationRepository) : base(posLogRepository, paymentLogRepository, "ZB")
//        {
//            #region Field
//            _configuration = ziraatbankPosConfigurationRepository.Read();
//            #endregion
//        }
//        #endregion

//        #region Method
//        public Response Pay(Parameters parameters, CreditCard creditCard)
//        {
//            var data =
//            $"DATA=" +
//            $"<CC5Request>" +
//            $"<Name>{_configuration.Username}</Name>" +
//            $"<Password>{_configuration.Password}</Password>" +
//            $"<ClientId>{_configuration.AccountCode}</ClientId>" +
//            $"<Mode>P</Mode>" +
//            $"<OrderId>{parameters.OrderId}</OrderId>" +
//            $"<Number>{creditCard.Number}</Number>" +
//            $"<Expires>{creditCard.ExpireMonth.ToString().PadLeft(2, '0')}/{creditCard.ExpireYear.ToString()}</Expires>" +
//            $"<Cvv2Val>{creditCard.Cvv.ToString()}</Cvv2Val>" +
//            $"<Total>{parameters.Amount.ToString(new CultureInfo("en-US"))}</Total>" +
//            $"<Type>Auth</Type>" +
//            $"<UserId>1000-1</UserId>" +
//            $"<Currency>949</Currency>" +
//            $"<IPAddress>{parameters.CustomerIpAddress}</IPAddress>" +
//            $"<BillTo>" +
//            $"<Name>Sinoz</Name>" +
//            $"<Street1>42. Ada</Street1>" +
//            $"<City>İstanbul</City>" +
//            $"<PostalCode>34000</PostalCode>" +
//            $"<email>a@a.com</email>" +
//            $"<TelVoice>02122121212</TelVoice>" +
//            $"<Country>TR</Country>" +
//            $"</BillTo>" +
//            $"</CC5Request>";

//            #region PosLog
//            var posLog = new PosLog
//            {
//                OrderId = parameters.OrderId,
//                CustomerIpAddress = parameters.CustomerIpAddress,
//                Is3d = false,
//                HasException = false,
//                Exception = string.Empty,
//                HasError = false,
//                Error = string.Empty,
//                ReturnMessage = string.Empty,
//                Timestamp = DateTime.Now,
//                PosCode = Code,
//                Request = string.Empty,
//                PaymentId = parameters.PaymentId
//            };
//            #endregion

//            #region PaymentLog
//            var paymentLog = new PaymentLog
//            {
//                OrderId = parameters.OrderId,
//                PaymentId = parameters.PaymentId,
//                PosResponseCode = string.Empty,
//                PosResponseMessage = string.Empty,
//                PosCode = Code
//            };
//            #endregion

//            XmlDocument xmlDocument = null;
//            using (var httpClient = new HttpClient())
//            {
//                try
//                {
//                    var result = httpClient.
//                                            PostAsync(_configuration.Url, new StringContent(data, Encoding.UTF8, "application/xml")).
//                                            Result.
//                                            Content.
//                                            ReadAsStringAsync().
//                                            Result;

//                    xmlDocument = new XmlDocument();
//                    xmlDocument.LoadXml(result);

//                    #region PosLog
//                    posLog.Response = result;
//                    posLog.Response3d = "";
//                    #endregion
//                    paymentLog.PosResponse = result;
//                }
//                catch (Exception e)
//                {
//                    #region PosLog Exception Logging
//                    posLog.HasException = true;
//                    posLog.Exception = e.Message;
//                    #endregion

//                    paymentLog.PosResponse = e.Message;
//                }
//            }
//            String response = xmlDocument.SelectSingleNode("CC5Response").SelectSingleNode("Response").InnerText;
//            paymentLog.PosResponseCode = response;
//            if (response == "Approved")
//            {
//                #region PosLog
//                posLog.ReturnMessage = response;
//                #endregion

//                #region Save PosLog
//                _posLogRepository.Create(posLog);
//                #endregion

//                #region Save PaymentLog
//                paymentLog.PosResponseMessage = response;
//                try
//                {
//                    _paymentLogRepository.Create(paymentLog);
//                }
//                catch (Exception ex)
//                {
//                }
//                #endregion

//                return new Response
//                {
//                    Succeed = true
//                };
//            }
//            else
//            {
//                #region PosLog
//                posLog.HasError = true;
//                posLog.Error = xmlDocument.SelectSingleNode("CC5Response").SelectSingleNode("ErrMsg").InnerText;
//                #endregion

//                #region Save PosLog
//                _posLogRepository.Create(posLog);
//                #endregion

//                #region Save PaymentLog
//                paymentLog.PosResponseMessage = posLog.Error;
//                try
//                {
//                    _paymentLogRepository.Create(paymentLog);
//                }
//                catch (Exception ex)
//                {
//                }
//                #endregion

//                return new Response
//                {
//                    Succeed = false,
//                    //Message = xmlDocument.SelectSingleNode("CC5Response").SelectSingleNode("ErrMsg").InnerText
//                    Message = "Ödeme onaylanmadı."
//                };
//            }
//        }

//        public Response3d Pay3d(Parameters parameters, CreditCard creditCard)
//        {
//            return new Response3d
//            {
//                Response = CreateSecureHtmlString(parameters, creditCard),
//                RequireRedirect = false,
//                Succeed = true
//            };
//        }

//        public Check3d Check3d(HttpRequestBase httpRequestBase)
//        {
//            try
//            {
//                var responseMessage = PaymentLog(httpRequestBase);

//                string hashparams = httpRequestBase.Params["HASHPARAMS"];
//                string hash = httpRequestBase.Params["HASH"];

//                int index1 = 0, index2 = 0;
//                string paramsval = "";
//                do
//                {
//                    index2 = hashparams.IndexOf(":", index1);
//                    string key = hashparams.Substring(index1, index2 - index1);
//                    string val = httpRequestBase.Params[key] == null ? "" : httpRequestBase.Params[key];
//                    paramsval += val;
//                    index1 = index2 + 1;
//                }
//                while (index1 < hashparams.Length);
//                paramsval += _configuration.SecretKey;
//                bool condition = GetSHA1(paramsval) == hash;

//                return new Check3d
//                {
//                    Status = condition && !string.IsNullOrEmpty(httpRequestBase.Form["response"]) && httpRequestBase.Form["response"].ToLower() == "approved",
//                    Response = responseMessage
//                };
//            }
//            catch (Exception ex)
//            {
//                this._logger.Error(ex, "Check3d - " + ex.Message);
//                return new Check3d
//                {
//                    Status = false
//                };
//            }
//        }

//        public override string PaymentLog(HttpRequestBase httpRequestBase)
//        {
//            #region PaymentLog
//            try
//            {
//                var paymentLog = new PaymentLog
//                {
//                    OrderId = GetRequestValue<int>(httpRequestBase, "OrderId"),
//                    PaymentId = GetRequestValue<int>(httpRequestBase, "OurPaymentId"),
//                    PosResponseCode = GetRequestFormValue<string>(httpRequestBase.Form, "response", "mdErrorMsg"),
//                    PosResponseMessage = HttpUtility.UrlDecode(GetRequestFormValue<string>(httpRequestBase.Form, "ErrMsg", "ErrorMsg", "mdErrorMsg")).Replace("+", " "),
//                    PosResponse = httpRequestBase.Form != null ? httpRequestBase.Form.ToString() : string.Empty,
//                    PosCode = this.Code,
//                    Is3d = true
//                };
//                _paymentLogRepository.Create(paymentLog);

//                return paymentLog.PosResponseMessage;
//            }
//            catch (Exception ex)
//            {
//                this._logger.Error(ex, $"PaymentLog - Request:{JsonConvert.SerializeObject(httpRequestBase)} Message:" + ex.Message);
//                return string.Empty;
//            }
//            #endregion
//        }
//        #endregion

//        #region Helper Method
//        private Dictionary<string, string> CreateRequestParam3D(Parameters parameters, CreditCard creditCard)
//        {
//            var returnUrl = GetReturnUrl(parameters);

//            string shopCode = _configuration.AccountCode;
//            string orderId = parameters.OrderId.ToString();
//            string purchAmount = parameters.Amount.ToString(CultureInfo.InvariantCulture);
//            string okUrl = returnUrl;
//            string failUrl = returnUrl;
//            string txnType = "Auth";
//            string hashParam = shopCode + orderId + purchAmount + okUrl + failUrl + txnType + "" + orderId + _configuration.SecretKey;

//            Dictionary<string, string> param3D = new Dictionary<string, string> {
//                { "CustomerSuccessUrl", okUrl },
//                { "CustomerErrorUrl", failUrl },
//                { "clientid", shopCode },
//                { "storetype", "3D_PAY" },
//                { "cardType", "1" },
//                { "hash", GetSHA1(hashParam) },
//                { "islemtipi", txnType },
//                { "amount", purchAmount },
//                { "currency", "949"  },
//                { "oid", orderId},
//                { "okUrl", okUrl },
//                { "failUrl", failUrl },
//                { "lang", "tr" },
//                { "rnd", orderId },
//                { "pan", creditCard.Number},
//                { "cv2", creditCard.Cvv },
//                { "Ecom_Payment_Card_ExpDate_Month", creditCard.ExpireMonth.ToString().PadLeft(2, '0')},
//                { "Ecom_Payment_Card_ExpDate_Year", creditCard.ExpireYear.ToString().Substring(2, 2)},
//                //{ "", "" }, CUSTOM PARAMETER
//            };

//            return param3D;
//        }

//        private string CreateSecureHtmlString(Parameters parameters, CreditCard creditCard)
//        {
//            var param3D = CreateRequestParam3D(parameters, creditCard);

//            StringBuilder htmlForm = new StringBuilder();
//            htmlForm.AppendFormat("<form method='post' action='{0}'>", _configuration.SecureUrl);
//            foreach (var param in param3D)
//            {
//                htmlForm.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", param.Key, param.Value);
//            }
//            htmlForm.Append("</form>");
//            htmlForm.Append("<script language='javascript'>document.forms[0].submit();</script>");
//            return htmlForm.ToString();
//        }

//        private string GetSHA1(string SHA1Data)
//        {
//            EncodingProvider instance = CodePagesEncodingProvider.Instance;
//            Encoding.RegisterProvider(instance);

//            SHA1 sha = new SHA1CryptoServiceProvider();
//            string HashedPassword = SHA1Data;
//            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
//            byte[] inputbytes = sha.ComputeHash(hashbytes);
//            return Convert.ToBase64String(inputbytes);
//        }
//        #endregion
//    }
//}