﻿using ECommerce.Application.Common.Interfaces.Caching;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.ExternalCache.File
{
    public class ExternalFileCache : IExternalCache
    {
        #region Constants
        private const string _basePath = "C:\\ExternalFileCache";
        #endregion

        #region Methods
        public List<string> GetAllKeys()
        {
            var files = System.IO.Directory.GetFiles(_basePath, "*.txt", System.IO.SearchOption.AllDirectories);
            var allKeys = files.Select(f =>
            {
                return string.Join("_", f.Replace(_basePath, "").Replace(".txt", "").Split("\\", System.StringSplitOptions.RemoveEmptyEntries));
            }).ToList();
            return allKeys;
        }

        public bool ContainsKey(string key)
        {
            var fileName = this.ConvertFileName(key);
            return System.IO.File.Exists(fileName);
        }

        public string GetValue(string key)
        {
            return System.IO.File.ReadAllText(this.ConvertFileName(key));
        }

        public void SetEntry(string key, string value)
        {
            var directoryPath = this.ConvertDirectoryPath(key);
            System.IO.Directory.CreateDirectory(directoryPath);

            var fileName = this.ConvertFileName(key);
            System.IO.File.WriteAllText(fileName, value);
        }

        public bool RemoveEntry(params string[] args)
        {
            bool deleted = true;
            Parallel.ForEach(args, theKey =>
            {
                var fileName = this.ConvertFileName(theKey);
                if (System.IO.File.Exists(fileName))
                    try
                    {
                        System.IO.File.Delete(fileName);
                    }
                    catch
                    {
                        deleted = false;
                    }
            });
            return deleted;
        }
        #endregion

        #region Helper Methods
        private string ConvertFileName(string key)
        {
            var keyParts = key.Split('_');
            var tenantId = keyParts[0];
            var domainId = keyParts[1];
            var companyId = keyParts[2];
            var type = keyParts[3];
            var marketplaceId = keyParts[4];
            var uid = keyParts[5];

            return $"{_basePath}\\{tenantId}\\{domainId}\\{companyId}\\{type}\\{marketplaceId}\\{uid}.txt";
        }

        private string ConvertDirectoryPath(string key)
        {
            var keyParts = key.Split('_');
            var tenantId = keyParts[0];
            var domainId = keyParts[1];
            var companyId = keyParts[2];
            var type = keyParts[3];
            var marketplaceId = keyParts[4];

            return $"{_basePath}\\{tenantId}\\{domainId}\\{companyId}\\{type}\\{marketplaceId}";
        }
        #endregion
    }
}
