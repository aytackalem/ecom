﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrendyolEFaturamServiceReference;
using ECommerce.Application.Common.Interfaces.EInvoice;

namespace ECommerce.Infrastructure.EInvoice.TrendyolEFaturam
{
    public class TrendyolEFaturamProvider : IEInvoiceProvider
	{
        #region Fields
        private readonly DateTime _transactionDate = DateTime.Now;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IntegrationServiceSoapClient _client;

        private String _ticket;

        private List<Domain.Entities.AccountingConfiguration> _accountingCompanyConfigurations;
        #endregion

        #region Constructors
        public TrendyolEFaturamProvider(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            var endpointConfiguration = IntegrationServiceSoapClient.EndpointConfiguration.IntegrationServiceSoap;
            this._client = new IntegrationServiceSoapClient(endpointConfiguration);
            #endregion
        }
		#endregion

		#region Methods
		public DataResponse<AccountingInfo> CreateInvoice(int orderId)
		{
			return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
			{
				#region Read Accounting Company Configurations
				this._accountingCompanyConfigurations ??= this
					._unitOfWork
					.AccountingCompanyConfigurationRepository
					.DbSet()
					.Where(x => x.AccountingCompany.AccountingId == "T")
					.ToList();

				var loginName = this._accountingCompanyConfigurations.First(x => x.Key == "LoginName").Value;
				var password = this._accountingCompanyConfigurations.First(x => x.Key == "Password").Value;
				var corporateCode = this._accountingCompanyConfigurations.First(x => x.Key == "CorporateCode").Value;
				var receiverPostboxName = this._accountingCompanyConfigurations.First(x => x.Key == "ReceiverPostboxName").Value;
				#endregion

				#region Read Order
				var order = _unitOfWork
							.OrderRepository
							.DbSet()
							.Include(o => o.Customer)
							.Include(o => o.Company.CompanyContact)
							.Include(o => o.OrderDetails)
								.ThenInclude(x => x.ProductInformation)
							.Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
							.FirstOrDefault(x => x.Id == orderId);
				#endregion

				this._ticket ??= this._client.GetFormsAuthenticationTicketAsync(corporateCode, loginName, password).Result;

				String xml = this.PrepareXml(order);

				SendInvoiceDataResponse sendInvoiceDataResponse = this
					._client
					.SendInvoiceDataAsync(
						this._ticket,
						File.Xml,
						Encoding.UTF8.GetBytes(xml),
						corporateCode,
						String.Empty,
						receiverPostboxName)
					.Result;
			});
		}
		#endregion

		#region Helper Methods
		public string PrepareXml(Domain.Entities.Order order)
        {
            return @$"
<Invoices>
	<Version>2.1</Version>
	<Invoice>


		<ReceipentInfo>
			<Identifications>
				<Identification>
					<Attribute>{(string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) ? "TCKN" : "VKN")}</Attribute>
					<Value>{(string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) ? "11111111111" : order.OrderInvoiceInformation.TaxNumber)}</Value>
				</Identification>
			</Identifications>
{(string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) ? @$"
			<Person>
				<FirstName>{order.OrderInvoiceInformation.FirstName}</FirstName>
				<FamilyName>{order.OrderInvoiceInformation.LastName}</FamilyName>
				<Title></Title>
				<MiddleName></MiddleName>
				<NameSuffix></NameSuffix>
			</Person>
" : @$"
			<PartyName>
				{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}
			</PartyName>
")}			
			<WebURL></WebURL>
			<Address>
				<Country>{order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name}</Country>
				<CityName>{order.OrderInvoiceInformation.Neighborhood.District.City.Name}</CityName>
				<CitySubdivisionName>{order.OrderInvoiceInformation.Neighborhood.District.Name}</CitySubdivisionName>
				<Room></Room>
				<StreetName>
					{order.OrderInvoiceInformation.Address}
				</StreetName>
				<BuildingName></BuildingName>
				<BuildingNumber></BuildingNumber>
				<PostalZone></PostalZone>
				<Region>{order.OrderInvoiceInformation.Neighborhood.Name}</Region>
				<ID></ID>
			</Address>
			<CommunicationChannels>
				<Telephone>{order.OrderInvoiceInformation.Phone}</Telephone>
				<Telefax></Telefax>
				<ElectronicMail>{order.OrderInvoiceInformation.Mail}</ElectronicMail>
				<Others>
					<ChannelCode></ChannelCode>
					<Channel></Channel>
					<Value></Value>
				</Others>
			</CommunicationChannels>
{(string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber) ? "" : @$"
			<PartyTaxScheme>{order.OrderInvoiceInformation.TaxOffice}</PartyTaxScheme>
")}
		</ReceipentInfo>


		<SenderInfo>
			<Identifications>
				<Identification>
					<Attribute>VKN</Attribute>
					<Value>{order.Company.CompanyConfigurations.First(cc => cc.Key == "TaxNumber").Value}</Value>
				</Identification>
			</Identifications>
			<PartyName>{order.Company.FullName}</PartyName>
			<WebURL>{order.Company.CompanyContact.WebUrl}</WebURL>
			<Address>
				<Country></Country>
				<CityName></CityName>
				<CitySubdivisionName></CitySubdivisionName>
				<Room></Room>
				<StreetName></StreetName>
				<BuildingName></BuildingName>
				<BuildingNumber></BuildingNumber>
				<PostalZone></PostalZone>
				<Region></Region>
				<ID></ID>
			</Address>
			<CommunicationChannels>
				<Telephone>{order.Company.CompanyContact.PhoneNumber}</Telephone>
				<Telefax></Telefax>
				<ElectronicMail>{order.Company.CompanyContact.Email}</ElectronicMail>
				<Others>
					<ChannelCode></ChannelCode>
					<Channel></Channel>
					<Value></Value>
				</Others>
			</CommunicationChannels>
			<PartyTaxScheme>
				{order.Company.CompanyContact.TaxOffice}
			</PartyTaxScheme>
		</SenderInfo>


		<InvoiceInfo>
			<InvoiceID>{this._transactionDate.Year:yyyy}{order.Id.ToString().PadLeft(9, '0')}</InvoiceID>
			<LineCount>{order.OrderDetails.Count}</LineCount>
			<Scenario>TEMELFATURA</Scenario>
			<IssueDate>{this._transactionDate:yyyy-MM-dd}</IssueDate>
			<IssueTime>{this._transactionDate:HH-mm-ss}</IssueTime>
			<InvoiceTypeCode>SATIS</InvoiceTypeCode>
			<CopyIndicator>false</CopyIndicator>
			<UUID>{Guid.NewGuid()}</UUID>
			<Currency>
				<Attribute>DocumentCurrencyCode</Attribute>
				<Value>TRL</Value>
			</Currency>
			<InvoicePeriod>
				<StartDate>{new DateTime(this._transactionDate.Year, this._transactionDate.Month, 1):yyyy-MM-dd}</StartDate>
				<EndDate>{new DateTime(this._transactionDate.Year, this._transactionDate.Month, 1).AddMonths(1).AddDays(-1):yyyy-MM-dd}</EndDate>
				<DurationMeasure>
					<Attribute></Attribute>
					<Value></Value>
				</DurationMeasure>
				<Description></Description>
			</InvoicePeriod>
			<SendingType>ELEKTRONIK</SendingType>
		</InvoiceInfo>


		<InvoiceLines>
{string.Join("", order.OrderDetails.Select(od => $@"
			<Line>
				<ID>{order.OrderDetails.IndexOf(od) + 1}</ID>
				<InvoicedQuantity>
					<UnitCode>{od.ProductInformation.StockCode}</UnitCode>
					<Quantity>{od.Quantity}</Quantity>
				</InvoicedQuantity>
				<LineExtensionAmount>
					<Currency>TRL</Currency>
					<Amount>{od.ListUnitPrice * od.Quantity}</Amount>
				</LineExtensionAmount>
				<AllowanceCharge>
					<ChargeIndicator>false</ChargeIndicator>
					<ChargeAmount>
						<Currency>TRL</Currency>
						<Amount>{od.UnitDiscount * od.Quantity}</Amount>
					</ChargeAmount>
					<BaseAmount>
						<Currency>TRL</Currency>
						<Amount>{od.UnitPrice * od.Quantity}</Amount>
					</BaseAmount>
				</AllowanceCharge>
				<TaxTotal>
					<TaxAmount>
						<Currency>TRL</Currency>
						<Amount>{od.UnitPrice - od.VatExcUnitPrice}</Amount>
					</TaxAmount>
					<TaxSubTotals>
						<TaxAmount>
							<Currency>TRL</Currency>
							<Amount>{od.UnitPrice - od.VatExcUnitPrice}</Amount>
						</TaxAmount>
						<TaxCategory>
							<TaxScheme>
								<Name>KDV</Name>
								<TaxTypeCode>00{od.VatRate * 100}</TaxTypeCode>
							</TaxScheme>
						</TaxCategory>
					</TaxSubTotals>
				</TaxTotal>
				<Item>
					<Description></Description>
					<Name>{od.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name}</Name>
					<BrandName></BrandName>
					<ModelName></ModelName>
					<BuyersItemIdentification></BuyersItemIdentification>
					<SellersItemIdentification></SellersItemIdentification>
					<ManufacturersItemIdentification></ManufacturersItemIdentification>
					<CommodityClassification></CommodityClassification>
				</Item>
				<Price>
					<Currency>TRL</Currency>
					<Amount>{od.UnitPrice}</Amount>
				</Price>
			</Line>
"))}
		</InvoiceLines>


		<Taxes>
			<Withholding>false</Withholding>
			<TaxAmount>
				<Currency>TRL</Currency>
				<Amount>{order.Total - order.VatExcTotal}</Amount>
			</TaxAmount>
			<TaxSubTotals>
				<TaxableAmount>
					<Currency>TRY</Currency>
					<Amount>{order.VatExcTotal}</Amount>
				</TaxableAmount>
				<TaxAmount>
					<Currency>TRY</Currency>
					<Amount>{order.Total - order.VatExcTotal}</Amount>
				</TaxAmount>
				<TaxCategory>
					<TaxExemptionReason>
						Vergi Muafiyeti
						uygulanmamıştır
					</TaxExemptionReason>
					<TaxScheme>
						<Name>KDV</Name>
						<TaxTypeCode>0015</TaxTypeCode>
					</TaxScheme>
				</TaxCategory>
			</TaxSubTotals>
		</Taxes>


		<InvoiceTotals>
			<LineExtensionAmount>
				<Currency>TRY</Currency>
				<Amount>{order.Total}</Amount>
			</LineExtensionAmount>
			<TaxExclusiveAmount>
				<Currency>TRY</Currency>
				<Amount>{order.Total}</Amount>
			</TaxExclusiveAmount>
			<TaxInclusiveAmount>
				<Currency>TRY</Currency>
				<Amount>{order.Total}</Amount>
			</TaxInclusiveAmount>
			<AllowanceTotalAmount>
				<Currency>TRY</Currency>
				<Amount>{order.Total - order.VatExcTotal}</Amount>
			</AllowanceTotalAmount>
			<ChargeTotalAmount>
				<Currency>TRY</Currency>
				<Amount>0.0</Amount>
			</ChargeTotalAmount>
			<PayableRoundingAmount>
				<Currency>TRY</Currency>
				<Amount>0.0</Amount>
			</PayableRoundingAmount>
			<PayableAmount>
				<Currency>TRY</Currency>
				<Amount>{order.Total}</Amount>
			</PayableAmount>
		</InvoiceTotals>


	</Invoice>
</Invoices>
";
        }
        #endregion
    }
}
