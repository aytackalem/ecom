﻿using DocumentFormat.OpenXml.Spreadsheet;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantValueService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplaceCatalog
{
    public class MarketplaceVariantValueService : IMarketplaceVariantValueService
    {
        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public MarketplaceVariantValueService(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<bool>> GetExsistName(SearchByNameParameters parameters)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<bool>>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<DataResponse<bool>>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"https://marketplacecatalog.helpy.com.tr/VariantValue/ExsistName?Code={parameters.Code}&MarketplaceId={parameters.MarketplaceId}&VariantCode={parameters.VariantCode}&CategoryCode={parameters.CategoryCode}"
                });

                response.Success = httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success;
                response.Data = httpResponse.Content.Data;
            }, response =>
            {
                response.Message = "Pazaryeri ürün özellikleri alınamadı.";
            });
        }
        #endregion
    }
}
