﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplaceCatalog
{
    public class MarketplaceVariantService : IMarketplaceVariantService
    {
        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public MarketplaceVariantService(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<GetByCategoryCodesResponse>> GetByCategoryCodesAsync(GetByCategoryCodesParameter parameters)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<GetByCategoryCodesResponse>>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<GetByCategoryCodesParameter, DataResponse<GetByCategoryCodesResponse>>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request<GetByCategoryCodesParameter>
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = "https://marketplacecatalog.helpy.com.tr/Variant/GetByCategoryCodes",
                    Request = parameters
                });

                response.Success = httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success;
                response.Data = httpResponse.Content.Data;
            });
        }
        #endregion
    }
}
