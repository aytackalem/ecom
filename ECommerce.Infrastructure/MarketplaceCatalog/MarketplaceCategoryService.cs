﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplaceCatalog
{
    public class MarketplaceCategoryService : IMarketplaceCategoryService
    {
        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public MarketplaceCategoryService(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<GetByMarketplaceIdResponse>>(async response =>
            {

                var httpResponse = await this._httpHelperV3.SendAsync<DataResponse<GetByMarketplaceIdResponse>>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"https://marketplacecatalog.helpy.com.tr/Category/GetByMarketplaceId?MarketplaceId={marketplaceId}"
                });

                response.Success = httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success;
                response.Data = httpResponse.Content.Data;

            }, response =>
            {
                response.Message = "Pazaryeri kategorileri alınamadı.";
            });
        }
        #endregion
    }
}
