﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Ups.DataTransferObjects
{
    public enum Enums
    {
        ShipperAccountNumber,
        Username,
        Password,
        ShipperName,
        ShipperAddress,
        ShipperCityName,
        ShipperDistrictName,
        ConsigneeName,
        ConsigneeAddress,
        ConsigneeCityCode,
        ConsigneeAreaCode,
        ConsigneePhoneNumber,
        ValueOfGoodsPaymentType,
        Amount,
        Currency,
        ShipperAreaCode,
        ShipperCityCode,
        TrackingNumber

    }
}
