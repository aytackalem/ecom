﻿using ECommerce.Application.Common;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using UpsCreateShipmentService;
using UpsQueryPackagesInfoService;

namespace ECommerce.Infrastructure.Shipment.Ups
{
    public class UpsProvider : IShipmentProvider
    {

  
        private IUnitOfWork _unitOfWork;
        public UpsProvider(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #region Methods
        public DataResponse<ShipmentInfo> Post(List<ShipmentCompanyConfiguration> configurations, Order order, bool payor, int[] weights, int piece)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentInfo>>((response) =>
            {
                var cityName = order.OrderDeliveryAddress.Neighborhood.District.City.Name;
                var districtName = order.OrderDeliveryAddress.Neighborhood.District.Name;

                var upsCity = _unitOfWork.UpsCityRepository.Read(cityName);
                if (upsCity == null)
                {
                    response.Message = "Ups İlçe bulunamadı.";
                    response.Success = false;
                    return;
                }

                if (districtName.Contains(cityName)) districtName = districtName.Replace(cityName, "").Replace("/", "").Replace("\\", "").Trim();

                var upsArea = _unitOfWork.UpsAreaRepository.Read(districtName, upsCity.Id);

                if (upsArea == null)
                {
                    response.Message = "Ups İlçe bulunamadı.";
                    response.Success = false;
                    return;
                }


                var client = new CreateShipmentSoapClient(CreateShipmentSoapClient.EndpointConfiguration.CreateShipmentSoap12);



                var username = "";
                var password = "";
                var shipperAccountNumber = "";
                var shipperName = "";
                var shipperAddress = "";
                var shipperCityCode = "";
                var shipperAreaCode = "";

                username = configurations.FirstOrDefault(x => x.Key == "Username")?.Value;
                password = configurations.FirstOrDefault(x => x.Key == "Password")?.Value;
                shipperAccountNumber = configurations.FirstOrDefault(x => x.Key == "ShipperAccountNumber")?.Value;
                shipperName = configurations.FirstOrDefault(x => x.Key == "ShipperName")?.Value;
                shipperAddress = configurations.FirstOrDefault(x => x.Key == "ShipperAddress")?.Value;
                shipperCityCode = configurations.FirstOrDefault(x => x.Key == "ShipperCityCode")?.Value;
                shipperAreaCode = configurations.FirstOrDefault(x => x.Key == "ShipperAreaCode")?.Value;


                
                var login = client.Login_Type1Async(shipperAccountNumber, username, password);
                login.Wait();
                if (string.IsNullOrEmpty(login.Result.Body.Login_Type1Result.SessionID))
                {
                    response.Message = "Ups ile bağlantı kurulamıyor.";

                    return;
                }

                if (string.IsNullOrEmpty(order.OrderDeliveryAddress.Recipient))
                {
                    response.Success = false;

                    response.Message = "Müsteri ismi boş gelmektedir.";

                    return;
                }

                if (string.IsNullOrEmpty(order.OrderDeliveryAddress.Address))
                {
                    response.Success = false;
                    response.Message = "Müsteri adresi boş gelmektedir.";

                    return;
                }

                var shipmentInfo = new ShipmentInfo_Type2
                {
                    ShipperAccountNumber = shipperAccountNumber,
                    ShipperName = shipperName,
                    ShipperAddress = shipperAddress,
                    ShipperCityCode = Convert.ToInt32(shipperCityCode),
                    ShipperAreaCode = Convert.ToInt32(shipperAreaCode),
                    ConsigneeName = order.OrderDeliveryAddress.Recipient,
                    ConsigneeAddress = order.OrderDeliveryAddress.Address,
                    ConsigneeCityCode = upsCity.Id,
                    ConsigneeAreaCode = upsArea.Id,
                    ConsigneePhoneNumber = order.OrderDeliveryAddress.PhoneNumber,
                    ServiceLevel = 3,
                    PaymentType = 2,
                    PackageType = "D",
                    NumberOfPackages = 1,

                };

                if (order.Payments.Any(x => x.PaymentTypeId == "DO")) //Kapıda ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "DO");

                    shipmentInfo.ValueOfGoods = payment.Amount;
                    shipmentInfo.ValueOfGoodsCurrency = order.CurrencyId;
                    shipmentInfo.ValueOfGoodsPaymentType = 1;
                }
                else if (order.Payments.Any(x => x.PaymentTypeId == "PDO")) //Kapıda kredi kartı ile ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "PDO");

                    shipmentInfo.ValueOfGoods = payment.Amount;
                    shipmentInfo.ValueOfGoodsCurrency = order.CurrencyId;
                    shipmentInfo.ValueOfGoodsPaymentType = 3;
                }

              

                    var resultShipment = client.CreateShipment_Type2Async(login.Result.Body.Login_Type1Result.SessionID, shipmentInfo, true, true);
                    resultShipment.Wait();
             



                if (resultShipment.Result.Body.CreateShipment_Type2Result.ErrorCode != 0)
                {
                    response.Success = false;
                    response.Message = "Kargo Ups'e kaydedilemedi.";

                    return;
                }

                var shipmentNo = resultShipment.Result.Body.CreateShipment_Type2Result.ShipmentNo;
                response.Data = new ShipmentInfo
                {
                    TrackingUrl = string.Empty,
                    ShipmentCompanyId = ShipmentCompanies.Ups,
                    TrackingBase64 = resultShipment.Result.Body.CreateShipment_Type2Result.BarkodArrayPng[0],
                    Barcode = shipmentNo,
                    ShipmentInfoDetails = new System.Collections.Generic.List<ShipmentInfoDetail>()
                };
                response.Success = true;

            });
        }

        public DataResponse<ShipmentTracking> Track(List<ShipmentCompanyConfiguration> configurations, string trackingCode)
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}
