﻿using ECommerce.Application.Common;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ECommerce.Infrastructure.Shipment.Aras
{
    public class ArasProvider : IShipmentProvider
    {
        #region Fields
        private readonly ArasServiceReference.ServiceSoapClient _client;

        private readonly ArasTracking.ArasCargoIntegrationServiceClient _service;
        #endregion

        #region Constructors
        public ArasProvider()
        {
            #region Fields
            var endPointConfiguration = ArasServiceReference.ServiceSoapClient.EndpointConfiguration.ServiceSoap;
            this._client = new ArasServiceReference.ServiceSoapClient(endPointConfiguration);

            this._service = new ArasTracking.ArasCargoIntegrationServiceClient(ArasTracking.ArasCargoIntegrationServiceClient.EndpointConfiguration.BasicHttpsBinding_IArasCargoIntegrationService);
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ShipmentInfo> Post(List<ShipmentCompanyConfiguration> configurations, Order order, bool payor, int[] weights, int piece)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentInfo>>((response) =>
            {
                var username = "";
                var password = "";
                var barcodePrefix = "";

                username = configurations.FirstOrDefault(x => x.Key == "Username")?.Value;
                password = configurations.FirstOrDefault(x => x.Key == "Password")?.Value;
                barcodePrefix = configurations.FirstOrDefault(x => x.Key == "BarcodePrefix")?.Value;

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                {
                    response.Success = false;
                    response.Message = "Kargo firması ayarları okunamadı.";

                    return;
                }

                var barcode = GenerateBarcode(order.Id, barcodePrefix, payor);

                var arasOrder = new ArasServiceReference.Order
                {
                    IntegrationCode = barcode,
                    TradingWaybillNumber = barcode,
                    InvoiceNumber = barcode,

                    ReceiverCityName = order.OrderDeliveryAddress.Neighborhood.District.City.Name,
                    ReceiverTownName = order.OrderDeliveryAddress.Neighborhood.District.Name,
                    ReceiverName = order.OrderDeliveryAddress.Recipient,
                    ReceiverAddress = order.OrderDeliveryAddress.Address,
                    ReceiverPhone1 = order.OrderDeliveryAddress.PhoneNumber,

                    IsWorldWide = "0",
                    /* Kargo ücretini kim öder? 1 gönderici 2 alıcı */
                    PayorTypeCode = payor ? "1" : "2",
                    /* Kapıda ödemeli mi? */
                    IsCod = order.Payments.Any(p => p.PaymentTypeId == "DO" && p.Amount > 0) ? "1" : "0",
                    PieceCount = piece.ToString(),
                    CodCollectionType = order.Payments.Any(p => p.PaymentTypeId == "DO" && p.Amount > 0) ? "1" : "",
                    CodAmount = order.Payments.Where(p => p.PaymentTypeId == "DO").Sum(p => p.Amount) > 0 ? Convert.ToDouble(order.Payments.Where(p => p.PaymentTypeId == "DO").Sum(p => p.Amount)).ToString(CultureInfo.CreateSpecificCulture("tr-TR")) : "",
                    PieceDetails = new ArasServiceReference.PieceDetail[piece]
                };
                for (int i = 0; i < piece; i++)
                {
                    arasOrder.PieceDetails[i] = new ArasServiceReference.PieceDetail
                    {
                        BarcodeNumber = $"{barcode}{(i + 1).ToString().PadLeft(2, '0')}",
                        Weight = weights[i].ToString(),
                        VolumetricWeight = weights[i].ToString()
                    };
                }

                var orders = new ArasServiceReference.Order[] { arasOrder };

                var orderResults = this._client.SetOrderAsync(orders, username, password).Result;
                if (orderResults.Length == 0 || orderResults[0].ResultCode != "0")
                {
                    response.Success = false;
                    response.Message = orderResults.Length == 0 ? "Response BOŞ." : orderResults[0].ResultMessage;

                    return;
                }

                response.Success = true;
                response.Data = new ShipmentInfo
                {
                    ShipmentCompanyId = ShipmentCompanies.ArasKargo,
                    Barcode = barcode,
                    ShipmentInfoDetails = arasOrder
                        .PieceDetails
                        .Select(pd => new ShipmentInfoDetail
                        {
                            TrackingCode = pd.BarcodeNumber,
                            Weight = pd.Weight
                        })
                        .ToList()
                };
            });
        }

        public DataResponse<ShipmentTracking> Track(List<ShipmentCompanyConfiguration> configurations, string trackingCode)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentTracking>>((response) =>
            {
                var username = "sinoz";
                var password = "sinoz2460!x";
                var barcodePrefix = "";
                var customerCode = "2104254551157";

                //username = configurations.FirstOrDefault(x => x.Key == "Username")?.Value;
                //password = configurations.FirstOrDefault(x => x.Key == "Password")?.Value;
                //barcodePrefix = configurations.FirstOrDefault(x => x.Key == "BarcodePrefix")?.Value;
                //customerCode = configurations.FirstOrDefault(x => x.Key == "CustomerCode")?.Value;

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(customerCode))
                {
                    response.Success = false;
                    response.Message = "Kargo firması ayarları okunamadı.";

                    return;
                }

                string loginInfo = "<LoginInfo><UserName>" + username + "</UserName><Password>" + password + "</Password><CustomerCode>" + customerCode + "</CustomerCode></LoginInfo>";
                string queryInfo = "<QueryInfo><QueryType>39</QueryType><IntegrationCode>" + trackingCode + "</IntegrationCode></QueryInfo>";

                var result = this._service.GetQueryJSONAsync(loginInfo, queryInfo).Result;
                response.Data = new ShipmentTracking
                {
                    Delivered = result.GetQueryJSONResult.Contains("\"DURUMU\":\"TESLİM EDİLDİ -")
                };
                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private string GenerateBarcode(int orderId, string barcodePrefix, bool payor)
        {
            var payorCode = payor ? "S" : "A";
            return $"{barcodePrefix}{payorCode}{DateTime.Now.Year}{orderId.ToString().PadLeft(8, '0')}";
        }
        #endregion
    }
}
