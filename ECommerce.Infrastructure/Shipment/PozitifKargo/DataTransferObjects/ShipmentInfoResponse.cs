﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.PozitifKargo.DataTransferObjects
{
    public class ShipmentInfoResponse
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("record_id")]
        public int RecordId { get; set; }

    }
}
