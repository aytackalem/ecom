﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.PozitifKargo.DataTransferObjects
{
    public class ShipmentInfoRequest
    {
        [JsonProperty("customer")]
        public string Customer { get; set; }

        [JsonProperty("province_name")]
        public string ProvinceName { get; set; }

        [JsonProperty("county_name")]
        public string CountyName { get; set; }

        [JsonProperty("district")]
        public string District { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [JsonProperty("branch_code")]
        public string BranchCode{ get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("currency_name")]
        public string CurrencyName { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("sender_note")]
        public string SenderNote { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("weight")]
        public int? Weight { get; set; }

        [JsonProperty("consignment_type_id")]
        public int ConsignmentTypeId { get; set; }

        [JsonProperty("amount_type_id")]
        public int AmountTypeId { get; set; }


        [JsonProperty("order_number")]
        public string OrderNumber { get; set; }
    }
}
