﻿using ECommerce.Application.Common;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Shipment.Artos.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ECommerce.Infrastructure.Shipment.PozitifKargo
{
    public class PozitifKargoProvider : IShipmentProvider
    {
        #region Fields
        private readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public PozitifKargoProvider(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ShipmentInfo> Post(List<ShipmentCompanyConfiguration> configurations, Order order, bool payor, int[] weights, int piece)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentInfo>>((response) =>
            {
                var apiKey = "";
                var apiFrom = "";
                var branchCode = "";

                apiKey = configurations.FirstOrDefault(x => x.Key == "ApiKey")?.Value;
                apiFrom = configurations.FirstOrDefault(x => x.Key == "ApiFrom")?.Value;
                branchCode = configurations.FirstOrDefault(x => x.Key == "BranchCode")?.Value;

                if (string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(apiFrom))
                {
                    response.Success = false;
                    response.Message = "Kargo firması ayarları okunamadı.";

                    return;
                }

                var headers = new Dictionary<string, string>();
                headers.Add("Authorization", apiKey);
                headers.Add("From", apiFrom);




                var request = new ShipmentInfoRequest
                {
                    Customer = order.OrderDeliveryAddress.Recipient,
                    ProvinceName = order.OrderDeliveryAddress.Neighborhood.District.City.Name,
                    CountyName = order.OrderDeliveryAddress.Neighborhood.District.Name,
                    District = order.OrderDeliveryAddress.Neighborhood.Name,
                    Address = $"{order.OrderDeliveryAddress.Address} Ilce:{order.OrderDeliveryAddress.Neighborhood.District.Name} Mahalle:{order.OrderDeliveryAddress.Neighborhood.Name}",
                    Telephone = order.OrderDeliveryAddress.PhoneNumber,
                    BranchCode = branchCode,
                    Barcode = null,
                    Weight = 1,
                    BulkValue = "2",
                    CurrencyName = "Türk Lirası",
                    Quantity = 1,
                    ConsignmentTypeId = 2,
                    OrderNumber = order.Id.ToString().PadLeft(8, '0'),
                    AmountTypeId = 3,
                    Summary = $"({order.OrderDeliveryAddress.PhoneNumber}) {string.Join(",", order.OrderDetails.GroupBy(x => x.CategoryName).Select(x => x.Key))}"
                };

                if (order.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaNakit)) //Kapıda ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == PaymentTypes.KapıdaNakit);

                    if (payment.Amount == 0)
                    {
                        response.Success = false;
                        return;
                    }
                    request.AmountTypeId = 3;
                    request.Amount = payment.Amount;
                }
                else if (order.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit)) //Kapıda kredi kartı ile ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit);

                    if (payment.Amount == 0)
                    {
                        response.Success = false;
                        return;
                    }
                    request.AmountTypeId = 6;
                    request.Amount = payment.Amount;
                }



                var invoiceInfo = this._httpHelper.Post<ShipmentInfoRequest, ShipmentInfoResponse>(request, "https://webpostman.pozitifkargo.com/restapi/client/consignment/add", null, null, headers);

                response.Success = invoiceInfo.Error == false;
                response.Data = new ShipmentInfo
                {
                    ShipmentCompanyId = ShipmentCompanies.ArtosDagitim,
                    Barcode = invoiceInfo.Barcode,
                    ShipmentInfoDetails = new List<ShipmentInfoDetail>(),
                    TrackingBase64 = "",
                    TrackingUrl = ""

                };
            });
        }

        public DataResponse<ShipmentTracking> Track(List<ShipmentCompanyConfiguration> configurations, string trackingCode)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Helper Methods

        #endregion
    }
}
