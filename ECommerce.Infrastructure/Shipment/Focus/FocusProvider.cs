﻿using ECommerce.Application.Common;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Infrastructure.Shipment.Focus.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Artos
{
    public class FocusProvider : IShipmentProvider
    {
        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;

        private readonly string _url = "https://api.focuswebpanel.com.tr/";

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public FocusProvider(IHttpHelperV3 httpHelper, ICacheHandler cacheHandler)
        {
            #region Fields
            this._httpHelperV3 = httpHelper;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ShipmentInfo> Post(List<ShipmentCompanyConfiguration> configurations, Order order, bool payor, int[] weights, int piece)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentInfo>>(async (response) =>
            {

                var apiKey = configurations.FirstOrDefault(x => x.Key == "ApiKey")?.Value;

                if (order.OrderDeliveryAddress.Neighborhood.District.City.Name == "AFYONKARAHISAR")
                {
                    order.OrderDeliveryAddress.Neighborhood.District.City.Name = "Afyon";
                }

                var cities = GetCities(apiKey);
                if (!cities.Success)
                {
                    return;
                }
                var _city = cities.Data.data.FirstOrDefault(x => x.il_ad.ReplaceChar() == order.OrderDeliveryAddress.Neighborhood.District.City.Name.ReplaceChar());
                if (_city == null) return;

                var districts = GetDistricts(apiKey, _city.il_kodu);
                if (!districts.Success)
                {
                    return;
                }

                if (order.OrderDeliveryAddress.Neighborhood.District.City.Name == "ZONGULDAK" && order.OrderDeliveryAddress.Neighborhood.District.Name == "EREĞLİ")
                {
                    order.OrderDeliveryAddress.Neighborhood.District.Name = "Karadenizereğli";
                }
                else if (order.OrderDeliveryAddress.Neighborhood.District.City.Name == "SAMSUN" && order.OrderDeliveryAddress.Neighborhood.District.Name == "19MAYIS")
                    {
                        order.OrderDeliveryAddress.Neighborhood.District.Name = "Ondokuzmayıs";
                    }

                //Ondokuzmayıs

                var _district = districts.Data.data.FirstOrDefault(x => x.ilce_ad.ReplaceChar() == order.OrderDeliveryAddress.Neighborhood.District.Name.ReplaceChar());
                if (_district == null) return;

                var neighborhoods = GetNeighborhoods(apiKey, _city.il_kodu, _district.ilce_kodu);

                NeighborhoodData _neighborhood = null;
                if (neighborhoods.Success && neighborhoods.Data.data.Count > 0)
                {
                    _neighborhood = neighborhoods.Data.data.FirstOrDefault(x => x.mahalle_adi.ReplaceChar().Split(new string[] { "mah" }, StringSplitOptions.TrimEntries)[0] == order.OrderDeliveryAddress.Neighborhood.Name.ReplaceChar().Split(new string[] { "mah" }, StringSplitOptions.TrimEntries)[0]);
                }



                var request = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("key", apiKey),
                        new KeyValuePair<string, string>("request", "kargo_ekle"),
                        new KeyValuePair<string, string>("musteri_adi", order.OrderDeliveryAddress.Recipient),
                        new KeyValuePair<string, string>("musteri_telefon", order.OrderDeliveryAddress.PhoneNumber),
                        new KeyValuePair<string, string>("musteri_il", _city.il_kodu),
                        new KeyValuePair<string, string>("musteri_ilce", _district.ilce_kodu),
                        new KeyValuePair<string, string>("musteri_mah", _neighborhood != null ? _neighborhood.mahalle_kodu : ""),//Zorunlu Değil
                        new KeyValuePair<string, string>("musteri_adres", order.OrderDeliveryAddress.Address),
                        new KeyValuePair<string, string>("kargo_icerik",$"({order.OrderDeliveryAddress.PhoneNumber}) {string.Join(",", order.OrderDetails.GroupBy(x=>x.CategoryName).Select(x => x.Key))}"),
                        new KeyValuePair<string, string>("kargo_dagitim_turu", "1"),
                        new KeyValuePair<string, string>("kargo_teslimat_tipi", "1"),
                        new KeyValuePair<string, string>("kargo_desi", "2"),
                        new KeyValuePair<string, string>("kargo_adet", order.OrderDetails.Sum(x => x.Quantity).ToString()),
                        new KeyValuePair<string, string>("siparis_no", order.Id.ToString()),
                        new KeyValuePair<string, string>("kargo_gonderi_turu", "4"),
                    };

                var shippingDoorType = 0;//(0 = YOK) (1 = Nakit) (2 = Kredi Kart)
                if (order.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaNakit)) //Kapıda ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == PaymentTypes.KapıdaNakit);
                    shippingDoorType = 1;
                    request.Add(new KeyValuePair<string, string>("kargo_kapida_odeme_ucreti", payment.Amount.ToString()));//Kargo Tutar Bilgisi (küsürat noktalı 
                }
                else if (order.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit)) //Kapıda kredi kartı ile ödeme
                {
                    var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit);
                    shippingDoorType = 2;
                    request.Add(new KeyValuePair<string, string>("kargo_kapida_odeme_ucreti", payment.Amount.ToString()));//Kargo Tutar Bilgisi (küsürat noktalı kullanım. örn: 100.75)
                }

                request.Add(new KeyValuePair<string, string>("kargo_kapida_odeme_turu", shippingDoorType.ToString()));


                var httpResponse = this._httpHelperV3.SendAsync<ShipmentInfoResponse>(new HttpHelperV3FormUrlEncodedRequest
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = _url,
                    ContentType = "application/x-www-form-urlencoded",
                    Data = request

                }).Result;

                if (httpResponse.HttpStatusCode == HttpStatusCode.OK && httpResponse.Content.status)
                {
                    response.Success = true;
                    response.Data = new ShipmentInfo
                    {
                        ShipmentCompanyId = ShipmentCompanies.Focus,
                        Barcode = httpResponse.Content.data,
                        ShipmentInfoDetails = new List<ShipmentInfoDetail>(),
                        TrackingBase64 = "",
                        TrackingUrl = ""
                    };
                }


            });
        }

        public DataResponse<ShipmentTracking> Track(List<ShipmentCompanyConfiguration> configurations, string trackingCode)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Helper Methods

        public DataResponse<CityResponse> GetCities(string apiKey)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<CityResponse>>((response) =>
            {
                var httpResponse = this._httpHelperV3.SendAsync<CityResponse>(new HttpHelperV3FormUrlEncodedRequest
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = _url,
                    ContentType = "application/x-www-form-urlencoded",
                    Data = new()
                    {
                        new KeyValuePair<string, string>("key",apiKey),
                        new KeyValuePair<string, string>("request","il_liste"),
                    }
                }).Result;

                if (httpResponse.HttpStatusCode == HttpStatusCode.OK && httpResponse.Content.status)
                {
                    response.Success = true;
                    response.Data = httpResponse.Content;
                }
            });
            }, "FocusCities");
        }

        public DataResponse<DistrictResponse> GetDistricts(string apiKey, string cityId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<DistrictResponse>>(response =>
            {
                var httpResponse = this._httpHelperV3.SendAsync<DistrictResponse>(new HttpHelperV3FormUrlEncodedRequest
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = _url,
                    ContentType = "application/x-www-form-urlencoded",
                    Data = new()
                    {
                        new KeyValuePair<string, string>("key",apiKey),
                        new KeyValuePair<string, string>("request","ilce_liste"),
                        new KeyValuePair<string, string>("il_kodu",cityId),
                    }
                }).Result;

                if (httpResponse.HttpStatusCode == HttpStatusCode.OK && httpResponse.Content.status)
                {
                    response.Success = true;
                    response.Data = httpResponse.Content;
                }
            });
            }, $"GetDistricts_{cityId}");
        }

        public DataResponse<NeighborhoodResponse> GetNeighborhoods(string apiKey, string cityId, string districtId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<NeighborhoodResponse>>(response =>
                {
                    var httpResponse = this._httpHelperV3.SendAsync<NeighborhoodResponse>(new HttpHelperV3FormUrlEncodedRequest
                    {
                        HttpMethod = HttpMethod.Post,
                        RequestUri = _url,
                        ContentType = "application/x-www-form-urlencoded",
                        Data = new()
                        {
                        new KeyValuePair<string, string>("key",apiKey),
                        new KeyValuePair<string, string>("request","mahalle_liste"),
                        new KeyValuePair<string, string>("il_kodu",cityId),
                        new KeyValuePair<string, string>("ilce_kodu",districtId),
                        }
                    }).Result;

                    if (httpResponse.HttpStatusCode == HttpStatusCode.OK && httpResponse.Content.status)
                    {
                        response.Success = true;
                        response.Data = httpResponse.Content;
                    }
                });
            }, $"GetNeighborhoods_{cityId}_{districtId}");
        }
        #endregion
    }
}
