﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Focus.DataTransferObjects
{


    public class NeighborhoodData
    {
        public string mahalle_adi { get; set; }
        public string mahalle_kodu { get; set; }
    }

    public class NeighborhoodResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<NeighborhoodData> data { get; set; }
    }

}
