﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Focus.DataTransferObjects
{
  


    public class ShipmentInfoResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string data { get; set; }
    }
}
