﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Focus.DataTransferObjects
{


    public class CityData
    {
        public string il_ad { get; set; }
        public string il_kodu { get; set; }
    }

    public class CityResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<CityData> data { get; set; }
    }

}
