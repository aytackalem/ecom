﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Shipment.Focus.DataTransferObjects
{


    public class DistrictData
    {
        public string ilce_ad { get; set; }
        public string ilce_kodu { get; set; }
    }

    public class DistrictResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<DistrictData> data { get; set; }
    }

}
