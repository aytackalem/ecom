﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace ECommerce.Infrastructure.Communication
{
    public class DefaultEmailProvider : IEmailProvider
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public DefaultEmailProvider(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public Response Send(List<string> emailAddress, string subject, string body, bool sendAsync = true)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var emailProvider = this
                ._unitOfWork
                .EmailProviderCompanyRepository
                .DbSet()
                .Include(c => c.EmailProviderConfigurations)
                .Include(c => c.EmailProvider)
                .Where(ep => ep.EmailProviderId == "DM")
                .Select(sp => new EmailProviderCompany
                {
                    Name = sp.EmailProvider.Name,
                    EmailProviderConfigurations = sp
                        .EmailProviderConfigurations
                        .Select(spc => new EmailProviderConfiguration
                        {
                            Key = spc.Key,
                            Value = spc.Value
                        })
                        .ToList()
                }).FirstOrDefault();

                if (emailProvider == null)
                {
                    response.Message = "Mail konfigirasyonu bulunamadı";
                    response.Success = false;
                    return;
                }

                var username = emailProvider.EmailProviderConfigurations.FirstOrDefault(spc => spc.Key == "username")?.Value;
                var password = emailProvider.EmailProviderConfigurations.FirstOrDefault(spc => spc.Key == "password")?.Value;
                var displayName = emailProvider.EmailProviderConfigurations.FirstOrDefault(spc => spc.Key == "displayName")?.Value;
                var host = emailProvider.EmailProviderConfigurations.FirstOrDefault(spc => spc.Key == "host")?.Value;
                var port = emailProvider.EmailProviderConfigurations.FirstOrDefault(spc => spc.Key == "port")?.Value;

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(displayName) || string.IsNullOrEmpty(host) || string.IsNullOrEmpty(port))
                    return;

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(username, displayName),
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body
                };
                foreach (var emLoop in emailAddress)
                {
                    mailMessage.To.Add(emLoop);
                }

                var smtp = new SmtpClient(host, int.Parse(port))
                {
                    //DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential(username, password)
                };

                if (sendAsync)
                    smtp.SendAsync(mailMessage, null);
                else
                    smtp.Send(mailMessage);

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Mail gönderilemedi.";
            });


        }


        #endregion
    }
}
