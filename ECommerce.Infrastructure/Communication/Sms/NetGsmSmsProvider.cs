﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace ECommerce.Infrastructure.Communication
{
    public class NetGsmSmsProvider : ISmsProvider
    {
        #region Classess
        class KobikomSmsResponse
        {
            public string code { get; set; }
        }
        #endregion

        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public NetGsmSmsProvider(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public Response Send(string phoneNumber, string message)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var smsProvider = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Include(x => x.SmsProviderConfigurations)
                    .Where(sp => sp.SmsProviderId == "NG")
                    .Select(sp => new SmsProviderCompany
                    {
                        SmsProviderConfigurations = sp
                            .SmsProviderConfigurations
                            .Select(spc => new SmsProviderConfiguration
                            {
                                Key = spc.Key,
                                Value = spc.Value
                            })
                            .ToList()
                    })
                    .FirstOrDefault();

                if (smsProvider == null)
                {
                    response.Success = false;
                    response.Message = "Ayarlar eksik.";

                    return;
                }

                var usercode = smsProvider.SmsProviderConfigurations.FirstOrDefault(spc => spc.Key == "Username")?.Value;
                var password = smsProvider.SmsProviderConfigurations.FirstOrDefault(spc => spc.Key == "Password")?.Value;
                var header = smsProvider.SmsProviderConfigurations.FirstOrDefault(spc => spc.Key == "Header")?.Value;

                if (string.IsNullOrEmpty(usercode) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(header))
                {
                    response.Success = false;
                    response.Message = "Ayarlar eksik.";

                    return;
                }

                var stringBuilder = new StringBuilder();
                stringBuilder.Append("<?xml version='1.0' encoding='UTF-8'?>");
                stringBuilder.Append("<mainbody>");
                stringBuilder.Append("<header>");
                stringBuilder.Append("<company dil='TR'>Netgsm</company>");
                stringBuilder.Append($"<usercode>{usercode}</usercode>");
                stringBuilder.Append($"<password>{password}</password>");
                stringBuilder.Append($"<type>n:n</type>");
                stringBuilder.Append($"<msgheader>{header}</msgheader>");
                stringBuilder.Append("</header>");
                stringBuilder.Append("<body>");
                stringBuilder.Append($"<mp><msg><![CDATA[{message}]]></msg><no>{phoneNumber}</no></mp>");
                stringBuilder.Append("</body>");
                stringBuilder.Append("</mainbody>");

                try
                {
                    WebClient wUpload = new();
                    HttpWebRequest request = WebRequest.Create("https://api.netgsm.com.tr/sms/send/xml") as HttpWebRequest;
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    Byte[] bPostArray = Encoding.UTF8.GetBytes(stringBuilder.ToString());
                    Byte[] bResponse = wUpload.UploadData("https://api.netgsm.com.tr/sms/send/xml", "POST", bPostArray);
                    Char[] sReturnChars = Encoding.UTF8.GetChars(bResponse);
                    string sWebPage = new string(sReturnChars);
                }
                catch
                {
                }
            });
        }
        #endregion
    }
}
