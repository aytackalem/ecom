﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;

namespace ECommerce.Infrastructure.Communication
{
    public class KobikomSmsProvider : ISmsProvider
    {
        #region Classess
        class KobikomSmsResponse
        {
            public string code { get; set; }
        }
        #endregion

        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public KobikomSmsProvider(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public Response Send(string phoneNumber, string message)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var smsProvider = this
                    ._unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .Include(x => x.SmsProviderConfigurations)
                    .Where(sp => sp.SmsProviderId == "KB")
                    .Select(sp => new SmsProviderCompany
                    {
                        SmsProviderConfigurations = sp
                            .SmsProviderConfigurations
                            .Select(spc => new SmsProviderConfiguration
                            {
                                Key = spc.Key,
                                Value = spc.Value
                            })
                            .ToList()
                    })
                    .FirstOrDefault();

                if (smsProvider == null)
                {
                    response.Success = false;
                    response.Message = "Ayarlar eksik.";

                    return;
                }

                var apiKey = smsProvider.SmsProviderConfigurations.FirstOrDefault(spc => spc.Key == "apiKey")?.Value;
                var from = smsProvider.SmsProviderConfigurations.FirstOrDefault(spc => spc.Key == "from")?.Value;

                if (string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(from))
                {
                    response.Success = false;
                    response.Message = "Ayarlar eksik.";

                    return;
                }

                var requestUri = $"https://smspaneli.kobikom.com.tr/sms/api?action=send-sms&api_key={apiKey}&to={phoneNumber}&from={from}&sms={message}&unicode=1";

                var httpClient = new HttpClient();
                var result = httpClient
                    .GetAsync(requestUri)
                    .Result
                    .Content
                    .ReadAsStringAsync()
                    .Result;

                var kobikomSmsResponse = JsonConvert.DeserializeObject<KobikomSmsResponse>(result);

                response.Success = kobikomSmsResponse.code == "ok";
                response.Message = kobikomSmsResponse.code == "ok" ? "Sms başarılı bir şekilde gönderildi." : "Sms gönderilemedi.";

                response.Success = true;
            });
        }
        #endregion
    }
}
