﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ozon.Response
{
    public class ProductCreateResponse
    {
        public ProductCreateResult result { get; set; }
    }

    public class ProductCreateResult
    {
        public int task_id { get; set; }
    }

}
