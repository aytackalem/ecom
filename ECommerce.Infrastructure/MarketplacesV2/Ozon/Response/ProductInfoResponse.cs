﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ozon.Response
{
    public class ProductInfoResponse
    {
        public ProductInfoResult result { get; set; }
    }

    public class ProductInfoResult
    {
        public List<Item> items { get; set; }
        public int total { get; set; }
    }

    public class Item
    {
        public string offer_id { get; set; }
        public int product_id { get; set; }
        public string status { get; set; }
        public List<Error> errors { get; set; }
    }

    public class Error
    {
        public string message { get; set; }
    }
}
