﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ozon.Request
{
    public class ProductCreateRequest
    {
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public Attribute[] attributes { get; set; }
        public string barcode { get; set; }
        public int category_id { get; set; }
        public string color_image { get; set; }
        public object[] complex_attributes { get; set; }
        public string currency_code { get; set; }
        public int depth { get; set; }
        public string dimension_unit { get; set; }
        public int height { get; set; }
        public object[] images { get; set; }
        public object[] images360 { get; set; }
        public string name { get; set; }
        public string offer_id { get; set; }
        public string old_price { get; set; }
        public object[] pdf_list { get; set; }
        public string premium_price { get; set; }
        public string price { get; set; }
        public string primary_image { get; set; }
        public string vat { get; set; }
        public int weight { get; set; }
        public string weight_unit { get; set; }
        public int width { get; set; }
    }

    public class Attribute
    {
        public int complex_id { get; set; }
        public int id { get; set; }
        public Value[] values { get; set; }
    }

    public class Value
    {
        public int dictionary_value_id { get; set; }
        public string value { get; set; }
    }

}
