﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Infrastructure.MarketplacesV2.Ozon.Request;
using ECommerce.Infrastructure.MarketplacesV2.Ozon.Response;
using Helpy.Shared.Crypto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ozon
{
    public class OzonV2 : IMarketplaceV2, IMarketplaceV2CreateProductTrackable
    {
        #region Constants
        private const string _baseUrl = "https://api-seller.ozon.ru";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public OzonV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => "OZ";

        public int? WaitingSecondPerCreateProductRequest => null;

        public bool IsUpsert => true;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            OzonConfigurationV2 ozonConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

            ProductCreateRequest productCreateRequest = new()
            {
                items = new()
            };

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new Request.Item()
                    {
                        barcode = pinLoop.Barcode,
                        name = pinLoop.ProductInformationName,
                        category_id = Convert.ToInt32(piLoop.CategoryCode),
                        old_price = pinLoop.ListUnitPrice.ToString(),
                        price = pinLoop.UnitPrice.ToString(),
                        vat = pinLoop.VatRate.ToString()
                    };

                    productCreateRequest.items.Add(item);
                }
            }

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<ProductCreateRequest, ProductCreateResponse>(
                    new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request<ProductCreateRequest>
                    {
                        Request = productCreateRequest,
                        RequestUri = $"{_baseUrl}/v2/product/import",
                        HttpMethod = HttpMethod.Post,
                        Headers = new Dictionary<string, string>
                        {
                            { "Client-Id", ozonConfigurationV2.ClientId },
                            { "Api-Key", ozonConfigurationV2.ApiKey }
                        }
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.result.task_id.ToString();

            return httpHelperV3Response;
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            OzonConfigurationV2 ozonConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

            ProductInfoRequest productInfoRequest = new()
            {
                task_id = request.TrackingId
            };

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<ProductInfoRequest, ProductInfoResponse>(
                    new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request<ProductInfoRequest>
                    {
                        Request = productInfoRequest,
                        RequestUri = $"{_baseUrl}/v1/product/import/info",
                        HttpMethod = HttpMethod.Post,
                        Headers = new Dictionary<string, string>
                        {
                            { "Client-Id", ozonConfigurationV2.ClientId },
                            { "Api-Key", ozonConfigurationV2.ApiKey }
                        }
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.result.items.Any(i => i.status != "pending")
                };

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i =>
                        i
                        .ProductInformationMarketplaces
                        .Select(pim =>
                            new TrackingResponseItem
                            {
                                ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                                Barcode = pim.Barcode,
                                StockCode = pim.StockCode
                            }))
                    .ToList();

                httpResultResponse
                    .Content
                    .result
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.offer_id);
                        if (i.status.ToLower() == "imported")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.errors.Select(e => e.message).ToList();
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion
        #endregion
    }
}
