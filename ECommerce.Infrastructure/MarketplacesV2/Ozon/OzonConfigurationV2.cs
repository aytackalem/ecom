﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ozon
{
    public class OzonConfigurationV2
    {
        #region Members
        public static implicit operator OzonConfigurationV2(Dictionary<string, string> configurations)
        {
            return new OzonConfigurationV2
            {
                ClientId = configurations["ClientId"],
                ApiKey = configurations["ApiKey"]
            };
        }
        #endregion

        #region Properties
        public string ClientId { get; set; }

        public string ApiKey { get; set; }
        #endregion
    }
}
