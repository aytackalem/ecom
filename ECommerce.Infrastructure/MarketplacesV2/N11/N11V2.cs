﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using N11CategoryService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11
{
    public class N11V2 : IMarketplaceV2, IMarketplaceV2CreateProductUntrackable, IMarketplaceV2UpdateProductUntrackable, IMarketplaceV2StockPriceUntrackable, IMarketplaceV2CategoryGetable
    {
        #region Constants
        #endregion

        #region Fields
        #endregion

        #region Constructors
        public N11V2()
        {
            #region Fields

            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.N11;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<UntrackableCreateProductProcessResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            N11ConfigurationV2 n11ConfigurationV2 = this.Configuration(request);

            var _productServicePortClient = new N11ProductService.ProductServicePortClient();
            var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

            var getCategoryAttributesIdRequest = new N11CategoryService.GetCategoryAttributesIdRequest
            {
                auth = new N11CategoryService.Authentication
                {
                    appKey = n11ConfigurationV2.AppKey,
                    appSecret = n11ConfigurationV2.AppSecret
                }
            };

            foreach (var piLoop in request.Items)
            {
                getCategoryAttributesIdRequest.categoryId = Convert.ToInt64(piLoop.CategoryCode);
                var categoryAttributes = await n11CategoryService.GetCategoryAttributesIdAsync(getCategoryAttributesIdRequest);
                var categoryProductAttributeList = categoryAttributes.GetCategoryAttributesIdResponse.categoryProductAttributeList.FirstOrDefault(x => x.name == "Marka");

                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var saveProductRequest = new N11ProductService.SaveProductRequest
                    {
                        auth = new N11ProductService.Authentication
                        {
                            appKey = n11ConfigurationV2.AppKey,
                            appSecret = n11ConfigurationV2.AppSecret
                        }
                    };

                    var subtitle = pinLoop.Subtitle;
                    var title = pinLoop.ProductInformationName + " -" + piLoop.SellerCode;
                    if (title.Length > 65)
                    {
                        title = title.Substring(0, 65);
                    }

                    if (subtitle.Length < 3)
                    {
                        subtitle = title;
                    }

                    saveProductRequest.product = new N11ProductService.ProductRequest();
                    saveProductRequest.product.productSellerCode = piLoop.SellerCode;
                    saveProductRequest.product.sellerNote = title;
                    saveProductRequest.product.title = title;
                    saveProductRequest.product.subtitle = subtitle;
                    saveProductRequest.product.description = pinLoop.Description;
                    saveProductRequest.product.category = new N11ProductService.CategoryRequest() { id = Convert.ToInt64(piLoop.CategoryCode) };

                    saveProductRequest.product.domestic = true;
                    saveProductRequest.product.currencyType = "1"; //productItem.CurrencyCode
                    saveProductRequest.product.images = pinLoop.ProductInformationPhotos.Select(im => new N11ProductService.ProductImage
                    {
                        order = (pinLoop.ProductInformationPhotos.IndexOf(im) + 1).ToString(),
                        url = im.Url
                    }).ToArray();
                    saveProductRequest.product.approvalStatus = "1";
                    saveProductRequest.product.productCondition = "1";
                    saveProductRequest.product.shipmentTemplate = n11ConfigurationV2.ShipmentTemplate;



                    var discount = Convert.ToDecimal(pinLoop.ListUnitPrice) - Convert.ToDecimal(pinLoop.UnitPrice);
                    var productPrice = discount > 0 ? pinLoop.ListUnitPrice : pinLoop.UnitPrice;

                    if (discount > 0)
                        saveProductRequest.product.discount = new N11ProductService.ProductDiscountRequest
                        {
                            type = "1",
                            value = discount < 0 ? "0" : discount.ToString()

                        };



                    saveProductRequest.product.price = productPrice;

                    var productAttributeRequests = new List<N11ProductService.ProductAttributeRequest>();


                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        var productAttributeRequest = new N11ProductService.ProductAttributeRequest()
                        {
                            name = attribute.MarketplaceVarinatName,
                            value = attribute.MarketplaceVariantValueName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }
                    if (categoryProductAttributeList != null && categoryProductAttributeList.mandatory)
                    {
                        var productAttributeRequest = new N11ProductService.ProductAttributeRequest()
                        {
                            name = "Marka",
                            value = piLoop.BrandName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }

                    saveProductRequest.product.attributes = productAttributeRequests.ToArray();
                    saveProductRequest.product.preparingDay = piLoop.DeliveryDay.ToString();

                    var productSkuRequests = new List<N11ProductService.ProductSkuRequest>();



                    var rnd = new Random();
                    var n11CatalogId = rnd.Next(1000, 999999999);

                    var productSkuRequest = new N11ProductService.ProductSkuRequest();
                    productSkuRequest.gtin = pinLoop.Barcode;
                    productSkuRequest.n11CatalogId = n11CatalogId;
                    productSkuRequest.quantity = pinLoop.Stock.ToString();
                    productSkuRequest.sellerStockCode = pinLoop.StockCode;
                    productSkuRequest.optionPrice = productPrice;
                    productSkuRequest.images = new N11ProductService.ProductImage[pinLoop.ProductInformationPhotos.Count];

                    for (int i = 0; i < pinLoop.ProductInformationPhotos.Count; i++)
                    {
                        productSkuRequest.images[i] = new N11ProductService.ProductImage
                        {
                            order = (i + 1).ToString(),
                            url = pinLoop.ProductInformationPhotos[i].Url
                        };
                    }

                    var stProductAttributeRequests = new List<N11ProductService.ProductAttributeRequest>();

                    foreach (var attr in pinLoop.ProductInformationMarketplaceVariantValues.Where(x => x.Varinater))
                    {
                        var productAttribute = new N11ProductService.ProductAttributeRequest()
                        {
                            name = attr.MarketplaceVarinatName,
                            value = attr.MarketplaceVariantValueName
                        };

                        stProductAttributeRequests.Add(productAttribute);
                    }

                    productSkuRequest.attributes = stProductAttributeRequests.ToArray();
                    productSkuRequests.Add(productSkuRequest);
                    saveProductRequest.product.stockItems = productSkuRequests.ToArray();
                    var success = false;
                    var failed = false;
                    var apiResponse = await _productServicePortClient.SaveProductAsync(saveProductRequest);

                    if (apiResponse.SaveProductResponse.result.status == "failure")
                        return new()
                        {
                            HttpStatusCode = System.Net.HttpStatusCode.OK,
                            Content = new()
                            {
                                UntrackableProcessResponseItems = request
                                    .Items
                                    .SelectMany(i => i
                                        .ProductInformationMarketplaces
                                        .Select(pim => new UntrackableCreateProductProcessResponseItem
                                        {
                                            Success = false,
                                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                                            Messages = new List<string>() { apiResponse.SaveProductResponse.result.errorMessage }
                                        }))
                                    .ToList()
                            }
                        };

                    if (apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.notAvailableForUpdateForSixtySeconds")
                    {
                        Thread.Sleep(new TimeSpan(0, 0, 10));
                        apiResponse = _productServicePortClient.SaveProductAsync(saveProductRequest).Result;
                    }

                    var status = apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.catalog.suggest.failed.alreadySuggested" || apiResponse.SaveProductResponse.result.errorCode == "stockItemsCannotBeUpdated" || apiResponse.SaveProductResponse.result.errorCode == "PRODUCT.categoryUpdateNotAllowed";

                    success = apiResponse.SaveProductResponse.result.status == "success" || status;

                    if (failed == false && apiResponse.SaveProductResponse.result.status == "failure")
                        failed = !status;
                }
            }

            n11CategoryService.InnerChannel.Close();
            _productServicePortClient.InnerChannel.Close();


            return new HttpHelperV3Response<UntrackableCreateProductProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new UntrackableCreateProductProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new UntrackableCreateProductProcessResponseItem
                        {
                            Success = true,
                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId
                        })).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            N11ConfigurationV2 n11ConfigurationV2 = this.Configuration(request);

            var _productServicePortClient = new N11ProductService.ProductServicePortClient();
            var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

            var getCategoryAttributesIdRequest = new N11CategoryService.GetCategoryAttributesIdRequest
            {
                auth = new N11CategoryService.Authentication
                {
                    appKey = n11ConfigurationV2.AppKey,
                    appSecret = n11ConfigurationV2.AppSecret
                }
            };

            foreach (var piLoop in request.Items)
            {
                getCategoryAttributesIdRequest.categoryId = Convert.ToInt64(piLoop.CategoryCode);
                var categoryAttributes = n11CategoryService.GetCategoryAttributesIdAsync(getCategoryAttributesIdRequest).Result;
                var categoryProductAttributeList = categoryAttributes.GetCategoryAttributesIdResponse.categoryProductAttributeList.FirstOrDefault(x => x.name == "Marka");

                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var saveProductRequest = new N11ProductService.SaveProductRequest
                    {
                        auth = new N11ProductService.Authentication
                        {
                            appKey = n11ConfigurationV2.AppKey,
                            appSecret = n11ConfigurationV2.AppSecret
                        }
                    };

                    var subtitle = pinLoop.Subtitle;
                    var title = pinLoop.ProductInformationName;
                    if (title.Length > 65)
                    {
                        title = title.Substring(0, 65);
                    }

                    if (subtitle.Length < 3)
                    {
                        subtitle = title;
                    }

                    saveProductRequest.product = new N11ProductService.ProductRequest();
                    saveProductRequest.product.productSellerCode = pinLoop.StockCode;
                    saveProductRequest.product.title = title;
                    saveProductRequest.product.sellerNote = title;
                    saveProductRequest.product.subtitle = subtitle;
                    saveProductRequest.product.description = pinLoop.Description;
                    saveProductRequest.product.category = new N11ProductService.CategoryRequest() { id = Convert.ToInt64(piLoop.CategoryCode) };

                    saveProductRequest.product.domestic = true;
                    saveProductRequest.product.currencyType = "1"; //productItem.CurrencyCode
                    saveProductRequest.product.images = pinLoop.ProductInformationPhotos.Select(im => new N11ProductService.ProductImage
                    {
                        order = (pinLoop.ProductInformationPhotos.IndexOf(im) + 1).ToString(),
                        url = im.Url
                    }).ToArray();
                    saveProductRequest.product.approvalStatus = "1";
                    saveProductRequest.product.productCondition = "1";
                    saveProductRequest.product.shipmentTemplate = n11ConfigurationV2.ShipmentTemplate;



                    var discount = Convert.ToDecimal(pinLoop.ListUnitPrice) - Convert.ToDecimal(pinLoop.UnitPrice);
                    var productPrice = discount > 0 ? pinLoop.ListUnitPrice : pinLoop.UnitPrice;

                    saveProductRequest.product.discount = new N11ProductService.ProductDiscountRequest
                    {
                        type = "1",
                        value = discount < 0 ? "0" : discount.ToString()

                    };



                    saveProductRequest.product.price = productPrice;

                    var productAttributeRequests = new List<N11ProductService.ProductAttributeRequest>();


                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        var productAttributeRequest = new N11ProductService.ProductAttributeRequest()
                        {
                            name = attribute.MarketplaceVarinatName,
                            value = attribute.MarketplaceVariantValueName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }
                    if (categoryProductAttributeList != null && categoryProductAttributeList.mandatory)
                    {
                        var productAttributeRequest = new N11ProductService.ProductAttributeRequest()
                        {
                            name = "Marka",
                            value = piLoop.BrandName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }

                    saveProductRequest.product.attributes = productAttributeRequests.ToArray();
                    saveProductRequest.product.preparingDay = piLoop.DeliveryDay.ToString();

                    var productSkuRequests = new List<N11ProductService.ProductSkuRequest>();



                    var rnd = new Random();
                    var n11CatalogId = rnd.Next(1000, 999999999);

                    var productSkuRequest = new N11ProductService.ProductSkuRequest();
                    productSkuRequest.gtin = pinLoop.Barcode;
                    productSkuRequest.n11CatalogId = n11CatalogId;
                    productSkuRequest.quantity = pinLoop.Stock.ToString();
                    productSkuRequest.sellerStockCode = pinLoop.StockCode;
                    productSkuRequest.optionPrice = productPrice;
                    productSkuRequest.images = new N11ProductService.ProductImage[pinLoop.ProductInformationPhotos.Count];

                    for (int i = 0; i < pinLoop.ProductInformationPhotos.Count; i++)
                    {
                        productSkuRequest.images[i] = new N11ProductService.ProductImage
                        {
                            order = (i + 1).ToString(),
                            url = pinLoop.ProductInformationPhotos[i].Url
                        };
                    }

                    var stProductAttributeRequests = new List<N11ProductService.ProductAttributeRequest>();

                    foreach (var attr in pinLoop.ProductInformationMarketplaceVariantValues.Where(x => x.Varinater))
                    {
                        var productAttribute = new N11ProductService.ProductAttributeRequest()
                        {
                            name = attr.MarketplaceVarinatName,
                            value = attr.MarketplaceVariantValueName
                        };

                        stProductAttributeRequests.Add(productAttribute);
                    }

                    productSkuRequest.attributes = stProductAttributeRequests.ToArray();
                    productSkuRequests.Add(productSkuRequest);
                    saveProductRequest.product.stockItems = productSkuRequests.ToArray();
                    var success = false;
                    var failed = false;
                    var apiResponse = _productServicePortClient.SaveProductAsync(saveProductRequest).Result;

                    if (apiResponse.SaveProductResponse.result.status == "failure")
                    {
                        piLoop.ErrorMessage = apiResponse.SaveProductResponse.result.errorMessage;
                        request.ErrorItems.Add(piLoop);
                        continue;
                    }

                    if (apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.notAvailableForUpdateForSixtySeconds")
                    {
                        Thread.Sleep(new TimeSpan(0, 0, 10));
                        apiResponse = _productServicePortClient.SaveProductAsync(saveProductRequest).Result;
                    }

                    var status = apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.catalog.suggest.failed.alreadySuggested" || apiResponse.SaveProductResponse.result.errorCode == "stockItemsCannotBeUpdated" || apiResponse.SaveProductResponse.result.errorCode == "PRODUCT.categoryUpdateNotAllowed";

                    success = apiResponse.SaveProductResponse.result.status == "success" || status;

                    if (failed == false && apiResponse.SaveProductResponse.result.status == "failure")
                        failed = !status;
                }
            }

            n11CategoryService.InnerChannel.Close();
            _productServicePortClient.InnerChannel.Close();

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new UntrackableProcessResponseItem
                        {
                            Success = true,
                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId
                        })).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            N11ConfigurationV2 n11ConfigurationV2 = this.Configuration(request);

            var item = request.Items.FirstOrDefault();

            #region Get Stock Item Id

            var productServicePortClient = new N11ProductService.ProductServicePortClient();

            long productId = 0;

            long.TryParse(item.ProductInformationUUId, out productId);

            if (productId == 0)
            {
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = false,
                            Messages = new List<string>
                            {
                             "Ürün şuanda N11 sisteminde kapalı gözükmektedir. Lütfen ürün N11 sisteminde açılınca tekrardan fiyat güncelleyiniz."
                            }
                        })
                            .ToList()
                    }
                };
            }

            await productServicePortClient.OpenAsync();

            var getProductBySellerCodeResult = await productServicePortClient
                .GetProductByProductIdAsync(new()
                {
                    auth = new()
                    {
                        appKey = n11ConfigurationV2.AppKey,
                        appSecret = n11ConfigurationV2.AppSecret
                    },
                    productId = productId
                });

            if (getProductBySellerCodeResult.GetProductByProductIdResponse.result.status == "failure")
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                            .Items
                            .Select(i => new UntrackableProcessResponseItem
                            {
                                ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                Success = false,
                                Messages = new List<string>
                                {
                                    getProductBySellerCodeResult.GetProductByProductIdResponse.result.errorMessage
                                }
                            })
                            .ToList()
                    }
                };

            if (getProductBySellerCodeResult.GetProductByProductIdResponse.product == null ||
                getProductBySellerCodeResult.GetProductByProductIdResponse.product.id == 0)
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                            .Items
                            .Select(i => new UntrackableProcessResponseItem
                            {
                                ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                Success = false,
                                Messages = new List<string>
                                {
                                    "Ürün bulunamadı."
                                }
                            })
                            .ToList()
                    }
                };

            var id = getProductBySellerCodeResult.GetProductByProductIdResponse.product.id;
            #endregion


            if (getProductBySellerCodeResult.GetProductByProductIdResponse != null && getProductBySellerCodeResult.GetProductByProductIdResponse.result.status.ToLower() == "success" && getProductBySellerCodeResult.GetProductByProductIdResponse.product.price != item.ListUnitPrice)
            {

                var updateProductPriceByIdRequest = new N11ProductService.UpdateProductPriceByIdRequest
                {
                    auth = new()
                    {
                        appKey = n11ConfigurationV2.AppKey,
                        appSecret = n11ConfigurationV2.AppSecret
                    },
                    productId = id,
                    currencyType = "TL",
                    price = item.ListUnitPrice

                };
                var responseUpdateProductPriceById = await productServicePortClient.UpdateProductPriceByIdAsync(updateProductPriceByIdRequest);

                if (responseUpdateProductPriceById != null && responseUpdateProductPriceById.UpdateProductPriceByIdResponse.result.errorCode == "SELLER_API.notAvailableForUpdateForParamSeconds")
                {

                    Thread.Sleep(new TimeSpan(0, 0, 22));
                    responseUpdateProductPriceById = await productServicePortClient.UpdateProductPriceByIdAsync(updateProductPriceByIdRequest);
                }
;

                if (responseUpdateProductPriceById != null && responseUpdateProductPriceById.UpdateProductPriceByIdResponse.result.status.ToLower() != "success")
                {
                    return new HttpHelperV3Response<UntrackableProcessResponse>
                    {
                        HttpStatusCode = System.Net.HttpStatusCode.OK,
                        Content = new()
                        {
                            UntrackableProcessResponseItems = request
                                .Items
                                .Select(i => new UntrackableProcessResponseItem
                                {
                                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                    Success = false,
                                    Messages = new List<string>
                                    {
                                        responseUpdateProductPriceById.UpdateProductPriceByIdResponse.result.errorMessage
                                    }
                                })
                                .ToList()
                        }
                    };
                }

            }



            var updateProductBasicRequest = new N11ProductService.UpdateProductBasicRequest
            {
                auth = new()
                {
                    appKey = n11ConfigurationV2.AppKey,
                    appSecret = n11ConfigurationV2.AppSecret
                },
                productId = id
            };

            var discount = item.ListUnitPrice - item.UnitPrice;

            updateProductBasicRequest.price = discount > 0 ? item.ListUnitPrice : item.UnitPrice;
            updateProductBasicRequest.productDiscount = new()
            {
                discountType = "1",
                discountValue = discount < 0 ? 0 : Convert.ToDouble(discount)
            };


            var updateProductBasicResponse = await productServicePortClient.UpdateProductBasicAsync(updateProductBasicRequest);

            if (updateProductBasicResponse.UpdateProductBasicResponse != null && updateProductBasicResponse.UpdateProductBasicResponse.result.errorCode == "SELLER_API.notAvailableForUpdateForParamSeconds")
            {
                Thread.Sleep(new TimeSpan(0, 0, 22));

                updateProductBasicResponse = await productServicePortClient.UpdateProductBasicAsync(updateProductBasicRequest);
            }

            await productServicePortClient.CloseAsync();

            if (updateProductBasicResponse != null && updateProductBasicResponse.UpdateProductBasicResponse.result.status.ToLower() != "success")
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                                .Items
                                .Select(i => new UntrackableProcessResponseItem
                                {
                                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                    Success = false,
                                    Messages = new List<string>
                                    {
                                        updateProductBasicResponse.UpdateProductBasicResponse.result.errorMessage
                                    }
                                })
                                .ToList()
                    }
                };

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new()
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ListUnitPrice = i.ListUnitPrice,
                            UnitPrice = i.UnitPrice,
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = true
                        }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            N11ConfigurationV2 n11ConfigurationV2 = this.Configuration(request);

            var item = request.Items.FirstOrDefault();

            #region Get Stock Item Id
            long productId = 0;

            long.TryParse(item.ProductInformationUUId, out productId);

            if (productId == 0)
            {
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = false,
                            Messages = new List<string>
                            {
                             "Ürün şuanda N11 sisteminde kapalı gözükmektedir. Lütfen ürün N11 sisteminde açılınca tekrardan stok güncelleyiniz."
                            }
                        })
                            .ToList()
                    }
                };
            }

            var productServicePortClient = new N11ProductService.ProductServicePortClient();

            await productServicePortClient.OpenAsync();

            var getProductBySellerCodeResult = await productServicePortClient
                .GetProductByProductIdAsync(new()
                {
                    auth = new()
                    {
                        appKey = n11ConfigurationV2.AppKey,
                        appSecret = n11ConfigurationV2.AppSecret
                    },
                    productId = productId
                });

            await productServicePortClient.CloseAsync();

            if (getProductBySellerCodeResult.GetProductByProductIdResponse.result.status.ToLower() == "failure")
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                            .Items
                            .Select(i => new UntrackableProcessResponseItem
                            {
                                ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                Success = false,
                                Messages = new List<string>
                                {
                                    getProductBySellerCodeResult.GetProductByProductIdResponse.result.errorMessage
                                }
                            })
                            .ToList()
                    }
                };

            if (getProductBySellerCodeResult.GetProductByProductIdResponse.product.stockItems.stockItem == null)
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new()
                    {
                        UntrackableProcessResponseItems = request
                            .Items
                            .Select(i => new UntrackableProcessResponseItem
                            {
                                ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                Success = false,
                                Messages = new List<string>
                                {
                                    "Ürün N11'de bulunamadı."
                                }
                            })
                            .ToList()
                    }
                };

            var id = getProductBySellerCodeResult.GetProductByProductIdResponse.product.stockItems.stockItem[0].id;
            #endregion

            var productStockServicePortClient = new N11ProductStockService.ProductStockServicePortClient();

            await productStockServicePortClient.OpenAsync();

            var updateStockByStockIdResponse = await productStockServicePortClient.UpdateStockByStockIdAsync(new()
            {
                auth = new()
                {
                    appKey = n11ConfigurationV2.AppKey,
                    appSecret = n11ConfigurationV2.AppSecret
                },
                stockItems = new N11ProductStockService.StockItemForUpdateStockWithId[1]
                {
                    new ()
                    {
                        id = id,
                        quantity = item.Stock.ToString(),
                        delist = false
                    }
                }
            });

            productStockServicePortClient.Close();

            if (updateStockByStockIdResponse.UpdateStockByStockIdResponse.result.status.ToLower() != "success")
                return new HttpHelperV3Response<UntrackableProcessResponse>
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK,
                    Content = new UntrackableProcessResponse
                    {
                        UntrackableProcessResponseItems = request
                                .Items
                                .Select(i => new UntrackableProcessResponseItem
                                {
                                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                    Success = false,
                                    Messages = new List<string>
                                    {
                                        updateStockByStockIdResponse.UpdateStockByStockIdResponse.result.errorMessage
                                    }
                                })
                                .ToList()
                    }
                };

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = true,
                            Stock = i.Stock
                        })
                        .ToList()
                }
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            return requests
                .SelectMany(r =>
                    r.Items.SelectMany(i =>
                        i.ProductInformationMarketplaces.Select(pim => new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                            Items = new List<MarketplaceRequestCreateProductItem>
                            {
                                new MarketplaceRequestCreateProductItem
                                {
                                    BrandCode = i.BrandCode,
                                    BrandName = i.BrandName,
                                    CategoryCode = i.CategoryCode,
                                    CategoryName= i.CategoryName,
                                    DeliveryDay= i.DeliveryDay,
                                    DimensionalWeight= i.DimensionalWeight,
                                    ProductId= i.ProductId,
                                    ProductMarketplaceId= i.ProductMarketplaceId,
                                    ProductMarketplaceUUId= i.ProductMarketplaceUUId,
                                    ProductName= i.ProductName,
                                    SellerCode= i.SellerCode,
                                    ErrorMessage= i.ErrorMessage,
                                    ProductInformationMarketplaces = new List<ProductInformationMarketplace>
                                    {
                                        pim
                                    }
                                }
                            }
                        })))
                .ToList();
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            return requests
                .SelectMany(r =>
                    r.Items.SelectMany(i =>
                        i.ProductInformationMarketplaces.Select(pim => new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                            Items = new List<MarketplaceRequestUpdateProductItem>
                            {
                                new MarketplaceRequestUpdateProductItem
                                {
                                    BrandId = i.BrandId,
                                    BrandName = i.BrandName,
                                    CategoryCode = i.CategoryCode,
                                    CategoryName= i.CategoryName,
                                    DeliveryDay= i.DeliveryDay,
                                    DimensionalWeight= i.DimensionalWeight,
                                    ProductId= i.ProductId,
                                    ProductMarketplaceId= i.ProductMarketplaceId,
                                    ProductMarketplaceUUId= i.ProductMarketplaceUUId,
                                    ProductName= i.ProductName,
                                    SellerCode= i.SellerCode,
                                    ErrorMessage= i.ErrorMessage,
                                    ProductInformationMarketplaces = new List<ProductInformationMarketplace>
                                    {
                                        pim
                                    }
                                }
                            }
                        })))
                .ToList();
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            return requests
                .SelectMany(r => r.Items.Select(i => new MarketplaceRequestUpdatePrice
                {
                    Id = Guid.NewGuid(),
                    MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                    Items = new List<MarketplaceRequestUpdatePriceItem> { i }
                }))
                .ToList();
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            return requests
                .SelectMany(r => r.Items.Select(i => new MarketplaceRequestUpdateStock
                {
                    Id = Guid.NewGuid(),
                    MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                    Items = new List<MarketplaceRequestUpdateStockItem> { i }
                }))
                .ToList();
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            N11ConfigurationV2 n11ConfigurationV2 = this.Configuration(request);

            List<Category> categories = new();

            var client = new CategoryServicePortClient();

            await client.OpenAsync();

            var getTopLevelCategoriesResponse = await client.GetTopLevelCategoriesAsync(new GetTopLevelCategoriesRequest
            {
                auth = new Authentication
                {
                    appKey = n11ConfigurationV2.AppKey,
                    appSecret = n11ConfigurationV2.AppSecret
                },
            });

            await this.FetchSubCategories(n11ConfigurationV2, client, getTopLevelCategoriesResponse.GetTopLevelCategoriesResponse.categoryList, null, categories);

            categories.ForEach(async theCategory =>
            {
                var getCategoryAttributesIdResponse = await client.GetCategoryAttributesIdAsync(new()
                {
                    auth = new()
                    {
                        appKey = n11ConfigurationV2.AppKey,
                        appSecret = n11ConfigurationV2.AppSecret
                    },
                    categoryId = long.Parse(theCategory.Code)
                });

                foreach (var theCategoryProductAttribute in getCategoryAttributesIdResponse.GetCategoryAttributesIdResponse.categoryProductAttributeList)
                {
                    List<CategoryProductAttributeValueData> categoryAttributeValueDatas = new();
                    PagingData pagingData;
                    do
                    {
                        var getCategoryAttributeValueResponse1 = await client.GetCategoryAttributeValueAsync(new()
                        {
                            auth = new()
                            {
                                appKey = n11ConfigurationV2.AppKey,
                                appSecret = n11ConfigurationV2.AppSecret
                            },
                            categoryId = long.Parse(theCategory.Code),
                            categoryProductAttributeId = theCategoryProductAttribute.id
                        });

                        pagingData = getCategoryAttributeValueResponse1.GetCategoryAttributeValueResponse.pagingData;

                        categoryAttributeValueDatas.AddRange(getCategoryAttributeValueResponse1
                            .GetCategoryAttributeValueResponse
                            .categoryProductAttributeValueList);

                    } while (pagingData.currentPage > pagingData.pageCount);

                    theCategory.CategoryAttributes.Add(new CategoryAttribute
                    {
                        Code = theCategoryProductAttribute.id.ToString(),
                        Name = theCategoryProductAttribute.name,
                        Mandatory = theCategoryProductAttribute.mandatory,
                        Variantable = theCategoryProductAttribute.variant,
                        MultiValue = theCategoryProductAttribute.multipleSelect,
                        CategoryCode = theCategory.Code,
                        CategoryAttributesValues = categoryAttributeValueDatas
                            .Select(cpavl => new CategoryAttributeValue
                            {
                                Code = cpavl.id.ToString(),
                                Name = cpavl.name,
                                VarinatCode = theCategoryProductAttribute.id.ToString(),
                            })
                            .ToList()
                    });
                }
            });

            await client.CloseAsync();

            return categories;
        }
        #endregion

        #region Helper Methods
        private N11ConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private N11ConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }

        private async Task FetchSubCategories(N11ConfigurationV2 n11ConfigurationV2, CategoryServicePortClient client, N11CategoryService.SubCategory[] subCategories, long? parentCategoryId, List<Category> categories)
        {
            foreach (var theSubCategory in subCategories)
            {
                var getSubCategoriesResponse = await client.GetSubCategoriesAsync(new N11CategoryService.GetSubCategoriesRequest
                {
                    auth = new N11CategoryService.Authentication
                    {
                        appKey = n11ConfigurationV2.AppKey,
                        appSecret = n11ConfigurationV2.AppSecret
                    },
                    categoryId = theSubCategory.id
                });

                if (getSubCategoriesResponse.GetSubCategoriesResponse.category.Any(c => c.subCategoryList != null))
                    foreach (var theCategory in getSubCategoriesResponse.GetSubCategoriesResponse.category)
                        await this.FetchSubCategories(n11ConfigurationV2, client, theCategory.subCategoryList, theSubCategory.id, categories);
                else
                    categories.Add(new Category
                    {
                        Code = theSubCategory.id.ToString(),
                        Name = theSubCategory.name,
                        ParentCategoryCode = parentCategoryId.ToString()
                    });
            }
        }
        #endregion
    }
}