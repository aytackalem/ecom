﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.N11
{
    public class N11ConfigurationV2
    {
        #region Members
        public static implicit operator N11ConfigurationV2(Dictionary<string, string> configurations)
        {
            return new N11ConfigurationV2
            {
                AppSecret = configurations["AppSecret"],
                AppKey = configurations["AppKey"],
                ShipmentTemplate = configurations["ShipmentTemplate"]
            };
        }
        #endregion

        #region Properties
        public string AppSecret { get; internal set; }

        public string AppKey { get; internal set; }

        public string ShipmentTemplate { get; internal set; }
        #endregion
    }
}
