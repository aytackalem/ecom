﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Batch
{
    public class BatchProduct
    {
        #region Properties
        public string Barcode { get; set; }
        #endregion
    }
}
