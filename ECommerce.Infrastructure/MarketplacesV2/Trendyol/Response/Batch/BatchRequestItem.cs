﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Batch
{
    public class BatchRequestItem
    {
        #region Properties
        public string status { get; set; }

        public string Barcode { get; set; }

        public List<string> failureReasons { get; set; }
        #endregion

        #region Navigation Properties
        public BatchProduct Product { get; set; }
        #endregion
    }
}
