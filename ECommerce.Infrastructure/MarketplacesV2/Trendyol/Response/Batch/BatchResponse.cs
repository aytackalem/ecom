﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Batch
{
    public class BatchResponse
    {
        #region Properties
        public string batchRequestId { get; set; }

        public string status { get; set; }
        #endregion

        #region Navigation Properties
        public List<BatchItem> items { get; set; }
        #endregion
    }
}
