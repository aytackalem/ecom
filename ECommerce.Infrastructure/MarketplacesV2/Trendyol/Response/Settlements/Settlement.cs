﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Settlements
{
    public sealed class Settlement
    {
        public string id { get; set; }
        public long transactionDate { get; set; }
        public string barcode { get; set; }
        public string transactionType { get; set; }
        public int receiptId { get; set; }
        public string description { get; set; }
        public float debt { get; set; }
        public float credit { get; set; }
        public int paymentPeriod { get; set; }
        public float commissionRate { get; set; }
        public decimal commissionAmount { get; set; }
        public string commissionInvoiceSerialNumber { get; set; }
        public decimal sellerRevenue { get; set; }
        public string orderNumber { get; set; }
        public int? paymentOrderId { get; set; }
        public long paymentDate { get; set; }
        public int sellerId { get; set; }
        public object storeId { get; set; }
        public object storeName { get; set; }
        public object storeAddress { get; set; }
        public string country { get; set; }
    }
}
