﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.CargoInvoices
{
    public sealed class CargoInvoice
    {
        public string shipmentPackageType { get; set; }
        public long parcelUniqueId { get; set; }
        public string orderNumber { get; set; }
        public decimal amount { get; set; }
        public int desi { get; set; }
    }
}
