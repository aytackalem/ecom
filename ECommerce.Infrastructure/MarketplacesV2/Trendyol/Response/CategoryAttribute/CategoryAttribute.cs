﻿using System.Collections.Generic;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Category;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.CategoryAttribute
{
    public class CategoryAttribute
    {
        #region Properties
        public bool AllowCustom { get; set; }

        public int CategoryId { get; set; }

        public bool Required { get; set; }

        public bool Varianter { get; set; }

        public bool Slicer { get; set; }
        #endregion

        #region Navigation Properties
        public Attribute Attribute { get; set; }

        public List<AttributeValue> AttributeValues { get; set; }
        #endregion
    }
}
