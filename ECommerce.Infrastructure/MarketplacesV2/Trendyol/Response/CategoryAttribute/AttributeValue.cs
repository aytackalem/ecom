﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.CategoryAttribute
{
    public class AttributeValue
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
