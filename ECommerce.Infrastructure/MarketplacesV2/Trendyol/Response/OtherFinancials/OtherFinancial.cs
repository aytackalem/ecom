﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.OtherFinancials
{
    public sealed class OtherFinancial
    {
        public string id { get; set; }
        public long transactionDate { get; set; }
        public object barcode { get; set; }
        public string transactionType { get; set; }
        public object receiptId { get; set; }
        public string description { get; set; }
        public float debt { get; set; }
        public float credit { get; set; }
        public object paymentPeriod { get; set; }
        public object commissionRate { get; set; }
        public object commissionAmount { get; set; }
        public string commissionInvoiceSerialNumber { get; set; }
        public object sellerRevenue { get; set; }
        public object orderNumber { get; set; }
        public int? paymentOrderId { get; set; }
        public long? paymentDate { get; set; }
        public int sellerId { get; set; }
        public object storeId { get; set; }
        public object storeName { get; set; }
        public object storeAddress { get; set; }
        public object country { get; set; }
    }
}
