﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Category
{
    public class CategoryResponse
    {
        #region Navigation Properties
        public List<SubCategory> Categories { get; set; } 
        #endregion
    }
}
