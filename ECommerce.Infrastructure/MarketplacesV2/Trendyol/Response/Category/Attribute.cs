﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Category
{
    public class Attribute
    {
        #region Properties
        public int Id { get; set; }
        
        public string Name { get; set; } 
        #endregion
    }
}
