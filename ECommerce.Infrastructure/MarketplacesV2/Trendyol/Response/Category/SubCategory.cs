﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Category
{
    public class SubCategory
    {
        #region Properties
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public int? ParentId { get; set; }
        
        public List<SubCategory> SubCategories { get; set; } 
        #endregion
    }
}
