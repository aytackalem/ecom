﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Base
{
    public class ResponseBase<T>
    {
        public int Page { get; set; }

        public int Size { get; set; }

        public int TotalPages { get; set; }

        public int TotalElements { get; set; }

        public T[] Content { get; set; }
    }
}
