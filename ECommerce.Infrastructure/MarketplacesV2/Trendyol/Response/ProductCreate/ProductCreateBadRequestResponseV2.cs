﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.ProductCreate
{
    public class ProductCreateBadRequestResponseV2
    {
        #region Properties
        public long timestamp { get; set; }
        public string exception { get; set; }
        public List<Error> errors { get; set; }
        #endregion
    }

    public class Error
    {
        #region Properties
        public string key { get; set; }
        public string message { get; set; }
        public object errorCode { get; set; }
        public List<object> args { get; set; } 
        #endregion
    }
}
