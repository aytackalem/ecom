﻿namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.PriceStock
{
    public class PriceStockResponseV2
    {
        #region Properties
        public string BatchRequestId { get; set; }
        #endregion
    }
}
