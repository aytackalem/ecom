﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.ProductCreate
{
    public class ProductCreateRequestV2
    {
        #region Navigation Properties
        [JsonProperty("items")]
        public List<ProductCreateItemV2> Items { get; set; }
        #endregion
    }
}
