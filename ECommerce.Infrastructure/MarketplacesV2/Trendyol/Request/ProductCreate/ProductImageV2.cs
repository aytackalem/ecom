﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.ProductCreate
{
    public class ProductImageV2
    {
        #region Properties
        [JsonProperty("url")]
        public string Url { get; set; }
        #endregion
    }
}
