﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.ProductCreate
{
    public class ProductAttributeV2
    {
        #region Properties
        [JsonProperty("attributeId")]
        public string AttributeId { get; set; }

        [JsonProperty("attributeValueId")]
        public string AttributeValueId { get; set; }

        [JsonProperty("customAttributeValue")]
        public string CustomAttributeValue { get; set; }
        #endregion
    }
}
