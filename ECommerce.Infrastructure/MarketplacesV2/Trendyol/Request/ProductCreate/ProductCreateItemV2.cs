﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.ProductCreate
{
    public class ProductCreateItemV2
    {
        #region Properties
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("productMainId")]
        public string ProductMainId { get; set; }

        [JsonProperty("brandId")]
        public int BrandId { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("stockCode")]
        public string StockCode { get; set; }

        [JsonProperty("dimensionalWeight")]
        public int DimensionalWeight { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        [JsonProperty("listPrice")]
        public double ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public double SalePrice { get; set; }

        [JsonProperty("deliveryDuration")]
        public int DeliveryDuration { get; set; }

        [JsonProperty("images")]
        public List<ProductImageV2> Images { get; set; }

        [JsonProperty("vatRate")]
        public int VatRate { get; set; }

        [JsonProperty("cargoCompanyId")]
        public int CargoCompanyId { get; set; }

        [JsonProperty("attributes")]
        public List<ProductAttributeV2> Attributes { get; set; }

        [JsonProperty("currencyType")]
        public string CurrencyType { get; set; }
        #endregion
    }
}
