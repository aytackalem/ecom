﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.PriceStock
{
    public class PriceStockItemV2
    {
        #region Properties
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("listPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal? SalePrice { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
        #endregion
    }
}
