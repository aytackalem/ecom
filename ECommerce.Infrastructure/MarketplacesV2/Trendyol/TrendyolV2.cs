﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Commissions;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Shipments;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.Marketplaces.Ikas.Common;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.PriceStock;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Request.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Base;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.CargoInvoices;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Category;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.CategoryAttribute;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.OtherFinancials;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.PriceStock;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Trendyol.Response.Settlements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Category = ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories.Category;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol
{
    public class TrendyolV2 : 
        IMarketplaceV2, 
        IMarketplaceV2CreateProductTrackable, 
        IMarketplaceV2UpdateProductTrackable, 
        IMarketplaceV2StockPriceTrackable, 
        IMarketplaceV2CategoryGetable, 
        ICommission, 
        ICargo
    {
        #region Constants
        private const string _baseUrl = "https://api.trendyol.com/sapigw";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public TrendyolV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Trendyol;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        public async Task<HttpHelperV3Response<List<Commission>>> GetCommissions(DateTime startDate, DateTime endDate, Dictionary<string, string> marketplaceConfigurations)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(marketplaceConfigurations, false);

            HttpHelperV3Response<List<Commission>> httpHelperV3Response = new()
            {
                HttpStatusCode = HttpStatusCode.OK,
                Content = new()
            };

            var transactionTypes = new[] { "Sale", "Return", "ManualRefund", "ManualRefundCancel" };

            foreach (var theTransactionType in transactionTypes)
            {
                var page = 0;

                var url = $"https://api.trendyol.com/integration/finance/che/sellers/150051/settlements?endDate={endDate.ConvertToUnixTimeStamp()}&startDate={startDate.ConvertToUnixTimeStamp()}&transactionType={theTransactionType}&page={page}&size=500";

                MarketplacesV2.Trendyol.Response.Base.ResponseBase<Settlement> response = null;

                do
                {

                    var v3Response = await this._httpHelperV3.SendAsync<MarketplacesV2.Trendyol.Response.Base.ResponseBase<Settlement>>(new HttpHelperV3Request
                    {
                        RequestUri = url,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

                    if (v3Response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        httpHelperV3Response.HttpStatusCode = v3Response.HttpStatusCode;
                        return httpHelperV3Response;
                    }

                    response = v3Response.Content;

                    httpHelperV3Response.Content.AddRange(response.Content.Select(c => new Commission
                    {
                        Barcode = c.barcode,
                        CommissionAmount = c.commissionAmount,
                        CommissionRate = c.commissionRate,
                        OrderNumber = c.orderNumber,
                        Type = c.transactionType,
                        TransactionDate = c.transactionDate.ConvertToDateTime(),
                        PaymentPeriod = c.paymentPeriod,
                        SellerRevenue = c.sellerRevenue,
                        PaymentOrderId = c.paymentOrderId
                    }));

                    page++;
                } while (response.TotalPages > page);
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<List<Cargo>>> GetCargoFees(DateTime startDate, DateTime endDate, Dictionary<string, string> marketplaceConfigurations)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(marketplaceConfigurations, false);

            HttpHelperV3Response<List<Cargo>> httpHelperV3Response = new()
            {
                HttpStatusCode = HttpStatusCode.OK,
                Content = new()
            };

            var transactionTypes = new[] { "DeductionInvoices" };

            foreach (var theTransactionType in transactionTypes)
            {
                List<OtherFinancial> otherFinancials = new();
                ResponseBase<OtherFinancial> response = null;
                int page = 0;
                do
                {
                    var url = $"https://api.trendyol.com/integration/finance/che/sellers/150051/otherfinancials?endDate={endDate.ConvertToUnixTimeStamp()}&startDate={startDate.ConvertToUnixTimeStamp()}&transactionType={theTransactionType}&page={page}&size=500";

                    var v3Response = await this._httpHelperV3.SendAsync<ResponseBase<OtherFinancial>>(new HttpHelperV3Request
                    {
                        RequestUri = url,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

                    if (v3Response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        httpHelperV3Response.HttpStatusCode = v3Response.HttpStatusCode;
                        return httpHelperV3Response;
                    }

                    response = v3Response.Content;

                    otherFinancials.AddRange(response.Content);

                    page++;
                } while (response.TotalPages > page);

                foreach (var theOtherFinancial in otherFinancials)
                {
                    if (theOtherFinancial.transactionType == "Kargo Fatura")
                    {
                        ResponseBase<CargoInvoice> response2 = null;
                        page = 0;
                        do
                        {
                            var url = $"https://api.trendyol.com/integration/finance/che/sellers/150051/cargo-invoice/{theOtherFinancial.id}/items?page={page}";

                            var v3Response = await this._httpHelperV3.SendAsync<ResponseBase<CargoInvoice>>(new HttpHelperV3Request
                            {
                                RequestUri = url,
                                HttpMethod = HttpMethod.Get,
                                AuthorizationType = AuthorizationType.Basic,
                                Authorization = trendyolConfigurationV2.Authorization
                            });

                            if (v3Response.HttpStatusCode != HttpStatusCode.OK)
                            {
                                httpHelperV3Response.HttpStatusCode = v3Response.HttpStatusCode;
                                return httpHelperV3Response;
                            }

                            response2 = v3Response.Content;

                            httpHelperV3Response.Content.AddRange(response2.Content.Select(c => new Cargo
                            {
                                Amount = c.amount,
                                Desi = c.desi,
                                OrderNumber = c.orderNumber,
                                Type = c.shipmentPackageType
                            }));

                            page++;
                        } while (response2.TotalPages > page);
                    }
                }
            }

            return httpHelperV3Response;
        }

        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var productCreateRequestV2 = new ProductCreateRequestV2
            {
                Items = new List<ProductCreateItemV2>()
            };

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new ProductCreateItemV2
                    {
                        Barcode = pinLoop.Barcode,
                        Title = pinLoop.ProductInformationName,
                        ProductMainId = piLoop.SellerCode, // Guid.NewGuid().ToString();//
                        BrandId = Convert.ToInt32(piLoop.BrandCode), //bakılacak
                        CategoryId = Convert.ToInt32(piLoop.CategoryCode),
                        Quantity = pinLoop.Stock,
                        StockCode = pinLoop.StockCode,
                        Description = pinLoop.Description,
                        CurrencyType = "TRL",
                        ListPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        SalePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        VatRate = Convert.ToInt32(pinLoop.VatRate * 100)
                    };
                    item.Description = pinLoop.Description;
                    item.DeliveryDuration = piLoop.DeliveryDay == 0 ? 2 : (piLoop.DeliveryDay > 9 ? 9 : piLoop.DeliveryDay);
                    item.CargoCompanyId = 11;//Trendyol express
                    item.DimensionalWeight = Convert.ToInt32(piLoop.DimensionalWeight);
                    item.Images = new List<ProductImageV2>();
                    item.Attributes = new List<ProductAttributeV2>();
                    foreach (var image in pinLoop.ProductInformationPhotos.Take(8))
                    {
                        item.Images.Add(new ProductImageV2
                        {
                            Url = image.Url
                        });
                    }

                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        if (attribute.AllowCustom)
                        {
                            item.Attributes.Add(new ProductAttributeV2
                            {
                                AttributeId = attribute.MarketplaceVarinatCode,
                                CustomAttributeValue = attribute.MarketplaceVariantValueName,
                                AttributeValueId = null
                            });
                        }
                        else
                        {
                            item.Attributes.Add(new ProductAttributeV2
                            {
                                AttributeId = attribute.MarketplaceVarinatCode,
                                AttributeValueId = attribute.MarketplaceVariantValueCode
                            });
                        }
                    }

                    productCreateRequestV2.Items.Add(item);
                }
            }

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<ProductCreateRequestV2, ProductCreateResponseV2, ProductCreateBadRequestResponseV2>(
                    new HttpHelperV3Request<ProductCreateRequestV2>
                    {
                        RequestUri = $"{_baseUrl}/suppliers/{trendyolConfigurationV2.SupplierId}/v2/products",
                        Request = productCreateRequestV2,
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.batchRequestId;

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = httpResultResponse.ContentError.errors.Select(e => e.message).ToList();

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var productupdateRequestV2 = new ProductCreateRequestV2
            {
                Items = new List<ProductCreateItemV2>()
            };

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new ProductCreateItemV2
                    {
                        Barcode = pinLoop.Barcode,
                        Title = pinLoop.ProductInformationName,
                        ProductMainId = piLoop.SellerCode, // Guid.NewGuid().ToString();//
                        BrandId = Convert.ToInt32(piLoop.BrandId), //bakılacak
                        CategoryId = Convert.ToInt32(piLoop.CategoryCode),
                        Quantity = pinLoop.Stock,
                        StockCode = pinLoop.StockCode,
                        Description = pinLoop.Description,
                        CurrencyType = "TRL",
                        ListPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        SalePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        VatRate = Convert.ToInt32(pinLoop.VatRate * 100)
                    };
                    item.Description = pinLoop.Description;
                    item.DeliveryDuration = piLoop.DeliveryDay == 0 ? 2 : (piLoop.DeliveryDay > 9 ? 9 : piLoop.DeliveryDay);
                    item.CargoCompanyId = 11;//Trendyol express
                    item.DimensionalWeight = Convert.ToInt32(piLoop.DimensionalWeight);
                    item.Images = new List<ProductImageV2>();
                    item.Attributes = new List<ProductAttributeV2>();
                    foreach (var image in pinLoop.ProductInformationPhotos)
                    {
                        item.Images.Add(new ProductImageV2
                        {
                            Url = image.Url
                        });
                    }

                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        if (attribute.AllowCustom)
                        {
                            item.Attributes.Add(new ProductAttributeV2
                            {
                                AttributeId = attribute.MarketplaceVarinatCode,
                                CustomAttributeValue = attribute.MarketplaceVariantValueName,
                                AttributeValueId = null
                            });
                        }
                        else
                        {
                            item.Attributes.Add(new ProductAttributeV2
                            {
                                AttributeId = attribute.MarketplaceVarinatCode,
                                AttributeValueId = attribute.MarketplaceVariantValueCode
                            });
                        }
                    }

                    productupdateRequestV2.Items.Add(item);
                }
            }

            var httpResultResponse = await this._httpHelperV3.SendAsync<ProductCreateRequestV2, ProductCreateResponseV2>(
                        new HttpHelperV3Request<ProductCreateRequestV2>
                        {
                            RequestUri = $"{_baseUrl}/suppliers/{trendyolConfigurationV2.SupplierId}/v2/products",
                            Request = productupdateRequestV2,
                            HttpMethod = HttpMethod.Put,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = trendyolConfigurationV2.Authorization
                        });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.batchRequestId
                    }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var priceStockRequestV2 = new PriceStockRequestV2
            {
                Items = request
                        .Items
                        .Select(dpi => new PriceStockItemV2
                        {
                            Barcode = dpi.Barcode,
                            Quantity = null,
                            ListPrice = dpi.ListUnitPrice,
                            SalePrice = dpi.UnitPrice
                        })
                    .ToList()
            };

            var requestUri = $"{_baseUrl}/suppliers/{trendyolConfigurationV2.SupplierId}/products/price-and-inventory";
            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceStockRequestV2, PriceStockResponseV2>(
                        new HttpHelperV3Request<PriceStockRequestV2>
                        {
                            RequestUri = requestUri,
                            Request = priceStockRequestV2,
                            HttpMethod = HttpMethod.Post,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = trendyolConfigurationV2.Authorization
                        });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.BatchRequestId }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var priceStockRequestV2 = new PriceStockRequestV2
            {
                Items = request
                        .Items
                        .Select(dpi => new PriceStockItemV2
                        {
                            Barcode = dpi.Barcode,
                            Quantity = dpi.Stock,
                            ListPrice = dpi.UpdatePrice ? dpi.ListUnitPrice : null,
                            SalePrice = dpi.UpdatePrice ? dpi.UnitPrice : null
                        })
                    .ToList()
            };

            var requestUri = $"{_baseUrl}/suppliers/{trendyolConfigurationV2.SupplierId}/products/price-and-inventory";

            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceStockRequestV2, PriceStockResponseV2>(
                new HttpHelperV3Request<PriceStockRequestV2>
                {
                    RequestUri = requestUri,
                    Request = priceStockRequestV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = trendyolConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.BatchRequestId }
                    : null
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Pazaryeri kategori bilgisi boş olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationName.Length > 100))
                    {
                        var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(pim => pim.ProductInformationName.Length > 100)
                        .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün ismi 100 karekterden fazla olmamalıdır."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationName.Length > 100);

                    }

                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Pazaryeri kategori bilgisi boş olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationName.Length > 100))
                    {
                        var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(pim => pim.ProductInformationName.Length > 100)
                        .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestUpdateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün ismi 100 karekterden fazla olmamalıdır."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationName.Length > 100);

                    }

                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestUpdateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceUpdateProductRequests.Add(new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var supplierId = trendyolConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"{_baseUrl}/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<MarketplacesV2.Trendyol.Response.Batch.BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status == "COMPLETED"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request.Items.SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem { ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId, Barcode = pim.Barcode, StockCode = pim.StockCode })).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status.ToLower() == "success")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var supplierId = trendyolConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"{_baseUrl}/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<MarketplacesV2.Trendyol.Response.Batch.BatchResponse>(new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = trendyolConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status == "COMPLETED"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i
                        .ProductInformationMarketplaces
                        .Select(pim => new TrackingResponseItem
                        {
                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                            Barcode = pim.Barcode,
                            StockCode = pim.StockCode
                        }))
                    .ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status.ToLower() == "success")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var supplierId = trendyolConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"{_baseUrl}/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<MarketplacesV2.Trendyol.Response.Batch.BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse.Content.items.Any(i => i.status != "SUCCESS" && i.status != "FAILED")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status == "SUCCESS")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var supplierId = trendyolConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"{_baseUrl}/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<MarketplacesV2.Trendyol.Response.Batch.BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = trendyolConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse.Content.items.Any(i => i.status != "SUCCESS" && i.status != "FAILED")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status == "SUCCESS")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            TrendyolConfigurationV2 trendyolConfigurationV2 = Configuration(request);

            var categoryResponse = await this._httpHelperV3.SendAsync<CategoryResponse>(new HttpHelperV3Request
            {
                RequestUri = $"{_baseUrl}/product-categories",
                AuthorizationType = AuthorizationType.Basic,
                Authorization = trendyolConfigurationV2.Authorization,
                HttpMethod = HttpMethod.Get
            });

            List<Category> categories = new();

            this.FetchSubCategories(categoryResponse.Content.Categories, categories);

            categories.ForEach(async theCategory =>
            {
                var categoryAttributeResponse = await this._httpHelperV3.SendAsync<CategoryAttributeResponse>(new HttpHelperV3Request
                {
                    RequestUri = $"{_baseUrl}/product-categories/{theCategory.Code}/attributes",
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = trendyolConfigurationV2.Authorization,
                    HttpMethod = HttpMethod.Get
                });

                theCategory.CategoryAttributes = categoryAttributeResponse.Content.CategoryAttributes.Select(ca => new Application.Common.Wrappers.MarketplacesV2.Categories.CategoryAttribute
                {
                    Code = ca.Attribute.Id.ToString(),
                    Name = ca.Attribute.Name,
                    AllowCustom = ca.AllowCustom,
                    CategoryCode = theCategory.Code,
                    Mandatory = ca.Required,
                    CategoryAttributesValues = ca.AttributeValues.Select(av => new CategoryAttributeValue
                    {
                        Code = av.Id.ToString(),
                        Name = av.Name,
                        VarinatCode = ca.Attribute.Id.ToString()
                    }).ToList()
                }).ToList();
            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private TrendyolConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private TrendyolConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations, bool decrypt = true)
        {
            if (decrypt)
                return marketplaceConfigurations.Decrypt();

            return marketplaceConfigurations;
        }

        /// <summary>
        /// Trendyol api sinden donen kategoriler icerisinden sadece en alt kategorileri ayiklamak icin yazilmis metod.
        /// </summary>
        /// <param name="subCategories">Trendyol api sinden donen kategoriler.</param>
        /// <param name="categories">En alt kategorilerin eklenecegi kategori listesi.</param>
        private void FetchSubCategories(List<SubCategory> subCategories, List<Category> categories)
        {
            if (subCategories.HasItem())
                foreach (var theSubCategory in subCategories)
                {
                    if (theSubCategory.SubCategories.HasItem())
                        this.FetchSubCategories(theSubCategory.SubCategories, categories);
                    else
                        categories.Add(new()
                        {
                            Code = theSubCategory.Id.ToString(),
                            Name = theSubCategory.Name,
                            ParentCategoryCode = theSubCategory.ParentId.HasValue ? theSubCategory.ParentId.ToString() : null
                        });
                }
        }
        #endregion
    }
}