﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3
{
    public class N11ConfigurationV3
    {
        #region Members
        public static implicit operator N11ConfigurationV3(Dictionary<string, string> configurations)
        {
            return new N11ConfigurationV3
            {
                AppSecret = configurations["AppSecret"],
                AppKey = configurations["AppKey"],
                ShipmentTemplate = configurations["ShipmentTemplate"]
            };
        }
        #endregion

        #region Properties
        public string AppSecret { get; internal set; }

        public string AppKey { get; internal set; }

        public string ShipmentTemplate { get; internal set; }
        #endregion
    }
}
