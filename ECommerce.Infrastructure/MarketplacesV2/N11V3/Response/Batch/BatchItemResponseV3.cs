﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Response.Batch
{
    public class BatchItemResponseV3
    {
        #region Properties
        public List<BatchContentResponseV3> Content { get; set; }

        public int TotalElements { get; set; }

        public int TotalPages { get; set; }
        #endregion
    }
}
