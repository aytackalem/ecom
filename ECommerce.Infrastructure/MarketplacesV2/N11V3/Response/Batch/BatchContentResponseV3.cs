﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Response.Batch
{
    public class BatchContentResponseV3
    {
        public long Id { get; set; }

        public int TaskId { get; set; }

        public int OwnerId { get; set; }

        public string ItemCode { get; set; }

        public string Status { get; set; }

        public BatchSkuResponseV3 Sku { get; set; }

        public List<string> Reasons { get; set; }
    }
}
