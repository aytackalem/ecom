﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Response.Batch
{
    public class BatchSkuResponseV3
    {
        public double? SalePrice { get; set; }

        public int? CategoryId { get; set; }


        public double? ListPrice { get; set; }

        public string Barcode { get; set; }

        public string Description { get; set; }

        public string ProductMainId { get; set; }

        public string StockCode { get; set; }

        public string CurrencyType { get; set; }

        public List<string> Reasons { get; set; }

        public int? Stock { get; set; }

    }
}
