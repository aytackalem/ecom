﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Response.Batch
{
    public class BatchResponseV3
    {
        public string Status { get; set; }

        public int TaskId { get; set; }

        public BatchItemResponseV3 Skus { get; set; }


    }
}
