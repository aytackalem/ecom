﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Response.ProductCreate
{
    public class ProductCreateBadRequestResponseV3
    {
        public List<Error> errors { get; set; }
    }

    public class Error
    {
        #region Properties
        public string key { get; set; }
        public string message { get; set; }
        public object errorCode { get; set; }
        public List<object> args { get; set; }
        #endregion
    }
}
