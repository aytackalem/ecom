﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.PriceStock
{
    public class PriceStockRequestV3
    {
        [JsonProperty("payload")]
        public PriceStockSkuV3 Payload { get; set; }

    }
}
