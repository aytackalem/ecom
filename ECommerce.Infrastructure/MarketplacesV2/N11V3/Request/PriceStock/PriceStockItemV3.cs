﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.PriceStock
{
    public class PriceStockItemV3
    {
        #region Properties
        [JsonProperty("stockCode")]
        public string StockCode { get; set; }

        [JsonProperty("listPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal? SalePrice { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("currencyType")]
        public string CurrencyType { get; set; }
        #endregion
    }
}
