﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductCreate
{
    public class ProductCreateSkuV3
    {
        #region Navigation Properties
        [JsonProperty("integrator")]
        public string Integrator { get; set; }

        [JsonProperty("skus")]
        public List<ProductCreateItemV3> Skus { get; set; }
        #endregion
    }
}
