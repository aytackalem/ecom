﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductCreate
{
    public class ProductCreateItemV3
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("currencyType")]
        public string CurrencyType { get; set; }

        [JsonProperty("productMainId")]
        public string ProductMainId { get; set; }

        [JsonProperty("preparingDay")]
        public int PreparingDay { get; set; }

        [JsonProperty("shipmentTemplate")]
        public string ShipmentTemplate { get; set; }

        [JsonProperty("maxPurchaseQuantity")]
        public int MaxPurchaseQuantity { get; set; }

        [JsonProperty("stockCode")]
        public string StockCode { get; set; }

        [JsonProperty("catalogId")]
        public object CatalogId { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("images")]
        public List<ProductImageV3> Images { get; set; }

        [JsonProperty("attributes")]
        public List<ProductAttributeV3> Attributes { get; set; }

        [JsonProperty("salePrice")]
        public double SalePrice { get; set; }

        [JsonProperty("listPrice")]
        public double ListPrice { get; set; }


        [JsonProperty("vatRate")]
        public int VatRate { get; internal set; }
    }
}
