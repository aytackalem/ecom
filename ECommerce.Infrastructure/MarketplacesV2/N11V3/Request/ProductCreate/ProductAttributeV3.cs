﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductCreate
{
    public class ProductAttributeV3
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("valueId")]
        public int? ValueId { get; set; }

        [JsonProperty("customValue")]
        public string CustomValue { get; set; }
    }
}
