﻿using ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductUpdate;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductCreate
{
    public class ProductCreateRequestV3
    {
        [JsonProperty("payload")]
        public ProductCreateSkuV3 Payload { get; set; }
    }
}
