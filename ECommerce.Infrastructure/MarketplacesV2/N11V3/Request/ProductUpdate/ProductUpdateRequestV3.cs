﻿using ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.PriceStock;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductUpdate
{
    public class ProductUpdateRequestV3
    {
        [JsonProperty("payload")]
        public ProductUpdateSkuV3 Payload { get; set; }
    }
}
