﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.ProductUpdate
{
    public class ProductUpdateItemV3
    {
        [JsonProperty("stockCode")]
        public string StockCode { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("preparingDay")]
        public int PreparingDay { get; set; }

        [JsonProperty("shipmentTemplate")]
        public string ShipmentTemplate { get; set; }

        [JsonProperty("deleteProductMainId")]
        public bool? DeleteProductMainId { get; set; }

        [JsonProperty("productMainId")]
        public string ProductMainId { get; set; }

        [JsonProperty("deleteMaxPurchaseQuantity")]
        public bool? DeleteMaxPurchaseQuantity { get; set; }

        [JsonProperty("maxPurchaseQuantity")]
        public int MaxPurchaseQuantity { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        public int VatRate { get; internal set; }
    }
}
