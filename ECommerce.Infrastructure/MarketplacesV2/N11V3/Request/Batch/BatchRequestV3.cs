﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.N11V3.Request.Batch
{
    public class BatchRequestV3
    {
        [JsonProperty("taskId")]
        public int TaskId { get; set; }


        [JsonProperty("pageable")]
        public BatchPageableRequestV3 Pageable { get; set; }

    }
}
