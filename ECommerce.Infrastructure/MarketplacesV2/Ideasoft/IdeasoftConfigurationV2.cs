﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ideasoft
{
    public class IdeasoftConfigurationV2
    {
        #region Members
        public static implicit operator IdeasoftConfigurationV2(Dictionary<string, string> configurations)
        {
            return new IdeasoftConfigurationV2
            {
                ShopAddress = configurations["ShopAddress"],
                ClientId = configurations["ClientId"],
                ClientSecret = configurations["ClientSecret"],
                AccessToken = configurations["AccessToken"],
                RefreshToken = configurations["RefreshToken"]
            };
        }
        #endregion

        #region Properties
        public string ShopAddress { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
        #endregion
    }
}
