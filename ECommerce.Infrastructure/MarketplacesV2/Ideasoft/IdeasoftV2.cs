﻿//using ECommerce.Application.Common.ExceptionHandling;
//using ECommerce.Application.Common.Extensions;
//using ECommerce.Application.Common.Interfaces.Helpers;
//using ECommerce.Application.Common.Interfaces.MarketplacesV2;
//using ECommerce.Application.Common.Parameters.MarketplacesV2;
//using ECommerce.Application.Common.Wrappers;
//using ECommerce.Application.Common.Wrappers.MarketplacesV2;
//using ECommerce.Infrastructure.MarketplacesV2.Ikas;
//using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Product;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Threading.Tasks;

//namespace ECommerce.Infrastructure.MarketplacesV2.Ideasoft
//{
//    public class IdeasoftV2 : IMarketplaceV2
//    {
//        #region Constants
//        private readonly string _urlTemplate = $"https://{{0}}/api/{{1}}?Host={{0}}";

//        private readonly string _marketplaceId = Application.Common.Constants.Marketplaces.Ideasoft;
//        #endregion

//        #region Fields
//        private readonly IHttpHelperV2 _httpHelperV2;

//        private readonly ICryptoHelper _cryptoHelper;
//        #endregion

//        #region Constructors
//        public IdeasoftV2(IHttpHelperV2 httpHelperV2, ICryptoHelper cryptoHelper)
//        {
//            #region Fields
//            this._httpHelperV2 = httpHelperV2;
//            this._cryptoHelper = cryptoHelper;
//            #endregion
//        }
//        #endregion

//        #region Methods
//        #region Dirty Price
//        public async Task<HttpResultResponse<ProcessResponse>> UpdatePriceAsync(DirtyPriceRequest dirtyPriceRequest)
//        {
//            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<ProcessResponse>>(async response =>
//            {
//                if (dirtyPriceRequest.Items.Any(i => string.IsNullOrEmpty(i.ProductUUId)))
//                {
//                    response.Message = "ProductUUId is cannot be null.";
//                    return;
//                }

//                IdeasoftConfigurationV2 ideasoftConfigurationV2 = dirtyPriceRequest
//                    .RequestHeader
//                    .MarketplaceConfigurations
//                    .Decrypt(this._cryptoHelper);

//                var item = dirtyPriceRequest.Items[0];

//                var uri = string.Format(
//                    _urlTemplate,
//                    ideasoftConfigurationV2.ShopAddress,
//                    $"products/{item.ProductUUId}");

//                var httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2>(
//                    uri,
//                    HttpMethod.Get,
//                    AuthorizationType.Bearer,
//                    ideasoftConfigurationV2.AccessToken);

//                if (httpResultResponse.ContentObject == null)
//                {
//                    dirtyPriceRequest.ErrorItems.Add(new DirtyPriceItem
//                    {
//                        ProductUUId = item.ProductUUId,
//                        Barcode = item.Barcode,
//                        ErrorMessage = "Fiyat güncelleme için ürün bulunamadı",
//                        ListUnitPrice = item.ListUnitPrice,
//                        UnitPrice = item.UnitPrice,
//                        ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
//                        ProductInformationUUId = item.ProductInformationUUId,
//                        SkuCode = item.SkuCode,
//                        Stock = item.Stock,
//                        StockCode = item.StockCode,
//                        VatRate = item.VatRate
//                    });
//                    response.Success = true;
//                    return;
//                }

//                var product = httpResultResponse.ContentObject;
//                product.price1 = item.UnitPrice;

//                httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2, ProductResponseV2>(
//                    uri,
//                    product,
//                    HttpMethod.Put,
//                    AuthorizationType.Bearer,
//                    ideasoftConfigurationV2.AccessToken);

//                response.Success = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK;
//            });
//        }
//        #endregion

//        #region Dirty Stock
//        public async Task<HttpResultResponse<ProcessResponse>> UpdateStockAsync(DirtyStockRequest dirtyStockRequest)
//        {
//            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<ProcessResponse>>(async response =>
//            {
//                if (dirtyStockRequest.Items.Any(i => string.IsNullOrEmpty(i.ProductUUId)))
//                {
//                    response.Message = "ProductUUId is cannot be null.";
//                    return;
//                }

//                IdeasoftConfigurationV2 ideasoftConfigurationV2 = dirtyStockRequest
//                    .RequestHeader
//                    .MarketplaceConfigurations
//                    .Decrypt(this._cryptoHelper);

//                var item = dirtyStockRequest.Items[0];

//                var uri = string.Format(
//                    _urlTemplate,
//                    ideasoftConfigurationV2.ShopAddress,
//                    $"products/{item.ProductUUId}");

//                var httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2>(
//                    uri,
//                    HttpMethod.Get,
//                    AuthorizationType.Bearer,
//                    ideasoftConfigurationV2.AccessToken);

//                if (httpResultResponse.ContentObject == null)
//                {
//                    dirtyStockRequest.ErrorItems.Add(new DirtyStockItem
//                    {
//                        ProductUUId = item.ProductUUId,
//                        Barcode = item.Barcode,
//                        ErrorMessage = "Stok güncelleme için ürün bulunamadı",
//                        ListUnitPrice = item.ListUnitPrice,
//                        UnitPrice = item.UnitPrice,
//                        ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
//                        ProductInformationUUId = item.ProductInformationUUId,
//                        SkuCode = item.SkuCode,
//                        Stock = item.Stock,
//                        StockCode = item.StockCode,
//                        VatRate = item.VatRate
//                    });
//                    response.Success = true;
//                    return;
//                    response.Success = true;
//                    return;
//                }

//                var product = httpResultResponse.ContentObject;
//                product.stockAmount = item.Stock;

//                httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2, ProductResponseV2>(
//                    uri,
//                    product,
//                    HttpMethod.Put,
//                    AuthorizationType.Bearer,
//                    ideasoftConfigurationV2.AccessToken);

//                response.Success = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK;
//            });
//        }
//        #endregion

//        #region Dirty Product
//        public async Task<HttpResultResponse<ProcessResponse>> UpsertProduct(DirtyProductRequest dirtyProductRequest)
//        {
//            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<ProcessResponse>>(async response =>
//            {
//                IdeasoftConfigurationV2 ideasoftConfigurationV2 = dirtyProductRequest
//                    .RequestHeader
//                    .MarketplaceConfigurations
//                    .Decrypt(this._cryptoHelper);

//                var item = dirtyProductRequest.Items[0];
//                var productInformationMarketplace = item.ProductInformationMarketplaces[0];

//                if (productInformationMarketplace.Opened)
//                {
//                    var uri = string.Format(
//                        _urlTemplate,
//                        ideasoftConfigurationV2.ShopAddress,
//                        $"products/{item.ProductMarketplaceUUId}");

//                    var httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2>(
//                        uri,
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        ideasoftConfigurationV2.AccessToken);

//                    var product = httpResultResponse.ContentObject;
//                    product.price1 = productInformationMarketplace.UnitPrice;
//                    product.name = productInformationMarketplace.ProductInformationName;
//                    product.stockAmount = productInformationMarketplace.Stock;

//                    httpResultResponse = await this._httpHelperV2.SendJsonAsync<ProductResponseV2, ProductResponseV2>(
//                        uri,
//                        product,
//                        HttpMethod.Put,
//                        AuthorizationType.Bearer,
//                        ideasoftConfigurationV2.AccessToken);
//                }
//                else
//                {
//                    //TO DO: Create...
//                }
//            });
//        }
//        #endregion
//        #endregion
//    }
//}
