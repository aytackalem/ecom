﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.PttAvm
{
    public class PttAvmConfigurationV2
    {
        #region Members
        public static implicit operator PttAvmConfigurationV2(Dictionary<string, string> configurations)
        {
            return new PttAvmConfigurationV2
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                ShopId = int.Parse(configurations["ShopId"])
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public int ShopId { get; set; }
        #endregion
    }
}
