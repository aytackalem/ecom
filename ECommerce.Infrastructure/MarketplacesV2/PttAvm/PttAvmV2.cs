﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using PttAvmService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.PttAvm
{
    public class PttAvmV2 : IMarketplaceV2, IMarketplaceV2StockPriceUntrackable, IMarketplaceV2CategoryGetable
    {
        #region Fields
        #endregion

        #region Constructors
        public PttAvmV2()
        {
            #region Fields
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.PttAvm;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool FullUpdate => false;

        public bool StockPriceLoggable => true;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            PttAvmConfigurationV2 pttAvmConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            ServiceClient serviceClient = new();
            serviceClient.ClientCredentials.UserName.UserName = pttAvmConfigurationV2.Username;
            serviceClient.ClientCredentials.UserName.Password = pttAvmConfigurationV2.Password;

            await serviceClient.OpenAsync();

            var serviceResults = await serviceClient
                .StokGuncelleV2BulkAsync(request
                    .Items
                    .SelectMany(dpi => dpi
                        .ProductInformationMarketplaces
                        .Select(pim => new StokUrun
                        {
                            ShopId = pttAvmConfigurationV2.ShopId,
                            Barkod = dpi.SellerCode,
                            UrunAdi = dpi.ProductName,
                            UrunKodu = dpi.SellerCode,
                            Aciklama = pim.Description,
                            KDVOran = pim.VatRate * 100M,
                            KDVsiz = pim.UnitPrice / (1M + pim.VatRate),
                            KDVli = pim.UnitPrice,
                            Miktar = pim.Stock,
                            Aktif = true,
                            Mevcut = true,
                            Durum = KayitDurum.Yeni,
                            UrunResimleri = pim
                                .ProductInformationPhotos
                                .Select(pip => new UrunResim
                                {
                                    Sira = pim.ProductInformationPhotos.IndexOf(pip),
                                    Url = pip.Url
                                })
                                .ToArray(),
                            VariantListesi = new Variant[] {
                                    new Variant
                                    {
                                        VariantBarkod = pim.Barcode,
                                        AnaUrunKodu = dpi.SellerCode,
                                        Durum = KayitDurum.Yeni,
                                        Miktar = pim.Stock,
                                        KayitDegisti = 1,
                                        Attributes = pim
                                            .ProductInformationMarketplaceVariantValues
                                            .Select(pimvv => new VariantAttr
                                            {
                                                Deger = pimvv.MarketplaceVariantValueName,
                                                FiyatFarkiMi = false,
                                                Tanim = pimvv.MarketplaceVarinatName
                                            })
                                            .ToArray()
                                    }
                            }
                        }))
                    .ToArray());

            serviceClient.Close();

            return null;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            PttAvmConfigurationV2 pttAvmConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            ServiceClient serviceClient = new();
            serviceClient.ClientCredentials.UserName.UserName = pttAvmConfigurationV2.Username;
            serviceClient.ClientCredentials.UserName.Password = pttAvmConfigurationV2.Password;

            await serviceClient.OpenAsync();

            var serviceResults = await serviceClient
                .StokGuncelleV2BulkAsync(request
                    .Items
                    .SelectMany(dpi => dpi
                        .ProductInformationMarketplaces
                        .Select(pim => new StokUrun
                        {
                            ShopId = pttAvmConfigurationV2.ShopId,
                            Barkod = dpi.SellerCode,
                            UrunAdi = dpi.ProductName,
                            UrunKodu = dpi.SellerCode,
                            Aciklama = pim.Description,
                            KDVOran = pim.VatRate * 100M,
                            KDVsiz = pim.UnitPrice / (1M + pim.VatRate),
                            KDVli = pim.UnitPrice,
                            Miktar = pim.Stock,
                            Mevcut = true,                            
                            Aktif = true,
                            Durum = KayitDurum.Mevcut,
                            UrunResimleri = pim
                                .ProductInformationPhotos
                                .Select(pip => new UrunResim
                                {
                                    Sira = pim.ProductInformationPhotos.IndexOf(pip),
                                    Url = pip.Url
                                })
                                .ToArray(),
                            VariantListesi = new Variant[] {
                                    new Variant
                                    {
                                        VariantBarkod = pim.Barcode,
                                        AnaUrunKodu = dpi.SellerCode,
                                        Durum = KayitDurum.Mevcut,
                                        Miktar = pim.Stock,
                                        KayitDegisti = 1,
                                        Attributes = pim
                                            .ProductInformationMarketplaceVariantValues
                                            .Select(pimvv => new VariantAttr
                                            {
                                                Deger = pimvv.MarketplaceVariantValueName,
                                                FiyatFarkiMi = false,
                                                Tanim = pimvv.MarketplaceVarinatName
                                            })
                                            .ToArray()
                                    }
                            }
                        }))
                    .ToArray());

            serviceClient.Close();

            return null;
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            PttAvmConfigurationV2 pttAvmConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

            ServiceClient serviceClient = new();

            serviceClient.ClientCredentials.UserName.UserName = pttAvmConfigurationV2.Username;
            serviceClient.ClientCredentials.UserName.Password = pttAvmConfigurationV2.Password;

            await serviceClient.OpenAsync();

            var serviceResults = await serviceClient
                    .StokFiyatGuncelle3Async(request
                        .Items
                        .Select(dpi => new StokUrun
                        {
                            ShopId = pttAvmConfigurationV2.ShopId,
                            Barkod = dpi.StockCode,
                            KDVli = dpi.UnitPrice,
                            KDVsiz = dpi.UnitPrice / (1M + dpi.VatRate),
                            Miktar = dpi.Stock,
                            KDVOran = dpi.VatRate * 100M,
                            Aktif = true
                        })
                        .FirstOrDefault());

            serviceClient.Close();

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = serviceResults.Success
                        }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            PttAvmConfigurationV2 pttAvmConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            ServiceClient serviceClient = new();

            serviceClient.ClientCredentials.UserName.UserName = pttAvmConfigurationV2.Username;
            serviceClient.ClientCredentials.UserName.Password = pttAvmConfigurationV2.Password;

            await serviceClient.OpenAsync();

            var i = request
                    .Items
                    .Select(dpi => new StokUrun
                    {
                        ShopId = pttAvmConfigurationV2.ShopId,
                        Barkod = dpi.StockCode,
                        KDVli = dpi.UnitPrice,
                        KDVsiz = dpi.UnitPrice / (1M + dpi.VatRate),
                        Miktar = dpi.Stock,
                        KDVOran = dpi.VatRate * 100M,
                        Aktif = true
                    })
                    .FirstOrDefault();

            var serviceResults = await serviceClient.StokFiyatGuncelle3Async(i);

            serviceClient.Close();

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = serviceResults.Success
                        }).ToList()
                }
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {
            requests.ForEach(theRequest =>
            {
                var invalidItems = theRequest
                            .Items
                            .Where(i =>
                                i.LastRequestDateTime.HasValue &&
                                (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 6)
                            .ToList();

                if (invalidItems.HasItem())
                {
                    invalidItems.ForEach(i =>
                        i.ErrorMessage = "Güncellenmek istenen değer pazaryerine son 5 dk içerisinde bir kez gönderildi.");

                    theRequest.InvalidItems.AddRange(invalidItems);

                    theRequest.Items.RemoveAll(i =>
                        i.LastRequestDateTime.HasValue &&
                        (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 6);
                }
            });
        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {
            requests.ForEach(theRequest =>
            {
                var invalidItems = theRequest
                            .Items
                            .Where(i =>
                                i.LastRequestDateTime.HasValue &&
                                (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 6)
                            .ToList();

                if (invalidItems.HasItem())
                {
                    invalidItems.ForEach(i =>
                        i.ErrorMessage = "Güncellenmek istenen değer pazaryerine son 5 dk içerisinde bir kez gönderildi.");

                    theRequest.InvalidItems.AddRange(invalidItems);

                    theRequest.Items.RemoveAll(i =>
                        i.LastRequestDateTime.HasValue &&
                        (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 6);
                }
            });
        }

        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceUpdateProductRequests.Add(new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            List<Category> categories = new();

            PttAvmConfigurationV2 pttAvmConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            ServiceClient serviceClient = new(ServiceClient.EndpointConfiguration.WsSecured);
            serviceClient.ClientCredentials.UserName.UserName = pttAvmConfigurationV2.Username;
            serviceClient.ClientCredentials.UserName.Password = pttAvmConfigurationV2.Password;

            await serviceClient.OpenAsync();

            var categoryResponseTree = await serviceClient.GetCategoryTreeAsync(string.Empty, string.Empty);

            this.FetchSubCategories(categoryResponseTree.category_tree, categories);

            serviceClient.Close();

            return categories;
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategories(Category1[] childrens, List<Category> categories)
        {
            if (childrens.Any())
                foreach (var theChildren in childrens)
                {
                    if (theChildren.children.Any())
                        this.FetchSubCategories(theChildren.children, categories);
                    else
                        categories.Add(new()
                        {
                            Code = theChildren.id,
                            Name = theChildren.name,
                            ParentCategoryCode = theChildren.parent_id
                        });
                }
        }
        #endregion
    }
}
