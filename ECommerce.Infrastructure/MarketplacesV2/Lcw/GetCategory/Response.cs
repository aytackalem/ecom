﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.GetCategory;

public class Response
{
    public int page { get; set; }
    public int size { get; set; }
    public int count { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
}

public class Datum
{
    public int categoryId { get; set; }
    public int? parentCategoryId { get; set; }
    public Value[] values { get; set; }
}

public class Value
{
    public string languageCode { get; set; }
    public string value { get; set; }
}
