﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.UpdatePrice;

public class Request
{
    public Product[] products { get; set; }
}

public class Product
{
    public decimal listPrice { get; set; }
    public decimal salePrice { get; set; }
    public string countryCode { get; set; }
    public string currencyCode { get; set; }
    public int SupplierId { get; set; }
    public int productId { get; set; }
    //public string Barcode { get; set; }
}
