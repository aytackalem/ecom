﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.GetCategoryAttribute;

public class Response
{
    public int page { get; set; }
    public int size { get; set; }
    public int count { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
}

public class Datum
{
    public int categoryId { get; set; }
    public Categoryattribute[] categoryAttributes { get; set; }
}

public class Categoryattribute
{
    public int attributeId { get; set; }
    public bool required { get; set; }
    public bool allowCustom { get; set; }
}
