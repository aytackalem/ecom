﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.UpdateStock;

public class Request
{
    public Item[] items { get; set; }
}

public class Item
{
    public int supplierId { get; set; }
    public string barcode { get; set; }
    public int quantity { get; set; }
}
