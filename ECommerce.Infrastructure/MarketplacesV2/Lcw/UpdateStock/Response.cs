﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.UpdateStock;

public class Response
{
    public bool status { get; set; }
    public string message { get; set; }
    public string batchRequestId { get; set; }
    public string[] successBarcodes { get; set; }
    public object[] failedBarcodes { get; set; }
}
