﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.GetBatchRequestForPrice;

public class Response
{
    public string isSuccessful { get; set; }
    public string message { get; set; }
    public int batchCount { get; set; }
    public int successfulCount { get; set; }
    public int QueueProcessType { get; set; }
    public product[] successfulProducts { get; set; }
    public product[] failedProducts { get; set; }
    public product[] InProgressProduct { get; set; }
}

public class product
{
    public int productId { get; set; }
    public string message { get; set; }
    public string barcode { get; set; }
}
