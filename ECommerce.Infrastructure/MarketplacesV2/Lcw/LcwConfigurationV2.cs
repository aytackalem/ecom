﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Lcw;

public class LcwConfigurationV2
{
    public int SupplierId { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public static implicit operator LcwConfigurationV2(Dictionary<string, string> configurations)
    {
        return new LcwConfigurationV2
        {
            SupplierId = int.Parse(configurations["SupplierId"]),
            Username = configurations["Username"],
            Password = configurations["Password"]
        };
    }
}