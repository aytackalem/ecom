﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.GetBatchRequestForProduct;

public class Response
{
    public string isSuccessful { get; set; }
    public string message { get; set; }
    public int batchCount { get; set; }
    public int successfulCount { get; set; }
    public Product[] successfulProducts { get; set; }
    public Product[] failedProducts { get; set; }
    public Product[] inProgressOptions { get; set; }
    public int queueProcessType { get; set; }
}

public class Product
{
    public string barcode { get; set; }
    public string stockCode { get; set; }
    public string productMainId { get; set; }
    public string message { get; set; }
}
