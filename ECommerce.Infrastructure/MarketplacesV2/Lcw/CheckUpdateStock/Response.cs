﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.CheckUpdateStock;

public class Response
{
    public string status { get; set; }
    public int batchCount { get; set; }
    public int successCount { get; set; }
    public Erroritem[] errorItems { get; set; }
}

public class Erroritem
{
    public string barcode { get; set; }
}
