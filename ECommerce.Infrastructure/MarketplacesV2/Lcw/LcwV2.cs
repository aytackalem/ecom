﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Lcw;

public class LcwV2 :
    IMarketplaceV2,
    IMarketplaceV2CreateProductTrackable,
    IMarketplaceV2StockPriceTrackable
{
    private const string _baseUrl = "https://mpapi.lcw.com/integration/product-integration/service/api";

    private const string _tokenUrl = "https://pars.lcwaikiki.com/sts/issue/oidc/token";

    public string Id => Application.Common.Constants.Marketplaces.Lcw;

    public int? WaitingSecondPerCreateProductRequest => 0;

    public bool IsUpsert => false;

    public int? WaitingSecondPerStockRequest => 0;

    public int? WaitingSecondPerPriceRequest => 0;

    public bool StockPriceLoggable => false;

    private readonly IHttpHelperV3 _httpHelperV3;

    public LcwV2(IHttpHelperV3 httpHelperV3)
    {
        _httpHelperV3 = httpHelperV3;
    }

    #region Validate
    public void Validate(List<MarketplaceRequestCreateProduct> requests)
    {
        foreach (var theRequest in requests)
        {
            foreach (var theItem in theRequest.Items)
            {
                #region Broken Photo Url
                foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                {
                    foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                    {
                        if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                        {
                            productInformationMarketplace.HasBrokenPhotoUrl = true;
                        }
                    }
                }

                if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                {
                    var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(_ => _.HasBrokenPhotoUrl)
                        .ToList();

                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                    {
                        ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                        ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                    });

                    theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                }
                #endregion

                #region Color Size
                Predicate<ProductInformationMarketplace> colorSizePredicate = _ => _
                    .ProductInformationMarketplaceVariantValues
                    .Count(_ => _.MarketplaceVarinatCode == "Size" || _.MarketplaceVarinatCode == "Color") != 2;

                Func<ProductInformationMarketplace, bool> colorSizeFunc = _ => colorSizePredicate(_);

                if (theItem.ProductInformationMarketplaces.Any(colorSizeFunc))
                {
                    var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(colorSizeFunc)
                        .ToList();

                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                    {
                        ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                        ErrorMessage = "Ürün renk ve beden zorunlu alanlarını içermelidir."
                    });

                    theItem.ProductInformationMarketplaces.RemoveAll(colorSizePredicate);
                }
                #endregion
            }

            theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
        }
    }

    public void Validate(List<MarketplaceRequestUpdateStock> requests)
    {
    }

    public void Validate(List<MarketplaceRequestUpdatePrice> requests)
    {

    }
    #endregion

    #region Split
    public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
    {
        /*
         * LCW bir istekte bir renk kabul ediyor bu sebeple urunler renk bazli ayriliyor.
         */

        List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();

        foreach (MarketplaceRequestCreateProduct request in requests)
        {
            foreach (MarketplaceRequestCreateProductItem item in request.Items)
            {
                Dictionary<string, List<ProductInformationMarketplace>> dictionary = new();

                foreach (var productInformationMarketplace in item.ProductInformationMarketplaces)
                {
                    var color = productInformationMarketplace
                        .ProductInformationMarketplaceVariantValues
                        .FirstOrDefault(_ => _.MarketplaceVarinatName == "Renk")
                        .MarketplaceVariantValueName;

                    if (dictionary.ContainsKey(color) == false)
                    {
                        dictionary.Add(color, new());
                    }

                    dictionary[color].Add(productInformationMarketplace);
                }

                foreach (var dict in dictionary)
                {
                    MarketplaceRequestCreateProduct marketplaceRequestCreateProduct = new()
                    {
                        Id = Guid.NewGuid(),
                        MarketplaceRequestHeader = request.MarketplaceRequestHeader,
                        Items = new()
                        {
                            new()
                            {
                                BrandCode = item.BrandCode,
                                BrandName = item.BrandName,
                                CategoryCode = item.CategoryCode,
                                CategoryName = item.CategoryName,
                                DeliveryDay = item.DeliveryDay,
                                DimensionalWeight = item.DimensionalWeight,
                                ProductId = item.ProductId,
                                ProductMarketplaceId = item.ProductMarketplaceId,
                                ProductMarketplaceUUId = item.ProductMarketplaceUUId,
                                ProductName = item.ProductName,
                                SellerCode = item.SellerCode,
                                ProductInformationMarketplaces = dict.Value
                            }
                        }
                    };

                    marketplaceCreateProductRequests.Add(marketplaceRequestCreateProduct);
                }
            }
        }

        return marketplaceCreateProductRequests;
    }

    public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
    {
        List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
        requests
            .ForEach(theRequest =>
            {
                var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                for (int i = 0; i < requestCount; i++)
                {
                    marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                    {
                        Id = Guid.NewGuid(),
                        MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                        Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                    });
                }
            });

        return marketplaceStockRequests;
    }

    public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
    {
        List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
        requests
            .ForEach(theRequest =>
            {
                var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                for (int i = 0; i < requestCount; i++)
                {
                    marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                    {
                        Id = Guid.NewGuid(),
                        MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                        Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                    });
                }
            });

        return marketplacePriceRequests;
    }
    #endregion

    #region Check
    public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        var httpResultResponse = await this._httpHelperV3.SendAsync<GetBatchRequestForProduct.Response>(
            new HttpHelperV3Request
            {
                RequestUri = $"{_baseUrl}/Product/GetBatchRequestForProduct?BatchRequestId={request.TrackingId}",
                HttpMethod = HttpMethod.Get,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token
            });

        var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode
        };

        /*
         * 
         * Eğer tüm elemanlar işlenmiş ve bitmiş ise (Başarılı veya başarısız) "Done" özelliği "true" olarak atanır -ki başarılı olanlar "Opened" olarak belirlensin, başarısız olanlar için uyarılar oluşturulsun.
         * 
         */
        if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
            httpHelperV3Response.Content = new TrackingResponse
            {
                Done = httpResultResponse.Content.batchCount == httpResultResponse.Content.successfulCount + httpResultResponse.Content.failedProducts?.Length
            };
        else
            return httpHelperV3Response;

        if (httpHelperV3Response.Content.Done)
        {
            var trackingResponseItems = request
                .Items
                .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                {
                    ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                    Barcode = pim.Barcode,
                    StockCode = pim.StockCode
                }))
                .ToList();

            /* Successful Products */
            foreach (var item in httpResultResponse.Content.successfulProducts)
            {
                var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == item.barcode);
                trackingResponseItem.Success = true;
            }

            /* Failed Products */
            foreach (var item in httpResultResponse.Content.failedProducts)
            {
                var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == item.barcode);
                trackingResponseItem.Success = false;
            }

            httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
        }

        return httpHelperV3Response;
    }

    public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<GetBatchRequestForPrice.Response>(
                    new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/Product/GetBatchRequestForPrice?BatchRequestId={request.TrackingId}",
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token
                    });

        var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode
        };

        /*
         * 
         * Eğer tüm elemanlar işlenmiş ve bitmiş ise (Başarılı veya başarısız) "Done" özelliği "true" olarak atanır -ki başarısız olanlar için uyarılar oluşturulsun.
         * 
         */
        if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
            httpHelperV3Response.Content = new TrackingResponse
            {
                Done = httpResultResponse.Content.batchCount == httpResultResponse.Content.successfulCount + httpResultResponse.Content.failedProducts?.Length
            };
        else
            return httpHelperV3Response;

        if (httpHelperV3Response.Content.Done)
        {
            var trackingResponseItems = request
                .Items
                .Select(i => new TrackingResponseItem
                {
                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                    Barcode = i.Barcode,
                    StockCode = i.StockCode,
                    ListUnitPrice = i.ListUnitPrice,
                    UnitPrice = i.UnitPrice
                }).ToList();

            /* Successful Products */
            foreach (var item in httpResultResponse.Content.successfulProducts)
            {
                var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == item.barcode);
                trackingResponseItem.Success = true;
            }

            /* Failed Products */
            foreach (var item in httpResultResponse.Content.successfulProducts)
            {
                var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == item.barcode);
                trackingResponseItem.Success = false;
            }

            httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
        }

        return httpHelperV3Response;
    }

    public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<CheckUpdateStock.Request, CheckUpdateStock.Response>(
                    new HttpHelperV3Request<CheckUpdateStock.Request>
                    {
                        RequestUri = $"{_baseUrl}/StockManagement/CheckUpdateStock",
                        Request = new CheckUpdateStock.Request
                        {
                            batchRequestId = request.TrackingId
                        },
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token
                    });

        var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode
        };

        /*
         * 
         * Eğer tüm elemanlar işlenmiş ve bitmiş ise (Başarılı veya başarısız) "Done" özelliği "true" olarak atanır -ki başarısız olanlar için uyarılar oluşturulsun.
         * 
         */
        if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
            httpHelperV3Response.Content = new TrackingResponse
            {
                Done = httpResultResponse.Content.batchCount == httpResultResponse.Content.successCount + httpResultResponse.Content.errorItems?.Length
            };
        else
            return httpHelperV3Response;

        if (httpHelperV3Response.Content.Done)
        {
            var trackingResponseItems = request
                .Items
                .Select(i => new TrackingResponseItem
                {
                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                    Barcode = i.Barcode,
                    StockCode = i.StockCode,
                    ListUnitPrice = i.ListUnitPrice,
                    UnitPrice = i.UnitPrice,
                    Success = true
                }).ToList();

            /* Failed Products */
            foreach (var item in httpResultResponse.Content.errorItems)
            {
                var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == item.barcode);
                trackingResponseItem.Messages = new List<string>
                {
                    "Stok güncellenemedi."
                };
                trackingResponseItem.Success = false;
            }

            httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
        }

        return httpHelperV3Response;
    }
    #endregion

    #region Send
    public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        CreateProduct.Request createProductRequest = new()
        {
            supplierid = lcwConfigurationV2.SupplierId,
            options = request.Items.Select(_ => new CreateProduct.Option
            {
                brandId = 1002999,
                productMainId = request.Items[0].SellerCode,
                mainColorCode = request.Items[0].ProductInformationMarketplaces[0].ProductInformationMarketplaceVariantValues.First(_ => _.MarketplaceVarinatCode == "Color").MarketplaceVariantValueCode,
                titles = new CreateProduct.Title[]
                {
                    new CreateProduct.Title
                    {
                        languageCode = "tr-TR",
                        value = request.Items[0].ProductInformationMarketplaces[0].ProductInformationName
                    }
                },
                categories = new CreateProduct.Category[]
                {
                    new CreateProduct.Category
                    {
                        categoryId = int.Parse( request.Items[0].CategoryCode)
                    }
                },
                descriptions = new CreateProduct.Description[]
                {
                    new CreateProduct.Description
                    {
                        languageCode = "tr-TR",
                        value = request.Items[0].ProductInformationMarketplaces[0].Description
                    }
                },
                images = request.Items[0].ProductInformationMarketplaces[0].ProductInformationPhotos.Select(_ => new CreateProduct.Image
                {
                    displayorder = request.Items[0].ProductInformationMarketplaces[0].ProductInformationPhotos.IndexOf(_),
                    url = _.Url
                }).ToArray(),
                products = request.Items[0].ProductInformationMarketplaces.Select(_ => new CreateProduct.Product
                {
                    barcode = _.Barcode,
                    prices = new CreateProduct.Price[]
                    {
                        new CreateProduct.Price
                        {
                            countrycode = "TR",
                            currencyType = "TRY",
                            listPrice = _.ListUnitPrice,
                            salePrice = _.UnitPrice,
                            vatRate = Convert.ToInt32(100 * _.VatRate)
                        }
                    },
                    quantity = 0,
                    stockCode = _.StockCode,
                    sizeId = int.Parse(_.ProductInformationMarketplaceVariantValues.First(_ => _.MarketplaceVarinatCode == "Size").MarketplaceVariantValueCode)
                }).ToArray(),
                attributes = request.Items[0].ProductInformationMarketplaces[0].ProductInformationMarketplaceVariantValues.Where(_ => _.MarketplaceVarinatCode != "Color" && _.MarketplaceVarinatCode != "Size").Select(_ => new CreateProduct.Attribute
                {
                    attributeId = int.Parse(_.MarketplaceVarinatCode),
                    attributeValueId = int.Parse(_.MarketplaceVariantValueCode)
                }).ToArray()
            }).ToArray()
        };

        var httpResultResponse = await this._httpHelperV3.SendAsync<CreateProduct.Request, CreateProduct.Response>(
            new HttpHelperV3Request<CreateProduct.Request>
            {
                RequestUri = $"{_baseUrl}/Product/CreateProduct",
                Request = createProductRequest,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token
            });

        return new HttpHelperV3Response<TrackableResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode,
            Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                ? new TrackableResponse { TrackingId = httpResultResponse.Content.batchRequestId }
                : null
        };
    }

    public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        UpdatePrice.Request updatePriceRequest = new()
        {
            products = request
                .Items
                .Select(_ => new UpdatePrice.Product
                {
                    listPrice = _.ListUnitPrice,
                    salePrice = _.UnitPrice,
                    productId = Convert.ToInt32(_.ProductInformationUUId),
                    countryCode = "TR",
                    SupplierId = lcwConfigurationV2.SupplierId,
                    currencyCode = "TRY"
                })
                .ToArray()
        };

        var httpResultResponse = await this._httpHelperV3.SendAsync<UpdatePrice.Request, UpdatePrice.Response>(
            new HttpHelperV3Request<UpdatePrice.Request>
            {
                RequestUri = $"{_baseUrl}/Product/UpdateProductPrice",
                Request = updatePriceRequest,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token
            });

        return new HttpHelperV3Response<TrackableResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode,
            Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                ? new TrackableResponse { TrackingId = httpResultResponse.Content.batchRequestId }
                : null
        };
    }

    public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
    {
        LcwConfigurationV2 lcwConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

        var token = await GetToken(lcwConfigurationV2);

        UpdateStock.Request updateStockRequest = new()
        {
            items = request.Items.Select(_ => new UpdateStock.Item
            {
                barcode = _.Barcode,
                supplierId = lcwConfigurationV2.SupplierId,
                quantity = _.Stock
            }).ToArray()
        };

        var httpResultResponse = await this._httpHelperV3.SendAsync<UpdateStock.Request, UpdateStock.Response>(
            new HttpHelperV3Request<UpdateStock.Request>
            {
                RequestUri = $"{_baseUrl}/StockManagement/UpdateStock",
                Request = updateStockRequest,
                HttpMethod = HttpMethod.Post,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token
            });

        return new HttpHelperV3Response<TrackableResponse>
        {
            HttpStatusCode = httpResultResponse.HttpStatusCode,
            Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                ? new TrackableResponse { TrackingId = httpResultResponse.Content.batchRequestId }
                : null
        };
    }
    #endregion

    #region Helper Methods
    private async Task<string> GetToken(LcwConfigurationV2 lcwConfigurationV2)
    {
        var data = new List<KeyValuePair<string, string>> {
                new("client_id", "LCWSellerEntegrator"),
                new("username", lcwConfigurationV2.Username),
                new("password", lcwConfigurationV2.Password),
                new("grant_type", "password"),
                new("client_secret", "D51C3FADCA8A40118A88F3CD6C197EEA"),
                new("scope", "openid")
            };

        var tokenResponse = await this._httpHelperV3.SendAsync<Token.Response>(new HttpHelperV3FormUrlEncodedRequest
        {
            RequestUri = _tokenUrl,
            Data = data,
            HttpMethod = HttpMethod.Post
        });
        return tokenResponse.Content.AccessToken;
    }
    #endregion
}
