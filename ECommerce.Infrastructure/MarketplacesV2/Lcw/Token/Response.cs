﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.Token;

public class Response
{
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }
}
