﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.GetBrand;


public class Response
{
    public int page { get; set; }
    public int size { get; set; }
    public int count { get; set; }
    public int totalPages { get; set; }
    public Datum[] data { get; set; }
}

public class Datum
{
    public Brand[] brands { get; set; }
}

public class Brand
{
    public int supplierId { get; set; }
    public int brandId { get; set; }
    public string name { get; set; }
}

