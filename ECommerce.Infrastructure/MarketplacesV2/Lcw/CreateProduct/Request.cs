﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.CreateProduct;

public class Request
{
    public int supplierid { get; set; }
    public Option[] options { get; set; }
}

public class Option
{
    public int brandId { get; set; }
    public string productMainId { get; set; }
    public Title[] titles { get; set; }
    public string mainColorCode { get; set; }
    public Category[] categories { get; set; }
    public Description[] descriptions { get; set; }
    public Image[] images { get; set; }
    public Product[] products { get; set; }
    public Attribute[] attributes { get; set; }
}

public class Title
{
    public string languageCode { get; set; }
    public string value { get; set; }
}

public class Category
{
    public int categoryId { get; set; }
}

public class Description
{
    public string languageCode { get; set; }
    public string value { get; set; }
}

public class Image
{
    public string url { get; set; }
    public int displayorder { get; set; }
}

public class Product
{
    public string barcode { get; set; }
    public int sizeId { get; set; }
    public string stockCode { get; set; }
    public int quantity { get; set; }
    public Price[] prices { get; set; }
}

public class Price
{
    public string countrycode { get; set; }
    public string currencyType { get; set; }
    public decimal listPrice { get; set; }
    public decimal salePrice { get; set; }
    public int vatRate { get; set; }
}

public class Attribute
{
    public int attributeId { get; set; }
    public int attributeValueId { get; set; }
}
