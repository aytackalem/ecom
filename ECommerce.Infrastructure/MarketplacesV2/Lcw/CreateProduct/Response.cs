﻿namespace ECommerce.Infrastructure.MarketplacesV2.Lcw.CreateProduct;


public class Response
{
    public string batchRequestId { get; set; }
    public string errorMessage { get; set; }
}
