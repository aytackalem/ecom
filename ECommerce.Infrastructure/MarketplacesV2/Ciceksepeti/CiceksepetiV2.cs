﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.PriceStock;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductUpdate;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.Category;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.PriceStock;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.ProductCreate;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti
{
    public class CiceksepetiV2 : 
        IMarketplaceV2, 
        IMarketplaceV2CreateProductTrackable, 
        IMarketplaceV2UpdateProductTrackable, 
        IMarketplaceV2StockPriceTrackable, 
        IMarketplaceV2CategoryGetable
    {
        #region Constants
        public const string _baseUrl = "https://apis.ciceksepeti.com/api";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public CiceksepetiV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;

            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.CicekSepeti;

        public int? WaitingSecondPerCreateProductRequest => 5;

        public int? WaitingSecondPerUpdateProductRequest => 5;

        public int? WaitingSecondPerStockRequest => 1;

        public int? WaitingSecondPerPriceRequest => 1;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => true;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");

            var createRequest = new ProductCreateRequestV2
            {
                products = new List<ProductCreateV2>()
            };

            var customHeaders = new Dictionary<string, string> { { "x-api-key", ciceksepetiConfigurationV2.ApiKey } };

            foreach (var piLoop in request.Items)
            {
                var categoryAttributeUrl = $"{_baseUrl}/v1/categories/{piLoop.CategoryCode}/attributes";

                var cicekSepetiCategory = await this._httpHelperV3.SendAsync<ProductCategoryV2>(new HttpHelperV3Request
                {
                    RequestUri = categoryAttributeUrl,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey,
                    HttpMethod = HttpMethod.Get
                });

                var _brandCategoryAttribute = cicekSepetiCategory
                    .Content
                    .CategoryAttributes
                    .FirstOrDefault(x => x.AttributeName == "Marka");

                ProductInformationMarketplaceVariantValue categoryAttribute = null;
                if (_brandCategoryAttribute != null)
                {
                    var _brandAttributeValue = _brandCategoryAttribute.AttributeValues.FirstOrDefault(x => x.Name.ReplaceChar() == piLoop.BrandName.ReplaceChar());
                    if (_brandAttributeValue != null)
                    {
                        categoryAttribute = new ProductInformationMarketplaceVariantValue
                        {
                            MarketplaceVarinatCode = _brandCategoryAttribute.AttributeId.ToString(),
                            MarketplaceVariantValueCode = _brandAttributeValue.Id.ToString()
                        };
                    }
                }

                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    if (categoryAttribute != null)
                    {
                        pinLoop.ProductInformationMarketplaceVariantValues.Add(categoryAttribute);
                    }

                    createRequest.products.Add(new ProductCreateV2
                    {
                        productName = pinLoop.ProductInformationName,
                        barcode = pinLoop.Barcode,
                        categoryId = Convert.ToInt32(piLoop.CategoryCode),
                        description = pinLoop.Description,
                        images = pinLoop.ProductInformationPhotos.Take(5).Select(x => x.Url).ToList(),
                        mediaLink = null,
                        deliveryMessageType = 5,
                        deliveryType = 2,
                        stockQuantity = pinLoop.Stock,
                        salesPrice = Convert.ToDouble(pinLoop.UnitPrice, culture),
                        listPrice = Convert.ToDouble(pinLoop.ListUnitPrice, culture),
                        stockCode = pinLoop.StockCode,
                        mainProductCode = piLoop.SellerCode,
                        attributes = pinLoop.ProductInformationMarketplaceVariantValues.Select(x => new AttributeV2
                        {
                            id = Convert.ToInt32(x.MarketplaceVarinatCode),
                            valueId = Convert.ToInt32(x.MarketplaceVariantValueCode),
                            textLength = 0
                        }).ToList()

                    });
                }
            }

            var httpResultResponse = await this._httpHelperV3.SendAsync<ProductCreateRequestV2, ProductCreateResponseV2, ProductCreateBadRequestResponseV2>(
                    new HttpHelperV3Request<ProductCreateRequestV2>
                    {
                        RequestUri = $"{_baseUrl}/v1/Products",
                        Request = createRequest,
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.XApiKey,
                        Authorization = ciceksepetiConfigurationV2.ApiKey
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.batchId;
            else if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = new()
                {
                    httpResultResponse.ContentError.Message
                };

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");

            var updateRequest = new ProductUpdateRequestV2
            {
                products = new List<ProductUpdateV2>()
            };

            var customHeaders = new Dictionary<string, string> { { "x-api-key", ciceksepetiConfigurationV2.ApiKey } };

            foreach (var piLoop in request.Items)
            {
                var categoryAttributeUrl = $"{_baseUrl}/v1/categories/{piLoop.CategoryCode}/attributes";

                var cicekSepetiCategory = await this._httpHelperV3.SendAsync<ProductCategoryV2>(new HttpHelperV3Request
                {
                    RequestUri = categoryAttributeUrl,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

                var _brandCategoryAttribute = cicekSepetiCategory
                    .Content
                    .CategoryAttributes
                    .FirstOrDefault(x => x.AttributeName == "Marka");

                ProductInformationMarketplaceVariantValue categoryAttribute = null;
                if (_brandCategoryAttribute != null)
                {
                    var _brandAttributeValue = _brandCategoryAttribute.AttributeValues.FirstOrDefault(x => x.Name.ReplaceChar() == piLoop.BrandName.ReplaceChar());
                    if (_brandAttributeValue != null)
                    {
                        categoryAttribute = new ProductInformationMarketplaceVariantValue
                        {
                            MarketplaceVarinatCode = _brandCategoryAttribute.AttributeId.ToString(),
                            MarketplaceVariantValueCode = _brandAttributeValue.Id.ToString()

                        };
                    }
                }

                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    if (pinLoop.Description.Length < 10) pinLoop.Description += $" {pinLoop.ProductInformationName} ({pinLoop.StockCode})";

                    updateRequest.products.Add(new ProductUpdateV2
                    {
                        productName = pinLoop.ProductInformationName,
                        barcode = pinLoop.Barcode,
                        description = pinLoop.Description,
                        images = pinLoop.ProductInformationPhotos.Take(5).Select(x => x.Url).ToList(),
                        mediaLink = null,
                        deliveryMessageType = 5,
                        deliveryType = 2,
                        stockCode = pinLoop.StockCode,
                        isActive = true,
                        mainProductCode = piLoop.SellerCode
                    });
                }
            }

            var httpResultResponse = await this._httpHelperV3.SendAsync<ProductUpdateRequestV2, ProductCreateResponseV2>(
                     new HttpHelperV3Request<ProductUpdateRequestV2>
                     {
                         RequestUri = $"{_baseUrl}/v1/Products",
                         Request = updateRequest,
                         HttpMethod = HttpMethod.Put,
                         AuthorizationType = AuthorizationType.XApiKey,
                         Authorization = ciceksepetiConfigurationV2.ApiKey
                     });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.batchId
                    }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");
            var stockRequestV2s = new PriceStockRequestV2
            {
                Items = request
                    .Items
                    .Select(dpi => new PriceStockItemV2
                    {
                        listPrice = Convert.ToDouble(dpi.ListUnitPrice, culture),
                        salesPrice = Convert.ToDouble(dpi.UnitPrice, culture),
                        stockCode = dpi.StockCode,
                        StockQuantity = null
                    })
                    .ToList()
            };

            var requestUri = $"{_baseUrl}/v1/Products/price-and-stock";

            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceStockRequestV2, PriceStockResponseV2, PriceStockBadRequestResponseV2>(
                new HttpHelperV3Request<PriceStockRequestV2>
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Put,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey,
                    Request = stockRequestV2s
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.batchId;
            else if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = new()
                {
                    httpResultResponse.ContentError.Message
                };

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");
            var stockRequestV2s = new PriceStockRequestV2
            {
                Items = request
                    .Items
                    .Select(dpi => new PriceStockItemV2
                    {
                        listPrice = null,//dpi.UpdatePrice ? Convert.ToDouble(dpi.ListUnitPrice, culture) : null,
                        salesPrice = null,//dpi.UpdatePrice ? Convert.ToDouble(dpi.UnitPrice, culture) : null,
                        stockCode = dpi.StockCode,
                        StockQuantity = dpi.Stock
                    })
                    .ToList()
            };

            var requestUri = $"{_baseUrl}/v1/Products/price-and-stock";

            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceStockRequestV2, PriceStockResponseV2, PriceStockBadRequestResponseV2>(
                new HttpHelperV3Request<PriceStockRequestV2>
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Put,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey,
                    Request = stockRequestV2s
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.batchId;
            else if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = new()
                {
                    httpResultResponse.ContentError.Message
                };

            return httpHelperV3Response;
        }
        #endregion

        #region Validate
        #region Product
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 50))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 50).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 50 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 50);
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {

        } 
        #endregion

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {
            requests.ForEach(theRequest =>
            {
                var invalidItems = theRequest
                            .Items
                            .Where(i =>
                                i.LastRequestDateTime.HasValue &&
                                (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 31)
                            .ToList();

                if (invalidItems.HasItem())
                {
                    invalidItems.ForEach(i =>
                        i.ErrorMessage = "Güncellenmek istenen değer pazaryerine son 30 dk içerisinde bir kez gönderildi.");

                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);

                    theRequest.Items.RemoveAll(i =>
                        i.LastRequestDateTime.HasValue &&
                        (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 31);
                }
            });
        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {
            requests.ForEach(theRequest =>
            {
                var invalidItems = theRequest
                            .Items
                            .Where(i =>
                                i.LastRequestDateTime.HasValue &&
                                (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 31)
                            .ToList();

                if (invalidItems.HasItem())
                {
                    invalidItems.ForEach(i =>
                        i.ErrorMessage = "Güncellenmek istenen değer pazaryerine son 30 dk içerisinde bir kez gönderildi.");

                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);

                    theRequest.Items.RemoveAll(i =>
                        i.LastRequestDateTime.HasValue &&
                        (DateTime.Now - i.LastRequestDateTime.Value).TotalMinutes < 31);
                }
            });
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            return requests
                .SelectMany(r => r.Items.Select(i => new MarketplaceRequestCreateProduct
                {
                    Id = Guid.NewGuid(),
                    MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                    Items = new List<MarketplaceRequestCreateProductItem> { i }
                }))
                .ToList();
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            return requests
                .SelectMany(r => r.Items.Select(i => new MarketplaceRequestUpdateProduct
                {
                    Id = Guid.NewGuid(),
                    MarketplaceRequestHeader = r.MarketplaceRequestHeader,
                    Items = new List<MarketplaceRequestUpdateProductItem> { i }
                }))
                .ToList();
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceRequestUpdateStocks = new();

            requests.ForEach(theRequest =>
            {
                var pageCount = Math.Ceiling(theRequest.Items.Count / 200M);
                for (int i = 0; i < pageCount; i++)
                {
                    marketplaceRequestUpdateStocks.Add(new MarketplaceRequestUpdateStock
                    {
                        Id = Guid.NewGuid(),
                        MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                        Items = theRequest.Items.Skip(i * 200).Take(200).ToList()
                    });
                }
            });

            return marketplaceRequestUpdateStocks;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices = new();

            requests.ForEach(theRequest =>
            {
                var pageCount = Math.Ceiling(theRequest.Items.Count / 200M);
                for (int i = 0; i < pageCount; i++)
                {
                    marketplaceRequestUpdatePrices.Add(new MarketplaceRequestUpdatePrice
                    {
                        Id = Guid.NewGuid(),
                        MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                        Items = theRequest.Items.Skip(i * 200).Take(200).ToList()
                    });
                }
            });

            return marketplaceRequestUpdatePrices;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var requestUri = $"{_baseUrl}/v1/Products/batch-status/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<Response.ProductBatch.ProductBatchResponse>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse
                        .Content
                        .items
                        .Any(i => i.status.ToLower() == "pending" || i.status.ToLower() == "processing")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode
                    }))
                    .ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.data.stockCode);
                        switch (i.status.ToLower())
                        {
                            case "failed":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "warning":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "success":
                                trackingResponseItem.Success = true;
                                break;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var requestUri = $"{_baseUrl}/v1/Products/batch-status/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<Response.ProductBatch.ProductBatchResponse>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse
                        .Content
                        .items
                        .Any(i => i.status.ToLower() == "pending" || i.status.ToLower() == "processing")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode
                    }))
                    .ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.data.stockCode);
                        switch (i.status.ToLower())
                        {
                            case "failed":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "warning":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "success":
                                trackingResponseItem.Success = true;
                                break;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var requestUri = $"{_baseUrl}/v1/Products/batch-status/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<Response.Batch.BatchResponse>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse
                        .Content
                        .items
                        .Any(i => i.status.ToLower() == "pending" || i.status.ToLower() == "processing")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Success = true,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice
                    })
                    .ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.data.stockCode);
                        switch (i.status.ToLower())
                        {
                            case "failed":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "warning":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            var requestUri = $"{_baseUrl}/v1/Products/batch-status/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<Response.Batch.BatchResponse>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse
                        .Content
                        .items
                        .Any(i => i.status.ToLower() == "pending" || i.status.ToLower() == "processing")
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock,
                        Success = true
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.data.stockCode);

                        switch (i.status.ToLower())
                        {
                            case "failed":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                            case "warning":
                                trackingResponseItem.Success = false;
                                trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.message).ToList();
                                break;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            CiceksepetiConfigurationV2 ciceksepetiConfigurationV2 = Configuration(request);

            List<Category> categories = new();

            var response = await this._httpHelperV3.SendAsync<CicekSepetiCategory>(new HttpHelperV3Request
            {
                RequestUri = $"{_baseUrl}/v1/Categories",
                AuthorizationType = AuthorizationType.XApiKey,
                Authorization = ciceksepetiConfigurationV2.ApiKey
            });

            response.Content.Categories.ForEach(theCategory => this.FetchSubCategories(theCategory.SubCategories, categories));

            categories.ForEach(async theCategory =>
            {
                var response = await this._httpHelperV3.SendAsync<Response.CategoryAttribute.CicekSepetiCategory>(new HttpHelperV3Request
                {
                    RequestUri = $"{_baseUrl}/v1/categories/{theCategory.Code}/attributes",
                    AuthorizationType = AuthorizationType.XApiKey,
                    Authorization = ciceksepetiConfigurationV2.ApiKey
                });

                theCategory.CategoryAttributes.AddRange(response
                    .Content
                    .CategoryAttributes
                    .Where(x => (x.Type == "Ürün Özelliği" || x.Varianter) && x.AttributeName != "Marka")
                    .GroupBy(x => new { x.AttributeName, x.Required })
                    .Select(x => x.OrderByDescending(x => x.Varianter).First())
                    .Select(ca => new CategoryAttribute
                    {
                        Code = ca.AttributeId.ToString(),
                        Name = ca.AttributeName,
                        Mandatory = ca.Required || (ca.Varianter && ca.AttributeName.ToLower() == "renk"),
                        AllowCustom = false,
                        MultiValue = false,
                        Variantable = ca.Varianter,
                        CategoryCode = theCategory.Code,
                        CategoryAttributesValues = ca
                            .AttributeValues
                            .Select(cav => new CategoryAttributeValue
                            {
                                VarinatCode = ca.AttributeId.ToString(),
                                Code = cav.Id.ToString(),
                                Name = cav.Name
                            })
                            .ToList()
                    }));
            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private CiceksepetiConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private CiceksepetiConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }

        private void FetchSubCategories(List<CicekSepetiSubCategory> subCategories, List<Category> categories)
        {
            if (subCategories.HasItem())
                foreach (var theSubCategory in subCategories)
                {
                    if (theSubCategory.SubCategories.HasItem())
                        this.FetchSubCategories(theSubCategory.SubCategories, categories);
                    else
                        categories.Add(new()
                        {
                            Code = theSubCategory.Id.ToString(),
                            Name = theSubCategory.Name,
                            ParentCategoryCode = theSubCategory.ParentCategoryId.ToString()
                        });
                }
        }
        #endregion
    }
}