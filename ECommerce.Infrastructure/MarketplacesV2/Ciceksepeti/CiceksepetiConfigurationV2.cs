﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti
{
    public class CiceksepetiConfigurationV2
    {
        #region Members
        public static implicit operator CiceksepetiConfigurationV2(Dictionary<string, string> configurations)
        {
            return new CiceksepetiConfigurationV2
            {
                ApiKey = configurations["ApiKey"]
            };
        }
        #endregion

        #region Properties
        public string ApiKey { get; set; }
        #endregion
    }
}
