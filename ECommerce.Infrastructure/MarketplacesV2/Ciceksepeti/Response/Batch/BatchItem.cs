﻿using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.BatchStatus;
using System.Collections.Generic;
using System;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.Batch
{
    public class BatchItem
    {
        #region Properties
        public string itemId { get; set; }
        public Data data { get; set; }
        public string status { get; set; }
        public List<FailureReason> failureReasons { get; set; }
        public DateTime lastModificationDate { get; set; }
        #endregion
    }
}
