﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.Batch
{
    public class FailureReason
    {
        #region Properties
        public string message { get; set; }
        
        public int? code { get; set; }
        #endregion
    }
}
