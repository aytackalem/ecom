﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.Batch
{
    public class BatchResponse
    {
        #region Properties
        public string batchId { get; set; }
        
        public int itemCount { get; set; }
        #endregion

        #region Navigation Properties
        public List<BatchItem> items { get; set; }
        #endregion
    }
}
