﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.PriceStock
{
    public class PriceStockBadRequestResponseV2
    {
        #region Properties
        public string Message { get; set; }
        #endregion
    }
}
