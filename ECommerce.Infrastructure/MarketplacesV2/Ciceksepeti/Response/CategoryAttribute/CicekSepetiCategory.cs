﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.CategoryAttribute
{
    public class CicekSepetiCategory
    {
        #region Properties
        public int CategoryId { get; set; }
        
        public string CategoryName { get; set; }

        public List<CicekSepetiCategoryAttribute> CategoryAttributes { get; set; }
        #endregion
    }
}
