﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.CategoryAttribute
{
    public class CicekSepetiAttributeValue
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
