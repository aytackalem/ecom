﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.ProductCreate
{
    public class ProductCreateBadRequestResponseV2
    {
        #region Properties
        public string Message { get; set; }
        #endregion
    }
}
