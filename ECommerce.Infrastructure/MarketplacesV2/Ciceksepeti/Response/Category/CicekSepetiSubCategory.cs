﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.Category
{
    public class CicekSepetiSubCategory
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        
        public int ParentCategoryId { get; set; }
        
        public List<CicekSepetiSubCategory> SubCategories { get; set; }
        #endregion
    }
}
