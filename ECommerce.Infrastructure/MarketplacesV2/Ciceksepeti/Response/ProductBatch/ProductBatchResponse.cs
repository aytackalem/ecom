﻿using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Response.ProductBatch
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Attribute
    {
        public int id { get; set; }
        public int valueId { get; set; }
        public int textLength { get; set; }
    }

    public class Data
    {
        public string siteCode { get; set; }
        public string productName { get; set; }
        public string mainProductCode { get; set; }
        public string stockCode { get; set; }
        public int categoryId { get; set; }
        public string description { get; set; }
        public object mediaLink { get; set; }
        public int deliveryMessageType { get; set; }
        public int deliveryType { get; set; }
        public int stockQuantity { get; set; }
        public double salesPrice { get; set; }
        public double listPrice { get; set; }
        public string barcode { get; set; }
        public object origin { get; set; }
        public object productHarmonyCode { get; set; }
        public List<string> images { get; set; }
        public List<Attribute> attributes { get; set; }
    }

    public class Item
    {
        public Data data { get; set; }
        public string itemId { get; set; }
        public string status { get; set; }
        public List<FailureReason> failureReasons { get; set; }
        public DateTime lastModificationDate { get; set; }
    }

    public class FailureReason
    {
        #region Properties
        public string message { get; set; }

        public int? code { get; set; }
        #endregion
    }

    public class ProductBatchResponse
    {
        public string batchId { get; set; }
        public int itemCount { get; set; }
        public List<Item> items { get; set; }
    }


}
