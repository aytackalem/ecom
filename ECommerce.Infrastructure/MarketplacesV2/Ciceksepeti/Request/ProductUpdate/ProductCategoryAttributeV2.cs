﻿using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.CategoryAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductUpdate
{
    public class ProductCategoryAttributeV2
    {
        #region Properties
        public int AttributeId { get; set; }

        public string AttributeName { get; set; }

        public bool Required { get; set; }

        public bool Varianter { get; set; }

        public string Type { get; set; }

        public List<ProductAttributeValueV2> AttributeValues { get; set; }
        #endregion
    }
}
