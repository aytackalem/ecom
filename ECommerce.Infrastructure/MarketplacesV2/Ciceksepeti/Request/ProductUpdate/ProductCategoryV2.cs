﻿using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.CategoryAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductUpdate
{
    public class ProductCategoryV2
    {
        #region Properties
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public List<ProductCategoryAttributeV2> CategoryAttributes { get; set; }
        #endregion
    }
}
