﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductUpdate
{
    public class ProductAttributeValueV2
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
