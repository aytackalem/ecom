﻿using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductCreate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductUpdate
{
    public class ProductUpdateRequestV2
    {
        #region Navigation Properties
        public List<ProductUpdateV2> products { get; set; }
        #endregion
    }
}
