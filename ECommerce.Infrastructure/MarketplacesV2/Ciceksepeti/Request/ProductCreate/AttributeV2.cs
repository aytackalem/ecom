﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductCreate
{
    public class AttributeV2
    {
        #region Properties
        public int id { get; set; }
        
        public int valueId { get; set; }
        
        public int textLength { get; set; } 
        #endregion
    }
}
