﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductCreate
{
    public class ProductCreateV2
    {
        #region Properties
        /// <summary>
        /// Ürün adı
        /// </summary>
        public string productName { get; set; }
        /// <summary>
        /// Tedarikçi ürün kodu
        /// </summary>
        public string mainProductCode { get; set; }
        /// <summary>
        /// Tedarikçi varyant kodu	
        /// </summary>
        public string stockCode { get; set; }
        /// <summary>
        /// Ürün/varyant kategori kodu, kategori listesinden çekilecek	
        /// </summary>
        public int categoryId { get; set; }
        /// <summary>
        /// Ürün/varyant açıklaması	
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Ürün/varyant ile ilgili video linki(youtube). imaj linki gönderilmemelidir.	
        /// </summary>
        public string mediaLink { get; set; }
        /// <summary>
        /// 1:Çiçek_Servis 
        /// 4: Hediye Kargo AynıGün, 
        /// 5:Hediye Kargo 1-3 Is Gunu, 
        /// 18: Hediye Kargo 1-2 Is Gunu
        /// </summary>
        public int deliveryMessageType { get; set; }

        /// <summary>
        /// 1:Servis Aracı İle Gönderim,
        /// 2:Kargo İle Gönderim,
        /// 3: Kargo+Servis Aracı İle Gönderim
        /// </summary>
        public int deliveryType { get; set; }
        /// <summary>
        /// Ürün stok adedi	
        /// </summary>
        public int stockQuantity { get; set; }
        /// <summary>
        /// Satış fiyatı	
        /// </summary>
        public double salesPrice { get; set; }
        /// <summary>
        /// Üstü çizili fiyat	
        /// </summary>
        public double listPrice { get; set; }
        /// <summary>
        /// Ürün Barkodu 40 karakter üzerinde olmamalıdır. Barkodunuzda "? / & % + ^ ' * _" boşluk gibi özel karakterler kullanılamaz. Barkodunuzun ortasında boşluk varsa birleştirilerek içeri alınır.	
        /// </summary>
        public string barcode { get; set; }
        /// <summary>
        /// Ürün/varyant görseli	
        /// </summary>
        public List<string> images { get; set; }

        /// <summary>
        /// Kategori özellikleri
        /// </summary>
        public List<AttributeV2> attributes { get; set; }
        #endregion
    }
}
