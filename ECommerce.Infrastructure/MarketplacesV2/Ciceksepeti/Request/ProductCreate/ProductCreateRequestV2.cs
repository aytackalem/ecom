﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.ProductCreate
{
    public class ProductCreateRequestV2
    {
        #region Navigation Properties
        public List<ProductCreateV2> products { get; set; }
        #endregion
    }
}
