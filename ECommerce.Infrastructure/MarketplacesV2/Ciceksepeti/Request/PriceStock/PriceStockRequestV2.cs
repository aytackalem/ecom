﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.PriceStock
{
    public class PriceStockRequestV2
    {
        #region Navigation Properties
        [JsonProperty("items")]
        public List<PriceStockItemV2> Items { get; set; }
        #endregion
    }
}
