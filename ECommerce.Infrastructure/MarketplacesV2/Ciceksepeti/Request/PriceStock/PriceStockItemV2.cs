﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti.Request.PriceStock
{
    public class PriceStockItemV2
    {
        #region Properties
        public string stockCode { get; set; }
        
        public int? StockQuantity { get; set; }
        
        public double? listPrice { get; set; }
        
        public double? salesPrice { get; set; }
        #endregion
    }
}
