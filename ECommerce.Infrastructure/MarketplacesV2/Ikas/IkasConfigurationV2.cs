﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas
{
    public class IkasConfigurationV2
    {
        #region Members
        public static implicit operator IkasConfigurationV2(Dictionary<string, string> configurations)
        {
            return new IkasConfigurationV2
            {
                GrantType = "client_credentials", //configurations["GrantType"],
                ClientId = configurations["ClientId"],
                ClientSecret = configurations["ClientSecret"],
                StockLocationId = configurations["StockLocationId"],
                Url = configurations["Url"],
                SalesChannelId = configurations["SalesChannelId"],
                SlugTemplate = configurations["SlugTemplate"],
                TitleTemplate = configurations["TitleTemplate"],
                MetaDescriptionTemplate = configurations["MetaDescriptionTemplate"],
            };
        }
        #endregion

        #region Properties
        public string GrantType { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string StockLocationId { get; set; }

        public string SalesChannelId { get; set; }

        public string Url { get; set; }

        public string SlugTemplate { get; set; }

        public string TitleTemplate { get; set; }

        public string MetaDescriptionTemplate { get; set; }
        #endregion
    }
}
