﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Request
{
    public class ProductImageRequest
    {
        #region Navigation Properties
        public ProductImage productImage { get; set; }
        #endregion
    }
}
