﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Request
{
    public class ProductImage
    {
        #region Properties
        public string[] variantIds { get; set; }

        public int order { get; set; }

        public bool isMain { get; set; }

        public string url { get; set; }
        #endregion
    }
}
