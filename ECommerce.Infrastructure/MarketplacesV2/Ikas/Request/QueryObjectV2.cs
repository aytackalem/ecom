﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Request
{
    public class QueryObjectV2
    {
        #region Properties
        [JsonProperty("query")]
        public string Query { get; set; }
        #endregion
    }
}
