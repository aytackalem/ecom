﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.SaveProduct
{
    public class Data
    {


        public SaveProduct saveProduct { get; set; }
    }

    public class Error
    {
        public string message { get; set; }
        public List<string> path { get; set; }
        public Extensions extensions { get; set; }
    }

    public class Extensions
    {
        public string code { get; set; }
        public string serviceName { get; set; }

        public Validationerror[] validationErrors { get; set; }
    }


    public class Validationerror
    {
        public string property { get; set; }
        public string[] value { get; set; }
        public Constraints constraints { get; set; }
    }

    public class Constraints
    {
        public string ValidateObjectId { get; set; }
    }


    public class SaveProductResponse
    {
        public List<Error> errors { get; set; }

        public Data data { get; set; }
    }

    public class SaveProduct
    {
        public string id { get; set; }

        public List<Variant> variants { get; set; }
    }
}
