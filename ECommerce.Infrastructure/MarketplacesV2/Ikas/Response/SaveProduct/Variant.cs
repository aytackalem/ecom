﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.SaveProduct
{
    public class Variant
    {
        #region Properties
        public string Id { get; set; }
        #endregion
    }
}
