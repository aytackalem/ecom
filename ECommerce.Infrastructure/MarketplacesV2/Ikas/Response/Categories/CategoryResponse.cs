﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Category
{
    public class CategoryResponse
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
