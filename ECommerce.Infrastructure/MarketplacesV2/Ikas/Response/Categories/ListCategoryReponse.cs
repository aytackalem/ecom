﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Category
{
    public class ListCategoryReponse
    {
        #region Properties
        public List<CategoryResponse> ListCategory { get; set; }
        #endregion
    }
}
