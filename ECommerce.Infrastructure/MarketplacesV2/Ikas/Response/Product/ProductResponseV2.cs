﻿using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Product
{
    public class Brand
    {
        public int id { get; set; }
        public string name { get; set; }
        public string distributorCode { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string distributorCode { get; set; }
    }

    public class Countdown
    {
        public int id { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public DateTime expireDate { get; set; }
        public int useCountDown { get; set; }
    }

    public class Currency
    {
        public int id { get; set; }
        public string label { get; set; }
        public string abbr { get; set; }
    }

    public class Detail
    {
        public int id { get; set; }
        public string details { get; set; }
        public string extraDetails { get; set; }
    }

    public class Image
    {
        public int id { get; set; }
        public string filename { get; set; }
        public string extension { get; set; }
        public string directoryName { get; set; }
        public string revision { get; set; }
        public int sortOrder { get; set; }
    }

    public class Parent
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string fullName { get; set; }
        public string sku { get; set; }
        public string barcode { get; set; }
        public int price1 { get; set; }
        public int warranty { get; set; }
        public int tax { get; set; }
        public int stockAmount { get; set; }
        public int volumetricWeight { get; set; }
        public int buyingPrice { get; set; }
        public string stockTypeLabel { get; set; }
        public int discount { get; set; }
        public int discountType { get; set; }
        public int moneyOrderDiscount { get; set; }
        public int status { get; set; }
        public int taxIncluded { get; set; }
        public string distributor { get; set; }
        public int isGifted { get; set; }
        public string gift { get; set; }
        public int customShippingDisabled { get; set; }
        public int customShippingCost { get; set; }
        public int marketPriceDetail { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string metaKeywords { get; set; }
        public string metaDescription { get; set; }
        public string canonicalUrl { get; set; }
        public string pageTitle { get; set; }
        public int hasOption { get; set; }
        public string shortDetails { get; set; }
        public string searchKeywords { get; set; }
        public string installmentThreshold { get; set; }
        public int? homeSortOrder { get; set; }
        public int? popularSortOrder { get; set; }
        public int? brandSortOrder { get; set; }
        public int? featuredSortOrder { get; set; }
        public int? campaignedSortOrder { get; set; }
        public int? newSortOrder { get; set; }
        public int? discountedSortOrder { get; set; }
        public Brand brand { get; set; }
        public Currency currency { get; set; }
        public Parent parent { get; set; }
        public Countdown countdown { get; set; }
        public List<Price> prices { get; set; }
        public List<Image> images { get; set; }
        public List<Detail> details { get; set; }
        public List<ProductToCategory> productToCategories { get; set; }
    }

    public class Price
    {
        public int id { get; set; }
        public int value { get; set; }
        public int type { get; set; }
    }

    public class ProductToCategory
    {
        public int id { get; set; }
        public int? sortOrder { get; set; }
        public Category category { get; set; }
    }

    public class ProductResponseV2
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string fullName { get; set; }
        public string sku { get; set; }
        public string barcode { get; set; }
        public decimal price1 { get; set; }
        public int warranty { get; set; }
        public decimal tax { get; set; }
        public double stockAmount { get; set; }
        public double volumetricWeight { get; set; }
        public decimal buyingPrice { get; set; }
        public string stockTypeLabel { get; set; }
        public decimal discount { get; set; }
        public int discountType { get; set; }
        public decimal moneyOrderDiscount { get; set; }
        public int status { get; set; }
        public decimal taxIncluded { get; set; }
        public string distributor { get; set; }
        public int isGifted { get; set; }
        public string gift { get; set; }
        public int customShippingDisabled { get; set; }
        public decimal customShippingCost { get; set; }
        public decimal? marketPriceDetail { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string metaKeywords { get; set; }
        public string metaDescription { get; set; }
        public string canonicalUrl { get; set; }
        public string pageTitle { get; set; }
        public int hasOption { get; set; }
        public string shortDetails { get; set; }
        public string searchKeywords { get; set; }
        public string installmentThreshold { get; set; }
        public int? homeSortOrder { get; set; }
        public int? popularSortOrder { get; set; }
        public int? brandSortOrder { get; set; }
        public int? featuredSortOrder { get; set; }
        public int? campaignedSortOrder { get; set; }
        public int? newSortOrder { get; set; }
        public int? discountedSortOrder { get; set; }
        public Brand brand { get; set; }
        public Currency currency { get; set; }
        public Parent parent { get; set; }
        public Countdown countdown { get; set; }
        public List<Price> prices { get; set; }
        public List<Image> images { get; set; }
        public List<Detail> details { get; set; }
        public List<ProductToCategory> productToCategories { get; set; }
    }


}