﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Token
{
    public class TokenResponse
    {
        #region Properties
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        #endregion
    }
}
