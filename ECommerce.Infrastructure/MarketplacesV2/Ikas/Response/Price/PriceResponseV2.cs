﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Price
{
    public class PriceResponseV2
    {
        #region Properties
        public PriceResponseDataV2 data { get; set; }
        #endregion
    }
}
