﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Price
{
    public class PriceResponseDataV2
    {
        #region Properties
        public bool saveVariantPrices { get; set; }
        #endregion
    }
}
