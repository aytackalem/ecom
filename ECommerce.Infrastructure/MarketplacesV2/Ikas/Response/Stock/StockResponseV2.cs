﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Stock
{
    public class StockResponseV2
    {
        #region Properties
        public StockResponseDataV2 data { get; set; }
        #endregion
    }
}
