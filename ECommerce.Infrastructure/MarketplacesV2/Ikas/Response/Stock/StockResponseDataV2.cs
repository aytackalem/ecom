﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Stock
{
    public class StockResponseDataV2
    {
        #region Properties
        public bool saveProductStockLocations { get; set; }
        #endregion
    }
}
