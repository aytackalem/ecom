﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Variant
{
    public class ListVariantTypeReponse
    {
        #region Properties
        public List<VariantType> ListVariantType { get; set; }
        #endregion
    }
}
