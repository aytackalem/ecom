﻿namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Variant
{
    public class VariantValue
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
