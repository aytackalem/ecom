﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Variant
{
    public class VariantType
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantValue> Values { get; set; }
        #endregion
    }
}
