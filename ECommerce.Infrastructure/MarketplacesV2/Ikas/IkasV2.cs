﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Request;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Category;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Price;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.SaveProduct;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Token;
using ECommerce.Infrastructure.MarketplacesV2.Ikas.Response.Variant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Ikas
{
    public class IkasV2 : 
        IMarketplaceV2, 
        IMarketplaceV2CreateProductUntrackable, 
        IMarketplaceV2StockPriceUntrackable, 
        IMarketplaceV2CategoryGetable
    {
        #region Constants
        private readonly string _tokenUrl = "https://entegre.myikas.com/api/admin/oauth/token";

        private readonly string _graphQlUrl = "https://api.myikas.com/api/v1/admin/graphql";

        private readonly string _imageUploadUrl = "https://api.myikas.com/api/v1/admin/product/upload/image";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public IkasV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.IKAS;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerStockRequest => 2;

        public int? WaitingSecondPerPriceRequest => 2;

        public bool IsUpsert => true;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<UntrackableCreateProductProcessResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            IkasConfigurationV2 ikasConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var token = await this.GetToken(ikasConfigurationV2);

            var item = request.Items.First();

            var productInformationMarketplaces = item
                .ProductInformationMarketplaces
                .Where(pim => pim.ProductInformationPhotos.HasItem())
                .ToList();

            //TO DO: Prepare "Product Variant Types".
            //TO DO: Sort "Renk" And "Beden" variants.
            productInformationMarketplaces
                .ForEach(pim =>
                {
                    var searchIndex = 0;

                    var renk = pim
                        .ProductInformationMarketplaceVariantValues
                        .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renk");
                    if (renk != null)
                    {
                        var renkIndex = pim.ProductInformationMarketplaceVariantValues.IndexOf(renk);
                        if (renkIndex != searchIndex)
                        {
                            pim.ProductInformationMarketplaceVariantValues.RemoveAt(renkIndex);
                            pim.ProductInformationMarketplaceVariantValues.Insert(searchIndex, renk);
                        }
                    }

                    searchIndex = renk != null ? 1 : 0;

                    var beden = pim
                        .ProductInformationMarketplaceVariantValues
                        .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "beden");
                    if (beden != null)
                    {
                        var bedenIndex = pim.ProductInformationMarketplaceVariantValues.IndexOf(beden);
                        if (bedenIndex != searchIndex)
                        {
                            pim.ProductInformationMarketplaceVariantValues.RemoveAt(bedenIndex);
                            pim.ProductInformationMarketplaceVariantValues.Insert(searchIndex, beden);
                        }
                    }

                });

            productInformationMarketplaces = productInformationMarketplaces
                .OrderBy(pim => pim
                    .ProductInformationMarketplaceVariantValues
                    .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renk")?.MarketplaceVariantValueName)
                .ThenBy(pim => pim
                    .ProductInformationMarketplaceVariantValues
                    .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "beden")?.MarketplaceVariantValueName)
                .ToList();

            var groupedProductVariantTypes = productInformationMarketplaces
                .SelectMany(pim => pim
                    .ProductInformationMarketplaceVariantValues
                    .Select(pimvv => new
                    {
                        variantTypeId = pimvv.MarketplaceVarinatCode,
                        variantValueId = pimvv.MarketplaceVariantValueCode
                    }))
                .GroupBy(a => a.variantTypeId)
                .ToList();

            int order = 1;
            List<string> productVariantTypes = new();
            foreach (var variantType in groupedProductVariantTypes)
            {
                productVariantTypes.Add($@"{{
    order: {order},
    variantTypeId: ""{variantType.Key}"",
    variantValueIds: [{string.Join(",", variantType.GroupBy(x => x.variantValueId).Select(x => $"\"{x.Key}\""))}]
}}");
                order++;
            }

            var slug = ikasConfigurationV2
                .SlugTemplate
                .Replace("-", " ")
                .Replace("#urunAdi", item.ProductName)
                .Replace("#modelKodu", item.SellerCode)
                .GenerateUrl()
                .Replace("\n", ""); ;
            var title = ikasConfigurationV2
                .TitleTemplate
                .Replace("#urunAdi", item.ProductName)
                .Replace("#modelKodu", item.SellerCode)
                 .Replace("\n", ""); ;
            var metaDescription = ikasConfigurationV2
                .MetaDescriptionTemplate
                .Replace("#urunAdi", item.ProductName)
                .Replace("#modelKodu", item.SellerCode)
                .Replace("\n", "");

            var queryObject = new QueryObjectV2
            {
                Query = @$"
mutation {{
    saveProduct(input: {{
        id: ""{item.ProductMarketplaceUUId.ToLower()}"",
        metaData: {{
            id: ""{item.ProductMarketplaceUUId.ToLower()}"",
            slug: ""{slug}"",
            pageTitle: ""{title}"",
            description: ""{metaDescription}""
        }}
        name: ""{item.ProductName.Replace("\n", "")}"",
        type: PHYSICAL,
        brandId: ""{item.BrandCode}"",
        categoryIds: [""{item.CategoryCode}""],
        groupVariantsByVariantTypeId:""{groupedProductVariantTypes.FirstOrDefault()?.Key}""
        description: ""{productInformationMarketplaces.FirstOrDefault()?.Description.Replace("\"", "'").Replace("\r", "").Replace("\n", "")}"",
        productVariantTypes:[{string.Join(",", productVariantTypes)}],
        salesChannelIds: [""{ikasConfigurationV2.SalesChannelId}""],
        variants:[{string.Join(",", productInformationMarketplaces.Select(pim => @$"
{{
    id: ""{pim.ProductInformationUUId.ToLower()}"",
    isActive: true,
    barcodeList: [""{pim.Barcode}""],
    sku: ""{pim.StockCode}"",
    variantValueIds: [{string.Join(",", pim.ProductInformationMarketplaceVariantValues.Select(pimvv => $@"{{
        variantTypeId: ""{pimvv.MarketplaceVarinatCode}""
        variantValueId: ""{pimvv.MarketplaceVariantValueCode}""
    }}"))}],
    prices:[{{
        sellPrice: {pim.ListUnitPrice},
        {(pim.UnitPrice < pim.ListUnitPrice ? $"discountPrice: {pim.UnitPrice}" : "")}
    }}]
}}"))}]
    }}) {{
        id,
        variants {{
            id,
        images{{
        imageId
              }}
        }}
    }}
}}"
            };

            var saveProductResponse = await this
                ._httpHelperV3
                .SendAsync<QueryObjectV2, SaveProductResponse>(
                    new HttpHelperV3Request<QueryObjectV2>
                    {
                        RequestUri = this._graphQlUrl,
                        Request = queryObject,
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token
                    });

            if (saveProductResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                if (saveProductResponse.Content.data == null || saveProductResponse.Content.errors.HasItem())
                {
                    List<string> messages = new();

                    foreach (var error in saveProductResponse.Content.errors)
                    {
                        string message = $"{error.message}. ";
                        message += string.Join("", error?.extensions?.validationErrors?.Select(_ => $"{_.property} değeri hatalı."));

                        messages.Add(message);
                    }

                    return new()
                    {
                        HttpStatusCode = System.Net.HttpStatusCode.OK,
                        Content = new()
                        {
                            Messages = messages,
                            IncludeUrl = true,
                            MarkDirtyStock = true,
                            MarkDirtyPrice = false,
                            UntrackableProcessResponseItems = item
                                    .ProductInformationMarketplaces
                                    .Select(pim => new UntrackableCreateProductProcessResponseItem
                                    {
                                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                                        Url = $"{(ikasConfigurationV2.Url.EndsWith('/') ? ikasConfigurationV2.Url : $"{ikasConfigurationV2.Url}/")}{slug}",
                                        Success = false,
                                        Messages = messages
                                    })
                                    .ToList()
                        }
                    };
                }

                foreach (var theVariant in saveProductResponse.Content.data.saveProduct.variants)
                {
                    var theVariantIndex = saveProductResponse
                        .Content
                        .data
                        .saveProduct
                        .variants
                        .IndexOf(theVariant);

                    foreach (var theImage in productInformationMarketplaces[theVariantIndex].ProductInformationPhotos)
                    {
                        var theImageIndex = productInformationMarketplaces[theVariantIndex]
                            .ProductInformationPhotos
                            .IndexOf(theImage);

                        var productImageRequest = new ProductImageRequest
                        {
                            productImage = new ProductImage
                            {
                                variantIds = new string[] { theVariant.Id },
                                url = theImage.Url,
                                order = theImage.DisplayOrder,
                                isMain = theImageIndex == 0
                            }
                        };

                        /* Her bir istek öncesi 2sn ye bekler. */
                        Thread.Sleep(2000);

                        await this
                               ._httpHelperV3
                               .SendAsync<ProductImageRequest>(
                                   new HttpHelperV3Request<ProductImageRequest>
                                   {
                                       RequestUri = this._imageUploadUrl,
                                       Request = productImageRequest,
                                       HttpMethod = HttpMethod.Post,
                                       AuthorizationType = AuthorizationType.Bearer,
                                       Authorization = token
                                   });
                    }
                }
            }

            return new()
            {
                HttpStatusCode = saveProductResponse.HttpStatusCode,
                Content = new()
                {
                    IncludeUrl = true,
                    MarkDirtyStock = true,
                    MarkDirtyPrice = false,
                    UntrackableProcessResponseItems = item
                        .ProductInformationMarketplaces
                        .Select(pim => new UntrackableCreateProductProcessResponseItem
                        {
                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                            Url = $"{(ikasConfigurationV2.Url.EndsWith('/') ? ikasConfigurationV2.Url : $"{ikasConfigurationV2.Url}/")}{slug}",
                            Success = saveProductResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                        }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            IkasConfigurationV2 ikasConfigurationV2 = request.MarketplaceRequestHeader.MarketplaceConfigurations.Decrypt();

            var queryObjectV2 = new QueryObjectV2
            {
                Query = @$"
            mutation {{
                saveVariantPrices(input: {{
                    variantPriceInputs: [{string.Join(",", request.Items.Select(dpi => $@"
                    {{
                        productId: ""{dpi.ProductUUId}""
                        price: {{
                            sellPrice: {dpi.ListUnitPrice},
                            {(dpi.UnitPrice < dpi.ListUnitPrice ? $"discountPrice: {dpi.UnitPrice}" : "")}
                        }},
                        variantId: ""{dpi.ProductInformationUUId}""    
                    }}
"))}]
                }})
            }}"
            };

            var priceResponseV2 = await this._httpHelperV3.SendAsync<QueryObjectV2, PriceResponseV2>(
                new HttpHelperV3Request<QueryObjectV2>
                {
                    RequestUri = this._graphQlUrl,
                    Request = queryObjectV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = await this.GetToken(ikasConfigurationV2)
                });

            if (priceResponseV2.Content.data is null)
            {

            }

            if (priceResponseV2.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {

            }

            await Console.Out.WriteLineAsync(priceResponseV2.ContentString);

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = priceResponseV2.HttpStatusCode,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = priceResponseV2.HttpStatusCode == System.Net.HttpStatusCode.OK,
                            ListUnitPrice = i.ListUnitPrice,
                            UnitPrice = i.UnitPrice
                        }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            IkasConfigurationV2 ikasConfigurationV2 = request
                .MarketplaceRequestHeader
                .MarketplaceConfigurations
                .Decrypt();

            var queryObjectV2 = new QueryObjectV2
            {
                Query = @$"
mutation {{
    saveProductStockLocations(input: {{
        productStockLocationInputs: [{string.Join(",", request.Items.Select(dpi => $@"
                    {{
                        productId: ""{dpi.ProductUUId}""
                        stockCount: {dpi.Stock}
                        stockLocationId: ""{ikasConfigurationV2.StockLocationId}""
                        variantId: ""{dpi.ProductInformationUUId}""
                    }}
"))}]
    }})
}}"
            };

            var stockResponseV2 = await this._httpHelperV3.SendAsync<QueryObjectV2, StockResponseV2>(
                new HttpHelperV3Request<QueryObjectV2>
                {
                    RequestUri = this._graphQlUrl,
                    Request = queryObjectV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = await this.GetToken(ikasConfigurationV2)
                });

            await Console.Out.WriteLineAsync(stockResponseV2.ContentString);

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = stockResponseV2.HttpStatusCode,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = stockResponseV2.HttpStatusCode == System.Net.HttpStatusCode.OK,
                            Stock = i.Stock
                        }).ToList()
                }
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.BrandCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.BrandCode)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Marka bilgisi bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.BrandCode));
                }

                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.ProductName)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.ProductName)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Ürün adı bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.ProductName));
                }

                if (theRequest.Items.Any(_ => string.IsNullOrEmpty(_.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.CategoryCode)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Katrgori bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = theRequest.Items.Count;
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1).Take(1).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            IkasConfigurationV2 ikasConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            String token = await this.GetToken(ikasConfigurationV2);

            List<Category> categories = new();

            var queryObject = new QueryObjectV2
            {
                Query = @"
{
    listCategory{
        id
        name
    }
}"
            };

            var listCategoryResponse = await this._httpHelperV3.SendAsync<QueryObjectV2, ListCategoryReponse>(new HttpHelperV3Request<QueryObjectV2>
            {
                RequestUri = this._graphQlUrl,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token,
                HttpMethod = HttpMethod.Post,
                Request = queryObject
            });
            categories.AddRange(listCategoryResponse.Content.ListCategory.Select(lc => new Category
            {
                Code = lc.Id,
                Name = lc.Name
            }));

            queryObject = new QueryObjectV2
            {
                Query = @"
{
    listVariantType{
        id
        name,
        values{
            id
            name
        }
    }
}"
            };

            var listVariantTypeResponse = await this._httpHelperV3.SendAsync<QueryObjectV2, ListVariantTypeReponse>(new HttpHelperV3Request<QueryObjectV2>
            {
                RequestUri = this._graphQlUrl,
                AuthorizationType = AuthorizationType.Bearer,
                Authorization = token,
                HttpMethod = HttpMethod.Post,
                Request = queryObject
            });

            categories.ForEach(theCategory =>
            {
                theCategory.CategoryAttributes = listVariantTypeResponse.Content.ListVariantType
                        .Select(v => new CategoryAttribute
                        {
                            Code = v.Id,
                            Name = v.Name,
                            Mandatory = true,
                            AllowCustom = false,
                            MultiValue = false,
                            CategoryCode = theCategory.Code,
                            CategoryAttributesValues = v
                                .Values
                                .Select(vv => new CategoryAttributeValue
                                {
                                    Code = vv.Id,
                                    Name = vv.Name,
                                    VarinatCode = v.Id
                                })
                                .ToList()
                        })
                        .ToList();
            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private async Task<string> GetToken(IkasConfigurationV2 ikasConfigurationV2)
        {
            var data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("grant_type", ikasConfigurationV2.GrantType),
                new KeyValuePair<string, string>("client_id", ikasConfigurationV2.ClientId),
                new KeyValuePair<string, string>("client_secret", ikasConfigurationV2.ClientSecret)
            };

            var tokenResponse = await this._httpHelperV3.SendAsync<TokenResponse>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = this._tokenUrl,
                Data = data,
                HttpMethod = HttpMethod.Post
            });
            return tokenResponse.Content.AccessToken;
        }
        #endregion
    }
}