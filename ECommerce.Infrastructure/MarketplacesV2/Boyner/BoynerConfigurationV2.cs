﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner
{
    public class BoynerConfigurationV2
    {
        #region Members
        public static implicit operator BoynerConfigurationV2(Dictionary<string, string> configurations)
        {
            return new BoynerConfigurationV2
            {
                Username = configurations["ApiKey"],
                Password = configurations["ApiSecretKey"],
                SupplierId = configurations["SupplierId"]
            };
        } 
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));

        public string SupplierId { get; set; }
        #endregion
    }
}
