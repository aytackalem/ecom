﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Request.PriceStocks
{
    public class BoynerUpdatePriceRequestV2
    {
        [JsonProperty("items")]
        public List<BoynerUpdatePriceItem> Items { get; set; }

    }

    public class BoynerUpdatePriceItem
    {
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("listPrice")]
        public decimal ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal SalePrice { get; set; }
    }
}
