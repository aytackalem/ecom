﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplacesV2.Boyner;
using ECommerce.Infrastructure.MarketplacesV2.Boyner.Request.PriceStocks;
using ECommerce.Infrastructure.MarketplacesV2.Boyner.Request.Products;
using ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Batch;
using ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.PriceStocks;
using ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner
{
    public class BoynerV2 :
        IMarketplaceV2,
        IMarketplaceV2CreateProductTrackable,
        IMarketplaceV2UpdateProductTrackable,
        IMarketplaceV2StockPriceTrackable
    {
     
        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public BoynerV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Boyner;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods


        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);

            var productCreateRequestV2 = new BoynerProductCreateRequestV2
            {
                Items = new List<BoynerProductCreateItem>()
            };

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new BoynerProductCreateItem
                    {
                        Barcode = pinLoop.Barcode,
                        Title = pinLoop.ProductInformationName,
                        ProductMainId = piLoop.SellerCode, // Guid.NewGuid().ToString();//
                        BrandId = Convert.ToInt32(piLoop.BrandCode), //bakılacak
                        CategoryId = Convert.ToInt32(piLoop.CategoryCode),
                        Quantity = pinLoop.Stock,
                        StockCode = pinLoop.StockCode,
                        Description = pinLoop.Description,
                        CurrencyType = "TRY",
                        ListPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        SalePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        VatRate = Convert.ToInt32(pinLoop.VatRate * 100)
                    };
                    item.Description = pinLoop.Description;
                    item.DeliveryDuration = piLoop.DeliveryDay == 0 ? 2 : (piLoop.DeliveryDay > 9 ? 9 : piLoop.DeliveryDay);
                    item.CargoCompanyId = 4;//Yurtiçi Kargo Marketplace
                    item.BatchRequestType = 1;
                    item.DimensionalWeight = Convert.ToInt32(piLoop.DimensionalWeight);
                    item.Images = new List<BoynerProductCreateImage>();
                    item.Attributes = new List<BoynerProductCreateAttribute>();

                    foreach (var image in pinLoop.ProductInformationPhotos.Take(8))
                    {
                        item.Images.Add(new BoynerProductCreateImage
                        {
                            Url = image.Url
                        });
                    }

                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        if (attribute.AllowCustom)
                        {
                            item.Attributes.Add(new BoynerProductCreateAttribute
                            {
                                AttributeId = Convert.ToInt32(attribute.MarketplaceVarinatCode),
                                CustomAttributeValue = attribute.MarketplaceVariantValueName,
                                AttributeValueId = null
                            });
                        }
                        else
                        {
                            item.Attributes.Add(new BoynerProductCreateAttribute
                            {
                                AttributeId = Convert.ToInt32(attribute.MarketplaceVarinatCode),
                                AttributeValueId = Convert.ToInt32(attribute.MarketplaceVariantValueCode)
                            });
                        }
                    }

                    productCreateRequestV2.Items.Add(item);
                }
            }


            var httpResultResponse = await this
             ._httpHelperV3
             .SendAsync<BoynerProductCreateRequestV2, BoynerUpdateProductResponseV2, BoynerProductCreateBadRequestResponseV2>(
            new HttpHelperV3Request<BoynerProductCreateRequestV2>
            {
                     RequestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{BoynerConfigurationV2.SupplierId}/v2/products",
                     Request = productCreateRequestV2,
                     HttpMethod = HttpMethod.Post,
                     AuthorizationType = AuthorizationType.Basic,
                     Authorization = BoynerConfigurationV2.Authorization,
                     Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"
            });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.BatchRequestId;

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = httpResultResponse.ContentError.Errors.Select(e => e.Message).ToList();

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);


            return new HttpHelperV3Response<TrackableResponse>();
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);


            var priceStockRequestV2 = new BoynerUpdatePriceRequestV2
            {
                Items = request
            .Items
            .Select(dpi => new BoynerUpdatePriceItem
            {
                Barcode = dpi.Barcode,
                Quantity = dpi.Stock,
                ListPrice = dpi.ListUnitPrice,
                SalePrice = dpi.UnitPrice
            })
            .ToList()
            };


            var requestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{BoynerConfigurationV2.SupplierId}/products/price-and-inventory";

            var httpResultResponse = await this._httpHelperV3.SendAsync<BoynerUpdatePriceRequestV2, BoynerUpdatePriceResponseV2>(
                        new HttpHelperV3Request<BoynerUpdatePriceRequestV2>
                        {
                            RequestUri = requestUri,
                            Request = priceStockRequestV2,
                            HttpMethod = HttpMethod.Post,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = BoynerConfigurationV2.Authorization,
                            Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"
                        });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.BatchRequestId }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);


            var priceStockRequestV2 = new BoynerUpdatePriceRequestV2
            {
                Items = request
            .Items
            .Select(dpi => new BoynerUpdatePriceItem
            {
                Barcode = dpi.Barcode,
                Quantity = dpi.Stock,
                ListPrice = dpi.ListUnitPrice,
                SalePrice = dpi.UnitPrice
            })
            .ToList()
            };


            var requestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{BoynerConfigurationV2.SupplierId}/products/price-and-inventory";

            var httpResultResponse = await this._httpHelperV3.SendAsync<BoynerUpdatePriceRequestV2, BoynerUpdatePriceResponseV2>(
                        new HttpHelperV3Request<BoynerUpdatePriceRequestV2>
                        {
                            RequestUri = requestUri,
                            Request = priceStockRequestV2,
                            HttpMethod = HttpMethod.Post,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = BoynerConfigurationV2.Authorization,
                            Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"
                        });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.BatchRequestId }
                    : null
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Pazaryeri kategori bilgisi boş olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationName.Length > 100))
                    {
                        var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(pim => pim.ProductInformationName.Length > 100)
                        .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün ismi 100 karekterden fazla olmamalıdır."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationName.Length > 100);

                    }

                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Pazaryeri kategori bilgisi boş olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                if (theRequest.Items.Any(i => i.SellerCode.Length > 40))
                {
                    var invalidItems = theRequest.Items.Where(i => i.SellerCode.Length > 40).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = $"Model kodu 40 karakterden fazla olamaz. ({i.SellerCode})");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => i.SellerCode.Length > 40);
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationName.Length > 100))
                    {
                        var invalidProductInformationMarketplaces = theItem
                        .ProductInformationMarketplaces
                        .Where(pim => pim.ProductInformationName.Length > 100)
                        .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestUpdateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün ismi 100 karekterden fazla olmamalıdır."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationName.Length > 100);

                    }

                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestUpdateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceUpdateProductRequests.Add(new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);
            var supplierId = BoynerConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<BoynerBatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = BoynerConfigurationV2.Authorization,
                        Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"

                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };


            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status == "COMPLETED"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i
                        .ProductInformationMarketplaces
                        .Select(pim => new TrackingResponseItem
                        {
                            ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                            Barcode = pim.Barcode,
                            StockCode = pim.StockCode
                        }))
                    .ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status)
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);

            return new HttpHelperV3Response<TrackingResponse>();
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);

            var supplierId = BoynerConfigurationV2.SupplierId;
            var requestId = request.TrackingId;
            var requestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<BoynerBatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = BoynerConfigurationV2.Authorization,
                        Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"

                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse.Content.items.Any(i => i.status != true && i.status != false)
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status == true)
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;

        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            BoynerConfigurationV2 BoynerConfigurationV2 = Configuration(request);
            var supplierId = BoynerConfigurationV2.SupplierId;
            var requestId = request.TrackingId;

       
            var requestUri = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/products/batch-requests/{requestId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<BoynerBatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = BoynerConfigurationV2.Authorization,
                        Agent = $"{BoynerConfigurationV2.SupplierId} - Helpy"
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = !httpResultResponse.Content.items.Any(i => i.status != true && i.status != false)
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.RequestItem.Barcode);
                        if (i.status == true)
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons;
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;

        }
        #endregion

        #endregion

        #region Helper Methods
        private BoynerConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private BoynerConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations, bool decrypt = true)
        {
            if (decrypt)
                return marketplaceConfigurations.Decrypt();

            return marketplaceConfigurations;
        }
        #endregion
    }
}