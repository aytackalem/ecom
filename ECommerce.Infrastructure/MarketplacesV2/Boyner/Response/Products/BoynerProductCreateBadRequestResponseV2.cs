﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Products
{
 
    public class BoynerProductCreateBadRequestResponseV2
    {
        #region Properties
        [JsonProperty("exception")]
        public string Exception { get; set; }

        [JsonProperty("errors")]
        public List<BoynerProductCreateError> Errors { get; set; }
        #endregion
    }

    public class BoynerProductCreateError
    {
        #region Properties
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errorCode")]
        public object ErrorCode { get; set; }

        #endregion
    }
}
