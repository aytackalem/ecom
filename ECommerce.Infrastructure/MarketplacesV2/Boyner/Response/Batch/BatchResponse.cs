﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Batch
{
    public class BoynerBatchResponse
    {
        #region Properties
        public string batchRequestId { get; set; }

        public string status { get; set; }
        #endregion

        #region Navigation Properties
        public List<BoynerBatchItem> items { get; set; }
        #endregion
    }
}
