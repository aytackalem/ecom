﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Batch
{
    public class BoynerBatchItem
    {
        #region Properties
        public bool status { get; set; }

        public string Barcode { get; set; }

        public List<string> failureReasons { get; set; }
        #endregion

        #region Navigation Properties
        public BoynerBatchRequestItem RequestItem { get; set; }
        #endregion
    }
}
