﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Batch
{
    public class BoynerBatchRequestItem
    {
        #region Properties
        public string status { get; set; }

        public string Barcode { get; set; }

        public List<string> failureReasons { get; set; }
        #endregion

        #region Navigation Properties
        public BoynerBatchProduct Product { get; set; }
        #endregion
    }
}
