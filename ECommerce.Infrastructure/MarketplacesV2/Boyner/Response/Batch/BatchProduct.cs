﻿namespace ECommerce.Infrastructure.MarketplacesV2.Boyner.Response.Batch
{
    public class BoynerBatchProduct
    {
        #region Properties
        public string Barcode { get; set; }
        #endregion
    }
}
