﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Price;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Tracking;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Response.Price;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo.Response.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo
{
    public class MorhipoV2 : IMarketplaceV2, IMarketplaceV2CreateProductTrackable, IMarketplaceV2UpdateProductTrackable, IMarketplaceV2StockPriceTrackable, IMarketplaceV2CategoryGetable
    {
        #region Constants
        private const string _baseUrl = "https://mpapi.morhipo.com";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public MorhipoV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Morhipo;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var saveProductRequest = new SaveProductRequestV2
            {
                items = new List<ItemV2>()
            };
            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new ItemV2
                    {
                        barcode = pinLoop.Barcode,
                        brand = piLoop.BrandName,
                        cargoCompany = "YK",
                        stockCode = pinLoop.StockCode,
                        title = pinLoop.ProductInformationName,
                        productMainId = piLoop.SellerCode,
                        categoryName = piLoop.CategoryName,
                        currencyType = "TRY",
                        description = pinLoop.Description,
                        listPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        salePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        quantity = pinLoop.Stock,
                        images = pinLoop
                                  .ProductInformationPhotos
                                  .Select(i => new ImageV2 { url = i.Url })
                                  .ToList(),
                        vatRate = (pinLoop.VatRate * 100).ToString(),
                        variantAttributes = pinLoop
                                  .ProductInformationMarketplaceVariantValues
                                  .Select(a => new VariantAttributeV2
                                  {
                                      attributeName = a.MarketplaceVarinatName,
                                      attributeValue = a.MarketplaceVariantValueName
                                  })
                                  .ToList()
                    };

                    saveProductRequest.items.Add(item);
                }
            }

            var requestUri = $"{_baseUrl}/mpstore/listing/SaveProduct";
            var httpResultResponse = await this._httpHelperV3.SendAsync<SaveProductRequestV2, PriceResponseV2>(
                new HttpHelperV3Request<SaveProductRequestV2>
                {
                    RequestUri = requestUri,
                    Request = saveProductRequest,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.trackingId
                    }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var saveProductRequest = new SaveProductRequestV2
            {
                items = new System.Collections.Generic.List<ItemV2>()
            };
            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var item = new ItemV2
                    {
                        barcode = pinLoop.Barcode,
                        brand = piLoop.BrandName,
                        cargoCompany = "YK",
                        stockCode = pinLoop.StockCode,
                        title = pinLoop.ProductInformationName,
                        productMainId = piLoop.SellerCode,
                        categoryName = piLoop.CategoryName,
                        currencyType = "TRY",
                        description = pinLoop.Description,
                        listPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        salePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        quantity = pinLoop.Stock,
                        images = pinLoop
                                 .ProductInformationPhotos
                                 .Select(i => new ImageV2 { url = i.Url })
                                 .ToList(),
                        vatRate = (pinLoop.VatRate * 100).ToString(),
                        variantAttributes = pinLoop
                                 .ProductInformationMarketplaceVariantValues
                                 .Select(a => new VariantAttributeV2
                                 {
                                     attributeName = a.MarketplaceVarinatName,
                                     attributeValue = a.MarketplaceVariantValueName
                                 })
                                 .ToList()
                    };

                    saveProductRequest.items.Add(item);
                }
            }

            var requestUri = $"{_baseUrl}/mpstore/listing/SaveProduct";
            var httpResultResponse = await this._httpHelperV3.SendAsync<SaveProductRequestV2, PriceResponseV2>(
                new HttpHelperV3Request<SaveProductRequestV2>
                {
                    RequestUri = requestUri,
                    Request = saveProductRequest,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new TrackableResponse
                {
                    TrackingId = httpResultResponse.Content.trackingId
                }
            };

            //response.Success = httpResultResponse.Success;
            //response.ContentString = httpResultResponse.ContentString;
            //response.HttpStatusCode = httpResultResponse.HttpStatusCode;
            //response.Exception = httpResultResponse.Exception;
            //if (response.Success && response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            //    response.ContentObject = new ProcessResponse
            //    {
            //        TrackingId = httpResultResponse.ContentObject.trackingId
            //    };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var priceRequestV2 = new PriceRequestV2
            {
                products = request
                    .Items
                    .Select(dpi => new PriceRequestItemV2
                    {
                        b = dpi.Barcode,
                        lp = (double)dpi.ListUnitPrice,
                        sp = (double)dpi.UnitPrice
                    })
                    .ToList()
            };

            priceRequestV2.products = priceRequestV2.products.Distinct().ToList();

            var requestUri = $"{_baseUrl}/mpstore/listing/SavePriceByBarcode";
            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceRequestV2, PriceResponseV2>(
                new HttpHelperV3Request<PriceRequestV2>
                {
                    RequestUri = requestUri,
                    Request = priceRequestV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.trackingId
                    }
                    : null
            };

            //response.Success = httpResultResponse.Success;
            //if (httpResultResponse.Success && httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
            //    response.ContentObject = new()
            //    {
            //        TrackingId = httpResultResponse.ContentObject.trackingId
            //    };
            //response.ContentString = httpResultResponse.ContentString;
            //response.HttpStatusCode = httpResultResponse.HttpStatusCode;
            //response.Exception = httpResultResponse.Exception;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var stockRequestV2 = new StockRequestV2
            {
                products = request
                    .Items
                    .Select(dpi => new StockRequestItemV2
                    {
                        b = dpi.Barcode,
                        s = dpi.Stock
                    })
                    .ToList()
            };
            var requestUri = $"{_baseUrl}/mpstore/listing/SaveStockByBarcode";
            var httpResultResponse = await this._httpHelperV3.SendAsync<StockRequestV2, StockResponseV2>(
                new HttpHelperV3Request<StockRequestV2>
                {
                    RequestUri = requestUri,
                    Request = stockRequestV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.trackingId
                    }
                    : null
            };

            //response.Success = httpResultResponse.Success;
            //if (httpResultResponse.Success && httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
            //    response.ContentObject = new()
            //    {
            //        TrackingId = httpResultResponse.ContentObject.trackingId
            //    };
            //response.ContentString = httpResultResponse.ContentString;
            //response.HttpStatusCode = httpResultResponse.HttpStatusCode;
            //response.Exception = httpResultResponse.Exception;
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 20M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 20).Take(20).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 20M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceUpdateProductRequests.Add(new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 20).Take(20).ToList()
                        });
                    }
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var requestUri = $"{_baseUrl}/tracking/Status";
            var httpResultResponse = await this._httpHelperV3.SendAsync<TrackingRequestV2, Response.Batch.BatchResponse>(
                new HttpHelperV3Request<TrackingRequestV2>
                {
                    RequestUri = requestUri,
                    Request = new TrackingRequestV2
                    {
                        TrackingId = request.TrackingId
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = true
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode,
                        Success = true
                    }));

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems.ToList();
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var requestUri = $"{_baseUrl}/tracking/Status";
            var httpResultResponse = await this._httpHelperV3.SendAsync<TrackingRequestV2, Response.Batch.BatchResponse>(
                new HttpHelperV3Request<TrackingRequestV2>
                {
                    RequestUri = requestUri,
                    Request = new TrackingRequestV2
                    {
                        TrackingId = request.TrackingId
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = true
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode,
                        Success = true
                    }));

                httpHelperV3Response.Content.TrackingResponseItems=trackingResponseItems.ToList();
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var requestUri = $"{_baseUrl}/tracking/Status";
            var httpResultResponse = await this._httpHelperV3.SendAsync<TrackingRequestV2, Response.Batch.BatchResponse>(
                new HttpHelperV3Request<TrackingRequestV2>
                {
                    RequestUri = requestUri,
                    Request = new TrackingRequestV2
                    {
                        TrackingId = request.TrackingId
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = true
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Success = true,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice
                    })
                    .ToList();

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            MorhipoConfigurationV2 morhipoConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var requestUri = $"{_baseUrl}/tracking/Status";
            var httpResultResponse = await this._httpHelperV3.SendAsync<TrackingRequestV2, Response.Batch.BatchResponse>(
                new HttpHelperV3Request<TrackingRequestV2>
                {
                    RequestUri = requestUri,
                    Request = new TrackingRequestV2
                    {
                        TrackingId = request.TrackingId
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = morhipoConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = true
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Success = true,
                        Stock = i.Stock
                    })
                    .ToList();

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            throw new Exception();
        }
        #endregion
    }
}