﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Response.Stock
{
    public class StockResponseV2
    {
        #region Properties
        public string integrator { get; set; }

        public string trackingId { get; set; }

        public string code { get; set; }

        public string internalMessage { get; set; }

        public string message { get; set; }
        #endregion
    }
}
