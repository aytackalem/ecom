﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Response.Batch
{
    public class BatchResponse
    {
        public string[] data { get; set; }
        public int total { get; set; }
        public int code { get; set; }
        public string internalMessage { get; set; }
        public string message { get; set; }
    }
}
