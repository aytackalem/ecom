﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Response.ProductCreate
{
    public class ProductCreateResponseV2
    {
        #region Properties
        public string integrator { get; set; }

        public string trackingId { get; set; }

        public string code { get; set; }

        public string internalMessage { get; set; }

        public string message { get; set; }
        #endregion
    }
}
