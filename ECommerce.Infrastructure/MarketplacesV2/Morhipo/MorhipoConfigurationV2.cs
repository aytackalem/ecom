﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo
{
    public class MorhipoConfigurationV2
    {
        #region Members
        public static implicit operator MorhipoConfigurationV2(Dictionary<string, string> configurations)
        {
            return new MorhipoConfigurationV2
            {
                Username = configurations["Username"],
                Password = configurations["Password"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));
        #endregion
    }
}
