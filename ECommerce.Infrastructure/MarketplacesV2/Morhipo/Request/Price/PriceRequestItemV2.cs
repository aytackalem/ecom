﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Price
{
    public class PriceRequestItemV2
    {
        #region Properties
        public double lp { get; set; }
        
        public double sp { get; set; }
        
        public string b { get; set; }
        #endregion
    }
}
