﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Price
{
    public class PriceRequestV2
    {
        #region Properties
        public List<PriceRequestItemV2> products { get; set; }
        #endregion
    }
}
