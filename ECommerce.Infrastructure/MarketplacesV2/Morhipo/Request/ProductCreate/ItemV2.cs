using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.ProductCreate
{
    public class ItemV2
    {
        #region Properties
        public string productMainId { get; set; }
        
        public string stockCode { get; set; }
        
        public string title { get; set; }
        
        public string barcode { get; set; }
        
        public string brand { get; set; }
        
        public string cargoCompany { get; set; }
        
        public string categoryName { get; set; }
        
        public string currencyType { get; set; }
        
        public string description { get; set; }
        
        public string vatRate { get; set; }

        public int quantity { get; set; }

        public double listPrice { get; set; }

        public double salePrice { get; set; }
        #endregion

        #region Navigation Properties
        public List<ImageV2> images { get; set; }
        
        public List<VariantAttributeV2> variantAttributes { get; set; }
        #endregion
    }
}