namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.ProductCreate
{
    public class VariantAttributeV2
    {
        #region Properties
        public string attributeName { get; set; }
        
        public string attributeValue { get; set; } 
        #endregion
    }
}