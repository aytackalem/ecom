using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.ProductCreate
{
    public class SaveProductRequestV2
    {
        #region Navigation Properties
        public List<ItemV2> items { get; set; }
        #endregion
    }
}