﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Tracking
{
    public class TrackingRequestV2
    {
        #region Properties
        public string TrackingId { get; set; } 
        #endregion
    }
}
