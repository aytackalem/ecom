﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Stock
{
    public class StockRequestV2
    {
        #region Properties
        public List<StockRequestItemV2> products { get; set; }
        #endregion
    }
}
