﻿namespace ECommerce.Infrastructure.MarketplacesV2.Morhipo.Request.Stock
{
    public class StockRequestItemV2
    {
        #region Properties
        public string b { get; set; }
        
        public int s { get; set; }
        #endregion
    }
}
