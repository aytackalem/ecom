﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Stock
{
    public class StockRequestV2
    {
        #region Properties
        public string HepsiburadaSku { get; set; }
        
        public string MerchantSku { get; set; }
        
        public int AvailableStock { get; set; } 
        #endregion
    }
}
