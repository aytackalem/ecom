﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Token
{
    public class TokenRequest
    {
        #region Properties
        public string username { get; set; }

        public string password { get; set; }

        public string authenticationType => "INTEGRATOR";
        #endregion
    }
}
