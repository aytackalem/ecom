﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.ProductCreate
{
    public class ProductCreateRequestV2
    {
        #region Property
        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("merchant")]
        public string Merchant { get; set; }

        [JsonProperty("attributes")]
        public ProductAttributeV2 Attributes { get; set; }
        #endregion
    }
}
