﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Price
{
    public class PriceRequestV2
    {
        #region Properties
        public string HepsiburadaSku { get; set; }
        
        public string MerchantSku { get; set; }
        
        public double Price { get; set; } 
        #endregion
    }
}
