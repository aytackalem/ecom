﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Price;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Request.Token;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Batch;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Price;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Token;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada
{
    public class HepsiburadaV2 : 
        IMarketplaceV2, 
        IMarketplaceV2CreateProductTrackable, 
        IMarketplaceV2UpdateProductTrackable, 
        IMarketplaceV2StockPriceTrackable, 
        IMarketplaceV2CategoryGetable
    {
        #region Constants
        private const string _productUrl = "https://mpop.hepsiburada.com/product/api/products/import";

        private const string _listingUrl = "https://listing-external.hepsiburada.com";

        private const string _tokenUrl = "https://mpop.hepsiburada.com/api/authenticate";
        #endregion

        #region Fields
        public readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public HepsiburadaV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Hepsiburada;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var products = new List<ProductCreateRequestV2>();

            var data = string.Empty;

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var images = string.Empty;

                    for (int i = 0; i < (pinLoop.ProductInformationPhotos.Count >= 5 ? 5 : pinLoop.ProductInformationPhotos.Count); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                images += $"\"Image1\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 1:
                                images += $"\"Image2\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 2:
                                images += $"\"Image3\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 3:
                                images += $"\"Image4\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 4:
                                images += $"\"Image5\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                        }
                    }

                    var attirubutes = string.Empty;
                    foreach (var attLoop in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        var attributeName = attLoop.Varinater ? attLoop.MarketplaceVarinatCode : attLoop.MarketplaceVarinatCode.Replace(" ", "_");

                        attirubutes += $"\"{attributeName}\": \"{attLoop.MarketplaceVariantValueName}\",";
                    }
                    attirubutes = attirubutes.TrimEnd(',');

                    #region Description
                    var description = string.Empty;

                    if (string.IsNullOrEmpty(pinLoop.Description))
                        description = pinLoop.ProductInformationName;
                    else
                        description = pinLoop.Description;

                    description = description.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\"", "");

                    if (description.IndexOf("<p>") == -1)
                        description = $"<p>{description}</p>";
                    #endregion
                    if (string.IsNullOrEmpty(attirubutes))
                    {
                        images = images.TrimEnd(',');
                    }

                    data += $@"
        {{
            ""categoryId"": {int.Parse(piLoop.CategoryCode)},
            ""merchant"": ""{hepsiburadaConfigurationV2.MerchantId}"",
            ""attributes"": {{
              ""merchantSku"": ""{pinLoop.StockCode}"",
              ""VaryantGroupID"": ""{piLoop.SellerCode}"",
              ""Barcode"": ""{pinLoop.Barcode}"",
              ""UrunAdi"": ""{pinLoop.ProductInformationName}"",
              ""UrunAciklamasi"": ""{description}"",
              ""Marka"": ""{piLoop.BrandName}"",
              ""GarantiSuresi"": 0,
              ""kg"": ""{piLoop.DimensionalWeight.ToString().Replace(",", ".")}"",
              ""tax_vat_rate"": ""{(pinLoop.VatRate * 100).ToString("0")}"",
              ""price"": ""{pinLoop.UnitPrice.ToString().Replace(".", ",")}"",
              ""stock"": ""{pinLoop.Stock}"",
              {images}
              {attirubutes}
            }}
                    }},";
                }
            }

            data = data.TrimEnd(',');
            data =
            $@"[{data}]";

            //var token = await this.GetToken(hepsiburadaConfigurationV2);

            var httpResultResponse = await this._httpHelperV3.SendAsync<ProductCreateResponseV2>(
                new HttpHelperV3JsonFileRequest
                {
                    RequestUri = _productUrl,
                    Request = data,
                    FileName = "product.json",
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = hepsiburadaConfigurationV2.Authorization,
                    Accept = "application/json",
                    Agent = "helpy_dev"
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
            {
                if (httpResultResponse.Content.success == false)
                {
                    httpHelperV3Response.HttpStatusCode = HttpStatusCode.BadRequest;
                    httpHelperV3Response.Content.Messages = new List<string>
                    {
                        httpResultResponse.Content.message
                    };
                }
                else
                    httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.data.trackingId;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var products = new List<ProductCreateRequestV2>();

            var data = string.Empty;

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    var images = string.Empty;

                    for (int i = 0; i < (pinLoop.ProductInformationPhotos.Count >= 5 ? 5 : pinLoop.ProductInformationPhotos.Count); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                images += $"\"Image1\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 1:
                                images += $"\"Image2\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 2:
                                images += $"\"Image3\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 3:
                                images += $"\"Image4\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                            case 4:
                                images += $"\"Image5\": \"{pinLoop.ProductInformationPhotos[i].Url}\",";
                                break;
                        }
                    }

                    var attirubutes = string.Empty;
                    foreach (var attLoop in pinLoop.ProductInformationMarketplaceVariantValues)
                    {
                        var attributeName = attLoop.Varinater ? attLoop.MarketplaceVarinatCode : attLoop.MarketplaceVarinatCode.Replace(" ", "_");

                        attirubutes += $"\"{attributeName}\": \"{attLoop.MarketplaceVariantValueName}\",";
                    }
                    attirubutes = attirubutes.TrimEnd(',');

                    data += $@"
        {{
            ""categoryId"": {int.Parse(piLoop.CategoryCode)},
            ""merchant"": ""{hepsiburadaConfigurationV2.MerchantId}"",
            ""attributes"": {{
              ""merchantSku"": ""{pinLoop.StockCode}"",
              ""VaryantGroupID"": ""{piLoop.SellerCode}"",
              ""Barcode"": ""{pinLoop.Barcode}"",
              ""UrunAdi"": ""{pinLoop.ProductInformationName}"",
              ""UrunAciklamasi"": ""{pinLoop.Description}"",
              ""Marka"": ""{piLoop.BrandName}"",
              ""GarantiSuresi"": 0,
              ""kg"": ""{piLoop.DimensionalWeight.ToString().Replace(",", ".")}"",
              ""tax_vat_rate"": ""{(pinLoop.VatRate * 100).ToString("0")}"",
              ""price"": ""{pinLoop.UnitPrice.ToString().Replace(".", ",")}"",
              ""stock"": ""{pinLoop.Stock}"",
              {images}
              {attirubutes}
            }}
                    }},";
                }
            }

            data = data.TrimEnd(',');
            data =
            $@"[{data}]";

            var httpResultResponse = await this._httpHelperV3.SendAsync<ProductCreateResponseV2>(
                new HttpHelperV3JsonFileRequest
                {
                    RequestUri = _productUrl,
                    Request = data,
                    FileName = "product.json",
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = await this.GetToken(hepsiburadaConfigurationV2),
                    Agent = "helpy_dev"
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new()
                    {
                        TrackingId = httpResultResponse.Content.data.trackingId
                    }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");
            var priceRequestV2s = request
                        .Items
                        .Select(dpi => new PriceRequestV2
                        {
                            HepsiburadaSku = null,
                            MerchantSku = dpi.StockCode,
                            Price = Convert.ToDouble(dpi.UnitPrice.ToString().Replace(",", "."), culture)
                        })
                        .ToList();
            var requestUri = $"{_listingUrl}/listings/merchantid/{hepsiburadaConfigurationV2.MerchantId}/price-uploads";
            var httpResultResponse = await this._httpHelperV3.SendAsync<List<PriceRequestV2>, PriceResponseV2>(
                new HttpHelperV3Request<List<PriceRequestV2>>
                {
                    RequestUri = requestUri,
                    Request = priceRequestV2s,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = hepsiburadaConfigurationV2.Authorization,
                    Agent = "helpy_dev"
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.id }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var culture = new CultureInfo("en-US");
            var stockRequestV2s = request
                .Items
                .Select(dpi => new StockRequestV2
                {
                    MerchantSku = dpi.StockCode,
                    AvailableStock = dpi.Stock
                })
                .ToList();
            var requestUri = $"{_listingUrl}/listings/merchantid/{hepsiburadaConfigurationV2.MerchantId}/stock-uploads";
            var httpResultResponse = await this._httpHelperV3.SendAsync<List<StockRequestV2>, StockResponseV2>(
                new HttpHelperV3Request<List<StockRequestV2>>
                {
                    RequestUri = requestUri,
                    Request = stockRequestV2s,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = hepsiburadaConfigurationV2.Authorization,
                    Agent = "helpy_dev"
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                ? new TrackableResponse { TrackingId = httpResultResponse.Content.id }
                : null
            };
        }
        #endregion

        #region Validate
        #region Product
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {

        }
        #endregion

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceUpdateProductRequests.Add(new MarketplaceRequestUpdateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 5000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 5000).Take(5000).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 5000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 5000).Take(5000).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var page = 0;
            var size = 100;
            var totalPages = 0;
            var trackingId = request.TrackingId;
            var datums = new List<Datum>();
            var hasError = false;

            do
            {
                var requestUri = $"https://mpop.hepsiburada.com/product/api/products/status/{trackingId}?page={page}&size={size}";
                var httpResultResponse = await this._httpHelperV3.SendAsync<BatchProductResponse>(
                       new HttpHelperV3Request
                       {
                           RequestUri = requestUri,
                           HttpMethod = HttpMethod.Get,
                           AuthorizationType = AuthorizationType.Basic,
                           Authorization = hepsiburadaConfigurationV2.Authorization,
                           Agent = "helpy_dev"
                       });

                if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    datums.AddRange(httpResultResponse.Content.data);

                    if (totalPages == 0)
                        totalPages = httpResultResponse.Content.totalPages;
                }
                else
                    hasError = true;

                page++;
            } while (totalPages > page);

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = hasError == false ? HttpStatusCode.OK : HttpStatusCode.BadRequest
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = datums.Any(d => d.importStatus != "SUCCESS" && d.importStatus != "FAILED") == false
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode
                    })).ToList();

                datums
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.barcode);
                        if (trackingResponseItem == null)
                            trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.merchantSku);

                        if (i.importStatus.ToLower() == "success")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.importMessages.Select(im => im.message).ToList();
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var page = 0;
            var size = 100;
            var totalPages = 0;
            var trackingId = request.TrackingId;
            var datums = new List<Datum>();
            var hasError = false;

            do
            {
                var requestUri = $"https://mpop.hepsiburada.com/product/api/products/status/{trackingId}?page={page}&size={size}";
                var httpResultResponse = await this._httpHelperV3.SendAsync<BatchProductResponse>(
                       new HttpHelperV3Request
                       {
                           RequestUri = requestUri,
                           HttpMethod = HttpMethod.Get,
                           AuthorizationType = AuthorizationType.Basic,
                           Authorization = hepsiburadaConfigurationV2.Authorization,
                           Agent = "helpy_dev"
                       });

                if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    datums.AddRange(httpResultResponse.Content.data);

                    if (totalPages == 0)
                        totalPages = httpResultResponse.Content.totalPages;
                }
                else
                    hasError = true;

                page++;
            } while (totalPages > page);

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = hasError == false ? HttpStatusCode.OK : HttpStatusCode.BadRequest
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = datums.Any(d => d.importStatus != "SUCCESS" && d.importStatus != "FAILED") == false
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                        Barcode = pim.Barcode,
                        StockCode = pim.StockCode
                    })).ToList();

                datums
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.barcode);
                        if (i.importStatus.ToLower() == "success")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.importMessages.Select(im => im.message).ToList();
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var requestUri = $"{_listingUrl}/listings/merchantid/{hepsiburadaConfigurationV2.MerchantId}/price-uploads/id/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<BatchPriceStockResponse>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = hepsiburadaConfigurationV2.Authorization,
                    Agent = "helpy_dev"
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status.ToLower() == "done"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice,
                        Success = true
                    }).ToList();

                httpResultResponse
                    .Content
                    .errors
                    ?.ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.merchantSku);
                        trackingResponseItem.Success = false;
                        trackingResponseItem.Messages = i.errors;
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            var requestUri = $"{_listingUrl}/listings/merchantid/{hepsiburadaConfigurationV2.MerchantId}/stock-uploads/id/{request.TrackingId}";
            var httpResultResponse = await this._httpHelperV3.SendAsync<BatchPriceStockResponse>(
                   new HttpHelperV3Request
                   {
                       RequestUri = requestUri,
                       HttpMethod = HttpMethod.Get,
                       AuthorizationType = AuthorizationType.Basic,
                       Authorization = hepsiburadaConfigurationV2.Authorization,
                       Agent = "helpy_dev"
                   });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status.ToLower() == "done"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request.Items.Select(i => new TrackingResponseItem
                {
                    ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                    Barcode = i.Barcode,
                    StockCode = i.StockCode,
                    Stock = i.Stock,
                    Success = true
                }).ToList();

                httpResultResponse
                    .Content
                    .errors
                    ?.ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.StockCode == i.merchantSku);
                        trackingResponseItem.Success = false;
                        trackingResponseItem.Messages = i.errors;
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            HepsiburadaConfigurationV2 hepsiburadaConfigurationV2 = Configuration(request);

            String token = await this.GetToken(hepsiburadaConfigurationV2);

            List<Category> categories = new();

            Int32 page = 0;
            List<HepsiburadaCategory> hepsiburadaCategories = new();
            HepsiburadaCategoryPage categoryPage;
            do
            {
                var httpHelperV3Response = await this._httpHelperV3.SendAsync<HepsiburadaCategoryPage>(
                    new HttpHelperV3Request
                    {
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token,
                        RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/get-all-categories?leaf=true&status=ACTIVE&available=true&page={page}&size=7500&version=1",
                        Agent = "helpy_dev"
                    });

                categoryPage = httpHelperV3Response.Content;

                hepsiburadaCategories.AddRange(categoryPage.Data);

                page++;
            } while (!categoryPage.Last);

            categories.AddRange(hepsiburadaCategories.Select(hc => new Category
            {
                Code = hc.CategoryId.ToString(),
                Name = hc.Name,
                ParentCategoryCode = hc.ParentCategoryId.HasValue ? hc.ParentCategoryId.ToString() : null
            }));

            categories.ForEach(async theCategory =>
            {
                var attributePageResponse = await this._httpHelperV3.SendAsync<HepsiburadaCategoryAttributePage>(new HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token,
                    RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/{theCategory.Code}/attributes"
                });

                theCategory.CategoryAttributes.AddRange(attributePageResponse.Content.Data.Attributes.Where(a => a.Type != "string").Select(a => new CategoryAttribute
                {
                    Code = a.Id.ToString(),
                    Name = a.Name,
                    CategoryCode = theCategory.Code,
                    Mandatory = a.Mandatory,
                    MultiValue = a.MultiValue
                }));

                theCategory.CategoryAttributes.AddRange(attributePageResponse.Content.Data.VariantAttributes.Where(va => va.Type != "string").Select(va => new CategoryAttribute
                {
                    Code = va.Id.ToString(),
                    Name = va.Name,
                    CategoryCode = theCategory.Code,
                    Mandatory = va.Mandatory,
                    MultiValue = va.MultiValue,
                    Variantable = true
                }));

                theCategory.CategoryAttributes.ForEach(async theCategoryAttribute =>
                {
                    Int32 attributeValuePage = 0;
                    HepsiburadaCategoryAttributeValuePage categoryAttributeValuePage;
                    do
                    {
                        var categoryAttributeValuePageResponse = await this
                            ._httpHelperV3
                            .SendAsync<HepsiburadaCategoryAttributeValuePage>(
                                new HttpHelperV3Request
                                {
                                    HttpMethod = HttpMethod.Get,
                                    AuthorizationType = AuthorizationType.Bearer,
                                    Authorization = token,
                                    RequestUri = $"https://mpop.hepsiburada.com/product/api/categories/{theCategory.Code}/attribute/{theCategoryAttribute.Code}/values?page={attributeValuePage}&size=1000&version=4"
                                });

                        categoryAttributeValuePage = categoryAttributeValuePageResponse.Content;

                        theCategoryAttribute.CategoryAttributesValues.AddRange(categoryAttributeValuePage.Data.Select(d => new CategoryAttributeValue
                        {
                            Code = d.Id.ToString(),
                            Name = d.Value,
                            VarinatCode = theCategoryAttribute.Code
                        }));

                    } while (!categoryAttributeValuePage.Last && categoryAttributeValuePage.Success);
                });
            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private HepsiburadaConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private HepsiburadaConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }

        private async Task<string> GetToken(HepsiburadaConfigurationV2 hepsiburadaConfigurationV2)
        {
            var tokenResponse = await this._httpHelperV3.SendAsync<TokenRequest, TokenResponse>(
                new HttpHelperV3Request<TokenRequest>
                {
                    RequestUri = _tokenUrl,
                    Request = new TokenRequest
                    {
                        username = hepsiburadaConfigurationV2.Username,
                        password = hepsiburadaConfigurationV2.Password
                    },
                    HttpMethod = HttpMethod.Post
                });
            return tokenResponse.Content.id_token;
        }
        #endregion
    }
}