﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada
{
    public class HepsiburadaConfigurationV2
    {
        #region Members
        public static implicit operator HepsiburadaConfigurationV2(Dictionary<string, string> configurations)
        {
            return new HepsiburadaConfigurationV2
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                MerchantId = configurations["MerchantId"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));

        public string MerchantId { get; set; }
        #endregion
    }
}
