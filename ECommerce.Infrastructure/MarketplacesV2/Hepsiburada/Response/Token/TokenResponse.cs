﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Token
{
    public class TokenResponse
    {
        #region Properties
        public string id_token { get; set; }
        #endregion
    }
}
