﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Batch
{
    public class BatchPriceStockItem
    {
        #region Properties
        public int elementNo { get; set; }
        
        public string hepsiburadaSku { get; set; }
        
        public string merchantSku { get; set; }
        
        public List<string> errors { get; set; }
        #endregion
    }
}
