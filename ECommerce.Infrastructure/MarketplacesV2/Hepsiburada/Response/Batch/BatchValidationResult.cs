﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Batch
{
    public class BatchValidationResult
    {
        public string attributeName { get; set; }
        public string message { get; set; }
    }
}
