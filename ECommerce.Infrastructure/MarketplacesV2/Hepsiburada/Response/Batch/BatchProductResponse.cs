﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Batch
{
    public class BatchProductResponse
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public Datum[] data { get; set; }
    }

    public class Datum
    {
        public int itemOrderID { get; set; }
        public string merchant { get; set; }
        public string merchantSku { get; set; }
        public string hbSku { get; set; }
        public string barcode { get; set; }
        public string productStatus { get; set; }
        public string productName { get; set; }
        public string variantGroupId { get; set; }
        public object[] taskDetails { get; set; }
        public object[] validationResults { get; set; }
        public object rejectReasonsMessages { get; set; }
        public string importStatus { get; set; }
        public Importmessage[] importMessages { get; set; }
        public object matchedHbProductInfo { get; set; }
    }

    public class Importmessage
    {
        public string severity { get; set; }
        public string message { get; set; }
    }

}
