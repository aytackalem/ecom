﻿using System.Collections.Generic;
using System;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Batch
{
    public class BatchPriceStockResponse
    {
        #region Properties
        public string id { get; set; }
        
        public string status { get; set; }
        
        public DateTime createdAt { get; set; }
        
        public int total { get; set; }
        #endregion

        #region Navigation Properties
        public List<BatchPriceStockItem> errors { get; set; }
        #endregion
    }
}
