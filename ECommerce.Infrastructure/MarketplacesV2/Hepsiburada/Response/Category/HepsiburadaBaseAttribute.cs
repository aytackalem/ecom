﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category
{
    public class HepsiburadaBaseAttribute
    {
        #region Property
        public string Name { get; set; }

        public string Id { get; set; }

        public bool Mandatory { get; set; }

        public string Type { get; set; }

        public bool MultiValue { get; set; }
        #endregion
    }
}
