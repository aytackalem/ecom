﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category
{
    public class HepsiburadaAttribute
    {
        public HepsiburadaAttribute()
        {
            CategoryAttributeValues = new List<HepsiburadaCategoryAttributeValue>();
        }
        #region Property
        public string Name { get; set; }

        public string Id { get; set; }

        public bool Mandatory { get; set; }

        public bool Varinatable { get; set; }

        public string Type { get; set; }

        public bool MultiValue { get; set; }
        #endregion

        public List<HepsiburadaCategoryAttributeValue> CategoryAttributeValues { get; set; }

    }
}
