﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category
{
    public class HepsiburadaCategoryAttributeValue
    {
        #region Property
        public string Id { get; set; }

        public string Value { get; set; }
        #endregion
    }
}
