﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category
{
    public class HepsiburadaCategoryProductType
    {
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
    }
}
