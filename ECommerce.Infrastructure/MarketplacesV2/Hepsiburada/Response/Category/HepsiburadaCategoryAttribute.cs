﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.Category
{
    public class HepsiburadaCategoryAttribute
    {
        #region Properties
        public List<HepsiburadaBaseAttribute> BaseAttributes { get; set; }

        public List<HepsiburadaAttribute> Attributes { get; set; }

        public List<HepsiburadaVariantAttribute> VariantAttributes { get; set; }
        #endregion
    }
}
