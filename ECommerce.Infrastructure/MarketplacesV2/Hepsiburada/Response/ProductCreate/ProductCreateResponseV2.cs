﻿namespace ECommerce.Infrastructure.MarketplacesV2.Hepsiburada.Response.ProductCreate
{
    public class ProductCreateResponseV2
    {
        #region Property
        public bool success { get; set; }

        public int code { get; set; }

        public int version { get; set; }

        public string message { get; set; }

        public ProductCreateResponseDataV2 data { get; set; }
        #endregion
    }

    public class ProductCreateResponseDataV2
    {
        #region Property
        public string trackingId { get; set; }
        #endregion
    }
}
