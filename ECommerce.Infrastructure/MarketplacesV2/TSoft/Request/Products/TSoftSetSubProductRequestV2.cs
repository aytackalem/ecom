﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.Products
{
    public class TSoftSetSubProductRequestV2
    {
        public string MainProductCode { get; set; }
        public string SubProductCode { get; set; }
        public string Barcode { get; set; }
        public string SupplierSubProductCode { get; set; }
        public string Property1GroupId { get; set; }
        public string Property2GroupId { get; set; }
        public string Property1 { get; set; }
        public string Property2 { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string DiscountedSellingPrice { get; set; }
        public string VendorSellingPrice { get; set; }
        public string Stock { get; set; }
        public string CBM { get; set; }
        public string Weight { get; set; }
        public string IsActive { get; set; }
        public int Point { get; set; }
        public int IsDefaultSubProduct { get; set; }
        public List<string> AdditionalInfo { get; set; }
    }
}
