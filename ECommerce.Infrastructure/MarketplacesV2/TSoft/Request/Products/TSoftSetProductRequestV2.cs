﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.Products
{


    public class TSoftLabel1
    {
        public int Value { get; set; }
    }

    public class TSoftLabel2
    {
        public int Value { get; set; }
    }

    public class TSoftSetProductRequestV2
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultCategoryCode { get; set; }
        public string SupplierProductCode { get; set; }
        public string Barcode { get; set; }
        public string Stock { get; set; }
        public string StockUnit { get; set; }
        public string IsActive { get; set; }
        public string ComparisonSites { get; set; }
        public string Vat { get; set; }
        public string Currency { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string SearchKeywords { get; set; }
        public string DisplayOnHomepage { get; set; }
        public string IsNewProduct { get; set; }
        public string OnSale { get; set; }
        public string ImageUrl { get; set; }
        public string IsDisplayProduct { get; set; }
        public string VendorDisplayOnly { get; set; }
        public string DisplayWithVat { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string ModelActivity { get; set; }
        public bool HasSubProducts { get; set; }
        public string Supplier { get; set; }
        public string CustomerGroupDisplay { get; set; }
        public string Additional1 { get; set; }
        public string Additional2 { get; set; }
        public string Additional3 { get; set; }
        public string RelatedProductsBlock1 { get; set; }
        public string RelatedProductsBlock2 { get; set; }
        public string RelatedProductsBlock3 { get; set; }
        public string Magnifier { get; set; }
        public string MemberMinOrder { get; set; }
        public string MemberMaxOrder { get; set; }
        public string VendorMinOrder { get; set; }
        public string VendorMaxOrder { get; set; }
        public string FreeDeliveryMember { get; set; }
        public string FreeDeliveryVendor { get; set; }
        public string FreeDeliveryForProduct { get; set; }
        public string ShortDescription { get; set; }
        public string Details { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Depth { get; set; }
        public string Weight { get; set; }
        public string CBM { get; set; }
        public string Document { get; set; }
        public string Warehouse { get; set; }
        public string WarrantyInfo { get; set; }
        public string DeliveryInfo { get; set; }
        public string DeliveryTime { get; set; }
        public string ProductNote { get; set; }
        public string SeoSettingsId { get; set; }
        public string SeoTitle { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoDescription { get; set; }
        public string Gtip { get; set; }
        public int Gender { get; set; }
        public int IsCatalog { get; set; }
        public int OpportunityProduct { get; set; }
        public string ListNo { get; set; }
        public int PersonalizationId { get; set; }
        public string DisablePaymentTypes { get; set; }
        public string DisableCargoCompany { get; set; }
        public TSoftLabel1 Label1 { get; set; }
        public TSoftLabel2 Label2 { get; set; }
        public int Label3 { get; set; }
        public int Label4 { get; set; }
        public int Label5 { get; set; }
        public int Label6 { get; set; }
        public int Label7 { get; set; }
        public int Label8 { get; set; }
        public int Label9 { get; set; }
        public int Label10 { get; set; }
        public int HomepageSort { get; set; }
        public int MostSoldSort { get; set; }
        public int NewestSort { get; set; }
        public string AgeGroup { get; set; }
        public int Point { get; set; }
        public string Numeric1 { get; set; }
        public int DefaultSubProductId { get; set; }
        public string DefaultSubProductCode { get; set; }
        public int EftRate { get; set; }
        public string Tags { get; set; }
        public int SearchRank { get; set; }
        public string CountryOfOrigin { get; set; }
        public string ProformaTitle { get; set; }
    }


}
