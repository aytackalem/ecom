﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.Products
{
    public class TSoftProductAddImageFromLinkRequestV2
    {
        public string ProductCode { get; set; }
        public string ImageUrl { get; set; }
        public string ListNumber { get; set; }
    }
}
