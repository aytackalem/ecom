﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.PriceStocks
{
    public class TsoftUpdatePriceRequestV2
    {
        public string ProductCode { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string DiscountedSellingPrice { get; set; }
        public string Currency { get; set; }
        public string Vat { get; set; }
    }
}
