﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.PriceStocks
{
    public class TSoftUpdateStockAndPriceRequestV2
    {
        public string SubProductCode { get; set; }
        public string Stock { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string DiscountedSellingPrice { get; set; }
        public string VendorSellingPrice { get; set; }
    }
}
