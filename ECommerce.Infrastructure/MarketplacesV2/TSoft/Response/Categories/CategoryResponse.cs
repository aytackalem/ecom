﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Categories
{
    public class TSoftCategoryResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<TSoftCategoryItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

    public class TSoftCategoryItem
    {
        public string CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string Type { get; set; }
        public string IsActive { get; set; }
        public string ParentCode { get; set; }
        public string HasChild { get; set; }
        public string ListNo { get; set; }
        public string SeoLink { get; set; }
        public string SeoTitle { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoDescription { get; set; }
        public string SeoSettingsId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateDateTimeStamp { get; set; }
        public string FilteringPanel { get; set; }
        public string IsProductListPassive { get; set; }
        public string ShowInMenu { get; set; }
        public string ShowInMobile { get; set; }
        public string ShortDescription { get; set; }    
        public string ShowCategory { get; set; }
        public string ShowProduct { get; set; }
        public string Additional1 { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string GoogleProductsPath { get; set; }
    }
}
