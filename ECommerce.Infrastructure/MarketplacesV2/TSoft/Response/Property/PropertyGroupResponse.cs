﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Property
{
    public class PropertyGroupItem
    {
        public string GroupId { get; set; }
        public string Name { get; set; }
        public string PropertyCode { get; set; }
        public string GroupName { get; set; }
    }


    public class PropertyGroupResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<PropertyGroupItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

}
