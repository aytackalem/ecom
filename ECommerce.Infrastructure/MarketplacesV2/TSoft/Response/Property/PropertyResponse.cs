﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Property
{
    public class PropertyItem
    {
        public string PropertyId { get; set; }
        public string GroupId { get; set; }
        public string Property { get; set; }
        public string ColorCode { get; set; }
    }


    public class PropertyResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<PropertyItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

}
