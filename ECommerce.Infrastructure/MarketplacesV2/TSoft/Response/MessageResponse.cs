﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Response
{
    public class TSoftMessageResponse
    {
        public int type { get; set; }
        public string code { get; set; }
        public int index { get; set; }
        public string id { get; set; }
        public string subid { get; set; }
        public List<string> text { get; set; }
        public List<object> errorField { get; set; }
    }
}
