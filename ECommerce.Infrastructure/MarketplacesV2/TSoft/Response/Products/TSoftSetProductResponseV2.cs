﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Products
{


    public class TSoftSetProductResponseV2
    {
        public bool success { get; set; }
        public List<TSoftMessageResponse> message { get; set; }
        public string summary { get; set; }
    }
}
