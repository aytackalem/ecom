﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.PriceStocks;
using ECommerce.Infrastructure.MarketplacesV2.TSoft.Request.Products;
using ECommerce.Infrastructure.MarketplacesV2.TSoft.Response;
using ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Categories;
using ECommerce.Infrastructure.MarketplacesV2.TSoft.Response.Products;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft
{
    public class TSoftV2 : IMarketplaceV2, IMarketplaceV2CreateProductUntrackable, IMarketplaceV2StockPriceUntrackable, IMarketplaceV2CategoryGetable
    {
        #region Constants
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public TSoftV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.TSoft;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerStockRequest => 2;

        public int? WaitingSecondPerPriceRequest => 2;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<UntrackableCreateProductProcessResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            TSoftConfigurationV2 tSoftConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
            .Decrypt();



            var token = await this.GetToken(tSoftConfigurationV2);

            var setProductRequestV2 = new List<TSoftSetProductRequestV2>();
            var setSubProductRequestV2 = new List<TSoftSetSubProductRequestV2>();
            var addImageFromLinkRequestV2 = new List<TSoftProductAddImageFromLinkRequestV2>();

            var item = request.Items.First();

            setProductRequestV2.Add(new TSoftSetProductRequestV2
            {
                ProductCode = item.SellerCode,
                ProductName = item.ProductName,
                DefaultCategoryCode = item.CategoryCode,
                SupplierProductCode = item.SellerCode,
                Barcode = item.ProductInformationMarketplaces.FirstOrDefault().Barcode,
                Stock = item.ProductInformationMarketplaces.Sum(x => x.Stock).ToString(),
                StockUnit = "Adet",
                IsActive = "1",
                ComparisonSites = "1",
                Vat = (item.ProductInformationMarketplaces.FirstOrDefault().VatRate * 100).ToString(),
                Currency = "TL",
                BuyingPrice = "0",
                SellingPrice = (item.ProductInformationMarketplaces.FirstOrDefault().ListUnitPrice / (1 + item.ProductInformationMarketplaces.FirstOrDefault().VatRate)).ToString(),
                SearchKeywords = $"{item.ProductName},{item.SellerCode}",
                DisplayOnHomepage = "",
                IsNewProduct = item.ProductInformationMarketplaces.FirstOrDefault().Opened ? "" : "1",
                OnSale = "1",
                ImageUrl = string.Empty,
                IsDisplayProduct = "0",
                VendorDisplayOnly = "0",
                DisplayWithVat = "1",
                Brand = item.BrandName,// piLoop.BrandName,
                Model = string.Empty,
                ModelActivity = "0",
                HasSubProducts = true,
                Supplier = "",
                DefaultSubProductCode = item.SellerCode,
                CustomerGroupDisplay = "",
                Additional1 = string.Empty,
                Additional2 = string.Empty,
                Additional3 = string.Empty,
                RelatedProductsBlock1 = string.Empty,
                RelatedProductsBlock2 = string.Empty,
                RelatedProductsBlock3 = string.Empty,
                Magnifier = "1",
                MemberMinOrder = string.Empty,
                MemberMaxOrder = string.Empty,
                VendorMinOrder = string.Empty,
                VendorMaxOrder = string.Empty,
                FreeDeliveryMember = "0",
                FreeDeliveryVendor = "0",
                FreeDeliveryForProduct = "0",
                ShortDescription = $"{item.SellerCode} {item.ProductName}",
                Details = item.ProductInformationMarketplaces.FirstOrDefault().Description,
                Width = "0",
                Height = "0",
                Depth = "0",
                Weight = "0",
                CBM = string.Empty,
                Document = string.Empty,
                Warehouse = string.Empty,
                WarrantyInfo = string.Empty,
                DeliveryInfo = string.Empty,
                DeliveryTime = string.Empty,
                ProductNote = string.Empty,
                SeoSettingsId = string.Empty,
                SeoTitle = string.Empty,
                SeoKeywords = string.Empty,
                SeoDescription = string.Empty,
                Gtip = string.Empty,
                Gender = 0,
                IsCatalog = 0,
                OpportunityProduct = 0,
                ListNo = "999999",
                PersonalizationId = 0,
                DisablePaymentTypes = string.Empty,
                DisableCargoCompany = string.Empty,
                Label1 = new TSoftLabel1
                {
                    Value = 0
                },
                Label2 = new TSoftLabel2
                {
                    Value = 0
                },
                Label3 = 1,
                Label4 = 0,
                Label5 = 0,
                Label6 = 0,
                Label7 = 0,
                Label8 = 0,
                Label9 = 0,
                Label10 = 0,
                HomepageSort = 0,
                MostSoldSort = 0,
                NewestSort = 0,
                AgeGroup = "adult",
                Point = 0,
                Numeric1 = string.Empty,
                DefaultSubProductId = 0,
                EftRate = 0,
                Tags = string.Empty,
                SearchRank = 0,
                CountryOfOrigin = "TR",
                ProformaTitle = string.Empty,

            });

            foreach (var pinLoop in item.ProductInformationMarketplaces)
            {
                var itemSetSubProduct = new TSoftSetSubProductRequestV2
                {
                    MainProductCode = item.SellerCode,
                    Barcode = pinLoop.Barcode,
                    SubProductCode = pinLoop.StockCode,
                    SupplierSubProductCode = pinLoop.StockCode,
                    BuyingPrice = "0",
                    DiscountedSellingPrice = (pinLoop.UnitPrice / (1 + pinLoop.VatRate)).ToString(),
                    SellingPrice = (pinLoop.ListUnitPrice / (1 + pinLoop.VatRate)).ToString(),
                    VendorSellingPrice = "0",
                    Stock = pinLoop.Stock.ToString(),
                    IsActive = "1",
                    CBM = "0",
                    Weight = "0",
                    Point = 0,
                    IsDefaultSubProduct = 0,
                    AdditionalInfo = new List<string>(),
                };

                for (var i = 0; i < pinLoop.ProductInformationMarketplaceVariantValues.Count; i++)
                {
                    if (i == 0)
                    {
                        itemSetSubProduct.Property1GroupId = pinLoop.ProductInformationMarketplaceVariantValues[0].MarketplaceVarinatCode;
                        itemSetSubProduct.Property1 = pinLoop.ProductInformationMarketplaceVariantValues[0].MarketplaceVariantValueName;
                    }
                    else if (i == 1)
                    {
                        itemSetSubProduct.Property2GroupId = pinLoop.ProductInformationMarketplaceVariantValues[1].MarketplaceVarinatCode;
                        itemSetSubProduct.Property2 = pinLoop.ProductInformationMarketplaceVariantValues[1].MarketplaceVariantValueName;
                    }

                }


                setSubProductRequestV2.Add(itemSetSubProduct);



                foreach (var photoLoop in pinLoop.ProductInformationPhotos)
                {
                    addImageFromLinkRequestV2.Add(new TSoftProductAddImageFromLinkRequestV2
                    {
                        ListNumber = photoLoop.DisplayOrder.ToString(),
                        ImageUrl = photoLoop.Url,
                        ProductCode = item.SellerCode
                    });
                }

            }


            var setProductRequestV2Data = JsonConvert.SerializeObject(setProductRequestV2);

            var _setProductRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("data", setProductRequestV2Data),
            };

            var setProductRequestV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/product/setProducts",
                Data = _setProductRequestV2Data,
                HttpMethod = HttpMethod.Post
            });


            var httpHelperV3Response = new HttpHelperV3Response<UntrackableCreateProductProcessResponse>
            {
                HttpStatusCode = setProductRequestV2DataResponse.HttpStatusCode,
                Content = new()
            };


            if (setProductRequestV2DataResponse.Content != null && setProductRequestV2DataResponse.Content.success == false && setProductRequestV2DataResponse.Content.message.Count > 0)
            {

                httpHelperV3Response.Content.Messages = setProductRequestV2DataResponse.Content.message.SelectMany(e => e.text).Select(x => x).ToList();
                if (setProductRequestV2DataResponse.Content.message.Any(x => x.code == "AUI004"))
                {
                    return httpHelperV3Response;

                }
            }


            var setSubProductRequestV2Data = JsonConvert.SerializeObject(setSubProductRequestV2);


            var _setSubProductRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("data", setSubProductRequestV2Data),
            };



            var tSoftSetProductV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/subProduct/setSubProducts",
                Data = _setSubProductRequestV2Data,
                HttpMethod = HttpMethod.Post
            });



            if (tSoftSetProductV2DataResponse.Content != null && tSoftSetProductV2DataResponse.Content.success == false && tSoftSetProductV2DataResponse.Content.message.Count > 0)
            {
                var tSoftSetProductResponseError = tSoftSetProductV2DataResponse.Content.message.SelectMany(e => e.text).Select(x => x).ToList();

                if (httpHelperV3Response.Content.Messages == null) httpHelperV3Response.Content.Messages = new List<string>();



                httpHelperV3Response.Content.Messages.AddRange(tSoftSetProductResponseError);
                return httpHelperV3Response;

            }



            var addImageFromLinkRequestV2Data = JsonConvert.SerializeObject(addImageFromLinkRequestV2);


            var _addImageFromLinkRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("ImageSizeWidthHeightControl", "1"),
                new KeyValuePair<string, string>("data", addImageFromLinkRequestV2Data),
            };

            var tSoftAddImageFromLinkV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/product/addImageFromLink",
                Data = _addImageFromLinkRequestV2Data,
                HttpMethod = HttpMethod.Post
            });


            if (tSoftAddImageFromLinkV2DataResponse.Content != null && tSoftAddImageFromLinkV2DataResponse.Content.success == false && tSoftAddImageFromLinkV2DataResponse.Content.message.Count > 0)
            {
                var tSoftSetProductResponseError = tSoftAddImageFromLinkV2DataResponse.Content.message.SelectMany(e => e.text).Select(x => x).ToList();

                if (httpHelperV3Response.Content.Messages == null) httpHelperV3Response.Content.Messages = new List<string>();



                httpHelperV3Response.Content.Messages.AddRange(tSoftSetProductResponseError);
                return httpHelperV3Response;

            }


            return new()
            {
                HttpStatusCode = httpHelperV3Response.HttpStatusCode,
                Content = new()
                {
                    IncludeUrl = true,
                    MarkDirtyStock = false,
                    MarkDirtyPrice = true,
                    UntrackableProcessResponseItems = item
                         .ProductInformationMarketplaces
                         .Select(pim => new UntrackableCreateProductProcessResponseItem
                         {
                             ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                             Url = $"{tSoftConfigurationV2.Url}/{pim.ProductInformationName.GenerateUrl()}",
                             Success = httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK
                         }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            TSoftConfigurationV2 tSoftConfigurationV2 = request
           .MarketplaceRequestHeader
           .MarketplaceConfigurations
            .Decrypt();

            var untrackableProcessResponse = new UntrackableProcessResponse();
            untrackableProcessResponse.UntrackableProcessResponseItems = new List<UntrackableProcessResponseItem>();

            var token = await this.GetToken(tSoftConfigurationV2);


            var _updateStockAndPriceRequestV2 = request
                 .Items
                 .GroupBy(x => x.SellerCode)
                 .Select(x => x.First())
                 .Select(dpi => new TsoftUpdatePriceRequestV2
                 {
                     ProductCode = dpi.SellerCode,
                     BuyingPrice = "0",
                     DiscountedSellingPrice = (dpi.UnitPrice / (1 + dpi.VatRate)).ToString(),
                     SellingPrice = (dpi.ListUnitPrice / (1 + dpi.VatRate)).ToString(),
                     Vat = (dpi.VatRate * 100).ToString(),
                     Currency = "TL"
                 })
             .ToList();

            var _updateStockAndPriceRequestV2Data = JsonConvert.SerializeObject(_updateStockAndPriceRequestV2);

            var __setProductRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("data", _updateStockAndPriceRequestV2Data),
            };



            var _setProductRequestV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/product/updatePrice",
                Data = __setProductRequestV2Data,
                HttpMethod = HttpMethod.Post
            });

            if (_setProductRequestV2DataResponse.Content != null && _setProductRequestV2DataResponse.Content.success == false && _setProductRequestV2DataResponse.Content.message.Count > 0)
            {
                var contents = _setProductRequestV2DataResponse.Content.message.Where(x => x.errorField.Any()).ToList();

                foreach (var cLoop in contents)
                {
                    var item = request
                         .Items.FirstOrDefault(x => x.SellerCode == cLoop.id);

                    if (item != null)
                        untrackableProcessResponse.UntrackableProcessResponseItems.Add(new UntrackableProcessResponseItem
                        {
                            ListUnitPrice = item.ListUnitPrice,
                            UnitPrice = item.UnitPrice,
                            Stock = item.Stock,
                            ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
                            Success = false,
                            Messages = cLoop.text
                        });
                }
            }


            #region Setproducts


            var updateStockAndPriceRequestV2 = request
                 .Items
                 .Select(dpi => new TSoftUpdateStockAndPriceRequestV2
                 {
                     SubProductCode = dpi.ProductInformationUUId,
                     Stock = null,
                     BuyingPrice = "0",
                     DiscountedSellingPrice = (dpi.UnitPrice / (1 + dpi.VatRate)).ToString(),
                     SellingPrice = (dpi.ListUnitPrice / (1 + dpi.VatRate)).ToString(),
                     VendorSellingPrice = "0",
                 })
             .ToList();

            var updateStockAndPriceRequestV2Data = JsonConvert.SerializeObject(updateStockAndPriceRequestV2);

            var _setProductRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("data", updateStockAndPriceRequestV2Data),
            };

            var setProductRequestV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/subProduct/updateStockAndPrice",
                Data = _setProductRequestV2Data,
                HttpMethod = HttpMethod.Post
            });


            var httpHelperV3Response = new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = setProductRequestV2DataResponse.HttpStatusCode,
                Content = new()
            };

            if (setProductRequestV2DataResponse.Content != null && setProductRequestV2DataResponse.Content.success == false && setProductRequestV2DataResponse.Content.message.Count > 0)
            {
                var contents = setProductRequestV2DataResponse.Content.message.Where(x => x.errorField.Any()).ToList();

                foreach (var cLoop in contents)
                {
                    var item = request
                         .Items.FirstOrDefault(x => x.StockCode == cLoop.id);

                    if (item != null)
                        untrackableProcessResponse.UntrackableProcessResponseItems.Add(new UntrackableProcessResponseItem
                        {
                            ListUnitPrice = item.ListUnitPrice,
                            UnitPrice = item.UnitPrice,
                            Stock = item.Stock,
                            ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
                            Success = false,
                            Messages = cLoop.text
                        });

                }


            }

            #endregion
            await Console.Out.WriteLineAsync(httpHelperV3Response.ContentString);

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = httpHelperV3Response.HttpStatusCode,
                Content = untrackableProcessResponse

            };

        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            TSoftConfigurationV2 tSoftConfigurationV2 = request
           .MarketplaceRequestHeader
           .MarketplaceConfigurations
            .Decrypt();


            var token = await this.GetToken(tSoftConfigurationV2);

            var updateStockAndPriceRequestV2 = request
                 .Items
                 .Select(dpi => new TSoftUpdateStockAndPriceRequestV2
                 {
                     SubProductCode = dpi.ProductInformationUUId,
                     Stock = dpi.Stock.ToString(),
                     BuyingPrice = "0",
                     DiscountedSellingPrice = dpi.UpdatePrice ? (dpi.UnitPrice / (1 + dpi.VatRate)).ToString() : null,
                     SellingPrice = dpi.UpdatePrice ? (dpi.ListUnitPrice / (1 + dpi.VatRate)).ToString() : null,
                     VendorSellingPrice = "0",
                 })
             .ToList();

            var updateStockAndPriceRequestV2Data = JsonConvert.SerializeObject(updateStockAndPriceRequestV2);

            var _setProductRequestV2Data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("data", updateStockAndPriceRequestV2Data),
            };



            var setProductRequestV2DataResponse = await this._httpHelperV3.SendAsync<TSoftSetProductResponseV2>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = $"{tSoftConfigurationV2.Url}/rest1/subProduct/updateStockAndPrice",
                Data = _setProductRequestV2Data,
                HttpMethod = HttpMethod.Post
            });


            var httpHelperV3Response = new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = setProductRequestV2DataResponse.HttpStatusCode,
                Content = new()
            };

            var untrackableProcessResponse = new UntrackableProcessResponse();
            untrackableProcessResponse.UntrackableProcessResponseItems = new List<UntrackableProcessResponseItem>();

            if (setProductRequestV2DataResponse.Content != null && setProductRequestV2DataResponse.Content.success == false && setProductRequestV2DataResponse.Content.message.Count > 0)
            {
                var contents = setProductRequestV2DataResponse.Content.message.Where(x => x.errorField.Any()).ToList();

                foreach (var cLoop in contents)
                {
                    var item = request
                         .Items.FirstOrDefault(x => x.StockCode == cLoop.id);

                    if (item != null)
                        untrackableProcessResponse.UntrackableProcessResponseItems.Add(new UntrackableProcessResponseItem
                        {
                            ListUnitPrice = item.ListUnitPrice,
                            UnitPrice = item.UnitPrice,
                            Stock = item.Stock,
                            ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
                            Success = false,
                            Messages = cLoop.text
                        });

                }


            }

            await Console.Out.WriteLineAsync(httpHelperV3Response.ContentString);

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = httpHelperV3Response.HttpStatusCode,
                Content = untrackableProcessResponse

            };

        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.BrandCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.BrandCode)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Marka bilgisi bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.BrandCode));
                }

                if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.ProductName)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.ProductName)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Ürün adı bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.ProductName));
                }

                if (theRequest.Items.Any(_ => string.IsNullOrEmpty(_.CategoryCode)))
                {
                    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.CategoryCode)).ToList();
                    invalidItems.ForEach(i => i.ErrorMessage = "Katrgori bulunamadı.");
                    theRequest.InvalidItems ??= new();
                    theRequest.InvalidItems.AddRange(invalidItems);
                    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.CategoryCode));
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1).Take(1).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            TSoftConfigurationV2 tSoftConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            String token = await this.GetToken(tSoftConfigurationV2);
            List<Category> categories = new();

            var data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token),
            };

            var listCategoryResponse = await this._httpHelperV3.SendAsync<TSoftCategoryResponse>(new HttpHelperV3FormUrlEncodedRequest
            {
                RequestUri = tSoftConfigurationV2.Url + "/rest1/category/getCategories",
                Data = data,
                HttpMethod = HttpMethod.Post
            });

            categories.AddRange(listCategoryResponse.Content.Data.Select(lc => new Category
            {
                Code = lc.CategoryCode,
                Name = lc.CategoryName
            }));

            //            queryObject = new QueryObjectV2
            //            {
            //                Query = @"
            //{
            //    listVariantType{
            //        id
            //        name,
            //        values{
            //            id
            //            name
            //        }
            //    }
            //}"
            //            };

            //            var listVariantTypeResponse = await this._httpHelperV3.SendAsync<QueryObjectV2, ListVariantTypeReponse>(new HttpHelperV3Request<QueryObjectV2>
            //            {
            //                RequestUri = this._graphQlUrl,
            //                AuthorizationType = AuthorizationType.Bearer,
            //                Authorization = token,
            //                HttpMethod = HttpMethod.Post,
            //                Request = queryObject
            //            });

            //            categories.ForEach(theCategory =>
            //            {
            //                theCategory.CategoryAttributes = listVariantTypeResponse.Content.ListVariantType
            //                        .Select(v => new CategoryAttribute
            //                        {
            //                            Code = v.Id,
            //                            Name = v.Name,
            //                            Mandatory = true,
            //                            AllowCustom = false,
            //                            MultiValue = false,
            //                            CategoryCode = theCategory.Code,
            //                            CategoryAttributesValues = v
            //                                .Values
            //                                .Select(vv => new CategoryAttributeValue
            //                                {
            //                                    Code = vv.Id,
            //                                    Name = vv.Name,
            //                                    VarinatCode = v.Id
            //                                })
            //                                .ToList()
            //                        })
            //                        .ToList();
            //            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private async Task<string> GetToken(TSoftConfigurationV2 tSoftConfigurationV2)
        {
            if (tSoftConfigurationV2.ExpirationTime < DateTime.Now)
            {
                var tokenRequest = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("user", tSoftConfigurationV2.User),
                new KeyValuePair<string, string>("pass", tSoftConfigurationV2.Password),
                    };

                var tokenResponse = await this._httpHelperV3.SendAsync<TSoftTokenResponse>(new HttpHelperV3FormUrlEncodedRequest
                {
                    RequestUri = $"{tSoftConfigurationV2.Url}/rest1/auth/login/helpy",
                    Data = tokenRequest,
                    HttpMethod = HttpMethod.Post
                });
                var token = string.Empty;
                if (tokenResponse.Content != null && tokenResponse.Content.Success == true)
                {
                    token = tokenResponse.Content.Data[0].Token;
                }

                return await Task.FromResult<string>(token);

            }
            else
                return await Task.FromResult<string>(tSoftConfigurationV2.Token);


        }
        #endregion
    }
}