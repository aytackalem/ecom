﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ECommerce.Infrastructure.MarketplacesV2.TSoft
{
    public class TSoftConfigurationV2
    {
        #region Members
        public static implicit operator TSoftConfigurationV2(Dictionary<string, string> configurations)
        {
            return new TSoftConfigurationV2
            {
                Url = configurations["Url"],
                User = configurations["User"],
                Password = configurations["Password"],
                Token = configurations["Token"],
                ExpirationTime = Convert.ToDateTime(configurations["ExpirationTime"], System.Globalization.CultureInfo.GetCultureInfo("tr-TR")),
            };
        }
        #endregion

        #region Properties
        public string Url { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public DateTime ExpirationTime { get; set; }


        #endregion
    }
}
