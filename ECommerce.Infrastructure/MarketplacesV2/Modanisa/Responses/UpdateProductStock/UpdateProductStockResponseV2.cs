﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.UpdateProductStock
{
    public class UpdateProductStockResponseV2
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int status_code { get; set; }
        public string batchId { get; set; }
    }
}
