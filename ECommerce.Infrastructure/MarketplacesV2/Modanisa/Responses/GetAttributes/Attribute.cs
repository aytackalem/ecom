using System.Collections.Generic;
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes
{

    public class Attribute
    {
        public int id { get; set; }
        public string name { get; set; }
        public string mandatory { get; set; }
        public List<AttributeValue> attribute_values { get; set; }
    }

}