using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes
{

    public class CategoryResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<Attribute> attributes { get; set; }
    }

}