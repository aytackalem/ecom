namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes
{

    public class Data
    {
        public CategoryResponse category { get; set; }
    }

}