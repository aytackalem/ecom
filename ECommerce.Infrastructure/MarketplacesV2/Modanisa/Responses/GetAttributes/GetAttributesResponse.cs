﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes
{
    public class GetAttributesResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }
}
