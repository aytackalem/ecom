namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes
{

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}