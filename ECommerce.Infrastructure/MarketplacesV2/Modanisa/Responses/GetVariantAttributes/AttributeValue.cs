namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetVariantAttributes
{

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}