﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetVariantAttributes
{
    public class GetVariantAttributesResponse
    {
        public bool success { get; set; }
        public GetVariantAttributesResponseData data { get; set; }
        public object errors { get; set; }
    }
}
