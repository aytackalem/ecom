namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetVariantAttributes
{

    public class GetVariantAttributesResponseData
    {
        public GetVariantAttributesResponseDataItem category { get; set; }
    }

}