using System.Collections.Generic;
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetVariantAttributes
{

    public class GetVariantAttributesResponseDataItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<VariantAttribute> variant_attributes { get; set; }
    }

}