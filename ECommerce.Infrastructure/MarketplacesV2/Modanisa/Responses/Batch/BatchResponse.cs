﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.Batch
{
    public class BatchResponse
    {
        #region Properties
        public string BatchId { get; set; }

        public string Status { get; set; }

        public dynamic Items { get; set; }
        #endregion
    }
}
