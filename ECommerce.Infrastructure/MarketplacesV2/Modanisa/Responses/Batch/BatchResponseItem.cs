﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.Batch
{
    public class BatchResponseItem
    {
        #region Properties
        public string Barcode { get; set; }

        public string Status { get; set; }
        
        public List<string> Errors { get; set; }
        #endregion
    }
}
