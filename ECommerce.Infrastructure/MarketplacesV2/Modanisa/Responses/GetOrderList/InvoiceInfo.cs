using System.Collections.Generic; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class InvoiceInfo
    {
        public string address { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string phoneNumber { get; set; }
        public string taxAdministration { get; set; }
        public string taxNumber { get; set; }
    }

}