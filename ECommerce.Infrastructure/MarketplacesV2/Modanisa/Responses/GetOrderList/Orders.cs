using System.Collections.Generic; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class Orders
    {
        public int totalCount { get; set; }
        public List<Detail> details { get; set; }
    }

}