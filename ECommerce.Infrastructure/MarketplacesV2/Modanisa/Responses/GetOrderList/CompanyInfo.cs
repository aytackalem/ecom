using System.Collections.Generic; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class CompanyInfo
    {
        public string name { get; set; }
        public string address { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public InvoiceInfo invoiceInfo { get; set; }
    }

}