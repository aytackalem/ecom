using System.Collections.Generic; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class Pagination
    {
        public double page { get; set; }
        public int totalPage { get; set; }
        public double count { get; set; }
        public double limit { get; set; }
    }

}