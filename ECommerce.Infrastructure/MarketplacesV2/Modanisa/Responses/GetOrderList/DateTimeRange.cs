using System.Collections.Generic; 
using System; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class DateTimeRange
    {
        public string startDateTime { get; set; }
        public string endDateTime { get; set; }
    }

}