using System.Collections.Generic; 
namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList{ 

    public class Detail
    {
        public string orderId { get; set; }
        public string paymentStatus { get; set; }
        public string date { get; set; }
        public List<Product> products { get; set; }
    }

}