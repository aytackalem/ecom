﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.ProductCheck
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Data
    {
        public List<Product> products { get; set; }
        public Pagination pagination { get; set; }
    }

    public class Errors
    {
        public List<string> warning { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int total_page { get; set; }
        public int count { get; set; }
        public int limit { get; set; }
    }

    public class Price
    {
        public double price { get; set; }
        public double priceAlt { get; set; }
    }

    public class Product
    {
        public int request_id { get; set; }
        public string product_id { get; set; }
        public int? mdns_product_id { get; set; }
        public string status { get; set; }
        public string created_date { get; set; }
        public List<Variant> variants { get; set; }
        public Errors errors { get; set; }
        public List<Price> prices { get; set; }
        public int category_id { get; set; }
    }

    public class ProductCheckResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }

    public class Variant
    {
        public string variant_id { get; set; }
        public int mdns_variant_id { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
    }



    //public class ProductCheckResponse
    //{
    //    public bool success { get; set; }
    //    public Data data { get; set; }
    //    public object errors { get; set; }
    //}

    //public class Data
    //{
    //    public Product[] products { get; set; }
    //    public Pagination pagination { get; set; }
    //}

    //public class Pagination
    //{
    //    public int page { get; set; }
    //    public int total_page { get; set; }
    //    public int count { get; set; }
    //    public int limit { get; set; }
    //}

    //public class Product
    //{
    //    public int request_id { get; set; }
    //    public string product_id { get; set; }
    //    public int? mdns_product_id { get; set; }
    //    public string status { get; set; }
    //    public string created_date { get; set; }
    //    public Variant[] variants { get; set; }
    //    public Errors errors { get; set; }
    //    public float?[] prices { get; set; }
    //    public int? category_id { get; set; }
    //}

    //public class Variant
    //{
    //    public string variant_id { get; set; }
    //    public int mdns_variant_id { get; set; }
    //    public string barcode { get; set; }
    //    public int quantity { get; set; }
    //}

    //public class Errors
    //{
    //    public Product2 product { get; set; }
    //    public List<string> fatal { get; set; }
    //    public List<string> variant { get; set; }
    //}

    //public class Product2
    //{
    //    public List<string> attribute { get; set; }
    //}
}
