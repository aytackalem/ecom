﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.CreateProduct
{
    public class CreateProductBadRequestResponseV2
    {
        public bool success { get; set; }
        public List<object> data { get; set; }
        public Errors errors { get; set; }
    }

    public class Errors
    {
        public string[] attributes { get; set; }
    }
}
