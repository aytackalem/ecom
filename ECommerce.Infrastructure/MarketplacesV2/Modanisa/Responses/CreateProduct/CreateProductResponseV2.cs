﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.CreateProduct
{
    public class CreateProductResponseV2
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }

    public class Data
    {
        public string message { get; set; }
        public int request_id { get; set; }
    }
}
