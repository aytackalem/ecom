using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetCategories
{

    public class Data
    {
        public List<CategoryResponse> categories { get; set; }
    }

}