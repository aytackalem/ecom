using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetCategories
{
    public class CategoryResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public int parent_id { get; set; }
        public bool fabric_mandatory { get; set; }
        public List<SubCategory> sub_categories { get; set; }
    }

}