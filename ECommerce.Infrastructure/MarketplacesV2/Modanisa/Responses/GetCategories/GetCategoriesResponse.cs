﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetCategories
{
    public class GetCategoriesResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }
}
