﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.CreateProduct
{

    public class CreateProductRequestV2
    {
        public string product_id { get; set; }
        public string product_name { get; set; }
        public string description { get; set; }
        public int brand_id { get; set; }
        public int category_id { get; set; }
        public int origin_country_id { get; set; }
        public string stock_code { get; set; }
        public decimal price { get; set; }
        public decimal price_alt { get; set; }
        public int tax_rate { get; set; }
        public int season_id { get; set; }
        public float weight { get; set; }
        public Fabric[] fabrics { get; set; }
        public Model[] models { get; set; }
        public Image[] images { get; set; }
        public List<Variant> variants { get; set; }
        public List<Attribute1> attributes { get; set; }
    }

    public class Fabric
    {
        public int fabric_id { get; set; }
        public int percentage { get; set; }
    }

    public class Model
    {
        public int size { get; set; }
        public int waist { get; set; }
        public int length { get; set; }
        public int hip { get; set; }
        public int chest { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
    }

    public class Variant
    {
        public string variant_id { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
        public List<Attribute> attributes { get; set; }
        public List<Alternative_Barcodes> alternative_barcodes { get; set; }
    }

    public class Attribute
    {
        public int attribute_id { get; set; }
        public int attribute_value_id { get; set; }
    }

    public class Alternative_Barcodes
    {
        public string barcode { get; set; }
    }

    public class Attribute1
    {
        public int attribute_id { get; set; }
        public int attribute_value_id { get; set; }
    }

}
