﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice.UpdateProductStock
{
    public class ProductV2
    {
        #region Properties
        public string variant_id { get; set; }
        
        public int quantity { get; set; }
        
        public string barcode { get; set; } 
        #endregion
    }
}
