﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice.UpdateProductStock
{
    public class UpdateProductStockRequestV2
    {
        #region Navigation Properties
        public List<ProductV2> Products { get; set; } 
        #endregion
    }
}
