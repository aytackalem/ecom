﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice
{
    public class UpdateProductPriceRequestV2
    {
        #region Navigation Properties
        public List<ProductV2> Products { get; set; } 
        #endregion
    }
}
