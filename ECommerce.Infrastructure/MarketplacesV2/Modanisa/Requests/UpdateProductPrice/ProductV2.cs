﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice
{
    public class ProductV2
    {
        #region Properties
        public string variant_id { get; set; }
        
        public string price { get; set; }
        
        public string old_price { get; set; }
        
        public string barcode { get; set; } 
        #endregion
    }
}
