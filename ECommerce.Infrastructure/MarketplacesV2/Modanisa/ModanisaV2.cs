﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Orders;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.UpdateProductPrice;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.UpdateProductStock;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.CreateProduct;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.ProductCheck;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Request.UpdateProductPrice.UpdateProductStock;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.Batch;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.CreateProduct;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetAttributes;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetCategories;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Response.GetVariantAttributes;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa.Responses.GetOrderList;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Modanisa
{
    public class ModanisaV2 : IMarketplaceV2, IMarketplaceV2CreateProductTrackable, IMarketplaceV2UpdateProductTrackable, IMarketplaceV2StockPriceTrackable, IMarketplaceV2CategoryGetable, IMarketplaceV2OrderGetable
    {
        #region Constants
#if DEBUG
        //private const string _baseUrl = "https://marketplace-stg.modanisa.com/api/marketplace";
#endif

        private const string _baseUrl = "https://marketplace.modanisa.com/api/marketplace";


        private readonly string _marketplaceId = Application.Common.Constants.Marketplaces.Modanisa;

        private readonly List<Marketplaces.Modanisa.Responses.GetFabrics.Fabric> fabrics = JsonConvert.DeserializeObject<List<Marketplaces.Modanisa.Responses.GetFabrics.Fabric>>(@"[
            {
                ""id"": 2,
                ""name"": ""Polyester""
            },
            {
                ""id"": 3,
                ""name"": ""Pamuk""
            },
            {
                ""id"": 6,
                ""name"": ""Viskon""
            },
            {
                ""id"": 7,
                ""name"": ""Tensel""
            },
            {
                ""id"": 8,
                ""name"": ""Kot""
            },
            {
                ""id"": 9,
                ""name"": ""Elastan""
            },
            {
                ""id"": 10,
                ""name"": ""İpek""
            },
            {
                ""id"": 11,
                ""name"": ""Yün""
            },
            {
                ""id"": 12,
                ""name"": ""Keten""
            },
            {
                ""id"": 13,
                ""name"": ""Angora""
            },
            {
                ""id"": 14,
                ""name"": ""Likra""
            },
            {
                ""id"": 15,
                ""name"": ""Akrilik""
            },
            {
                ""id"": 16,
                ""name"": ""Floş""
            },
            {
                ""id"": 17,
                ""name"": ""Deri""
            },
            {
                ""id"": 18,
                ""name"": ""Suni Deri""
            },
            {
                ""id"": 19,
                ""name"": ""Süet""
            },
            {
                ""id"": 20,
                ""name"": ""Polyamid""
            },
            {
                ""id"": 21,
                ""name"": ""Bambu""
            },
            {
                ""id"": 22,
                ""name"": ""Penye""
            },
            {
                ""id"": 23,
                ""name"": ""Modal""
            },
            {
                ""id"": 24,
                ""name"": ""Coolmax""
            },
            {
                ""id"": 25,
                ""name"": ""Krep""
            },
            {
                ""id"": 26,
                ""name"": ""Metalik""
            },
            {
                ""id"": 27,
                ""name"": ""Rayon""
            },
            {
                ""id"": 28,
                ""name"": ""Poliviskon""
            },
            {
                ""id"": 29,
                ""name"": ""Viskoz""
            },
            {
                ""id"": 30,
                ""name"": ""Poliamid""
            },
            {
                ""id"": 31,
                ""name"": ""Kupro""
            },
            {
                ""id"": 32,
                ""name"": ""Poliüretan""
            },
            {
                ""id"": 33,
                ""name"": ""Polivinil Klorür""
            },
            {
                ""id"": 34,
                ""name"": ""Hasır""
            },
            {
                ""id"": 35,
                ""name"": ""Jüt""
            },
            {
                ""id"": 36,
                ""name"": ""Kağıt""
            },
            {
                ""id"": 37,
                ""name"": ""Lyocell""
            },
            {
                ""id"": 38,
                ""name"": ""Metal""
            },
            {
                ""id"": 39,
                ""name"": ""Plastik""
            },
            {
                ""id"": 40,
                ""name"": ""Polietilen""
            },
            {
                ""id"": 41,
                ""name"": ""Polipropilen""
            },
            {
                ""id"": 42,
                ""name"": ""Metal İplik""
            },
            {
                ""id"": 43,
                ""name"": ""Metalik İplik""
            },
            {
                ""id"": 44,
                ""name"": ""Asetat""
            },
            {
                ""id"": 45,
                ""name"": ""Elastomultiester""
            },
            {
                ""id"": 46,
                ""name"": ""Modakrilik""
            },
            {
                ""id"": 47,
                ""name"": ""Naylon""
            },
            {
                ""id"": 48,
                ""name"": ""EVA""
            },
            {
                ""id"": 49,
                ""name"": ""FAYLON""
            },
            {
                ""id"": 50,
                ""name"": ""PVC""
            },
            {
                ""id"": 51,
                ""name"": ""Alaşım""
            },
            {
                ""id"": 52,
                ""name"": ""Doğal taş""
            },
            {
                ""id"": 54,
                ""name"": ""Pirinç""
            },
            {
                ""id"": 55,
                ""name"": ""Boncuk""
            },
            {
                ""id"": 57,
                ""name"": ""Cam""
            },
            {
                ""id"": 58,
                ""name"": ""Çelik""
            },
            {
                ""id"": 59,
                ""name"": ""Çinko Alaşım""
            },
            {
                ""id"": 60,
                ""name"": ""Demir""
            },
            {
                ""id"": 61,
                ""name"": ""Elyaf""
            },
            {
                ""id"": 62,
                ""name"": ""Elasto Multi Ester""
            },
            {
                ""id"": 63,
                ""name"": ""Mıkrofıber""
            },
            {
                ""id"": 64,
                ""name"": ""Polikarbonat""
            },
            {
                ""id"": 65,
                ""name"": ""Rami""
            },
            {
                ""id"": 66,
                ""name"": ""Sim""
            },
            {
                ""id"": 67,
                ""name"": ""Teneke""
            },
            {
                ""id"": 68,
                ""name"": ""Ahşap""
            },
            {
                ""id"": 69,
                ""name"": ""Moher""
            },
            {
                ""id"": 70,
                ""name"": ""Tiftik""
            },
            {
                ""id"": 71,
                ""name"": ""Scuba""
            },
            {
                ""id"": 72,
                ""name"": ""Kaşmir""
            },
            {
                ""id"": 73,
                ""name"": ""İnterlok""
            },
            {
                ""id"": 74,
                ""name"": ""Likralı Suprem""
            },
            {
                ""id"": 75,
                ""name"": ""AĞAÇ KABUĞU""
            },
            {
                ""id"": 76,
                ""name"": ""AKİK""
            },
            {
                ""id"": 77,
                ""name"": ""AKRİLAT KOPOLİMERİ""
            },
            {
                ""id"": 78,
                ""name"": ""AKRİLİK BONCUK""
            },
            {
                ""id"": 79,
                ""name"": ""AKRİLİK TAŞ""
            },
            {
                ""id"": 80,
                ""name"": ""ALKOL""
            },
            {
                ""id"": 81,
                ""name"": ""ALLOY""
            },
            {
                ""id"": 82,
                ""name"": ""ALPAKA""
            },
            {
                ""id"": 83,
                ""name"": ""ALÜMİNYUM""
            },
            {
                ""id"": 84,
                ""name"": ""ALÜMİNYUM TOZU""
            },
            {
                ""id"": 85,
                ""name"": ""BAKIR""
            },
            {
                ""id"": 86,
                ""name"": ""BAKIR TOZU""
            },
            {
                ""id"": 87,
                ""name"": ""BAL MUMU İPLİK""
            },
            {
                ""id"": 88,
                ""name"": ""BOYA""
            },
            {
                ""id"": 89,
                ""name"": ""CAM BONCUK""
            },
            {
                ""id"": 90,
                ""name"": ""DACRON""
            },
            {
                ""id"": 91,
                ""name"": ""DİĞER KİMYASALLAR""
            },
            {
                ""id"": 92,
                ""name"": ""DİĞER MALZEMELER""
            },
            {
                ""id"": 93,
                ""name"": ""DÖVME MÜREKKEBİ""
            },
            {
                ""id"": 94,
                ""name"": ""EMAYE""
            },
            {
                ""id"": 95,
                ""name"": ""EPOKSİ""
            },
            {
                ""id"": 96,
                ""name"": ""FİBRA AKRİLİK""
            },
            {
                ""id"": 97,
                ""name"": ""GÜMÜŞ""
            },
            {
                ""id"": 98,
                ""name"": ""HALAT""
            },
            {
                ""id"": 99,
                ""name"": ""İNCİ""
            },
            {
                ""id"": 100,
                ""name"": ""İPLİK""
            },
            {
                ""id"": 101,
                ""name"": ""JELATİN""
            },
            {
                ""id"": 102,
                ""name"": ""KABUK""
            },
            {
                ""id"": 103,
                ""name"": ""KADİFE""
            },
            {
                ""id"": 104,
                ""name"": ""KAPLANMIŞ DERİ""
            },
            {
                ""id"": 105,
                ""name"": ""KARIŞIK""
            },
            {
                ""id"": 106,
                ""name"": ""KAZ TÜYÜ""
            },
            {
                ""id"": 107,
                ""name"": ""KECE""
            },
            {
                ""id"": 108,
                ""name"": ""KIRIKTAS""
            },
            {
                ""id"": 109,
                ""name"": ""KİL""
            },
            {
                ""id"": 110,
                ""name"": ""KRİSTAL""
            },
            {
                ""id"": 111,
                ""name"": ""KUMAŞ""
            },
            {
                ""id"": 112,
                ""name"": ""KURŞUN""
            },
            {
                ""id"": 113,
                ""name"": ""KURUTULMUŞ ÇİÇEK""
            },
            {
                ""id"": 114,
                ""name"": ""KUVARS""
            },
            {
                ""id"": 115,
                ""name"": ""LUREKS""
            },
            {
                ""id"": 116,
                ""name"": ""MAGNEZYUM""
            },
            {
                ""id"": 117,
                ""name"": ""MANTAR""
            },
            {
                ""id"": 118,
                ""name"": ""MİKRO POLYESTER""
            },
            {
                ""id"": 119,
                ""name"": ""MINE""
            },
            {
                ""id"": 120,
                ""name"": ""MIYUKI BONCUK""
            },
            {
                ""id"": 121,
                ""name"": ""MİNERAL YAĞ""
            },
            {
                ""id"": 122,
                ""name"": ""MUM İP""
            },
            {
                ""id"": 123,
                ""name"": ""NAYLON İPLİK""
            },
            {
                ""id"": 124,
                ""name"": ""NEOLITE""
            },
            {
                ""id"": 125,
                ""name"": ""NİKEL GÜMÜŞ""
            },
            {
                ""id"": 126,
                ""name"": ""Ördek Tüyü""
            },
            {
                ""id"": 127,
                ""name"": ""PARAFİN""
            },
            {
                ""id"": 128,
                ""name"": ""PLASTİK BONCUK""
            },
            {
                ""id"": 129,
                ""name"": ""POLİMER KÖPÜK""
            },
            {
                ""id"": 130,
                ""name"": ""POLİVİNİLBUTİRAL""
            },
            {
                ""id"": 131,
                ""name"": ""POLY AKRİLİK""
            },
            {
                ""id"": 132,
                ""name"": ""Polyamide 12""
            },
            {
                ""id"": 133,
                ""name"": ""PROPYLENE GLYCOL""
            },
            {
                ""id"": 134,
                ""name"": ""REÇİNE""
            },
            {
                ""id"": 135,
                ""name"": ""REÇİNE AKRİLİK ESTER""
            },
            {
                ""id"": 136,
                ""name"": ""SAKAROZ ESTERİ""
            },
            {
                ""id"": 137,
                ""name"": ""SATEN""
            },
            {
                ""id"": 138,
                ""name"": ""SEDEF""
            },
            {
                ""id"": 139,
                ""name"": ""SİLİKON KAUÇUK""
            },
            {
                ""id"": 140,
                ""name"": ""SİLİKON LASTİK""
            },
            {
                ""id"": 141,
                ""name"": ""SİMLİ NAYLON İPLİK""
            },
            {
                ""id"": 142,
                ""name"": ""SİMLİ POLYESTER İPLİK""
            },
            {
                ""id"": 143,
                ""name"": ""SPANDEKS""
            },
            {
                ""id"": 144,
                ""name"": ""TAHTA""
            },
            {
                ""id"": 145,
                ""name"": ""TAHTA BONCUK""
            },
            {
                ""id"": 146,
                ""name"": ""TAŞ""
            },
            {
                ""id"": 147,
                ""name"": ""TEKSTİL""
            },
            {
                ""id"": 148,
                ""name"": ""TEL""
            },
            {
                ""id"": 149,
                ""name"": ""THERMO""
            },
            {
                ""id"": 150,
                ""name"": ""TİTANYUM""
            },
            {
                ""id"": 151,
                ""name"": ""TİTANYUM DİOKSİT""
            },
            {
                ""id"": 152,
                ""name"": ""TÜL""
            },
            {
                ""id"": 153,
                ""name"": ""TÜY""
            },
            {
                ""id"": 154,
                ""name"": ""YAPAY SÜET""
            },
            {
                ""id"": 155,
                ""name"": ""YAPAY SÜNGER""
            },
            {
                ""id"": 156,
                ""name"": ""YAPIŞTIRICI""
            },
            {
                ""id"": 157,
                ""name"": ""YARI DEĞERLİ""
            },
            {
                ""id"": 158,
                ""name"": ""YOSUN""
            },
            {
                ""id"": 159,
                ""name"": ""ZAMAK""
            },
            {
                ""id"": 160,
                ""name"": ""Payet""
            },
            {
                ""id"": 161,
                ""name"": ""Dantel""
            },
            {
                ""id"": 162,
                ""name"": ""Jarse""
            },
            {
                ""id"": 163,
                ""name"": ""Atlas""
            },
            {
                ""id"": 164,
                ""name"": ""Şifon""
            }
        ]");
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public ModanisaV2(IHttpHelperV3 httpHelperV3)
        {
            _httpHelperV3 = httpHelperV3;
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Modanisa;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var item = request.Items.First();

            var productInformationMarketplace = item.ProductInformationMarketplaces.First();

            var fabric = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Kumaş");
            var origin = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Menşei");
            var season = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Sezon");

            var brand_id = int.Parse(item.BrandCode);
            var category_id = int.Parse(item.CategoryCode);
            var season_id = this.FindSeasonId(item.SellerCode);
            var fabricsFound = this.TryFindFabric(productInformationMarketplace.Description, out Fabric[] fabrics);
            if (fabricsFound == false)
            {
                return new HttpHelperV3Response<TrackableResponse>
                {
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Content = new()
                    {
                        Messages = new()
                        {
                            "Kumaş içeriği bulunamadı."
                        }
                    }
                };
            }

            var createProductRequestV2 = new CreateProductRequestV2
            {
                product_id = productInformationMarketplace.SkuCode,
                brand_id = brand_id,
                category_id = category_id,
                season_id = season_id,//int.Parse(season.MarketplaceVariantValueId),
                fabrics = fabrics,
                origin_country_id = 220,//int.Parse(origin.MarketplaceVariantValueId),
                product_name = item.ProductName,
                description = productInformationMarketplace.Description,
                price = productInformationMarketplace.UnitPrice,
                price_alt = productInformationMarketplace.ListUnitPrice > productInformationMarketplace.UnitPrice
                    ? productInformationMarketplace.ListUnitPrice
                    : 0,
                images = productInformationMarketplace
                    .ProductInformationPhotos
                    .Select(pip => new Image { url = pip.Url })
                    .Take(10)
                    .ToArray(),
                tax_rate = Convert.ToInt32(100 * productInformationMarketplace.VatRate),
                stock_code = item.SellerCode
            };

            HttpHelperV3Response<CreateProductResponseV2, CreateProductBadRequestResponseV2> httpResultResponse = null;

            var colors = item
                .ProductInformationMarketplaces
                .Select(pim => pim
                    .ProductInformationMarketplaceVariantValues
                    .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renkler")
                    .MarketplaceVariantValueName)
                .Distinct()
                .ToList();

            foreach (var theColor in colors)
            {
                createProductRequestV2.variants = new();
                createProductRequestV2.attributes = new();

                var productInformationMarketplaces = item
                    .ProductInformationMarketplaces
                    .Where(pim => pim
                        .ProductInformationMarketplaceVariantValues
                        .FirstOrDefault(pimvv => pimvv
                            .MarketplaceVarinatName
                            .ToLower() == "renkler")
                        .MarketplaceVariantValueName == theColor)
                    .ToList();

                createProductRequestV2.product_name = productInformationMarketplace.ProductInformationName;

                foreach (var pim in productInformationMarketplaces)
                {
                    var variantAttribute = pim
                        .ProductInformationMarketplaceVariantValues
                        .First(pimvv => pimvv.MarketplaceVarinatName.ToLower().Contains("beden"));
                    createProductRequestV2.variants ??= new();
                    createProductRequestV2.variants.Add(new Variant
                    {
                        variant_id = pim.StockCode,
                        barcode = pim.Barcode,
                        quantity = pim.Stock,
                        attributes = new()
                        {
                            new()
                            {
                                attribute_id = int.Parse(variantAttribute.MarketplaceVarinatCode.Replace($"VA_{item.CategoryCode}_", "")),
                                attribute_value_id = int.Parse(variantAttribute.MarketplaceVariantValueCode.Replace("VA_",""))
                            }
                        }
                    });

                    createProductRequestV2.attributes = pim
                        .ProductInformationMarketplaceVariantValues
                        .Where(pimvv => pimvv.MarketplaceVarinatName.ToLower().Contains("beden") == false &&
                                        pimvv.MarketplaceVarinatName.ToLower() != "menşei" &&
                                        pimvv.MarketplaceVarinatName.ToLower() != "sezon")
                        .GroupBy(pimvv => new
                        {
                            pimvv.MarketplaceVarinatCode,
                            pimvv.MarketplaceVariantValueCode
                        })
                        .Select(x => new Attribute1
                        {
                            attribute_id = int.Parse(x.Key.MarketplaceVarinatCode.Replace($"VA_{item.CategoryCode}_", "")),
                            attribute_value_id = int.Parse(x.Key.MarketplaceVariantValueCode.Replace("VA_", ""))
                        })
                        .ToList();
                }

                var requestUri = $"{_baseUrl}/create-product";
                var httpMethod = HttpMethod.Post;

                httpResultResponse = await this
                   ._httpHelperV3
                   .SendAsync<CreateProductRequestV2, CreateProductResponseV2, CreateProductBadRequestResponseV2>(
                       new HttpHelperV3Request<CreateProductRequestV2>
                       {
                           RequestUri = requestUri,
                           Request = createProductRequestV2,
                           HttpMethod = httpMethod,
                           AuthorizationType = AuthorizationType.Basic,
                           Authorization = modanisaConfigurationV2.Authorization
                       });
            }

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpResultResponse.HttpStatusCode == HttpStatusCode.Created)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.data.request_id.ToString();
            else if (httpResultResponse.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = new()
                {
                    string.Join("",httpResultResponse.ContentError.errors.attributes)
                };

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateProduct request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var item = request.Items.First();

            var productInformationMarketplace = item.ProductInformationMarketplaces.First();

            var fabric = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Kumaş");
            var origin = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Menşei");
            var season = productInformationMarketplace
                .ProductInformationMarketplaceVariantValues
                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName == "Sezon");

            var createProductRequestV2 = new CreateProductRequestV2
            {
                product_id = item.ProductId.ToString(),
                brand_id = int.Parse(item.BrandId),
                category_id = int.Parse(item.CategoryCode),
                season_id = int.Parse(season.MarketplaceVariantValueCode),
                fabrics = new Fabric[]
                {
                        new Fabric
                        {
                            fabric_id=2,
                            percentage=98
                        },
                        new Fabric
                        {
                            fabric_id=3,
                            percentage=2
                        }
                },
                origin_country_id = int.Parse(origin.MarketplaceVariantValueCode),
                product_name = item.ProductName,
                description = productInformationMarketplace.Description,
                price = productInformationMarketplace.UnitPrice,
                price_alt = productInformationMarketplace.ListUnitPrice > productInformationMarketplace.UnitPrice
                    ? productInformationMarketplace.ListUnitPrice
                    : 0,
                images = productInformationMarketplace
                    .ProductInformationPhotos
                    .Select(pip => new Image { url = pip.Url })
                    .ToArray(),
                tax_rate = Convert.ToInt32(100 * productInformationMarketplace.VatRate),
                stock_code = item.SellerCode
            };

            HttpHelperV3Response<CreateProductResponseV2> httpResultResponse = null;

            var colors = item
                .ProductInformationMarketplaces
                .Select(pim => pim
                    .ProductInformationMarketplaceVariantValues
                    .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renkler")
                    .MarketplaceVariantValueName)
                .Distinct()
                .ToList();

            foreach (var theColor in colors)
            {
                createProductRequestV2.variants = new();
                createProductRequestV2.attributes = new();

                var productInformationMarketplaces = item
                    .ProductInformationMarketplaces
                    .Where(pim => pim
                        .ProductInformationMarketplaceVariantValues
                        .FirstOrDefault(pimvv => pimvv
                            .MarketplaceVarinatName
                            .ToLower() == "renkler")
                        .MarketplaceVariantValueName == theColor)
                    .ToList();

                createProductRequestV2.product_name = productInformationMarketplace.ProductInformationName;

                foreach (var pim in productInformationMarketplaces)
                {
                    var variantAttribute = pim
                        .ProductInformationMarketplaceVariantValues
                        .First(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "beden");
                    createProductRequestV2.variants ??= new();
                    createProductRequestV2.variants.Add(new Variant
                    {
                        variant_id = pim.StockCode,
                        barcode = pim.Barcode,
                        quantity = pim.Stock,
                        attributes = new()
                        {
                            new()
                            {
                                attribute_id = int.Parse(variantAttribute.MarketplaceVarinatCode),
                                attribute_value_id = int.Parse(variantAttribute.MarketplaceVariantValueCode)
                            }
                        }
                    });

                    createProductRequestV2.attributes = pim
                        .ProductInformationMarketplaceVariantValues
                        .Where(pimvv => pimvv.MarketplaceVarinatName.ToLower() != "beden" &&
                                        pimvv.MarketplaceVarinatName.ToLower() != "menşei" &&
                                        pimvv.MarketplaceVarinatName.ToLower() != "sezon")
                        .GroupBy(pimvv => new
                        {
                            pimvv.MarketplaceVarinatCode,
                            pimvv.MarketplaceVariantValueCode
                        })
                        .Select(x => new Attribute1
                        {
                            attribute_id = int.Parse(x.Key.MarketplaceVarinatCode),
                            attribute_value_id = int.Parse(x.Key.MarketplaceVariantValueCode)
                        })
                        .ToList();
                }

                var requestUri = $"{_baseUrl}/update-product";
                var httpMethod = HttpMethod.Put;

                httpResultResponse = await this
                   ._httpHelperV3
                   .SendAsync<CreateProductRequestV2, CreateProductResponseV2>(
                        new HttpHelperV3Request<CreateProductRequestV2>
                        {
                            RequestUri = requestUri,
                            Request = createProductRequestV2,
                            HttpMethod = httpMethod,
                            AuthorizationType = AuthorizationType.Basic,
                            Authorization = modanisaConfigurationV2.Authorization
                        });
            }

            return null;

            //response.Success = httpResultResponse.Success;
            //response.HttpStatusCode = httpResultResponse.HttpStatusCode;
            //response.ContentString = httpResultResponse.ContentString;
            //response.Exception = httpResultResponse.Exception;
            //if (response.Success && response.HttpStatusCode == HttpStatusCode.OK)
            //    response.ContentObject = new()
            //    {
            //        TrackingId = httpResultResponse.ContentObject.data.request_id.ToString()
            //    };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<UpdateProductPriceRequestV2, UpdateProductPriceResponseV2>(
                    new HttpHelperV3Request<UpdateProductPriceRequestV2>
                    {
                        RequestUri = $"{_baseUrl}/updateProductPrice",
                        Request = new UpdateProductPriceRequestV2
                        {
                            Products = request.Items.Select(i => new Request.UpdateProductPrice.ProductV2
                            {
                                barcode = i.Barcode,
                                price = i.UnitPrice.ToString(".##"),
                                old_price = i.ListUnitPrice.ToString(".##")
                            }).ToList()
                        },
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.batchId
                    }
                    : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<UpdateProductStockRequestV2, UpdateProductStockResponseV2>(
                    new HttpHelperV3Request<UpdateProductStockRequestV2>
                    {
                        RequestUri = $"{_baseUrl}/updateProductStock",
                        Request = new UpdateProductStockRequestV2
                        {
                            Products = request.Items.Select(i => new Request.UpdateProductPrice.UpdateProductStock.ProductV2
                            {
                                barcode = i.Barcode,
                                quantity = i.Stock
                            }).ToList()
                        },
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == HttpStatusCode.OK
                    ? new TrackableResponse
                    {
                        TrackingId = httpResultResponse.Content.batchId
                    }
                    : null
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }

            foreach (var theRequest in requests)
            {
                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.Description.Contains("Kumaş İçeriği:") == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.Description.Contains("Kumaş İçeriği:") == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Kumaş İçeriği bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.Description.Contains("Kumaş İçeriği:") == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdateProduct> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> newRequests = new();

            requests
                .ForEach(theRequest =>
                {
                    List<MarketplaceRequestCreateProductItem> newItems = new();

                    theRequest.Items.ForEach(theItem =>
                    {
                        var colors = theItem
                            .ProductInformationMarketplaces
                            .Select(pim => pim
                                .ProductInformationMarketplaceVariantValues
                                .FirstOrDefault(pimvv =>
                                    pimvv
                                        .MarketplaceVarinatName.ToLower() == "renkler")
                                .MarketplaceVariantValueName)
                            .Distinct()
                            .ToList();

                        colors.ForEach(theColor =>
                        {
                            newRequests.Add(new MarketplaceRequestCreateProduct
                            {
                                Id = Guid.NewGuid(),
                                MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                                Path = theRequest.Path,
                                Items = new List<MarketplaceRequestCreateProductItem>
                                {
                                    new MarketplaceRequestCreateProductItem
                                    {
                                        BrandCode= theItem.BrandCode,
                                        BrandName= theItem.BrandName,
                                        CategoryCode= theItem.CategoryCode,
                                        CategoryName= theItem.CategoryName,
                                        DeliveryDay= theItem.DeliveryDay,
                                        DimensionalWeight= theItem.DimensionalWeight,
                                        ProductId= theItem.ProductId,
                                        ProductMarketplaceId= theItem.ProductMarketplaceId,
                                        ProductName= theItem.ProductName,
                                        SellerCode= theItem.SellerCode,
                                        ProductMarketplaceUUId = theItem.ProductMarketplaceUUId,
                                        ProductInformationMarketplaces = theItem
                                            .ProductInformationMarketplaces
                                                .Where(pim =>
                                                    pim
                                                        .ProductInformationMarketplaceVariantValues
                                                        .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renkler")
                                                        .MarketplaceVariantValueName == theColor)
                                                .ToList()
                                    }
                                }
                            });
                        });
                    });
                });

            return newRequests;
        }

        public List<MarketplaceRequestUpdateProduct> Split(List<MarketplaceRequestUpdateProduct> requests)
        {
            List<MarketplaceRequestUpdateProduct> marketplaceUpdateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    marketplaceUpdateProductRequests.AddRange(theRequest
                        .Items
                        .SelectMany(i => i
                            .ProductInformationMarketplaces
                            .GroupBy(pim => new
                            {
                                i.ProductId,
                                i.ProductMarketplaceId,
                                i.ProductName,
                                i.ProductMarketplaceUUId,
                                i.BrandId,
                                i.BrandName,
                                CategoryId = i.CategoryCode,
                                i.CategoryName,
                                i.DeliveryDay,
                                i.DimensionalWeight,
                                i.SellerCode,
                                pim.ProductInformationId,
                                pim
                                    .ProductInformationMarketplaceVariantValues
                                    .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renkler")
                                    .MarketplaceVariantValueName
                            })
                            .Select(r => new MarketplaceRequestUpdateProduct
                            {
                                Id = Guid.NewGuid(),
                                MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                                Items = new List<MarketplaceRequestUpdateProductItem>
                                {
                                    new MarketplaceRequestUpdateProductItem
                                    {
                                        BrandId = r.Key.BrandId,
                                        BrandName = r.Key.BrandName,
                                        CategoryCode = r.Key.CategoryId,
                                        CategoryName = r.Key.CategoryName,
                                        DeliveryDay = r.Key.DeliveryDay,
                                        DimensionalWeight = r.Key.DimensionalWeight,
                                        ProductId = r.Key.ProductId,
                                        ProductMarketplaceId = r.Key.ProductMarketplaceId,
                                        ProductMarketplaceUUId = r.Key.ProductMarketplaceUUId,
                                        ProductName = r.Key.ProductName,
                                        SellerCode = r.Key.SellerCode,
                                        ProductInformationMarketplaces = r.ToList()
                                    }
                                }
                            })));
                });

            return marketplaceUpdateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<Response.ProductCheck.ProductCheckResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/products/filter?request_id={request.TrackingId}",
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.success && !httpResultResponse.Content.data.products.Any(p => p.status != "CREATE_PRODUCT_SUCCESS" && p.status != "CREATE_PRODUCT_ERROR" && p.status != "CREATE_PRODUCT_REJECTED")
                };
            else
                return httpHelperV3Response;

            Console.WriteLine("OK" + Guid.NewGuid());

            if (httpHelperV3Response.Content.Done)
            {
                var success = httpHelperV3Response.Content.IncludeUUId = httpResultResponse.Content.data.products[0].status == "CREATE_PRODUCT_SUCCESS";

                httpHelperV3Response
                        .Content
                        .TrackingResponseItems = request
                            .Items
                            .SelectMany(i => i.ProductInformationMarketplaces.Select(pim =>
                            {
                                var trackingResponseItem = new TrackingResponseItem
                                {
                                    ProductId = i.ProductId,
                                    ProductMarketplaceId = i.ProductMarketplaceId,
                                    ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                                    Barcode = pim.Barcode,
                                    StockCode = pim.StockCode,
                                    Success = success
                                };

                                var variant = httpResultResponse.Content.data.products[0].variants.FirstOrDefault(v => v.barcode == pim.Barcode);
                                if (variant != null)
                                {
                                    trackingResponseItem.ProductInformationMarketplaceUUId = $"{httpResultResponse.Content.data.products[0].mdns_product_id}-{variant.mdns_variant_id}";
                                }

                                if (success == false)
                                    trackingResponseItem.Messages = httpResultResponse.Content.data.products[0].errors.warning;

                                return trackingResponseItem;
                            }))
                        .ToList();
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateProduct request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<ProductCheckRequest, Response.ProductCheck.ProductCheckResponse>(
                    new HttpHelperV3Request<ProductCheckRequest>
                    {
                        RequestUri = $"{_baseUrl}/marketplace/products/filter",
                        Request = new ProductCheckRequest
                        {
                            request_id = int.Parse(request.TrackingId)
                        },
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.success
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .SelectMany(i => i.ProductInformationMarketplaces.Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode
                    }));

                httpResultResponse
                    .Content
                    .data
                    .products
                    .ToList()
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.ProductId == int.Parse(i.product_id));
                        if (i.status == "CREATE_PRODUCT_SUCCESS")
                            trackingResponseItem.Success = true;
                        else
                            trackingResponseItem.Success = false;
                    });
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/marketplace/getBatchRequest?batchId={request.TrackingId}",
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>()
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new()
                {
                    Done = httpResultResponse.Content.Status == "complete" || httpResultResponse.Content.Status == "failed"
                };

            if (httpHelperV3Response.Content.Done)
            {
                httpHelperV3Response.Content.TrackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock,
                        Success = false,
                        ListUnitPrice = i.ListUnitPrice,
                        UnitPrice = i.UnitPrice
                    })
                    .ToList();

                if (httpResultResponse.Content.Status == "complete")
                    foreach (var theItem in httpResultResponse.Content.Items)
                    {
                        BatchResponseItem batchResponseItem = JsonConvert.DeserializeObject<BatchResponseItem>(theItem.Value.ToString());

                        var trackingResponseItem = httpHelperV3Response
                            .Content
                            .TrackingResponseItems
                            .FirstOrDefault(tri => tri.Barcode == batchResponseItem.Barcode);

                        if (trackingResponseItem != null)
                        {
                            trackingResponseItem.Success = batchResponseItem.Status == "complete";

                            if (trackingResponseItem.Success == false)
                                trackingResponseItem.Messages = batchResponseItem.Errors.ToList();
                        }
                    }
            }

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/marketplace/getBatchRequest?batchId={request.TrackingId}",
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>()
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new()
                {
                    Done = httpResultResponse.Content.Status == "complete" || httpResultResponse.Content.Status == "failed"
                };

            if (httpHelperV3Response.Content.Done)
            {
                httpHelperV3Response.Content.TrackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock,
                        Success = false
                    })
                    .ToList();

                if (httpResultResponse.Content.Status == "complete")
                    foreach (var theItem in httpResultResponse.Content.Items)
                    {
                        BatchResponseItem batchResponseItem = JsonConvert.DeserializeObject<BatchResponseItem>(theItem.Value.ToString());

                        var trackingResponseItem = httpHelperV3Response
                            .Content
                            .TrackingResponseItems
                            .FirstOrDefault(tri => tri.Barcode == batchResponseItem.Barcode);

                        if (trackingResponseItem != null)
                        {
                            trackingResponseItem.Success = batchResponseItem.Status == "complete";

                            if (trackingResponseItem.Success == false)
                                trackingResponseItem.Messages = batchResponseItem.Errors.ToList();
                        }
                    }
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            List<Category> categories = new();

            var getCategoriesResponse = await this
                ._httpHelperV3
                .SendAsync<GetCategoriesResponse>(new HttpHelperV3Request
                {
                    RequestUri = $"{_baseUrl}/categories",
                    Authorization = modanisaConfigurationV2.Authorization,
                    AuthorizationType = AuthorizationType.Basic,
                    HttpMethod = HttpMethod.Get
                });

            getCategoriesResponse.Content.data.categories.ForEach(theCategory =>
            {
                if (theCategory.sub_categories == null)
                    categories.Add(new Category
                    {
                        Code = theCategory.id.ToString(),
                        Name = theCategory.name
                    });
                else
                    FetchSubCategory(theCategory.sub_categories, categories);
            });

            categories.ForEach(async theCategory =>
            {
                #region Attributes
                var getAttributesResponse = await this
                    ._httpHelperV3
                    .SendAsync<GetAttributesResponse>(new HttpHelperV3Request
                    {
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Basic,
                        Authorization = modanisaConfigurationV2.Authorization,
                        RequestUri = $"{_baseUrl}/categories/id/{theCategory.Code}/attributes"
                    });

                getAttributesResponse.Content.data.category.attributes.RemoveAll(a =>
                    a.name.ToLower() == "sezon bilgisi" ||
                    a.name.ToLower() == "beden");

                theCategory.CategoryAttributes ??= new();
                theCategory.CategoryAttributes.AddRange(getAttributesResponse
                    .Content
                    .data
                    .category
                    .attributes
                    .Select(a => new CategoryAttribute
                    {
                        Code = a.id.ToString(),
                        Name = a.name,
                        Mandatory = a.mandatory == "true",
                        CategoryCode = theCategory.Code,
                        CategoryAttributesValues = a
                            .attribute_values
                            .Select(av => new CategoryAttributeValue
                            {
                                Code = av.id.ToString(),
                                Name = av.name,
                                VarinatCode = a.id.ToString()
                            })
                            .ToList()
                    }));
                #endregion

                #region Variant Attributes
                var getVariantAttributesResponse = await this
                    ._httpHelperV3
                    .SendAsync<GetVariantAttributesResponse>(new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/categories/id/{theCategory.Code}/variant-attributes",
                        Authorization = modanisaConfigurationV2.Authorization,
                        AuthorizationType = AuthorizationType.Basic,
                        HttpMethod = HttpMethod.Get
                    });

                theCategory.CategoryAttributes.AddRange(getVariantAttributesResponse
                    .Content
                    .data
                    .category
                    .variant_attributes
                    .Select(a => new CategoryAttribute
                    {
                        Code = $"VA_{theCategory.Code}_{a.id}",
                        Name = $"{theCategory.Name} {a.name}",
                        Mandatory = a.mandatory == "true",
                        CategoryCode = theCategory.Code,
                        CategoryAttributesValues = a
                            .attribute_values
                            .GroupBy(av => av.name)
                            .Select(av => new CategoryAttributeValue
                            {
                                Code = $"VA_{av.Max(av2 => av2.id)}",
                                Name = av.Key,
                                VarinatCode = $"VA_{theCategory.Code}_{a.id}"
                            })
                            .ToList()
                    }));
                #endregion
            });

            return categories;
        }

        public async Task<List<Order>> GetOrders(MarketplaceRequestGetOrder request)
        {
            ModanisaConfigurationV2 modanisaConfigurationV2 = this.Configuration(request);

            var details = new List<Detail>();

            var page = 1;
            var totalPage = 0;
            var url = $"{_baseUrl}/orderListV2?startDateTime={DateTime.Now.AddDays(-1):yyyy-MM-dd}T00:00:00&endDateTime={DateTime.Now:yyyy-MM-dd}T23:59:59&dateType=payment&size=100";

            do
            {
                var getOrderListResponse = await this._httpHelperV3.SendAsync<GetOrderListResponse>(new HttpHelperV3Request
                {
                    RequestUri = url + $"&page={page}",
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = modanisaConfigurationV2.Authorization
                });

                if (getOrderListResponse.Content.success)
                    details.AddRange(getOrderListResponse.Content.data.orders.details);
                else
                    break;

                if (totalPage == 0)
                    totalPage = getOrderListResponse.Content.data.pagination == null ? 1 : getOrderListResponse.Content.data.pagination.totalPage;

                page++;
            } while (page >= totalPage);

            details.ForEach(d => d.products.RemoveAll(p => p.status != "Pending"));

            return details.SelectMany(d => d.products.Select(p => new Order
            {
                CheckCustomer = true,
                BulkInvoicing = true,
                OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                MarketplaceId = Application.Common.Constants.Marketplaces.Modanisa,
                OrderCode = $"{d.orderId}-{p.productId}",
                OrderTypeId = "OS",
                CargoFee = 0,
                Discount = 0,
                ListTotalAmount = p.quantity * decimal.Parse(p.price),
                TotalAmount = p.quantity * decimal.Parse(p.price),
                OrderDate = DateTime.Parse(d.date),
                Customer = new Customer
                {
                    FirstName = "Modanisa",
                    LastName = "Modanisa",
                    Mail = "tedarik@modanisa.com",
                    TaxAdministration = String.Empty,
                    TaxNumber = String.Empty
                },
                OrderDeliveryAddress = new OrderDeliveryAddress()
                {
                    FirstName = "Modanisa",
                    LastName = "Modanisa",
                    Country = "Türkiye",
                    Email = "tedarik@modanisa.com",
                    Phone = "2164747473",
                    City = "İstanbul",
                    District = "Tuzla",
                    Neighborhood = "Tepeören",
                    Address = "Altunizade Mahallesi Kuşbakışı Caddesi No:27/1"
                },
                OrderInvoiceAddress = new OrderInvoiceAddress()
                {
                    FirstName = "Modanisa",
                    LastName = "Modanisa",
                    Email = "tedarik@modanisa.com",
                    Phone = "2164747473",
                    City = "İstanbul",
                    District = "Üsküdar",
                    Address = "Altunizade Mahallesi Kuşbakışı Caddesi No:27/1",
                    Neighborhood = "Altunizade",
                    TaxNumber = "6220586224",
                    TaxOffice = "Üsküdar"
                },
                OrderDetails = new List<OrderDetail>
                    {
                        new OrderDetail
                        {
                            ListPrice = decimal.Parse(p.price),
                            UnitPrice = decimal.Parse(p.price),
                            Payor = true,
                            Quantity = p.quantity,
                            UnitDiscount = 0,
                            Product = new Application.Common.Wrappers.MarketplacesV2.Orders.Product
                            {
                                Barcode = p.barcode,
                                Name = p.productName,
                                StockCode = String.Empty
                            }
                        }}
            }))
                .ToList();
        }
        #endregion

        #region Helper Methods
        private ModanisaConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private ModanisaConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }

        private bool TryFindFabric(string description, out Fabric[] result)
        {
            try
            {
                List<Fabric> returnValue = new();

                var startIndex = description.IndexOf("<b>Kumaş İçeriği:</b>") + 21;
                var endIndex = description.IndexOf("</div>", startIndex);
                var value = description.Substring(startIndex, endIndex - startIndex).Trim();
                var valueParts = value.Split("%", StringSplitOptions.RemoveEmptyEntries);
                foreach (var theValuePart in valueParts)
                {
                    var parts = theValuePart.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                    var name = parts[1];

                    if (name == "Viscon" || name == "Vıskon")
                        name = "Viskon";

                    if (name == "Polester" || name == "polyester" || name == "Pbt" || name == "Polyester,")
                        name = "Polyester";

                    if (name == "Mikrofiber")
                        name = "Mıkrofıber";

                    if (name == "Cotton" || name == "pamuk")
                        name = "Pamuk";

                    if (name == "Elestan")
                        name = "Elastan";

                    if (name == "Tencel")
                        name = "Tensel";

                    if (name == "Mohaır")
                        name = "Tiftik";

                    if (name == "Elit")
                        name = "Naylon";

                    if (name == "Viskos")
                        name = "Viskoz";

                    if (name == "Spandex" || name == "Spandeks")
                        name = "Likra";

                    if (name == "Tncl")
                        name = "Tensel";

                    if (name == "Akrililk")
                        name = "Akrilik";

                    if (name == "Mikrofibre")
                        name = "Mıkrofıber";

                    var fabric = fabrics.FirstOrDefault(f => f.name == name);

                    if (fabric == null)
                    {
                        result = null;

                        return false;
                    }

                    returnValue.Add(new Fabric
                    {
                        fabric_id = fabric.id,
                        percentage = int.Parse(parts[0])
                    });
                }

                result = returnValue.ToArray();

                return true;
            }
            catch (Exception e)
            {
                result = null;

                return false;
            }
        }

        private int FindSeasonId(string sellerCode)
        {
            var seasons = JsonConvert.DeserializeObject<List<Marketplaces.Modanisa.Responses.GetSeasons.Season>>(@"[
            {
                ""id"": 74,
                ""name"": ""Genel""
            },
            {
                ""id"": 75,
                ""name"": ""2012 Yaz""
            },
            {
                ""id"": 76,
                ""name"": ""Yeşil Topuklar""
            },
            {
                ""id"": 77,
                ""name"": ""Bayram Kıyafetleri""
            },
            {
                ""id"": 78,
                ""name"": ""2013 Kis""
            },
            {
                ""id"": 79,
                ""name"": ""2012 Sonbahar""
            },
            {
                ""id"": 80,
                ""name"": ""2013 İlkbahar - Yaz""
            },
            {
                ""id"": 81,
                ""name"": ""2014 Sonbahar - Kis""
            },
            {
                ""id"": 82,
                ""name"": ""2014 İlkbahar - Yaz""
            },
            {
                ""id"": 83,
                ""name"": ""2015 Sonbahar - Kış""
            },
            {
                ""id"": 84,
                ""name"": ""2015 Yaz""
            },
            {
                ""id"": 85,
                ""name"": ""Sezonsuz""
            },
            {
                ""id"": 86,
                ""name"": ""2016 Sonbahar - Kış""
            },
            {
                ""id"": 90,
                ""name"": ""2016 İlkbahar - Yaz""
            },
            {
                ""id"": 91,
                ""name"": ""2017 Sonbahar – Kış""
            },
            {
                ""id"": 92,
                ""name"": ""2017 İlkbahar – Yaz""
            },
            {
                ""id"": 93,
                ""name"": ""2017/18 Sonbahar - Kış""
            },
            {
                ""id"": 94,
                ""name"": ""2018 İlkbahar - Yaz""
            },
            {
                ""id"": 95,
                ""name"": ""2018/19 Sonbahar - Kış""
            },
            {
                ""id"": 96,
                ""name"": ""2019 İlkbahar - Yaz""
            },
            {
                ""id"": 97,
                ""name"": ""2019/20 Sonbahar - Kış""
            },
            {
                ""id"": 98,
                ""name"": ""2020 İlkbahar - Yaz""
            },
            {
                ""id"": 99,
                ""name"": ""SS20 Virman""
            },
            {
                ""id"": 100,
                ""name"": ""2020/21 Sonbahar - Kış""
            },
            {
                ""id"": 101,
                ""name"": ""2021 İlkbahar-Yaz""
            },
            {
                ""id"": 102,
                ""name"": ""2021/2022 Sonbahar-Kış""
            },
            {
                ""id"": 103,
                ""name"": ""2022 İlkbahar - Yaz""
            },
            {
                ""id"": 104,
                ""name"": ""2022/2023 Sonbahar-Kış""
            },
            {
                ""id"": 105,
                ""name"": ""2023 İlkbahar-Yaz""
            }
        ]");

            if (sellerCode.IndexOf("Y") > -1)
                return 105;
            else
                return 104;
        }

        private void FetchSubCategory(List<SubCategory> subCategories, List<Category> categories)
        {
            foreach (var theSubCategory in subCategories)
                if (theSubCategory.sub_categories != null)
                    FetchSubCategory(theSubCategory.sub_categories, categories);
                else
                    categories.Add(new Category
                    {
                        Code = theSubCategory.id.ToString(),
                        ParentCategoryCode = theSubCategory.parent_id.ToString(),
                        Name = theSubCategory.name
                    });
        }
        #endregion
    }
}