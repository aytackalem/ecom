﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using Helpy.Shared.Crypto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope
{
    public class TrendyolEuropeV2 : IMarketplaceV2, IMarketplaceV2StockPriceTrackable
    {
        #region Constants
        private const string _baseUrl = "https://api.trendyol.com/gpgw";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public TrendyolEuropeV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.TrendyolEurope;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            throw new System.NotImplementedException();
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            TrendyolEuropeConfigurationV2 trendyolEuropeConfigurationV2 = Configuration(request);

            Stock.Request stockRequest = new()
            {
                stocks = request.Items.Select(i => new Stock.Stock
                {
                    barcode = i.Barcode,
                    stock = i.Stock
                }).ToArray()
            };

            var requestUri = $"{_baseUrl}/v1/{trendyolEuropeConfigurationV2.SupplierId}/stocks";

            var httpResultResponse = await this._httpHelperV3.SendAsync<Stock.Request, Stock.Response>(
                new HttpHelperV3Request<Stock.Request>
                {
                    RequestUri = requestUri,
                    Request = stockRequest,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = trendyolEuropeConfigurationV2.Authorization
                });

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.batchId }
                    : null
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {

        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        #region Split
        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 1000M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 1000).Take(1000).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        #region Check
        public Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            throw new System.NotImplementedException();
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            TrendyolEuropeConfigurationV2 trendyolEuropeConfigurationV2 = Configuration(request);

            var requestUri = $"{_baseUrl}/v1/{trendyolEuropeConfigurationV2.SupplierId}/check-status?batchId={request.TrackingId}";

            var httpResultResponse = await this._httpHelperV3.SendAsync<StockBatch.Response>(
                new HttpHelperV3Request
                {
                    RequestUri = requestUri,
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = trendyolEuropeConfigurationV2.Authorization
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.status == "COMPLETED"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request
                    .Items
                    .Select(i => new TrackingResponseItem
                    {
                        ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                        Barcode = i.Barcode,
                        StockCode = i.StockCode,
                        Stock = i.Stock
                    }).ToList();

                httpResultResponse
                    .Content
                    .items
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.requestItem.stock.barcode);
                        if (i.status == "SUCCESS")
                            trackingResponseItem.Success = true;
                        else
                        {
                            trackingResponseItem.Success = false;
                            trackingResponseItem.Messages = i.failureReasons.Select(fr => fr.description).ToList();
                        }
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion
        #endregion

        #region Helper Methods
        private TrendyolEuropeConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private TrendyolEuropeConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }
        #endregion
    }
}
