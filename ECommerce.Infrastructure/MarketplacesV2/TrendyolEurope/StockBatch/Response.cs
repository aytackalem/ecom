﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope.StockBatch
{
    public class Response
    {
        public string batchId { get; set; }
        public string batchType { get; set; }
        public List<Item> items { get; set; }
        public string status { get; set; }
        public long creationDate { get; set; }
        public long lastModification { get; set; }
        public int itemCount { get; set; }
        public int successCount { get; set; }
        public int failedCount { get; set; }
        public long expirationDate { get; set; }
        public long completedDate { get; set; }
    }

    public class Item
    {
        public Requestitem requestItem { get; set; }
        public string status { get; set; }
        public Failurereason[] failureReasons { get; set; }
    }

    public class Requestitem
    {
        public Stock stock { get; set; }
    }

    public class Stock
    {
        public string barcode { get; set; }
        public int stock { get; set; }
    }

    public class Failurereason
    {
        public string description { get; set; }
        public string errorKey { get; set; }
    }

}
