﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope
{
    public class TrendyolEuropeConfigurationV2
    {
        #region Members
        public static implicit operator TrendyolEuropeConfigurationV2(Dictionary<string, string> configurations)
        {
            return new TrendyolEuropeConfigurationV2
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                SupplierId = configurations["SupplierId"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));

        public string SupplierId { get; set; }
        #endregion
    }
}
