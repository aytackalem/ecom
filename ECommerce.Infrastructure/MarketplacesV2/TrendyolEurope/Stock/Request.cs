﻿namespace ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope.Stock
{
    public class Request
    {
        public Stock[] stocks { get; set; }
    }

    public class Stock
    {
        public string barcode { get; set; }

        public int stock { get; set; }
    }

}
