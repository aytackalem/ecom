﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.SellerDelivery
{
    public class DataSellerDeliveryV2
    {
        public CargoCompanyV2 cargoCompany { get; set; }
        public object fastDelivery { get; set; }
        public object storeDelivery { get; set; }
        public object digital { get; set; }
        public object donation { get; set; }
    }
}
