﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.SellerDelivery
{
    public class GetSellerDeliveryV2
    {
        public DataSellerDeliveryV2 data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
        public bool fromCache { get; set; }
    }
}
