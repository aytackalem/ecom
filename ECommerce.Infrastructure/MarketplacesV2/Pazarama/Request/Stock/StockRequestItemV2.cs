﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Stock
{
    public class StockRequestItemV2
    {
        #region Properties
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("stockCount")]
        public int StockCount { get; set; }
        #endregion
    }
}
