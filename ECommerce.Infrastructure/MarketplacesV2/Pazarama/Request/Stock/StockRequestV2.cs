﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Stock
{
    public class StockRequestV2
    {
        #region Properties
        [JsonProperty("items")]
        public List<StockRequestItemV2> Items { get; set; }
        #endregion
    }
}
