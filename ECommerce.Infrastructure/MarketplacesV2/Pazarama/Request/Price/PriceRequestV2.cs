﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Price
{
    public class PriceRequestV2
    {
        #region Properties
        [JsonProperty("items")]
        public List<PriceRequestItemV2> Items { get; set; }
        #endregion
    }
}
