﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Price
{
    public class PriceRequestItemV2
    {
        #region Properties
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("listPrice")]
        public decimal ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal SalePrice { get; set; }
        #endregion
    }
}
