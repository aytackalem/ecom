﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.ProductCreate
{
    public class DeliveryV2
    {
        #region Properties
        public string deliveryId { get; set; }
        
        public List<string> cityList { get; set; } 
        #endregion
    }
}
