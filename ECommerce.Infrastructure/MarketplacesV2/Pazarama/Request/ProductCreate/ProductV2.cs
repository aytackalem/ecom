﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.ProductCreate
{
    public class ProductV2
    {
        #region Constructors
        public ProductV2()
        {
            #region Properties
            this.images = new List<ImageV2>();
            this.attributes = new List<AttributeV2>();
            this.deliveries = new List<DeliveryV2>();
            #endregion
        }
        #endregion

        #region Properties
        public string code { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public string description { get; set; }
        
        public string groupCode { get; set; }
        
        public string brandId { get; set; }
        
        public int desi { get; set; }
        
        public int stockCount { get; set; }
        
        public string stockCode { get; set; }
        
        public string currencyType { get; set; }
        
        public double listPrice { get; set; }
        
        public double salePrice { get; set; }
        
        public int vatRate { get; set; }

        public string categoryId { get; set; }
        #endregion

        #region Navigation Properties
        public List<ImageV2> images { get; set; }

        public List<AttributeV2> attributes { get; set; }
        
        public List<DeliveryV2> deliveries { get; set; }
        #endregion
    }
}
