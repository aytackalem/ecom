﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.ProductCreate
{
    public class ProductCreateRequestV2
    {
        #region Navigation Properties
        public List<ProductV2> products { get; set; } 
        #endregion
    }
}
