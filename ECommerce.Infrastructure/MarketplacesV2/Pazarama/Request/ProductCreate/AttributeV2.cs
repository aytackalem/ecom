﻿namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.ProductCreate
{
    public class AttributeV2
    {
        #region Properties
        public string attributeId { get; set; }
        
        public string attributeValueId { get; set; } 
        #endregion
    }
}
