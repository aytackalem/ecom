﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Price;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.SellerDelivery;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Request.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Categories;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.CategoryAttributes;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Price;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.ProductCreate;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Stock;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama
{
    public class PazaramaV2 : 
        IMarketplaceV2, 
        IMarketplaceV2CreateProductTrackable, 
        IMarketplaceV2StockPriceTrackable, 
        IMarketplaceV2CategoryGetable
    {
        #region Constants
        private const string _baseUrl = "https://isortagimapi.pazarama.com/";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public PazaramaV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Pazarama;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => true;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            PazaramaConfigurationV2 pazaramaConfigurationV2 = this.Configuration(request);

            var token = await this.GetToken(pazaramaConfigurationV2);

            var sellerDelivery = await this._httpHelperV3.SendAsync<GetSellerDeliveryV2>(
                new HttpHelperV3Request
                {
                    RequestUri = $"{_baseUrl}sellerRegister/getSellerDelivery",
                    HttpMethod = HttpMethod.Get,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token
                });

            var productCreateRequestV2 = new ProductCreateRequestV2
            {
                products = new List<ProductV2>()
            };

            foreach (var piLoop in request.Items)
            {
                foreach (var pinLoop in piLoop.ProductInformationMarketplaces)
                {
                    if (string.IsNullOrEmpty(pinLoop.Description)) pinLoop.Description = pinLoop.ProductInformationName;

                    var item = new ProductV2
                    {
                        code = pinLoop.Barcode,
                        name = pinLoop.ProductInformationName,
                        categoryId = piLoop.CategoryCode,
                        brandId = piLoop.BrandCode,
                        description = pinLoop.Description,
                        displayName = pinLoop.ProductInformationName,
                        groupCode = piLoop.SellerCode,
                        desi = Convert.ToInt32(piLoop.DimensionalWeight),
                        stockCount = pinLoop.Stock,
                        stockCode = pinLoop.StockCode,
                        currencyType = "TRY",
                        listPrice = Convert.ToDouble(pinLoop.ListUnitPrice),
                        salePrice = Convert.ToDouble(pinLoop.UnitPrice),
                        vatRate = Convert.ToInt32(pinLoop.VatRate * 100),
                        images = new List<ImageV2>(),
                        attributes = new List<AttributeV2>(),
                        deliveries = new List<DeliveryV2>()
                    };
                    item.deliveries.Add(new DeliveryV2
                    {
                        cityList = new List<string> { },
                        deliveryId = sellerDelivery.Content.data.cargoCompany.deliveryId
                    });

                    foreach (var image in pinLoop.ProductInformationPhotos)
                    {
                        item.images.Add(new ImageV2
                        {
                            imageurl = image.Url
                        });
                    }

                    foreach (var attribute in pinLoop.ProductInformationMarketplaceVariantValues)
                    {

                        item.attributes.Add(new AttributeV2
                        {
                            attributeId = attribute.MarketplaceVarinatCode,
                            attributeValueId = attribute.MarketplaceVariantValueCode
                        });

                    }
                    productCreateRequestV2.products.Add(item);

                }
            }

            productCreateRequestV2.products.RemoveAll(p => string.IsNullOrEmpty(p.brandId));

            var requestUri = $"{_baseUrl}product/create";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<ProductCreateRequestV2, ProductResponseV2>(
                    new HttpHelperV3Request<ProductCreateRequestV2>
                    {
                        RequestUri = requestUri,
                        Request = productCreateRequestV2,
                        HttpMethod = HttpMethod.Post,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = await this.GetToken(pazaramaConfigurationV2)
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpResultResponse.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.data.batchRequestId;

            if (httpResultResponse.HttpStatusCode == HttpStatusCode.BadRequest)
                if (httpResultResponse.ContentString.ToUpper().Contains("BRANDID"))
                    httpHelperV3Response.Content.Messages = new() {
                        "Lütfen marka eşleşmenizi kontrol ediniz."
                    };
                else
                    httpHelperV3Response.Content.Messages = new() {
                        "Lütfen ürün bilgilerini kontrol ediniz."
                    };

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            PazaramaConfigurationV2 pazaramaConfigurationV2 = request
                                .MarketplaceRequestHeader
                                .MarketplaceConfigurations
                                .Decrypt();

            var priceRequestV2 = new PriceRequestV2
            {
                Items = request
                    .Items
                    .Select(dpi => new PriceRequestItemV2
                    {
                        Code = dpi.Barcode,
                        ListPrice = dpi.ListUnitPrice,
                        SalePrice = dpi.UnitPrice
                    })
                    .ToList()
            };
            var requestUri = $"{_baseUrl}product/updatePrice";
            var token = await this.GetToken(pazaramaConfigurationV2);
            var httpResultResponse = await this._httpHelperV3.SendAsync<PriceRequestV2, PriceResponseV2>(
                new HttpHelperV3Request<PriceRequestV2>
                {
                    RequestUri = requestUri,
                    Request = priceRequestV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token
                });


            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
        ? new TrackableResponse { TrackingId = httpResultResponse.Content.data }
        : null
            };
        }

        public async Task<HttpHelperV3Response<TrackableResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            PazaramaConfigurationV2 pazaramaConfigurationV2 = request
                    .MarketplaceRequestHeader
                    .MarketplaceConfigurations
                    .Decrypt();

            var stockRequestV2 = new StockRequestV2
            {
                Items = request
                    .Items
                    .Select(dpi => new StockRequestItemV2
                    {
                        Code = dpi.Barcode,
                        StockCount = dpi.Stock
                    })
                    .ToList()
            };
            var requestUri = $"{_baseUrl}product/updateStock-v2";
            var token = await this.GetToken(pazaramaConfigurationV2);
            var httpResultResponse = await this._httpHelperV3.SendAsync<StockRequestV2, StockResponseV2>(
                new HttpHelperV3Request<StockRequestV2>
                {
                    RequestUri = requestUri,
                    Request = stockRequestV2,
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token
                });

            var httpHelperV3Response = new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = new()
            };

            if (httpResultResponse.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content.TrackingId = httpResultResponse.Content.data;

            if (httpResultResponse.HttpStatusCode == HttpStatusCode.BadRequest)
                httpHelperV3Response.Content.Messages = new() {
                        "Lütfen yapılan isteği kontrol ediniz."
                    };

            return new HttpHelperV3Response<TrackableResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode,
                Content = httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK
                    ? new TrackableResponse { TrackingId = httpResultResponse.Content.data }
                    : null
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                foreach (var theItem in theRequest.Items)
                {
                    #region Broken Photo Url
                    foreach (var productInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        foreach (var productInformationPhoto in productInformationMarketplace.ProductInformationPhotos)
                        {
                            if (FileChecker.CheckFileExistenceAsync(productInformationPhoto.Url).Result == false)
                            {
                                productInformationMarketplace.HasBrokenPhotoUrl = true;
                            }
                        }
                    }

                    if (theItem.ProductInformationMarketplaces.Any(_ => _.HasBrokenPhotoUrl))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(_ => _.HasBrokenPhotoUrl)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Ürün fotoğrafları içerisinde kırık link mevcut."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(_ => _.HasBrokenPhotoUrl);
                    }
                    #endregion
                }

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });

                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {
        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> marketplaceCreateProductRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 100M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceCreateProductRequests.Add(new MarketplaceRequestCreateProduct
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 100).Take(100).ToList()
                        });
                    }
                });

            return marketplaceCreateProductRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 500M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 500).Take(500).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 500M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 500).Take(500).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        #region Check
        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestCreateProduct request)
        {
            PazaramaConfigurationV2 pazaramaConfigurationV2 = request
            .MarketplaceRequestHeader
            .MarketplaceConfigurations
            .Decrypt();

            var requestUri = $"{_baseUrl}product/getProductBatchResult?BatchRequestId={request.TrackingId}";
            var httpResultResponse = await this
                ._httpHelperV3
                .SendAsync<Response.Batch.BatchResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = requestUri,
                        HttpMethod = HttpMethod.Get,
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = await this.GetToken(pazaramaConfigurationV2)
                    });

            var httpHelperV3Response = new HttpHelperV3Response<TrackingResponse>
            {
                HttpStatusCode = httpResultResponse.HttpStatusCode
            };

            if (httpHelperV3Response.HttpStatusCode == HttpStatusCode.OK)
                httpHelperV3Response.Content = new TrackingResponse
                {
                    Done = httpResultResponse.Content.messageCode == "BSK0"
                };
            else
                return httpHelperV3Response;

            if (httpHelperV3Response.Content.Done)
            {
                var trackingResponseItems = request.Items.SelectMany(i => i.ProductInformationMarketplaces.Select(pim => new TrackingResponseItem
                {
                    ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                    Barcode = pim.Barcode,
                    StockCode = pim.StockCode,
                    Success = true
                })).ToList();

                httpResultResponse
                    .Content
                    .data
                    .batchResult
                    .ToList()
                    .ForEach(i =>
                    {
                        var trackingResponseItem = trackingResponseItems.FirstOrDefault(tri => tri.Barcode == i.createProduct.code);
                        trackingResponseItem.Success = false;
                        trackingResponseItem.Messages = i.failureReasons.ToList();
                    });

                httpHelperV3Response.Content.TrackingResponseItems = trackingResponseItems;
            }

            return httpHelperV3Response;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            PazaramaConfigurationV2 pazaramaConfigurationV2 = request
                .MarketplaceRequestHeader
                .MarketplaceConfigurations
                .Decrypt();

            List<Category> categories = new();

            var categoryResponse = await this._httpHelperV3.SendAsync<CategoryResponse>(new HttpHelperV3Request
            {
                RequestUri = $"{_baseUrl}category/getCategoryTree"
            });

            categories.AddRange(categoryResponse.Content.data.Where(d => d.leaf).Select(d => new Category
            {
                Code = d.id,
                Name = d.name,
                ParentCategoryCode = d.parentId
            }));

            categories.ForEach(async theCategory =>
            {
                var categoryAttributeResponse = await this._httpHelperV3.SendAsync<CategoryAttributeResponse>(new HttpHelperV3Request
                {
                    RequestUri = $"{_baseUrl}category/getCategoryWithAttributes?Id={theCategory.Code}"
                });

                theCategory.CategoryAttributes.AddRange(categoryAttributeResponse.Content.data.attributes.Select(a => new Application.Common.Wrappers.MarketplacesV2.Categories.CategoryAttribute
                {
                    Code = a.id,
                    Name = a.name,
                    Mandatory = a.isRequired,
                    AllowCustom = false,
                    CategoryCode = theCategory.Code,
                    MultiValue = false,
                    CategoryAttributesValues = a.attributeValues.Select(av => new CategoryAttributeValue
                    {
                        Code = av.id,
                        Name = av.value,
                        VarinatCode = a.id
                    }).ToList()
                }));
            });

            return categories;
        }
        #endregion

        #region Helper Methods
        private PazaramaConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            PazaramaConfigurationV2 configuration = request
                .MarketplaceRequestHeader
                .MarketplaceConfigurations
                .Decrypt();

            return configuration;
        }

        private async Task<string> GetToken(PazaramaConfigurationV2 pazaramaConfigurationV2)
        {
            var requestUri = $"{_baseUrl}connect/token";
            var token = Convert.ToBase64String(
                Encoding.ASCII.GetBytes($"{pazaramaConfigurationV2.ApiKey}:{pazaramaConfigurationV2.SecretKey}"));
            var tokenResponse = await this._httpHelperV3.SendAsync<TokenResponse>(
                new HttpHelperV3FormUrlEncodedRequest
                {
                    RequestUri = requestUri,
                    Data = new List<KeyValuePair<string, string>> {
                        new KeyValuePair<string,string>( "grant_type", pazaramaConfigurationV2.GrantType ),
                        new KeyValuePair<string,string>( "scope", pazaramaConfigurationV2.Scope)
                    },
                    HttpMethod = HttpMethod.Post,
                    AuthorizationType = AuthorizationType.Basic,
                    Authorization = token
                });
            return tokenResponse.Content.data.accessToken;
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdatePrice request)
        {
            return new HttpHelperV3Response<TrackingResponse>();
        }

        public async Task<HttpHelperV3Response<TrackingResponse>> CheckAsync(MarketplaceRequestUpdateStock request)
        {
            return new HttpHelperV3Response<TrackingResponse>();
        }
        #endregion
    }
}