﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama
{
    public class PazaramaConfigurationV2
    {
        #region Members
        public static implicit operator PazaramaConfigurationV2(Dictionary<string, string> configurations)
        {
            return new PazaramaConfigurationV2
            {
                GrantType = "client_credentials",
                ApiKey = configurations["ApiKey"],
                MerchantId = configurations["MerchantId"],
                SecretKey = configurations["SecretKey"],
                Scope = "merchantgatewayapi.fullaccess"
            };
        }
        #endregion

        #region Properties
        public string GrantType { get; internal set; }

        public string ApiKey { get; internal set; }

        public string MerchantId { get; internal set; }

        public string SecretKey { get; internal set; }

        public string Scope { get; internal set; }
        #endregion
    }
}
