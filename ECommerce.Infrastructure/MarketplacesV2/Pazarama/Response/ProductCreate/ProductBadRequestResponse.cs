﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.ProductCreate
{
    public class ProductBadRequestResponse
    {
        public List<string> Errors { get; set; }
    }
}
