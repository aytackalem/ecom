﻿using System;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.ProductCreate
{
    public class ProductResponseV2
    {
        #region Properties
        public bool success { get; set; }
        
        public object messageCode { get; set; }
        
        public object message { get; set; }
        
        public object userMessage { get; set; }
        
        public bool fromCache { get; set; }
        #endregion

        #region Navigation Properties
        public ProductResponseDataV2 data { get; set; }
        #endregion        
    }


    public class ProductResponseDataV2
    {
        #region Properties
        public string batchRequestId { get; set; }

        public DateTime creationDate { get; set; }
        #endregion
    }
}
