﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Price
{

    public class PriceResponseV2
    {
        public string data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
        public bool fromCache { get; set; }
    }

    public class Datum
    {
        public string error { get; set; }
        public Item item { get; set; }
        public List<string> success { get; set; }
        public List<string> fail { get; set; }
    }

    public class Item
    {
        public string code { get; set; }
        public float listPrice { get; set; }
        public float salePrice { get; set; }
    }

}
