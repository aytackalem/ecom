﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Categories
{
    public class CategoryResponse
    {
        #region Navigation Properties
        public List<CategoryResponseItem> data { get; set; }
        #endregion
    }
}
