﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Stock
{
    public class StockResponseV2
    {
        #region Properties
        public string data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public string message { get; set; }
        public string userMessage { get; set; }
        public bool fromCache { get; set; }
        #endregion
    }

    public class Datum
    {
        public object error { get; set; }
        public object item { get; set; }
        public List<string> success { get; set; }
        public List<string> fail { get; set; }
        public object approve { get; set; }
    }
}
