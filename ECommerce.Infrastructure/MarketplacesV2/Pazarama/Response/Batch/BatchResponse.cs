﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.Batch
{

    public class BatchResponse
    {
        public Data data { get; set; }
        public bool success { get; set; }
        public string messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
        public bool fromCache { get; set; }
    }

    public class Data
    {
        public int status { get; set; }
        public string batchRequestId { get; set; }
        public Batchresult[] batchResult { get; set; }
        public int totalCount { get; set; }
        public int failedCount { get; set; }
        public DateTime creationDate { get; set; }
    }

    public class Batchresult
    {
        public Createproduct createProduct { get; set; }
        public string[] failureReasons { get; set; }
    }

    public class Createproduct
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string description { get; set; }
        public string brandId { get; set; }
        public int desi { get; set; }
        public string code { get; set; }
        public string groupCode { get; set; }
        public int stockCount { get; set; }
        public string stockCode { get; set; }
        public int priorityRank { get; set; }
        public int vatRate { get; set; }
        public float listPrice { get; set; }
        public float salePrice { get; set; }
        public int installmentCount { get; set; }
        public string categoryId { get; set; }
        public Attribute[] attributes { get; set; }
        public Image[] images { get; set; }
        public Delivery[] deliveries { get; set; }
    }

    public class Attribute
    {
        public string attributeId { get; set; }
        public string attributeValueId { get; set; }
    }

    public class Image
    {
        public string imageUrl { get; set; }
    }

    public class Delivery
    {
        public string deliveryId { get; set; }
        public int deliveryType { get; set; }
        public object cityList { get; set; }
    }




















}
