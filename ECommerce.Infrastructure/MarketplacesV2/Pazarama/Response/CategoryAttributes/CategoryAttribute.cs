﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.CategoryAttributes
{
    public class CategoryAttribute
    {
        #region Properties
        public string id { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public List<Attribute> attributes { get; set; } 
        #endregion
    }
}
