﻿namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.CategoryAttributes
{
    public class AttributeValue
    {
        #region Properties
        public string id { get; set; }
        
        public string value { get; set; } 
        #endregion
    }
}
