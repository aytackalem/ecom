﻿namespace ECommerce.Infrastructure.MarketplacesV2.Pazarama.Response.CategoryAttributes
{
    public class CategoryAttributeResponse
    {
        #region Properties
        public CategoryAttribute data { get; set; }
        
        public bool success { get; set; }
        
        public object messageCode { get; set; }
        
        public object message { get; set; }
        
        public object userMessage { get; set; } 
        #endregion
    }
}
