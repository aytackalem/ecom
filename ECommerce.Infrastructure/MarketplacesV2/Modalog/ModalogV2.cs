﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Application.Common.Wrappers.MarketplacesV2.Categories;
using ECommerce.Infrastructure.Marketplaces.Modalog.Requests.Token;
using ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Token;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.CreateProduct;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.UpdatePrice;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.UpdateStock;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.Product;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.UpdatePrice;
using ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.UpdateStock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog
{
    public class ModalogV2 : IMarketplaceV2, IMarketplaceV2CreateProductUntrackable, IMarketplaceV2StockPriceUntrackable, IMarketplaceV2CategoryGetable
    {
        #region Constants
        private const string _baseUrl = "https://merchant-api.modalog.com";
        #endregion

        #region Fields
        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public ModalogV2(IHttpHelperV3 httpHelperV3)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            #endregion
        }
        #endregion

        #region Properties
        public string Id => Application.Common.Constants.Marketplaces.Modalog;

        public int? WaitingSecondPerCreateProductRequest => null;

        public int? WaitingSecondPerUpdateProductRequest => null;

        public int? WaitingSecondPerStockRequest => null;

        public int? WaitingSecondPerPriceRequest => null;

        public bool IsUpsert => false;

        public bool StockPriceLoggable => false;
        #endregion

        #region Methods
        #region Send
        public async Task<HttpHelperV3Response<UntrackableCreateProductProcessResponse>> SendAsync(MarketplaceRequestCreateProduct request)
        {
            //request.MarketplaceRequestHeader.MarketplaceConfigurations = new Dictionary<string, string>
            //{
            //    { "JKLdu9NmquRihdjuR+FUnw==", "Rcdc1deccf7HIfwhYQuF/yMBxSC2kh5SIY3Rj7va1og=" },
            //    { "al3ooF9ZAdlv3Iv5kQP6Nw==", "mkbk2twOfrfI4F7bSJgD8g==" }
            //};

            ModalogConfigurationV2 modalogConfigurationV2 = Configuration(request);
            String token = await this.GetToken(modalogConfigurationV2);

            CreateProductRequestV2 createProductRequestV2 = new()
            {
                Products = request
                    .Items
                    .Select(theItem => new CreateProductRequestItem
                    {
                        BrandId = int.Parse(theItem.BrandCode),
                        CategoryId = int.Parse(theItem.CategoryCode),
                        Name = theItem.ProductName,
                        Description = theItem.ProductInformationMarketplaces[0].Description,
                        Images = theItem
                            .ProductInformationMarketplaces
                            .First()
                            .ProductInformationPhotos.Select(pip => new Image { url = pip.Url })
                            .ToList(),
                        Sku = theItem.SellerCode,
                        Vat = Convert.ToInt32(theItem.ProductInformationMarketplaces[0].VatRate * 100),
                        Variants = theItem.ProductInformationMarketplaces.Select(pim => new Variant
                        {
                            VariantValueId = int.Parse(pim
                                .ProductInformationMarketplaceVariantValues
                                .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "beden")
                                .MarketplaceVariantValueCode),
                            Barcode = pim.Barcode,
                            StockCode = pim.StockCode,
                            ListPrice = pim.ListUnitPrice,
                            SalePrice = pim.UnitPrice,
                            Stock = pim.Stock
                        }).ToList(),
                        Attributes = theItem
                            .ProductInformationMarketplaces
                            .First()
                            .ProductInformationMarketplaceVariantValues
                                .Where(pimvv => pimvv.MarketplaceVarinatName.ToLower() != "beden")
                                .Select(pimvv => new Request.CreateProduct.Attribute
                                {
                                    AttributeId = int.Parse(pimvv.MarketplaceVarinatCode),
                                    AttributeValueId = pimvv.AllowCustom ? 0 : int.Parse(pimvv.MarketplaceVariantValueCode),
                                    CustomValue = pimvv.MarketplaceVariantValueName
                                })
                                .ToList()
                    })
                    .ToList()
            };

            var createProductResponse = await this
                ._httpHelperV3
                .SendAsync<List<CreateProductRequestItem>, CreateProductResponse>(new HttpHelperV3Request<List<CreateProductRequestItem>>
                {
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token,
                    RequestUri = $"{_baseUrl}/Products",
                    Request = createProductRequestV2.Products,
                    HttpMethod = HttpMethod.Post
                });

            var httpHelperV3Response = new HttpHelperV3Response<UntrackableCreateProductProcessResponse>
            {
                HttpStatusCode = createProductResponse.HttpStatusCode,
                Content = new UntrackableCreateProductProcessResponse
                {
                    IncludeUrl = false,
                    UntrackableProcessResponseItems = request
                        .Items[0]
                        .ProductInformationMarketplaces
                        .Select(pim =>
                        {
                            var item = new UntrackableCreateProductProcessResponseItem
                            {
                                ProductInformationMarketplaceId = pim.ProductInformationMarketplaceId,
                                Success = createProductResponse.Content.data[0].status
                            };

                            return item;
                        })
                        .ToList()
                }
            };

            return httpHelperV3Response;
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdatePrice request)
        {
            ModalogConfigurationV2 modalogConfigurationV2 = Configuration(request);
            String token = await this.GetToken(modalogConfigurationV2);

            HttpHelperV3Response<UpdatePriceResponse> updatePriceResponse = await this
                ._httpHelperV3
                .SendAsync<List<UpdatePriceRequest>, UpdatePriceResponse>(new HttpHelperV3Request<List<UpdatePriceRequest>>
                {
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token,
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{_baseUrl}/ProductPriceUpdate",
                    Request = request
                        .Items
                        .Select(theItem => new UpdatePriceRequest
                        {
                            barcode = theItem.Barcode,
                            listPrice = theItem.ListUnitPrice,
                            salePrice = theItem.UnitPrice
                        })
                        .ToList()
                });

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = updatePriceResponse.HttpStatusCode,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i => new UntrackableProcessResponseItem
                        {
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            Success = updatePriceResponse.Content.data.FirstOrDefault(d => d.barcode == i.Barcode).success
                        }).ToList()
                }
            };
        }

        public async Task<HttpHelperV3Response<UntrackableProcessResponse>> SendAsync(MarketplaceRequestUpdateStock request)
        {
            ModalogConfigurationV2 modalogConfigurationV2 = Configuration(request);
            String token = await this.GetToken(modalogConfigurationV2);

            HttpHelperV3Response<UpdateStockResponse> updatePriceResponse = await this
                ._httpHelperV3
                .SendAsync<List<UpdateStockRequest>, UpdateStockResponse>(new HttpHelperV3Request<List<UpdateStockRequest>>
                {
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = token,
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{_baseUrl}/ProductStockUpdate",
                    Request = request
                        .Items
                        .Select(theItem => new UpdateStockRequest
                        {
                            barcode = theItem.Barcode,
                            stock = theItem.Stock
                        })
                        .ToList()
                });

            return new HttpHelperV3Response<UntrackableProcessResponse>
            {
                HttpStatusCode = updatePriceResponse.HttpStatusCode,
                Content = new UntrackableProcessResponse
                {
                    UntrackableProcessResponseItems = request
                        .Items
                        .Select(i =>
                        {
                            var item = new UntrackableProcessResponseItem
                            {
                                ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                                Success = updatePriceResponse.Content.data.FirstOrDefault(d => d.barcode == i.Barcode).success
                            };

                            return item;
                        })
                        .ToList()
                }
            };
        }
        #endregion

        #region Validate
        public void Validate(List<MarketplaceRequestCreateProduct> requests)
        {
            foreach (var theRequest in requests)
            {
                //if (theRequest.Items.Any(i => string.IsNullOrEmpty(i.BrandId)))
                //{
                //    var invalidItems = theRequest.Items.Where(i => string.IsNullOrEmpty(i.BrandId)).ToList();
                //    invalidItems.ForEach(i => i.ErrorMessage = "Marka bilgisi bulunamadı.");
                //    theRequest.InvalidItems ??= new();
                //    theRequest.InvalidItems.AddRange(invalidItems);
                //    theRequest.Items.RemoveAll(i => string.IsNullOrEmpty(i.BrandId));
                //}

                theRequest.Items.ForEach(theItem =>
                {
                    if (theItem.ProductInformationMarketplaces.Any(pim => pim.ProductInformationPhotos.HasItem() == false))
                    {
                        var invalidProductInformationMarketplaces = theItem
                            .ProductInformationMarketplaces
                            .Where(pim => pim.ProductInformationPhotos.HasItem() == false)
                            .ToList();

                        theRequest.InvalidItems ??= new();
                        theRequest.InvalidItems.Add(new MarketplaceRequestCreateProductItem
                        {
                            ProductInformationMarketplaces = invalidProductInformationMarketplaces,
                            ErrorMessage = "Fotoğraf bulunamadı."
                        });

                        theItem.ProductInformationMarketplaces.RemoveAll(pim => pim.ProductInformationPhotos.HasItem() == false);
                    }
                });
                theRequest.Items.RemoveAll(i => i.ProductInformationMarketplaces.HasItem() == false);
            }
        }

        public void Validate(List<MarketplaceRequestUpdatePrice> requests)
        {
        }

        public void Validate(List<MarketplaceRequestUpdateStock> requests)
        {
        }
        #endregion

        #region Split
        public List<MarketplaceRequestCreateProduct> Split(List<MarketplaceRequestCreateProduct> requests)
        {
            List<MarketplaceRequestCreateProduct> newRequests = new();

            requests
                .ForEach(theRequest =>
                {
                    List<MarketplaceRequestCreateProductItem> newItems = new();

                    theRequest.Items.ForEach(theItem =>
                    {
                        var size = theItem
                            .ProductInformationMarketplaces
                            .Select(pim => pim
                                .ProductInformationMarketplaceVariantValues
                                .FirstOrDefault(pimvv =>
                                    pimvv
                                        .MarketplaceVarinatName.ToLower() == "renk")
                                .MarketplaceVariantValueName)
                            .Distinct()
                            .ToList();

                        size.ForEach(theSize =>
                        {
                            newRequests.Add(new MarketplaceRequestCreateProduct
                            {
                                Id = Guid.NewGuid(),
                                MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                                Path = theRequest.Path,
                                Items = new List<MarketplaceRequestCreateProductItem>
                                {
                                    new MarketplaceRequestCreateProductItem
                                    {
                                        BrandCode= theItem.BrandCode,
                                        BrandName= theItem.BrandName,
                                        CategoryCode= theItem.CategoryCode,
                                        CategoryName= theItem.CategoryName,
                                        DeliveryDay= theItem.DeliveryDay,
                                        DimensionalWeight= theItem.DimensionalWeight,
                                        ProductId= theItem.ProductId,
                                        ProductMarketplaceId= theItem.ProductMarketplaceId,
                                        ProductName= theItem.ProductName,
                                        SellerCode= theItem.SellerCode,
                                        ProductMarketplaceUUId = theItem.ProductMarketplaceUUId,
                                        ProductInformationMarketplaces = theItem
                                            .ProductInformationMarketplaces
                                                .Where(pim =>
                                                    pim
                                                        .ProductInformationMarketplaceVariantValues
                                                        .FirstOrDefault(pimvv => pimvv.MarketplaceVarinatName.ToLower() == "renk")
                                                        .MarketplaceVariantValueName == theSize)
                                                .ToList()
                                    }
                                }
                            });
                        });
                    });
                });

            return newRequests;
        }

        public List<MarketplaceRequestUpdateStock> Split(List<MarketplaceRequestUpdateStock> requests)
        {
            List<MarketplaceRequestUpdateStock> marketplaceStockRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplaceStockRequests.Add(new MarketplaceRequestUpdateStock
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplaceStockRequests;
        }

        public List<MarketplaceRequestUpdatePrice> Split(List<MarketplaceRequestUpdatePrice> requests)
        {
            List<MarketplaceRequestUpdatePrice> marketplacePriceRequests = new();
            requests
                .ForEach(theRequest =>
                {
                    var requestCount = (int)Math.Ceiling(theRequest.Items.Count / 50M);
                    for (int i = 0; i < requestCount; i++)
                    {
                        marketplacePriceRequests.Add(new MarketplaceRequestUpdatePrice
                        {
                            Id = Guid.NewGuid(),
                            MarketplaceRequestHeader = theRequest.MarketplaceRequestHeader,
                            Items = theRequest.Items.Skip(i * 50).Take(50).ToList()
                        });
                    }
                });

            return marketplacePriceRequests;
        }
        #endregion

        public async Task<List<Category>> GetCategories(MarketplaceRequestGetCategory request)
        {
            ModalogConfigurationV2 modalogConfigurationV2 = Configuration(request);

            String token = await this.GetToken(modalogConfigurationV2);

            List<Category> categories = new();

            #region Fetch Categories
            var modalogCategories = new List<Response.Category.Category>();
            var categoryPageCount = 0;
            var categoryPage = 0;
            do
            {
                var categoryResponse = await this._httpHelperV3.SendAsync<Response.Category.CategoryResponse>(
                    new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/Categories",
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token,
                        HttpMethod = HttpMethod.Get
                    });

                if (categoryPageCount == 0)
                    categoryPageCount = categoryResponse.Content.pageCount;

                modalogCategories.AddRange(categoryResponse.Content.data);
            } while (categoryPageCount < categoryPage);

            foreach (var theModalogCategory in modalogCategories)
            {
                if (theModalogCategory.subCategories == null)
                    categories.Add(new Category
                    {
                        Code = theModalogCategory.id.ToString(),
                        Name = theModalogCategory.name
                    });
                else
                    FetchSubCategory(theModalogCategory.subCategories, categories);
            }
            #endregion

            #region Fetch Variants
            var variantValues = new List<Response.Variant.VariantValueResponseItem>();
            var variantValuePageCount = 0;
            var variantValuePage = 1;
            do
            {
                var variantValueResponse = await this
                    ._httpHelperV3
                    .SendAsync<Response.Variant.VariantValueResponse>(
                        new HttpHelperV3Request
                        {
                            RequestUri = $"{_baseUrl}/Variants?pageIndex={variantValuePage}",
                            AuthorizationType = AuthorizationType.Bearer,
                            Authorization = token,
                            HttpMethod = HttpMethod.Get
                        });

                if (variantValuePageCount == 0)
                    variantValuePageCount = variantValueResponse.Content.pageCount;

                variantValues.AddRange(variantValueResponse.Content.data);

                variantValuePage++;
            } while (variantValuePageCount >= variantValuePage);
            #endregion

            #region Fetch Attributes
            var attributes = new List<Response.Attribute.AttributeResponseItem>();
            var attributePageCount = 0;
            var attributePage = 1;
            do
            {
                var attributeResponse = await this
                    ._httpHelperV3
                    .SendAsync<Response.Attribute.AttributeResponse>(
                        new HttpHelperV3Request
                        {
                            RequestUri = $"{_baseUrl}/Attributes?pageIndex={attributePage}",
                            AuthorizationType = AuthorizationType.Bearer,
                            Authorization = token,
                            HttpMethod = HttpMethod.Get
                        });

                if (attributePageCount == 0)
                    attributePageCount = attributeResponse.Content.pageCount;

                attributes.AddRange(attributeResponse.Content.data);

                attributePage++;
            } while (attributePageCount >= attributePage);

            foreach (var theAttribute in attributes)
            {
                #region Fetch Attribute Values
                var attributeValues = new List<Response.AttributeValue.AttributeValueResponseItem>();
                var attributeValuePageCount = 0;
                var attributeValuePage = 1;
                do
                {
                    var attributeValueResponse = await this
                        ._httpHelperV3
                        .SendAsync<Response.AttributeValue.AttributeValueResponse>(
                            new HttpHelperV3Request
                            {
                                RequestUri = $"{_baseUrl}/Attributes/{theAttribute.id}/Values?pageIndex={attributeValuePage}",
                                AuthorizationType = AuthorizationType.Bearer,
                                Authorization = token,
                                HttpMethod = HttpMethod.Get
                            });

                    if (attributeValuePageCount == 0)
                        attributeValuePageCount = attributeValueResponse.Content.pageCount;

                    attributeValues.AddRange(attributeValueResponse.Content.data);

                    attributeValuePage++;
                } while (attributeValuePageCount >= attributeValuePage);
                #endregion

                theAttribute.Values = attributeValues;
            }
            #endregion

            var variantId = Guid.NewGuid().ToString();

            foreach (var theCategory in categories)
            {
                #region Add Variants As Category Attribute Values
                theCategory.CategoryAttributes.Add(new CategoryAttribute
                {
                    Code = variantId,
                    Name = "Beden",
                    Variantable = true,
                    CategoryCode = theCategory.Code,
                    CategoryAttributesValues = variantValues.Select(vv => new CategoryAttributeValue
                    {
                        Code = vv.id.ToString(),
                        Name = vv.name,
                        VarinatCode = variantId
                    }).ToList()
                });
                #endregion

                #region Fetch Category Attributes
                var categoryAttributes = new List<Response.CategoryAttribute.CategoryAttribute>();
                var categoryAttributePageCount = 0;
                var categoryAttributePage = 0;
                do
                {
                    var categoryAttributeResponse = await this._httpHelperV3.SendAsync<Response.CategoryAttribute.CategoryAttributeResponse>(new HttpHelperV3Request
                    {
                        RequestUri = $"{_baseUrl}/Categories/{theCategory.Code}/Attributes",
                        AuthorizationType = AuthorizationType.Bearer,
                        Authorization = token,
                        HttpMethod = HttpMethod.Get
                    });

                    if (categoryAttributePageCount == 0)
                        categoryAttributePageCount = categoryAttributeResponse.Content.pageCount;

                    categoryAttributes.AddRange(categoryAttributeResponse.Content.data);

                    categoryAttributePage++;
                } while (categoryAttributePageCount > categoryAttributePage);
                #endregion

                theCategory
                    .CategoryAttributes
                    .AddRange(attributes
                        .Where(a => categoryAttributes.Select(ca => ca.attributeId).Contains(a.id))
                        .Select(a => new CategoryAttribute
                        {
                            AllowCustom = a.isCustom,
                            Code = a.id.ToString(),
                            Name = a.name,
                            CategoryCode = theCategory.Code,
                            CategoryAttributesValues = a.Values.Select(v => new CategoryAttributeValue
                            {
                                Code = v.id.ToString(),
                                Name = v.name,
                                VarinatCode = v.attributeId.ToString()
                            }).ToList()
                        }));
            }

            return categories;
        }
        #endregion

        #region Helper Methods
        private ModalogConfigurationV2 Configuration(IMarketplaceRequest request)
        {
            return this.Configuration(request.MarketplaceRequestHeader.MarketplaceConfigurations);
        }

        private ModalogConfigurationV2 Configuration(Dictionary<string, string> marketplaceConfigurations)
        {
            return marketplaceConfigurations.Decrypt();
        }

        private async Task<string> GetToken(ModalogConfigurationV2 modalogConfigurationV2)
        {
            var tokenResponse = await this._httpHelperV3.SendAsync<TokenRequest, TokenResponse>(new HttpHelperV3Request<TokenRequest>
            {
                RequestUri = $"{_baseUrl}/Auth/Login",
                Request = new TokenRequest
                {
                    Email = modalogConfigurationV2.Email,
                    Password = modalogConfigurationV2.Password
                },
                HttpMethod = HttpMethod.Post
            });
            return tokenResponse.Content.data.token;
        }

        private void FetchSubCategory(List<Response.Category.SubCategory> subCategories, List<Category> categories)
        {
            foreach (var theSubCategory in subCategories)
            {
                if (theSubCategory.subCategories.HasItem())
                    FetchSubCategory(theSubCategory.subCategories, categories);
                else
                {
                    categories.Add(new Category
                    {
                        Code = theSubCategory.id.ToString(),
                        ParentCategoryCode = theSubCategory.parentId.ToString(),
                        Name = theSubCategory.name
                    });
                }
            }
        }
        #endregion
    }
}