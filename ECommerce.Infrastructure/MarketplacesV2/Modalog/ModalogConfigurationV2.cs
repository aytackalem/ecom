﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog
{
    public class ModalogConfigurationV2
    {
        #region Members
        public static implicit operator ModalogConfigurationV2(Dictionary<string, string> configurations)
        {
            return new ModalogConfigurationV2
            {
                Email = configurations["Email"],
                Password = configurations["Password"]
            };
        }
        #endregion

        #region Properties
        public string Email { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
