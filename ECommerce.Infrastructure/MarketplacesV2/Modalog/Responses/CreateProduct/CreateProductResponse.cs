﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.Product
{
    public class CreateProductResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public CreateProductResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class CreateProductResponseItem
    {
        public string sku { get; set; }
        public bool status { get; set; }
        public string message { get; set; }
    }

}
