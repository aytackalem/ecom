﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.UpdateStock
{
    public class UpdateStockResponse
    {
        public bool success { get; set; }
        public object message { get; set; }
        public UpdateStockResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class UpdateStockResponseItem
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string barcode { get; set; }
    }

}
