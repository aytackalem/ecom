﻿using System;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.Token
{
    public class TokenResponseData
    {
        #region Properties
        public string token { get; set; }

        public DateTime expiration { get; set; }
        #endregion
    }
}
