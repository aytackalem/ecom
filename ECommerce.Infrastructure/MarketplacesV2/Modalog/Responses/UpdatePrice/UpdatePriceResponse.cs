﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Response.UpdatePrice
{
    public class UpdatePriceResponse
    {
        public bool success { get; set; }
        public object message { get; set; }
        public UpdatePriceResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class UpdatePriceResponseItem
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string barcode { get; set; }
    }

}
