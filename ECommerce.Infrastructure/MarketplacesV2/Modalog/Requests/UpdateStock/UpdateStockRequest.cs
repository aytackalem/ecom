﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.UpdateStock
{
    public class UpdateStockRequest
    {
        public string barcode { get; set; }
        public int stock { get; set; }
    }
}
