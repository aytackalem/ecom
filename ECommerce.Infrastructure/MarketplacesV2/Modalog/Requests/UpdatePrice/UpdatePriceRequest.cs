﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.UpdatePrice
{
    public class UpdatePriceRequest
    {
        public string barcode { get; set; }
        public decimal listPrice { get; set; }
        public decimal salePrice { get; set; }
    }

}
