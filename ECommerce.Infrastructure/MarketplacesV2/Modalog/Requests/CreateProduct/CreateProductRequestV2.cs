﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.CreateProduct
{
    public class CreateProductRequestV2
    {
        public List<CreateProductRequestItem> Products { get; set; }
    }

    public class CreateProductRequestItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public int Vat { get; set; }
        public List<Image> Images { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<Variant> Variants { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
    }

    public class Attribute
    {
        public int AttributeId { get; set; }
        public int AttributeValueId { get; set; }
        public string CustomValue { get; set; }
    }

    public class Variant
    {
        public int VariantValueId { get; set; }
        public string StockCode { get; set; }
        public decimal ListPrice { get; set; }
        public decimal SalePrice { get; set; }
        public int Stock { get; set; }
        public string Barcode { get; set; }
        public int Desi { get; set; }
        public float Weight { get; set; }
        public float Width { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }
        public int ShipmentDuration { get; set; }
        public int ShipmentAddressId { get; set; }
        public int ReturningAddressId { get; set; }
        public int ShipmentProviderId { get; set; }
    }

}
