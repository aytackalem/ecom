﻿namespace ECommerce.Infrastructure.MarketplacesV2.Modalog.Request.Token
{
    public class TokenRequest
    {
        #region Properties
        public string Email { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
