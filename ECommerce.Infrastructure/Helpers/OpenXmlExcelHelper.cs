﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ECommerce.Infrastructure.Helpers
{
    public class OpenXmlExcelHelper : IExcelHelper
    {
        #region Methods
        public Application.Common.DataTransferObjects.Excel Read(Stream stream)
        {
            Application.Common.DataTransferObjects.Excel excel = new() { Header = new() { Cells = new() }, Rows = new() };

            using (SpreadsheetDocument document = SpreadsheetDocument.Open(stream, false))
            {
                WorkbookPart workbookPart = document.WorkbookPart;
                SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                if (sstpart != null)
                {
                    SharedStringTable sst = sstpart.SharedStringTable;

                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                    Worksheet sheet = worksheetPart.Worksheet;
                    var rows = sheet.Descendants<Row>();
                    bool header = true;
                    foreach (Row rLoop in rows)
                    {
                        if (header)
                        {
                            foreach (Cell c in rLoop.Elements<Cell>())
                            {
                                if ((c.DataType != null) && (c.DataType == CellValues.SharedString))
                                {
                                    int ssid = int.Parse(c.CellValue.Text);
                                    string str = sst.ChildElements[ssid].InnerText;

                                    excel.Header.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = str
                                    });
                                }
                                else if (c.CellValue != null)
                                    excel.Header.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = c.CellValue.Text
                                    });
                                else
                                    excel.Header.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = ""
                                    });
                            }

                            header = !header;
                        }
                        else
                        {
                            Application.Common.DataTransferObjects.Row row = new() { Cells = new() };
                            foreach (Cell cell in rLoop.Descendants<Cell>())
                            {
                                if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
                                {
                                    int ssid = int.Parse(cell.CellValue.Text);
                                    string str = sst.ChildElements[ssid].InnerText;

                                    row.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = str
                                    });
                                }
                                else if (cell.CellValue != null)
                                    row.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = cell.CellValue.Text
                                    });
                                else
                                    row.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                    {
                                        Value = ""
                                    });
                            }
                            excel.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    Sheet sheet = workbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                    Worksheet worksheet = (workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                    IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                    bool header = true;
                    foreach (Row rLoop in rows)
                    {
                        if (header)
                        {
                            foreach (Cell cell in rLoop.Descendants<Cell>())
                            {
                                excel.Header.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                {
                                    Value = cell.CellValue?.Text
                                });
                            }

                            header = !header;
                        }
                        else
                        {
                            Application.Common.DataTransferObjects.Row row = new() { Cells = new() };
                            foreach (Cell cell in rLoop.Descendants<Cell>())
                            {
                                row.Cells.Add(new Application.Common.DataTransferObjects.Cell
                                {
                                    Value = cell.CellValue?.Text
                                });
                            }
                            excel.Rows.Add(row);
                        }
                    }
                }
            }

            return excel;
        }

        public MemoryStream Write(Application.Common.DataTransferObjects.Excel excel)
        {
            MemoryStream memoryStream = new();
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                sheets.Append(sheet);

                var mergeCells = new MergeCells();

                int rowIndex = 1;
                this.Write(sheetData, excel.Header.Cells, rowIndex);
                excel.Rows.ForEach(rLoop =>
                {
                    rowIndex += 1;
                    this.Write(sheetData, rLoop.Cells, rowIndex);

                    rLoop.Cells.ForEach(theCell =>
                    {
                        if (theCell.Merge)
                        {
                            var mergeCell = new MergeCell
                            {
                                Reference = new StringValue($"{theCell.ColumnName}{rowIndex}:{theCell.ColumnName}{rowIndex + theCell.RowCount - 1}")
                            };
                            mergeCells.Append(mergeCell);
                        }
                    });
                });

                if (mergeCells.ChildElements?.Count > 0)
                    worksheetPart.Worksheet.InsertAfter(mergeCells, sheetData);

                workbookPart.Workbook.Save();
            }
            return memoryStream;
        }
        #endregion

        #region Private Methods
        private void Write(SheetData sheetData, List<Application.Common.DataTransferObjects.Cell> cells, int rowIndex)
        {
            Row row = new();
            foreach (var cLoop in cells)
            {
                Cell cell = new();

                if (string.IsNullOrEmpty(cLoop.Formula))
                {
                    cell.CellValue = new CellValue(cLoop.Value == null ? string.Empty : cLoop.Value.ToString());
                }
                else
                    cell.CellFormula = new(cLoop.Formula.Replace("{i}", rowIndex.ToString()));

                cell.DataType = cLoop.CellType switch
                {
                    Application.Common.DataTransferObjects.CellType.Number => (EnumValue<CellValues>)CellValues.Number,
                    Application.Common.DataTransferObjects.CellType.String => (EnumValue<CellValues>)CellValues.String,
                    _ => (EnumValue<CellValues>)CellValues.String,
                };
                row.AppendChild(cell);
            }
            sheetData.AppendChild(row);
        }
        #endregion
    }
}
