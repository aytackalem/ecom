﻿using System.Net.Http;
using System.Threading.Tasks;
using System;

namespace ECommerce.Infrastructure.Helpers;

public static class FileChecker
{
    public static async Task<bool> CheckFileExistenceAsync(string fileUrl)
    {
        try
        {
            using HttpClient client = new();
            HttpResponseMessage response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, fileUrl));
            return response.IsSuccessStatusCode;
        }
        catch (Exception)
        {
            return false;
        }
    }
}
