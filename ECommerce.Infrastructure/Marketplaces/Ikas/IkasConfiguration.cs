﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas
{
    public class IkasConfiguration
    {
        #region Properties
        public string GrantType { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string StockLocationId { get; set; }
        public string Url { get; set; }
        #endregion
    }
}
