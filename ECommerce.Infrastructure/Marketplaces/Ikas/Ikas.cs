﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Infrastructure.Marketplaces.Ikas.Common;
using ECommerce.Infrastructure.Marketplaces.Ikas.Responses.ListPoduct;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Ikas
{
    public class Ikas : TokenMarketplaceBase<IkasConfiguration>
    {
        #region Constants
        private readonly string _tokenUrl = "https://entegre.myikas.com/api/admin/oauth/token";

        private readonly string _graphQlUrl = "https://api.myikas.com/api/v1/admin/graphql";

        private readonly string _imageUploadUrl = "https://api.myikas.com/api/v1/admin/product/upload/image";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Ikas(IHttpHelper httpHelper, IServiceProvider serviceProvider, ICacheHandler cacheHandler) : base(serviceProvider, cacheHandler, 29)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.IKAS;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                List<string> productVariantTypes = new();
                var groupedProductVariantTypes = productRequest
                     .ProductItem
                     .StockItems
                     .SelectMany(si => si
                        .Attributes
                        .OrderByDescending(x => x.AttributeName.ToLower() == "renk")
                        .Select(a => new
                        {
                            variantTypeId = a.AttributeCode,
                            variantValueId = a.AttributeValueCode
                        }))
                     .GroupBy(n => n.variantTypeId)
                     .ToList();

                int order = 1;
                foreach (var variantType in groupedProductVariantTypes)
                {
                    productVariantTypes.Add($@"{{
    order: {order},
    variantTypeId: ""{variantType.Key}"",
    variantValueIds: [{string.Join(",", variantType.GroupBy(x => x.variantValueId).Select(x => $"\"{x.Key}\""))}]
}}");
                    order++;
                }

                var url = $"{productRequest.ProductItem.Name.GenerateUrl()}-{productRequest.ProductItem.SellerCode.GenerateUrl()}";

                var queryObject = new QueryObject
                {
                    Query = @$"
mutation {{
    saveProduct(input: {{
        id: ""{productRequest.ProductItem.UUId.ToLower()}"",
        metaData: {{
            id: ""{productRequest.ProductItem.UUId.ToLower()}"",
            slug: ""{url}"",
            pageTitle: ""{productRequest.ProductItem.Name} {productRequest.ProductItem.SellerCode} - Lafaba"",
            description: ""{productRequest.ProductItem.Name} {productRequest.ProductItem.SellerCode} ürününe en uygun fiyarlarla sahip ol. Hemen alışverişe başla.""
        }}
        name: ""{productRequest.ProductItem.Name}"",
        type: PHYSICAL,
        brandId: ""{productRequest.ProductItem.BrandId}"",
        categoryIds: [""{productRequest.ProductItem.CategoryId}""],
        groupVariantsByVariantTypeId:""{groupedProductVariantTypes.FirstOrDefault()?.Key}""
        description: ""{productRequest.ProductItem.StockItems.FirstOrDefault()?.Description}"",
        productVariantTypes:[{string.Join(",", productVariantTypes)}],
        salesChannelIds: [""35f561f7-406b-46d9-8b01-4ce56717bea9""],
        variants:[{string.Join(",", productRequest.ProductItem.StockItems.Select(si => @$"
{{
    id: ""{si.UUId.ToLower()}"",
    isActive: true,
    barcodeList: [""{si.Barcode}""],
    sku: ""{si.StockCode}"",
    variantValueIds: [{string.Join(",", si.Attributes.OrderByDescending(x => x.AttributeName.ToLower() == "renk").Select(a => $@"{{
        variantTypeId: ""{a.AttributeCode}""
        variantValueId: ""{a.AttributeValueCode}""
    }}"))}],
    prices:[{{
        sellPrice: {si.ListPrice},
        {(si.SalePrice < si.ListPrice ? $"discountPrice: {si.SalePrice}" : "")}
    }}]
}}"))}]
    }}) {{
        id,
        variants {{
            id,
        images{{
        imageId
              }}
        }}
    }}
}}"
                };

                var product = _httpHelper
                    .Post<QueryObject, SaveProductResponse>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this.GetAccessToken().Data, null, null);


                if (product != null && product.data != null)
                {
                    foreach (var theVariant in product.data.saveProduct.variants)
                    {
                        foreach (var theImage in productRequest
                            .ProductItem
                            .StockItems[product.data.saveProduct.variants.IndexOf(theVariant)]
                            .Images)
                        {
                            var body = $@"
                {{
                   ""productImage"": {{
                        ""variantIds"": [""{theVariant.Id}""],
                        ""url"": ""{theImage.Url}"",
                        ""order"": ""{theImage.Order}"",
                        ""isMain"": {(productRequest.ProductItem.StockItems[product.data.saveProduct.variants.IndexOf(theVariant)].Images.IndexOf(theImage) == 0 ? "true" : "false")}
                    }}
                }}";
                            _httpHelper
                                .Post(
                                this._imageUploadUrl,
                                body,
                                "Bearer",
                                this.GetAccessToken().Data);
                        }
                    }
                }

                response.Success = true;
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @$"
            mutation {{
                saveVariantPrices(input: {{
                    variantPriceInputs: [{{
                        productId: ""{stockRequest.UUId}""
                        price: {{
                            sellPrice: {stockRequest.PriceStock.ListPrice},
                            discountPrice: {stockRequest.PriceStock.SalePrice}
                        }},
                        variantId: ""{stockRequest.PriceStock.UUId}""    
                    }}]
                }})
            }}"
                };

                var responseString = _httpHelper.Post<QueryObject, SaveProductPriceLocationResponse>(
                    queryObject,
                    this._graphQlUrl,
                    "Bearer",
                    this.GetAccessToken().Data, null, null);


                if (responseString != null && responseString.data != null)
                {
                    response.Success = responseString.data.saveVariantPrices;
                }
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @$"
mutation {{
    saveProductStockLocations(input: {{
        productStockLocationInputs: [{{
            productId: ""{stockRequest.UUId}""
            stockCount: {stockRequest.PriceStock.Quantity}
            stockLocationId: ""{this._configuration.StockLocationId}""
            variantId: ""{stockRequest.PriceStock.UUId}""
        }}]
    }})
}}"
                };

                var responseString = _httpHelper.Post<QueryObject, SaveProductStockLocationResponse>(
                    queryObject,
                    this._graphQlUrl,
                    "Bearer",
                    this.GetAccessToken().Data, null, null);

                if (responseString != null && responseString.data != null)
                {
                    response.Success = responseString.data.saveProductStockLocations;
                    Console.WriteLine("OK " + stockRequest.PriceStock.UUId);
                }
                else
                {

                }
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new List<Application.Common.DataTransferObjects.MarketplaceProductStatus>();

                var url = this._configuration.Url.EndsWith("/") ? this._configuration.Url : $"{this._configuration.Url}/";
                var token = this.GetAccessToken().Data;
                var hasNext = false;
                var page = 1;
                do
                {
                    var queryObject = new QueryObject
                    {
                        Query = @$"
{{
    listProduct(pagination: {{limit: 100, page: {page}}}){{
        count
        page
        hasNext
        limit
        data{{
            metaData{{
                slug
            }}
            variants{{
                id
                barcodeList
                stocks{{
                    stockCount
                }}
                prices{{
                    sellPrice
                }}
            }}
        }}
    }}
}}"
                    };
                    var listProductResponse = _httpHelper.Post<QueryObject, ListProductResponse>(
                        queryObject, this._graphQlUrl, "Bearer", token, null, null);

                    hasNext = listProductResponse.data.listProduct.hasNext;

                    foreach (var theD in listProductResponse.data.listProduct.data)
                    {
                        foreach (var theVariant in theD.variants)
                        {
                            try
                            {
                                response.Data.Add(new()
                                {
                                    MarketplaceId = this.MarketplaceId,
                                    Barcode = theVariant.barcodeList[0],

                                    Opened = true,
                                    OnSale = true,
                                    Locked = false,

                                    Stock = theVariant.stocks.Length == 0 ? 0 : theVariant.stocks[0].stockCount,
                                    Url = $"{url}{theD.metaData.slug}",
                                    ListUnitPrice = theVariant.prices[0].sellPrice,
                                    UnitPrice = theVariant.prices[0].discountPrice == 0 ? theVariant.prices[0].sellPrice : theVariant.prices[0].discountPrice
                                });
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }

                    page++;
                } while (hasNext);

                response.Success = true;
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<Application.Common.Wrappers.Marketplaces.CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.Wrappers.Marketplaces.CategoryResponse>>>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @"
{
    listCategory{
        id
        name
    }
}"
                };

                var listCategoryResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListCategoryReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this.GetAccessToken().Data, null, null).Data.ListCategory;

                queryObject = new QueryObject
                {
                    Query = @"
{
    listVariantType{
        id
        name,
        values{
            id
            name
        }
    }
}"
                };

                var listVariantTypeResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListVariantTypeReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this.GetAccessToken().Data, null, null).Data.ListVariantType;

                response.Data = new();

                listCategoryResponse.ForEach(cLoop =>
                {
                    response.Data.Add(new Application.Common.Wrappers.Marketplaces.CategoryResponse
                    {
                        Code = cLoop.Id,
                        Name = cLoop.Name,
                        CategoryAttributes = listVariantTypeResponse
                            .Select(v => new CategoryAttributeResponse
                            {
                                Code = v.Id,
                                Name = v.Name,
                                Mandatory = true,
                                AllowCustom = false,
                                MultiValue = false,
                                CategoryCode = cLoop.Id,
                                CategoryAttributesValues = v
                                    .Values
                                    .Select(vv => new CategoryAttributeValueResponse
                                    {
                                        Code = vv.Id,
                                        Name = vv.Name,
                                        VarinatCode = v.Id
                                    })
                                    .ToList()
                            })
                            .ToList()
                    });
                });

                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var hasNext = false;
                var page = 0;
                var orders = new List<Order>();
                OrderPaginationResponse orderPaginationResponse = null;

                do
                {
                    var queryObject = new QueryObject
                    {
                        Query = @$"
{{listOrder(status: {{ eq: CREATED }}, pagination:{{page: {page}, limit: 10}} ){{
        count,
        data{{
            id,
            customerId
            customer{{
                id
                firstName
                lastName
            }}
            orderPackages{{
                id
                orderPackageNumber
            }}
            orderLineItems{{
                id,
                finalPrice,
                originalOrderLineItemId,
                variant{{
                    name,
                    barcodeList
                    sku
                    variantValues{{
                        variantTypeId
                        variantTypeName
                        variantValueId
                        variantValueName
                    }}
                }}
            }}
        }},
        hasNext,
        limit,
        page
    }}
}}
"
                    };

                    orderPaginationResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListOrderReponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this.GetAccessToken().Data, null, null).Data.ListOrder;

                    if (orderPaginationResponse != null)
                    {
                        hasNext = orderPaginationResponse.HasNext;

                        orders.AddRange(orderPaginationResponse.Data);
                    }

                    page++;
                } while (hasNext);

                Parallel.ForEach(response.Data.Orders, theOrder =>
                {
                    theOrder.UId = $"{Application.Common.Constants.Marketplaces.IKAS}_{theOrder.OrderCode}";
                });

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var token = this.GetAccessToken().Data;

                var graphQLHttpClient = new GraphQLHttpClient(new GraphQLHttpClientOptions
                {
                    EndPoint = new Uri(_graphQlUrl)
                }, new NewtonsoftJsonSerializer());
                graphQLHttpClient
                    .HttpClient
                    .DefaultRequestHeaders
                    .Add("Authorization", $"Bearer {token}");

                /* 1. UNFULFILLED */
                var unfulfilledGraphQLRequest = new GraphQLRequest
                {
                    Query = "{listOrder(status: { eq: CREATED }, orderPackageStatus: { eq: UNFULFILLED }, pagination: {page: 1, limit: 200}) { count, data { id, orderLineItems { id, quantity } }, hasNext, limit, page } }"
                };
                var unfulfilledGraphQLResponse = graphQLHttpClient
                    .SendQueryAsync<ListOrderReponse>(unfulfilledGraphQLRequest)
                    .Result;

                /* 2. FULFILL ORDER */
                foreach (var loopOrder in unfulfilledGraphQLResponse.Data.ListOrder.Data)
                {
                    var fulfillOrderGraphQLRequest = new GraphQLRequest
                    {
                        Query = $"mutation {{ fulfillOrder(input: {{ lines: [{string.Join(",", loopOrder.OrderLineItems.Select(oli => $"{{ orderLineItemId: \"{oli.Id}\", quantity: {oli.Quantity} }}"))}], markAsReadyForShipment: true, orderId: \"{loopOrder.Id}\" }}){{id, orderNumber}} }}"
                    };
                    var fulfillOrderGraphQLResponse = graphQLHttpClient
                        .SendQueryAsync<FulfillOrderResponse>(fulfillOrderGraphQLRequest)
                        .Result;
                }

                /* 3. READY FOR SHIPMENT */
                var readyForShipmentGraphQLRequest = new GraphQLRequest
                {
                    Query = "{listOrder(status: { eq: CREATED }, orderPackageStatus: { eq: READY_FOR_SHIPMENT }, pagination: {page: 1, limit: 200}){ count, data { id, orderPaymentStatus, customerId, orderNumber, totalFinalPrice,  shippingLines { finalPrice, price, taxValue, price, paymentMethod, title }, paymentMethods { type, paymentGatewayCode, paymentGatewayId, paymentGatewayName, price }, billingAddress { firstName, lastName, identityNumber, district { name }, city { name }, country { name }, addressLine1, addressLine2, company, phone, taxNumber, taxOffice }, shippingAddress { firstName, lastName, identityNumber, district { name }, city { name }, country { name }, addressLine1, addressLine2, company, phone, taxNumber, taxOffice }, customer { id, firstName, lastName, phone }, orderPackages { id, orderPackageNumber, trackingInfo { barcode, cargoCompany, trackingLink } }, orderLineItems { id, finalPrice, quantity, originalOrderLineItemId, variant { name, barcodeList, sku, variantValues { variantTypeId, variantTypeName, variantValueId, variantValueName }  } } }, hasNext, limit, page } }"
                };
                var readyForShipmentGraphQLResponse = graphQLHttpClient
                    .SendQueryAsync<ListOrderReponse>(readyForShipmentGraphQLRequest)
                    .Result;

                var validOrders = readyForShipmentGraphQLResponse
                    .Data
                    .ListOrder
                    .Data
                    .Where(o => !o.OrderPackages.Any(op => op.TrackingInfo == null || string.IsNullOrEmpty(op.TrackingInfo.Barcode)))
                    .ToList();

                response.Data = new()
                {
                    Orders = this.MapOrder(validOrders)
                };
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var hasNext = false;
                var page = 0;
                var orders = new List<Order>();
                OrderPaginationResponse orderPaginationResponse = null;
                var token = this.GetAccessToken();
                if (token.Success)
                {
                    do
                    {
                        var queryObject = new QueryObject
                        {
                            Query = @$"
{{listOrder(status: {{ eq: CANCELLED }}, pagination:{{page: {page}, limit: 10}} ){{
        count,
        data{{
            id,
            customerId
            customer{{
                id
                firstName
                lastName
            }}
            orderPackages{{
                id
                orderPackageNumber
            }}
            orderLineItems{{
                id,
                finalPrice,
                originalOrderLineItemId,
                variant{{
                    name,
                    barcodeList
                    sku
                    variantValues{{
                        variantTypeId
                        variantTypeName
                        variantValueId
                        variantValueName
                    }}
                }}
            }}
        }},
        hasNext,
        limit,
        page
    }}
}}
"
                        };


                        orderPaginationResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListOrderReponse>>(
                            queryObject,
                            this._graphQlUrl,
                            "Bearer",
                            token.Data, null, null).Data.ListOrder;

                        if (orderPaginationResponse != null)
                        {
                            hasNext = orderPaginationResponse.HasNext;

                            orders.AddRange(orderPaginationResponse.Data);
                        }

                        page++;
                    } while (hasNext);

                    response.Success = true;
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var hasNext = false;
                var page = 0;
                var orders = new List<Order>();
                OrderPaginationResponse orderPaginationResponse = null;
                var token = this.GetAccessToken();
                if (token.Success)
                {

                    do
                    {
                        var queryObject = new QueryObject
                        {
                            Query = @$"
{{listOrder(orderPackageStatus: {{ eq: DELIVERED }}, pagination:{{page: {page}, limit: 10}} ){{
        count,
        data{{
            id,
            customerId
            customer{{
                id
                firstName
                lastName
            }}
            orderPackages{{
                id
                orderPackageNumber
            }}
            orderLineItems{{
                id,
                finalPrice,
                originalOrderLineItemId,
                variant{{
                    name,
                    barcodeList
                    sku
                    variantValues{{
                        variantTypeId
                        variantTypeName
                        variantValueId
                        variantValueName
                    }}
                }}
            }}
        }},
        hasNext,
        limit,
        page
    }}
}}
"
                        };

                        orderPaginationResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListOrderReponse>>(
                            queryObject,
                            this._graphQlUrl,
                            "Bearer",
                            token.Data, null, null).Data.ListOrder;

                        if (orderPaginationResponse != null)
                        {
                            hasNext = orderPaginationResponse.HasNext;

                            orders.AddRange(orderPaginationResponse.Data);
                        }

                        page++;
                    } while (hasNext);
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var hasNext = false;
                var page = 0;
                var orders = new List<Order>();

                do
                {
                    var queryObject = new QueryObject
                    {
                        Query = @$"
{{listOrder(status: {{ eq: UNABLE_TO_DELIVER }}, pagination:{{page: {page}, limit: 10}} ){{
        count,
        data{{
            id,
            customerId
            customer{{
                id
                firstName
                lastName
            }}
            orderPackages{{
                id
                orderPackageNumber
            }}
            orderLineItems{{
                id,
                finalPrice,
                originalOrderLineItemId,
                variant{{
                    name,
                    barcodeList
                    sku
                    variantValues{{
                        variantTypeId
                        variantTypeName
                        variantValueId
                        variantValueName
                    }}
                }}
            }}
        }},
        hasNext,
        limit,
        page
    }}
}}
"
                    };

                    //ResponseWrapper<ListOrderReponse> orderPaginationResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListOrderReponse>>(
                    //    queryObject,
                    //    this._graphQlUrl,
                    //    "Bearer",
                    //    this.GetAccessToken().Data);





                    //if (orderPaginationResponse != null)
                    //{
                    //    hasNext = orderPaginationResponse.Data.ListOrder.HasNext;

                    //    orders.AddRange(orderPaginationResponse.Data);
                    //}

                    page++;
                } while (hasNext);

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
            });
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {
                var queryObject = new QueryObject
                {
                    Query = @"
{
    listProductBrand ( name: { like: """ + q + @""" } ) {
        id,
        name
    }
}"
                };

                var listProductBrandResponse = this._httpHelper.Post<QueryObject, ResponseWrapper<ListProductBrandResponse>>(
                        queryObject,
                        this._graphQlUrl,
                        "Bearer",
                        this.GetAccessToken().Data, null, null).Data.ListCategory;

                response.Data = listProductBrandResponse
                    .Select(x => new Application.Common.DataTransferObjects.KeyValue<string, string>
                    {
                        Key = x.Id,
                        Value = x.Name
                    })
                    .ToList();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(IkasConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.GrantType = "client_credentials";
            configuration.ClientId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ClientId").Value;
            configuration.ClientSecret = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ClientSecret").Value;
            configuration.StockLocationId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "StockLocationId").Value;
            configuration.Url = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Url").Value;
        }

        protected override String GetAccessTokenConcreate()
        {
            var data = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("grant_type", this._configuration.GrantType),
                new KeyValuePair<string, string>("client_id", this._configuration.ClientId),
                new KeyValuePair<string, string>("client_secret", this._configuration.ClientSecret)
            };

            return this._httpHelper.Post<Token>(_tokenUrl, data).AccessToken;
        }

        private List<Application.Common.Wrappers.Marketplaces.Order.Order> MapOrder(List<Order> providerOrders)
        {
            List<Application.Common.Wrappers.Marketplaces.Order.Order> orders = new();

            foreach (var theProviderOrder in providerOrders)
            {
                try
                {
                    var trackingInfoBarcode = theProviderOrder.OrderPackages.FirstOrDefault()?.TrackingInfo?.Barcode;
                    var shipmentCompanyId = "ARS";
                    if (!string.IsNullOrEmpty(trackingInfoBarcode))
                    {
                        if (trackingInfoBarcode.StartsWith("1Z"))
                            shipmentCompanyId = ShipmentCompanies.Ups;
                    }

                    if (theProviderOrder.OrderPackages.FirstOrDefault()?.TrackingInfo?.CargoCompany?.ToUpper() == "MNG KARGO")
                        shipmentCompanyId = ShipmentCompanies.MngKargo;

                    if (theProviderOrder.OrderPackages.FirstOrDefault()?.TrackingInfo?.CargoCompany?.ToUpper() == "UPS KARGO MARKETPLACE")
                        shipmentCompanyId = ShipmentCompanies.Ups;

                    if (theProviderOrder.OrderPackages.FirstOrDefault()?.TrackingInfo?.CargoCompany?.ToUpper() == "HEPSIJET")
                        shipmentCompanyId = ShipmentCompanies.HepsiJet;

                    var cargoFee = 0m;
                    if (theProviderOrder.ShippingLines != null && theProviderOrder.ShippingLines.FirstOrDefault(x => x.Title == "Kargo ücreti") != null)
                    {
                        cargoFee = theProviderOrder.ShippingLines.FirstOrDefault(x => x.Title == "Kargo ücreti").Price;
                    }

                    var surchargeFee = 0m;
                    if (theProviderOrder.ShippingLines != null && theProviderOrder.ShippingLines.FirstOrDefault(x => x.PaymentMethod == "CASH_ON_DELIVERY") != null)
                    {
                        surchargeFee = theProviderOrder.ShippingLines.FirstOrDefault(x => x.PaymentMethod == "CASH_ON_DELIVERY").Price;
                    }


                    Application.Common.Wrappers.Marketplaces.Order.Order order = new()
                    {
                        UUId = theProviderOrder.Id,
                        UId = $"{Application.Common.Constants.Marketplaces.IKAS}_{theProviderOrder}-{theProviderOrder.OrderPackages.FirstOrDefault().orderPackageNumber}",
                        OrderSourceId = OrderSources.Web,
                        OrderSource = "WB",
                        MarketplaceId = Application.Common.Constants.Marketplaces.IKAS,
                        OrderCode = theProviderOrder.orderNumber,
                        OrderTypeId = "OS",
                        TotalAmount = (decimal)theProviderOrder.totalFinalPrice,
                        ListTotalAmount = (decimal)theProviderOrder.totalFinalPrice,
                        Discount = 0M,
                        SurchargeFee = surchargeFee,
                        CargoFee = cargoFee,
                        OrderDate = DateTime.Now,
                        OrderShipment = theProviderOrder
                            .OrderPackages
                            .Select(op => new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                Payor = false,
                                ShipmentCompanyId = shipmentCompanyId,
                                PackageNumber = op.orderPackageNumber,
                                TrackingCode = op.TrackingInfo.Barcode
                            })
                            .FirstOrDefault(),
                        Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer()
                        {
                            FirstName = theProviderOrder.Customer.FirstName,
                            LastName = theProviderOrder.Customer.LastName,
                            Mail = theProviderOrder.Customer.Email,
                            Phone = theProviderOrder.Customer.Phone
                        },
                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                        {
                            FirstName = theProviderOrder.ShippingAddress.FirstName,
                            LastName = theProviderOrder.ShippingAddress.LastName,
                            Country = theProviderOrder.ShippingAddress.Country.Name,
                            Email = "",
                            Phone = theProviderOrder.ShippingAddress.Phone,
                            City = theProviderOrder.ShippingAddress.City.Name,
                            District = theProviderOrder.ShippingAddress.District.Name,
                            Neighborhood = string.Empty,
                            Address = $"{theProviderOrder.ShippingAddress.AddressLine1} {theProviderOrder.ShippingAddress.AddressLine2}"
                        },
                        OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                        {
                            FirstName = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.FirstName : theProviderOrder.ShippingAddress.FirstName,
                            LastName = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.LastName : theProviderOrder.ShippingAddress.LastName,
                            Email = "",
                            Phone = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.Phone : theProviderOrder.ShippingAddress.Phone,
                            City = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.City.Name : theProviderOrder.ShippingAddress.City.Name,
                            District = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.District.Name : theProviderOrder.ShippingAddress.District.Name,
                            Address = theProviderOrder.BillingAddress != null ? $"{theProviderOrder.BillingAddress.AddressLine1} {theProviderOrder.BillingAddress.AddressLine2}" : $"{theProviderOrder.ShippingAddress.AddressLine1}",
                            Neighborhood = string.Empty,
                            TaxNumber = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.TaxNumber : "",
                            TaxOffice = theProviderOrder.BillingAddress != null ? theProviderOrder.BillingAddress.TaxOffice : ""
                        },
                        Payments = theProviderOrder
                            .paymentMethods
                            .Select(pm => new Application.Common.Wrappers.Marketplaces.Order.Payment
                            {
                                Amount = (decimal)pm.Price,
                                CurrencyId = "TRY",
                                Date = DateTime.Now,
                                PaymentTypeId = ConvertPaymentType(pm.Type),
                                Paid = theProviderOrder.orderPaymentStatus == "PAID"
                            })
                            .ToList()
                    };

                    if (order.Payments.Any(p => p.PaymentTypeId == "C"))
                    {
                        var payment = order.Payments.First(p => p.PaymentTypeId == "C");
                        if (payment.Paid == false)
                            order.OrderTypeId = "BE";
                    }

                    order.OrderPicking = new Application.Common.Parameters.Order.OrderPicking
                    {
                        MarketPlaceOrderId = theProviderOrder.Id.ToString(),
                        OrderDetails = new List<Application.Common.Parameters.Order.OrderPickingDetail>()
                    };

                    #region Order Details
                    order.OrderDetails = new List<Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();
                    foreach (var theOrderLineItem in theProviderOrder.OrderLineItems)
                    {
                        order.OrderDetails.Add(new Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                        {
                            UUId = theOrderLineItem.Id,
                            UId = theOrderLineItem.Id.ToString(),
                            UnitPrice = theOrderLineItem.finalPrice,
                            ListPrice = theOrderLineItem.finalPrice,
                            UnitDiscount = 0M,
                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                            {
                                Barcode = theOrderLineItem.Variant.BarcodeList[0].ToUpper(),
                                Name = theOrderLineItem.Variant.Name
                            },
                            Quantity = (int)theOrderLineItem.Quantity
                        });
                        order.OrderPicking.OrderDetails.Add(new Application.Common.Parameters.Order.OrderPickingDetail
                        {
                            Id = theOrderLineItem.Id.ToString(),
                            Quantity = (int)theOrderLineItem.Quantity,
                        });
                    }
                    #endregion

                    orders.Add(order);
                }
                catch (Exception ex)
                {

                }
            }

            return orders;
        }

        private string ConvertPaymentType(string paymentType)
        {
            string text = "OO";
            switch (paymentType)
            {
                case "CREDIT_CARD":
                    text = "OO";
                    break;
                case "CASH_ON_DELIVERY":
                    text = "DO";
                    break;
                case "CREDIT_CARD_ON_DELIVERY":
                    text = "PDO";
                    break;
                case "MONEY_ORDER":
                    text = "C";
                    break;
            }
            return text;
        }
        #endregion
    }
}