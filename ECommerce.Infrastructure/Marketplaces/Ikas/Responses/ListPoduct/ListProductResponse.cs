﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Responses.ListPoduct
{

    public class ListProductResponse
    {
        public Data data { get; set; }
    }

    public class Data
    {
        public Listproduct listProduct { get; set; }
    }

    public class Listproduct
    {
        public int count { get; set; }
        public int page { get; set; }
        public bool hasNext { get; set; }
        public int limit { get; set; }
        public Datum[] data { get; set; }
    }

    public class Datum
    {
        public Metadata metaData { get; set; }
        public Variant[] variants { get; set; }
    }

    public class Metadata
    {
        public string slug { get; set; }
    }

    public class Variant
    {
        public string[] barcodeList { get; set; }
        public Stock[] stocks { get; set; }
        public Price[] prices { get; set; }
    }

    public class Stock
    {
        public int stockCount { get; set; }
    }

    public class Price
    {
        public decimal sellPrice { get; set; }
        public decimal discountPrice { get; set; }
    }

}
