﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class VariantType
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantValue> Values { get; set; }
        #endregion
    }
}
