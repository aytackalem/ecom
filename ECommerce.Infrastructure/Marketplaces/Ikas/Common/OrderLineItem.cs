﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class OrderLineItem
    {
        #region Properties
        public string Id { get; set; }

        public float Quantity { get; set; }

        public string OriginalOrderLineItemId { get; set; }

        public decimal finalPrice { get; set; }
        #endregion

        #region Navigation Properties
        public OrderLineVariant Variant { get; set; }
        #endregion
    }
}
