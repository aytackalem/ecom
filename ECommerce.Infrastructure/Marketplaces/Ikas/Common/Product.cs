﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Product
    {
        #region Properties
        public string Id { get; set; }
        #endregion

        #region Navigation Properties
        public List<Variant> Variants { get; set; }
        #endregion
    }
}