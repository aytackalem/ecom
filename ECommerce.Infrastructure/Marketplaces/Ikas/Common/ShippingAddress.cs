﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ShippingAddress
    {
        #region Properties
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdentityNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Company { get; set; }

        public string Phone { get; set; }

        public string TaxNumber { get; set; }

        public string TaxOffice { get; set; }
        #endregion

        #region Navigation Properties
        public Country Country { get; set; }

        public City City { get; set; }

        public District District { get; set; }
        #endregion
    }
}
