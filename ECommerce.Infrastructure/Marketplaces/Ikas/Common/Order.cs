﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Order
    {
        #region Properties
        public string Id { get; set; }

        public string orderPaymentStatus { get; set; }

        public string CustomerId { get; set; }

        public string orderNumber { get; set; }

        public float totalPrice { get; set; }

        public float totalFinalPrice { get; set; }
        #endregion

        #region Navigation Properties
        public List<PaymentMethod> paymentMethods { get; set; }

        public BillingAddress BillingAddress { get; set; }

        public ShippingAddress ShippingAddress { get; set; }

        public OrderCustomer Customer { get; set; }

        public List<OrderLineItem> OrderLineItems { get; set; }

        public List<OrderPackage> OrderPackages { get; set; }

        public List<ShippingLine> ShippingLines { get; set; }
        #endregion
    }
}
