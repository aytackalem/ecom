﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ListProductBrandResponse
    {
        #region Properties
        public List<Category> ListCategory { get; set; }
        #endregion
    }
}
