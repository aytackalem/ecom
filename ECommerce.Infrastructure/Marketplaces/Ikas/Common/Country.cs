﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Country
    {
        public string Name { get; set; }
    }
}
