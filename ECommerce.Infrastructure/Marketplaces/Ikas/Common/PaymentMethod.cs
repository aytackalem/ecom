﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class PaymentMethod
    {
        #region Properties
        public string Type { get; set; }

        public string PaymentGatewayCode { get; set; }

        public string PaymentGatewayId { get; set; }

        public string paymentGatewayName { get; set; }

        public float Price { get; set; }
        #endregion
    }
}
