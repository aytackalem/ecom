﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class FulfillOrderResponse
    {
        public Order FulfillOrder { get; set; }
    }
}
