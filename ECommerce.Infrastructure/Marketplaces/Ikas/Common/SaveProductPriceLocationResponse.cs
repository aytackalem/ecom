﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
   

    public class SaveProductPriceLocation
    {
        public bool saveVariantPrices { get; set; }
    }

    public class SaveProductPriceLocationResponse
    {
        public SaveProductPriceLocation data { get; set; }
    }

}
