﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Token
    {
        #region Properties
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        #endregion
    }
}
