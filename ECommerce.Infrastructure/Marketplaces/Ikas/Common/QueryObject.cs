﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class QueryObject
    {
        #region Properties
        [JsonProperty("query")]
        public string Query { get; set; }
        #endregion
    }
}
