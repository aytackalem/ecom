﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Data
    {
        public SaveProduct saveProduct { get; set; }
    }

    public class SaveProductResponse
    {
        public Data data { get; set; }
    }

    public class SaveProduct
    {
        public string id { get; set; }
        public List<Variant> variants { get; set; }
    }
}
