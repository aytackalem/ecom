﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ListVariantTypeReponse
    {
        #region Properties
        public List<VariantType> ListVariantType { get; set; }
        #endregion
    }
}
