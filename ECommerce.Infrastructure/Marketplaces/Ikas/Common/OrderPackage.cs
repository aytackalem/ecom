﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class OrderPackage
    {
        #region Properties
        public string Id { get; set; }

        public string orderPackageNumber { get; set; }
        #endregion

        #region Navigation Properties
        public TrackingInfo TrackingInfo { get; set; }
        #endregion
    }
}
