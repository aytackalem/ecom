﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ResponseWrapper<T>
    {
        #region Properties
        public T Data { get; set; }
        #endregion
    }
}
