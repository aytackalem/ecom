﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ListCategoryReponse
    {
        #region Properties
        public List<Category> ListCategory { get; set; }
        #endregion
    }
}
