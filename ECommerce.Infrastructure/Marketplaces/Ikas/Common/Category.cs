﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class Category
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
