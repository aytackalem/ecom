﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class ShippingLine
    {
        #region Properties


        public decimal FinalPrice { get; set; }
        public decimal Price { get; set; }
        public string TaxValue { get; set; }
        public string PaymentMethod { get; set; }
        public string Title { get; set; }
        #endregion

    }
}
