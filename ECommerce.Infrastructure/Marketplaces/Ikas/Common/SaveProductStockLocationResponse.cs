﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
   

    public class SaveProductStockLocation
    {
        public bool saveProductStockLocations { get; set; }
    }

    public class SaveProductStockLocationResponse
    {
        public SaveProductStockLocation data { get; set; }
    }

}
