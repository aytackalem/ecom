﻿namespace ECommerce.Infrastructure.Marketplaces.Ikas.Common
{
    public class TrackingInfo
    {
        #region Properties
        public string Barcode { get; set; }

        public string CargoCompany { get; set; }

        public string TrackingLink { get; set; }
        #endregion
    }
}
