using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList
{
    public class OrderListResponse
    {
        #region Properties
        public int total { get; set; }
        public int code { get; set; }
        public string internalMessage { get; set; }
        public string message { get; set; }
        #endregion

        #region Navigation Properties
        public List<Datum> data { get; set; }
        #endregion
    }
}