using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class Attribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }

}