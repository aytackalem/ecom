using System.Collections.Generic; 
using System; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class Item
    {
        public int itemid { get; set; }
        public int itemIndex { get; set; }
        public int statusTypeId { get; set; }
        public string statusDescription { get; set; }
        public string packageId { get; set; }
        public int packageStatusTypeId { get; set; }
        public string packageStatusDescription { get; set; }
        public int pieceCount { get; set; }
        public string sku { get; set; }
        public string merchantsku { get; set; }
        public string barcode { get; set; }
        public string productDescription { get; set; }
        public int quantity { get; set; }
        public string unit { get; set; }
        public double unitPrice { get; set; }
        public double grossAmount { get; set; }
        public double taxAmount { get; set; }
        public double discountAmount { get; set; }
        public double netAmount { get; set; }
        public double allowanceAmount { get; set; }
        public double commisionAmount { get; set; }
        public double commisionRate { get; set; }
        public string currency { get; set; }
        public string vatcode { get; set; }
        public string cancelReason { get; set; }
        public List<Attribute> attributes { get; set; }
        public DateTime lastUpdatedate { get; set; }
        public string lastUpatetime { get; set; }
        public int timestamp { get; set; }
        public string image { get; set; }
        public int morhipoProductId { get; set; }
        public bool cancelable { get; set; }
    }

}