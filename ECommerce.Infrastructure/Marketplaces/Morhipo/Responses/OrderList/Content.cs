using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class Content
    {
        public string itemId { get; set; }
        public int orderId { get; set; }
        public int quantity { get; set; }
        public string unit { get; set; }
    }

}