using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class ShipmentAddress
    {
        public string name { get; set; }
        public string address { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string citycode { get; set; }
        public string country { get; set; }
        public string postalcode { get; set; }
        public string phoneNumber { get; set; }
    }

}