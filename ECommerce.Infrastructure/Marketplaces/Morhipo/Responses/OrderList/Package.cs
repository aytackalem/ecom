using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class Package
    {
        public string packageId { get; set; }
        public int statusTypeId { get; set; }
        public string statusDescription { get; set; }
        public int pieceCount { get; set; }
        public string trackingCode { get; set; }
        public string trackingUrl { get; set; }
        public string shippingPartner { get; set; }
        public List<Content> content { get; set; }
    }

}