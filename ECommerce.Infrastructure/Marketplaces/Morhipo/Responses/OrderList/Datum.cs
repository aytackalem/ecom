using System.Collections.Generic; 
using System; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderList{ 

    public class Datum
    {
        public int orderId { get; set; }
        public string order { get; set; }
        public int itemCount { get; set; }
        public int quantity { get; set; }
        public double grossAmount { get; set; }
        public double taxAmount { get; set; }
        public double netAmount { get; set; }
        public double discountAmount { get; set; }
        public double allowanceAmount { get; set; }
        public string shopperId { get; set; }
        public string shopperEmail { get; set; }
        public DateTime date { get; set; }
        public DateTime commitmentDate { get; set; }
        public ShipmentAddress shipmentAddress { get; set; }
        public InvoiceAddress invoiceAddress { get; set; }
        public List<Item> items { get; set; }
        public List<Package> packages { get; set; }
    }

}