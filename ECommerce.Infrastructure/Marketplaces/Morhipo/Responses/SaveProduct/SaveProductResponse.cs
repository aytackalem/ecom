﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.SaveProduct
{
    public class SaveProductResponse
    {
        #region Properties
        public string integrator { get; set; }

        public string trackingId { get; set; }

        public string code { get; set; }

        public string internalMessage { get; set; }

        public string message { get; set; }
        #endregion
    }
}
