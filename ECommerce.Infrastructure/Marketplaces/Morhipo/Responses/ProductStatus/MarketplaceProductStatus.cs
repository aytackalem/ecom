﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.ProductStatus
{
    public class MarketplaceProductStatus
    {
        public List<MarketplaceProductStatusData> data { get; set; }
        public int total { get; set; }
        public int code { get; set; }
        public string internalMessage { get; set; }
        public string message { get; set; }
    }

    public class MarketplaceProductStatusData
    {
        public int productId { get; set; }
        public int quantity { get; set; }
        public double salePrice { get; set; }
        public double listPrice { get; set; }
        public string sku { get; set; }
        public string barcode { get; set; }
        public string merchantProductId { get; set; }


    }
}
