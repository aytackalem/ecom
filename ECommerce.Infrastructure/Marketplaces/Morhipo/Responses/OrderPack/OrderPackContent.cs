﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderPack
{
    public class OrderPackContent
    {
        #region Properties
        public int itemId { get; set; }
        
        public int quantity { get; set; }
        
        public string unit { get; set; }
        #endregion
    }
}
