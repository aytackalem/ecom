﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderPack
{
    public class OrderPackResponse
    {
        #region Properties
        public string integrator { get; set; }

        public string trackingId { get; set; }

        public string code { get; set; }

        public string internalMessage { get; set; }

        public string message { get; set; }
        #endregion

        #region Navigation Properties
        public List<OrderPackData> data { get; set; }
        #endregion
    }
}
