﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.OrderPack
{
    public class OrderPackData
    {
        #region Properties
        public string packageId { get; set; }
        
        public int pieceCount { get; set; }
        #endregion

        #region Navigation Properties
        public List<OrderPackContent> content { get; set; }
        #endregion
    }
}
