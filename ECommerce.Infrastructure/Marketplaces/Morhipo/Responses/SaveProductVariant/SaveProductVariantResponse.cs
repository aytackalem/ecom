﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.SaveProductVariant
{
    public class SaveProductVariantResponse
    {
        #region Properties
        public string integrator { get; set; }

        public string trackingId { get; set; }

        public string code { get; set; }

        public string internalMessage { get; set; }

        public string message { get; set; }
        #endregion
    }
}
