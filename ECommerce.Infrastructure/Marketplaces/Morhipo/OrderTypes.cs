﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo
{
    public enum OrderTypes
    {
        #region Members
        /// <summary>
        /// Onayda, Fraud Sürecinde Bekliyor
        /// </summary>
        UnConfirmed = 0,

        /// <summary>
        /// Yeni Siparişler
        /// </summary>
        New = 1,

        /// <summary>
        /// Hazırlanann siparişler
        /// </summary>
        WaitingProcess = 2,

        /// <summary>
        /// Paketlenen Siparişler
        /// </summary>
        ReadyToShipped = 3,

        /// <summary>
        /// Kargoya Teslim Edilen Siparişler
        /// </summary>
        DeliveredToShippingAgent = 4,

        /// <summary>
        /// Kargoya Firmasın Tatrafınca Kabul Edilen Siparişler
        /// </summary>
        AcceptedByShippingAgent = 5,

        /// <summary>
        /// Taşıma, Yolda olan siparişler
        /// </summary>
        InTransit = 6,

        /// <summary>
        /// Müşteriye Teslim Edilen Siparişler
        /// </summary>
        DeliveredToCustomer = 7,

        /// <summary>
        /// Faturalandırılmış Siparişler
        /// </summary>
        Invoiced = 8,

        /// <summary>
        /// Teslim Edilemeyen Dönen Siparişler
        /// </summary>
        Returned = 9,

        /// <summary>
        /// Tedarik Edilemeyen Siparişler
        /// </summary>
        NotSupplied = 10,

        /// <summary>
        /// İptal Edilen Siparişler
        /// </summary>
        Canceled = 11
        #endregion
    }
}
