﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveStockByBarcode;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Category;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.ProductFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo
{
    public class Morhipo : MarketplaceBase<MorhipoConfiguration>, IBatchable
    {
        #region Constants
        private const string _baseUrl = "https://mpapi.morhipo.com";

        //private const string _baseUrl = "https:/mptest-api.morhipo.com";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Morhipo(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Morhipo;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var token = this._configuration.Authorization;

                if (productRequest.ProductItem.StockItems.Count > 1)
                {
                    var saveProductVariantRequest = new Requests.SaveProductVariant.SaveProductVariantRequest
                    {
                        brand = productRequest.ProductItem.BrandName,
                        categoryName = productRequest.ProductItem.CategoryName,
                        description = productRequest.ProductItem.StockItems[0].Description,
                        productCode = productRequest.ProductItem.SellerCode,
                        productName = productRequest.ProductItem.Name,
                        mainProductCode = productRequest.ProductItem.SellerCode,
                        vatRate = (int)productRequest.ProductItem.StockItems[0].VatRate * 100,
                        variantAttributes = productRequest
                            .ProductItem
                            .StockItems
                            .Select(si => new Requests.SaveProductVariant.VariantAttribute
                            {
                                stockCode = si.StockCode,
                                barcode = si.Barcode,
                                variants = si
                                    .Attributes
                                    .Select(a => new Requests.SaveProductVariant.Variant
                                    {
                                        variantValues = new Requests.SaveProductVariant.VariantValues
                                        {
                                            variantName = a.AttributeName,
                                            variantValue = a.AttributeValueName
                                        }
                                    })
                                    .ToList(),
                                imageList = si
                                    .Images
                                    .Select(i => i.FileName)
                                    .ToList()
                            })
                            .ToList()
                    };

                    var saveProductVariantResponse = this._httpHelper.Post<Requests.SaveProductVariant.SaveProductVariantRequest, Responses.SaveProductVariant.SaveProductVariantResponse>(
                        saveProductVariantRequest,
                        $"{_baseUrl}/mpstore/listing/SaveProduct",
                        "Basic",
                        token, null, null);

                    response.Data = saveProductVariantResponse.trackingId;
                    response.Success = saveProductVariantResponse.code == "200" && saveProductVariantResponse.internalMessage == "OK";
                }
                else
                {
                    var saveProductRequest = new Requests.SaveProduct.SaveProductRequest
                    {
                        items = new()
                        {
                            new Requests.SaveProduct.Item
                            {
                                barcode = productRequest.ProductItem.StockItems[0].Barcode,
                                brand = productRequest.ProductItem.BrandName,
                                cargoCompany = "",
                                stockCode=productRequest.ProductItem.StockItems[0].StockCode,
                                title=productRequest.ProductItem.StockItems[0].Title,
                                productMainId=productRequest.ProductItem.SellerCode,
                                categoryName = productRequest.ProductItem.CategoryName,
                                currencyType = "TRY",
                                description = productRequest.ProductItem.StockItems[0].Description,
                                images = productRequest
                                    .ProductItem
                                    .StockItems[0]
                                    .Images
                                    .Select(i=> new Requests.SaveProduct.Image { url = i.FileName })
                                    .ToList(),
                                vatRate = (productRequest.ProductItem.StockItems[0].VatRate * 100).ToString(),
                                variantAttributes = productRequest
                                    .ProductItem
                                    .StockItems[0]
                                    .Attributes
                                    .Select(a => new Requests.SaveProduct.VariantAttribute
                                    {
                                        attributeName = a.AttributeName,
                                        attributeValue = a.AttributeValueName
                                    })
                                    .ToList()
                            }
                        }
                    };

                    var saveProductResponse = this._httpHelper.Post<Requests.SaveProduct.SaveProductRequest, Responses.SaveProduct.SaveProductResponse>(
                        saveProductRequest,
                        $"{_baseUrl}/mpstore/listing/SaveProduct",
                        "Basic",
                        token, null, null);

                    response.Data = saveProductResponse.trackingId;
                    response.Success = saveProductResponse.code == "200" && saveProductResponse.internalMessage == "OK";
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                var token = this._configuration.Authorization;

                var savePriceByBarcodeRequest = new Requests.SavePriceByBarcode.SavePriceByBarcodeRequest
                {
                    products = new()
                    {
                        new Requests.SavePriceByBarcode.Product
                        {
                            b = stockRequest.PriceStock.Barcode,
                            lp = (double)stockRequest.PriceStock.ListPrice,
                            sp = (double)stockRequest.PriceStock.SalePrice
                        }
                    }
                };

                var savePriceByBarcodeResponse = this._httpHelper.Post<Requests.SavePriceByBarcode.SavePriceByBarcodeRequest, Responses.SaveStockByBarcode.SavePriceByBarcodeResponse>(
                    savePriceByBarcodeRequest,
                    $"{_baseUrl}/mpstore/listing/SavePriceByBarcode",
                    "Basic",
                    token, null, null);

                response.Success = savePriceByBarcodeResponse.code == "200" && savePriceByBarcodeResponse.internalMessage == "OK";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                var token = this._configuration.Authorization;

                var saveStockByBarcodeRequest = new Requests.SaveStockByBarcode.SaveStockByBarcodeRequest
                {
                    products = new()
                    {
                        new Requests.SaveStockByBarcode.Product
                        {
                            b = stockRequest.PriceStock.Barcode,
                            s = stockRequest.PriceStock.Quantity
                        }
                    }
                };

                var saveStockByBarcodeResponse = this._httpHelper.Post<Requests.SaveStockByBarcode.SaveStockByBarcodeRequest, Responses.SavePriceByBarcode.SaveStockByBarcodeResponse>(
                    saveStockByBarcodeRequest,
                    $"{_baseUrl}/mpstore/listing/SaveStockByBarcode",
                    "Basic",
                    token, null, null);

                response.Success = saveStockByBarcodeResponse != null && saveStockByBarcodeResponse.code == "200" && saveStockByBarcodeResponse.internalMessage == "OK";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new List<MarketplaceProductStatus>();


                var authorization = this._configuration.Authorization;

                var pricesList = new List<ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.ProductStatus.MarketplaceProductStatusData>();
                var stockList = new List<ECommerce.Infrastructure.Marketplaces.Morhipo.Responses.ProductStatus.MarketplaceProductStatusData>();



                var skip = 1;
                var total = 0;
                while (true)
                {
                    var prices = this
                    ._httpHelper
                    .Get<Responses.ProductStatus.MarketplaceProductStatus>($"{_baseUrl}/mpstore/listing/prices?Skip={skip}", "Basic", authorization);

                    if (prices != null && prices.internalMessage == "OK" && prices.data.Count > 0)
                    {
                        pricesList.AddRange(prices.data);

                        if (pricesList.Count == prices.total) break;

                        skip++;
                    }
                    else
                        break;
                }

                skip = 1;
                total = 0;
                while (true)
                {
                    var stocks = this
                                    ._httpHelper
                                    .Get<Responses.ProductStatus.MarketplaceProductStatus>($"{_baseUrl}/mpstore/listing/stocks?Skip={skip}", "Basic", authorization);

                    if (stocks != null && stocks.internalMessage == "OK" && stocks.data.Count > 0)
                    {
                        stockList.AddRange(stocks.data);
                        if (stockList.Count == stocks.total) break;
                        skip++;
                    }
                    else
                        break;
                }




                if (pricesList.Count > 0)
                {

                    foreach (var pLoop in pricesList)
                    {

                        var stok = stockList.FirstOrDefault(x => x.productId == pLoop.productId);

                        var marketplaceProductStatus = new MarketplaceProductStatus
                        {
                            MarketplaceId = this.MarketplaceId,
                            MarketplaceProductId = pLoop.productId.ToString(),
                            Barcode = pLoop.barcode,
                            StockCode = pLoop.merchantProductId,

                            Opened = true,
                            OnSale = stok != null && stok.quantity > 0 && pLoop.salePrice > 0 && pLoop.listPrice > 0,
                            Locked = false,

                            Url = $"https://www.morhipo.com/{pLoop.productId}/{pLoop.productId}/detay",
                            ListUnitPrice = (decimal)pLoop.listPrice,
                            UnitPrice = (decimal)pLoop.salePrice,
                            Stock = stok != null ? stok.quantity : 0
                        };
                        response.Data.Add(marketplaceProductStatus);

                    }
                }

                response.Success = true;
            });
        }

        public DataResponse<bool> StockCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>(response =>
            {
                var token = this._configuration.Authorization;

                this._httpHelper.Post<Requests.Tracking.TrackingRequest, Responses.Tracking.TrackingResponse>(
                    new Requests.Tracking.TrackingRequest
                    {
                        TrackingId = batchId
                    },
                    $"{_baseUrl}/tracking/Status",
                    "Basic",
                    token, null, null);
            });
        }

        public DataResponse<bool> PriceCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>(response =>
            {
                var token = this._configuration.Authorization;

                this._httpHelper.Post<Requests.Tracking.TrackingRequest, Responses.Tracking.TrackingResponse>(
                    new Requests.Tracking.TrackingRequest
                    {
                        TrackingId = batchId
                    },
                    $"{_baseUrl}/tracking/Status",
                    "Basic",
                    token, null, null);
            });
        }

        public DataResponse<bool> CreateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>(response =>
            {
                var token = this._configuration.Authorization;

                this._httpHelper.Post<Requests.Tracking.TrackingRequest, Responses.Tracking.TrackingResponse>(
                    new Requests.Tracking.TrackingRequest
                    {
                        TrackingId = batchId
                    },
                    $"{_baseUrl}/tracking/Status",
                    "Basic",
                    token, null, null);
            });
        }

        public DataResponse<bool> UpdateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>(response =>
            {
                var token = this._configuration.Authorization;

                this._httpHelper.Post<Requests.Tracking.TrackingRequest, Responses.Tracking.TrackingResponse>(
                    new Requests.Tracking.TrackingRequest
                    {
                        TrackingId = batchId
                    },
                    $"{_baseUrl}/tracking/Status",
                    "Basic",
                    token, null, null);
            });
        }
        #endregion

        #region Category Methods
        /// <summary>
        /// Morhipo Kategori göndermediği için biz trendyolun kategorilerini çekiyoruz.
        /// Morhiponun çalışma mantığında marka bazlı ürün açmak var biz sistem gereği kategori ekliyoruz.
        /// </summary>
        /// <param name="configurations"></param>
        /// <returns></returns>
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {


                var authorization = this._configuration.Authorization;

                CategoryRoot root = _httpHelper.Get<CategoryRoot>($"https://api.trendyol.com/sapigw/product-categories", "Basic", authorization);

                root.categories = root.categories.Where(x => x.id == 368 || x.id == 403 || x.id == 522 || x.id == 1219).ToList();


                var categoryAttributeRoots = new List<CategoryAttributeRoot>();

                if (root != null)
                {
                    foreach (var category in root.categories)
                    {

                        if (category != null)
                        {
                            categoryAttributeRoots.AddRange(Recursive(category, authorization));
                        }
                    }
                }

                var data = categoryAttributeRoots.Where(x => x != null).Select(x => new CategoryResponse
                {
                    Code = x.id.ToString(),
                    Name = x.name,
                    CategoryAttributes = x?.categoryAttributes?.Select(ca => new CategoryAttributeResponse
                    {
                        CategoryCode = x.id.ToString(),
                        Code = ca.attribute.id.ToString(),
                        Name = ca.attribute.name,
                        Mandatory = ca.required,
                        AllowCustom = ca.allowCustom,
                        CategoryAttributesValues = ca?.attributeValues?.Select(av => new CategoryAttributeValueResponse
                        {
                            VarinatCode = ca.attribute.id.ToString(),
                            Code = av.id.ToString(),
                            Name = av.name
                        }).ToList()
                    }).ToList()
                }).ToList();


                response.Data = data;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse();

                var authorization = this._configuration.Authorization;

                var orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/List?statusId=1", "Basic", authorization);

                if (orderListResponse.internalMessage == "OK" && orderListResponse.data.Count > 0)
                {
                    response.Data = new()
                    {
                        Orders = orderListResponse
                            .data
                            .Select(d => new Application.Common.Wrappers.Marketplaces.Order.Order
                            {
                                UId = $"{Application.Common.Constants.Marketplaces.Morhipo}_{d.orderId}",
                                OrderCode = d.orderId.ToString(),
                                TotalAmount = Convert.ToDecimal(d.grossAmount),
                                CargoFee = 0,
                                OrderDate = d.date,
                                Discount = Convert.ToDecimal(d.discountAmount),
                                ListTotalAmount = d
                                    .items.Sum(x => Convert.ToDecimal(x.unitPrice)),
                                Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
                                {
                                    FirstName = d.shipmentAddress.name,
                                    LastName = d.shipmentAddress.name,
                                    Mail = d.shopperEmail,
                                    Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                                    TaxNumber = d.invoiceAddress.taxnumber,
                                    TaxAdministration = d.invoiceAddress.taxoffice,
                                },
                                OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress
                                {
                                    City = d.shipmentAddress.city,
                                    Address = d.shipmentAddress.address,
                                    Country = "Türkiye",
                                    District = d.shipmentAddress.district,
                                    Email = d.shopperEmail,
                                    FirstName = d.shipmentAddress.name,
                                    LastName = d.shipmentAddress.name,
                                    Neighborhood = String.Empty,
                                    Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                                },
                                MarketplaceId = Application.Common.Constants.Marketplaces.Morhipo,
                                OrderTypeId = "OS",
                                Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>(),
                                OrderNote = String.Empty,
                                OrderPicking = new Application.Common.Parameters.Order.OrderPicking
                                {
                                    MarketPlaceOrderId = d.orderId.ToString(),
                                    OrderDetails = new List<Application.Common.Parameters.Order.OrderPickingDetail>()

                                },
                                OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress
                                {
                                    Address = d.invoiceAddress.address,
                                    City = d.invoiceAddress.city,
                                    Country = "Türkiye",
                                    District = d.invoiceAddress.district,
                                    Email = d.shopperEmail,
                                    FirstName = d.invoiceAddress.name,
                                    LastName = d.invoiceAddress.name,
                                    Neighborhood = String.Empty,
                                    Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                                    TaxNumber = d.invoiceAddress.taxnumber,
                                    TaxOffice = d.invoiceAddress.taxoffice
                                },
                                OrderSourceId = OrderSources.Pazaryeri,
                                OrderDetails = d
                                    .items
                                    .Select(i => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                    {
                                        UId = i.packageId,
                                        ListPrice = Convert.ToDecimal(i.unitPrice),
                                        UnitPrice = Convert.ToDecimal(i.unitPrice),
                                        Payor = true,
                                        Quantity = i.quantity,
                                        TaxRate = int.Parse(i.vatcode) / 100d,
                                        Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                        {
                                            Barcode = i.barcode,
                                            StockCode = i.merchantsku,
                                            Name = i.productDescription
                                        }
                                    })
                                    .ToList(),
                                OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                                {
                                    PackageNumber = d.packages[0].packageId,
                                    ShipmentCompanyId = d.packages[0].shippingPartner, //BURASI GELEN DATAYA GÖRE BIZIM ShipmentCompanyId'YE GORE DEGISECEK
                                    TrackingUrl = d.packages[0].trackingUrl,
                                    TrackingCode = d.packages[0].trackingCode,
                                }
                            })
                            .ToList()
                    };

                    Parallel.ForEach(response.Data.Orders, theOrder =>
                    {
                        theOrder.UId = $"M_{theOrder.OrderCode}";
                    });
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>();
                var token = this._configuration.Authorization;

                var orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/List?statusId=1", "Basic", token);

                if (orderListResponse != null && orderListResponse.internalMessage == "OK" && orderListResponse.data.Count > 0)
                {
                    foreach (var data in orderListResponse.data)
                    {

                        try
                        {
                            var request = new Requests.OrderFullFill.OrderFullFillRequest()
                            {
                                orderId = data.orderId,
                                items = new List<int>()
                            };

                            var fullFill = this._httpHelper.Post<List<Requests.OrderFullFill.OrderFullFillRequest>, Responses.OrderFullFill.OrderFullFillResponse>(new List<Requests.OrderFullFill.OrderFullFillRequest> { request }, $"{_baseUrl}/mpstore/order/Fullfill", "Basic", token, null, null);


                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }


                orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/List?statusId=2", "Basic", token);


                if (orderListResponse != null && orderListResponse.internalMessage == "OK" && orderListResponse.data.Count > 0)
                {
                    foreach (var data in orderListResponse.data)
                    {

                        try
                        {




                            var request = new Requests.OrderPack.OrderPackRequest()
                            {
                                packageId = data.packages[0].packageId,
                                pieceCount = 0,
                                content = data.items.Select(x => new Requests.OrderPack.OrderPackContent
                                {
                                    unit = x.unit,
                                    itemId = x.itemid,
                                    quantity = x.quantity

                                }).ToList()
                            };


                            var pack = this._httpHelper.Post<List<Requests.OrderPack.OrderPackRequest>, Responses.OrderPack.OrderPackResponse>(new List<Requests.OrderPack.OrderPackRequest> { request }, $"{_baseUrl}/mpstore/order/Pack", "Basic", token, null, null);


                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }

                orderListResponse = this
                  ._httpHelper
                  .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/List?statusId=3", "Basic", token);
                if (orderListResponse != null && orderListResponse.internalMessage == "OK" && orderListResponse.data.Count > 0)
                {
                    var orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>();

                    foreach (var d in orderListResponse.data)
                    {

                        var invoiceAddressName = d.invoiceAddress.name.Split(new String[] { " " }, StringSplitOptions.None);
                        var shipmentAddressName = d.shipmentAddress.name.Split(new String[] { " " }, StringSplitOptions.None);

                        var order = new Application.Common.Wrappers.Marketplaces.Order.Order
                        {
                            OrderCode = d.orderId.ToString(),
                            TotalAmount = Convert.ToDecimal(d.grossAmount),
                            CargoFee = 0,
                            OrderDate = d.date,
                            Discount = Convert.ToDecimal(d.discountAmount),
                            ListTotalAmount = d
                                    .items.Sum(x => Convert.ToDecimal(x.unitPrice)),
                            Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
                            {
                                FirstName = shipmentAddressName[0],
                                LastName = shipmentAddressName.Length > 1 ? shipmentAddressName[1] : shipmentAddressName[0],
                                Mail = d.shopperEmail,
                                Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                                TaxNumber = d.invoiceAddress.taxnumber,
                                TaxAdministration = d.invoiceAddress.taxoffice,
                            },
                            OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress
                            {
                                City = d.shipmentAddress.city,
                                Address = d.shipmentAddress.address,
                                Country = "Türkiye",
                                District = d.shipmentAddress.district,
                                Email = d.shopperEmail,
                                FirstName = d.shipmentAddress.name,
                                LastName = d.shipmentAddress.name,
                                Neighborhood = String.Empty,
                                Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                            },
                            MarketplaceId = Application.Common.Constants.Marketplaces.Morhipo,
                            OrderTypeId = "OS",
                            Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>(),
                            OrderNote = String.Empty,
                            OrderPicking = new Application.Common.Parameters.Order.OrderPicking
                            {
                                MarketPlaceOrderId = d.orderId.ToString(),
                                OrderDetails = new List<Application.Common.Parameters.Order.OrderPickingDetail>()

                            },
                            OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress
                            {
                                Address = d.invoiceAddress.address,
                                City = d.invoiceAddress.city,
                                Country = "Türkiye",
                                District = d.invoiceAddress.district,
                                Email = d.shopperEmail,
                                FirstName = invoiceAddressName[0],
                                LastName = invoiceAddressName.Length > 1 ? invoiceAddressName[1] : "",
                                Neighborhood = String.Empty,
                                Phone = d.shipmentAddress.phoneNumber.Replace(" ", "").Replace("(", "").Replace(")", ""),
                                TaxNumber = d.invoiceAddress.taxnumber,
                                TaxOffice = d.invoiceAddress.taxoffice
                            },
                            OrderSourceId = OrderSources.Pazaryeri,
                            OrderDetails = d
                                    .items
                                    .Select(i => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                    {
                                        ListPrice = Convert.ToDecimal(i.unitPrice),
                                        UnitPrice = Convert.ToDecimal(i.unitPrice),
                                        Payor = true,
                                        Quantity = i.quantity,
                                        TaxRate = int.Parse(i.vatcode) / 100d,
                                        Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                        {
                                            Barcode = i.barcode,
                                            StockCode = i.merchantsku,
                                            Name = i.productDescription
                                        }
                                    })
                                    .ToList(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                PackageNumber = d.packages.Count > 0 ? d.packages[0].packageId : "",
                                ShipmentCompanyId = "Y",
                                TrackingUrl = d.packages.Count > 0 ? d.packages[0].trackingUrl : "",
                                TrackingCode = d.packages.Count > 0 ? d.packages[0].packageId : "",
                            }
                        };

                        orders.Add(order);
                    }

                    response.Data = new()
                    {
                        Orders = orders
                    };
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var token = this._configuration.Authorization;

                var orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/status?statusId=10,11", "Basic", token);

                response.Data = new()
                {
                    Orders = orderListResponse
                        .data
                        .Select(d => new Application.Common.Wrappers.Marketplaces.Order.Order
                        {
                            OrderCode = d.orderId.ToString(),
                            OrderDetails = d
                                .items
                                .Select(i => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                {
                                    ListPrice = Convert.ToDecimal(i.unitPrice),
                                    UnitPrice = Convert.ToDecimal(i.unitPrice),
                                    Payor = true,
                                    Quantity = i.quantity,
                                    TaxRate = int.Parse(i.vatcode) / 100d,
                                    Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                    {
                                        Barcode = i.barcode,
                                        StockCode = i.merchantsku,
                                        Name = i.productDescription
                                    }
                                })
                                .ToList(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                PackageNumber = d.packages[0].packageId,
                                ShipmentCompanyId = d.packages[0].shippingPartner,
                                TrackingUrl = d.packages[0].trackingUrl,
                                TrackingCode = d.packages[0].trackingCode
                            }
                        })
                        .ToList()
                };
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var token = this._configuration.Authorization;

                var orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/status?statusId=7", "Basic", token);

                response.Data = new()
                {
                    Orders = orderListResponse
                        .data
                        .Select(d => new Application.Common.Wrappers.Marketplaces.Order.Order
                        {
                            OrderCode = d.orderId.ToString(),
                            OrderDetails = d
                                .items
                                .Select(i => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                {
                                    ListPrice = Convert.ToDecimal(i.unitPrice),
                                    UnitPrice = Convert.ToDecimal(i.unitPrice),
                                    Payor = true,
                                    Quantity = i.quantity,
                                    TaxRate = int.Parse(i.vatcode) / 100d,
                                    Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                    {
                                        Barcode = i.barcode,
                                        StockCode = i.merchantsku,
                                        Name = i.productDescription
                                    }
                                })
                                .ToList(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                PackageNumber = d.packages[0].packageId,
                                ShipmentCompanyId = d.packages[0].shippingPartner,
                                TrackingUrl = d.packages[0].trackingUrl,
                                TrackingCode = d.packages[0].trackingCode
                            }
                        })
                        .ToList()
                };
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var token = this._configuration.Authorization;

                var orderListResponse = this
                    ._httpHelper
                    .Get<Responses.OrderList.OrderListResponse>($"{_baseUrl}/mpstore/order/status?statusId=9", "Basic", token);

                response.Data = new()
                {
                    Orders = orderListResponse
                        .data
                        .Select(d => new Application.Common.Wrappers.Marketplaces.Order.Order
                        {
                            OrderCode = d.orderId.ToString(),
                            OrderDetails = d
                                .items
                                .Select(i => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                {
                                    ListPrice = Convert.ToDecimal(i.unitPrice),
                                    UnitPrice = Convert.ToDecimal(i.unitPrice),
                                    Payor = true,
                                    Quantity = i.quantity,
                                    TaxRate = int.Parse(i.vatcode) / 100d,
                                    Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                    {
                                        Barcode = i.barcode,
                                        StockCode = i.merchantsku,
                                        Name = i.productDescription
                                    }
                                })
                                .ToList(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                PackageNumber = d.packages[0].packageId,
                                ShipmentCompanyId = d.packages[0].shippingPartner,
                                TrackingUrl = d.packages[0].trackingUrl,
                                TrackingCode = d.packages[0].trackingCode
                            }
                        })
                        .ToList()
                };
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {

            return new Response { Success = true };
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(MorhipoConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }
        #endregion

        #region Helper Methods
        private List<CategoryAttributeRoot> Recursive(SubCategory category, string authorization)
        {
            var categoryAttributeRoots = new List<CategoryAttributeRoot>();

            if (category.subCategories == null || category.subCategories.Count == 0)
            {

                var categoryAttributeRoot = _httpHelper.Get<CategoryAttributeRoot>($"https://api.trendyol.com/sapigw/product-categories/{category.id}/attributes", "Basic", authorization);

                if (categoryAttributeRoot != null)
                    categoryAttributeRoots.Add(categoryAttributeRoot);
            }

            foreach (var scLoop in category.subCategories)
            {
                categoryAttributeRoots.AddRange(Recursive(scLoop, authorization));
            }

            return categoryAttributeRoots;
        }
        #endregion
    }
}