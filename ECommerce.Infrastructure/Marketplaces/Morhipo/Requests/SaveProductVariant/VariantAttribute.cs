using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProductVariant{ 

    public class VariantAttribute
    {
        public string stockCode { get; set; }
        public string barcode { get; set; }
        public List<Variant> variants { get; set; }
        public List<string> imageList { get; set; }
    }

}