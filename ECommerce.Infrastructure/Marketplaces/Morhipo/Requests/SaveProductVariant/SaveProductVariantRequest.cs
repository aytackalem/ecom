using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProductVariant
{
    public class SaveProductVariantRequest
    {
        #region Properties
        public string brand { get; set; }

        public string categoryName { get; set; }

        public string description { get; set; }

        public string productCode { get; set; }

        public string mainProductCode { get; set; }

        public string productName { get; set; }

        public int vatRate { get; set; }
        #endregion

        #region Navigation Properties
        public List<Attribute> attributes { get; set; }

        public List<VariantAttribute> variantAttributes { get; set; }
        #endregion
    }
}