namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProductVariant{ 

    public class VariantValues
    {
        public string variantName { get; set; }
        public string variantValue { get; set; }
    }

}