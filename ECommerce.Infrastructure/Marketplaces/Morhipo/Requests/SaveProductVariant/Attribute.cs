namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProductVariant{ 

    public class Attribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }

}