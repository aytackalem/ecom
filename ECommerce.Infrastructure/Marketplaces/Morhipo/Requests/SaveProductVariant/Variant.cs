namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProductVariant{ 

    public class Variant
    {
        public VariantValues variantValues { get; set; }
    }

}