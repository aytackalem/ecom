﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.Tracking
{
    public class TrackingRequest
    {
        #region Properties
        public string TrackingId { get; set; } 
        #endregion
    }
}
