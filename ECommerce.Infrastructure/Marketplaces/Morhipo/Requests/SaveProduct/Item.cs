using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProduct{ 

    public class Item
    {
        public string productMainId { get; set; }
        public string stockCode { get; set; }
        public string title { get; set; }
        public string barcode { get; set; }
        public string brand { get; set; }
        public string cargoCompany { get; set; }
        public string categoryName { get; set; }
        public string currencyType { get; set; }
        public string description { get; set; }
        public List<Image> images { get; set; }
        public List<VariantAttribute> variantAttributes { get; set; }
        public string vatRate { get; set; }
    }

}