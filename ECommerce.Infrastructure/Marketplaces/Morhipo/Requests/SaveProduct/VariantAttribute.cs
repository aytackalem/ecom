namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProduct{ 

    public class VariantAttribute
    {
        public string attributeName { get; set; }
        public string attributeValue { get; set; }
    }

}