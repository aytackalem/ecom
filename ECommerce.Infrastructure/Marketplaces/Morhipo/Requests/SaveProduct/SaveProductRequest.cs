using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveProduct
{
    public class SaveProductRequest
    {
        #region Navigation Properties
        public List<Item> items { get; set; }
        #endregion
    }
}