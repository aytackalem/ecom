using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SavePriceByBarcode
{
    public class SavePriceByBarcodeRequest
    {
        #region Navigation Properties
        public List<Product> products { get; set; }
        #endregion
    }
}