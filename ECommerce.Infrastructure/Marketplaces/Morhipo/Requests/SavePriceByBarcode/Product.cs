namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SavePriceByBarcode{ 

    public class Product
    {
        public double lp { get; set; }
        public double sp { get; set; }
        public string b { get; set; }
    }

}