﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.OrderFullFill
{
    public class OrderFullFillRequest
    {

        #region Properties
        public int orderId { get; set; }

        public List<int> items { get; set; }
        #endregion
    }
}
