using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.SaveStockByBarcode
{
    public class SaveStockByBarcodeRequest
    {
        #region Navigation Properties
        public List<Product> products { get; set; }
        #endregion
    }
}