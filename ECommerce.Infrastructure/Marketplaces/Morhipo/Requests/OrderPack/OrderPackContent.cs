﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo.Requests.OrderPack
{
    public class OrderPackContent
    {
        #region Properties
        public int itemId { get; set; }
        
        public int quantity { get; set; }
        
        public string unit { get; set; } 
        #endregion
    }
}
