﻿namespace ECommerce.Infrastructure.Marketplaces.Morhipo
{
    public enum PackageTypes
    {
        #region Members
        /// <summary>
        /// Tanımsız, Boş
        /// </summary>
        Unassigned = -1,

        /// <summary>
        /// Ön/Tanımlı Rezerve Edilen Paketler
        /// </summary>
        PreDefined = 0,

        /// <summary>
        /// Yeni Paketler
        /// </summary>
        New = 1,

        /// <summary>
        /// Kargoya Teslim Edilen Paketler
        /// </summary>
        ShippedToShippingAgent = 2,

        /// <summary>
        /// Kargoya Firmasınca Alınan Paketler
        /// </summary>
        OnShippingAgent = 3,

        /// <summary>
        /// Taşımada Aşamasınd Paketler
        /// </summary>
        CaseOfTransport = 4,

        /// <summary>
        /// Teslim Edildi
        /// </summary>
        DeliveredToCustomer = 5,

        /// <summary>
        /// İade Edilen Paketler
        /// </summary>
        Returned = 6,

        /// <summary>
        /// Silinen Paketler
        /// </summary>
        DeletedToPackage = 7
        #endregion
    }
}
