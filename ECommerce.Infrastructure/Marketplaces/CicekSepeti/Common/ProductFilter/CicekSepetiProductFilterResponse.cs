﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.ProductFilter
{
    public class CicekSepetiProductFilterResponse
    {
        public int totalCount { get; set; }
        public List<CicekSepetiProductFilterProduct> products { get; set; }
    }
}
