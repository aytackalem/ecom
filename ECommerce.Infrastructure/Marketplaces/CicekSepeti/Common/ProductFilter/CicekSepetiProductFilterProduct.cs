﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.ProductFilter
{
    public class CicekSepetiProductFilterProduct
    {
        public string productName { get; set; }
        public string productCode { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public string stockCode { get; set; }
        public string mainProductCode { get; set; }
        public string productStatusType { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public object mediaLink { get; set; }
        public int deliverType { get; set; }
        public int deliveryMessageType { get; set; }
        public bool isUseStockQuantity { get; set; }
        public int StockQuantity { get; set; }
        public double salesPrice { get; set; }
        public string barcode { get; set; }
        public List<string> images { get; set; }
        public List<CicekSepetiProductFilterAttribute> attributes { get; set; }
        public int? deliveryType { get; set; }
        public int? stock { get; set; }
        public bool isActive { get; set; }
    }
}
