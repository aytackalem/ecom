﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti
{
    public class CicekSepetiStockRequest
    {
        public List<CicekSepetiStockItem> items { get; set; }

    }
}
