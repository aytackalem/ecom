﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti
{
    /// <summary>
    /// Stok ve fiyat güncelleme işlemi için istek gönderildiğinde sıraya alınır ve batch kontrolünde statü Pending olarak döner. Güncelleme işlemi maksimum 4 saat içinde tamamlanır.
    /// </summary>
    public class CicekSepetiStockResponse
    {
        public string batchId { get; set; }

    }
}
