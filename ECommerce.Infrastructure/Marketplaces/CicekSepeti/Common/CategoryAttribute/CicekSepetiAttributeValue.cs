﻿namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.CategoryAttribute
{
    public class CicekSepetiAttributeValue
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
