﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.CategoryAttribute
{
    public class CicekSepetiCategory
    {
        #region Properties
        public int CategoryId { get; set; }
        
        public string CategoryName { get; set; }

        public List<CicekSepetiCategoryAttribute> CategoryAttributes { get; set; }
        #endregion
    }
}
