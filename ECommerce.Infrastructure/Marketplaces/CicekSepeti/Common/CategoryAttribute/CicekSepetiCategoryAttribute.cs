﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.CategoryAttribute
{
    public class CicekSepetiCategoryAttribute
    {
        #region Properties
        public int AttributeId { get; set; }

        public string AttributeName { get; set; }

        public bool Required { get; set; }

        public bool Varianter { get; set; }

        public string Type { get; set; }

        public List<CicekSepetiAttributeValue> AttributeValues { get; set; }
        #endregion
    }
}
