﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Product
{
    public class CicekSepetiAttribute
    {
        public int id { get; set; }
        public int valueId { get; set; }
        public int textLength { get; set; }
    }
}
