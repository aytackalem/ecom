﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Product
{
    public class CicekSepetiProductUpdateRequest
    {
        public List<CicekSepetiProductUpdate> products { get; set; }

    }
}
