﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Product
{
    public class CicekSepetiBatchStatusResponse
    {
        public string batchId { get; set; }
        public int itemCount { get; set; }
        public List<CicekSepetiBatch> items { get; set; }
    }

    public class CicekSepetiBatch
    {
       
        public string itemId { get; set; }
        public string status { get; set; }
        public List<CicekSepetiBatchFailureReason> failureReasons { get; set; }
        public DateTime lastModificationDate { get; set; }
    }

    public class CicekSepetiBatchFailureReason
    {
        public string message { get; set; }
        public int code { get; set; }
    }
}
