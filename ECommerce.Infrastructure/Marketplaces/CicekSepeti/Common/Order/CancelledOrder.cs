﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order
{
    public class CancelledOrder
    {
        public int orderId { get; set; }
        public int orderItemId { get; set; }
        public string customerName { get; set; }
        public double price { get; set; }
        public string orderItemStatus { get; set; }
        public string cancelReason { get; set; }
        public string subCancelReason { get; set; }
        public int cancelReasonId { get; set; }
        public int subCancelReasonId { get; set; }
        public string orderItemCancelStatus { get; set; }
        public string cargoCompany { get; set; }
        public object shipmentTrackingUrl { get; set; }
        public object shipmentNumber { get; set; }
        public string partialNumber { get; set; }
        public string variantName { get; set; }
        public object supplierProductVariantCode { get; set; }
        public string productName { get; set; }
        public string productCode { get; set; }
        public object supplierProductCode { get; set; }
        public List<Text> texts { get; set; }
    }

    public class Text
    {
        public string name { get; set; }
        public string textValue { get; set; }
    }
}
