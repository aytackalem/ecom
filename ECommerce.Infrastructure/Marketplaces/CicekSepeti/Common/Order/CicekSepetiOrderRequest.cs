﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order
{
    public class CicekSepetiOrderRequest
    {
        #region Properties
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int PageSize { get; set; }

        public int Page { get; set; }

        public int StatusId { get; set; }

        public int? OrderNo { get; set; }
        #endregion
    }
}
