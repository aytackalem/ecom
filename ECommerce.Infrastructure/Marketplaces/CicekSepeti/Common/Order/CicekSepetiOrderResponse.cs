﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order
{
    public class CicekSepetiOrderResponse
    {
        #region Property
        public int orderListCount { get; set; }

        public int pageCount { get; set; }

        public List<CicekSepetiSupplierOrderListWithBranch> supplierOrderListWithBranch { get; set; }
        #endregion
    }
}
