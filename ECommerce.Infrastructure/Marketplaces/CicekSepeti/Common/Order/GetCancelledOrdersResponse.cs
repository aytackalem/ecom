﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order
{
    public class GetCancelledOrdersResponse
    {
        #region Properties
        public int pageCount { get; set; }
        #endregion

        #region Navigation Properties
        public List<CancelledOrder> orderItemList { get; set; }
        #endregion
    }
}
