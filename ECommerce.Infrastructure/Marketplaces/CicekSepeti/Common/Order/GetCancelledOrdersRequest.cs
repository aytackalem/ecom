﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order
{
    public class GetCancelledOrdersRequest
    {
        #region Properties
        public int PageSize { get; set; }

        public int Page { get; set; }
        #endregion
    }
}
