﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Cargo
{
    public class CicekSepetiCargoRequest
    {
        #region Property
        public List<OrderItemsGroup> orderItemsGroup { get; set; }
        #endregion
    }

    public class OrderItemsGroup
    {
        #region Property
        public List<int> orderItemIds { get; set; }
        #endregion
    }
}
