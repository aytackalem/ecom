﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Cargo
{
    public class CicekSepetiStatusUpdateResponse
    {
        #region Property
        public List<CicekSepetiOrderItem> orderItems { get; set; }

        public bool isSuccess { get; set; }

        public string message { get; set; }
        #endregion
    }
}
