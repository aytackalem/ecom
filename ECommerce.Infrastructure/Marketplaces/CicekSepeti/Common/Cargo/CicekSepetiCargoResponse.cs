﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Cargo
{
    public class CicekSepetiCargoResponse
    {
        #region Field
        public List<CicekSepetiStatusUpdateResponse> statusUpdateResponse { get; set; }
        #endregion
    }

}
