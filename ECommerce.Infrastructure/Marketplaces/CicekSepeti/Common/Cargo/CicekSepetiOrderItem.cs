﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Cargo
{
    public class CicekSepetiOrderItem
    {
        #region Property
        public int orderItemId { get; set; }

        public string partialNumber { get; set; }

        public string cargoCompany { get; set; }
        #endregion
    }
}
