﻿using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.BatchStatus
{
    public class BatchStatusResponse
    {
        public string batchId { get; set; }
        public int itemCount { get; set; }
        public List<Item> items { get; set; }
    }

    public class Data
    {
        public string siteCode { get; set; }
        public string stockCode { get; set; }
        public int quantity { get; set; }
        public decimal? listPrice { get; set; }
        public decimal? salesPrice { get; set; }
    }

    public class FailureReason
    {
        public string message { get; set; }
        public int? code { get; set; }
    }

    public class Item
    {
        public string itemId { get; set; }
        public Data data { get; set; }
        public string status { get; set; }
        public List<FailureReason> failureReasons { get; set; }
        public DateTime lastModificationDate { get; set; }
    }
}
