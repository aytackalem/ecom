﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.BatchStatus;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Cargo;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Category;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Order;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.Product;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti.Common.ProductFilter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.CicekSepeti
{
    public class CicekSepeti : MarketplaceBase<CicekSepetiConfiguration>, IBatchable
    {
        #region Constants
        public const string _baseUrl = "https://apis.ciceksepeti.com/api";
        #endregion

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public CicekSepeti(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var createRequest = new CicekSepetiProductCreateRequest();
                createRequest.products = new List<CicekSepetiProductCreate>();

                var updateRequest = new CicekSepetiProductUpdateRequest();
                updateRequest.products = new List<CicekSepetiProductUpdate>();

                #region BrandCategoryAttribute
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                CategoryAttribute categoryAttribute = null;
                var categoryAttributeUrl = $"{_baseUrl}/v1/categories/{productRequest.ProductItem.CategoryId}/attributes";
                var cicekSepetiCategory = this._httpHelper.Get<Common.CategoryAttribute.CicekSepetiCategory>(categoryAttributeUrl, null, null, customHeaders);
                var _brandCategoryAttribute = cicekSepetiCategory
                        .CategoryAttributes.FirstOrDefault(x => x.AttributeName == "Marka");
                if (_brandCategoryAttribute != null)
                {
                    var _brandAttributeValue = _brandCategoryAttribute.AttributeValues.FirstOrDefault(x => x.Name.ReplaceChar() == productRequest.ProductItem.BrandName.ReplaceChar());
                    if (_brandAttributeValue != null)
                    {
                        categoryAttribute = new Application.Common.Parameters.Product.CategoryAttribute
                        {
                            AttributeCode = _brandCategoryAttribute.AttributeId.ToString(),
                            AttributeValueCode = _brandAttributeValue.Id.ToString()

                        };
                    }
                    else
                    {

                    }
                }
                #endregion

                foreach (var stLoop in productRequest.ProductItem.StockItems)
                {
                    CultureInfo culture = new CultureInfo("en-US");

                    var filterResponse = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Stockcode = stLoop.StockCode
                    });

                    if (filterResponse.Success)
                    {
                        if (filterResponse.Data.totalCount == 1) //Ürün güncelleme
                        {
                            ///NOT
                            ///Ürünlerin attributes bilgisi güncellemeleri api üzerinden yapılamamaktadır. Güncellemeler için destek ekibine talepte bulunmanız gerekmektedir. Güncellenmek istenen kayıt bir varyant özellik ise, güncelleme yerine yeni varyant oluşturabilirsiniz. Ancak ilgili kayıt dinamik bir özellikse güncelleme yapabilirsiniz. Örneğin bileklik üzerinde yazılacak yazı karakteri için textLength 25 den 23'e düşürmek için güncelleme metodunu kullanabilirsiniz. Kategori Özellik Listesi metodunu inceleyebilirsiniz.
                            if (stLoop.Description.Length < 10) stLoop.Description += $" {stLoop.Title} ({stLoop.StockCode})";

                            updateRequest.products.Add(new CicekSepetiProductUpdate
                            {
                                productName = stLoop.Title,
                                barcode = stLoop.Barcode,
                                description = stLoop.Description,
                                images = stLoop.Images.Take(5).Select(x => x.Url).ToList(),
                                mediaLink = null,
                                deliveryMessageType = 5,
                                deliveryType = 2,
                                stockCode = stLoop.StockCode,
                                isActive = stLoop.Active,
                                mainProductCode = productRequest.ProductItem.SellerCode,
                                attributes = null,
                            });


                        }
                        else  //Yeni Ürün Yükeleme Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                        {

                            if (categoryAttribute != null)
                            {
                                stLoop.Attributes.Add(categoryAttribute);
                            }



                            createRequest.products.Add(new CicekSepetiProductCreate
                            {
                                productName = stLoop.Title,
                                barcode = stLoop.Barcode,
                                categoryId = Convert.ToInt32(productRequest.ProductItem.CategoryId),
                                description = stLoop.Description,
                                images = stLoop.Images.Take(5).Select(x => x.Url).ToList(),
                                mediaLink = null,
                                deliveryMessageType = 5,
                                deliveryType = 2,
                                stockQuantity = stLoop.Quantity,
                                salesPrice = Convert.ToDouble(stLoop.SalePrice, culture),
                                listPrice = Convert.ToDouble(stLoop.ListPrice, culture),
                                stockCode = stLoop.StockCode,
                                mainProductCode = productRequest.ProductItem.SellerCode,
                                attributes = stLoop.Attributes.Select(x => new CicekSepetiAttribute
                                {
                                    id = Convert.ToInt32(x.AttributeCode),
                                    valueId = Convert.ToInt32(x.AttributeValueCode),
                                    textLength = 0
                                }).ToList()

                            });

                        }
                    }

                }
                var failedStatus = false;

                if (createRequest.products.Count > 0)
                {
                    var productCreateResponse = _httpHelper
                        .Post<CicekSepetiProductCreateRequest, CicekSepetiProductResponse>(
                            createRequest,
                            $"{_baseUrl}/v1/Products",
                            null,
                            null,
                            new Dictionary<string, string> {
                                { "x-api-key", this._configuration.ApiKey }
                            });

                    if (productCreateResponse == null || productCreateResponse.batchId == null)
                    {
                        failedStatus = true;
                    }
                    else
                        response.Data = productCreateResponse.batchId;
                }

                if (updateRequest.products.Count > 0)
                {
                    var productUpdateResponse = _httpHelper.Put<CicekSepetiProductUpdateRequest, CicekSepetiProductResponse>(updateRequest, $"{_baseUrl}/v1/Products", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }
                    });
                    if (productUpdateResponse == null)
                        failedStatus = true;
                    else
                        response.Data = productUpdateResponse.batchId;
                }

                response.Success = !failedStatus;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                CultureInfo culture = new CultureInfo("en-US");

                var request = new CicekSepetiStockRequest();
                request.items = new List<CicekSepetiStockItem>();
                request.items.Add(new CicekSepetiStockItem
                {
                    listPrice = stockRequest.PriceStock.SalePrice.HasValue ? Convert.ToDouble(stockRequest.PriceStock.ListPrice, culture) : null,
                    salesPrice = stockRequest.PriceStock.SalePrice.HasValue ? Convert.ToDouble(stockRequest.PriceStock.SalePrice, culture) : null,
                    stockCode = stockRequest.PriceStock.StockCode,
                    StockQuantity = null
                });

                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };

                //Stok ve fiyat güncelleme işlemi için istek gönderildiğinde sıraya alınır ve batch kontrolünde statü Pending olarak döner. Güncelleme işlemi maksimum 4 saat içinde tamamlanır.
                var updatePriceAndStockResponse = _httpHelper.Put<CicekSepetiStockRequest, CicekSepetiStockResponse>(request, $"{_baseUrl}/v1/Products/price-and-stock", null, null, customHeaders);

                if (updatePriceAndStockResponse != null && updatePriceAndStockResponse.batchId != null)
                {
                    response.Data = updatePriceAndStockResponse.batchId;
                    response.Success = true;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                CultureInfo culture = new CultureInfo("en-US");

                var request = new CicekSepetiStockRequest();
                request.items = new List<CicekSepetiStockItem>();
                request.items.Add(new CicekSepetiStockItem
                {
                    listPrice = null,
                    salesPrice = null,
                    stockCode = stockRequest.PriceStock.StockCode,
                    StockQuantity = stockRequest.PriceStock.Quantity
                });
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };

                //Stok ve fiyat güncelleme işlemi için istek gönderildiğinde sıraya alınır ve batch kontrolünde statü Pending olarak döner. Güncelleme işlemi maksimum 4 saat içinde tamamlanır.
                var updatePriceAndStockResponse = _httpHelper.Put<CicekSepetiStockRequest, CicekSepetiStockResponse>(request, $"{_baseUrl}/v1/Products/price-and-stock", null, null, customHeaders);

                if (updatePriceAndStockResponse != null && updatePriceAndStockResponse.batchId != null)
                {
                    response.Data = updatePriceAndStockResponse.batchId;
                    response.Success = true;

                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new();

                var preUrl = $"{_baseUrl}/v1/Products";
                var page = 0;
                var pageSize = 60;
                var pagesCount = 0;

                do
                {
                    var url = $"{preUrl}?Page={page}&PageSize={pageSize}";
                    var productResponse = this._httpHelper.Get<CicekSepetiProductFilterResponse>(
                    url,
                    null,
                    null,
                    new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } });

                    if (pagesCount == 0)
                        pagesCount = (int)Math.Ceiling(productResponse.totalCount / (decimal)pageSize) + 1;

                    if (productResponse.products != null)
                        response.Data.AddRange(productResponse
                            .products
                            .Select(p => new Application.Common.DataTransferObjects.MarketplaceProductStatus
                            {
                                MarketplaceId = this.MarketplaceId,
                                MarketplaceProductId = p.productCode,
                                StockCode = p.stockCode,
                                Barcode = p.barcode,
                                Url = p.link,
                                UUId = p.productCode,
                                Opened = true,
                                OnSale = p.productStatusType == "YAYINDA",
                                Locked = false,

                                UnitPrice = (decimal)p.salesPrice,
                                Stock = p.StockQuantity
                            }));
                    else
                    {

                    }

                    page++;

                    if (pagesCount > page)
                        Thread.Sleep(6000);

                } while (pagesCount > page);

                response.Success = true;
            });
        }

        public DataResponse<bool> StockCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                var url = $"{_baseUrl}/v1/Products/batch-status/{batchId}";
                var result = this._httpHelper.Get<BatchStatusResponse>(url, null, null, customHeaders);
                response.Success = true;
                if (result != null)
                    response.Data = result.itemCount == 0;

                if (response.Data == false)
                    response.Message = String.Join("", result.items.SelectMany(i => i.failureReasons.Select(fr => fr.message)));
            });
        }

        public DataResponse<bool> PriceCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                var url = $"{_baseUrl}/v1/Products/batch-status/{batchId}";
                var result = this._httpHelper.Get<BatchStatusResponse>(url, null, null, customHeaders);
                response.Success = true;
                if (result != null)
                    response.Data = result.itemCount == 0;

                if (response.Data == false)
                    response.Message = String.Join("", result.items.SelectMany(i => i.failureReasons.Select(fr => fr.message)));
            });
        }

        public DataResponse<bool> CreateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                var url = $"{_baseUrl}/v1/Products/batch-status/{batchId}";
                var result = this._httpHelper.Get<BatchStatusResponse>(url, null, null, customHeaders);
                response.Success = true;
                if (result != null)
                    response.Data = result.itemCount == 0;

                if (response.Data == false)
                    response.Message = String.Join("", result.items.SelectMany(i => i.failureReasons.Select(fr => fr.message)));
            });
        }

        public DataResponse<bool> UpdateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                var url = $"{_baseUrl}/v1/Products/batch-status/{batchId}";
                var result = this._httpHelper.Get<BatchStatusResponse>(url, null, null, customHeaders);
                response.Success = true;
                if (result != null)
                    response.Data = result.itemCount == 0;

                if (response.Data == false)
                    response.Message = String.Join("", result.items.SelectMany(i => i.failureReasons.Select(fr => fr.message)));
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                response.Data = new List<CategoryResponse>();

                var categoryUrl = $"{_baseUrl}/v1/Categories";
                var customHeaders = new Dictionary<string, string> { { "x-api-key", this._configuration.ApiKey } };
                var cicekSepetiCategory = this._httpHelper.Get<CicekSepetiCategory>(categoryUrl, null, null, customHeaders);

                CategoryRecursive(cicekSepetiCategory.Categories
                    .First(csc => csc.Id == 255)
                    .SubCategories
                    .Where(sc => sc.Id == 12466)
                    .ToList(), response.Data);


                response.Data.ForEach(d =>
                {
                    var categoryAttributeUrl = $"{_baseUrl}/v1/categories/{d.Code}/attributes";
                    var cicekSepetiCategory = this._httpHelper.Get<Common.CategoryAttribute.CicekSepetiCategory>(categoryAttributeUrl, null, null, customHeaders);
                    d.CategoryAttributes = cicekSepetiCategory
                        .CategoryAttributes.Where(x => (x.Type == "Ürün Özelliği" || x.Varianter) && x.AttributeName != "Marka").GroupBy(x => new { x.AttributeName, x.Required }).Select(x => x.OrderByDescending(x => x.Varianter).First())
                        .Select(ca => new CategoryAttributeResponse
                        {
                            Code = ca.AttributeId.ToString(),
                            Name = ca.AttributeName,
                            Mandatory = ca.Required || (ca.Varianter && ca.AttributeName.ToLower() == "renk"),
                            AllowCustom = false,
                            MultiValue = false,
                            Variantable = ca.Varianter,
                            CategoryCode = d.Code,
                            CategoryAttributesValues = ca
                                .AttributeValues
                                .Select(cav => new CategoryAttributeValueResponse
                                {
                                    VarinatCode = ca.AttributeId.ToString(),
                                    Code = cav.Id.ToString(),
                                    Name = cav.Name
                                })
                                .ToList()
                        })
                        .ToList();
                });

                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                int page = 0;
                CicekSepetiOrderResponse orderResponse = null;
                do
                {
                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 1 //1 : Yeni 2 : Hazırlanıyor 5 : Kargoya Verildi 11 : Kargoya Verilecek 7 : Teslim Edildi
                    };

                    orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(
                        request,
                        $"{_baseUrl}/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }});

                    if (orderResponse != null && orderResponse.supplierOrderListWithBranch.Count == 0) return;

                    var _orders = orderResponse
                        .supplierOrderListWithBranch
                        .GroupBy(x => x.orderId)
                        .Select(x => x)
                        .ToList();

                    foreach (var odLoop in _orders)
                    {
                        var firstOrder = odLoop.FirstOrDefault();

                        if (!firstOrder.isOrderStatusActive)
                            continue;

                        string shipmentId = "";

                        switch (firstOrder.cargoCompany)
                        {
                            case "Aras Kargo":
                                shipmentId = ShipmentCompanies.ArasKargo;
                                break;
                            case "Yurtiçi Kargo":
                                shipmentId = ShipmentCompanies.YurticiKargo;
                                break;
                            case "PTT Kargo":
                                shipmentId = ShipmentCompanies.PttKargo;
                                break;
                            case "MNG Kargo":
                                shipmentId = ShipmentCompanies.MngKargo;
                                break;
                            case "Sürat Kargo":
                                shipmentId = ShipmentCompanies.SuratKargo;
                                break;

                        }


                        var customerName = firstOrder.senderName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var invoiceFirstName = "";
                        var invoiceLastName = "";
                        if (customerName.Length > 1)
                        {
                            invoiceFirstName = customerName[0];
                            invoiceLastName = customerName[1];
                        }


                        customerName = firstOrder.receiverName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var firstName = "";
                        var lastName = "";
                        if (customerName.Length > 1)
                        {
                            firstName = customerName[0];
                            lastName = customerName[1];
                        }

                        var order = new Order
                        {
                            UId = $"{Application.Common.Constants.Marketplaces.CicekSepeti}_{odLoop.Key}",
                            OrderCode = odLoop.Key.ToString(),
                            OrderTypeId = "OS",
                            TotalAmount = Convert.ToDecimal(odLoop.Sum(x => x.totalPrice)),
                            OrderSourceId = OrderSources.Pazaryeri,
                            MarketplaceId = Application.Common.Constants.Marketplaces.CicekSepeti,
                            OrderNote = firstOrder.cardMessage,
                            Customer = new Customer
                            {
                                FirstName = firstName,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                LastName = lastName,
                                TaxNumber = "",
                                Mail = firstOrder.invoiceEmail,
                                TaxAdministration = "",
                            },
                            OrderDetails = new List<OrderDetail>(),
                            Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>(),
                            OrderDeliveryAddress = new OrderDeliveryAddress
                            {
                                Email = firstOrder.invoiceEmail,
                                Neighborhood = CleanRegionName(firstOrder.receiverRegion),
                                FirstName = firstName,
                                LastName = lastName,
                                Phone = firstOrder.receiverPhone,
                                Country = "Türkiye",
                                City = CleanCityName(firstOrder.receiverCity),
                                District = firstOrder.receiverDistrict,
                                Address = firstOrder.receiverAddress
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress
                            {
                                Address = !string.IsNullOrEmpty(firstOrder.senderAddress) ? firstOrder.senderAddress : firstOrder.receiverAddress,
                                City = !string.IsNullOrEmpty(firstOrder.senderCity) ? CleanCityName(firstOrder.senderCity) : CleanCityName(firstOrder.receiverCity),
                                Country = "Türkiye",
                                District = !string.IsNullOrEmpty(firstOrder.senderDistrict) ? firstOrder.senderDistrict : firstOrder.receiverDistrict,
                                Email = firstOrder.invoiceEmail,
                                FirstName = invoiceFirstName,
                                LastName = invoiceLastName,
                                Neighborhood = !string.IsNullOrEmpty(firstOrder.senderRegion) ? CleanRegionName(firstOrder.senderRegion) : firstOrder.receiverRegion,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                TaxNumber = "",
                                TaxOffice = ""
                            },
                            OrderDate = DateTime.Now,
                            OrderShipment = new OrderShipment
                            {
                                ShipmentCompanyId = shipmentId,
                                ShipmentTypeId = "3",
                                TrackingCode = firstOrder.partialNumber,
                                TrackingUrl = firstOrder.shipmentTrackingUrl
                            }
                        };
                        foreach (var orderDetail in odLoop)
                        {


                            order.OrderDetails.Add(new OrderDetail
                            {
                                UId = orderDetail.orderItemId.ToString(),
                                Quantity = orderDetail.quantity,
                                UnitPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                ListPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                TaxRate = orderDetail.tax / 100,
                                Product = new Product
                                {
                                    Name = orderDetail.name,
                                    StockCode = orderDetail.code,
                                    Barcode = orderDetail.productCode
                                }
                            });
                        }

                        order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment
                        {

                            Amount = Convert.ToDecimal(firstOrder.totalPrice),
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Date = DateTime.Now
                        });
                        response.Data.Orders.Add(order);

                    }

                } while (orderResponse?.pageCount < page + 1);

                Parallel.ForEach(response.Data.Orders, theOrder =>
                {
                    theOrder.UId = $"M_{theOrder.OrderCode}";
                });

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                //Siparişler ilk başta paketlenerek kargoya hazırlanıyor. Sonrasında statusid 11 olan Kargoya Verilecek siparişler çekiliyor.
                SetCargo(orderRequest);

                int page = 0;
                CicekSepetiOrderResponse orderResponse = null;
                do
                {
                    Console.WriteLine("Çicek Sepeti 30 Second Waiting");
                    Thread.Sleep(new TimeSpan(0, 0, 30));

                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 11 //1 : Yeni 2 : Hazırlanıyor 5 : Kargoya Verildi 11 : Kargoya Verilecek 7 : Teslim Edildi
                    };

                    orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"{_baseUrl}/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }

                });

                    if (orderResponse != null && orderResponse.supplierOrderListWithBranch.Count == 0) return;

                    var _orders = orderResponse
                        .supplierOrderListWithBranch
                        .GroupBy(x => x.orderId)
                        .Select(x => x)
                        .ToList();

                    foreach (var odLoop in _orders)
                    {
                        var firstOrder = odLoop.FirstOrDefault();

                        if (!firstOrder.isOrderStatusActive)
                            continue;

                        string shipmentId = "";

                        switch (firstOrder.cargoCompany)
                        {
                            case "Aras Kargo":
                                shipmentId = ShipmentCompanies.ArasKargo;
                                break;
                            case "Yurtiçi Kargo":
                                shipmentId = ShipmentCompanies.YurticiKargo;
                                break;
                            case "PTT Kargo":
                                shipmentId = ShipmentCompanies.PttKargo;
                                break;
                            case "MNG Kargo":
                                shipmentId = ShipmentCompanies.MngKargo;
                                break;
                            case "Sürat Kargo":
                                shipmentId = ShipmentCompanies.SuratKargo;
                                break;

                        }


                        var customerName = firstOrder.senderName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var invoiceFirstName = "";
                        var invoiceLastName = "";
                        if (customerName.Length > 1)
                        {
                            invoiceFirstName = customerName[0];
                            invoiceLastName = customerName[1];
                        }


                        customerName = firstOrder.receiverName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        var firstName = "";
                        var lastName = "";
                        if (customerName.Length > 1)
                        {
                            firstName = customerName[0];
                            lastName = customerName[1];
                        }

                        var order = new Order
                        {
                            OrderCode = odLoop.Key.ToString(),
                            OrderTypeId = "OS",
                            TotalAmount = Convert.ToDecimal(odLoop.Sum(x => x.totalPrice)),
                            OrderSourceId = OrderSources.Pazaryeri,
                            MarketplaceId = Application.Common.Constants.Marketplaces.CicekSepeti,
                            OrderNote = firstOrder.cardMessage,
                            Customer = new Customer
                            {
                                FirstName = firstName,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                LastName = lastName,
                                TaxNumber = "",
                                Mail = firstOrder.invoiceEmail,
                                TaxAdministration = "",
                            },
                            OrderDetails = new List<OrderDetail>(),
                            Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>(),
                            OrderDeliveryAddress = new OrderDeliveryAddress
                            {
                                Email = firstOrder.invoiceEmail,
                                Neighborhood = CleanRegionName(firstOrder.receiverRegion),
                                FirstName = firstName,
                                LastName = lastName,
                                Phone = firstOrder.receiverPhone,
                                Country = "Türkiye",
                                City = CleanCityName(firstOrder.receiverCity),
                                District = firstOrder.receiverDistrict,
                                Address = firstOrder.receiverAddress
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress
                            {
                                Address = !string.IsNullOrEmpty(firstOrder.senderAddress) ? firstOrder.senderAddress : firstOrder.receiverAddress,
                                City = !string.IsNullOrEmpty(firstOrder.senderCity) ? CleanCityName(firstOrder.senderCity) : CleanCityName(firstOrder.receiverCity),
                                Country = "Türkiye",
                                District = !string.IsNullOrEmpty(firstOrder.senderDistrict) ? firstOrder.senderDistrict : firstOrder.receiverDistrict,
                                Email = firstOrder.invoiceEmail,
                                FirstName = invoiceFirstName,
                                LastName = invoiceLastName,
                                Neighborhood = !string.IsNullOrEmpty(firstOrder.senderRegion) ? CleanRegionName(firstOrder.senderRegion) : firstOrder.receiverRegion,
                                Phone = string.IsNullOrEmpty(firstOrder.receiverPhone) ?
                                            firstOrder.orderId.ToString() :
                                            CleanPhoneNumber(firstOrder.receiverPhone),
                                TaxNumber = "",
                                TaxOffice = ""
                            },
                            OrderDate = DateTime.Now,
                            OrderShipment = new OrderShipment
                            {
                                ShipmentCompanyId = shipmentId,
                                ShipmentTypeId = "3",
                                TrackingCode = firstOrder.partialNumber,
                                TrackingUrl = firstOrder.shipmentTrackingUrl
                            }
                        };
                        foreach (var orderDetail in odLoop)
                        {


                            order.OrderDetails.Add(new OrderDetail
                            {
                                Quantity = orderDetail.quantity,
                                UnitPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                ListPrice = Convert.ToDecimal(orderDetail.totalPrice / orderDetail.quantity),
                                TaxRate = orderDetail.tax / 100,
                                Product = new Product
                                {
                                    Name = orderDetail.name,
                                    StockCode = orderDetail.code,
                                    Barcode = orderDetail.productCode
                                }
                            });
                        }

                        order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment
                        {

                            Amount = Convert.ToDecimal(firstOrder.totalPrice),
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Date = DateTime.Now
                        });
                        response.Data.Orders.Add(order);

                    }

                } while (orderResponse?.pageCount < page + 1);

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var request = new CicekSepetiOrderRequest
                {

                    Page = 0,
                    PageSize = 100,
                    OrderNo = Convert.ToInt32(orderRequest.MarketplaceOrderCode)
                };

                var orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"{_baseUrl}/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }

                });

                if (orderResponse != null && orderResponse.supplierOrderListWithBranch.Count > 0 && !orderResponse.supplierOrderListWithBranch[0].isOrderStatusActive)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Çiceksepeti siparişiniz iptal statüsündedir. Siparişin devam edilemesi çiceksepeti panelinden kontrol edilmesi gerekiyor.";
                }


                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                List<int> orderIds = new();

                int pageCount = 0;
                int page = 0;

                do
                {
                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 7
                    };

                    var orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(
                        request,
                        $"{_baseUrl}/v1/Order/GetOrders",
                        null,
                        null,
                        new Dictionary<string, string> {
                            {
                                "x-api-key", this._configuration.ApiKey
                            }
                        });

                    if ((orderResponse != null && orderResponse.supplierOrderListWithBranch.Count == 0) || orderResponse == null)
                        return;

                    if (pageCount == 0)
                        pageCount = orderResponse.pageCount;

                    page++;

                    orderIds.AddRange(orderResponse.supplierOrderListWithBranch.Select(x => x.orderId));
                } while (pageCount > page);

                if (orderIds.Count > 0)
                    response.Data.Orders.AddRange(orderIds.Select(oi => new Order
                    {
                        OrderCode = oi.ToString(),
                        MarketplaceId = this.MarketplaceId
                    }));

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {

            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Çiçeksepeti ile bağlantı kurulamıyor.";
            });

        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {

            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Çiçeksepeti ile bağlantı kurulamıyor.";
            });

            //return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            //{
            //    response.Data = new()
            //    {
            //        Orders = new()
            //    };

            //    List<int> orderIds = new();
            //    int page = 0;
            //    int pageCount = 0;
            //    do
            //    {
            //        var request = new GetCancelledOrdersRequest
            //        {
            //            Page = page,
            //            PageSize = 100
            //        };

            //        var orderResponse = _httpHelper.Post<GetCancelledOrdersRequest, GetCancelledOrdersResponse>(
            //            request,
            //            $"{_baseUrl}/v1/Order/getcanceledorders",
            //            null,
            //            null,
            //            new Dictionary<string, string> {
            //                {
            //                    "x-api-key", this._configuration.ApiKey
            //                }
            //            });

            //        if (orderResponse != null && orderResponse.orderItemList.Count == 0)
            //            return;

            //        if (pageCount == 0)
            //            pageCount = orderResponse.pageCount;

            //        page++;

            //        orderIds.AddRange(orderResponse.orderItemList.Select(x => x.orderId));
            //    } while (pageCount > page);

            //    if (orderIds.Count > 0)
            //        response.Data.Orders.AddRange(orderIds.Select(oi => new Order
            //        {
            //            OrderCode = oi.ToString(),
            //            MarketplaceId = this.MarketplaceId
            //        }));

            //    response.Success = true;
            //}, (response, exception) =>
            //{
            //    response.Success = false;
            //    response.Message = "";
            //});
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.CicekSepeti;
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(CicekSepetiConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.ApiKey = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ApiKey").Value;
        }
        #endregion

        #region Helper Methods
        private void CategoryRecursive(List<CicekSepetiSubCategory> cicekSepetiCategories, List<CategoryResponse> categoryResponses)
        {
            foreach (var cscLoop in cicekSepetiCategories)
            {
                if (cscLoop.SubCategories.Count == 0)
                    categoryResponses.Add(new CategoryResponse
                    {
                        Code = cscLoop.Id.ToString(),
                        Name = cscLoop.Name,
                        ParentCategoryCode = cscLoop.ParentCategoryId.ToString(),
                    });

                CategoryRecursive(cscLoop.SubCategories, categoryResponses);
            }
        }

        public void SetCargo(OrderRequest orderRequest)
        {
            try
            {
                int page = 0;


                CicekSepetiOrderResponse orderResponse = null;
                do
                {


                    var request = new CicekSepetiOrderRequest
                    {
                        StartDate = DateTime.Now.AddDays(-7),
                        EndDate = DateTime.Now,
                        Page = page,
                        PageSize = 100,
                        StatusId = 1 //1 : Yeni 2 : Hazırlanıyor 5 : Kargoya Verildi 11 : Kargoya Verilecek 7 : Teslim Edildi
                    };

                    orderResponse = _httpHelper.Post<CicekSepetiOrderRequest, CicekSepetiOrderResponse>(request, $"{_baseUrl}/v1/Order/GetOrders", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }

                });

                    if (orderResponse != null && (orderResponse.supplierOrderListWithBranch == null || orderResponse.supplierOrderListWithBranch.Count == 0)) return;

                    var _orders = orderResponse.supplierOrderListWithBranch.GroupBy(x => x.orderId).Select(x => x).ToList();
                    foreach (var odLoop in _orders)
                    {
                        var firstOrder = odLoop.FirstOrDefault();

                        if (!firstOrder.isOrderStatusActive)
                            continue;


                        var cargoRequest = new CicekSepetiCargoRequest
                        {
                            orderItemsGroup = new List<OrderItemsGroup>
                                {
                                    new OrderItemsGroup
                                    {
                                        orderItemIds=odLoop.Select(x => x.orderItemId).ToList()
                                    }
                                }
                        };

                        //Sipariş kargoya hazır hale getiriliyor
                        var cargoResponse = _httpHelper.Put<CicekSepetiCargoRequest, CicekSepetiCargoResponse>(cargoRequest, $"{_baseUrl}/v1/Order/readyforcargowithcsintegration", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }

                });
                    }

                } while (orderResponse?.pageCount < page + 1);
            }
            catch
            {

            }
        }

        public string CleanCityName(string cityName)
        {
            if (cityName == "ISTANBUL-ANADOLU" || cityName == "ISTANBUL-AVRUPA" || cityName == "ISTANBUL AVRUPA" || cityName == "ISTANBUL ANADOLU")
                return "İstanbul";

            return cityName;
        }

        public string CleanRegionName(string districtName)
        {
            if (districtName.EndsWith(" Mahallesi"))
                return districtName.Replace(" Mahallesi", "");

            return districtName;
        }

        public string CleanPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.StartsWith("0"))
                return phoneNumber.TrimStart(new char[] { '0' });

            return phoneNumber;
        }

        private DataResponse<CicekSepetiProductFilterResponse> Filter(ProductFilterRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CicekSepetiProductFilterResponse>>((response) =>
            {

                response.Data = _httpHelper.Get<CicekSepetiProductFilterResponse>($"{_baseUrl}/v1/Products?stockCode={productRequest.Stockcode}", null, null, new Dictionary<string, string> {
                    { "x-api-key", this._configuration.ApiKey }});

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion
    }
}