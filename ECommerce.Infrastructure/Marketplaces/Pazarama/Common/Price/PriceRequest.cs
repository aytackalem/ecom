﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Price
{
    public class PriceRequest
    {
        [JsonProperty("items")]
        public List<Price> Items { get; set; }
    }


    public class Price
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("listPrice")]
        public decimal ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal SalePrice { get; set; }
    }
}
