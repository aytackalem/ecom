﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Price
{


    public class PriceResponse
    {
        public object data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
    }
}
