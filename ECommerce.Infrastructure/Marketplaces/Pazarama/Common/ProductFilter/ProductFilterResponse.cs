﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.ProductFilter
{
 


    public class Attribute
    {
        public string attributeName { get; set; }
        public string attributeValue { get; set; }
    }

    public class ProductFilter
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string description { get; set; }
        public string brandName { get; set; }
        public string code { get; set; }
        public string groupCode { get; set; }
        public int stockCount { get; set; }
        public string stockCode { get; set; }
        public int priorityRank { get; set; }
        public double listPrice { get; set; }
        public double salePrice { get; set; }
        public int vatRate { get; set; }
        public string categoryName { get; set; }
        public List<Attribute> attributes { get; set; }
        public List<Image> images { get; set; }
        public List<DeliveryType> deliveryTypes { get; set; }
    }

    public class DeliveryType
    {
        public List<string> cityName { get; set; }
        public string deliveryTypeName { get; set; }
    }

    public class Image
    {
        public string imageUrl { get; set; }
    }

    public class ProductFilterResponse
    {
        public List<ProductFilter> data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
    }
}
