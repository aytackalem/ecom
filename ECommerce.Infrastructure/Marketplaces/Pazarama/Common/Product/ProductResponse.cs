﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Product
{
    public class ProductResponse
    {
        public ProductResponseData data { get; set; }
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
        public object userMessage { get; set; }
        public bool fromCache { get; set; }
    }


    public class ProductResponseData
    {
        public string batchRequestId { get; set; }
        public DateTime creationDate { get; set; }
    }
}
