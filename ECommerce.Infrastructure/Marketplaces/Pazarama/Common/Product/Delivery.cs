﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Product
{
    public class Delivery
    {
        public string deliveryId { get; set; }
        public List<string> cityList { get; set; }
    }
}
