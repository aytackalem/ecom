﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Product
{
    public class Attribute
    {
        public string attributeId { get; set; }
        public string attributeValueId { get; set; }
    }
}
