﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Product
{
    public class Product
    {
        public Product()
        {
            this.images = new List<Image>();
            this.attributes = new List<Attribute>();
            this.deliveries = new List<Delivery>();
        }
        public string code { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public string description { get; set; }
        public string groupCode { get; set; }
        public string brandId { get; set; }
        public int desi { get; set; }
        public int stockCount { get; set; }
        public string stockCode { get; set; }
        public string currencyType { get; set; }
        public double listPrice { get; set; }
        public double salePrice { get; set; }
        public int vatRate { get; set; }
        public List<Image> images { get; set; }
        public string categoryId { get; set; }
        public List<Attribute> attributes { get; set; }
        public List<Delivery> deliveries { get; set; }
    }
}
