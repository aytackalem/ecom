﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.ProductBatchResult
{
    public class ProductBatchResult
    {
        public ProductBatchResultData data { get; set; }
        public bool success { get; set; }
        public string messageCode { get; set; }
        public string message { get; set; }
        public string userMessage { get; set; }
        public bool fromCache { get; set; }
    }

    public class ProductBatchResultData
    {
        public int status { get; set; }
        public string batchRequestId { get; set; }
        public List<BatchResult> batchResult { get; set; }
        public int totalCount { get; set; }
        public int failedCount { get; set; }
        public DateTime creationDate { get; set; }
    }

    public class BatchResult
    {
        public List<string> failureReasons { get; set; }
    }
}
