﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.SellerDelivery
{
    public class DataSellerDelivery
    {
        public CargoCompany cargoCompany { get; set; }
        public object fastDelivery { get; set; }
        public object storeDelivery { get; set; }
        public object digital { get; set; }
        public object donation { get; set; }
    }
}
