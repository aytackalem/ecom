﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.SellerDelivery
{
    public class CargoCompany
    {
        public string deliveryId { get; set; }
        public string cargoCompanyId { get; set; }
        public object otherCargoCompanyName { get; set; }
        public double price { get; set; }
        public double campaignPrice { get; set; }
        public double campaignAmount { get; set; }
        public object campaignText { get; set; }
        public int deliveryDuration { get; set; }
        public bool contracted { get; set; }
        public object secondCargoCompanyId { get; set; }
        public object returnCargoCompanyId { get; set; }
        public bool hybridCargo { get; set; }
    }
}
