﻿namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Order
{
    public class UpdateOrderStatusList
    {
        public string orderNumber { get; set; }

        public int status { get; set; }
    }
}
