﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Order
{
    public class Datum
    {
        public string orderId { get; set; }
        public int orderNumber { get; set; }
        public DateTime orderDate { get; set; }
        public double orderAmount { get; set; }
        public double shipmentAmount { get; set; }
        public double discountAmount { get; set; }
        public string discountDescription { get; set; }
        public string currency { get; set; }
        public int paymentType { get; set; }
        public int orderStatus { get; set; }
        public string customerId { get; set; }
        public string customerName { get; set; }
        public string customerEmail { get; set; }
        public ShipmentAddress shipmentAddress { get; set; }
        public BillingAddress billingAddress { get; set; }
        public List<Item> items { get; set; }
    }
}
