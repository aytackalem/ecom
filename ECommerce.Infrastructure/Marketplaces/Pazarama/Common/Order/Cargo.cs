﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Order
{
    public class Cargo
    {
        public string companyName { get; set; }
        public string trackingNumber { get; set; }
        public string trackingUrl { get; set; }
    }
}
