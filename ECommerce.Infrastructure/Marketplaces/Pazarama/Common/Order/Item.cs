﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Order
{
    public class Item
    {
        public string orderItemId { get; set; }
        public int orderItemStatus { get; set; }
        public string shipmentCode { get; set; }
        public ShipmentCost shipmentCost { get; set; }
        public int deliveryType { get; set; }
        public int quantity { get; set; }
        public ListPrice listPrice { get; set; }
        public SalePrice salePrice { get; set; }
        public TaxAmount taxAmount { get; set; }
        public ShipmentAmount shipmentAmount { get; set; }
        public TotalPrice totalPrice { get; set; }
        public DiscountAmount discountAmount { get; set; }
        public string discountDescription { get; set; }
        public bool taxIncluded { get; set; }
        public Cargo cargo { get; set; }
        public Product product { get; set; }
    }

}
