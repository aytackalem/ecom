﻿namespace ECommerce.Infrastructure.Marketplaces.Pazarama
{
    public class PazaramaConfiguration
    {
        #region Properties
        public string GrantType { get; internal set; }

        public string ApiKey { get; internal set; }
        
        public string MerchantId { get; internal set; }
        
        public string SecretKey { get; internal set; }

        public string Scope { get; internal set; }
        #endregion
    }
}
