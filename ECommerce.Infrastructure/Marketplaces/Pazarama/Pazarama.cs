﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Brand;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.CargoTraking;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Product;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.ProductBatchResult;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.SellerDelivery;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Token;
using Microsoft.EntityFrameworkCore.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Order = ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order;

namespace ECommerce.Infrastructure.Marketplaces.Pazarama
{
    public class Pazarama : TokenMarketplaceBase<PazaramaConfiguration>
    {
        #region Constants
        private const string _baseUrl = "https://isortagimapi.pazarama.com/";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;

        private readonly ICacheHandler _cacheHandler;

        private readonly IShipmentProviderService _shipmentProviderService;

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public Pazarama(IHttpHelper httpHelper, ICacheHandler cacheHandler, IServiceProvider serviceProvider, IShipmentProviderService shipmentProviderService, IUnitOfWork unitOfWork) : base(serviceProvider, cacheHandler, 9)
        {
            #region Fields
            this._httpHelper = httpHelper;
            this._cacheHandler = cacheHandler;
            this._shipmentProviderService = shipmentProviderService;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Pazarama;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var requestCreate = new Products();
                requestCreate.products = new List<Common.Product.Product>();

                var sellerDelivery = _httpHelper.Get<GetSellerDelivery>($"{_baseUrl}sellerRegister/getSellerDelivery", "Bearer", this.GetAccessToken().Data);

                if (!sellerDelivery.success)
                {
                    return;
                }


                foreach (var stockItem in productRequest.ProductItem.StockItems)
                {
                    if (string.IsNullOrEmpty(stockItem.Description)) stockItem.Description = stockItem.Title;

                    var item = new Common.Product.Product
                    {
                        code = stockItem.Barcode,
                        name = stockItem.Title,
                        categoryId = productRequest.ProductItem.CategoryId,
                        brandId = productRequest.ProductItem.BrandId,
                        description = stockItem.Description,
                        displayName = stockItem.Title,
                        groupCode = productRequest.ProductItem.SellerCode,
                        desi = Convert.ToInt32(productRequest.ProductItem.DimensionalWeight),
                        stockCount = stockItem.Quantity,
                        stockCode = stockItem.StockCode,
                        currencyType = "TRY",
                        listPrice = Convert.ToDouble(stockItem.ListPrice),
                        salePrice = Convert.ToDouble(stockItem.SalePrice),
                        vatRate = Convert.ToInt32(stockItem.VatRate * 100),
                        images = new List<Common.Product.Image>(),
                        attributes = new List<Common.Product.Attribute>(),
                        deliveries = new List<Delivery>()
                    };
                    item.deliveries.Add(new Delivery
                    {
                        cityList = new List<string> { },
                        deliveryId = sellerDelivery.data.cargoCompany.deliveryId
                    });



                    foreach (var image in stockItem.Images)
                    {
                        item.images.Add(new Common.Product.Image
                        {
                            imageurl = image.Url
                        });
                    }

                    foreach (var attribute in stockItem.Attributes)
                    {

                        item.attributes.Add(new Common.Product.Attribute
                        {
                            attributeId = attribute.AttributeCode,
                            attributeValueId = attribute.AttributeValueCode
                        });

                    }

                    if (stockItem.Active) //Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                    {
                        requestCreate.products.Add(item);
                    }
                }



                var _response = this.
              _httpHelper.
              Post<Products, ProductResponse>(requestCreate, $"{_baseUrl}product/create", "Bearer", this.GetAccessToken().Data, null, null);
                if (_response != null && _response.success)
                {

                    var productBatchResultResponse = this.
                       _httpHelper.
                       Get<ProductBatchResult>($"{_baseUrl}product/getProductBatchResult?BatchRequestId={_response.data.batchRequestId}", "Bearer", this.GetAccessToken().Data);


                    response.Success = productBatchResultResponse != null && productBatchResultResponse.data.totalCount == requestCreate.products.Count
                     && productBatchResultResponse.data.failedCount == 0;

                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var stokRequest = new Common.Stok.StokRequest
                {
                    Items = new List<Common.Stok.Stok>
                    {
                        new Common.Stok.Stok
                        {
                            Code=stockRequest.PriceStock.Barcode,
                            StockCount=stockRequest.PriceStock.Quantity
                        }
                    }

                };

                var stockResponse = this.
                    _httpHelper.
                    Post<Common.Stok.StokRequest, Common.Stok.StokResponse>(stokRequest, $"{_baseUrl}product/updateStock", "Bearer", this.GetAccessToken().Data, null, null);
                if (stockResponse.success)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[PAZARAMA] [{stockRequest.PriceStock.Barcode}]  stok güncellendi");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    response.Success = true;
                }
                else
                {

                    var productFilter = Filter(this.GetAccessToken().Data, stockRequest.PriceStock.Barcode);
                    if (productFilter.Success)
                    {
                        //ürün açılmamış demek stok güncellenmeyecek
                        response.Success = productFilter.Data.data.Count == 0 && productFilter.Data.success;
                    }
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var stokRequest = new Common.Price.PriceRequest
                {
                    Items = new List<Common.Price.Price>
                    {
                        new Common.Price.Price
                        {
                            Code=stockRequest.PriceStock.Barcode,
                            ListPrice=stockRequest.PriceStock.ListPrice.Value,
                            SalePrice=stockRequest.PriceStock.SalePrice.Value
                        }
                    }

                };

                var priceResponse = this.
                    _httpHelper.
                    Post<Common.Price.PriceRequest, Common.Price.PriceResponse>(stokRequest, $"{_baseUrl}product/updatePrice", "Bearer", this.GetAccessToken().Data, null, null);
                if (priceResponse.success)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[PAZARAMA] [{stockRequest.PriceStock.Barcode}]  fiyat güncellendi");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    response.Success = true;
                }
                else
                {

                    var productFilter = Filter(this.GetAccessToken().Data, stockRequest.PriceStock.Barcode);
                    if (productFilter.Success)
                    {
                        //ürün açılmamış demek stok güncellenmeyecek
                        response.Success = productFilter.Data.data.Count == 0 && productFilter.Data.success;
                    }
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new();

                var token = this.GetAccessToken().Data;
                var page = 0;
                var preUrl = $"{_baseUrl}product/products";

                var productItems = new List<ProductItem>();

                do
                {
                    var url = $"{preUrl}?Approved=true&Size=100&Page={page}";
                    var productFilterResponse = _httpHelper.Get<Common.ProductFilter.ProductFilterResponse>(url, "Bearer", token);

                    if (productFilterResponse.data.Count == 0)
                        break;

                    foreach (var pp in productFilterResponse.data)
                    {

                        var product = new ProductItem
                        {
                            SellerCode = pp.code,
                            CategoryId = "",
                            BrandId = "0ece9bfa-af63-4a04-20d8-08dad854c9da",
                            CategoryName = pp.categoryName,
                            BrandName = pp.brandName,
                            Name = pp.name,
                            StockItems = new List<StockItem>
                            {
                                new StockItem
                                {
                                    Images=new List<Application.Common.Parameters.Product.Image>(),
                                    Active = true,
                                    Attributes = new List<CategoryAttribute>(),
                                    Barcode = pp.code,
                                    BrandName = pp.brandName,
                                    Description = pp.description,
                                    ListPrice = Convert.ToDecimal(pp.listPrice),
                                    SalePrice = Convert.ToDecimal(pp.salePrice),
                                    StockCode = pp.stockCode,
                                    Quantity = pp.stockCount,
                                    Title = pp.displayName,
                                    VatRate = pp.vatRate,

                                }
                            }
                        };


                        productItems.Add(product);
                    }

                    page++;
                } while (true);

                do
                {
                    var url = $"{preUrl}?Approved=false&Size=100&Page={page}";
                    var productFilterResponse = _httpHelper.Get<Common.ProductFilter.ProductFilterResponse>(url, "Bearer", token);

                    if (productFilterResponse.data.Count == 0)
                        break;

                    foreach (var pp in productFilterResponse.data)
                    {

                        var product = new ProductItem
                        {
                            SellerCode = pp.code,
                            CategoryId = "",
                            CategoryName = pp.categoryName,
                            BrandId = "0ece9bfa-af63-4a04-20d8-08dad854c9da",
                            BrandName = pp.brandName,
                            Name = pp.name,
                            StockItems = new List<StockItem>
                            {
                                new StockItem
                                {
                                    Images=new List<Application.Common.Parameters.Product.Image>(),
                                    Active = true,
                                    Attributes = new List<CategoryAttribute>(),
                                    Barcode = pp.code,
                                    BrandName = pp.brandName,
                                    Description = pp.description,
                                    ListPrice = Convert.ToDecimal(pp.listPrice),
                                    SalePrice = Convert.ToDecimal(pp.salePrice),
                                    StockCode = pp.stockCode,
                                    Quantity = pp.stockCount,
                                    Title = pp.displayName,
                                    VatRate = pp.vatRate,

                                }
                            }
                        };


                        productItems.Add(product);
                    }

                    page++;
                } while (true);
                response.Data.AddRange(productItems.GroupBy(x => x.SellerCode).Select(x => x.First()).ToList());
                response.Success = true;
            });
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceProductStatus>>>((response) =>
            {
                response.Data = new();

                var token = this.GetAccessToken().Data;
                var page = 0;
                var preUrl = $"{_baseUrl}product/products";

                do
                {
                    var url = $"{preUrl}?Approved=true&Size=100&Page={page}";
                    var productFilterResponse = _httpHelper.Get<Common.ProductFilter.ProductFilterResponse>(url, "Bearer", token);

                    if (productFilterResponse.data.Count == 0)
                        break;

                    response.Data.AddRange(productFilterResponse.data.Select(d => new MarketplaceProductStatus
                    {
                        MarketplaceId = this.MarketplaceId,
                        MarketplaceProductId = d.code,
                        Barcode = d.code,
                        StockCode = d.stockCode,

                        Opened = true,
                        OnSale = true,
                        Locked = false,

                        Url = $"https://www.pazarama.com/a-p-{d.code}",
                        Stock = d.stockCount,
                        ListUnitPrice = (decimal)d.listPrice,
                        UnitPrice = (decimal)d.salePrice
                    }));

                    page++;
                } while (true);

                do
                {
                    var url = $"{preUrl}?Approved=false&Size=100&Page={page}";
                    var productFilterResponse = _httpHelper.Get<Common.ProductFilter.ProductFilterResponse>(url, "Bearer", token);

                    if (productFilterResponse.data.Count == 0)
                        break;

                    response.Data.AddRange(productFilterResponse.data.Select(d => new MarketplaceProductStatus
                    {
                        MarketplaceId = this.MarketplaceId,
                        MarketplaceProductId = d.code,
                        Barcode = d.code,
                        StockCode = d.stockCode,
                        Opened = false,
                        OnSale = false,
                        Url = $"https://www.pazarama.com/a-p-{d.code}",
                        Stock = d.stockCount,
                        ListUnitPrice = (decimal)d.listPrice,
                        UnitPrice = (decimal)d.salePrice
                    }));

                    page++;
                } while (true);

                response.Success = true;
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var url = $"{_baseUrl}category/getCategoryTree";

                var _categoryResponse = this._httpHelper.Get<Common.Categories.CategoryResponse>(url, null, null);


                var categories = _categoryResponse.data.Where(x => x.parentCategories.Any(y => y.Trim().ToLower() == "mobilya") && x.leaf).ToList();
                var _categories = new List<CategoryResponse>();

                foreach (var cLoop in categories)
                {
                    var categoryResponse = new CategoryResponse()
                    {
                        Code = cLoop.id.ToString(),
                        Name = cLoop.name,
                        ParentCategoryCode = cLoop.parentId

                    };

                    var categoryAttributeResponse = this
                        ._httpHelper
                        .Get<Common.CategoryAttributes.CategoryAttributeResponse>(
                            $"{_baseUrl}category/getCategoryWithAttributes?Id={cLoop.id}",
                            null,
                            null);

                    if (categoryAttributeResponse.success)
                    {

                        categoryResponse.CategoryAttributes = categoryAttributeResponse
                            .data
                            .attributes
                            .Select(a => new CategoryAttributeResponse
                            {
                                Code = a.id,
                                Mandatory = a.isRequired,
                                AllowCustom = false,
                                CategoryCode = cLoop.id,
                                MultiValue = false,
                                Name = a.name,
                                CategoryAttributesValues = a
                                    .attributeValues
                                    .Select(av => new CategoryAttributeValueResponse
                                    {
                                        VarinatCode = a.id,
                                        Code = av.id,
                                        Name = av.value
                                    })
                                    .ToList()
                            })
                            .ToList();

                        _categories.Add(categoryResponse);
                    }
                }

                response.Data = _categories;
                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                var request = new Common.Order.OrderRequest
                {
                    startDate = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd"),
                    endDate = DateTime.Now.ToString("yyyy-MM-dd")
                };

                var orderResponse = _httpHelper.Post<Common.Order.OrderRequest, Common.Order.OrderResponse>(request, $"{_baseUrl}order/getOrdersForApi", "Bearer", this.GetAccessToken().Data, null, null);
                if (orderResponse.success)
                {
                    var orders = new List<Order>();

                    foreach (var pOrder in orderResponse.data)
                    {
                        if (pOrder.items.Count(x => x.orderItemStatus == 3) != pOrder.items.Count) continue;

                        var customerNameSurname = pOrder.customerName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var ordershipmentNameSurname = pOrder.shipmentAddress.nameSurname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderBillingNameSurname = pOrder.billingAddress.nameSurname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        string shipmentId = "OT";//Diğer Kargo



                        var cargo = pOrder.items[0].cargo;

                        if (cargo != null)
                        {

                            switch (cargo.companyName.ToLower())
                            {
                                case "aras":
                                    shipmentId = ShipmentCompanies.ArasKargo;
                                    break;
                                case "ptt":
                                    shipmentId = ShipmentCompanies.PttKargo;
                                    break;
                                case "mng":
                                    shipmentId = ShipmentCompanies.MngKargo;
                                    break;
                                case "ups":
                                    shipmentId = ShipmentCompanies.Ups;
                                    break;

                            }
                        }

                        Order order = new()
                        {
                            OrderSourceId = OrderSources.Pazaryeri,
                            MarketplaceId = Application.Common.Constants.Marketplaces.Pazarama,
                            OrderDate = pOrder.orderDate,
                            OrderTypeId = "BE",
                            OrderCode = pOrder.orderNumber.ToString(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                TrackingCode = cargo != null ? cargo.trackingNumber : "",
                                ShipmentTypeId = "PND",
                                TrackingUrl = cargo != null ? cargo.trackingUrl : "",
                                ShipmentCompanyId = shipmentId
                            },
                            Customer = new Customer()
                            {
                                FirstName = customerNameSurname[0],
                                LastName = customerNameSurname.Length > 1 ? customerNameSurname[1] : customerNameSurname[0],
                                Mail = pOrder.customerEmail,
                                TaxNumber = string.Empty,
                                Phone = pOrder.shipmentAddress.phoneNumber
                            },
                            OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                            {
                                FirstName = ordershipmentNameSurname[0],
                                LastName = ordershipmentNameSurname.Length > 1 ? ordershipmentNameSurname[1] : ordershipmentNameSurname[0],
                                Country = "Türkiye",
                                Email = "",
                                Phone = pOrder.shipmentAddress.phoneNumber,
                                City = pOrder.shipmentAddress.cityName,
                                District = pOrder.shipmentAddress.districtName,
                                Neighborhood = pOrder.shipmentAddress.neighborhoodName,
                                Address = $"{pOrder.shipmentAddress.displayAddressText}"
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress()
                            {
                                FirstName = orderBillingNameSurname[0],
                                LastName = orderBillingNameSurname.Length > 1 ? orderBillingNameSurname[1] : orderBillingNameSurname[0],
                                Email = pOrder.billingAddress.customerEmail,
                                Phone = pOrder.billingAddress.phoneNumber,
                                City = pOrder.billingAddress.cityName,
                                District = pOrder.billingAddress.districtName,
                                Address = $"{pOrder.billingAddress.displayAddressText}",
                                Neighborhood = pOrder.shipmentAddress.neighborhoodName,

                            },
                            Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                        };
                        order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                        {
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Amount = Convert.ToDecimal(pOrder.orderAmount),
                            Date = pOrder.orderDate
                        });

                        #region Order Details
                        order.OrderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

                        double cargoFee = 0;
                        foreach (var orderDetail in pOrder.items)
                        {

                            if (orderDetail.orderItemStatus == 3)
                            {
                                cargoFee += orderDetail.shipmentAmount.value;

                                var unitDiscount = Convert.ToDecimal(orderDetail.discountAmount.value);

                                order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                                {
                                    UId = orderDetail.orderItemId,
                                    TaxRate = Convert.ToDouble(orderDetail.product.vatRate) / 100,
                                    UnitPrice = Convert.ToDecimal(orderDetail.salePrice.value),
                                    ListPrice = Convert.ToDecimal(orderDetail.listPrice.value),
                                    UnitCommissionAmount = 0,
                                    UnitCost = 0,
                                    UnitDiscount = unitDiscount,
                                    Payor = true,
                                    Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                                    {
                                        Barcode = orderDetail.product.code,
                                        Name = orderDetail.product.name,
                                        StockCode = orderDetail.product.stockCode.ToString()

                                    },
                                    Quantity = orderDetail.quantity
                                });
                            }
                        }
                        #endregion
                        order.TotalAmount = order.OrderDetails.Sum(x => x.UnitPrice) + Convert.ToDecimal(cargoFee);
                        order.ListTotalAmount = order.OrderDetails.Sum(x => x.ListPrice);
                        order.Discount = order.OrderDetails.Sum(x => x.UnitDiscount);
                        order.CargoFee = Convert.ToDecimal(cargoFee);

                        if (order.OrderDetails.Count > 0)
                        {
                            order.UId = $"{Application.Common.Constants.Marketplaces.Pazarama}_{pOrder.orderNumber}";

                            orders.Add(order);
                        }
                    }

                    response.Success = true;
                    response.Data = new OrderResponse { Orders = orders };
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderTypeUpdateResponse>>((response) =>
            {
                if (orderTypeUpdateRequest.MarketplaceStatus == ECommerce.Application.Common.Parameters.Enums.MarketplaceStatus.Picking)
                {
                    _httpHelper.Put<Common.Order.UpdateOrderStatusList>(new Common.Order.UpdateOrderStatusList
                    {
                        orderNumber = orderTypeUpdateRequest.Order.OrderCode,
                        status = 12
                    }, $"{_baseUrl}order/updateOrderStatusList", "Bearer", this.GetAccessToken().Data);
                }

                response.Success = true;
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var request = new Common.Order.OrderRequest
                {
                    startDate = DateTime.Now.AddDays(-20).ToString("yyyy-MM-dd"),
                    endDate = DateTime.Now.ToString("yyyy-MM-dd")
                };

                var orderResponse = _httpHelper.Post<Common.Order.OrderRequest, Common.Order.OrderResponse>(request, $"{_baseUrl}order/getOrdersForApi", "Bearer", this.GetAccessToken().Data, null, null);
                if (orderResponse.success)
                {
                    var orders = new List<Order>();

                    foreach (var pOrder in orderResponse.data)
                    {

                        if (pOrder.items.Count(x => x.orderItemStatus == 3) != pOrder.items.Count) continue;

                        var customerNameSurname = pOrder.customerName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var ordershipmentNameSurname = pOrder.shipmentAddress.nameSurname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        var billingFirstName = string.Empty;
                        var billingLastName = string.Empty;
                        if (string.IsNullOrEmpty(pOrder.billingAddress.companyName) == false)
                        {
                            billingFirstName = pOrder.billingAddress.companyName;
                        }
                        else
                        {
                            var orderBillingNameSurname = pOrder
                                .billingAddress
                                .nameSurname
                                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            if (orderBillingNameSurname.Length > 0)
                            {
                                billingFirstName = orderBillingNameSurname[0];
                            }

                            if (orderBillingNameSurname.Length > 1)
                            {
                                billingLastName = string.Join(" ", orderBillingNameSurname[1..]);
                            }
                        }

                        var cargo = pOrder.items[0].cargo;

                        string shipmentId = ShipmentCompanies.DigerKargo; //Diğer Kargo

                        if (cargo != null)
                        {

                            switch (cargo.companyName.ToLower())
                            {
                                case "aras kargo":
                                    shipmentId = ShipmentCompanies.ArasKargo;
                                    break;
                                case "ptt":
                                    shipmentId = ShipmentCompanies.PttKargo;
                                    break;
                                case "mng":
                                    shipmentId = ShipmentCompanies.MngKargo;
                                    break;
                                case "ups":
                                    shipmentId = ShipmentCompanies.Ups;
                                    break;
                                case "yurtiçi kargo":
                                    shipmentId = ShipmentCompanies.YurticiKargo;
                                    break;
                            }
                        }
                        var trackingCode = pOrder.items.Count > 0 ? pOrder.items[0].shipmentCode : "";
                        Order order = new()
                        {
                            Commercial = string.IsNullOrEmpty(pOrder.billingAddress.companyName) == false,
                            OrderSourceId = OrderSources.Pazaryeri,
                            MarketplaceId = Application.Common.Constants.Marketplaces.Pazarama,
                            OrderDate = pOrder.orderDate,
                            OrderTypeId = String.IsNullOrEmpty(trackingCode) ? "BE" : "OS",
                            OrderCode = pOrder.orderNumber.ToString(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                TrackingCode = trackingCode,
                                ShipmentTypeId = "PND",
                                TrackingUrl = cargo != null ? cargo.trackingUrl : "",
                                ShipmentCompanyId = shipmentId
                            },
                            Customer = new Customer()
                            {
                                FirstName = customerNameSurname[0],
                                LastName = customerNameSurname.Length > 1 ? customerNameSurname[1] : customerNameSurname[0],
                                Mail = pOrder.customerEmail,
                                TaxNumber = string.Empty,
                                Phone = pOrder.shipmentAddress.phoneNumber
                            },
                            OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                            {
                                FirstName = ordershipmentNameSurname[0],
                                LastName = ordershipmentNameSurname.Length > 1 ? ordershipmentNameSurname[1] : ordershipmentNameSurname[0],
                                Country = "Türkiye",
                                Email = "",
                                Phone = pOrder.shipmentAddress.phoneNumber,
                                City = pOrder.shipmentAddress.cityName,
                                District = pOrder.shipmentAddress.districtName,
                                Neighborhood = pOrder.shipmentAddress.neighborhoodName,
                                Address = $"{pOrder.shipmentAddress.displayAddressText}"
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress()
                            {
                                FirstName = billingFirstName,
                                LastName = billingLastName,
                                Email = pOrder.billingAddress.customerEmail,
                                Phone = pOrder.billingAddress.phoneNumber,
                                City = pOrder.billingAddress.cityName,
                                District = pOrder.billingAddress.districtName,
                                Address = $"{pOrder.billingAddress.displayAddressText}",
                                Neighborhood = pOrder.shipmentAddress.neighborhoodName,
                                TaxNumber = pOrder.billingAddress.taxNumber,
                                TaxOffice = pOrder.billingAddress.taxOffice
                            },
                            Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                        };
                        order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                        {
                            CurrencyId = "TRY",
                            PaymentTypeId = "OO",
                            Amount = Convert.ToDecimal(pOrder.orderAmount),
                            Date = pOrder.orderDate
                        });

                        #region Order Details
                        order.OrderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

                        double cargoFee = 0;
                        foreach (var orderDetail in pOrder.items)
                        {

                            if (orderDetail.orderItemStatus == 3)
                            {
                                cargoFee += orderDetail.shipmentAmount.value;

                                var unitDiscount = Convert.ToDecimal(orderDetail.discountAmount.value);

                                order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                                {
                                    TaxRate = Convert.ToDouble(orderDetail.product.vatRate) / 100,
                                    UnitPrice = Convert.ToDecimal(orderDetail.salePrice.value),
                                    ListPrice = Convert.ToDecimal(orderDetail.listPrice.value),
                                    UnitCommissionAmount = 0,
                                    UnitCost = 0,
                                    UnitDiscount = unitDiscount,
                                    Payor = true,
                                    Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                                    {
                                        Barcode = orderDetail.product.code,
                                        Name = orderDetail.product.name,
                                        StockCode = orderDetail.product.stockCode.ToString()

                                    },
                                    Quantity = orderDetail.quantity
                                });
                            }
                        }
                        #endregion
                        order.TotalAmount = order.OrderDetails.Sum(x => x.UnitPrice) + Convert.ToDecimal(cargoFee);
                        order.ListTotalAmount = order.OrderDetails.Sum(x => x.ListPrice);
                        order.Discount = order.OrderDetails.Sum(x => x.UnitDiscount);
                        order.CargoFee = Convert.ToDecimal(cargoFee);

                        order.OrderPicking = new OrderPicking
                        {
                            MarketPlaceOrderId = pOrder.orderNumber.ToString(),
                            TrackingNumber = trackingCode,
                            OrderDetails = pOrder.items.Select(x => new OrderPickingDetail()
                            {
                                Id = x.orderItemId,
                                Quantity = x.quantity
                            }).ToList()
                        };
                        if (order.OrderDetails.Count > 0)
                            orders.Add(order);
                    }

                    response.Success = true;
                    response.Data = new OrderResponse { Orders = orders };
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (request.Status == ECommerce.Application.Common.Parameters.Enums.MarketplaceStatus.Picking)
                {
                    _httpHelper.Put<Common.Order.UpdateOrderStatusList>(new Common.Order.UpdateOrderStatusList
                    {
                        orderNumber = request.OrderPicking.MarketPlaceOrderId,
                        status = 12
                    }, $"{_baseUrl}order/updateOrderStatusList", "Bearer", this.GetAccessToken().Data);
                }
                else if (request.Status == Application.Common.Parameters.Enums.MarketplaceStatus.Delivered)
                {
                    _httpHelper.Put<Common.Order.UpdateOrderStatusList>(new Common.Order.UpdateOrderStatusList
                    {
                        orderNumber = request.OrderPicking.MarketPlaceOrderId,
                        status = 11
                    }, $"{_baseUrl}order/updateOrderStatusList", "Bearer", this.GetAccessToken().Data);
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var orders = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Where(o => o.OrderTypeId == "KTE" && o.MarketplaceId == Application.Common.Constants.Marketplaces.Pazarama && !string.IsNullOrEmpty(o.MarketplaceOrderNumber))
                    .Select(o => new Order
                    {
                        Id = o.Id,
                        OrderCode = o.MarketplaceOrderNumber,
                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                        {
                            TrackingCode = o.OrderShipments.FirstOrDefault().TrackingCode
                        }
                    })
                    .ToList();

                orders.ForEach(o =>
                {
                    var deliveredResponse = this._shipmentProviderService.Track(o.Id, o.OrderShipment.TrackingCode);
                    if (deliveredResponse.Success && deliveredResponse.Data.Delivered)
                    {
                        response.Data.Orders.Add(new Order
                        {
                            MarketplaceId = Application.Common.Constants.Marketplaces.Pazarama,
                            OrderCode = o.OrderCode.ToString(),
                            OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                            {
                                TrackingCode = o.OrderShipment.TrackingCode
                            }
                        });

                        this.OrderTypeUpdate(new OrderPickingRequest
                        {
                            Configurations = orderRequest.Configurations,
                            MarketPlaceId = Application.Common.Constants.Marketplaces.Pazarama,
                            Status = Application.Common.Parameters.Enums.MarketplaceStatus.Delivered,
                            OrderPicking = new OrderPicking
                            {
                                MarketPlaceOrderId = o.OrderCode,
                                TrackingNumber = o.OrderShipment.TrackingCode
                            }
                        });
                    }
                });

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {
                var marketplaceBrands = this._httpHelper.Get<BrandResponse>($"https://isortagimapi.pazarama.com/brand/getBrands?name={q}", null, null);


                response.Data = marketplaceBrands.data.Select(x => new KeyValue<string, string>
                {
                    Key = x.id,
                    Value = x.name
                }
               ).ToList();

                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(PazaramaConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.GrantType = "client_credentials";
            configuration.ApiKey = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ApiKey").Value;
            configuration.MerchantId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "MerchantId").Value;
            configuration.SecretKey = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "SecretKey").Value;
            configuration.Scope = "merchantgatewayapi.fullaccess";
        }

        protected override String GetAccessTokenConcreate()
        {
            var url = $"{_baseUrl}connect/token";

            Token token = null;
            var authType = "basic";
            var byteArray = Encoding.ASCII.GetBytes($"{this._configuration.ApiKey}:{this._configuration.SecretKey}");
            var _token = Convert.ToBase64String(byteArray);


            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, _token);

                var dict = new Dictionary<string, string>();
                //dict.Add("grant_type", "client_credentials");
                //dict.Add("scope", "merchantgatewayapi.fullaccess");
                dict.Add("grant_type", this._configuration.GrantType);
                dict.Add("scope", this._configuration.Scope);

                using (HttpResponseMessage httpResponseMessage = client.PostAsync(url, new FormUrlEncodedContent(dict)).Result)
                using (HttpContent content = httpResponseMessage.Content)
                {
                    string responseString = content.ReadAsStringAsync().Result;
                    if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");
                    token = JsonConvert.DeserializeObject<Token>(responseString);
                }
            }

            return token.data.accessToken;
        }
        #endregion

        #region Helper Methods
        private DataResponse<Common.ProductFilter.ProductFilterResponse> Filter(string token, string barcode)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Common.ProductFilter.ProductFilterResponse>>((response) =>
            {
                var productFilterResponse = _httpHelper.Get<Common.ProductFilter.ProductFilterResponse>($"{_baseUrl}product/products?Approved=true&Code={barcode}&Size=100&Page=1", "Bearer", token);

                response.Data = productFilterResponse;
                response.Success = true;
            });
        }
        #endregion
    }
}