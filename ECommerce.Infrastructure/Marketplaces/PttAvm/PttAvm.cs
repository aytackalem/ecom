﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Category;
using PttAvmService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Infrastructure.Marketplaces.PttAvm
{
    public class PttAvm : MarketplaceBase<PttAvmConfiguration>
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public PttAvm(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        public override string MarketplaceId => "PTT";

        protected override DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return new DataResponse<List<KeyValue<string, string>>>();
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            return new DataResponse<CheckPreparingResponse>();
        }

        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>(async (response) =>
            {

                ServiceClient serviceClient = new();
                serviceClient.ClientCredentials.UserName.UserName = _configuration.Username;
                serviceClient.ClientCredentials.UserName.Password = _configuration.Password;


                var categoryResponseTree = serviceClient.GetCategoryTreeAsync(string.Empty, string.Empty);
                var result = categoryResponseTree.Result;

                var data = new List<CategoryResponse>();

                foreach (var theChildren in result.category_tree)
                {
                    data.Add(new CategoryResponse
                    {
                        Code = theChildren.id,
                        ParentCategoryCode = theChildren.parent_id,
                        Name = theChildren.name,
                        CategoryAttributes = new List<CategoryAttributeResponse>()
                    });
                    var categoryId = 0;
                    if (theChildren.id == "2")
                    {
                        categoryId = 1182;
                    }

                    data.AddRange(this.Recursive(theChildren.children, categoryId));

                }





                response.Data = data;
                response.Success = true;


            }, (a, ex) =>
            {

            });
        }

        private List<SubCategory> TrendyolRecursive(SubCategory category)
        {
            var categoryAttributeRoots = new List<SubCategory>();
            categoryAttributeRoots.Add(category);


            if (category.subCategories == null || category.subCategories.Count == 0)
            {
                return categoryAttributeRoots;

                //var categoryAttributeRoot = _httpHelper.Get<CategoryAttributeRoot>($"https://api.trendyol.com/sapigw/product-categories/{category.id}/attributes", "Basic", authorization);
            }

            foreach (var scLoop in category.subCategories)
            {
                categoryAttributeRoots.AddRange(TrendyolRecursive(scLoop));
            }

            return categoryAttributeRoots;
        }

        private List<CategoryResponse> Recursive(Category1[] childrens, int categoryId)
        {
            var categoryAttributeRoots = new List<CategoryResponse>();
            if (childrens.Any())
            {
                foreach (var theChildren in childrens)
                {

                    if (categoryId > 0)
                    {
                        var categoryAttributeRoot = _httpHelper.Get<CategoryAttributeRoot>($"https://api.trendyol.com/sapigw/product-categories/{categoryId}/attributes", "Basic", "ZVRmRU91VW5lVjVzM1pNZGd4OWc6V1ZYTXpoQzZIU09nOUlIUGI5c3U=");

                        if (categoryAttributeRoot != null)
                        {
                            categoryAttributeRoots.Add(new CategoryResponse
                            {
                                Code = theChildren.id,
                                ParentCategoryCode = theChildren.parent_id,
                                Name = theChildren.name,
                                CategoryAttributes = categoryAttributeRoot.categoryAttributes.Where(x => x.required).Select(ca => new CategoryAttributeResponse
                                {
                                    CategoryCode = theChildren.id.ToString(),
                                    Code = ca.attribute.id.ToString(),
                                    Name = ca.attribute.name,
                                    Mandatory = ca.required,
                                    AllowCustom = ca.allowCustom,
                                    CategoryAttributesValues = ca.attributeValues?.Select(av => new CategoryAttributeValueResponse
                                    {
                                        VarinatCode = ca.attribute.id.ToString(),
                                        Code = av.id.ToString(),
                                        Name = av.name
                                    }).ToList()
                                }).ToList()
                            });
                        }

                    }
                    else
                    {
                        categoryAttributeRoots.Add(new CategoryResponse
                        {
                            Code = theChildren.id,
                            ParentCategoryCode = theChildren.parent_id,
                            Name = theChildren.name,
                            CategoryAttributes = new List<CategoryAttributeResponse>()
                        });
                    }

                    categoryAttributeRoots.AddRange(Recursive(theChildren.children, categoryId));
                }
            }

            return categoryAttributeRoots;

        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>(async (response) =>
            {
                response.Data = new OrderResponse { Orders = new() };

                ServiceClient serviceClient = new();
                serviceClient.ClientCredentials.UserName.UserName = _configuration.Username;
                serviceClient.ClientCredentials.UserName.Password = _configuration.Password;

                var _ = serviceClient.SiparisKontrolListesiV2Async(DateTime.Now.AddDays(-1), DateTime.Now, 1).Result;

                response.Data.Orders.AddRange(_.Select(o => new Application.Common.Wrappers.Marketplaces.Order.Order
                {
                    UId = $"{Application.Common.Constants.Marketplaces.PttAvm}_{o.SiparisNo}",
                    OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                    MarketplaceId = Application.Common.Constants.Marketplaces.PttAvm,
                    OrderCode = o.SiparisNo,
                    OrderTypeId = "OS",
                    TotalAmount = 0,
                    ListTotalAmount = 0,
                    Discount = 0,
                    OrderDate = o.IslemTarihi,
                    OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                    {
                        PackageNumber = string.Empty,
                        TrackingCode = string.Empty,
                        ShipmentTypeId = "PND",
                        TrackingUrl = string.Empty,
                        ShipmentCompanyId = string.Empty
                    },
                    Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer()
                    {
                        FirstName = o.MusteriAdi,
                        LastName = o.MusteriSoyadi,
                        Mail = o.Eposta,
                        TaxAdministration = o.VergiDaire,
                        TaxNumber = o.VergiNo,
                        Phone = o.TelefonNo
                    },
                    OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                    {
                        FirstName = o.MusteriAdi,
                        LastName = o.MusteriSoyadi,
                        Country = string.Empty,
                        Email = o.Eposta,
                        Phone = o.TelefonNo,
                        City = o.SiparisIli,
                        District = o.SiparisIlce,
                        Neighborhood = string.Empty,
                        Address = o.SiparisAdresi
                    },
                    OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                    {
                        FirstName = o.FaturaMusteriAdi,
                        LastName = o.FaturaMusteriSoyadi,
                        Email = o.Eposta,
                        Phone = o.TelefonNo,
                        City = o.FaturaIli,
                        District = o.FaturaIlce,
                        Address = o.FaturaAdresi,
                        Neighborhood = string.Empty,
                        TaxNumber = o.VergiNo,
                        TaxOffice = o.VergiDaire
                    },
                    OrderDetails = o
                        .SiparisUrunler
                        .Select(su => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                        {
                            ListPrice = Convert.ToDecimal(su.KdvDahilToplamTutar),
                            UnitPrice = Convert.ToDecimal(su.KdvDahilToplamTutar),
                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                            {
                                Barcode = su.VariantBarkod
                            }
                        })
                        .ToList()
                }));
                response.Success = true;
            });
        }

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return new DataResponse<List<ProductItem>>();
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new List<MarketplaceProductStatus>();

                ServiceClient serviceClient = new();
                serviceClient.ClientCredentials.UserName.UserName = _configuration.Username;
                serviceClient.ClientCredentials.UserName.Password = _configuration.Password;
                var getProductsWithVariants = serviceClient.StokKontrolListesiAsync(this._configuration.ShopId, 0, 0, "", "", 1, 1, 0, "");

                var result = getProductsWithVariants.Result;

                foreach (var theContent in result)
                {
                    var splitUrl = theContent.UrunUrl.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                    var url = "";
                    for (int i = 1; i < splitUrl.Length; i++)
                    {
                        url += string.Concat(splitUrl[i]);

                    }
                    url = $"https://www.pttavm.com/{url}-p-{splitUrl[0]}";

                    var marketplaceProductStatus = new MarketplaceProductStatus
                    {
                        MarketplaceId = this.MarketplaceId,
                        UUId= theContent.UrunId.ToString(),
                        MarketplaceProductId = theContent.UrunId.ToString(),
                        //Barcode = theContent.Barkod,
                        Barcode = "",
                        StockCode = theContent.UrunKodu,
                        Opened = true,
                        OnSale = true,
                        Locked = false,
                        Url = url,
                        ListUnitPrice = (decimal)theContent.KDVsiz,
                        UnitPrice = (decimal)theContent.KDVsiz,
                        Stock = theContent.Miktar
                    };
                    response.Data.Add(marketplaceProductStatus);
                }


                response.Success = true;
            });
        }

        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return new DataResponse<OrderTypeUpdateResponse>();
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return new Response();
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }

        #region Helper Methods
        protected override void BindConfigurationConcreate(PttAvmConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.ShopId = Convert.ToInt32(marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ShopId").Value);
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }
        #endregion

    }
}
