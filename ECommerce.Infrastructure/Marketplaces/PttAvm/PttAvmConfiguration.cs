﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.PttAvm
{
    public class PttAvmConfiguration
    {
        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public int ShopId { get; set; }
        #endregion
    }
}
