﻿//using ECommerce.Application.Common.Constants;
//using ECommerce.Application.Common.DataTransferObjects;
//using ECommerce.Application.Common.ExceptionHandling;
//using ECommerce.Application.Common.Extensions;
//using ECommerce.Application.Common.Interfaces.Helpers;
//using ECommerce.Application.Common.Interfaces.Marketplaces;
//using ECommerce.Application.Common.Parameters;
//using ECommerce.Application.Common.Parameters.Product;
//using ECommerce.Application.Common.Wrappers;
//using ECommerce.Application.Common.Wrappers.Marketplaces;
//using ECommerce.Domain.Entities;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;

//namespace ECommerce.Infrastructure.Marketplaces.Ideasoft
//{
//    public class Ideasoft : MarketplaceBase<IdeasoftConfiguration>
//    {
//        #region Fields
//        private readonly IHttpHelperV2 _httpHelperV2;
//        #endregion

//        #region Constructors
//        public Ideasoft(IServiceProvider serviceProvider, IHttpHelperV2 httpHelperV2) : base(serviceProvider)
//        {
//            #region Fields
//            this._httpHelperV2 = httpHelperV2;
//            #endregion
//        }
//        #endregion

//        #region Properties
//        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Ideasoft;
//        #endregion

//        #region Methods
//        #region Category Methods
//        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
//        {
//            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>(response =>
//            {
//                List<CategoryResponse> categories = new();

//                var preUrl = $"https://{this._configuration.ShopAddress}/api/{{0}}?Host={this._configuration.ShopAddress}&startDate={DateTime.Now.AddDays(-1):yyyy-MM-dd HH-mm-ss}&endDate={DateTime.Now:yyyy-MM-dd HH-mm-ss}";
//                var page = 1;
//                var limit = 100;
//                do
//                {
//                    var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.Category>>(
//                        $"{string.Format(preUrl, "categories")}&page={page}&limit={limit}",
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        configurations["AccessToken"]).Result;

//                    if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                        httpResultResponse.ContentObject != null &&
//                        httpResultResponse.ContentObject.HasItem())
//                    {
//                        categories.AddRange(httpResultResponse.ContentObject.Select(c =>
//                        {
//                            var name = c.name;

//                            Response.Parent parent = c.parent;
//                            while (parent != null)
//                            {
//                                name = $"{parent.name} > {name}";
//                                parent = parent.parent;
//                            }

//                            var c2 = new CategoryResponse
//                            {
//                                Code = c.id.ToString(),
//                                Name = name,
//                                ParentCategoryCode = c.parent?.id.ToString()
//                            };
//                            return c2;
//                        }));

//                        page++;
//                    }
//                    else
//                        break;
//                } while (true);

//                page = 1;
//                List<CategoryAttributeResponse> categoryAttributes = new();

//                do
//                {
//                    var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.OptionGroup>>(
//                        $"{string.Format(preUrl, "option_groups")}&page={page}&limit={limit}",
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        configurations["AccessToken"]).Result;

//                    if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                        httpResultResponse.ContentObject != null &&
//                        httpResultResponse.ContentObject.HasItem())
//                    {
//                        categoryAttributes
//                            .AddRange(httpResultResponse.ContentObject.Select(o => new CategoryAttributeResponse
//                            {
//                                Code = o.id.ToString(),
//                                Name = o.title,
//                                MultiValue = o.id == 13
//                            }));

//                        page++;
//                    }
//                    else
//                        break;
//                } while (true);

//                categoryAttributes.ForEach(theCategoryAttribute =>
//                {
//                    page = 1;
//                    List<CategoryAttributeValueResponse> categoryAttributeValues = new();

//                    do
//                    {
//                        var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.Option>>(
//                            $"{string.Format(preUrl, "options")}&page={page}&limit={limit}&optionGroup={theCategoryAttribute.Code}",
//                            HttpMethod.Get,
//                            AuthorizationType.Bearer,
//                            configurations["AccessToken"]).Result;

//                        if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                            httpResultResponse.ContentObject != null &&
//                            httpResultResponse.ContentObject.HasItem())
//                        {
//                            categoryAttributeValues
//                                .AddRange(httpResultResponse.ContentObject.Select(o => new CategoryAttributeValueResponse
//                                {
//                                    Code = o.id.ToString(),
//                                    Name = o.title,
//                                    VarinatCode = theCategoryAttribute.Code
//                                }));

//                            page++;
//                        }
//                        else
//                            break;
//                    } while (true);

//                    theCategoryAttribute.CategoryAttributesValues.AddRange(categoryAttributeValues);
//                });

//                categories.ForEach(theCategory =>
//                {
//                    var newCategoryAttributes = JsonConvert.DeserializeObject<List<CategoryAttributeResponse>>(JsonConvert.SerializeObject(categoryAttributes));

//                    newCategoryAttributes.ForEach(y => y.CategoryCode = theCategory.Code);

//                    theCategory.CategoryAttributes = newCategoryAttributes;
//                });

//                response.Data = categories;
//                response.Success = true;
//            });
//        }
//        #endregion

//        #region Product Methods
//        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
//        {
//            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>(response =>
//            {
//                List<ProductItem> products = new();

//                var preUrl = $"https://{this._configuration.ShopAddress}/api/{{0}}?Host={this._configuration.ShopAddress}";
//                var page = 1;
//                var limit = 100;
//                do
//                {
//                    Console.WriteLine($"Page: {page}");

//                    var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.Product>>(
//                        $"{string.Format(preUrl, "products")}&page={page}&limit={limit}&sort=-id",
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        this._configuration.AccessToken).Result;

//                    if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                        httpResultResponse.ContentObject != null &&
//                        httpResultResponse.ContentObject.HasItem())
//                    {
//                        products.AddRange(httpResultResponse
//                            .ContentObject
//                            .Select(p => new ProductItem
//                            {
//                                UUId = p.id.ToString(),
//                                Name = p.name,
//                                BrandId = p.brand != null ? p.brand.id.ToString() : "",
//                                BrandName = p.brand != null ? p.brand.name : "",
//                                CategoryId = p.productToCategories[0].category.id.ToString(),
//                                CategoryName = p.productToCategories[0].category.name,
//                                SellerCode = p.sku,
//                                DeliveryDay = 21,
//                                StockItems = new List<StockItem>
//                                {
//                                    new StockItem
//                                    {
//                                        UUId = p.id.ToString(),
//                                        Active = p.status == 1,
//                                        StockCode = p.sku,
//                                        Barcode = p.barcode,
//                                        BrandName = p.brand != null ? p.brand.name : "",
//                                        Description = p.details[0].extraDetails,
//                                        Images = p
//                                            .images
//                                            .Select(i => new Image
//                                            {
//                                                FileName = i.filename
//                                            })
//                                            .ToList(),
//                                        ListPrice = p.price1,
//                                        SalePrice = p.price1,
//                                        Quantity = Convert.ToInt32(p.stockAmount),
//                                        VatRate = p.tax / 100M,
//                                        Title = p.name,
//                                        Subtitle = p.name
//                                    }
//                                }
//                            }));

//                        page++;
//                    }
//                    else if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                             httpResultResponse.ContentObject.HasItem() == false)
//                        break;
//                    else if (httpResultResponse.HttpStatusCode != System.Net.HttpStatusCode.OK)
//                        Console.WriteLine("Try again...");
//                } while (true);

//                products.ForEach(theProduct =>
//                {
//                    Console.WriteLine(theProduct.UUId);

//                    List<ProductItem> products = new();

//                    var preUrl = $"https://{this._configuration.ShopAddress}/api/{{0}}?Host={this._configuration.ShopAddress}";
//                    var page = 1;
//                    var limit = 100;
//                    do
//                    {
//                        var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.OptionToProduct>>(
//                            $"{string.Format(preUrl, "option_to_products")}&page={page}&limit={limit}&parentProductId={theProduct.UUId}",
//                            HttpMethod.Get,
//                            AuthorizationType.Bearer,
//                            this._configuration.AccessToken).Result;

//                        if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                            httpResultResponse.ContentObject != null &&
//                            httpResultResponse.ContentObject.HasItem())
//                        {
//                            theProduct.StockItems[0].Attributes = httpResultResponse
//                                .ContentObject
//                                .Select(p => new CategoryAttribute
//                                {
//                                    AttributeCode = p.optionGroup.id.ToString(),
//                                    AttributeValueCode = p.option.id.ToString()
//                                }).ToList();

//                            page++;
//                        }
//                        else if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                             httpResultResponse.ContentObject.HasItem() == false)
//                            break;
//                        else if (httpResultResponse.HttpStatusCode != System.Net.HttpStatusCode.OK)
//                            Console.WriteLine("Try again...");
//                    } while (true);
//                });

//                response.Data = products;
//                response.Success = true;
//            });
//        }
//        #endregion

//        #region Brand Methods
//        protected override DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
//        {
//            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>(response =>
//            {
//                List<KeyValue<string, string>> brands = new();

//                var preUrl = $"https://{this._configuration.ShopAddress}/api/{{0}}?Host={this._configuration.ShopAddress}";
//                var page = 1;
//                var limit = 100;
//                do
//                {
//                    var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.Brand1>>(
//                        $"{string.Format(preUrl, "brands")}&page={page}&limit={limit}",
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        this._configuration.AccessToken).Result;

//                    if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                        httpResultResponse.ContentObject != null &&
//                        httpResultResponse.ContentObject.HasItem())
//                    {
//                        brands.AddRange(httpResultResponse.ContentObject.Select(c => new KeyValue<string, string>
//                        {
//                            Key = c.id.ToString(),
//                            Value = c.name
//                        }));

//                        page++;
//                    }
//                    else
//                        break;
//                } while (true);

//                response.Data = brands;
//                response.Success = true;
//            });
//        }
//        #endregion

//        #region Order Methods
//        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
//        {
//            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
//            {
//                List<Application.Common.Wrappers.Marketplaces.Order.Order> orders = new();

//                var preUrl = $"https://{this._configuration.ShopAddress}/api/{{0}}?Host={this._configuration.ShopAddress}";
//                var page = 1;
//                var limit = 100;
//                do
//                {
//                    var httpResultResponse = this._httpHelperV2.SendJsonAsync<List<Response.Order.OrderResponse>>(
//                        $"{string.Format(preUrl, "orders")}&page={page}&limit={limit}&sort=-id&startDate={DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00")}&endDate={DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00")}",
//                        HttpMethod.Get,
//                        AuthorizationType.Bearer,
//                        this._configuration.AccessToken).Result;

//                    if (httpResultResponse.HttpStatusCode == System.Net.HttpStatusCode.OK &&
//                        httpResultResponse.ContentObject != null &&
//                        httpResultResponse.ContentObject.HasItem())
//                    {
//                        var status = new List<string>
//                        {
//                            "approved","fulfilled","delivered","on_accumulation"
//                        };

//                        orders
//                            .AddRange(httpResultResponse
//                                .ContentObject.Where(x => status.Contains(x.status))
//                                .Select(o =>
//                                {
//                                    decimal promotionDiscountPerProduct = 0M;
//                                    if (o.promotionDiscount > 0)
//                                        promotionDiscountPerProduct = o.promotionDiscount / (decimal)o.orderItems.Sum(oi => oi.productQuantity);

//                                    var order = new Application.Common.Wrappers.Marketplaces.Order.Order
//                                    {
//                                        UId = $"{Application.Common.Constants.Marketplaces.Ideasoft}_{o.id}",
//                                        OrderSourceId = OrderSources.Web,
//                                        MarketplaceId = Application.Common.Constants.Marketplaces.Ideasoft,
//                                        OrderCode = o.id.ToString(),
//                                        OrderTypeId = "OS",
//                                        TotalAmount = o.finalAmount,
//                                        ListTotalAmount = o.generalAmount + o.promotionDiscount,
//                                        CargoFee = o.shippingAmount,
//                                        OrderDate = DateTime.Now,
//                                        Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>
//                                    {
//                                        new Application.Common.Wrappers.Marketplaces.Order.Payment
//                                        {
//                                            Paid = o.paymentStatus == "success",
//                                            Amount = o.finalAmount,
//                                            CurrencyId = "TL",
//                                            Date = DateTime.Now,
//                                            PaymentTypeId = this.ConvertPaymentType(o.paymentProviderName)
//                                        }
//                                    },
//                                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
//                                        {
//                                            ShipmentCompanyId = this.ConvertShipmentCompany(o.shippingProviderCode)
//                                        },
//                                        Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
//                                        {
//                                            FirstName = o.customerFirstname,
//                                            LastName = o.customerSurname,
//                                            Mail = o.customerEmail,
//                                            Phone = o.customerPhone
//                                        },
//                                        OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress
//                                        {
//                                            FirstName = o.billingAddress.firstname,
//                                            LastName = o.billingAddress.surname,
//                                            Phone = o.billingAddress.phoneNumber,
//                                            Address = o.billingAddress.address,
//                                            TaxNumber = o.billingAddress.taxNo,
//                                            TaxOffice = o.billingAddress.taxOffice,
//                                            Email = o.customerEmail,
//                                            City = o.billingAddress.location,
//                                            District = o.billingAddress.subLocation
//                                        },
//                                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress
//                                        {
//                                            FirstName = o.shippingAddress.firstname,
//                                            LastName = o.shippingAddress.surname,
//                                            Phone = o.shippingAddress.phoneNumber,
//                                            Address = o.shippingAddress.address,
//                                            Email = o.customerEmail,
//                                            City = o.shippingAddress.location,
//                                            District = o.shippingAddress.subLocation
//                                        },
//                                        OrderDetails = o
//                                        .orderItems
//                                        .Select(od =>
//                                        {
//                                            var vatRate = (1M + (od.productTax / 100M));
//                                            var unitDiscount = 0M;

//                                            if (o.promotionDiscount == 0M)
//                                            {
//                                                unitDiscount = od.discount + od.productDiscount;

//                                                if (o.paymentTypeName == "Havale")
//                                                    unitDiscount += od.productMoneyOrderDiscount;
//                                            }

//                                            var orderDetail = new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
//                                            {
//                                                UId = od.id.ToString(),
//                                                ListPrice = od.productPrice * vatRate,
//                                                UnitPrice = (od.productPrice - unitDiscount) * vatRate,
//                                                UnitDiscount = unitDiscount * vatRate,
//                                                TaxRate = Convert.ToDouble(vatRate - 1.0M),
//                                                Quantity = (int)od.productQuantity,
//                                                Product = new Application.Common.Wrappers.Marketplaces.Order.Product
//                                                {
//                                                    Barcode = od.productBarcode,
//                                                    StockCode = od.productSku,
//                                                    Name = od.productName
//                                                }
//                                            };

//                                            if (o.promotionDiscount > 0M)
//                                            {
//                                                orderDetail.UnitPrice -= promotionDiscountPerProduct;
//                                                orderDetail.UnitDiscount += promotionDiscountPerProduct;
//                                            }

//                                            if (o.installmentRate > 0M)
//                                                orderDetail.UnitPrice *= o.installmentRate;

//                                            return orderDetail;
//                                        })
//                                        .ToList()
//                                    };

//                                    return order;
//                                }));

//                        page++;
//                    }
//                    else
//                        break;
//                } while (true);

//                response.Data = new OrderResponse { Orders = orders };
//                response.Success = true;
//            });
//        }
//        #endregion
//        #endregion

//        #region Helper Methods
//        protected override void BindConfigurationConcreate(IdeasoftConfiguration configuration, List<MarketplaceConfiguration> marketplaceConfigurations)
//        {
//            configuration.ShopAddress = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ShopAddress").Value;
//            configuration.ClientId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ClientId").Value;
//            configuration.ClientSecret = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ClientSecret").Value;
//            configuration.AccessToken = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "AccessToken").Value;
//            configuration.RefreshToken = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "RefreshToken").Value;
//        }

//        private string ConvertPaymentType(string paymentType)
//        {
//            string text = "OO";
//            switch (paymentType)
//            {
//                case "Kredi Kartı":
//                    text = "OO";
//                    break;
//                case "Havale":
//                    text = "C";
//                    break;
//            }
//            return text;
//        }

//        private string ConvertShipmentCompany(string shippingProviderCode)
//        {
//            string text = "OT";
//            switch (shippingProviderCode)
//            {
//                case "yurtici":
//                case "yurtici_self_service":
//                case "yurtici_api":
//                    text = "Y";
//                    break;
//                case "ptt":
//                    text = "P";
//                    break;
//                case "mng":
//                    text = "M";
//                    break;
//                case "surat":
//                    text = "S";
//                    break;
//                case "ups":
//                    text = "U";
//                    break;
//            }
//            return text;
//        }
//        #endregion


//        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
//        {
//            return new DataResponse<string>();
//        }

//        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
//        {
//            return new DataResponse<CheckPreparingResponse> { Success = true };
//        }

//        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
//        {
//            return new DataResponse<OrderResponse> { Success = true, Data = new OrderResponse() { Orders=new List<Application.Common.Wrappers.Marketplaces.Order.Order>()} };
//        }

//        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
//        {
//            return new DataResponse<OrderResponse> { Success = true,Data=new OrderResponse() { Orders=new List<Application.Common.Wrappers.Marketplaces.Order.Order>()} };
//        }

//        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
//        {
//            return new DataResponse<List<MarketplaceProductStatus>> { Success = true };
//        }

//        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
//        {
//            return new DataResponse<OrderResponse> { Success = true };
//        }

//        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
//        {
//            return new DataResponse<OrderResponse> { Success = true };
//        }

//        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
//        {
//            return new DataResponse<OrderTypeUpdateResponse> { Success = true };
//        }

//        protected override Application.Common.Wrappers.Response OrderTypeUpdateConcreate(OrderPickingRequest request)
//        {
//            return new Application.Common.Wrappers.Response { Success = true };
//        }

//        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
//        {
//            return new DataResponse<string>();
//        }

//        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
//        {
//            return new DataResponse<string>();
//        }
//    }
//}