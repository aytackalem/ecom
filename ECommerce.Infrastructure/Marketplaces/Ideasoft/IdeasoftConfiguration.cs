﻿namespace ECommerce.Infrastructure.Marketplaces.Ideasoft
{
    public class IdeasoftConfiguration
    {
        #region Properties
        public string ShopAddress { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
        #endregion
    }
}
