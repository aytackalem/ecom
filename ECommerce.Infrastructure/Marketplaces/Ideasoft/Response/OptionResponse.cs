﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.Ideasoft.Response
{
    public class OptionResponse
    {
        public Option[] Options { get; set; }
    }

    public class Option
    {
        public int id { get; set; }
        public string title { get; set; }
        public string slug { get; set; }
        public int sortOrder { get; set; }
        public object logo { get; set; }
        public Optiongroup optionGroup { get; set; }
        public DateTime updatedAt { get; set; }
        public DateTime createdAt { get; set; }
    }

    public class Optiongroup
    {
        public int id { get; set; }
        public string title { get; set; }
    }

}
