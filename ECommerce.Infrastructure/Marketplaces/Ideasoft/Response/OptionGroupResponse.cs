﻿namespace ECommerce.Infrastructure.Marketplaces.Ideasoft.Response
{
    public class OptionGroupResponse
    {
        public OptionGroup[] OptionGroups { get; set; }
    }

    public class OptionGroup
    {
        public int id { get; set; }
        public string title { get; set; }
        public string slug { get; set; }
        public int sortOrder { get; set; }
        public int filterStatus { get; set; }
        public int aggregator { get; set; }
    }

}
