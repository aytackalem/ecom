﻿namespace ECommerce.Infrastructure.Marketplaces.Ideasoft.Response
{
    public class OptionToProductResponse
    {
        public OptionToProduct[] OptionToProducts { get; set; }
    }

    public class OptionToProduct
    {
        public int id { get; set; }
        public int parentProductId { get; set; }
        public Optiongroup1 optionGroup { get; set; }
        public Option1 option { get; set; }
        public Product1 product { get; set; }
    }

    public class Optiongroup1
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class Option1
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class Product1
    {
        public int id { get; set; }
    }

}
