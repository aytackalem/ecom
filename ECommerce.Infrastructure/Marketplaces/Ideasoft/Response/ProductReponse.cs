﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.Ideasoft.Response
{
    public class ProductReponse
    {
        public Product[] Products { get; set; }
    }

    public class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string fullName { get; set; }
        public string sku { get; set; }
        public string barcode { get; set; }
        public decimal price1 { get; set; }
        public int warranty { get; set; }
        public int tax { get; set; }
        public decimal stockAmount { get; set; }
        public float volumetricWeight { get; set; }
        public float buyingPrice { get; set; }
        public string stockTypeLabel { get; set; }
        public float discount { get; set; }
        public int discountType { get; set; }
        public float moneyOrderDiscount { get; set; }
        public int status { get; set; }
        public int taxIncluded { get; set; }
        public object distributor { get; set; }
        public int isGifted { get; set; }
        public string gift { get; set; }
        public int customShippingDisabled { get; set; }
        public float customShippingCost { get; set; }
        public string marketPriceDetail { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string metaKeywords { get; set; }
        public string metaDescription { get; set; }
        public string pageTitle { get; set; }
        public int hasOption { get; set; }
        public string shortDetails { get; set; }
        public string searchKeywords { get; set; }
        public string installmentThreshold { get; set; }
        public object homeSortOrder { get; set; }
        public object popularSortOrder { get; set; }
        public object brandSortOrder { get; set; }
        public object featuredSortOrder { get; set; }
        public object campaignedSortOrder { get; set; }
        public object newSortOrder { get; set; }
        public object discountedSortOrder { get; set; }
        public Brand brand { get; set; }
        public Currency currency { get; set; }
        public object parent { get; set; }
        public object countdown { get; set; }
        public object[] prices { get; set; }
        public Image[] images { get; set; }
        public Detail[] details { get; set; }
        public Producttocategory[] productToCategories { get; set; }
    }

    public class Brand
    {
        public int id { get; set; }
        public string name { get; set; }
        public string distributorCode { get; set; }
    }

    public class Currency
    {
        public int id { get; set; }
        public string label { get; set; }
        public string abbr { get; set; }
    }

    public class Image
    {
        public int id { get; set; }
        public string filename { get; set; }
        public string extension { get; set; }
        public string directoryName { get; set; }
        public string revision { get; set; }
        public int sortOrder { get; set; }
    }

    public class Detail
    {
        public int id { get; set; }
        public string details { get; set; }
        public string extraDetails { get; set; }
    }

    public class Producttocategory
    {
        public int id { get; set; }
        public int? sortOrder { get; set; }
        public Category1 category { get; set; }
    }

    public class Category1
    {
        public int id { get; set; }
        public string name { get; set; }
        public string distributorCode { get; set; }
    }

}
