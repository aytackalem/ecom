﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.Ideasoft.Response
{
    public class BrandResponse
    {
        public Brand1[] Property1 { get; set; }
    }

    public class Brand1
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public int sortOrder { get; set; }
        public int status { get; set; }
        public string distributorCode { get; set; }
        public object distributor { get; set; }
        public object imageFile { get; set; }
        public object showcaseContent { get; set; }
        public int displayShowcaseContent { get; set; }
        public string metaKeywords { get; set; }
        public string metaDescription { get; set; }
        public string canonicalUrl { get; set; }
        public string pageTitle { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

}
