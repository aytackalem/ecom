﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol
{
    public class TrendyolConfiguration : UsernamePasswordConfigurationBase
    {
        #region Properties
        public string SupplierId { get; set; }
        #endregion
    }
}
