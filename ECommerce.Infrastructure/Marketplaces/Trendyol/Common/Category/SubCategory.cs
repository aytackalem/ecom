﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Category
{
    public class SubCategory
    {
    

        public int id { get; set; }
        public string name { get; set; }
        public int? parentId { get; set; }
        public List<SubCategory> subCategories { get; set; }


    }
}
