﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Base
{
    public abstract class AddressBase
    {
        #region Properties
        public long Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string District { get; set; }

        public string Neighborhood { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public string Fullname { get; set; }

        public string FullAddress { get; set; }

        public string Phone { get; set; }

        public string TaxNumber { get; set; }

        public string TaxOffice { get; set; }
        #endregion
    }
}
