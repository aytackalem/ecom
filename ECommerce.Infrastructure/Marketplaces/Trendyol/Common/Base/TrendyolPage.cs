﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Base
{
    public abstract class TrendyolPage
    {
        #region Properties
        public int Size { get; set; }

        public int TotalPages { get; set; }

        public int Page { get; set; }

        public int TotalElements { get; set; }
        #endregion
    }
}
