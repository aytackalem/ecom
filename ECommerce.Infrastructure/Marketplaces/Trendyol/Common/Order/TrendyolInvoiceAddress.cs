﻿using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common
{
    public class TrendyolInvoiceAddress : AddressBase
    {
        #region Properties
        public string Company { get; set; }
        #endregion
    }
}
