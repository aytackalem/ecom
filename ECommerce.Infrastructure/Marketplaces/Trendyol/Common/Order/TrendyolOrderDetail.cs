﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common
{
    public class TrendyolOrderDetail
    {
        #region Properties
        public int Quantity { get; set; }

        public long ProductId { get; set; }

        public long SalesCampaignId { get; set; }

        public string ProductSize { get; set; }

        public string MerchantSku { get; set; }

        public string ProductName { get; set; }

        public long ProductCode { get; set; }

        public long MerchantId { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        public decimal Discount { get; set; }

        public string ProductColor { get; set; }

        public long Id { get; set; }

        public string Sku { get; set; }

        public decimal VatBaseAmount { get; set; }

        public string Barcode { get; set; }

        public string OrderLineItemStatusName { get; set; }
        #endregion
    }
}
