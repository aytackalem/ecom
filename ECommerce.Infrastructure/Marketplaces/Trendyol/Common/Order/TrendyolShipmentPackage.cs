﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Order
{
    public class TrendyolShipmentPackage
    {
        public List<TrendyolShipmentPackageLine> lines { get; set; }
        public string status { get; set; }
    }

    public class TrendyolShipmentPackageLine
    {
        public long lineId { get; set; }
        public int quantity { get; set; }
    }
}
