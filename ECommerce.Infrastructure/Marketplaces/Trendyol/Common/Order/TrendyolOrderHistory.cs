﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common
{
    public class TrendyolOrderHistory
    {
        #region Properties
        public long CreatedDate { get; set; }

        public string Status { get; set; }
        #endregion
    }
}
