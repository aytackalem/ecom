﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common
{
    public class TrendyolOrder
    {
        #region Properties
        public string OrderNumber { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal GrossAmount { get; set; }

        public decimal TotalDiscount { get; set; }

        public string TaxNumber { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerEmail { get; set; }

        public long CustomerId { get; set; }

        public string CustomerLastName { get; set; }

        public long Id { get; set; }

        public string CargoTrackingNumber { get; set; }

        public string CargoTrackingLink { get; set; }

        public string CargoSenderNumber { get; set; }

        public long OrderDate { get; set; }

        public string TcIdentityNumber { get; set; }

        public string CurrencyCode { get; set; }

        public string ShipmentPackageStatus { get; set; }

        public string CargoProviderName { get; set; }

        public bool FastDelivery { get; set; }

        public string FastDeliveryType { get; set; }

        /// <summary>
        /// Mikro ihracat mi?
        /// </summary>
        public bool Micro { get; set; }

        /// <summary>
        /// Kurumsal fatura mı?
        /// </summary>
        public bool Commercial { get; set; }

        /// <summary>
        /// Hediye paketi
        /// </summary>
        public bool GiftBoxRequested { get; set; }
        #endregion

        #region Navigation Properties
        public TrendyolShipmentAddress ShipmentAddress { get; set; }

        public TrendyolInvoiceAddress InvoiceAddress { get; set; }

        public List<TrendyolOrderDetail> Lines { get; set; }

        public List<TrendyolOrderHistory> PackageHistories { get; set; }
        #endregion
    }
}
