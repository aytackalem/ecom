﻿using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common
{
    public class TrendyolShipmentAddress : AddressBase
    {
        #region Properties
        public int CityCode { get; set; }

        public int DistrictId { get; set; }
        #endregion
    }
}
