﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Product
{
    public class TrendyolProduct
    {
        [JsonProperty("items")]
        public List<TrendyolProductItem> Items { get; set; }
    }
}
