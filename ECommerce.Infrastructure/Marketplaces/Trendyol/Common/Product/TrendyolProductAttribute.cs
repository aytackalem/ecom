﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Product
{
    public class TrendyolProductAttribute
    {
        #region Properties
        [JsonProperty("attributeId")]
        public string AttributeId { get; set; }

        [JsonProperty("attributeValueId")]
        public string AttributeValueId { get; set; }

        [JsonProperty("customAttributeValue")]
        public string CustomAttributeValue { get; set; } 
        #endregion
    }
}
