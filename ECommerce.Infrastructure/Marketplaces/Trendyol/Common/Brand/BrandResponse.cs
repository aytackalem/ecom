﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Brand
{
    public class BrandResponse
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
