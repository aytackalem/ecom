﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.ProductBatchResult
{
  
    public class Item
    {
        public string status { get; set; }
        public List<string> failureReasons { get; set; }
    }



    public class ProductBatchResult
    {
        public string batchRequestId { get; set; }
        public List<Item> items { get; set; }
        public int itemCount { get; set; }
        public int failedItemCount { get; set; }

    }


}
