﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol.Common.InvoiceLinks
{
    public class InvoiceLink
    {
        public string invoiceLink { get; set; }
        public int shipmentPackageId { get; set; }
    }
}
