﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Enums;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Parameters.Stock;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilter;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Brand;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Category;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.InvoiceLinks;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Order;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Product;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.ProductBatchResult;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.ProductFilter;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.ShipmentPackage;
using ECommerce.Infrastructure.Marketplaces.Trendyol.Common.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Order = ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order;

namespace ECommerce.Infrastructure.Marketplaces.Trendyol
{
    public class Trendyol : MarketplaceBase<TrendyolConfiguration>, ICargoChangable, IBatchable
    {
        #region Constants
        private const string _baseUrl = "https://api.trendyol.com/sapigw";
        #endregion

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Trendyol(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Trendyol;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                ///Trendyol Ürün ekleme için kullanılır
                var requestCreate = new TrendyolProduct
                {
                    Items = new List<TrendyolProductItem>()
                };

                //Trendyol Ürün Güncelleme için kullanılır
                var requestUpdate = new TrendyolProduct
                {
                    Items = new List<TrendyolProductItem>()
                };

                foreach (var stockItem in productRequest.ProductItem.StockItems)
                {
                    var item = new TrendyolProductItem
                    {
                        Barcode = stockItem.Barcode,
                        Active = stockItem.Active,
                        Title = stockItem.Title,
                        ProductMainId = productRequest.ProductItem.SellerCode, // Guid.NewGuid().ToString();//
                        BrandId = Convert.ToInt32(productRequest.ProductItem.BrandId), //bakılacak
                        CategoryId = Convert.ToInt32(productRequest.ProductItem.CategoryId),
                        Quantity = stockItem.Quantity,
                        StockCode = stockItem.StockCode,
                        Description = stockItem.Title,
                        CurrencyType = "TRL",
                        ListPrice = Convert.ToDouble(stockItem.ListPrice),
                        SalePrice = Convert.ToDouble(stockItem.SalePrice),
                        VatRate = Convert.ToInt32(stockItem.VatRate * 100)
                    };
                    item.Description = stockItem.Description;
                    item.DeliveryDuration = productRequest.ProductItem.DeliveryDay == 0 ? 2 : (productRequest.ProductItem.DeliveryDay > 9 ? 9 : productRequest.ProductItem.DeliveryDay);
                    item.CargoCompanyId = 11;//Trendyol express
                    item.DimensionalWeight = Convert.ToInt32(productRequest.ProductItem.DimensionalWeight);
                    item.Images = new List<TrendyolProductImage>();
                    item.Attributes = new List<TrendyolProductAttribute>();
                    foreach (var image in stockItem.Images)
                    {
                        item.Images.Add(new TrendyolProductImage
                        {
                            Url = image.Url
                        });
                    }

                    foreach (var attribute in stockItem.Attributes)
                    {
                        if (attribute.AllowCustom)
                        {
                            item.Attributes.Add(new TrendyolProductAttribute
                            {
                                AttributeId = attribute.AttributeCode,
                                CustomAttributeValue = attribute.AttributeValueName,
                                AttributeValueId = null
                            });
                        }
                        else
                        {
                            item.Attributes.Add(new TrendyolProductAttribute
                            {
                                AttributeId = attribute.AttributeCode,
                                AttributeValueId = attribute.AttributeValueCode
                            });
                        }
                    }

                    var filter = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Barcode = stockItem.Barcode
                    });

                    if (filter.Success && filter.Data.ProductItems.Count == 1)
                    {
                        requestUpdate.Items.Add(item);
                    }
                    else if (stockItem.Active) //Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                    {
                        requestCreate.Items.Add(item);
                    }
                }

                var authorization = this._configuration.Authorization;

                var apiResponse = new TrendyolProductResponse();

                if (requestCreate.Items.Count > 0 && string.IsNullOrEmpty(requestCreate.Items[0].Description))
                {
                    requestCreate.Items[0].Description = requestCreate.Items[0].Title;
                }

                if (requestCreate.Items.Count > 0)
                    apiResponse = _httpHelper.Post<TrendyolProduct, TrendyolProductResponse>(requestCreate, $"{_baseUrl}/suppliers/{_configuration.SupplierId}/v2/products", "Basic", authorization, null, null);

                if (requestUpdate.Items.Count > 0)
                {
                    apiResponse = _httpHelper.Put<TrendyolProduct, TrendyolProductResponse>(requestUpdate, $"{_baseUrl}/suppliers/{_configuration.SupplierId}/v2/products", "Basic", authorization);

                    #region UpdatePriceAndStockRequest
                    foreach (var product in requestUpdate.Items)
                    {
                        if (!product.Active)
                        {
                            UpdateStock(new StockRequest
                            {
                                Configurations = productRequest.Configurations,
                                PriceStock = new PriceStock
                                {
                                    StockCode = product.StockCode,
                                    Quantity = 0
                                }
                            });
                        }
                    }
                    #endregion

                }

                if (apiResponse != null && apiResponse.batchRequestId != null)
                {
                    response.Success = true;
                    response.Data = apiResponse.batchRequestId;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var request = new TrendyolPriceStock
                {
                    Items = new List<TrendyolPriceStockItem>
                    {
                        new TrendyolPriceStockItem()
                        {
                            Barcode = stockRequest.PriceStock.Barcode,
                            Quantity = null,
                            ListPrice = stockRequest.PriceStock.ListPrice,
                            SalePrice = stockRequest.PriceStock.SalePrice
                        }
                    }
                };
                var authorization = this._configuration.Authorization;

                var apiResonse = _httpHelper.Post<TrendyolPriceStock, TrendyolStockResponse>(request, $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/price-and-inventory", "Basic", authorization, null, null);

                if (apiResonse != null)
                {
                    response.Data = apiResonse.batchRequestId;
                    response.Success = true;

                    //var productBatchResult = _httpHelper.Get<ProductBatchResult>($"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{apiResonse.batchRequestId}", "Basic", authorization);
                    //if (productBatchResult == null)
                    //{
                    //    Thread.Sleep(new TimeSpan(0, 0, 3));
                    //    productBatchResult = _httpHelper.Get<ProductBatchResult>($"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{apiResonse.batchRequestId}", "Basic", authorization);

                    //}

                    //if (productBatchResult.failedItemCount > 0)
                    //{
                    //    response.Success = productBatchResult.items.Any(x =>
                    //    x.failureReasons.Any(y => y.Contains("ürün bulunamadı")));
                    //}
                    //else if (productBatchResult.itemCount == 1)
                    //{
                    //    response.Success = productBatchResult.items.Any(x => x.status == "SUCCESS");
                    //}
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {


                var request = new TrendyolPriceStock();
                request.Items = new List<TrendyolPriceStockItem>();
                request.Items.Add(new TrendyolPriceStockItem()
                {
                    Barcode = stockRequest.PriceStock.Barcode,
                    Quantity = stockRequest.PriceStock.Quantity,
                    ListPrice = stockRequest.UpdatePrice ? stockRequest.PriceStock.ListPrice : null,
                    SalePrice = stockRequest.UpdatePrice ? stockRequest.PriceStock.SalePrice : null
                });
                var authorization = this._configuration.Authorization;

                var apiResonse = _httpHelper.Post<TrendyolPriceStock, TrendyolStockResponse>(request, $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/price-and-inventory", "Basic", authorization, null, null);

                if (apiResonse != null)
                {
                    response.Data = apiResonse.batchRequestId;
                    response.Success = true;
                    //var productBatchResult = _httpHelper.Get<ProductBatchResult>($"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{apiResonse.batchRequestId}", "Basic", authorization);
                    //if (productBatchResult == null)
                    //{
                    //    Thread.Sleep(new TimeSpan(0, 0, 3));
                    //    productBatchResult = _httpHelper.Get<ProductBatchResult>($"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{apiResonse.batchRequestId}", "Basic", authorization);

                    //}

                    //if (productBatchResult != null)
                    //{


                    //    if (productBatchResult.failedItemCount > 0)
                    //    {
                    //        response.Success = productBatchResult.items.Any(x =>
                    //        x.failureReasons.Any(y => y.Contains("ürün bulunamadı")));
                    //    }
                    //    else if (productBatchResult.itemCount == 1)
                    //    {
                    //        response.Success = productBatchResult.items.Any(x => x.status == "SUCCESS");
                    //    }
                    //}
                }




            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new List<ProductItem>();


                var supplierId = this._configuration.SupplierId;
                var authorization = this._configuration.Authorization;

                var trendyolFilterProducts = new List<TrendyolFilterProduct>();
                var page = 0;
                var size = 1000;
                TrendyolFilterProductPage trendyolFilterProductPageResult = null;
                do
                {
                    var trendyolApiUrl = $"{_baseUrl}/suppliers/{supplierId}/products?approved=true&page={page}&size={size}";



                    trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                    if (trendyolFilterProductPageResult == null)
                    {
                        Thread.Sleep(5000);

                        trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                        if (trendyolFilterProductPageResult == null)
                        {
                            response.Success = false;
                            return;
                        }

                    }

                    if (trendyolFilterProductPageResult != null && trendyolFilterProductPageResult.content != null)
                    {
                        trendyolFilterProducts.AddRange(trendyolFilterProductPageResult.content);
                    }



                    page++;
                } while ((trendyolFilterProductPageResult.page + 1) < trendyolFilterProductPageResult.totalPages);



                var products = trendyolFilterProducts.GroupBy(x => x.productMainId).Select(x => new { TrendyolFilterProducts = x.ToList(), x.Key }).ToList();

                response.Data = new List<ProductItem>();

                response.Data.AddRange(products.Select(c => new ProductItem
                {
                    CategoryId = c.TrendyolFilterProducts.First().pimCategoryId.ToString(),
                    SellerCode = c.Key,
                    BrandId = c.TrendyolFilterProducts.First().brandId.ToString(),
                    BrandName = c.TrendyolFilterProducts.First().brand,
                    CategoryName = c.TrendyolFilterProducts.First().categoryName,
                    DimensionalWeight = c.TrendyolFilterProducts.First().dimensionalWeight,
                    Name = c.TrendyolFilterProducts.First().title,
                    StockItems = c.TrendyolFilterProducts.Select(x => new StockItem
                    {
                        MarketplaceProductId = x.productContentId.ToString(),
                        Barcode = x.barcode,
                        BrandName = x.brand,
                        Title = x.title,
                        Active = x.approved,
                        VatRate = x.vatRate,
                        Subtitle = "",
                        Description = x.description,
                        Images = x.images?.
                                        Select(i => new ECommerce.Application.Common.Parameters.Product.Image
                                        {
                                            Order = x.images.IndexOf(i),
                                            Url = i.url
                                        }).
                                        ToList(),
                        Quantity = x.quantity,
                        ListPrice = (decimal)x.listPrice,
                        SalePrice = (decimal)x.salePrice,
                        StockCode = x.stockCode,
                        Attributes = x.attributes?.
                                        Select(a => new ECommerce.Application.Common.Parameters.Product.CategoryAttribute
                                        {
                                            AttributeCode = a.attributeId.ToString(),
                                            AttributeName = a.attributeName,
                                            AttributeValueCode = a.attributeValueId.ToString(),
                                            AttributeValueName = a.attributeValue,
                                            Mandatory = true
                                        }).
                                        ToList()
                    }).ToList()
                }));

                response.Success = true;
            });

        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new List<MarketplaceProductStatus>();

                var supplierId = this._configuration.SupplierId;
                var authorization = this._configuration.Authorization;

                if (string.IsNullOrEmpty(this._configuration.SupplierId) || string.IsNullOrEmpty(this._configuration.Username) || string.IsNullOrEmpty(this._configuration.Password))
                {
                    response.Success = false;
                    return;
                }

                var trendyolFilterProducts = new List<TrendyolFilterProduct>();
                var page = 0;
                var size = 1000;
                TrendyolFilterProductPage trendyolFilterProductPageResult = null;
                do
                {
                    var trendyolApiUrl = $"{_baseUrl}/suppliers/{supplierId}/products?page={page}&size={size}";

                    trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                    if (trendyolFilterProductPageResult != null && trendyolFilterProductPageResult.content != null)
                        foreach (var theContent in trendyolFilterProductPageResult.content)
                        {
                            var marketplaceProductStatus = new MarketplaceProductStatus
                            {
                                MarketplaceId = this.MarketplaceId,
                                MarketplaceProductId = theContent.productMainId,
                                Barcode = theContent.barcode,
                                StockCode = null,
                                Opened = true,
                                OnSale = theContent.onsale,
                                Locked = theContent.locked || theContent.archived || theContent.blacklisted,

                                Url = theContent.productContentId > 0 ? $"https://www.trendyol.com/a/a-p-{theContent.productContentId}" : "",
                                ListUnitPrice = (decimal)theContent.listPrice,
                                UnitPrice = (decimal)theContent.salePrice,
                                Stock = theContent.quantity
                            };
                            response.Data.Add(marketplaceProductStatus);
                        }

                    page++;
                } while ((trendyolFilterProductPageResult.page + 1) < trendyolFilterProductPageResult.totalPages);

                response.Success = true;
            });
        }

        public DataResponse<bool> StockCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var url = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{batchId}";
                var productBatchResult = _httpHelper.Get<ProductBatchResult>(url, "Basic", authorization);
                if (productBatchResult.items[0].status == "SUCCESS")
                {
                    response.Success = true;
                    response.Data = true;
                }
                else if (productBatchResult.failedItemCount > 0)
                {
                    response.Success = true;
                    response.Data = false;
                    response.Message = String.Join("", productBatchResult.items[0].failureReasons);
                }
            });
        }

        public DataResponse<bool> PriceCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var url = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{batchId}";
                var productBatchResult = _httpHelper.Get<ProductBatchResult>(url, "Basic", authorization);
                if (productBatchResult.items[0].status == "SUCCESS")
                {
                    response.Success = true;
                    response.Data = true;
                }
                else if (productBatchResult.failedItemCount > 0)
                {
                    response.Success = true;
                    response.Data = false;
                    response.Message = String.Join("", productBatchResult.items[0].failureReasons);
                }
            });
        }

        public DataResponse<bool> CreateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var url = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{batchId}";
                var productBatchResult = _httpHelper.Get<ProductBatchResult>(url, "Basic", authorization);
                if (productBatchResult.items[0].status == "SUCCESS")
                {
                    response.Success = true;
                    response.Data = true;
                }
                else if (productBatchResult.failedItemCount > 0)
                {
                    response.Success = true;
                    response.Data = false;
                    response.Message = String.Join("", productBatchResult.items[0].failureReasons);
                }
            });
        }

        public DataResponse<bool> UpdateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var url = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/products/batch-requests/{batchId}";
                var productBatchResult = _httpHelper.Get<ProductBatchResult>(url, "Basic", authorization);
                if (productBatchResult.items[0].status == "SUCCESS")
                {
                    response.Success = true;
                    response.Data = true;
                }
                else if (productBatchResult.failedItemCount > 0)
                {
                    response.Success = true;
                    response.Data = false;
                    response.Message = String.Join("", productBatchResult.items[0].failureReasons);
                }
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {

                var authorization = this._configuration.Authorization;

                CategoryRoot root = _httpHelper.Get<CategoryRoot>($"{_baseUrl}/product-categories", "Basic", authorization);

                root.categories = root.categories.Where(x => x.id == 685).ToList();


                var categoryAttributeRoots = new List<CategoryAttributeRoot>();

                if (root != null)
                {
                    foreach (var category in root.categories)
                    {

                        if (category != null)
                        {
                            categoryAttributeRoots.AddRange(Recursive(category, authorization));
                        }
                    }
                }

                var data = categoryAttributeRoots.Where(x => x != null).Select(x => new CategoryResponse
                {
                    Code = x.id.ToString(),
                    Name = x.name,
                    CategoryAttributes = x?.categoryAttributes?.Select(ca => new CategoryAttributeResponse
                    {
                        CategoryCode = x.id.ToString(),
                        Code = ca.attribute.id.ToString(),
                        Name = ca.attribute.name,
                        Mandatory = ca.required,
                        AllowCustom = ca.allowCustom,
                        CategoryAttributesValues = ca?.attributeValues?.Select(av => new CategoryAttributeValueResponse
                        {
                            VarinatCode = ca.attribute.id.ToString(),
                            Code = av.id.ToString(),
                            Name = av.name
                        }).ToList()
                    }).ToList()
                }).ToList();


                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                var authorization = this._configuration.Authorization;
                var size = 200;
                var page = 0;
                var totalPages = 0;
                var status = "Created";
                var orderByField = "PackageLastModifiedDate";
                var orderByDirection = "DESC";
                var preUrl = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/orders?size={size}&orderByField={orderByField}&orderByDirection={orderByDirection}&status={status}";

                do
                {
                    var trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"{preUrl}&page={page}", "Basic", authorization);
                    totalPages = trendyolOrderPages.TotalPages;

                    if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                    {
                        var orders = MapOrder(trendyolOrderPages.Content);

                        response.Data.Orders.AddRange(orders);
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Errorfrom trendyol service";
                    }

                    page++;
                } while (page < totalPages);
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                var authorization = this._configuration.Authorization;
                var size = 200;
                var page = 0;
                var totalPages = 0;
                var preUrl = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/orders";

                do
                {

                    var url = string.Empty;

                    if (orderRequest.StartDate.HasValue && orderRequest.EndDate.HasValue)
                    {
                        long startDate = orderRequest.StartDate.Value.ConvertToUnixTimeStamp();
                        long endDate = orderRequest.EndDate.Value.ConvertToUnixTimeStamp();
                        string status = "Created,Picking,Shipped,Delivered";

                        url = $"{preUrl}?page={page}&size={size}&startDate={startDate}&endDate={endDate}&status={status}";
                    }
                    else if (string.IsNullOrEmpty(orderRequest.MarketplaceOrderCode))
                    {
                        string status = "Created";
                        string orderByField = "PackageLastModifiedDate";
                        string orderByDirection = "DESC";

                        url = $"{preUrl}?page={page}&size={size}&orderByField={orderByField}&orderByDirection={orderByDirection}&status={status}";
                    }
                    else
                    {
                        url = $"{preUrl}?orderNumber={orderRequest.MarketplaceOrderCode}";
                    }

                    var trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>(url, "Basic", authorization);
                    totalPages = trendyolOrderPages.TotalPages;

                    /*
                     * Trendyol servisinden cevap gelip gelmediği kontrol edilir.
                     */
                    if (trendyolOrderPages != null && trendyolOrderPages.Content != null && trendyolOrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "";

                        break;
                    }

                    page++;

                } while (page < totalPages);

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderTypeUpdateResponse>>((response) =>
            {
                if (orderTypeUpdateRequest.MarketplaceStatus == MarketplaceStatus.Picking)
                {
                    var authorization = this._configuration.Authorization;
                    var supplierId = _configuration.SupplierId;
                    var marketplaceOrderNumber = orderTypeUpdateRequest.Order.OrderCode;
                    var url = $"{_baseUrl}/suppliers/{supplierId}/shipment-packages/{orderTypeUpdateRequest.Order.OrderShipment.PackageNumber}";

                    var requestObject = new TrendyolShipmentPackage
                    {
                        lines = orderTypeUpdateRequest.Order.OrderDetails.Select(od => new TrendyolShipmentPackageLine
                        {
                            lineId = long.Parse(od.UId),
                            quantity = od.Quantity
                        }).ToList(),
                        status = "Picking"
                    };
                    response.Success = _httpHelper.Put<TrendyolShipmentPackage>(requestObject, url, "Basic", authorization);
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG

                if (request.Status == MarketplaceStatus.Picking)
                {
                    var supplierId = this._configuration.SupplierId;
                    var authorization = this._configuration.Authorization;

                    var url = $"{_baseUrl}/suppliers/{supplierId}/shipment-packages/{request.OrderPicking.MarketPlaceOrderId}";

                    var aa = _httpHelper.Put<TrendyolShipmentPackage, Response>(new TrendyolShipmentPackage
                    {
                        lines = request.OrderPicking.OrderDetails.Select(l => new TrendyolShipmentPackageLine
                        {
                            lineId = Convert.ToInt64(l.Id),
                            quantity = l.Quantity
                        }).ToList(),
                        status = "Picking"
                    }, url, "Basic", authorization);
                }



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse { Orders = new List<Order>() };

                var page = 0;
                var totalPages = 1;
                var size = 200;
                var authorization = this._configuration.Authorization;
                var status = "Delivered";

                do
                {
                    var startDate = DateTime.Now.AddDays(-14).ConvertToUnixTimeStamp();
                    var endDate = DateTime.Now.ConvertToUnixTimeStamp();
                    var url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&status={status}&startDate={startDate}&endDate={endDate}";
                    var trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>(url, "Basic", authorization);
                    if (trendyolOrderPages.Content == null) return;

                    if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                        response.Success = true;
                    }

                    totalPages = trendyolOrderPages.TotalPages;
                    page++;
                } while (page < totalPages);

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string status = "Cancelled,UnSupplied";

                var authorization = this._configuration.Authorization;

                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                int totalPage = 1;
                while ((page + 1) <= totalPage)
                {
                    TrendyolOrderPage trendyolOrderPages = null;

                    String url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&status={status}";

                    trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>(url, "Basic", authorization);

                    totalPage = trendyolOrderPages.TotalPages;
                    page++;

                    if (trendyolOrderPages != null && trendyolOrderPages.Content != null && trendyolOrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Errorfrom trendyol service";
                    }
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string orderByDirection = "ASC";
                string status = "Returned";

                var authorization = this._configuration.Authorization;

                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                for (int i = 5; i >= 0; i--)
                {
                    int totalPage = 1;
                    var startDate = DateTime.Now.ToUniversalTime().Date.AddDays(i * -1);
                    var endDate = DateTime.Now.ToUniversalTime().Date.AddDays((i - 1) * -1);

                    while ((page + 1) <= totalPage)
                    {
                        TrendyolOrderPage trendyolOrderPages = null;

                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&orderByDirection={orderByDirection}&status={status}&startDate={startDate.ConvertToUnixTimeStamp()}&endDate={endDate.ConvertToUnixTimeStamp()}", "Basic", authorization);

                        totalPage = trendyolOrderPages.TotalPages;
                        page++;
                        if (trendyolOrderPages != null && trendyolOrderPages.Content != null && trendyolOrderPages.Content.Count > 0)
                        {
                            response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                            response.Success = true;
                        }
                        else
                        {
                            response.Success = false;
                            response.Message = "Error from trendyol service";
                        }
                    }

                    page = 0;
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var authorization = this._configuration.Authorization;

                var trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?orderNumber={orderRequest.MarketplaceOrderCode}", "Basic", authorization);

                var trendyolOrder = trendyolOrderPages.Content.FirstOrDefault(x => x.ShipmentPackageStatus.ToLower() == "cancelled" && x.CargoTrackingNumber == orderRequest.TrackingCode);
                if (trendyolOrder != null)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Siparişiniz iptal statüsündedir. Trendyol panelinden siparişinizi kontrol ediniz.";
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        public DataResponse<List<KeyValue<string, string>>> GetChangableCargoCompanies(string packageNumber)
        {
            return new DataResponse<List<KeyValue<string, string>>>
            {
                Data = new List<KeyValue<string, string>> {
                    new KeyValue<string, string>{ Key = "YKMP", Value = "Yurtiçi Kargo"},
                    new KeyValue<string, string>{ Key = "ARASMP", Value = "Aras Kargo"},
                    new KeyValue<string, string>{ Key = "SURATMP", Value = "Sürat Kargo"},
                    new KeyValue<string, string>{ Key = "HOROZMP", Value = "Horoz Lojistik"},
                    new KeyValue<string, string>{ Key = "MNGMP", Value = "MNG Kargo"},
                    new KeyValue<string, string>{ Key = "PTTMP", Value = "PTT Kargo"},
                    new KeyValue<string, string>{ Key = "TEXMP", Value = "Trendyol Express"},
                    new KeyValue<string, string>{ Key = "UPSMP", Value = "UPS"}
                },
                Success = true
            };
        }

        public DataResponse<ChangeCargoCompanyResponse> ChangeCargoCompany(string packageNumber, string cargoCompanyId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ChangeCargoCompanyResponse>>(response =>
            {
                var authorization = this._configuration.Authorization;
                var url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/shipment-packages/{packageNumber}/cargo-providers";
                var shipmentPackageRequest = new ShipmentPackageRequest
                {
                    cargoProvider = cargoCompanyId
                };

                response.Success = _httpHelper.Put(shipmentPackageRequest, url, "Basic", authorization);
                response.Data = new ChangeCargoCompanyResponse
                {
                    Retake = true
                };

                var shipmentCompanyId = string.Empty;
                switch (cargoCompanyId)
                {
                    case "YKMP":
                        shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                        break;
                    case "ARASMP":
                        shipmentCompanyId = ShipmentCompanies.ArasKargo;
                        break;
                    case "SURATMP":
                        shipmentCompanyId = ShipmentCompanies.SuratKargo;
                        break;
                    case "HOROZMP":
                        shipmentCompanyId = ShipmentCompanies.HorozLojistik;
                        break;
                    case "MNGMP":
                        shipmentCompanyId = ShipmentCompanies.MngKargo;
                        break;
                    case "PTTMP":
                        shipmentCompanyId = ShipmentCompanies.PttKargo;
                        break;
                    case "TEXMP":
                        shipmentCompanyId = ShipmentCompanies.TrendyolExpress;
                        break;
                    case "UPSMP":
                        shipmentCompanyId = ShipmentCompanies.Ups;
                        break;
                }

                response.Data.ShipmentCompanyId = shipmentCompanyId;
            });
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {
                var marketplaceBrands = this._httpHelper.Get<List<BrandResponse>>($"{_baseUrl}/brands/by-name?name={q}", null, null);


                response.Data = marketplaceBrands.Select(x => new KeyValue<string, string>
                {
                    Key = x.id.ToString(),
                    Value = x.name
                }
               ).ToList();

                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var authorization = this._configuration.Authorization;

                var preUrl = $"{_baseUrl}/suppliers/{_configuration.SupplierId}/supplier-invoice-links";

                var request = new InvoiceLink
                {
                    invoiceLink = invoiceLink.Replace(" ", "%20"),
                    shipmentPackageId = Convert.ToInt32(shipmentPackageId)
                };

                var apiResponse = _httpHelper.Post<InvoiceLink, string>(request, preUrl, "Basic", authorization, null, null);

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(TrendyolConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.SupplierId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "SupplierId").Value;
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }
        #endregion

        #region Helper Methods
        private List<CategoryAttributeRoot> Recursive(SubCategory category, string authorization)
        {
            var categoryAttributeRoots = new List<CategoryAttributeRoot>();

            if (category.subCategories == null || category.subCategories.Count == 0)
            {

                var categoryAttributeRoot = _httpHelper.Get<CategoryAttributeRoot>($"{_baseUrl}/product-categories/{category.id}/attributes", "Basic", authorization);

                if (categoryAttributeRoot != null)
                    categoryAttributeRoots.Add(categoryAttributeRoot);


            }

            foreach (var scLoop in category.subCategories)
            {
                categoryAttributeRoots.AddRange(Recursive(scLoop, authorization));
            }

            return categoryAttributeRoots;
        }

        private List<Order> MapOrder(List<TrendyolOrder> providerOrders)
        {
            List<Order> orders = new();

            foreach (var pOrder in providerOrders)
            {
                if (pOrder.CargoTrackingNumber == "0") continue;
                try
                {
                    var shipmentCompanyId = "ARS";
                    if (!string.IsNullOrEmpty(pOrder.CargoTrackingNumber))
                    {
                        if (pOrder.CargoTrackingNumber.StartsWith("733"))
                            shipmentCompanyId = ShipmentCompanies.TrendyolExpress;
                        else if (pOrder.CargoTrackingNumber.StartsWith("725"))
                            shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("726"))
                            shipmentCompanyId = ShipmentCompanies.ArasKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("727"))
                            shipmentCompanyId = ShipmentCompanies.SuratKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("728"))
                            shipmentCompanyId = ShipmentCompanies.MngKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("729"))
                            shipmentCompanyId = ShipmentCompanies.Ups;
                        else if (pOrder.CargoTrackingNumber.StartsWith("734"))
                            shipmentCompanyId = ShipmentCompanies.PttKargo;
                    }

                    Order order = new()
                    {
                        UId = $"{Application.Common.Constants.Marketplaces.Trendyol}_{pOrder.OrderNumber}-{pOrder.CargoTrackingNumber}",
                        OrderSourceId = OrderSources.Pazaryeri,
                        MarketplaceId = Application.Common.Constants.Marketplaces.Trendyol,
                        OrderCode = pOrder.OrderNumber,
                        OrderTypeId = "OS",
                        Micro = pOrder.Micro,
                        Commercial = pOrder.Commercial,
                        TotalAmount = pOrder.TotalPrice,
                        ListTotalAmount = pOrder.GrossAmount,
                        Discount = pOrder.TotalDiscount,
                        OrderDate = pOrder.OrderDate.ConvertToDateTime(),
                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                        {
                            PackageNumber = pOrder.Id.ToString(),
                            TrackingCode = pOrder.CargoTrackingNumber,
                            ShipmentTypeId = "PND",
                            TrackingUrl = pOrder.CargoTrackingLink,
                            ShipmentCompanyId = shipmentCompanyId //.ToLower() == "trendyol express marketplace" ? "TYEX" : "SRTTY" //Lafaba için yapıldı fatura kesilirken lazım oluyor
                        },
                        Customer = new Customer()
                        {
                            FirstName = pOrder.CustomerFirstName,
                            LastName = pOrder.CustomerLastName,
                            Mail = pOrder.CustomerEmail,
                            TaxNumber = pOrder.TaxNumber,
                            Phone = "" //TODO set phone number
                        },
                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                        {
                            FirstName = pOrder.ShipmentAddress.Firstname,
                            LastName = pOrder.ShipmentAddress.Lastname,
                            Country = pOrder.ShipmentAddress.CountryCode.ToUpper() == "TR" ? "Türkiye" : "",
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.ShipmentAddress.Phone, //TODO set phone number
                            City = pOrder.ShipmentAddress.City,
                            District = pOrder.ShipmentAddress.District,
                            Neighborhood = pOrder.ShipmentAddress.Neighborhood,
                            Address = $"{pOrder.ShipmentAddress.Address1} {pOrder.ShipmentAddress.Address2}"
                        },
                        OrderInvoiceAddress = new OrderInvoiceAddress()
                        {
                            FirstName = pOrder.InvoiceAddress.Firstname,
                            LastName = pOrder.InvoiceAddress.Lastname,
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.InvoiceAddress.Phone, //TODO set phone number
                            City = pOrder.InvoiceAddress.City,
                            District = pOrder.InvoiceAddress.District,
                            Address = pOrder.Micro
                                ? pOrder.InvoiceAddress.FullAddress
                                : $"{pOrder.InvoiceAddress.Address1} {pOrder.InvoiceAddress.Address2}",
                            Neighborhood = !string.IsNullOrEmpty(pOrder.InvoiceAddress.Neighborhood) ? pOrder.InvoiceAddress.Neighborhood : pOrder.InvoiceAddress.District,
                            TaxNumber = pOrder.Commercial ? pOrder.InvoiceAddress.TaxNumber : "",
                            TaxOffice = pOrder.Commercial ? pOrder.InvoiceAddress.TaxOffice : "",
                            Country = pOrder.InvoiceAddress.CountryCode

                        },
                        Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                    };
                    order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                    {
                        CurrencyId = "TRY",
                        PaymentTypeId = "OO",
                        Amount = pOrder.TotalPrice,
                        Date = pOrder.OrderDate.ConvertToDateTime()
                    });

                    order.OrderPicking = new OrderPicking
                    {
                        MarketPlaceOrderId = pOrder.Id.ToString(),
                        OrderDetails = new List<OrderPickingDetail>()
                    };

                    #region Order Details
                    order.OrderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();
                    foreach (var orderDetail in pOrder.Lines)
                    {
                        order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                        {
                            UId = orderDetail.Id.ToString(),
                            TaxRate = Convert.ToDouble(orderDetail.VatBaseAmount) / 100,
                            UnitPrice = orderDetail.Price,
                            ListPrice = orderDetail.Amount,
                            UnitDiscount = orderDetail.Discount,
                            Product = new Product()
                            {
                                Barcode = orderDetail.Barcode.ToUpper(),
                                Name = orderDetail.ProductName,
                                StockCode = orderDetail.MerchantSku?.ToString()
                            },
                            Quantity = orderDetail.Quantity
                        });
                        order.OrderPicking.OrderDetails.Add(new OrderPickingDetail
                        {
                            Id = orderDetail.Id.ToString(),
                            Quantity = orderDetail.Quantity,
                        });
                    }
                    #endregion

                    orders.Add(order);
                }
                catch (Exception ex)
                {

                }
            }

            return orders;
        }

        private DataResponse<ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilterResponse>>((response) =>
            {
                response.Data = new ECommerce.Application.Common.Wrappers.Marketplaces.ProductFilterResponse
                {
                    ProductItems = new List<ProductFilterItem>()
                };

                var supplierId = this._configuration.SupplierId;
                var authorization = this._configuration.Authorization;

                var trendyolFilterProducts = new List<TrendyolFilterProduct>();
                var page = 0;
                var size = 1000;
                TrendyolFilterProductPage trendyolFilterProductPageResult = null;
                do
                {
                    var trendyolApiUrl = $"{_baseUrl}/suppliers/{supplierId}/products?approved=true&page={page}&size={size}";
                    if (!string.IsNullOrEmpty(productFilter.Barcode))
                    {
                        trendyolApiUrl += $"&barcode={productFilter.Barcode}";
                    }

                    if (!string.IsNullOrEmpty(productFilter.Stockcode))
                    {
                        trendyolApiUrl += $"&stockCode={productFilter.Stockcode}";
                    }

                    trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                    if (trendyolFilterProductPageResult == null)
                    {
                        Thread.Sleep(5000);

                        trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                        if (trendyolFilterProductPageResult == null)
                        {
                            response.Success = false;
                            return;
                        }

                    }

                    if (trendyolFilterProductPageResult != null && trendyolFilterProductPageResult.content != null)
                    {
                        trendyolFilterProductPageResult.content.ForEach(c =>
                        {
                            response.Data.ProductItems.Add(new ProductFilterItem
                            {
                                CategoryId = c.pimCategoryId.ToString(),
                                SellerCode = c.productMainId,
                                Barcode = c.barcode,
                                CurrencyCode = "TRL",
                                Description = c.description,
                                Title = c.title,
                                VatRate = c.vatRate,
                                StockItems = new List<ProductFilterStockItem>
                            {
                                new ProductFilterStockItem
                                {
                                    Barcode = c.barcode,
                                    Images = c.images?.
                                                Select(i => new ProductFilterImage
                                                    {
                                                        Order = c.images.IndexOf(i),
                                                        Url = i.url
                                                    }).
                                                ToList(),
                                    Quantity = c.quantity,
                                    ListPrice = (decimal)c.listPrice,
                                    SalePrice = (decimal)c.salePrice,
                                    SellerStockCode = c.stockCode,
                                    Attributes = c.attributes?.
                                                Select(a => new ProductFilterCategoryAttribute
                                                    {
                                                        AttributeCode = a.attributeId.ToString(),
                                                        AttributeName = a.attributeName,
                                                        AttributeValueCode = a.attributeValueId.ToString(),
                                                        AttributeValueName = a.attributeValue,
                                                        Mandatory = true
                                                    }).
                                                ToList()
                                }
                            }
                            });
                        });

                        trendyolFilterProducts.AddRange(trendyolFilterProductPageResult.content);
                    }



                    page++;
                } while ((trendyolFilterProductPageResult.page + 1) < trendyolFilterProductPageResult.totalPages);

                response.Success = true;
            });
        }


        #endregion
    }
}