﻿using System.Xml.Serialization;

namespace ECommerce.Infrastructure.Marketplaces.N11.Common.Stock
{

	public class UpdateStockByStockIdResponse
	{

		[XmlElement(ElementName = "result")]
		public Result Result { get; set; }

		[XmlElement(ElementName = "stockItems")]
		public StockItems StockItems { get; set; }


	}

	[XmlRoot(ElementName = "result")]
	public class Result
	{

		[XmlElement(ElementName = "status")]
		public string Status { get; set; }
	}

	[XmlRoot(ElementName = "stockItem")]
	public class StockItem
	{

		[XmlElement(ElementName = "attributes")]
		public object Attributes { get; set; }

		[XmlElement(ElementName = "id")]
		public double Id { get; set; }

		[XmlElement(ElementName = "stock")]
		public int Stock { get; set; }

		[XmlElement(ElementName = "version")]
		public int Version { get; set; }
	}

	[XmlRoot(ElementName = "stockItems")]
	public class StockItems
	{

		[XmlElement(ElementName = "stockItem")]
		public StockItem StockItem { get; set; }
	}
}
