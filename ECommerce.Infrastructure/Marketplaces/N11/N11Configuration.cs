﻿namespace ECommerce.Infrastructure.Marketplaces.N11
{
    public class N11Configuration
    {
        #region Properties
        public string AppSecret { get; internal set; }

        public string AppKey { get; internal set; }

        public string ShipmentTemplate { get; internal set; }
        #endregion
    }
}
