﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Parameters.Stock;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.N11.Common;
using ECommerce.Infrastructure.Marketplaces.N11.Common.Stock;
using N11CategoryService;
using N11OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ProductService = N11ProductService;

namespace ECommerce.Infrastructure.Marketplaces.N11
{
    public class N11 : MarketplaceBase<N11Configuration>
    {
        #region Constructors
        public N11(IUnitOfWork unitOfWork, IServiceProvider serviceProvider) : base(serviceProvider)
        {

        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.N11;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var _productServicePortClient = new ProductService.ProductServicePortClient();
                var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

                var getCategoryAttributesIdRequest = new N11CategoryService.GetCategoryAttributesIdRequest
                {
                    auth = new N11CategoryService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                    categoryId = Convert.ToInt64(productRequest.ProductItem.CategoryId)
                };

                var categoryAttributes = n11CategoryService.GetCategoryAttributesIdAsync(getCategoryAttributesIdRequest).Result;
                var categoryProductAttributeList = categoryAttributes.GetCategoryAttributesIdResponse.categoryProductAttributeList.FirstOrDefault(x => x.name == "Marka");
                var success = false;
                var failed = false;
                foreach (var siLoop in productRequest.ProductItem.StockItems)
                {

                    var request = new ProductService.SaveProductRequest();
                    request.auth = new ProductService.Authentication
                    {
                        appKey = this._configuration.AppKey,
                        appSecret = this._configuration.AppSecret
                    };

                    var subtitle = siLoop.Subtitle;
                    var title = siLoop.Title;
                    if (siLoop.Title.Length > 65)
                    {
                        title = siLoop.Title.Substring(0, 65);
                    }

                    if (subtitle.Length < 3)
                    {
                        subtitle = title;
                    }

                    request.product = new ProductService.ProductRequest();
                    request.product.productSellerCode = siLoop.StockCode;
                    request.product.title = title;
                    request.product.subtitle = subtitle;
                    request.product.description = siLoop.Description;
                    request.product.category = new ProductService.CategoryRequest() { id = Convert.ToInt64(productRequest.ProductItem.CategoryId) };

                    request.product.domestic = true;
                    request.product.currencyType = "1"; //productItem.CurrencyCode
                    request.product.images = siLoop.Images.Select(im => new ProductService.ProductImage
                    {
                        order = (siLoop.Images.IndexOf(im) + 1).ToString(),
                        url = im.Url
                    }).ToArray();
                    request.product.approvalStatus = "1";
                    request.product.productCondition = "1";
                    request.product.shipmentTemplate = this._configuration.ShipmentTemplate;



                    var discount = Convert.ToDecimal(siLoop.ListPrice) - Convert.ToDecimal(siLoop.SalePrice);
                    var productPrice = discount > 0 ? siLoop.ListPrice : siLoop.SalePrice;

                    if (discount > 0)
                        request.product.discount = new ProductService.ProductDiscountRequest
                        {
                            type = "1",
                            value = discount < 0 ? "0" : discount.ToString()

                        };



                    request.product.price = productPrice;

                    var productAttributeRequests = new List<ProductService.ProductAttributeRequest>();


                    foreach (var attribute in siLoop.Attributes)
                    {
                        var productAttributeRequest = new ProductService.ProductAttributeRequest()
                        {
                            name = attribute.AttributeName,
                            value = attribute.AttributeValueName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }
                    if (categoryProductAttributeList != null && categoryProductAttributeList.mandatory)
                    {
                        var productAttributeRequest = new ProductService.ProductAttributeRequest()
                        {
                            name = "Marka",
                            value = productRequest.ProductItem.BrandName
                        };
                        productAttributeRequests.Add(productAttributeRequest);
                    }

                    request.product.attributes = productAttributeRequests.ToArray();
                    request.product.preparingDay = productRequest.ProductItem.DeliveryDay.ToString();

                    var productSkuRequests = new List<ProductService.ProductSkuRequest>();

                    var filter = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Stockcode = siLoop.StockCode
                    });


                    var rnd = new Random();
                    var n11CatalogId = rnd.Next(1000, 999999999);

                    var productSkuRequest = new ProductService.ProductSkuRequest();
                    productSkuRequest.gtin = siLoop.Barcode;
                    productSkuRequest.n11CatalogId = n11CatalogId;
                    productSkuRequest.quantity = siLoop.Active ? siLoop.Quantity.ToString() : "0";
                    productSkuRequest.sellerStockCode = siLoop.StockCode;
                    productSkuRequest.optionPrice = productPrice;
                    productSkuRequest.images = new ProductService.ProductImage[siLoop.Images.Count];

                    for (int i = 0; i < siLoop.Images.Count; i++)
                    {
                        productSkuRequest.images[i] = new ProductService.ProductImage
                        {
                            order = (i + 1).ToString(),
                            url = siLoop.Images[i].Url
                        };
                    }

                    var stProductAttributeRequests = new List<ProductService.ProductAttributeRequest>();

                    foreach (var attr in siLoop.Attributes.Where(x => x.Varinatable))
                    {
                        var productAttribute = new ProductService.ProductAttributeRequest()
                        {
                            name = attr.AttributeName,
                            value = attr.AttributeValueName
                        };

                        stProductAttributeRequests.Add(productAttribute);
                    }


                    productSkuRequest.attributes = stProductAttributeRequests.ToArray();
                    productSkuRequests.Add(productSkuRequest);


                    request.product.stockItems = productSkuRequests.ToArray();

                    try
                    {

                        var apiResponse = _productServicePortClient.SaveProductAsync(request).Result;

                        if (apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.notAvailableForUpdateForSixtySeconds")
                        {
                            Thread.Sleep(new TimeSpan(0, 0, 10));
                            apiResponse = _productServicePortClient.SaveProductAsync(request).Result;
                        }

                        var status = apiResponse.SaveProductResponse.result.errorCode == "SELLER_API.catalog.suggest.failed.alreadySuggested" || apiResponse.SaveProductResponse.result.errorCode == "stockItemsCannotBeUpdated" || apiResponse.SaveProductResponse.result.errorCode == "PRODUCT.categoryUpdateNotAllowed";

                        success = apiResponse.SaveProductResponse.result.status == "success" || status;

                        n11CategoryService.InnerChannel.Close();
                        _productServicePortClient.InnerChannel.Close();

                        if (failed == false && apiResponse.SaveProductResponse.result.status == "failure")
                            failed = !status;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        failed = true;
                    }

                }
                response.Success = success && !failed;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var _productServicePortClient = new ProductService.ProductServicePortClient();
                var getProductBySellerCodeRequest = new ProductService.GetProductBySellerCodeRequest();
                getProductBySellerCodeRequest.auth = new ProductService.Authentication
                {
                    appKey = this._configuration.AppKey,
                    appSecret = this._configuration.AppSecret
                };
                var sellerCode = stockRequest.PriceStock.StockCode.Split(new string[] { "|" }, StringSplitOptions.None);

                getProductBySellerCodeRequest.sellerCode = sellerCode[0];

                var getProductBySellerCodeResult = _productServicePortClient.GetProductBySellerCodeAsync(getProductBySellerCodeRequest).Result;
                if (getProductBySellerCodeResult.GetProductBySellerCodeResponse.result.status == "success" && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product != null && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.stockItems.stockItem.Length > 0)
                {

                    var data = $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
   <soapenv:Header/>
   <soapenv:Body>
      <sch:UpdateStockByStockIdRequest>
         <auth>
        <appKey>{this._configuration.AppKey}</appKey>
            <appSecret>{this._configuration.AppSecret}</appSecret>
         </auth>
         <stockItems>
            <!--1 or more repetitions:-->
            <stockItem>
               <id>{getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.stockItems.stockItem[0].id}</id>
               <quantity>{stockRequest.PriceStock.Quantity}</quantity>
               <version></version>
               <delist></delist>
            </stockItem>
         </stockItems>
      </sch:UpdateStockByStockIdRequest>
   </soapenv:Body>
</soapenv:Envelope>";
                    var stockResponseStr = Serializer.Post(data, "https://api.n11.com/ws/ProductStockService.wsdl");
                    if (stockResponseStr.Success)
                    {

                        var stockResponse = Serializer.Deserialize<UpdateStockByStockIdResponse>(stockResponseStr.Data);
                        response.Success = stockResponse.Result.Status == "success";
                        return;
                    }
                }
                else
                {
                    response.Success = getProductBySellerCodeResult.GetProductBySellerCodeResponse.result.errorMessage == "ürün bulunamadı";


                }
                _productServicePortClient.InnerChannel.Close();
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var _productServicePortClient = new ProductService.ProductServicePortClient();
                var getProductBySellerCodeRequest = new ProductService.GetProductBySellerCodeRequest();
                getProductBySellerCodeRequest.auth = new ProductService.Authentication
                {
                    appKey = this._configuration.AppKey,
                    appSecret = this._configuration.AppSecret
                };
                var sellerCode = stockRequest.PriceStock.StockCode.Split(new string[] { "|" }, StringSplitOptions.None);
                getProductBySellerCodeRequest.sellerCode = sellerCode[0];

                var getProductBySellerCodeResult = _productServicePortClient.GetProductBySellerCodeAsync(getProductBySellerCodeRequest).Result;
                if (getProductBySellerCodeResult.GetProductBySellerCodeResponse.result.status == "success" && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product != null && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.stockItems.stockItem.Length > 0)
                {


                    if (stockRequest.PriceStock.SalePrice.HasValue)
                    {

                        var priceRequest = new ProductService.UpdateProductBasicRequest();

                        priceRequest.auth = new ProductService.Authentication
                        {
                            appKey = this._configuration.AppKey,
                            appSecret = this._configuration.AppSecret
                        };
                        var discount = stockRequest.PriceStock.ListPrice - stockRequest.PriceStock.SalePrice;

                        priceRequest.productId = getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.id;
                        //priceRequest.productSellerCode = stockRequest.PriceStock.StockCode;
                        priceRequest.price = discount.Value > 0 ? stockRequest.PriceStock.ListPrice.Value : stockRequest.PriceStock.SalePrice.Value;
                        priceRequest.productDiscount = new ProductService.SellerProductDiscount();
                        priceRequest.productDiscount.discountType = "1";
                        priceRequest.productDiscount.discountValue = discount.Value < 0 ? 0 : Convert.ToDouble(discount.Value);




                        var priceResponse = _productServicePortClient.UpdateProductBasicAsync(priceRequest).Result;


                        response.Success = priceResponse.UpdateProductBasicResponse.result.status == "success";

                    }
                    else
                    {
                        response.Success = getProductBySellerCodeResult.GetProductBySellerCodeResponse.result.errorMessage == "ürün bulunamadı";


                    }
                }
                _productServicePortClient.InnerChannel.Close();

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                var _productServicePortClient = new ProductService.ProductServicePortClient();
                var currentPage = 0;
                var request = new ProductService.GetProductListRequest()
                {
                    auth = new ProductService.Authentication
                    {
                        appKey = this._configuration.AppKey,
                        appSecret = this._configuration.AppSecret
                    },
                    pagingData = new ProductService.RequestPagingData
                    {
                        pageSize = 100,
                        currentPage = currentPage
                    }

                };

                var productItems = new List<ProductItem>();
                var loop = true;
                while (loop)
                {

                    var apiResponse = _productServicePortClient.GetProductListAsync(request).Result;

                    foreach (var pp in apiResponse.GetProductListResponse.products)
                    {
                        var failed = true;
                        while (failed)
                        {


                            try
                            {
                                if (_productServicePortClient.InnerChannel.State != System.ServiceModel.CommunicationState.Opened)
                                    _productServicePortClient.InnerChannel.Open();

                                var aa = _productServicePortClient.GetProductBySellerCodeAsync(new ProductService.GetProductBySellerCodeRequest
                                {
                                    auth = new ProductService.Authentication
                                    {
                                        appKey = this._configuration.AppKey,
                                        appSecret = this._configuration.AppSecret
                                    },
                                    sellerCode = pp.productSellerCode
                                }).Result;

                                var product = new ProductItem
                                {
                                    SellerCode = pp.productSellerCode,
                                    CategoryId = aa.GetProductBySellerCodeResponse.product.category.id.ToString(),
                                    StockItems = aa.GetProductBySellerCodeResponse.product.stockItems.stockItem.Select(x => new Application.Common.Parameters.Product.StockItem
                                    {
                                        Active = true,
                                        Barcode = x.mpn,
                                        StockCode = x.sellerStockCode,
                                        BrandName = "",
                                        Description = "",
                                        ListPrice = x.displayPrice,
                                        SalePrice = x.displayPrice,
                                        Attributes = aa.GetProductBySellerCodeResponse.product.attributes.Select(y => new CategoryAttribute
                                        {
                                            AttributeCode = y.id.ToString(),
                                            AttributeName = y.name,
                                            AttributeValueName = y.value.ToString()
                                        }).ToList()
                                    }).ToList()
                                };

                                productItems.Add(product);
                                failed = false;
                            }
                            catch (Exception ex)
                            {
                                Thread.Sleep(new TimeSpan(0, 0, 1));
                                Console.WriteLine($"{productItems.Count} product count 1 second");
                                failed = true;
                            }
                        }
                    }


                    if (apiResponse.GetProductListResponse.pagingData.pageCount == apiResponse.GetProductListResponse.pagingData.currentPage + 1)
                    {
                        loop = false;
                        break;
                    }
                    currentPage++;
                    request.pagingData.currentPage = currentPage;
                }

                response.Data = productItems;

                response.Success = true;
            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "";
                });

        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>((response) =>
            {
                response.Data = new();

                var productServicePortClient = new ProductService.ProductServicePortClient();
                var pageCount = 0;
                var currentPage = 0;

                do
                {
                    var request = new ProductService.GetProductListRequest()
                    {
                        auth = new ProductService.Authentication
                        {
                            appKey = this._configuration.AppKey,
                            appSecret = this._configuration.AppSecret
                        },
                        pagingData = new ProductService.RequestPagingData
                        {
                            pageSize = 100,
                            currentPage = currentPage
                        }
                    };
                    var productListResponse = productServicePortClient.GetProductListAsync(request).Result;
                    pageCount = productListResponse.GetProductListResponse.pagingData.pageCount.Value;

                    response.Data.AddRange(productListResponse
                        .GetProductListResponse
                        .products
                        .Where(x => x.stockItems.stockItem != null)
                        .Select(p => new Application.Common.DataTransferObjects.MarketplaceProductStatus
                        {
                            MarketplaceId = this.MarketplaceId,
                            MarketplaceProductId = p.id.ToString(),
                            Opened = true,
                            OnSale = p.saleStatus == "2",
                            Locked = !(p.approvalStatus == "1" || p.approvalStatus == "2" || p.approvalStatus == "5"),
                            UUId = p.id.ToString(),
                            StockCode = p.stockItems.stockItem[0].sellerStockCode,
                            Url = $"https://www.n11.com/arama?q={p.id}",
                            ListUnitPrice = p.stockItems.stockItem[0].optionPrice,
                            UnitPrice = p.stockItems.stockItem[0].displayPrice,
                            Stock = int.Parse(p.stockItems.stockItem[0].quantity)
                        }));

                    currentPage++;
                } while (pageCount > currentPage);

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

                var request = new N11CategoryService.GetTopLevelCategoriesRequest
                {
                    auth = new N11CategoryService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                };

                var categories = n11CategoryService.GetTopLevelCategoriesAsync(request).Result;

                string[] anyCategories = new string[] { "1001155" };

                //string[] anyCategories = new string[] { "1002605", "1001935", "1001929", "1001873", "1002816", "1002717", "1001770", "1002663", "1000145", "1003335", "1002690", "1002742", "1002816", "1191215", "1062100", "1002809", "1002579", "1000472", "1002583", "1218200", "1002639", "1002553", "1002543", "1002507", "1002493", "1002515", "1191215", "1002520" };

                var data = new List<CategoryResponse>();
                int i = 1;
                foreach (var caList in categories.GetTopLevelCategoriesResponse.categoryList)
                {

                    if (!anyCategories.Contains(caList.id.ToString())) continue;

                    Console.WriteLine($"{i} {caList.name} {caList.id}");
                    i++;

                    data.AddRange(GetSubCategory(configurations, caList, null, new CategoryResponse()).Data);
                }


                response.Data = data;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                var n11OrderService = new OrderServicePortClient();

                var appKey = this._configuration.AppKey;
                var appSecret = this._configuration.AppSecret;

                var request = new DetailedOrderListRequest
                {
                    auth = new N11OrderService.Authentication { appKey = appKey, appSecret = appSecret },
                    searchData = new OrderDataListRequest { status = "New" }
                };

                var detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0)
                {
                    foreach (var order in detailedOrderListResponse.DetailedOrderListResponse.orderList)
                    {
                        var orderDetailResult = OrderDetail(n11OrderService, order.id);

                        if (orderDetailResult.Success)
                        {
                            var orderDetail = orderDetailResult.Data.OrderDetailResponse;
                            var orderDetails = new List<OrderDetail>();
                            var payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>();

                            switch (orderDetail.orderDetail.shippingAddress.city)
                            {
                                case "K.Maraş":
                                    orderDetail.orderDetail.shippingAddress.city = "Kahramanmaraş";
                                    break;
                                default:
                                    break;
                            }

                            foreach (var item in orderDetail.orderDetail.itemList)
                            {
                                orderDetails.Add(new OrderDetail
                                {
                                    UId = item.id.ToString(),
                                    Product = new Product
                                    {
                                        Barcode = item.shipmentInfo.shipmentCode,
                                        Name = item.productName,
                                        StockCode = item.productSellerCode,
                                    },
                                    UnitPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    ListPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    TaxRate = 0.18,
                                    Quantity = Convert.ToInt32(item.quantity)
                                });
                            }

                            payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment
                            {
                                Amount = order.totalAmount,
                                Date = DateTime.Now,
                                CurrencyId = "TRY",
                                PaymentTypeId = "OO"
                                //CurrencyId= order.
                            });

                            string shipmentId = "";
                            switch (orderDetail.orderDetail.itemList[0].shipmentInfo.shipmentCompany.id)
                            {
                                case 345:
                                    shipmentId = ShipmentCompanies.ArasKargo;
                                    break;
                                case 344:
                                    shipmentId = ShipmentCompanies.YurticiKargo;
                                    break;
                                case 381:
                                    shipmentId = ShipmentCompanies.PttKargo;
                                    break;
                                case 342:
                                    shipmentId = ShipmentCompanies.MngKargo;
                                    break;
                                case 341:
                                    shipmentId = ShipmentCompanies.SuratKargo;
                                    break;
                                case 343:
                                    shipmentId = ShipmentCompanies.Ups;
                                    break;

                            }

                            var customerName = orderDetail.orderDetail.billingAddress.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var invoiceFirstName = "";
                            var invoiceLastName = "";
                            if (customerName.Length > 1)
                            {
                                invoiceFirstName = customerName[0];
                                invoiceLastName = customerName[1];
                            }

                            customerName = orderDetail.orderDetail.buyer.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var firstName = "";
                            var lastName = "";
                            if (customerName.Length > 1)
                            {
                                firstName = customerName[0];
                                lastName = customerName[1];
                            }

                            response.Data.Orders.Add(new Order
                            {
                                UId = $"{Application.Common.Constants.Marketplaces.N11}_{order.orderNumber}",
                                Customer = new Customer
                                {
                                    FirstName = orderDetail.orderDetail.buyer.fullName,
                                    LastName = orderDetail.orderDetail.buyer.fullName,
                                    Mail = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.billingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.buyer.taxId,

                                },
                                OrderShipment = new OrderShipment
                                {
                                    TrackingCode = orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber != null ? orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber : orderDetail.orderDetail.itemList[0].shipmentInfo.campaignNumber,
                                    TrackingUrl = "",
                                    ShipmentTypeId = "3",
                                    ShipmentCompanyId = shipmentId
                                },
                                OrderCode = order.orderNumber.ToString(),
                                OrderTypeId = "OS",
                                Discount = 0,
                                ListTotalAmount = order.totalAmount,
                                OrderNote = "",
                                OrderSourceId = OrderSources.Pazaryeri,
                                MarketplaceId = Application.Common.Constants.Marketplaces.N11,
                                OrderDetails = orderDetails,
                                OrderDate = DateTime.Now,
                                TotalAmount = order.totalAmount,
                                OrderDeliveryAddress = new OrderDeliveryAddress
                                {
                                    Address = orderDetail.orderDetail.shippingAddress.address,
                                    City = orderDetail.orderDetail.shippingAddress.city,
                                    Neighborhood = $"{orderDetail.orderDetail.shippingAddress.neighborhood.Replace("Mh.", "")}",
                                    Country = "",
                                    District = orderDetail.orderDetail.shippingAddress.district,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    Email = orderDetail.orderDetail.buyer.email,
                                    FirstName = firstName,
                                    LastName = lastName
                                },
                                OrderInvoiceAddress = new OrderInvoiceAddress
                                {
                                    FirstName = invoiceFirstName,
                                    LastName = invoiceLastName,
                                    Address = orderDetail.orderDetail.billingAddress.address,
                                    City = orderDetail.orderDetail.billingAddress.city,
                                    Country = orderDetail.orderDetail.billingAddress.postalCode,
                                    District = orderDetail.orderDetail.billingAddress.district,
                                    Neighborhood = $"{orderDetail.orderDetail.billingAddress.neighborhood.Replace("Mh.", "")}",
                                    Email = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.billingAddress.taxId,
                                    TaxOffice = orderDetail.orderDetail.billingAddress.taxHouse
                                },
                                Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                                {
                                    new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment   {
                                        Amount = order.totalAmount,
                                        Date = DateTime.Now,
                                        CurrencyId = "TRY",
                                        PaymentTypeId = "OO"
                                    }}
                            });
                        }
                    }
                    response.Success = true;
                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {

                var n11OrderService = new OrderServicePortClient();
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                var request = new DetailedOrderListRequest
                {
                    auth = new N11OrderService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                    searchData = new OrderDataListRequest { status = "New" }
                };
                var detailedOrderListResponse = new DetailedOrderListResponse1();
                detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                try
                {
                    if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0)
                    {
                        foreach (var data in detailedOrderListResponse.DetailedOrderListResponse.orderList)
                        {
                            var orderItemList = new OrderItemDataRequest[data.orderItemList.Length];

                            for (int i = 0; i < data.orderItemList.Length; i++)
                            {
                                try
                                {
                                    Console.WriteLine();
                                    orderItemList[i] = new OrderItemDataRequest { id = data.orderItemList[i].id };

                                    Console.WriteLine($"N11 {data.orderItemList[i].id} sipariş numarası eklendi");
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                            }

                            if (data.orderItemList != null && data.orderItemList.Length > 0)
                            {
                                var orderItemAccept = new OrderItemAcceptRequest
                                {

                                    auth = new N11OrderService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                                    numberOfPackages = "1",
                                    orderItemList = orderItemList
                                };
                                var orderItemAcceptResponse = n11OrderService.OrderItemAcceptAsync(orderItemAccept).Result;
                                if (orderItemAcceptResponse.OrderItemAcceptResponse.result.status == "success")
                                {
                                    Console.BackgroundColor = ConsoleColor.Green;
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine($"N11 {data.orderItemList[0].id} sipariş numarası başarılı bir şekilde onaylandı statüsünüe geçti");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                else
                                {
                                    Console.BackgroundColor = ConsoleColor.Red;
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine($"[E] [{data.orderItemList[0].id}] siparişi onayladı statüsüne geçiş yapamadı.");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;
                                }

                            }

                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }
                Console.WriteLine("N11 30 Second Waiting");
                Thread.Sleep(new TimeSpan(0, 0, 30));


                request = new DetailedOrderListRequest
                {
                    auth = new N11OrderService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                    searchData = new OrderDataListRequest
                    {
                        status = "Approved"
                        // orderNumber = "205258698354"//orderRequest.MarketPlaceOrderCode
                        //status = "Shipped"
                    }
                };

                detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0)
                {


                    foreach (var order in detailedOrderListResponse.DetailedOrderListResponse.orderList)
                    {


                        var orderDetailResult = OrderDetail(n11OrderService, order.id);

                        if (orderDetailResult.Success)
                        {
                            var orderDetail = orderDetailResult.Data.OrderDetailResponse;
                            var orderDetails = new List<OrderDetail>();
                            var payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>();


                            switch (orderDetail.orderDetail.shippingAddress.city)
                            {
                                case "K.Maraş":
                                    orderDetail.orderDetail.shippingAddress.city = "Kahramanmaraş";
                                    break;
                                default:
                                    break;
                            }

                            foreach (var item in orderDetail.orderDetail.itemList)
                            {
                                orderDetails.Add(new OrderDetail
                                {
                                    Product = new Product
                                    {
                                        Barcode = item.shipmentInfo.shipmentCode,
                                        Name = item.productName,
                                        StockCode = item.sellerStockCode,
                                    },
                                    UnitPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    ListPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    TaxRate = 0.18,
                                    Quantity = Convert.ToInt32(item.quantity)
                                });
                            }

                            payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment
                            {
                                Amount = order.totalAmount,
                                Date = DateTime.Now,
                                CurrencyId = "TRY",
                                PaymentTypeId = "OO"
                                //CurrencyId= order.
                            });




                            string shipmentId = "";
                            switch (orderDetail.orderDetail.itemList[0].shipmentInfo.shipmentCompany.id)
                            {
                                case 345:
                                    shipmentId = ShipmentCompanies.ArasKargo;
                                    break;
                                case 344:
                                    shipmentId = ShipmentCompanies.YurticiKargo;
                                    break;
                                case 381:
                                    shipmentId = ShipmentCompanies.PttKargo;
                                    break;
                                case 342:
                                    shipmentId = ShipmentCompanies.MngKargo;
                                    break;
                                case 341:
                                    shipmentId = ShipmentCompanies.SuratKargo;
                                    break;
                                case 343:
                                    shipmentId = ShipmentCompanies.Ups;
                                    break;

                            }


                            var customerName = orderDetail.orderDetail.billingAddress.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var invoiceFirstName = "";
                            var invoiceLastName = "";
                            if (customerName.Length > 1)
                            {
                                invoiceFirstName = customerName[0];
                                invoiceLastName = customerName[1];
                            }

                            customerName = orderDetail.orderDetail.buyer.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var firstName = "";
                            var lastName = "";
                            if (customerName.Length > 1)
                            {
                                firstName = customerName[0];
                                lastName = customerName[1];
                            }

                            if (string.IsNullOrEmpty(firstName))
                            {
                                firstName = invoiceFirstName;
                                lastName = invoiceLastName;
                            }

                            response.Data.Orders.Add(new Order
                            {
                                Customer = new Customer
                                {
                                    FirstName = firstName,
                                    LastName = lastName,
                                    Mail = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.billingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.buyer.taxId,

                                },
                                OrderShipment = new OrderShipment
                                {
                                    TrackingCode = orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber != null ? orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber : orderDetail.orderDetail.itemList[0].shipmentInfo.campaignNumber,
                                    TrackingUrl = "",
                                    ShipmentTypeId = "3",
                                    ShipmentCompanyId = shipmentId
                                },
                                OrderCode = order.orderNumber.ToString(),
                                OrderTypeId = "OS",
                                Discount = 0,
                                ListTotalAmount = order.totalAmount,
                                OrderNote = "",
                                OrderSourceId = OrderSources.Pazaryeri,
                                MarketplaceId = Application.Common.Constants.Marketplaces.N11,
                                OrderDetails = orderDetails,
                                OrderDate = DateTime.Now,
                                TotalAmount = order.totalAmount,
                                OrderDeliveryAddress = new OrderDeliveryAddress
                                {
                                    Address = orderDetail.orderDetail.shippingAddress.address,
                                    City = orderDetail.orderDetail.shippingAddress.city,
                                    Neighborhood = $"{orderDetail.orderDetail.shippingAddress.neighborhood.Replace("Mh.", "")}",
                                    Country = "",
                                    District = orderDetail.orderDetail.shippingAddress.district,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    Email = orderDetail.orderDetail.buyer.email,
                                    FirstName = firstName,
                                    LastName = lastName
                                },
                                OrderInvoiceAddress = new OrderInvoiceAddress
                                {
                                    FirstName = invoiceFirstName,
                                    LastName = invoiceLastName,
                                    Address = orderDetail.orderDetail.billingAddress.address,
                                    City = orderDetail.orderDetail.billingAddress.city,
                                    Country = orderDetail.orderDetail.billingAddress.postalCode,
                                    District = orderDetail.orderDetail.billingAddress.district,
                                    Neighborhood = $"{orderDetail.orderDetail.billingAddress.neighborhood.Replace("Mh.", "")}",
                                    Email = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.billingAddress.taxId,
                                    TaxOffice = orderDetail.orderDetail.billingAddress.taxHouse
                                },
                                Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                                {
                                    new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment   {
                                        Amount = order.totalAmount,
                                        Date = DateTime.Now,
                                        CurrencyId = "TRY",
                                        PaymentTypeId = "OO"
                                    }}
                            });
                        }
                    }
                    response.Success = true;
                }


            }, (response, exception) =>
            {

                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                // Sipariş maddesi durum bilgisi:
                //1:İşlem Bekliyor
                //2:Ödendi
                //3:Geçersiz
                //4:İptal Edilmiş
                //5:Kabul Edilmiş
                //6:Kargoda
                //7:Teslim Edilmiş
                //8:Reddedilmiş
                //9:İade Edildi
                //10:Tamamlandı
                //11: İade İptal Değişim Talep Edildi
                //12: İade İptal Değişim Tamamlandı
                //13:Kargoda İade
                //14:Kargo Yapılması Gecikmiş,
                //15:Kabul Edilmiş Ama Zamanında Kargoya Verilmemiş
                //16:Teslim Edilmiş İade, 
                //17:Tamamlandıktan Sonra İade

                var n11OrderService = new OrderServicePortClient();
                string[] cancelOrderStatus = new string[3] { "3", "4", "8" };
                var request = new DetailedOrderListRequest
                {
                    auth = new N11OrderService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                    searchData = new OrderDataListRequest
                    {
                        //status = "Approved"
                        orderNumber = orderRequest.MarketplaceOrderCode
                        //status = "Shipped"
                    }
                };
                var detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0 && (detailedOrderListResponse.DetailedOrderListResponse.orderList[0].orderItemList.Any(x => cancelOrderStatus.Contains(x.status)) || detailedOrderListResponse.DetailedOrderListResponse.orderList[0].status == "3"))
                {
                    response.Data.IsCancel = true;
                    response.Message = $"Siparişiniz iptal statüsündedir. N11 panelinden siparişinizi kontrol ediniz.";

                }
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>(response =>
            {
                response.Success = true;
                response.Data = new() { Orders = new() };

                List<string> orderNumbers = new();

                N11CancelService.ClaimCancelServicePortClient claimCancelServicePortClient = new();

                int page = 0;
                int pagesCount = 0;

                do
                {
                    var request = new N11CancelService.ClaimCancelListRequest
                    {
                        auth = new N11CancelService.Authentication
                        {
                            appKey = this._configuration.AppKey,
                            appSecret = this._configuration.AppSecret
                        },
                        pagingData = new N11CancelService.CancelRequestPagingData
                        {
                            currentPage = page
                        },
                        searchData = new N11CancelService.ClaimCancelSearch
                        {
                            status = "COMPLETED"
                        }
                    };
                    var cancelList = claimCancelServicePortClient.ClaimCancelListAsync(request).Result;
                    if (cancelList.ClaimCancelListResponse.result.status == "success")
                    {
                        orderNumbers.AddRange(cancelList
                            .ClaimCancelListResponse
                            .claimCancelList
                            .Select(x => x.orderNumber));

                        pagesCount = cancelList.ClaimCancelListResponse.pagingData.pageCount.Value;
                    }

                    page++;
                } while (pagesCount > page);

                claimCancelServicePortClient.InnerChannel.Close();
                if (orderNumbers.Count > 0)
                    response.Data.Orders.AddRange(orderNumbers.Select(oi => new Order
                    {
                        OrderCode = oi.ToString(),
                        MarketplaceId = this.MarketplaceId
                    }));
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(N11Configuration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.AppKey = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "AppKey").Value;
            configuration.AppSecret = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "AppSecret").Value;
            configuration.ShipmentTemplate = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ShipmentTemplate").Value;
        }
        #endregion

        #region Helper Methods
        private DataResponse<List<CategoryResponse>> GetSubCategory(Dictionary<string, string> configurations, SubCategory subCategory, string parentCategoryCode, CategoryResponse setCategories)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var _categories = new List<CategoryResponse>();
                var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

                try
                {
                    var categoryResponse = new CategoryResponse()
                    {
                        Code = subCategory.id.ToString(),
                        Name = subCategory.name,
                        ParentCategoryCode = parentCategoryCode

                    };

                    var getCategoryAttributesIdRequest = new N11CategoryService.GetCategoryAttributesIdRequest
                    {
                        auth = new N11CategoryService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                        categoryId = subCategory.id
                    };

                    var categoryAttributes = n11CategoryService.GetCategoryAttributesIdAsync(getCategoryAttributesIdRequest).Result;

                    if (categoryAttributes.GetCategoryAttributesIdResponse.result.status != "success")
                    {
                        response.Data = _categories;
                        response.Success = true;
                        return;
                    }



                    foreach (var catLoop in categoryAttributes.GetCategoryAttributesIdResponse.categoryProductAttributeList)
                    {
                        if (catLoop.name == "Marka") continue;

                        var categoryAttibute = new CategoryAttributeResponse
                        {
                            Code = catLoop.id.ToString(),
                            Name = catLoop.name,
                            AllowCustom = false,
                            CategoryCode = subCategory.id.ToString(),
                            Mandatory = catLoop.mandatory,
                            MultiValue = catLoop.multipleSelect,
                            Variantable = catLoop.variant
                        };


                        categoryAttibute.CategoryAttributesValues = setCategories.CategoryAttributes.SelectMany(x => x.CategoryAttributesValues).Where(x => x.VarinatCode == catLoop.id.ToString()).Select(x => new CategoryAttributeValueResponse
                        {
                            Code = x.Code,
                            Name = x.Name,
                            VarinatCode = x.VarinatCode

                        }).ToList();


                        if (categoryAttibute.CategoryAttributesValues.Count == 0)
                        {
                            var page = 0;
                            var isLoop = true;
                            while (isLoop)
                            {

                                var getCategoryAttributeValueRequest = new N11CategoryService.GetCategoryAttributeValueRequest
                                {
                                    auth = new N11CategoryService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                                    categoryId = subCategory.id,
                                    categoryProductAttributeId = catLoop.id,
                                    pagingData = new N11CategoryService.RequestPagingData { currentPage = page, pageSize = 100 },
                                };

                                var getCategoryAttributeValues = n11CategoryService.GetCategoryAttributeValueAsync(getCategoryAttributeValueRequest).Result;

                                if (getCategoryAttributeValues.GetCategoryAttributeValueResponse.result.status != "success" || getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList == null ||
                                getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList.Length == 0)
                                {
                                    isLoop = false;
                                    continue;
                                }

                                foreach (var atvLoop in getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList)
                                {
                                    categoryAttibute.CategoryAttributesValues.Add(new CategoryAttributeValueResponse
                                    {
                                        VarinatCode = catLoop.id.ToString(),
                                        Code = atvLoop.id == 0 ? $"{atvLoop.name.ReplaceChar().Replace(" ", "-")}" : atvLoop.id.ToString(),
                                        Name = atvLoop.name
                                    });
                                }
                                page++;
                                isLoop = getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList.Length == 100 && getCategoryAttributeValues.GetCategoryAttributeValueResponse.pagingData.currentPage != getCategoryAttributeValues.GetCategoryAttributeValueResponse.pagingData.pageCount;

                            }
                        }

                        categoryResponse.CategoryAttributes.Add(categoryAttibute);

                    }

                    _categories.Add(categoryResponse);


                    var subCategoriesRequest = new N11CategoryService.GetSubCategoriesRequest
                    {
                        auth = new N11CategoryService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                        categoryId = subCategory.id
                    };

                    var subCategories = n11CategoryService.GetSubCategoriesAsync(subCategoriesRequest).Result;

                    if (subCategories.GetSubCategoriesResponse.result.status != "success")
                    {
                        response.Data = _categories;
                        response.Success = true;
                        return;
                    }


                    foreach (var cLoop in subCategories.GetSubCategoriesResponse.category)
                    {
                        if (cLoop.subCategoryList == null) continue;

                        foreach (var subLoop in cLoop.subCategoryList)
                        {

                            _categories.AddRange(GetSubCategory(configurations, subLoop, subCategory.id.ToString(), categoryResponse).Data);

                        }

                    }
                }
                catch (Exception ex)
                {

                    n11CategoryService.InnerChannel.Close();
                    Thread.Sleep(new TimeSpan(0, 0, 5));
                    _categories.AddRange(GetSubCategory(configurations, subCategory, parentCategoryCode, setCategories).Data);

                }

                response.Data = _categories.GroupBy(x => x.Code).Select(x => x.Last()).ToList();
                response.Success = true;
            });
        }

        private DataResponse<OrderDetailResponse1> OrderDetail(OrderServicePortClient n11OrderService, long id)
        {

            return ExceptionHandler.ResultHandle<DataResponse<OrderDetailResponse1>>((response) =>
            {

                var orderDetailResponse = n11OrderService.OrderDetailAsync(new OrderDetailRequest
                {
                    orderRequest = new OrderDataRequest
                    {
                        id = id
                    },
                    auth = new N11OrderService.Authentication { appKey = this._configuration.AppKey, appSecret = this._configuration.AppSecret },
                });

                response.Data = orderDetailResponse.Result;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri datalarını çekerken hata aldı";
            });

        }

        private DataResponse<N11ProductService.GetProductBySellerCodeResponse1> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<N11ProductService.GetProductBySellerCodeResponse1>>((response) =>
            {
                var _productServicePortClient = new ProductService.ProductServicePortClient();
                var request = new ProductService.GetProductBySellerCodeRequest();
                request.auth = new ProductService.Authentication
                {
                    appKey = this._configuration.AppKey,
                    appSecret = this._configuration.AppSecret
                };
                request.sellerCode = productFilter.Stockcode;

                response.Data = _productServicePortClient.GetProductBySellerCodeAsync(request).Result;

                _productServicePortClient.InnerChannel.Close();
                response.Success = true;
            });
        }


        #endregion
    }
}