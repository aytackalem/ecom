﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Marketplaces.Omni
{
    public class Omni : MarketplaceBase<OmniConfiguration>
    {
        #region Constants
        public readonly string _apiUrl = "http://localhost:11201";
        #endregion

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Omni(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.OMNI;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                var _productResponse = this._httpHelper.Post<ProductRequest, Response>(productRequest, $"{_apiUrl}/Product/Create", null, null, null, null);

                response.Success = _productResponse.Success;
                response.Message = _productResponse.Message;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                var _stockResponse = this._httpHelper.Post<StockRequest, Response>(stockRequest, $"{_apiUrl}/Product/UpdateStock", null, null, null, null);
                response.Success = _stockResponse.Success;
                response.Message = _stockResponse.Message;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return new DataResponse<List<MarketplaceProductStatus>>();
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                //var _orderResponse = this._httpHelper.Post<OrderRequest, DataResponse<OrderResponse>>(request, $"{_apiUrl}/Order/Get", null, null);

                //Parallel.ForEach(_orderResponse.Data.Orders, theOrder =>
                //{
                //    theOrder.UId = $"OM_{theOrder.OrderCode}";
                //});

                //response.Data = _orderResponse.Data;
                //response.Success = _orderResponse.Success;
                //response.Message = _orderResponse.Message;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {

                var _orderResponse = this._httpHelper.Post<OrderRequest, DataResponse<OrderResponse>>(orderRequest, $"{_apiUrl}/Order/Get", null, null, null, null);

                response.Data = _orderResponse.Data;
                response.Success = _orderResponse.Success;
                response.Message = _orderResponse.Message;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {

                var _checkPreparingResponse = this._httpHelper.Post<OrderRequest, DataResponse<CheckPreparingResponse>>(orderRequest, $"{_apiUrl}/Order/CheckPreparing", null, null, null, null);

                response.Data = _checkPreparingResponse.Data;
                response.Success = _checkPreparingResponse.Success;
                response.Message = _checkPreparingResponse.Message;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Omni ile bağlantı kurulamıyor.";
            });

        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
#if !DEBUG


                switch (orderPackaging.Status)
                {
                    case Application.Common.Parameters.Enums.MarketplaceStatus.Picking:
                        orderPackaging.Configurations.Add("StatusId", "0");
                        break;
                    case Application.Common.Parameters.Enums.MarketplaceStatus.Packaging:
                        orderPackaging.Configurations.Add("StatusId", "12");
                        break;
                }

                var _orderSourceUpdate = this._httpHelper.Post<OrderPickingRequest, Response>(orderPackaging, $"{_apiUrl}/Order/OrderSourceUpdate", null, null, null, null);
                response.Success = _orderSourceUpdate.Success;
                response.Message = _orderSourceUpdate.Message; 
#endif


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(OmniConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
        }
        #endregion

        #region Helper Methods
        private DataResponse<ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductFilterResponse>>((response) =>
            {


                response.Success = true;
            });
        }
        #endregion
    }
}