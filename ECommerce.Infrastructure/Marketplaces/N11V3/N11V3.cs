﻿using ECommerce.Accounting.Mikro.UnitOfWork.Base;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order;
using ECommerce.Infrastructure.Marketplaces.N11V3.Common.ProductStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Order = ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order;

namespace ECommerce.Infrastructure.Marketplaces.N11V3
{
    public class N11V3 : MarketplaceBase<N11V3Configuration>
    {

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public N11V3(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.N11V2;
        #endregion





        protected override void BindConfigurationConcreate(N11V3Configuration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.ApiKey = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "AppKey").Value;
            configuration.Appsecret = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "AppSecret").Value;
            configuration.ShipmentTemplate = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ShipmentTemplate").Value;
        }

        protected override DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return new DataResponse<List<KeyValue<string, string>>>();
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            return new DataResponse<CheckPreparingResponse>();
        }

        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return new DataResponse<List<CategoryResponse>>();
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                var size = 200;
                var page = 0;
                var totalPages = 0;

                do
                {
                    //status = Created
                    var url = $"https://api.n11.com/rest/delivery/v1/shipmentPackages?&page={page}&status=Created";


                    var customHeaders = new Dictionary<string, string> {
                    { "appkey", this._configuration.ApiKey },
                    { "appsecret", this._configuration.Appsecret }

                    };

                    var N11OrderPages = _httpHelper.Get<N11V3OrderPage>(url, "", "", customHeaders);
                    totalPages = N11OrderPages.TotalPages;

                    /*
                     * N11 servisinden cevap gelip gelmediği kontrol edilir.
                     */
                    if (N11OrderPages != null && N11OrderPages.Content != null && N11OrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(N11OrderPages.Content));
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "";

                        break;
                    }

                    page++;

                } while (page < totalPages);

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return new DataResponse<List<ProductItem>>();
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceProductStatus>>>((response) =>
            {
                response.Data = new List<MarketplaceProductStatus>();

                var size = 50;
                var page = 0;
                var totalPages = 0;

                do
                {

                    var url = $"https://api.n11.com/ms/product-query?page={page}&size={size}&productStatus=Active";


                    var customHeaders = new Dictionary<string, string> {
                    { "appkey", this._configuration.ApiKey },
                    { "appsecret", this._configuration.Appsecret }

                    };

                    var N11V3ProductStatusPages = _httpHelper.Get<N11V3ProductStatusPage>(url, "", "", customHeaders);


                    if (N11V3ProductStatusPages != null && N11V3ProductStatusPages.Content != null)
                    {
                        if(N11V3ProductStatusPages.Content.Any(x=>x.ProductMainId == "SD/MYAZ23PL0120"))
                        {

                        }

                        totalPages = N11V3ProductStatusPages.TotalPages;
                        foreach (var theContent in N11V3ProductStatusPages.Content)
                        {
                            var marketplaceProductStatus = new MarketplaceProductStatus
                            {
                                MarketplaceId = this.MarketplaceId,
                                MarketplaceProductId = theContent.ProductMainId,
                                Barcode = null,
                                StockCode = theContent.StockCode,
                                Opened = true,
                                OnSale = theContent.Status == "Active",
                                Locked = theContent.Status == "Sale_Closed",
                                Url = theContent.GroupId > 0 ? $"https://www.n11.com/urun/lacivert-yirtmacli-mini-dokuma-etek-lacivert-{theContent.GroupId}" : "",
                                ListUnitPrice = (decimal)theContent.ListPrice,
                                UnitPrice = (decimal)theContent.SalePrice,
                                Stock = theContent.Quantity
                            };
                            response.Data.Add(marketplaceProductStatus);
                        }
                    }
                    else
                    {

                    }


                    page++;

                } while (page < totalPages);

                response.Success = true;
            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "";
                });
        }

        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse>();
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return new DataResponse<OrderTypeUpdateResponse>();
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return new Response();
        }

        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }

        private List<Order> MapOrder(List<N11V3Order> providerOrders)
        {
            List<Order> orders = new();

            foreach (var pOrder in providerOrders)
            {
                if (pOrder.CargoTrackingNumber == "0") continue;
                try
                {
                    var shipmentCompanyId = ShipmentCompanies.ArasKargo;

                    switch (pOrder.ShipmentCompanyId)
                    {
                        case 345:
                            shipmentCompanyId = ShipmentCompanies.ArasKargo;
                            break;
                        case 344:
                            shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                            break;
                        case 381:
                            shipmentCompanyId = ShipmentCompanies.PttKargo;
                            break;
                        case 342:
                            shipmentCompanyId = ShipmentCompanies.MngKargo;
                            break;
                        case 341:
                            shipmentCompanyId = ShipmentCompanies.SuratKargo;
                            break;
                        case 343:
                            shipmentCompanyId = ShipmentCompanies.Ups;
                            break;
                    }


                    var shippingAddressFullName = pOrder.ShippingAddress.FullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var billingAddressFullName = pOrder.BillingAddress.FullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var customerfullName = pOrder.CustomerfullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (customerfullName.Length == 0) customerfullName = billingAddressFullName;

                    switch (pOrder.ShippingAddress.City)
                    {
                        case "K.Maraş":
                            pOrder.ShippingAddress.City = "Kahramanmaraş";
                            break;
                        default:
                            break;
                    }

                    switch (pOrder.BillingAddress.City)
                    {
                        case "K.Maraş":
                            pOrder.BillingAddress.City = "Kahramanmaraş";
                            break;
                        default:
                            break;
                    }


                    var commercial = pOrder.BillingAddress.InvoiceType == 2;
                    var totalAmount = pOrder.Lines.Sum(x => x.SellerInvoiceAmount/x.Quantity);
                    Order order = new()
                    {
                        UId = $"{Application.Common.Constants.Marketplaces.N11V2}_{pOrder.OrderNumber}-{pOrder.CargoTrackingNumber}",
                        OrderSourceId = OrderSources.Pazaryeri,
                        MarketplaceId = Application.Common.Constants.Marketplaces.N11V2,
                        OrderCode = pOrder.OrderNumber,
                        OrderTypeId = "OS",
                        Micro = false,
                        Commercial = commercial,
                        TotalAmount = totalAmount,
                        ListTotalAmount = totalAmount,
                        Discount = 0,
                        OrderDate = DateTime.Now,
                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                        {
                            PackageNumber = pOrder.Id.ToString(),
                            TrackingCode = pOrder.CargoTrackingNumber,
                            ShipmentTypeId = "PND",
                            TrackingUrl = pOrder.CargoTrackingLink,
                            ShipmentCompanyId = shipmentCompanyId
                        },
                        Customer = new Customer()
                        {
                            FirstName = customerfullName[0],
                            LastName = customerfullName.Length > 1 ? customerfullName[1] : "",
                            Mail = pOrder.CustomerEmail,
                            TaxNumber = pOrder.BillingAddress.TaxId,
                            Phone = pOrder.BillingAddress.Gsm //TODO set phone number
                        },
                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                        {
                            FirstName = shippingAddressFullName[0],
                            LastName = shippingAddressFullName.Length > 1 ? shippingAddressFullName[1] : "",
                            Country = "Türkiye",
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.ShippingAddress.Gsm, //TODO set phone number
                            City = pOrder.ShippingAddress.City,
                            District = pOrder.ShippingAddress.District,
                            Neighborhood = pOrder.ShippingAddress.Neighborhood,
                            Address = $"{pOrder.ShippingAddress.Address}"
                        },
                        OrderInvoiceAddress = new OrderInvoiceAddress()
                        {
                            FirstName = billingAddressFullName[0],
                            LastName = billingAddressFullName.Length > 1 ? billingAddressFullName[1] : "",
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.BillingAddress.Gsm, //TODO set phone number
                            City = pOrder.BillingAddress.City,
                            District = pOrder.BillingAddress.District,
                            Address = pOrder.BillingAddress.Address,
                            Neighborhood = pOrder.BillingAddress.Neighborhood,
                            TaxNumber = commercial ? pOrder.BillingAddress.TaxId : "",
                            TaxOffice = commercial ? pOrder.BillingAddress.TaxHouse : ""

                        },
                        Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                    };
                    order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                    {
                        CurrencyId = "TRY",
                        PaymentTypeId = "OO",
                        Amount = totalAmount,
                        Date = DateTime.Now
                    }); 

                    order.OrderPicking = new OrderPicking
                    {
                        MarketPlaceOrderId = pOrder.Id.ToString(),
                        OrderDetails = new List<OrderPickingDetail>()
                    };

                    
                    #region Order Details
                    order.OrderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();
                    foreach (var orderDetail in pOrder.Lines)
                    {
                        order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                        {
                            UId = orderDetail.OrderLineId.ToString(),
                            TaxRate = 0.20,
                            UnitPrice = orderDetail.SellerInvoiceAmount / orderDetail.Quantity,
                            ListPrice = orderDetail.SellerInvoiceAmount / orderDetail.Quantity,
                            UnitDiscount = 0,//orderDetail.MallDiscount
                            Product = new Product()
                            {
                                Barcode = orderDetail.ProductId.ToString(),
                                Name = orderDetail.ProductName,
                                StockCode = orderDetail.StockCode
                            },
                            Quantity = orderDetail.Quantity
                        });
                        order.OrderPicking.OrderDetails.Add(new OrderPickingDetail
                        {
                            Id = orderDetail.OrderLineId.ToString(),
                            Quantity = orderDetail.Quantity,
                        });
                    }
                    #endregion

                    orders.Add(order);
                }
                catch (Exception ex)
                {

                }
            }

            return orders;
        }

    }
}
