﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.N11V3
{
    public class N11V3Configuration
    {
        #region Properties
        public string ApiKey { get; internal set; }

        public string Appsecret { get; internal set; }

        public string ShipmentTemplate { get; internal set; }
        #endregion
    }
}
