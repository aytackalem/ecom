﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Base
{
    public abstract class N11V3Page
    {

        #region Properties
        public int TotalElements { get; set; }

        public int TotalPages { get; set; }

        public int Page { get; set; }

        public int Size { get; set; }
        #endregion
    }
}
