﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order
{
    public class N11V3OrderLine
    {

        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string StockCode { get; set; }
        //public List<N11V3VariantAttribute> VariantAttributes { get; set; }
        //public List<object> CustomTextOptionValues { get; set; }
        //public decimal Price { get; set; }
        public decimal DueAmount { get; set; }
        //public int InstallmentChargeWithVAT { get; set; }
        //public int SellerCouponDiscount { get; set; }
        //public double SellerDiscount { get; set; }
        //public decimal MallDiscount { get; set; }
        public decimal SellerInvoiceAmount { get; set; }
        //public double TotalMallDiscountPrice { get; set; }
        public int OrderLineId { get; set; }
        //public string OrderItemLineItemStatusName { get; set; }
        //public double TotalSellerDiscountPrice { get; set; }
    }
}
