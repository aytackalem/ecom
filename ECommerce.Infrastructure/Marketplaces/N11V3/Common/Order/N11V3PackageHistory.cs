﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order
{
    public class N11V3PackageHistory
    {
        public string Status { get; set; }
    }
}
