﻿using ECommerce.Infrastructure.Marketplaces.N11V3.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order
{
    public class N11V3OrderPage : N11V3Page
    {
        #region NavigationProperties
        public List<N11V3Order> Content { get; set; }
        #endregion
    }
}
