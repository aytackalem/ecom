﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order
{
    public class N11V3Order
    {
        #region Properties
        public string OrderNumber { get; set; }

        public string Id { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerfullName { get; set; }

        //public int CustomerId { get; set; }

        //public string TaxId { get; set; }

        //public string TaxOffice { get; set; }

        public string TcIdentityNumber { get; set; }

        //public object CargoSenderNumber { get; set; }

        public string CargoTrackingNumber { get; set; }

        public string CargoTrackingLink { get; set; }

        public int ShipmentCompanyId { get; set; }

        //public string CargoProviderName { get; set; }

        //public int ShipmentMethod { get; set; }

        //public double InstallmentChargeWithVATprice { get; set; }

        //public decimal TotalAmount { get; set; }

        //public decimal TotalDiscountAmount { get; set; }

        //public string ShipmentPackageStatus { get; set; }

        //public int SellerId { get; set; }

        //public List<N11V3PackageHistory> PackageHistories { get; set; }

        public List<N11V3OrderLine> Lines { get; set; }

        public N11V3BillingAdress BillingAddress { get; set; }

        public N11V3ShippingAddress ShippingAddress { get; set; }
        #endregion
    }
}
