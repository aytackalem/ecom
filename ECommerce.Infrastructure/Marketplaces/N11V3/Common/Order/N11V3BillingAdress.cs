﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.Order
{
    public class N11V3BillingAdress
    {
        #region Properties
        public string Address { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Neighborhood { get; set; }
        public string FullName { get; set; }
        public string Gsm { get; set; }
        public string TcId { get; set; }
        public string PostalCode { get; set; }
        public string TaxId { get; set; }
        public string TaxHouse { get; set; }
        public int InvoiceType { get; set; }
        #endregion

    }
}
