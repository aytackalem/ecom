﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.ProductStatus
{
    public class N11V3ProductStatusPage
    {
        public List<N11V3ProductStatusContent> Content { get; set; }
        public N11V3ProductStatusPageable Pageable { get; set; }
        public bool Last { get; set; }
        public int TotalPages { get; set; }
        public int TotalElements { get; set; }
        public bool First { get; set; }
        public int Number { get; set; }
        public int NumberOfElements { get; set; }
        public int Size { get; set; }
        public bool Empty { get; set; }
    }
}
