﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.N11V3.Common.ProductStatus
{
    public class N11V3ProductStatusContent
    {
    
        public string SellerNickname { get; set; }
        public string StockCode { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string ProductMainId { get; set; }
        public string Status { get; set; }     
        public string Barcode { get; set; }
        public int GroupId { get; set; }
        public string CurrencyType { get; set; }
        public double SalePrice { get; set; }
        public double ListPrice { get; set; }
        public int Quantity { get; set; }
    }
}
