﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Accounting.Mikro.DataTransferObjects;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Category;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Packages;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Product;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductBatchResult;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductFilter;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductStatus;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Stock;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.StockBatchResult;
using ECommerce.Infrastructure.Marketplaces.N11.Common.Stock;
using ECommerce.Infrastructure.Marketplaces.Pazarama.Common.Stok;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada
{
    public class Hepsiburada : TokenMarketplaceBase<HepsiburadaConfiguration>, IMutualBarcodable, ICargoChangable, IPackable, IBatchable
    {
        #region Constants
        private const string _tokenUrl = "https://mpop.hepsiburada.com/api/authenticate";

        private const string _baseUrl = "https://oms-external.hepsiburada.com";

        private const string _listingUrl = "https://listing-external.hepsiburada.com";
        #endregion

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Hepsiburada(IHttpHelper httpHelper, ICacheHandler cacheHandler, IServiceProvider serviceProvider) : base(serviceProvider, cacheHandler, 29)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Hepsiburada;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var products = new List<Common.Product.HepsiburadaProduct>();


                var request = string.Empty;

                foreach (var siLoop in productRequest.ProductItem.StockItems)
                {


                    //var filter = Filter(new ProductFilterRequest
                    //{
                    //    Stockcode = siLoop.StockCode,
                    //    Configurations = productRequest.Configurations
                    //});

                    //filter.Success true demek ürün sistemde var demek
                    //Ürün yükleme yapılır Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz 
                    //if (!filter.Success || !siLoop.Active)
                    //{
                    //    continue;
                    //}




                    var images = string.Empty;

                    for (int i = 0; i < (siLoop.Images.Count >= 5 ? 5 : siLoop.Images.Count); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                images += $"\"Image1\": \"{siLoop.Images[i].Url}\",";
                                break;
                            case 1:
                                images += $"\"Image2\": \"{siLoop.Images[i].Url}\",";
                                break;
                            case 2:
                                images += $"\"Image3\": \"{siLoop.Images[i].Url}\",";
                                break;
                            case 3:
                                images += $"\"Image4\": \"{siLoop.Images[i].Url}\",";
                                break;
                            case 4:
                                images += $"\"Image5\": \"{siLoop.Images[i].Url}\",";
                                break;
                        }
                    }

                    var attirubutes = string.Empty;
                    foreach (var attLoop in siLoop.Attributes)
                    {
                        var attributeName = attLoop.Varinatable ? attLoop.AttributeCode : attLoop.AttributeCode.Replace(" ", "_");

                        attirubutes += $"\"{attributeName}\": \"{attLoop.AttributeValueName}\",";
                    }
                    attirubutes = attirubutes.TrimEnd(',');

                    request += $@"
{{
    ""categoryId"": {int.Parse(productRequest.ProductItem.CategoryId)},
    ""merchant"": ""{_configuration.MerchantId}"",
    ""attributes"": {{
      ""merchantSku"": ""{siLoop.StockCode}"",
      ""VaryantGroupID"": ""{productRequest.ProductItem.SellerCode}"",
      ""Barcode"": ""{siLoop.Barcode}"",
      ""UrunAdi"": ""{siLoop.Title}"",
      ""UrunAciklamasi"": ""{siLoop.Description}"",
      ""Marka"": ""{productRequest.ProductItem.BrandName}"",
      ""GarantiSuresi"": 0,
      ""kg"": ""{productRequest.ProductItem.DimensionalWeight.ToString().Replace(",", ".")}"",
      ""tax_vat_rate"": ""{(siLoop.VatRate * 100).ToString("0")}"",
      ""price"": ""{siLoop.SalePrice.ToString().Replace(".", ",")}"",
      ""stock"": ""{siLoop.Quantity.ToString()}"",
      {images}
      {attirubutes}
    }}
            }},";
                }

                request = request.TrimEnd(',');
                request =
                $@"[{request}]";

                var hepsiburadaProductResponse = this._httpHelper.PostForm<Common.Product.HepsiburadaProductResponse>(
                    request,
                    $"https://mpop.hepsiburada.com/product/api/products/import",
                    "Bearer",
                    this.GetAccessToken().Data,
                    "application/json");

                if (hepsiburadaProductResponse.success)
                {
                    response.Success = true;
                    response.Data = hepsiburadaProductResponse.data.trackingId;
                }
            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "";
                });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest updatePriceAndStock)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var merchantid = _configuration.MerchantId;



                var stockRequest = new List<HepsiburadaStockRequest>
                {
                    new HepsiburadaStockRequest
                    {
                        HepsiburadaSku = null, //Şimdilik hepsiburada kodu olduğu için bunu açtık.Normalde MerchantSku açık olması gerekiyor.
                        MerchantSku = updatePriceAndStock.PriceStock.StockCode, //updatePriceAndStock.PriceStock.StockCode, 
                        AvailableStock = updatePriceAndStock.PriceStock.Quantity
                    }
                };

                var stockResponse = this._httpHelper.Post<List<HepsiburadaStockRequest>, HepsiburadaStockResponse>(
                   stockRequest,
                   $"{_listingUrl}/listings/merchantid/{merchantid}/stock-uploads",
                   "Basic",
                   this._configuration.Authorization, null, null);

                if (stockResponse != null)
                {
                    response.Data = stockResponse.id;
                    response.Success = true;
                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest updatePriceAndStock)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var merchantid = _configuration.MerchantId;

                var culture = new CultureInfo("en-US");


                var priceRequest = new List<HepsiburadaPriceRequest>
                    {
                        new HepsiburadaPriceRequest()
                        {
                            HepsiburadaSku = null,
                            MerchantSku = updatePriceAndStock.PriceStock.StockCode,
                            Price = Convert.ToDouble(updatePriceAndStock.PriceStock.SalePrice.ToString().Replace(",", "."), culture)
                        }
                    };
                var priceResponse = this._httpHelper.Post<List<HepsiburadaPriceRequest>, HepsiburadaPriceResponse>(
                   priceRequest,
                   $"https://listing-external.hepsiburada.com/listings/merchantid/{merchantid}/price-uploads",
                   "Basic",
                   this._configuration.Authorization, null, null);

                if (priceResponse != null)
                {
                    response.Data = priceResponse.id;
                    response.Success = true;
                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new List<ProductItem>();


                var limit = 1000;
                var page = 0;
                var pagesCount = 0;
                var merchantId = _configuration.MerchantId;
                var authorization = this._configuration.Authorization;
                var url = $"https://mpop.hepsiburada.com/product/api/products/all-products-of-merchant/{merchantId}";
                var hepsiburadaProducts = new List<Common.ProductFilter.HepsiburadaProduct>();
                while (true)
                {
                    var requestUrl = $"{url}?page={page}&size=100";
                    var filterResponse = this._httpHelper.Get<Common.ProductFilter.HepsiburadaProductResponse>(requestUrl, "Basic", authorization);


                    pagesCount = filterResponse.totalPages;

                    hepsiburadaProducts.AddRange(filterResponse.data);

                    page++;

                    if (filterResponse.last) break;

                }

                var items = hepsiburadaProducts.GroupBy(x => x.variantGroupId).Select(x => new { SellerCode = x.Key, StockItems = x.ToList() });

                foreach (var product in items)
                {
                    var productItem = new ProductItem
                    {
                        SellerCode = product.SellerCode,
                        CategoryId = product.StockItems.First().categoryId.ToString(),
                        CategoryName = product.StockItems.First().categoryName.ToString(),
                        BrandName = product.StockItems.First().brand,
                        Name = product.StockItems.First().productName,
                        StockItems = new List<Application.Common.Parameters.Product.StockItem>()
                    };

                    foreach (var siLoop in product.StockItems)
                    {
                        var stokItem = new Application.Common.Parameters.Product.StockItem
                        {

                            Active = true,
                            Barcode = siLoop.barcode,
                            BrandName = siLoop.brand,
                            Description = siLoop.description,
                            ListPrice = !string.IsNullOrEmpty(siLoop.price) ? Convert.ToDecimal(siLoop.price) : 0,
                            SalePrice = !string.IsNullOrEmpty(siLoop.price) ? Convert.ToDecimal(siLoop.price) : 0,
                            Quantity = 0,
                            Images = siLoop.images.Select(im => new Image
                            {
                                FileName = im,
                                Url = im,
                                FilePath = im
                            }).ToList(),
                            MarketplaceProductId = siLoop.hbSku,
                            Subtitle = string.Empty,
                            StockCode = siLoop.merchantSku,
                            VatRate = Convert.ToDecimal(siLoop.tax),
                            Title = siLoop.productName,
                            UUId = siLoop.hbSku,
                            Attributes = new List<CategoryAttribute>()
                        };

                        stokItem.Attributes.AddRange(siLoop.productAttributes.Select(x => new CategoryAttribute
                        {
                            AllowCustom = false,
                            AttributeName = x.name,
                            AttributeValueName = x.value,
                            Mandatory = x.mandatory,
                        }).ToList());

                        stokItem.Attributes.AddRange(siLoop.variantTypeAttributes.Select(x => new CategoryAttribute
                        {
                            AllowCustom = false,
                            AttributeName = x.name,
                            AttributeValueName = x.value,
                            Mandatory = x.mandatory,
                        }).ToList());

                        productItem.StockItems.Add(stokItem);
                    }
                    response.Data.Add(productItem);
                }
                response.Success = true;
            });
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {

                if (string.IsNullOrEmpty(this._configuration.MerchantId))
                {
                    response.Success = false;
                    return;
                }

                response.Success = true;
                response.Data = new();

                var limit = 1000;
                var page = 0;
                var pagesCount = 0;
                var merchantId = _configuration.MerchantId;
                var authorization = this._configuration.Authorization;
                var url = $"{_listingUrl}/listings/merchantid/{merchantId}";

                do
                {
                    var requestUrl = $"{url}?offset={page * limit}&limit={limit}";
                    var filterResponse = this._httpHelper.Get<HepsiburadaProductFilterResponse>(requestUrl, "Basic", authorization);

                    if (filterResponse == null)
                    {
                        response.Success = false;
                        return;
                    }

                    if (filterResponse != null && pagesCount == 0)
                        pagesCount = (int)Math.Ceiling(filterResponse.totalCount / (decimal)limit);

                    foreach (var l in filterResponse.listings)
                    {
                        var marketplaceProductStatus = new Application.Common.DataTransferObjects.MarketplaceProductStatus
                        {
                            MarketplaceId = this.MarketplaceId,
                            MarketplaceProductId = l.hepsiburadaSku,
                            StockCode = l.merchantSku,

                            Opened = true,
                            OnSale = l.isSalable,// || !(l.isLocked || l.isFrozen || l.isSuspended),
                            Locked = l.isFrozen,
                            DirtyPriceDisabled = l.priceIncreaseDisabled || l.priceDecreaseDisabled,
                            DirtyStockDisabled = l.stockDecreaseDisabled,
                            Url = $"https://www.hepsiburada.com/a-p-{l.hepsiburadaSku}",
                            UnitPrice = (decimal)l.price,
                            Stock = l.availableStock
                        };

                        response.Data.Add(marketplaceProductStatus);
                    }

                    page++;
                } while (pagesCount > page);

            });
        }

        public DataResponse<bool> StockCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var merchantid = _configuration.MerchantId;
                var url = $"{_listingUrl}/listings/merchantid/{merchantid}/stock-uploads/id/{batchId}";
                var result = this._httpHelper.Get<StockBatchResult>(url, "Basic", authorization);
                response.Success = true;
                response.Data = result.status == "Done" && result.errors == null;
                if (!response.Data)
                {
                    response.Message = String.Join("", result.errors.SelectMany(e => e.errors.Select(e => e)));
                }
            });
        }

        public DataResponse<bool> PriceCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var merchantid = _configuration.MerchantId;
                var url = $"{_listingUrl}/listings/merchantid/{merchantid}/price-uploads/id/{batchId}";
                var result = this._httpHelper.Get<StockBatchResult>(url, "Basic", authorization);
                response.Success = true;
                response.Data = result.status == "Done" && result.errors == null;
                if (!response.Data)
                {
                    response.Message = String.Join("", result.errors.SelectMany(e => e.errors.Select(e => e)));
                }
            });
        }

        public DataResponse<bool> CreateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var merchantid = _configuration.MerchantId;
                var url = $"https://mpop.hepsiburada.com/product/api/products/status/{batchId}";
                var result = this._httpHelper.Get<ProductStatusReponse>(url, "Basic", authorization);
                response.Success = result.success;
                response.Data = result.data[0].productStatus == "Satışa Hazır";
                if (result.data[0].validationResults?.Count > 0)
                {
                    response.Message = String.Join("", result.data[0].validationResults.SelectMany(e => $"{e.attributeName}: {e.message}"));
                }
            });
        }

        public DataResponse<bool> UpdateCheck(string batchId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var merchantid = _configuration.MerchantId;
                var url = $"https://mpop.hepsiburada.com/product/api/products/status/{batchId}";
                var result = this._httpHelper.Get<ProductStatusReponse>(url, "Basic", authorization);
                response.Success = result.success;
                response.Data = result.data[0].productStatus == "Satışa Hazır";
                if (result.data[0].validationResults?.Count > 0)
                {
                    response.Message = String.Join("", result.data[0].validationResults.SelectMany(e => $"{e.attributeName}: {e.message}"));
                }
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {


                var page = 0;

                var categories = new List<HepsiburadaCategory>();

                HepsiburadaCategoryPage categoryPage;

                do
                {
                    categoryPage = this._httpHelper.Get<HepsiburadaCategoryPage>(
                        $"https://mpop.hepsiburada.com/product/api/categories/get-all-categories?leaf=true&status=ACTIVE&available=true&page={page}&size=7500&version=1",
                        "Bearer",
                        this.GetAccessToken().Data);

                    categories.AddRange(categoryPage.Data);

                    page++;
                } while (!categoryPage.Last);

                categories = categories.Where(x => x.Leaf && x.Available && x.Status == "ACTIVE").ToList();


                List<string> newCategories = new List<string>();
                newCategories.Add("erkek");
                newCategories.Add("kadın");
                newCategories.Add("elbise");
                newCategories.Add("kazak");
                newCategories.Add("yelek");
                newCategories.Add("abiye");
                newCategories.Add("aksesuar");
                newCategories.Add("ayakkabı");
                newCategories.Add("kozmetik");
                newCategories.Add("krem");
                newCategories.Add("sneakers");
                newCategories.Add("giyim / ayakkabı");
                newCategories.Add("saatler");
                newCategories.Add("saat/gözlük/aksesuar");
                newCategories.Add("takı");
                newCategories.Add("bijuteri");
                newCategories.Add("güneş gözlüğü");
                newCategories.Add("ev dekorasyon");
                newCategories.Add("mobilya");
                newCategories.Add("oturma odası");
                newCategories.Add("ofis mobilyaları");
                newCategories.Add("salon");
                newCategories.Add("ofis");
                newCategories.Add("mutfak");
                newCategories.Add("banyo");



                var _categories = categories.Where(x => x.Paths.Any(y => newCategories.Contains(y.ToLower()))).ToList();

                foreach (var cLoop in _categories)
                {

                    Console.WriteLine($"{_categories.IndexOf(cLoop)} {cLoop.DisplayName}");
                    var categoryAttributePage = this._httpHelper.Get<HepsiburadaCategoryAttributePage>(
                             $"https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attributes",
                             "Bearer",
                             this.GetAccessToken().Data);
                    if (categoryAttributePage.Success)
                    {
                        foreach (var atLoop in categoryAttributePage.Data.Attributes)
                        {
                            if (atLoop.Type == "string") continue;

                            var attributeValuePage = 0;
                            HepsiburadaCategoryAttributeValuePage categoryAttributeValuePage;

                            #region Get Attribute Value
                            do
                            {
                                categoryAttributeValuePage = this._httpHelper.Get<HepsiburadaCategoryAttributeValuePage>(
                                   $"https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attribute/{atLoop.Id}/values?page={attributeValuePage}&size=1000&version=4",
                                   "Bearer",
                                   this.GetAccessToken().Data);
                                if (categoryAttributeValuePage.Success)
                                {
                                    atLoop.CategoryAttributeValues.AddRange(categoryAttributeValuePage.Data);
                                }
                                else
                                {
                                    Thread.Sleep(new TimeSpan(0, 1, 0));
                                    categoryAttributeValuePage = this._httpHelper.Get<HepsiburadaCategoryAttributeValuePage>(
                                   $" https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attribute/{atLoop.Id}/values?page={attributeValuePage}&size=1000&version=4",
                                   "Bearer",
                                   this.GetAccessToken().Data);
                                    if (categoryAttributeValuePage.Success)
                                    {
                                        atLoop.CategoryAttributeValues.AddRange(categoryAttributeValuePage.Data);
                                    }

                                }

                                attributeValuePage++;
                            } while (!categoryAttributeValuePage.Last && categoryAttributeValuePage.Success);

                            #endregion

                        }

                        foreach (var atLoop in categoryAttributePage.Data.VariantAttributes)
                        {
                            if (atLoop.Type == "string") continue;

                            var attributeValuePage = 0;
                            HepsiburadaCategoryAttributeValuePage categoryAttributeValuePage;

                            #region Get Attribute Value
                            do
                            {
                                categoryAttributeValuePage = this._httpHelper.Get<HepsiburadaCategoryAttributeValuePage>(
                                   $"https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attribute/{atLoop.Id}/values?page={attributeValuePage}&size=1000&version=4",
                                   "Bearer",
                                   this.GetAccessToken().Data);
                                if (categoryAttributeValuePage.Success)
                                {
                                    atLoop.CategoryAttributeValues.AddRange(categoryAttributeValuePage.Data);
                                }
                                else
                                {
                                    Thread.Sleep(new TimeSpan(0, 1, 0));
                                    categoryAttributeValuePage = this._httpHelper.Get<HepsiburadaCategoryAttributeValuePage>(
                                   $"https://mpop.hepsiburada.com/product/api/categories/{cLoop.CategoryId}/attribute/{atLoop.Id}/values?page={attributeValuePage}&size=1000&version=4",
                                   "Bearer",
                                   this.GetAccessToken().Data);
                                    if (categoryAttributeValuePage.Success)
                                    {
                                        atLoop.CategoryAttributeValues.AddRange(categoryAttributeValuePage.Data);
                                    }

                                }

                                attributeValuePage++;
                            } while (!categoryAttributeValuePage.Last && categoryAttributeValuePage.Success);

                            #endregion

                            cLoop.Attributes.Add(new HepsiburadaAttribute
                            {
                                CategoryAttributeValues = atLoop.CategoryAttributeValues,
                                Id = atLoop.Id,
                                Varinatable = true,
                                Mandatory = atLoop.Mandatory,
                                MultiValue = atLoop.MultiValue,
                                Name = atLoop.Name,
                                Type = atLoop.Type
                            });
                        }

                        cLoop.Attributes.AddRange(categoryAttributePage.Data.Attributes);
                    }
                }

                var data = _categories.Select(x => new CategoryResponse
                {
                    Code = x.CategoryId.ToString(),
                    Name = x.DisplayName,
                    ParentCategoryCode = x.ParentCategoryId.ToString(),
                    CategoryAttributes = x.Attributes.Select(ca => new CategoryAttributeResponse
                    {
                        CategoryCode = x.CategoryId.ToString(),
                        Code = ca.Id.ToString(),
                        Name = ca.Name,
                        Variantable = ca.Varinatable,
                        Mandatory = ca.Mandatory,
                        AllowCustom = ca.Type == "string",
                        MultiValue = ca.MultiValue,
                        CategoryAttributesValues = ca.CategoryAttributeValues.Select(av => new CategoryAttributeValueResponse
                        {
                            VarinatCode = ca.Id.ToString(),
                            Code = av.Value.Replace(" ", "-"),
                            Name = av.Value
                        }).ToList()
                    }).ToList()
                }).ToList();


                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new() { Orders = new List<Order>() };

                List<Item> hepsiburadaItems = new();

                var page = 0;
                var pageCount = 0;
                var offset = 100;
                var token = this._configuration.Authorization;
                var merchantId = _configuration.MerchantId;
                var preUrl = $"{_baseUrl}/orders/merchantid/{merchantId}?&limit={offset}";

                do
                {
                    var providerData = _httpHelper.Get<OrderPage>($"{preUrl}&offset={offset * page}", "Basic", token);

                    if (providerData == null || providerData.Items == null) break;

                    pageCount = providerData.PageCount;

                    hepsiburadaItems.AddRange(providerData.Items);

                    page++;
                } while (page < pageCount);

                if (hepsiburadaItems.Count > 0)
                {
                    var orderNumbers = hepsiburadaItems.Select(i => i.OrderNumber).Distinct();

                    Parallel.ForEach(orderNumbers, theOrderNumber =>
                    {
                        var shoppingCartItems = hepsiburadaItems.Where(i => i.OrderNumber == theOrderNumber);

                        var order = MapOrder(shoppingCartItems, null, null, null);
                        order.UId = $"{Application.Common.Constants.Marketplaces.Hepsiburada}_{theOrderNumber}";
                        order.MutualBarcode = this._configuration.MutualBarcode;

                        orderResponse.Orders.Add(order);
                    });
                }

                response.Data = orderResponse;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderTypeUpdateResponse>>((response) =>
            {
                response.Data = new();

                var merchantId = _configuration.MerchantId;
                var token = this._configuration.Authorization;
                var packageUrl = $"{_baseUrl}/packages/merchantid/{merchantId}";
                var preCargoUrl = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber";

                if (orderTypeUpdateRequest.MarketplaceStatus == Application.Common.Parameters.Enums.MarketplaceStatus.Picking)
                {
                    var packageRequest = new PackagePage
                    {
                        LineItemRequests = orderTypeUpdateRequest.Order.OrderDetails.Select(od => new LineItemRequest
                        {
                            Id = od.UId,
                            Quantity = od.Quantity.ToString()
                        }).ToList()
                    };

                    var package = _httpHelper.Post<PackagePage, Package>(packageRequest, packageUrl, "Basic", token, null, null);
                    var cargo = _httpHelper.Get<CargoPage[]>($"{preCargoUrl}/{package.PackageNumber}", "Basic", token)[0];

                    response.Data.PackageNumber = package.PackageNumber;
                    response.Data.TrackingCode = cargo.Barcode;
                    response.Success = true;
                }
            }, (response, exception) =>
            {
                //TO DO: Unpack

                response.Success = false;
                response.Message = "Hepsiburada ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<String> GetMutualBarcode(MutualBarcodeRequest mutualBarcodeRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<String>>((response) =>
            {
                var authorization = this._configuration.Authorization;
                var merchantId = _configuration.MerchantId;
                var packageNumber = mutualBarcodeRequest.PackageNumber;

                string url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}/labels?format=png";

                var mutualBarcode = _httpHelper.Get<MutualBarcode>(url, "Basic", authorization);

                if (mutualBarcode.Code == "100")
                {
                    response.Success = true;
                    response.Data = mutualBarcode.Data[0];
                }
                else
                    response.Success = false;
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            /* Sisteme aktarılmamış fakat HB de gönderime hazır statüsünde kalmış siparişlere "Paket Boz" işlemi uygulamak için hazırlanan kod. */

            //try
            //{
            //    var beginDate = "2022-12-07 00:00";
            //    var endDate = "2022-12-07 23:59";
            //    var offset = 0;
            //    var url = $"https://oms-external.hepsiburada.com/packages/merchantid/{this._configuration.MerchantId}?beginDate={beginDate}&endDate={endDate}&offset={0}";

            //    while (true)
            //    {
            //        Console.WriteLine(offset);
            //        var ms = this._httpHelper.Get<List<Package>>(String.Format(url, offset), "Basic", this._configuration.Authorization);
            //        offset += 10;

            //        if (ms == null || ms.Count == 0)
            //            break;

            //        using (var conn = new SqlConnection("Server=172.25.28.10;Database=Ecommerce;UId=sa;Pwd=!X135790zxcYz!;"))
            //        {
            //            conn.Open();

            //            var text = @"Select	Count(0)
            //From	Orders As O
            //Join	OrderShipments As OS
            //On		O.Id = OS.OrderId
            //		And OS.TrackingCode = @TrackingCode
            //Where	O.CompanyId = 3
            //		And O.MarketplaceId = 'HB'";

            //            foreach (var mLoop in ms)
            //            {
            //                using (var cmd = new SqlCommand(text, conn))
            //                {
            //                    cmd.Parameters.AddWithValue("@TrackingCode", mLoop.Barcode);
            //                    if (Convert.ToInt32(cmd.ExecuteScalar()) == 0)
            //                    {
            //                        url = $"https://oms-external.hepsiburada.com/packages/merchantid/{request.Configurations["MerchantId"]}/packagenumber/{mLoop.PackageNumber}/unpack";
            //                        this._httpHelper.Post(url, "Basic", this._configuration.Authorization);

            //                        Console.WriteLine($"{ms.IndexOf(mLoop)} Unpack");
            //                    }
            //                    else
            //                    {
            //                        Console.WriteLine($"{ms.IndexOf(mLoop)} C");
            //                    }
            //                }

            //            }

            //            conn.Close();
            //        }

            //    }
            //}
            //catch
            //{

            //}

            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new() { Orders = new List<Order>() };
                //helpy_dev
                //Bvc_12345*

                List<Item> hepsiburadaItems = new();

                var merchantId = _configuration.MerchantId;
                var page = 0;
                var pageCount = 0;
                var offset = 100;
                var authorization = this._configuration.Authorization;

                if (string.IsNullOrEmpty(request.MarketplaceOrderCode))
                {
                    var url = $"https://oms-external.hepsiburada.com/orders/merchantid/{merchantId}?&limit={offset}";
                    do
                    {
                        var providerData = _httpHelper.Get<OrderPage>(url + $"&offset={offset * page}", "Basic", authorization);

                        if (providerData == null || providerData.Items == null) break;

                        hepsiburadaItems.AddRange(providerData.Items);

                        pageCount = providerData.PageCount;
                        page++;
                    } while (page < pageCount);
                }
                else
                {
                    string url = $"https://oms-external.hepsiburada.com/orders/merchantid/{merchantId}/ordernumber/{request.MarketplaceOrderCode}";
                    var providerData = _httpHelper.Get<OrderPage>(url, "Basic", this._configuration.Authorization);
                    if (providerData != null || providerData.Items != null)
                        hepsiburadaItems.AddRange(providerData.Items);
                }

                XConsole.Info($"HB {hepsiburadaItems.Count} items found.");

                if (hepsiburadaItems.Count > 0)
                {
                    /* Geçici olarak şuan her turda sadece ilk 100 sipariş alınacak. */
                    var orderNumbers = hepsiburadaItems.Select(i => i.OrderNumber).Distinct().Take(100).ToList();

                    XConsole.Info($"HB {orderNumbers.Count} orders found.");

                    Parallel.ForEach(orderNumbers, theOrderNumber =>
                    {

                        //foreach (var theOrderNumber in orderNumbers)
                        //{


                        XConsole.Info($"{orderNumbers.IndexOf(theOrderNumber)} starting.");

                        var shoppingCartItems = hepsiburadaItems.Where(i => i.OrderNumber == theOrderNumber);
                        if (shoppingCartItems == null) return;

                        var package = Package(shoppingCartItems, request);
                        if (package == null) return;

                        var cargo = CargoPage(package.PackageNumber, request);
                        if (cargo == null) return;

                        if (cargo.CargoCompany.ToLower() == "hepsijet")
                        {
                            MutualBarcode mutualBarcode = null;
                            for (int i = 0; i < 3; i++)
                            {
                                try
                                {
                                    mutualBarcode = MutualBarcode(package.PackageNumber, request);

                                    if (mutualBarcode != null &&
                                        mutualBarcode.Data != null &&
                                        mutualBarcode.Data.Count > 0 &&
                                        mutualBarcode.Code == "100")
                                    {
                                        orderResponse
                                            .Orders
                                            .Add(MapOrder(shoppingCartItems, cargo.Barcode, mutualBarcode.Data[0], package.PackageNumber));

                                        break;
                                    }

                                    /* 
                                     * Hepsiburada yoğunluk anında yanıt hatalı dönebiliyor 1 saniye bekleyip tekrar istek atılması
                                     * için bu kod yazılmıştır.
                                     */
                                    Thread.Sleep(1000);
                                }
                                catch { }
                            }

                            if (mutualBarcode == null ||
                                mutualBarcode.Data == null ||
                                mutualBarcode.Data.Count == 0 ||
                                mutualBarcode.Code != "100")
                                orderResponse.Orders.Add(MapOrder(shoppingCartItems, cargo.Barcode, null, package.PackageNumber));
                        }
                        else
                            orderResponse.Orders.Add(MapOrder(shoppingCartItems, cargo.Barcode, null, package.PackageNumber));

                        //}
                    });
                }

                response.Data = orderResponse;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var merchantId = _configuration.MerchantId;
                string url = $"https://oms-external.hepsiburada.com/orders/merchantid/{merchantId}/ordernumber/{request.MarketplaceOrderCode}";
                var providerData = _httpHelper.Get<OrderPage>(url, "Basic", this._configuration.Authorization).Items.Any(x => x.Status.Contains("Cancelled"));
                if (providerData)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Hepsiburda siparişiniz iptal statüsündedir. Siparişin devam edilemesi hepsiburada panelinden kontrol edilmesi gerekiyor.";
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Hepsiburada ile bağlantı kurulamıyor.";
            });

        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Hepsiburada ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                var merchantId = _configuration.MerchantId;

                int page = 0;
                int pageCount = 0;
                int offset = 50;

                do
                {
                    string url = $"https://oms-external.hepsiburada.com/packages/merchantid/{merchantId}/delivered?offset={page * offset}&limit={offset}";
                    var data = _httpHelper.Get<OrderPage>(url, "Basic", this._configuration.Authorization);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order { OrderCode = i.OrderNumber, MarketplaceId = Application.Common.Constants.Marketplaces.Hepsiburada }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                var merchantId = _configuration.MerchantId;
                var page = 0;
                var pageCount = 0;
                var offset = 50;

                do
                {
                    var url = $"{_baseUrl}/orders/merchantid/{merchantId}/cancelled?offset={page * offset}&limit={offset}";
                    var data = _httpHelper.Get<OrderPage>(url, "Basic", this._configuration.Authorization);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order
                    {
                        OrderCode = i.OrderNumber,
                        MarketplaceId = "HB"
                    }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                //do
                //{
                //    var url = $"{_baseUrl}/packages/merchantid/{merchantId}/status/unpacked?offset={page * offset}&limit={offset}";
                //    var data = _httpHelper.Get<UnpackedResponse>(url, "Basic", this._configuration.Authorization);

                //    if (data == null || data.items == null)
                //        break;

                //    orderResponse.Orders.AddRange(data.items.Select(i => new Order
                //    {
                //        OrderShipment = new OrderShipment
                //        {
                //            PackageNumber = i.packageNumber,
                //            TrackingCode = i.barcode
                //        },
                //        MarketplaceId = "HB"
                //    }));

                //    pageCount = data.pageCount;

                //    page++;
                //} while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });

        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                OrderResponse orderResponse = new()
                {
                    Orders = new List<Order>()
                };

                var merchantId = _configuration.MerchantId;
                int page = 0;
                int pageCount = 0;
                int offset = 50;

                do
                {
                    var url = $"{_baseUrl}/packages/merchantid/{merchantId}/undelivered?offset={page * offset}&limit={offset}";
                    var data = _httpHelper.Get<OrderPage>(url, "Basic", this._configuration.Authorization);

                    if (data == null || data.Items == null)
                        break;

                    orderResponse.Orders.AddRange(data.Items.Select(i => new Order { OrderCode = i.OrderNumber }));

                    pageCount = data.PageCount;

                    page++;
                } while (page < pageCount);

                response.Data = orderResponse;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "NotFound";
            });
        }

        public DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> GetChangableCargoCompanies(string packageNumber)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>(response =>
            {
                var merchantId = _configuration.MerchantId;
                var url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}/changablecargocompanies";
                var data = _httpHelper
                    .Get<List<Common.ShipmentPackages.ChangableCargoCompaniesResponse>>(
                        url,
                        "Basic",
                        this._configuration.Authorization);

                if (data == null)
                {
                    response.Success = false;
                    return;
                }

                response.Data = data.Select(d => new Application.Common.DataTransferObjects.KeyValue<string, string>
                {
                    Key = d.ShortName,
                    Value = d.Name
                }).ToList();
                response.Success = true;
            });
        }

        public DataResponse<ChangeCargoCompanyResponse> ChangeCargoCompany(string packageNumber, string cargoCompanyId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ChangeCargoCompanyResponse>>(response =>
            {
                var merchantId = _configuration.MerchantId;
                var url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}/changecargocompany";
                var changeCargoCompanyRequest = new ChangeCargoCompanyRequest
                {
                    CargoCompanyShortName = cargoCompanyId
                };

                response.Success = _httpHelper.Put(changeCargoCompanyRequest, url, "Basic", this._configuration.Authorization);
                response.Data = new ChangeCargoCompanyResponse
                {
                    Retake = false
                };

                var shipmentCompanyId = string.Empty;
                switch (cargoCompanyId)
                {
                    case "YK":
                        shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                        break;
                    case "AR":
                        shipmentCompanyId = ShipmentCompanies.ArasKargo;
                        break;
                    case "SK":
                        shipmentCompanyId = ShipmentCompanies.SuratKargo;
                        break;
                    case "MK":
                        shipmentCompanyId = ShipmentCompanies.MngKargo;
                        break;
                    case "PK":
                        shipmentCompanyId = ShipmentCompanies.PttKargo;
                        break;
                }

                response.Data.ShipmentCompanyId = shipmentCompanyId;
            });
        }

        public Response Unpack(string packageNumber)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var merchantId = _configuration.MerchantId;
                var packageNumber = string.Empty;
                var url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}/unpack";
                response.Success = _httpHelper.Post(url, "Basic", this._configuration.Authorization);
            });
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(HepsiburadaConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            var mutualBarcode = false;
            bool.TryParse(marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "MutualBarcode").Value, out mutualBarcode);

            configuration.MerchantId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "MerchantId").Value;
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
            configuration.MutualBarcode = mutualBarcode;
        }

        protected override String GetAccessTokenConcreate()
        {
            var authenticate = new Authenticate
            {
                username = _configuration.Username,
                password = _configuration.Password
            };
            return this._httpHelper.Post<Authenticate, Token>(authenticate, _tokenUrl, string.Empty, string.Empty, null, null).id_token;
        }
        #endregion

        #region Helper Methods
        //private Token GetToken(Dictionary<string, string> configurations)
        //{
        //    var key = "HepsiburadaProductToken";

        //    if (!this._memoryCache.TryGetValue(key, out Token token))
        //    {
        //        token = this._httpHelper.Post<Authenticate, Token>(
        //            new Authenticate
        //            {
        //                username = _configuration.Username,
        //                password = _configuration.Password
        //                //username = "sinozkozmetik_dev",
        //                //password = "aoUlyaxJDWnzca!"
        //            },
        //            _tokenUrl,
        //            string.Empty,
        //            string.Empty);

        //        this._memoryCache.Set(key, token, new TimeSpan(0, 29, 0));
        //    }

        //    return token;
        //}

        private DataResponse<HepsiburadaProductFilterResponse> Filter(ProductFilterRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<HepsiburadaProductFilterResponse>>((response) =>
            {
                var merchantId = _configuration.MerchantId;

                var filterResponse = this._httpHelper.Get<HepsiburadaProductFilterResponse>(
                   $"https://listing-external.hepsiburada.com/listings/merchantid/{merchantId}?offset=0&limit=1&merchantskulist={productRequest.Stockcode}",
                   "Basic",
                  this._configuration.Authorization);
                if (filterResponse == null)
                {
                    response.Success = false;
                    response.Message = "Ürün bulunamadı";
                    return;
                }

                response.Data = filterResponse;
                response.Success = true;


            });
        }

        private Package Package(IEnumerable<Item> items, OrderRequest request)
        {
            var merchantId = _configuration.MerchantId;

            var packageRequest = new PackagePage
            {
                LineItemRequests = items.Select(sc => new LineItemRequest
                {
                    Id = sc.Id,
                    Quantity = sc.Quantity.ToString()
                }).ToList()
            };

            string url = $"{_baseUrl}/packages/merchantid/{merchantId}";

            Package package = _httpHelper.Post<PackagePage, Package>(packageRequest, url, "Basic", this._configuration.Authorization, null, null);
            return package;
        }

        private CargoPage CargoPage(string packageNumber, OrderRequest request)
        {
            if (string.IsNullOrEmpty(packageNumber)) return null;

            var merchantId = _configuration.MerchantId;

            string url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}";

            CargoPage cargo = _httpHelper.Get<CargoPage[]>(url, "Basic", this._configuration.Authorization)[0];
            return cargo;
        }

        private MutualBarcode MutualBarcode(string packageNumber, OrderRequest request)
        {
            var merchantId = _configuration.MerchantId;

            string url = $"{_baseUrl}/packages/merchantid/{merchantId}/packagenumber/{packageNumber}/labels?format=png";

            MutualBarcode mutualBarcode = _httpHelper.Get<MutualBarcode>(url, "Basic", this._configuration.Authorization);
            return mutualBarcode;
        }

        private ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order MapOrder(IEnumerable<Item> items, string cargoBarcode, string base64, string packageNumber)
        {
            var customerData = items.FirstOrDefault();
            ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order order = new()
            {
                ListTotalAmount = Convert.ToDecimal(items.Sum(x => x.TotalPrice.Amount)) + Convert.ToDecimal(items.Sum(x => x.HbDiscount.TotalPrice.Amount)),
                Discount = Convert.ToDecimal(items.Sum(x => x.HbDiscount.TotalPrice.Amount)),
                OrderCode = customerData.OrderNumber,
                TotalAmount = Convert.ToDecimal(items.Sum(x => x.TotalPrice.Amount)),
                OrderTypeId = OrderTypes.OnaylanmisSiparis,
                OrderDate = customerData.OrderDate,
                OrderSourceId = OrderSources.Pazaryeri,
                MarketplaceId = Application.Common.Constants.Marketplaces.Hepsiburada
            };
            var customerName = customerData.CustomerName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var firstName = customerName.Length > 2 ? customerName[0] + " " + customerName[1] : customerName[0];
            var lastName = customerName[customerName.Length - 1];


            customerName = customerData.Invoice?.Address.Name.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var invoiceFirstName = "";
            var invoiceLastName = "";
            if (customerName.Length > 1)
            {
                invoiceFirstName = customerName.Length > 2 ? customerName[0] + " " + customerName[1] : customerName[0];
                invoiceLastName = customerName[customerName.Length - 1];
            }

            order.Customer = new Customer()
            {
                FirstName = firstName,
                LastName = lastName,
                Phone = customerData.ShippingAddress?.PhoneNumber
            };
            order.OrderDeliveryAddress = new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
            {
                Country = customerData.ShippingAddress.CountryCode == "TR" ? "Türkiye" : customerData.ShippingAddress.CountryCode,
                City = customerData.ShippingAddress.City,
                District = customerData.ShippingAddress.Town,
                Phone = customerData.ShippingAddress.PhoneNumber,
                Email = customerData.ShippingAddress.Email,
                Neighborhood = $"{customerData.ShippingAddress.District}",
                Address = customerData.ShippingAddress.Address,
                FirstName = firstName,
                LastName = lastName
            };
            order.OrderInvoiceAddress = new OrderInvoiceAddress()
            {
                Country = customerData.Invoice?.Address?.CountryCode == "TR" ? "Türkiye" : customerData.Invoice?.Address?.CountryCode,
                City = customerData.Invoice?.Address?.City,
                District = customerData.Invoice?.Address?.Town,
                Neighborhood = $"{customerData.Invoice?.Address?.District}",
                Address = customerData.Invoice?.Address?.address,
                Phone = customerData.Invoice?.Address?.PhoneNumber,
                Email = customerData.Invoice?.Address?.Email,
                TaxOffice = customerData.Invoice?.TaxOffice?.ToString(),
                TaxNumber = customerData.Invoice?.TaxNumber,
                FirstName = invoiceFirstName,
                LastName = invoiceLastName
            };
            order.Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>
            {
                new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                {
                    Amount = Decimal.Parse(items.Sum(tp => tp.TotalPrice.Amount).ToString()),
                    Date = customerData.OrderDate,
                    PaymentTypeId = "OO",
                    CurrencyId = "TL"
                }
            };

            string shipmentId = "";

            switch (customerData.CargoCompanyModel.ShortName)
            {
                case "AR":
                    shipmentId = ShipmentCompanies.ArasKargo;
                    break;
                case "YK":
                    shipmentId = ShipmentCompanies.YurticiKargo;
                    break;
                case "PK":
                    shipmentId = ShipmentCompanies.PttKargo;
                    break;
                case "MK":
                    shipmentId = ShipmentCompanies.MngKargo;
                    break;
                case "SK":
                    shipmentId = ShipmentCompanies.SuratKargo;
                    break;
                case "HX":
                    shipmentId = ShipmentCompanies.HepsiJet;
                    break;
            }

            order.OrderShipment = new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderShipment()
            {
                TrackingCode = cargoBarcode,
                TrackingUrl = customerData.CargoCompanyModel?.TrackingUrl?.Replace("{0}", cargoBarcode),
                TrackingBase64 = base64,
                ShipmentCompanyId = shipmentId, //mapleme yapılacak
                ShipmentTypeId = "3",
                Payor = false,
                PackageNumber = packageNumber
            };

            #region Order Details
            order.OrderDetails = new List<OrderDetail>();
            foreach (var orderDetail in items)
            {

                decimal discountPrice = 0;

                if (orderDetail.HbDiscount != null)
                {
                    discountPrice = Convert.ToDecimal(orderDetail.HbDiscount.UnitPrice.Amount);

                }

                order.OrderDetails.Add(new OrderDetail()
                {
                    UId = orderDetail.Id,
                    Product = new Product()
                    {
                        Barcode = orderDetail.Sku,
                        StockCode = orderDetail.MerchantSKU,
                        Name = orderDetail.Name
                    },
                    Quantity = orderDetail.Quantity,
                    UnitPrice = Convert.ToDecimal(orderDetail.UnitPrice.Amount),
                    ListPrice = Convert.ToDecimal(orderDetail.UnitPrice.Amount) + discountPrice,
                    TaxRate = orderDetail.VatRate / 100,
                    UnitDiscount = discountPrice,
                    UnitCommissionAmount = Convert.ToDecimal(orderDetail.Commission.amount)

                });
            }
            #endregion
            return order;
        }
        #endregion
    }
}