﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada
{
    public class HepsiburadaConfiguration: UsernamePasswordConfigurationBase
    {
        #region Properties
        public string MerchantId { get; set; }

        public bool MutualBarcode { get; set; }
        #endregion
    }
}
