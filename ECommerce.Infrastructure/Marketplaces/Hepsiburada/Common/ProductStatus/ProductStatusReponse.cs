﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductStatus
{
    public class ProductStatusReponse
    {
        #region Properties
        public bool success { get; set; }
        
        public int code { get; set; }
        
        public int version { get; set; }
        
        public object message { get; set; }
        
        public int totalElements { get; set; }
        
        public int totalPages { get; set; }
        
        public int number { get; set; }
        
        public int numberOfElements { get; set; }
        
        public bool first { get; set; }
        
        public bool last { get; set; }
        
        public List<Datum> data { get; set; }
        #endregion
    }

    public class Datum
    {
        #region Properties
        public int itemOrderID { get; set; }
        
        public string merchant { get; set; }
        
        public string merchantSku { get; set; }
        
        public string hbSku { get; set; }
        
        public object barcode { get; set; }
        
        public string productStatus { get; set; }
        
        public string productName { get; set; }
        
        public string variantGroupId { get; set; }
        
        public List<object> taskDetails { get; set; }
        
        public List<ValidationResult> validationResults { get; set; }
        
        public string importStatus { get; set; }
        
        public List<object> importMessages { get; set; } 
        #endregion
    }

    public class ValidationResult
    {
        #region Properties
        public string attributeName { get; set; }
        
        public string message { get; set; } 
        #endregion
    }
}
