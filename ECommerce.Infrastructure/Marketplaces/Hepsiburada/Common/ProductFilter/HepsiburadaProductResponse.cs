﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductFilter
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class HepsiburadaProduct
    {
        public string merchantSku { get; set; }
        public string barcode { get; set; }
        public string hbSku { get; set; }
        public string variantGroupId { get; set; }
        public string productName { get; set; }
        public string brand { get; set; }
        public List<string> images { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public string tax { get; set; }
        public string price { get; set; }
        public string description { get; set; }
        public List<VariantTypeAttribute> variantTypeAttributes { get; set; }
        public List<ProductAttribute> productAttributes { get; set; }
    }

    public class VariantTypeAttribute
    {
        public string name { get; set; }
        public string value { get; set; }
        public bool mandatory { get; set; }
    }

    public class ProductAttribute
    {
        public string name { get; set; }
        public string value { get; set; }
        public bool mandatory { get; set; }
    }

    public class HepsiburadaProductResponse
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public List<HepsiburadaProduct> data { get; set; }
    }

  
}
