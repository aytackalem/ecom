﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.StockBatchResult
{
    
    public class Error
    {
        public int elementNo { get; set; }
        public string hepsiburadaSku { get; set; }
        public string merchantSku { get; set; }
        public List<string> errors { get; set; }
    }

    public class StockBatchResult
    {
        public string id { get; set; }
        public string status { get; set; }
        public DateTime createdAt { get; set; }
        public int total { get; set; }
        public List<Error> errors { get; set; }
    }
}
