﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Status
{
    public class Unpacked
    {
        #region Properties
        public DateTime unpackedDate { get; set; }
        
        public string packageNumber { get; set; }
        
        public string barcode { get; set; }
        #endregion
    }
}
