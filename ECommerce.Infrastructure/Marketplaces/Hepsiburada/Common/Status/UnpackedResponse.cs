﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Status
{
    public class UnpackedResponse
    {
        #region Properties
        public int totalCount { get; set; }
        
        public int limit { get; set; }
        
        public int offset { get; set; }
        
        public int pageCount { get; set; }

        public List<Unpacked> items { get; set; }
        #endregion
    }
}
