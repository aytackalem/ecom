﻿namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ShipmentPackages
{
    public class ChangableCargoCompaniesResponse
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string LogoUrl { get; set; }

        public bool IsActive { get; set; }
        #endregion
    }
}
