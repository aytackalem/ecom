﻿namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Packages
{
    public class ChangeCargoCompanyRequest
    {
        #region Properties
        public string CargoCompanyShortName { get; set; }
        #endregion
    }
}
