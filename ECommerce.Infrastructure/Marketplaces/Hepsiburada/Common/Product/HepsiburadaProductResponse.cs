﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Product
{
    public class HepsiburadaProductResponse
    {
        #region Property
        public bool success { get; set; }

        public int code { get; set; }

        public int version { get; set; }

        public object message { get; set; }

        public HepsiburadaProductData data { get; set; }
        #endregion
    }

    public class HepsiburadaProductData
    {
        #region Property
        public string trackingId { get; set; }
        #endregion
    }
}
