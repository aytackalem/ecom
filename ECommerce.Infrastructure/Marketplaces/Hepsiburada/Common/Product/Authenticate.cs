﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Product
{
    public class Authenticate
    {
        #region Propery
        public string username { get; set; }

        public string password { get; set; }

        public string authenticationType => "INTEGRATOR";
        #endregion
    }
}
