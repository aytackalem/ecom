﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Stock
{
    public class HepsiburadaPriceRequest
    {
        public string HepsiburadaSku { get; set; }
        public string MerchantSku { get; set; }
        public double Price { get; set; }
    }
}
