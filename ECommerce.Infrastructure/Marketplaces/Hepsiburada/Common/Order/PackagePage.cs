﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order
{
    public class PackagePage
    {
        public IList<LineItemRequest> LineItemRequests { get; set; }
    }
}
