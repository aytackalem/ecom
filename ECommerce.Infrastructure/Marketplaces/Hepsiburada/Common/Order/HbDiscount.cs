﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order
{
    public class HbDiscount
    {
        public TotalPrice TotalPrice { get; set; }
        public UnitPrice UnitPrice { get; set; }
    }
}
