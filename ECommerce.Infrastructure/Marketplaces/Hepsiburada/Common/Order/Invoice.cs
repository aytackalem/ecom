﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order
{
    public class Invoice
    {
        public string TurkishIdentityNumber { get; set; }
        public string TaxNumber { get; set; }
        public object TaxOffice { get; set; }
        public Address Address { get; set; }
    }
}
