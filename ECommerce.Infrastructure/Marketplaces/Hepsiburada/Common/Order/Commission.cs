﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order
{
    public class Commission
    {
        public string currency { get; set; }
        public double amount { get; set; }
    }
}
