﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Order
{
    public class MutualBarcode
    {
        public string Format { get; set; }
        
        public List<string> Data { get; set; }
        
        public string Description { get; set; }
        
        public string Code { get; set; }
    }
}
