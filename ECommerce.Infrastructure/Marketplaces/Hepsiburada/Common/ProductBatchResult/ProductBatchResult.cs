﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.ProductBatchResult
{

    public class ProductBatchResultData
    {
        public int itemOrderID { get; set; }
        public string merchant { get; set; }
        public string merchantSku { get; set; }
        public string hbSku { get; set; }
        public string barcode { get; set; }
        public object productStatus { get; set; }
        public string productName { get; set; }
        public string variantGroupId { get; set; }
        public List<string> taskDetails { get; set; }
        public List<string> validationResults { get; set; }
        public List<string> rejectReasonsMessages { get; set; }
        public string importStatus { get; set; }
        public List<ImportMessage> importMessages { get; set; }
    }

    public class ProductBatchResult
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public string message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public List<ProductBatchResultData> data { get; set; }
    }
    public class ImportMessage
    {
        public string severity { get; set; }
        public string message { get; set; }
    }
}
