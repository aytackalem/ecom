﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Category
{
    public class HepsiburadaCategoryAttributePage
    {
        #region Property
        public bool Success { get; set; }

        public int Code { get; set; }

        public int Version { get; set; }

        public object Message { get; set; }

        public HepsiburadaCategoryAttribute Data { get; set; }
        #endregion
    }
}
