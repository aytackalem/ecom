﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Category
{
    public class HepsiburadaVariantAttribute
    {
        public HepsiburadaVariantAttribute()
        {
            this.CategoryAttributeValues = new List<HepsiburadaCategoryAttributeValue>();
        }
        #region Property
        public string Name { get; set; }

        public string Id { get; set; }

        public bool Mandatory { get; set; }

        public string Type { get; set; }

        public bool MultiValue { get; set; }

        public int MyProperty { get; set; }

        public List<HepsiburadaCategoryAttributeValue> CategoryAttributeValues { get; set; }

        #endregion
    }
}
