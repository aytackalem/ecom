﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Hepsiburada.Common.Category
{
    public class HepsiburadaCategoryProductType
    {
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
    }
}
