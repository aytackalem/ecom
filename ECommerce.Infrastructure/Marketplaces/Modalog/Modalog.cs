﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Infrastructure.Marketplaces.Modalog.Requests.Token;
using ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Order;
using ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Token;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Marketplaces.Modalog
{
    public class Modalog : TokenMarketplaceBase<ModalogConfiguration>
    {
        #region Constants
        //private const string _baseUrl = "https://marketplace.modanisa.com/api/marketplace";

        private const string _baseUrl = "https://merchant-api.modalog.com";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Modalog(IHttpHelper httpHelper, IServiceProvider serviceProvider, ICacheHandler cacheHandler) : base(serviceProvider, cacheHandler, 29)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Modalog;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new();

                var token = this.GetAccessToken().Data;

                var page = 1;
                var totalPage = 0;

                do
                {
                    var url = $"{_baseUrl}/Products?pageIndex={page}";
                    var _ = this._httpHelper.Get<Responses.Product.ProductResponse>(url, "Bearer", token);
                    if (_.success)
                    {
                        totalPage = _.pageCount;
                        response.Data.AddRange(_.data.Select(d => new ProductItem
                        {
                            StockItems = d
                                .variantMerchants
                                .Select(v => new StockItem
                                {
                                    Barcode = v.barcode,
                                    StockCode = v.stockCode,
                                    Quantity = v.stock,
                                    ListPrice = v.listPrice,
                                    SalePrice = v.salePrice
                                })
                                .ToList()
                        }));
                        page++;
                    }
                    else
                        break;

                } while (page < totalPage);
            });
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>((response) =>
            {
                response.Data = new();

                var token = this.GetAccessToken().Data;

                var page = 1;
                var totalPage = 0;

                do
                {
                    var url = $"{_baseUrl}/Products?pageIndex={page}";
                    var _ = this._httpHelper.Get<Responses.Product.ProductResponse>(url, "Bearer", token);
                    if (_.success)
                    {
                        totalPage = _.pageCount;

                        response.Data.AddRange(_
                            .data
                            .SelectMany(d => d
                                .variantMerchants
                                .Select(vm => new Application.Common.DataTransferObjects.MarketplaceProductStatus
                                {
                                    Barcode = d.barcode,
                                    OnSale = d.statusName == "Confirmed",
                                    Opened = true,
                                    Locked = false,
                                    Url = "",
                                    ListUnitPrice = vm.listPrice,
                                    UnitPrice = vm.salePrice,
                                    Stock = vm.stock,
                                    StockCode = null,
                                    MarketplaceId = "ML"
                                })));

                        page++;
                    }
                    else
                        break;

                } while (page <= totalPage);

                response.Success = true;
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var token = this.GetAccessTokenConcreate();

                List<CategoryResponse> categories = new();

                #region Fetch Categories
                var modalogCategories = new List<Responses.Category.Category>();
                var categoryPageCount = 0;
                var categoryPage = 0;
                do
                {
                    var categoryResponse = this._httpHelper.Get<Responses.Category.CategoryResponse>(
                        $"{_baseUrl}/Categories",
                        "Bearer",
                        token);

                    if (categoryPageCount == 0)
                        categoryPageCount = categoryResponse.pageCount;

                    modalogCategories.AddRange(categoryResponse.data);
                } while (categoryPageCount < categoryPage);

                foreach (var theModalogCategory in modalogCategories)
                {
                    if (theModalogCategory.subCategories == null)
                        categories.Add(new CategoryResponse
                        {
                            Code = theModalogCategory.id.ToString(),
                            Name = theModalogCategory.name
                        });
                    else
                        FetchSubCategory(categories, theModalogCategory.subCategories);
                }
                #endregion

                #region Fetch Variants
                var variantValues = new List<Responses.Variant.VariantValueResponseItem>();
                var variantValuePageCount = 0;
                var variantValuePage = 1;
                do
                {
                    var variantValueResponse = this
                        ._httpHelper
                        .Get<Responses.Variant.VariantValueResponse>(
                            $"{_baseUrl}/Variants?pageIndex={variantValuePage}",
                            "Bearer",
                            token);

                    if (variantValuePageCount == 0)
                        variantValuePageCount = variantValueResponse.pageCount;

                    variantValues.AddRange(variantValueResponse.data);

                    variantValuePage++;
                } while (variantValuePageCount >= variantValuePage);
                #endregion

                #region Fetch Attributes
                var attributes = new List<Responses.Attribute.AttributeResponseItem>();
                var attributePageCount = 0;
                var attributePage = 1;
                do
                {
                    var attributeResponse = this
                        ._httpHelper
                        .Get<Responses.Attribute.AttributeResponse>(
                            $"{_baseUrl}/Attributes?pageIndex={attributePage}",
                            "Bearer",
                            token);

                    if (attributePageCount == 0)
                        attributePageCount = attributeResponse.pageCount;

                    attributes.AddRange(attributeResponse.data);

                    attributePage++;
                } while (attributePageCount >= attributePage);

                foreach (var theAttribute in attributes)
                {
                    #region Fetch Attribute Values
                    var attributeValues = new List<Responses.AttributeValue.AttributeValueResponseItem>();
                    var attributeValuePageCount = 0;
                    var attributeValuePage = 1;
                    do
                    {
                        var attributeValueResponse = this
                            ._httpHelper
                            .Get<Responses.AttributeValue.AttributeValueResponse>(
                                $"{_baseUrl}/Attributes/{theAttribute.id}/Values?pageIndex={attributeValuePage}",
                                "Bearer",
                                token);

                        if (attributeValuePageCount == 0)
                            attributeValuePageCount = attributeValueResponse.pageCount;

                        attributeValues.AddRange(attributeValueResponse.data);

                        attributeValuePage++;
                    } while (attributeValuePageCount >= attributeValuePage);
                    #endregion

                    theAttribute.Values = attributeValues;
                }
                #endregion

                var variantId = Guid.NewGuid().ToString();

                foreach (var theCategory in categories)
                {
                    #region Add Variants As Category Attribute Values
                    theCategory.CategoryAttributes.Add(new CategoryAttributeResponse
                    {
                        Code = variantId,
                        Name = "Beden",
                        Variantable = true,
                        CategoryCode = theCategory.Code,
                        CategoryAttributesValues = variantValues.Select(vv => new CategoryAttributeValueResponse
                        {
                            Code = vv.id.ToString(),
                            Name = vv.name,
                            VarinatCode = variantId
                        }).ToList()
                    });
                    #endregion

                    #region Fetch Category Attributes
                    var categoryAttributes = new List<Responses.CategoryAttribute.CategoryAttribute>();
                    var categoryAttributePageCount = 0;
                    var categoryAttributePage = 0;
                    do
                    {
                        var categoryAttributeResponse = this._httpHelper.Get<Responses.CategoryAttribute.CategoryAttributeResponse>($"{_baseUrl}/Categories/{theCategory.Code}/Attributes", "Bearer", token);

                        if (categoryAttributePageCount == 0)
                            categoryAttributePageCount = categoryAttributeResponse.pageCount;

                        categoryAttributes.AddRange(categoryAttributeResponse.data);

                        categoryAttributePage++;
                    } while (categoryAttributePageCount > categoryAttributePage);
                    #endregion

                    theCategory
                        .CategoryAttributes
                        .AddRange(attributes
                            .Where(a => categoryAttributes.Select(ca => ca.attributeId).Contains(a.id))
                            .Select(a => new CategoryAttributeResponse
                            {
                                AllowCustom = a.isCustom,
                                Code = a.id.ToString(),
                                Name = a.name,
                                CategoryCode = theCategory.Code,
                                CategoryAttributesValues = a.Values.Select(v => new CategoryAttributeValueResponse
                                {
                                    Code = v.id.ToString(),
                                    Name = v.name,
                                    VarinatCode = v.attributeId.ToString()
                                }).ToList()
                            }));
                }
                response.Success = true;
                response.Data = categories;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var orders = new List<Responses.Order.Datum>();

                var token = this.GetAccessToken().Data;

                var page = 1;
                var totalPage = 0;
                var successFetch = true;

                do
                {
                    var url = $"{_baseUrl}/Orders/NEW?pi={page}";
                    var rootObject = this._httpHelper.Get<Responses.Order.Rootobject>(url, "Bearer", token);
                    if (rootObject.success)
                    {
                        totalPage = rootObject.pageCount;
                        orders.AddRange(rootObject.data);
                        page++;
                    }
                    else
                    {
                        successFetch = false;
                        break;
                    }

                } while (page < totalPage);

                if (successFetch)
                {
                    response
                        .Data
                        .Orders
                        .AddRange(orders
                            .GroupBy(o => o.orderNumber)
                            .Select(o =>
                            {
                                var invoiceAddressParts = o.First().invoiceAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                var invoiceRecipientFullNameParts = o.First().invoiceRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                var invoiceName = invoiceRecipientFullNameParts.Take(invoiceRecipientFullNameParts.Length - 1).First();
                                var invoiceSurname = string.Join(" ", invoiceRecipientFullNameParts.Skip(invoiceRecipientFullNameParts.Length - 1).Take(1));
                                var invoiceCountry = invoiceAddressParts.Skip(invoiceAddressParts.Length - 1).Take(1).First();
                                var invoiceCity = invoiceAddressParts.Skip(invoiceAddressParts.Length - 2).Take(1).First();
                                var invoiceDistrict = invoiceAddressParts.Skip(invoiceAddressParts.Length - 3).Take(1).First();
                                var invoiceAddress = o.First().invoiceAddress;

                                var deliveryName = string.Empty;
                                var deliverySurname = string.Empty;
                                var deliveryCountry = string.Empty;
                                var deliveryCity = string.Empty;
                                var deliveryDistrict = string.Empty;
                                var deliveryAddress = invoiceAddress;

                                if (string.IsNullOrEmpty(o.First().shipmentRecipientFullName))
                                {
                                    deliveryName = invoiceName;
                                    deliverySurname = invoiceSurname;
                                    deliveryCountry = invoiceCountry;
                                    deliveryCity = invoiceCity;
                                    deliveryDistrict = invoiceDistrict;
                                    deliveryAddress = invoiceAddress;
                                }
                                else
                                {
                                    var shipmentAddressParts = o.First().shipmentAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                    var shipmentRecipientFullNameParts = o.First().shipmentRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                    deliveryName = shipmentRecipientFullNameParts.Take(shipmentRecipientFullNameParts.Length - 1).First();
                                    deliverySurname = string.Join(" ", shipmentRecipientFullNameParts.Skip(shipmentRecipientFullNameParts.Length - 1).Take(1));
                                    deliveryCountry = shipmentAddressParts.Skip(shipmentAddressParts.Length - 1).Take(1).First();
                                    deliveryCity = shipmentAddressParts.Skip(shipmentAddressParts.Length - 2).Take(1).First();
                                    deliveryDistrict = shipmentAddressParts.Skip(shipmentAddressParts.Length - 3).Take(1).First();
                                }

                                return new Application.Common.Wrappers.Marketplaces.Order.Order
                                {
                                    OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                                    MarketplaceId = Application.Common.Constants.Marketplaces.Modalog,
                                    OrderCode = $"{o.Key}",
                                    OrderTypeId = "OS",
                                    CargoFee = 0,
                                    Discount = 0,
                                    ListTotalAmount = o.Sum(od => od.price),
                                    TotalAmount = o.Sum(od => od.discountedPrice),
                                    OrderDate = o.First().createDate,
                                    OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                                    {
                                        PackageNumber = o.First().packageNumber
                                    },
                                    Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Mail = "",
                                        TaxAdministration = String.Empty,
                                        TaxNumber = String.Empty
                                    },
                                    OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Country = deliveryCountry,
                                        Email = "",
                                        Phone = "",
                                        City = deliveryCity,
                                        District = deliveryDistrict,
                                        Neighborhood = string.Empty,
                                        Address = deliveryAddress
                                    },
                                    OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                                    {
                                        FirstName = invoiceName,
                                        LastName = invoiceSurname,
                                        Email = "",
                                        Phone = "",
                                        Country = invoiceCountry,
                                        City = invoiceCity,
                                        District = invoiceDistrict,
                                        Address = invoiceAddress,
                                        Neighborhood = string.Empty,
                                        TaxNumber = "",
                                        TaxOffice = ""
                                    },
                                    OrderDetails = o
                                        .Select(od => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                        {
                                            ListPrice = od.price,
                                            UnitPrice = od.discountedPrice,
                                            Payor = true,
                                            Quantity = od.qty,
                                            UnitDiscount = 0,
                                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                            {
                                                Barcode = od.barcode,
                                                Name = od.productName,
                                                StockCode = String.Empty
                                            }
                                        })
                                        .ToList()
                                };
                            }));
                    response.Success = true;

                    foreach (var theOrder in response.Data.Orders)
                    {
                        var _ = this
                            ._httpHelper
                            .Post<SetOrderPreparingResponse>($"{_baseUrl}/SetOrderPreparing/{theOrder.OrderShipment.PackageNumber}", "Bearer", token);
                        theOrder.OrderShipment.TrackingCode = _.trackingNumber;
                        switch (_.cargoAgent)
                        {
                            case "ARAS":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.ArasKargo;
                                break;
                            case "YURT":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.YurticiKargo;
                                break;
                            case "MNG":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.MngKargo;
                                break;
                            case "PTT":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.PttKargo;
                                break;
                            case "SURT":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.SuratKargo;
                                break;
                            case "UPS":
                                theOrder.OrderShipment.ShipmentCompanyId = Application.Common.Constants.ShipmentCompanies.Ups;
                                break;
                            default:
                                throw new Exception("Unsupported shipment company.");
                        }
                    }
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var orders = new List<Responses.Order.Datum>();

                var token = this.GetAccessToken().Data;

                var page = 1;
                var totalPage = 0;
                var successFetch = true;

                do
                {
                    var url = $"{_baseUrl}/CanceledOrders/UNRESOLVED?pi={page}";
                    var rootObject = this._httpHelper.Get<Responses.Order.Rootobject>(url, "Bearer", token);
                    if (rootObject.success)
                    {
                        totalPage = rootObject.pageCount;
                        orders.AddRange(rootObject.data);
                        page++;
                    }
                    else
                    {
                        successFetch = false;
                        break;
                    }

                } while (page < totalPage);

                if (successFetch)
                {
                    response
                        .Data
                        .Orders
                        .AddRange(orders
                            .GroupBy(o => o.orderNumber)
                            .Select(o =>
                            {
                                var invoiceAddressParts = o.First().invoiceAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                var invoiceRecipientFullNameParts = o.First().invoiceRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                var invoiceName = invoiceRecipientFullNameParts.Take(invoiceRecipientFullNameParts.Length - 1).First();
                                var invoiceSurname = string.Join(" ", invoiceRecipientFullNameParts.Skip(invoiceRecipientFullNameParts.Length - 1).Take(1));
                                var invoiceCountry = invoiceAddressParts.Skip(invoiceAddressParts.Length - 1).Take(1).First();
                                var invoiceCity = invoiceAddressParts.Skip(invoiceAddressParts.Length - 2).Take(1).First();
                                var invoiceDistrict = invoiceAddressParts.Skip(invoiceAddressParts.Length - 3).Take(1).First();
                                var invoiceAddress = o.First().invoiceAddress;

                                var deliveryName = string.Empty;
                                var deliverySurname = string.Empty;
                                var deliveryCountry = string.Empty;
                                var deliveryCity = string.Empty;
                                var deliveryDistrict = string.Empty;
                                var deliveryAddress = invoiceAddress;

                                if (string.IsNullOrEmpty(o.First().shipmentRecipientFullName))
                                {
                                    deliveryName = invoiceName;
                                    deliverySurname = invoiceSurname;
                                    deliveryCountry = invoiceCountry;
                                    deliveryCity = invoiceCity;
                                    deliveryDistrict = invoiceDistrict;
                                    deliveryAddress = invoiceAddress;
                                }
                                else
                                {
                                    var shipmentAddressParts = o.First().shipmentAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                    var shipmentRecipientFullNameParts = o.First().shipmentRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                    deliveryName = shipmentRecipientFullNameParts.Take(shipmentRecipientFullNameParts.Length - 1).First();
                                    deliverySurname = string.Join(" ", shipmentRecipientFullNameParts.Skip(shipmentRecipientFullNameParts.Length - 1).Take(1));
                                    deliveryCountry = shipmentAddressParts.Skip(shipmentAddressParts.Length - 1).Take(1).First();
                                    deliveryCity = shipmentAddressParts.Skip(shipmentAddressParts.Length - 2).Take(1).First();
                                    deliveryDistrict = shipmentAddressParts.Skip(shipmentAddressParts.Length - 3).Take(1).First();
                                }

                                return new Application.Common.Wrappers.Marketplaces.Order.Order
                                {
                                    OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                                    MarketplaceId = Application.Common.Constants.Marketplaces.Modalog,
                                    OrderCode = $"{o.Key}",
                                    OrderTypeId = "OS",
                                    CargoFee = 0,
                                    Discount = 0,
                                    ListTotalAmount = o.Sum(od => od.price),
                                    TotalAmount = o.Sum(od => od.discountedPrice),
                                    OrderDate = o.First().createDate,
                                    Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Mail = "",
                                        TaxAdministration = String.Empty,
                                        TaxNumber = String.Empty
                                    },
                                    OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Country = deliveryCountry,
                                        Email = "",
                                        Phone = "",
                                        City = deliveryCity,
                                        District = deliveryDistrict,
                                        Neighborhood = string.Empty,
                                        Address = deliveryAddress
                                    },
                                    OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                                    {
                                        FirstName = invoiceName,
                                        LastName = invoiceSurname,
                                        Email = "",
                                        Phone = "",
                                        Country = invoiceCountry,
                                        City = invoiceCity,
                                        District = invoiceDistrict,
                                        Address = invoiceAddress,
                                        Neighborhood = string.Empty,
                                        TaxNumber = "",
                                        TaxOffice = ""
                                    },
                                    OrderDetails = o
                                        .Select(od => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                        {
                                            ListPrice = od.price,
                                            UnitPrice = od.discountedPrice,
                                            Payor = true,
                                            Quantity = od.qty,
                                            UnitDiscount = 0,
                                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                            {
                                                Barcode = od.barcode,
                                                Name = od.productName,
                                                StockCode = String.Empty
                                            }
                                        })
                                        .ToList()
                                };
                            }));
                    response.Success = true;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var orders = new List<Responses.Order.Datum>();

                var token = this.GetAccessToken().Data;

                var page = 1;
                var totalPage = 0;
                var successFetch = true;

                do
                {
                    var url = $"{_baseUrl}/Orders/DELIVERED?pi={page}";
                    var rootObject = this._httpHelper.Get<Responses.Order.Rootobject>(url, "Bearer", token);
                    if (rootObject.success)
                    {
                        totalPage = rootObject.pageCount;
                        orders.AddRange(rootObject.data);
                        page++;
                    }
                    else
                    {
                        successFetch = false;
                        break;
                    }

                } while (page < totalPage);

                if (successFetch)
                {
                    response
                        .Data
                        .Orders
                        .AddRange(orders
                            .GroupBy(o => o.orderNumber)
                            .Select(o =>
                            {
                                var invoiceAddressParts = o.First().invoiceAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                var invoiceRecipientFullNameParts = o.First().invoiceRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                var invoiceName = invoiceRecipientFullNameParts.Take(invoiceRecipientFullNameParts.Length - 1).First();
                                var invoiceSurname = string.Join(" ", invoiceRecipientFullNameParts.Skip(invoiceRecipientFullNameParts.Length - 1).Take(1));
                                var invoiceCountry = invoiceAddressParts.Skip(invoiceAddressParts.Length - 1).Take(1).First();
                                var invoiceCity = invoiceAddressParts.Skip(invoiceAddressParts.Length - 2).Take(1).First();
                                var invoiceDistrict = invoiceAddressParts.Skip(invoiceAddressParts.Length - 3).Take(1).First();
                                var invoiceAddress = o.First().invoiceAddress;

                                var deliveryName = string.Empty;
                                var deliverySurname = string.Empty;
                                var deliveryCountry = string.Empty;
                                var deliveryCity = string.Empty;
                                var deliveryDistrict = string.Empty;
                                var deliveryAddress = invoiceAddress;

                                if (string.IsNullOrEmpty(o.First().shipmentRecipientFullName))
                                {
                                    deliveryName = invoiceName;
                                    deliverySurname = invoiceSurname;
                                    deliveryCountry = invoiceCountry;
                                    deliveryCity = invoiceCity;
                                    deliveryDistrict = invoiceDistrict;
                                    deliveryAddress = invoiceAddress;
                                }
                                else
                                {
                                    var shipmentAddressParts = o.First().shipmentAddress.Split(new[] { "/", "\n", " " }, StringSplitOptions.RemoveEmptyEntries);
                                    var shipmentRecipientFullNameParts = o.First().shipmentRecipientFullName.Split(" ", StringSplitOptions.RemoveEmptyEntries);

                                    deliveryName = shipmentRecipientFullNameParts.Take(shipmentRecipientFullNameParts.Length - 1).First();
                                    deliverySurname = string.Join(" ", shipmentRecipientFullNameParts.Skip(shipmentRecipientFullNameParts.Length - 1).Take(1));
                                    deliveryCountry = shipmentAddressParts.Skip(shipmentAddressParts.Length - 1).Take(1).First();
                                    deliveryCity = shipmentAddressParts.Skip(shipmentAddressParts.Length - 2).Take(1).First();
                                    deliveryDistrict = shipmentAddressParts.Skip(shipmentAddressParts.Length - 3).Take(1).First();
                                }

                                return new Application.Common.Wrappers.Marketplaces.Order.Order
                                {
                                    OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                                    MarketplaceId = Application.Common.Constants.Marketplaces.Modalog,
                                    OrderCode = $"{o.Key}",
                                    OrderTypeId = "OS",
                                    CargoFee = 0,
                                    Discount = 0,
                                    ListTotalAmount = o.Sum(od => od.price),
                                    TotalAmount = o.Sum(od => od.discountedPrice),
                                    OrderDate = o.First().createDate,
                                    Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Mail = "",
                                        TaxAdministration = String.Empty,
                                        TaxNumber = String.Empty
                                    },
                                    OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                                    {
                                        FirstName = deliveryName,
                                        LastName = deliverySurname,
                                        Country = deliveryCountry,
                                        Email = "",
                                        Phone = "",
                                        City = deliveryCity,
                                        District = deliveryDistrict,
                                        Neighborhood = string.Empty,
                                        Address = deliveryAddress
                                    },
                                    OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                                    {
                                        FirstName = invoiceName,
                                        LastName = invoiceSurname,
                                        Email = "",
                                        Phone = "",
                                        Country = invoiceCountry,
                                        City = invoiceCity,
                                        District = invoiceDistrict,
                                        Address = invoiceAddress,
                                        Neighborhood = string.Empty,
                                        TaxNumber = "",
                                        TaxOffice = ""
                                    },
                                    OrderDetails = o
                                        .Select(od => new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                                        {
                                            ListPrice = od.price,
                                            UnitPrice = od.discountedPrice,
                                            Payor = true,
                                            Quantity = od.qty,
                                            UnitDiscount = 0,
                                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                            {
                                                Barcode = od.barcode,
                                                Name = od.productName,
                                                StockCode = String.Empty
                                            }
                                        })
                                        .ToList()
                                };
                            }));
                    response.Success = true;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() } };
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

            });
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return null;
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(ModalogConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.Email = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Email").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }

        protected override string GetAccessTokenConcreate()
        {
            return this
                ._httpHelper
                .Post<TokenRequest, TokenResponse>(
                    new TokenRequest
                    {
                        Email = this._configuration.Email,
                        Password = this._configuration.Password
                    }, $"{_baseUrl}/Auth/Login", null, null, null, null).data.token;
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategory(List<CategoryResponse> categoryResponse, List<Responses.Category.SubCategory> subCategories)
        {
            foreach (var theSubCategory in subCategories)
            {
                if (theSubCategory.subCategories != null && theSubCategory.subCategories.Count > 0)
                {
                    FetchSubCategory(categoryResponse, theSubCategory.subCategories);
                }
                else
                {
                    categoryResponse.Add(new CategoryResponse
                    {
                        Code = theSubCategory.id.ToString(),
                        ParentCategoryCode = theSubCategory.parentId.ToString(),
                        Name = theSubCategory.name
                    });
                }
            }
        }
        #endregion
    }
}
