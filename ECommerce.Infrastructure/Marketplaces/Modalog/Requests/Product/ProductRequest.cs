﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Modalog.Requests.Product
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class Attribute
    {
        public int AttributeId { get; set; }
        public int AttributeValueId { get; set; }
        public string AttributeCustomValue { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
    }

    public class ProductRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public int VatRate { get; set; }
        public List<Image> Images { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<Variant> Variants { get; set; }
    }

    public class Variant
    {
        public int VariantValueId { get; set; }
        public string StockCode { get; set; }
        public double ListPrice { get; set; }
        public double SalePrice { get; set; }
        public int Stock { get; set; }
        public string Barcode { get; set; }
        public int Desi { get; set; }
        public double Weight { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public double Height { get; set; }
        public int ShipmentDuration { get; set; }
        public int ShipmentAddressId { get; set; }
        public int ReturningAddressId { get; set; }
        public int? ShipmentProviderId { get; set; }
    }


}
