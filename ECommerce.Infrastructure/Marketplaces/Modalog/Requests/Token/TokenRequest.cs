﻿namespace ECommerce.Infrastructure.Marketplaces.Modalog.Requests.Token
{
    public class TokenRequest
    {
        #region Properties
        public string Email { get; set; }

        public string Password { get; set; } 
        #endregion
    }
}
