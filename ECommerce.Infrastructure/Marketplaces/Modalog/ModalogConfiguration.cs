﻿namespace ECommerce.Infrastructure.Marketplaces.Modalog
{
    public class ModalogConfiguration
    {
        #region Properties
        public string Email { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
