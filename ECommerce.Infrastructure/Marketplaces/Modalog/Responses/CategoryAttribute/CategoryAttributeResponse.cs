﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.CategoryAttribute
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class CategoryAttribute
    {
        public int attributeId { get; set; }
        public bool isRequired { get; set; }
        public string attributeName { get; set; }
        public object categoryName { get; set; }
        public bool isCustom { get; set; }
        public int categoryId { get; set; }
        public int id { get; set; }
    }

    public class CategoryAttributeResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<CategoryAttribute> data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }


}
