﻿namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Product
{

    public class ProductResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public Datum[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class Datum
    {
        public string sku { get; set; }
        public object mpn { get; set; }
        public string gtin { get; set; }
        public string barcode { get; set; }
        public int groupId { get; set; }
        public int brandId { get; set; }
        public string brandName { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public int statusId { get; set; }
        public string statusName { get; set; }
        public int vat { get; set; }
        public string[] images { get; set; }
        public Langdetail[] langDetails { get; set; }
        public Attributevalue[] attributeValues { get; set; }
        public Variantmerchant[] variantMerchants { get; set; }
    }

    public class Langdetail
    {
        public string languageCode { get; set; }
        public string languageName { get; set; }
        public string name { get; set; }
        public string descriptionHtml { get; set; }
    }

    public class Attributevalue
    {
        public int attributeId { get; set; }
        public string attributeName { get; set; }
        public int attributeValueId { get; set; }
        public string attributeValueName { get; set; }
        public string attributeCustomValue { get; set; }
        public string languageCode { get; set; }
        public string languageName { get; set; }
    }

    public class Variantmerchant
    {
        public string barcode { get; set; }
        public int variantValueMerchantId { get; set; }
        public int variantValueId { get; set; }
        public string variantValueName { get; set; }
        public string stockCode { get; set; }
        public decimal listPrice { get; set; }
        public decimal salePrice { get; set; }
        public int commissionRate { get; set; }
        public int statusId { get; set; }
        public string statusName { get; set; }
        public int stock { get; set; }
        public int desi { get; set; }
        public int weight { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public int shipmentDuration { get; set; }
        public int shipmentAddressId { get; set; }
        public int returnAddressId { get; set; }
    }

}
