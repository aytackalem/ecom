﻿namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Order
{
    public class SetOrderPreparingResponse
    {
        public string cargoAgent { get; set; }

        public string trackingNumber { get; set; }
    }
}
