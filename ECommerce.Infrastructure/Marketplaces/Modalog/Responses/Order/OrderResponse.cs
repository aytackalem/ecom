﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Order
{

    public class Rootobject
    {
        public bool success { get; set; }
        public string message { get; set; }
        public Datum[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class Datum
    {
        public string orderId { get; set; }
        public string orderNumber { get; set; }
        public string packageId { get; set; }
        public string packageNumber { get; set; }
        public string firstLastName { get; set; }
        public int qty { get; set; }
        public int canceledQty { get; set; }
        public decimal price { get; set; }
        public decimal discountedPrice { get; set; }
        public string imageLink { get; set; }
        public string stockCode { get; set; }
        public string productName { get; set; }
        public string barcode { get; set; }
        public int variantValueId { get; set; }
        public string variantValueName { get; set; }
        public string packageStatusCode { get; set; }
        public string packageStatusName { get; set; }
        public string cargoAgent { get; set; }
        public int deliveryDuration { get; set; }
        public DateTime deliveredDate { get; set; }
        public string cargoReservationNumber { get; set; }
        public string cargoTrackingNumber { get; set; }
        public bool isCargoFree { get; set; }
        public DateTime createDate { get; set; }
        public int totalCount { get; set; }
        public string invoiceAddress { get; set; }
        public string invoiceRecipientFullName { get; set; }
        public string shipmentAddress { get; set; }
        public string shipmentRecipientFullName { get; set; }
    }

}
