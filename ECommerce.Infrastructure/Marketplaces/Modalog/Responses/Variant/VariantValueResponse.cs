﻿namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Variant
{
    public class VariantValueResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public VariantValueResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class VariantValueResponseItem
    {
        public string name { get; set; }
        public int sort { get; set; }
        public int id { get; set; }
    }

}
