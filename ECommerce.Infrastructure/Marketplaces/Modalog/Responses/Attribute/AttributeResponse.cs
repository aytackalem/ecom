﻿using ECommerce.Infrastructure.Marketplaces.Modalog.Responses.AttributeValue;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Modalog.Responses.Attribute
{
    public class AttributeResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public AttributeResponseItem[] data { get; set; }
        public int rowCount { get; set; }
        public int currentPageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }

    public class AttributeResponseItem
    {
        public string name { get; set; }
        public bool isCustom { get; set; }
        public int id { get; set; }

        /// <summary>
        /// Custom olusturulmustur.
        /// </summary>
        public List<AttributeValueResponseItem> Values { get; set; }
    }

}
