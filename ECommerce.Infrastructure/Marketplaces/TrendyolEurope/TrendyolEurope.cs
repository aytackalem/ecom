﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using ECommerce.Application.Common.Extensions;

/*
 * Status
 * 3: Waiting
 * 4: UnSupplied
 * 5: Package Created
 */

namespace ECommerce.Infrastructure.Marketplaces.TrendyolEurope
{
    public class TrendyolEurope : MarketplaceBase<TrendyolEuropeConfiguration>
    {
        #region Constants
        private const string _baseUrl = "https://api.trendyol.com/gpgw";
        #endregion

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public TrendyolEurope(IServiceProvider serviceProvider, IHttpHelper httpHelper) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.TrendyolEurope;
        #endregion

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>() };

                //var authorization = this._configuration.Authorization;
                //var size = 20;
                //var preUrl = $"{_baseUrl}/v1/{this._configuration.SupplierId}/orders?{size}&startDate={DateTime.Now.AddDays(-3).Date:yyyy-MM-dd}T00:00:00&endDate={DateTime.Now.Date:yyyy-MM-dd}T23:59:59";

                //var pageKey = string.Empty;
                //do
                //{
                //    var url = preUrl;
                //    if (!string.IsNullOrEmpty(pageKey))
                //        url += $"&pageKey={pageKey}";

                //    var orderResponse = _httpHelper.Get<Responses.OrderResponse>(url, "Basic", authorization);
                //    if (orderResponse.items?.Length > 0)
                //        foreach (var theItem in orderResponse.items)
                //            foreach (var theLine in theItem.lines.Where(l => l.status.id == 4 || l.status.id == 6))
                //                MapOrder(response, theItem, theLine);

                //    pageKey = orderResponse.pageKey;
                //} while (!string.IsNullOrEmpty(pageKey));

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>() };

                var creationStartDate = DateTime.Now.AddDays(-3).ConvertToUnixTimeStamp();
                var creationEndDate = DateTime.Now.ConvertToUnixTimeStamp();

                var authorization = this._configuration.Authorization;

                var pendingPreUrl = $"{_baseUrl}/v3/{this._configuration.SupplierId}/packages?size=50&status=pending&creationStartDate={creationStartDate}&creationEndDate={creationEndDate}";

                var page = 1;
                var totalPageCount = 1;
                do
                {
                    var url = pendingPreUrl;
                    url += $"&page={page}";

                    var orderResponse = _httpHelper.Get<Responses.OrderResponseV2>(url, "Basic", authorization);
                    if (orderResponse?.items?.Length > 0)
                    {
                        foreach (var item in orderResponse.items)
                        {
                            MapOrderPending(response, item);
                        }
                    }

                    totalPageCount = orderResponse.totalPageCount;

                    page++;
                } while (page <= totalPageCount);

                var newPreUrl = $"{_baseUrl}/v3/{this._configuration.SupplierId}/packages?size=50&status=new";

                page = 1;
                totalPageCount = 1;
                do
                {
                    var url = newPreUrl;
                    url += $"&page={page}";

                    var orderResponse = _httpHelper.Get<Responses.OrderResponseV2>(url, "Basic", authorization);
                    if (orderResponse?.items?.Length > 0)
                    {
                        foreach (var item in orderResponse.items)
                        {
                            MapOrderNew(response, item);
                        }
                    }

                    totalPageCount = orderResponse.totalPageCount;

                    page++;
                } while (page <= totalPageCount);

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        private void MapOrderPending(DataResponse<OrderResponse> response, Responses.ItemV2 item)
        {
            response.Data.Orders.Add(new()
            {
                BulkInvoicing = true,
                MarketplaceId = this.MarketplaceId,
                OrderCode = item.itemId,
                OrderSourceId = OrderSources.Pazaryeri,
                OrderTypeId = OrderTypes.OnaylanmisSiparis,
                OrderDate = item.creationDate.ConvertToDateTime(),
                OrderShipment = new()
                {
                    PackageNumber = item.boutiqueId.ToString(),
                    TrackingCode = item.trackingNumber.ToString(),
                    ShipmentTypeId = "PND",
                    TrackingUrl = string.Empty,
                    ShipmentCompanyId = "TYFT"
                },
                Customer = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Mail = string.Empty,
                    TaxNumber = string.Empty,
                    Phone = string.Empty
                },
                OrderDeliveryAddress = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Country = "Türkiye",
                    Email = string.Empty,
                    Phone = string.Empty,
                    City = "İstanbul",
                    District = "",
                    Neighborhood = "",
                    Address = string.Empty
                },
                OrderInvoiceAddress = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Email = string.Empty,
                    Phone = string.Empty,
                    City = "İstanbul",
                    District = "",
                    Neighborhood = "",
                    Address = string.Empty,
                    TaxNumber = string.Empty,
                    TaxOffice = string.Empty
                },
                CurrencyId = "EUR",
                OrderDetails = new()
                {
                    new()
                    {
                        UId = item.itemId,
                        UnitPrice =Convert.ToDecimal( item.unitBuyingPrice),
                        ListPrice = Convert.ToDecimal( item.unitBuyingPrice),
                        UnitDiscount = 0M,
                        Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                        {
                            Barcode = item.barcode.ToUpper()
                        },
                        Quantity = item.pendingQuantity
                    }
                },
                Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>
                {
                    new Application.Common.Wrappers.Marketplaces.Order.Payment
                    {
                        Amount =Convert.ToDecimal( item.pendingQuantity * item.unitBuyingPrice),
                        CurrencyId = "EUR",
                        Date = item.creationDate.ConvertToDateTime(),
                        Paid = true,
                        PaymentTypeId = PaymentTypes.OnlineOdeme
                    }
                },
                TotalAmount = Convert.ToDecimal(item.pendingQuantity * item.unitBuyingPrice),
                ListTotalAmount = Convert.ToDecimal(item.pendingQuantity * item.unitBuyingPrice)
            });
        }

        private void MapOrderNew(DataResponse<OrderResponse> response, Responses.ItemV2 item)
        {
            response.Data.Orders.Add(new()
            {
                BulkInvoicing = true,
                MarketplaceId = this.MarketplaceId,
                OrderCode = item.itemId,
                OrderSourceId = OrderSources.Pazaryeri,
                OrderTypeId = OrderTypes.OnaylanmisSiparis,
                OrderDate = item.creationDate.ConvertToDateTime(),
                OrderShipment = new()
                {
                    PackageNumber = item.boutiqueId.ToString(),
                    TrackingCode = null,
                    ShipmentTypeId = "PND",
                    TrackingUrl = string.Empty,
                    ShipmentCompanyId = "TYFT"
                },
                Customer = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Mail = string.Empty,
                    TaxNumber = string.Empty,
                    Phone = string.Empty
                },
                OrderDeliveryAddress = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Country = "Türkiye",
                    Email = string.Empty,
                    Phone = string.Empty,
                    City = "İstanbul",
                    District = "",
                    Neighborhood = "",
                    Address = string.Empty
                },
                OrderInvoiceAddress = new()
                {
                    FirstName = "Trendyol",
                    LastName = "Europe",
                    Email = string.Empty,
                    Phone = string.Empty,
                    City = "İstanbul",
                    District = "",
                    Neighborhood = "",
                    Address = string.Empty,
                    TaxNumber = string.Empty,
                    TaxOffice = string.Empty
                },
                CurrencyId = "EUR",
                OrderDetails = new()
                {
                    new()
                    {
                        UId = item.itemId,
                        UnitPrice =Convert.ToDecimal(item.unitBuyingPrice),
                        ListPrice = Convert.ToDecimal(item.unitBuyingPrice),
                        UnitDiscount = 0M,
                        Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                        {
                            Barcode = item.barcode.ToUpper()
                        },
                        Quantity = item.newQuantity
                    }
                },
                Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>
                {
                    new Application.Common.Wrappers.Marketplaces.Order.Payment
                    {
                        Amount =Convert.ToDecimal( item.pendingQuantity * item.unitBuyingPrice),
                        CurrencyId = "EUR",
                        Date = item.creationDate.ConvertToDateTime(),
                        Paid = true,
                        PaymentTypeId = PaymentTypes.OnlineOdeme
                    }
                },
                TotalAmount = Convert.ToDecimal(item.newQuantity * item.unitBuyingPrice),
                ListTotalAmount = Convert.ToDecimal(item.newQuantity * item.unitBuyingPrice)
            });
        }

        protected override void BindConfigurationConcreate(TrendyolEuropeConfiguration configuration, List<MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
            configuration.SupplierId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "SupplierId").Value;
        }

        protected override DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return new DataResponse<List<KeyValue<string, string>>>();
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            return new DataResponse<CheckPreparingResponse>();
        }

        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return new DataResponse<List<CategoryResponse>>();
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse>() { Success = false };
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return new DataResponse<List<ProductItem>> { Success = false };
        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return new DataResponse<List<MarketplaceProductStatus>> { Success = false };
        }

        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return new DataResponse<OrderResponse>() { Success = false };
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse>() { Success = false };
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return new DataResponse<OrderTypeUpdateResponse>();
        }

        protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return new Response { Success = true };
        }

        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return new DataResponse<string>();
        }


    }
}
