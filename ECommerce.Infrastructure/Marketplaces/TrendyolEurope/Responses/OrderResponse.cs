﻿using System;

namespace ECommerce.Infrastructure.Marketplaces.TrendyolEurope.Responses
{

    /* v1 */

    public class OrderResponse
    {
        public string pageKey { get; set; }
        public Item[] items { get; set; }
    }

    public class Item
    {
        public int sellerId { get; set; }
        public DateTime creationDate { get; set; }
        public DateTime lastModifiedDate { get; set; }
        public string orderNumber { get; set; }
        public string trackingNo { get; set; }
        public Line[] lines { get; set; }
        public int partnerId { get; set; }
        public bool isCancellable { get; set; }
    }

    public class Line
    {
        public string id { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
        public decimal buyingPrice { get; set; }
        public string currency { get; set; }
        public Status status { get; set; }
        public string trackingNo { get; set; }
        public long? boutiqueId { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    /* v2 */


    public class OrderResponseV2
    {
        public ItemV2[] items { get; set; }
        public int page { get; set; }
        public int size { get; set; }
        public int totalItemCount { get; set; }
        public int totalPageCount { get; set; }
    }

    public class ItemV2
    {
        public string itemId { get; set; }
        public int? trackingNumber { get; set; }
        public string packageId { get; set; }
        public long boutiqueId { get; set; }
        public object[] cargos { get; set; }
        public string currency { get; set; }
        public int platformId { get; set; }
        public string barcode { get; set; }
        public string sellerBarcode { get; set; }
        public long creationDate { get; set; }
        public long lastModifiedDate { get; set; }
        public string name { get; set; }
        public int newQuantity { get; set; }
        public int pendingQuantity { get; set; }
        public int unSuppliedQuantity { get; set; }
        public int cancelledQuantity { get; set; }
        public int completedQuantity { get; set; }
        public float unitBuyingPrice { get; set; }
        public string status { get; set; }
    }


}
