﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.TrendyolEurope
{
    public class TrendyolEuropeConfiguration : UsernamePasswordConfigurationBase
    {
        #region Properties
        public string SupplierId { get; set; }

        public string BoutiqueId { get; set; }
        #endregion
    }
}
