﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Enums;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Request.OrderStatus;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Request.Shipment;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Request.Token;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Category;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.CategoryAttribute;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.OrderStatus;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Shipment;
using ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone
{
    /// <summary>
    /// Vodafone pazaryerini temsil eden sınıf. Bearer authentication ile çalışır.
    /// </summary>
    public class Vodafone : TokenMarketplaceBase<VodafoneConfiguration>
    {
        #region Constants
        private const string _baseUrl = "https://vfmallapi.vodafone.com.tr/vfmallapi";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Vodafone(IHttpHelper httpHelper, IServiceProvider serviceProvider, ICacheHandler cacheHandler) : base(serviceProvider, cacheHandler, 9)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Vodafone;
        #endregion

        #region Methods
        #region Product Methods
        protected override Application.Common.Wrappers.DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            throw new NotImplementedException();
        }

        protected override Application.Common.Wrappers.DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            throw new NotImplementedException();
        }

        protected override Application.Common.Wrappers.DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                response.Data = new List<CategoryResponse>();

                var page = 0;
                var size = 100;
                var categoryUrl = $"{_baseUrl}/getVfMallCategories?page={page}&size={size}";

                VodafoneCategoryResponse vodafoneCategoryResponse = null;
                do
                {
                    vodafoneCategoryResponse = this
                        ._httpHelper
                        .Get<VodafoneCategoryResponse>(categoryUrl, "Bearer", this.GetAccessToken().Data);

                    if (vodafoneCategoryResponse.Result.Result.ToLower() == "success" &&
                        vodafoneCategoryResponse.Result.ResultCode == "0")
                    {
                        vodafoneCategoryResponse.GetCategories.Categories.ForEach(c =>
                        {
                            if (c.Name.Contains("Güneş Koruyucuları") || c.Name.Contains("Yüz Nemlendiriciler") || c.Name.Contains("Vücut Nemlendirici Krem, Losyon"))
                                response.Data.Add(new CategoryResponse
                                {
                                    Code = c.Id.ToString(),
                                    Name = c.Name,
                                    ParentCategoryCode = c.ParentId,
                                });
                        });
                    }

                    page++;
                    categoryUrl = $"{_baseUrl}/getVfMallCategories?page={page}&size={size}";

                    if (response.Data.Count == 3)
                    {
                        break;
                    }
                } while (vodafoneCategoryResponse.GetCategories.TotalPages > page);

                response.Data.ForEach(d =>
                {
                    var categoryAttributeUrl = $"{_baseUrl}/getVfMallCategoryAttributes?id={d.Code}";
                    var vodafoneCategoryAttributeResponse = this
                        ._httpHelper
                        .Get<VodafoneCategoryAttributeResponse>(categoryAttributeUrl, "Bearer", this.GetAccessToken().Data);

                    if (vodafoneCategoryAttributeResponse.Result.Result.ToLower() == "success" &&
                        vodafoneCategoryAttributeResponse.Result.ResultCode == "0")
                    {
                        d.CategoryAttributes = vodafoneCategoryAttributeResponse
                            .ExtensionList
                            .First()
                            .Parameters
                            .Select(p => new CategoryAttributeResponse
                            {
                                Code = p.Code,
                                CategoryCode = d.Code,
                                Variantable = p.IsVariant,
                                MultiValue = false,
                                Mandatory = false,
                                AllowCustom = false,
                                Name = p.Name,
                                CategoryAttributesValues = p
                                    .Value
                                    .Select(v => new CategoryAttributeValueResponse
                                    {
                                        VarinatCode = p.Code,
                                        Code = v.GenerateUrl(),
                                        Name = v
                                    })
                                    .ToList()
                            })
                            .ToList();
                    }
                });

                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            OrderResponse response = new() { Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>() };

            var vodafoneOrders = new List<VodafoneOrder>();

            #region Fetch Data
            var page = 0;

            VodafoneOrderResponse vodafoneOrderResponse = null;

            do
            {
                var url = $"{_baseUrl}/getVfMallOrders?status=PENDING&shipmentStatus=UNPACKED&page={page}";

                vodafoneOrderResponse = _httpHelper.Get<VodafoneOrderResponse>(url, "Bearer", this.GetAccessToken().Data);

                if (vodafoneOrderResponse != null &&
                    vodafoneOrderResponse.Result.Result.ToLower() == "success" &&
                    vodafoneOrderResponse.Result.ResultCode == "0")
                {
                    vodafoneOrders.AddRange(vodafoneOrderResponse.Orders);
                }

                page++;
            } while (vodafoneOrderResponse?.totalPages > page);
            #endregion

            /*
             * Aynı sepet (sipariş) numarasına sahip satırlar gruplanarak gönderileceğinden dolayı sepet (sipariş) numaraları 
             * unique halde alınır. Yoğunluk anında problem olmaması açısından ilk 100 sepet (sipariş) numarası alınır.
             */
            var shoppingCartIds = vodafoneOrders.Select(vo => vo.shoppingCartId).Distinct().ToList();

            Parallel.ForEach(shoppingCartIds, theShoppingCartId =>
            {
                Console.WriteLine(theShoppingCartId);

                var shoppingCartItems = vodafoneOrders.Where(vo => vo.shoppingCartId == theShoppingCartId).ToList();

                var delivery = shoppingCartItems.First().customerContactInfoList.FirstOrDefault(ccil => ccil.type == "DELIVERY");
                var invoice = shoppingCartItems.First().customerContactInfoList.FirstOrDefault(ccil => ccil.type == "INVOICE");
                var trackingCode = "";
                var shipmentCompanyId = "";

                var order = new Application.Common.Wrappers.Marketplaces.Order.Order
                {
                    OrderSourceId = OrderSources.Pazaryeri,
                    MarketplaceId = Application.Common.Constants.Marketplaces.Vodafone,
                    TotalAmount = shoppingCartItems.Sum(x => x.salePrice),
                    ListTotalAmount = shoppingCartItems.Sum(x => x.listPrice),
                    Discount = shoppingCartItems.Sum(x => x.discount),
                    OrderTypeId = "OS",
                    OrderPicking = new OrderPicking { MarketPlaceOrderId = theShoppingCartId },
                    OrderCode = theShoppingCartId,
                    OrderDate = DateTime.Parse(shoppingCartItems.First().created.Value),
                    OrderShipment = new OrderShipment
                    {
                        TrackingCode = trackingCode,
                        ShipmentTypeId = "PND",
                        ShipmentCompanyId = shipmentCompanyId
                    },
                    Customer = new Customer
                    {
                        FirstName = invoice.FullName.Split(" ")[0],
                        LastName = invoice.FullName.Split(" ")[1],
                        Mail = invoice.Email,
                        Phone = invoice.PhoneNumber
                    },
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        FirstName = delivery.FullName.Split(" ")[0],
                        LastName = delivery.FullName.Split(" ")[1],
                        Address = delivery.address,
                        City = delivery.City,
                        District = delivery.Town,
                        Email = delivery.Email,
                        Phone = delivery.PhoneNumber
                    },
                    OrderInvoiceAddress = new OrderInvoiceAddress
                    {
                        FirstName = invoice.FullName.Split(" ")[0],
                        LastName = invoice.FullName.Split(" ")[1],
                        Address = invoice.address,
                        City = invoice.City,
                        District = invoice.Town,
                        Email = invoice.Email,
                        Phone = invoice.PhoneNumber
                    },
                    OrderDetails = shoppingCartItems
                        .GroupBy(sci => sci.barcode)
                        .Select(gsci => new OrderDetail
                        {
                            UId = gsci.First().id,
                            TaxRate = Convert.ToDouble(gsci.First().vatRate) / 100,
                            UnitPrice = gsci.First().salePrice,
                            ListPrice = gsci.First().listPrice,
                            UnitDiscount = gsci.First().discount,
                            Product = new Product()
                            {
                                Barcode = gsci.First().barcode.ToUpper(),
                                Name = gsci.First().offeringName,
                                StockCode = gsci.First().stockCode
                            },
                            Quantity = gsci.Count()
                        })
                        .ToList()
                };

                order.UId = $"{Application.Common.Constants.Marketplaces.Vodafone}_{theShoppingCartId}";

                response.Orders.Add(order);
            });

            return new DataResponse<OrderResponse>
            {
                Success = true,
                Data = response
            };
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderTypeUpdateResponse>>((response) =>
            {
                response.Data = new();

                /*
                 * Satırlar hazırlanıyor statüsüne güncelleniyor.
                 */
                orderTypeUpdateRequest.Order.OrderDetails.ForEach(od =>
                {
                    this._httpHelper.Post<VodafoneOrderStatusRequest, VodafoneOrderStatusResponse>(
                        new VodafoneOrderStatusRequest
                        {
                            orderId = od.UId,
                            newStatus = "PREPARING"
                        },
                        $"{_baseUrl}/v2/updateVfMallOrderStatus",
                        "Bearer",
                        this.GetAccessToken().Data, null, null);
                });

                /*
                 * Satırlar aynı paketin içerisine konuluyor.
                 */
                var shipmentResponse = this
                    ._httpHelper
                    .Post<VodafoneShipmentRequest, VodafoneShipmentResponse>(new VodafoneShipmentRequest
                    {
                        orderIds = orderTypeUpdateRequest.Order.OrderDetails.Select(x => x.UId).ToList()
                    },
                    $"{_baseUrl}/v2/shipVfMallOrders",
                    "Bearer",
                    this.GetAccessToken().Data, null, null);

                var trackingCode = shipmentResponse.shipmentRefNoList[0];
                var shipmentCompanyId = this.FindShipmentCompanyId(shipmentResponse.shipmentCompanyList[0]);

                response.Data.ShipmentCompanyId = shipmentCompanyId;
                response.Data.TrackingCode = trackingCode;
                response.Success = true;
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            OrderResponse response = new() { Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>() };

            var vodafoneOrders = new List<VodafoneOrder>();

            #region Fetch Data
            var page = 0;

            VodafoneOrderResponse vodafoneOrderResponse = null;

            do
            {
                var url = $"{_baseUrl}/getVfMallOrders?status=PREPARING&shipmentStatus=PACKED&page={page}";

                vodafoneOrderResponse = _httpHelper.Get<VodafoneOrderResponse>(url, "Bearer", this.GetAccessToken().Data);

                if (vodafoneOrderResponse != null &&
                    vodafoneOrderResponse.Result.Result.ToLower() == "success" &&
                    vodafoneOrderResponse.Result.ResultCode == "0")
                {
                    vodafoneOrders.AddRange(vodafoneOrderResponse.Orders);
                }

                page++;
            } while (vodafoneOrderResponse?.totalPages > page);
            #endregion

            /*
             * Aynı sepet (sipariş) numarasına sahip satırlar gruplanarak gönderileceğinden dolayı sepet (sipariş) numaraları 
             * unique halde alınır. Yoğunluk anında problem olmaması açısından ilk 100 sepet (sipariş) numarası alınır.
             */
            var shoppingCartIds = vodafoneOrders.Select(vo => vo.shoppingCartId).Distinct().Take(100).ToList();

            Parallel.ForEach(shoppingCartIds, theShoppingCartId =>
            {
                Console.WriteLine(theShoppingCartId);

                var shoppingCartItems = vodafoneOrders.Where(vo => vo.shoppingCartId == theShoppingCartId).ToList();

                /*
                 * Satırlar hazırlanıyor statüsüne güncelleniyor.
                 */
                shoppingCartItems.ForEach(theShoppingCartItem =>
                {
                    this._httpHelper.Post<VodafoneOrderStatusRequest, VodafoneOrderStatusResponse>(
                        new VodafoneOrderStatusRequest
                        {
                            orderId = theShoppingCartItem.id,
                            newStatus = "PREPARING"
                        },
                        $"{_baseUrl}/v2/updateVfMallOrderStatus",
                        "Bearer",
                        this.GetAccessToken().Data, null, null);
                });

                /*
                 * Satırlar aynı paketin içerisine konuluyor.
                 */
                var shipmentResponse = this
                    ._httpHelper
                    .Post<VodafoneShipmentRequest, VodafoneShipmentResponse>(new VodafoneShipmentRequest
                    {
                        orderIds = shoppingCartItems.Select(x => x.id).ToList()
                    },
                    $"{_baseUrl}/v2/shipVfMallOrders",
                    "Bearer",
                    this.GetAccessToken().Data, null, null);

                var delivery = shipmentResponse.customerContactInfoList.FirstOrDefault(ccil => ccil.type == "DELIVERY");
                var invoice = shipmentResponse.customerContactInfoList.FirstOrDefault(ccil => ccil.type == "INVOICE");
                var trackingCode = shipmentResponse.shipmentRefNoList[0];
                var shipmentCompanyId = this.FindShipmentCompanyId(shipmentResponse.shipmentCompanyList[0]);

                var order = new Application.Common.Wrappers.Marketplaces.Order.Order
                {
                    OrderSourceId = OrderSources.Pazaryeri,
                    MarketplaceId = Application.Common.Constants.Marketplaces.Vodafone,
                    TotalAmount = shoppingCartItems.Sum(x => x.salePrice),
                    ListTotalAmount = shoppingCartItems.Sum(x => x.listPrice),
                    Discount = shoppingCartItems.Sum(x => x.discount),
                    OrderTypeId = "OS",
                    OrderPicking = new OrderPicking { MarketPlaceOrderId = theShoppingCartId },
                    OrderCode = theShoppingCartId,
                    OrderDate = DateTime.Parse(shoppingCartItems.First().created.Value),
                    OrderShipment = new OrderShipment
                    {
                        TrackingCode = trackingCode,
                        ShipmentTypeId = "PND",
                        ShipmentCompanyId = shipmentCompanyId
                    },
                    Customer = new Customer
                    {
                        FirstName = invoice.fullName.Split(" ")[0],
                        LastName = invoice.fullName.Split(" ")[1],
                        Mail = invoice.email,
                        Phone = invoice.phoneNumber
                    },
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        FirstName = delivery.fullName.Split(" ")[0],
                        LastName = delivery.fullName.Split(" ")[1],
                        Address = delivery.address,
                        City = delivery.city,
                        District = delivery.town,
                        Email = delivery.email,
                        Phone = delivery.phoneNumber
                    },
                    OrderInvoiceAddress = new OrderInvoiceAddress
                    {
                        FirstName = invoice.fullName.Split(" ")[0],
                        LastName = invoice.fullName.Split(" ")[1],
                        Address = invoice.address,
                        City = invoice.city,
                        District = invoice.town,
                        Email = invoice.email,
                        Phone = invoice.phoneNumber
                    },
                    OrderDetails = shoppingCartItems
                        .GroupBy(sci => sci.barcode)
                        .Select(gsci => new OrderDetail
                        {
                            TaxRate = Convert.ToDouble(gsci.First().vatRate) / 100,
                            UnitPrice = gsci.First().salePrice,
                            ListPrice = gsci.First().listPrice,
                            UnitDiscount = gsci.First().discount,
                            Product = new Product()
                            {
                                Barcode = gsci.First().barcode.ToUpper(),
                                Name = gsci.First().offeringName,
                                StockCode = gsci.First().stockCode
                            },
                            Quantity = gsci.Count()
                        })
                        .ToList()
                };

                response.Orders.Add(order);
            });

            return new DataResponse<OrderResponse>
            {
                Data = response,
                Success = true
            };
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }

        protected override Application.Common.Wrappers.Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            _httpHelper.Post<VodafoneOrderStatusRequest, VodafoneOrderStatusResponse>(new VodafoneOrderStatusRequest
            {
                newStatus = request.Status == MarketplaceStatus.Picking ? "PREPARING" : "COMPLETED",
                orderId = request.OrderPicking.MarketPlaceOrderId
            }, $"{_baseUrl}/v2/updateVfMallOrderStatus", "Bearer", this.GetAccessToken().Data, null, null);

            return new Application.Common.Wrappers.Response { };
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() }, Success = true };
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();
                response.Success = true;
            });
        }
        #endregion

        protected override Application.Common.Wrappers.Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(VodafoneConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            //configuration.IntegratorCode = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "IntegratorCode").Value;
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }

        protected override String GetAccessTokenConcreate()
        {
            var tokenResponse = this._httpHelper.Post<TokenRequest, TokenResponse>(new TokenRequest
            {
                integratorCode = this._configuration.IntegratorCode,
                password = this._configuration.Password,
                username = this._configuration.Username
            }, $"{_baseUrl}/createVfMallToken", null, null, null, null);

            return tokenResponse.token.Replace("bearer ", "");
        }
        #endregion

        #region Helper Methods
        //private TokenResponse GetToken()
        //{
        //    var tokenResponse = this._httpHelper.Post<TokenRequest, TokenResponse>(new TokenRequest
        //    {
        //        //integratorCode = null,
        //        //password = "SinozZ!2022+noz&",
        //        //username = "pazaryeri@sinoz.com.tr"
        //        integratorCode = this._configuration.IntegratorCode,
        //        password = this._configuration.Password,
        //        username = this._configuration.Username
        //    }, $"{_baseUrl}/createVfMallToken", null, null);

        //    tokenResponse.token = tokenResponse.token.Replace("bearer ", "");

        //    return tokenResponse;
        //}

        private string FindShipmentCompanyId(string cargoCompanyId)
        {
            switch (cargoCompanyId)
            {
                case "2":
                    return ShipmentCompanies.ArasKargo;
                case "Hepsi Jet":
                    return ShipmentCompanies.HepsiJet;
                default:
                    throw new Exception("Unsupported shipment company.");
            }
        }


        #endregion
    }
}