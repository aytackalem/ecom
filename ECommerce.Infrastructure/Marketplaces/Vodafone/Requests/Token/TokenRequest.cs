﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Request.Token
{
    public class TokenRequest
    {
        #region Properties
        public string username { get; set; }

        public string password { get; set; }

        public string integratorCode { get; set; }
        #endregion
    }
}
