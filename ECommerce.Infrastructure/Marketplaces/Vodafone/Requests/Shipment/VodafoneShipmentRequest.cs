﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Request.Shipment
{
    public class VodafoneShipmentRequest
    {
        #region Properties
        public List<string> orderIds { get; set; }
        #endregion
    }
}
