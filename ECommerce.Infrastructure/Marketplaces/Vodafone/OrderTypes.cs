﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone
{
    public enum OrderTypes
    {
        #region Members
        PENDING,

        PREPARING,
        
        DELIVERED_TO_CUSTOMER, 
        
        NOT_DELIVERED_TO_CUSTOMER, 
        
        RETURN_REQUESTED,
        
        SHIPPED_TO_DEALER,

        DELIVERED_TO_DEALER, 
        
        NOT_DELIVERED_TO_DEALER, 
        
        RETURN_ACCEPTED,
        
        RETURN_PENDING,
        
        RETURN_DENIED,
        
        REFUNDED,
        
        CANCELLED,
        
        COMPLETED
        #endregion
    }
}
