﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Token
{
    public class TokenResponse : ResponseBase
    {
        public string username { get; set; }
        public string token { get; set; }
        public string expireDate { get; set; }
    }
}
