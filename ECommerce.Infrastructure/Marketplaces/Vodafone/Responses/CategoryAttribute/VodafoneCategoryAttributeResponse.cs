﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.CategoryAttribute
{
    public class VodafoneCategoryAttributeResponse : ResponseBase
    {
        #region Properties
        public List<VodafoneExtensionList> ExtensionList { get; set; }
        #endregion
    }
}
