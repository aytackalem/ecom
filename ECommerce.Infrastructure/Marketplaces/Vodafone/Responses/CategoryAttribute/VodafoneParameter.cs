﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.CategoryAttribute
{
    public class VodafoneParameter
    {
        #region Properties
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public object CommercialName { get; set; }
        
        public string Code { get; set; }
        
        public object ChoiceType { get; set; }
        
        public object ValueType { get; set; }
        
        public List<string> Value { get; set; }
        
        public string ParameterTypeCode { get; set; }
        
        public bool IsVariant { get; set; }
        #endregion
    }
}
