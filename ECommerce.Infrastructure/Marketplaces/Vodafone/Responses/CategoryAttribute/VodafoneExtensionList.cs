﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.CategoryAttribute
{
    public class VodafoneExtensionList
    {
        #region Properties
        public List<VodafoneParameter> Parameters { get; set; }
        #endregion
    }
}
