﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders
{
    public class CustomerContactInfo
    {
        #region Properties
        public string FullName { get; set; }
        
        public string Town { get; set; }
        
        public string City { get; set; }
        
        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public object postCode { get; set; }
        
        public string address { get; set; }
        
        public string type { get; set; } 
        #endregion
    }
}
