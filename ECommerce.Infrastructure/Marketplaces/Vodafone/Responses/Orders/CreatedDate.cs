﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders
{
    public class CreatedDate
    {
        #region Properties
        public string Value { get; set; } 
        #endregion
    }
}
