﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders
{
    public class Date
    {
        public string Value { get; set; }
    }
}
