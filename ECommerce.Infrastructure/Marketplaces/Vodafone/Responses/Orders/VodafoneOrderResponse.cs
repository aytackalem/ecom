﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders
{
    public class VodafoneOrderResponse : ResponseBase
    {
        #region Properties
        [JsonProperty("listSalesOrders")]
        public List<VodafoneOrder> Orders { get; set; }

        public int currentPage { get; set; }

        public int totalPages { get; set; }

        public int totalItems { get; set; }
        #endregion
    }
}
