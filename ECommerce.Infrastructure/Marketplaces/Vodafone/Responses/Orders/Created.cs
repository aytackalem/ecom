﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Orders
{
    public class Created
    {
        #region Properties
        public string Value { get; set; } 
        #endregion
    }
}
