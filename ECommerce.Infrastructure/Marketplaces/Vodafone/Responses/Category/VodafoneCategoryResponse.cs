﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Category
{
    public class VodafoneCategoryResponse : ResponseBase
    {
        #region Properties
        public VodafoneGetCategories GetCategories { get; set; }
        #endregion
    }
}
