﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Category
{
    public class VodafoneGetCategories
    {
        #region Properties
        public List<VodafoneCategory> Categories { get; set; }
        
        public int CurrentPage { get; set; }
        
        public int TotalPages { get; set; }
        
        public int TotalItems { get; set; }
        #endregion
    }
}
