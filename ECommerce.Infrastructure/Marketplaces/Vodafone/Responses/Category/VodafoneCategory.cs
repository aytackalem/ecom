﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Category
{
    public class VodafoneCategory
    {
        #region Properties
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public bool IsMainCategory { get; set; }
        
        public bool HasSubCategory { get; set; }
        
        public string ParentId { get; set; }
        #endregion
    }
}
