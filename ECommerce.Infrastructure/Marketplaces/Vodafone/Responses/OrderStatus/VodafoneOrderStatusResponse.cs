﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.OrderStatus
{
    public class Created
    {
        public string value { get; set; }
    }

    public class CustomerContactInfo
    {
        public string fullName { get; set; }
        public string town { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public object postCode { get; set; }
        public string address { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public string shoppingCartId { get; set; }
        public string dealerId { get; set; }
        public string offeringId { get; set; }
        public string offeringName { get; set; }
        public string status { get; set; }
        public bool isPacked { get; set; }
        public object lastShipmentRefNo { get; set; }
        public List<object> shipmentInfo { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public Created created { get; set; }
        public Updated updated { get; set; }
        public string barcode { get; set; }
        public string listPrice { get; set; }
        public string salePrice { get; set; }
        public string cargoCompanyId { get; set; }
        public string stockCode { get; set; }
        public string deliveryDuration { get; set; }
        public string vatRate { get; set; }
        public int quantity { get; set; }
        public object createDate { get; set; }
        public object updateDate { get; set; }
        public string discount { get; set; }
        public string cargoPaymentFlag { get; set; }
        public bool isDigital { get; set; }
        public object statusHistory { get; set; }
        public CustomerContactInfo customerContactInfo { get; set; }
    }

    public class Result
    {
        public string result { get; set; }
        public string resultCode { get; set; }
        public string resultDesc { get; set; }
    }

    public class VodafoneOrderStatusResponse : ResponseBase
    {
        public Order order { get; set; }
    }

    public class Updated
    {
        public string value { get; set; }
    }


}
