﻿using ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Common.Response.Shipment
{
    public class VodafoneShipmentResponse : ResponseBase
    {
        public List<string> shipmentRefNoList { get; set; }
        public List<CustomerContactInfoList> customerContactInfoList { get; set; }
        public List<string> shipmentCompanyList { get; set; }
        public List<object> failedPackagesList { get; set; }
    }
}
