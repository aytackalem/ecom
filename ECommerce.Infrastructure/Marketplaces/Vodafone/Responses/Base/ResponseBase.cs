﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base
{
    public abstract class ResponseBase
    {
        public ResultObj Result { get; set; }
    }
}
