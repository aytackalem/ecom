﻿namespace ECommerce.Infrastructure.Marketplaces.Vodafone.Response.Base
{
    public class ResultObj
    {
        #region Properties
        public string Result { get; set; }

        public string ResultCode { get; set; }

        public string ResultDesc { get; set; }
        #endregion
    }
}
