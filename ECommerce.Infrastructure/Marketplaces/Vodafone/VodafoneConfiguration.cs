﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.Vodafone
{
    public class VodafoneConfiguration: UsernamePasswordConfigurationBase
    {
        #region Properties
        public string IntegratorCode { get; set; }
        #endregion
    }
}