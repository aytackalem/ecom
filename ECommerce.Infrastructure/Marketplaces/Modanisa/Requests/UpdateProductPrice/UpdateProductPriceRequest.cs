﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductPrice
{
    public class UpdateProductPriceRequest
    {
        public List<Product> Products { get; set; }
    }
}
