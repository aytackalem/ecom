﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductPrice
{
    public class Product
    {
        public string variant_id { get; set; }
        public string price { get; set; }
        public string old_price { get; set; }
        public string barcode { get; set; }
    }
}
