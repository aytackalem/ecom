﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductStock
{
    public class Product
    {
        public string variant_id { get; set; }
        public int quantity { get; set; }
        public string barcode { get; set; }
    }
}
