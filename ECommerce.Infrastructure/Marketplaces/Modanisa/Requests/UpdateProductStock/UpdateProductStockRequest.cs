﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductStock
{
    public class UpdateProductStockRequest
    {
        public List<Product> Products { get; set; }
    }
}
