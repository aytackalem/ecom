﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetFabrics
{
    public class FabricsResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }

    public class Data
    {
        public Fabric[] fabrics { get; set; }
        public Pagination pagination { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int total_page { get; set; }
        public int count { get; set; }
        public int limit { get; set; }
    }

    public class Fabric
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}
