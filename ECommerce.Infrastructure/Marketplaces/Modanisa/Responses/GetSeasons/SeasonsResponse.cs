﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetSeasons
{
    public class SeasonsResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }

    public class Data
    {
        public Season[] seasons { get; set; }
        public Pagination pagination { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int total_page { get; set; }
        public int count { get; set; }
        public int limit { get; set; }
    }

    public class Season
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
