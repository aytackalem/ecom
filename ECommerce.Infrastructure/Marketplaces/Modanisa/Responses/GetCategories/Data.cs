using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetCategories{ 

    public class Data
    {
        public List<Category> categories { get; set; }
    }

}