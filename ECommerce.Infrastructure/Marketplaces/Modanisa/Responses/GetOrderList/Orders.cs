using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList{ 

    public class Orders
    {
        public int totalCount { get; set; }
        public List<Detail> details { get; set; }
    }

}