﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList
{
    public class GetOrderListResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int status_code { get; set; }
        public Data data { get; set; }
    }
}
