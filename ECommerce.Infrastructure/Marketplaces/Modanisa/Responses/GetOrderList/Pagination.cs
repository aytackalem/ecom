using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList{ 

    public class Pagination
    {
        public double page { get; set; }
        public double totalPage { get; set; }
        public double count { get; set; }
        public double limit { get; set; }
    }

}