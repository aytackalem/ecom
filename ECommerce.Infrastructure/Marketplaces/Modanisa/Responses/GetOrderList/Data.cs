using System.Collections.Generic; 
using System; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList{ 

    public class Data
    {
        public DateTimeRange dateTimeRange { get; set; }
        public Orders orders { get; set; }
        public Pagination pagination { get; set; }
        public CompanyInfo companyInfo { get; set; }
    }

}