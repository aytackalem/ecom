using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList{ 

    public class Product
    {
        public string productName { get; set; }
        public string productId { get; set; }
        public string variantId { get; set; }
        public string modanisaProductId { get; set; }
        public string modanisaVariantId { get; set; }
        public string modanisaOrderProductId { get; set; }
        public string price { get; set; }
        public int quantity { get; set; }
        public string barcode { get; set; }
        public string alternative_barcode { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
    }

}