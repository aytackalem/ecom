﻿namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.UpdateProductPrice
{
    public class UpdateProductPriceResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int status_code { get; set; }
        public string batchId { get; set; }
    }
}
