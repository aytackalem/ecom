﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetVariantAttributes
{
    public class GetVariantAttributesResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
        public object errors { get; set; }
    }
}
