using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetVariantAttributes{ 

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<VariantAttribute> variant_attributes { get; set; }
    }

}