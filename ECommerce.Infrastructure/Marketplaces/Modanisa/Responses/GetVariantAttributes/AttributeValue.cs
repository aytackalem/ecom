namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetVariantAttributes{ 

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}