﻿using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.Product
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Data
    {
        public List<Product> products { get; set; }
        public Pagination pagination { get; set; }
    }


    public class Pagination
    {
        public int page { get; set; }
        public int total_page { get; set; }
        public int count { get; set; }
        public int limit { get; set; }
    }

    public class Price
    {
        public double price { get; set; }
        public double priceAlt { get; set; }
    }

    public class Product
    {

        public string product_id { get; set; }
        public int? mdns_product_id { get; set; }
        public string status { get; set; }
        public List<Variant> variants { get; set; }
        public List<Price> prices { get; set; }
    }

    public class ProductResponse
    {
        public bool success { get; set; }
        public Data data { get; set; }
    }

    public class Variant
    {

        public string barcode { get; set; }
        public int quantity { get; set; }
    }



}