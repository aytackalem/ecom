namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetAttributes{ 

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}