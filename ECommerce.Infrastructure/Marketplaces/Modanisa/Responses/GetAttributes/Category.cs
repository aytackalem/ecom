using System.Collections.Generic; 
namespace ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetAttributes{ 

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<Attribute> attributes { get; set; }
    }

}