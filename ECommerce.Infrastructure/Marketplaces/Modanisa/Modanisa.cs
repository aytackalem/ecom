﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductPrice;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Requests.UpdateProductStock;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetAttributes;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetBrand;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetCategories;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetOrderList;
using ECommerce.Infrastructure.Marketplaces.Modanisa.Responses.GetVariantAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Modanisa
{
    public class Modanisa : MarketplaceBase<ModanisaConfiguration>
    {
        #region Constants
        private const string _baseUrl = "https://marketplace.modanisa.com/api/marketplace";

        //private const string _baseUrl = "https://marketplace-stg.modanisa.com/api/marketplace";
        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public Modanisa(IHttpHelper httpHelper, IServiceProvider serviceProvider, ICacheHandler cacheHandler) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Modanisa;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var token = this._configuration.Authorization;

                var updateProductStockResponse = this
                        ._httpHelper
                        .Post<UpdateProductPriceRequest, Responses.UpdateProductPrice.UpdateProductPriceResponse>(new UpdateProductPriceRequest
                        {
                            Products = new List<Requests.UpdateProductPrice.Product>
                            {
                                new Requests.UpdateProductPrice.Product
                                {
                                    barcode = stockRequest.PriceStock.Barcode,
                                    price = stockRequest.PriceStock.SalePrice.ToString(),
                                    old_price = stockRequest.PriceStock.ListPrice.ToString(),
                                }
                            }
                        }, $"{_baseUrl}/updateProductStock", "Basic", token, null, null);
                response.Data = updateProductStockResponse.batchId;
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var token = this._configuration.Authorization;

                var updateProductStockResponse = this
                        ._httpHelper
                        .Post<UpdateProductStockRequest, Responses.UpdateProductStock.UpdateProductStockResponse>(new UpdateProductStockRequest
                        {
                            Products = new List<Requests.UpdateProductStock.Product>
                            {
                                new Requests.UpdateProductStock.Product
                                {
                                    barcode = stockRequest.PriceStock.Barcode,
                                    quantity=stockRequest.PriceStock.Quantity
                                }
                            }
                        }, $"{_baseUrl}/updateProductStock", "Basic", token, null, null);
                response.Data = updateProductStockResponse.batchId;
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new();

                var token = this._configuration.Authorization;

                var page = 1;
                var totalPage = 0;

                do
                {
                    var url = $"{_baseUrl}/products/filter?page={page}";

                    var _ = this._httpHelper.Get<Responses.Product.ProductResponse>(url, "Basic", token);
                    if (_.success)
                    {
                        if (totalPage == 0)
                            totalPage = _.data.pagination.total_page;

                        response.Data.AddRange(_.data.products.Select(p => new ProductItem
                        {
                            StockItems = p
                                .variants
                                .Select(v => new StockItem
                                {
                                    Barcode = v.barcode,
                                    ListPrice = Convert.ToDecimal(p.prices[0].price),
                                    SalePrice = Convert.ToDecimal(p.prices[0].priceAlt == 0F ?
                                        p.prices[0].price :
                                        p.prices[0].priceAlt),
                                    Quantity = v.quantity
                                })
                                .ToList()
                        }));

                        page++;
                    }
                    else
                        break;

                    page++;
                } while (page >= totalPage);
            });
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>((response) =>
            {
                response.Data = new();

                var token = this._configuration.Authorization;

                var page = 1;
                var totalPage = 0;

                do
                {
                    var url = $"{_baseUrl}/products/filter?page={page}";

                    var _ = this._httpHelper.Get<Responses.Product.ProductResponse>(url, "Basic", token);
                    if (_.success)
                    {
                        if (totalPage == 0)
                            totalPage = _.data.pagination.total_page;

                        response.Data.AddRange(_
                            .data
                            .products
                            .SelectMany(p => p
                                .variants
                                .Select(v => new Application.Common.DataTransferObjects.MarketplaceProductStatus
                                {
                                    Barcode = v.barcode,
                                    Opened = p.mdns_product_id.HasValue,
                                    OnSale = p.mdns_product_id.HasValue,
                                    Url = "",
                                    Locked = p.status == "CREATE_PRODUCT_REJECTED",
                                    StockCode = null,
                                    ListUnitPrice = Convert.ToDecimal(p.prices[0].price),
                                    UnitPrice = Convert.ToDecimal(p.prices[0].priceAlt == 0F ?
                                        p.prices[0].price :
                                        p.prices[0].priceAlt),
                                    Stock = v.quantity,
                                    MarketplaceId = "MN"
                                })));

                        page++;
                    }
                    else
                        break;

                } while (totalPage >= page);
                response.Success = true;
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                response.Data = new();

                var token = this._configuration.Authorization;

                var getCategoriesResponse = this
                    ._httpHelper
                    .Get<GetCategoriesResponse>($"{_baseUrl}/categories", "Basic", token);

                foreach (var theCategory in getCategoriesResponse.data.categories)
                {
                    if (theCategory.sub_categories == null)
                    {
                        response.Data.Add(new CategoryResponse
                        {
                            Code = theCategory.id.ToString(),
                            Name = theCategory.name
                        });
                    }
                    else
                        FetchSubCategory(response.Data, theCategory.sub_categories);
                }

                //var fabricUrl = "fabrics?page={0}&size={1}";
                //var fabrics = new List<Fabric>();
                //var fabricPage = 0;
                //var fabricTotalPage = 0;
                //do
                //{
                //    var getFabricsResponse = this
                //        ._httpHelper
                //        .Get<FabricsResponse>($"{_baseUrl}/{string.Format(fabricUrl, fabricPage, 100)}", "Basic", token);

                //    fabricTotalPage = getFabricsResponse.data.pagination.total_page;

                //    fabrics.AddRange(getFabricsResponse.data.fabrics);
                //} while (fabricTotalPage > fabricPage);

                //var originUrl = "origin-countries?page={0}&size={1}";
                //var origins = new List<Origincountry>();
                //var originPage = 1;
                //var originTotalPage = 0;
                //do
                //{
                //    var getOriginsResponse = this
                //        ._httpHelper
                //        .Get<OriginsResponse>($"{_baseUrl}/{string.Format(originUrl, originPage, 100)}", "Basic", token);

                //    originTotalPage = getOriginsResponse.data.pagination.total_page;

                //    origins.AddRange(getOriginsResponse.data.originCountries);

                //    originPage++;
                //} while (originTotalPage >= originPage);

                //var seasonUrl = "seasons?page={0}&size={1}";
                //var seasons = new List<Season>();
                //var seasonPage = 1;
                //var seasonTotalPage = 0;
                //do
                //{
                //    var getSeasonsResponse = this
                //        ._httpHelper
                //        .Get<SeasonsResponse>($"{_baseUrl}/{string.Format(seasonUrl, seasonPage, 100)}", "Basic", token);

                //    seasonTotalPage = getSeasonsResponse.data.pagination.total_page;

                //    seasons.AddRange(getSeasonsResponse.data.seasons);

                //    seasonPage++;
                //} while (seasonTotalPage >= seasonPage);

                response.Data.ForEach(d =>
                {
                    Console.WriteLine(response.Data.IndexOf(d));

                    #region Attributes
                    var getAttributesResponse = this
                        ._httpHelper
                        .Get<GetAttributesResponse>($"{_baseUrl}/categories/id/{d.Code}/attributes", "Basic", token);
                    getAttributesResponse.data.category.attributes.RemoveAll(a =>
                        a.name.ToLower() == "sezon bilgisi" ||
                        a.name.ToLower() == "beden");

                    d.CategoryAttributes ??= new();
                    d.CategoryAttributes.AddRange(getAttributesResponse
                        .data
                        .category
                        .attributes
                        .Select(a => new CategoryAttributeResponse
                        {
                            Code = a.id.ToString(),
                            Name = a.name,
                            Mandatory = a.mandatory == "true",
                            CategoryCode = d.Code,
                            CategoryAttributesValues = a
                                .attribute_values
                                .Select(av => new CategoryAttributeValueResponse
                                {
                                    Code = av.id.ToString(),
                                    Name = av.name,
                                    VarinatCode = a.id.ToString()
                                })
                                .ToList()
                        }));
                    #endregion

                    #region Variant Attributes
                    var getVariantAttributesResponse = this
                        ._httpHelper
                        .Get<GetVariantAttributesResponse>($"{_baseUrl}/categories/id/{d.Code}/variant-attributes", "Basic", token);

                    d.CategoryAttributes.AddRange(getVariantAttributesResponse
                        .data
                        .category
                        .variant_attributes
                        .Select(a => new CategoryAttributeResponse
                        {
                            Code = $"VA_{d.Code}_{a.id}",
                            Name = $"{d.Name} {a.name}",
                            Mandatory = a.mandatory == "true",
                            CategoryCode = d.Code,
                            CategoryAttributesValues = a
                                .attribute_values
                                .GroupBy(av => av.name)
                                .Select(av => new CategoryAttributeValueResponse
                                {
                                    Code = $"VA_{av.Max(av2 => av2.id)}",
                                    Name = av.Key,
                                    VarinatCode = $"VA_{d.Code}_{a.id}"
                                })
                                .ToList()
                        }));
                    #endregion

                    //d.CategoryAttributes.Add(new CategoryAttributeResponse
                    //{
                    //    Code = "Kumaş",
                    //    Name = "Kumaş",
                    //    CategoryAttributesValues = fabrics
                    //        .Select(f => new CategoryAttributeValueResponse
                    //        {
                    //            Code = f.id.ToString(),
                    //            Name = f.name
                    //        })
                    //        .ToList()
                    //});

                    //d.CategoryAttributes.Add(new CategoryAttributeResponse
                    //{
                    //    Code = "Sezon",
                    //    Name = "Sezon",
                    //    Mandatory = true,
                    //    CategoryCode = d.Code,
                    //    CategoryAttributesValues = seasons
                    //        .Select(s => new CategoryAttributeValueResponse
                    //        {
                    //            Code = s.id.ToString(),
                    //            Name = s.name,
                    //            VarinatCode = s.id.ToString()
                    //        })
                    //        .ToList()
                    //});

                    //d.CategoryAttributes.Add(new CategoryAttributeResponse
                    //{
                    //    Code = "Menşei",
                    //    Name = "Menşei",
                    //    Mandatory = true,
                    //    CategoryCode = d.Code,
                    //    CategoryAttributesValues = origins
                    //        .Select(o => new CategoryAttributeValueResponse
                    //        {
                    //            Code = o.id.ToString(),
                    //            Name = o.name,
                    //            VarinatCode = o.id.ToString()
                    //        })
                    //        .ToList()
                    //});
                });

                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var authorization = this._configuration.Authorization;

                var details = new List<Detail>();

                var page = 0;
                var totalPage = 0;
                var url = $"{_baseUrl}/orderList?startDateTime={DateTime.Now.AddDays(-1):yyyy-MM-dd}T00:00:00&endDateTime={DateTime.Now:yyyy-MM-dd}T23:59:59&dateType=payment&paymentStatus=approved&size=100";

                do
                {
                    var getOrderListResponse = this._httpHelper.Get<GetOrderListResponse>(url + $"&page={page}", "Basic", authorization);
                    if (getOrderListResponse.success)
                        details.AddRange(getOrderListResponse.data.orders.details);
                    else
                        break;

                    page++;
                } while (page >= totalPage);

                response
                    .Data
                    .Orders
                    .AddRange(details
                        .GroupBy(d => d.orderId)
                        .Select(d => new Order
                        {
                            UId = $"{Application.Common.Constants.Marketplaces.Modanisa}_{d.Key}",
                            OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                            MarketplaceId = Application.Common.Constants.Marketplaces.Modanisa,
                            OrderCode = d.Key,
                            OrderTypeId = "OS",
                            CargoFee = 0,
                            Discount = 0,
                            ListTotalAmount = d.Sum(d2 => d2.products.Sum(d3 => d3.quantity * decimal.Parse(d3.price))),
                            TotalAmount = d.Sum(d2 => d2.products.Sum(d3 => d3.quantity * decimal.Parse(d3.price))),
                            OrderDate = DateTime.Parse(d.First().date),
                            Customer = new Customer
                            {
                                FirstName = String.Empty,
                                LastName = String.Empty,
                                Mail = String.Empty,
                                TaxAdministration = String.Empty,
                                TaxNumber = String.Empty
                            },
                            OrderDeliveryAddress = new OrderDeliveryAddress()
                            {
                                FirstName = String.Empty,
                                LastName = String.Empty,
                                Country = "Türkiye",
                                Email = String.Empty,
                                Phone = String.Empty,
                                City = "İstanbul",
                                District = "Tuzla",
                                Neighborhood = "Tepeören",
                                Address = String.Empty,
                            },
                            OrderInvoiceAddress = new OrderInvoiceAddress()
                            {
                                FirstName = String.Empty,
                                LastName = String.Empty,
                                Email = String.Empty,
                                Phone = String.Empty,
                                City = "İstanbul",
                                District = "Üsküdar",
                                Address = String.Empty,
                                Neighborhood = "Altunizade",
                                TaxNumber = "6220586224",
                                TaxOffice = "Üsküdar"
                            },
                            OrderDetails = d
                                .SelectMany(d2 => d2
                                    .products
                                    .Select(d3 => new OrderDetail
                                    {
                                        ListPrice = decimal.Parse(d3.price),
                                        UnitPrice = decimal.Parse(d3.price),
                                        Payor = true,
                                        Quantity = d3.quantity,
                                        UnitDiscount = 0,
                                        Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                        {
                                            Barcode = d3.barcode,
                                            Name = d3.productName,
                                            StockCode = String.Empty
                                        }
                                    }))
                                .ToList()
                        }));

                Parallel.ForEach(response.Data.Orders, theOrder =>
                {
                    theOrder.UId = $"M_{theOrder.OrderCode}";
                });
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new() };

                var token = this._configuration.Authorization;

                var details = new List<Detail>();

                var page = 1;
                var totalPage = 0;
                var url = $"{_baseUrl}/orderListV2?startDateTime={DateTime.Now.AddDays(-5):yyyy-MM-dd}T00:00:00&endDateTime={DateTime.Now:yyyy-MM-dd}T23:59:59&paymentStatus=approved&size=100";

                do
                {
                    var getOrderListResponse = this._httpHelper.Get<GetOrderListResponse>(url + $"&page={page}", "Basic", token);
                    if (getOrderListResponse.success)
                        details.AddRange(getOrderListResponse.data.orders.details);
                    else
                        break;

                    if (totalPage == 0)
                        totalPage = getOrderListResponse.data.pagination == null
                            ? 1
                            : Convert.ToInt32(getOrderListResponse.data.pagination.totalPage);

                    page++;
                } while (totalPage > page);

                details.ForEach(d => d.products.RemoveAll(p => p.status != "Pending"));

                response.Data.Orders = details.SelectMany(d => d.products.Select(p => new Order
                {
                    CheckCustomer = true,
                    BulkInvoicing = true,
                    OrderSourceId = Application.Common.Constants.OrderSources.Pazaryeri,
                    MarketplaceId = Application.Common.Constants.Marketplaces.Modanisa,
                    OrderCode = $"{d.orderId}-{p.variantId}",
                    OrderTypeId = "OS",
                    CargoFee = 0,
                    Discount = 0,
                    ListTotalAmount = p.quantity * decimal.Parse(p.price),
                    TotalAmount = p.quantity * decimal.Parse(p.price),
                    OrderDate = DateTime.Parse(d.date),
                    Customer = new Customer
                    {
                        FirstName = "Modanisa",
                        LastName = "Modanisa",
                        Mail = "tedarik@modanisa.com",
                        TaxAdministration = String.Empty,
                        TaxNumber = String.Empty
                    },
                    OrderDeliveryAddress = new OrderDeliveryAddress()
                    {
                        FirstName = "Modanisa",
                        LastName = "Modanisa",
                        Country = "Türkiye",
                        Email = "tedarik@modanisa.com",
                        Phone = "2164747473",
                        City = "İstanbul",
                        District = "Tuzla",
                        Neighborhood = "Tepeören",
                        Address = "Altunizade Mahallesi Kuşbakışı Caddesi No:27/1"
                    },
                    OrderInvoiceAddress = new OrderInvoiceAddress()
                    {
                        FirstName = "MODANİSA ELEKTRONİK MAĞAZACILIK VE TİCARET ANONİM ŞİRKETİ",
                        LastName = " ",
                        Email = "tedarik@modanisa.com",
                        Phone = "2164747473",
                        City = "İstanbul",
                        District = "Üsküdar",
                        Address = "Altunizade Mahallesi Kuşbakışı Caddesi No:27/1",
                        Neighborhood = "Altunizade",
                        TaxNumber = "6220586224",
                        TaxOffice = "Üsküdar"
                    },
                    OrderDetails = new List<OrderDetail>
                    {
                        new OrderDetail
                        {
                            ListPrice = decimal.Parse(p.price),
                            UnitPrice = decimal.Parse(p.price),
                            Payor = true,
                            Quantity = p.quantity,
                            UnitDiscount = 0,
                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                            {
                                Barcode = p.barcode,
                                Name = p.productName,
                                StockCode = String.Empty
                            }
                        }
                    },
                    OrderShipment = new OrderShipment
                    {
                        ShipmentCompanyId = "M"
                    }
                }))
                .ToList();
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() } };
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() } };
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return new DataResponse<OrderResponse> { Data = new() { Orders = new() } };
        }

        protected override Application.Common.Wrappers.Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Application.Common.Wrappers.Response>((response) =>
            {

            });
        }
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            var brands = this._cacheHandler.ResponseHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>(() =>
            {

                return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
                {
                    response.Data = new List<Application.Common.DataTransferObjects.KeyValue<string, string>>();

                    var token = this._configuration.Authorization;

                    var page = 0;
                    var totalPage = 0;
                    var url = $"{_baseUrl}/brands?size=200";

                    do
                    {
                        var getOrderListResponse = this._httpHelper.Get<GetBrandResponse>(url + $"&page={page}", "Basic", token);
                        if (getOrderListResponse.success)
                        {
                            response.Data.AddRange(getOrderListResponse.data.brands.Select(b => new Application.Common.DataTransferObjects.KeyValue<string, string>
                            {
                                Key = b.id.ToString(),
                                Value = b.name,
                            }));

                            totalPage = getOrderListResponse.data.pagination.total_page;
                        }
                        else
                            break;

                        page++;
                    } while (page <= totalPage);

                    response.Success = true;
                });
            }, "ModaniseBrands", 30);

            var response = new DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>();

            if (brands.Success)
            {
                response.Success = true;
                response.Data = brands.Data.Where(x => x.Value.ToLower().Contains(q.ToLower())).ToList();
            }
            return response;
        }
        #endregion

        #region Invoice Methods
        protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return new Response();
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(ModanisaConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }
        #endregion

        #region Helper Methods
        private void FetchSubCategory(List<CategoryResponse> categoryResponse, List<Responses.GetCategories.SubCategory> subCategories)
        {


            foreach (var theSubCategory in subCategories)
            {
                if (theSubCategory.sub_categories != null)
                {
                    FetchSubCategory(categoryResponse, theSubCategory.sub_categories);
                }
                else
                {
                    categoryResponse.Add(new CategoryResponse
                    {
                        Code = theSubCategory.id.ToString(),
                        ParentCategoryCode = theSubCategory.parent_id.ToString(),
                        Name = theSubCategory.name
                    });
                }
            }
        }
        #endregion
    }
}