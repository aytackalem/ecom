﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using ECommerce.Infrastructure.Marketplaces.LCW.ProductFilter;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Marketplaces.LCW;

public class Lcw : MarketplaceBase<LcwConfiguration>
{
    #region Fields
    public readonly IHttpHelper _httpHelper;
    #endregion

    #region Constructors
    public Lcw(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
    {
        #region Fields
        this._httpHelper = httpHelper;
        #endregion
    }
    #endregion

    #region Properties
    public override string MarketplaceId => Application.Common.Constants.Marketplaces.Lcw;
    #endregion

    #region Methods
    #region Order Methods
    protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
    {
        return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
        {
            response.Data = new OrderResponse
            {
                Orders = new List<Application.Common.Wrappers.Marketplaces.Order.Order>()
            };

            var token = GetToken();

            GetOrder.Request _request = new()
            {
                PageIndex = 0,
                ShippingListStatusId = 27,
                Size = 100,
                SupplierId = this._configuration.SupplierId
            };

            GetOrder.Response _response = null;

            do
            {
                _response = _httpHelper.Post<GetOrder.Request, GetOrder.Response>(
                    _request,
                    "https://mpapi.lcw.com/integration/integrator/service/api/order/GetOrder",
                    "Bearer",
                    token,
                    new());

                //TO DO: Map
                foreach (var _ in _response.shippingLists)
                {

                    var shipmentCompanyId = ShipmentCompanies.ArasKargo;

                    switch (_.cargoCompanyId)
                    {
                        case 14:
                            shipmentCompanyId = ShipmentCompanies.ArasKargo;
                            break;
                        case 20:
                        case 27:
                            shipmentCompanyId = ShipmentCompanies.MngKargo;
                            break;
                        case 28:
                            shipmentCompanyId = ShipmentCompanies.HepsiJet;
                            break;
                        case 35:
                            shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                            break;
                        case 78:
                            shipmentCompanyId = ShipmentCompanies.SuratKargo;
                            break;

                        default:
                            shipmentCompanyId = ShipmentCompanies.ArasKargo;
                            break;
                    }




                    var order = new Application.Common.Wrappers.Marketplaces.Order.Order
                    {
                        UId = $"{Application.Common.Constants.Marketplaces.Lcw}_{_.orderNumber}",
                        OrderSourceId = OrderSources.Pazaryeri,
                        MarketplaceId = Application.Common.Constants.Marketplaces.Lcw,
                        OrderCode = _.orderNumber.ToString(),
                        OrderTypeId = "OS",
                        Commercial = false,
                        TotalAmount = _.totalPrice,
                        ListTotalAmount = _.grossAmount,
                        Discount = _.totalDiscount,
                        OrderDate = _.orderDate.ConvertToDateTime(),
                        OrderShipment = new()
                        {
                            PackageNumber = _.orderNumber.ToString(),
                            TrackingCode = _.cargoShipmentCode,
                            ShipmentTypeId = "PND",
                            TrackingUrl = "",
                            ShipmentCompanyId = shipmentCompanyId
                        },
                        Customer = new()
                        {
                            FirstName = _.customerFirstName,
                            LastName = _.customerLastName,
                            Mail = _.customerEmail,
                            TaxNumber = "",//_.TaxNumber,
                            Phone = "" //TODO set phone number
                        },
                        OrderDeliveryAddress = new()
                        {
                            FirstName = _.shipmentAddress.firstName,
                            LastName = _.shipmentAddress.lastName,
                            Country = _.shipmentAddress.countryCode.ToUpper() == "TR" ? "Türkiye" : "",
                            Email = _.customerEmail,
                            Phone = "", //TODO set phone number
                            City = _.shipmentAddress.city,
                            District = _.shipmentAddress.district,
                            Neighborhood = _.shipmentAddress.neighborhood,
                            Address = $"{_.shipmentAddress.address1} {_.shipmentAddress.address2}"
                        },
                        OrderInvoiceAddress = new()
                        {
                            FirstName = _.invoiceAddress.firstName,
                            LastName = _.invoiceAddress.lastName,
                            Email = _.customerEmail,
                            Phone = "", //TODO set phone number
                            City = _.invoiceAddress.city,
                            District = _.invoiceAddress.district,
                            Address = _.invoiceAddress.address1 + " " + _.invoiceAddress.address2,
                            Neighborhood = _.invoiceAddress.neighborhood,
                            TaxNumber = _.invoiceAddress.taxNumber,
                            TaxOffice = _.invoiceAddress.taxOffice

                        },
                        Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                        {
                            new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                            {
                                CurrencyId = "TRY",
                                PaymentTypeId = "OO",
                                Amount = _.totalPrice,
                                Date = _.orderDate.ConvertToDateTime()
                            }
                        },

                        OrderPicking = new()
                        {
                            Token = token,
                            MarketPlaceOrderId = this.MarketplaceId,
                            TrackingNumber = _.shippingListNo.ToString(),
                            OrderDetails = new List<OrderPickingDetail>()
                        }
                    };

                    order.OrderDetails = new();

                    foreach (var orderDetail in _.shippingItemLines)
                    {
                        var existingOrderDetail = order
                            .OrderDetails
                            .FirstOrDefault(_ => _.Product.Barcode == orderDetail.barcode.ToUpper());

                        if (existingOrderDetail == null)
                        {
                            order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                            {
                                UId = orderDetail.orderItemId.ToString(),
                                TaxRate = Convert.ToDouble(orderDetail.vatBaseAmount) / 100,
                                UnitPrice = orderDetail.price,
                                ListPrice = orderDetail.price,
                                UnitDiscount = orderDetail.discount,
                                Product = new()
                                {
                                    Barcode = orderDetail.barcode.ToUpper(),
                                    Name = orderDetail.productName
                                },
                                Quantity = 1
                            });
                        }
                        else
                        {
                            existingOrderDetail.Quantity += 1;
                        }

                        order.OrderPicking.OrderDetails.Add(new OrderPickingDetail
                        {
                            Id = orderDetail.orderItemId.ToString(),
                            Quantity = 1
                        });
                    }

                    response.Data.Orders.Add(order);
                }


                _request.PageIndex++;
            }
            while (_request.PageIndex < _response.totalPages);

            response.Success = true;
            response.Message = "Siparişler başarılı şekilde getirildi";

        }, (response, exception) =>
        {
            response.Success = false;
            response.Message = "Bilinmeyen bir hata oluştu.";
        });
    }

    protected override Response OrderTypeUpdateConcreate(OrderPickingRequest request)
    {
        return ExceptionHandler.ResultHandle<Response>((response) =>
        {
            if (request.Status == Application.Common.Parameters.Enums.MarketplaceStatus.Picking)
            {
                _httpHelper.Post<PutUpdatePackageStatus.Request, PutUpdatePackageStatus.Response>(
                    new PutUpdatePackageStatus.Request
                    {
                        ShippingListNo = request.OrderPicking.TrackingNumber,
                        ShippingListStatusId = "15"
                    },
                    "https://mpapi.lcw.com/integration/integrator/service/api/order/PutUpdatePackageStatus",
                    "Bearer",
                    request.Token,
                    new());
            }

        }, (response, exception) =>
        {
            response.Success = false;
            response.Message = "LCW ile bağlantı kurulamıyor.";
        });
    }

    protected override void BindConfigurationConcreate(LcwConfiguration configuration, List<MarketplaceConfiguration> marketplaceConfigurations)
    {
        var supplierId = 0;
        int.TryParse(marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "SupplierId").Value, out supplierId);
        configuration.SupplierId = supplierId;
        configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Username").Value;
        configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
    }
    #endregion

    #region Helper Methods
    private string GetToken()
    {
        var data = new List<KeyValuePair<string, string>> {
                new("client_id", "LCWSellerEntegrator"),
                new("username", _configuration.Username),
                new("password", _configuration.Password),
                new("grant_type", "password"),
                new("client_secret", "D51C3FADCA8A40118A88F3CD6C197EEA"),
                new("scope", "openid")
            };

        var tokenResponse = this._httpHelper.Post<Token.Response>("https://pars.lcwaikiki.com/sts/issue/oidc/token", data);
        return tokenResponse.AccessToken;
    }
    #endregion

    #region Deprecated Methods
    protected override DataResponse<List<KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
    {
        return new DataResponse<List<KeyValue<string, string>>>();
    }

    protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
    {
        return new DataResponse<CheckPreparingResponse>();
    }

    protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
    {
        return new DataResponse<string>();
    }

    protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
    {
        return new DataResponse<OrderResponse>();
    }

    protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
    {
        return new DataResponse<OrderTypeUpdateResponse>();
    }

    protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
    {
        return new DataResponse<List<CategoryResponse>>();
    }

    protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
    {
        return new DataResponse<OrderResponse>();
    }

    protected override DataResponse<List<ProductItem>> GetProductsConcreate()
    {
        return new DataResponse<List<ProductItem>>();
    }

    protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
    {
        return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
        {
            response.Data = new List<MarketplaceProductStatus>();


            var token = GetToken();

            var pageIndex = 0;
            LcwFilterProduct _response = null;
            do
            {
                _response = _httpHelper.Get<LcwFilterProduct>(
                     $"https://mpapi.lcw.com/integration/product-integration/service/api/Product/FilterProduct?SupplierId={this._configuration.SupplierId}&Page={pageIndex}&Size=100",
                     "Bearer",
                     token);


                if (_response != null && _response.Data.Count > 0)
                {

                    foreach (var theContent in _response.Data)
                    {
                        foreach (var sProduct in theContent.Products)
                        {

                            var marketplaceProductStatus = new MarketplaceProductStatus
                            {
                                MarketplaceId = this.MarketplaceId,
                                Barcode = sProduct.Barcode,
                                UUId = sProduct.ProductId.ToString(),
                                Opened = true,
                                OnSale = theContent.Onsale,
                                Locked = false,
                                Url = !string.IsNullOrEmpty(sProduct.EcommerceURL) ? sProduct.EcommerceURL : string.Empty,
                                ListUnitPrice = sProduct.Prices[0].SalePrice,
                                UnitPrice = sProduct.Prices[0].SalePrice,
                                Stock = sProduct.Quantity
                            };
                            response.Data.Add(marketplaceProductStatus);
                        }

                    }
                }


                pageIndex++;
            }
            while (pageIndex < _response.TotalPages);

            response.Success = true;
        });
    }

    protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
    {
        return new DataResponse<OrderResponse>();
    }

    protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
    {
        return new DataResponse<OrderResponse>();
    }

    protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
    {
        return new Response();
    }

    protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
    {
        return new DataResponse<string>();
    }

    protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
    {
        return new DataResponse<string>();
    }
    #endregion
    #endregion
}
