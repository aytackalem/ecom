﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.LCW.ProductFilter
{
    public class LcwFilterProduct
    {
        public int Count { get; set; }
        public int Size { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public List<LcwDatum> Data { get; set; }
        public object ErrorMessage { get; set; }
    }

    public class LcwAttribute
    {
        public int AttributeId { get; set; }
        public int AttributeValueId { get; set; }
    }

    public class LcwCategory
    {
        public int CategoryId { get; set; }
    }

    public class LcwDatum
    {
        public int OptionId { get; set; }
        public int SupplierId { get; set; }
        public int BrandId { get; set; }
        public string ProductMainId { get; set; }
        public string MainColorCode { get; set; }
        public List<LcwCategory> Categories { get; set; }
        public int GenderId { get; set; }
        public List<LcwTitle> Titles { get; set; }
        public List<LcwImage> Images { get; set; }
        public List<LcwProduct> Products { get; set; }
        public List<LcwAttribute> Attributes { get; set; }
        public int StatusId { get; set; }
        public int PassiveStatusReasonId { get; set; }
        public bool Approved { get; set; }
        public bool Onsale { get; set; }
    }


    public class LcwImage
    {
        public string Url { get; set; }
        public string LcwUrl { get; set; }
        public int Displayorder { get; set; }
    }

    public class LcwPrice
    {
        public string Countrycode { get; set; }
        public string CurrencyType { get; set; }
        public decimal SalePrice { get; set; }
        public int VatRate { get; set; }
    }

    public class LcwProduct
    {
        public int ProductId { get; set; }
        public int ProductSkuId { get; set; }
        public string Barcode { get; set; }
        public int SizeId { get; set; }
        public string StockCode { get; set; }
        public int Quantity { get; set; }
        public List<LcwPrice> Prices { get; set; }
        public string EcommerceURL { get; set; }
    }

    public class Root
    {
        public int Count { get; set; }
        public int Size { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public List<LcwDatum> Data { get; set; }
        public object errorMessage { get; set; }
    }

    public class LcwTitle
    {
        public string languageCode { get; set; }
        public string value { get; set; }
    }
}
