﻿namespace ECommerce.Infrastructure.Marketplaces.LCW.GetOrder;



public class Response
{
    public Shippinglist[] shippingLists { get; set; }
    public int totalCount { get; set; }
    public int totalPages { get; set; }
    public int pageIndex { get; set; }
    public int size { get; set; }
}

public class Shippinglist
{
    public int shippingListId { get; set; }
    public long shippingListNo { get; set; }
    public string orderNumber { get; set; }
    public long orderDate { get; set; }
    public long modifiedDate { get; set; }
    public long createdDate { get; set; }
    public int shippingListStatusId { get; set; }
    public string shippingListStatus { get; set; }
    public Shipmentaddress shipmentAddress { get; set; }
    public Invoiceaddress invoiceAddress { get; set; }
    public Shippingitemline[] shippingItemLines { get; set; }
    public Shipmentstatushistory[] shipmentStatusHistory { get; set; }
    public decimal grossAmount { get; set; }
    public decimal totalDiscount { get; set; }
    public decimal totalPrice { get; set; }
    public decimal interestAmount { get; set; }
    public decimal showedGrossAmount { get; set; }
    public decimal showedTotalDiscount { get; set; }
    public int customerId { get; set; }
    public string customerFirstName { get; set; }
    public string customerLastName { get; set; }
    public string customerEmail { get; set; }
    public string customerTCKNo { get; set; }
    public object cargoTrackingNumber { get; set; }
    public object cargoTrackingLink { get; set; }
    public string cargoShipmentCode { get; set; }
    public int cargoCompanyId { get; set; }
    public string cargoProviderName { get; set; }
    public int cargoIntegratorType { get; set; }
    public int packageQuantity { get; set; }
    public object currencyCode { get; set; }
}

public class Shipmentaddress
{
    public int shipmentAddressId { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string address1 { get; set; }
    public object address2 { get; set; }
    public string city { get; set; }
    public int cityId { get; set; }
    public string district { get; set; }
    public int districtId { get; set; }
    public string postalCode { get; set; }
    public string countryCode { get; set; }
    public string neighborhood { get; set; }
    public int neighborhoodId { get; set; }
    public object phoneNumber { get; set; }
}

public class Invoiceaddress
{
    public int invoiceAddressId { get; set; }
    public string taxOffice { get; set; }
    public string taxNumber { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public object company { get; set; }
    public string address1 { get; set; }
    public object address2 { get; set; }
    public string city { get; set; }
    public int cityId { get; set; }
    public string district { get; set; }
    public int districtId { get; set; }
    public string postalCode { get; set; }
    public string countryCode { get; set; }
    public string neighborhood { get; set; }
    public int neighborhoodId { get; set; }
}

public class Shippingitemline
{
    public int salesCampaignId { get; set; }
    public int productId { get; set; }
    public string productName { get; set; }
    public string productCode { get; set; }
    public string productSize { get; set; }
    public string productColor { get; set; }
    public int orderItemId { get; set; }
    public decimal discount { get; set; }
    public decimal amount { get; set; }
    public decimal price { get; set; }
    public decimal oneInstallmentDiscountedPrice { get; set; }
    public decimal interestAmountPerUnit { get; set; }
    public decimal cashPrice { get; set; }
    public decimal showedAmount { get; set; }
    public decimal showedDiscount { get; set; }
    public decimal showedPrice { get; set; }
    public object currencyCode { get; set; }
    public int shippingListItemId { get; set; }
    public string productSkuId { get; set; }
    public string supplierStockCode { get; set; }
    public int vatBaseAmount { get; set; }
    public string barcode { get; set; }
    public string orderLineItemStatusName { get; set; }
    public int orderLineItemStatusId { get; set; }
}

public class Shipmentstatushistory
{
    public long createdDate { get; set; }
    public string status { get; set; }
    public int shippingListStatusId { get; set; }
}