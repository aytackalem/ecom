﻿namespace ECommerce.Infrastructure.Marketplaces.LCW.GetOrder;

public class Request
{
    public int SupplierId { get; set; }

    public int Size { get; set; }

    public int PageIndex { get; set; }

    public int ShippingListStatusId { get; set; }

    public string OrderNumber { get; set; }
}
