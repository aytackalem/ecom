﻿namespace ECommerce.Infrastructure.Marketplaces.LCW;

public class LcwConfiguration
{
    public string Username { get; set; }

    public string Password { get; set; }

    public int SupplierId { get; set; }
}
