﻿using Newtonsoft.Json;

namespace ECommerce.Infrastructure.Marketplaces.LCW.Token;

public class Response
{
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }
}