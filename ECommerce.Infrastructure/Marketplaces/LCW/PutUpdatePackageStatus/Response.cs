﻿namespace ECommerce.Infrastructure.Marketplaces.LCW.PutUpdatePackageStatus;

public class Response
{
    public bool isSuccess { get; set; }
    public object description { get; set; }
    public object errorMessage { get; set; }
}
