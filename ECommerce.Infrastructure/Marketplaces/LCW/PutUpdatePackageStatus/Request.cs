﻿namespace ECommerce.Infrastructure.Marketplaces.LCW.PutUpdatePackageStatus;

public class Request
{
    public string ShippingListNo { get; set; }

    public string ShippingListStatusId { get; set; }
}
