﻿using ECommerce.Application.Common.Interfaces.Marketplaces;

namespace ECommerce.Infrastructure.Marketplaces.Boyner
{
    public class BoynerConfiguration : UsernamePasswordConfigurationBase
    {
        #region Properties
        public string SupplierId { get; set; }
        #endregion
    }
}
