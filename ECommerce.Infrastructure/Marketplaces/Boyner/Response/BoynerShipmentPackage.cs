﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{

    public class BoynerShipmentPackage
    {
        public List<BoynerShipmentPackageLine> lines { get; set; }
        public string status { get; set; }
    }

    public class BoynerShipmentPackageLine
    {
        public long lineId { get; set; }
        public int quantity { get; set; }
    }
}
