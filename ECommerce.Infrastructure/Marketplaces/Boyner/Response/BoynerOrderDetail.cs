﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{
    public class BoynerOrderDetail
    {
        public int Quantity { get; set; }
        public int SalesCampaignId { get; set; }
        public string ProductSize { get; set; }
        public string MerchantSku { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public int MerchantId { get; set; }
        public int Discount { get; set; }
        public string CurrencyCode { get; set; }
        public string ProductColor { get; set; }
        public int Id { get; set; }
        public string Sku { get; set; }
        public int VatBaseAmount { get; set; }
        public string Barcode { get; set; }
        public string OrderLineItemStatusName { get; set; }
        public decimal Price { get; set; }
        public DateTime EstimatedDeliveryEndDate { get; set; }
        public string CargoProviderName { get; set; }
    }
}
