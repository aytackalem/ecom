﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{
    public class BoynerShipmentAddress
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string NeighborhoodCode { get; set; }
        public string NeighborhoodName { get; set; }
        public string PostalCode { get; set; }
        public string FullAddress { get; set; }
        public string Phone { get; set; }
    }
}
