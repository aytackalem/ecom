﻿using System;
using System.Collections.Generic;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{
    public class BoynerOrder
    {
        #region Properties
        public string OrderNumber { get; set; }
        public string PackageNumber { get; set; }
        public decimal GrossAmount { get; set; }
        public int TotalDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public string TaxNumber { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerEmail { get; set; }
        public int CustomerId { get; set; }
        public int Id { get; set; }
        public int OrderId { get; set; }
        public long OrderDate { get; set; }
        public DateTime PlatformCreateDate { get; set; }
        public string TcIdentityNumber { get; set; }
        public string CurrencyCode { get; set; }
        public string ShipmentPackageStatus { get; set; }
        public string Status { get; set; }
        public string DeliveryType { get; set; }
        public int TimeSlotId { get; set; }
        public string ScheduledDeliveryStoreId { get; set; }
        public string DeliveryAddressType { get; set; }
        public int AgreedDeliveryDate { get; set; }
        public string InvoiceLink { get; set; }
        public bool FastDelivery { get; set; }
        public string FastDeliveryType { get; set; }
        public int? OriginShipmentDate { get; set; }
        public int LastModifiedDate { get; set; }
        public bool Commercial { get; set; }
        public string CargoTrackingNumber { get; set; }
        public string CargoTrackingLink { get; set; }
        public object CargoSenderNumber { get; set; }
        public string CargoProviderName { get; set; }
        public bool IsSellerCarrier { get; set; }
        public List<BoynerOrderDetail> Lines { get; set; }
        public BoynerInvoiceAddress InvoiceAddress { get; set; }
        public BoynerShipmentAddress ShipmentAddress { get; set; }
        #endregion
    }
}
