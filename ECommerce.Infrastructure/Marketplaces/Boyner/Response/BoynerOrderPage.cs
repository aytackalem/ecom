﻿using ECommerce.Infrastructure.Marketplaces.Boyner.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{
    public class BoynerOrderPage : BoynerPage
    {
        public List<BoynerOrder> Content { get; set; }
    }
}
