﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.Boyner.Response
{
  


    public class BoynerFilterProductContent
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("approved")]

        public bool Approved { get; set; }
        [JsonProperty("archived")]

        public bool Archived { get; set; }
        [JsonProperty("productCode")]

        public string ProductCode { get; set; }
        [JsonProperty("batchRequestId")]

        public string BatchRequestId { get; set; }
        [JsonProperty("supplierId")]

        public int SupplierId { get; set; }
        [JsonProperty("brand")]

        public string Brand { get; set; }
        [JsonProperty("brandId")]

        public int BrandId { get; set; }
        [JsonProperty("barcode")]

        public string Barcode { get; set; }
        [JsonProperty("title")]

        public string Title { get; set; }
        [JsonProperty("categoryName")]

        public string CategoryName { get; set; }
        [JsonProperty("productMainId")]

        public string ProductMainId { get; set; }
        [JsonProperty("description")]

        public string Description { get; set; }
        [JsonProperty("quantity")]

        public int Quantity { get; set; }
        [JsonProperty("listPrice")]

        public double ListPrice { get; set; }
        [JsonProperty("salePrice")]

        public double SalePrice { get; set; }
        [JsonProperty("vatRate")]

        public int VatRate { get; set; }
        [JsonProperty("dimensionalWeight")]

        public int DimensionalWeight { get; set; }
        [JsonProperty("stockCode")]

        public string StockCode { get; set; }
        [JsonProperty("hasActiveCampaign")]

        public bool HasActiveCampaign { get; set; }
        [JsonProperty("locked")]

        public bool Locked { get; set; }
        [JsonProperty("productContentId")]

        public int ProductContentId { get; set; }
        [JsonProperty("pimCategoryId")]

        public int PimCategoryId { get; set; }
        [JsonProperty("version")]

        public int Version { get; set; }
        [JsonProperty("onsale")]

        public bool Onsale { get; set; }
        [JsonProperty("deliveryDuration")]

        public int DeliveryDuration { get; set; }
        [JsonProperty("deliveryOptionId")]

        public int DeliveryOptionId { get; set; }
        [JsonProperty("cargoCompanyId")]

        public int CargoCompanyId { get; set; }
        [JsonProperty("expiry")]

        public int Expiry { get; set; }
        [JsonProperty("stateId")]

        public int StateId { get; set; }
        [JsonProperty("status")]

        public string Status { get; set; }
        [JsonProperty("shipmentAddress")]

        public BoynerFilterProductShipmentAddress ShipmentAddress { get; set; }
    
        [JsonProperty("returningAddress")]
        public BoynerFilterProductReturningAddress ReturningAddress { get; set; }

        [JsonProperty("images")]
        public List<BoynerFilterProductImage> Images { get; set; }
 
    }

    public class BoynerFilterProductImage
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("url")]

        public string Url { get; set; }
    }

    public class BoynerFilterProductReturningAddress
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("addressType")]
        public string AddressType { get; set; }
        [JsonProperty("country")]

        public object Country { get; set; }
        [JsonProperty("countryId")]

        public int CountryId { get; set; }
        [JsonProperty("city")]

        public object City { get; set; }
        [JsonProperty("cityCode")]

        public int CityCode { get; set; }
        [JsonProperty("district")]

        public object District { get; set; }
        [JsonProperty("districtId")]

        public int DistrictId { get; set; }
        [JsonProperty("neighbourhood")]

        public object Neighbourhood { get; set; }
        [JsonProperty("neighbourhoodId")]

        public int NeighbourhoodId { get; set; }
        [JsonProperty("postCode")]

        public object PostCode { get; set; }
        [JsonProperty("address")]

        public string Address { get; set; }
        [JsonProperty("returningAddress")]

        public bool ReturningAddress { get; set; }
        [JsonProperty("fullAddress")]

        public string FullAddress { get; set; }
        [JsonProperty("shipmentAddress")]

        public bool ShipmentAddress { get; set; }
        [JsonProperty("invoiceAddress")]

        public bool InvoiceAddress { get; set; }

    }

    public class BoynerFilterProductPage
    {
        [JsonProperty("totalElements")]

        public int TotalElements { get; set; }
        [JsonProperty("totalPages")]

        public int TotalPages { get; set; }
        [JsonProperty("page")]

        public int Page { get; set; }
        [JsonProperty("size")]

        public int Size { get; set; }
        [JsonProperty("content")]

        public List<BoynerFilterProductContent> Content { get; set; }
    }

    public class BoynerFilterProductShipmentAddress
    {
        [JsonProperty("id")]

        public int Id { get; set; }
        [JsonProperty("addressType")]

        public string AddressType { get; set; }
        [JsonProperty("country")]

        public object Country { get; set; }
        [JsonProperty("countryId")]

        public int CountryId { get; set; }
        [JsonProperty("city")]

        public object City { get; set; }
        [JsonProperty("cityCode")]

        public int CityCode { get; set; }
        [JsonProperty("district")]

        public object District { get; set; }
        [JsonProperty("districtId")]

        public int DistrictId { get; set; }
        [JsonProperty("neighbourhood")]

        public object Neighbourhood { get; set; }
        [JsonProperty("neighbourhoodId")]

        public int NeighbourhoodId { get; set; }
        [JsonProperty("postCode")]

        public object PostCode { get; set; }
        [JsonProperty("address")]

        public string Address { get; set; }
        [JsonProperty("returningAddress")]

        public bool ReturningAddress { get; set; }
        [JsonProperty("fullAddress")]

        public string FullAddress { get; set; }
        [JsonProperty("shipmentAddress")]

        public bool ShipmentAddress { get; set; }
        [JsonProperty("invoiceAddress")]

        public bool InvoiceAddress { get; set; }
    }


}
