﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Enums;
using ECommerce.Application.Common.Parameters.Order;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Application.Common.Wrappers.Marketplaces.Order;
using ECommerce.Infrastructure.Marketplaces.Boyner.Response;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using Order = ECommerce.Application.Common.Wrappers.Marketplaces.Order.Order;

namespace ECommerce.Infrastructure.Marketplaces.Boyner
{
    public class Boyner : MarketplaceBase<BoynerConfiguration>
    {


        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public Boyner(IHttpHelper httpHelper, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.Boyner;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ProductItem>>>((response) =>
            {
                response.Data = new List<ProductItem>();

                response.Success = true;
            });

        }

        protected override DataResponse<List<MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                response.Data = new List<MarketplaceProductStatus>();

                var supplierId = this._configuration.SupplierId;
                var authorization = this._configuration.Authorization;


                var BoynerFilterProducts = new List<BoynerFilterProductPage>();
                var page = 0;
                var size = 500;
                BoynerFilterProductPage BoynerFilterProductPageResult = null;
                var customHeader = new Dictionary<string, string>
                    {
                        {"User-Agent",$"{this._configuration.SupplierId} - Helpy" }
                    };

                do
                {
                    var BoynerApiUrl = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/products?size={size}&page={page}";

                    BoynerFilterProductPageResult = this._httpHelper.Get<BoynerFilterProductPage>(
                        BoynerApiUrl,
                        "Basic",
                        authorization,
                        customHeader);

                    if (BoynerFilterProductPageResult != null && BoynerFilterProductPageResult.Content != null)
                        foreach (var theContent in BoynerFilterProductPageResult.Content)
                        {
                            var marketplaceProductStatus = new MarketplaceProductStatus
                            {
                                MarketplaceId = this.MarketplaceId,
                                MarketplaceProductId = theContent.ProductMainId,
                                Barcode = theContent.Barcode,
                                StockCode = null,
                                Opened = true,
                                OnSale = theContent.Onsale,
                                Locked = theContent.Locked || theContent.Archived,

                                Url = theContent.ProductContentId > 0 ? $"https://www.Boyner.com/a/a-p-{theContent.ProductContentId}" : "",
                                ListUnitPrice = (decimal)theContent.ListPrice,
                                UnitPrice = (decimal)theContent.SalePrice,
                                Stock = theContent.Quantity
                            };
                            response.Data.Add(marketplaceProductStatus);
                        }

                    page++;
                } while ((BoynerFilterProductPageResult.Page + 1) < BoynerFilterProductPageResult.TotalPages);

                response.Success = true;
            });
        }

        #endregion

        #region Category Methods
        protected override DataResponse<List<CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new() { Orders = new List<Order>() };

                var authorization = this._configuration.Authorization;
                var size = 200;
                var page = 0;
                var totalPages = 0;
                var preUrl = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{this._configuration.SupplierId}/orders";

                do
                {

                    var url = string.Empty;

                    if (orderRequest.StartDate.HasValue && orderRequest.EndDate.HasValue)
                    {
                        long startDate = orderRequest.StartDate.Value.ConvertToUnixTimeStamp();
                        long endDate = orderRequest.EndDate.Value.ConvertToUnixTimeStamp();
                        string status = "Created,Picking,Shipped,Delivered";

                        url = $"{preUrl}?page={page}&size={size}&startDate={startDate}&endDate={endDate}&status={status}";
                    }
                    else if (string.IsNullOrEmpty(orderRequest.MarketplaceOrderCode))
                    {
                        string status = "Created";
                        string orderByField = "PackageLastModifiedDate";
                        string orderByDirection = "DESC";

                        url = $"{preUrl}?page={page}&size={size}&orderByField={orderByField}&orderByDirection={orderByDirection}&status={status}";
                    }
                    else
                    {
                        url = $"{preUrl}?PackageNo={orderRequest.MarketplaceOrderCode}";
                    }

                    var customHeader = new Dictionary<string, string>
                    {
                        {"User-Agent",$"{this._configuration.SupplierId} - Helpy" }
                    };

                    var BoynerOrderPages = _httpHelper.Get<BoynerOrderPage>(url, "Basic", authorization, customHeader);
                    totalPages = BoynerOrderPages.TotalPages;

                    /*
                     * Boyner servisinden cevap gelip gelmediği kontrol edilir.
                     */
                    if (BoynerOrderPages != null && BoynerOrderPages.Content != null && BoynerOrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(BoynerOrderPages.Content));
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "";

                        break;
                    }

                    page++;

                } while (page < totalPages);

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderTypeUpdateResponse>>((response) =>
            {
                if (orderTypeUpdateRequest.MarketplaceStatus == MarketplaceStatus.Picking)
                {
                    var authorization = this._configuration.Authorization;
                    var supplierId = _configuration.SupplierId;
                    var marketplaceOrderNumber = orderTypeUpdateRequest.Order.OrderCode;
                    var url = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/shipmentpackages/{orderTypeUpdateRequest.Order.OrderCode}/status/Picking";

                    var customHeader = new Dictionary<string, string>
                    {
                        {"User-Agent",$"{this._configuration.SupplierId} - Helpy" }
                    };


                    var requestObject = new BoynerShipmentPackage
                    {
                        lines = orderTypeUpdateRequest.Order.OrderDetails.Select(od => new BoynerShipmentPackageLine
                        {
                            lineId = long.Parse(od.UId),
                            quantity = od.Quantity
                        }).ToList(),
                        status = "Picking"
                    };

                    var aa = _httpHelper.Put<BoynerShipmentPackage, Application.Common.Wrappers.Response>(requestObject, url, "Basic", authorization, customHeader);
                    response.Success = true;

                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }

        protected override ECommerce.Application.Common.Wrappers.Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<ECommerce.Application.Common.Wrappers.Response>((response) =>
            {
                response.Success = true;

                if (request.Status == MarketplaceStatus.Picking)
                {
                    var supplierId = this._configuration.SupplierId;
                    var authorization = this._configuration.Authorization;

                    var url = $"https://merchantapi.boyner.com.tr/sapigw/suppliers/{supplierId}/shipmentpackages/{request.OrderPicking.MarketPlaceOrderId}/status/Picking";

                    var customHeader = new Dictionary<string, string>
                    {
                        {"User-Agent",$"{this._configuration.SupplierId} - Helpy" }
                    };

                    var aa = _httpHelper.Put<BoynerShipmentPackage, Application.Common.Wrappers.Response>(new BoynerShipmentPackage
                    {
                        lines = request.OrderPicking.OrderDetails.Select(l => new BoynerShipmentPackageLine
                        {
                            lineId = Convert.ToInt64(l.Id),
                            quantity = l.Quantity
                        }).ToList(),
                        status = "Picking"
                    }, url, "Basic", authorization, customHeader);
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Boyner ile bağlantı kurulamıyor.";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse { Orders = new List<Order>() };

                //var page = 0;
                //var totalPages = 1;
                //var size = 200;
                //var authorization = this._configuration.Authorization;
                //var status = "Delivered";

                //do
                //{
                //    var startDate = DateTime.Now.AddDays(-14).ConvertToUnixTimeStamp();
                //    var endDate = DateTime.Now.ConvertToUnixTimeStamp();
                //    var url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&status={status}&startDate={startDate}&endDate={endDate}";
                //    var BoynerOrderPages = _httpHelper.Get<BoynerOrderPage>(url, "Basic", authorization);

                //    if (BoynerOrderPages != null && BoynerOrderPages.Content.Count > 0)
                //    {
                //        response.Data.Orders.AddRange(MapOrder(BoynerOrderPages.Content));
                //        response.Success = true;
                //    }

                //    totalPages = BoynerOrderPages.TotalPages;
                //    page++;
                //} while (page < totalPages);

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string status = "Cancelled,UnSupplied";

                var authorization = this._configuration.Authorization;

                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                //int totalPage = 1;
                //while ((page + 1) <= totalPage)
                //{
                //    BoynerOrderPage BoynerOrderPages = null;

                //    String url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&status={status}";

                //    BoynerOrderPages = _httpHelper.Get<BoynerOrderPage>(url, "Basic", authorization);

                //    totalPage = BoynerOrderPages.TotalPages;
                //    page++;

                //    if (BoynerOrderPages != null && BoynerOrderPages.Content != null && BoynerOrderPages.Content.Count > 0)
                //    {
                //        response.Data.Orders.AddRange(MapOrder(BoynerOrderPages.Content));
                //        response.Success = true;
                //    }
                //    else
                //    {
                //        response.Success = false;
                //        response.Message = "Errorfrom Boyner service";
                //    }
                //}

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string orderByDirection = "ASC";
                string status = "Returned";

                //var authorization = this._configuration.Authorization;

                //response.Data = new OrderResponse
                //{
                //    Orders = new List<Order>()
                //};

                //for (int i = 5; i >= 0; i--)
                //{
                //    int totalPage = 1;
                //    var startDate = DateTime.Now.ToUniversalTime().Date.AddDays(i * -1);
                //    var endDate = DateTime.Now.ToUniversalTime().Date.AddDays((i - 1) * -1);

                //    while ((page + 1) <= totalPage)
                //    {
                //        BoynerOrderPage BoynerOrderPages = null;

                //        BoynerOrderPages = _httpHelper.Get<BoynerOrderPage>($"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?page={page}&size={size}&orderByDirection={orderByDirection}&status={status}&startDate={startDate.ConvertToUnixTimeStamp()}&endDate={endDate.ConvertToUnixTimeStamp()}", "Basic", authorization);

                //        totalPage = BoynerOrderPages.TotalPages;
                //        page++;
                //        if (BoynerOrderPages != null && BoynerOrderPages.Content != null && BoynerOrderPages.Content.Count > 0)
                //        {
                //            response.Data.Orders.AddRange(MapOrder(BoynerOrderPages.Content));
                //            response.Success = true;
                //        }
                //        else
                //        {
                //            response.Success = false;
                //            response.Message = "Error from Boyner service";
                //        }
                //    }

                //    page = 0;
                //}

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var authorization = this._configuration.Authorization;

                //var BoynerOrderPages = _httpHelper.Get<BoynerOrderPage>($"{_baseUrl}/suppliers/{this._configuration.SupplierId}/orders?orderNumber={orderRequest.MarketplaceOrderCode}", "Basic", authorization);

                //var BoynerOrder = BoynerOrderPages.Content.FirstOrDefault(x => x.ShipmentPackageStatus.ToLower() == "cancelled" && x.CargoTrackingNumber == orderRequest.TrackingCode);
                //if (BoynerOrder != null)
                //{
                //    response.Data.IsCancel = true; //iptal mi
                //    response.Message = $"Siparişiniz iptal statüsündedir. Boyner panelinden siparişinizi kontrol ediniz.";
                //}
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Boyner ile bağlantı kurulamıyor.";
            });

        }

        //public DataResponse<List<KeyValue<string, string>>> GetChangableCargoCompanies(string packageNumber)
        //{
        //    return new DataResponse<List<KeyValue<string, string>>>
        //    {
        //        Data = new List<KeyValue<string, string>> {
        //            new KeyValue<string, string>{ Key = "YKMP", Value = "Yurtiçi Kargo"},
        //            new KeyValue<string, string>{ Key = "ARASMP", Value = "Aras Kargo"},
        //            new KeyValue<string, string>{ Key = "SURATMP", Value = "Sürat Kargo"},
        //            new KeyValue<string, string>{ Key = "HOROZMP", Value = "Horoz Lojistik"},
        //            new KeyValue<string, string>{ Key = "MNGMP", Value = "MNG Kargo"},
        //            new KeyValue<string, string>{ Key = "PTTMP", Value = "PTT Kargo"},
        //            new KeyValue<string, string>{ Key = "TEXMP", Value = "Boyner Express"},
        //            new KeyValue<string, string>{ Key = "UPSMP", Value = "UPS"}
        //        },
        //        Success = true
        //    };
        //}

        //public DataResponse<ChangeCargoCompanyResponse> ChangeCargoCompany(string packageNumber, string cargoCompanyId)
        //{
        //    return ExceptionHandler.ResultHandle<DataResponse<ChangeCargoCompanyResponse>>(response =>
        //    {
        //        var authorization = this._configuration.Authorization;
        //        var url = $"{_baseUrl}/suppliers/{this._configuration.SupplierId}/shipment-packages/{packageNumber}/cargo-providers";
        //        var shipmentPackageRequest = new ShipmentPackageRequest
        //        {
        //            cargoProvider = cargoCompanyId
        //        };

        //        response.Success = _httpHelper.Put(shipmentPackageRequest, url, "Basic", authorization);
        //        response.Data = new ChangeCargoCompanyResponse
        //        {
        //            Retake = true
        //        };

        //        var shipmentCompanyId = string.Empty;
        //        switch (cargoCompanyId)
        //        {
        //            case "YKMP":
        //                shipmentCompanyId = ShipmentCompanies.YurticiKargo;
        //                break;
        //            case "ARASMP":
        //                shipmentCompanyId = ShipmentCompanies.ArasKargo;
        //                break;
        //            case "SURATMP":
        //                shipmentCompanyId = ShipmentCompanies.SuratKargo;
        //                break;
        //            case "HOROZMP":
        //                shipmentCompanyId = ShipmentCompanies.HorozLojistik;
        //                break;
        //            case "MNGMP":
        //                shipmentCompanyId = ShipmentCompanies.MngKargo;
        //                break;
        //            case "PTTMP":
        //                shipmentCompanyId = ShipmentCompanies.PttKargo;
        //                break;
        //            case "TEXMP":
        //                shipmentCompanyId = ShipmentCompanies.BoynerExpress;
        //                break;
        //            case "UPSMP":
        //                shipmentCompanyId = ShipmentCompanies.Ups;
        //                break;
        //        }

        //        response.Data.ShipmentCompanyId = shipmentCompanyId;
        //    });
        //}
        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {
                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        protected override ECommerce.Application.Common.Wrappers.Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            return ExceptionHandler.ResultHandle<ECommerce.Application.Common.Wrappers.Response>((response) =>
            {

                var authorization = this._configuration.Authorization;

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(BoynerConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.SupplierId = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "SupplierId").Value;
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ApiKey").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ApiSecretKey").Value;
        }
        #endregion

        #region Helper Methods

        private List<Order> MapOrder(List<BoynerOrder> providerOrders)
        {
            List<Order> orders = new();

            foreach (var pOrder in providerOrders)
            {
                if (string.IsNullOrEmpty(pOrder.CargoTrackingNumber) || pOrder.CargoTrackingNumber == "0") continue;
                try
                {
                    var shipmentCompanyId = "ARS";
                    if (!string.IsNullOrEmpty(pOrder.CargoTrackingNumber))
                    {
                        if (pOrder.CargoTrackingNumber.StartsWith("909"))
                            shipmentCompanyId = ShipmentCompanies.YurticiKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("726"))
                            shipmentCompanyId = ShipmentCompanies.ArasKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("727"))
                            shipmentCompanyId = ShipmentCompanies.SuratKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("728"))
                            shipmentCompanyId = ShipmentCompanies.MngKargo;
                        else if (pOrder.CargoTrackingNumber.StartsWith("729"))
                            shipmentCompanyId = ShipmentCompanies.Ups;
                        else if (pOrder.CargoTrackingNumber.StartsWith("734"))
                            shipmentCompanyId = ShipmentCompanies.PttKargo;
                    }

                    var customer = pOrder.CustomerFullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var shipmentAddress = pOrder.ShipmentAddress.FullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var invoiceAddress = pOrder.InvoiceAddress.FullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    var customerFirstName = customer.Length > 2 ? customer[0] + " " + customer[1] : customer[0];
                    var shipmentAddressFirstName = shipmentAddress.Length > 2 ? shipmentAddress[0] + " " + shipmentAddress[1] : shipmentAddress[0];
                    var invoiceAddressFirstName = invoiceAddress.Length > 2 ? invoiceAddress[0] + " " + invoiceAddress[1] : invoiceAddress[0];


                    Order order = new()
                    {
                        UId = $"{Application.Common.Constants.Marketplaces.Boyner}_{pOrder.OrderNumber}-{pOrder.CargoTrackingNumber}",
                        OrderSourceId = OrderSources.Pazaryeri,
                        MarketplaceId = Application.Common.Constants.Marketplaces.Boyner,
                        OrderCode = pOrder.OrderNumber,
                        OrderTypeId = "OS",
                        Micro = false,
                        Commercial = pOrder.Commercial && string.IsNullOrEmpty(pOrder.InvoiceAddress.TaxNumber) == false,
                        TotalAmount = pOrder.TotalPrice,
                        ListTotalAmount = pOrder.GrossAmount,
                        Discount = pOrder.TotalDiscount,
                        OrderDate = pOrder.OrderDate.ConvertToDateTime(),
                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                        {
                            PackageNumber = pOrder.Id.ToString(),
                            TrackingCode = pOrder.CargoTrackingNumber,
                            ShipmentTypeId = "PND",
                            TrackingUrl = pOrder.CargoTrackingLink,
                            ShipmentCompanyId = shipmentCompanyId //.ToLower() == "Boyner express marketplace" ? "TYEX" : "SRTTY" //Lafaba için yapıldı fatura kesilirken lazım oluyor
                        },
                        Customer = new Customer()
                        {
                            FirstName = customerFirstName,
                            LastName = customer.Length > 1 ? customer[customer.Length - 1] : "",
                            Mail = pOrder.CustomerEmail,
                            TaxNumber = pOrder.TaxNumber,
                            Phone = "" //TODO set phone number
                        },
                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                        {
                            FirstName = shipmentAddressFirstName,
                            LastName = shipmentAddress.Length > 1 ? shipmentAddress[1] : "",
                            Country = pOrder.ShipmentAddress.CountryCode.ToUpper() == "TR" ? "Türkiye" : "",
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.ShipmentAddress.Phone, //TODO set phone number
                            City = pOrder.ShipmentAddress.CityName,
                            District = pOrder.ShipmentAddress.DistrictName,
                            Neighborhood = pOrder.ShipmentAddress.NeighborhoodName,
                            Address = $"{pOrder.ShipmentAddress.Address1} {pOrder.ShipmentAddress.Address2}"
                        },
                        OrderInvoiceAddress = new OrderInvoiceAddress()
                        {
                            FirstName = invoiceAddressFirstName,
                            LastName = invoiceAddress.Length > 1 ? invoiceAddress[1] : "",
                            Email = pOrder.CustomerEmail,
                            Phone = pOrder.InvoiceAddress.Phone, //TODO set phone number
                            City = pOrder.InvoiceAddress.CityName,
                            District = pOrder.InvoiceAddress.DistrictName,
                            Address = false
                                ? pOrder.InvoiceAddress.FullAddress
                                : $"{pOrder.InvoiceAddress.Address1} {pOrder.InvoiceAddress.Address2}",
                            Neighborhood = !string.IsNullOrEmpty(pOrder.InvoiceAddress.NeighborhoodName) ? pOrder.InvoiceAddress.NeighborhoodName : pOrder.InvoiceAddress.DistrictName,
                            TaxNumber = pOrder.Commercial ? pOrder.InvoiceAddress.TaxNumber : "",
                            TaxOffice = pOrder.Commercial ? pOrder.InvoiceAddress.TaxOffice : "",
                            Country = pOrder.InvoiceAddress.CountryCode

                        },
                        Payments = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment>()
                    };
                    order.Payments.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.Payment()
                    {
                        CurrencyId = "TRY",
                        PaymentTypeId = "OO",
                        Amount = pOrder.TotalPrice,
                        Date = pOrder.OrderDate.ConvertToDateTime()
                    });

                    order.OrderPicking = new OrderPicking
                    {
                        MarketPlaceOrderId = pOrder.Id.ToString(),
                        OrderDetails = new List<OrderPickingDetail>()
                    };

                    #region Order Details
                    order.OrderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();
                    foreach (var orderDetail in pOrder.Lines)
                    {
                        order.OrderDetails.Add(new ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                        {
                            UId = orderDetail.Id.ToString(),
                            TaxRate = Convert.ToDouble(orderDetail.VatBaseAmount) / 100,
                            UnitPrice = orderDetail.Price,
                            ListPrice = orderDetail.Price,
                            UnitDiscount = orderDetail.Discount,
                            Product = new Product()
                            {
                                Barcode = orderDetail.Barcode.ToUpper(),
                                Name = orderDetail.ProductName,
                                StockCode = orderDetail.MerchantSku?.ToString()
                            },
                            Quantity = orderDetail.Quantity
                        });
                        order.OrderPicking.OrderDetails.Add(new OrderPickingDetail
                        {
                            Id = orderDetail.Id.ToString(),
                            Quantity = orderDetail.Quantity,
                        });
                    }
                    #endregion

                    orders.Add(order);
                }
                catch (Exception ex)
                {

                }
            }

            return orders;
        }

        #endregion
    }
}