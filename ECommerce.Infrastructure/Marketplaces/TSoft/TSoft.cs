﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities.Enum;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response.Categories;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response.Orders;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response.ProductFilter;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response.Property;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Marketplaces.TSoft
{
    public class TSoft : TokenMarketplaceBase<TSoftConfiguration>
    {
        #region Constants

        #endregion

        #region Fields
        private readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public TSoft(IHttpHelper httpHelper, IServiceProvider serviceProvider, ICacheHandler cacheHandler) : base(serviceProvider, cacheHandler, 29)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Properties
        public override string MarketplaceId => Application.Common.Constants.Marketplaces.TSoft;
        #endregion

        #region Methods
        #region Product Methods
        protected override DataResponse<string> CreateProductConcreate(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                response.Success = true;
            });
        }

        protected override DataResponse<string> UpdatePriceConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;
            });
        }

        protected override DataResponse<string> UpdateStockConcreate(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                response.Success = true;
            });
        }

        protected override DataResponse<List<ProductItem>> GetProductsConcreate()
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>> GetProductsStatusConcreate()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.MarketplaceProductStatus>>>(response =>
            {
                var token = this.GetAccessToken();
                List<Application.Common.Wrappers.Marketplaces.CategoryResponse> categories = new();
                response.Data = new List<MarketplaceProductStatus>();
                var url = this._configuration.Url;

                var page = 0;
                var totalCount = 0;

                do
                {
                    var getRequestProduct = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token.Data),
                new KeyValuePair<string, string>("limit", "500"),
                new KeyValuePair<string, string>("FetchSubProducts", "true"),
                //new KeyValuePair<string, string>("ProductCode", "M1ML1020110006-SİYAH"),
                new KeyValuePair<string, string>("start", (page * 500).ToString())
                    };


                    var listCategoryResponse = this._httpHelper.Post<TSoftFilterProduct>($"{this._configuration.Url}/rest1/product/get", getRequestProduct);


                    if (listCategoryResponse != null && listCategoryResponse.Data.Count > 0)
                    {

                        foreach (var theContent in listCategoryResponse.Data)
                        {
                            foreach (var sProduct in theContent.SubProducts)
                            {

                                var marketplaceProductStatus = new MarketplaceProductStatus
                                {
                                    MarketplaceId = this.MarketplaceId,
                                    Barcode = sProduct.Barcode,
                                    UUId = sProduct.SubProductCode,
                                    Opened = true,
                                    OnSale = sProduct.IsActive,
                                    Locked = false,
                                    Url = $"{url}/{theContent.SeoLink}",
                                    ListUnitPrice = Math.Round(sProduct.SellingPrice * (1 + theContent.Vat / 100m), 2),
                                    UnitPrice = Math.Round(sProduct.DiscountedSellingPrice * (1 + theContent.Vat / 100m), 2),
                                    Stock = sProduct.Stock
                                };
                                response.Data.Add(marketplaceProductStatus);
                            }

                        }
                        totalCount = Convert.ToInt32(listCategoryResponse.Summary.TotalRecordCount);
                    }
                    page++;
                } while ((page * 500) <= totalCount);

                response.Success = true;
            });
        }
        #endregion

        #region Category Methods
        protected override DataResponse<List<Application.Common.Wrappers.Marketplaces.CategoryResponse>> GetCategoriesConcreate(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.Wrappers.Marketplaces.CategoryResponse>>>((response) =>
            {

                var token = this.GetAccessToken();
                List<Application.Common.Wrappers.Marketplaces.CategoryResponse> categories = new();

                var getRequestCategory = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("token", token.Data),
                new KeyValuePair<string, string>("limit", "500"),
                    };

                var getPropertyGroup = new List<KeyValuePair<string, string>> {
                     new KeyValuePair<string, string>("token", token.Data),
                     new KeyValuePair<string, string>("limit", "2000"),

                    };



                var listCategoryResponse = this._httpHelper.Post<TSoftCategoryResponse>($"{this._configuration.Url}/rest1/category/getCategories", getRequestCategory);

                var listVariantTypeResponse = this._httpHelper.Post<TSoftPropertyGroupResponse>($"{this._configuration.Url}/rest1/subProduct/getPropertyGroups", getPropertyGroup);

                var listVariantValueResponse = this._httpHelper.Post<TSoftPropertyResponse>($"{this._configuration.Url}/rest1/subProduct/getProperties", getPropertyGroup);

                response.Data = new();
                List<TSoftCategoryItemPropertyGroupItem> tSoftCategoryItemPropertyGroupItems = new();

                foreach (var vLoop in listVariantTypeResponse.Data)
                {
                    if (listVariantValueResponse.Data.Count(x => x.GroupId == vLoop.GroupId) > 0)
                    {
                        tSoftCategoryItemPropertyGroupItems.Add(vLoop);
                    }
                }

                listCategoryResponse.Data.ForEach(cLoop =>
                {
                    response.Data.Add(new Application.Common.Wrappers.Marketplaces.CategoryResponse
                    {
                        Code = cLoop.CategoryCode,
                        Name = cLoop.CategoryName,
                        CategoryAttributes = tSoftCategoryItemPropertyGroupItems
                            .Select(v => new CategoryAttributeResponse
                            {
                                Code = v.GroupId,
                                Name = v.Name,
                                Mandatory = true,
                                AllowCustom = false,
                                MultiValue = false,
                                CategoryCode = cLoop.CategoryCode,
                                CategoryAttributesValues = listVariantValueResponse.Data.Where(x => x.GroupId == v.GroupId)
                                    .Select(vv => new CategoryAttributeValueResponse
                                    {
                                        Code = vv.PropertyId,
                                        Name = vv.Property,
                                        VarinatCode = v.GroupId
                                    })
                                    .ToList()
                            })
                            .ToList()
                    });
                });


                response.Success = true;
            });
        }
        #endregion

        #region Order Methods
        protected override DataResponse<OrderResponse> GetRawOrdersConcreate(RawOrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {



            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderTypeUpdateResponse> OrderTypeUpdateConcreate(OrderTypeUpdateRequest orderTypeUpdateRequest)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<CheckPreparingResponse> CheckPreparingOrderConcreate(OrderRequest request)
        {
            throw new NotImplementedException();
        }

        protected override DataResponse<OrderResponse> GetOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var token = this.GetAccessToken();

                var getOrder = new List<KeyValuePair<string, string>> {
                     new KeyValuePair<string, string>("token", token.Data),
                     new KeyValuePair<string, string>("OrderDateTimeStart", DateTime.Now.AddDays(-1).ToString("yyyy-MM-ddT00:00:00Z")),
                     new KeyValuePair<string, string>("limit", "500"),
                     new KeyValuePair<string, string>("OrderStatusId", "12"),
                     new KeyValuePair<string, string>("FetchProductData", "true"),
                     new KeyValuePair<string, string>("FetchDeliveryAddress", "true"),
                     new KeyValuePair<string, string>("FetchInvoiceAddress", "true")
                    };

                var listOrderResponse = this._httpHelper.Post<TSoftOrderResponse>($"{this._configuration.Url}/rest1/order/get", getOrder);

                if (listOrderResponse.Success)
                {
                    response.Data = new()
                    {
                        Orders = this.MapOrder(listOrderResponse.Data)
                    };

                    response.Success = true;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetCancelledOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetDeliveredOrdersConcreate(OrderRequest request)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        protected override DataResponse<OrderResponse> GetReturnedOrdersConcreate(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }


        #endregion

        #region Brand Methods
        protected override DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValueConcreate(string q, Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {


                response.Success = true;
            });
        }
        #endregion

        #region Invoice Methods
        //protected override Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        //{
        //    return new Response();
        //}
        #endregion
        #endregion

        #region Helper Methods
        protected override void BindConfigurationConcreate(TSoftConfiguration configuration, List<Domain.Entities.MarketplaceConfiguration> marketplaceConfigurations)
        {
            configuration.Url = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Url").Value;
            configuration.Token = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Token").Value;
            configuration.ExpirationTime = Convert.ToDateTime(marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ExpirationTime").Value, System.Globalization.CultureInfo.GetCultureInfo("tr-TR"));
            configuration.Username = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "User").Value;
            configuration.Password = marketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value;
        }

        protected override String GetAccessTokenConcreate()
        {
            if (this._configuration.ExpirationTime < DateTime.Now)
            {
                var tokenRequest = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("user", this._configuration.Username),
                new KeyValuePair<string, string>("pass", this._configuration.Password),
            };

                var tokenResponse = this._httpHelper.Post<TSoftTokenResponse>($"{this._configuration.Url}/rest1/auth/login/helpy", tokenRequest);
                var token = string.Empty;
                if (tokenResponse.Success == true)
                {
                    token = tokenResponse.Data[0].Token;
                }

                return token;
            }
            else
                return this._configuration.Token;




        }

        protected override Application.Common.Wrappers.Response PostInvoiceConcreate(string shipmentPackageId, string invoiceLink)
        {
            throw new NotImplementedException();
        }

        protected override Application.Common.Wrappers.Response OrderTypeUpdateConcreate(OrderPickingRequest request)
        {
            return new Application.Common.Wrappers.Response();
        }

        private List<Application.Common.Wrappers.Marketplaces.Order.Order> MapOrder(List<TSoftOrderItem> providerOrders)
        {

            List<Application.Common.Wrappers.Marketplaces.Order.Order> orders = new();

            foreach (var theProviderOrder in providerOrders)
            {
                try
                {


                    var deliveryAddress = theProviderOrder.DeliveryAddress.Name.Split(new char[] { ' ' }, StringSplitOptions.TrimEntries);
                    var deliveryAddressFirstName = deliveryAddress.Length > 2 ? deliveryAddress[0] + " " + deliveryAddress[1] : deliveryAddress[0];
                    var deliveryAddressLastName = deliveryAddress.Length > 1 ? deliveryAddress[deliveryAddress.Length - 1] : string.Empty;



                    var invoiceAddress = theProviderOrder.InvoiceAddress.Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var invoiceAddressFirstName = invoiceAddress.Length > 2 ? invoiceAddress[0] + " " + invoiceAddress[1] : invoiceAddress[0];
                    var invoiceAddressLastName = invoiceAddress.Length > 1 ? invoiceAddress[invoiceAddress.Length - 1] : string.Empty;



                    Application.Common.Wrappers.Marketplaces.Order.Order order = new()
                    {
                        UUId = theProviderOrder.OrderId,
                        UId = $"{Application.Common.Constants.Marketplaces.TSoft}_{theProviderOrder}-{theProviderOrder.OrderCode}",
                        OrderSourceId = OrderSources.Web,
                        OrderSource = "WB",
                        MarketplaceId = Application.Common.Constants.Marketplaces.TSoft,
                        OrderCode = theProviderOrder.OrderCode,
                        OrderTypeId = "OS",
                        TotalAmount = (decimal)theProviderOrder.OrderTotalPrice,
                        ListTotalAmount = (decimal)theProviderOrder.OrderTotalPrice,
                        Discount = theProviderOrder.DiscountTotal,
                        SurchargeFee = theProviderOrder.ServiceChargeWithVat,
                        CargoFee = theProviderOrder.CargoChargeWithVat,
                        OrderDate = theProviderOrder.OrderDate,
                        OrderShipment = new Application.Common.Wrappers.Marketplaces.Order.OrderShipment
                        {
                            TrackingCode = !string.IsNullOrEmpty(theProviderOrder.CargoTrackingCode) ? theProviderOrder.CargoTrackingCode : theProviderOrder.OrderCode,
                            ShipmentCompanyId = ConverCargoCompany(theProviderOrder.Cargo),
                            Payor = false,
                            PackageNumber = !string.IsNullOrEmpty(theProviderOrder.CargoTrackingCode) ? theProviderOrder.CargoTrackingCode : theProviderOrder.OrderCode

                        },
                        Customer = new Application.Common.Wrappers.Marketplaces.Order.Customer()
                        {
                            FirstName = deliveryAddressFirstName,
                            LastName = deliveryAddressLastName,
                            Mail = theProviderOrder.CustomerUsername,
                            Phone = theProviderOrder.CustomerPhone
                        },
                        OrderDeliveryAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderDeliveryAddress()
                        {
                            FirstName = deliveryAddressFirstName,
                            LastName = deliveryAddressLastName,
                            Country = theProviderOrder.DeliveryAddress.Country,
                            Email = theProviderOrder.CustomerUsername,
                            Phone = theProviderOrder.DeliveryAddress.Phone,
                            City = theProviderOrder.DeliveryAddress.City,
                            District = theProviderOrder.DeliveryAddress.Town,
                            Neighborhood = theProviderOrder.DeliveryAddress.Neighbourhood,
                            Address = $"{theProviderOrder.DeliveryAddress.Address}"
                        },
                        OrderInvoiceAddress = new Application.Common.Wrappers.Marketplaces.Order.OrderInvoiceAddress()
                        {
                            FirstName = invoiceAddressFirstName,
                            LastName = invoiceAddressLastName,
                            Email = theProviderOrder.CustomerUsername,
                            Phone = theProviderOrder.InvoiceAddress.Phone,
                            City = theProviderOrder.InvoiceAddress.City,
                            District = theProviderOrder.InvoiceAddress.Town,
                            Address = theProviderOrder.InvoiceAddress.Address,
                            Neighborhood = theProviderOrder.InvoiceAddress.Neighbourhood,
                            TaxNumber = theProviderOrder.InvoiceAddress.Taxno,
                            TaxOffice = theProviderOrder.InvoiceAddress.Taxdep
                        },
                        Payments = new List<Application.Common.Wrappers.Marketplaces.Order.Payment>
                        {
                            new Application.Common.Wrappers.Marketplaces.Order.Payment
                            {
                                Amount = theProviderOrder.OrderTotalPrice,
                                CurrencyId = "TRY",
                                Date = DateTime.Now,
                                PaymentTypeId = ConvertPaymentType(theProviderOrder.PaymentType,theProviderOrder.Bank),
                                Paid =true,
                                BankId = ConvertBankType(theProviderOrder.BankTransactionVendor)
                            }
                        }
                    };

                    order.OrderPicking = new Application.Common.Parameters.Order.OrderPicking
                    {
                        MarketPlaceOrderId = theProviderOrder.OrderId.ToString(),
                        OrderDetails = new List<Application.Common.Parameters.Order.OrderPickingDetail>()
                    };

                    order.OrderDetails = new List<Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();
                    foreach (var theOrderLineItem in theProviderOrder.OrderDetails)
                    {
                        order.OrderDetails.Add(new Application.Common.Wrappers.Marketplaces.Order.OrderDetail()
                        {
                            UUId = theOrderLineItem.OrderProductId,
                            UId = theOrderLineItem.OrderProductId.ToString(),
                            UnitPrice = theOrderLineItem.SellingPrice,
                            ListPrice = theOrderLineItem.SellingPrice,
                            UnitDiscount = 0M,
                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product()
                            {
                                Barcode = theOrderLineItem.Barcode,
                                Name = theOrderLineItem.ProductName
                            },
                            Quantity = theOrderLineItem.Quantity,
                            TaxRate = Convert.ToDouble(theOrderLineItem.Vat) / 100
                        });
                        order.OrderPicking.OrderDetails.Add(new Application.Common.Parameters.Order.OrderPickingDetail
                        {
                            Id = theOrderLineItem.OrderProductId.ToString(),
                            Quantity = theOrderLineItem.Quantity,
                        });
                    }

                    orders.Add(order);
                }
                catch (Exception ex)
                {

                }
            }

            return orders;


        }

        private string ConvertPaymentType(string paymentType, string bank)
        {
            string text = "OO";

            if (paymentType == "Havale / EFT")
            {
                text = "C";
            }
            else if (paymentType == "Kapıda Ödeme")
            {
                text = bank == "Nakit" ? "DO" : "PDO";
            }

            return text;
        }


        private BankType? ConvertBankType(string bank)
        {
            BankType? bankType = null;

            if (bank.Contains("HepsiPay"))
            {
                bankType = BankType.HepsiPay;
            }
            else if (bank.Contains("Sipay"))
            {
                bankType = BankType.Sipay;
            }
            else if (bank.Contains("PayTr"))
            {
                bankType = BankType.PayTr;
            }

            return bankType;
        }

        private string ConverCargoCompany(string shipmentType)
        {
            string text = "OT";
            switch (shipmentType)
            {
                case "Aras Kargo":
                    text = "ARS";
                    break;
                case "UPS":
                    text = "U";
                    break;
                case "MNG Kargo":
                    text = "M";
                    break;
                case "MIZALLE EXPRESS":
                    text = "MIZALLEEXPRESS";
                    break;
                case "Yurtiçi Kargo":
                    text = "Y";
                    break;
            }
            return text;
        }

        #endregion
    }
}