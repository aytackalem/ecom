﻿using ECommerce.Application.Common.Interfaces.Marketplaces;
using System;

namespace ECommerce.Infrastructure.Marketplaces.TSoft
{
    public class TSoftConfiguration : UsernamePasswordConfigurationBase
    {
        #region Properties
        public string Token { get; set; }
        public string Url { get; set; }
        public DateTime ExpirationTime { get; set; }
        #endregion
    }
}
