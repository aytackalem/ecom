﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response
{
    public class TSoftMessageResponse
    {
        public string code { get; set; }
        public List<string> text { get; set; }
        public List<object> errorField { get; set; }
    }
}
