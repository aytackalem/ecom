﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response.Property
{
    public class TSoftCategoryItemPropertyItem
    {
        public string PropertyId { get; set; }
        public string GroupId { get; set; }
        public string Property { get; set; }
        public string ColorCode { get; set; }
    }


    public class TSoftPropertyResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<TSoftCategoryItemPropertyItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

}
