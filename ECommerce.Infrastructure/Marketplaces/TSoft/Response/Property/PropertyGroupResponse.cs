﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response.Property
{
    public class TSoftCategoryItemPropertyGroupItem
    {
        public string GroupId { get; set; }
        public string Name { get; set; }
        public string PropertyCode { get; set; }
        public string GroupName { get; set; }
    }


    public class TSoftPropertyGroupResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<TSoftCategoryItemPropertyGroupItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

}
