﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response.ProductFilter
{
    public class TSoftFilterProduct
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<TSoftFilterProductItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }

    public class TSoftFilterProductItem
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultCategoryId { get; set; }
        public string DefaultCategoryName { get; set; }
        public string DefaultCategoryPath { get; set; }
        public string SupplierProductCode { get; set; }
        public string Barcode { get; set; }
        public string Stock { get; set; }
        public string IsActive { get; set; }
        public string IsApproved { get; set; }
        public string HasSubProducts { get; set; }
        public string ComparisonSites { get; set; }
        public decimal Vat { get; set; }
        public string CurrencyId { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string DiscountedPrice { get; set; }
        public string SellingPriceVatIncludedNoDiscount { get; set; }
        public string SellingPriceVatIncluded { get; set; }
        public string SeoLink { get; set; }
        public string StockUnit { get; set; }
        public string StockUnitId { get; set; }
        public string SearchKeywords { get; set; }
        public string DisplayOnHomepage { get; set; }
        public string IsNewProduct { get; set; }
        public string OnSale { get; set; }
        public string IsDisplayProduct { get; set; }
        public string VendorDisplayOnly { get; set; }
        public string DisplayWithVat { get; set; }
        public string Brand { get; set; }
        public string BrandId { get; set; }
        public string BrandLink { get; set; }
        public string Model { get; set; }
        public string ModelId { get; set; }
        public string SupplierId { get; set; }
        public string CustomerGroupDisplay { get; set; }
        public string ImageUrl { get; set; }
        //public string Magnifier { get; set; }
        //public string MemberMinOrder { get; set; }
        //public string MemberMaxOrder { get; set; }
        //public string VendorMinOrder { get; set; }
        //public string VendorMaxOrder { get; set; }
        //public string ShortDescription { get; set; }
        //public string SavingDate { get; set; }
        //public string CreateDateTimeStamp { get; set; }
        //public DateTime CreateDate { get; set; }
        //public string FilterGroupId { get; set; }
        //public string ListNo { get; set; }
        //public string OwnerId { get; set; }
        //public string UpdateDate { get; set; }
        //public string StockUpdateDate { get; set; }
        //public string PriceUpdateDate { get; set; }
        //public string IsActiveUpdateDate { get; set; }
        //public string StockUpdatePlatform { get; set; }
        //public string PriceUpdatePlatform { get; set; }
        //public string IsActiveUpdatePlatform { get; set; }
        //public string Gender { get; set; }
        //public string OpportunityProduct { get; set; }
        //public string OpportunityStart { get; set; }
        //public string OpportunityFinish { get; set; }
        //public string AgeGroup { get; set; }
        //public string CommentRate { get; set; }
        //public string CommentCount { get; set; }
        //public object DisablePaymentTypes { get; set; }
        //public object DisableCargoCompany { get; set; }
        //public string StatViews { get; set; }
        //public string StatRecommendations { get; set; }
        //public string HomepageSort { get; set; }
        //public string MostSoldSort { get; set; }
        //public string NewestSort { get; set; }
        //public string Point { get; set; }
        //public string EftRate { get; set; }
        //public string Numeric1 { get; set; }
        //public string HasImages { get; set; }
        //public string DefaultSubProductId { get; set; }
        //public string IsCatalog { get; set; }
        //public string RelatedProductsIds1 { get; set; }
        //public string RelatedProductsIds2 { get; set; }
        //public string RelatedProductsIds3 { get; set; }
        //public string FreeDeliveryMember { get; set; }
        //public string FreeDeliveryVendor { get; set; }
        //public string FreeDeliveryForProduct { get; set; }
        //public string SearchRank { get; set; }
        //public string VariantFeature1Title { get; set; }
        //public string VariantFeature2Title { get; set; }
        //public string CountTotalSales { get; set; }
        //public string Gtip { get; set; }
        //public string CBM2 { get; set; }
        //public string ProformaTitle { get; set; }
        //public string CountryOfOrigin { get; set; }
        //public string Label1 { get; set; }
        //public string Label2 { get; set; }
        //public string Label3 { get; set; }
        //public string Label4 { get; set; }
        //public string Label5 { get; set; }
        //public string Label6 { get; set; }
        //public string Label7 { get; set; }
        //public string Label8 { get; set; }
        //public string Label9 { get; set; }
        //public string Label10 { get; set; }
        //public string Label11 { get; set; }
        //public string Label15 { get; set; }
        //public string Additional1 { get; set; }
        //public string Additional2 { get; set; }
        //public string Additional3 { get; set; }
        //public string Additional4 { get; set; }
        //public string Additional5 { get; set; }
        //public string Additional6 { get; set; }
        //public string Currency { get; set; }
        //public string Supplier { get; set; }
        //public string DefaultCategoryCode { get; set; }
        //public string ImageUrlCdn { get; set; }
        public List<TSoftFilterSubProduct> SubProducts { get; set; }
    }

    public class TSoftFilterSubProduct
    {
        public string SubProductId { get; set; }
        public string SubProductCode { get; set; }
        public string MainProductId { get; set; }
        public string Barcode { get; set; }

        //public string Property1 { get; set; }
        //public object Property2 { get; set; }
        //public string PropertyId1 { get; set; }
        //public string PropertyId2 { get; set; }
        //public string Property1ListNo { get; set; }
        //public object Property2ListNo { get; set; }
        public int Stock { get; set; }
        public string CBM { get; set; }
        public string Weight { get; set; }
        public string SupplierSubProductCode { get; set; }
        public string BuyingPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal DiscountedSellingPrice { get; set; }
        public string VendorSellingPrice { get; set; }
        public bool IsActive { get; set; }

        //public string AddingDate { get; set; }
        //public string CreateDateTimeStamp { get; set; }
        //public DateTime CreateDate { get; set; }
        //public string UpdateDateTimeStamp { get; set; }
        //public DateTime UpdateDate { get; set; }
        //public string HasImage { get; set; }
        //public string OwnerId { get; set; }
        //public string OwnerName { get; set; }
        //public string Point { get; set; }
        //public string StockUpdateDate { get; set; }
        //public string PriceUpdateDate { get; set; }
    }

}
