﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response.Orders
{

    public class TSoftOrderItem
    {
        public string OrderId { get; set; }
        public string OrderCode { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderDateTimeStamp { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateDateTimeStamp { get; set; }
        public string ApprovalTime { get; set; }
        public string OrderStatusId { get; set; }
        public decimal OrderTotalPrice { get; set; }
        public decimal OrderSubtotal { get; set; }
        public decimal DiscountTotal { get; set; }
        public string Currency { get; set; }
        public string SiteDefaultCurrency { get; set; }
        public string Language { get; set; }
        public string ExchangeRate { get; set; }
        public string CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerUsername { get; set; }
        public string CustomerGroupId { get; set; }
        public string PaymentTypeId { get; set; }
        public string SubPaymentTypeId { get; set; }
        public string PaymentType { get; set; }
        public string Bank { get; set; }
        public string BankTransactionVendor { get; set; }
        public string CCBank { get; set; }
        public string HopiCampaign { get; set; }
        public string IsTransferred { get; set; }
        public string Installment { get; set; }
        public string InstallmentPlus { get; set; }
        public string PaymentInfo { get; set; }
        public string CargoTrackingCode { get; set; }
        public string CargoId { get; set; }
        public string CargoCode { get; set; }
        public string Cargo { get; set; }
        public string CargoPaymentMethod { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string ServiceName { get; set; }
        public string ServiceVatPercent { get; set; }

        public decimal ServiceChargeWithVat { get; set; }
        public decimal CargoChargeWithoutVat { get; set; }
        public decimal CargoChargeWithVat { get; set; }
        public string CargoChargeWithoutDiscount { get; set; }
        public string CargoVatPercent { get; set; }
        public string RepresentativeCode { get; set; }
        public string RepresentativeName { get; set; }
        public string Application { get; set; }
        public string WsOrderNumber { get; set; }
        public string CampaignDetail { get; set; }
        public string CampaignIds { get; set; }
        public string NonMemberShopping { get; set; }
        public string GeneralOrderNote { get; set; }
        public string DutyServiceRate { get; set; }
        public string DutyServiceTotal { get; set; }
        public string ShipmentTime { get; set; }
        public string ShipmentDeliveryTime { get; set; }
        public string IsDeleted { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string ErpPointVendor { get; set; }
        public string ErpPoint { get; set; }
        public string ErpPointAmount { get; set; }
        public string CargoServiceDate { get; set; }
        public string CargoServiceTime { get; set; }
        public string IsInvoiced { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceTime { get; set; }
        public string WaybillNumber { get; set; }
        public string EInvoiceLink { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public string DeliveryDateTimeStamp { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string CustomData { get; set; }
        public string CustomData2 { get; set; }
        public string CustomData3 { get; set; }
        public string CustomData4 { get; set; }
        public string OrderStatus { get; set; }
        public List<TSoftOrderDetail> OrderDetails { get; set; }
        public string VoucherCode { get; set; }
        public string VoucherDiscountType { get; set; }
        public string VoucherDiscountValue { get; set; }
        public TSoftDeliveryAddress DeliveryAddress { get; set; }
        public TSoftInvoiceAddress InvoiceAddress { get; set; }
        public string VoucherDiscountTypeId { get; set; }

    }

    public class TSoftDeliveryAddress
    {
        public string AddressId { get; set; }
        public string CustomerAdressId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Taxdep { get; set; }
        public string Taxno { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string OtherPhone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string Town { get; set; }
        public string TownCode { get; set; }
        public string Neighbourhood { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Zipcode { get; set; }
    }

    public class TSoftInvoiceAddress
    {
        public string AddressId { get; set; }
        public string CustomerAdressId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Taxdep { get; set; }
        public string Taxno { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string OtherPhone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string Town { get; set; }
        public string TownCode { get; set; }
        public string Neighbourhood { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Zipcode { get; set; }
    }


    public class TSoftOrderDetail
    {
        public string OrderProductId { get; set; }
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Vat { get; set; }
        public int Quantity { get; set; }
        public string StockUnit { get; set; }
        public string StockUnitId { get; set; }
        public string SellingCartPrice { get; set; }
        public string SellingPriceWithoutDiscount { get; set; }
        public string SellingPriceWithoutVat { get; set; }
        public decimal SellingPrice { get; set; }
        public string SellingCurrency { get; set; }
        public string SellingCurrencyExchangeRate { get; set; }
        public string BuyingPrice { get; set; }
        public string Barcode { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string DefaultCategoryId { get; set; }
        public string SubProductId { get; set; }
        public string SubProductCode { get; set; }
        public string Property1 { get; set; }
        public string Property2 { get; set; }
        public object Personalization { get; set; }
        public string OrderNote { get; set; }
        public string ImageUrl { get; set; }
        public string GiftPackage { get; set; }
        public string GiftNote { get; set; }
        public string PostStatus { get; set; }
        public string PostNote { get; set; }
        public string SupplyStatus { get; set; }
        public string SupplyNote { get; set; }
        public string OwnerId { get; set; }
        public string IsPackage { get; set; }
        public string Supplier { get; set; }
        public string Warehouse { get; set; }
        public string SupplierProductCode { get; set; }
        public string SupplierSubProductCode { get; set; }
        public string RefundCount { get; set; }
        public string StockField { get; set; }
        public string ShipmentId { get; set; }
        public double DiscountPercent { get; set; }
    }

    public class TSoftOrderResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public List<TSoftOrderItem> Data { get; set; }

        [JsonProperty("message")]
        public List<TSoftMessageResponse> Message { get; set; }

        [JsonProperty("summary")]
        public TSoftSummaryResponse Summary { get; set; }
    }


}
