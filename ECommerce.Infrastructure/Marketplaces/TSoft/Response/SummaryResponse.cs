﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Marketplaces.TSoft.Response
{
    public class TSoftSummaryResponse
    {
        [JsonProperty("totalRecordCount")]
        public string TotalRecordCount { get; set; }

        [JsonProperty("primaryKey")]
        public string PrimaryKey { get; set; }
    }
}
