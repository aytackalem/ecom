﻿using ECommerce.Accounting.Nebim;
using ECommerce.Accounting.Winka;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.EInvoice;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Infrastructure.Accounting.BizimHesap;
using ECommerce.Infrastructure.Accounting.Mikro;
using ECommerce.Infrastructure.Ads;
using ECommerce.Infrastructure.Communication;
using ECommerce.Infrastructure.EInvoice.TrendyolEFaturam;
using ECommerce.Infrastructure.ExternalCache.File;
using ECommerce.Infrastructure.Helpers;
using ECommerce.Infrastructure.MarketplaceCatalog;
using ECommerce.Infrastructure.Marketplaces.Boyner;
using ECommerce.Infrastructure.Marketplaces.CicekSepeti;
using ECommerce.Infrastructure.Marketplaces.Hepsiburada;
using ECommerce.Infrastructure.Marketplaces.Ikas;
using ECommerce.Infrastructure.Marketplaces.LCW;
using ECommerce.Infrastructure.Marketplaces.Modalog;
using ECommerce.Infrastructure.Marketplaces.Modanisa;
using ECommerce.Infrastructure.Marketplaces.Morhipo;
using ECommerce.Infrastructure.Marketplaces.N11;
using ECommerce.Infrastructure.Marketplaces.Omni;
using ECommerce.Infrastructure.Marketplaces.Pazarama;
using ECommerce.Infrastructure.Marketplaces.PttAvm;
using ECommerce.Infrastructure.Marketplaces.Trendyol;
using ECommerce.Infrastructure.Marketplaces.TrendyolEurope;
using ECommerce.Infrastructure.Marketplaces.TSoft;
using ECommerce.Infrastructure.Marketplaces.Vodafone;
using ECommerce.Infrastructure.MarketplacesV2.Ciceksepeti;
using ECommerce.Infrastructure.MarketplacesV2.Hepsiburada;
using ECommerce.Infrastructure.MarketplacesV2.Ikas;
using ECommerce.Infrastructure.MarketplacesV2.Lcw;
using ECommerce.Infrastructure.MarketplacesV2.Modalog;
using ECommerce.Infrastructure.MarketplacesV2.Modanisa;
using ECommerce.Infrastructure.MarketplacesV2.Morhipo;
using ECommerce.Infrastructure.MarketplacesV2.N11;
using ECommerce.Infrastructure.MarketplacesV2.N11V3;
using ECommerce.Infrastructure.MarketplacesV2.Pazarama;
using ECommerce.Infrastructure.MarketplacesV2.PttAvm;
using ECommerce.Infrastructure.MarketplacesV2.TrendyolEurope;
using ECommerce.Infrastructure.MarketplacesV2.TSoft;
using ECommerce.Infrastructure.Notification;
using ECommerce.Infrastructure.Payment;
using ECommerce.Infrastructure.Shipment.Aras;
using ECommerce.Infrastructure.Shipment.Artos;
using ECommerce.Infrastructure.Shipment.PozitifKargo;
using ECommerce.Infrastructure.Shipment.Ups;
using ECommerce.Persistence.Common.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Infrastructure
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddInfrastructureServices(this IServiceCollection serviceCollection, IConfiguration configuration = null)
        {
            #region Shipment Providers
            serviceCollection.AddScoped<UpsProvider>();

            serviceCollection.AddScoped<ArasProvider>();

            serviceCollection.AddScoped<ArtosProvider>();

            serviceCollection.AddScoped<FocusProvider>();

            serviceCollection.AddScoped<PozitifKargoProvider>();

            serviceCollection.AddTransient<ShipmentProviderServiceResolver>(serviceProvider => shipmentProviderId =>
            {
                IShipmentProvider p = shipmentProviderId switch
                {
                    "U" => serviceProvider.GetService<UpsProvider>(),
                    "ARS" => serviceProvider.GetService<ArasProvider>(),
                    "ARD" => serviceProvider.GetService<ArtosProvider>(),
                    "FC" => serviceProvider.GetService<FocusProvider>(),
                    "PK" => serviceProvider.GetService<PozitifKargoProvider>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Accounting Providers
            serviceCollection.AddScoped<WinkaProviderService>();

            serviceCollection.AddScoped<BizimHesapProviderService>();

            serviceCollection.AddScoped<MikroProviderService>();

            serviceCollection.AddScoped<NebimProviderService>();

            serviceCollection.AddTransient<AccountingProviderResolver>(serviceProvider => accountingProviderId =>
            {
                IAccountingProvider p = accountingProviderId switch
                {
                    "W" => serviceProvider.GetService<WinkaProviderService>(),
                    "BH" => serviceProvider.GetService<BizimHesapProviderService>(),
                    "M" => serviceProvider.GetService<MikroProviderService>(),
                    "NB" => serviceProvider.GetService<NebimProviderService>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region EInvoice Providers
            serviceCollection.AddScoped<TrendyolEFaturamProvider>();

            serviceCollection.AddTransient<EInvoiceProviderResolver>(serviceProvider => eInvoiceProviderId =>
            {
                IEInvoiceProvider p = eInvoiceProviderId switch
                {
                    "T" => serviceProvider.GetService<TrendyolEFaturamProvider>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Marketplaces
            serviceCollection.AddScoped<Trendyol>();

            serviceCollection.AddScoped<Hepsiburada>();

            serviceCollection.AddScoped<Ikas>();

            serviceCollection.AddScoped<N11>();


            serviceCollection.AddScoped<Pazarama>();

            serviceCollection.AddScoped<CicekSepeti>();

            serviceCollection.AddScoped<Omni>();

            serviceCollection.AddScoped<Vodafone>();

            serviceCollection.AddScoped<Morhipo>();

            serviceCollection.AddScoped<Modanisa>();

            serviceCollection.AddScoped<Modalog>();

            serviceCollection.AddScoped<PttAvm>();

            serviceCollection.AddScoped<TrendyolEurope>();

            serviceCollection.AddScoped<TSoft>();

            serviceCollection.AddScoped<Marketplaces.N11V3.N11V3>();

            serviceCollection.AddScoped<Lcw>();

            serviceCollection.AddScoped<Boyner>();

            serviceCollection.AddScoped<MarketplaceResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplace p = marketplaceId switch
                {
                    "TY" => serviceProvider.GetService<Trendyol>(),
                    "HB" => serviceProvider.GetService<Hepsiburada>(),
                    "IK" => serviceProvider.GetService<Ikas>(),
                    "N11" => serviceProvider.GetService<N11>(),
                    "PA" => serviceProvider.GetService<Pazarama>(),
                    "CS" => serviceProvider.GetService<CicekSepeti>(),
                    "OMNI" => serviceProvider.GetService<Omni>(),
                    "VO" => serviceProvider.GetService<Vodafone>(),
                    "M" => serviceProvider.GetService<Morhipo>(),
                    "MN" => serviceProvider.GetService<Modanisa>(),
                    "ML" => serviceProvider.GetService<Modalog>(),
                    "PTT" => serviceProvider.GetService<PttAvm>(),
                    "TYE" => serviceProvider.GetService<TrendyolEurope>(),
                    "N11V2" => serviceProvider.GetService<Marketplaces.N11V3.N11V3>(),
                    "TSoft" => serviceProvider.GetService<TSoft>(),
                    "LCW" => serviceProvider.GetService<Lcw>(),
                    "BY" => serviceProvider.GetService<Boyner>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Marketplace V2s
            serviceCollection.AddScoped<TrendyolV2>();

            serviceCollection.AddScoped<HepsiburadaV2>();

            serviceCollection.AddScoped<IkasV2>();

            serviceCollection.AddScoped<TSoftV2>();

            serviceCollection.AddScoped<N11V2>();

            serviceCollection.AddScoped<N11V3>();

            serviceCollection.AddScoped<PazaramaV2>();

            serviceCollection.AddScoped<CiceksepetiV2>();

            serviceCollection.AddScoped<MorhipoV2>();

            serviceCollection.AddScoped<PttAvmV2>();

            serviceCollection.AddScoped<ModanisaV2>();

            serviceCollection.AddScoped<ModalogV2>();

            serviceCollection.AddScoped<TrendyolEuropeV2>();

            serviceCollection.AddScoped<LcwV2>();

            serviceCollection.AddScoped<BoynerV2>();

            #region Resolvers
            serviceCollection.AddTransient<MarketplaceV2Resolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2 p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "PTT" => serviceProvider.GetService<PttAvmV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "TSOFT" => serviceProvider.GetService<TSoftV2>(),
                    _ => null
                };
                return p;
            });

            //Financial
            serviceCollection.AddTransient<MarketplaceV2CargoResolver>(serviceProvider => marketplaceId =>
            {
                ICargo p = marketplaceId switch
                {
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2CommissionResolver>(serviceProvider => marketplaceId =>
            {
                ICommission p = marketplaceId switch
                {
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2CategoryGetableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2CategoryGetable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "PTT" => serviceProvider.GetService<PttAvmV2>(),
                    "TSOFT" => serviceProvider.GetService<TSoftV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2OrderGetableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2OrderGetable p = marketplaceId switch
                {
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    _ => null
                };
                return p;
            });

            #region Trackable
            serviceCollection.AddTransient<MarketplaceV2CreateProductTrackableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2CreateProductTrackable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    "LCW" => serviceProvider.GetService<LcwV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2UpdateProductTrackableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2UpdateProductTrackable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2StockPriceTrackableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2StockPriceTrackable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "TYE" => serviceProvider.GetService<TrendyolEuropeV2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    "LCW" => serviceProvider.GetService<LcwV2>(),
                    "BY" => serviceProvider.GetService<BoynerV2>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Untrackable
            serviceCollection.AddTransient<MarketplaceV2CreateProductUntrackableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2CreateProductUntrackable p = marketplaceId switch
                {
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "TSoft" => serviceProvider.GetService<TSoftV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2StockPriceUntrackableResolver>(serviceProvider => marketplaceId =>
            {
                IMarketplaceV2StockPriceUntrackable p = marketplaceId switch
                {
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "PTT" => serviceProvider.GetService<PttAvmV2>(),
                    "TSoft" => serviceProvider.GetService<TSoftV2>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Exportable
            serviceCollection.AddTransient<MarketplaceV2CreateProductExportableResolver>(serviceProvider => marketplaceId =>
            {
                ICreateProductRequestExportable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    "TSoft" => serviceProvider.GetService<TSoftV2>(),
                    "LCW" => serviceProvider.GetService<LcwV2>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2UpdateProductExportableResolver>(serviceProvider => marketplaceId =>
            {
                IUpdateProductRequestExportable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<MarketplaceV2StockPriceExportableResolver>(serviceProvider => marketplaceId =>
            {
                IStockPriceRequestExportable p = marketplaceId switch
                {
                    "CS" => serviceProvider.GetService<CiceksepetiV2>(),
                    "HB" => serviceProvider.GetService<HepsiburadaV2>(),
                    "M" => serviceProvider.GetService<MorhipoV2>(),
                    "MN" => serviceProvider.GetService<ModanisaV2>(),
                    "TY" => serviceProvider.GetService<TrendyolV2>(),
                    "IK" => serviceProvider.GetService<IkasV2>(),
                    "N11" => serviceProvider.GetService<N11V2>(),
                    "PA" => serviceProvider.GetService<PazaramaV2>(),
                    "ML" => serviceProvider.GetService<ModalogV2>(),
                    "PTT" => serviceProvider.GetService<PttAvmV2>(),
                    "TYE" => serviceProvider.GetService<TrendyolEuropeV2>(),
                    "N11V2" => serviceProvider.GetService<N11V3>(),
                    "TSoft" => serviceProvider.GetService<TSoftV2>(),
                    "LCW" => serviceProvider.GetService<LcwV2>(),
                    "BY" => serviceProvider.GetService<BoynerV2>(),
                    _ => null
                };
                return p;
            });
            #endregion
            #endregion
            #endregion

            #region Notifications
            serviceCollection.AddScoped<INotificationService, NotificationService>();
            #endregion

            #region Communication
            #region Sms Providers
            serviceCollection.AddScoped<KobikomSmsProvider>();

            serviceCollection.AddScoped<NetGsmSmsProvider>();

            serviceCollection.AddTransient<SmsProviderResolver>(serviceProvider => smsProviderId =>
            {
                ISmsProvider p = smsProviderId switch
                {
                    "NG" => serviceProvider.GetService<NetGsmSmsProvider>(),
                    "KB" => serviceProvider.GetService<KobikomSmsProvider>(),
                    _ => null
                };
                return p;
            });
            #endregion

            #region Email Providers
            serviceCollection.AddTransient<IEmailProvider, DefaultEmailProvider>();

            serviceCollection.AddTransient<EmailProviderResolver>(serviceProvider => emailProviderId =>
            {
                IEmailProvider p = emailProviderId switch
                {
                    "D" => serviceProvider.GetService<DefaultEmailProvider>(),
                    _ => null
                };
                return p;
            });
            #endregion
            #endregion

            #region External Cache
            serviceCollection.AddTransient<IExternalCache, ExternalFileCache>();
            #endregion

            serviceCollection.AddTransient<IyzicoPosProvider>();

            serviceCollection.AddTransient<PttAkilliEsnafPosProvider>();

            serviceCollection.AddTransient<PosProviderResolver>(serviceProvider => posProviderId =>
            {
                IPosProvider p = posProviderId switch
                {
                    "IY" => serviceProvider.GetService<IyzicoPosProvider>(),
                    "PTT" => serviceProvider.GetService<PttAkilliEsnafPosProvider>(),
                    _ => null
                };
                return p;
            });

            serviceCollection.AddTransient<LicenseIyzicoPos>();

            serviceCollection.AddTransient<IExcelHelper, OpenXmlExcelHelper>();

            serviceCollection.AddTransient<IFacebookPixelService, FacebookPixelService>();

            serviceCollection.AddTransient<IGoogleAdsService, GoogleAdsService>();

            serviceCollection.AddTransient<IGoogleAnalyticsService, GoogleAnalyticsService>();

            serviceCollection.AddTransient<IGoogleTagManagerService, GoogleTagManagerService>();

            serviceCollection.AddTransient<IMarketplaceCategoryService, MarketplaceCategoryService>();

            serviceCollection.AddTransient<IMarketplaceVariantService, MarketplaceVariantService>();

            serviceCollection.AddTransient<IMarketplaceVariantValueService, MarketplaceVariantValueService>();

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();
        }
        #endregion
    }
}
