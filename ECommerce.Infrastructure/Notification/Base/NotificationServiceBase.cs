﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Notification.Base;
using ECommerce.Application.Common.Parameters.Notification;
using ECommerce.Application.Common.Wrappers;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Infrastructure.Notification.Base
{
    public abstract class NotificationServiceBase : INotificationService
    {
        #region Fields
        //private const string _apiUrl = "http://localhost:5300";
        private const string _apiUrl = "https://notification.helpy.com.tr";

        private readonly IHttpHelperV3 _httpHelperV3;

        private readonly string _endPoint;
        #endregion

        #region Constructors
        public NotificationServiceBase(IHttpHelperV3 httpHelperV3, string endPoint)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            this._endPoint = endPoint;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<Response> DeleteAsync(DeleteNotificationParameter parameter)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<Response>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Delete,
                    RequestUri = $"{_apiUrl}/{this._endPoint}?Brand={parameter.Brand}&CompanyId={parameter.CompanyId}&Id={parameter.Id}"
                });

                if (httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success)
                    response.Success = true;
            });
        }

        public async Task<PagedResponse<Application.Common.Wrappers.Notification.Notification>> GetAsync(GetParameter parameter)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<PagedResponse<Application.Common.Wrappers.Notification.Notification>>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<PagedResponse<Application.Common.Wrappers.Notification.Notification>>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{_apiUrl}/{this._endPoint}?Brand={parameter.Brand}&CompanyId={parameter.CompanyId}&ModelCode={parameter.ModelCode}&Barcode={parameter.Barcode}&StockCode={parameter.Stockcode}&MarketplaceId={parameter.MarketplaceId}&ProductInformationName={parameter.ProductInformationName}&Type={parameter.Type}&Page={parameter.Page}&PageRecordsCount={parameter.RecordsCount}"
                });

                if (httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success)
                {
                    response.Data = httpResponse.Content.Data;
                    response.PageRecordsCount = httpResponse.Content.PageRecordsCount;
                    response.RecordsCount = httpResponse.Content.RecordsCount;
                    response.Success = true;
                }
            });
        }

        public async Task<DataResponse<long>> GetAsync(GetCountParameter parameter)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<long>>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<DataResponse<long>>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{_apiUrl}/{this._endPoint}/Count?Brand={parameter.Brand}&CompanyId={parameter.CompanyId}&ModelCode={parameter.ModelCode}&Barcode={parameter.Barcode}&StockCode={parameter.Stockcode}&MarketplaceId={parameter.MarketplaceId}&ProductInformationName={parameter.ProductInformationName}"
                });

                if (httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success)
                {
                    response.Data = httpResponse.Content.Data;
                    response.Success = true;
                }
            });
        }

        public async Task<Response> PostAsync(InsertNotificationParameter parameter)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<Response>(async response =>
            {
                var httpResponse = await this._httpHelperV3.SendAsync<InsertNotificationParameter, Response>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request<InsertNotificationParameter>
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = $"{_apiUrl}/{this._endPoint}",
                    Request = parameter
                });

                if (httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && httpResponse.Content.Success)
                    response.Success = true;
            });
        }
        #endregion
    }
}
