﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Infrastructure.Notification.Base;

namespace ECommerce.Infrastructure.Notification
{
    public class NotificationService : NotificationServiceBase, INotificationService
    {
        public NotificationService(IHttpHelperV3 httpHelperV3) : base(httpHelperV3, "Notification")
        {
        }
    }
}
