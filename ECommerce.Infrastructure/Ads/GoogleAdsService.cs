﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using System.Linq;

namespace ECommerce.Infrastructure.Ads
{
    public class GoogleAdsService : IGoogleAdsService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public GoogleAdsService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<string> Layout()
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var googleConfiguration = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "Ads");
                }, string.Format(CacheKey.GoogleConfiguration, "Ads"), 30);

                if (googleConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Google Ads konfigurasyonu bulunamadı.";
                    return;
                }

                if (!googleConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Google Ads pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(googleConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Google Ads konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script async src=""https://www.googletagmanager.com/gtag/js?id={googleConfiguration.Code}""></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){{dataLayer.push(arguments);}}
  gtag('js', new Date());

  gtag('config', '{googleConfiguration.Code}');
</script>";
                response.Success = true;
                response.Message = "Google Ads kodu başarılı bir şekilde oluşturuldu.";
            });
        }

        public DataResponse<string> Done(LastOrderItem lastOrderItem)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var googleConfigurations = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .Where(gc => gc.Key == "Ads" || gc.Key == "AdsUIKey")
                        .ToList();
                }, string.Format(CacheKey.GoogleConfiguration, "AdsAdsUIKey"), 30);

                if (googleConfigurations == null || googleConfigurations.Count == 0 || googleConfigurations.Count < 2)
                {
                    response.Success = false;
                    response.Message = "Google Ads konfigurasyonu bulunamadı.";
                    return;
                }

                if (googleConfigurations.Any(gc => !gc.Active))
                {
                    response.Success = false;
                    response.Message = "Google Ads pasif.";
                    return;
                }

                if (googleConfigurations.Any(gc => string.IsNullOrEmpty(gc.Code)))
                {
                    response.Success = false;
                    response.Message = "Google Ads konfigurasyonu hatalı.";
                    return;
                }

                var adsCode = googleConfigurations.First(gc => gc.Key == "Ads");
                var adsUiCode = googleConfigurations.First(gc => gc.Key == "Ads");

                response.Data = @$"<script>
  gtag('event', 'conversion', {{
      'send_to': '{adsCode.Code}/{adsUiCode.Code}',
      'value': {lastOrderItem.Amount:#.##},
      'currency': 'TRY',
      'transaction_id': '{lastOrderItem.Id}'
  }});
</script>";
                response.Success = true;
                response.Message = "Google Ads kodu başarılı bir şekilde oluşturuldu.";
            });
        }
        #endregion
    }
}
