﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Infrastructure.Ads
{
    public class GoogleAnalyticsService : IGoogleAnalyticsService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public GoogleAnalyticsService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<string> Layout()
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var googleConfiguration = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "Analytics");
                }, string.Format(CacheKey.GoogleConfiguration, "Analytics"), 30);

                if (googleConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Google Analytics konfigurasyonu bulunamadı.";
                    return;
                }

                if (!googleConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Google Analytics pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(googleConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Google Analytics konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script async src=""https://www.googletagmanager.com/gtag/js?id={googleConfiguration.Code}""></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){{dataLayer.push(arguments);}}
  gtag('js', new Date());

  gtag('config', '{googleConfiguration.Code}');
</script>";
                response.Success = true;
                response.Message = "Google Analytics kodu başarılı bir şekilde oluşturuldu.";
            });
        }

        public DataResponse<string> Done(LastOrderItem lastOrderItem, List<ShoppingCartItem> shoppingCartItems)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                if (shoppingCartItems == null || shoppingCartItems.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Satın alma bilgileri alınamadı.";
                    return;
                }

                var googleConfiguration = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "Analytics");
                }, string.Format(CacheKey.GoogleConfiguration, "Analytics"), 30);

                if (googleConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Google Analytics konfigurasyonu bulunamadı.";
                    return;
                }

                if (!googleConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Google Analytics pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(googleConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Google Analytics konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script>
  window.dataLayer = window.dataLayer || [];
  dataLayer.push({{
  'transactionId': '{lastOrderItem.Id}',
  'transactionAffiliation': 'E-Ticaret',
  'transactionTotal': {lastOrderItem.Amount:#.##},
  'transactionTax': 0,
  'transactionShipping': 0,
  'transactionProducts': [{string.Join(",", shoppingCartItems.Select(sci => $@"{{
  'sku': '{sci.ProductInformationId}',
  'name': '{sci.ProductInformation.Name}',
  'category': '{sci.ProductInformation.Product.CategoryId}',
  'price': {sci.ProductInformation.ProductInformationPrice.UnitPrice.ToString("N0").Replace(",", ".")},
  'quantity': {sci.Quantity}
  }}"))}]
  }});
  gtag('create', '{googleConfiguration.Code}');
  gtag('event', 'purchase', {{
    'transaction_id': '{lastOrderItem.Id}',
    'affiliation': 'E-Ticaret',
    'value': '{lastOrderItem.Amount}',
    'currency': 'TRY',
    'tax': 0.18,
    'shipping': 0,
    'items': [{string.Join(",", shoppingCartItems.Select(sci => $@"{{
  'id': '{sci.ProductInformationId}',
  'name': '{sci.ProductInformation.Name}',
  'list_name': '',
  'brand': '{sci.ProductInformation.Product.BrandId}',
  'category': '{sci.ProductInformation.Product.CategoryId}',
  'variant': '',
  'list_position':1,
  'quantity': {sci.Quantity},
  'price': {sci.ProductInformation.ProductInformationPrice.UnitPrice.ToString("N0").Replace(",", ".")}
  }}"))}]
  }});
  </script>";
                response.Success = true;
                response.Message = "Google Analytics kodu başarılı bir şekilde oluşturuldu.";
            });
        }
        #endregion
    }
}