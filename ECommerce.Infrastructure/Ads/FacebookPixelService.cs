﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using System.Linq;

namespace ECommerce.Infrastructure.Ads
{
    public class FacebookPixelService : IFacebookPixelService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public FacebookPixelService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<string> Layout(string path)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var facebookConfiguration = this._cacheHandler.Handle<FacebookConfigration>(() =>
                {
                    return this
                        ._unitOfWork
                        .FacebookConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "Pixel");
                }, string.Format(CacheKey.FacebookConfiguration, "Pixel"), 30);

                if (facebookConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel konfigurasyonu bulunamadı.";
                    return;
                }

                if (!facebookConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(facebookConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script>
  !function(f,b,e,v,n,t,s)
  {{if(f.fbq)return;n=f.fbq=function(){{n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)}};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '{facebookConfiguration.Code}');
  fbq('track', '{path}');
  </script>
  <noscript><img height=""1"" width=""1"" style=""display:none""
  src=""https://www.facebook.com/tr?id={facebookConfiguration.Code}&ev=PageView&noscript=1"" 
  /></noscript>";
                response.Success = true;
                response.Message = "Facebook Pixel kodu başarılı bir şekilde oluşturuldu.";
            });
        }

        public DataResponse<string> Done(LastOrderItem lastOrderItem)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var facebookConfiguration = this._cacheHandler.Handle<FacebookConfigration>(() =>
                {
                    return this
                        ._unitOfWork
                        .FacebookConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "Pixel");
                }, string.Format(CacheKey.FacebookConfiguration, "Pixel"), 30);

                if (facebookConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel konfigurasyonu bulunamadı.";
                    return;
                }

                if (!facebookConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(facebookConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Facebook Pixel konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script>
  fbq('track', 'Purchase', {{currency: ""TRY"", value: {lastOrderItem.Amount:#.##}}});
</script>";
                response.Success = true;
                response.Message = "Facebook Pixel kodu başarılı bir şekilde oluşturuldu.";
            });
        }
        #endregion
    }
}
