﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.Interfaces.Ads;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using System.Linq;

namespace ECommerce.Infrastructure.Ads
{
    public class GoogleTagManagerService : IGoogleTagManagerService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public GoogleTagManagerService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<string> LayoutHead()
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var googleConfiguration = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "TagManager");
                }, string.Format(CacheKey.GoogleConfiguration, "TagManager"), 30);

                if (googleConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager konfigurasyonu bulunamadı.";
                    return;
                }

                if (!googleConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(googleConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<script>(function(w,d,s,l,i){{w[l]=w[l]||[];w[l].push({{'gtm.start':
  new Date().getTime(),event:'gtm.js'}});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  }})(window,document,'script','dataLayer','{googleConfiguration.Code}');</script>";
                response.Success = true;
                response.Message = "Google Tag Manager kodu başarılı bir şekilde oluşturuldu.";
            });
        }

        public DataResponse<string> LayoutBody()
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var googleConfiguration = this._cacheHandler.Handle<GoogleConfiguration>(() =>
                {
                    return this
                        ._unitOfWork
                        .GoogleConfigrationRepository
                        .DbSet()
                        .FirstOrDefault(gc => gc.Key == "TagManager");
                }, string.Format(CacheKey.GoogleConfiguration, "TagManager"), 30);

                if (googleConfiguration == null)
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager konfigurasyonu bulunamadı.";
                    return;
                }

                if (!googleConfiguration.Active)
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager pasif.";
                    return;
                }

                if (string.IsNullOrEmpty(googleConfiguration.Code))
                {
                    response.Success = false;
                    response.Message = "Google Tag Manager konfigurasyonu hatalı.";
                    return;
                }

                response.Data = @$"<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id={googleConfiguration.Code}""
height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>";
                response.Success = true;
                response.Message = "Google Tag Manager kodu başarılı bir şekilde oluşturuldu.";
            });
        }
        #endregion
    }
}