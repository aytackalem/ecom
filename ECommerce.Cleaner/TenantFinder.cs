﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Cleaner
{
    public class TenantFinder : ITenantFinder
    {
        #region Fields
        public int _tenantId;
        #endregion

        #region Constructors
        public TenantFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _tenantId;
        }

        public void Set(int tenantId)
        {
            _tenantId = tenantId;
        }
        #endregion
    }
}
