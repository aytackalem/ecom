﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Cleaner
{
    public class CompanyFinder : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Constructors
        public CompanyFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _companyId;
        }

        public void Set(int companyId)
        {
            _companyId = companyId;
        }
        #endregion
    }
}
