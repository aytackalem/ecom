﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Text;

namespace ECommerce.Cleaner
{
    internal class Program
    {
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();
            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.AddDbContext<ApplicationDbContext>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddSingleton<IDbService, DbService>();
            serviceCollection.AddSingleton<IDbNameFinder, DbNameFinder>();
            serviceCollection.AddSingleton<ITenantFinder, TenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, DomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, CompanyFinder>();
            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            var serviceCollection = ConfigureServiceCollection(configuration);

            List<string> dbNames = new();
            using (var scope = serviceCollection.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = dbService.GetNamesAsync().Result;
                if (getNamesResponse.Success)
                    dbNames = getNamesResponse.Data;
            }

            foreach (var theDbName in dbNames)
            {
                Console.Clear();
                Console.WriteLine(theDbName);

#if DEBUG
                if (theDbName != "Lafaba")
                    continue;
#endif

                using (var scope = serviceCollection.CreateScope())
                {
                    var serviceProvider = scope.ServiceProvider;

                    var unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();

                    var dbNameFinder = (DbNameFinder)serviceProvider.GetRequiredService<IDbNameFinder>();
                    dbNameFinder.Set(theDbName);

                    var tenants = unitOfWork.TenantRepository.DbSet().ToList();
                    foreach (var theTenant in tenants)
                    {
                        Console.WriteLine(theTenant.Name);

                        var tenantFinder = (TenantFinder)serviceProvider.GetRequiredService<ITenantFinder>();
                        tenantFinder.Set(theTenant.Id);

                        var domains = unitOfWork.DomainRepository.DbSet().ToList();
                        foreach (var theDomain in domains)
                        {
                            Console.WriteLine(theDomain.Name);

                            var domainFinder = (DomainFinder)serviceProvider.GetRequiredService<IDomainFinder>();
                            domainFinder.Set(theDomain.Id);

                            var companies = unitOfWork.CompanyRepository.DbSet().ToList();
                            foreach (var theCompany in companies)
                            {
                                Console.WriteLine(theCompany.Name);

                                var companyFinder = (CompanyFinder)serviceProvider.GetRequiredService<ICompanyFinder>();
                                companyFinder.Set(theCompany.Id);

                                var count = unitOfWork
                                    .OrderRepository
                                    .DbSet()
                                    .Where(
                                        o => o.Anonymized == false &&
                                        o.CreatedDate < DateTime.Now.Date.AddDays(-60))
                                    .Count();

                                var pageCount = (int)Math.Ceiling(count / 5M);

                                for (int i = 0; i < pageCount; i++)
                                {
                                    var orderIds = unitOfWork
                                        .OrderRepository
                                        .DbSet()
                                        .Where(
                                            o => o.Anonymized == false &&
                                            o.CreatedDate < DateTime.Now.Date.AddDays(-60))
                                        .OrderBy(o => o.Id)
                                        .Take(5)
                                        .Select(o => o.Id)
                                        .ToList();

                                    Update(serviceCollection, orderIds);
                                }
                            }
                        }
                    }
                }
            }
        }

        static void Update(IServiceProvider serviceCollection, List<int> orderIds)
        {
            using (var scope = serviceCollection.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();

                foreach (var theOrderId in orderIds)
                {
                    Console.WriteLine(theOrderId);

                    var order = unitOfWork
                        .OrderRepository
                        .DbSet()
                        .Include(o => o.Customer)
                            .ThenInclude(c => c.CustomerContact)
                        .Include(o => o.OrderDeliveryAddress)
                        .Include(o => o.OrderInvoiceInformation)
                        .Include(o => o.OrderShipments)
                        .FirstOrDefault(o => o.Id == theOrderId);

                    order.Customer.Name = Anonymize(order.Customer.Name);
                    order.Customer.Surname = Anonymize(order.Customer.Surname);

                    order.Customer.CustomerContact.TaxNumber = string.Empty;
                    order.Customer.CustomerContact.TaxOffice = string.Empty;
                    order.Customer.CustomerContact.Mail = string.Empty;
                    order.Customer.CustomerContact.Phone = string.Empty;

                    order.OrderInvoiceInformation.Address = string.Empty;
                    order.OrderInvoiceInformation.FirstName = Anonymize(order.OrderInvoiceInformation.FirstName);
                    order.OrderInvoiceInformation.LastName = Anonymize(order.OrderInvoiceInformation.LastName);
                    order.OrderInvoiceInformation.Mail = string.Empty;
                    order.OrderInvoiceInformation.Phone = string.Empty;
                    order.OrderInvoiceInformation.NeighborhoodId = 1;

                    order.OrderDeliveryAddress.Address = string.Empty;
                    order.OrderDeliveryAddress.PhoneNumber = string.Empty;
                    order.OrderDeliveryAddress.Recipient = Anonymize(order.OrderDeliveryAddress.Recipient);
                    order.OrderDeliveryAddress.NeighborhoodId = 1;

                    if (order.OrderShipments?.Count > 0)
                        foreach (var theOrderShipment in order.OrderShipments)
                        {
                            theOrderShipment.TrackingBase64 = string.Empty;
                            theOrderShipment.TrackingUrl = string.Empty;
                        }

                    order.Anonymized = true;

                    unitOfWork.OrderRepository.Update(order);
                }
            }
        }

        static string Anonymize(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            StringBuilder stringBuilder = new();

            var parts = text.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            foreach (var thePart in parts)
            {
                if (thePart.Length > 1)
                {
                    stringBuilder.Append(thePart[0]);
                    stringBuilder.Append("*** ");
                }
            }

            return stringBuilder.ToString().ToUpper().TrimEnd(' ');
        }
    }
}