﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Parameters.Order;

namespace ECommerce.Application.Packaging.Interfaces.Services.Base
{
    public interface IOrderService
    {
        #region Methods
        /// <summary>
        /// OrderId veya PackarBarcode gibi degerleri kullanarak siparis filtrelemeyi saglayan metod.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        DataResponse<Order> Get(GetParameter parameter);

        /// <summary>
        /// Paketlenmeyi bekleyen OS tipindeki siparişlerden ilk sırada olanı getiren metod.
        /// </summary>
        /// <param name="domainId"></param>
        /// <param name="marketplaceId"></param>
        /// <param name="shipmentCompanyId"></param>
        /// <param name="orderSourceId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        DataResponse<int> Reserve(ReserveParameter parameter);

        /// <summary>
        /// Siparis adetlerini donduren metod.
        /// </summary>
        /// <returns></returns>
        DataResponse<OrderSummary> Summary();

        /// <summary>
        /// İlgili siparişin paketlenip/paketlenmediği bilgisini veren metod.
        /// </summary>
        /// <param name="id">Sipariş id bilgisi.</param>
        /// <returns>Sipariş paketlenmiş ise "True" paketlenmemiş ise "False" değeri döner.</returns>
        DataResponse<bool> Packed(int id);

        /// <summary>
        /// Paketleniyor statusundeki siparisi onaylanmis siparis statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response Cancel(int id);

        /// <summary>
        /// Ilgili siparisi kargoya teslim edildi statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response Complete(int id);

        /// <summary>
        /// Ilgili siparisi temin edilemiyor statusune ceviren metod.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response NotAvailable(int id);
        #endregion
    }
}
