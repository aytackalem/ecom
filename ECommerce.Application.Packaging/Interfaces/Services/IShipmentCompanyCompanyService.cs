﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IShipmentCompanyCompanyService
    {
        DataResponse<List<ShipmentCompany>> Get();
    }
}
