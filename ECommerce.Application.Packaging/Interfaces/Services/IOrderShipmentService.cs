﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IOrderShipmentService
    {
        #region Methods
        Response Create (OrderShipment orderShipment);

        Response Update(OrderShipment orderShipment); 
        #endregion
    }
}
