﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IDomainService
    {
        DataResponse<List<DataTransferObjects.Domain>> Get();
    }
}
