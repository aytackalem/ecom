﻿using ECommerce.Application.Packaging.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IOrderSourceService
    {
        DataResponse<List<OrderSource>> Get();
    }
}
