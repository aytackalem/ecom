﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IOrderInvoiceInformationService
    {
        #region Methods
        /// <summary>
        /// Faturaya ilişkin bilgileri güncelleyen fonksiyon.
        /// </summary>
        /// <param name="orderInvoiceInformation">İlgili fatura bilgileri.</param>
        /// <returns>İşlem sonucu.</returns>
        Response Update(OrderInvoiceInformation orderInvoiceInformation); 




        #endregion
    }
}
