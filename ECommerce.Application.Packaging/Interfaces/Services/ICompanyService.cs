﻿using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface ICompanyService
    {
        DataResponse<List<DataTransferObjects.Company>> Get();
    }
}
