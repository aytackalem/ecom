﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface IMarketplaceService
    {
        #region Methods
        DataResponse<Marketplace> Get(string id);

        DataResponse<List<Marketplace>> Get();
        #endregion
    }
}
