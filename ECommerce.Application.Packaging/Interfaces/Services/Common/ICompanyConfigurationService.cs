﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Application.Packaging.Interfaces.Services
{
    public interface ICompanyConfigurationService
    {
        #region Methods
        DataResponse<List<KeyValue<string, string>>> Get();
        #endregion
    }
}
