﻿namespace ECommerce.Application.Packaging.Parameters.Order
{
    public class ReserveParameter
    {
        #region Properties
        public int DomainId { get; set; }

        public string MarketplaceId { get; set; }

        public string ShipmentCompanyId { get; set; }

        public int? OrderSourceId { get; set; }

        public int? OrderId { get; set; }
        #endregion
    }
}
