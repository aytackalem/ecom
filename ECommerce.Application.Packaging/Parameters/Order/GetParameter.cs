﻿namespace ECommerce.Application.Packaging.Parameters.Order
{
    public class GetParameter
    {
        #region Properties
        public int OrderId { get; set; }

        public string PackerBarcode { get; set; }
        #endregion
    }
}
