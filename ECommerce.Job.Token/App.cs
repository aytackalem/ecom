﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Job.Token
{

    public class Rootobject
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public string scope { get; set; }
        public string refresh_token { get; set; }
    }


    public class App
    {
        IUnitOfWork _unitOfWork;

        IHttpHelper _httpHelper;

        public App(IUnitOfWork unitOfWork, IHttpHelper httpHelper)
        {
            _unitOfWork = unitOfWork;
            _httpHelper = httpHelper;
        }

        public void Run()
        {
            try
            {
                var marketplaceCompany = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(mc => mc.MarketplaceConfigurations)
                    .First(mc => mc.Id == 58);
                var refreshToken = marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "RefreshToken").Value;
                var shopAddress = marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "ShopAddress").Value;
                var clientId = marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "ClientId").Value;
                var clientSecret = marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "ClientSecret").Value;

                var response = this
                    ._httpHelper
                    .Get<Rootobject>($"http://{shopAddress}/oauth/v2/token?grant_type=refresh_token&client_id={clientId}&client_secret={clientSecret}&refresh_token={refreshToken}", null, null, new Dictionary<string, string>());

                marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "AccessToken").Value = response.access_token;
                marketplaceCompany.MarketplaceConfigurations.First(mc => mc.Key == "RefreshToken").Value = response.refresh_token;

                this._unitOfWork.MarketplaceCompanyRepository.Update(marketplaceCompany);
                Console.WriteLine("TOKEN ALINDI");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
