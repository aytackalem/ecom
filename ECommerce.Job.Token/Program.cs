﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Token
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<App>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();



            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinderFinder>();
            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();
            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.AddScoped<App>();

            return serviceCollection.BuildServiceProvider();
        }
    }
}