﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace ECommerce.Job.Token
{
    public class AppConfigTenantFinder : ITenantFinder
    {
        #region Fields
        #endregion

        #region Constructors
        public AppConfigTenantFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return 9;
        }
        #endregion
    }
}
