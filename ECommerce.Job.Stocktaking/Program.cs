﻿using ECommerce.Accounting.Winka;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.Accounting.BizimHesap;
using ECommerce.Infrastructure.Accounting.Mikro;
using ECommerce.Infrastructure.Accounting.Winka.UnitOfWork;
using ECommerce.Infrastructure.Accounting.Winka.UnitOfWork.Base;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Stocktaking
{
    internal class Program
    {
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();
            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.AddDbContext<ApplicationDbContext>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddSingleton<IDbService, DbService>();
            serviceCollection.AddSingleton<IDbNameFinder, DbNameFinder>();
            serviceCollection.AddSingleton<ITenantFinder, TenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, DomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, CompanyFinder>();

            serviceCollection.AddSingleton<IWinkaUnitOfWork, WinkaUnitOfWork>();
            serviceCollection.AddSingleton<WinkaProviderService>();

            serviceCollection.AddScoped<IAccountingProviderService, AccountingProviderService>();
            serviceCollection.AddScoped<IAccountingProvider, WinkaProviderService>();
            serviceCollection.AddTransient<AccountingProviderResolver>(serviceProvider => accountingProviderId =>
            {
                IAccountingProvider p = accountingProviderId switch
                {
                    "W" => serviceProvider.GetService<WinkaProviderService>(),
                    "BH" => serviceProvider.GetService<BizimHesapProviderService>(),
                    "M" => serviceProvider.GetService<MikroProviderService>(),
                    _ => null
                };
                return p;
            });

            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            var serviceCollection = ConfigureServiceCollection(configuration);

            List<string> dbNames = new();
            using (var scope = serviceCollection.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = dbService.GetNamesAsync().Result;
                if (getNamesResponse.Success)
                    dbNames = getNamesResponse.Data;
            }

            foreach (var theDbName in dbNames)
            {
                Console.Clear();
                Console.WriteLine(theDbName);

#if DEBUG
                if (theDbName != "Lafaba")
                    continue;
#endif

                using (var scope = serviceCollection.CreateScope())
                {
                    var serviceProvider = scope.ServiceProvider;

                    var unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();

                    var dbNameFinder = (DbNameFinder)serviceProvider.GetRequiredService<IDbNameFinder>();
                    dbNameFinder.Set(theDbName);

                    var tenants = unitOfWork.TenantRepository.DbSet().ToList();
                    foreach (var theTenant in tenants)
                    {
                        Console.WriteLine(theTenant.Name);

                        var tenantFinder = (TenantFinder)serviceProvider.GetRequiredService<ITenantFinder>();
                        tenantFinder.Set(theTenant.Id);

                        var domains = unitOfWork.DomainRepository.DbSet().ToList();
                        foreach (var theDomain in domains)
                        {
                            Console.WriteLine(theDomain.Name);

                            var domainFinder = (DomainFinder)serviceProvider.GetRequiredService<IDomainFinder>();
                            domainFinder.Set(theDomain.Id);

                            var companies = unitOfWork.CompanyRepository.DbSet().ToList();
                            foreach (var theCompany in companies)
                            {
                                Console.WriteLine(theCompany.Name);

                                var companyFinder = (CompanyFinder)serviceProvider.GetRequiredService<ICompanyFinder>();
                                companyFinder.Set(theCompany.Id);

                                Work(serviceProvider);
                            }
                        }
                    }
                }
            }
        }

        static void Work(IServiceProvider serviceCollection)
        {
            using (var scope = serviceCollection.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();

                var stocktakings = unitOfWork
                    .StocktakingRepository
                    .DbSet()
                    .Where(s => s.StocktakingTypeId == StocktakingTypes.Onaylanmis)
                    .ToList();

                foreach (var theStocktaking in stocktakings)
                {
                    var accountingProviderService = serviceProvider.GetRequiredService<IAccountingProviderService>();
                    var createStocktakingResponse = accountingProviderService.CreateStocktaking(theStocktaking.Id);
                    if (createStocktakingResponse.Success &&
                        createStocktakingResponse.Data)
                    {
                        theStocktaking.StocktakingTypeId = StocktakingTypes.Tamamlanmis;
                        unitOfWork.StocktakingRepository.Update(theStocktaking);
                    }
                }
            }
        }
    }
}