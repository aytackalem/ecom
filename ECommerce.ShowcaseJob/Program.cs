﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Panel.Interfaces.Services;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Panel.Services;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ECommerce.ShowcaseJob
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();

            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {

            ServiceCollection serviceCollection = new ServiceCollection();

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddOptions();

            serviceCollection.AddScoped<ISettingService, SettingService>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });
            serviceCollection.AddSingleton<IApp, App>();
            serviceCollection.AddScoped<IImageHelper, ImageHelper>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<IShowcaseService, ShowcaseService>();
            serviceCollection.AddScoped<IProductService, ProductService>();
            serviceCollection.AddScoped<IRandomHelper, RandomHelper>();
            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();
            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();
            serviceCollection.AddScoped<ICacheHandler, CacheHandler>();
            serviceCollection.AddScoped<IExcelHelper>(x => null);
            serviceCollection.AddScoped<IProductInformationStockService, ProductInformationStockService>();
            serviceCollection.AddScoped<IProductInformationProductService, ProductInformationProductService>();


            serviceCollection.AddMemoryCache();

            return serviceCollection.BuildServiceProvider();
        }
    }
}
