﻿namespace ECommerce.ShowcaseJob
{
    public interface IApp
    {
        #region Methods
        void Run();
        #endregion
    }
}
