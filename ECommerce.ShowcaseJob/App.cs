﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Panel.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.ShowcaseJob
{
    public class App : IApp
    {
        #region Fields
        private readonly IShowcaseService _showcaseService;

        private readonly IProductService _productService;

        private readonly IUnitOfWork _unitOfWork;

        private Domain.Entities.Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Domain.Entities.Company _company;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public App(IShowcaseService showcaseService, IProductService productService, IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder)
        {
            #region Fields
            this._showcaseService = showcaseService;
            this._productService = productService;
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var tenants = this
  ._unitOfWork
  .TenantRepository
  .DbSet()
  .Include(t => t.TenantSetting)
  .ToList();

            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                if (theTenant.Id != 1) continue;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .Where(x => x.CompanySetting.IncludeECommerce)
                        .ToList();
                    foreach (var theCompany in companies)
                    {

                        ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;
                        this._company = theCompany;

                        var keyValueResponse = this._showcaseService.ReadAsKeyValue();
                        if (keyValueResponse.Success)
                        {
                            foreach (var dLoop in keyValueResponse.Data)
                            {
                                var showcaseResponse = this._showcaseService.Read(dLoop.Key);
                                if (showcaseResponse.Success)
                                {
                                    showcaseResponse.Data.RowComponents.ForEach(rc =>
                                    {
                                        rc.ColumnComponents.ForEach(cc =>
                                        {
                                            cc.ProductComponents.ForEach(pc =>
                                            {
                                                var entity = this
                                                    ._unitOfWork
                                                    .ProductInformationRepository
                                                    .DbSet()
                                                    .Include(pi => pi.ProductInformationTranslations)
                                                    .Include(pi => pi.ProductInformationPriceses)
                                                    .Include(pi => pi.ProductInformationPhotos)
                                                    .Where(pi => pi.Id == pc.ProductInformationId)
                                                    .FirstOrDefault();
                                                if (entity != null)
                                                {
                                                    pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                                                    pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                                                    pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                                                    pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                                                    pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                                                    pc.Url = entity.ProductInformationTranslations[0].Url;
                                                    pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                                                    pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                                                }

                                            });


                                            cc.SliderComponents.ForEach(sc =>
                                            {
                                                sc.ProductComponents.ForEach(pc =>
                                                {
                                                    var entity = this
                                                        ._unitOfWork
                                                        .ProductInformationRepository
                                                        .DbSet()
                                                        .Include(pi => pi.ProductInformationTranslations)
                                                        .Include(pi => pi.ProductInformationPriceses)
                                                         .Include(pi => pi.ProductInformationPhotos)
                                                        .Where(pi => pi.Id == pc.ProductInformationId)
                                                        .FirstOrDefault();
                                                    if (entity != null)
                                                    {
                                                        pc.ListUnitPrice = entity.ProductInformationPriceses[0].ListUnitPrice;
                                                        pc.UnitPrice = entity.ProductInformationPriceses[0].UnitPrice;
                                                        pc.VatRate = entity.ProductInformationPriceses[0].VatRate;
                                                        pc.Name = $"{entity.ProductInformationTranslations[0].Name}";
                                                        pc.VariantValuesDescription = entity.ProductInformationTranslations[0].VariantValuesDescription;
                                                        pc.Url = entity.ProductInformationTranslations[0].Url;
                                                        pc.SubTitle = entity.ProductInformationTranslations[0].SubTitle;
                                                        pc.FileName = entity.ProductInformationPhotos.FirstOrDefault()?.FileName;
                                                    }
                                                });
                                            });
                                        });
                                    });

                                    this._showcaseService.Update(showcaseResponse.Data);
                                }
                            }
                        }

                    }
                }
            }


        }
        #endregion
    }
}
