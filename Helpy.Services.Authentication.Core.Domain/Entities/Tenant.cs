﻿using Helpy.Services.Authentication.Core.Domain.Entities.Base;

namespace Helpy.Services.Authentication.Core.Domain.Entities
{
    public class Tenant : EntityBase<int>
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}