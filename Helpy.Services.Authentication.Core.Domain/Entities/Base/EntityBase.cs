﻿namespace Helpy.Services.Authentication.Core.Domain.Entities.Base
{
    public abstract class EntityBase<TId>
    {
        #region Properties
        public TId Id { get; set; }
        #endregion
    }
}
