﻿using Helpy.Services.Authentication.Core.Domain.Entities.Base;

namespace Helpy.Services.Authentication.Core.Domain.Entities
{
    public class ManagerUser : EntityBase<int>
    {
        #region Properties
        public int TenantId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        #endregion

        #region Navigation Properties
        public Tenant Tenant { get; set; }
        #endregion
    }
}
