﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Request.Order;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Trendyol.Common;
using ECommerce.MarketPlace.Trendyol.Common.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECommerce.MarketPlace.Trendyol.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            #region Fields
            this._httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string orderByDirection = "ASC";
                string status = "Delivered";

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{orderRequest.Configurations["Username"]}:{orderRequest.Configurations["Password"]}"));
                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                for (int i = 5; i >= 0; i--)
                {
                    int totalPage = 1;
                    var startDate = DateTime.Now.ToUniversalTime().Date.AddDays(i * -1);
                    var endDate = DateTime.Now.ToUniversalTime().Date.AddDays((i - 1) * -1);

                    while ((page + 1) <= totalPage)
                    {
                        TrendyolOrderPage trendyolOrderPages = null;

                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?page={page}&size={size}&orderByDirection={orderByDirection}&status={status}&startDate={startDate.ConvertToUnixTimeStamp()}&endDate={endDate.ConvertToUnixTimeStamp()}", "Basic", authorization);

                        totalPage = trendyolOrderPages.TotalPages;
                        page++;
                        if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                        {
                            response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                        }
                        else
                        {
                            response.Success = false;
                            response.Message = "Errorfrom trendyol service";
                        }
                    }

                    page = 0;
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string orderByDirection = "ASC";
                string status = "Cancelled";

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{orderRequest.Configurations["Username"]}:{orderRequest.Configurations["Password"]}"));

                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                for (int i = 5; i >= 0; i--)
                {
                    int totalPage = 1;
                    var startDate = DateTime.Now.ToUniversalTime().Date.AddDays(i * -1);
                    var endDate = DateTime.Now.ToUniversalTime().Date.AddDays((i - 1) * -1);

                    while ((page + 1) <= totalPage)
                    {
                        TrendyolOrderPage trendyolOrderPages = null;

                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?page={page}&size={size}&orderByDirection={orderByDirection}&status={status}", "Basic", authorization);

                        totalPage = trendyolOrderPages.TotalPages;
                        page++;

                        if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                        {
                            response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                            response.Success = true;
                        }
                        else
                        {
                            response.Success = false;
                            response.Message = "Errorfrom trendyol service";
                        }
                    }
                    page = 0;
                }

                response.Success = true;


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                int page = 0;
                int size = 200;
                string orderByDirection = "ASC";
                string status = "Returned";

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{orderRequest.Configurations["Username"]}:{orderRequest.Configurations["Password"]}"));

                response.Data = new OrderResponse
                {
                    Orders = new List<Order>()
                };

                for (int i = 5; i >= 0; i--)
                {
                    int totalPage = 1;
                    var startDate = DateTime.Now.ToUniversalTime().Date.AddDays(i * -1);
                    var endDate = DateTime.Now.ToUniversalTime().Date.AddDays((i - 1) * -1);

                    while ((page + 1) <= totalPage)
                    {
                        TrendyolOrderPage trendyolOrderPages = null;

                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?page={page}&size={size}&orderByDirection={orderByDirection}&status={status}&startDate={startDate.ConvertToUnixTimeStamp()}&endDate={endDate.ConvertToUnixTimeStamp()}", "Basic", authorization);

                        totalPage = trendyolOrderPages.TotalPages;
                        page++;
                        if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                        {
                            response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                            response.Success = true;
                        }
                        else
                        {
                            response.Success = false;
                            response.Message = "Error from trendyol service";
                        }
                    }

                    page = 0;
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                //long startDate = DateTime.Now.AddDays(-1).ConvertToUnixTimeStamp();
                //long endDate = DateTime.Now.ConvertToUnixTimeStamp();
                //startDate={startDate}&endDate={endDate}&
                int page = 0;
                int size = 200;
                string orderByField = "CreatedDate";
                string orderByDirection = "DESC";
                string status = "Created";//Picking

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{orderRequest.Configurations["Username"]}:{orderRequest.Configurations["Password"]}"));
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                int totalPage = 1;
                while ((page + 1) <= totalPage)
                {
                    TrendyolOrderPage trendyolOrderPages = null;

                    if (string.IsNullOrEmpty(orderRequest.MarketPlaceOrderCode))
                    {
                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?page={page}&size={size}&orderByField={orderByField}&orderByDirection={orderByDirection}&status={status}", "Basic", authorization);
                    }
                    else
                    {
                        trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?orderNumber={orderRequest.MarketPlaceOrderCode}", "Basic", authorization);
                    }

                    totalPage = trendyolOrderPages.TotalPages;
                    page++;
                    if (trendyolOrderPages != null && trendyolOrderPages.Content.Count > 0)
                    {
                        response.Data.Orders.AddRange(MapOrder(trendyolOrderPages.Content));
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Errorfrom trendyol service";
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{orderRequest.Configurations["Username"]}:{orderRequest.Configurations["Password"]}"));

                var trendyolOrderPages = _httpHelper.Get<TrendyolOrderPage>($"https://api.trendyol.com/sapigw/suppliers/{orderRequest.Configurations["SupplierId"]}/orders?orderNumber={orderRequest.MarketPlaceOrderCode}", "Basic", authorization);

                var trendyolOrder = trendyolOrderPages.Content.FirstOrDefault(x => x.ShipmentPackageStatus.ToLower() == "cancelled" && x.CargoTrackingNumber == orderRequest.TrackingCode);
                if (trendyolOrder != null)
                {
                    response.Data.IsCancel = true; //iptal mi
                    response.Message = $"Siparişiniz iptal statüsündedir. Trendyol panelinden siparişinizi kontrol ediniz.";
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG

                if (request.Status == MarketPlaceStatus.Picking)
                {
                    var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{request.Configurations["Username"]}:{request.Configurations["Password"]}"));

                    var aa = _httpHelper.Put<TrendyolShipmentPackage, Response>(new TrendyolShipmentPackage
                    {
                        lines = request.OrderPicking.OrderDetails.Select(l => new TrendyolShipmentPackageLine
                        {
                            lineId = Convert.ToInt64(l.Id),
                            quantity = l.Quantity
                        }).ToList(),
                        status = "Picking"
                    }, $"https://api.trendyol.com/sapigw/suppliers/{request.Configurations["SupplierId"]}/shipment-packages/{request.OrderPicking.MarketPlaceOrderId}", "Basic", authorization);
                }



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        private List<Order> MapOrder(List<TrendyolOrder> providerOrders)
        {
            List<Order> orders = new List<Order>();

            foreach (var pOrder in providerOrders)
            {
                var shipmentCompanyId = "ARS";
                if (!string.IsNullOrEmpty(pOrder.CargoTrackingNumber))
                {
                    if (pOrder.CargoTrackingNumber.StartsWith("733"))
                        shipmentCompanyId = ShipmentCompanies.TrendyolExpress;
                    else if (pOrder.CargoTrackingNumber.StartsWith("725"))
                        shipmentCompanyId = ShipmentCompanies.YurtiçiKargo;
                    else if (pOrder.CargoTrackingNumber.StartsWith("726"))
                        shipmentCompanyId = ShipmentCompanies.ArasKargo;
                    else if (pOrder.CargoTrackingNumber.StartsWith("727"))
                        shipmentCompanyId = ShipmentCompanies.SüratKargo;
                    else if (pOrder.CargoTrackingNumber.StartsWith("728"))
                        shipmentCompanyId = ShipmentCompanies.MngKargo;
                    else if (pOrder.CargoTrackingNumber.StartsWith("729"))
                        shipmentCompanyId = ShipmentCompanies.Ups;
                    else if (pOrder.CargoTrackingNumber.StartsWith("734"))
                        shipmentCompanyId = ShipmentCompanies.PttKargo;
                }

                Order order = new Order();

                order.OrderSourceId = OrderSources.Pazaryeri;
                order.MarketPlaceId = Marketplaces.Trendyol;
                order.OrderCode = pOrder.OrderNumber;
                order.CurrencyId = pOrder.CurrencyCode;
                order.OrderTypeId = "OS";
                order.TotalAmount = pOrder.TotalPrice;
                order.ListTotalAmount = pOrder.GrossAmount;
                order.Discount = pOrder.TotalDiscount;
                order.ShippingCost = 0;
                order.Source = "TY";
                order.OrderDate = pOrder.OrderDate.ConvertToDateTime();
                order.OrderShipment = new OrderShipment
                {
                    TrackingCode = pOrder.CargoTrackingNumber,
                    ShipmentTypeId = "PND",
                    TrackingUrl = pOrder.CargoTrackingLink,
                    ShipmentCompanyId = shipmentCompanyId //.ToLower() == "trendyol express marketplace" ? "TYEX" : "SRTTY" //Lafaba için yapıldı fatura kesilirken lazım oluyor
                };
                order.Customer = new Customer()
                {
                    FirstName = pOrder.CustomerFirstName,
                    LastName = pOrder.CustomerLastName,
                    IdentityNumber = pOrder.TcIdentityNumber,
                    Mail = pOrder.CustomerEmail,
                    TaxNumber = pOrder.TaxNumber,
                    Phone = "" //TODO set phone number
                };
                order.OrderDeliveryAddress = new OrderDeliveryAddress()
                {
                    FirstName = pOrder.ShipmentAddress.Firstname,
                    LastName = pOrder.ShipmentAddress.Lastname,
                    Country = pOrder.ShipmentAddress.CountryCode.ToUpper() == "TR" ? "Türkiye" : "",
                    Email = "",
                    Phone = pOrder.ShipmentAddress.Phone, //TODO set phone number
                    City = pOrder.ShipmentAddress.City,
                    District = pOrder.ShipmentAddress.District,
                    Neighborhood = pOrder.ShipmentAddress.Neighborhood,
                    Address = $"{pOrder.ShipmentAddress.Address1} {pOrder.ShipmentAddress.Address2}",
                    Code = pOrder.ShipmentAddress.PostalCode
                };
                order.OrderInvoiceAddress = new OrderInvoiceAddress()
                {
                    FirstName = pOrder.InvoiceAddress.Firstname,
                    LastName = pOrder.InvoiceAddress.Lastname,
                    Email = "",
                    Phone = pOrder.InvoiceAddress.Phone, //TODO set phone number
                    City = pOrder.InvoiceAddress.City,
                    District = pOrder.InvoiceAddress.District,
                    Address = $"{pOrder.InvoiceAddress.Address1} {pOrder.InvoiceAddress.Address2}",
                    Neighborhood = pOrder.InvoiceAddress.Neighborhood,
                    TaxNumber = pOrder.commercial ? pOrder.InvoiceAddress.TaxNumber : "",
                    TaxOffice = pOrder.commercial ? pOrder.InvoiceAddress.TaxOffice : ""

                };
                order.Payments = new List<Payment>();
                order.Payments.Add(new Payment()
                {
                    CurrencyId = "TRY",
                    PaymentTypeId = "OO",
                    Amount = pOrder.TotalPrice,
                    Date = pOrder.OrderDate.ConvertToDateTime()
                });

                order.OrderPicking = new MarketPlace.Common.Request.Order.OrderPicking
                {
                    MarketPlaceOrderId = pOrder.Id.ToString(),
                    OrderDetails = new List<OrderPickingDetail>()

                };

                #region Order Details
                order.OrderDetails = new List<OrderDetail>();
                foreach (var orderDetail in pOrder.Lines)
                {
                    order.OrderDetails.Add(new OrderDetail()
                    {
                        TaxRate = Convert.ToDouble(orderDetail.VatBaseAmount) / 100,
                        UnitPrice = orderDetail.Price,
                        ListPrice = orderDetail.Amount,
                        UnitDiscount = orderDetail.Discount,
                        Product = new Product()
                        {
                            Barcode = orderDetail.Barcode.ToUpper(),
                            Name = orderDetail.ProductName,
                            StockCode = orderDetail.MerchantSku?.ToString()
                        },
                        Quantity = orderDetail.Quantity
                    });

                    order.OrderPicking.OrderDetails.Add(new OrderPickingDetail
                    {
                        Id = orderDetail.Id.ToString(),
                        Quantity = orderDetail.Quantity,
                    });

                }
                #endregion

                orders.Add(order);
            }

            return orders;
        }
        #endregion
    }
}