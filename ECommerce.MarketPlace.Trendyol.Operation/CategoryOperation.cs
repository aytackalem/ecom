﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Trendyol.Common.Category;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private const string _baseUrl = "https://vfmallapi.vodafone.com.tr/vfmallapi";
        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper)
        {
            #region Fields
            _httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{configurations["Username"]}:{configurations["Password"]}"));

                CategoryRoot root = _httpHelper.Get<CategoryRoot>($"https://api.trendyol.com/sapigw/product-categories", "Basic", authorization);

                root.categories = root.categories.Where(x => x.id == 368 || x.id == 403 || x.id == 522  || x.id == 1070).ToList();


                var categoryAttributeRoots = new List<CategoryAttributeRoot>();

                if (root != null)
                {
                    foreach (var category in root.categories)
                    {

                        if (category != null)
                        {
                            categoryAttributeRoots.AddRange(Recursive(category, authorization));
                        }
                    }
                }

                var data = categoryAttributeRoots.Where(x => x != null).Select(x => new CategoryResponse
                {
                    Code = x.id.ToString(),
                    Name = x.name,
                    CategoryAttributes = x?.categoryAttributes?.Select(ca => new CategoryAttributeResponse
                    {
                        CategoryCode = x.id.ToString(),
                        Code = ca.attribute.id.ToString(),
                        Name = ca.attribute.name,
                        Mandatory = ca.required,
                        AllowCustom = ca.allowCustom,
                        CategoryAttributesValues = ca?.attributeValues?.Select(av => new CategoryAttributeValueResponse
                        {
                            VarinatCode = ca.attribute.id.ToString(),
                            Code = av.id.ToString(),
                            Name = av.name
                        }).ToList()
                    }).ToList()
                }).ToList();


                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        private List<CategoryAttributeRoot> Recursive(SubCategory category, string authorization)
        {
            var categoryAttributeRoots = new List<CategoryAttributeRoot>();

            if (category.subCategories == null || category.subCategories.Count == 0)
            {

                var categoryAttributeRoot = _httpHelper.Get<CategoryAttributeRoot>($"https://api.trendyol.com/sapigw/product-categories/{category.id}/attributes", "Basic", authorization);

                if (categoryAttributeRoot != null)
                    categoryAttributeRoots.Add(categoryAttributeRoot);


            }

            foreach (var scLoop in category.subCategories)
            {
                categoryAttributeRoots.AddRange(Recursive(scLoop, authorization));
            }

            return categoryAttributeRoots;
        }

    }
}
