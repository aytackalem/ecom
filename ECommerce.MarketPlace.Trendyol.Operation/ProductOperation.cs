﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Stock;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.ProductFilter;
using ECommerce.MarketPlace.Trendyol.Common.Product;
using ECommerce.MarketPlace.Trendyol.Common.ProductFilter;
using ECommerce.MarketPlace.Trendyol.Common.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;

        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var supplierId = productRequest.Configurations["SupplierId"];
                var username = productRequest.Configurations["Username"];
                var password = productRequest.Configurations["Password"];

                //var brandId = productRequest.Configurations["BrandId"];

                ///Trendyol Ürün ekleme için kullanılır
                var requestCreate = new TrendyolProduct();
                requestCreate.Items = new List<TrendyolProductItem>();

                //Trendyol Ürün Güncelleme için kullanılır
                var requestUpdate = new TrendyolProduct();
                requestUpdate.Items = new List<TrendyolProductItem>();

                foreach (var stockItem in productRequest.ProductItem.StockItems)
                {
                    var item = new TrendyolProductItem();
                    item.Barcode = stockItem.Barcode;
                    item.Active = stockItem.Active;
                    item.Title = stockItem.Title;
                    item.ProductMainId = productRequest.ProductItem.SellerCode; // Guid.NewGuid().ToString();//
                    item.BrandId = Convert.ToInt32(productRequest.ProductItem.BrandId); //bakılacak
                    item.CategoryId = Convert.ToInt32(productRequest.ProductItem.CategoryId);
                    item.Quantity = stockItem.Quantity;
                    item.StockCode = stockItem.StockCode;
                    item.Description = stockItem.Title;
                    item.CurrencyType = "TRL";
                    item.ListPrice = Convert.ToDouble(stockItem.ListPrice);
                    item.SalePrice = Convert.ToDouble(stockItem.SalePrice);
                    item.VatRate = Convert.ToInt32(stockItem.VatRate * 100);
                    item.Description = stockItem.Description;
                    //item.DeliveryDuration = Convert.ToInt32(productRequest.Configurations["DeliveryDuration"]);
                    item.CargoCompanyId = 2;
                    item.DimensionalWeight = Convert.ToInt32(productRequest.ProductItem.DimensionalWeight);
                    item.Images = new List<TrendyolProductImage>();
                    item.Attributes = new List<TrendyolProductAttribute>();
                    foreach (var image in stockItem.Images)
                    {
                        item.Images.Add(new TrendyolProductImage
                        {
                            Url = image.Url
                        });
                    }

                    foreach (var attribute in stockItem.Attributes)
                    {
                        if (attribute.AttributeName == "Renk")
                        {
                            item.Attributes.Add(new TrendyolProductAttribute
                            {
                                AttributeId = attribute.AttributeCode,
                                CustomAttributeValue = attribute.AttributeValueName,
                                AttributeValueId = null
                            });
                        }
                        else
                        {
                            item.Attributes.Add(new TrendyolProductAttribute
                            {
                                AttributeId = attribute.AttributeCode,
                                AttributeValueId = attribute.AttributeValueCode
                            });
                        }
                    }

                    var filter = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Stockcode = stockItem.StockCode
                    });

                    if (filter.Success && filter.Data.ProductItems.Count == 1)
                    {
                        requestUpdate.Items.Add(item);
                    }
                    else if (stockItem.Active) //Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                    {
                        requestCreate.Items.Add(item);
                    }
                }

                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));

                var apiResponse = new Response();

                if (requestCreate.Items.Count > 0 && string.IsNullOrEmpty(requestCreate.Items[0].Description))
                {
                    requestCreate.Items[0].Description = requestCreate.Items[0].Title;
                }

                if (requestCreate.Items.Count > 0)
                    apiResponse = _httpHelper.Post<TrendyolProduct, Response>(requestCreate, $"https://api.trendyol.com/sapigw/suppliers/{supplierId}/v2/products", "Basic", authorization);

                if (requestUpdate.Items.Count > 0)
                {
                    apiResponse = _httpHelper.Put<TrendyolProduct, Response>(requestUpdate, $"https://api.trendyol.com/sapigw/suppliers/{supplierId}/v2/products", "Basic", authorization);

                    #region UpdatePriceAndStockRequest
                    foreach (var product in requestUpdate.Items)
                    {
                        if (!product.Active)
                        {
                            UpdateStock(new StockRequest
                            {
                                Configurations = productRequest.Configurations,
                                PriceStock = new PriceStock
                                {
                                    StockCode = product.StockCode,
                                    Quantity = 0
                                }
                            });
                        }
                    }
                    #endregion

                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var supplierId = stockRequest.Configurations["SupplierId"];
                var username = stockRequest.Configurations["Username"];
                var password = stockRequest.Configurations["Password"];

                var request = new TrendyolPriceStock();
                request.Items = new List<TrendyolPriceStockItem>();
                request.Items.Add(new TrendyolPriceStockItem()
                {
                    Barcode = stockRequest.PriceStock.Barcode,
                    Quantity = null,
                    ListPrice = stockRequest.PriceStock.ListPrice,
                    SalePrice = stockRequest.PriceStock.SalePrice
                });
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));

                var apiResonse = _httpHelper.Post<TrendyolPriceStock, Response>(request, $"https://api.trendyol.com/sapigw/suppliers/{supplierId}/products/price-and-inventory", "Basic", authorization);
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var supplierId = stockRequest.Configurations["SupplierId"];
                var username = stockRequest.Configurations["Username"];
                var password = stockRequest.Configurations["Password"];
                var filter = Filter(new ProductFilterRequest
                {
                    Stockcode = stockRequest.PriceStock.StockCode,
                    Barcode = null,
                    Configurations = stockRequest.Configurations
                });

                if (filter.Success && filter.Data.ProductItems.Count == 1)
                {
                    var request = new TrendyolPriceStock();
                    request.Items = new List<TrendyolPriceStockItem>();
                    request.Items.Add(new TrendyolPriceStockItem()
                    {
                        Barcode = filter.Data.ProductItems[0].Barcode,
                        Quantity = stockRequest.PriceStock.Quantity,
                        ListPrice = null,
                        SalePrice = null
                    });
                    var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));

                    var apiResonse = _httpHelper.Post<TrendyolPriceStock, Response>(request, $"https://api.trendyol.com/sapigw/suppliers/{supplierId}/products/price-and-inventory", "Basic", authorization);
                    response.Success = true;
                }
                response.Success = false;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        private DataResponse<ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductFilterResponse>>((response) =>
            {
                response.Data = new ProductFilterResponse
                {
                    ProductItems = new List<ProductFilterItem>()
                };

                var supplierId = productFilter.Configurations["SupplierId"];
                var authorization = Convert.ToBase64String(
                    Encoding.UTF8.GetBytes(
                        $"{productFilter.Configurations["Username"]}:{productFilter.Configurations["Password"]}"));

                var trendyolFilterProducts = new List<TrendyolFilterProduct>();
                var page = 0;
                var size = 1000;
                TrendyolFilterProductPage trendyolFilterProductPageResult = null;
                do
                {
                    var trendyolApiUrl = $"https://api.trendyol.com/sapigw/suppliers/{supplierId}/products?approved=true&page={page}&size={size}";
                    if (!string.IsNullOrEmpty(productFilter.Barcode))
                    {
                        trendyolApiUrl += $"&barcode={productFilter.Barcode}";
                    }

                    if (!string.IsNullOrEmpty(productFilter.Stockcode))
                    {
                        trendyolApiUrl += $"&stockCode={productFilter.Stockcode}";
                    }

                    trendyolFilterProductPageResult = this._httpHelper.Get<TrendyolFilterProductPage>(
                        trendyolApiUrl,
                        "Basic",
                        authorization);

                    trendyolFilterProductPageResult.content.ForEach(c =>
                    {
                        response.Data.ProductItems.Add(new ProductFilterItem
                        {
                            CategoryId = c.pimCategoryId.ToString(),
                            SellerCode = c.productMainId,
                            Barcode = c.barcode,
                            CurrencyCode = "TRL",
                            Description = c.description,
                            Title = c.title,
                            VatRate = c.vatRate,
                            StockItems = new List<ProductFilterStockItem>
                            {
                                new ProductFilterStockItem
                                {
                                    Barcode = c.barcode,
                                    Images = c.images?.
                                                Select(i => new ProductFilterImage
                                                    {
                                                        Order = c.images.IndexOf(i),
                                                        Url = i.url
                                                    }).
                                                ToList(),
                                    Quantity = c.quantity,
                                    ListPrice = (decimal)c.listPrice,
                                    SalePrice = (decimal)c.salePrice,
                                    SellerStockCode = c.stockCode,
                                    Attributes = c.attributes?.
                                                Select(a => new ProductFilterCategoryAttribute
                                                    {
                                                        AttributeCode = a.attributeId.ToString(),
                                                        AttributeName = a.attributeName,
                                                        AttributeValueCode = a.attributeValueId.ToString(),
                                                        AttributeValueName = a.attributeValue,
                                                        Mandatory = true
                                                    }).
                                                ToList()
                                }
                            }
                        });
                    });

                    trendyolFilterProducts.AddRange(trendyolFilterProductPageResult.content);

                    page++;
                } while ((trendyolFilterProductPageResult.page + 1) < trendyolFilterProductPageResult.totalPages);

                response.Success = true;
            });
        }
        #endregion
    }
}
