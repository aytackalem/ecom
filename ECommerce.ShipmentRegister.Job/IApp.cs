﻿namespace ECommerce.ShipmentRegister.Job
{
    public interface IApp
    {
        #region Methods
        void Run(); 
        #endregion
    }
}
