﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace ECommerce.ShipmentRegister.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICryptoHelper _cryptoHelper;

        private readonly IShipmentProviderService _shipmentService;

        private readonly IOptions<AppConfig> _options;

        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ICryptoHelper cryptoHelper, IShipmentProviderService shipmentService, IOptions<AppConfig> options)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cryptoHelper = cryptoHelper;
            this._shipmentService = shipmentService;
            this._options = options;

            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            Console.WriteLine($"Shipment Register {this._options.Value.Tenant.Type} JOB RUN");

            var orders = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderShipments)
                .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
                .Where(o =>
                o.OrderShipments.Any(x => String.IsNullOrEmpty(x.TrackingCode)) &&

                (o.OrderTypeId == OrderTypes.Beklemede
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.KapıdaKrediKartıNakit))
                ||
                (o.OrderTypeId == OrderTypes.Beklemede
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.Havale && x.Paid))
                ||
                (o.OrderTypeId == OrderTypes.OnaylanmisSiparis
                && o.Payments.Any(x => x.PaymentTypeId == PaymentTypes.OnlineÖdeme && x.Paid)))
                .ToList();

            //var orders = this
            //    ._unitOfWork
            //    .OrderRepository
            //    .DbSet()
            //    .Include(o => o.OrderShipments)
            //    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
            //    .Where(o => o.OrderTypeId == OrderTypes.Beklemede && o.OrderSourceId == 8)
            //    .ToList();

            foreach (var oLoop in orders)
            {
                var postResponse = this._shipmentService.Post(oLoop.Id, true, new int[] { 1 }, 1);
                if (postResponse.Success)
                {

                    oLoop.OrderTypeId = OrderTypes.OnaylanmisSiparis;

                    this
                        ._unitOfWork
                        .OrderRepository
                        .Update(oLoop);

                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($" [{oLoop.Id}] sipariş numarası başarılı sisteme aktarıldı");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                }
            }
        }
        #endregion

    }
}
