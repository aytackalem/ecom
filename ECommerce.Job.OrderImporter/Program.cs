﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using ECommerce.Infrastructure;

namespace ECommerce.Job.OrderImporter
{
    internal class Program
    {
        #region Methods
        #region Startup
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddDbContext<ApplicationDbContext>(options => 
                options.UseSqlServer(configuration.GetConnectionString("Ecommerce")), 
                ServiceLifetime.Scoped);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddSingleton<ITenantFinder, TenantFinderDynamic>();

            serviceCollection.AddSingleton<IDomainFinder, DomainFinderDynamic>();

            serviceCollection.AddSingleton<ICompanyFinder, CompanyFinderDynamic>();

            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddScoped<Importer>();

            return serviceCollection.BuildServiceProvider();
        }
        #endregion

        static async Task Main(string[] args)
        {
            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);
            using (var scope = serviceProvider.CreateScope())
            {
                var importer = scope.ServiceProvider.GetRequiredService<Importer>();
                await importer.RunAsync(args);
            }
        } 
        #endregion
    }
}