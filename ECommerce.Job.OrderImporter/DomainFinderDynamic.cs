﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.OrderImporter
{
    public class DomainFinderDynamic : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public DomainFinderDynamic()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }
        #endregion
    }
}
