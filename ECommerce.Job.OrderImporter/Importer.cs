﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Importer.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Importer;

namespace ECommerce.Job.OrderImporter
{
    public class Importer
    {
        #region Fields
        private readonly IMarketplaceOrderImporter _marketplaceOrderImporter;
        #endregion

        #region Constructors
        public Importer(IMarketplaceOrderImporter marketplaceOrderImporter)
        {
            #region Fields
            this._marketplaceOrderImporter = marketplaceOrderImporter;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(string[] args)
        {
            var queueId = int.Parse(args[0]);
            var marketplaceId = args[1];

            await this._marketplaceOrderImporter.ImportAsync(new MarketplaceOrderImporterRequest
            {
                QueueId = queueId,
                MarketplaceId = marketplaceId
            });
        }
        #endregion
    }
}
