﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.OrderImporter
{
    public class CompanyFinderDynamic : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Constructors
        public CompanyFinderDynamic()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _companyId;
        }
        #endregion
    }
}
