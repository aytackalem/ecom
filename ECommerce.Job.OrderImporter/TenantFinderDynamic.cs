﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.OrderImporter
{
    public class TenantFinderDynamic : ITenantFinder
    {
        #region Fields
        public int _tenantId;
        #endregion

        #region Constructors
        public TenantFinderDynamic()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _tenantId;
        }
        #endregion
    }
}
