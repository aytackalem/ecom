﻿using ECommerce.MarketPlace.Trendyol.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common
{
    public class TrendyolOrderPage : TrendyolPage
    {
        #region NavigationProperties
        public List<TrendyolOrder> Content { get; set; }
        #endregion
    }
}
