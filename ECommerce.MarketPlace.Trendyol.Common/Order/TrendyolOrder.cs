﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common
{
    public class TrendyolOrder
    {
        #region Properties
        public string OrderNumber { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal GrossAmount { get; set; }

        public decimal TotalDiscount { get; set; }

        public string TaxNumber { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerEmail { get; set; }

        public long CustomerId { get; set; }

        public string CustomerLastName { get; set; }

        public long Id { get; set; }

        public string CargoTrackingNumber { get; set; }

        public string CargoTrackingLink { get; set; }

        public string CargoSenderNumber { get; set; }

        public long OrderDate { get; set; }

        public string TcIdentityNumber { get; set; }

        public string CurrencyCode { get; set; }

        public string ShipmentPackageStatus { get; set; }

        public string cargoProviderName { get; set; }

        /// <summary>
        /// Kurumsal fatura mı
        /// </summary>
        public bool commercial { get; set; }

        public TrendyolShipmentAddress ShipmentAddress { get; set; }

        public TrendyolInvoiceAddress InvoiceAddress { get; set; }

        public List<TrendyolOrderDetail> Lines { get; set; }

        public List<TrendyolOrderHistory> PackageHistories { get; set; }
        #endregion
    }
}
