﻿using ECommerce.MarketPlace.Trendyol.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common
{
    public class TrendyolShipmentAddress : AddressBase
    {
        #region Properties
        public int CityCode { get; set; }

        public int DistrictId { get; set; }
        #endregion
    }
}
