﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.ProductFilter
{
    public class TrendyolFilterProductPage
    {
        #region Property
        public int totalElements { get; set; }

        public int totalPages { get; set; }

        public int page { get; set; }

        public int size { get; set; }

        public List<TrendyolFilterProduct> content { get; set; }
        #endregion
    }
}
