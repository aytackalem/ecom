﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.ProductFilter
{
    public class TrendyolFilterProductAttribute
    {
        #region Property
        public int attributeId { get; set; }

        public string attributeName { get; set; }

        public int attributeValueId { get; set; }

        public string attributeValue { get; set; }
        #endregion
    }
}
