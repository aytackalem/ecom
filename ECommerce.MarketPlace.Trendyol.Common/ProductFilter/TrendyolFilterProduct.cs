﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.ProductFilter
{
    public class TrendyolFilterProduct
    {
        #region Property
        public string id { get; set; }

        public bool approved { get; set; }

        public int productCode { get; set; }

        public string batchRequestId { get; set; }

        public int supplierId { get; set; }

        public long createDateTime { get; set; }

        public long lastUpdateDate { get; set; }

        public string gender { get; set; }

        public string brand { get; set; }

        public string barcode { get; set; }

        public string title { get; set; }

        public string categoryName { get; set; }

        public string productMainId { get; set; }

        public string description { get; set; }

        public string stockUnitType { get; set; }

        public int quantity { get; set; }

        public double listPrice { get; set; }

        public double salePrice { get; set; }

        public int vatRate { get; set; }

        public double dimensionalWeight { get; set; }

        public string stockCode { get; set; }

        public List<TrendyolFilterProductImage> images { get; set; }

        public List<TrendyolFilterProductAttribute> attributes { get; set; }

        public string platformListingId { get; set; }

        public string stockId { get; set; }

        public bool hasActiveCampaign { get; set; }

        public bool locked { get; set; }

        public int productContentId { get; set; }

        public int pimCategoryId { get; set; }

        public int brandId { get; set; }

        public int version { get; set; }

        public string color { get; set; }

        public string size { get; set; }

        public bool lockedByUnSuppliedReason { get; set; }

        public bool onsale { get; set; }
        #endregion
    }
}
