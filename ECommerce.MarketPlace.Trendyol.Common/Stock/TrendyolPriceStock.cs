﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Stock
{
    public class TrendyolPriceStock
    {
        #region Properties
        [JsonProperty("items")]
        public List<TrendyolPriceStockItem> Items { get; set; } 
        #endregion
    }
}
