﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Stock
{
    public class TrendyolPriceStockItem
    {
        #region Properties
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("listPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty("salePrice")]
        public decimal? SalePrice { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; } 
        #endregion

    }
}
