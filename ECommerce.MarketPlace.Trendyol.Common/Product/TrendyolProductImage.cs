﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Product
{
    public class TrendyolProductImage
    {
        #region Properties
        [JsonProperty("url")]
        public string Url { get; set; } 
        #endregion
    }
}
