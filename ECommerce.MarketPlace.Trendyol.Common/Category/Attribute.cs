﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Category
{
    public class Attribute
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
