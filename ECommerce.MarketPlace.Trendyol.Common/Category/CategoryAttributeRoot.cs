﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Category
{
    public class CategoryAttributeRoot
    {
        public int id { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public List<CategoryAttribute> categoryAttributes { get; set; }
    }
}
