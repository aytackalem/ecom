﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Category
{
    public class CategoryAttribute
    {
        public bool allowCustom { get; set; }
        public Attribute attribute { get; set; }
        public List<AttributeValue> attributeValues { get; set; }
        public int categoryId { get; set; }
        public bool required { get; set; }
        public bool varianter { get; set; }
        public bool slicer { get; set; }
    }
}
