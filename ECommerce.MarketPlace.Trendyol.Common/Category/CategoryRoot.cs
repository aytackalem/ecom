﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Category
{
    public class CategoryRoot
    {
        public List<SubCategory> categories { get; set; }
    }
}
