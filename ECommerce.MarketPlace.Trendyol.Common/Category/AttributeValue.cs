﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Trendyol.Common.Category
{
    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
