﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Stock
{
    public class CicekSepetiStockRequest
    {
        public List<CicekSepetiStockItem> items { get; set; }

    }
}
