﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Stock
{
    public class CicekSepetiStockItem
    {
        public string stockCode { get; set; }
        public int? StockQuantity { get; set; }
        public double? listPrice { get; set; }
        public double? salesPrice { get; set; }
    }
}
