﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Product
{
    public class CicekSepetiProductCreateRequest
    {
        public List<CicekSepetiProductCreate> products { get; set; }

    }
}
