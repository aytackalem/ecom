﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Cargo
{
    public class CicekSepetiCargoResponse
    {
        #region Field
        public List<CicekSepetiStatusUpdateResponse> statusUpdateResponse { get; set; }
        #endregion
    }

}
