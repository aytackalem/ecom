﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Category
{
    public class CicekSepetiCategoryList
    {
        #region Properties
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public object ParentCategoryId { get; set; }
        
        public List<CicekSepetiSubCategory> SubCategories { get; set; }
        #endregion
    }


    public class CicekSepetiCategory
    {
        #region Properties
        public List<CicekSepetiCategoryList> Categories { get; set; }
        #endregion
    }
}
