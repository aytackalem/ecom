﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Order
{
    public class CicekSepetiSupplierOrderListWithBranch
    {
        #region Property
        public int branchId { get; set; }

        public int customerId { get; set; }

        public string accountCode { get; set; }

        public string accountCodePrefix { get; set; }

        public int orderId { get; set; }

        public int orderItemId { get; set; }

        public string orderCreateDate { get; set; }

        public double cargoPrice { get; set; }

        public string orderCreateTime { get; set; }

        public string orderModifyDate { get; set; }

        public string orderModifyTime { get; set; }

        public string barcode { get; set; }

        public string cardMessage { get; set; }

        public string cardName { get; set; }

        public double deliveryCharge { get; set; }

        public string orderPaymentType { get; set; }

        public int orderItemStatusId { get; set; }

        public string orderProductStatus { get; set; }

        public List<object> orderItemTextListModel { get; set; }

        public double discount { get; set; }

        public double totalPrice { get; set; }

        public double tax { get; set; }

        public string receiverName { get; set; }

        public string receiverPhone { get; set; }

        public string receiverAddress { get; set; }

        public string deliveryType { get; set; }

        public object deliveryDate { get; set; }

        public string requestedDeliveryDate { get; set; }

        public string cargoCompany { get; set; }

        public string receiverCity { get; set; }

        public string receiverRegion { get; set; }

        public string receiverDistrict { get; set; }

        public string senderName { get; set; }

        public string senderAddress { get; set; }

        public string senderTaxNumber { get; set; }

        public string senderTaxOfficeName { get; set; }

        public string senderCity { get; set; }

        public string senderRegion { get; set; }

        public string senderDistrict { get; set; }

        public string cargoNumber { get; set; }

        public string shipmentTrackingUrl { get; set; }

        public int productId { get; set; }

        public string productCode { get; set; }

        public string code { get; set; }

        public string name { get; set; }

        public int quantity { get; set; }

        public string quantityUnit { get; set; }

        public string invoiceEmail { get; set; }

        public bool isOrderStatusActive { get; set; }

        public string partialNumber { get; set; }

        public object senderCompanyName { get; set; }

        public double allowanceRate { get; set; }

        public double credit { get; set; }

        public object deliveryOptionName { get; set; }

        public string deliveryTime { get; set; }

        public object cancellationResult { get; set; }

        public bool isFloristCargoOrder { get; set; }

        public object receiverCompanyName { get; set; }

        public string floristName { get; set; }

        public string floristAddress { get; set; }

        public bool isLateToCargo { get; set; }
        #endregion
    }
}
