﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Order
{
    public class CicekSepetiOrderRequest
    {
        #region Property
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int PageSize { get; set; }

        public int Page { get; set; }

        public int StatusId { get; set; }

        public int? OrderNo { get; set; }
        #endregion
    }
}
