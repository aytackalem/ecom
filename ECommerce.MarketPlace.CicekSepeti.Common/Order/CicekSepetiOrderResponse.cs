﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.CicekSepeti.Common.Order
{
    public class CicekSepetiOrderResponse
    {
        #region Property
        public int orderListCount { get; set; }

        public int pageCount { get; set; }

        public List<CicekSepetiSupplierOrderListWithBranch> supplierOrderListWithBranch { get; set; }
        #endregion
    }
}
