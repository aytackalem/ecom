﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.CicekSepeti.Common.ProductFilter
{
    public class CicekSepetiProductFilterAttribute
    {
        public int id { get; set; }
        public string name { get; set; }
        public int textLength { get; set; }
    }
}
