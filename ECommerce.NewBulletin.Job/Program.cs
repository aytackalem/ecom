﻿using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.Communication;
using ECommerce.NewBulletin.Job.Base;
using ECommerce.Persistence.Client.Caching;
using ECommerce.Persistence.Client.Services;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ECommerce.NewBulletin.Job
{
    class Program
    {

        static void Main(string[] args)
        {

            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.AddSingleton<IApp, App>();
            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("EcommerceConnection"));
            });
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<IConfigrationService, ConfigrationService>();
            serviceCollection.AddScoped<ICacheHandler, CacheHandler>();
            serviceCollection.AddScoped<IEmailTemplateService, EmailTemplateService>();
            //serviceCollection.AddScoped<IEmailService, DefaultEmailService>();
            serviceCollection.AddScoped<ILocalizationHelper, LocalizationHelper>();

            serviceCollection.AddMemoryCache();
            return serviceCollection.BuildServiceProvider();
        }

    }
}
