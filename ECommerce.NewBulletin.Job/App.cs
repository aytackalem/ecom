﻿using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using ECommerce.NewBulletin.Job.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.NewBulletin.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IConfigrationService _configrationService;

        private readonly IEmailTemplateService _emailTemplateService;

        private readonly IEmailService _emailService;
        #endregion

        public App(IUnitOfWork unitOfWork, IConfigrationService configrationService, IEmailTemplateService emailTemplateService, IEmailService emailService)
        {
            #region Field
            this._unitOfWork = unitOfWork;
            this._configrationService = configrationService;
            this._emailTemplateService = emailTemplateService;
            this._emailService = emailService;
            #endregion
        }

        public void Run()
        {


            var informations = this._unitOfWork.InformationRepository
                .DbSet()
                .Where(x =>
                       x.Active
                       && !x.NewBulletinSend).ToList();

            var newBulletins = this._unitOfWork.NewBulletinRepository
                .DbSet()
                .Where(x =>
                    x.Active
                    && !x.Deleted).ToList();


            foreach (var inLoop in informations)
            {
                var newBulletinInformations = new List<NewBulletinInformation>();
                var emailTemplate = this._emailTemplateService.NewBulletinTemplate(inLoop.Id);
                if (emailTemplate.Success)
                {
                    foreach (var nbLoop in newBulletins)
                    {
                        var send = this._emailService.Send(new List<string> { nbLoop.Mail }, emailTemplate.Data.Subject, emailTemplate.Data.Template, false);
                        if (send.Success)
                        {
                            Console.WriteLine($"{nbLoop.Mail} müşteri {emailTemplate.Data.Subject} bülten gönderildi");
                            newBulletinInformations.Add(new NewBulletinInformation
                            {
                                InformationId = inLoop.Id,
                                NewBulletinId = nbLoop.Id,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }

                    if (newBulletinInformations.Count > 0)
                    {
                        inLoop.NewBulletinSend = true;
                        this._unitOfWork.InformationRepository.Update(inLoop);
                        _unitOfWork.NewBulletinInformationRepository.BulkInsert(newBulletinInformations);
                    }


                }

            }



        }



    }
}
