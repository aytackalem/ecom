﻿using Amazon.Runtime.Internal.Transform;
using DocumentFormat.OpenXml;
using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.ProductInformations;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Data;

namespace ECommerce.Job.Marketplace
{
    public class AppV2 : IAppV2
    {
        #region Fields
        private readonly IConfiguration _configuration;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceService _marketplaceService;

        //private readonly Application.Common.Interfaces.MarketplacesV2.IMarketplaceServiceV2 _marketplaceServiceV2;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private Domain.Entities.Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Domain.Entities.Company _company;

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructors
        public AppV2(IConfiguration configuration, IUnitOfWork unitOfWork, IMarketplaceService marketplaceService, /*Application.Common.Interfaces.MarketplacesV2.IMarketplaceServiceV2 marketplaceServiceV2,*/ ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IServiceProvider serviceProvider)
        {
            #region Fields
            this._configuration = configuration;
            this._unitOfWork = unitOfWork;
            this._marketplaceService = marketplaceService;
            //this._marketplaceServiceV2 = marketplaceServiceV2;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._serviceProvider = serviceProvider;
            #endregion
        }
        #endregion

        #region Methods
        public void Run(string[] args)
        {
            this._tenant = this
                ._unitOfWork
                .TenantRepository
                .DbSet()
                .Include(t => t.TenantSetting)
                .First(t => t.Id == this._tenantFinder.FindId());
            this._domain = this
                ._unitOfWork
                .DomainRepository
                .DbSet()
                .Include(d => d.DomainSetting).First(d => d.Id == this._domainFinder.FindId());
            this._company = this
                ._unitOfWork
                .CompanyRepository
                .DbSet()
                .Include(c => c.CompanySetting).First(c => c.Id == this._companyFinder.FindId());

#if DEBUG
 
#endif

            if (args != null && args[0] == "Orders")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting orders for {this._company.Name} {args[1]}");
                    Orders(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting orders for {this._company.Name}.");
                    Orders();
                }
            }
            else if (args != null && args[0] == "RawOrders")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting raw orders for {this._company.Name} {args[1]}");
                    RawOrders(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting raw orders for {this._company.Name}.");
                    RawOrders();
                }
            }
            else if (args != null && args[0] == "ProcessRawOrders")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting process raw orders for ${this._company.Name} {args[1]}");
                    ProcessRawOrders(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting process raw orders for ${this._company.Name}.");
                    ProcessRawOrders();
                }
            }
            else if (args != null && args[0] == "ProcessOrderTypeUpdates")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting process order type updates for ${this._company.Name} {args[1]}");
                    ProcessOrderTypeUpdates(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting process order type updates for ${this._company.Name}.");
                    ProcessOrderTypeUpdates();
                }
            }
            else if (args != null && args[0] == "ProcessMutualBarcodes")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting process mutual barcodes for ${this._company.Name} {args[1]}");
                    ProcessMutualBarcodes(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting process mutual barcodes for ${this._company.Name}.");
                    ProcessMutualBarcodes();
                }
            }
            else if (args != null && args[0] == "CancelledReturned")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting delivered orders for ${this._company.Name}.");


                    XConsole.Info("Starting cancelled orders.");
                    CancelledOrders(args[1]);

                    XConsole.Info("Starting returned orders.");
                    ReturnedOrders(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting delivered orders for ${this._company.Name}.");


                    XConsole.Info("Starting cancelled orders.");
                    CancelledOrders();

                    XConsole.Info("Starting returned orders.");
                    ReturnedOrders();
                }
            }
            else if (args != null && args[0] == "Delivered")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting delivered orders for ${this._company.Name}.");
                    DeliveredOrders(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting delivered orders for ${this._company.Name}.");
                    DeliveredOrders();
                }
            }
            else if (args != null && args[0] == "ProductsStatus")
            {
                if (args.Length > 1)
                {
                    XConsole.Info($"Starting product status for ${this._company.Name} {args[1]}.");
                    ProductsStatus(args[1]);
                }
                else
                {
                    XConsole.Info($"Starting product status for ${this._company.Name}.");
                    ProductsStatus();
                }
            }
        }

        public void Orders(string? marketplaceId = null)
        {
            var response = this._marketplaceService.GetOrders(marketplaceId: marketplaceId);

            if (response.Success == false)
            {
                XConsole.Warning($"Orders not fetched.");
                return;
            }

            XConsole.Info($"{response.Data.Orders.Count} orders found.");

            foreach (var theOrder in response.Data.Orders)
            {
                XConsole.Info($"{response.Data.Orders.Count} / {response.Data.Orders.IndexOf(theOrder)} will create.");

                if (theOrder.MarketplaceId == Marketplaces.TrendyolEurope)
                {
                    CreateTrendyolEuropeOrder(theOrder);
                }
                else
                {
                    CreateOrder(theOrder);
                }
            }
        }

        List<ExchangeRate> _exchangeRates = null;

        private void CreateOrder(Application.Common.Wrappers.Marketplaces.Order.Order theOrder)
        {
            this.Scope(scope =>
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                var shipmentProviderService = scope.ServiceProvider.GetRequiredService<IShipmentProviderService>();
                var productInformationStockService = scope.ServiceProvider.GetRequiredService<IProductInformationStockService>();

                #region Check Duplicate Order
                var duplicateOrder = unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Any(x =>
                        x.MarketplaceId == theOrder.MarketplaceId &&
                        x.MarketplaceOrderNumber == theOrder.OrderCode &&
                        (
                            string.IsNullOrEmpty(theOrder.OrderShipment.TrackingCode) ||
                            x.OrderShipments.Any(x => x.TrackingCode == theOrder.OrderShipment.TrackingCode)));

                if (duplicateOrder)
                {
                    var pickingRequest = new Application.Common.Parameters.OrderPickingRequest
                    {
                        Token = theOrder.OrderPicking.Token,
                        OrderPicking = theOrder.OrderPicking,
                        MarketPlaceId = theOrder.MarketplaceId,
                        Status = Application.Common.Parameters.Enums.MarketplaceStatus.Picking,
                        Configurations = this
                            ._marketplaceService
                            .GetMarketplaceConfigurations(theOrder.MarketplaceId)
                            .ToDictionary(mc => mc.Key, mc => mc.Value)
                    };
                    this._marketplaceService.OrderTypeUpdate(pickingRequest);

                    XConsole.Warning("Duplicate order.");
                    return;
                }
                #endregion

                var deliveryFindNeighborhoodId = 0;
                var invoiceFindNeighborhoodId = 0;

                if (theOrder.Micro)
                {
                    #region Find Delivery Neighborhood
                    var deliveryFindNeighborhoodIdData = FindMicroNeighborhoodId(
                                    unitOfWork,
                                    theOrder.OrderDeliveryAddress.Country,
                                    theOrder.OrderDeliveryAddress.City,
                                    theOrder.OrderDeliveryAddress.District,
                                    theOrder.OrderDeliveryAddress.Neighborhood);
                    if (!deliveryFindNeighborhoodIdData.Success)
                    {
                        XConsole.Error("Delivery neighborhood not found.");
                        return;
                    }
                    deliveryFindNeighborhoodId = deliveryFindNeighborhoodIdData.Data;
                    #endregion

                    #region Find Invoice Neighborhood
                    var invoiceFindNeighborhoodIdData = FindMicroNeighborhoodId(
                                    unitOfWork,
                                    theOrder.OrderInvoiceAddress.Country,
                                    theOrder.OrderInvoiceAddress.City,
                                    theOrder.OrderInvoiceAddress.District,
                                    theOrder.OrderInvoiceAddress.Neighborhood);
                    if (!invoiceFindNeighborhoodIdData.Success)
                    {
                        XConsole.Error("Invoice neighborhood not found.");
                        return;
                    }
                    invoiceFindNeighborhoodId = invoiceFindNeighborhoodIdData.Data;
                    #endregion
                }
                else
                {
                    #region Find Delivery Neighborhood
                    var deliveryFindNeighborhoodIdData = FindNeighborhoodId(
                                    unitOfWork,
                                    theOrder.OrderDeliveryAddress.City,
                                    theOrder.OrderDeliveryAddress.District,
                                    theOrder.OrderDeliveryAddress.Neighborhood);
                    if (!deliveryFindNeighborhoodIdData.Success)
                    {
                        XConsole.Error("Delivery neighborhood not found.");
                        return;
                    }
                    deliveryFindNeighborhoodId = deliveryFindNeighborhoodIdData.Data;
                    #endregion

                    #region Find Invoice Neighborhood
                    var invoiceFindNeighborhoodIdData = FindNeighborhoodId(
                                    unitOfWork,
                                    theOrder.OrderInvoiceAddress.City,
                                    theOrder.OrderInvoiceAddress.District,
                                    theOrder.OrderInvoiceAddress.Neighborhood);
                    if (!invoiceFindNeighborhoodIdData.Success)
                    {
                        XConsole.Error("Invoice neighborhood not found.");
                        return;
                    }
                    invoiceFindNeighborhoodId = invoiceFindNeighborhoodIdData.Data;
                    #endregion
                }




                #region Product Create
                var orderDetails = new List<Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

                var orderSkip = false;

                foreach (var theOrderDetail in theOrder.OrderDetails)
                {
                    #region Find Product Information
                    /* İlgili pazaryeri için stok kodu ve barkod alanları kullanılarak ürün eşleşmesi bulunur. */
                    ProductInformationMarketplace? productInformationMarketplace = null;

                    var marketplaceId = theOrder.MarketplaceId;

                    /* Barkod'dan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.Barcode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.Barcode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .OrderBy(x => x.ProductInformation.Product.IsOnlyHidden)
                            .FirstOrDefault(x => x.MarketplaceId == marketplaceId &&
                                                 x.Barcode.ToLower() == theOrderDetail.Product.Barcode.ToLower());


                    /* Stok kodundan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.StockCode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.StockCode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .FirstOrDefault(x => x.MarketplaceId == marketplaceId &&
                                                 x.StockCode.ToLower() == theOrderDetail.Product.StockCode.ToLower());
                    #endregion

                    var productInformationId = 0;
                    var unitCost = 0m;
                    var taxRate = 0m;
                    if (productInformationMarketplace == null)
                    {
                        if (this._company.CompanySetting.CreateNonexistProduct)
                        {
                            taxRate = Convert.ToDecimal(theOrderDetail.TaxRate);

                            //TO DO: Create Nonexist Product
                            var product = new Product
                            {
                                Active = false,
                                BrandId = _unitOfWork.BrandRepository.DbSet().FirstOrDefault().Id,
                                CategoryId = _unitOfWork.CategoryRepository.DbSet().FirstOrDefault().Id,
                                //GroupId = _group.Id,
                                SupplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id,
                                CreatedDate = DateTime.Now,
                                IsOnlyHidden = true,
                                ProductTranslations = new List<ProductTranslation>
                                {
                                        new ProductTranslation
                                        {
                                            LanguageId="TR",
                                            CreatedDate=DateTime.Now,

                                            Name=theOrderDetail.Product.Name
                                        }
                                },
                                ProductInformations = new List<ProductInformation>
                                {
                                        new ProductInformation
                                        {
                                            Barcode=theOrderDetail.Product.Barcode,

                                            StockCode=theOrderDetail.Product.StockCode,
                                            IsSale=false,
                                            CreatedDate=DateTime.Now,
                                            Stock=0,

                                            ProductInformationTranslations=new List<ProductInformationTranslation>
                                            {
                                                new ProductInformationTranslation{
                                                    CreatedDate=DateTime.Now,
                                                    LanguageId="TR",

                                                    Name=theOrderDetail.Product.Name,
                                                    Url="",
                                                    SubTitle=""}
                                            },
                                            ProductInformationPriceses=new List<ProductInformationPrice>
                                            {
                                                new ProductInformationPrice
                                                {

                                                    CreatedDate=DateTime.Now,
                                                    CurrencyId="TL",
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    VatRate=Convert.ToDecimal(theOrderDetail.TaxRate),
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    UnitCost=0
                                                }
                                            },
                                            ProductInformationMarketplaces = new()
                                            {
                                                new()
                                                {
                                                    Active=true,
                                                    Barcode=theOrderDetail.Product.Barcode,
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    StockCode=Guid.NewGuid().ToString()
                                                }
                                            }
                                        }
                                }
                            };
                            unitOfWork.ProductRepository.Create(product);

                            productInformationId = product.ProductInformations[0].Id;
                            unitCost = 0;
                        }
                        else
                        {
                            orderSkip = true;
                            break;
                        }
                    }
                    else
                    {
                        productInformationId = productInformationMarketplace.ProductInformationId;
                        taxRate = productInformationMarketplace
                          .ProductInformation
                          .ProductInformationPriceses
                          .FirstOrDefault()
                          .VatRate;
                        unitCost = productInformationMarketplace
                          .ProductInformation
                          .ProductInformationPriceses
                          .FirstOrDefault()
                          .UnitCost;
                    }

                    theOrderDetail.TaxRate = theOrder.Micro ? 0 : (double)taxRate; //Mikro ihracatsa fatura 0 olarak kesilir.
                    theOrderDetail.UnitCost = unitCost;
                    theOrderDetail.Product.ProductInformationId = productInformationId;

                    var averageCommission = this._marketplaceService.GetMarketplaceConfiguration(theOrder.MarketplaceId, "AverageCommission");

                    var unitCommissionAmount = 0.10m;
                    decimal.TryParse(averageCommission?.Value, out unitCommissionAmount);
                    if (theOrderDetail.UnitCommissionAmount == 0 && unitCommissionAmount > 0)
                    {
                        theOrderDetail.UnitCommissionAmount = theOrderDetail.UnitPrice * unitCommissionAmount;
                    }

                    if (productInformationMarketplace?.ProductInformation?.Type == "PC")
                    {
                        var productInformationCombines = unitOfWork
                            .ProductInformationCombineRepository
                            .DbSet()
                            .Include(x => x.ProductInformation)
                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                            .Where(x => x.ProductInformationId == productInformationMarketplace.ProductInformationId)
                            .ToList();

                        var totalQuantity = productInformationCombines.Sum(x => x.Quantity);
                        var ratioListPrice = theOrderDetail.ListPrice / totalQuantity;
                        var ratioUnitPrice = theOrderDetail.UnitPrice / totalQuantity;
                        var ratioUnitDiscount = theOrderDetail.UnitDiscount / totalQuantity;
                        var ratioUnitCommissionAmount = theOrderDetail.UnitCommissionAmount / totalQuantity;

                        foreach (var picLoop in productInformationCombines)
                        {
                            var productInformationPriceses = picLoop.ProductInformation.ProductInformationPriceses.FirstOrDefault();

                            var orderDetail = new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                            {
                                ListPrice = ratioListPrice,
                                Quantity = picLoop.Quantity * theOrderDetail.Quantity,
                                UnitPrice = ratioUnitPrice,
                                TaxRate = Convert.ToDouble(productInformationPriceses.VatRate),
                                UnitCost = productInformationPriceses.UnitCost,
                                Payor = false,
                                UnitDiscount = ratioUnitDiscount,
                                UnitCommissionAmount = ratioUnitCommissionAmount,
                                Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                {
                                    ProductInformationId = picLoop.ContainProductInformationId
                                }
                            };

                            orderDetails.Add(orderDetail);
                        }
                    }
                    else
                        orderDetails.Add(theOrderDetail);
                }

                if (orderSkip)
                {
                    XConsole.Error("Product not found.");
                    return;
                }
                #endregion

                #region Check And Create Shipment Company
                var _shipmentCompany = unitOfWork
                                    .ShipmentCompanyRepository
                                    .DbSet()
                                    .FirstOrDefault(x => x.Id == theOrder.OrderShipment.ShipmentCompanyId);
                if (_shipmentCompany == null)
                    unitOfWork.ShipmentCompanyRepository.Create(new ShipmentCompany
                    {
                        Id = theOrder.OrderShipment.ShipmentCompanyId,
                        Name = theOrder.OrderShipment.ShipmentCompanyId
                    });
                #endregion

                Customer customer = null;
                if (theOrder.CheckCustomer)
                    customer = unitOfWork
                        .CustomerRepository
                        .DbSet()
                        .Include(c => c.CustomerContact)
                        .Include(c => c.CustomerAddresses)
                        .FirstOrDefault(c => c.Name == theOrder.Customer.FirstName.ToUpperInvariant() &&
                                             c.Surname == theOrder.Customer.LastName.ToUpperInvariant());

                if (customer == null)
                    customer = new()
                    {
                        Name = theOrder.Customer.FirstName.ToUpperInvariant(),
                        Surname = theOrder.Customer.LastName.ToUpperInvariant(),
                        CustomerContact = new()
                        {
                            Phone = theOrder.Customer.Phone.ToNumber(),
                            Mail = theOrder.Customer.Mail,
                            TaxNumber = theOrder.Customer.TaxNumber,
                            TaxOffice = theOrder.Customer.TaxAdministration,
                            CreatedDate = DateTime.Now
                        },
                        CustomerAddresses = new()
                            {
                                new()
                                {
                                    NeighborhoodId = deliveryFindNeighborhoodId,
                                    Address = theOrder.OrderDeliveryAddress.Address,
                                    Recipient = $"{theOrder.Customer.FirstName} {theOrder.Customer.LastName}",
                                    CreatedDate = DateTime.Now
                                }
                            },
                        CreatedDate = DateTime.Now
                    };


                var exchangeRate = 1m;
                if (theOrder.CurrencyId != "TL")
                {
                    if (_exchangeRates == null)
                    {
                        _exchangeRates = _unitOfWork.ExchangeRateRepository.DbSet().OrderByDescending(x => x.CreatedDate).Where(d => d.CreatedDate >= DateTime.Now.Date.AddDays(-1)).ToList();
                    }

                    var firtExchangeRate = _exchangeRates.FirstOrDefault(x => x.CurrencyId == theOrder.CurrencyId);
                    if (firtExchangeRate != null)
                    {
                        exchangeRate = firtExchangeRate.Rate;
                    }
                }

                #region Create Order
                var orderEntity = new Order
                {
                    Commercial = theOrder.Commercial,
                    Micro = theOrder.Micro,
                    BulkInvoicing = theOrder.BulkInvoicing,
                    UUId = theOrder.UUId,
                    Customer = customer,
                    ExchangeRate = exchangeRate,
                    CurrencyId = theOrder.CurrencyId,
                    LanguageId = "TR",
                    OrderTypeId = theOrder.OrderTypeId,
                    OrderSourceId = theOrder.OrderSourceId,
                    OrderShipments = new List<OrderShipment>
                        {
                            new OrderShipment
                            {
                                TrackingUrl = theOrder.OrderShipment.TrackingUrl,
                                    TrackingCode = theOrder.OrderShipment.TrackingCode,
                                    TrackingBase64 = theOrder.OrderShipment.TrackingBase64,
                                    ShipmentCompanyId = _shipmentCompany.Id,
                                    CreatedDate = DateTime.Now,
                                    Payor = theOrder.OrderShipment.Payor,
                                    PackageNumber = theOrder.OrderShipment.PackageNumber
                                }
                        },
                    ListTotal = theOrder.ListTotalAmount,
                    Total = theOrder.TotalAmount,
                    VatExcTotal = theOrder.VatExcTotal,
                    VatExcDiscount = theOrder.VatExcDiscount,
                    VatExcListTotal = theOrder.VatExcListTotal,
                    Discount = theOrder.Discount,
                    CreatedDate = DateTime.Now,
                    CargoFee = theOrder.CargoFee,
                    SurchargeFee = theOrder.SurchargeFee,
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        Recipient = $"{theOrder.OrderDeliveryAddress.FirstName} {theOrder.OrderDeliveryAddress.LastName}",
                        CreatedDate = DateTime.Now,
                        PhoneNumber = theOrder.OrderDeliveryAddress.Phone.ToNumber(),
                        Address = theOrder.OrderDeliveryAddress.Address,
                        NeighborhoodId = deliveryFindNeighborhoodId
                    },
                    OrderInvoiceInformation = new OrderInvoiceInformation
                    {
                        CreatedDate = DateTime.Now,
                        NeighborhoodId = invoiceFindNeighborhoodId,
                        Address = theOrder.OrderInvoiceAddress.Address,
                        Phone = theOrder.OrderInvoiceAddress.Phone.ToNumber(),
                        TaxNumber = theOrder.OrderInvoiceAddress.TaxNumber,
                        TaxOffice = theOrder.OrderInvoiceAddress.TaxOffice,
                        FirstName = theOrder.OrderInvoiceAddress.FirstName,
                        LastName = theOrder.OrderInvoiceAddress.LastName,
                        Mail = theOrder.OrderInvoiceAddress.Email
                    },
                    OrderTechnicInformation = new OrderTechnicInformation
                    {
                        Browser = "",
                        IpAddress = "",
                        Platform = "",
                    },
                    OrderDetails = orderDetails.Select(x => new OrderDetail
                    {
                        UUId = x.UUId,
                        ListUnitPrice = x.ListPrice,
                        UnitDiscount = x.UnitDiscount,
                        ProductInformationId = x.Product.ProductInformationId,
                        Quantity = x.Quantity,
                        UnitCost = x.UnitCost,
                        UnitPrice = x.UnitPrice,
                        VatRate = Convert.ToDecimal(x.TaxRate),
                        CreatedDate = DateTime.Now,
                        Payor = x.Payor,
                        UnitCargoFee = theOrder.CargoFee > 0 ? theOrder.CargoFee / orderDetails.Sum(od => od.Quantity) : 0,
                        UnitCommissionAmount = x.UnitCommissionAmount,
                        VatExcUnitCost = x.VatExcUnitCost,
                        VatExcListUnitPrice = x.VatExcListUnitPrice,
                        VatExcUnitDiscount = x.VatExcUnitDiscount,
                        VatExcUnitPrice = x.VatExcUnitPrice
                    }).ToList(),
                    MarketplaceId = theOrder.MarketplaceId,
                    MarketplaceOrderNumber = theOrder.OrderCode,
                    CommissionAmount = theOrder.CommissionAmount,
                    OrderDate = theOrder.OrderDate,
                    EstimatedPackingDate = DateTime.Now,
                    Application = null
                };

                if (theOrder.Payments.HasItem())
                {
                    orderEntity.Payments = theOrder
                        .Payments
                        .Select(p => new Payment
                        {
                            Amount = p.Amount,
                            CreatedDate = DateTime.Now,
                            PaymentTypeId = p.PaymentTypeId,
                            Paid = p.Paid,
                            BankId = p.BankId,
                        })
                        .ToList();
                }

                if (!string.IsNullOrEmpty(theOrder.OrderNote))
                {
                    orderEntity.OrderNotes = new List<OrderNote>
                        {
                            new OrderNote
                            {
                                CreatedDate = DateTime.Now,
                                Note = theOrder.OrderNote
                            }
                        };
                }



                /*
                 * Operasyonel etiketler.
                 * Tekli, çoklu, farklı depolardan ürün içerir v.b.
                 */
                var operationalTags = new List<string>();
                operationalTags.Add(orderEntity.OrderDetails.Count == 1 ? "Tekli" : "Çoklu");



                var created = unitOfWork.OrderRepository.Create(orderEntity);
                #endregion

                if (created)
                {
                    XConsole.Success("Created.");

#if !DEBUG
                    var pickingRequest = new Application.Common.Parameters.OrderPickingRequest
                    {
                        OrderPicking = theOrder.OrderPicking,
                        MarketPlaceId = theOrder.MarketplaceId,
                        Status = Application.Common.Parameters.Enums.MarketplaceStatus.Picking,
                        Configurations = this
                                       ._marketplaceService
                                       .GetMarketplaceConfigurations(theOrder.MarketplaceId)
                                       .ToDictionary(mc => mc.Key, mc => mc.Value)
                    };

                    this._marketplaceService.OrderTypeUpdate(pickingRequest);

                    #region Update Stock
                    if (this._company.CompanySetting.StockControl)
                    {
                        orderEntity
                            .OrderDetails
                            .ForEach(odLoop => productInformationStockService.DecreaseStock(odLoop.ProductInformationId, odLoop.Quantity));
                    }
                    #endregion
#endif
                }
                else
                {
                    XConsole.Error("Not created.");
                }
            });
        }

        private void CreateTrendyolEuropeOrder(Application.Common.Wrappers.Marketplaces.Order.Order theOrder)
        {
            this.Scope(scope =>
            {

                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                var shipmentProviderService = scope.ServiceProvider.GetRequiredService<IShipmentProviderService>();
                var productInformationStockService = scope.ServiceProvider.GetRequiredService<IProductInformationStockService>();

                #region Find Delivery Neighborhood
                var deliveryFindNeighborhoodId = FindNeighborhoodId(
                                unitOfWork,
                                theOrder.OrderDeliveryAddress.City,
                                theOrder.OrderDeliveryAddress.District,
                                theOrder.OrderDeliveryAddress.Neighborhood);
                if (!deliveryFindNeighborhoodId.Success)
                {
                    XConsole.Error("Delivery neighborhood not found.");
                    return;
                }
                #endregion

                #region Find Invoice Neighborhood
                var invoiceFindNeighborhoodId = FindNeighborhoodId(
                                unitOfWork,
                                theOrder.OrderInvoiceAddress.City,
                                theOrder.OrderInvoiceAddress.District,
                                theOrder.OrderInvoiceAddress.Neighborhood);
                if (!invoiceFindNeighborhoodId.Success)
                {
                    XConsole.Error("Invoice neighborhood not found.");
                    return;
                }
                #endregion

                #region Product Create
                var orderDetails = new List<Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

                var orderSkip = false;

                foreach (var theOrderDetail in theOrder.OrderDetails)
                {
                    #region Find Product Information
                    /* İlgili pazaryeri için stok kodu ve barkod alanları kullanılarak ürün eşleşmesi bulunur. */
                    ProductInformationMarketplace? productInformationMarketplace = null;

                    var marketplaceId = theOrder.MarketplaceId;

                    /* Barkod'dan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.Barcode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.Barcode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .OrderBy(x => x.ProductInformation.Product.IsOnlyHidden)
                            .FirstOrDefault(x => x.MarketplaceId == marketplaceId &&
                                                 x.Barcode.ToLower() == theOrderDetail.Product.Barcode.ToLower());


                    /* Stok kodundan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.StockCode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.StockCode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .FirstOrDefault(x => x.MarketplaceId == marketplaceId &&
                                                 x.StockCode.ToLower() == theOrderDetail.Product.StockCode.ToLower());
                    #endregion

                    var productInformationId = 0;
                    var unitCost = 0m;
                    var taxRate = 0m;
                    if (productInformationMarketplace == null)
                    {
                        if (this._company.CompanySetting.CreateNonexistProduct)
                        {
                            taxRate = Convert.ToDecimal(theOrderDetail.TaxRate);

                            //TO DO: Create Nonexist Product
                            var product = new Product
                            {
                                Active = false,
                                BrandId = _unitOfWork.BrandRepository.DbSet().FirstOrDefault().Id,
                                CategoryId = _unitOfWork.CategoryRepository.DbSet().FirstOrDefault().Id,
                                //GroupId = _group.Id,
                                SupplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id,
                                CreatedDate = DateTime.Now,
                                IsOnlyHidden = true,
                                ProductTranslations = new List<ProductTranslation>
                                {
                                        new ProductTranslation
                                        {
                                            LanguageId="TR",
                                            CreatedDate=DateTime.Now,

                                            Name=theOrderDetail.Product.Name
                                        }
                                },
                                ProductInformations = new List<ProductInformation>
                                {
                                        new ProductInformation
                                        {
                                            Barcode=theOrderDetail.Product.Barcode,

                                            StockCode=theOrderDetail.Product.StockCode,
                                            IsSale=false,
                                            CreatedDate=DateTime.Now,
                                            Stock=0,

                                            ProductInformationTranslations=new List<ProductInformationTranslation>
                                            {
                                                new ProductInformationTranslation{
                                                    CreatedDate=DateTime.Now,
                                                    LanguageId="TR",

                                                    Name=theOrderDetail.Product.Name,
                                                    Url="",
                                                    SubTitle=""}
                                            },
                                            ProductInformationPriceses=new List<ProductInformationPrice>
                                            {
                                                new ProductInformationPrice
                                                {

                                                    CreatedDate=DateTime.Now,
                                                    CurrencyId="TL",
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    VatRate=Convert.ToDecimal(theOrderDetail.TaxRate),
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    UnitCost=0
                                                }
                                            },
                                            ProductInformationMarketplaces = new()
                                            {
                                                new()
                                                {
                                                    Active=true,
                                                    Barcode=theOrderDetail.Product.Barcode,
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    StockCode=Guid.NewGuid().ToString()
                                                }
                                            }
                                        }
                                }
                            };
                            unitOfWork.ProductRepository.Create(product);

                            productInformationId = product.ProductInformations[0].Id;
                            unitCost = 0;
                        }
                        else
                        {
                            orderSkip = true;
                            break;
                        }
                    }
                    else
                    {
                        productInformationId = productInformationMarketplace.ProductInformationId;
                        taxRate = productInformationMarketplace
                          .ProductInformation
                          .ProductInformationPriceses
                          .FirstOrDefault()
                          .VatRate;
                        unitCost = productInformationMarketplace
                          .ProductInformation
                          .ProductInformationPriceses
                          .FirstOrDefault()
                          .UnitCost;
                    }

                    theOrderDetail.TaxRate = theOrder.Micro ? 0 : (double)taxRate; //Mikro ihracatsa fatura 0 olarak kesilir.
                    theOrderDetail.UnitCost = unitCost;
                    theOrderDetail.Product.ProductInformationId = productInformationId;

                    var averageCommission = this._marketplaceService.GetMarketplaceConfiguration(theOrder.MarketplaceId, "AverageCommission");

                    var unitCommissionAmount = 0.10m;
                    decimal.TryParse(averageCommission?.Value, out unitCommissionAmount);
                    if (theOrderDetail.UnitCommissionAmount == 0 && unitCommissionAmount > 0)
                    {
                        theOrderDetail.UnitCommissionAmount = theOrderDetail.UnitPrice * unitCommissionAmount;
                    }

                    if (productInformationMarketplace?.ProductInformation?.Type == "PC")
                    {
                        var productInformationCombines = unitOfWork
                            .ProductInformationCombineRepository
                            .DbSet()
                            .Include(x => x.ProductInformation)
                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                            .Where(x => x.ProductInformationId == productInformationMarketplace.ProductInformationId)
                            .ToList();

                        var totalQuantity = productInformationCombines.Sum(x => x.Quantity);
                        var ratioListPrice = theOrderDetail.ListPrice / totalQuantity;
                        var ratioUnitPrice = theOrderDetail.UnitPrice / totalQuantity;
                        var ratioUnitDiscount = theOrderDetail.UnitDiscount / totalQuantity;
                        var ratioUnitCommissionAmount = theOrderDetail.UnitCommissionAmount / totalQuantity;

                        foreach (var picLoop in productInformationCombines)
                        {
                            var productInformationPriceses = picLoop.ProductInformation.ProductInformationPriceses.FirstOrDefault();

                            var orderDetail = new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                            {
                                ListPrice = ratioListPrice,
                                Quantity = picLoop.Quantity * theOrderDetail.Quantity,
                                UnitPrice = ratioUnitPrice,
                                TaxRate = Convert.ToDouble(productInformationPriceses.VatRate),
                                UnitCost = productInformationPriceses.UnitCost,
                                Payor = false,
                                UnitDiscount = ratioUnitDiscount,
                                UnitCommissionAmount = ratioUnitCommissionAmount,
                                Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                {
                                    ProductInformationId = picLoop.ContainProductInformationId
                                }
                            };

                            orderDetails.Add(orderDetail);
                        }
                    }
                    else
                        orderDetails.Add(theOrderDetail);
                }

                if (orderSkip)
                {
                    XConsole.Error("Product not found.");
                    return;
                }
                #endregion

                #region Check And Create Shipment Company
                var _shipmentCompany = unitOfWork
                                    .ShipmentCompanyRepository
                                    .DbSet()
                                    .FirstOrDefault(x => x.Id == theOrder.OrderShipment.ShipmentCompanyId);
                if (_shipmentCompany == null)
                    unitOfWork.ShipmentCompanyRepository.Create(new ShipmentCompany
                    {
                        Id = theOrder.OrderShipment.ShipmentCompanyId,
                        Name = theOrder.OrderShipment.ShipmentCompanyId
                    });
                #endregion

                Customer customer = null;
                if (theOrder.CheckCustomer)
                    customer = unitOfWork
                        .CustomerRepository
                        .DbSet()
                        .Include(c => c.CustomerContact)
                        .Include(c => c.CustomerAddresses)
                        .FirstOrDefault(c => c.Name == theOrder.Customer.FirstName.ToUpperInvariant() &&
                                             c.Surname == theOrder.Customer.LastName.ToUpperInvariant());

                if (customer == null)
                    customer = new()
                    {
                        Name = theOrder.Customer.FirstName.ToUpperInvariant(),
                        Surname = theOrder.Customer.LastName.ToUpperInvariant(),
                        CustomerContact = new()
                        {
                            Phone = theOrder.Customer.Phone.ToNumber(),
                            Mail = theOrder.Customer.Mail,
                            TaxNumber = theOrder.Customer.TaxNumber,
                            TaxOffice = theOrder.Customer.TaxAdministration,
                            CreatedDate = DateTime.Now
                        },
                        CustomerAddresses = new()
                            {
                                new()
                                {
                                    NeighborhoodId = deliveryFindNeighborhoodId.Data,
                                    Address = theOrder.OrderDeliveryAddress.Address,
                                    Recipient = $"{theOrder.Customer.FirstName} {theOrder.Customer.LastName}",
                                    CreatedDate = DateTime.Now
                                }
                            },
                        CreatedDate = DateTime.Now
                    };

                var exchangeRate = 1m;
                if (theOrder.CurrencyId != "TL")
                {
                    if (_exchangeRates == null)
                    {
                        _exchangeRates = _unitOfWork.ExchangeRateRepository.DbSet().OrderByDescending(x => x.CreatedDate).Where(d => d.CreatedDate >= DateTime.Now.Date.AddDays(-1)).ToList();
                    }

                    var firtExchangeRate = _exchangeRates.FirstOrDefault(x => x.CurrencyId == theOrder.CurrencyId);
                    if (firtExchangeRate != null)
                    {
                        exchangeRate = firtExchangeRate.Rate;
                    }
                }

                /* 
                 * Azalacak stok rakamları. 
                 */
                Dictionary<int, int> willDecreaseStock = new();
                Dictionary<int, int> willIncreaseStock = new();

                #region Create Order
                var newOrder = new Order
                {
                    Commercial = theOrder.Commercial,
                    Micro = theOrder.Micro,
                    BulkInvoicing = theOrder.BulkInvoicing,
                    UUId = theOrder.UUId,
                    Customer = customer,
                    ExchangeRate = exchangeRate,
                    CurrencyId = theOrder.CurrencyId,
                    LanguageId = "TR",
                    OrderTypeId = string.IsNullOrEmpty(theOrder.OrderShipment.TrackingCode)
                        ? OrderTypes.Beklemede
                        : OrderTypes.OnaylanmisSiparis,
                    OrderSourceId = theOrder.OrderSourceId,
                    OrderShipments = new List<OrderShipment>
                        {
                            new OrderShipment
                            {
                                TrackingUrl = theOrder.OrderShipment.TrackingUrl,
                                    TrackingCode = theOrder.OrderShipment.TrackingCode,
                                    TrackingBase64 = theOrder.OrderShipment.TrackingBase64,
                                    ShipmentCompanyId = _shipmentCompany.Id,
                                    CreatedDate = DateTime.Now,
                                    Payor = theOrder.OrderShipment.Payor,
                                    PackageNumber = theOrder.OrderShipment.PackageNumber
                                }
                        },
                    ListTotal = theOrder.ListTotalAmount,
                    Total = theOrder.TotalAmount,
                    VatExcTotal = theOrder.VatExcTotal,
                    VatExcDiscount = theOrder.VatExcDiscount,
                    VatExcListTotal = theOrder.VatExcListTotal,
                    Discount = theOrder.Discount,
                    CreatedDate = DateTime.Now,
                    CargoFee = theOrder.CargoFee,
                    SurchargeFee = theOrder.SurchargeFee,
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        Recipient = $"{theOrder.OrderDeliveryAddress.FirstName} {theOrder.OrderDeliveryAddress.LastName}",
                        CreatedDate = DateTime.Now,
                        PhoneNumber = theOrder.OrderDeliveryAddress.Phone.ToNumber(),
                        Address = theOrder.OrderDeliveryAddress.Address,
                        NeighborhoodId = deliveryFindNeighborhoodId.Data
                    },
                    OrderInvoiceInformation = new OrderInvoiceInformation
                    {
                        CreatedDate = DateTime.Now,
                        NeighborhoodId = invoiceFindNeighborhoodId.Data,
                        Address = theOrder.OrderInvoiceAddress.Address,
                        Phone = theOrder.OrderInvoiceAddress.Phone.ToNumber(),
                        TaxNumber = theOrder.OrderInvoiceAddress.TaxNumber,
                        TaxOffice = theOrder.OrderInvoiceAddress.TaxOffice,
                        FirstName = theOrder.OrderInvoiceAddress.FirstName,
                        LastName = theOrder.OrderInvoiceAddress.LastName,
                        Mail = theOrder.OrderInvoiceAddress.Email
                    },
                    OrderTechnicInformation = new OrderTechnicInformation
                    {
                        Browser = "",
                        IpAddress = "",
                        Platform = "",
                    },
                    OrderDetails = orderDetails.Select(x => new OrderDetail
                    {
                        UUId = x.UUId,
                        ListUnitPrice = x.ListPrice,
                        UnitDiscount = x.UnitDiscount,
                        ProductInformationId = x.Product.ProductInformationId,
                        Quantity = x.Quantity,
                        UnitCost = x.UnitCost,
                        UnitPrice = x.UnitPrice,
                        VatRate = Convert.ToDecimal(x.TaxRate),
                        CreatedDate = DateTime.Now,
                        Payor = x.Payor,
                        UnitCargoFee = theOrder.CargoFee > 0 ? theOrder.CargoFee / orderDetails.Sum(od => od.Quantity) : 0,
                        UnitCommissionAmount = x.UnitCommissionAmount,
                        VatExcUnitCost = x.VatExcUnitCost,
                        VatExcListUnitPrice = x.VatExcListUnitPrice,
                        VatExcUnitDiscount = x.VatExcUnitDiscount,
                        VatExcUnitPrice = x.VatExcUnitPrice
                    }).ToList(),
                    MarketplaceId = theOrder.MarketplaceId,
                    MarketplaceOrderNumber = theOrder.OrderCode,
                    CommissionAmount = theOrder.CommissionAmount,
                    OrderDate = theOrder.OrderDate,
                    EstimatedPackingDate = DateTime.Now,
                    Application = null
                };

                if (theOrder.Payments.HasItem())
                {
                    newOrder.Payments = theOrder
                        .Payments
                        .Select(p => new Payment
                        {
                            Amount = p.Amount,
                            CreatedDate = DateTime.Now,
                            PaymentTypeId = p.PaymentTypeId,
                            Paid = p.Paid
                        })
                        .ToList();
                }

                if (!string.IsNullOrEmpty(theOrder.OrderNote))
                {
                    newOrder.OrderNotes = new List<OrderNote>
                        {
                            new OrderNote
                            {
                                CreatedDate = DateTime.Now,
                                Note = theOrder.OrderNote
                            }
                        };
                }

                /*
                 * Operasyonel etiketler.
                 * Tekli, çoklu, farklı depolardan ürün içerir v.b.
                 */
                var operationalTags = new List<string>
                {
                    newOrder.OrderDetails.Count == 1 ? "Tekli" : "Çoklu"
                };

                bool createdOrDuplicate = false;

                /* Check Duplicate */
                var duplicateOrder = unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Any(_ => _.MarketplaceId == theOrder.MarketplaceId &&
                              _.MarketplaceOrderNumber == theOrder.OrderCode);

                if (duplicateOrder)
                {
                    var existingOrder = unitOfWork
                        .OrderRepository
                        .DbSet()
                        .Include(_ => _.OrderDetails)
                        .Include(_ => _.OrderShipments)
                        .Include(_ => _.OrderBilling)
                        .FirstOrDefault(_ => _.MarketplaceId == theOrder.MarketplaceId &&
                                             _.MarketplaceOrderNumber == theOrder.OrderCode);

                    if (existingOrder.OrderBilling is not null)
                    {
                        Console.WriteLine("Sipariş faturalandırılmış.");
                        return;
                    }

                    createdOrDuplicate = true;

                    var requireUpdate = false;

                    foreach (var newOrderDetail in newOrder.OrderDetails)
                    {
                        var existingOrderDetail = existingOrder
                            .OrderDetails
                            .FirstOrDefault(_ => _.ProductInformationId == newOrderDetail.ProductInformationId);

                        if (existingOrderDetail.Quantity < newOrderDetail.Quantity)
                        {
                            willDecreaseStock.Add(
                                existingOrderDetail.ProductInformationId,
                                newOrderDetail.Quantity - existingOrderDetail.Quantity);

                            existingOrderDetail.Quantity = newOrderDetail.Quantity;

                            requireUpdate = true;

                            continue;
                        }

                        if (existingOrderDetail.Quantity > newOrderDetail.Quantity)
                        {
                            willIncreaseStock.Add(
                                existingOrderDetail.ProductInformationId,
                                existingOrderDetail.Quantity - newOrderDetail.Quantity);

                            existingOrderDetail.Quantity = newOrderDetail.Quantity;

                            requireUpdate = true;
                        }
                    }

                    if (string.IsNullOrEmpty(newOrder.OrderShipments.FirstOrDefault().TrackingCode) == false &&
                        existingOrder.OrderTypeId == OrderTypes.Beklemede)
                    {
                        requireUpdate = true;
                        existingOrder.OrderTypeId = OrderTypes.OnaylanmisSiparis;
                        existingOrder.OrderShipments.FirstOrDefault().TrackingCode = newOrder.OrderShipments.FirstOrDefault().TrackingCode;
                    }

                    if (requireUpdate)
                    {
                        existingOrder.Total = newOrder.Total;
                        existingOrder.ListTotal = newOrder.ListTotal;

                        unitOfWork.OrderRepository.Update(existingOrder);
                    }
                }
                else
                {
                    createdOrDuplicate = unitOfWork.OrderRepository.Create(newOrder);

                    newOrder.OrderDetails.ForEach(_ => willDecreaseStock.Add(_.ProductInformationId, _.Quantity));
                }
                #endregion

                if (createdOrDuplicate)
                {
                    XConsole.Success("Created.");

                    #region Update Stock
                    if (this._company.CompanySetting.StockControl)
                    {
                        foreach (var item in willDecreaseStock)
                        {
                            productInformationStockService.DecreaseStock(item.Key, item.Value);
                        }

                        foreach (var item in willIncreaseStock)
                        {
                            productInformationStockService.IncreaseStock(item.Key, item.Value);
                        }
                    }
                    #endregion
                }
                else
                {
                    XConsole.Error("Not created.");
                }
            });
        }


        public void RawOrders(string? marketplaceId = null)
        {
            this._marketplaceService.RawOrders(marketplaceId: marketplaceId);
        }

        public void ProcessRawOrders(string? marketplaceId = null)
        {
            this._marketplaceService.ProcessRawOrders(marketplaceId: marketplaceId);
        }

        public void ProcessOrderTypeUpdates(string? marketplaceId = null)
        {
            this._marketplaceService.ProcessOrderTypeUpdates(marketplaceId: marketplaceId);
        }

        public void ProcessMutualBarcodes(string? marketplaceId = null)
        {
            this._marketplaceService.ProcessMutualBarcodes(marketplaceId: marketplaceId);
        }

        public void CancelledOrders(string? marketplaceId = null)
        {
            var response = this._marketplaceService.GetCancelledOrders(marketplaceId);
            if (response.Success == false) return;

            var updatedOrdersCount = this.BulkCancelledUpdate(response.Data.Orders.Select(o => new Order
            {
                TenantId = this._tenant.Id,
                DomainId = this._domain.Id,
                CompanyId = this._company.Id,
                MarketplaceId = o.MarketplaceId,
                MarketplaceOrderNumber = o.OrderCode,
                OrderShipments = o.OrderShipment != null ?
                    new List<OrderShipment>
                    {
                        new OrderShipment
                        {
                            PackageNumber = o.OrderShipment.PackageNumber,
                            TrackingCode = o.OrderShipment.TrackingCode
                        }
                    } :
                    null
            }).ToList());

            Console.WriteLine($"CancelledOrders {updatedOrdersCount}");
        }

        public void DeliveredOrders(string? marketplaceId = null)
        {
            var response = this._marketplaceService.GetDeliveredOrders(marketplaceId);
            if (response.Success == false) return;

            var updatedOrdersCount = this.BulkUpdate(response.Data.Orders.Select(o => new Order
            {
                TenantId = this._tenant.Id,
                DomainId = this._domain.Id,
                CompanyId = this._company.Id,
                MarketplaceId = o.MarketplaceId,
                MarketplaceOrderNumber = o.OrderCode,
                OrderShipments = o.OrderShipment != null ?
                    new List<OrderShipment>
                    {
                        new OrderShipment
                        {
                            TrackingCode = o.OrderShipment.TrackingCode,
                        }
                    } :
                    null
            }).ToList(), "TS", new[] { "TA", "TS", "IP", "IA" });

            Console.WriteLine($"DeliveredOrders {updatedOrdersCount}");
        }

        public void ReturnedOrders(string? marketplaceId = null)
        {
            var response = this._marketplaceService.GetReturnedOrders(marketplaceId);
            if (response.Success == false) return;

            var updatedOrdersCount = this.BulkUpdate(response.Data.Orders.Select(o => new Order
            {
                TenantId = this._tenant.Id,
                DomainId = this._domain.Id,
                CompanyId = this._company.Id,
                MarketplaceId = o.MarketplaceId,
                MarketplaceOrderNumber = o.OrderCode,
                OrderShipments = o.OrderShipment != null ?
                    new List<OrderShipment>
                    {
                        new OrderShipment
                        {
                            TrackingCode = o.OrderShipment.TrackingCode,
                        }
                    } :
                    null
            }).ToList(), "IP", new[] { "IP", "IA" });

            Console.WriteLine($"ReturnedOrders {updatedOrdersCount}");
        }

        public void ProductsStatus(string? marketplaceId = null)
        {
            var productsStatusResponse = this._marketplaceService.GetProductsStatus(marketplaceId);
            if (productsStatusResponse.Success && productsStatusResponse.Data.Count > 0)
            {
                #region Bulk Compare
                this
                    ._unitOfWork
                    .ProductInformationMarketplaceRepository
                    .BulkCompare(new BulkCompareRequest
                    {
                        KeyColumnName = "StockCode",
                        MarketplaceId = marketplaceId,
                        BulkCompareItems = productsStatusResponse
                            .Data
                            .Where(ps => string.IsNullOrEmpty(ps.Barcode) && !string.IsNullOrEmpty(ps.StockCode))
                            .Select(ps => new BulkCompareItem
                            {
                                MarketplaceId = ps.MarketplaceId,
                                Opened = ps.Opened,
                                UUId = ps.UUId,
                                Locked = ps.Locked,
                                OnSale = ps.OnSale,
                                Url = ps.Url,
                                Key = ps.StockCode,
                                UnitPrice = ps.UnitPrice,
                                Stock = ps.Stock,
                                DirtyStockDisabled = ps.DirtyStockDisabled,
                                DirtyPriceDisabled = ps.DirtyPriceDisabled,
                            })
                            .DistinctBy(bci => new
                            {
                                bci.Key,
                                bci.MarketplaceId
                            })
                            .ToList()
                    });

                this
                    ._unitOfWork
                    .ProductInformationMarketplaceRepository
                    .BulkCompare(new BulkCompareRequest
                    {
                        KeyColumnName = "Barcode",
                        MarketplaceId = marketplaceId,
                        BulkCompareItems = productsStatusResponse
                            .Data
                            .Where(ps => !string.IsNullOrEmpty(ps.Barcode))
                            .Select(ps => new BulkCompareItem
                            {
                                MarketplaceId = ps.MarketplaceId,
                                Opened = ps.Opened,
                                Locked = ps.Locked,
                                Url = ps.Url,
                                UUId = ps.UUId,
                                OnSale = ps.OnSale,
                                Key = ps.Barcode,
                                UnitPrice = ps.UnitPrice,
                                Stock = ps.Stock,
                                DirtyStockDisabled = ps.DirtyStockDisabled,
                                DirtyPriceDisabled = ps.DirtyPriceDisabled,
                            })
                            .DistinctBy(bci => new
                            {
                                bci.Key,
                                bci.MarketplaceId
                            })
                            .ToList()
                    });
                #endregion
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Scope içerisinde çalışması istenilen metodlara şablon görevi görür.
        /// </summary>
        /// <param name="action">Metod.</param>
        private void Scope(Action<IServiceScope> action)
        {
            using var scope = this._serviceProvider.CreateScope();
            action(scope);
        }

        private DataResponse<int> FindNeighborhoodId(IUnitOfWork unitOfWork, string cityName, string districtName, string neighborhoodName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Find Address
                cityName = cityName.Trim();

                if (cityName.ToLower() == "afyonkarahisar")
                    cityName = "Afyon";

                if (cityName.ReplaceChar().ToLower() == "adapazari")
                {
                    cityName = "sakarya";
                    districtName = "adapazari";
                }

                if (cityName.ReplaceChar().ToLower() == "icel")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }


                if (cityName.ReplaceChar().ToLower() == "Afyon")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }

                var city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == cityName.ReplaceChar());

                if (city == null)
                {
                    city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).FirstOrDefault();
                }

                if (districtName == "Eyüpsultan" && city.Id == 1)
                    districtName = "Eyüp";

                else if (districtName == "Kahramankazan" && city.Id == 8)
                    districtName = "Kazan";

                else if (districtName == "Merkez" && city.Id == 32)
                    districtName = "Palandöken";

                else if (districtName == "Merkez" && city.Id == 26)
                    districtName = "Merkezefendi";

                else if (districtName == "Merkez" && city.Id == 63)
                    districtName = "Altınordu";

                else if (districtName == "Merkez" && city.Id == 58)
                    districtName = "Yenişehir";

                else if (districtName == "Merkez" && city.Id == 42)
                    districtName = "Onikişubat";

                else if (districtName == "Mustafa k. paşa" && city.Id == 22)
                    districtName = "Mustafakemalpaşa";

                else if (districtName == "Marmara Ereğlisi" && city.Id == 73)
                    districtName = "Marmaraereğlisi";

                else if (districtName == "Beşikdözü" && city.Id == 75)
                    districtName = "Beşikdüzü";

                else if (districtName == "Merkez" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Lara Kundu" && city.Id == 9)
                    districtName = "Aksu";

                else if (districtName == "Merkez" && city.Id == 71)
                    districtName = "Karaköprü";

                else if (districtName == "Merkez" && city.Id == 73)
                    districtName = "Süleymanpaşa";

                else if (districtName == "Merkez" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 78)
                    districtName = "Tuşba";

                else if (districtName == "Merkez" && city.Id == 12)
                    districtName = "Efeler";

                else if (districtName == "Merkez" && city.Id == 56)
                    districtName = "Yunusemre";

                else if (districtName == "Merkez" && city.Id == 55)
                    districtName = "Battalgazi";

                else if (districtName == "Merkez" && city.Id == 33)
                    districtName = "Odunpazarı";

                else if (districtName == "Yenişehir" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 59)
                    districtName = "Menteşe";

                else if (districtName == "Merkez" && city.Id == 75)
                    districtName = "Ortahisar";

                else if (districtName == "Baraj Yolu" && city.Id == 2)
                    districtName = "Seyhan";

                else if (districtName == "Merkez" && city.Id == 34)
                    districtName = "Şahinbey";

                else if (districtName == "Merkez" && city.Id == 41)
                    districtName = "Konak";

                else if (districtName == "Ondokuz Mayıs" && city.Id == 67)
                    districtName = "19 Mayıs";

                else if (districtName == "Merkez" && city.Id == 67)
                    districtName = "İlkadım";

                else if (districtName == "Kuruçaşile" && city.Id == 14)
                    districtName = "Kurucaşile";

                else if (districtName == "İçel" && city.Id == 58)
                    districtName = "Silifke";

                else if (districtName == "Ofis" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Gördeş" && city.Id == 56)
                    districtName = "Gördes";

                else if (districtName == "Sultanhanı" && city.Id == 6)
                    districtName = "Merkez";

                else if (districtName == "Merkez" && city.Id == 22)
                    districtName = "Osmangazi";

                else if (districtName == "Merkez" && city.Id == 66)
                    districtName = "Adapazarı";

                var district = unitOfWork.DistrictRepository.DbSet().Where(x => x.CityId == city.Id).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == districtName.ReplaceChar());


                if (district == null)
                {
                    district = unitOfWork.DistrictRepository.DbSet().FirstOrDefault(x => x.CityId == city.Id);
                }

                var _neighborhoodName = neighborhoodName.ReplaceChar().Split(new string[] { "mah" }, StringSplitOptions.TrimEntries)[0];

                var neighborhood = unitOfWork.NeighborhoodRepository.DbSet()
                    .Where(x => x.DistrictId == district.Id)
                    .ToList()
                    .FirstOrDefault(x => x.Name.ReplaceChar() == $"{_neighborhoodName} mah");
                if (neighborhood == null)
                {
                    neighborhood = unitOfWork.NeighborhoodRepository.DbSet().FirstOrDefault(x => x.DistrictId == district.Id);
                }

                response.Data = neighborhood.Id;
                response.Success = true;
                #endregion

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }


        private DataResponse<int> FindMicroNeighborhoodId(IUnitOfWork unitOfWork, string countryName, string cityName, string districtName, string neighborhoodName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Find Address
                var country = unitOfWork.CountryRepository.DbSet().ToList().FirstOrDefault(x => x.Name.ReplaceChar() == countryName.ReplaceChar());
                if (country == null)
                {
                    country = new Country
                    {
                        Active = true,
                        Name = countryName
                    };

                    unitOfWork.CountryRepository.Create(country);

                }

                var city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == country.Id).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == cityName.ReplaceChar());

                if (city == null)
                {
                    city = new City
                    {
                        Name = cityName,
                        Active = true,
                        CountryId = country.Id
                    };
                    unitOfWork.CityRepository.Create(city);
                }

                var district = unitOfWork.DistrictRepository.DbSet().Where(x => x.CityId == city.Id).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == districtName.ReplaceChar());


                if (district == null)
                {
                    district = new District
                    {
                        Active = true,
                        Name = districtName,
                        CityId = city.Id,
                    };
                    unitOfWork.DistrictRepository.Create(district);
                }


                var neighborhood = unitOfWork.NeighborhoodRepository.DbSet()
                    .Where(x => x.DistrictId == district.Id)
                    .ToList()
                    .FirstOrDefault(x => x.Name.ReplaceChar() == neighborhoodName.ReplaceChar());
                if (neighborhood == null)
                {
                    neighborhood = new Neighborhood
                    {
                        Name = neighborhoodName,
                        Active = true,
                        DistrictId = district.Id,
                    };
                    unitOfWork.NeighborhoodRepository.Create(neighborhood);
                }

                response.Data = neighborhood.Id;
                response.Success = true;
                #endregion

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }


        private Int32 BulkUpdate(List<Order> orders, string orderTypeId, string[] notOrderTypeIds = null)
        {
            var delivered = this
                   ._unitOfWork
                   .OrderRepository
                   .DeliveredBulkCompare(orders, orderTypeId, notOrderTypeIds);

            return delivered;
        }

        private Int32 BulkCancelledUpdate(List<Order> orders)
        {
            var cancelled = this
                 ._unitOfWork
                 .OrderRepository
                 .CanceledBulkCompare(orders);

            return cancelled;
        }



        #endregion
    }
}