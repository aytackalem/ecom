﻿namespace ECommerce.Job.Marketplace
{
    public interface IMain
    {
        #region Methods
        Task Run(string[] args); 
        #endregion
    }
}
