﻿namespace ECommerce.Job.Marketplace
{
    public interface IAppV2
    {
        #region Methods
        void Run(string[] args); 
        #endregion
    }
}
