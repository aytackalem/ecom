﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Marketplace;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Terminal.Helpers;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Marketplace
{
    class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddScoped<IMain, Main>();

            serviceCollection.AddScoped<IAppV2, AppV2>();

            serviceCollection.AddScoped<IDbService, DbService>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();

            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();

            serviceCollection.AddScoped<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddScoped<IProductInformationStockService, ProductInformationStockService>();

            serviceCollection.AddMemoryCache();

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();

            serviceCollection.AddTransient<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();

            serviceCollection.AddDbContext<ApplicationDbContext>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            Stopwatch stopwatch = new();
            stopwatch.Start();

#if DEBUG
            args = new string[] { "Delivered" };
#endif
            //args = new string[] { "Orders", "BY" };
            //args = new string[] { "Product", "IK" };
            //args = new string[] { "ProductsStatus", "IK" };
            //args = new string[] { "Orders", "PA" };
            //args = new string[] { "CancelledReturned", "HB" };
            //args = new string[] { "Delivered" };
            //args = new string[] { "RawOrders", "TY" };
            //args = new string[] { "ProcessRawOrders", "TY" };
            //args = new string[] { "ProcessOrderTypeUpdates", "TY" };
            //args = new string[] { "ProcessMutualBarcodes", "HB" };

            await ConfigureServiceCollection(GetConfiguration()).GetRequiredService<IMain>()?.Run(args);

            stopwatch.Stop();
        }
        #endregion
    }
}