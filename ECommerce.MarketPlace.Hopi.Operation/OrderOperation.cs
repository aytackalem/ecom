﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hopi.Operation
{
    public class OrderOperation : IOrderFacade
    {

        #region Fields
        public readonly IHttpHelper _httpHelper;
        #endregion


        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;

        }
        #endregion


        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                Order order = new Order();

                order.OrderSourceId = OrderSources.Pazaryeri;
                order.MarketPlaceId = Marketplaces.Hopi;


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }


        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
         

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }


        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Pazarama ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
    }
}
