﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Stock;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.ProductFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Hopi.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;

        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

              

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                response.Success = false;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }


        private DataResponse<ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductFilterResponse>>((response) =>
            {
               

                response.Success = true;
            });
        }
        #endregion
    }
}
