﻿using ECommerce.Application.Common.DataTransferObjects.XmlService;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.Net;

namespace ECommerce.Job.XmlV2
{
    public class App
    {
        #region Fields
        private readonly IHttpHelperV2 _httpHelperV2;

        private readonly IXmlService _xmlService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public App(IHttpHelperV2 httpHelperV2, IXmlService xmlService, IConfiguration configuration)
        {
            #region Fields
            this._httpHelperV2 = httpHelperV2;
            this._xmlService = xmlService;
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(string[] args)
        {
            var xmlSources = new List<XmlSource>()
            {
                new XmlSource
                {
                    RequestUri = "https://backoffice.helpy.com.tr/query5.xml",
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "product",
                        XmlItemMappings = new List<XmlItemMapping> {
                            new XmlItemMapping("urunkodu", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("urunadi", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("marka", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("model", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("vergi", "VatRate", XmlItemType.Property),
                            new XmlItemMapping("indirimlifiyatvergili", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("satisfiyativergili", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("resimler", "Photos", XmlItemType.Property)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("resim", "Photos", XmlItemType.Property)
                                }
                            },
                            new XmlItemMapping("ayrintilar2", "ProductDescription", XmlItemType.Property),
                            new XmlItemMapping("ayrintilar2", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("ozellikler", "ProductContent", XmlItemType.Attribute)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("ozellik", "Photos", XmlItemType.Attribute)
                                    {
                                        KeyNodeName = "adi",
                                        ValueNodeName = "degeri"
                                    }
                                }
                            }
                        }
                    }
                },
                new XmlSource
                {
                    RequestUri = "https://www.aslansaat.com/media/feed/simgesaat101121v1ss.xml",
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "item",
                        XmlItemMappings = new List<XmlItemMapping> {
                            new XmlItemMapping("URUN_ADI", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("MARKA", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("SKU", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("BARCODE", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("PSF", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("SATIS_FIYATI", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("STOK", "Stock", XmlItemType.Property),
                            new XmlItemMapping("GORSEL", "Photos", XmlItemType.Property),
                            new XmlItemMapping("GORSEL_DIGER2", "Photos", XmlItemType.Property),
                            new XmlItemMapping("GORSEL_DIGER3", "Photos", XmlItemType.Property),
                            new XmlItemMapping("GORSEL_DIGER4", "Photos", XmlItemType.Property),
                            new XmlItemMapping("VARYANT_GRUP_KODU", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("CINSIYET", string.Empty, XmlItemType.Attribute) { Key = "Cinsiyet" },
                            new XmlItemMapping("KOLEKSIYON", string.Empty, XmlItemType.Attribute) { Key = "Koleksiyon" },
                            new XmlItemMapping("TEKNOLOJI", string.Empty, XmlItemType.Attribute) { Key = "Tekonoloji" },
                            new XmlItemMapping("KASA_MATERYALI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Materyali" },
                            new XmlItemMapping("KASA_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Rengi" },
                            new XmlItemMapping("KASA_SEKLI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Şekli" },
                            new XmlItemMapping("KASA_CAPI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Çapı" },
                            new XmlItemMapping("KASA_KALINLIGI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Kalınlığı" },
                            new XmlItemMapping("KASA_KAPAK", string.Empty, XmlItemType.Attribute) { Key = "Kasa Kapak" },
                            new XmlItemMapping("KADRAN_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kadran Rengi" },
                            new XmlItemMapping("KADRAN_TASARIMI", string.Empty, XmlItemType.Attribute) { Key = "Kadran Tasarımı" },
                            new XmlItemMapping("CAM_CINSI", string.Empty, XmlItemType.Attribute) { Key = "Cam Cinsi" },
                            new XmlItemMapping("CAM_SEKLI", string.Empty, XmlItemType.Attribute) { Key = "Cam Şekli" },
                            new XmlItemMapping("KORDON_TIPI", string.Empty, XmlItemType.Attribute) { Key = "Kodron Tipi" },
                            new XmlItemMapping("KORDON_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Rengi" },
                            new XmlItemMapping("KORDON_KILIDI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Kilidi" },
                            new XmlItemMapping("KORDON_GENISLIGI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Genişliği" },
                            new XmlItemMapping("SU_GECIRMEZLIK", string.Empty, XmlItemType.Attribute) { Key = "Su Geçirmezlik" },
                            new XmlItemMapping("TAKVIM", string.Empty, XmlItemType.Attribute) { Key = "Takvim" },
                            new XmlItemMapping("KRONOMETRE", string.Empty, XmlItemType.Attribute) { Key = "Kronometre" },
                            new XmlItemMapping("SANIYE_DURDURMA", string.Empty, XmlItemType.Attribute) { Key = "Saniye Durdurma" },
                            new XmlItemMapping("DUAL_TIME", string.Empty, XmlItemType.Attribute) { Key = "Dual Time" },
                            new XmlItemMapping("POWER_RESERVE", string.Empty, XmlItemType.Attribute) { Key = "Power Reserve" },
                            new XmlItemMapping("AGIRLIK", string.Empty, XmlItemType.Attribute) { Key = "Ağırlık" }
                        }
                    }
                },
                new XmlSource
                {
                    RequestUri = "https://www.zamanatolyesi.com/feed/index/index/profile/zamomreg47301",
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "product",
                        XmlItemMappings = new List<XmlItemMapping> {
                            new XmlItemMapping("SKU", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("BARKOD", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("MARKA", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("URUN_ADI", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("STOK", "Stock", XmlItemType.Property),
                            new XmlItemMapping("PSF", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("SATIS_FIYATI", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("URUN_GORSEL1", "Photos", XmlItemType.Property),
                            new XmlItemMapping("URUN_GORSER_DIGER", string.Empty, XmlItemType.Property)
                            {
                                ChildXmlItemMappings = new() {
                                    new XmlItemMapping("image1", "Photos", XmlItemType.Property),
                                    new XmlItemMapping("image2", "Photos", XmlItemType.Property),
                                    new XmlItemMapping("image3", "Photos", XmlItemType.Property),
                                    new XmlItemMapping("image4", "Photos", XmlItemType.Property),
                                    new XmlItemMapping("image5", "Photos", XmlItemType.Property)
                                }
                            },
                            new XmlItemMapping("VARYANT_GRUP_KODU", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("CINSIYET", string.Empty, XmlItemType.Attribute) { Key = "Cinsiyet" },
                            new XmlItemMapping("CALISMA_SEKLI", string.Empty, XmlItemType.Attribute) { Key = "Çalışma Şekli" },
                            new XmlItemMapping("MAKINA_MODELI", string.Empty, XmlItemType.Attribute) { Key = "Makina Modeli" },
                            new XmlItemMapping("KASA_MATERIYALI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Materiyali" },
                            new XmlItemMapping("KASA_SEKLI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Şekli" },
                            new XmlItemMapping("KASA_CAP", string.Empty, XmlItemType.Attribute) { Key = "Kasa Çap" },
                            new XmlItemMapping("KASA_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kasa Rengi" },
                            new XmlItemMapping("KADRAN_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kadran Rengi" },
                            new XmlItemMapping("KADRAN_ISIGI", string.Empty, XmlItemType.Attribute) { Key = "Kadran Işığı" },
                            new XmlItemMapping("CAM_CINSI", string.Empty, XmlItemType.Attribute) { Key = "Cam Cinsi" },
                            new XmlItemMapping("CAM_SEKLI", string.Empty, XmlItemType.Attribute) { Key = "Cam Şekli" },
                            new XmlItemMapping("KORDON_TIPI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Tipi" },
                            new XmlItemMapping("KORDON_RENGI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Rengi" },
                            new XmlItemMapping("KORDON_KILIDI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Kilidi" },
                            new XmlItemMapping("KORDON_GENISLIGI", string.Empty, XmlItemType.Attribute) { Key = "Kordon Genişliği" },
                            new XmlItemMapping("SU_GECIRMEZLIK", string.Empty, XmlItemType.Attribute) { Key = "Su Geçirmezlik" },
                            new XmlItemMapping("KRONOMETRE", string.Empty, XmlItemType.Attribute) { Key = "Kronometre" },
                            new XmlItemMapping("TAKVIM", string.Empty, XmlItemType.Attribute) { Key = "Takvim" },
                            new XmlItemMapping("ALARM", string.Empty, XmlItemType.Attribute) { Key = "Alarm" },
                            new XmlItemMapping("FONKSIYONLAR", string.Empty, XmlItemType.Attribute) { Key = "Fonksiyonlar" },
                        }
                    }
                }
            };

            var parseds = new List<DataResponse<List<ParsedXmlItem>>>();
            Stopwatch stopwatch = Stopwatch.StartNew();
            Task[] tasks = new Task[xmlSources.Count];
            for (int i = 0; i < xmlSources.Count; i++)
            {
                tasks[i] = new TaskFactory().StartNew(xmlSource =>
                {
                    var theXmlSource = xmlSource as XmlSource;

                    var httpResultResponse = this._httpHelperV2.SendAsync(theXmlSource.RequestUri, HttpMethod.Get).Result;
                    if (httpResultResponse.HttpStatusCode == HttpStatusCode.OK && httpResultResponse.Success)
                    {
                        parseds.Add(this._xmlService.Parse(httpResultResponse.ContentString, theXmlSource.XmlMapping));
                    }
                }, xmlSources[i]);
            }
            await Task.WhenAll(tasks);
            stopwatch.Stop();


        }
        #endregion
    }
}
