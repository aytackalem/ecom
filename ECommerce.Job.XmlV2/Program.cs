﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.XmlV2
{
    internal class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("Ecommerce")), ServiceLifetime.Scoped);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ITenantFinder>(tf => null);

            serviceCollection.AddScoped<IDomainFinder>(df => null);

            serviceCollection.AddScoped<ICompanyFinder>(cf => null);

            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddScoped<IXmlService, XmlService>();

            serviceCollection.AddScoped<App>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);
            using (var scope = serviceProvider.CreateScope())
            {
                var processor = scope.ServiceProvider.GetRequiredService<App>();
                Stopwatch stopwatch = Stopwatch.StartNew();
                await processor.RunAsync(args);
                stopwatch.Stop();
            }
        }
        #endregion
    }
}