﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Packaging.Interfaces.Services;
using ECommerce.Infrastructure;
using ECommerce.MarketPlace.Collector.Base;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Operation;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Packaging.Services;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.IO;

namespace ECommerce.MarketPlace.Collector
{
    class Program
    {
        #region Methods
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(appConfig.Tenant.EcommerceConnectionStrings);
            });

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddSingleton<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddSingleton<IOrderFacade, OrderProxy>();

            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();

            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinderFinder>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ICacheHandler, CacheHandler>();

            serviceCollection.AddScoped<IProductInformationStockService, ProductInformationStockService>();

            serviceCollection.AddInfrastructureServices();
            serviceCollection.AddMemoryCache();
            return serviceCollection.BuildServiceProvider();
        }

        #endregion
    }
}
