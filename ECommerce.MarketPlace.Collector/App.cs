﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using ECommerce.MarketPlace.Collector.Base;
using ECommerce.MarketPlace.Common.Facade;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ECommerce.MarketPlace.Collector
{
    public class App : IApp
    {
        #region Fields
        private readonly IServiceProvider _serviceProvider;

        private readonly IOrderFacade _orderFacade;

        private readonly IOptions<AppConfig> _options;

        private readonly IShipmentProviderService _shipmentService;

        private readonly IProductInformationStockService _productInformationStockService;
        #endregion

        #region Constructors
        public App(IServiceProvider serviceProvider, IOrderFacade orderFacade, IOptions<AppConfig> options, IShipmentProviderService shipmentService, IProductInformationStockService productInformationStockService)
        {
            #region Fields
            this._serviceProvider = serviceProvider;
            this._shipmentService = shipmentService;
            this._orderFacade = orderFacade;
            this._options = options;
            this._productInformationStockService = productInformationStockService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            Console.WriteLine($"{this._options.Value.Tenant.Type} JOB RUN");

            List<MarketplaceCompany> marketplaces = null;
            using (var scope = this._serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                marketplaces = unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(x => x.Marketplace)
                    .Include(x => x.MarketplaceConfigurations)
                    .Include(x => x.ProductInformationMarketplaces)
                    .ThenInclude(x => x.ProductInformation.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                    .Where(x => x.Active)
                    .ToList();
            }

            foreach (var mpLoop in marketplaces)
            {
                if (mpLoop.MarketplaceId == "VO")
                {
                    continue;
                }

                var request = new Common.Request.OrderRequest
                {
                    Configurations = new Dictionary<string, string>(),
                    MarketPlaceId = mpLoop.Marketplace.Id
                };

                mpLoop.MarketplaceConfigurations.ForEach(x => request.Configurations.Add(x.Key, x.Value));

                var response = _orderFacade.Get(request);
                if (response.Success)
                {
                    foreach (var orderLoop in response.Data.Orders)
                    {
                        using (var scope = this._serviceProvider.CreateScope())
                        {
                            var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                            var duplicateOrder = unitOfWork
                                .OrderRepository
                                .DbSet()
                                .Any(x => x.MarketplaceId == mpLoop.Marketplace.Id && x.MarketplaceOrderNumber == orderLoop.OrderCode && (string.IsNullOrEmpty(orderLoop.OrderShipment.TrackingCode) || x.OrderShipments.Any(x => x.TrackingCode == orderLoop.OrderShipment.TrackingCode)));

                            if (duplicateOrder)
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{orderLoop.OrderCode}] sipariş sistemde bulundu.");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }

                            var deliveryFindNeighborhoodId = FindNeighborhoodId(unitOfWork, orderLoop.OrderDeliveryAddress.City, orderLoop.OrderDeliveryAddress.District, orderLoop.OrderDeliveryAddress.Neighborhood);
                            if (!deliveryFindNeighborhoodId.Success)
                            {
                                Console.BackgroundColor = ConsoleColor.DarkYellow;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{orderLoop.OrderCode}] sipariş sisteme aktarılmadı. {deliveryFindNeighborhoodId.Message}");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }

                            var invoiceFindNeighborhoodId = FindNeighborhoodId(unitOfWork, orderLoop.OrderInvoiceAddress.City, orderLoop.OrderInvoiceAddress.District, orderLoop.OrderInvoiceAddress.Neighborhood);
                            if (!invoiceFindNeighborhoodId.Success)
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{orderLoop.OrderCode}] sipariş sisteme aktarılmadı. {invoiceFindNeighborhoodId.Message}");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }

                            #region Product Create

                            var orderDetails = new List<Common.Response.Order.OrderDetail>();

                            var orderSkip = false;

                            foreach (var odLoop in orderLoop.OrderDetails)
                            {
                                var productInformationMarket = mpLoop.ProductInformationMarketplaces.FirstOrDefault(x => x.Barcode.ToLower() == odLoop.Product.Barcode.ToLower());

                                if (productInformationMarket == null)
                                {
                                    productInformationMarket = mpLoop.ProductInformationMarketplaces.FirstOrDefault(x => x.StockCode == odLoop.Product.StockCode);
                                }

                                if (productInformationMarket == null)
                                {
                                    Console.BackgroundColor = ConsoleColor.Red;
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine($"Sistem tarafından yeni ürün açma engellendi");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;

                                    orderSkip = true;

                                    break;
                                }
                                else
                                {
                                    odLoop.UnitCost = productInformationMarket
                                        .ProductInformation
                                        .ProductInformationPriceses
                                        .FirstOrDefault()
                                        .UnitCost;
                                    odLoop.Product.ProductInformationId = productInformationMarket.ProductInformationId;

                                    var averageCommission = mpLoop.MarketplaceConfigurations.FirstOrDefault(x => x.Key == "AverageCommission");

                                    var unitCommissionAmount = 0.10m;
                                    decimal.TryParse(averageCommission?.Value, out unitCommissionAmount);
                                    if (odLoop.UnitCommissionAmount == 0 && unitCommissionAmount > 0)
                                    {
                                        odLoop.UnitCommissionAmount = odLoop.UnitPrice * unitCommissionAmount;
                                    }
                                }

                                if (productInformationMarket?.ProductInformation?.Type == "PC")
                                {
                                    var productInformationCombines = unitOfWork
                                        .ProductInformationCombineRepository
                                        .DbSet()
                                        .Include(x => x.ProductInformation)
                                        .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                                        .Where(x => x.ProductInformationId == productInformationMarket.ProductInformationId)
                                        .ToList();

                                    var totalQuantity = productInformationCombines.Sum(x => x.Quantity);
                                    var ratioListPrice = odLoop.ListPrice / totalQuantity;
                                    var ratioUnitPrice = odLoop.UnitPrice / totalQuantity;
                                    var ratioUnitDiscount = odLoop.UnitDiscount / totalQuantity;
                                    var ratioUnitCommissionAmount = odLoop.UnitCommissionAmount / totalQuantity;

                                    foreach (var picLoop in productInformationCombines)
                                    {
                                        var productInformationPriceses = picLoop.ProductInformation.ProductInformationPriceses.FirstOrDefault();

                                        var orderDetail = new Common.Response.Order.OrderDetail
                                        {
                                            ListPrice = ratioListPrice,
                                            Quantity = picLoop.Quantity * odLoop.Quantity,
                                            UnitPrice = ratioUnitPrice,
                                            TaxRate = Convert.ToDouble(productInformationPriceses.VatRate),
                                            UnitCost = productInformationPriceses.UnitCost,
                                            Payor = false,
                                            UnitDiscount = ratioUnitDiscount,
                                            UnitCommissionAmount = ratioUnitCommissionAmount,
                                            Product = new Common.Response.Order.Product
                                            {
                                                ProductInformationId = picLoop.ContainProductInformationId
                                            }
                                        };

                                        orderDetails.Add(orderDetail);
                                    }
                                }
                                else
                                    orderDetails.Add(odLoop);
                            }

                            if (orderSkip)
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{orderLoop.OrderCode}] Sistem tarafından sipariş atlatıldı");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }

                            #endregion

                            var eCustomerAddress = new List<CustomerAddress>();
                            eCustomerAddress.Add(new CustomerAddress
                            {

                                CompanyId = 1,
                                NeighborhoodId = deliveryFindNeighborhoodId.Data,
                                Address = orderLoop.OrderDeliveryAddress.Address,
                                Recipient = $"{orderLoop.Customer.FirstName} {orderLoop.Customer.LastName}",
                                CreatedDate = DateTime.Now
                            });
                            var eCustomer = new Customer
                            {
                                CompanyId = 1,
                                Name = orderLoop.Customer.FirstName.ToUpperInvariant(),
                                Surname = orderLoop.Customer.LastName.ToUpperInvariant(),
                                CustomerContact = new CustomerContact
                                {

                                    CompanyId = 1,
                                    Phone = orderLoop.Customer.Phone.ToNumber(),
                                    Mail = orderLoop.Customer.Mail,
                                    TaxNumber = orderLoop.Customer.TaxNumber,
                                    TaxOffice = orderLoop.Customer.TaxAdministration,
                                    CreatedDate = DateTime.Now
                                },
                                CustomerAddresses = eCustomerAddress,
                                CreatedDate = DateTime.Now
                            };
                            unitOfWork.CustomerRepository.Create(eCustomer);
                            var customerId = eCustomer.Id;

                            var _shipmentCompany = unitOfWork.ShipmentCompanyRepository.DbSet().FirstOrDefault(x => x.Id == orderLoop.OrderShipment.ShipmentCompanyId);
                            if (_shipmentCompany == null)
                            {
                                unitOfWork.ShipmentCompanyRepository.Create(new ShipmentCompany
                                {
                                    Id = orderLoop.OrderShipment.ShipmentCompanyId,
                                    Name = orderLoop.OrderShipment.ShipmentCompanyId
                                });
                            }


                            var orderShipments = new List<OrderShipment>();
                            orderShipments.Add(new OrderShipment
                            {

                                CompanyId = 1,
                                TrackingUrl = orderLoop.OrderShipment.TrackingUrl,
                                TrackingCode = orderLoop.OrderShipment.TrackingCode,
                                TrackingBase64 = orderLoop.OrderShipment.TrackingBase64,
                                ShipmentCompanyId = _shipmentCompany.Id,
                                CreatedDate = DateTime.Now,
                                Payor = orderLoop.OrderShipment.Payor,
                                PackageNumber = orderLoop.OrderShipment.PackageNumber
                            });

                            var eOrder = new Order
                            {

                                CustomerId = customerId,
                                CurrencyId = "TL",
                                LanguageId = "TR",
                                OrderTypeId = orderLoop.OrderTypeId,
                                OrderSourceId = orderLoop.OrderSourceId,
                                OrderShipments = orderShipments,
                                ListTotal = orderLoop.ListTotalAmount,
                                Total = orderLoop.TotalAmount,
                                VatExcTotal = orderLoop.VatExcTotal,
                                VatExcDiscount = orderLoop.VatExcDiscount,
                                VatExcListTotal = orderLoop.VatExcListTotal,
                                Discount = orderLoop.Discount,
                                CreatedDate = DateTime.Now,
                                CargoFee = orderLoop.CargoFee,
                                SurchargeFee = orderLoop.SurchargeFee,
                                OrderDeliveryAddress = new OrderDeliveryAddress
                                {

                                    Recipient = $"{orderLoop.OrderDeliveryAddress.FirstName} {orderLoop.OrderDeliveryAddress.LastName}",
                                    CreatedDate = DateTime.Now,
                                    PhoneNumber = orderLoop.OrderDeliveryAddress.Phone.ToNumber(),
                                    Address = orderLoop.OrderDeliveryAddress.Address,
                                    NeighborhoodId = deliveryFindNeighborhoodId.Data
                                },
                                OrderInvoiceInformation = new OrderInvoiceInformation
                                {

                                    CreatedDate = DateTime.Now,
                                    NeighborhoodId = invoiceFindNeighborhoodId.Data,
                                    Address = orderLoop.OrderInvoiceAddress.Address,
                                    Phone = orderLoop.OrderInvoiceAddress.Phone.ToNumber(),
                                    TaxNumber = orderLoop.OrderInvoiceAddress.TaxNumber,
                                    TaxOffice = orderLoop.OrderInvoiceAddress.TaxOffice,
                                    FirstName = orderLoop.OrderInvoiceAddress.FirstName,
                                    LastName = orderLoop.OrderInvoiceAddress.LastName,
                                    Mail = orderLoop.OrderInvoiceAddress.Email
                                },
                                OrderTechnicInformation = new OrderTechnicInformation
                                {
                                    Browser = "",
                                    IpAddress = "",
                                    Platform = "",
                                },
                                OrderDetails = orderDetails.Select(x => new OrderDetail
                                {
                                    ListUnitPrice = x.ListPrice,
                                    UnitDiscount = x.UnitDiscount,
                                    ProductInformationId = x.Product.ProductInformationId,
                                    Quantity = x.Quantity,
                                    UnitCost = x.UnitCost,
                                    UnitPrice = x.UnitPrice,
                                    VatRate = Convert.ToDecimal(x.TaxRate),
                                    CreatedDate = DateTime.Now,
                                    Payor = x.Payor,
                                    UnitCargoFee = orderLoop.CargoFee > 0 ? orderLoop.CargoFee / orderDetails.Sum(od => od.Quantity) : 0,
                                    UnitCommissionAmount = x.UnitCommissionAmount,
                                    VatExcUnitCost = x.VatExcUnitCost,
                                    VatExcListUnitPrice = x.VatExcListUnitPrice,
                                    VatExcUnitDiscount = x.VatExcUnitDiscount,
                                    VatExcUnitPrice = x.VatExcUnitPrice

                                }).ToList(),
                                Payments = orderLoop.Payments.Select(x => new Payment
                                {
                                    PaymentTypeId = x.PaymentTypeId,
                                    Amount = orderLoop.TotalAmount,
                                    CreatedDate = DateTime.Now,
                                    DiscountCouponId = null,
                                    Paid = true
                                }).ToList(),
                                MarketplaceId = orderLoop.MarketPlaceId,
                                MarketplaceOrderNumber = orderLoop.OrderCode,
                                CommissionAmount = orderLoop.CommissionAmount,
                                OrderDate = orderLoop.OrderDate,
                                EstimatedPackingDate = DateTime.Now,
                                Application = null,
                                Currency = null,
                                Customer = null,
                                Language = null,
                                Marketplace = null
                            };

                            if (!string.IsNullOrEmpty(orderLoop.OrderNote))
                            {
                                eOrder.OrderNotes = new List<OrderNote>
                            {
                                new OrderNote
                                {
                                    CreatedDate=DateTime.Now,
                                    Note=orderLoop.OrderNote
                                }
                            };
                            }

                            var created = unitOfWork.OrderRepository.Create(eOrder);
                            if (created)
                            {
                                var pickingRequest = new Common.Request.OrderPickingRequest();
                                pickingRequest.Configurations = new Dictionary<string, string>();
                                mpLoop.MarketplaceConfigurations.ForEach(x =>
                                {
                                    pickingRequest.Configurations.Add(x.Key, x.Value);
                                });
                                pickingRequest.OrderPicking = orderLoop.OrderPicking;
                                pickingRequest.MarketPlaceId = mpLoop.Marketplace.Id;
                                pickingRequest.Status = Common.Request.Enums.MarketPlaceStatus.Picking;

                                if (mpLoop.Marketplace.Id == "PA")
                                {

                                    var postResponse = this._shipmentService.Post(eOrder.Id, true, new int[] { 1 }, 1);
                                    if (postResponse.Success)
                                    {

                                        eOrder.OrderTypeId = OrderTypes.OnaylanmisSiparis;
                                        unitOfWork
                                            .OrderRepository
                                            .Update(eOrder);
                                        pickingRequest.OrderPicking.TrackingNumber = postResponse.Data.Barcode;
                                    }
                                }

                                _orderFacade.OrderTypeUpdate(pickingRequest);


                                #region Update Stock
                                if (this._options.Value.Tenant.IsStock)
                                {
                                    eOrder.OrderDetails.ForEach(odLoop => this._productInformationStockService.DecreaseStock(odLoop.ProductInformationId, odLoop.Quantity));
                                }
                                #endregion

                            }

                            Console.BackgroundColor = ConsoleColor.Green;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{orderLoop.OrderCode}] sipariş numarası başarılı sisteme aktarıldı");
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                }
            }
        }
        #endregion

        #region Helper Methods
        private DataResponse<int> FindNeighborhoodId(IUnitOfWork unitOfWork, string cityName, string districtName, string neighborhoodName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Find Address
                cityName = cityName.Trim();


                if (cityName.ToLower() == "afyonkarahisar")
                    cityName = "Afyon";

                if (cityName.ReplaceChar().ToLower() == "adapazari")
                {
                    cityName = "sakarya";
                    districtName = "adapazari";
                }

                if (cityName.ReplaceChar().ToLower() == "icel")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }


                if (cityName.ReplaceChar().ToLower() == "Afyon")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }

                var city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == cityName.ReplaceChar());

                if (city == null)
                {
                    city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).FirstOrDefault();
                }

                if (districtName == "Eyüpsultan" && city.Id == 1)
                    districtName = "Eyüp";

                else if (districtName == "Kahramankazan" && city.Id == 8)
                    districtName = "Kazan";

                else if (districtName == "Merkez" && city.Id == 32)
                    districtName = "Palandöken";

                else if (districtName == "Merkez" && city.Id == 26)
                    districtName = "Merkezefendi";

                else if (districtName == "Merkez" && city.Id == 63)
                    districtName = "Altınordu";

                else if (districtName == "Merkez" && city.Id == 58)
                    districtName = "Yenişehir";

                else if (districtName == "Merkez" && city.Id == 42)
                    districtName = "Onikişubat";

                else if (districtName == "Mustafa k. paşa" && city.Id == 22)
                    districtName = "Mustafakemalpaşa";

                else if (districtName == "Marmara Ereğlisi" && city.Id == 73)
                    districtName = "Marmaraereğlisi";

                else if (districtName == "Beşikdözü" && city.Id == 75)
                    districtName = "Beşikdüzü";

                else if (districtName == "Merkez" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Lara Kundu" && city.Id == 9)
                    districtName = "Aksu";

                else if (districtName == "Merkez" && city.Id == 71)
                    districtName = "Karaköprü";

                else if (districtName == "Merkez" && city.Id == 73)
                    districtName = "Süleymanpaşa";

                else if (districtName == "Merkez" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 78)
                    districtName = "Tuşba";

                else if (districtName == "Merkez" && city.Id == 12)
                    districtName = "Efeler";

                else if (districtName == "Merkez" && city.Id == 56)
                    districtName = "Yunusemre";

                else if (districtName == "Merkez" && city.Id == 55)
                    districtName = "Battalgazi";

                else if (districtName == "Merkez" && city.Id == 33)
                    districtName = "Odunpazarı";

                else if (districtName == "Yenişehir" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 59)
                    districtName = "Menteşe";

                else if (districtName == "Merkez" && city.Id == 75)
                    districtName = "Ortahisar";

                else if (districtName == "Baraj Yolu" && city.Id == 2)
                    districtName = "Seyhan";

                else if (districtName == "Merkez" && city.Id == 34)
                    districtName = "Şahinbey";

                else if (districtName == "Merkez" && city.Id == 41)
                    districtName = "Konak";

                else if (districtName == "Ondokuz Mayıs" && city.Id == 67)
                    districtName = "19 Mayıs";

                else if (districtName == "Merkez" && city.Id == 67)
                    districtName = "İlkadım";

                else if (districtName == "Kuruçaşile" && city.Id == 14)
                    districtName = "Kurucaşile";

                else if (districtName == "İçel" && city.Id == 58)
                    districtName = "Silifke";

                else if (districtName == "Ofis" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Gördeş" && city.Id == 56)
                    districtName = "Gördes";

                else if (districtName == "Sultanhanı" && city.Id == 6)
                    districtName = "Merkez";

                else if (districtName == "Merkez" && city.Id == 22)
                    districtName = "Osmangazi";

                else if (districtName == "Merkez" && city.Id == 66)
                    districtName = "Adapazarı";

                var district = unitOfWork.DistrictRepository.DbSet().Where(x => x.CityId == city.Id).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == districtName.ReplaceChar());


                if (district == null)
                {
                    district = unitOfWork.DistrictRepository.DbSet().FirstOrDefault(x => x.CityId == city.Id);
                }

                var _neighborhoodName = neighborhoodName.ReplaceChar().Split(new string[] { "mah" }, StringSplitOptions.TrimEntries)[0];

                var neighborhood = unitOfWork.NeighborhoodRepository.DbSet()
                    .Where(x => x.DistrictId == district.Id)
                    .ToList()
                    .FirstOrDefault(x => x.Name.ReplaceChar() == $"{_neighborhoodName} mah");
                if (neighborhood == null)
                {
                    neighborhood = unitOfWork.NeighborhoodRepository.DbSet().FirstOrDefault(x => x.DistrictId == district.Id);
                }

                response.Data = neighborhood.Id;
                response.Success = true;
                #endregion

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }


        #endregion
    }
}
