﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Delivered.Base;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.MarketPlace.Delivered
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IOrderFacade _orderFacade;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IOrderFacade orderFacade)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._orderFacade = orderFacade;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var marketPlaces = this
                         ._unitOfWork
                         .MarketplaceCompanyRepository
                         .DbSet()
                         .Include(x => x.Marketplace)
                         .Include(x => x.MarketplaceConfigurations)
                         .Where(x => x.Active)
                         .ToList();

            foreach (var mpLoop in marketPlaces)
            {
                if (mpLoop.MarketplaceId != "TY" && mpLoop.MarketplaceId != "HB")
                    continue;

                var request = new Common.Request.OrderRequest
                {
                    MarketPlaceId = mpLoop.Marketplace.Id,
                    Configurations = mpLoop
                        .MarketplaceConfigurations
                        .ToDictionary(mc => mc.Key, mc => mc.Value)
                };

                if (mpLoop.MarketplaceConfigurations.Count > 0)
                {
                    var response = _orderFacade.GetDelivered(request);
                    if (response.Success)
                    {
                        foreach (var odLoop in response.Data.Orders)
                        {
                            var order = _unitOfWork.OrderRepository
                                .DbSet()
                                .FirstOrDefault(x =>
                                x.MarketplaceOrderNumber == odLoop.OrderCode
                                && x.MarketplaceId == mpLoop.Marketplace.Id
                                && (odLoop.OrderShipment == null
                                || x.OrderShipments.Any(x => x.TrackingCode == odLoop.OrderShipment.TrackingCode))
                                && x.OrderTypeId == "KTE");

                            Console.WriteLine(response.Data.Orders.IndexOf(odLoop));

                            if (order != null)
                            {
                                order.OrderTypeId = "TS";
                                _unitOfWork.OrderRepository.Update(order);

                                Console.BackgroundColor = ConsoleColor.Green;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{order.MarketplaceOrderNumber}] sipariş numarası teslim edildi.");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                            else
                            {

                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{odLoop.OrderCode}] sipariş sistemde bulunamadı.");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;

                            }
                        }

                    }
                }
            }


        }
        #endregion
    }
}
