﻿using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Wrappers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Delivered
{
    public class CacheHandler : ICacheHandler
    {
        public TEntity Handle<TEntity>(Func<TEntity> func, string cacheKey, int minutes = 10) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public List<TEntity> Handle<TEntity>(Func<List<TEntity>> func, string cacheKey, int minutes = 10) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public TResponse ResponseHandle<TResponse>(Func<TResponse> func, string cacheKey, int minutes = 10) where TResponse : ResponseBase, new()
        {
            throw new NotImplementedException();
        }
    }
}
