﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ECommerce.MarketPlace.GittiGidiyor.Common.Stock
{
	[XmlRoot(ElementName = "return")]
	public class Return
	{

		[XmlElement(ElementName = "ackCode")]
		public string AckCode { get; set; }

		[XmlElement(ElementName = "timeElapsed")]
		public string TimeElapsed { get; set; }

		[XmlElement(ElementName = "message")]
		public string message { get; set; }
	}
}
