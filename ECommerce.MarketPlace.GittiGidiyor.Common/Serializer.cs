﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ECommerce.MarketPlace.GittiGidiyor.Common
{
    public static class Serializer
    {

        public static long TimeSpan()
        {

            TimeSpan span = (TimeSpan)(DateTime.UtcNow - new DateTime(1970, 1, 1));
            return (span.Ticks / 0x2710L);

        }

        public static string Md5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static DataResponse<string> Post(string data, string address, string authorization)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
                webRequest.ContentType = "text/xml";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";
                webRequest.Headers.Add("Authorization", "Basic " + authorization);

                using (var stream = webRequest.GetRequestStream())
                {
                    using (var streamWriter = new StreamWriter(stream))
                    {
                        streamWriter.Write(data);
                    }
                }

                var result = string.Empty;

                using (var streamReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                response.Data = result;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }       


        public static T Deserialize<T>(string response)
        {

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);
            T t = default(T);
            var tString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlDocument.ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerXml;


            using (var stringReader = new StringReader(tString))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                t = (T)xmlSerializer.Deserialize(stringReader);
            }

            return t;
        }
    }
}
