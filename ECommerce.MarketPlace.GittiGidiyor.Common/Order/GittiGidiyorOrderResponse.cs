﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ECommerce.MarketPlace.GittiGidiyor.Common.Order
{
    [XmlRoot(ElementName = "return")]
    public class Return
    {
        [XmlElement(ElementName = "ackCode")]
        public string ackCode { get; set; }
        [XmlElement(ElementName = "responseTime")]
        public string responseTime { get; set; }
        [XmlElement(ElementName = "timeElapsed")]
        public string timeElapsed { get; set; }
        [XmlElement(ElementName = "saleCount")]
        public string saleCount { get; set; }
        [XmlElement(ElementName = "nextPageAvailable")]
        public bool nextPageAvailable { get; set; }

        [XmlElement(ElementName = "sales")]
        public sales Sales { get; set; }
    }
    [XmlRoot(ElementName = "sales")]
    public class sales
    {
        [XmlElement(ElementName = "sale")]
        public sale[] sale { get; set; }
    }

    [XmlRoot(ElementName = "sale")]
    public class sale
    {
        public string saleCode { get; set; }
        public string status { get; set; }
        public string statusCode { get; set; }
        public string productId { get; set; }
        public string productTitle { get; set; }
        public string price { get; set; }
        public string cargoPayment { get; set; }
        public string cargoCode { get; set; }
        public string amount { get; set; }
        public string endDate { get; set; }
        public BuyerInfo buyerInfo { get; set; }
        public string thumbImageLink { get; set; }
        public string lastActionDate { get; set; }
        public string variantId { get; set; }
        public string moneyDate { get; set; }
        public string itemId { get; set; }
        public ShippingInfo shippingInfo { get; set; }
    }
    [XmlRoot(ElementName = "buyerInfo")]
    public class BuyerInfo
    {
        [XmlElement(ElementName = "username")]
        public string username { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string phone { get; set; }
        public string mobilePhone { get; set; }
        public string address { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public NeighborhoodType neighborhoodType { get; set; }
        public string zipCode { get; set; }
    }

    [XmlRoot(ElementName = "neighborhoodType")]
    public class NeighborhoodType
    {
        public string neighborhoodId { get; set; }
        public string neighborhoodName { get; set; }
        public string districtId { get; set; }
        public string active { get; set; }
    }

    [XmlRoot(ElementName = "shippingInfo")]
    public class ShippingInfo
    {
        public string cargoCode { get; set; }
        public string shippingExpireDate { get; set; }
        public string shippingFirmName { get; set; }
    }

 


}
