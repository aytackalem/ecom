﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ECommerce.MarketPlace.GittiGidiyor.Common.Product
{
    public class GittiGidiyorProductResponse
    {
        [XmlRoot(ElementName = "return")]
        public class Return
        {
            [XmlElement(ElementName = "ackCode")]
            public string ackCode { get; set; }

            [XmlElement(ElementName = "responseTime")]
            public string responseTime { get; set; }

            [XmlElement(ElementName = "timeElapsed")]
            public string timeElapsed { get; set; }

            [XmlElement(ElementName = "productId")]
            public string productId { get; set; }

            [XmlElement(ElementName = "result")]
            public string result { get; set; }
        }
    }
}
