﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Terminal.DataTransferObjects;
using ECommerce.Application.Terminal.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Client.Terminal.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        #region Fields
        private readonly IOrderService _orderService;
        #endregion

        #region Constructors
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public DataResponse<Order> Get()
        {
            return _orderService.Get();
        }

        [HttpPost("Update")]
        public IActionResult Update(Order order)
        {

            return Ok(_orderService.Update(order));
        }

        [HttpPost("EstimatedPacking")]
        public IActionResult EstimatedPacking(Order order)
        {
            return Ok(_orderService.EstimatedPacking(order));
        }
        #endregion


    }
}
