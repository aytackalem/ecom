﻿using AutoMapper;
using Helpy.Shared.Http.Base;
using MarketplaceIntegration.Price.Core.Application.Parameters;
using MarketplaceIntegration.Price.Infrastructure.Marketplaces.Ciceksepeti;
using MarketplaceIntegration.Price.Infrastructure.Marketplaces.Ciceksepeti.Requests;
using MassTransit;
using System.Globalization;

namespace MarketplaceIntegration.Price.Consumers
{
    public class CiceksepetiConsumer : IConsumer<PriceRequest>
    {
        public const string _baseUrl = "https://apis.ciceksepeti.com/api";

        IHttpHelper _httpHelper;

        IMapper _mapper;

        public CiceksepetiConsumer(IHttpHelper httpHelper, IMapper mapper)
        {
            _httpHelper = httpHelper;
            _mapper = mapper;
        }

        public Task Consume(ConsumeContext<PriceRequest> context)
        {
            CiceksepetiConfiguration ciceksepetiConfiguration = context.Message.Header.Configurations;

            var culture = new CultureInfo("en-US");

            var priceStockRequest = this._mapper.Map<PriceStockRequest>(context.Message);

            return null;
        }
    }
}
