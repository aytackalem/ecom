using MarketplaceIntegration.Price.Consumers;
using MassTransit;

namespace MarketplaceIntegration.Price
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddMassTransit(mt =>
            {
                mt.AddConsumer<CiceksepetiConsumer>();
                mt.AddConsumer<HepsiburadaConsumer>();
                mt.AddConsumer<ModalogConsumer>();
                mt.AddConsumer<ModanisaConsumer>();
                mt.AddConsumer<MorhipoConsumer>();
                mt.AddConsumer<N11Consumer>();
                mt.AddConsumer<PazaramaConsumer>();
                mt.AddConsumer<PttAvmConsumer>();
                mt.AddConsumer<TrendyolConsumer>();

                mt.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(builder.Configuration["RabbitMqUrl"], "/", host =>
                    {
                        host.Username("guest");
                        host.Password("guest");
                    });

                    configurator.ReceiveEndpoint("CS-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<CiceksepetiConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("HB-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<HepsiburadaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("ML-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<ModalogConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("MN-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<ModanisaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("M-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<MorhipoConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("N11-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<N11Consumer>(context);
                    });

                    configurator.ReceiveEndpoint("PA-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<PazaramaConsumer>(context);
                    });

                    configurator.ReceiveEndpoint("PTT-price", configureEndpoint =>
                    {
                        configureEndpoint.ConfigureConsumer<PttAvmConsumer>(context);
                    });
                });
            });

            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}