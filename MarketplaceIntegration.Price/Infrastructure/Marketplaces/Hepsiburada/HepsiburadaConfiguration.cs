﻿using System.Text;

namespace MarketplaceIntegration.Price.Infrastructure.Marketplaces.Hepsiburada
{
    public class HepsiburadaConfiguration
    {
        #region Members
        public static implicit operator HepsiburadaConfiguration(Dictionary<string, string> configurations)
        {
            return new HepsiburadaConfiguration
            {
                Username = configurations["Username"],
                Password = configurations["Password"],
                MerchantId = configurations["MerchantId"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string MerchantId { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));
        #endregion
    }
}
