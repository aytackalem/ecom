﻿namespace MarketplaceIntegration.Price.Infrastructure.Marketplaces.Modalog
{
    public class ModalogConfiguration
    {
        #region Members
        public static implicit operator ModalogConfiguration(Dictionary<string, string> configurations)
        {
            return new ModalogConfiguration
            {
                Email = configurations["Email"],
                Password = configurations["Password"]
            };
        }
        #endregion

        #region Properties
        public string Email { get; set; }

        public string Password { get; set; }
        #endregion
    }
}
