﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Price.Infrastructure.Marketplaces.Ciceksepeti.Requests
{
    public class PriceStockRequest
    {
        public List<PriceStockRequestItem> Items { get; set; }
    }
}
