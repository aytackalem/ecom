﻿using Newtonsoft.Json;

namespace MarketplaceIntegration.Price.Infrastructure.Marketplaces.Ciceksepeti.Requests
{
    public class PriceStockRequestItem
    {
        public string StockCode { get; set; }

        public int? StockQuantity { get; set; }

        public double? ListPrice { get; set; }

        public double? SalesPrice { get; set; }
    }
}
