﻿using System.Text;

namespace MarketplaceIntegration.Price.Infrastructure.Marketplaces.Morhipo
{
    public class MorhipoConfiguration
    {
        #region Members
        public static implicit operator MorhipoConfiguration(Dictionary<string, string> configurations)
        {
            return new MorhipoConfiguration
            {
                Username = configurations["Username"],
                Password = configurations["Password"]
            };
        }
        #endregion

        #region Properties
        public string Username { get; set; }

        public string Password { get; set; }

        public string Authorization => Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.Username}:{this.Password}"));
        #endregion
    }
}
