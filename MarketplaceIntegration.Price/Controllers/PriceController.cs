using MarketplaceIntegration.Price.Core.Application.Parameters;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace MarketplaceIntegration.Price.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PriceController : ControllerBase
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;

        public PriceController(ISendEndpointProvider sendEndpointProvider)
        {
            this._sendEndpointProvider = sendEndpointProvider;
        }

        [HttpPost]
        public async Task<IActionResult> Post(PriceRequest priceRequest)
        {
            var sendEndpoint = await this._sendEndpointProvider.GetSendEndpoint(new Uri($"{priceRequest.Header.MarketplaceId}-price"));
            await sendEndpoint.Send<PriceRequest>(priceRequest);

            return Ok();
        }
    }
}