﻿using MarketplaceIntegration.Price.Core.Application.Parameters;

namespace MarketplaceIntegration.Price.Core.Application.Contracts
{
    public interface IMarketplace
    {
        Task SendAsync(PriceRequest priceRequest);
    }
}
