﻿namespace MarketplaceIntegration.Price.Core.Application.Parameters
{
    public sealed class PriceRequest
    {
        public PriceRequestHeader Header { get; set; }

        public List<PriceRequestItem> Items { get; set; }
    }
}
