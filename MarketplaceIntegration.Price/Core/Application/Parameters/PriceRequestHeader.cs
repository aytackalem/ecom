﻿namespace MarketplaceIntegration.Price.Core.Application.Parameters
{
    public sealed class PriceRequestHeader
    {
        public string MarketplaceId { get; set; }

        public Dictionary<string, string> Configurations { get; set; }
    }
}
