﻿namespace MarketplaceIntegration.Price.Core.Application.Parameters
{
    public sealed class PriceRequestItem
    {
        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
