﻿using ECommerce.Accounting.Common;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;

namespace ECommerce.Accounting.Base
{
    public interface IAccounting
    {
        #region Methods
        /// <summary>
        /// Gönderilen sipariş bilgilerini kullanarak fatura işlemini gerçekleştiren fonksiyon.
        /// </summary>
        /// <param name="order">İlgili sipariş.</param>
        /// <returns>Sonuç.</returns>
        DataResponse<AccountingInfo> Create(Order order);


        /// <summary>
        /// Gönderilen sipariş bilgilerini kullanarak fatura işlemini iptal fonksiyon.
        /// </summary>
        /// <param name="order">İlgili sipariş.</param>
        /// <returns>Sonuç.</returns>
        DataResponse<AccountingCancel> Cancel(CancelOrder cancelOrder);
        #endregion
    }
}
