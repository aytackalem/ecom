﻿using ECommerce.HBFetch.Models.Response.AttributeValue;
using System.Collections.Generic;

namespace ECommerce.HBFetch.Models.Response.Attribute
{
    public class BaseAttribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
    }

    public class Attribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
    }

    public class VariantAttribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
        public List<CategoryDatum> vals { get; internal set; }
    }

    public class AttributeData
    {
        public List<BaseAttribute> baseAttributes { get; set; }
        public List<Attribute> attributes { get; set; }
        public List<VariantAttribute> variantAttributes { get; set; }
    }

    public class Response
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public AttributeData data { get; set; }
    }


}
