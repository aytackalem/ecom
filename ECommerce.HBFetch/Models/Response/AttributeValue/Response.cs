﻿using System.Collections.Generic;

namespace ECommerce.HBFetch.Models.Response.AttributeValue
{
    public class Datum
    {
        public string value { get; set; }
    }

    public class Response
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public List<Datum> data { get; set; }
    }


}
