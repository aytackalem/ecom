﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Common.Facade;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.HBFetch
{
    class M
    {
        public string barcode { get; set; }

        public string packageNumber { get; set; }
    }

    public class App : IApp
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICryptoHelper _cryptoHelper;

        private readonly IHttpHelper _httpHelper;

        private readonly IProductFacade _productFacade;

        private readonly IOrderFacade _orderFacade;

        private readonly ICategoryFacade _categoryFacade;

        public App(IUnitOfWork unitOfWork, ICryptoHelper cryptoHelper, IHttpHelper httpHelper, IProductFacade productFacade, IOrderFacade orderFacade, ICategoryFacade categoryFacade)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cryptoHelper = cryptoHelper;
            this._httpHelper = httpHelper;
            this._productFacade = productFacade;
            this._orderFacade = orderFacade;
            this._categoryFacade = categoryFacade;
            #endregion
        }

        public string GetBasicAuth(string username, string password)
        {
            string txt = username + ":" + password;
            byte[] encodedBytes = System.Text.Encoding.UTF8.GetBytes(txt);
            return Convert.ToBase64String(encodedBytes);
        }

        public void Run()
        {
            //this._categoryFacade.Get(null);
            //this._orderFacade.Get(null);
            //this._productFacade.Create(null);

            //return;

            var beginDate = "2022-10-01 00:00";
            var endDate = "2022-10-01 23:59";
            var offset = 0;
            var url = $"https://oms-external.hepsiburada.com/packages/merchantid/fb5ff060-6b86-4f2f-9455-60922ffb994f?beginDate={beginDate}&endDate={endDate}&offset={0}";

            while (true)
            {
                Console.WriteLine(offset);
                var ms = this._httpHelper.Get<List<M>>(String.Format(url, offset), "Basic", GetBasicAuth("sinozkozmetik_dev", "aoUlyaxJDWnzca!"));
                offset += 10;

                if (ms == null || ms.Count == 0)
                    break;

                using (var conn = new SqlConnection("Server=192.168.19.156;Database=Ecommerce;UId=sa;Pwd=!X135790zxcYz!;"))
                {
                    conn.Open();

                    var text = @"Select	Count(0)
From	Orders As O
Join	OrderShipments As OS
On		O.Id = OS.OrderId
		And OS.TrackingCode = @TrackingCode
Where	O.DomainId = 4
		And O.MarketplaceId = 'HB'";

                    foreach (var mLoop in ms)
                    {
                        using (var cmd = new SqlCommand(text, conn))
                        {
                            cmd.Parameters.AddWithValue("@TrackingCode", mLoop.barcode);
                            if (Convert.ToInt32(cmd.ExecuteScalar()) == 0)
                            {
                                url = $"https://oms-external.hepsiburada.com/packages/merchantid/fb5ff060-6b86-4f2f-9455-60922ffb994f/packagenumber/{mLoop.packageNumber}/unpack";
                                this._httpHelper.Post(url, "Basic", GetBasicAuth("sinozkozmetik_dev", "aoUlyaxJDWnzca!"));

                                Console.WriteLine($"{ms.IndexOf(mLoop)} Unpack");
                            }
                            else
                            {
                                Console.WriteLine($"{ms.IndexOf(mLoop)} C");
                            }
                        }

                    }

                    conn.Close();
                }
                
            }








            return;

            var uid = Guid.NewGuid().ToString();

            var entities = this._unitOfWork.MarketplaceCompanyRepository.DbSet().Include(mc => mc.MarketplaceConfigurations).ToList();
            foreach (var eLoop in entities)
            {
                eLoop.MarketplaceConfigurations.Add(new Domain.Entities.MarketplaceConfiguration
                {
                    Key = "AverageCommission",
                    Value = "0.10",
                    MarketplaceCompanyId = eLoop.Id
                });
            }
            entities.ForEach(eLoop => this._unitOfWork.MarketplaceCompanyRepository.Update(eLoop));

            var arr = new List<object[]>
            {
                //new object[] { "Lale",      "Erkıran",    "lale.erkiran",       "!T33vf2Tp",    5 },
                //new object[] { "Sevil",      "Bektaş",    "sevil.bektas",       "!E34vd2Ok",    5 },
                //new object[] { "Dilara",      "Türkyılmaz",    "dilara.turkyilmaz",       "!W76th2tD",    5 },
                //new object[] { "Yusuf",     "Dağkuş",  "yusuf.dagkus",    "X3drRFr!",       3 },
                //new object[] { "Taner",      "Civelek",    "taner.civelek",       "!E34ef2Ux",    5 },
                //new object[] { "Yasin",     "Çörekci",  "yasin.corekci",    "927466",       3 },
                //new object[] { "Selim",     "Çörekci",  "selim.corekci",    "!ed4+23dY",    3 },
                //new object[] { "Mahmut",    "Saray",    "mahmut.saray",     "!158688m!",    3 },
                //new object[] { "Burak",     "Zengin",   "burak.zengin",     "!031903x!",    3 },
                //new object[] { "Emel",      "Demir",    "emel.demir",       "!Em629xsS",    3 },
                //new object[] { "Ebru",      "Bakanay",  "ebru.bakanay",     "!#4fAKdfn",    5 },
                //new object[] { "Murat",     "Yerli",    "murat.yerli",      "!3dnjDdn3",    3 },
                //new object[] { "Okay",      "Akça",     "okay.akca",        "#dq3Doeqw",    5 },
                //new object[] { "Erol",      "Yıldız",   "erol.yildiz",      "#4rReFre9",    4 },
                //new object[] { "Yakup",     "Çırpan",   "yakup.cirpan",     "-dfd3!dEG",    4 },
                //new object[] { "Semiha",    "Elkıran",  "semiha.elkiran",   "!d4-Sdwde",    5 },
                //new object[] { "Burçin",    "Pırlant",  "burcin.pirlant",   "x4564Fp3-",    5 },
                //new object[] { "Mahmut",    "Baş",      "mahmut.bas",       "xdw234eD",    2 },
                //new object[] { "Şaban",     "Baş",      "saban.bas",        "!d24Fwed",    2 },
                //new object[] { "Fırat",     "Yaşar",    "firat.yasar",      "!e53gTRd",    2 },
                //new object[] { "Veli",      "Kumtemir", "veli.kumtemir",    "!y43Yrep",    2 },
                //new object[] { "Sefa",      "Değerli",  "sefa.degerli",     "x!43Ywee",    2 },
                //new object[] { "Erhan",     "Köker",    "erhan.koker",      "X!71ySXS",    2 },
                //new object[] { "Ömer",      "Yıldız",   "omer.yildiz",      "x!30TqpS",    2 },
                //new object[] { "Mücahit",   "Baş",      "mucahit.bas",      "T!sy32Ek",    2 },
            };

            foreach (var item in arr)
                this._unitOfWork.ManagerRepository.Create(new Domain.Entities.Manager
                {
                    Name = item[0].ToString(),
                    Surname = item[1].ToString(),
                    TenantId = 1,
                    ManagerContact = new Domain.Entities.ManagerContact
                    {
                        Mail = "",
                        TenantId = 1,
                        Phone = ""
                    },
                    ManagerUser = new Domain.Entities.ManagerUser
                    {
                        Username = item[2].ToString(),
                        Password = item[3].ToString(),
                        TenantId = 1,
                        ManagerUserRoles = new List<Domain.Entities.ManagerUserRole>()
                    {
                        new Domain.Entities.ManagerUserRole
                        {
                            TenantId = 1,
                            ManagerRoleId = Convert.ToInt32(item[4])
                        }
                    },
                        ManagerUserDomains = new List<Domain.Entities.ManagerUserDomain>
                    {
                        new Domain.Entities.ManagerUserDomain
                        {
                            TenantId = 1,
                            DomainId = 1
                        },
                        new Domain.Entities.ManagerUserDomain
                        {
                            TenantId = 1,
                            DomainId = 3,
                            Default = true
                        }
                    }
                    }
                });







            //List<CategoryDatum> categories = this.GetAsync<CategoryRoot>("").Result.data;
            List<CategoryDatum> categories = new List<CategoryDatum>
            {
                //new CategoryDatum{ categoryId = 32010921, parentCategoryId = 32010889, name = "Güneş Krem ve Losyonları"},
                //new CategoryDatum{ categoryId = 24001692, parentCategoryId = 24001689, name = "Şampuanlar"},
                //new CategoryDatum{ categoryId = 32010894, parentCategoryId = 32010885, name = "Nemlendirici Kremler"},
                new CategoryDatum{ categoryId = 32010930, parentCategoryId = 32010925, name = "Vücut Losyon ve Kremleri"},
            };

            //categories.ForEach(c =>
            //{
            //    var marketplaceCategory = new Domain.Entities.MarketplaceCategory
            //    {
            //        Name = c.name,
            //        Code = c.categoryId.ToString(),
            //        ParentCode = c.parentCategoryId.ToString(),
            //        MarketPlaceId = "HB",
            //        MarketplaceCategoryVariants = new()
            //    };

            //    var a = this.GetAsync<Root>($"https://mpop.hepsiburada.com/product/api/categories/{c.categoryId}/attributes").Result;
            //    var variantAttributes = a.data.variantAttributes;
            //    variantAttributes.AddRange(a.data.attributes.Select(x => new VariantAttribute { id = x.id, mandatory = x.mandatory, multiValue = x.multiValue, name = x.name, type = x.type }));
            //    variantAttributes = variantAttributes.Where(x => x.mandatory).ToList();
            //    variantAttributes.ForEach(va =>
            //    {
            //        var b = this.GetAsync<ValueRoot>($"https://mpop.hepsiburada.com/product/api/categories/{c.categoryId}/attribute/{va.id}/values?page=0&size=1000&version=4").Result;
            //        var variantAttributes = b.data;

            //        marketplaceCategory.MarketplaceCategoryVariants.Add(new Domain.Entities.MarketPlaceCategoryVariant
            //        {
            //            Name = va.name,
            //            Code = va.id.ToString(),
            //            MarketplaceCategoryVariantValues = variantAttributes.Select(x => new Domain.Entities.MarketPlaceCategoryVariantValue
            //            {
            //                Value = x.value
            //            }).ToList()
            //        });
            //    });

            //    this
            //        ._unitOfWork.MarketplaceCategoryRepository
            //        .Create(marketplaceCategory);
            //});
        }

        async Task<T> GetAsync<T>(string url) where T : new()
        {
            T t = default;

            using (HttpClient httpClient = new())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "RmlkYW5pc3RhbmJ1bF9kZXY6RzFXSWRtNHdnMmdwIQ==");

                var httpResponseMessage = await httpClient.GetAsync(url);

                HttpContent content = httpResponseMessage.Content;
                string responseString = await content.ReadAsStringAsync();

                try
                {
                    t = JsonConvert.DeserializeObject<T>(responseString);
                }
                catch (Exception e)
                {
                }
            }

            return t;
        }
    }


    public class CategoryDatum
    {
        public int categoryId { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public int parentCategoryId { get; set; }
        public List<string> paths { get; set; }
        public bool leaf { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string sortId { get; set; }
        public object imageURL { get; set; }
        public bool available { get; set; }
        public List<object> productTypes { get; set; }
    }

    public class CategoryRoot
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public List<CategoryDatum> data { get; set; }
    }


    public class Attribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
    }

    public class BaseAttribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
    }

    public class Data
    {
        public List<BaseAttribute> baseAttributes { get; set; }
        public List<Attribute> attributes { get; set; }
        public List<VariantAttribute> variantAttributes { get; set; }
    }

    public class Root
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public Data data { get; set; }
    }

    public class VariantAttribute
    {
        public string name { get; set; }
        public string id { get; set; }
        public bool mandatory { get; set; }
        public string type { get; set; }
        public bool multiValue { get; set; }
    }


    public class ValueDatum
    {
        public string value { get; set; }
    }

    public class ValueRoot
    {
        public bool success { get; set; }
        public int code { get; set; }
        public int version { get; set; }
        public object message { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
        public int numberOfElements { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public List<ValueDatum> data { get; set; }
    }


}
