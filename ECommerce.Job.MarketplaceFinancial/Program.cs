﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.MarketplaceFinancial
{
    internal class Program
    {
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IConfiguration>(p => configuration);
            serviceCollection.AddSingleton<IDbService, DbService>();
            serviceCollection.AddSingleton<IDbNameFinder, DbNameFinder>();
            serviceCollection.AddSingleton<ITenantFinder, TenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, DomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, CompanyFinder>();

            serviceCollection.AddDbContext<ApplicationDbContext>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddInfrastructureServices(configuration);

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);

            var dbNames = new List<string>();

            using (var scope = serviceProvider.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = await dbService.GetNamesAsync();
                if (getNamesResponse.Success == false)
                {
                    SystemAlertHelper.SendMail("Error From Marketplace Financial", "Db names not fetched.");
                    return;
                }

                dbNames = getNamesResponse.Data;
            }

            if (dbNames.HasItem())
                foreach (var theDbName in dbNames)
                {
                    Console.Clear();
                    Console.WriteLine(theDbName);

#if DEBUG
                    if (theDbName != "Lafaba")
                        continue;
#endif

                    using (var scope = serviceProvider.CreateScope())
                    {
                        var unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();

                        var dbNameFinder = (DbNameFinder)serviceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(theDbName);

                        var tenants = unitOfWork.TenantRepository.DbSet().ToList();
                        foreach (var theTenant in tenants)
                        {
                            Console.WriteLine(theTenant.Name);

                            var tenantFinder = (TenantFinder)serviceProvider.GetRequiredService<ITenantFinder>();
                            tenantFinder.Set(theTenant.Id);

                            var domains = unitOfWork.DomainRepository.DbSet().ToList();
                            foreach (var theDomain in domains)
                            {
                                Console.WriteLine(theDomain.Name);

                                var domainFinder = (DomainFinder)serviceProvider.GetRequiredService<IDomainFinder>();
                                domainFinder.Set(theDomain.Id);

                                var companies = unitOfWork.CompanyRepository.DbSet().ToList();
                                foreach (var theCompany in companies)
                                {
                                    Console.WriteLine(theCompany.Name);

                                    var companyFinder = (CompanyFinder)serviceProvider.GetRequiredService<ICompanyFinder>();
                                    companyFinder.Set(theCompany.Id);

                                    var marketplaceCompany = unitOfWork
                                        .MarketplaceCompanyRepository
                                        .DbSet()
                                        .Include(mc => mc.MarketplaceConfigurations)
                                        .FirstOrDefault(mc => mc.MarketplaceId == "TY");

                                    if (marketplaceCompany != null)
                                    {
                                        var now = new DateTime(2024, 1, 20);//DateTime.Now.AddDays(-1);

                                        /* Cargo */
                                        //var cargoResolver = serviceProvider.GetRequiredService<MarketplaceV2CargoResolver>();
                                        //var cargo = cargoResolver("TY");
                                        //var cargoResponse = await cargo
                                        //    .GetCargoFees(
                                        //        new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                                        //        new DateTime(now.Year, now.Month, now.Day, 23, 59, 59),
                                        //        marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value));

                                        //foreach (var theContent in cargoResponse.Content)
                                        //{
                                        //    await Console.Out.WriteLineAsync($"{cargoResponse.Content.IndexOf(theContent)}/{cargoResponse.Content.Count}");

                                        //    InsertCargo(serviceProvider, theContent);
                                        //}

                                        /* Commission */
                                        var commissionResolver = serviceProvider.GetRequiredService<MarketplaceV2CommissionResolver>();
                                        var commission = commissionResolver("TY");
                                        //var commissionResponse = await commission
                                        //    .GetCommissions(
                                        //        new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                                        //        new DateTime(now.Year, now.Month, now.Day, 23, 59, 59),
                                        //        marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value));

                                        //foreach (var theCommission in commissionResponse.Content)
                                        //{
                                        //    await Console.Out.WriteLineAsync($"{commissionResponse.Content.IndexOf(theCommission)}/{commissionResponse.Content.Count}");

                                        //    InsertCommission(serviceProvider, theCommission);
                                        //}

                                        now = now.AddDays(-45);

                                        /* Payment */
                                        var commissionResponse = await commission
                                            .GetCommissions(
                                                new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                                                new DateTime(now.Year, now.Month, now.Day, 23, 59, 59),
                                                marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value));

                                        foreach (var theCommission in commissionResponse.Content)
                                        {
                                            await Console.Out.WriteLineAsync($"{commissionResponse.Content.IndexOf(theCommission)}/{commissionResponse.Content.Count}");

                                            InsertPayment(serviceProvider, theCommission);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        }

        static void InsertCommission(IServiceProvider serviceProvider, ECommerce.Application.Common.Wrappers.MarketplacesV2.Commissions.Commission commission)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                /* 1. Find order. */
                var order = unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationMarketplaces)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.MarketplaceCommissions)
                    .FirstOrDefault(o => o.MarketplaceOrderNumber == commission.OrderNumber);

                if (order != null)
                {
                    /* 2. Find order detail. */
                    var orderDetail = order
                        .OrderDetails
                        .FirstOrDefault(od => od
                            .ProductInformation
                            .ProductInformationMarketplaces
                            .Any(pim => pim.MarketplaceId == "TY" && pim.Barcode == commission.Barcode));

                    if (orderDetail != null)
                    {
                        orderDetail.MarketplaceCommissions ??= new();
                        orderDetail.MarketplaceCommissions.Add(new Domain.Entities.MarketplaceCommission
                        {
                            Amount = commission.CommissionAmount,
                            Rate = (decimal)commission.CommissionRate,
                            Type = commission.Type
                        });

                        /* 3. Set commission. */
                        orderDetail.UnitCommissionAmount = orderDetail
                            .MarketplaceCommissions
                            .Sum(mc => mc.Type == "İade" ? mc.Amount * -1M : mc.Amount);

                        if (orderDetail.UnitCommissionAmount > 0 && orderDetail.Quantity > 1)
                            orderDetail.UnitCommissionAmount /= orderDetail.Quantity;

                        order.CommissionAmount = order
                            .OrderDetails
                            .Sum(od => od.MarketplaceCommissions.Sum(mc => mc.Type == "İade" ? mc.Amount * -1M : mc.Amount));

                        /* 3. Update order. */
                        unitOfWork.OrderRepository.Update(order);
                    }
                }
            }
        }

        static void InsertCargo(IServiceProvider serviceProvider, ECommerce.Application.Common.Wrappers.MarketplacesV2.Shipments.Cargo cargo)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                /* 1. Find order. */
                var order = unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.MarketplaceCargoFees)
                    .FirstOrDefault(o => o.MarketplaceOrderNumber == cargo.OrderNumber);

                if (order != null)
                {
                    /* 2. Set cargo fee. */
                    order.MarketplaceCargoFees ??= new();
                    order.MarketplaceCargoFees.Add(new Domain.Entities.MarketplaceCargoFee
                    {
                        Amount = cargo.Amount,
                        Desi = cargo.Desi,
                        Type = cargo.Type
                    });

                    /* 3. Set cargo fee. */
                    order.CargoFee = order.MarketplaceCargoFees.Sum(mcf => mcf.Amount);

                    /* 4. Update order. */
                    unitOfWork.OrderRepository.Update(order);
                }
            }
        }

        static void InsertPayment(IServiceProvider serviceProvider, ECommerce.Application.Common.Wrappers.MarketplacesV2.Commissions.Commission commission)
        {
            if (commission.PaymentOrderId.HasValue == false)
                return;

            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                /* 1. Find order. */
                var order = unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.MarketplaceInvoice)
                    .FirstOrDefault(o => o.MarketplaceOrderNumber == commission.OrderNumber);

                if (order != null && order.MarketplaceInvoice == null)
                {
                    order.MarketplaceInvoice = new Domain.Entities.MarketplaceInvoice
                    {
                        Number = commission.PaymentOrderId.Value.ToString()
                    };

                    /* 3. Update order. */
                    unitOfWork.OrderRepository.Update(order);

                    Console.WriteLine("UPDATED");
                }
            }
        }
    }
}