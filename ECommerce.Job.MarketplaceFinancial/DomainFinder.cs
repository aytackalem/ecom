﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.MarketplaceFinancial
{
    public class DomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Constructors
        public DomainFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _domainId;
        }

        public void Set(int domainId)
        {
            _domainId = domainId;
        }
        #endregion
    }
}
