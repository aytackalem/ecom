﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.GittiGidiyor.Common;
using ECommerce.MarketPlace.Common.Facade;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ECommerce.MarketPlace.GittiGidiyor.Common.Order;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.Application.Common.Constants;

namespace ECommerce.MarketPlace.GittiGidiyor.Operation
{
    public class OrderOperation : IOrderFacade
    {

        #region Methods
        [Obsolete]
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                var ApiKey = orderRequest.Configurations["ApiKey"];
                var SecretKey = orderRequest.Configurations["SecretKey"];
                var RoleName = orderRequest.Configurations["Username"];
                var RolePass = orderRequest.Configurations["Password"];
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));

                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());

                //byStatus: String 
                //Durum parametresi(filtresi)
                //S: Kargo Yapılacaklar
                //C: Onay Bekleyenler
                //P: Para transferleri
                //O: Tamamlananlar
                //V: Aktif Satışlar
                //I: İptali beklenenler
                //R: İptal olanlar / İade konumunda olanlar

                int page = 1;
                bool nextPageAvailable = true;
                while (nextPageAvailable)
                {
                    var request = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sale=""http://sale.individual.ws.listingapi.gg.com"" >
   <soapenv:Header/>
   <soapenv:Body>
      <sale:getSales>
         <apiKey>{ApiKey}</apiKey>
         <sign>{sign}</sign>
         <time>{timelong}</time>
         <withData>true</withData>
         <byStatus>S</byStatus>
         <byUser>A</byUser>
         <orderBy>C</orderBy>
         <orderType>D</orderType>
         <pageNumber>{page}</pageNumber>
         <pageSize>50</pageSize>
         <lang>tr</lang>
      </sale:getSales>
   </soapenv:Body>
</soapenv:Envelope>";


                    var detailResponse = Serializer.Post(request, $"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualSaleService?wsdl", authorization);

                    if (detailResponse.Success)
                    {
                        var getSalesResponse = Serializer.Deserialize<Return>(detailResponse.Data);
                        if (getSalesResponse.Sales != null)
                        {
                            foreach (var sale in getSalesResponse.Sales.sale)
                            {


                                string shipmentId = null;
                                switch (sale.shippingInfo.shippingFirmName)
                                {
                                    case "aras":
                                        shipmentId = ShipmentCompanies.ArasKargo;
                                        break;
                                    case "yurtiçi":
                                        shipmentId = ShipmentCompanies.YurtiçiKargo;
                                        break;
                                    case "ptt":
                                        shipmentId = ShipmentCompanies.PttKargo;
                                        break;
                                    case "mng":
                                        shipmentId = ShipmentCompanies.MngKargo;
                                        break;
                                    case "sürat":
                                        shipmentId = ShipmentCompanies.SüratKargo;
                                        break;
                                    case "ups":
                                        shipmentId = ShipmentCompanies.Ups;
                                        break;
                                    default:
                                        shipmentId = ShipmentCompanies.ArasKargo;
                                        break;

                                }

                                var order = new Order
                                {
                                    OrderCode = sale.saleCode,
                                    Source = "GG",
                                    CurrencyId = "TRY",
                                    OrderTypeId = "OS",
                                    TotalAmount = Convert.ToDecimal(sale.price),
                                    Shipper = sale.cargoPayment,
                                    ShippingCost = 0,
                                    OrderSourceId = OrderSources.Pazaryeri,
                                    MarketPlaceId = Marketplaces.GittiGidiyor,
                                    Barcode = sale.productId,
                                    SurchargeAmount = 0,
                                    Customer = new Customer
                                    {
                                        FirstName = sale.buyerInfo.name,
                                        Phone = sale.buyerInfo.mobilePhone,
                                        LastName = sale.buyerInfo.surname,
                                        TaxNumber = "",
                                        SourceReferenceId = "",
                                        Mail = "",
                                        TaxAdministration = "",
                                        IdentityNumber = ""
                                    },
                                    OrderDetails = new List<OrderDetail>(),
                                    Payments = new List<Payment>(),
                                    OrderDeliveryAddress = new OrderDeliveryAddress
                                    {
                                        Address = sale.buyerInfo.address,
                                        Code = "",
                                        Email = "",
                                        FirstName = sale.buyerInfo.name,
                                        Phone = sale.buyerInfo.mobilePhone,
                                        LastName = sale.buyerInfo.surname,
                                        Country = "",
                                        City = sale.buyerInfo.city,
                                        District = sale.buyerInfo.district
                                    },
                                    OrderInvoiceAddress = new OrderInvoiceAddress
                                    {
                                        Address = sale.buyerInfo.address,
                                        City = sale.buyerInfo.city,
                                        Country = "",
                                        District = sale.buyerInfo.district,
                                        Email = "",
                                        FirstName = sale.buyerInfo.name,
                                        LastName = sale.buyerInfo.surname,
                                        Phone = sale.buyerInfo.mobilePhone,
                                        TaxNumber = "",
                                        TaxOffice = ""
                                    },
                                    OrderDate = DateTime.Now,
                                    OrderShipment = new OrderShipment
                                    {
                                        ShipmentCompanyId = shipmentId,
                                        ShipmentTypeId = "3",
                                        TrackingCode = sale.shippingInfo.cargoCode,
                                        TrackingUrl = ""
                                    }
                                };
                                order.OrderDetails.Add(new OrderDetail
                                {
                                    Quantity = Convert.ToInt32(sale.amount),
                                    UnitPrice = Convert.ToDecimal(sale.price) / Convert.ToInt32(sale.amount),
                                    ListPrice = Convert.ToDecimal(sale.price) / Convert.ToInt32(sale.amount),
                                    TaxRate = 0.18,
                                    Product = new Product
                                    {
                                        Name = sale.productTitle,
                                        StockCode = sale.productId,
                                        Barcode = ""
                                    }
                                });
                                order.Payments.Add(new Payment
                                {

                                    Amount = Convert.ToDecimal(sale.price),
                                    CurrencyId = "TRY",
                                    PaymentTypeId = "OO",
                                    Date = DateTime.Now
                                });
                                response.Data.Orders.Add(order);
                            }
                        }

                        nextPageAvailable = getSalesResponse.nextPageAvailable;
                        page++;
                    }
                    else
                        nextPageAvailable = false;
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });

        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                var ApiKey = orderRequest.Configurations["ApiKey"];
                var SecretKey = orderRequest.Configurations["SecretKey"];
                var RoleName = orderRequest.Configurations["Username"];
                var RolePass = orderRequest.Configurations["Password"];
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));

                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());

                var request = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sale=""http://sale.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <sale:getSale>
         <apiKey>{ApiKey}</apiKey>
         <sign>{sign}</sign>
         <time>{timelong}</time>
         <saleCode>{orderRequest.MarketPlaceOrderCode}</saleCode>
         <lang>tr</lang>
      </sale:getSale>
   </soapenv:Body>
</soapenv:Envelope>";

                var detailResponse = Serializer.Post(request, $"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualSaleService?wsdl", authorization);
                if (detailResponse.Success)
                {
                    var getSalesResponse = Serializer.Deserialize<Return>(detailResponse.Data);
                    if (getSalesResponse.Sales != null && (getSalesResponse.Sales.sale[0].statusCode == "STATUS_GIVEN_BACK_PAYMENT" || getSalesResponse.Sales.sale[0].statusCode.Contains("CANCEL")))
                    {
                        response.Data.IsCancel = true;
                        response.Message = $"Siparişiniz iptal statüsündedir. Gittigidiyor panelinden siparişinizi kontrol ediniz.";

                    }
                }
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });

        }


        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}
