﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.GittiGidiyor.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ECommerce.MarketPlace.GittiGidiyor.Common.Product.GittiGidiyorProductResponse;

namespace ECommerce.MarketPlace.GittiGidiyor.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields 


        #endregion

        #region Constructors
        public ProductOperation()
        {
        }

        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var ApiKey = productRequest.Configurations["ApiKey"];
                var SecretKey = productRequest.Configurations["SecretKey"];
                var RoleName = productRequest.Configurations["Username"];
                var RolePass = productRequest.Configurations["Password"];
                var CargoCompany = productRequest.Configurations["CargoCompany"];
                var City = productRequest.Configurations["City"];
                var ShippingPayment = productRequest.Configurations["ShippingPayment"];
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));
                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());

                foreach (var stLoop in productRequest.ProductItem.StockItems)
                {
                    var products = string.Empty;
                    var photos = string.Empty;
                    var variantGroups = string.Empty;
                    var variantPhotos = string.Empty;
                    var filter = Filter(new ProductFilterRequest
                    {
                        Stockcode = stLoop.StockCode,
                        Configurations = productRequest.Configurations
                    });

                    if (filter.Success)
                    {

                        if (filter.Data.AckCode == "success") // Ürün güncelleme yapılır
                        {

                        }
                        else if (stLoop.Active) //Ürün yükleme yapılır Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                        {
                            var specGroups = string.Empty;
                            var variantSpecs = string.Empty;
                            foreach (var atLoop in stLoop.Attributes)
                            {
                                variantSpecs += $@"<variantSpec nameId="""" name=""{atLoop.AttributeName}"" valueId="""" value=""{atLoop.AttributeValueName}"" orderNumber="""" specDataOrderNumber="" />";

                                specGroups += $@"<spec name=""{atLoop.AttributeName}"" value=""{atLoop.AttributeValueName}"" type=""Combo"" required=""false""/>";
                            }
                            variantGroups += $@"            
               <!--Zero or more repetitions:-->
               <variantGroup nameId="""" valueId="""" alias="""">
                  <!--Optional:-->
                  <variants>
                    <!--Zero or more repetitions:-->
                     <variant variantId="""" operation="""">
                        <!--Optional:-->
                     <variantSpecs>
                       {variantSpecs}
                    </variantSpecs>
                        <!--Optional:-->
                        <quantity>{stLoop.Quantity}</quantity>
                        <!--Optional:-->
                        <stockCode>{stLoop.StockCode}</stockCode>
                        <!--Optional:-->
                        <soldCount>0</soldCount>
                        <!--Optional:-->
                        <newCatalogId>0</newCatalogId>
                     </variant>
                  </variants>
                  <!--Optional:-->
                  <photos>
                     <!--Zero or more repetitions:-->
                    {variantPhotos}
                  </photos>
               </variantGroup>";
                            variantGroups = @$"<variantGroups>
                            {variantGroups}
                            </variantGroups>";
                            variantGroups = string.Empty;

                            foreach (var imLoop in stLoop.Images)
                            {
                                variantPhotos += @$"<photo photoId=""{imLoop.Order}"">
                                        <!--Optional:-->
                                        <url>{imLoop.Url}</url>
                                        <!--Optional:-->
                                        <base64></base64>
                                     </photo>";

                                photos += variantPhotos;
                            }

                            string data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
   <soapenv:Header/>
   <soapenv:Body>
      <prod:insertAndActivateProduct>
         <apiKey>{ApiKey}</apiKey>
         <sign>{sign}</sign>
         <time>{timelong}</time>
         <itemId>{productRequest.ProductItem.SellerCode}</itemId>            
           <product>
            <!--Optional:-->
            <categoryCode>{productRequest.ProductItem.CategoryId}</categoryCode>
            <!--Optional:-->
            <storeCategoryId>0</storeCategoryId>
            <!--Optional:-->
            <title>{stLoop.Title}</title>
            <!--Optional:-->
            <subtitle>{stLoop.Subtitle}</subtitle>
            <!--Optional:-->
            <specs>
               <!--Zero or more repetitions:-->
               {specGroups}
            </specs>
            <!--Optional:-->
            <photos>
               <!--Zero or more repetitions:-->
                {photos}
            </photos>
            <!--Optional:-->
            <pageTemplate>4</pageTemplate>
            <!--Optional:-->
            <description><![CDATA[<body>{stLoop.Description}</body>]]></description>
            <!--Optional:-->
            <startDate></startDate>
            <!--Optional:-->
            <catalogId>0</catalogId>
            <!--Optional:-->
            <newCatalogId>0</newCatalogId>
            <!--Optional:-->
            <catalogDetail>0</catalogDetail>
            <!--Optional:-->
            <catalogFilter></catalogFilter>
            <!--Optional:-->
            <format>S</format>
            <!--Optional:-->
            <startPrice></startPrice>
            <!--Optional:-->
            <buyNowPrice>{stLoop.ListPrice}</buyNowPrice>
            <!--Optional:-->
            <netEarning></netEarning>
            <!--Optional:-->
            <listingDays>360</listingDays>
            <!--Optional:-->
            <productCount>50</productCount>
            <!--Optional:-->
            <cargoDetail>
               <!--Optional:-->
               <city>{City}</city>
               <!--Optional:-->
               <cargoCompanies>
                  <!--Zero or more repetitions:-->
                  <cargoCompany>{CargoCompany}</cargoCompany>
               </cargoCompanies>
               <!--Optional:-->
               <shippingPayment>{ShippingPayment}</shippingPayment>
               <!--Optional:-->
               <cargoDescription></cargoDescription>
               <!--Optional:-->
               <shippingWhere>country</shippingWhere>
               <!--Optional:-->
               <shippingFeePaymentType></shippingFeePaymentType>
               <!--Optional:-->
               <cargoCompanyDetails>
                  <!--Zero or more repetitions:-->
                  <cargoCompanyDetail>
                     <!--Optional:-->
                     <name>{CargoCompany}</name>
                     <!--Optional:-->
                     <value></value>
                     <!--Optional:-->
                     <cityPrice>0.0</cityPrice>
                     <!--Optional:-->
                     <countryPrice>9.0</countryPrice>
                  </cargoCompanyDetail>
               </cargoCompanyDetails>
               <!--Optional:-->
               <shippingTime>
                  <!--Optional:-->
                  <days>today</days>
                  <!--Optional:-->
                  <beforeTime>13:00</beforeTime>
               </shippingTime>
               <!--Optional:-->
               <productPackageSize>
                  <!--Optional:-->
                  <width>0</width>
                  <!--Optional:-->
                  <height>0</height>
                  <!--Optional:-->
                  <depth>0</depth>
                  <!--Optional:-->
                  <weight>0</weight>
                  <!--Optional:-->
                  <desi>0</desi>
               </productPackageSize>
            </cargoDetail>
            <!--Optional:-->
            <affiliateOption>false</affiliateOption>
            <!--Optional:-->
            <boldOption>false</boldOption>
            <!--Optional:-->
            <catalogOption>false</catalogOption>
            <!--Optional:-->
            <vitrineOption>false</vitrineOption>
            <!--Optional:-->
                {variantGroups}
            <!--Optional:-->
            <auctionProfilePercentage></auctionProfilePercentage>
            <!--Optional:-->
            <marketPrice>{stLoop.ListPrice}</marketPrice>
            <!--Optional:-->
            <globalTradeItemNo>{stLoop.StockCode}</globalTradeItemNo>
            <!--Optional:-->
            <manufacturerPartNo></manufacturerPartNo>
            <!--Optional:-->
            <sameDayDeliveryTypes>
               <!--Zero or more repetitions:-->
               <sameDayDeliveryType>
                  <!--Optional:-->
                  <lastReceivingTime></lastReceivingTime>
                  <!--Optional:-->
                  <shippingFirmId></shippingFirmId>
                  <!--Optional:-->
                  <deliveryOption></deliveryOption>
               </sameDayDeliveryType>
            </sameDayDeliveryTypes>
         </product>
         <forceToSpecEntry>false</forceToSpecEntry>
         <nextDateOption>false</nextDateOption>
         <lang>tr</lang>
      </prod:insertAndActivateProduct>
   </soapenv:Body>
</soapenv:Envelope>";
                            var productResponse = Serializer.Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl", authorization);

                            if (productResponse.Success)
                            {
                                var responseReturn = Serializer.Deserialize<Return>(productResponse.Data);

                            }
                        }


                    }
                }


                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }



        public Response UpdateStock(StockRequest priceAndStock)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var ApiKey = priceAndStock.Configurations["ApiKey"];
                var SecretKey = priceAndStock.Configurations["SecretKey"];
                var RoleName = priceAndStock.Configurations["Username"];
                var RolePass = priceAndStock.Configurations["Password"];
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));
                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());


                var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
                                    <soapenv:Header/>
                                       <soapenv:Body>
                                          <prod:updateProductSales>
                                         <apiKey>{ApiKey}</apiKey>
                                             <sign>{sign}</sign>
                                             <time>{timelong}</time>
                                             <productUpdateSales>
                                                <!--Zero or more repetitions:-->
                                                <item>
                                                   <!--Optional:-->
                                                   <productId>{priceAndStock.PriceStock.StockCode}</productId>
                                                   <!--Optional:-->
                                                   <price></price>
                                                   <!--Optional:-->
                                                   <quantity>{priceAndStock.PriceStock.Quantity}</quantity>
                                                   <!--Optional:-->
                                                   <updatedSaleFields>
                                                      <!--Zero or more repetitions:-->                                              
                                                <field>quantity</field>
                                                   </updatedSaleFields>
                                                </item>
                                             </productUpdateSales>
                                             <lang>tr</lang>
                                          </prod:updateProductSales>
                                       </soapenv:Body>
                                    </soapenv:Envelope>";

                var productResponse = Serializer.Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl", authorization);

                if (productResponse.Success)
                {
                    var responseReturn = Serializer.Deserialize<Common.Stock.Return>(productResponse.Data);
                    response.Success = responseReturn.AckCode == "success";
                }
                response.Success = false;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }



        public Response UpdatePrice(StockRequest priceAndStock)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var ApiKey = priceAndStock.Configurations["ApiKey"];
                var SecretKey = priceAndStock.Configurations["SecretKey"];
                var RoleName = priceAndStock.Configurations["Username"];
                var RolePass = priceAndStock.Configurations["Password"];
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));
                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());


                var fieldPrice = priceAndStock.PriceStock.SalePrice.HasValue ? "<field>price</field>" : "";
                var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
                                    <soapenv:Header/>
                                       <soapenv:Body>
                                          <prod:updateProductSales>
                                         <apiKey>{ApiKey}</apiKey>
                                             <sign>{sign}</sign>
                                             <time>{timelong}</time>
                                             <productUpdateSales>
                                                <!--Zero or more repetitions:-->
                                                <item>
                                                   <!--Optional:-->
                                                   <productId>{priceAndStock.PriceStock.StockCode}</productId>
                                                   <!--Optional:-->
                                                   <price>{priceAndStock.PriceStock.SalePrice}</price>
                                                   <!--Optional:-->
                                                   <quantity>{priceAndStock.PriceStock.Quantity}</quantity>
                                                   <!--Optional:-->
                                                   <updatedSaleFields>
                                                      <!--Zero or more repetitions:-->
                                               {fieldPrice}
                                                <field>quantity</field>
                                                   </updatedSaleFields>
                                                </item>
                                             </productUpdateSales>
                                             <lang>tr</lang>
                                          </prod:updateProductSales>
                                       </soapenv:Body>
                                    </soapenv:Envelope>";

                var productResponse = Serializer.Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl", authorization);

                if (productResponse.Success)
                {
                    var responseReturn = Serializer.Deserialize<Common.Stock.Return>(productResponse.Data);
                    response.Success = responseReturn.AckCode == "success";
                }



                response.Success = false;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        private DataResponse<Common.ProductFilter.Return> Filter(ProductFilterRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Common.ProductFilter.Return>>((response) =>
            {
                var ApiKey = productRequest.Configurations["ApiKey"];
                var SecretKey = productRequest.Configurations["SecretKey"];
                var RoleName = productRequest.Configurations["Username"];
                var RolePass = productRequest.Configurations["Password"];
                long timelong = Serializer.TimeSpan();
                string sign = Serializer.Md5Hash(ApiKey + SecretKey + timelong.ToString());

                var data = @$"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:prod=""https://product.individual.ws.listingapi.gg.com"">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <prod:getProduct>
                                     <apiKey>{ApiKey}</apiKey>
                                     <sign>{sign}</sign>
                                     <time>{timelong}</time>
                                     <itemId></itemId> 
                                     <productId>{productRequest.Stockcode}</productId>        
                                     <lang>tr</lang>
                                  </prod:getProduct>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{RoleName}:{RolePass}"));

                var productResponse = Serializer.Post(data, "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl", authorization);
                if (productResponse.Success)
                {
                    response.Data = Serializer.Deserialize<Common.ProductFilter.Return>(productResponse.Data);

                }

                response.Success = true;
            });
        }
        #endregion
    }
}
