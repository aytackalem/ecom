﻿using AutoMapper;

namespace Helpy.Services.Catalog.Persistence
{
    public class PersistenceMapping : Profile
    {
        #region Constructors
        public PersistenceMapping()
        {
            #region Parameters
            CreateMap<Core.Application.Parameters.Brand.InsertRequest, Core.Domain.Documents.Brand>();
            CreateMap<Core.Application.Parameters.Category.InsertRequest, Core.Domain.Documents.Category>();
            CreateMap<Core.Application.Parameters.Variant.InsertRequest, Core.Domain.Documents.Variant>();
            CreateMap<Core.Application.Parameters.VariantValue.InsertRequest, Core.Domain.Documents.VariantValue>();
            CreateMap<Core.Application.Parameters.Product.InsertRequest, Core.Domain.Documents.Product>();
            CreateMap<Core.Application.Parameters.ProductInformation.InsertRequest, Core.Domain.Documents.ProductInformation>();
            #endregion

            CreateMap<Core.Domain.Documents.Brand, Core.Application.DataTransferObjects.Brand>().ReverseMap();
            CreateMap<Core.Domain.Documents.Category, Core.Application.DataTransferObjects.Category>().ReverseMap();
            CreateMap<Core.Domain.Documents.Variant, Core.Application.DataTransferObjects.Variant>().ReverseMap();
            CreateMap<Core.Domain.Documents.VariantValue, Core.Application.DataTransferObjects.VariantValue>().ReverseMap();
            CreateMap<Core.Domain.Documents.Product, Core.Application.DataTransferObjects.Product>().ReverseMap();
            CreateMap<Core.Domain.Documents.ProductInformation, Core.Application.DataTransferObjects.ProductInformation>().ReverseMap();
            CreateMap<Core.Domain.Documents.Label, Core.Application.DataTransferObjects.Label>().ReverseMap();
            CreateMap<Core.Domain.Documents.Price, Core.Application.DataTransferObjects.Price>().ReverseMap();
            CreateMap<Core.Domain.Documents.Shelf, Core.Application.DataTransferObjects.Shelf>().ReverseMap();
        }
        #endregion
    }
}
