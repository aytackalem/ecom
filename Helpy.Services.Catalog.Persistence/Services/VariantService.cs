﻿using Amazon.Runtime.Internal;
using AutoMapper;
using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Variant;
using Helpy.Services.Catalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Helpy.Services.Catalog.Persistence.Services
{
    public class VariantService : ServiceBase<Core.Domain.Documents.Variant>, IVariantService
    {
        #region Constructors
        public VariantService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.VariantCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Variant>> GetByIdAsync(string id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Variant>>(async resoponse =>
            {
                var result = await base._mongoCollection.FindAsync(v => v.Id == id);
                var variant = await result.FirstOrDefaultAsync();

                resoponse.Data = base._mapper.Map<Variant>(variant);
            });
        }

        public async Task<DataResponse<Variant>> InsertAsync(InsertRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Variant>>(async resoponse =>
            {
                var document = this._mapper.Map<Core.Domain.Documents.Variant>(request);

                await base._mongoCollection.InsertOneAsync(document);

                resoponse.Data = base._mapper.Map<Variant>(document);
            });
        }

        public async Task<DataResponse<VariantValue>> InsertVariantValueAsync(Core.Application.Parameters.VariantValue.InsertRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<VariantValue>>(async resoponse =>
            {
                var document = this._mapper.Map<Core.Domain.Documents.VariantValue>(request);
                document.Id = ObjectId.GenerateNewId().ToString();

                var result = await base._mongoCollection.FindAsync(v => v.Id == request.VariantId);
                var variant = await result.FirstOrDefaultAsync();

                variant.VariantValues.Add(document);

                var replaceOneResult = await base._mongoCollection.ReplaceOneAsync(v => v.Id == request.VariantId, variant);

                resoponse.Data = base._mapper.Map<VariantValue>(document);
            });
        }
        #endregion
    }
}
