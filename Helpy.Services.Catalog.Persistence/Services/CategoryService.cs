﻿using AutoMapper;
using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Category;
using Helpy.Services.Catalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Persistence.Services
{
    public class CategoryService : ServiceBase<Core.Domain.Documents.Category>, ICategoryService
    {
        #region Constructors
        public CategoryService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.CategoryCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Category>> InsertAsync(InsertRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Category>>(async resoponse =>
            {
                var document = this._mapper.Map<Core.Domain.Documents.Category>(request);

                await base._mongoCollection.InsertOneAsync(document);

                resoponse.Data = base._mapper.Map<Category>(document);
            });
        } 
        #endregion
    }
}
