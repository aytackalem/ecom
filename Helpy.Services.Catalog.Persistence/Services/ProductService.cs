﻿using AutoMapper;
using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Product;
using Helpy.Services.Catalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Helpy.Services.Catalog.Persistence.Services
{
    public class ProductService : ServiceBase<Core.Domain.Documents.Product>, IProductService
    {
        #region Constructors
        public ProductService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.ProductCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Product>> GetByIdAsync(string id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Product>>(async resoponse =>
            {
                var result = await base._mongoCollection.FindAsync<Core.Domain.Documents.Product>(p => p.Id == id);
                var product = await result.FirstOrDefaultAsync();

                resoponse.Data = base._mapper.Map<Product>(product);
            });
        }

        public async Task<DataResponse<Product>> InsertAsync(InsertRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Product>>(async resoponse =>
            {
                var document = this._mapper.Map<Core.Domain.Documents.Product>(request);
                document.ProductInformations.ForEach(pi => pi.Id = ObjectId.GenerateNewId().ToString());

                await base._mongoCollection.InsertOneAsync(document);

                resoponse.Data = base._mapper.Map<Product>(document);
            });
        }
        #endregion
    }
}
