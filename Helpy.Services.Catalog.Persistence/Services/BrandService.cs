﻿using AutoMapper;
using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Brand;
using Helpy.Services.Catalog.Persistence.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Persistence.Services
{
    public class BrandService : ServiceBase<Core.Domain.Documents.Brand>, IBrandService
    {
        #region Constructors
        public BrandService(MongoConfiguration mongoConfiguration, IMapper mapper) : base(mongoConfiguration.ConnectionString, mongoConfiguration.DatabaseName, mongoConfiguration.BrandCollectionName, mapper)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Brand>> InsertAsync(InsertRequest request)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Brand>>(async resoponse =>
            {
                var document = this._mapper.Map<Core.Domain.Documents.Brand>(request);

                await base._mongoCollection.InsertOneAsync(document);

                resoponse.Data = base._mapper.Map<Brand>(document);
            });
        }
        #endregion
    }
}
