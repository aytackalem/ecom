﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.Accounting.Mikro;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Mikro.Stock.Update.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();

            var fileName = Process.GetCurrentProcess().MainModule.FileName;

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(fileName).FullName)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddSingleton<IApp, App>();
            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();
            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinder>();
            serviceCollection.AddScoped<IAccountingProvider, MikroProviderService>();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(appConfig.Tenant.EcommerceConnectionStrings);
            });
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            return serviceCollection.BuildServiceProvider();
        }
    }
}











//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//namespace ECommerce.Mikro.Stock.Update.Job
//{
//    class Program
//    {
//        static async Task Main(string[] args)
//        {
//            var keywords = new[] { "mangallar", "fidan", "saksı", "bahçe", "gübre", "sera", "tohum", "bitki", "teraryum", "toprak", "çiçek", "yağdanlık", "yapıştırıcı", "takım çantaları", "vida", "iş güvenlik", "pense", "silikon", "maket bıçak", "su terazi", "testere", "çizme", "tornavida", "bilim teknik", "dekoratif ürünler", "kova", "çekiç" };

//            var categories = await GetCategoriesAsync();
//            var foundCategories = categories.Where(c =>
//            {
//                foreach (var kLoop in keywords)
//                {
//                    var contains = c.name.ToLowerInvariant().IndexOf(kLoop) > 0;
//                    if (contains)
//                        return true;
//                }

//                return false;
//            }).ToList();

//            //var a = JsonConvert.SerializeObject(foundCategories.Select(x => new { Id = x.categoryId, ParentCategoryId = x.parentCategoryId, Name = x.name }));

//            //var abc = string.Join(",", foundCategories.Select(x => x.categoryId));

//            foundCategories.ForEach(async c =>
//            {
//                c.attrs = await GetAttributesAsync(c.categoryId);
//                c.attrs.variantAttributes.ForEach(async va =>
//                {
//                    va.vals = await GetAttributeValuesAsync(c.categoryId, va.name);
//                });
//            });
//        }

//        static async Task<List<Models.Response.Category.CategoryData>> GetCategoriesAsync()
//        {
//            List<Models.Response.Category.CategoryData> list = new();

//            Models.Response.Category.Response response = null;
//            int page = 0;
//            do
//            {
//                response = await GetAsync<Models.Response.Category.Response>($"https://mpop.hepsiburada.com/product/api/categories/get-all-categories?leaf=true&status=ACTIVE&available=true&page={page}&size=2000&version=1");

//                list.AddRange(response.data);

//                page++;
//            } while (response != null && response.totalPages > page);

//            return list;
//        }

//        static async Task<Models.Response.Attribute.AttributeData> GetAttributesAsync(int categoryId)
//        {
//            Models.Response.Attribute.Response response = await GetAsync<Models.Response.Attribute.Response>($"https://mpop.hepsiburada.com/product/api/categories/{categoryId}/attributes");

//            return response.data;
//        }

//        static async Task<List<Models.Response.AttributeValue.Datum>> GetAttributeValuesAsync(int categoryId, string attibuteName)
//        {
//            Models.Response.AttributeValue.Response response = await GetAsync<Models.Response.AttributeValue.Response>($"https://mpop.hepsiburada.com/product/api/categories/{categoryId}/attribute/{attibuteName}/values?page=0&size=1000&version=4");

//            return response.data;
//        }

//        static async Task<T> GetAsync<T>(string url) where T : new()
//        {
//            T t = default;

//            using (HttpClient httpClient = new())
//            {
//                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "RmlkYW5pc3RhbmJ1bF9kZXY6RzFXSWRtNHdnMmdwIQ==");

//                var httpResponseMessage = await httpClient.GetAsync(url);

//                HttpContent content = httpResponseMessage.Content;
//                string responseString = await content.ReadAsStringAsync();

//                try
//                {
//                    t = JsonConvert.DeserializeObject<T>(responseString);
//                }
//                catch (Exception e)
//                {
//                }
//            }

//            return t;
//        }
//    }
//}
