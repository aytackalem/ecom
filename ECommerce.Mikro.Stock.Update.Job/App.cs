﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace ECommerce.Mikro.Stock.Update.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IAccountingProvider _accountingService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IAccountingProvider accountingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._accountingService = accountingService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var productInformations = this
                ._unitOfWork
                .ProductInformationRepository
                .DbSet()
                .Include(pi => pi.ProductInformationCombines)
                .ThenInclude(pic => pic.ContainProductInformation)
                .ToList();

            foreach (var piLoop in productInformations.Where(pi => pi.Type == "PI"))
                piLoop.Stock = this._accountingService.ReadStock(piLoop.StockCode, "1").Data;

            var productCombines = productInformations.Where(pi => pi.Type == "PC");
            foreach (var piLoop in productCombines)
            {
                if (piLoop.ProductInformationCombines.Count == 1)
                {
                    piLoop.Stock = piLoop.ProductInformationCombines[0].ContainProductInformation.Stock / piLoop.ProductInformationCombines[0].Quantity;
                }
                else
                {
                    piLoop.Stock = piLoop.ProductInformationCombines.Min(pic => pic.ContainProductInformation.Stock);
                }
            }

            foreach (var piLoop in productInformations)
                this
                    ._unitOfWork
                    .ProductInformationRepository
                    .Update(piLoop);
        }
        #endregion
    }
}
