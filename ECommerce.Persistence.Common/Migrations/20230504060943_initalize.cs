﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class initalize : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accountings",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accountings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmailProviders",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FeedTemplates",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceRequestTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceRequestTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Marketplaces",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    BrandAutocomplete = table.Column<bool>(type: "bit", nullable: false),
                    CreateProductTrackable = table.Column<bool>(type: "bit", nullable: false),
                    UpdateProductTrackable = table.Column<bool>(type: "bit", nullable: false),
                    UpdatePriceTrackable = table.Column<bool>(type: "bit", nullable: false),
                    UpdateStockTrackable = table.Column<bool>(type: "bit", nullable: false),
                    IsECommerce = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marketplaces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posses",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Default = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductSources",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentCompanies",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentCompanies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmsProviders",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tenants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QueueId = table.Column<int>(type: "int", nullable: false),
                    Indefinite = table.Column<bool>(type: "bit", nullable: false),
                    ContractDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpirationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenants", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UpsCities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpsCities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cities_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExchangeRates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CurrencyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Rate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExchangeRates_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderTypeTranslations",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTypeTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderTypeTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderTypeTranslations_OrderTypes_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalTable: "OrderTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PaymentTypeTranslations",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaymentTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTypeTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTypeTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentTypeTranslations_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Banks",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    PosId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Banks_Posses_PosId",
                        column: x => x.PosId,
                        principalTable: "Posses",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Domains",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Domains_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ExpenseSources",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpenseSources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExpenseSources_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderSources",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderSources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderSources_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TenantInvoiceInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    TaxOffice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mail = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantInvoiceInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantInvoiceInformations_Tenants_Id",
                        column: x => x.Id,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenantPayments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    Paid = table.Column<bool>(type: "bit", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantPayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantPayments_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenantSalesAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Monthly = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    HalfYearly = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Yearly = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantSalesAmounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantSalesAmounts_Tenants_Id",
                        column: x => x.Id,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenantSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantSettings_Tenants_Id",
                        column: x => x.Id,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpsAreas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentUpsAreaId = table.Column<int>(type: "int", nullable: true),
                    UpsCityId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpsAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpsAreas_UpsAreas_ParentUpsAreaId",
                        column: x => x.ParentUpsAreaId,
                        principalTable: "UpsAreas",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_UpsAreas_UpsCities_UpsCityId",
                        column: x => x.UpsCityId,
                        principalTable: "UpsCities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CityId = table.Column<int>(type: "int", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Districts_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BankId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Number = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bins_Banks_BankId",
                        column: x => x.BankId,
                        principalTable: "Banks",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Brands_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Brands_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentCategoryId = table.Column<int>(type: "int", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    SortNumber = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Categories_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Categories_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Companies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Companies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Configurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Configurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Configurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Whatsapp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contacts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Contacts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Contents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contents_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Contents_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DiscountCoupons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountCoupons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiscountCoupons_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DiscountCoupons_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DiscountCoupons_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DomainSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImagePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DomainSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DomainSettings_Domains_Id",
                        column: x => x.Id,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DomainSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EntireDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntireDiscounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EntireDiscounts_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EntireDiscounts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EntireDiscounts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FacebookConfigrations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacebookConfigrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FacebookConfigrations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FacebookConfigrations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Feeds",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeedTemplateId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feeds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feeds_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Feeds_FeedTemplates_FeedTemplateId",
                        column: x => x.FeedTemplateId,
                        principalTable: "FeedTemplates",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Feeds_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FrequentlyAskedQuestions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrequentlyAskedQuestions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestions_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestions_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GetXPayYSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    X = table.Column<int>(type: "int", nullable: false),
                    XLimit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Y = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GetXPayYSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GetXPayYSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_GetXPayYSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GoogleConfigrations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoogleConfigrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoogleConfigrations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_GoogleConfigrations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Informations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    NewBulletinSend = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Informations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Informations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Informations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Labels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Labels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Labels_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Labels_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MoneyPointSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Limit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    EarningIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Earning = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoneyPointSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MoneyPointSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MoneyPointSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NewBulletins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Mail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewBulletins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewBulletins_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NewBulletins_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PaymentTypeDomains",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaymentTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTypeDomains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTypeDomains_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentTypeDomains_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentTypeDomains_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PosConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PosId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PosConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PosConfigurations_Posses_PosId",
                        column: x => x.PosId,
                        principalTable: "Posses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PosConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductSourceDomains",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductSourceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSourceDomains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductSourceDomains_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductSourceDomains_ProductSources_ProductSourceId",
                        column: x => x.ProductSourceId,
                        principalTable: "ProductSources",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductSourceDomains_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Properties_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Properties_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCartDiscounteds",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCartDiscounteds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscounteds_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscounteds_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscounteds_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Suppliers_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Suppliers_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tags_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Tags_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Photoable = table.Column<bool>(type: "bit", nullable: false),
                    ShowVariant = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Variants_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Variants_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Neighborhoods",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DistrictId = table.Column<int>(type: "int", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Neighborhoods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Neighborhoods_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BrandDiscountSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandDiscountSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrandDiscountSettings_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BrandDiscountSettings_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BrandDiscountSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BrandDiscountSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CategoryDiscountSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ParentCategoryDiscountSettingId = table.Column<int>(type: "int", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryDiscountSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryDiscountSettings_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryDiscountSettings_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryDiscountSettings_CategoryDiscountSettings_ParentCategoryDiscountSettingId",
                        column: x => x.ParentCategoryDiscountSettingId,
                        principalTable: "CategoryDiscountSettings",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryDiscountSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryDiscountSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CategoryTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IconFileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceCategoryMappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CompanyId = table.Column<int>(type: "int", nullable: true),
                    MarketplaceCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceCategoryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceCategoryMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryMappings_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryMappings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryMappings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Showcases",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryId = table.Column<int>(type: "int", nullable: true),
                    BrandId = table.Column<int>(type: "int", nullable: true),
                    Json = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Showcases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Showcases_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Showcases_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Showcases_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Showcases_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Showcases_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AccountingCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountingId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingCompanies_Accountings_AccountingId",
                        column: x => x.AccountingId,
                        principalTable: "Accountings",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingCompanies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingCompanies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CompanyConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CompanyConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CompanyContacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxOffice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WebUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegistrationNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyContacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyContacts_Companies_Id",
                        column: x => x.Id,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyContacts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CompanyContacts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CompanySettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    StockControl = table.Column<bool>(type: "bit", nullable: false),
                    CreateNonexistProduct = table.Column<bool>(type: "bit", nullable: false),
                    DefaultCurrencyId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DefaultLanguageId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DefaultCountryId = table.Column<int>(type: "int", nullable: false),
                    Theme = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UUId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IncludeMarketplace = table.Column<bool>(type: "bit", nullable: false),
                    IncludeECommerce = table.Column<bool>(type: "bit", nullable: false),
                    IncludeExport = table.Column<bool>(type: "bit", nullable: false),
                    WebUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderDetailMultiselect = table.Column<bool>(type: "bit", nullable: false),
                    ShipmentSecret = table.Column<bool>(type: "bit", nullable: false),
                    ShipmentSecretProductName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IncludeAccounting = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanySettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanySettings_Companies_Id",
                        column: x => x.Id,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanySettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CompanySettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customers_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customers_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ECommerceBrands",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommerceBrands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECommerceBrands_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceBrands_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceBrands_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ECommerceCategories",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParentId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommerceCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECommerceCategories_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceCategories_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceCategories_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ECommerceCategories_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ECommerceVariants",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ECommerceCategoryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mandatory = table.Column<bool>(type: "bit", nullable: false),
                    AllowCustom = table.Column<bool>(type: "bit", nullable: false),
                    MultiValue = table.Column<bool>(type: "bit", nullable: false),
                    Variantable = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommerceVariants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECommerceVariants_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceVariants_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceVariants_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ECommerceVariants_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ECommerceVariantValues",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ECommerceVariantCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommerceVariantValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECommerceVariantValues_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceVariantValues_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECommerceVariantValues_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EmailProviderCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailProviderId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailProviderCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailProviderCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailProviderCompanies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailProviderCompanies_EmailProviders_EmailProviderId",
                        column: x => x.EmailProviderId,
                        principalTable: "EmailProviders",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailProviderCompanies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceBrandMappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceBrandCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceBrandName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceBrandMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceBrandMappings_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketplaceBrandMappings_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceBrandMappings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceBrandMappings_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceBrandMappings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    ReadOrder = table.Column<bool>(type: "bit", nullable: false),
                    CreateProduct = table.Column<bool>(type: "bit", nullable: false),
                    UpdatePrice = table.Column<bool>(type: "bit", nullable: false),
                    UpdateStock = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCompanies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCompanies_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCompanies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ShipmentCompanyCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShipmentCompanyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentCompanyCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyCompanies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyCompanies_ShipmentCompanies_ShipmentCompanyId",
                        column: x => x.ShipmentCompanyId,
                        principalTable: "ShipmentCompanies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyCompanies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SmsProviderCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SmsProviderId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsProviderCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmsProviderCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsProviderCompanies_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsProviderCompanies_SmsProviders_SmsProviderId",
                        column: x => x.SmsProviderId,
                        principalTable: "SmsProviders",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsProviderCompanies_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SmsTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    PaymentTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Template = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmsTemplates_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsTemplates_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsTemplates_OrderTypes_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalTable: "OrderTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsTemplates_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsTemplates_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContactTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContactId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactTranslations_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContactTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContactTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContactTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContentTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Header = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContentId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentTranslations_Contents_ContentId",
                        column: x => x.ContentId,
                        principalTable: "Contents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContentTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContentTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DiscountCouponSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Limit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    SingleUse = table.Column<bool>(type: "bit", nullable: false),
                    Used = table.Column<bool>(type: "bit", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountCouponSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiscountCouponSettings_DiscountCoupons_Id",
                        column: x => x.Id,
                        principalTable: "DiscountCoupons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DiscountCouponSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DiscountCouponSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EntireDiscountSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Limit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntireDiscountSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EntireDiscountSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EntireDiscountSettings_EntireDiscounts_Id",
                        column: x => x.Id,
                        principalTable: "EntireDiscounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EntireDiscountSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FrequentlyAskedQuestionTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Header = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FrequentlyAskedQuestionId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrequentlyAskedQuestionTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestionTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestionTranslations_FrequentlyAskedQuestions_FrequentlyAskedQuestionId",
                        column: x => x.FrequentlyAskedQuestionId,
                        principalTable: "FrequentlyAskedQuestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestionTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FrequentlyAskedQuestionTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "InformationTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Header = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InformationId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InformationTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InformationTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_InformationTranslations_Informations_InformationId",
                        column: x => x.InformationId,
                        principalTable: "Informations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InformationTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_InformationTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "LabelTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LabelId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LabelTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LabelTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LabelTranslations_Labels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Labels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LabelTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LabelTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NewBulletinInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NewBulletinId = table.Column<int>(type: "int", nullable: false),
                    InformationId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewBulletinInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewBulletinInformations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NewBulletinInformations_Informations_InformationId",
                        column: x => x.InformationId,
                        principalTable: "Informations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NewBulletinInformations_NewBulletins_NewBulletinId",
                        column: x => x.NewBulletinId,
                        principalTable: "NewBulletins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NewBulletinInformations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PaymentTypeSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CostIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Cost = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTypeSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTypeSettings_PaymentTypeDomains_Id",
                        column: x => x.Id,
                        principalTable: "PaymentTypeDomains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductSourceConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductSourceDomainId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSourceConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductSourceConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductSourceConfigurations_ProductSourceDomains_ProductSourceDomainId",
                        column: x => x.ProductSourceDomainId,
                        principalTable: "ProductSourceDomains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductSourceConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductSourceMappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductSourceDomainId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSourceMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductSourceMappings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductSourceMappings_ProductSourceDomains_ProductSourceDomainId",
                        column: x => x.ProductSourceDomainId,
                        principalTable: "ProductSourceDomains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductSourceMappings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PropertyTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PropertyTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyTranslations_Properties_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Properties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PropertyValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyValues_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PropertyValues_Properties_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Properties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyValues_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCartDiscountedSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Limit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCartDiscountedSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedSettings_ShoppingCartDiscounteds_Id",
                        column: x => x.Id,
                        principalTable: "ShoppingCartDiscounteds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    SupplierId = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsOnlyHidden = table.Column<bool>(type: "bit", nullable: false),
                    SellerCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccountingProperty = table.Column<bool>(type: "bit", nullable: false),
                    ProductSourceDomainId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Products_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Products_ProductSourceDomains_ProductSourceDomainId",
                        column: x => x.ProductSourceDomainId,
                        principalTable: "ProductSourceDomains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Products_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TagTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TagTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TagTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagTranslations_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "VariantTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VariantId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariantTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VariantTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantTranslations_Variants_VariantId",
                        column: x => x.VariantId,
                        principalTable: "Variants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VariantValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VariantId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariantValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VariantValues_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantValues_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantValues_Variants_VariantId",
                        column: x => x.VariantId,
                        principalTable: "Variants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryContentTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryContentTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryContentTranslations_CategoryTranslations_Id",
                        column: x => x.Id,
                        principalTable: "CategoryTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryContentTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryContentTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CategorySeoTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaKeywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategorySeoTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategorySeoTranslations_CategoryTranslations_Id",
                        column: x => x.Id,
                        principalTable: "CategoryTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategorySeoTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategorySeoTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CategoryTranslationBreadcrumbs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTranslationBreadcrumbs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryTranslationBreadcrumbs_CategoryTranslations_Id",
                        column: x => x.Id,
                        principalTable: "CategoryTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryTranslationBreadcrumbs_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryTranslationBreadcrumbs_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AccountingConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountingCompanyId = table.Column<int>(type: "int", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccountingId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingConfigurations_AccountingCompanies_AccountingCompanyId",
                        column: x => x.AccountingCompanyId,
                        principalTable: "AccountingCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountingConfigurations_Accountings_AccountingId",
                        column: x => x.AccountingId,
                        principalTable: "Accountings",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountingConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CustomerAddresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Recipient = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    NeighborhoodId = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Default = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerAddresses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerAddresses_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerAddresses_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerAddresses_Neighborhoods_NeighborhoodId",
                        column: x => x.NeighborhoodId,
                        principalTable: "Neighborhoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerAddresses_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CustomerContacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxOffice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerContacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerContacts_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerContacts_Customers_Id",
                        column: x => x.Id,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerContacts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerContacts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CustomerInvoiceInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    NeighborhoodId = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxOffice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Default = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerInvoiceInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerInvoiceInformations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerInvoiceInformations_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerInvoiceInformations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerInvoiceInformations_Neighborhoods_NeighborhoodId",
                        column: x => x.NeighborhoodId,
                        principalTable: "Neighborhoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerInvoiceInformations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CustomerNotes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerNotes_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerNotes_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerNotes_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerNotes_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CustomerUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CustomerRoleId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Token = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    TokenExpried = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MoneyPointAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Verified = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerUsers_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerUsers_CustomerRoles_CustomerRoleId",
                        column: x => x.CustomerRoleId,
                        principalTable: "CustomerRoles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerUsers_Customers_Id",
                        column: x => x.Id,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerUsers_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CustomerUsers_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UUId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    OrderSourceId = table.Column<int>(type: "int", nullable: false),
                    ListTotal = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Total = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    CargoFee = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    SurchargeFee = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    ShoppingCartDiscount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    CommissionAmount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcListTotal = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcTotal = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcDiscount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    CurrencyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    OrderTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    BulkInvoicing = table.Column<bool>(type: "bit", nullable: false),
                    EstimatedPackingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MarketplaceOrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_OrderSources_OrderSourceId",
                        column: x => x.OrderSourceId,
                        principalTable: "OrderSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_OrderTypes_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalTable: "OrderTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EmailProviderConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailProviderCompanyId = table.Column<int>(type: "int", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailProviderConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailProviderConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailProviderConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailProviderConfigurations_EmailProviderCompanies_EmailProviderCompanyId",
                        column: x => x.EmailProviderCompanyId,
                        principalTable: "EmailProviderCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailProviderConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketPlaceConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceCompanyId = table.Column<int>(type: "int", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketPlaceConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketPlaceConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketPlaceConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketPlaceConfigurations_MarketplaceCompanies_MarketplaceCompanyId",
                        column: x => x.MarketplaceCompanyId,
                        principalTable: "MarketplaceCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketPlaceConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ShipmentCompanyConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShipmentCompanyCompanyId = table.Column<int>(type: "int", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentCompanyConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyConfigurations_ShipmentCompanyCompanies_ShipmentCompanyCompanyId",
                        column: x => x.ShipmentCompanyCompanyId,
                        principalTable: "ShipmentCompanyCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShipmentCompanyConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SmsProviderConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SmsProviderCompanyId = table.Column<int>(type: "int", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsProviderConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmsProviderConfigurations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsProviderConfigurations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsProviderConfigurations_SmsProviderCompanies_SmsProviderCompanyId",
                        column: x => x.SmsProviderCompanyId,
                        principalTable: "SmsProviderCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SmsProviderConfigurations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContentSeoTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaKeywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentSeoTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentSeoTranslations_ContentTranslations_Id",
                        column: x => x.Id,
                        principalTable: "ContentTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentSeoTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContentSeoTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContentTranslationContents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentTranslationContents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentTranslationContents_ContentTranslations_Id",
                        column: x => x.Id,
                        principalTable: "ContentTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentTranslationContents_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContentTranslationContents_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "InformationSeoTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaKeywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InformationSeoTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InformationSeoTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_InformationSeoTranslations_InformationTranslations_Id",
                        column: x => x.Id,
                        principalTable: "InformationTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InformationSeoTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "InformationTranslationContents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InformationTranslationContents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InformationTranslationContents_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_InformationTranslationContents_InformationTranslations_Id",
                        column: x => x.Id,
                        principalTable: "InformationTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InformationTranslationContents_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PropertyValueTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyValueId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyValueTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyValueTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PropertyValueTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyValueTranslations_PropertyValues_PropertyValueId",
                        column: x => x.PropertyValueId,
                        principalTable: "PropertyValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PropertyValueTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Barcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StockCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SkuCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stock = table.Column<int>(type: "int", nullable: false),
                    UpdateStock = table.Column<int>(type: "int", nullable: false),
                    VirtualStock = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsSale = table.Column<bool>(type: "bit", nullable: false),
                    Payor = table.Column<bool>(type: "bit", nullable: false),
                    GroupId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Photoable = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    ShelfZone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShelfCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformations_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductLabels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    LabelId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductLabels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductLabels_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductLabels_Labels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Labels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductLabels_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductLabels_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductMarketplaces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    UUId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceBrandCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceBrandName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceCategoryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceCategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SellerCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    DeliveryDay = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductMarketplaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductMarketplaces_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductMarketplaces_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductMarketplaces_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductMarketplaces_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductProperties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    PropertyValueId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductProperties_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductProperties_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductProperties_PropertyValues_PropertyValueId",
                        column: x => x.PropertyValueId,
                        principalTable: "PropertyValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductProperties_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductTags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    TagId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductTags_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductTags_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTags_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTranslations_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCartDiscountedProducts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCartDiscountedProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProducts_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProducts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProducts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceVariantValueMappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VariantValueId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceVariantCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AllowCustom = table.Column<bool>(type: "bit", nullable: false),
                    Mandatory = table.Column<bool>(type: "bit", nullable: false),
                    Varianter = table.Column<bool>(type: "bit", nullable: false),
                    Multiple = table.Column<bool>(type: "bit", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceVariantValueMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceVariantValueMappings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceVariantValueMappings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceVariantValueMappings_VariantValues_VariantValueId",
                        column: x => x.VariantValueId,
                        principalTable: "VariantValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VariantValueTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VariantValueId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariantValueTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VariantValueTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantValueTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantValueTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VariantValueTranslations_VariantValues_VariantValueId",
                        column: x => x.VariantValueId,
                        principalTable: "VariantValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TwoFactorCustomerUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TryCount = table.Column<int>(type: "int", nullable: false),
                    CustomerUserId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TwoFactorCustomerUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TwoFactorCustomerUsers_CustomerUsers_CustomerUserId",
                        column: x => x.CustomerUserId,
                        principalTable: "CustomerUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TwoFactorCustomerUsers_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TwoFactorCustomerUsers_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MoneyPoints",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CustomerUserId = table.Column<int>(type: "int", nullable: false),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Approved = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoneyPoints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MoneyPoints_CustomerUsers_CustomerUserId",
                        column: x => x.CustomerUserId,
                        principalTable: "CustomerUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MoneyPoints_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MoneyPoints_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoneyPoints_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderBillings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    InvoiceNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReturnNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReturnDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderBillings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderBillings_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderBillings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderBillings_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderBillings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDeliveryAddresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Recipient = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NeighborhoodId = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDeliveryAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDeliveryAddresses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderDeliveryAddresses_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderDeliveryAddresses_Neighborhoods_NeighborhoodId",
                        column: x => x.NeighborhoodId,
                        principalTable: "Neighborhoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDeliveryAddresses_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDeliveryAddresses_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderInvoiceInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    AccountingCompanyId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NeighborhoodId = table.Column<int>(type: "int", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxOffice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Number = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderInvoiceInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderInvoiceInformations_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderInvoiceInformations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderInvoiceInformations_Neighborhoods_NeighborhoodId",
                        column: x => x.NeighborhoodId,
                        principalTable: "Neighborhoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderInvoiceInformations_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderInvoiceInformations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderNotes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderNotes_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderNotes_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderNotes_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderNotes_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderPackings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPackings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderPackings_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPackings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPackings_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPackings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderPickings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PackerBarcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPickings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderPickings_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPickings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPickings_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderPickings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderShipments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    ShipmentCompanyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TrackingUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrackingBase64 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrackingCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PackageNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Payor = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderShipments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderShipments_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderShipments_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderShipments_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderShipments_ShipmentCompanies_ShipmentCompanyId",
                        column: x => x.ShipmentCompanyId,
                        principalTable: "ShipmentCompanies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderShipments_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderSmsses",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderSmsses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderSmsses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderSmsses_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderSmsses_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderSmsses_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderTechnicInformations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Browser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Platform = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTechnicInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderTechnicInformations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderTechnicInformations_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderTechnicInformations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderViewings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderViewings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderViewings_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderViewings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderViewings_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderViewings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    PaymentTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DiscountCouponId = table.Column<int>(type: "int", nullable: true),
                    Paid = table.Column<bool>(type: "bit", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payments_DiscountCoupons_DiscountCouponId",
                        column: x => x.DiscountCouponId,
                        principalTable: "DiscountCoupons",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Payments_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Payments_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payments_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Payments_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CategoryProducts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: true),
                    ProductInformationId = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Leaf = table.Column<bool>(type: "bit", nullable: false),
                    Multi = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryProducts_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryProducts_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryProducts_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CategoryProducts_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Expenses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExpenseSourceId = table.Column<int>(type: "int", nullable: false),
                    OrderSourceId = table.Column<int>(type: "int", nullable: true),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ProductInformationId = table.Column<int>(type: "int", nullable: true),
                    ExpenseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expenses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Expenses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Expenses_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Expenses_ExpenseSources_ExpenseSourceId",
                        column: x => x.ExpenseSourceId,
                        principalTable: "ExpenseSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Expenses_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Expenses_OrderSources_OrderSourceId",
                        column: x => x.OrderSourceId,
                        principalTable: "OrderSources",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Expenses_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Expenses_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FeedDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeedId = table.Column<int>(type: "int", nullable: false),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedDetails_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FeedDetails_Feeds_FeedId",
                        column: x => x.FeedId,
                        principalTable: "Feeds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeedDetails_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeedDetails_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UUId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    ListUnitPrice = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UnitDiscount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UnitCost = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UnitCargoFee = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UnitCommissionAmount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcListUnitPrice = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcUnitPrice = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcUnitDiscount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatExcUnitCost = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    VatRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Payor = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderDetails_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderDetails_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderReturnDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    IsReturn = table.Column<bool>(type: "bit", nullable: false),
                    IsLoss = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderReturnDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderReturnDetails_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderReturnDetails_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderReturnDetails_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderReturnDetails_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderReturnDetails_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationCombines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    ContainProductInformationId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationCombines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationCombines_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationCombines_ProductInformations_ContainProductInformationId",
                        column: x => x.ContainProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationCombines_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationCombines_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationComments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rating = table.Column<float>(type: "real", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Approved = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationComments_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationComments_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationComments_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationMarketplaces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ListUnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StockCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Barcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UUId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    DirtyStock = table.Column<bool>(type: "bit", nullable: false),
                    DirtyPrice = table.Column<bool>(type: "bit", nullable: false),
                    DirtyProductInformation = table.Column<bool>(type: "bit", nullable: false),
                    AccountingPrice = table.Column<bool>(type: "bit", nullable: false),
                    Opened = table.Column<bool>(type: "bit", nullable: false),
                    OnSale = table.Column<bool>(type: "bit", nullable: false),
                    Locked = table.Column<bool>(type: "bit", nullable: false),
                    IgnoreSystemNotification = table.Column<bool>(type: "bit", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusCheckDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationMarketplaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaces_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaces_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaces_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaces_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationPhotos_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationPhotos_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationPhotos_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationPriceses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CurrencyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ConversionCurrencyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    ListUnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ConversionUnitCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    OriginalUnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    VatRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AccountingPrice = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationPriceses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationPriceses_Currencies_ConversionCurrencyId",
                        column: x => x.ConversionCurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationPriceses_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationPriceses_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationPriceses_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationPriceses_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationSubscriptions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsSend = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationSubscriptions_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationSubscriptions_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationSubscriptions_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    VariantValuesDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FullVariantValuesDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubTitle = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Url = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslations_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslations_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationVariants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    VariantValueId = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationVariants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationVariants_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationVariants_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationVariants_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationVariants_VariantValues_VariantValueId",
                        column: x => x.VariantValueId,
                        principalTable: "VariantValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCartDiscountedProductSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DiscountIsRate = table.Column<bool>(type: "bit", nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCartDiscountedProductSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProductSettings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProductSettings_ShoppingCartDiscountedProducts_Id",
                        column: x => x.Id,
                        principalTable: "ShoppingCartDiscountedProducts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShoppingCartDiscountedProductSettings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "OrderShipmentDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderShipmentId = table.Column<int>(type: "int", nullable: false),
                    TrackingCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Weight = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderShipmentDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderShipmentDetails_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderShipmentDetails_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderShipmentDetails_OrderShipments_OrderShipmentId",
                        column: x => x.OrderShipmentId,
                        principalTable: "OrderShipments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderShipmentDetails_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PaymentLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Pay3dMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Check3dMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentLogs_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentLogs_Payments_Id",
                        column: x => x.Id,
                        principalTable: "Payments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentLogs_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationMarketplaceBulkPrices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ListUnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AccountingPrice = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationMarketplaceBulkPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceBulkPrices_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceBulkPrices_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceBulkPrices_ProductInformationMarketplaces_Id",
                        column: x => x.Id,
                        principalTable: "ProductInformationMarketplaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceBulkPrices_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationMarketplaceVariantValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationMarketplaceId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceVariantCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AllowCustom = table.Column<bool>(type: "bit", nullable: false),
                    Mandatory = table.Column<bool>(type: "bit", nullable: false),
                    Varianter = table.Column<bool>(type: "bit", nullable: false),
                    Multiple = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationMarketplaceVariantValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceVariantValues_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceVariantValues_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceVariantValues_ProductInformationMarketplaces_ProductInformationMarketplaceId",
                        column: x => x.ProductInformationMarketplaceId,
                        principalTable: "ProductInformationMarketplaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationMarketplaceVariantValues_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationContentTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ExpertOpinion = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationContentTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationContentTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationContentTranslations_ProductInformationTranslations_Id",
                        column: x => x.Id,
                        principalTable: "ProductInformationTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationContentTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationSeoTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaKeywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationSeoTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationSeoTranslations_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationSeoTranslations_ProductInformationTranslations_Id",
                        column: x => x.Id,
                        principalTable: "ProductInformationTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationSeoTranslations_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationTranslationBreadcrumbs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationTranslationBreadcrumbs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslationBreadcrumbs_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslationBreadcrumbs_ProductInformationTranslations_Id",
                        column: x => x.Id,
                        principalTable: "ProductInformationTranslations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductInformationTranslationBreadcrumbs_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Currencies",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[] { "TL", true, "Türk Lirası" });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[] { "TR", true, "Türkçe" });

            migrationBuilder.InsertData(
                table: "Marketplaces",
                columns: new[] { "Id", "Active", "BrandAutocomplete", "CreateProductTrackable", "IsECommerce", "Name", "UpdatePriceTrackable", "UpdateProductTrackable", "UpdateStockTrackable" },
                values: new object[,]
                {
                    { "CS", true, false, false, false, "Ciceksepeti", false, false, false },
                    { "HB", true, false, false, false, "Hepsiburada", false, false, false },
                    { "IK", true, false, false, true, "Ikas", false, false, false },
                    { "M", true, false, false, false, "Morhipo", false, false, false },
                    { "ML", true, true, false, false, "ModaLog", false, false, false },
                    { "MN", true, true, false, false, "Modanisa", false, false, false },
                    { "N11", true, false, false, false, "N11", false, false, false },
                    { "PA", true, false, false, false, "Pazarama", false, false, false },
                    { "PTT", true, false, false, false, "PttAvm", false, false, false },
                    { "TY", true, false, false, false, "Trendyol", false, false, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCompanies_AccountingId",
                table: "AccountingCompanies",
                column: "AccountingId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCompanies_CompanyId",
                table: "AccountingCompanies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCompanies_DomainId",
                table: "AccountingCompanies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCompanies_TenantId",
                table: "AccountingCompanies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingConfigurations_AccountingCompanyId",
                table: "AccountingConfigurations",
                column: "AccountingCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingConfigurations_AccountingId",
                table: "AccountingConfigurations",
                column: "AccountingId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingConfigurations_CompanyId",
                table: "AccountingConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingConfigurations_DomainId",
                table: "AccountingConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingConfigurations_TenantId",
                table: "AccountingConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Banks_PosId",
                table: "Banks",
                column: "PosId");

            migrationBuilder.CreateIndex(
                name: "IX_Bins_BankId",
                table: "Bins",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandDiscountSettings_ApplicationId",
                table: "BrandDiscountSettings",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandDiscountSettings_BrandId",
                table: "BrandDiscountSettings",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandDiscountSettings_DomainId",
                table: "BrandDiscountSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandDiscountSettings_TenantId",
                table: "BrandDiscountSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Brands_DomainId",
                table: "Brands",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Brands_TenantId",
                table: "Brands",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_DomainId",
                table: "Categories",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryId",
                table: "Categories",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_TenantId",
                table: "Categories",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryContentTranslations_DomainId",
                table: "CategoryContentTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryContentTranslations_TenantId",
                table: "CategoryContentTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryDiscountSettings_ApplicationId",
                table: "CategoryDiscountSettings",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryDiscountSettings_CategoryId",
                table: "CategoryDiscountSettings",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryDiscountSettings_DomainId",
                table: "CategoryDiscountSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryDiscountSettings_ParentCategoryDiscountSettingId",
                table: "CategoryDiscountSettings",
                column: "ParentCategoryDiscountSettingId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryDiscountSettings_TenantId",
                table: "CategoryDiscountSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProducts_CategoryId",
                table: "CategoryProducts",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProducts_DomainId",
                table: "CategoryProducts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProducts_ProductId",
                table: "CategoryProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProducts_ProductInformationId",
                table: "CategoryProducts",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProducts_TenantId",
                table: "CategoryProducts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategorySeoTranslations_DomainId",
                table: "CategorySeoTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategorySeoTranslations_TenantId",
                table: "CategorySeoTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslationBreadcrumbs_DomainId",
                table: "CategoryTranslationBreadcrumbs",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslationBreadcrumbs_TenantId",
                table: "CategoryTranslationBreadcrumbs",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_CategoryId",
                table: "CategoryTranslations",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_DomainId",
                table: "CategoryTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_LanguageId",
                table: "CategoryTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_TenantId",
                table: "CategoryTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_CountryId",
                table: "Cities",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_DomainId",
                table: "Companies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_TenantId",
                table: "Companies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyConfigurations_CompanyId",
                table: "CompanyConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyConfigurations_DomainId",
                table: "CompanyConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyConfigurations_TenantId",
                table: "CompanyConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyContacts_DomainId",
                table: "CompanyContacts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyContacts_TenantId",
                table: "CompanyContacts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanySettings_DomainId",
                table: "CompanySettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanySettings_TenantId",
                table: "CompanySettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Configurations_DomainId",
                table: "Configurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Configurations_TenantId",
                table: "Configurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_DomainId",
                table: "Contacts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_TenantId",
                table: "Contacts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTranslations_ContactId",
                table: "ContactTranslations",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTranslations_DomainId",
                table: "ContactTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTranslations_LanguageId",
                table: "ContactTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTranslations_TenantId",
                table: "ContactTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Contents_DomainId",
                table: "Contents",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Contents_TenantId",
                table: "Contents",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSeoTranslations_DomainId",
                table: "ContentSeoTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSeoTranslations_TenantId",
                table: "ContentSeoTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslationContents_DomainId",
                table: "ContentTranslationContents",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslationContents_TenantId",
                table: "ContentTranslationContents",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslations_ContentId",
                table: "ContentTranslations",
                column: "ContentId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslations_DomainId",
                table: "ContentTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslations_LanguageId",
                table: "ContentTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslations_TenantId",
                table: "ContentTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddresses_CompanyId",
                table: "CustomerAddresses",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddresses_CustomerId",
                table: "CustomerAddresses",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddresses_DomainId",
                table: "CustomerAddresses",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddresses_NeighborhoodId",
                table: "CustomerAddresses",
                column: "NeighborhoodId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddresses_TenantId",
                table: "CustomerAddresses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerContacts_CompanyId",
                table: "CustomerContacts",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerContacts_DomainId",
                table: "CustomerContacts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerContacts_TenantId",
                table: "CustomerContacts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInvoiceInformations_CompanyId",
                table: "CustomerInvoiceInformations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInvoiceInformations_CustomerId",
                table: "CustomerInvoiceInformations",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInvoiceInformations_DomainId",
                table: "CustomerInvoiceInformations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInvoiceInformations_NeighborhoodId",
                table: "CustomerInvoiceInformations",
                column: "NeighborhoodId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInvoiceInformations_TenantId",
                table: "CustomerInvoiceInformations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerNotes_CompanyId",
                table: "CustomerNotes",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerNotes_CustomerId",
                table: "CustomerNotes",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerNotes_DomainId",
                table: "CustomerNotes",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerNotes_TenantId",
                table: "CustomerNotes",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CompanyId",
                table: "Customers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_DomainId",
                table: "Customers",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_TenantId",
                table: "Customers",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerUsers_CompanyId",
                table: "CustomerUsers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerUsers_CustomerRoleId",
                table: "CustomerUsers",
                column: "CustomerRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerUsers_DomainId",
                table: "CustomerUsers",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerUsers_TenantId",
                table: "CustomerUsers",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountCoupons_ApplicationId",
                table: "DiscountCoupons",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountCoupons_DomainId",
                table: "DiscountCoupons",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountCoupons_TenantId",
                table: "DiscountCoupons",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountCouponSettings_DomainId",
                table: "DiscountCouponSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountCouponSettings_TenantId",
                table: "DiscountCouponSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Districts_CityId",
                table: "Districts",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Domains_TenantId",
                table: "Domains",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_DomainSettings_TenantId",
                table: "DomainSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceBrands_CompanyId",
                table: "ECommerceBrands",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceBrands_DomainId",
                table: "ECommerceBrands",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceBrands_TenantId",
                table: "ECommerceBrands",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceCategories_CompanyId",
                table: "ECommerceCategories",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceCategories_DomainId",
                table: "ECommerceCategories",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceCategories_MarketplaceId",
                table: "ECommerceCategories",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceCategories_TenantId",
                table: "ECommerceCategories",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariants_CompanyId",
                table: "ECommerceVariants",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariants_DomainId",
                table: "ECommerceVariants",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariants_MarketplaceId",
                table: "ECommerceVariants",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariants_TenantId",
                table: "ECommerceVariants",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariantValues_CompanyId",
                table: "ECommerceVariantValues",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariantValues_DomainId",
                table: "ECommerceVariantValues",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ECommerceVariantValues_TenantId",
                table: "ECommerceVariantValues",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderCompanies_CompanyId",
                table: "EmailProviderCompanies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderCompanies_DomainId",
                table: "EmailProviderCompanies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderCompanies_EmailProviderId",
                table: "EmailProviderCompanies",
                column: "EmailProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderCompanies_TenantId",
                table: "EmailProviderCompanies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderConfigurations_CompanyId",
                table: "EmailProviderConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderConfigurations_DomainId",
                table: "EmailProviderConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderConfigurations_EmailProviderCompanyId",
                table: "EmailProviderConfigurations",
                column: "EmailProviderCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailProviderConfigurations_TenantId",
                table: "EmailProviderConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_EntireDiscounts_ApplicationId",
                table: "EntireDiscounts",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_EntireDiscounts_DomainId",
                table: "EntireDiscounts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_EntireDiscounts_TenantId",
                table: "EntireDiscounts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_EntireDiscountSettings_DomainId",
                table: "EntireDiscountSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_EntireDiscountSettings_TenantId",
                table: "EntireDiscountSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_CurrencyId",
                table: "ExchangeRates",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_CompanyId",
                table: "Expenses",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_DomainId",
                table: "Expenses",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ExpenseSourceId",
                table: "Expenses",
                column: "ExpenseSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_MarketplaceId",
                table: "Expenses",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_OrderSourceId",
                table: "Expenses",
                column: "OrderSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ProductInformationId",
                table: "Expenses",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_TenantId",
                table: "Expenses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ExpenseSources_TenantId",
                table: "ExpenseSources",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_FacebookConfigrations_DomainId",
                table: "FacebookConfigrations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_FacebookConfigrations_TenantId",
                table: "FacebookConfigrations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedDetails_DomainId",
                table: "FeedDetails",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedDetails_FeedId",
                table: "FeedDetails",
                column: "FeedId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedDetails_ProductInformationId",
                table: "FeedDetails",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedDetails_TenantId",
                table: "FeedDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_DomainId",
                table: "Feeds",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_FeedTemplateId",
                table: "Feeds",
                column: "FeedTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_TenantId",
                table: "Feeds",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestions_DomainId",
                table: "FrequentlyAskedQuestions",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestions_TenantId",
                table: "FrequentlyAskedQuestions",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestionTranslations_DomainId",
                table: "FrequentlyAskedQuestionTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestionTranslations_FrequentlyAskedQuestionId",
                table: "FrequentlyAskedQuestionTranslations",
                column: "FrequentlyAskedQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestionTranslations_LanguageId",
                table: "FrequentlyAskedQuestionTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_FrequentlyAskedQuestionTranslations_TenantId",
                table: "FrequentlyAskedQuestionTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_GetXPayYSettings_DomainId",
                table: "GetXPayYSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_GetXPayYSettings_TenantId",
                table: "GetXPayYSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_GoogleConfigrations_DomainId",
                table: "GoogleConfigrations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_GoogleConfigrations_TenantId",
                table: "GoogleConfigrations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Informations_DomainId",
                table: "Informations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Informations_TenantId",
                table: "Informations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationSeoTranslations_DomainId",
                table: "InformationSeoTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationSeoTranslations_TenantId",
                table: "InformationSeoTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslationContents_DomainId",
                table: "InformationTranslationContents",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslationContents_TenantId",
                table: "InformationTranslationContents",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslations_DomainId",
                table: "InformationTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslations_InformationId",
                table: "InformationTranslations",
                column: "InformationId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslations_LanguageId",
                table: "InformationTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_InformationTranslations_TenantId",
                table: "InformationTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Labels_DomainId",
                table: "Labels",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Labels_TenantId",
                table: "Labels",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_LabelTranslations_DomainId",
                table: "LabelTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_LabelTranslations_LabelId",
                table: "LabelTranslations",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_LabelTranslations_LanguageId",
                table: "LabelTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_LabelTranslations_TenantId",
                table: "LabelTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceBrandMappings_BrandId",
                table: "MarketplaceBrandMappings",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceBrandMappings_CompanyId",
                table: "MarketplaceBrandMappings",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceBrandMappings_DomainId",
                table: "MarketplaceBrandMappings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceBrandMappings_MarketplaceId",
                table: "MarketplaceBrandMappings",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceBrandMappings_TenantId",
                table: "MarketplaceBrandMappings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryMappings_CategoryId",
                table: "MarketplaceCategoryMappings",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryMappings_DomainId",
                table: "MarketplaceCategoryMappings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryMappings_TenantId",
                table: "MarketplaceCategoryMappings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCompanies_CompanyId",
                table: "MarketplaceCompanies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCompanies_DomainId",
                table: "MarketplaceCompanies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCompanies_MarketplaceId",
                table: "MarketplaceCompanies",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCompanies_TenantId",
                table: "MarketplaceCompanies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceConfigurations_CompanyId",
                table: "MarketPlaceConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceConfigurations_DomainId",
                table: "MarketPlaceConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceConfigurations_MarketplaceCompanyId",
                table: "MarketPlaceConfigurations",
                column: "MarketplaceCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceConfigurations_TenantId",
                table: "MarketPlaceConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceVariantValueMappings_DomainId",
                table: "MarketplaceVariantValueMappings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceVariantValueMappings_TenantId",
                table: "MarketplaceVariantValueMappings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceVariantValueMappings_VariantValueId",
                table: "MarketplaceVariantValueMappings",
                column: "VariantValueId");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyPoints_CustomerUserId",
                table: "MoneyPoints",
                column: "CustomerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyPoints_DomainId",
                table: "MoneyPoints",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyPoints_TenantId",
                table: "MoneyPoints",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyPointSettings_DomainId",
                table: "MoneyPointSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MoneyPointSettings_TenantId",
                table: "MoneyPointSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Neighborhoods_DistrictId",
                table: "Neighborhoods",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletinInformations_DomainId",
                table: "NewBulletinInformations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletinInformations_InformationId",
                table: "NewBulletinInformations",
                column: "InformationId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletinInformations_NewBulletinId",
                table: "NewBulletinInformations",
                column: "NewBulletinId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletinInformations_TenantId",
                table: "NewBulletinInformations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletins_DomainId",
                table: "NewBulletins",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NewBulletins_TenantId",
                table: "NewBulletins",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBillings_CompanyId",
                table: "OrderBillings",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBillings_DomainId",
                table: "OrderBillings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBillings_TenantId",
                table: "OrderBillings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDeliveryAddresses_CompanyId",
                table: "OrderDeliveryAddresses",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDeliveryAddresses_DomainId",
                table: "OrderDeliveryAddresses",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDeliveryAddresses_NeighborhoodId",
                table: "OrderDeliveryAddresses",
                column: "NeighborhoodId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDeliveryAddresses_TenantId",
                table: "OrderDeliveryAddresses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_CompanyId",
                table: "OrderDetails",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_DomainId",
                table: "OrderDetails",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_OrderId",
                table: "OrderDetails",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ProductInformationId",
                table: "OrderDetails",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_TenantId",
                table: "OrderDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInvoiceInformations_CompanyId",
                table: "OrderInvoiceInformations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInvoiceInformations_DomainId",
                table: "OrderInvoiceInformations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInvoiceInformations_NeighborhoodId",
                table: "OrderInvoiceInformations",
                column: "NeighborhoodId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInvoiceInformations_TenantId",
                table: "OrderInvoiceInformations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNotes_CompanyId",
                table: "OrderNotes",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNotes_DomainId",
                table: "OrderNotes",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNotes_OrderId",
                table: "OrderNotes",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderNotes_TenantId",
                table: "OrderNotes",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPackings_CompanyId",
                table: "OrderPackings",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPackings_DomainId",
                table: "OrderPackings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPackings_TenantId",
                table: "OrderPackings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPickings_CompanyId",
                table: "OrderPickings",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPickings_DomainId",
                table: "OrderPickings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPickings_TenantId",
                table: "OrderPickings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderReturnDetails_CompanyId",
                table: "OrderReturnDetails",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderReturnDetails_DomainId",
                table: "OrderReturnDetails",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderReturnDetails_OrderId",
                table: "OrderReturnDetails",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderReturnDetails_ProductInformationId",
                table: "OrderReturnDetails",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderReturnDetails_TenantId",
                table: "OrderReturnDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ApplicationId",
                table: "Orders",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CompanyId",
                table: "Orders",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CurrencyId",
                table: "Orders",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DomainId",
                table: "Orders",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_LanguageId",
                table: "Orders",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MarketplaceId",
                table: "Orders",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderSourceId",
                table: "Orders",
                column: "OrderSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderTypeId",
                table: "Orders",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_TenantId",
                table: "Orders",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipmentDetails_CompanyId",
                table: "OrderShipmentDetails",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipmentDetails_DomainId",
                table: "OrderShipmentDetails",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipmentDetails_OrderShipmentId",
                table: "OrderShipmentDetails",
                column: "OrderShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipmentDetails_TenantId",
                table: "OrderShipmentDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipments_CompanyId",
                table: "OrderShipments",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipments_DomainId",
                table: "OrderShipments",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipments_OrderId",
                table: "OrderShipments",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipments_ShipmentCompanyId",
                table: "OrderShipments",
                column: "ShipmentCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderShipments_TenantId",
                table: "OrderShipments",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderSmsses_CompanyId",
                table: "OrderSmsses",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderSmsses_DomainId",
                table: "OrderSmsses",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderSmsses_OrderId",
                table: "OrderSmsses",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderSmsses_TenantId",
                table: "OrderSmsses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderSources_TenantId",
                table: "OrderSources",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTechnicInformations_DomainId",
                table: "OrderTechnicInformations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTechnicInformations_TenantId",
                table: "OrderTechnicInformations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTypeTranslations_LanguageId",
                table: "OrderTypeTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTypeTranslations_OrderTypeId",
                table: "OrderTypeTranslations",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderViewings_CompanyId",
                table: "OrderViewings",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderViewings_DomainId",
                table: "OrderViewings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderViewings_OrderId",
                table: "OrderViewings",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderViewings_TenantId",
                table: "OrderViewings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentLogs_DomainId",
                table: "PaymentLogs",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentLogs_TenantId",
                table: "PaymentLogs",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_DiscountCouponId",
                table: "Payments",
                column: "DiscountCouponId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_DomainId",
                table: "Payments",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_OrderId",
                table: "Payments",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_PaymentTypeId",
                table: "Payments",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_TenantId",
                table: "Payments",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeDomains_DomainId",
                table: "PaymentTypeDomains",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeDomains_PaymentTypeId",
                table: "PaymentTypeDomains",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeDomains_TenantId",
                table: "PaymentTypeDomains",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeTranslations_LanguageId",
                table: "PaymentTypeTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeTranslations_PaymentTypeId",
                table: "PaymentTypeTranslations",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PosConfigurations_DomainId",
                table: "PosConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PosConfigurations_PosId",
                table: "PosConfigurations",
                column: "PosId");

            migrationBuilder.CreateIndex(
                name: "IX_PosConfigurations_TenantId",
                table: "PosConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationCombines_ContainProductInformationId",
                table: "ProductInformationCombines",
                column: "ContainProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationCombines_DomainId",
                table: "ProductInformationCombines",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationCombines_ProductInformationId",
                table: "ProductInformationCombines",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationCombines_TenantId",
                table: "ProductInformationCombines",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationComments_DomainId",
                table: "ProductInformationComments",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationComments_ProductInformationId",
                table: "ProductInformationComments",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationComments_TenantId",
                table: "ProductInformationComments",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationContentTranslations_DomainId",
                table: "ProductInformationContentTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationContentTranslations_TenantId",
                table: "ProductInformationContentTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceBulkPrices_CompanyId",
                table: "ProductInformationMarketplaceBulkPrices",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceBulkPrices_DomainId",
                table: "ProductInformationMarketplaceBulkPrices",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceBulkPrices_TenantId",
                table: "ProductInformationMarketplaceBulkPrices",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaces_CompanyId",
                table: "ProductInformationMarketplaces",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaces_DomainId",
                table: "ProductInformationMarketplaces",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaces_ProductInformationId",
                table: "ProductInformationMarketplaces",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaces_TenantId",
                table: "ProductInformationMarketplaces",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceVariantValues_CompanyId",
                table: "ProductInformationMarketplaceVariantValues",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceVariantValues_DomainId",
                table: "ProductInformationMarketplaceVariantValues",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceVariantValues_ProductInformationMarketplaceId",
                table: "ProductInformationMarketplaceVariantValues",
                column: "ProductInformationMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationMarketplaceVariantValues_TenantId",
                table: "ProductInformationMarketplaceVariantValues",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPhotos_DomainId",
                table: "ProductInformationPhotos",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPhotos_ProductInformationId",
                table: "ProductInformationPhotos",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPhotos_TenantId",
                table: "ProductInformationPhotos",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPriceses_ConversionCurrencyId",
                table: "ProductInformationPriceses",
                column: "ConversionCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPriceses_CurrencyId",
                table: "ProductInformationPriceses",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPriceses_DomainId",
                table: "ProductInformationPriceses",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPriceses_ProductInformationId",
                table: "ProductInformationPriceses",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationPriceses_TenantId",
                table: "ProductInformationPriceses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformations_DomainId",
                table: "ProductInformations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformations_ProductId",
                table: "ProductInformations",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformations_TenantId",
                table: "ProductInformations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationSeoTranslations_DomainId",
                table: "ProductInformationSeoTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationSeoTranslations_TenantId",
                table: "ProductInformationSeoTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationSubscriptions_DomainId",
                table: "ProductInformationSubscriptions",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationSubscriptions_ProductInformationId",
                table: "ProductInformationSubscriptions",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationSubscriptions_TenantId",
                table: "ProductInformationSubscriptions",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslationBreadcrumbs_DomainId",
                table: "ProductInformationTranslationBreadcrumbs",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslationBreadcrumbs_TenantId",
                table: "ProductInformationTranslationBreadcrumbs",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslations_DomainId",
                table: "ProductInformationTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslations_LanguageId",
                table: "ProductInformationTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslations_ProductInformationId",
                table: "ProductInformationTranslations",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationTranslations_TenantId",
                table: "ProductInformationTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationVariants_DomainId",
                table: "ProductInformationVariants",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationVariants_ProductInformationId",
                table: "ProductInformationVariants",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationVariants_TenantId",
                table: "ProductInformationVariants",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductInformationVariants_VariantValueId",
                table: "ProductInformationVariants",
                column: "VariantValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductLabels_DomainId",
                table: "ProductLabels",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductLabels_LabelId",
                table: "ProductLabels",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductLabels_ProductId",
                table: "ProductLabels",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductLabels_TenantId",
                table: "ProductLabels",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMarketplaces_CompanyId",
                table: "ProductMarketplaces",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMarketplaces_DomainId",
                table: "ProductMarketplaces",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMarketplaces_ProductId",
                table: "ProductMarketplaces",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMarketplaces_TenantId",
                table: "ProductMarketplaces",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductProperties_DomainId",
                table: "ProductProperties",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductProperties_ProductId",
                table: "ProductProperties",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductProperties_PropertyValueId",
                table: "ProductProperties",
                column: "PropertyValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductProperties_TenantId",
                table: "ProductProperties",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_BrandId",
                table: "Products",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_DomainId",
                table: "Products",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductSourceDomainId",
                table: "Products",
                column: "ProductSourceDomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SupplierId",
                table: "Products",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_TenantId",
                table: "Products",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceConfigurations_DomainId",
                table: "ProductSourceConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceConfigurations_ProductSourceDomainId",
                table: "ProductSourceConfigurations",
                column: "ProductSourceDomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceConfigurations_TenantId",
                table: "ProductSourceConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceDomains_DomainId",
                table: "ProductSourceDomains",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceDomains_ProductSourceId",
                table: "ProductSourceDomains",
                column: "ProductSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceDomains_TenantId",
                table: "ProductSourceDomains",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceMappings_DomainId",
                table: "ProductSourceMappings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceMappings_ProductSourceDomainId",
                table: "ProductSourceMappings",
                column: "ProductSourceDomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSourceMappings_TenantId",
                table: "ProductSourceMappings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTags_DomainId",
                table: "ProductTags",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTags_ProductId",
                table: "ProductTags",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTags_TagId",
                table: "ProductTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTags_TenantId",
                table: "ProductTags",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslations_DomainId",
                table: "ProductTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslations_LanguageId",
                table: "ProductTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslations_ProductId",
                table: "ProductTranslations",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslations_TenantId",
                table: "ProductTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_DomainId",
                table: "Properties",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Properties_TenantId",
                table: "Properties",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyTranslations_DomainId",
                table: "PropertyTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyTranslations_LanguageId",
                table: "PropertyTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyTranslations_PropertyId",
                table: "PropertyTranslations",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyTranslations_TenantId",
                table: "PropertyTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValues_DomainId",
                table: "PropertyValues",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValues_PropertyId",
                table: "PropertyValues",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValues_TenantId",
                table: "PropertyValues",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValueTranslations_DomainId",
                table: "PropertyValueTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValueTranslations_LanguageId",
                table: "PropertyValueTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValueTranslations_PropertyValueId",
                table: "PropertyValueTranslations",
                column: "PropertyValueId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValueTranslations_TenantId",
                table: "PropertyValueTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyCompanies_CompanyId",
                table: "ShipmentCompanyCompanies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyCompanies_DomainId",
                table: "ShipmentCompanyCompanies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyCompanies_ShipmentCompanyId",
                table: "ShipmentCompanyCompanies",
                column: "ShipmentCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyCompanies_TenantId",
                table: "ShipmentCompanyCompanies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyConfigurations_CompanyId",
                table: "ShipmentCompanyConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyConfigurations_DomainId",
                table: "ShipmentCompanyConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyConfigurations_ShipmentCompanyCompanyId",
                table: "ShipmentCompanyConfigurations",
                column: "ShipmentCompanyCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentCompanyConfigurations_TenantId",
                table: "ShipmentCompanyConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProducts_ApplicationId",
                table: "ShoppingCartDiscountedProducts",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProducts_DomainId",
                table: "ShoppingCartDiscountedProducts",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProducts_ProductId",
                table: "ShoppingCartDiscountedProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProducts_TenantId",
                table: "ShoppingCartDiscountedProducts",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProductSettings_DomainId",
                table: "ShoppingCartDiscountedProductSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedProductSettings_TenantId",
                table: "ShoppingCartDiscountedProductSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscounteds_ApplicationId",
                table: "ShoppingCartDiscounteds",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscounteds_DomainId",
                table: "ShoppingCartDiscounteds",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscounteds_TenantId",
                table: "ShoppingCartDiscounteds",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedSettings_DomainId",
                table: "ShoppingCartDiscountedSettings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartDiscountedSettings_TenantId",
                table: "ShoppingCartDiscountedSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Showcases_BrandId",
                table: "Showcases",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Showcases_CategoryId",
                table: "Showcases",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Showcases_DomainId",
                table: "Showcases",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Showcases_LanguageId",
                table: "Showcases",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Showcases_TenantId",
                table: "Showcases",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderCompanies_CompanyId",
                table: "SmsProviderCompanies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderCompanies_DomainId",
                table: "SmsProviderCompanies",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderCompanies_SmsProviderId",
                table: "SmsProviderCompanies",
                column: "SmsProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderCompanies_TenantId",
                table: "SmsProviderCompanies",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderConfigurations_CompanyId",
                table: "SmsProviderConfigurations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderConfigurations_DomainId",
                table: "SmsProviderConfigurations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderConfigurations_SmsProviderCompanyId",
                table: "SmsProviderConfigurations",
                column: "SmsProviderCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsProviderConfigurations_TenantId",
                table: "SmsProviderConfigurations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsTemplates_CompanyId",
                table: "SmsTemplates",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsTemplates_DomainId",
                table: "SmsTemplates",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsTemplates_OrderTypeId",
                table: "SmsTemplates",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsTemplates_PaymentTypeId",
                table: "SmsTemplates",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsTemplates_TenantId",
                table: "SmsTemplates",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_DomainId",
                table: "Suppliers",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_TenantId",
                table: "Suppliers",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_DomainId",
                table: "Tags",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_TenantId",
                table: "Tags",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_TagTranslations_DomainId",
                table: "TagTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_TagTranslations_LanguageId",
                table: "TagTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_TagTranslations_TagId",
                table: "TagTranslations",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_TagTranslations_TenantId",
                table: "TagTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantPayments_TenantId",
                table: "TenantPayments",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_TwoFactorCustomerUsers_CustomerUserId",
                table: "TwoFactorCustomerUsers",
                column: "CustomerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TwoFactorCustomerUsers_DomainId",
                table: "TwoFactorCustomerUsers",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_TwoFactorCustomerUsers_TenantId",
                table: "TwoFactorCustomerUsers",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_UpsAreas_ParentUpsAreaId",
                table: "UpsAreas",
                column: "ParentUpsAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_UpsAreas_UpsCityId",
                table: "UpsAreas",
                column: "UpsCityId");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_DomainId",
                table: "Variants",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_TenantId",
                table: "Variants",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantTranslations_DomainId",
                table: "VariantTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantTranslations_LanguageId",
                table: "VariantTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantTranslations_TenantId",
                table: "VariantTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantTranslations_VariantId",
                table: "VariantTranslations",
                column: "VariantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValues_DomainId",
                table: "VariantValues",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValues_TenantId",
                table: "VariantValues",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValues_VariantId",
                table: "VariantValues",
                column: "VariantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValueTranslations_DomainId",
                table: "VariantValueTranslations",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValueTranslations_LanguageId",
                table: "VariantValueTranslations",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValueTranslations_TenantId",
                table: "VariantValueTranslations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_VariantValueTranslations_VariantValueId",
                table: "VariantValueTranslations",
                column: "VariantValueId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountingConfigurations");

            migrationBuilder.DropTable(
                name: "Bins");

            migrationBuilder.DropTable(
                name: "BrandDiscountSettings");

            migrationBuilder.DropTable(
                name: "CategoryContentTranslations");

            migrationBuilder.DropTable(
                name: "CategoryDiscountSettings");

            migrationBuilder.DropTable(
                name: "CategoryProducts");

            migrationBuilder.DropTable(
                name: "CategorySeoTranslations");

            migrationBuilder.DropTable(
                name: "CategoryTranslationBreadcrumbs");

            migrationBuilder.DropTable(
                name: "CompanyConfigurations");

            migrationBuilder.DropTable(
                name: "CompanyContacts");

            migrationBuilder.DropTable(
                name: "CompanySettings");

            migrationBuilder.DropTable(
                name: "Configurations");

            migrationBuilder.DropTable(
                name: "ContactTranslations");

            migrationBuilder.DropTable(
                name: "ContentSeoTranslations");

            migrationBuilder.DropTable(
                name: "ContentTranslationContents");

            migrationBuilder.DropTable(
                name: "CustomerAddresses");

            migrationBuilder.DropTable(
                name: "CustomerContacts");

            migrationBuilder.DropTable(
                name: "CustomerInvoiceInformations");

            migrationBuilder.DropTable(
                name: "CustomerNotes");

            migrationBuilder.DropTable(
                name: "DiscountCouponSettings");

            migrationBuilder.DropTable(
                name: "DomainSettings");

            migrationBuilder.DropTable(
                name: "ECommerceBrands");

            migrationBuilder.DropTable(
                name: "ECommerceCategories");

            migrationBuilder.DropTable(
                name: "ECommerceVariants");

            migrationBuilder.DropTable(
                name: "ECommerceVariantValues");

            migrationBuilder.DropTable(
                name: "EmailProviderConfigurations");

            migrationBuilder.DropTable(
                name: "EntireDiscountSettings");

            migrationBuilder.DropTable(
                name: "ExchangeRates");

            migrationBuilder.DropTable(
                name: "Expenses");

            migrationBuilder.DropTable(
                name: "FacebookConfigrations");

            migrationBuilder.DropTable(
                name: "FeedDetails");

            migrationBuilder.DropTable(
                name: "FrequentlyAskedQuestionTranslations");

            migrationBuilder.DropTable(
                name: "GetXPayYSettings");

            migrationBuilder.DropTable(
                name: "GoogleConfigrations");

            migrationBuilder.DropTable(
                name: "InformationSeoTranslations");

            migrationBuilder.DropTable(
                name: "InformationTranslationContents");

            migrationBuilder.DropTable(
                name: "LabelTranslations");

            migrationBuilder.DropTable(
                name: "MarketplaceBrandMappings");

            migrationBuilder.DropTable(
                name: "MarketplaceCategoryMappings");

            migrationBuilder.DropTable(
                name: "MarketPlaceConfigurations");

            migrationBuilder.DropTable(
                name: "MarketplaceRequestTypes");

            migrationBuilder.DropTable(
                name: "MarketplaceVariantValueMappings");

            migrationBuilder.DropTable(
                name: "MoneyPoints");

            migrationBuilder.DropTable(
                name: "MoneyPointSettings");

            migrationBuilder.DropTable(
                name: "NewBulletinInformations");

            migrationBuilder.DropTable(
                name: "OrderBillings");

            migrationBuilder.DropTable(
                name: "OrderDeliveryAddresses");

            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "OrderInvoiceInformations");

            migrationBuilder.DropTable(
                name: "OrderNotes");

            migrationBuilder.DropTable(
                name: "OrderPackings");

            migrationBuilder.DropTable(
                name: "OrderPickings");

            migrationBuilder.DropTable(
                name: "OrderReturnDetails");

            migrationBuilder.DropTable(
                name: "OrderShipmentDetails");

            migrationBuilder.DropTable(
                name: "OrderSmsses");

            migrationBuilder.DropTable(
                name: "OrderTechnicInformations");

            migrationBuilder.DropTable(
                name: "OrderTypeTranslations");

            migrationBuilder.DropTable(
                name: "OrderViewings");

            migrationBuilder.DropTable(
                name: "PaymentLogs");

            migrationBuilder.DropTable(
                name: "PaymentTypeSettings");

            migrationBuilder.DropTable(
                name: "PaymentTypeTranslations");

            migrationBuilder.DropTable(
                name: "PosConfigurations");

            migrationBuilder.DropTable(
                name: "ProductInformationCombines");

            migrationBuilder.DropTable(
                name: "ProductInformationComments");

            migrationBuilder.DropTable(
                name: "ProductInformationContentTranslations");

            migrationBuilder.DropTable(
                name: "ProductInformationMarketplaceBulkPrices");

            migrationBuilder.DropTable(
                name: "ProductInformationMarketplaceVariantValues");

            migrationBuilder.DropTable(
                name: "ProductInformationPhotos");

            migrationBuilder.DropTable(
                name: "ProductInformationPriceses");

            migrationBuilder.DropTable(
                name: "ProductInformationSeoTranslations");

            migrationBuilder.DropTable(
                name: "ProductInformationSubscriptions");

            migrationBuilder.DropTable(
                name: "ProductInformationTranslationBreadcrumbs");

            migrationBuilder.DropTable(
                name: "ProductInformationVariants");

            migrationBuilder.DropTable(
                name: "ProductLabels");

            migrationBuilder.DropTable(
                name: "ProductMarketplaces");

            migrationBuilder.DropTable(
                name: "ProductProperties");

            migrationBuilder.DropTable(
                name: "ProductSourceConfigurations");

            migrationBuilder.DropTable(
                name: "ProductSourceMappings");

            migrationBuilder.DropTable(
                name: "ProductTags");

            migrationBuilder.DropTable(
                name: "ProductTranslations");

            migrationBuilder.DropTable(
                name: "PropertyTranslations");

            migrationBuilder.DropTable(
                name: "PropertyValueTranslations");

            migrationBuilder.DropTable(
                name: "ShipmentCompanyConfigurations");

            migrationBuilder.DropTable(
                name: "ShoppingCartDiscountedProductSettings");

            migrationBuilder.DropTable(
                name: "ShoppingCartDiscountedSettings");

            migrationBuilder.DropTable(
                name: "Showcases");

            migrationBuilder.DropTable(
                name: "SmsProviderConfigurations");

            migrationBuilder.DropTable(
                name: "SmsTemplates");

            migrationBuilder.DropTable(
                name: "TagTranslations");

            migrationBuilder.DropTable(
                name: "TenantInvoiceInformations");

            migrationBuilder.DropTable(
                name: "TenantPayments");

            migrationBuilder.DropTable(
                name: "TenantSalesAmounts");

            migrationBuilder.DropTable(
                name: "TenantSettings");

            migrationBuilder.DropTable(
                name: "TwoFactorCustomerUsers");

            migrationBuilder.DropTable(
                name: "UpsAreas");

            migrationBuilder.DropTable(
                name: "VariantTranslations");

            migrationBuilder.DropTable(
                name: "VariantValueTranslations");

            migrationBuilder.DropTable(
                name: "AccountingCompanies");

            migrationBuilder.DropTable(
                name: "Banks");

            migrationBuilder.DropTable(
                name: "CategoryTranslations");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "ContentTranslations");

            migrationBuilder.DropTable(
                name: "EmailProviderCompanies");

            migrationBuilder.DropTable(
                name: "EntireDiscounts");

            migrationBuilder.DropTable(
                name: "ExpenseSources");

            migrationBuilder.DropTable(
                name: "Feeds");

            migrationBuilder.DropTable(
                name: "FrequentlyAskedQuestions");

            migrationBuilder.DropTable(
                name: "InformationTranslations");

            migrationBuilder.DropTable(
                name: "MarketplaceCompanies");

            migrationBuilder.DropTable(
                name: "NewBulletins");

            migrationBuilder.DropTable(
                name: "Neighborhoods");

            migrationBuilder.DropTable(
                name: "OrderShipments");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "PaymentTypeDomains");

            migrationBuilder.DropTable(
                name: "ProductInformationMarketplaces");

            migrationBuilder.DropTable(
                name: "ProductInformationTranslations");

            migrationBuilder.DropTable(
                name: "Labels");

            migrationBuilder.DropTable(
                name: "PropertyValues");

            migrationBuilder.DropTable(
                name: "ShipmentCompanyCompanies");

            migrationBuilder.DropTable(
                name: "ShoppingCartDiscountedProducts");

            migrationBuilder.DropTable(
                name: "ShoppingCartDiscounteds");

            migrationBuilder.DropTable(
                name: "SmsProviderCompanies");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "CustomerUsers");

            migrationBuilder.DropTable(
                name: "UpsCities");

            migrationBuilder.DropTable(
                name: "VariantValues");

            migrationBuilder.DropTable(
                name: "Accountings");

            migrationBuilder.DropTable(
                name: "Posses");

            migrationBuilder.DropTable(
                name: "Contents");

            migrationBuilder.DropTable(
                name: "EmailProviders");

            migrationBuilder.DropTable(
                name: "FeedTemplates");

            migrationBuilder.DropTable(
                name: "Informations");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "DiscountCoupons");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "PaymentTypes");

            migrationBuilder.DropTable(
                name: "ProductInformations");

            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "ShipmentCompanies");

            migrationBuilder.DropTable(
                name: "SmsProviders");

            migrationBuilder.DropTable(
                name: "CustomerRoles");

            migrationBuilder.DropTable(
                name: "Variants");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Marketplaces");

            migrationBuilder.DropTable(
                name: "OrderSources");

            migrationBuilder.DropTable(
                name: "OrderTypes");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "ProductSourceDomains");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "ProductSources");

            migrationBuilder.DropTable(
                name: "Domains");

            migrationBuilder.DropTable(
                name: "Tenants");
        }
    }
}
