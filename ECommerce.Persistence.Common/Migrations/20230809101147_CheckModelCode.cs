﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class CheckModelCode : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CheckModelCode",
                table: "Stocktakings",
                newName: "CheckGroup");

            migrationBuilder.AddColumn<string>(
                name: "ModelCode",
                table: "Stocktakings",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModelCode",
                table: "Stocktakings");

            migrationBuilder.RenameColumn(
                name: "CheckGroup",
                table: "Stocktakings",
                newName: "CheckModelCode");
        }
    }
}
