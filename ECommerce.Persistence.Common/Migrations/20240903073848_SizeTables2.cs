﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class SizeTables2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SizeTableGenerations");

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "SizeTables",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Generated",
                table: "SizeTables",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_SizeTables_ProductId",
                table: "SizeTables",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_SizeTables_Products_ProductId",
                table: "SizeTables",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SizeTables_Products_ProductId",
                table: "SizeTables");

            migrationBuilder.DropIndex(
                name: "IX_SizeTables_ProductId",
                table: "SizeTables");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "SizeTables");

            migrationBuilder.DropColumn(
                name: "Generated",
                table: "SizeTables");

            migrationBuilder.CreateTable(
                name: "SizeTableGenerations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Generated = table.Column<bool>(type: "bit", nullable: false),
                    SkuCode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SizeTableGenerations", x => x.Id);
                });
        }
    }
}
