﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class Financial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "OrderShipments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "OrderInvoiceInformations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "OrderDeliveryAddresses",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "Customers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "CustomerContacts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "MarketplaceCargoFee",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Desi = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceCargoFee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceCargoFee_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCargoFee_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCargoFee_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketplaceCargoFee_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MarketplaceCommission",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderDetailId = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Rate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceCommission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceCommission_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCommission_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCommission_OrderDetails_OrderDetailId",
                        column: x => x.OrderDetailId,
                        principalTable: "OrderDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketplaceCommission_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCargoFee_CompanyId",
                table: "MarketplaceCargoFee",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCargoFee_DomainId",
                table: "MarketplaceCargoFee",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCargoFee_OrderId",
                table: "MarketplaceCargoFee",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCargoFee_TenantId",
                table: "MarketplaceCargoFee",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCommission_CompanyId",
                table: "MarketplaceCommission",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCommission_DomainId",
                table: "MarketplaceCommission",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCommission_OrderDetailId",
                table: "MarketplaceCommission",
                column: "OrderDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCommission_TenantId",
                table: "MarketplaceCommission",
                column: "TenantId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MarketplaceCargoFee");

            migrationBuilder.DropTable(
                name: "MarketplaceCommission");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "OrderShipments");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "OrderInvoiceInformations");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "OrderDeliveryAddresses");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "CustomerContacts");
        }
    }
}
