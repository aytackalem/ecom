﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class XmlDefinationBrand : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "XmlDefinationBrands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    XmlDefinationId = table.Column<int>(type: "int", nullable: false),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_XmlDefinationBrands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_XmlDefinationBrands_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_XmlDefinationBrands_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_XmlDefinationBrands_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_XmlDefinationBrands_XmlDefinations_XmlDefinationId",
                        column: x => x.XmlDefinationId,
                        principalTable: "XmlDefinations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_XmlDefinationBrands_BrandId",
                table: "XmlDefinationBrands",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_XmlDefinationBrands_DomainId",
                table: "XmlDefinationBrands",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_XmlDefinationBrands_TenantId",
                table: "XmlDefinationBrands",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_XmlDefinationBrands_XmlDefinationId",
                table: "XmlDefinationBrands",
                column: "XmlDefinationId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "XmlDefinationBrands");
        }
    }
}
