﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class NebimMarketplaceCreditCard : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NebimMarketplaceCreditCardCodes_PaymentTypes_PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes");

            migrationBuilder.DropIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes");

            migrationBuilder.DropColumn(
                name: "PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "PaymentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimMarketplaceCreditCardCodes_PaymentTypes_PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "PaymentTypeId",
                principalTable: "PaymentTypes",
                principalColumn: "Id");
        }
    }
}
