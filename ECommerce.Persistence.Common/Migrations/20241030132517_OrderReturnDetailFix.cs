﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class OrderReturnDetailFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReturnNumber",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "OrderDetails");

            migrationBuilder.AddColumn<string>(
                name: "ReturnDescription",
                table: "OrderReturnDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnNumber",
                table: "OrderReturnDetails",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReturnDescription",
                table: "OrderReturnDetails");

            migrationBuilder.DropColumn(
                name: "ReturnNumber",
                table: "OrderReturnDetails");

            migrationBuilder.AddColumn<string>(
                name: "ReturnNumber",
                table: "OrderDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "OrderDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
