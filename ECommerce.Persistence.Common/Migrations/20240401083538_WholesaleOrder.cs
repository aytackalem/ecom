﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class WholesaleOrder : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WholesaleOrderTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholesaleOrderTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WholesaleOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WholesaleOrderTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholesaleOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholesaleOrders_WholesaleOrderTypes_WholesaleOrderTypeId",
                        column: x => x.WholesaleOrderTypeId,
                        principalTable: "WholesaleOrderTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WholesaleOrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WholesaleOrderId = table.Column<int>(type: "int", nullable: false),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholesaleOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholesaleOrderDetails_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WholesaleOrderDetails_WholesaleOrders_WholesaleOrderId",
                        column: x => x.WholesaleOrderId,
                        principalTable: "WholesaleOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleOrderDetails_ProductInformationId",
                table: "WholesaleOrderDetails",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleOrderDetails_WholesaleOrderId",
                table: "WholesaleOrderDetails",
                column: "WholesaleOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleOrders_WholesaleOrderTypeId",
                table: "WholesaleOrders",
                column: "WholesaleOrderTypeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholesaleOrderDetails");

            migrationBuilder.DropTable(
                name: "WholesaleOrders");

            migrationBuilder.DropTable(
                name: "WholesaleOrderTypes");
        }
    }
}
