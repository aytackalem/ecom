﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class ProductGenericRepository2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperty_Domains_DomainId",
                table: "ProductGenericProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperty_Products_ProductId",
                table: "ProductGenericProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperty_Tenants_TenantId",
                table: "ProductGenericProperty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGenericProperty",
                table: "ProductGenericProperty");

            migrationBuilder.RenameTable(
                name: "ProductGenericProperty",
                newName: "ProductGenericProperties");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperty_TenantId",
                table: "ProductGenericProperties",
                newName: "IX_ProductGenericProperties_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperty_ProductId",
                table: "ProductGenericProperties",
                newName: "IX_ProductGenericProperties_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperty_DomainId",
                table: "ProductGenericProperties",
                newName: "IX_ProductGenericProperties_DomainId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGenericProperties",
                table: "ProductGenericProperties",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperties_Domains_DomainId",
                table: "ProductGenericProperties",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperties_Products_ProductId",
                table: "ProductGenericProperties",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperties_Tenants_TenantId",
                table: "ProductGenericProperties",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperties_Domains_DomainId",
                table: "ProductGenericProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperties_Products_ProductId",
                table: "ProductGenericProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGenericProperties_Tenants_TenantId",
                table: "ProductGenericProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGenericProperties",
                table: "ProductGenericProperties");

            migrationBuilder.RenameTable(
                name: "ProductGenericProperties",
                newName: "ProductGenericProperty");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperties_TenantId",
                table: "ProductGenericProperty",
                newName: "IX_ProductGenericProperty_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperties_ProductId",
                table: "ProductGenericProperty",
                newName: "IX_ProductGenericProperty_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGenericProperties_DomainId",
                table: "ProductGenericProperty",
                newName: "IX_ProductGenericProperty_DomainId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGenericProperty",
                table: "ProductGenericProperty",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperty_Domains_DomainId",
                table: "ProductGenericProperty",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperty_Products_ProductId",
                table: "ProductGenericProperty",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGenericProperty_Tenants_TenantId",
                table: "ProductGenericProperty",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
