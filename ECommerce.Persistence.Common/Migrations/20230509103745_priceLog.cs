﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class priceLog : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductInformationMarketplacePriceLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductInformationMarketplaceId = table.Column<int>(type: "int", nullable: false),
                    CreatedDateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationMarketplacePriceLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductInformationMarketplaceStockLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductInformationMarketplaceId = table.Column<int>(type: "int", nullable: false),
                    CreatedDateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInformationMarketplaceStockLogs", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "IK",
                column: "BrandAutocomplete",
                value: true);

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "PA",
                column: "BrandAutocomplete",
                value: true);

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "TY",
                column: "BrandAutocomplete",
                value: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductInformationMarketplacePriceLogs");

            migrationBuilder.DropTable(
                name: "ProductInformationMarketplaceStockLogs");

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "IK",
                column: "BrandAutocomplete",
                value: false);

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "PA",
                column: "BrandAutocomplete",
                value: false);

            migrationBuilder.UpdateData(
                table: "Marketplaces",
                keyColumn: "Id",
                keyValue: "TY",
                column: "BrandAutocomplete",
                value: false);
        }
    }
}
