﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class NebimMarketplace : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BankId",
                table: "Payments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Micro",
                table: "NebimOrders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "NebimMarketplaceCreditCardCodes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    PaymentTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    BankId = table.Column<int>(type: "int", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NebimMarketplaceCreditCardCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceCreditCardCodes_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceCreditCardCodes_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceCreditCardCodes_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceCreditCardCodes_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceCreditCardCodes_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NebimMarketplaceSalesPersonnelCodes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NebimMarketplaceSalesPersonnelCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceSalesPersonnelCodes_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceSalesPersonnelCodes_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceSalesPersonnelCodes_Marketplaces_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "Marketplaces",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimMarketplaceSalesPersonnelCodes_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_CompanyId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_DomainId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_MarketplaceId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_PaymentTypeId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceCreditCardCodes_TenantId",
                table: "NebimMarketplaceCreditCardCodes",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceSalesPersonnelCodes_CompanyId",
                table: "NebimMarketplaceSalesPersonnelCodes",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceSalesPersonnelCodes_DomainId",
                table: "NebimMarketplaceSalesPersonnelCodes",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceSalesPersonnelCodes_MarketplaceId",
                table: "NebimMarketplaceSalesPersonnelCodes",
                column: "MarketplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimMarketplaceSalesPersonnelCodes_TenantId",
                table: "NebimMarketplaceSalesPersonnelCodes",
                column: "TenantId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NebimMarketplaceCreditCardCodes");

            migrationBuilder.DropTable(
                name: "NebimMarketplaceSalesPersonnelCodes");

            migrationBuilder.DropColumn(
                name: "BankId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "Micro",
                table: "NebimOrders");
        }
    }
}
