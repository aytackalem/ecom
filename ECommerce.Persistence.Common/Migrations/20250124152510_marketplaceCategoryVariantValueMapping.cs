﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class marketplaceCategoryVariantValueMapping : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MarketplaceCategoryVariantValueMappings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketplaceCategoryMappingId = table.Column<int>(type: "int", nullable: false),
                    MarketplaceVariantCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MarketplaceVariantValueName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AllowCustom = table.Column<bool>(type: "bit", nullable: false),
                    Mandatory = table.Column<bool>(type: "bit", nullable: false),
                    Varianter = table.Column<bool>(type: "bit", nullable: false),
                    Multiple = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceCategoryVariantValueMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryVariantValueMappings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryVariantValueMappings_MarketplaceCategoryMappings_MarketplaceCategoryMappingId",
                        column: x => x.MarketplaceCategoryMappingId,
                        principalTable: "MarketplaceCategoryMappings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketplaceCategoryVariantValueMappings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryVariantValueMappings_DomainId",
                table: "MarketplaceCategoryVariantValueMappings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryVariantValueMappings_MarketplaceCategoryMappingId",
                table: "MarketplaceCategoryVariantValueMappings",
                column: "MarketplaceCategoryMappingId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceCategoryVariantValueMappings_TenantId",
                table: "MarketplaceCategoryVariantValueMappings",
                column: "TenantId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MarketplaceCategoryVariantValueMappings");
        }
    }
}
