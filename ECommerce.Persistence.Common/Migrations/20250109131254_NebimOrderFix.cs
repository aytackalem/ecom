﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class NebimOrderFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrder_Companies_CompanyId",
                table: "NebimOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrder_Domains_DomainId",
                table: "NebimOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrder_Orders_Id",
                table: "NebimOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrder_Tenants_TenantId",
                table: "NebimOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetail_Companies_CompanyId",
                table: "NebimOrderDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetail_Domains_DomainId",
                table: "NebimOrderDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetail_NebimOrder_NebimOrderId",
                table: "NebimOrderDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetail_OrderDetails_Id",
                table: "NebimOrderDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetail_Tenants_TenantId",
                table: "NebimOrderDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NebimOrderDetail",
                table: "NebimOrderDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NebimOrder",
                table: "NebimOrder");

            migrationBuilder.RenameTable(
                name: "NebimOrderDetail",
                newName: "NebimOrderDetails");

            migrationBuilder.RenameTable(
                name: "NebimOrder",
                newName: "NebimOrders");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetail_TenantId",
                table: "NebimOrderDetails",
                newName: "IX_NebimOrderDetails_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetail_NebimOrderId",
                table: "NebimOrderDetails",
                newName: "IX_NebimOrderDetails_NebimOrderId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetail_DomainId",
                table: "NebimOrderDetails",
                newName: "IX_NebimOrderDetails_DomainId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetail_CompanyId",
                table: "NebimOrderDetails",
                newName: "IX_NebimOrderDetails_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrder_TenantId",
                table: "NebimOrders",
                newName: "IX_NebimOrders_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrder_DomainId",
                table: "NebimOrders",
                newName: "IX_NebimOrders_DomainId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrder_CompanyId",
                table: "NebimOrders",
                newName: "IX_NebimOrders_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NebimOrderDetails",
                table: "NebimOrderDetails",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NebimOrders",
                table: "NebimOrders",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetails_Companies_CompanyId",
                table: "NebimOrderDetails",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetails_Domains_DomainId",
                table: "NebimOrderDetails",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetails_NebimOrders_NebimOrderId",
                table: "NebimOrderDetails",
                column: "NebimOrderId",
                principalTable: "NebimOrders",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetails_OrderDetails_Id",
                table: "NebimOrderDetails",
                column: "Id",
                principalTable: "OrderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetails_Tenants_TenantId",
                table: "NebimOrderDetails",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrders_Companies_CompanyId",
                table: "NebimOrders",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrders_Domains_DomainId",
                table: "NebimOrders",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrders_Orders_Id",
                table: "NebimOrders",
                column: "Id",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrders_Tenants_TenantId",
                table: "NebimOrders",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetails_Companies_CompanyId",
                table: "NebimOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetails_Domains_DomainId",
                table: "NebimOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetails_NebimOrders_NebimOrderId",
                table: "NebimOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetails_OrderDetails_Id",
                table: "NebimOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrderDetails_Tenants_TenantId",
                table: "NebimOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrders_Companies_CompanyId",
                table: "NebimOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrders_Domains_DomainId",
                table: "NebimOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrders_Orders_Id",
                table: "NebimOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_NebimOrders_Tenants_TenantId",
                table: "NebimOrders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NebimOrders",
                table: "NebimOrders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NebimOrderDetails",
                table: "NebimOrderDetails");

            migrationBuilder.RenameTable(
                name: "NebimOrders",
                newName: "NebimOrder");

            migrationBuilder.RenameTable(
                name: "NebimOrderDetails",
                newName: "NebimOrderDetail");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrders_TenantId",
                table: "NebimOrder",
                newName: "IX_NebimOrder_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrders_DomainId",
                table: "NebimOrder",
                newName: "IX_NebimOrder_DomainId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrders_CompanyId",
                table: "NebimOrder",
                newName: "IX_NebimOrder_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetails_TenantId",
                table: "NebimOrderDetail",
                newName: "IX_NebimOrderDetail_TenantId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetails_NebimOrderId",
                table: "NebimOrderDetail",
                newName: "IX_NebimOrderDetail_NebimOrderId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetails_DomainId",
                table: "NebimOrderDetail",
                newName: "IX_NebimOrderDetail_DomainId");

            migrationBuilder.RenameIndex(
                name: "IX_NebimOrderDetails_CompanyId",
                table: "NebimOrderDetail",
                newName: "IX_NebimOrderDetail_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NebimOrder",
                table: "NebimOrder",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NebimOrderDetail",
                table: "NebimOrderDetail",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrder_Companies_CompanyId",
                table: "NebimOrder",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrder_Domains_DomainId",
                table: "NebimOrder",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrder_Orders_Id",
                table: "NebimOrder",
                column: "Id",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrder_Tenants_TenantId",
                table: "NebimOrder",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetail_Companies_CompanyId",
                table: "NebimOrderDetail",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetail_Domains_DomainId",
                table: "NebimOrderDetail",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetail_NebimOrder_NebimOrderId",
                table: "NebimOrderDetail",
                column: "NebimOrderId",
                principalTable: "NebimOrder",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetail_OrderDetails_Id",
                table: "NebimOrderDetail",
                column: "Id",
                principalTable: "OrderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NebimOrderDetail_Tenants_TenantId",
                table: "NebimOrderDetail",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");
        }
    }
}
