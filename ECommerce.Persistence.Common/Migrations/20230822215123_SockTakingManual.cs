﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class SockTakingManual : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StocktakingDetails_Stocktakings_StocktakingId",
                table: "StocktakingDetails");

            migrationBuilder.AddColumn<bool>(
                name: "Manuel",
                table: "Stocktakings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "StocktakingId",
                table: "StocktakingDetails",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Anonymized",
                table: "Orders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Commercial",
                table: "Orders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_StocktakingDetails_Stocktakings_StocktakingId",
                table: "StocktakingDetails",
                column: "StocktakingId",
                principalTable: "Stocktakings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StocktakingDetails_Stocktakings_StocktakingId",
                table: "StocktakingDetails");

            migrationBuilder.DropColumn(
                name: "Manuel",
                table: "Stocktakings");

            migrationBuilder.DropColumn(
                name: "Anonymized",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Commercial",
                table: "Orders");

            migrationBuilder.AlterColumn<int>(
                name: "StocktakingId",
                table: "StocktakingDetails",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_StocktakingDetails_Stocktakings_StocktakingId",
                table: "StocktakingDetails",
                column: "StocktakingId",
                principalTable: "Stocktakings",
                principalColumn: "Id");
        }
    }
}
