﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class Stocktaking : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StocktakingTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StocktakingTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stocktakings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StocktakingTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CheckModelCode = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stocktakings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stocktakings_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Stocktakings_StocktakingTypes_StocktakingTypeId",
                        column: x => x.StocktakingTypeId,
                        principalTable: "StocktakingTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Stocktakings_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "StocktakingDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductInformationId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    StocktakingId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StocktakingDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StocktakingDetails_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_StocktakingDetails_ProductInformations_ProductInformationId",
                        column: x => x.ProductInformationId,
                        principalTable: "ProductInformations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StocktakingDetails_Stocktakings_StocktakingId",
                        column: x => x.StocktakingId,
                        principalTable: "Stocktakings",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_StocktakingDetails_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_StocktakingDetails_DomainId",
                table: "StocktakingDetails",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_StocktakingDetails_ProductInformationId",
                table: "StocktakingDetails",
                column: "ProductInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_StocktakingDetails_StocktakingId",
                table: "StocktakingDetails",
                column: "StocktakingId");

            migrationBuilder.CreateIndex(
                name: "IX_StocktakingDetails_TenantId",
                table: "StocktakingDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Stocktakings_DomainId",
                table: "Stocktakings",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Stocktakings_StocktakingTypeId",
                table: "Stocktakings",
                column: "StocktakingTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Stocktakings_TenantId",
                table: "Stocktakings",
                column: "TenantId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StocktakingDetails");

            migrationBuilder.DropTable(
                name: "Stocktakings");

            migrationBuilder.DropTable(
                name: "StocktakingTypes");
        }
    }
}
