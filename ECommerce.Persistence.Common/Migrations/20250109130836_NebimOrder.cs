﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerce.Persistence.Common.Migrations
{
    /// <inheritdoc />
    public partial class NebimOrder : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NebimOrder",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DocumentNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HeaderId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CurrAccCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NebimOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NebimOrder_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimOrder_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimOrder_Orders_Id",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NebimOrder_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NebimOrderDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    UsedBarcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LineId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Qty1 = table.Column<int>(type: "int", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    VatRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    NebimOrderId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    DomainId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NebimOrderDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NebimOrderDetail_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimOrderDetail_Domains_DomainId",
                        column: x => x.DomainId,
                        principalTable: "Domains",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimOrderDetail_NebimOrder_NebimOrderId",
                        column: x => x.NebimOrderId,
                        principalTable: "NebimOrder",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NebimOrderDetail_OrderDetails_Id",
                        column: x => x.Id,
                        principalTable: "OrderDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NebimOrderDetail_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrder_CompanyId",
                table: "NebimOrder",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrder_DomainId",
                table: "NebimOrder",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrder_TenantId",
                table: "NebimOrder",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrderDetail_CompanyId",
                table: "NebimOrderDetail",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrderDetail_DomainId",
                table: "NebimOrderDetail",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrderDetail_NebimOrderId",
                table: "NebimOrderDetail",
                column: "NebimOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_NebimOrderDetail_TenantId",
                table: "NebimOrderDetail",
                column: "TenantId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NebimOrderDetail");

            migrationBuilder.DropTable(
                name: "NebimOrder");
        }
    }
}
