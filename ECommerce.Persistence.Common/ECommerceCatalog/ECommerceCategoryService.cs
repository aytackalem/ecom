﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.CategoryService;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.ECommerceCatalog
{
    public class ECommerceCategoryService : IECommerceCategoryService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ECommerceCategoryService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<GetByMarketplaceIdResponse>> GetByMarketplaceIdAsync(string marketplaceId)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<GetByMarketplaceIdResponse>>(async response =>
            {
                var entities =  this
                    ._unitOfWork
                    .ECommerceCategoryRepository
                    .DbSet()
                    .Where(ec => ec.MarketplaceId == marketplaceId)
                    .ToList();

                if (entities.Count == 0)
                {
                    response.Data = new GetByMarketplaceIdResponse();
                    response.Message = "Herhangi bir e-ticaret kategorisi bulunamadı.";
                    return;
                }

                response.Data = entities
                    .GroupBy(e => e.MarketplaceId)
                    .Select(ge => new GetByMarketplaceIdResponse
                    {
                        MarketplaceId = ge.Key,
                        Items = ge.Select(e => new Category
                        {
                            Code = e.Code,
                            Name = e.Name,
                            CompanyId = e.CompanyId
                        }).ToList()
                    })
                    .First();
                response.Success = true;
            });
        }
        #endregion
    }
}
