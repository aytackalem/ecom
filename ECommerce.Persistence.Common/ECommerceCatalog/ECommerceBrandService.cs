﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.BrandService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.BrandService;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.ECommerceCatalog
{
    public class ECommerceBrandService : IECommerceBrandService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ECommerceBrandService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<SearchByNameResponse>>(async response =>
            {
                response.Data = new SearchByNameResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = await this
                        ._unitOfWork
                        .ECommerceBrandRepository
                        .DbSet()
                        .Where(ecvv => ecvv.Name.Contains(parameters.Name))
                        .Select(ecvv => new Brand
                        {
                            Code = ecvv.Code,
                            Name = ecvv.Name,
                            CompanyId = ecvv.CompanyId
                        })
                        .ToListAsync()
                };
                response.Success = true;
            });
        }
        #endregion
    }
}
