﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantValueService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantValueService;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.ECommerceCatalog
{
    public class ECommerceVariantValueService : IECommerceVariantValueService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ECommerceVariantValueService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<SearchByNameResponse>> SearchByNameAsync(SearchByNameParameters parameters)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<SearchByNameResponse>>(async response =>
            {
                response.Data = new SearchByNameResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = await this
                        ._unitOfWork
                        .ECommerceVariantValueRepository
                        .DbSet()
                        .Where(ecvv => ecvv.ECommerceVariantCode == parameters.VariantCode && ecvv.Name.Contains(parameters.Name))
                        .Select(ecvv => new VariantValue
                        {
                            Code = ecvv.Code,
                            Name = ecvv.Name,
                            VariantCode = ecvv.ECommerceVariantCode
                        })
                        .ToListAsync()
                };
                response.Success = true;
            });
        }
        #endregion
    }
}
