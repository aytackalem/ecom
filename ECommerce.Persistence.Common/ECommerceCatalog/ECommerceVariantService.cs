﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.ECommerceCatalog;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplaceCatalog.VariantService;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.MarketplaceCatalog.VariantService;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.ECommerceCatalog
{
    public class ECommerceVariantService : IECommerceVariantService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ECommerceVariantService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<GetByCategoryCodesResponse>> GetByCategoryCodesAsync(GetByCategoryCodesParameter parameters)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<GetByCategoryCodesResponse>>(async response =>
            {
                var ecommerceVariants =  this
                    ._unitOfWork
                    .ECommerceVariantRepository
                    .DbSet()
                    .Where(ev => ev.MarketplaceId == parameters.MarketplaceId && parameters.CategoryCodes.Contains(ev.ECommerceCategoryCode))
                    .ToList();

                response.Data = new GetByCategoryCodesResponse
                {
                    MarketplaceId = parameters.MarketplaceId,
                    Items = ecommerceVariants
                        .GroupBy(ev => new
                        {
                            ev.AllowCustom,
                            ev.Code,
                            ev.CompanyId,
                            ev.Mandatory,
                            ev.Name
                        })
                        .Select(ev => new Application.Common.Wrappers.MarketplaceCatalog.VariantService.Variant
                        {
                            AllowCustom = ev.Key.AllowCustom,
                            Code = ev.Key.Code,
                            CompanyId = ev.Key.CompanyId,
                            Mandatory = ev.Key.Mandatory,
                            Name = ev.Key.Name
                        })
                        .ToList()
                };
                response.Success = true;
            });
        }
        #endregion
    }
}
