﻿using Dapper;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.Services
{
    public class DbService : IDbService
    {
        #region Fields
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public DbService(IConfiguration configuration)
        {
            #region Fields
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<bool>> ExistAsync(string name)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<bool>>(async response =>
            {
                var connectionString = string.Format(this._configuration.GetConnectionString("Helpy"), "master");
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    var query = @"Select Case Count(0) When 0 Then 0 Else 1 End From master.sys.databases Where database_id > 4 And [name] Not In ('IdentityServer', 'IdentityServer.Config', 'Lafaba_ECommerce', 'Marketplace') And [name] = @Name";
                    response.Data = await sqlConnection.QueryFirstAsync<bool>(query, name);
                    response.Success = true;
                }
            });
        }

        public async Task<DataResponse<List<string>>> GetNamesAsync()
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<DataResponse<List<string>>>(async response =>
            {
                var connectionString = string.Format(this._configuration.GetConnectionString("Helpy"), "master");
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    var query = @"Select [name] From master.sys.databases Where database_id > 4 And [name] Not In ('IdentityServer', 'IdentityServer.Config', 'Lafaba_ECommerce', 'Marketplace') and [name] NOT LIKE '%Warehouse%'";
                    response.Data = (await sqlConnection.QueryAsync<string>(query)).ToList();
                    response.Success = true;
                }
            });
        }
        #endregion
    }
}
