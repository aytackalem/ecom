﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class EmailService : IEmailService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly EmailProviderResolver _emailProviderResolver;
        #endregion

        #region Constructors
        public EmailService(IUnitOfWork unitOfWork, EmailProviderResolver emailProviderResolver)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._emailProviderResolver = emailProviderResolver;
            #endregion
        }
        #endregion

        #region Methods
        public Response Send(List<string> emailAddress, string subject, string body, bool sendAsync = true)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var emailProviderCompanies = this
                    ._unitOfWork
                    .EmailProviderCompanyRepository
                    .DbSet()
                    .ToList();

                if (emailProviderCompanies == null || emailProviderCompanies.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Email sağlayıcı bulunamadı.";
                    return;
                }

                foreach (var theEmailProviderCompany in emailProviderCompanies)
                {
                    IEmailProvider emailProvider = this._emailProviderResolver(theEmailProviderCompany.EmailProviderId);
                    var providerResponse = emailProvider.Send(emailAddress, subject, body, sendAsync);
                    if (providerResponse.Success)
                    {
                        response.Message = providerResponse.Message;
                        response.Success = providerResponse.Success;
                        break;
                    }
                }
            });
        }
        #endregion
    }
}
