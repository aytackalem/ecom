﻿using ECommerce.Application.Common.DataTransferObjects.XmlService;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class XmlService : IXmlService
    {
        #region Methods
        public XmlSource GetXmlSource(string requestUri, int productSourceDomainId, string productSourceId)
        {
            XmlSource xmlSource = null;
            if (requestUri.StartsWith("http://www.simsar.com.tr"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    UserAgent = "Helpy",
                    ProductSourceDomainId = productSourceDomainId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "product",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "stoksayisal",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "productid",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping> {
                            /*
                             * kategoriadi Replace "Kadın/Kız", "Kadın" +1
                             */
                            new XmlItemMapping("kategoriadi", "CategoryName", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new List<XmlItemMappingRule>
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = "Kadın/Kız",
                                        NewValue = "Kadın"
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = "-",
                                        NewValue = ""
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Edit,
                                        Command = "FirstLetterUppercase"
                                    }
                                }
                            },

                            new XmlItemMapping("stoksayisal", "Stock", XmlItemType.Property),
                            new XmlItemMapping("urunkodu", "StockCode", XmlItemType.Property),

                            new XmlItemMapping("productid", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("urunadi", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("marka", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("model", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("vergi", "VatRate", XmlItemType.Property),
                            new XmlItemMapping("indirimlifiyatvergili", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("satisfiyativergili", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("resimler", "Photos", XmlItemType.Property)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("resim", "Photos", XmlItemType.Property)
                                }
                            },
                            new XmlItemMapping("ayrintilar2", "ProductDescription", XmlItemType.Property),
                            new XmlItemMapping("ayrintilar2", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("cinsiyet", string.Empty, XmlItemType.Attribute) { Key = "Cinsiyet" },
                            new XmlItemMapping("ozellikler", "", XmlItemType.Attribute)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("ozellik", "", XmlItemType.Attribute)
                                    {
                                        KeyNodeName = "adi",
                                        ValueNodeName = "degeri"
                                    }
                                }
                            }
                        }
                    },
                    ProductSourceId = productSourceId
                };
            }
            else if (requestUri.StartsWith("https://www.zamanatolyesi.com"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    ProductSourceDomainId = productSourceDomainId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "item",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "stok",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "barkod",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping> {
                            /*
                             * URUN_ADI Replace SKU, ""
                             */
                            new XmlItemMapping("urun_ad", "CategoryName", XmlItemType.Property)
                            {
                                XmlItemMappingRules= new List<XmlItemMappingRule>
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValueAttributeName = "referans",
                                        NewValue = ""
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Edit,
                                        Command = "FirstLetterUppercase"
                                    }
                                }
                            },
                            new XmlItemMapping("", "VatRate", XmlItemType.Property)
                            {
                                DefaultValue = true,
                                Value = "20"
                            },
                            new XmlItemMapping("barkod", "StockCode", XmlItemType.Property),
                            new XmlItemMapping("barkod", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("barkod", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("marka", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("urun_ad", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("urun_ad", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("urun_ad", "ProductDescription", XmlItemType.Property),
                            new XmlItemMapping("stok", "Stock", XmlItemType.Property),
                            new XmlItemMapping("psf", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("tsf", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("image", "Photos", XmlItemType.Property),
                            new XmlItemMapping("additional_image_2", "Photos", XmlItemType.Property),
                            new XmlItemMapping("additional_image_3", "Photos", XmlItemType.Property),
                            new XmlItemMapping("additional_image_4", "Photos", XmlItemType.Property),
                            //new XmlItemMapping("image", string.Empty, XmlItemType.Property)
                            //{
                            //    ChildXmlItemMappings = new() {
                            //        new XmlItemMapping("image", "Photos", XmlItemType.Property),
                            //        new XmlItemMapping("additional_image_1", "Photos", XmlItemType.Property),
                            //        new XmlItemMapping("additional_image_2", "Photos", XmlItemType.Property),
                            //        new XmlItemMapping("additional_image_3", "Photos", XmlItemType.Property),
                            //        new XmlItemMapping("additional_image_4", "Photos", XmlItemType.Property),
                            //        new XmlItemMapping("additional_image_5", "Photos", XmlItemType.Property)
                            //    }
                            //},
                            new XmlItemMapping("referans", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("cinsiyet", string.Empty, XmlItemType.Attribute) { Key = "Cinsiyet" },
                            new XmlItemMapping("calisma_sekli", string.Empty, XmlItemType.Attribute) { Key = "Çalışma Şekli" },
                            new XmlItemMapping("kasa_materyal", string.Empty, XmlItemType.Attribute) { Key = "Kasa Materiyali" },
                            new XmlItemMapping("kasa_sekil", string.Empty, XmlItemType.Attribute) { Key = "Kasa Şekli" },
                            new XmlItemMapping("kasa_capi", string.Empty, XmlItemType.Attribute) { Key = "Kasa Çap" },
                            new XmlItemMapping("kasa_renk", string.Empty, XmlItemType.Attribute) { Key = "Kasa Rengi" },
                            new XmlItemMapping("kadran_renk", string.Empty, XmlItemType.Attribute) { Key = "Kadran Rengi" },
                            new XmlItemMapping("cam", string.Empty, XmlItemType.Attribute) { Key = "Cam Cinsi" },
                            new XmlItemMapping("kasa_sekil", string.Empty, XmlItemType.Attribute) { Key = "Cam Şekli" },
                            new XmlItemMapping("kordon_tip", string.Empty, XmlItemType.Attribute) { Key = "Kordon Tipi" },
                            new XmlItemMapping("kordon_renk", string.Empty, XmlItemType.Attribute) { Key = "Kordon Rengi" },
                            new XmlItemMapping("kordon_kilit", string.Empty, XmlItemType.Attribute) { Key = "Kordon Kilidi" },
                            new XmlItemMapping("atm", string.Empty, XmlItemType.Attribute) { Key = "Su Geçirmezlik" },
                            new XmlItemMapping("takvim", string.Empty, XmlItemType.Attribute) { Key = "Takvim" },
                            new XmlItemMapping("kronometre", string.Empty, XmlItemType.Attribute) { Key = "Kronometre" },
                        }
                    },
                    ProductSourceId = productSourceId
                };
            }
            else if (requestUri.StartsWith("https://www.aslansaat.com"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    ProductSourceDomainId = productSourceDomainId,
                    ProductSourceId = productSourceId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "product",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "stock",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "barcode",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping> {
                            /*
                             * URUN_ADI Replace SKU, ""
                             * URUN_ADI Replace "otomatik", "" +1
                             */
                            new XmlItemMapping("name", "CategoryName", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new List<XmlItemMappingRule>
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValueAttributeName = "ws_code",
                                        NewValue = ""
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValueAttributeName = "brand",
                                        NewValue = ""
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = "Otomatik",
                                        NewValue = ""
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Edit,
                                        Command = "FirstLetterUppercase"
                                    }
                                }
                            },
                            new XmlItemMapping("", "VatRate", XmlItemType.Property)
                            {
                                DefaultValue = true,
                                Value = "20"
                            },
                            new XmlItemMapping("ws_code", "StockCode", XmlItemType.Property),
                            new XmlItemMapping("name", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("name", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("name", "ProductDescription", XmlItemType.Property),
                            new XmlItemMapping("brand", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("ws_code", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("barcode", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("price_list_vat_included", "ListUnitPrice", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new()
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = ",",
                                        NewValue = "."
                                    }
                                }
                            },
                            new XmlItemMapping("price_list_vat_included", "UnitPrice", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new()
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = ",",
                                        NewValue = "."
                                    }
                                }
                            },
                            new XmlItemMapping("stock", "Stock", XmlItemType.Property),
                            new XmlItemMapping("img_item", "Photos", XmlItemType.Property),
                            new XmlItemMapping("ws_code", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("ProductProperties", "", XmlItemType.Composite)
                            {
                                SourceChildNodeName = "ProductProperty",
                                KeyNodeName = "Key",
                                ValueNodeName = "Value",
                                SourceXmlItemType = XmlItemType.Attribute,
                            },
                            new XmlItemMapping("filters", "", XmlItemType.Attribute)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("filter", "", XmlItemType.Attribute)
                                    {
                                        KeyNodeName = "key",
                                        ValueNodeName = "value"
                                    }
                                }
                            }
                        }
                    }
                };
            }
            else if (requestUri.StartsWith("https://www.konyalibayi.com"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    ProductSourceDomainId = productSourceDomainId,
                    ProductSourceId = productSourceId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "Urun",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "Stok",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "Barkod",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "Durum",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "Pasif"
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping> {
                            new XmlItemMapping("Barkod", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("Kod", "StockCode", XmlItemType.Property),
                            new XmlItemMapping("Kod", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("Stok", "Stock", XmlItemType.Property),
                            new XmlItemMapping("Marka", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("UrunAd", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("KDV", "VatRate", XmlItemType.Property),
                            new XmlItemMapping("Fiyat", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("Fiyat", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("Baslik", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("Baslik", "ProductDescription", XmlItemType.Property),
                            new XmlItemMapping("KategoriAds", "CategoryName", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new List<XmlItemMappingRule>
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = "#",
                                        NewValue = " "
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Concat,
                                        NewValueAttributeName = "Cinsiyet"
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Edit,
                                        Command = "FirstLetterUppercase"
                                    }
                                }
                            },
                            new XmlItemMapping("UrunID", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("Resims", "Photos", XmlItemType.MultiProperty)
                            {
                                Seperator = "#",
                                SourceXmlItemType = XmlItemType.Property
                            },
                            new XmlItemMapping("", "", XmlItemType.KeyValueProperty)
                            {
                                Seperator = "#",
                                KeyNodeName = "SpecIds",
                                ValueNodeName = "SpecAds",
                                SourceXmlItemType = XmlItemType.Attribute
                            }
                        }
                    }
                };
            }
            else if (requestUri.StartsWith("https://www.kosovalisaat.com"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    ProductSourceDomainId = productSourceDomainId,
                    ProductSourceId = productSourceId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "Product",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "UnitInStock",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "BarCode",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping> {
                            new XmlItemMapping("BarCode", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("SKU", "StockCode", XmlItemType.Property),
                            new XmlItemMapping("SKU", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("UnitInStock", "Stock", XmlItemType.Property),
                            new XmlItemMapping("Manufacture", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("Manufacture", "ProductName", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new List<XmlItemMappingRule>
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Append,
                                        NewValue = " "
                                    },
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Concat,
                                        NewValueAttributeName = "SKU"
                                    },
                                }
                            },
                            new XmlItemMapping("", "VatRate", XmlItemType.Property)
                            {
                                DefaultValue = true,
                                Value = "20"
                            },
                            new XmlItemMapping("KDVTRLListPrice", "ListUnitPrice", XmlItemType.Property),
                            new XmlItemMapping("KDVTRLListPrice", "UnitPrice", XmlItemType.Property),
                            new XmlItemMapping("", "CategoryName", XmlItemType.Property)
                            {
                                DefaultValue = true,
                                Value = "Kadın Kol Saati"
                            },
                            new XmlItemMapping("SKU", "SkuCode", XmlItemType.Property),
                            new XmlItemMapping("Images", "Photos", XmlItemType.Property)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("Image", "Photos", XmlItemType.Property)
                                }
                            },
                            new XmlItemMapping("ProductProperties", "", XmlItemType.Composite)
                            {
                                SourceChildNodeName = "ProductProperty",
                                KeyNodeName = "Key",
                                ValueNodeName = "Value",
                                SourceXmlItemType = XmlItemType.Attribute,
                            }
                        }
                    }
                };
            }
            else if (requestUri.StartsWith("https://erp.helpy.com.tr"))
            {
                xmlSource = new XmlSource
                {
                    RequestUri = requestUri,
                    ProductSourceDomainId = productSourceDomainId,
                    ProductSourceId = productSourceId,
                    XmlMapping = new XmlMapping
                    {
                        ListItemNodeName = "Urun",
                        XmlItemMappingValidates = new List<XmlItemMappingValidate>
                        {
                            new XmlItemMappingValidate
                            {
                                ElementName= "StokAdedi",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = "0"
                            },
                            new XmlItemMappingValidate
                            {
                                ElementName= "Barkod",
                                XmlItemMappingValidateType = XmlItemMappingValidateType.NotEq,
                                Value = ""
                            }
                        },
                        XmlItemMappings = new List<XmlItemMapping>
                        {
                           new XmlItemMapping("KategoriTree", "CategoryName", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new List<XmlItemMappingRule>
                                {

                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Edit,
                                        Command = "FirstLetterUppercase"
                                    }
                                }
                            },
                           new XmlItemMapping("StokKodu", "SellerCode", XmlItemType.Property),
                            new XmlItemMapping("UrunAdi", "ProductName", XmlItemType.Property),
                            new XmlItemMapping("Aciklama", "ProductContent", XmlItemType.Property),
                            new XmlItemMapping("Marka", "BrandName", XmlItemType.Property),
                            new XmlItemMapping("StokKodu", "StockCode", XmlItemType.Property),
                            new XmlItemMapping("Barkod", "Barcode", XmlItemType.Property),
                            new XmlItemMapping("StokAdedi", "Stock", XmlItemType.Property),
                            new XmlItemMapping("SatisFiyati", "ListUnitPrice", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new()
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = ",",
                                        NewValue = "."
                                    }
                                }
                            },
                            new XmlItemMapping("IndirimliFiyat", "UnitPrice", XmlItemType.Property)
                            {
                                XmlItemMappingRules = new()
                                {
                                    new XmlItemMappingRule
                                    {
                                        XmlItemMappingRuleType = XmlItemMappingRuleType.Replace,
                                        OldValue = ",",
                                        NewValue = "."
                                    }
                                }
                            },
                            new XmlItemMapping("KdvOrani", "VatRate", XmlItemType.Property),
                            new XmlItemMapping("Resimler", "Photos", XmlItemType.Property)
                            {
                                ChildXmlItemMappings = new()
                                {
                                    new XmlItemMapping("Resim", "Photos", XmlItemType.Property)
                                }
                            },
                            new XmlItemMapping("TeknikDetaylar", "", XmlItemType.Composite)
                            {
                                SourceChildNodeName = "TeknikDetay",
                                KeyNodeName = "OzellikTanim",
                                ValueNodeName = "DegerTanim",
                                SourceXmlItemType = XmlItemType.Attribute,
                            }
                        }
                    }
                };
            }
            return xmlSource;
        }

        public DataResponse<List<ParsedXmlItem>> Parse(string text, XmlSource xmlSource)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ParsedXmlItem>>>(response =>
            {
                XmlMapping xmlMapping = xmlSource.XmlMapping;

                List<ParsedXmlItem> list = new();

                var xDocument = XDocument.Parse(text);
                var elements = xDocument.Descendants(xmlMapping.ListItemNodeName);
                foreach (var theElement in elements)
                {
                    var valid = true;

                    xmlMapping.XmlItemMappingValidates.ForEach(v =>
                    {
                        switch (v.XmlItemMappingValidateType)
                        {
                            case XmlItemMappingValidateType.Eq:

                                if (theElement.Element(v.ElementName).Value != v.Value)
                                    valid = false;

                                break;
                            case XmlItemMappingValidateType.NotEq:

                                if (theElement.Element(v.ElementName).Value == v.Value)
                                    valid = false;

                                break;
                        }
                    });

                    if (valid == false)
                        continue;

                    ParsedXmlItem parsedXmlItem = new()
                    {
                        ProductSourceDomainId = xmlSource.ProductSourceDomainId,
                        
                    };

                    foreach (var theXmlItemMapping in xmlMapping.XmlItemMappings)
                    {
                        if (theXmlItemMapping.DefaultValue)
                        {
                            XmlItemMapping xmlItemMapping = new()
                            {
                                XmlItemType = theXmlItemMapping.XmlItemType,
                                SourceNodeName = theXmlItemMapping.SourceNodeName,
                                TargetPropertyName = theXmlItemMapping.TargetPropertyName,
                                Value = theXmlItemMapping.Value,
                                Key = theXmlItemMapping.Key
                            };

                            parsedXmlItem.XmlItemMappings.Add(xmlItemMapping);
                        }
                        else if (theXmlItemMapping.ChildXmlItemMappings.Count == 0)
                        {
                            if (theXmlItemMapping.XmlItemType == XmlItemType.MultiProperty)
                            {
                                var values = theElement
                                    .Element(theXmlItemMapping.SourceNodeName)
                                    .Value
                                    .Split(theXmlItemMapping.Seperator, System.StringSplitOptions.RemoveEmptyEntries);

                                List<XmlItemMapping> xmlItemMappings = values
                                    .Select(v => new XmlItemMapping
                                    {
                                        XmlItemType = theXmlItemMapping.SourceXmlItemType,
                                        SourceNodeName = theXmlItemMapping.SourceNodeName,
                                        TargetPropertyName = theXmlItemMapping.TargetPropertyName,
                                        Value = v.Trim(),
                                        Key = theXmlItemMapping.Key
                                    })
                                    .ToList();

                                xmlItemMappings.ForEach(xim =>
                                {
                                    if (theXmlItemMapping.XmlItemMappingRules.HasItem())
                                    {
                                        theXmlItemMapping.XmlItemMappingRules.ForEach(theRule =>
                                        {
                                            switch (theRule.XmlItemMappingRuleType)
                                            {
                                                case XmlItemMappingRuleType.Replace:
                                                    var oldValue = string.Empty;
                                                    if (string.IsNullOrEmpty(theRule.OldValue) == false)
                                                        oldValue = theRule.OldValue;
                                                    else
                                                        oldValue = theElement.Element(theRule.OldValueAttributeName).Value;

                                                    xim.Value = xim.Value.Replace(oldValue, theRule.NewValue, System.StringComparison.InvariantCulture);
                                                    break;
                                                case XmlItemMappingRuleType.Edit:
                                                    if (theRule.Command == "FirstLetterUppercase")
                                                        xim.Value = xim.Value.FirstLetterUppercase();
                                                    break;
                                                case XmlItemMappingRuleType.Concat:
                                                    xim.Value = $"{theElement.Element(theRule.NewValueAttributeName).Value} {xim.Value}";
                                                    break;
                                                case XmlItemMappingRuleType.Append:
                                                    xim.Value += theRule.NewValue;
                                                    break;
                                            }
                                        });
                                    }
                                });

                                parsedXmlItem.XmlItemMappings.AddRange(xmlItemMappings);
                            }
                            else if (theXmlItemMapping.XmlItemType == XmlItemType.KeyValueProperty)
                            {
                                var keys = theElement
                                    .Element(theXmlItemMapping.KeyNodeName)
                                    .Value
                                    .Split(theXmlItemMapping.Seperator);

                                var values = theElement
                                    .Element(theXmlItemMapping.ValueNodeName)
                                    .Value
                                    .Split(theXmlItemMapping.Seperator);

                                if (keys.Length == values.Length)
                                {
                                    List<XmlItemMapping> xmlItemMappings = new();

                                    for (int i = 0; i < keys.Length; i++)
                                    {
                                        xmlItemMappings.Add(new XmlItemMapping
                                        {
                                            XmlItemType = theXmlItemMapping.SourceXmlItemType,
                                            SourceNodeName = theXmlItemMapping.SourceNodeName,
                                            TargetPropertyName = theXmlItemMapping.TargetPropertyName,
                                            Value = values[i].Trim(),
                                            Key = keys[i]
                                        });
                                    }

                                    xmlItemMappings.ForEach(xim =>
                                    {
                                        if (theXmlItemMapping.XmlItemMappingRules.HasItem())
                                        {
                                            theXmlItemMapping.XmlItemMappingRules.ForEach(theRule =>
                                            {
                                                switch (theRule.XmlItemMappingRuleType)
                                                {
                                                    case XmlItemMappingRuleType.Replace:
                                                        var oldValue = string.Empty;
                                                        if (string.IsNullOrEmpty(theRule.OldValue) == false)
                                                            oldValue = theRule.OldValue;
                                                        else
                                                            oldValue = theElement.Element(theRule.OldValueAttributeName).Value;

                                                        xim.Value = xim.Value.Replace(oldValue, theRule.NewValue, System.StringComparison.InvariantCulture);
                                                        break;
                                                    case XmlItemMappingRuleType.Edit:
                                                        if (theRule.Command == "FirstLetterUppercase")
                                                            xim.Value = xim.Value.FirstLetterUppercase();
                                                        break;
                                                    case XmlItemMappingRuleType.Concat:
                                                        xim.Value = $"{theElement.Element(theRule.NewValueAttributeName).Value} {xim.Value}";
                                                        break;
                                                    case XmlItemMappingRuleType.Append:
                                                        xim.Value += theRule.NewValue;
                                                        break;
                                                }
                                            });
                                        }
                                    });

                                    parsedXmlItem.XmlItemMappings.AddRange(xmlItemMappings);
                                }
                            }
                            else if (theXmlItemMapping.XmlItemType == XmlItemType.Composite)
                            {
                                List<XmlItemMapping> xmlItemMappings = new();

                                var els = theElement.Elements(theXmlItemMapping.SourceNodeName);
                                foreach (var item in els)
                                {
                                    var a = item.Elements(theXmlItemMapping.SourceChildNodeName).Select(e => new XmlItemMapping
                                    {
                                        XmlItemType = theXmlItemMapping.SourceXmlItemType,
                                        SourceNodeName = theXmlItemMapping.SourceNodeName,
                                        TargetPropertyName = theXmlItemMapping.TargetPropertyName,
                                        Value = e.Element(theXmlItemMapping.ValueNodeName).Value,
                                        Key = e.Element(theXmlItemMapping.KeyNodeName).Value
                                    }).ToList();

                                    xmlItemMappings.AddRange(a);
                                }

                                parsedXmlItem.XmlItemMappings.AddRange(xmlItemMappings);
                            }
                            else
                            {
                                XmlItemMapping xmlItemMapping = new()
                                {
                                    XmlItemType = theXmlItemMapping.XmlItemType,
                                    SourceNodeName = theXmlItemMapping.SourceNodeName,
                                    TargetPropertyName = theXmlItemMapping.TargetPropertyName,
                                    Value = theElement.Element(theXmlItemMapping.SourceNodeName).Value,
                                    Key = theXmlItemMapping.Key
                                };

                                if (theXmlItemMapping.XmlItemMappingRules.HasItem())
                                {
                                    theXmlItemMapping.XmlItemMappingRules.ForEach(theRule =>
                                    {
                                        switch (theRule.XmlItemMappingRuleType)
                                        {
                                            case XmlItemMappingRuleType.Replace:
                                                var oldValue = string.Empty;
                                                if (string.IsNullOrEmpty(theRule.OldValue) == false)
                                                    oldValue = theRule.OldValue;
                                                else
                                                    oldValue = theElement.Element(theRule.OldValueAttributeName).Value;

                                                xmlItemMapping.Value = xmlItemMapping.Value.Replace(oldValue, theRule.NewValue, System.StringComparison.InvariantCulture);
                                                break;
                                            case XmlItemMappingRuleType.Edit:
                                                if (theRule.Command == "FirstLetterUppercase")
                                                    xmlItemMapping.Value = xmlItemMapping.Value.FirstLetterUppercase();
                                                break;
                                            case XmlItemMappingRuleType.Concat:
                                                xmlItemMapping.Value = $"{theElement.Element(theRule.NewValueAttributeName).Value} {xmlItemMapping.Value}";
                                                break;
                                            case XmlItemMappingRuleType.Append:
                                                xmlItemMapping.Value += theRule.NewValue;
                                                break;
                                        }
                                    });
                                }

                                xmlItemMapping.Value = xmlItemMapping.Value.Trim();

                                parsedXmlItem.XmlItemMappings.Add(xmlItemMapping);
                            }
                        }
                        else
                            foreach (var theChildXmlItemMapping in theXmlItemMapping.ChildXmlItemMappings)
                            {
                                var xmlItemMappings = theElement
                                    .Element(theXmlItemMapping.SourceNodeName)
                                    ?.Elements(theChildXmlItemMapping.SourceNodeName)
                                    ?.Select(x =>
                                    {
                                        if (string.IsNullOrEmpty(theChildXmlItemMapping.KeyNodeName) &&
                                            string.IsNullOrEmpty(theChildXmlItemMapping.ValueNodeName))
                                            return new XmlItemMapping
                                            {
                                                XmlItemType = theChildXmlItemMapping.XmlItemType,
                                                SourceNodeName = theChildXmlItemMapping.SourceNodeName,
                                                TargetPropertyName = theChildXmlItemMapping.TargetPropertyName,
                                                KeyNodeName = theChildXmlItemMapping.KeyNodeName,
                                                ValueNodeName = theChildXmlItemMapping.ValueNodeName,
                                                Key = theChildXmlItemMapping.Key,
                                                Value = x.Value
                                            };
                                        else
                                            return new XmlItemMapping
                                            {
                                                XmlItemType = theChildXmlItemMapping.XmlItemType,
                                                SourceNodeName = theChildXmlItemMapping.SourceNodeName,
                                                TargetPropertyName = theChildXmlItemMapping.TargetPropertyName,
                                                KeyNodeName = theChildXmlItemMapping.KeyNodeName,
                                                ValueNodeName = theChildXmlItemMapping.ValueNodeName,
                                                Key = x.Element(theChildXmlItemMapping.KeyNodeName).Value,
                                                Value = x.Element(theChildXmlItemMapping.ValueNodeName).Value
                                            };
                                    });
                                if (xmlItemMappings != null)
                                    parsedXmlItem.XmlItemMappings.AddRange(xmlItemMappings);
                            }
                    }
                    list.Add(parsedXmlItem);
                }

                response.Data = list;
                response.Success = list.Count > 0;
            });
        }

        public Response Write<T>(string path, List<T> values)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var xml = new System.Xml.Serialization.XmlSerializer(typeof(List<T>), new System.Xml.Serialization.XmlRootAttribute("Products"));
                using (StreamWriter sw = new StreamWriter(path))
                {
                    xml.Serialize(sw, values);
                }
            });
        }
        #endregion
    }
}