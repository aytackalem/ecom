﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.EInvoice;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class EInvoiceService : IEInvoiceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly EInvoiceProviderResolver _eInvoiceProviderResolver;
        #endregion

        #region Constructors
        public EInvoiceService(IUnitOfWork unitOfWork, EInvoiceProviderResolver eInvoiceProviderResolver)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            this._eInvoiceProviderResolver = eInvoiceProviderResolver;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                var accountingcompany = this
                    ._unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                IEInvoiceProvider eInvoiceProvider = this._eInvoiceProviderResolver(accountingcompany.AccountingId);
                var sendResponse = eInvoiceProvider.CreateInvoice(orderId);

                response.Data = sendResponse.Data;
                response.Success = sendResponse.Success;
                response.Message = sendResponse.Message;
            });
        }
        #endregion
    }
}
