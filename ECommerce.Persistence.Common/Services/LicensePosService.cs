﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class LicensePosService : IPosService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IEnumerable<IPosProvider> _posses;
        #endregion

        #region Constructors
        public LicensePosService(IUnitOfWork unitOfWork, IEnumerable<IPosProvider> posses)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._posses = posses;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<PaymentResponse> Check3d()
        {
            var check3dResponse = this._posses.First().Check3d();

            if (check3dResponse.Success == false)
                this.Log(check3dResponse.Data.PaymentId, string.Empty, check3dResponse.Message);

            return check3dResponse;
        }

        public DataResponse<IPosProvider> Find(string creditCardNumber)
        {
            throw new NotImplementedException();
        }

        public DataResponse<Payment3DResponse> Pay3d(CreditCardPayment creditCardPayment)
        {
            var pay3dResponse = this._posses.First().Pay3d(creditCardPayment);

            if (pay3dResponse.Success == false)
                this.Log(creditCardPayment.PaymentId, pay3dResponse.Message, string.Empty);

            return pay3dResponse;
        }
        #endregion

        #region Private Methods
        private void Log(int paymentId, string pay3dMessage, string check3dMessage)
        {
            try
            {
                var paymentLog = this
                    ._unitOfWork
                    .PaymentLogRepository
                    .DbSet()
                    .FirstOrDefault(pl => pl.Id == paymentId);
                if (paymentLog == null)
                {
                    this
                        ._unitOfWork
                        .PaymentLogRepository
                        .Create(new Domain.Entities.PaymentLog
                        {
                            Id = paymentId,
                            CreatedDate = DateTime.Now,
                            Pay3dMessage = pay3dMessage,
                            Check3dMessage = check3dMessage
                        });
                }
                else
                {
                    paymentLog.Pay3dMessage ??= pay3dMessage;
                    paymentLog.Check3dMessage ??= check3dMessage;

                    this
                        ._unitOfWork
                        .PaymentLogRepository
                        .Update(paymentLog);
                }
            }
            catch { }
        }
        #endregion
    }
}
