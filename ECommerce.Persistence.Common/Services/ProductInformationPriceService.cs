﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;

namespace ECommerce.Persistence.Common.Services
{
    public class ProductInformationPriceService : IProductInformationPriceService
    {
        #region Fields
        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public ProductInformationPriceService(IConfiguration configuration, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._configuration = configuration;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods

        public Response MakeDirtyPrice(int id)
        {
            return this.Execute((sqlCommand) =>
            {
                sqlCommand.CommandText = @"                
                UPDATE 
                ProductInformationMarketplaces 
                SET 
                     DirtyPrice=1
                FROM 
                ProductInformationMarketplaces M
                INNER JOIN ProductInformations PIN ON M.ProductInformationId=PIN.Id
                Where PIN.Id=@Id";

                sqlCommand.Parameters.AddWithValue("@Id", id);
            });
        }
        #endregion

        #region Helper Methods
        private Response Execute(Action<SqlCommand> action)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                using (sqlConnection = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using (SqlCommand sqlCommand = new("", sqlConnection))
                    {
                        action(sqlCommand);

                        sqlConnection.Open();

                        response.Success = sqlCommand.ExecuteNonQuery() > 0;
                    }
                }

                sqlConnection.Close();
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }
        #endregion
    }
}
