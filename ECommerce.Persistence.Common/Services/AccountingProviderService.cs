﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class AccountingProviderService : IAccountingProviderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly AccountingProviderResolver _accountingProviderServiceResolver;
        #endregion

        #region Constructors
        public AccountingProviderService(IUnitOfWork unitOfWork, AccountingProviderResolver accountingProviderService)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            this._accountingProviderServiceResolver = accountingProviderService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<AccountingInfo> CreateInvoice(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                var accountingcompany = this.
                    _unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .Include(x => x.AccountingConfigurations)
                    .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.CreateInvoice(orderId, configurations);

                response.Data = sendResponse.Data;
                response.Success = sendResponse.Success;
                response.Message = sendResponse.Message;
            });
        }

        public DataResponse<AccountingInfo> CreateInvoice(List<int> orderIds)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                var accountingcompany = this.
                    _unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .Include(x => x.AccountingConfigurations)
                    .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.CreateInvoice(orderIds, configurations);

                response.Data = sendResponse.Data;
                response.Success = sendResponse.Success;
                response.Message = sendResponse.Message;
            });
        }

        public DataResponse<AccountingCancel> CancelInvoice(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingCancel>>((response) =>
            {
                var accountingcompany = _unitOfWork
                  .AccountingCompanyRepository
                  .DbSet()
                  .Include(x => x.AccountingConfigurations)
                  .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.CancelInvoice(orderId, configurations);
                if (sendResponse.Success)
                {
                    response.Data = sendResponse.Data;
                    response.Success = true;
                }

            });
        }

        public DataResponse<string> CreateStockTransfer(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<string>>((response) =>
            {
                var accountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.CreateStockTransfer(orderId, configurations);
                if (sendResponse.Success)
                {
                    response.Data = sendResponse.Data;
                    response.Success = true;
                }

            });
        }

        public DataResponse<List<int>> ReadStockTransfers()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<int>>>((response) =>
            {
                var accountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.ReadStockTransfers();
                if (sendResponse.Success)
                {
                    response.Data = sendResponse.Data;
                    response.Success = true;
                }
            });
        }

        public DataResponse<int> ReadLastStockTransfer()
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                var ccountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();

                if (ccountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(ccountingcompany.AccountingId);
                var sendResponse = accountingProviderService.ReadLastStockTransfer();
                if (sendResponse.Success)
                {
                    response.Data = sendResponse.Data;
                    response.Success = true;
                }

            });
        }

        public Response DeleteStockTransfer(int orderId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var accountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.DeleteStockTransfer(orderId, configurations);
                if (sendResponse.Success)
                {
                    response.Success = true;
                }

            });
        }

        public DataResponse<int> ReadStock(string stockCode, string warehouseId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                var accountingcompany = _unitOfWork
                        .AccountingCompanyRepository
                        .DbSet()
                        .Include(x => x.AccountingConfigurations)
                        .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);

                response = accountingProviderService.ReadStock(stockCode, warehouseId);
            });
        }

        public DataResponse<bool> CreateStocktaking(int stocktakingId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>((response) =>
            {
                var accountingcompany = this.
                    _unitOfWork
                    .AccountingCompanyRepository
                    .DbSet()
                    .Include(x => x.AccountingConfigurations)
                    .FirstOrDefault();

                if (accountingcompany == null)
                {
                    response.Success = false;
                    response.Message = "Muhasebe sağlayıcısı bulunamadı.";
                    return;
                }

                var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

                IAccountingProvider accountingProviderService = _accountingProviderServiceResolver(accountingcompany.AccountingId);
                var sendResponse = accountingProviderService.CreateStocktaking(stocktakingId, configurations);

                response.Data = sendResponse.Data;
                response.Success = sendResponse.Success;
                response.Message = sendResponse.Message;
            }, (r, e) =>
            {

            });
        }
        #endregion
    }
}
