﻿using Dapper;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.DataTransferObjects.Widgets;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters.Widgets;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Interfaces.Services;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class WidgetService : IWidgetService
    {
        #region Fields
        private readonly ISettingService _settingService;

        private readonly ICompanyFinder _companyFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ITenantFinder _tenantFinder;

        private readonly IOrderService _orderService;
        #endregion

        #region Constructors
        public WidgetService(IOrderService orderService, ISettingService settingService, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder)
        {
            #region Fields
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._settingService = settingService;
            this._orderService = orderService;
            #endregion
        }
        #endregion

        #region Methodss
        private DataResponse<List<MarketplaceCount>> GetMarketplaceCounts(WitgetRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceCount>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Select		M.Id As MarketplaceId,
			Count(O.Id) As [Count],
			Sum(IsNull(O.ExchangeRate * O.Total, 0)) As SalesAmount

From		Marketplaces As M With(NoLock)

Left Join	Orders As O With(NoLock)
On			O.MarketplaceId = M.Id
			And Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
			And O.TenantId = @TenantId
			And O.DomainId = @DomainId
			And O.CompanyId = @CompanyId
			And O.OrderTypeId <> 'IP'
            And O.OrderTypeId <> 'IA'

Group By	M.Id
Order By	Count(O.Id) Desc";

                    response.Data = sqlConnection
                        .Query<MarketplaceCount>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private DataResponse<List<MarketplaceProductCount>> GetMarketplaceProductCounts(WitgetRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<MarketplaceProductCount>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Select		M.Id As MarketplaceId,
			Count(PIM.Id) As [Count]
From		Marketplaces As M With(NoLock)

Left Join	ProductInformationMarketplaces As PIM With(NoLock)
On			PIM.MarketplaceId = M.Id
			And PIM.TenantId = @TenantId
			And PIM.DomainId = @DomainId
			And PIM.CompanyId = @CompanyId
			And PIM.Opened = 1

Group By	M.Id
Order By	Count(PIM.Id) Desc";

                    response.Data = sqlConnection
                        .Query<MarketplaceProductCount>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        public DataResponse<List<SaleSummary>> GetFinancialSummary()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<SaleSummary>>>((response) =>
            {
                #region Fetch Data
                List<SaleSummary> saleSummaries = null;
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
Declare @Today Int, @Yesterday Int, @TheDayBefore Int

Set @Today = DatePart(Day, GetDate())
Set @Yesterday = DatePart(Day, DateAdd(Day, -1, GetDate()))
Set @TheDayBefore = DatePart(Day, DateAdd(Day, -2, GetDate()))

Select		Case DatePart(Day, O.OrderDate)
					When @Today
						Then 'T'
					When @Yesterday
						Then 'Y'
					When @TheDayBefore
						Then 'TDB'
				End As Day,
				DatePart(Hour, O.OrderDate) As Hour,
				Count(Distinct O.Id) As OrderCount,
				Sum(OD.Quantity) As ProductCount,
				IsNull(Sum(OD.Quantity * (O.ExchangeRate * OD.UnitPrice)), 0) As SalesAmount,
			    IsNull(Sum(OD.Quantity * OD.UnitCost), 0) As CostAmount
	From		Orders As O With(NoLock)
	Join	    OrderDetails As OD With(NoLock)
	On		    O.Id = OD.OrderId
	Where		O.TenantId = @TenantId
                And O.DomainId = @DomainId
                And O.CompanyId = @CompanyId
				And O.OrderTypeId <> 'IP'
				And O.OrderTypeId <> 'IA'
				And Cast(O.OrderDate As Date) > Cast(DateAdd(Day, -3, GetDate()) As Date)
	Group By	DatePart(Day, O.OrderDate),
				DatePart(Hour, O.OrderDate)";

                    saleSummaries = sqlConnection
                        .Query<SaleSummary>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId()
                        })
                        .ToList();
                }
                #endregion

                response.Data = saleSummaries;
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private DataResponse<OrderSummary> GetOrderSummary(WitgetRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<OrderSummary>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
With T As (
	Select  Count(0) As [OrdersCount]
	From    Orders As O With(NoLock)
    Where   Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		    And O.TenantId = @TenantId
		    And O.DomainId = @DomainId
		    And O.CompanyId = @CompanyId
            And O.OrderTypeId <> 'IP'
            And O.OrderTypeId <> 'IA'
), T2 As (
	Select  Count(0) As [PackedOrdersCount]
	From    Orders As O With(NoLock)
    Where   Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
		    And O.TenantId = @TenantId
		    And O.DomainId = @DomainId
		    And O.CompanyId = @CompanyId
            And O.OrderTypeId In ('KTE', 'TA', 'TS')
)
Select	(Select [OrdersCount] From T) As [OrdersCount], (Select [PackedOrdersCount] From T2) As [PackedOrdersCount]";

                    response.Data = sqlConnection
                        .QueryFirstOrDefault<OrderSummary>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        });
                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        private DataResponse<List<TopSeller>> GetTopSellers(WitgetRequest request)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<TopSeller>>>((response) =>
            {
                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"
With T As (
	Select		OD.ProductInformationId,
				Sum(OD.Quantity) [Quantity],
				Sum(OD.UnitPrice * (O.ExchangeRate * OD.Quantity)) SalesAmount
	From		Orders As O With(NoLock)
	Join		OrderDetails As OD With(NoLock)
	On			O.Id = OD.OrderId
	Where		Cast(O.OrderDate As Date) Between Cast(@StartDate As Date) And Cast(@EndDate As Date)
				And O.TenantId = @TenantId
				And O.DomainId = @DomainId
				And O.CompanyId = @CompanyId
				And O.OrderTypeId <> 'IP'
                And O.OrderTypeId <> 'IA'
	Group By	OD.ProductInformationId
), T2 As (
	Select	Top 5	ProductInformationId,
                    Quantity,
					SalesAmount
	From			T
	Order By		Quantity Desc
)
Select	    T2.ProductInformationId,
			PN.Stock,
		    PIT.[Name]+ ' ' + ISNULL(PIT.VariantValuesDescription,'') AS Name,
			PIPR.UnitPrice,
            Quantity,
			SalesAmount,
		    Min(PIP.[FileName]) As [FileName]
From	    T2
Join		ProductInformations As PN With(NoLock)
On			T2.ProductInformationId = PN.Id
Join	    ProductInformationTranslations As PIT With(NoLock)
On		    T2.ProductInformationId = PIT.ProductInformationId
		    And PIT.LanguageId = 'TR'
Join		ProductInformationPriceses As PIPR With(NoLock)
On			T2.ProductInformationId = PIPR.ProductInformationId
			And PIPR.CurrencyId = 'TL'
Join	    ProductInformationPhotos As PIP
On		    T2.ProductInformationId = PIP.ProductInformationId
Group By    T2.ProductInformationId,
			PN.Stock,
		    PIT.[Name],
			PIPR.UnitPrice,
            Quantity,
			SalesAmount,
            PIT.VariantValuesDescription
Order By	Quantity Desc";

                    response.Data = sqlConnection
                        .Query<TopSeller>(query, new
                        {
                            TenantId = this._tenantFinder.FindId(),
                            DomainId = this._domainFinder.FindId(),
                            CompanyId = this._companyFinder.FindId(),
                            request.StartDate,
                            request.EndDate
                        })
                        .ToList();

                    if (response.Data != null && response.Data.Count > 0)
                        response.Data.ForEach(d => d.FileName = $"{this._settingService.ProductImageUrl}/{d.FileName}");

                    response.Success = true;
                    response.Message = "Widget oluşturuldu.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            });
        }

        public DataResponse<Widget> Read(WitgetRequest request, string userRole)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Widget>>((response) =>
            {
                if (userRole == "sales")
                {
                    response.Data = new Widget();
                    response.Success = true;

                    return;
                }

                bool isManager = userRole == "manager";

                response.Data = new Widget
                {
                    MarketplaceCounts = GetMarketplaceCounts(request).Data,
                    MarketplaceProductCounts = GetMarketplaceProductCounts(request).Data,
                    OrderSummary = this._orderService.Summary().Data,
                    TopSellers = GetTopSellers(request).Data,
                };

                response.Success = true;
            });
        }
        #endregion
    }
}
