﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Wrappers;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ECommerce.Persistence.Common.Services
{
    public class CaptchaService : ICaptchaService
    {
        #region Methods
        public Response Verify<T>(CaptchaRequest<T> captchaRequest)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                if (string.IsNullOrEmpty(captchaRequest.CaptchaResponse))
                {
                    response.Success = false;
                    response.Message = "Lütfen captcha'yı doğrulayınız.";

                    return;
                }

                var client = new WebClient();
                var responseString = client.DownloadString(
                        String.Format(
                            "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}",
                            "6LdHTp4eAAAAALsFFbgD1IQpPA9Gm3sh4yug00N3",
                            captchaRequest.CaptchaResponse
                            )
                    );
                var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(responseString);
                if (!captchaResponse.Success)
                {
                    response.Success = false;
                    response.Message = "Lütfen captcha'yı doğrulayınız.";

                    return;
                }

                response.Success = true;
            });
        }
        #endregion
    }
}
