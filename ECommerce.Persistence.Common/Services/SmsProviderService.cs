﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class SmsProviderService : ISmsService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly SmsProviderResolver _smsProviderServiceResolver;
        #endregion

        #region Constructors
        public SmsProviderService(IUnitOfWork unitOfWork, SmsProviderResolver smsProviderServiceResolver)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _smsProviderServiceResolver = smsProviderServiceResolver;
            #endregion
        }
        #endregion

        #region Methods
        public Response Send(string phoneNumber, string orderTypeId, string paymentTypeId, Dictionary<string, string> parameters)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var smsProviderCompanies =
                    _unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .ToList();

                if (smsProviderCompanies == null || smsProviderCompanies.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Sms sağlayıcı bulunamadı.";
                    return;
                }

                var smsTemplate =
                    _unitOfWork
                    .SmsTemplateRepository
                    .DbSet()
                    .FirstOrDefault(x => x.OrderTypeId == orderTypeId && x.PaymentTypeId == paymentTypeId);

                if (smsTemplate == null)
                {
                    response.Success = false;
                    response.Message = "Sms şablonu bulunamadı.";
                    return;
                }

                var message = smsTemplate.Template;

                if (parameters != null)
                    foreach (var pLoop in parameters)
                        message = message.Replace(pLoop.Key, pLoop.Value);

                foreach (var spcLoop in smsProviderCompanies)
                {
                    ISmsProvider smsProviderService = _smsProviderServiceResolver(spcLoop.SmsProviderId);
                    var sendResponse = smsProviderService.Send(phoneNumber, message);
                    if (sendResponse.Success)
                    {
                        response = sendResponse;
                        response.Success = true;
                        break;
                    }
                }
            });
        }

        public Response Send(string phoneNumber, string message)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var smsProviderCompanies =
                    _unitOfWork
                    .SmsProviderCompanyRepository
                    .DbSet()
                    .ToList();

                if (smsProviderCompanies == null || smsProviderCompanies.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Sms sağlayıcı bulunamadı.";
                    return;
                }


                foreach (var spcLoop in smsProviderCompanies)
                {
                    ISmsProvider smsProviderService = _smsProviderServiceResolver(spcLoop.SmsProviderId);
                    var sendResponse = smsProviderService.Send(phoneNumber, message);
                    if (sendResponse.Success)
                    {
                        response = sendResponse;
                        response.Success = true;
                        break;
                    }
                }
            });
        }
        #endregion
    }
}
