﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class PosService : IPosService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IEnumerable<IPosProvider> _posses;

        private readonly PosProviderResolver _posProviderResolver;
        #endregion

        #region Constructors
        public PosService(IUnitOfWork unitOfWork, IEnumerable<IPosProvider> posses, PosProviderResolver posProviderResolver)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._posses = posses;
            this._posProviderResolver = posProviderResolver;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<PaymentResponse> Check3d()
        {

            return ExceptionHandler.ResultHandle<DataResponse<PaymentResponse>>(((response) =>
            {
                var posProviders = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Where(x => x.Active)
                    .OrderByDescending(x => x.Default)
                    .ToList();

                if (posProviders == null || posProviders.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Pos sağlayıcı bulunamadı.";
                    return;
                }

                foreach (var pLoop in posProviders)
                {
                    IPosProvider posProvider = this._posProviderResolver((string)pLoop.Id);
                    var check3dResponse = posProvider.Check3d();
                    if (check3dResponse.Success)
                    {
                        response.Message = check3dResponse.Message;
                        response.Success = true;
                        response.Data = new PaymentResponse
                        {
                            OrderId = check3dResponse.Data.OrderId,
                            PaymentId = check3dResponse.Data.PaymentId,
                            AllowFrame = check3dResponse.Data.AllowFrame
                        };
                        break;
                    }
                    else
                    {
                        response.Data = new PaymentResponse
                        {
                            AllowFrame = check3dResponse.Data.AllowFrame
                        };
                        response.Message = check3dResponse.Message;
                        this.Log(check3dResponse.Data.PaymentId, string.Empty, check3dResponse.Message);

                    }

                }
            }));

        }

        public DataResponse<IPosProvider> Find(string creditCardNumber)
        {
            throw new NotImplementedException();
        }

        public DataResponse<Payment3DResponse> Pay3d(CreditCardPayment creditCardPayment)
        {

            return ExceptionHandler.ResultHandle<DataResponse<Payment3DResponse>>(((response) =>
            {
                var posProviders = this
                    ._unitOfWork
                    .PosRepository
                    .DbSet()
                    .Where(x => x.Active)
                    .OrderByDescending(x => x.Default)
                    .ToList();

                if (posProviders == null || posProviders.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Pos sağlayıcı bulunamadı.";
                    return;
                }

                foreach (var pLoop in posProviders)
                {
                    IPosProvider posProvider = this._posProviderResolver((string)pLoop.Id);
                    var providerResponse = posProvider.Pay3d(creditCardPayment);
                    if (providerResponse.Success)
                    {
                        response.Message = providerResponse.Message;
                        response.Success = true;
                        response.Data = new Payment3DResponse
                        {
                            Content = providerResponse.Data.Content,
                            ThreeDRedirect = providerResponse.Data.ThreeDRedirect,
                            AllowFrame = providerResponse.Data.AllowFrame
                        };
                        break;
                    }
                    else
                        this.Log(creditCardPayment.PaymentId, providerResponse.Data != null ? providerResponse.Data.Content : "", providerResponse.Message);

                }
            }));


        }
        #endregion

        #region Private Methods
        private void Log(int paymentId, string pay3dMessage, string check3dMessage)
        {
            try
            {
                var paymentLog = this
                    ._unitOfWork
                    .PaymentLogRepository
                    .DbSet()
                    .FirstOrDefault(pl => pl.Id == paymentId);
                if (paymentLog == null)
                {
                    this
                        ._unitOfWork
                        .PaymentLogRepository
                        .Create(new Domain.Entities.PaymentLog
                        {
                            Id = paymentId,
                            CreatedDate = DateTime.Now,
                            Pay3dMessage = pay3dMessage,
                            Check3dMessage = check3dMessage
                        });
                }
                else
                {
                    paymentLog.Pay3dMessage ??= pay3dMessage;
                    paymentLog.Check3dMessage ??= check3dMessage;

                    this
                        ._unitOfWork
                        .PaymentLogRepository
                        .Update(paymentLog);
                }
            }
            catch { }
        }
        #endregion
    }
}
