﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace ECommerce.Persistence.Common.Services
{
    public class SettingService : ISettingService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public SettingService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IConfiguration configuration, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Properties
        public string ConnectionString
        {
            get
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                return string.Format(connectionStringTemplate, dbName);
            }
        }

        public bool IllegalSale => true;

        public bool BarcodeWritable => true;

        public bool Waybill => true;

        public string ImagePath => this.Get().Data.Domains.First().DomainSetting.ImagePath;

        public string ProductImagePath => $"{this.Get().Data.Domains.First().DomainSetting.ImagePath}\\product";

        public string ProductImageThumbPath => $"{this.Get().Data.Domains.First().DomainSetting.ImagePath}\\product\\t";

        public string CategoryImagePath => $"{this.Get().Data.Domains.First().DomainSetting.ImagePath}\\category";

        public string ShowcaseImagePath => $"{this.Get().Data.Domains.First().DomainSetting.ImagePath}";

        public string CompanyImagePath => $"{this.Get().Data.Domains.First().DomainSetting.ImagePath}\\company";

        public string ImageUrl => this.Get().Data.Domains.First().DomainSetting.ImageUrl;

        public string ProductImageUrl => $"{this.Get().Data.Domains.First().DomainSetting.ImageUrl}/product";

        public string ProductImageThumbUrl => $"{this.Get().Data.Domains.First().DomainSetting.ImageUrl}/product/t";

        public string CategoryImageUrl => $"{this.Get().Data.Domains.First().DomainSetting.ImageUrl}/product";

        public string ShowcaseImageUrl => $"{this.Get().Data.Domains.First().DomainSetting.ImageUrl}";

        public string CompanyImageUrl => $"{this.Get().Data.Domains.First().DomainSetting.ImageUrl}/company";

        public string DefaultLanguageId => this.Get().Data.Domains.First().Companies.First().CompanySetting.DefaultLanguageId;

        public string DefaultCurrencyId => this.Get().Data.Domains.First().Companies.First().CompanySetting.DefaultCurrencyId;

        public int DefaultCountryId => this.Get().Data.Domains.First().Companies.First().CompanySetting.DefaultCountryId;

        public bool IncludeMarketplace => this.Get().Data.Domains.First().Companies.First().CompanySetting.IncludeMarketplace;

        public bool IncludeWMS => this.Get().Data.Domains.First().Companies.First().CompanySetting.IncludeWMS;

        public bool IncludeECommerce => this.Get().Data.Domains.First().Companies.First().CompanySetting.IncludeECommerce;

        public bool IncludeAccounting => this.Get().Data.Domains.First().Companies.First().CompanySetting.IncludeAccounting;

        public bool IncludeExport => this.Get().Data.Domains.First().Companies.First().CompanySetting.IncludeExport;

        public bool CreateNonexistProduct => this.Get().Data.Domains.First().Companies.First().CompanySetting.CreateNonexistProduct;

        public bool StockControl => this.Get().Data.Domains.First().Companies.First().CompanySetting.StockControl;

        public string UUId => this.Get().Data.Domains.FirstOrDefault()?.Companies.FirstOrDefault()?.CompanySetting.UUId;

        public string WebUrl => this.Get().Data.Domains.FirstOrDefault()?.Companies.FirstOrDefault()?.CompanySetting.WebUrl;

        public string Theme => this.Get().Data.Domains.FirstOrDefault()?.Companies.FirstOrDefault()?.CompanySetting.Theme;

        public bool OrderDetailMultiselect => this.Get().Data.Domains.First().Companies.First().CompanySetting.OrderDetailMultiselect;

        public string Tenant => this._dbNameFinder.FindName();
        #endregion

        #region Helper Methods
        private DataResponse<Tenant> Get()
        {
            var dbName = this._dbNameFinder.FindName();
            var tenantId = this._tenantFinder.FindId();
            var domainId = this._domainFinder.FindId();
            var companyId = this._companyFinder.FindId();

            var response = this._cacheHandler.ResponseHandle<DataResponse<Tenant>>(() =>
            {
                var result = ExceptionHandler.ResultHandle<DataResponse<Tenant>>((response) =>
                {
                    response.Data = this
                        ._unitOfWork
                        .TenantRepository
                        .DbSet()

                        .Include(t => t.TenantSetting)
                        .Include(t => t.Domains)
                        .ThenInclude(d => d.DomainSetting)

                        .Include(t => t.Domains)
                        .ThenInclude(d => d.Companies)
                        .ThenInclude(c => c.CompanySetting)

                        .Where(t => t.Id == tenantId)
                        .Select(t => new Tenant
                        {
                            Id = t.Id,
                            Name = t.Name,
                            TenantSetting = new()
                            {
                            },
                            Domains = t
                                .Domains
                                .Where(d => d.Id == domainId)
                                .Select(d => new Application.Common.DataTransferObjects.Domain
                                {
                                    Id = d.Id,
                                    Name = d.Name,
                                    DomainSetting = new()
                                    {
                                        ImagePath = d.DomainSetting.ImagePath,
                                        ImageUrl = d.DomainSetting.ImageUrl
                                    },
                                    Companies = d
                                        .Companies
                                        .Where(c => c.Id == companyId)
                                        .Select(c => new Company
                                        {
                                            Id = c.Id,
                                            Name = c.Name,
                                            FullName = c.FullName,
                                            FileName = c.FileName,
                                            CompanySetting = new()
                                            {
                                                CreateNonexistProduct = c.CompanySetting.CreateNonexistProduct,
                                                DefaultCountryId = c.CompanySetting.DefaultCountryId,
                                                DefaultCurrencyId = c.CompanySetting.DefaultCurrencyId,
                                                DefaultLanguageId = c.CompanySetting.DefaultLanguageId,
                                                IncludeMarketplace = c.CompanySetting.IncludeMarketplace,
                                                IncludeWMS = c.CompanySetting.IncludeWMS,
                                                IncludeECommerce = c.CompanySetting.IncludeECommerce,
                                                IncludeExport = c.CompanySetting.IncludeExport,
                                                IncludeAccounting = c.CompanySetting.IncludeAccounting,
                                                StockControl = c.CompanySetting.StockControl,
                                                Theme = c.CompanySetting.Theme,
                                                UUId = c.CompanySetting.UUId,
                                                WebUrl = c.CompanySetting.WebUrl,
                                                OrderDetailMultiselect = c.CompanySetting.OrderDetailMultiselect
                                            }
                                        })
                                        .ToList()
                                })
                                .ToList()
                        })
                        .FirstOrDefault();

                    response.Success = true;
                });
                return result;

            }, String.Format(CacheKey.Setting, dbName, tenantId, domainId, companyId), 60);

            return response;
        }
        #endregion
    }
}