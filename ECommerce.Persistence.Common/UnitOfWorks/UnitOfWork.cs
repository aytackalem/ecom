﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Context;
using ECommerce.Repository;

namespace ECommerce.Persistence.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly ApplicationDbContext _dbContext;

        private IProductGenericPropertyRepository _productGenericPropertyRepository;

        private IWholesaleOrderRepository _wholesaleOrderRepository;

        private IWholesaleOrderTypeRepository _wholesaleOrderTypeRepository;

        private IProductCategorizationRepository _productCategorizationRepository;

        private IOrderPickingRepository _orderPickingRepository;

        private IOrderReserveRepository _orderReserveRepository;

        private IStocktakingTypeRepository _stocktakingTypeRepository;

        private IStocktakingRepository _stocktakingRepository;

        private IECommerceCategoryRepository _ecommerceCategoryRepository;

        private IECommerceBrandRepository _ecommerceBrandRepository;

        private IECommerceVariantRepository _ecommerceVariantRepository;

        private IECommerceVariantValueRepository _ecommerceVariantValueRepository;

        private ITenantPaymentRepository _tenantPaymentRepository;

        private IPaymentLogRepository _paymentLogRepository;

        private IFeedRepository _feedRepository;

        private IFeedTemplateRepository _feedTemplateRepository;

        private IProductMarketplaceRepository _productMarketplaceRepository;

        private IMarketplaceVariantValueMappingRepository _marketplaceVariantValueMappingRepository;

        private IOrderViewingRepository _orderViewingRepository;

        private IExpenseRepository _expenseRepository;

        private IExpenseSourceRepository _expenseSourceRepository;

        private IExchangeRateRepository _exchangeRateRepository;

        private IOrderPackingRepository _orderPackingRepository;

        private IDomainRepository _domainRepository;

        private IApplicationRepository _applicationRepository;

        private IBankRepository _bankRepository;

        private IBinRepository _binRepository;

        private IBrandDiscountSettingRepository _brandDiscountSettingRepository;

        private IBrandRepository _brandRepository;

        private ICompanyConfigurationRepository _companyConfigurationRepository;

        private ISupplierRepository _supplierRepository;

        private ICategoryRepository _categoryRepository;

        private ICategoryProductRepository _categoryProductRepository;

        private ICustomerAddressRepository _customerAddressRepository;

        private ICountryRepository _countryRepository;

        private ICustomerInvoiceInformationRepository _customerInvoiceInformationRepository;

        private ILabelRepository _labelRepository;

        private IVariantRepository _variantRepository;

        private IVariantValueRepository _variantValueRepository;

        private ILanguageRepository _languageRepository;

        private IProductRepository _productRepository;

        private IProductInformationMarketplaceBulkPriceRepository _productInformationMarketplaceBulkPriceRepository;

        private ICurrencyRepository _currencyRepository;

        private IProductInformationCommentRepository _productInformationCommentRepository;

        private IProductInformationContentTranslationRepository _productInformationContentTranslationRepository;

        private IDiscountCouponRepository _discountCouponRepository;

        private IGetXPayYSettingRepository _getXPayYSettingRepository;

        private IEntireDiscountRepository _entireDiscountRepository;

        private ICategoryDiscountSettingRepository _categoryDiscountSettingRepository;

        private ICategoryTranslationBreadcrumbRepository _categoryTranslationBreadcrumbRepository;

        private ICategoryTranslationRepository _categoryTranslationRepository;

        private IPosRepository _posRepository;

        private IPosConfigurationRepository _posConfigurationRepository;

        private IPaymentTypeDomainRepository _paymentTypeDomainRepository;

        private IPaymentTypeRepository _paymentTypeRepository;

        private IShoppingCartDiscountedProductRepository _shoppingCartDiscountedProductRepository;

        private IMarketplaceRepository _marketPlaceRepository;

        private IMarketPlaceCompanyRepository _marketPlaceCompanyRepository;

        private IMarketplaceConfigurationRepository _marketplaceConfigurationRepository;

        private IProductInformationMarketplaceRepository _productInformationMarketplaceRepository;

        private ISmsProviderRepository _smsProviderRepository;

        private ISmsProviderCompanyRepository _smsProviderCompanyRepository;

        private ISmsProviderConfigurationRepository _smsProviderConfigurationRepository;

        private ISmsTemplateRepository _smsTemplateRepository;

        private IEmailProviderCompanyRepository _emailProviderCompanyRepository;

        private IEmailProviderRepository _emailProviderRepository;

        private IEmailProviderConfigurationRepository _emailProviderConfigurationRepository;

        private IShipmentCompanyRepository _shipmentCompanyRepository;

        private IShipmentCompanyCompanyRepository _shipmentCompanyCompanyRepository;

        private IShipmentCompanyConfigurationRepository _shipmentCompanyConfigurationRepository;

        private IProductPropertyRepository _productPropertyRepository;

        private IProductPropertyTranslationRepository _productPropertyTranslationRepository;

        private IProductInformationTranslationRepository _productInformationTranslationRepository;

        private IProductInformationTranslationBreadcrumbRepository _productInformationTranslationBreadcrumbRepository;

        private IProductPropertyValueRepository _productPropertyValueRepository;

        private IProductPropertyValueTranslationRepository _productPropertyValueTranslationRepository;

        private IMoneyPointSettingRepository _moneyPointSettingRepository;

        private IMoneyPointRepository _moneyPointRepository;

        private IInformationTranslationRepository _informationTranslationRepository;

        private IContentTranslationRepository _contentTranslationRepository;

        private IInformationRepository _informationRepository;

        private IFrequentlyAskedQuestionRepository _frequentlyAskedQuestionRepository;

        private IContactRepository _contactRepository;

        private IConfigurationRepository _configurationRepository;

        private IGoogleConfigrationRepository _googleConfigrationRepository;

        private IFacebookConfigrationRepository _facebookConfigrationRepository;

        private IContentRepository _contentRepository;

        private ICityRepository _cityRepository;

        private INeighborhoodRepository _neighborhoodRepository;

        private IDistrictRepository _districtRepository;

        private IPropertyRepository _propertyRepository;

        private IShowcaseRepository _showcaseRepository;

        private IProductInformationRepository _productInformationRepository;

        private IProductInformationSubscriptionRepository _productInformationSubscriptionRepository;

        private ICustomerUserRepository _customerUserRepository;

        private ITwoFactorCustomerUserRepository _twoFactorCustomerUserRepository;

        private IOrderRepository _orderRepository;

        private IOrderTypeRepository _orderTypeRepository;

        private IOrderBillingRepository _orderBillingRepository;

        private IOrderDetailRepository _orderDetailRepository;

        private IOrderTechnicInformationRepository _orderTechnicInformationRepository;

        private IOrderInvoiceInformationRepository _orderInvoiceInformationRepository;

        private INebimOrderRepository _orderNebimOrderRepository;

        private INebimMarketplaceCreditCardCodeRepository _nebimMarketplaceCreditCardCodeRepository;

        private INebimMarketplaceSalesPersonnelCodeRepository _nebimMarketplaceSalesPersonnelCodeRepository;

        private IOrderShipmentRepository _orderShipmentRepository;

        private ICustomerRepository _customerRepository;

        private ICustomerRoleRepository _customerRoleRepository;

        private ICustomerContactRepository _customerContactRepository;

        private IPaymentRepository _paymentRepository;

        private IProductInformationPriceRepository _productInformationPriceRepository;

        private IProductInformationPhotoRepository _productInformationPhotoRepository;

        private IProductInformationMarketplaceVariantValueRepository _productInformationMarketplaceVariantValueRepository;

        private IAccountingCompanyConfigurationRepository _accountingCompanyConfigurationRepository;

        private IAccountingRepository _accountingRepository;

        private IAccountingCompanyRepository _accountingCompanyRepository;

        private IProductLabelRepository _productLabelRepository;

        private IProductInformationVariantRepository _productInformationVariantRepository;

        private IProductInformationCombineRepository _productInformationCombineRepository;

        private INewBulletinRepository _newBulletinRepository;

        private INewBulletinInformationRepository _newBulletinInformationRepository;

        private IMarketplaceCategoryMappingRepository _marketplaceCategoryMappingRepository;

        private IMarketplaceBrandMappingRepository _marketplaceBrandMappingRepository;

        //private IMarketplaceCategoriesMarketplaceVariantRepository _marketplaceCategoriesMarketplaceVariantRepository;

        //private IMarketplaceCategoriesMarketplaceVariantValueRepository _marketplaceCategoriesMarketplaceVariantValueRepository;



        private IUpsAreaRepository _upsAreaRepository;

        private IUpsCityRepository _upsCityRepository;

        private ICompanyRepository _companyRepository;

        private IOrderSourceRepository _orderSourceRepository;

        private ITenantRepository _tenantRepository;

        private IProductSourceDomainRepository _productSourceDomainRepository;

        private IReceiptRepository _receiptRepository;
        #endregion

        #region Constructors
        public UnitOfWork(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder)
        {
            #region Fields
            this._dbContext = dbContext;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Properties
        public IProductGenericPropertyRepository ProductGenericPropertyRepository => _productGenericPropertyRepository ??= new ProductGenericPropertyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductCategorizationRepository ProductCategorizationRepository => _productCategorizationRepository ??= new ProductCategorizationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IWholesaleOrderTypeRepository WholesaleOrderTypeRepository => _wholesaleOrderTypeRepository ??= new WholesaleOrderTypeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderPickingRepository OrderPickingRepository => _orderPickingRepository ??= new OrderPickingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderReserveRepository OrderReserveRepository => _orderReserveRepository ??= new OrderReserveRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IStocktakingRepository StocktakingRepository => _stocktakingRepository ??= new StocktakingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IStocktakingTypeRepository StocktakingTypeRepository => _stocktakingTypeRepository ??= new StocktakingTypeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IECommerceCategoryRepository ECommerceCategoryRepository => _ecommerceCategoryRepository ??= new ECommerceCategoryRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IECommerceBrandRepository ECommerceBrandRepository => _ecommerceBrandRepository ??= new ECommerceBrandRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IECommerceVariantRepository ECommerceVariantRepository => _ecommerceVariantRepository ??= new ECommerceVariantRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IECommerceVariantValueRepository ECommerceVariantValueRepository => _ecommerceVariantValueRepository ??= new ECommerceVariantValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ITenantPaymentRepository TenantPaymentRepository => _tenantPaymentRepository ??= new TenantPaymentRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPaymentLogRepository PaymentLogRepository => _paymentLogRepository ??= new PaymentLogRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IFeedRepository FeedRepository => _feedRepository ??= new FeedRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IFeedTemplateRepository FeedTemplateRepository => _feedTemplateRepository ??= new FeedTemplateRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IWholesaleOrderRepository WholesaleOrderRepository=> _wholesaleOrderRepository ??= new WholesaleOrderRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductMarketplaceRepository ProductMarketplaceRepository => _productMarketplaceRepository ??= new ProductMarketplaceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMarketplaceVariantValueMappingRepository MarketplaceVariantValueMappingRepository => _marketplaceVariantValueMappingRepository ??= new MarketplaceVariantValueMappingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderViewingRepository OrderViewingRepository => _orderViewingRepository ??= new OrderViewingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IExpenseRepository ExpenseRepository => _expenseRepository ??= new ExpenseRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationPriceRepository ProductInformationPriceRepository => _productInformationPriceRepository ??= new ProductInformationPriceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IExpenseSourceRepository ExpenseSourceRepository => _expenseSourceRepository ??= new ExpenseSourceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IExchangeRateRepository ExchangeRateRepository => _exchangeRateRepository ??= new ExchangeRateRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICompanyConfigurationRepository CompanyConfigurationRepository => _companyConfigurationRepository ??= new CompanyConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderPackingRepository OrderPackingRepository => _orderPackingRepository ??= new OrderPackingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IDomainRepository DomainRepository => _domainRepository ??= new DomainRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IAccountingCompanyConfigurationRepository AccountingCompanyConfigurationRepository => _accountingCompanyConfigurationRepository ??= new AccountingCompanyConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IAccountingRepository AccountingRepository => _accountingRepository ??= new AccountingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IAccountingCompanyRepository AccountingCompanyRepository => _accountingCompanyRepository ??= new AccountingCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IApplicationRepository ApplicationRepository => _applicationRepository ??= new ApplicationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IBankRepository BankRepository => _bankRepository ??= new BankRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IBinRepository BinRepository => _binRepository ??= new BinRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IBrandDiscountSettingRepository BrandDiscountSettingRepository => _brandDiscountSettingRepository ??= new BrandDiscountSettingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IBrandRepository BrandRepository => _brandRepository ??= new BrandRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ISupplierRepository SupplierRepository => _supplierRepository ??= new SupplierRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICategoryRepository CategoryRepository => _categoryRepository ??= new CategoryRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICategoryProductRepository CategoryProductRepository => _categoryProductRepository ??= new CategoryProductRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerAddressRepository CustomerAddressRepository => _customerAddressRepository ??= new CustomerAddressRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICountryRepository CountryRepository => _countryRepository ??= new CountryRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerInvoiceInformationRepository CustomerInvoiceInformationRepository => _customerInvoiceInformationRepository ??= new CustomerInvoiceInformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ILabelRepository LabelRepository => _labelRepository ??= new LabelRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IVariantRepository VariantRepository => _variantRepository ??= new VariantRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IVariantValueRepository VariantValueRepository => _variantValueRepository ??= new VariantValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ILanguageRepository LanguageRepository => _languageRepository ??= new LanguageRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductRepository ProductRepository => _productRepository ??= new ProductRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationMarketplaceBulkPriceRepository ProductInformationMarketplaceBulkPriceRepository => _productInformationMarketplaceBulkPriceRepository ??= new ProductInformationMarketplaceBulkPriceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public ICurrencyRepository CurrencyRepository => _currencyRepository ??= new CurrencyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationCommentRepository ProductInformationCommentRepository => _productInformationCommentRepository ??= new ProductInformationCommentRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IProductInformationContentTranslationRepository ProductInformationContentTranslationRepository => _productInformationContentTranslationRepository ??= new ProductInformationContentTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IDiscountCouponRepository DiscountCouponRepository => _discountCouponRepository ??= new DiscountCouponRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IGetXPayYSettingRepository GetXPayYSettingRepository => _getXPayYSettingRepository ??= new GetXPayYSettingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IEntireDiscountRepository EntireDiscountRepository => _entireDiscountRepository ??= new EntireDiscountRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICategoryDiscountSettingRepository CategoryDiscountSettingRepository => _categoryDiscountSettingRepository ??= new CategoryDiscountSettingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICategoryTranslationBreadcrumbRepository CategoryTranslationBreadcrumbRepository => _categoryTranslationBreadcrumbRepository ??= new CategoryTranslationBreadcrumbRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICategoryTranslationRepository CategoryTranslationRepository => _categoryTranslationRepository ??= new CategoryTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPosRepository PosRepository => _posRepository ??= new PosRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPosConfigurationRepository PosConfigurationRepository => _posConfigurationRepository ??= new PosConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPaymentTypeDomainRepository PaymentTypeDomainRepository => _paymentTypeDomainRepository ??= new PaymentTypeDomainRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPaymentTypeRepository PaymentTypeRepository => _paymentTypeRepository ??= new PaymentTypeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IShoppingCartDiscountedProductRepository ShoppingCartDiscountedProductRepository => _shoppingCartDiscountedProductRepository ??= new ShoppingCartDiscountedProductRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMarketplaceRepository MarketPlaceRepository => _marketPlaceRepository ??= new MarketPlaceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMarketPlaceCompanyRepository MarketplaceCompanyRepository => _marketPlaceCompanyRepository ??= new MarketPlaceCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IMarketplaceConfigurationRepository MarketplaceConfigurationRepository => _marketplaceConfigurationRepository ??= new MarketplaceConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IProductInformationMarketplaceRepository ProductInformationMarketplaceRepository => _productInformationMarketplaceRepository ??= new ProductInformationMarketplaceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        //public IMarketplaceVariantRepository MarketplaceVariantRepository => _marketPlaceVariantRepository ??= new MarketplaceVariantRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        //public IMarketplaceVariantValueRepository MarketplaceVariantValueRepository => _marketplaceVariantValueRepository ??= new MarketplaceVariantValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        //public IMarketplaceVariantsMarketplaceVariantValueRepository MarketplaceVariantsMarketplaceVariantValueRepository => _marketplaceVariantsMarketplaceVariantValueRepository ??= new MarketplaceVariantsMarketplaceVariantValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ISmsProviderRepository SmsProviderRepository => _smsProviderRepository ??= new SmsProviderRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ISmsProviderCompanyRepository SmsProviderCompanyRepository => _smsProviderCompanyRepository ??= new SmsProviderCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public ISmsProviderConfigurationRepository SmsProviderConfigurationRepository => _smsProviderConfigurationRepository ??= new SmsProviderConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ISmsTemplateRepository SmsTemplateRepository => _smsTemplateRepository ??= new SmsTemplateRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IEmailProviderCompanyRepository EmailProviderCompanyRepository => _emailProviderCompanyRepository ??= new EmailProviderCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IEmailProviderRepository EmailProviderRepository => _emailProviderRepository ??= new EmailProviderRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IEmailProviderConfigurationRepository EmailProviderConfigurationRepository => _emailProviderConfigurationRepository ??= new EmailProviderConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IShipmentCompanyRepository ShipmentCompanyRepository => _shipmentCompanyRepository ??= new ShipmentCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IShipmentCompanyCompanyRepository ShipmentCompanyCompanyRepository => _shipmentCompanyCompanyRepository ??= new ShipmentCompanyCompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IShipmentCompanyConfigurationRepository ShipmentCompanyConfigurationRepository => _shipmentCompanyConfigurationRepository ??= new ShipmentCompanyConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductPropertyRepository ProductPropertyRepository => _productPropertyRepository ??= new ProductPropertyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductPropertyTranslationRepository ProductPropertyTranslationRepository => _productPropertyTranslationRepository ??= new ProductPropertyTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationTranslationRepository ProductInformationTranslationRepository => _productInformationTranslationRepository ??= new ProductInformationTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationTranslationBreadcrumbRepository ProductInformationTranslationBreadcrumbRepository => _productInformationTranslationBreadcrumbRepository ??= new ProductInformationTranslationBreadcrumbRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductPropertyValueRepository ProductPropertyValueRepository => _productPropertyValueRepository ??= new ProductPropertyValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductPropertyValueTranslationRepository ProductPropertyValueTranslationRepository => _productPropertyValueTranslationRepository ??= new ProductPropertyValueTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMoneyPointSettingRepository MoneyPointSettingRepository => _moneyPointSettingRepository ??= new MoneyPointSettingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMoneyPointRepository MoneyPointRepository => _moneyPointRepository ??= new MoneyPointRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IInformationTranslationRepository InformationTranslationRepository => _informationTranslationRepository ??= new InformationTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IContentTranslationRepository ContentTranslationRepository => _contentTranslationRepository ??= new ContentTranslationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationPhotoRepository ProductInformationPhotoRepository => _productInformationPhotoRepository ??= new ProductInformationPhotoRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IProductInformationMarketplaceVariantValueRepository ProductInformationMarketplaceVariantValueRepository => _productInformationMarketplaceVariantValueRepository ??= new ProductInformationMarketplaceVariantValueRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IInformationRepository InformationRepository => _informationRepository ??= new InformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IFrequentlyAskedQuestionRepository FrequentlyAskedQuestionRepository => _frequentlyAskedQuestionRepository ??= new FrequentlyAskedQuestionRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IContactRepository ContactRepository => _contactRepository ??= new ContactRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IConfigurationRepository ConfigurationRepository => _configurationRepository ??= new ConfigurationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IGoogleConfigrationRepository GoogleConfigrationRepository => _googleConfigrationRepository ??= new GoogleConfigrationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IFacebookConfigrationRepository FacebookConfigrationRepository => _facebookConfigrationRepository ??= new FacebookConfigrationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IContentRepository ContentRepository => _contentRepository ??= new ContentRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICityRepository CityRepository => _cityRepository ??= new CityRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public INeighborhoodRepository NeighborhoodRepository => _neighborhoodRepository ??= new NeighborhoodRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IDistrictRepository DistrictRepository => _districtRepository ??= new DistrictRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPropertyRepository PropertyRepository => _propertyRepository ??= new PropertyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);



        public IShowcaseRepository ShowcaseRepository => _showcaseRepository ??= new ShowcaseRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationSubscriptionRepository ProductInformationSubscriptionRepository => _productInformationSubscriptionRepository ??= new ProductInformationSubscriptionRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerUserRepository CustomerUserRepository => _customerUserRepository ??= new CustomerUserRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ITwoFactorCustomerUserRepository TwoFactorCustomerUserRepository => _twoFactorCustomerUserRepository ??= new TwoFactorCustomerUserRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderRepository OrderRepository => _orderRepository ??= new OrderRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderTypeRepository OrderTypeRepository => _orderTypeRepository ??= new OrderTypeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);


        public IOrderBillingRepository OrderBillingRepository => _orderBillingRepository ??= new OrderBillingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository ??= new OrderDetailRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderTechnicInformationRepository OrderTechnicInformationRepository => _orderTechnicInformationRepository ??= new OrderTechnicInformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderInvoiceInformationRepository OrderInvoiceInformationRepository => _orderInvoiceInformationRepository ??= new OrderInvoiceInformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public INebimOrderRepository NebimOrderRepository => _orderNebimOrderRepository ??= new NebimOrderRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public INebimMarketplaceCreditCardCodeRepository NebimMarketplaceCreditCardCodeRepository => _nebimMarketplaceCreditCardCodeRepository ??= new NebimMarketplaceCreditCardCodeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public INebimMarketplaceSalesPersonnelCodeRepository NebimMarketplaceSalesPersonnelCodeRepository => _nebimMarketplaceSalesPersonnelCodeRepository ??= new NebimMarketplaceSalesPersonnelCodeRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderShipmentRepository OrderShipmentRepository => _orderShipmentRepository ??= new OrderShipmentRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerRepository CustomerRepository => _customerRepository ??= new CustomerRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerRoleRepository CustomerRoleRepository => _customerRoleRepository ??= new CustomerRoleRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ICustomerContactRepository CustomerContactRepository => _customerContactRepository ??= new CustomerContactRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IPaymentRepository PaymentRepository => _paymentRepository ??= new PaymentRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductLabelRepository ProductLabelRepository => _productLabelRepository ??= new ProductLabelRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public INewBulletinInformationRepository NewBulletinInformationRepository => _newBulletinInformationRepository ?? new NewBulletinInformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IMarketplaceCategoryMappingRepository MarketplaceCategoryMappingRepository => _marketplaceCategoryMappingRepository ?? new MarketplaceCategoryMappingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        public IMarketplaceBrandMappingRepository MarketplaceBrandMappingRepository => _marketplaceBrandMappingRepository ?? new MarketplaceBrandMappingRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);


        public ICompanyRepository CompanyRepository => _companyRepository ?? new CompanyRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationRepository ProductInformationRepository => _productInformationRepository ?? new ProductInformationRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationVariantRepository ProductInformationVariantRepository => _productInformationVariantRepository ?? new ProductInformationVariantRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductInformationCombineRepository ProductInformationCombineRepository => _productInformationCombineRepository ?? new ProductInformationCombineRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);


        public INewBulletinRepository NewBulletinRepository => _newBulletinRepository ?? new NewBulletinRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IUpsAreaRepository UpsAreaRepository => _upsAreaRepository ?? new UpsAreaRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IUpsCityRepository UpsCityRepository => _upsCityRepository ?? new UpsCityRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IOrderSourceRepository OrderSourceRepository => _orderSourceRepository ?? new OrderSourceRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public ITenantRepository TenantRepository => _tenantRepository ?? new TenantRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IProductSourceDomainRepository ProductSourceDomainRepository => _productSourceDomainRepository ?? new ProductSourceDomainRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);

        public IReceiptRepository ReceiptRepository => _receiptRepository ?? new ReceiptRepository(this._dbContext, this._tenantFinder, this._domainFinder, this._companyFinder);
        #endregion
    }
}