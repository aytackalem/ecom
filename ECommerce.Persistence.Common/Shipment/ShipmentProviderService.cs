﻿using ECommerce.Application.Common;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Linq;

namespace ECommerce.Persistence.Common.Shipment
{
    public class ShipmentProviderService : IShipmentProviderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ShipmentProviderServiceResolver _shipmentProviderServiceResolver;
        #endregion

        #region Constructors
        public ShipmentProviderService(IUnitOfWork unitOfWork, ShipmentProviderServiceResolver shipmentProviderServiceResolver)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _shipmentProviderServiceResolver = shipmentProviderServiceResolver;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ShipmentInfo> Post(int orderId, bool payor, int[] weights, int piece)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentInfo>>((response) =>
            {
                var orderShipment =
                    _unitOfWork
                    .OrderShipmentRepository
                    .DbSet()
                    .FirstOrDefault(x => x.OrderId == orderId);

                if (orderShipment == null)
                {
                    response.Success = false;
                    response.Message = "Kargo sağlayıcı bulunamadı.";
                    return;
                }

                var shipmentCompanyConfigurations =
                 _unitOfWork
                 .ShipmentCompanyConfigurationRepository
                 .DbSet()
                 .Where(x => x.ShipmentCompanyCompany.ShipmentCompanyId == orderShipment.ShipmentCompanyId)
                 .Select(x => new ShipmentCompanyConfiguration
                 {
                     Key = x.Key,
                     Value = x.Value,
                 })
                 .ToList();

                if (shipmentCompanyConfigurations?.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Kargo firması ayarları okunamadı.";
                    return;
                }

                var order = _unitOfWork
                .OrderRepository
                .DbSet()
                .Select(o => new Order
                {
                    Id = o.Id,
                    CargoFee = o.CargoFee,
                    Total = o.Total,
                    CurrencyId = o.CurrencyId,
                    OrderDetails = o.OrderDetails.Select(x => new OrderDetail
                    {
                        Quantity = x.Quantity,
                        CategoryName = x.ProductInformation.Product.Category.CategoryTranslations[0].Name
                    }).ToList(),
                    OrderDeliveryAddress = new OrderDeliveryAddress
                    {
                        Address = o.OrderDeliveryAddress.Address,
                        PhoneNumber = o.OrderDeliveryAddress.PhoneNumber,
                        Recipient = o.OrderDeliveryAddress.Recipient.Trim(),
                        Neighborhood = new Neighborhood
                        {
                            Name = o.OrderDeliveryAddress.Neighborhood.Name,
                            District = new District
                            {
                                Name = o.OrderDeliveryAddress.Neighborhood.District.Name,
                                City = new City
                                {
                                    Id = o.OrderDeliveryAddress.Neighborhood.District.City.Id,
                                    Name = o.OrderDeliveryAddress.Neighborhood.District.City.Name
                                }
                            }
                        }
                    },
                    Payments = o.Payments.Select(p => new Application.Common.DataTransferObjects.Payment
                    {
                        Amount = p.Amount,
                        PaymentTypeId = p.PaymentTypeId

                    }).ToList()
                }).FirstOrDefault(o => o.Id == orderId);

                if (!string.IsNullOrWhiteSpace(order.OrderDeliveryAddress.PhoneNumber) &&
                    !string.IsNullOrEmpty(order.OrderDeliveryAddress.PhoneNumber))
                    order.OrderDeliveryAddress.PhoneNumber = order.OrderDeliveryAddress.PhoneNumber.ToNumber();
                else
                    order.OrderDeliveryAddress.PhoneNumber = "5555555555";

                IShipmentProvider shipmentProviderService = _shipmentProviderServiceResolver(orderShipment.ShipmentCompanyId);
                var sendResponse = shipmentProviderService.Post(shipmentCompanyConfigurations, order, payor, weights, piece);
                if (sendResponse.Success)
                {

                    orderShipment.TrackingCode = sendResponse.Data.Barcode;
                    orderShipment.TrackingUrl = sendResponse.Data.TrackingUrl;
                    orderShipment.TrackingBase64 = sendResponse.Data.TrackingBase64;
                    orderShipment.OrderShipmentDetail = sendResponse.Data
                         .ShipmentInfoDetails
                         .Select(sid => new Domain.Entities.OrderShipmentDetail
                         {
                             TrackingCode = sid.TrackingCode,
                             Weight = "1"
                         })
                         .ToList();

                    _unitOfWork
                    .OrderShipmentRepository.Update(orderShipment);

                    response.Data = sendResponse.Data;
                    response.Success = true;
                }

            });
        }

        public DataResponse<ShipmentTracking> Track(int orderId, string trackingCode)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ShipmentTracking>>((response) =>
            {
                var orderShipment =
                    _unitOfWork
                    .OrderShipmentRepository
                    .DbSet()
                    .FirstOrDefault(x => x.OrderId == orderId);

                if (orderShipment == null)
                {
                    response.Success = false;
                    response.Message = "Kargo sağlayıcı bulunamadı.";
                    return;
                }

                var shipmentCompanyConfigurations =
                 _unitOfWork
                 .ShipmentCompanyConfigurationRepository
                 .DbSet()
                 .Where(x => x.ShipmentCompanyCompany.ShipmentCompanyId == orderShipment.ShipmentCompanyId)
                 .Select(x => new ShipmentCompanyConfiguration
                 {
                     Key = x.Key,
                     Value = x.Value,
                 })
                 .ToList();

                if (shipmentCompanyConfigurations?.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Kargo firması ayarları okunamadı.";
                    return;
                }

                IShipmentProvider shipmentProviderService = _shipmentProviderServiceResolver(orderShipment.ShipmentCompanyId);
                var trackResponse = shipmentProviderService.Track(shipmentCompanyConfigurations, trackingCode);

                response.Success = trackResponse.Success;
                response.Data = trackResponse.Data;
                response.Message = trackResponse.Message;
            });
        }
        #endregion
    }
}
