﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Persistence.Common.Helpers;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ECommerce.Persistence.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        
        #region Methods
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            //var appConfig = new AppConfig();

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            //var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();

            builder.UseSqlServer(configuration.GetConnectionString("Helpy"));

            return new ApplicationDbContext(builder.Options, null, configuration);
        }
        #endregion
    }
}
