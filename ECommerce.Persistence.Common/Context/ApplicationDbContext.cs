﻿using ECommerce.Application.Common.Interfaces.Context;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Domain.Entities;
using ECommerce.Domain.Entities.Base;
using ECommerce.Domain.Entities.Companyable;
using ECommerce.Domain.Entities.Domainable;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace ECommerce.Persistence.Context
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        #region Fields
        private readonly IDbNameFinder _dbNameFinder;

        private readonly IConfiguration _configuration;
        #endregion

        #region Properties
        public DbSet<XmlDefination> XmlDefinations { get; set; }
        public DbSet<XmlDefinationBrand> XmlDefinationBrands { get; set; }

        #region Wholesales
        public DbSet<WholesaleOrder> WholesaleOrders { get; set; }

        public DbSet<WholesaleOrderDetail> WholesaleOrderDetails { get; set; }

        public DbSet<WholesaleOrderType> WholesaleOrderTypes { get; set; }
        #endregion

        public DbSet<ProductCategorizationHistory> ProductCategorizationHistories { get; set; }

        public DbSet<SizeTable> SizeTables { get; set; }

        public DbSet<SizeTableItem> SizeTableItems { get; set; }

        public DbSet<StocktakingType> StocktakingTypes { get; set; }

        public DbSet<Stocktaking> Stocktakings { get; set; }

        public DbSet<StocktakingDetail> StocktakingDetails { get; set; }

        public DbSet<ECommerceBrand> ECommerceBrands { get; set; }

        public DbSet<ECommerceCategory> ECommerceCategories { get; set; }

        public DbSet<ECommerceVariant> ECommerceVariants { get; set; }

        public DbSet<ECommerceVariantValue> ECommerceVariantValues { get; set; }

        public DbSet<MarketplaceRequestType> MarketplaceRequestTypes { get; set; }

        public DbSet<ProductSourceDomain> ProductSourceDomains { get; set; }

        public DbSet<ProductSourceConfiguration> ProductSourceConfigurations { get; set; }

        public DbSet<ProductSourceMapping> ProductSourceMappings { get; set; }

        public DbSet<ProductSource> ProductSources { get; set; }

        public DbSet<PaymentLog> PaymentLogs { get; set; }

        public DbSet<FeedTemplate> FeedTemplates { get; set; }

        public DbSet<Feed> Feeds { get; set; }

        public DbSet<FeedDetail> FeedDetails { get; set; }

        public DbSet<ProductInformationMarketplaceVariantValue> ProductInformationMarketplaceVariantValues { get; set; }

        public DbSet<OrderReturnDetail> OrderReturnDetails { get; set; }

        public DbSet<OrderViewing> OrderViewings { get; set; }

        public DbSet<Expense> Expenses { get; set; }

        public DbSet<ExpenseSource> ExpenseSources { get; set; }

        public DbSet<ExchangeRate> ExchangeRates { get; set; }

        public DbSet<OrderPacking> OrderPackings { get; set; }

        public DbSet<OrderPicking> OrderPickings { get; set; }

        public DbSet<Tenant> Tenants { get; set; }

        public DbSet<TenantSetting> TenantSettings { get; set; }

        public DbSet<TenantPayment> TenantPayments { get; set; }

        public DbSet<TenantInvoiceInformation> TenantInvoiceInformations { get; set; }

        public DbSet<TenantSalesAmount> TenantSalesAmounts { get; set; }

        public DbSet<Accounting> Accountings { get; set; }

        public DbSet<AccountingConfiguration> AccountingConfigurations { get; set; }

        public DbSet<AccountingCompany> AccountingCompanies { get; set; }

        public DbSet<Domain.Entities.Application> Applications { get; set; }

        public DbSet<Bank> Banks { get; set; }

        public DbSet<MarketplaceBrandMapping> MarketplaceBrandMappings { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<CompanySetting> CompanySettings { get; set; }

        public DbSet<CompanyContact> CompanyContacts { get; set; }

        public DbSet<CustomerNote> CustomerNotes { get; set; }

        public DbSet<CompanyConfiguration> CompanyConfigurations { get; set; }

        public DbSet<OrderSource> OrderSources { get; set; }

        public DbSet<Bin> Bins { get; set; }

        public DbSet<Brand> Brands { get; set; }

        public DbSet<BrandDiscountSetting> BrandDiscountSettings { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<CategoryTranslationBreadcrumb> CategoryTranslationBreadcrumbs { get; set; }

        public DbSet<CategoryDiscountSetting> CategoryDiscountSettings { get; set; }

        public DbSet<CategoryProduct> CategoryProducts { get; set; }

        public DbSet<CategoryTranslation> CategoryTranslations { get; set; }

        public DbSet<Configuration> Configurations { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<ContactTranslation> ContactTranslations { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerAddress> CustomerAddresses { get; set; }

        public DbSet<CustomerInvoiceInformation> CustomerInvoiceInformations { get; set; }

        public DbSet<CustomerContact> CustomerContacts { get; set; }

        public DbSet<CustomerRole> CustomerRoles { get; set; }

        public DbSet<CustomerUser> CustomerUsers { get; set; }

        public DbSet<TwoFactorCustomerUser> TwoFactorCustomerUsers { get; set; }

        public DbSet<DiscountCoupon> DiscountCoupons { get; set; }

        public DbSet<DiscountCouponSetting> DiscountCouponSettings { get; set; }

        public DbSet<District> Districts { get; set; }

        public DbSet<Domain.Entities.Domain> Domains { get; set; }

        public DbSet<DomainSetting> DomainSettings { get; set; }

        public DbSet<EntireDiscount> EntireDiscounts { get; set; }

        public DbSet<EntireDiscountSetting> EntireDiscountSettings { get; set; }

        public DbSet<FacebookConfigration> FacebookConfigrations { get; set; }

        public DbSet<FrequentlyAskedQuestion> FrequentlyAskedQuestions { get; set; }

        public DbSet<FrequentlyAskedQuestionTranslation> FrequentlyAskedQuestionTranslations { get; set; }

        public DbSet<GetXPayYSetting> GetXPayYSettings { get; set; }

        public DbSet<GoogleConfiguration> GoogleConfigrations { get; set; }

        public DbSet<Label> Labels { get; set; }

        public DbSet<LabelTranslation> LabelTranslations { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<NewBulletin> NewBulletins { get; set; }

        public DbSet<NewBulletinInformation> NewBulletinInformations { get; set; }

        public DbSet<Neighborhood> Neighborhoods { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderReserve> OrderReserves { get; set; }

        public DbSet<OrderBilling> OrderBillings { get; set; }

        public DbSet<OrderDeliveryAddress> OrderDeliveryAddresses { get; set; }

        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<OrderInvoiceInformation> OrderInvoiceInformations { get; set; }

        public DbSet<OrderNote> OrderNotes { get; set; }

        public DbSet<OrderShipment> OrderShipments { get; set; }

        public DbSet<OrderShipmentDetail> OrderShipmentDetails { get; set; }

        public DbSet<OrderSms> OrderSmsses { get; set; }

        public DbSet<OrderTechnicInformation> OrderTechnicInformations { get; set; }

        public DbSet<OrderType> OrderTypes { get; set; }

        public DbSet<OrderTypeTranslation> OrderTypeTranslations { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<PaymentType> PaymentTypes { get; set; }

        public DbSet<PaymentTypeSetting> PaymentTypeSettings { get; set; }

        public DbSet<PaymentTypeDomain> PaymentTypeDomains { get; set; }

        public DbSet<PaymentTypeTranslation> PaymentTypeTranslations { get; set; }

        public DbSet<Pos> Posses { get; set; }

        public DbSet<PosConfiguration> PosConfigurations { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductCategorization> ProductCategorizations { get; set; }

        public DbSet<ProductGenericProperty> ProductGenericProperties { get; set; }

        public DbSet<ProductInformationSubscription> ProductInformationSubscriptions { get; set; }

        public DbSet<ProductInformationComment> ProductInformationComments { get; set; }

        public DbSet<ProductInformation> ProductInformations { get; set; }

        public DbSet<ProductInformationCombine> ProductInformationCombines { get; set; }

        public DbSet<ProductInformationPhoto> ProductInformationPhotos { get; set; }

        public DbSet<ProductInformationPrice> ProductInformationPriceses { get; set; }

        public DbSet<ProductInformationTranslation> ProductInformationTranslations { get; set; }

        public DbSet<ProductInformationTranslationBreadcrumb> ProductInformationTranslationBreadcrumbs { get; set; }

        public DbSet<ProductTranslation> ProductTranslations { get; set; }

        public DbSet<ProductLabel> ProductLabels { get; set; }

        public DbSet<ProductInformationVariant> ProductInformationVariants { get; set; }

        public DbSet<ProductMarketplace> ProductMarketplaces { get; set; }

        public DbSet<ShipmentCompany> ShipmentCompanies { get; set; }

        public DbSet<ShipmentCompanyCompany> ShipmentCompanyCompanies { get; set; }

        public DbSet<ShipmentCompanyConfiguration> ShipmentCompanyConfigurations { get; set; }

        public DbSet<ShoppingCartDiscountedProduct> ShoppingCartDiscountedProducts { get; set; }

        public DbSet<ShoppingCartDiscountedProductSetting> ShoppingCartDiscountedProductSettings { get; set; }

        public DbSet<SmsProvider> SmsProviders { get; set; }

        public DbSet<SmsProviderCompany> SmsProviderCompanies { get; set; }

        public DbSet<SmsProviderConfiguration> SmsProviderConfigurations { get; set; }

        public DbSet<ShoppingCartDiscounted> ShoppingCartDiscounteds { get; set; }

        public DbSet<ShoppingCartDiscountedSetting> ShoppingCartDiscountedSettings { get; set; }

        public DbSet<EmailProvider> EmailProviders { get; set; }

        public DbSet<EmailProviderCompany> EmailProviderCompanies { get; set; }

        public DbSet<EmailProviderConfiguration> EmailProviderConfigurations { get; set; }

        public DbSet<SmsTemplate> SmsTemplates { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Variant> Variants { get; set; }

        public DbSet<VariantTranslation> VariantTranslations { get; set; }

        public DbSet<VariantValue> VariantValues { get; set; }

        public DbSet<VariantValueTranslation> VariantValueTranslations { get; set; }

        public DbSet<Property> Properties { get; set; }

        public DbSet<PropertyTranslation> PropertyTranslations { get; set; }

        public DbSet<PropertyValue> PropertyValues { get; set; }

        public DbSet<PropertyValueTranslation> PropertyValueTranslations { get; set; }

        public DbSet<ProductProperty> ProductProperties { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<TagTranslation> TagTranslations { get; set; }

        public DbSet<ProductTag> ProductTags { get; set; }

        public DbSet<ProductInformationSeoTranslation> ProductInformationSeoTranslations { get; set; }

        public DbSet<ProductInformationContentTranslation> ProductInformationContentTranslations { get; set; }

        public DbSet<ProductInformationMarketplace> ProductInformationMarketplaces { get; set; }

        public DbSet<ProductInformationMarketplacePriceLog> ProductInformationMarketplacePriceLogs { get; set; }

        public DbSet<ProductInformationMarketplaceStockLog> ProductInformationMarketplaceStockLogs { get; set; }

        public DbSet<ProductInformationMarketplaceBulkPrice> ProductInformationMarketplaceBulkPrices { get; set; }

        public DbSet<CategorySeoTranslation> CategorySeoTranslations { get; set; }

        public DbSet<CategoryContentTranslation> CategoryContentTranslations { get; set; }

        public DbSet<MoneyPoint> MoneyPoints { get; set; }

        public DbSet<MoneyPointSetting> MoneyPointSettings { get; set; }

        public DbSet<Showcase> Showcases { get; set; }

        public DbSet<Marketplace> Marketplaces { get; set; }

        public DbSet<MarketplaceCompany> MarketplaceCompanies { get; set; }

        public DbSet<MarketplaceConfiguration> MarketPlaceConfigurations { get; set; }

        public DbSet<Content> Contents { get; set; }

        public DbSet<ContentTranslation> ContentTranslations { get; set; }

        public DbSet<ContentSeoTranslation> ContentSeoTranslations { get; set; }

        public DbSet<ContentTranslationContent> ContentTranslationContents { get; set; }

        public DbSet<Information> Informations { get; set; }

        public DbSet<InformationTranslation> InformationTranslations { get; set; }

        public DbSet<InformationSeoTranslation> InformationSeoTranslations { get; set; }

        public DbSet<InformationTranslationContent> InformationTranslationContents { get; set; }

        public DbSet<UpsCity> UpsCities { get; set; }

        public DbSet<UpsArea> UpsAreas { get; set; }

        public DbSet<MarketplaceCategoryMapping> MarketplaceCategoryMappings { get; set; }

        public DbSet<MarketplaceCategoryVariantValueMapping> MarketplaceCategoryVariantValueMappings { get; set; }

        public DbSet<MarketplaceVariantValueMapping> MarketplaceVariantValueMappings { get; set; }

        public DbSet<Receipt> Receipts { get; set; }

        public DbSet<ReceiptItem> ReceiptItems { get; set; }

        public DbSet<NebimOrder> NebimOrders { get; set; }

        public DbSet<NebimOrderDetail> NebimOrderDetails { get; set; }

        public DbSet<NebimMarketplaceCreditCardCode> NebimMarketplaceCreditCardCodes { get; set; }

        public DbSet<NebimMarketplaceSalesPersonnelCode> NebimMarketplaceSalesPersonnelCodes { get; set; }
        #endregion

        #region Constructors
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IDbNameFinder dbNameFinder, IConfiguration configuration) : base(options)
        {
            #region Fields
            this._dbNameFinder = dbNameFinder;
            this._configuration = configuration;
            #endregion

            //this.Database.EnsureCreated();
        }

        protected ApplicationDbContext(DbContextOptions contextOptions) : base(contextOptions)
        {
        }
        #endregion

        #region Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (this._dbNameFinder != null)
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                optionsBuilder.UseSqlServer(connectionString);
            }
            else
            {
                var connectionString = this._configuration.GetConnectionString("Helpy");

                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Relations
            modelBuilder
                .Entity<Stocktaking>()
                .HasMany<StocktakingDetail>(s => s.StocktakingDetails)
                .WithOne(sd => sd.Stocktaking)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<ProductInformationMarketplace>()
                .HasOne<ProductInformationMarketplaceBulkPrice>(p => p.ProductInformationMarketplaceBulkPrice)
                .WithOne(ps => ps.ProductInformationMarketplace)
                .HasForeignKey<ProductInformationMarketplaceBulkPrice>(p => p.Id);

            modelBuilder
                .Entity<Tenant>()
                .HasOne<TenantSetting>(t => t.TenantSetting)
                .WithOne(ts => ts.Tenant)
                .HasForeignKey<TenantSetting>(ts => ts.Id);

            modelBuilder
                .Entity<Tenant>()
                .HasOne<TenantInvoiceInformation>(t => t.TenantInvoiceInformation)
                .WithOne(ts => ts.Tenant)
                .HasForeignKey<TenantInvoiceInformation>(ts => ts.Id);

            modelBuilder
                .Entity<Tenant>()
                .HasOne<TenantSalesAmount>(t => t.TenantSalesAmount)
                .WithOne(ts => ts.Tenant)
                .HasForeignKey<TenantSalesAmount>(ts => ts.Id);

            modelBuilder
                .Entity<Domain.Entities.Domain>()
                .HasOne<DomainSetting>(d => d.DomainSetting)
                .WithOne(ds => ds.Domain)
                .HasForeignKey<DomainSetting>(ds => ds.Id);

            modelBuilder
                .Entity<Company>()
                .HasOne<CompanySetting>(c => c.CompanySetting)
                .WithOne(cs => cs.Company)
                .HasForeignKey<CompanySetting>(cs => cs.Id);

            modelBuilder
                .Entity<Payment>()
                .HasOne<PaymentLog>(p => p.PaymentLog)
                .WithOne(pl => pl.Payment)
                .HasForeignKey<PaymentLog>(pl => pl.Id);

            modelBuilder
                .Entity<Customer>()
                .HasOne<CustomerContact>(c => c.CustomerContact)
                .WithOne(ca => ca.Customer)
                .HasForeignKey<CustomerContact>(cc => cc.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderPacking>(o => o.OrderPacking)
                .WithOne(op => op.Order)
                .HasForeignKey<OrderPacking>(op => op.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<MarketplaceInvoice>(o => o.MarketplaceInvoice)
                .WithOne(mi => mi.Order)
                .HasForeignKey<MarketplaceInvoice>(o => o.Id);

            modelBuilder
               .Entity<Order>()
               .HasOne<OrderPicking>(o => o.OrderPicking)
               .WithOne(op => op.Order)
               .HasForeignKey<OrderPicking>(op => op.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderBilling>(o => o.OrderBilling)
                .WithOne(ob => ob.Order)
                .HasForeignKey<OrderBilling>(ob => ob.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderDeliveryAddress>(o => o.OrderDeliveryAddress)
                .WithOne(oda => oda.Order)
                .HasForeignKey<OrderDeliveryAddress>(oda => oda.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderDeliveryAddress>(o => o.OrderDeliveryAddress)
                .WithOne(oda => oda.Order)
                .HasForeignKey<OrderDeliveryAddress>(oda => oda.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderTechnicInformation>(o => o.OrderTechnicInformation)
                .WithOne(oti => oti.Order)
                .HasForeignKey<OrderTechnicInformation>(oti => oti.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<MoneyPoint>(o => o.MoneyPoint)
                .WithOne(mp => mp.Order)
                .HasForeignKey<MoneyPoint>(mp => mp.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderInvoiceInformation>(o => o.OrderInvoiceInformation)
                .WithOne(oii => oii.Order)
                .HasForeignKey<OrderInvoiceInformation>(oii => oii.Id);

            modelBuilder
                .Entity<Order>()
                .HasOne<OrderReserve>(o => o.OrderReserve)
                .WithOne(or => or.Order)
                .HasForeignKey<OrderReserve>(or => or.Id);

            modelBuilder
                .Entity<PaymentTypeDomain>()
                .HasOne<PaymentTypeSetting>(o => o.PaymentTypeSetting)
                .WithOne(oii => oii.PaymentTypeDomain)
                .HasForeignKey<PaymentTypeSetting>(oii => oii.Id);

            modelBuilder
                .Entity<DiscountCoupon>()
                .HasOne<DiscountCouponSetting>(dcs => dcs.DisountCouponSetting)
                .WithOne(dc => dc.DisountCoupon)
                .HasForeignKey<DiscountCouponSetting>(dcs => dcs.Id);

            modelBuilder
                .Entity<EntireDiscount>()
                .HasOne<EntireDiscountSetting>(ets => ets.EntireDiscountSetting)
                .WithOne(ed => ed.EntireDiscount)
                .HasForeignKey<EntireDiscountSetting>(ets => ets.Id);

            modelBuilder
                .Entity<Customer>()
                .HasOne<CustomerUser>(c => c.CustomerUser)
                .WithOne(cu => cu.Customer)
                .HasForeignKey<CustomerUser>(cu => cu.Id);

            modelBuilder
                .Entity<ShoppingCartDiscounted>()
                .HasOne<ShoppingCartDiscountedSetting>(scdps => scdps.ShoppingCartDiscountedSetting)
                .WithOne(scdp => scdp.ShoppingCartDiscounted)
                .HasForeignKey<ShoppingCartDiscountedSetting>(scdps => scdps.Id);

            modelBuilder
                .Entity<ShoppingCartDiscountedProduct>()
                .HasOne<ShoppingCartDiscountedProductSetting>(scdps => scdps.ShoppingCartDiscountedProductSetting)
                .WithOne(scdp => scdp.ShoppingCartDiscountedProduct)
                .HasForeignKey<ShoppingCartDiscountedProductSetting>(scdps => scdps.Id);

            modelBuilder
                .Entity<CategoryTranslation>()
                .HasOne<CategoryContentTranslation>(c => c.CategoryContentTranslation)
                .WithOne(cd => cd.CategoryTranslation)
                .HasForeignKey<CategoryContentTranslation>(c => c.Id);

            modelBuilder
                .Entity<ProductInformationTranslation>()
                .HasOne<ProductInformationContentTranslation>(p => p.ProductInformationContentTranslation)
                .WithOne(pc => pc.ProductInformationTranslation)
                .HasForeignKey<ProductInformationContentTranslation>(p => p.Id);

            modelBuilder
                .Entity<ProductInformationTranslation>()
                .HasOne<ProductInformationSeoTranslation>(p => p.ProductInformationSeoTranslation)
                .WithOne(ps => ps.ProductInformationTranslation)
                .HasForeignKey<ProductInformationSeoTranslation>(p => p.Id);

            modelBuilder
                .Entity<ProductInformationTranslation>()
                .HasOne<ProductInformationTranslationBreadcrumb>(p => p.ProductInformationTranslationBreadcrumb)
                .WithOne(ps => ps.ProductInformationTranslation)
                .HasForeignKey<ProductInformationTranslationBreadcrumb>(p => p.Id);

            modelBuilder
                .Entity<CategoryTranslation>()
                .HasOne<CategoryTranslationBreadcrumb>(p => p.CategoryTranslationBreadcrumb)
                .WithOne(ps => ps.CategoryTranslation)
                .HasForeignKey<CategoryTranslationBreadcrumb>(p => p.Id);

            modelBuilder
                .Entity<CategoryTranslation>()
                .HasOne<CategorySeoTranslation>(p => p.CategorySeoTranslation)
                .WithOne(ps => ps.CategoryTranslation)
                .HasForeignKey<CategorySeoTranslation>(p => p.Id);

            modelBuilder
                .Entity<ContentTranslation>()
                .HasOne<ContentSeoTranslation>(p => p.ContentSeoTranslation)
                .WithOne(ps => ps.ContentTranslation)
                .HasForeignKey<ContentSeoTranslation>(p => p.Id);

            modelBuilder.Entity<Category>()
                .HasMany(c => c.Products)
                .WithOne(x => x.Category)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<ContentTranslation>()
                .HasOne<ContentTranslationContent>(ct => ct.ContentTranslationContent)
                .WithOne(ctc => ctc.ContentTranslation)
                .HasForeignKey<ContentTranslationContent>(ctc => ctc.Id);

            modelBuilder
                .Entity<InformationTranslation>()
                .HasOne<InformationTranslationContent>(it => it.InformationTranslationContent)
                .WithOne(itc => itc.InformationTranslation)
                .HasForeignKey<InformationTranslationContent>(itc => itc.Id);

            modelBuilder
                .Entity<InformationTranslation>()
                .HasOne<InformationSeoTranslation>(p => p.InformationSeoTranslation)
                .WithOne(ps => ps.InformationTranslation)
                .HasForeignKey<InformationSeoTranslation>(p => p.Id);

            modelBuilder
                .Entity<Company>()
                .HasOne<CompanyContact>(o => o.CompanyContact)
                .WithOne(op => op.Company)
                .HasForeignKey<CompanyContact>(op => op.Id);

            modelBuilder
                .Entity<ProductInformation>()
                .HasMany(pi => pi.ProductInformationCombines)
                .WithOne(pic => pic.ProductInformation)
                .HasForeignKey(pic => pic.ProductInformationId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<ProductInformation>()
                .HasMany(pi => pi.ContainProductInformationCombines)
                .WithOne(pic => pic.ContainProductInformation)
                .HasForeignKey(pic => pic.ContainProductInformationId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ECommerce.Domain.Entities.Tenant>()
                .HasMany(d => d.Domains)
                .WithOne(c => c.Tenant)
                .HasForeignKey(c => c.TenantId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<ExchangeRate>()
                .Property(od => od.Rate)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.Total)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.ExchangeRate)
                .HasColumnType("decimal(18, 4)")
                .HasDefaultValue(1);

            modelBuilder
                .Entity<Order>()
                .Property(od => od.ListTotal)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.Discount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.ShoppingCartDiscount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.CargoFee)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.SurchargeFee)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.CommissionAmount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.VatExcListTotal)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.VatExcTotal)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Order>()
                .Property(od => od.VatExcDiscount)
                .HasColumnType("decimal(18, 4)");


            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.ListUnitPrice)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.UnitPrice)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.UnitDiscount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.UnitCost)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.UnitCargoFee)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.UnitCommissionAmount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.VatExcListUnitPrice)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.VatExcUnitPrice)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.VatExcUnitDiscount)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<OrderDetail>()
                .Property(od => od.VatExcUnitCost)
                .HasColumnType("decimal(18, 4)");

            modelBuilder
                .Entity<Receipt>()
                .HasMany(pi => pi.ReceiptItems)
                .WithOne(pic => pic.Receipt)
                .HasForeignKey(pic => pic.ReceiptId)
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder
               .Entity<Order>()
               .HasOne<NebimOrder>(o => o.NebimOrder)
               .WithOne(op => op.Order)
               .HasForeignKey<NebimOrder>(op => op.Id);

            modelBuilder
                .Entity<OrderDetail>()
                .HasOne<NebimOrderDetail>(o => o.NebimOrderDetail)
                .WithOne(op => op.OrderDetail)
                .HasForeignKey<NebimOrderDetail>(op => op.Id);

            #endregion

            #region ICustomerUserCreatable
            OnModelCreatingICustomerUserCreatable<MoneyPoint>(modelBuilder);
            OnModelCreatingICustomerUserCreatable<TwoFactorCustomerUser>(modelBuilder);
            #endregion

            #region ITenantable
            OnModelCreatingITenantable<Receipt>(modelBuilder);
            OnModelCreatingITenantable<ReceiptItem>(modelBuilder);
            OnModelCreatingITenantable<Stocktaking>(modelBuilder);
            OnModelCreatingITenantable<StocktakingDetail>(modelBuilder);
            OnModelCreatingITenantable<ProductSourceDomain>(modelBuilder);
            OnModelCreatingITenantable<ProductSourceConfiguration>(modelBuilder);
            OnModelCreatingITenantable<ProductSourceMapping>(modelBuilder);
            OnModelCreatingITenantable<Feed>(modelBuilder);
            OnModelCreatingITenantable<FeedDetail>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceCategoryMapping>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceBrandMapping>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceVariantValueMapping>(modelBuilder);
            OnModelCreatingITenantable<OrderViewing>(modelBuilder);
            OnModelCreatingITenantable<Expense>(modelBuilder);
            OnModelCreatingITenantable<ExpenseSource>(modelBuilder);
            OnModelCreatingITenantable<AccountingCompany>(modelBuilder);
            OnModelCreatingITenantable<AccountingConfiguration>(modelBuilder);
            OnModelCreatingITenantable<Brand>(modelBuilder);
            OnModelCreatingITenantable<BrandDiscountSetting>(modelBuilder);
            OnModelCreatingITenantable<Category>(modelBuilder);
            OnModelCreatingITenantable<CategoryContentTranslation>(modelBuilder);
            OnModelCreatingITenantable<CategoryDiscountSetting>(modelBuilder);
            OnModelCreatingITenantable<CategoryProduct>(modelBuilder);
            OnModelCreatingITenantable<CategorySeoTranslation>(modelBuilder);
            OnModelCreatingITenantable<CategoryTranslation>(modelBuilder);
            OnModelCreatingITenantable<CategoryTranslationBreadcrumb>(modelBuilder);
            OnModelCreatingITenantable<Company>(modelBuilder);
            OnModelCreatingITenantable<CompanyConfiguration>(modelBuilder);
            OnModelCreatingITenantable<CompanyContact>(modelBuilder);
            OnModelCreatingITenantable<Configuration>(modelBuilder);
            OnModelCreatingITenantable<Contact>(modelBuilder);
            OnModelCreatingITenantable<ContactTranslation>(modelBuilder);
            OnModelCreatingITenantable<Content>(modelBuilder);
            OnModelCreatingITenantable<ContentSeoTranslation>(modelBuilder);
            OnModelCreatingITenantable<ContentTranslation>(modelBuilder);
            OnModelCreatingITenantable<ContentTranslationContent>(modelBuilder);
            OnModelCreatingITenantable<Customer>(modelBuilder);
            OnModelCreatingITenantable<CustomerAddress>(modelBuilder);
            OnModelCreatingITenantable<CustomerContact>(modelBuilder);
            OnModelCreatingITenantable<CustomerInvoiceInformation>(modelBuilder);
            OnModelCreatingITenantable<CustomerNote>(modelBuilder);
            OnModelCreatingITenantable<CustomerUser>(modelBuilder);
            //OnModelCreatingITenantable<Domain.Entities.Domain>(modelBuilder);
            OnModelCreatingITenantable<Domain.Entities.DomainSetting>(modelBuilder);
            OnModelCreatingITenantable<DiscountCoupon>(modelBuilder);
            OnModelCreatingITenantable<DiscountCouponSetting>(modelBuilder);
            OnModelCreatingITenantable<EmailProviderCompany>(modelBuilder);
            OnModelCreatingITenantable<EmailProviderConfiguration>(modelBuilder);
            OnModelCreatingITenantable<EntireDiscount>(modelBuilder);
            OnModelCreatingITenantable<EntireDiscountSetting>(modelBuilder);
            OnModelCreatingITenantable<FacebookConfigration>(modelBuilder);
            OnModelCreatingITenantable<FrequentlyAskedQuestion>(modelBuilder);
            OnModelCreatingITenantable<FrequentlyAskedQuestionTranslation>(modelBuilder);
            OnModelCreatingITenantable<GetXPayYSetting>(modelBuilder);
            OnModelCreatingITenantable<GoogleConfiguration>(modelBuilder);
            OnModelCreatingITenantable<Information>(modelBuilder);
            OnModelCreatingITenantable<InformationSeoTranslation>(modelBuilder);
            OnModelCreatingITenantable<InformationTranslation>(modelBuilder);
            OnModelCreatingITenantable<InformationTranslationContent>(modelBuilder);
            OnModelCreatingITenantable<Label>(modelBuilder);
            OnModelCreatingITenantable<LabelTranslation>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceCompany>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceConfiguration>(modelBuilder);
            OnModelCreatingITenantable<MoneyPoint>(modelBuilder);
            OnModelCreatingITenantable<MoneyPointSetting>(modelBuilder);
            OnModelCreatingITenantable<NewBulletin>(modelBuilder);
            OnModelCreatingITenantable<NewBulletinInformation>(modelBuilder);
            OnModelCreatingITenantable<Order>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceCargoFee>(modelBuilder);
            OnModelCreatingITenantable<OrderDeliveryAddress>(modelBuilder);
            OnModelCreatingITenantable<OrderDetail>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceCommission>(modelBuilder);
            OnModelCreatingITenantable<OrderInvoiceInformation>(modelBuilder);
            OnModelCreatingITenantable<OrderReserve>(modelBuilder);
            OnModelCreatingITenantable<OrderNote>(modelBuilder);
            OnModelCreatingITenantable<OrderShipment>(modelBuilder);
            OnModelCreatingITenantable<OrderShipmentDetail>(modelBuilder);
            OnModelCreatingITenantable<OrderSms>(modelBuilder);
            OnModelCreatingITenantable<OrderSource>(modelBuilder);
            OnModelCreatingITenantable<OrderTechnicInformation>(modelBuilder);
            OnModelCreatingITenantable<Payment>(modelBuilder);
            OnModelCreatingITenantable<PaymentLog>(modelBuilder);
            OnModelCreatingITenantable<PaymentTypeDomain>(modelBuilder);
            OnModelCreatingITenantable<PosConfiguration>(modelBuilder);
            OnModelCreatingITenantable<XmlDefination>(modelBuilder);
            OnModelCreatingITenantable<XmlDefinationBrand>(modelBuilder);
            OnModelCreatingITenantable<Product>(modelBuilder);
            OnModelCreatingITenantable<ProductCategorization>(modelBuilder);
            OnModelCreatingITenantable<ProductGenericProperty>(modelBuilder);
            OnModelCreatingITenantable<ProductInformation>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationCombine>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationComment>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationContentTranslation>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationMarketplace>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationPhoto>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationPrice>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationSeoTranslation>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationSubscription>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationTranslation>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationTranslationBreadcrumb>(modelBuilder);
            OnModelCreatingITenantable<ProductTranslation>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationVariant>(modelBuilder);

            OnModelCreatingITenantable<ProductLabel>(modelBuilder);
            OnModelCreatingITenantable<ProductProperty>(modelBuilder);
            OnModelCreatingITenantable<ProductTag>(modelBuilder);
            OnModelCreatingITenantable<ProductMarketplace>(modelBuilder);
            OnModelCreatingITenantable<Property>(modelBuilder);
            OnModelCreatingITenantable<PropertyTranslation>(modelBuilder);
            OnModelCreatingITenantable<PropertyValue>(modelBuilder);
            OnModelCreatingITenantable<PropertyValueTranslation>(modelBuilder);
            OnModelCreatingITenantable<ShipmentCompanyCompany>(modelBuilder);
            OnModelCreatingITenantable<ShipmentCompanyConfiguration>(modelBuilder);
            OnModelCreatingITenantable<ShoppingCartDiscounted>(modelBuilder);
            OnModelCreatingITenantable<ShoppingCartDiscountedSetting>(modelBuilder);
            OnModelCreatingITenantable<ShoppingCartDiscountedProduct>(modelBuilder);
            OnModelCreatingITenantable<ShoppingCartDiscountedProductSetting>(modelBuilder);
            OnModelCreatingITenantable<Showcase>(modelBuilder);
            OnModelCreatingITenantable<SmsProviderConfiguration>(modelBuilder);
            OnModelCreatingITenantable<SmsTemplate>(modelBuilder);
            OnModelCreatingITenantable<SmsProviderCompany>(modelBuilder);
            OnModelCreatingITenantable<Supplier>(modelBuilder);
            OnModelCreatingITenantable<Tag>(modelBuilder);
            OnModelCreatingITenantable<TagTranslation>(modelBuilder);
            OnModelCreatingITenantable<TwoFactorCustomerUser>(modelBuilder);
            OnModelCreatingITenantable<Variant>(modelBuilder);
            OnModelCreatingITenantable<VariantTranslation>(modelBuilder);
            OnModelCreatingITenantable<VariantValue>(modelBuilder);
            OnModelCreatingITenantable<VariantValueTranslation>(modelBuilder);
            OnModelCreatingITenantable<OrderReturnDetail>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationMarketplaceVariantValue>(modelBuilder);
            OnModelCreatingITenantable<CompanySetting>(modelBuilder);
            OnModelCreatingITenantable<ProductInformationMarketplaceBulkPrice>(modelBuilder);
            OnModelCreatingITenantable<NebimOrder>(modelBuilder);
            OnModelCreatingITenantable<NebimOrderDetail>(modelBuilder);
            OnModelCreatingITenantable<NebimMarketplaceCreditCardCode>(modelBuilder);
            OnModelCreatingITenantable<NebimMarketplaceSalesPersonnelCode>(modelBuilder);
            OnModelCreatingITenantable<MarketplaceCategoryVariantValueMapping>(modelBuilder);

            #endregion

            #region IDomainable
            OnModelCreatingIDomainable<XmlDefination>(modelBuilder);
            OnModelCreatingIDomainable<XmlDefinationBrand>(modelBuilder);
            OnModelCreatingIDomainable<Receipt>(modelBuilder);
            OnModelCreatingIDomainable<ReceiptItem>(modelBuilder);
            OnModelCreatingIDomainable<Stocktaking>(modelBuilder);
            OnModelCreatingIDomainable<StocktakingDetail>(modelBuilder);
            OnModelCreatingIDomainable<ProductSourceDomain>(modelBuilder);
            OnModelCreatingIDomainable<ProductSourceConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<ProductSourceMapping>(modelBuilder);
            OnModelCreatingIDomainable<Feed>(modelBuilder);
            OnModelCreatingIDomainable<FeedDetail>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceCategoryMapping>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceBrandMapping>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceVariantValueMapping>(modelBuilder);
            OnModelCreatingIDomainable<OrderViewing>(modelBuilder);
            OnModelCreatingIDomainable<Expense>(modelBuilder);
            OnModelCreatingIDomainable<AccountingCompany>(modelBuilder);
            OnModelCreatingIDomainable<AccountingConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<Brand>(modelBuilder);
            OnModelCreatingIDomainable<BrandDiscountSetting>(modelBuilder);
            OnModelCreatingIDomainable<Category>(modelBuilder);
            OnModelCreatingIDomainable<CategoryContentTranslation>(modelBuilder);
            OnModelCreatingIDomainable<CategoryDiscountSetting>(modelBuilder);
            OnModelCreatingIDomainable<CategoryProduct>(modelBuilder);
            OnModelCreatingIDomainable<CategorySeoTranslation>(modelBuilder);
            OnModelCreatingIDomainable<CategoryTranslation>(modelBuilder);
            OnModelCreatingIDomainable<CategoryTranslationBreadcrumb>(modelBuilder);
            OnModelCreatingIDomainable<CompanySetting>(modelBuilder);
            OnModelCreatingIDomainable<CompanyConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<CompanyContact>(modelBuilder);
            OnModelCreatingIDomainable<Configuration>(modelBuilder);
            OnModelCreatingIDomainable<Contact>(modelBuilder);
            OnModelCreatingIDomainable<ContactTranslation>(modelBuilder);
            OnModelCreatingIDomainable<Content>(modelBuilder);
            OnModelCreatingIDomainable<ContentSeoTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ContentTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ContentTranslationContent>(modelBuilder);
            OnModelCreatingIDomainable<Customer>(modelBuilder);
            OnModelCreatingIDomainable<CustomerAddress>(modelBuilder);
            OnModelCreatingIDomainable<CustomerContact>(modelBuilder);
            OnModelCreatingIDomainable<CustomerInvoiceInformation>(modelBuilder);
            OnModelCreatingIDomainable<CustomerNote>(modelBuilder);
            OnModelCreatingIDomainable<CustomerUser>(modelBuilder);
            OnModelCreatingIDomainable<DiscountCoupon>(modelBuilder);
            OnModelCreatingIDomainable<DiscountCouponSetting>(modelBuilder);
            OnModelCreatingIDomainable<EmailProviderCompany>(modelBuilder);
            OnModelCreatingIDomainable<EmailProviderConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<EntireDiscount>(modelBuilder);
            OnModelCreatingIDomainable<EntireDiscountSetting>(modelBuilder);
            OnModelCreatingIDomainable<FacebookConfigration>(modelBuilder);
            OnModelCreatingIDomainable<FrequentlyAskedQuestion>(modelBuilder);
            OnModelCreatingIDomainable<FrequentlyAskedQuestionTranslation>(modelBuilder);
            OnModelCreatingIDomainable<GetXPayYSetting>(modelBuilder);
            OnModelCreatingIDomainable<GoogleConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<Information>(modelBuilder);
            OnModelCreatingIDomainable<InformationSeoTranslation>(modelBuilder);
            OnModelCreatingIDomainable<InformationTranslation>(modelBuilder);
            OnModelCreatingIDomainable<InformationTranslationContent>(modelBuilder);
            OnModelCreatingIDomainable<Label>(modelBuilder);
            OnModelCreatingIDomainable<LabelTranslation>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceCompany>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<MoneyPoint>(modelBuilder);
            OnModelCreatingIDomainable<MoneyPointSetting>(modelBuilder);
            OnModelCreatingIDomainable<NewBulletin>(modelBuilder);
            OnModelCreatingIDomainable<NewBulletinInformation>(modelBuilder);
            OnModelCreatingIDomainable<Order>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceCargoFee>(modelBuilder);
            OnModelCreatingIDomainable<OrderDeliveryAddress>(modelBuilder);
            OnModelCreatingIDomainable<OrderDetail>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceCommission>(modelBuilder);
            OnModelCreatingIDomainable<OrderInvoiceInformation>(modelBuilder);
            OnModelCreatingIDomainable<OrderReserve>(modelBuilder);
            OnModelCreatingIDomainable<OrderNote>(modelBuilder);
            OnModelCreatingIDomainable<OrderShipment>(modelBuilder);
            OnModelCreatingIDomainable<OrderShipmentDetail>(modelBuilder);
            OnModelCreatingIDomainable<OrderSms>(modelBuilder);
            OnModelCreatingIDomainable<OrderTechnicInformation>(modelBuilder);
            OnModelCreatingIDomainable<Payment>(modelBuilder);
            OnModelCreatingIDomainable<PaymentLog>(modelBuilder);
            OnModelCreatingIDomainable<PaymentTypeDomain>(modelBuilder);
            OnModelCreatingIDomainable<PosConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<Product>(modelBuilder);
            OnModelCreatingIDomainable<ProductCategorization>(modelBuilder);
            OnModelCreatingIDomainable<ProductGenericProperty>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformation>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationCombine>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationComment>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationContentTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationMarketplace>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationMarketplaceBulkPrice>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationPhoto>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationPrice>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationSeoTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationSubscription>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationTranslationBreadcrumb>(modelBuilder);
            OnModelCreatingIDomainable<ProductTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationVariant>(modelBuilder);

            OnModelCreatingIDomainable<ProductMarketplace>(modelBuilder);

            OnModelCreatingIDomainable<ProductLabel>(modelBuilder);
            OnModelCreatingIDomainable<ProductProperty>(modelBuilder);
            OnModelCreatingIDomainable<ProductTag>(modelBuilder);
            OnModelCreatingIDomainable<Property>(modelBuilder);
            OnModelCreatingIDomainable<PropertyTranslation>(modelBuilder);
            OnModelCreatingIDomainable<PropertyValue>(modelBuilder);
            OnModelCreatingIDomainable<PropertyValueTranslation>(modelBuilder);
            OnModelCreatingIDomainable<ShipmentCompanyCompany>(modelBuilder);
            OnModelCreatingIDomainable<ShipmentCompanyConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<ShoppingCartDiscounted>(modelBuilder);
            OnModelCreatingIDomainable<ShoppingCartDiscountedSetting>(modelBuilder);
            OnModelCreatingIDomainable<ShoppingCartDiscountedProduct>(modelBuilder);
            OnModelCreatingIDomainable<ShoppingCartDiscountedProductSetting>(modelBuilder);
            OnModelCreatingIDomainable<Showcase>(modelBuilder);
            OnModelCreatingIDomainable<SmsProviderCompany>(modelBuilder);
            OnModelCreatingIDomainable<SmsProviderConfiguration>(modelBuilder);
            OnModelCreatingIDomainable<SmsTemplate>(modelBuilder);
            OnModelCreatingIDomainable<Supplier>(modelBuilder);
            OnModelCreatingIDomainable<Tag>(modelBuilder);
            OnModelCreatingIDomainable<TagTranslation>(modelBuilder);
            OnModelCreatingIDomainable<TwoFactorCustomerUser>(modelBuilder);
            OnModelCreatingIDomainable<Variant>(modelBuilder);
            OnModelCreatingIDomainable<VariantTranslation>(modelBuilder);
            OnModelCreatingIDomainable<VariantValue>(modelBuilder);
            OnModelCreatingIDomainable<VariantValueTranslation>(modelBuilder);
            OnModelCreatingIDomainable<OrderReturnDetail>(modelBuilder);
            OnModelCreatingIDomainable<ProductInformationMarketplaceVariantValue>(modelBuilder);
            OnModelCreatingIDomainable<NebimOrder>(modelBuilder);
            OnModelCreatingIDomainable<NebimOrderDetail>(modelBuilder);
            OnModelCreatingIDomainable<NebimMarketplaceCreditCardCode>(modelBuilder);
            OnModelCreatingIDomainable<NebimMarketplaceSalesPersonnelCode>(modelBuilder);
            OnModelCreatingIDomainable<MarketplaceCategoryVariantValueMapping>(modelBuilder);

            #endregion

            #region ICompanyable
            OnModelCreatingICompanyable<OrderViewing>(modelBuilder);
            OnModelCreatingICompanyable<Expense>(modelBuilder);
            OnModelCreatingICompanyable<AccountingCompany>(modelBuilder);
            OnModelCreatingICompanyable<AccountingConfiguration>(modelBuilder);
            OnModelCreatingICompanyable<Customer>(modelBuilder);
            OnModelCreatingICompanyable<CustomerAddress>(modelBuilder);
            OnModelCreatingICompanyable<CustomerContact>(modelBuilder);
            OnModelCreatingICompanyable<CustomerInvoiceInformation>(modelBuilder);
            OnModelCreatingICompanyable<CustomerNote>(modelBuilder);
            OnModelCreatingICompanyable<CustomerUser>(modelBuilder);
            OnModelCreatingICompanyable<EmailProviderCompany>(modelBuilder);
            OnModelCreatingICompanyable<EmailProviderConfiguration>(modelBuilder);
            OnModelCreatingICompanyable<MarketplaceCompany>(modelBuilder);
            OnModelCreatingICompanyable<MarketplaceConfiguration>(modelBuilder);
            OnModelCreatingICompanyable<Order>(modelBuilder);
            OnModelCreatingICompanyable<MarketplaceCargoFee>(modelBuilder);
            OnModelCreatingICompanyable<OrderDeliveryAddress>(modelBuilder);
            OnModelCreatingICompanyable<OrderDetail>(modelBuilder);
            OnModelCreatingICompanyable<MarketplaceCommission>(modelBuilder);
            OnModelCreatingICompanyable<OrderInvoiceInformation>(modelBuilder);
            OnModelCreatingICompanyable<OrderReserve>(modelBuilder);
            OnModelCreatingICompanyable<OrderNote>(modelBuilder);
            OnModelCreatingICompanyable<OrderShipment>(modelBuilder);
            OnModelCreatingICompanyable<OrderShipmentDetail>(modelBuilder);
            OnModelCreatingICompanyable<OrderSms>(modelBuilder);
            OnModelCreatingICompanyable<ShipmentCompanyCompany>(modelBuilder);
            OnModelCreatingICompanyable<ShipmentCompanyConfiguration>(modelBuilder);
            OnModelCreatingICompanyable<SmsProviderConfiguration>(modelBuilder);
            OnModelCreatingICompanyable<SmsTemplate>(modelBuilder);
            OnModelCreatingICompanyable<SmsProviderCompany>(modelBuilder);

            OnModelCreatingICompanyable<OrderReturnDetail>(modelBuilder);
            OnModelCreatingICompanyable<ProductInformationMarketplaceVariantValue>(modelBuilder);
            OnModelCreatingICompanyable<ProductMarketplace>(modelBuilder);
            OnModelCreatingICompanyable<MarketplaceBrandMapping>(modelBuilder);
            OnModelCreatingICompanyable<ProductInformationMarketplaceBulkPrice>(modelBuilder);
            OnModelCreatingICompanyable<NebimOrder>(modelBuilder);
            OnModelCreatingICompanyable<NebimOrderDetail>(modelBuilder);
            OnModelCreatingICompanyable<NebimMarketplaceCreditCardCode>(modelBuilder);
            OnModelCreatingICompanyable<NebimMarketplaceSalesPersonnelCode>(modelBuilder);
            #endregion

            #region Encryption Conversions
            if (true)
            {
                modelBuilder
                       .Entity<ProductSourceConfiguration>()
                       .Property(e => e.Name)
                       .HasConversion(
                           v => CryptoHelper.Encrypt(v),
                           v => CryptoHelper.Decrypt(v)
                       );

                modelBuilder
                       .Entity<ProductSourceConfiguration>()
                       .Property(e => e.Key)
                       .HasConversion(
                           v => CryptoHelper.Encrypt(v),
                           v => CryptoHelper.Decrypt(v)
                       );

                modelBuilder
                       .Entity<ProductSourceConfiguration>()
                       .Property(e => e.Value)
                       .HasConversion(
                           v => CryptoHelper.Encrypt(v),
                           v => CryptoHelper.Decrypt(v)
                       );

                modelBuilder
                       .Entity<Tenant>()
                       .Property(e => e.Name)
                       .HasConversion(
                           v => CryptoHelper.Encrypt(v),
                           v => CryptoHelper.Decrypt(v)
                       );

                modelBuilder
                   .Entity<Domain.Entities.Domain>()
                   .Property(e => e.Name)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<Company>()
                   .Property(e => e.Name)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<Company>()
                   .Property(e => e.FullName)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<CompanyConfiguration>()
                   .Property(e => e.Name)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<CompanyConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<CompanyConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                    .Entity<CustomerUser>()
                    .Property(e => e.Username)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerUser>()
                    .Property(e => e.Password)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerUser>()
                    .Property(e => e.Username)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<Customer>()
                    .Property(e => e.Name)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<Customer>()
                    .Property(e => e.Surname)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerContact>()
                    .Property(e => e.Mail)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerContact>()
                    .Property(e => e.Phone)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerContact>()
                    .Property(e => e.TaxOffice)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<CustomerContact>()
                    .Property(e => e.TaxNumber)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderDeliveryAddress>()
                    .Property(e => e.Address)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderDeliveryAddress>()
                    .Property(e => e.PhoneNumber)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderDeliveryAddress>()
                    .Property(e => e.Recipient)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderInvoiceInformation>()
                    .Property(e => e.Address)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderInvoiceInformation>()
                    .Property(e => e.FirstName)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                    .Entity<OrderInvoiceInformation>()
                    .Property(e => e.LastName)
                    .HasConversion(
                        v => CryptoHelper.Encrypt(v),
                        v => CryptoHelper.Decrypt(v)
                    );

                modelBuilder
                   .Entity<OrderInvoiceInformation>()
                   .Property(e => e.Mail)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<OrderInvoiceInformation>()
                   .Property(e => e.Phone)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<OrderInvoiceInformation>()
                   .Property(e => e.TaxNumber)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<OrderInvoiceInformation>()
                   .Property(e => e.TaxOffice)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<PosConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<PosConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<AccountingConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<AccountingConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<EmailProviderConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<EmailProviderConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<MarketplaceConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<MarketplaceConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<ShipmentCompanyConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<ShipmentCompanyConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<SmsProviderConfiguration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<SmsProviderConfiguration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<Configuration>()
                   .Property(e => e.Name)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<Configuration>()
                   .Property(e => e.Key)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<Configuration>()
                   .Property(e => e.Value)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<NewBulletin>()
                   .Property(e => e.IpAddress)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<NewBulletin>()
                   .Property(e => e.Mail)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<TwoFactorCustomerUser>()
                   .Property(e => e.Code)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                   .Entity<TwoFactorCustomerUser>()
                   .Property(e => e.Phone)
                   .HasConversion(
                       v => CryptoHelper.Encrypt(v),
                       v => CryptoHelper.Decrypt(v)
                   );


                modelBuilder
                  .Entity<CompanyContact>()
                  .Property(e => e.Email)
                  .HasConversion(
                      v => CryptoHelper.Encrypt(v),
                      v => CryptoHelper.Decrypt(v)
                  );

                modelBuilder
                .Entity<CompanyContact>()
                .Property(e => e.Address)
                .HasConversion(
                    v => CryptoHelper.Encrypt(v),
                    v => CryptoHelper.Decrypt(v)
                );

                modelBuilder
                 .Entity<CompanyContact>()
                 .Property(e => e.TaxOffice)
                 .HasConversion(
                     v => CryptoHelper.Encrypt(v),
                     v => CryptoHelper.Decrypt(v)
                 );

                modelBuilder
                 .Entity<CompanyContact>()
                 .Property(e => e.TaxNumber)
                 .HasConversion(
                     v => CryptoHelper.Encrypt(v),
                     v => CryptoHelper.Decrypt(v)
                 );

                modelBuilder
                   .Entity<CompanyContact>()
                   .Property(e => e.PhoneNumber)
                   .HasConversion(
                   v => CryptoHelper.Encrypt(v),
                   v => CryptoHelper.Decrypt(v)
                   );

                modelBuilder
                .Entity<CompanyContact>()
                .Property(e => e.RegistrationNumber)
                .HasConversion(
                v => CryptoHelper.Encrypt(v),
                v => CryptoHelper.Decrypt(v)
                );
                modelBuilder
               .Entity<CompanyContact>()
               .Property(e => e.WebUrl)
               .HasConversion(
               v => CryptoHelper.Encrypt(v),
               v => CryptoHelper.Decrypt(v)
               );
            }
            #endregion

            modelBuilder
                .Entity<XmlDefination>()
                .Property(_ => _.Fields)
                .HasConversion(
                    _ => string.Join(",", _),
                    _ => _.Split(new[] { ',' }).ToList());

            modelBuilder
                .Entity<Company>()
                .HasOne(t => t.Domain)
                .WithMany(x => x.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            #region Seed
            //modelBuilder.Entity<Tenant>().HasData(new Tenant
            //{
            //    Id = 1,
            //    Name = "Tenant 1"
            //});

            //modelBuilder.Entity<Domain.Entities.Domain>().HasData(new Domain.Entities.Domain
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    Name = "Domain 1"
            //});

            //modelBuilder.Entity<Company>().HasData(new Company
            //{
            //    Id = 1,
            //    DomainId = 1,
            //    TenantId = 1,
            //    Name = "Company 1"
            //});

            modelBuilder.Entity<Language>().HasData(new Language
            {
                Id = "TR",
                Active = true,
                Name = "Türkçe"
            });

            modelBuilder.Entity<Currency>().HasData(new Currency
            {
                Id = "TL",
                Active = true,
                Name = "Türk Lirası"
            });

            //modelBuilder.Entity<ManagerRole>().HasData(new ManagerRole
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    Name = "Admin"
            //});

            //modelBuilder.Entity<Manager>().HasData(new Manager
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    CreatedDate = System.DateTime.Now,
            //    Name = "",
            //    Surname = ""
            //});

            //modelBuilder.Entity<ManagerUser>().HasData(new ManagerUser
            //{
            //    Id = 1,
            //    Username = "admin",
            //    Password = "admin",
            //    TenantId = 1
            //});

            //modelBuilder.Entity<ManagerUserDomain>().HasData(new ManagerUserDomain
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    DomainId = 1,
            //    
            //});

            //modelBuilder.Entity<ManagerUserRole>().HasData(new ManagerUserRole
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    
            //    ManagerRoleId = 1,
            //});

            //modelBuilder.Entity<CustomerRole>().HasData(new CustomerRole
            //{

            //    Id= "DC",
            //    Name= "Default Customer"

            //});

            //modelBuilder.Entity<DomainSetting>().HasData(new DomainSetting
            //{
            //    Id = 1,
            //    TenantId = 1,
            //    ImagePath = "",
            //    ImageUrl = ""
            //});

            //modelBuilder.Entity<CompanySetting>().HasData(new CompanySetting
            //{
            //    Id = 1,
            //    DomainId = 1,
            //    TenantId = 1,
            //    IncludeMarketplace = true
            //});

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "TY",
                Active = true,
                BrandAutocomplete = true,
                Name = "Trendyol"
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "HB",
                Active = true,
                BrandAutocomplete = false,
                Name = "Hepsiburada"
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "CS",
                Active = true,
                BrandAutocomplete = false,
                Name = "Ciceksepeti"
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "PA",
                Active = true,
                BrandAutocomplete = true,
                Name = "Pazarama"
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "IK",
                Active = true,
                BrandAutocomplete = true,
                Name = "Ikas",
                IsECommerce = true
            });


            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "M",
                Active = true,
                BrandAutocomplete = false,
                Name = "Morhipo",
                IsECommerce = false
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "ML",
                Active = true,
                BrandAutocomplete = true,
                Name = "ModaLog",
                IsECommerce = false
            });

            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "MN",
                Active = true,
                BrandAutocomplete = true,
                Name = "Modanisa",
                IsECommerce = false
            });
            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "N11",
                Active = true,
                BrandAutocomplete = false,
                Name = "N11",
                IsECommerce = false
            });
            modelBuilder.Entity<Marketplace>().HasData(new Marketplace
            {
                Id = "PTT",
                Active = true,
                BrandAutocomplete = false,
                Name = "PttAvm",
                IsECommerce = false
            });

            //modelBuilder.Entity<OrderSource>().HasData(new OrderSource
            //{
            //    Id = 1,
            //    Name = "Pazaryeri",
            //    TenantId = 1
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "BE",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Beklemede",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "IP",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "İptal",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "OS",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Onaylanmış Sipariş",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "KTE",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Kargoya Teslim Edildi",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "PA",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Paketleniyor",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "TE",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Tedarik Edilemeyen",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "TS",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Teslim Edildi",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "TO",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Toplanıyor",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "TD",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Toplandı",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "PD",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Paketlendi",
            //            LanguageId = "TR"
            //        }
            //    }
            //});


            //modelBuilder.Entity<OrderType>().HasData(new OrderType
            //{
            //    Id = "TA",
            //    OrderTypeTranslations = new System.Collections.Generic.List<OrderTypeTranslation>
            //    {
            //        new OrderTypeTranslation
            //        {
            //            Name= "Tamamlandı",
            //            LanguageId = "TR"
            //        }
            //    }
            //});

            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "ARS",
            //    Name = "Aras Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "B",
            //    Name = "Borusan Lojistik"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "H",
            //    Name = "Horoz Lojistik"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "HX",
            //    Name = "HepsiJet"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "KU",
            //    Name = "Kurye"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "M",
            //    Name = "Mng Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "MN",
            //    Name = "Modanisa Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "P",
            //    Name = "Ptt Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "S",
            //    Name = "Sürat Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "TYEX",
            //    Name = "Trendyol Express"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "Y",
            //    Name = "Yurtiçi Kargo"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "ARD",
            //    Name = "Artos Datğıtım"
            //});
            //modelBuilder.Entity<ShipmentCompany>().HasData(new ShipmentCompany
            //{
            //    Id = "U",
            //    Name = "Ups"
            //});

            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "C",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "HAVALE",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=1,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});
            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "DO",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "Kapıda Ödeme",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=2,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});
            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "IK",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "İndirim Kuponu",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=3,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});
            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "MP",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "Para Puan",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=4,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});
            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "OO",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "Online Ödeme",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=5,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});
            //modelBuilder.Entity<PaymentType>().HasData(new PaymentType
            //{
            //    Id = "PDO",
            //    Active = true,
            //    PaymentTypeTranslations = new System.Collections.Generic.List<PaymentTypeTranslation>
            //    {
            //        new PaymentTypeTranslation
            //        {
            //            Name= "Kapıda Kredi Kartı / Nakit",
            //            LanguageId="TR"
            //        }
            //    },
            //    PaymentTypeDomains = new System.Collections.Generic.List<PaymentTypeDomain>
            //    {
            //        new PaymentTypeDomain
            //        {
            //            Id=6,
            //            TenantId=1,
            //            DomainId=1,
            //            Active=true,
            //            PaymentTypeSetting=new PaymentTypeSetting
            //            {
            //                Cost=0,
            //                CostIsRate=false,
            //                Discount=0,
            //                DiscountIsRate=false
            //            }
            //        }
            //    }
            //});

            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "EXC",
            //    Name = "Excel"
            //});
            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "JS",
            //    Name = "Json"
            //});
            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "M",
            //    Name = "Mikro"
            //});
            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "SM",
            //    Name = "Simsar"
            //});
            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "W",
            //    Name = "Winka"
            //});
            //modelBuilder.Entity<ProductSource>().HasData(new ProductSource
            //{
            //    Id = "XML",
            //    Name = "XML"
            //});

            //modelBuilder.Entity<SmsProvider>().HasData(new SmsProvider
            //{
            //    Id = "NG",
            //    Name = "NetGsm"
            //});
            #endregion

            base.OnModelCreating(modelBuilder);
        }
        #endregion

        #region Helper Methods
        private void OnModelCreatingICustomerUserCreatable<TIUserCreatable>(ModelBuilder modelBuilder) where TIUserCreatable : class, ICustomerUserCreatable
        {
            modelBuilder
                .Entity<TIUserCreatable>()
                .HasOne(t => t.CustomerUser)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
        }

        private void OnModelCreatingITenantable<TITenantEntity>(ModelBuilder modelBuilder) where TITenantEntity : class, ITenantable
        {
            modelBuilder
                .Entity<TITenantEntity>()
                .HasOne(t => t.Tenant)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
        }

        private void OnModelCreatingIDomainable<TIDomainEntity>(ModelBuilder modelBuilder) where TIDomainEntity : class, IDomainable
        {
            modelBuilder
                .Entity<TIDomainEntity>()
                .HasOne(t => t.Domain)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
        }

        private void OnModelCreatingICompanyable<TICompanyEntity>(ModelBuilder modelBuilder) where TICompanyEntity : class, ICompanyable
        {
            modelBuilder
                .Entity<TICompanyEntity>()
                .HasOne(t => t.Company)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
        }
        #endregion
    }
}