using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ProductInformationTranslationBreadcrumbRepository : RepositoryBase<Domain.Entities.ProductInformationTranslationBreadcrumb, int>, IProductInformationTranslationBreadcrumbRepository
	{
		#region Constructor
		public ProductInformationTranslationBreadcrumbRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public Domain.Entities.ProductInformationTranslationBreadcrumb Read(int id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}