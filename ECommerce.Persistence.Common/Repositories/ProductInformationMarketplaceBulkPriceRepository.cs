using Dapper;
using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Repository
{
    public partial class ProductInformationMarketplaceBulkPriceRepository : RepositoryBase<ProductInformationMarketplaceBulkPrice, Int32>, IProductInformationMarketplaceBulkPriceRepository
    {
        #region Constructors
        public ProductInformationMarketplaceBulkPriceRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        #region Dirty Product
        public async Task<Tuple<List<DirtyProduct>, List<ProductInformationPhoto>>> ReadDirtyProductsAsync(int queueId, string marketplaceId)
        {
            var tuple = new Tuple<List<DirtyProduct>, List<ProductInformationPhoto>>(
                new List<DirtyProduct>(),
                new List<ProductInformationPhoto>());

            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
With T As (
	Select	PIM.Id As ProductInformationMarketplaceId,
			PIM.ProductInformationId,
			PIM.Opened,
			PIM.AccountingPrice,
			PIM.Barcode,
			PIM.UUId As ProductInformationMarketplaceUUId,
			PIM.StockCode,
			PIM.UnitPrice,
			PIM.ListUnitPrice,
            PIM.TenantId,
            PIM.DomainId,
            PIM.CompanyId,
			MC.MarketplaceId,
			MC.Id As MarketplaceCompanyId
	From	ProductInformationMarketplaces As PIM With(NoLock)

    Join    Tenants As T
    On      PIM.TenantId = T.Id
            And T.QueueId = @QueueId

	Join	MarketplaceCompanies As MC With(NoLock)
	On		PIM.MarketplaceCompanyId = MC.Id
            --And MC.CreateProduct = 1

    Join    Marketplaces As M With(NoLock)
    On      MC.MarketplaceId = M.Id
            And M.Id = @MarketplaceId

	Where	PIM.Barcode <> ''
            And PIM.Barcode Is Not Null
            And PIM.DirtyProductInformation = 1
			And PIM.Active = 1
            And PIM.ListUnitPrice > 0
            And PIM.UnitPrice > 0
), T2 As (
	Select		T.*,
				[PI].ProductId,
				[PI].Stock,
				IsNull(PT.[Name], '') As ProductName
	From		T

	Join		ProductInformations As [PI] With(NoLock)
	On			T.ProductInformationId = [PI].Id

	Join		Products As P With(NoLock)
	On			[PI].ProductId = P.Id
                And P.IsOnlyHidden = 0
                --And P.SellerCode In ('23Y262041', '23Y695067', '23Y695066', '23Y262040', '23Y262030')

	Left Join	ProductTranslations As PT With(NoLock)
	On			PT.ProductId = P.Id
				And PT.LanguageId = 'TR'
), T3 As (
	Select	T2.ProductInformationMarketplaceId,
			T2.ProductInformationId,
			T2.Barcode,
			T2.ProductInformationMarketplaceUUId,
			T2.StockCode,
			T2.UnitPrice,
			T2.ListUnitPrice,
			T2.MarketplaceId,
			T2.ProductId,
			T2.Stock,
            T2.ProductName,
			T2.MarketplaceCompanyId,
            T2.TenantId,
            T2.DomainId,
            T2.CompanyId,
            T2.Opened,
			PIP.VatRate,
			PIT.[Name] As ProductInformationName,
			PIT.SubTitle,
			PICT.Content
	From	T2

	Join	ProductInformationTranslations As PIT With(NoLock)
	On		T2.ProductInformationId = PIT.ProductInformationId
			And PIT.LanguageId = 'TR'

	Join	ProductInformationPriceses As PIP With(NoLock)
	On		T2.ProductInformationId = PIP.ProductInformationId
			And PIP.CurrencyId = 'TL'

	Join	ProductInformationContentTranslations As PICT With(NoLock)
	On		PICT.Id = PIT.Id
)
Select	    T3.*,
		    PM.MarketplaceCategoryId,
		    MCMV.MarketplaceVariantId,
		    MVV.[Value] As MarketplaceVariantValue,
		    PIMVV.Id As ProductInformationMarketplaceVariantValueId,
		    MCMVV.MarketplaceVariantValueId,
		    MCMV.AllowCustom,
            MCMV.Mandatory,
            PM.Id As ProductMarketplaceId,
            PM.MarketplaceBrandId,
		    PM.MarketplaceBrandName,
		    PM.SellerCode,
		    PM.UUId As ProductMarketplaceUUId,
		    MC.[Name] As MarketplaceCategoryName,
            PIMVV.MarketplaceCategoriesMarketplaceVariantValueId,
            MCMVV.MarketplaceCategoriesMarketplaceVariantId,
            MV.Name as MarketplaceVariantName,
			PM.DeliveryDay
Into		#CreateTable
From	    T3

Join	    ProductInformationMarketplaceVariantValues As PIMVV With(NoLock)
On		    T3.ProductInformationMarketplaceId = PIMVV.ProductInformationMarketplaceId

Join	    MarketplaceCategoriesMarketplaceVariantValues As MCMVV With(NoLock)
On		    MCMVV.Id = PIMVV.MarketplaceCategoriesMarketplaceVariantValueId

Join	    MarketplaceCategoriesMarketplaceVariants As MCMV With(NoLock)
On		    MCMVV.MarketplaceCategoriesMarketplaceVariantId = MCMV.Id

Join	    MarketplaceVariantValues As MVV With(NoLock)
On		    MCMVV.MarketplaceVariantValueId = MVV.Id

Join	    ProductMarketplaces As PM With(NoLock)
On		    T3.ProductId = PM.ProductId
		    And T3.MarketplaceCompanyId = PM.MarketplaceCompanyId

Join	    MarketplaceCategories As MC With(NoLock)
On		    PM.MarketplaceCategoryId = MC.Id

Join        MarketplaceVariants As MV With(NoLock)
On          MV.Id = MCMV.MarketplaceVariantId
--Where       Len(PM.SellerCode) <= 40

Order By    MVV.[Value]

Select      *
From        #CreateTable

Select		DS.ImageUrl + '/product/' + PIP.[FileName] As [FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder
From		ProductInformationPhotos PIP With(NoLock)
Join		DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId
Where		PIP.Deleted = 0
			And PIP.ProductInformationId In (
				Select	ProductInformationId
				From	#CreateTable
			)
Group By	PIP.[FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder,
			DS.ImageUrl,
            PIP.Id
Order By	PIP.ProductInformationId,
			PIP.DisplayOrder,
            PIP.Id

Drop Table	#CreateTable";
#warning Remove

                try
                {
                    var gridReader = await sqlConnection.QueryMultipleAsync(query, new
                    {
                        QueueId = queueId,
                        MarketplaceId = marketplaceId
                    });
                    tuple.Item1.AddRange(gridReader.Read<DirtyProduct>());
                    tuple.Item2.AddRange(gridReader.Read<ProductInformationPhoto>());
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (sqlConnection.State != System.Data.ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }

            return tuple;
        }

        public async Task<Tuple<List<DirtyProduct>, List<ProductInformationPhoto>>> ReadDirtyProductsAsync(int queueId, bool opened, string marketplaceId)
        {
            var tuple = new Tuple<List<DirtyProduct>, List<ProductInformationPhoto>>(
                new List<DirtyProduct>(),
                new List<ProductInformationPhoto>());

            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
With T As (
	Select	PIM.Id As ProductInformationMarketplaceId,
			PIM.ProductInformationId,
			PIM.Opened,
			PIM.AccountingPrice,
			PIM.Barcode,
			PIM.UUId As ProductInformationMarketplaceUUId,
			PIM.StockCode,
			PIM.UnitPrice,
			PIM.ListUnitPrice,
            PIM.TenantId,
            PIM.DomainId,
            PIM.CompanyId,
			MC.MarketplaceId,
			MC.Id As MarketplaceCompanyId
	From	ProductInformationMarketplaces As PIM With(NoLock)

    Join    Tenants As T
    On      PIM.TenantId = T.Id
            And T.QueueId = @QueueId

	Join	MarketplaceCompanies As MC With(NoLock)
	On		PIM.MarketplaceCompanyId = MC.Id
            --And MC.CreateProduct = 1

    Join    Marketplaces As M With(NoLock)
    On      MC.MarketplaceId = M.Id
            And M.Id = @MarketplaceId

	Where	PIM.Barcode <> ''
            And PIM.Barcode Is Not Null
            And PIM.DirtyProductInformation = 1
			And PIM.Active = 1
            And PIM.Opened = @Opened
            And PIM.ListUnitPrice > 0
            And PIM.UnitPrice > 0
), T2 As (
	Select		T.*,
				[PI].ProductId,
				[PI].Stock,
				IsNull(PT.[Name], '') As ProductName
	From		T

	Join		ProductInformations As [PI] With(NoLock)
	On			T.ProductInformationId = [PI].Id

	Join		Products As P With(NoLock)
	On			[PI].ProductId = P.Id
                And P.IsOnlyHidden = 0
                And P.SellerCode In ('23Y262041', '23Y695067', '23Y695066', '23Y262040', '23Y262030')

	Left Join	ProductTranslations As PT With(NoLock)
	On			PT.ProductId = P.Id
				And PT.LanguageId = 'TR'
), T3 As (
	Select	T2.ProductInformationMarketplaceId,
			T2.ProductInformationId,
			T2.Barcode,
			T2.ProductInformationMarketplaceUUId,
			T2.StockCode,
			T2.UnitPrice,
			T2.ListUnitPrice,
			T2.MarketplaceId,
			T2.ProductId,
			T2.Stock,
            T2.ProductName,
			T2.MarketplaceCompanyId,
            T2.TenantId,
            T2.DomainId,
            T2.CompanyId,
            T2.Opened,
			PIP.VatRate,
			PIT.[Name] As ProductInformationName,
			PIT.SubTitle,
			PICT.Content
	From	T2

	Join	ProductInformationTranslations As PIT With(NoLock)
	On		T2.ProductInformationId = PIT.ProductInformationId
			And PIT.LanguageId = 'TR'

	Join	ProductInformationPriceses As PIP With(NoLock)
	On		T2.ProductInformationId = PIP.ProductInformationId
			And PIP.CurrencyId = 'TL'

	Join	ProductInformationContentTranslations As PICT With(NoLock)
	On		PICT.Id = PIT.Id
)
Select	    T3.*,
		    PM.MarketplaceCategoryId,
		    MCMV.MarketplaceVariantId,
		    MVV.[Value] As MarketplaceVariantValue,
		    PIMVV.Id As ProductInformationMarketplaceVariantValueId,
		    MCMVV.MarketplaceVariantValueId,
		    MCMV.AllowCustom,
            MCMV.Mandatory,
            PM.Id As ProductMarketplaceId,
            PM.MarketplaceBrandId,
		    PM.MarketplaceBrandName,
		    PM.SellerCode,
		    PM.UUId As ProductMarketplaceUUId,
		    MC.[Name] As MarketplaceCategoryName,
            PIMVV.MarketplaceCategoriesMarketplaceVariantValueId,
            MCMVV.MarketplaceCategoriesMarketplaceVariantId,
            MV.Name as MarketplaceVariantName,
			PM.DeliveryDay
Into		#CreateTable
From	    T3

Join	    ProductInformationMarketplaceVariantValues As PIMVV With(NoLock)
On		    T3.ProductInformationMarketplaceId = PIMVV.ProductInformationMarketplaceId

Join	    MarketplaceCategoriesMarketplaceVariantValues As MCMVV With(NoLock)
On		    MCMVV.Id = PIMVV.MarketplaceCategoriesMarketplaceVariantValueId

Join	    MarketplaceCategoriesMarketplaceVariants As MCMV With(NoLock)
On		    MCMVV.MarketplaceCategoriesMarketplaceVariantId = MCMV.Id

Join	    MarketplaceVariantValues As MVV With(NoLock)
On		    MCMVV.MarketplaceVariantValueId = MVV.Id

Join	    ProductMarketplaces As PM With(NoLock)
On		    T3.ProductId = PM.ProductId
		    And T3.MarketplaceCompanyId = PM.MarketplaceCompanyId

Join	    MarketplaceCategories As MC With(NoLock)
On		    PM.MarketplaceCategoryId = MC.Id

Join        MarketplaceVariants As MV With(NoLock)
On          MV.Id = MCMV.MarketplaceVariantId
--Where       Len(PM.SellerCode) <= 40

Order By    MVV.[Value]

Select      *
From        #CreateTable

Select		DS.ImageUrl + '/product/' + PIP.[FileName] As [FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder
From		ProductInformationPhotos PIP With(NoLock)
Join		DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId
Where		PIP.Deleted = 0
			And PIP.ProductInformationId In (
				Select	ProductInformationId
				From	#CreateTable
			)
Group By	PIP.[FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder,
			DS.ImageUrl,
            PIP.Id
Order By	PIP.ProductInformationId,
			PIP.DisplayOrder,
            PIP.Id

Drop Table	#CreateTable";
#warning Remove

                try
                {
                    var gridReader = await sqlConnection.QueryMultipleAsync(query, new
                    {
                        QueueId = queueId,
                        Opened = opened,
                        MarketplaceId = marketplaceId
                    });
                    tuple.Item1.AddRange(gridReader.Read<DirtyProduct>());
                    tuple.Item2.AddRange(gridReader.Read<ProductInformationPhoto>());
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (sqlConnection.State != System.Data.ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }

            return tuple;
        }

        public bool UpdateDirtyProduct(List<int> productInformationMarketplaceIds, bool dirtyProductInformation)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId   Int   Not Null,
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyProductInformation = @DirtyProductInformation
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.ProductInformationMarketplaceId = PIM.Id
            
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyProductInformation", dirtyProductInformation);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        //TO DO: UpdateDirtyProduct Id

        //TO DO: UpdateDirtyProduct List<Id>

        //TO DO: UpdateDirtyListStockByBarcode
        #endregion

        public ProductInformationMarketplaceBulkPrice Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        


        #endregion
    }
}