using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;
using ECommerce.Application.Common.ExceptionHandling;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Repository
{
    public partial class OrderShipmentRepository : RepositoryBase<OrderShipment, Int32>, IOrderShipmentRepository
    {
        #region Constructors
        public OrderShipmentRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        public OrderShipment Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public bool UpdateOrderType(int orderId, string trackingBase64)
        {
            bool updated = false;

            SqlConnection sqlConnection = null;

            ExceptionHandler.Handle(() =>
            {
                using (sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update OrderShipments Set TrackingBase64 = @TrackingBase64 Where OrderId = @OrderId", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
                        sqlCommand.Parameters.AddWithValue("@TrackingBase64", trackingBase64);

                        sqlConnection.Open();

                        var affectedRows = sqlCommand.ExecuteNonQuery();

                        updated = affectedRows > 0;
                    }
                }
            }, (exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
            return updated;
        }
        #endregion
    }
}