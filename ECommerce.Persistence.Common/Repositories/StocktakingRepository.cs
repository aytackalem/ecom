using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities.Domainable;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class StocktakingRepository : RepositoryBase<Stocktaking, int>, IStocktakingRepository
    {
        #region Constructor
        public StocktakingRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Stocktaking Read(int id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public Boolean Delete(int id)
        {
            var stocktaking = base.DbSet().FirstOrDefault(x => x.Id == id);

            base.OriginalDbSet().Remove(stocktaking);

            return base._dbContext.SaveChanges() > 0;
        }
        #endregion
    }
}