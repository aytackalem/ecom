using Dapper;
using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Parameters.ProductInformationMarketplaces;
using ECommerce.Application.Common.Parameters.ProductInformations;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Repository
{
    public partial class ProductInformationMarketplaceRepository : RepositoryBase<ProductInformationMarketplace, int>, IProductInformationMarketplaceRepository
    {
        #region Constructors
        public ProductInformationMarketplaceRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        #region Dirty Price
        public async Task<List<DirtyPrice>> ReadDirtyPricesAsync(int queueId, string marketplaceId)
        {
            List<DirtyPrice> list = new();
            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
Select		PIM.TenantId,
			PIM.DomainId,
			PIM.CompanyId,
			PIM.Id As ProductInformationMarketplaceId,
			[PI].SkuCode,
			PIM.StockCode,
			PIM.Barcode,
			PIM.ListUnitPrice,
			PIM.UnitPrice,
			@MarketplaceId As MarketplaceId,
			[PI].Stock,
			[PIP].VatRate,
			PIM.UUId As ProductInformationUUId,
			IsNull(PM.UUId, '') As ProductUUId,
            (Select Top 1 CreatedDateTime From ProductInformationMarketplacePriceLogs Where ProductInformationMarketplaceId = PIM.Id) As LastRequestDateTime,
			[PI].ProductId,
			PIM.ProductInformationId,
			IsNull(PM.SellerCode,P.SellerCode) As SellerCode,
			DS.ImageUrl + '/product/' + IsNull(PT.[FileName],'0/no-image.jpg') As PhotoUrl,
			ISNULL(PIT.Name,'') AS ProductInformationName,
            ISNULL(PIT.VariantValuesDescription,'') AS VariantValuesDescription

From		ProductInformationMarketplaces As PIM With(NoLock)

Join		Tenants As T With(NoLock)
On			PIM.TenantId = T.Id
			And T.QueueId = @QueueId

Join		MarketplaceCompanies As MC With(NoLock)
On			PIM.MarketplaceId = MC.MarketplaceId
			And PIM.TenantId = MC.TenantId
			And PIM.DomainId = MC.DomainId
			And PIM.CompanyId = MC.CompanyId
			And MC.UpdatePrice = 1

Join		ProductInformations As [PI] With(NoLock)
On			PIM.ProductInformationId = [PI].Id

Join		ProductInformationPriceses As PIP With(NoLock)
On			PIP.ProductInformationId = [PI].Id
			And PIP.CurrencyId = 'TL'

Join        Products As P With(NoLock)
On          P.Id = [PI].ProductId
            And P.IsOnlyHidden = 0

Left Join	ProductMarketplaces As PM With(NoLock)
On			[PI].ProductId = PM.ProductId
            And PM.TenantId = MC.TenantId
			And PM.DomainId = MC.DomainId
			And PM.CompanyId = MC.CompanyId
			And PM.MarketplaceId = @MarketplaceId

Left Join   ProductInformationPhotos  PT ON PT.Id = 
			(Select TOP 1 Id From ProductInformationPhotos Where ProductInformationId = PIM.ProductInformationId)   

Left Join  ProductInformationTranslations PIT ON PIT.Id =
		   (Select TOP 1 Id From ProductInformationTranslations Where ProductInformationId = PIM.ProductInformationId And LanguageId='TR')

Left Join	DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId

Where		PIM.DirtyPrice = 1
			And PIM.Active = 1
            And PIM.Locked = 0
            And PIM.UnitPrice > 0
            And PIM.MarketplaceId = @MarketplaceId           
            And PIM.Opened = 1
            And PIM.DisabledUpdateProduct = 0";
                try
                {
                    list.AddRange(await sqlConnection.QueryAsync<DirtyPrice>(query, new
                    {
                        QueueId = queueId,
                        MarketplaceId = marketplaceId
                    }));
                }
                catch
                {
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }
            return list;
        }

        public void UpdateDirtyPrice(int id, bool dirtyPrice)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                using (var sqlCommand = new SqlCommand("Update ProductInformationMarketplaces Set DirtyPrice = @DirtyPrice Where Id = @Id", sqlConnection))
                {
                    try
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@DirtyPrice", dirtyPrice);

                        sqlConnection.Open();

                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                            sqlConnection.Close();
                    }
                }
            }
        }

        public bool UpdateDirtyPrice(List<int> productInformationMarketplaceIds, bool dirtyPrice)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId   Int   Not Null,
            )", sqlConnection))
                {
                    try
                    {
                        var arc = sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyPrice = @DirtyPrice
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.ProductInformationMarketplaceId = PIM.Id
            
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyPrice", dirtyPrice);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        public void UpdateDirtyListPriceByBarcode(List<string> barcodes, int marketplaceCompanyId, bool dirtyPrice, int companyId)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                Barcode   NVarChar(500)  Not Null
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(int)));


                foreach (var id in barcodes)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["Barcode"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyPrice = @DirtyPrice
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.Barcode = PIM.Barcode
			Where	PIM.MarketplaceCompanyId= @MarketplaceCompanyId
            And     PIM.CompanyId=@CompanyId
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyPrice", dirtyPrice);
                        sqlCommand.Parameters.AddWithValue("@MarketplaceCompanyId", marketplaceCompanyId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }
        }
        #endregion

        #region Dirty Stock
        public async Task<List<DirtyStock>> ReadDirtyStocksAsync(int queueId, string marketplaceId)
        {
            List<DirtyStock> list = new();
            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
Select		PIM.TenantId,
			PIM.DomainId,
			PIM.CompanyId,
			PIM.Id As ProductInformationMarketplaceId,
			[PI].SkuCode,
			PIM.StockCode,
			PIM.Barcode,
			[PI].Stock,
			PIM.ListUnitPrice,
			PIM.UnitPrice,
			[PIP].VatRate,
			@MarketplaceId As MarketplaceId,
			PIM.UUId As ProductInformationUUId,
			IsNull(PM.UUId, '') As ProductUUId,
			MC.UpdatePrice,
            (Select Top 1 CreatedDateTime From ProductInformationMarketplaceStockLogs Where ProductInformationMarketplaceId = PIM.Id) As LastRequestDateTime,
			[PI].ProductId,
			PIM.ProductInformationId,
			IsNull(PM.SellerCode,P.SellerCode) As SellerCode,
			DS.ImageUrl + '/product/' + IsNull(PT.[FileName],'0/no-image.jpg') As PhotoUrl,
			ISNULL(PIT.Name,'') AS ProductInformationName,
            ISNULL(PIT.VariantValuesDescription,'') AS VariantValuesDescription

From		ProductInformationMarketplaces As PIM With(NoLock)

Join		Tenants As T With(NoLock)
On			PIM.TenantId = T.Id
			And T.QueueId = @QueueId

Join		MarketplaceCompanies As MC With(NoLock)
On			PIM.MarketplaceId = MC.MarketplaceId
			And PIM.TenantId = MC.TenantId
			And PIM.DomainId = MC.DomainId
			And PIM.CompanyId = MC.CompanyId
			And MC.UpdateStock = 1

Join		ProductInformations As [PI] With(NoLock)
On			PIM.ProductInformationId = [PI].Id

Join		ProductInformationPriceses As PIP With(NoLock)
On			PIP.ProductInformationId = [PI].Id
			And PIP.CurrencyId = 'TL'

Join        Products As P With(NoLock)
On          P.Id = [PI].ProductId
            And P.IsOnlyHidden = 0

Left Join	ProductMarketplaces As PM With(NoLock)
On			[PI].ProductId = PM.ProductId
            And PM.TenantId = MC.TenantId
			And PM.DomainId = MC.DomainId
			And PM.CompanyId = MC.CompanyId
			And PM.MarketplaceId = @MarketplaceId

Left Join   ProductInformationPhotos  PT ON PT.Id = 
			(Select TOP 1 Id From ProductInformationPhotos Where ProductInformationId = PIM.ProductInformationId)   

Left Join  ProductInformationTranslations PIT ON PIT.Id =
		   (Select TOP 1 Id From ProductInformationTranslations Where ProductInformationId = PIM.ProductInformationId And LanguageId='TR')

Left Join	DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId

Where		PIM.DirtyStock = 1
			And PIM.Active = 1
            And PIM.Locked = 0
            And PIM.MarketplaceId = @MarketplaceId
            And PIM.Opened = 1";
                try
                {
                    list.AddRange(await sqlConnection.QueryAsync<DirtyStock>(query, new
                    {
                        QueueId = queueId,
                        MarketplaceId = marketplaceId
                    }));
                }
                catch
                {
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }
            return list;
        }

        public void UpdateDirtyStock(int id, bool dirtyStock)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                using (var sqlCommand = new SqlCommand("Update ProductInformationMarketplaces Set DirtyStock = @DirtyStock Where Id = @Id", sqlConnection))
                {
                    try
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@DirtyStock", dirtyStock);

                        sqlConnection.Open();

                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                            sqlConnection.Close();
                    }
                }
            }
        }

        public bool UpdateDirtyStock(List<int> productInformationMarketplaceIds, bool dirtyStock)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId   Int   Not Null,
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyStock = @DirtyStock
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.ProductInformationMarketplaceId = PIM.Id
            
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyStock", dirtyStock);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        public void UpdateDirtyListStockByBarcode(List<string> barcodes, int marketplaceCompanyId, bool dirtyStock, int companyId)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                Barcode   NVarChar(500)  Not Null
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(int)));


                foreach (var id in barcodes)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["Barcode"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyStock = @DirtyStock
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.Barcode = PIM.Barcode
			Where	PIM.MarketplaceCompanyId = @MarketplaceCompanyId
            And     PIM.CompanyId = @CompanyId
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyStock", dirtyStock);
                        sqlCommand.Parameters.AddWithValue("@MarketplaceCompanyId", marketplaceCompanyId);
                        sqlCommand.Parameters.AddWithValue("@CompanyId", companyId);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();


                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

        }
        #endregion

        public ProductInformationMarketplace Read(int id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public void BulkInsertMapping(List<ProductInformationMarketplace> productInformationMarketplaces)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {

                var productsCopy = new List<ProductInformationMarketplace>();
                productsCopy.AddRange(productInformationMarketplaces);

                _dbContext.BulkInsert(productsCopy, new BulkConfig { SetOutputIdentity = true });


                var productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();
                foreach (var piLoop in productInformationMarketplaces)
                {

                    foreach (var pivLoop in piLoop.ProductInformationMarketplaceVariantValues)
                    {
                        pivLoop.ProductInformationMarketplaceId = piLoop.Id;
                    }

                    productInformationMarketplaceVariantValues.AddRange(piLoop.ProductInformationMarketplaceVariantValues);
                }

                _dbContext.BulkInsert(productInformationMarketplaceVariantValues, new BulkConfig { SetOutputIdentity = true });


                transaction.Commit();
            }
        }

        public void BulkCompare(BulkCompareRequest bulkCompareRequest)
        {

            if (bulkCompareRequest.BulkCompareItems.Count == 0) return;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                var keyColumnName = bulkCompareRequest.KeyColumnName;
                var companyId = base._companyFinder.FindId();
                var temporaryTableName = $"_Compare{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
Create Table {temporaryTableName} (
    MarketplaceId   NVarChar(10)    Not Null,
    Opened          Bit             Not Null,
    Locked          Bit             Not Null,
    Url             NVarChar(1000)  Not Null,
    OnSale          Bit             Not Null,
    {keyColumnName} NVarChar(1000)  Not Null,
    CompanyId       Int             Not Null,
    UnitPrice       Decimal(18,2)   Not Null,
    Stock           Int             Not Null,
    UUId            NVarChar(1000)  Null,
    DirtyPriceDisabled  Bit         Not Null,
    DirtyStockDisabled  Bit         Not Null,
)", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("MarketplaceId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Opened", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Locked", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Url", typeof(string)));
                dataTable.Columns.Add(new DataColumn("OnSale", typeof(bool)));
                dataTable.Columns.Add(new DataColumn(keyColumnName, typeof(string)));
                dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("Stock", typeof(int)));
                dataTable.Columns.Add(new DataColumn("UUId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("DirtyPriceDisabled", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("DirtyStockDisabled", typeof(bool)));
                foreach (var theBulkCompareItem in bulkCompareRequest.BulkCompareItems)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["MarketplaceId"] = theBulkCompareItem.MarketplaceId;
                    dataRow["Opened"] = theBulkCompareItem.Opened;
                    dataRow["Locked"] = theBulkCompareItem.Locked;
                    dataRow["Url"] = theBulkCompareItem.Url;
                    dataRow["OnSale"] = theBulkCompareItem.OnSale;
                    dataRow[keyColumnName] = theBulkCompareItem.Key;
                    dataRow["CompanyId"] = companyId;
                    dataRow["UnitPrice"] = theBulkCompareItem.UnitPrice;
                    dataRow["Stock"] = theBulkCompareItem.Stock;
                    dataRow["UUId"] = theBulkCompareItem.UUId;
                    dataRow["DirtyPriceDisabled"] = theBulkCompareItem.DirtyPriceDisabled;
                    dataRow["DirtyStockDisabled"] = theBulkCompareItem.DirtyStockDisabled;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"

Update     PIM
Set        PIM.Opened = 0
           ,PIM.OnSale = 0
           ,PIM.Url = null
           ,PIM.Locked=0
           ,PIM.DirtyStock =   0
           ,PIM.DirtyPrice =   0
           ,PIM.StatusCheckDate = GetDate()
From       ProductInformationMarketplaces PIM With(NoLock)
Left Join  {temporaryTableName} T With(NoLock) ON T.{keyColumnName} = PIM.{keyColumnName}
Where	   PIM.CompanyId=@CompanyId 
            And PIM.DomainId=@DomainId 
            And PIM.TenantId=@TenantId 
            And T.Opened IS NULL
            And PIM.MarketplaceId=@MarketplaceId


Update  PIM
Set     PIM.Opened = T.Opened
        ,PIM.OnSale = T.OnSale
        ,PIM.Url = T.Url
        ,PIM.Locked=T.Locked
        ,PIM.DirtyStock =   Case 
                                When T.DirtyStockDisabled = 0 And T.Locked = 0 And T.Opened = 1 And T.MarketplaceStock <> T.Stock 
                                Then 1 
                                Else 0 
                            End
        ,PIM.DirtyPrice =   Case 
                                When  T.DirtyPriceDisabled = 0 And T.Locked = 0 And T.Opened = 1 And T.MarketplaceUnitPrice <> T.UnitPrice 
                                Then 1 
                                Else 0 
                            End
        ,PIM.StatusCheckDate = GetDate()
        ,PIM.UUId = Case 
                        When ISNULL(T.UUId,'')<>''
                        Then T.UUId
                        Else PIM.UUId
                        End
From    ProductInformationMarketplaces As PIM With(NoLock)
Join    (
    Select		PIM.Id
                ,PIM.ProductInformationId
                ,PIM.StockCode
                ,PIM.Barcode
    			,T.UnitPrice As MarketplaceUnitPrice
                ,PIM.UnitPrice
                ,PIM.ListUnitPrice
                ,T.MarketplaceId
    			,T.Stock As MarketplaceStock
    			,[PI].Stock
                ,T.OnSale
                ,T.Opened
                ,T.Url
                ,T.Locked
                ,T.UUId
                ,T.DirtyPriceDisabled
                ,T.DirtyStockDisabled
    From		{temporaryTableName} As T With(NoLock)
    
    Join		ProductInformationMarketplaces As PIM With(NoLock)
    On			T.{keyColumnName} = PIM.{keyColumnName}
    			And PIM.Active = 1
    			And T.CompanyId = PIM.CompanyId And PIM.MarketplaceId=@MarketplaceId
    
    Join		ProductInformations As [PI] With(NoLock)
    On			[PI].Id = PIM.ProductInformationId
    
    Group By	PIM.Id
                ,PIM.ProductInformationId
                ,PIM.StockCode
                ,PIM.Barcode
    			,T.UnitPrice
                ,PIM.UnitPrice
                ,PIM.ListUnitPrice
                ,T.MarketplaceId
    			,T.Stock
    			,[PI].Stock
                ,T.OnSale
                ,T.Opened
                ,T.Url
                ,T.Locked
                ,T.UUId
                ,T.DirtyPriceDisabled
                ,T.DirtyStockDisabled
) As T
On	    T.Id = PIM.Id

Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.CommandTimeout = 300;
                        sqlCommand.Parameters.AddWithValue("@TenantId", base._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", base._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", base._companyFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@MarketplaceId", bulkCompareRequest.MarketplaceId);
                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }
        }

        public void UpdateDirtyProductInformation(int id, bool dirtyProductInformation)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                using (var sqlCommand = new SqlCommand("Update ProductInformationMarketplaces Set DirtyProductInformation = @DirtyProductInformation Where Id = @Id", sqlConnection))
                {
                    try
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@DirtyProductInformation", dirtyProductInformation);

                        sqlConnection.Open();

                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                            sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateDirtyProductInformationByProductId(int productId)
        {
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                using (var sqlCommand = new SqlCommand(@"
UPDATE 
		ProductInformationMarketplaces 
SET			
		DirtyProductInformation=1
From	ProductInformationMarketplaces PIM
Join	ProductInformations PIN On PIM.ProductInformationId=PIN.Id
Where	PIN.ProductId=@ProductId
		And PIN.TenantId=@TenantId
		And PIN.DomainId=@DomainId", sqlConnection))
                {
                    try
                    {

                        sqlCommand.Parameters.AddWithValue("@ProductId", productId);
                        sqlCommand.Parameters.AddWithValue("@TenantId", base._tenantFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@DomainId", base._domainFinder.FindId());

                        sqlConnection.Open();

                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                            sqlConnection.Close();
                    }
                }
            }
        }

        public int UpdateIgnoreSystemNotification(int id, bool ignoreSystemNotification)
        {
            int affectedRowsCount = 0;
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                using (var sqlCommand = new SqlCommand("Update ProductInformationMarketplaces Set IgnoreSystemNotification = @IgnoreSystemNotification Where Id = @Id", sqlConnection))
                {
                    try
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@IgnoreSystemNotification", ignoreSystemNotification);

                        sqlConnection.Open();

                        affectedRowsCount = sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                            sqlConnection.Close();
                    }
                }
            }
            return affectedRowsCount;
        }

        public bool UpdateOpened(List<int> productInformationMarketplaceIds, bool opened)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId Int Not Null
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  PIM
            Set     PIM.Opened = @Opened
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  
            On      T.ProductInformationMarketplaceId = PIM.Id
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Opened", opened);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        public bool UpdateUUId(List<UpdateUUIdRequest> requests)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                Id      Int             Not Null,
                UUId    NVarChar(1000)  Not Null
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
                dataTable.Columns.Add(new DataColumn("UUId", typeof(string)));


                foreach (var theRequest in requests)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["Id"] = theRequest.Id;
                    dataRow["UUId"] = theRequest.UUId;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  PIM
            Set     PIM.UUId = T.UUId
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  
            On      T.Id = PIM.Id
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == requests.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        public bool UpdateOpenedUrl(Dictionary<int, string> productInformationMarketplaceIds, bool opened, bool? dirtyStock = null, bool? dirtyPrice = null)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId     Int             Not Null,
                Url                                 NVarChar(1000)   Not Null
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Url", typeof(string)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id.Key;
                    dataRow["Url"] = id.Value;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    var query = @$"
            Update  PIM

            Set     PIM.Opened = @Opened,
                    {(dirtyStock.HasValue ? "PIM.DirtyStock = @DirtyStock," : "")}
                    {(dirtyPrice.HasValue ? "PIM.DirtyPrice = @DirtyPrice," : "")}
                    PIM.Url = T.Url
            From    ProductInformationMarketplaces As PIM

            Join	{temporaryTableName} As T  
            On      T.ProductInformationMarketplaceId = PIM.Id
            
            Drop table {temporaryTableName}";

                    using (var sqlCommand = new SqlCommand(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Opened", opened);

                        if (dirtyStock.HasValue)
                            sqlCommand.Parameters.AddWithValue("@DirtyStock", dirtyStock);

                        if (dirtyPrice.HasValue)
                            sqlCommand.Parameters.AddWithValue("@DirtyPrice", dirtyPrice);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        public void BulkStockLastUpdate(List<ProductInformationMarketplace> productInformationMarketplaces)
        {
            throw new Exception("ProductInformationMarketplaceLastDirtyPrice silindi");


//            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
//            {
//                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

//                sqlConnection.Open();

//                /* Create temporary table */
//                using (var sqlCommand = new SqlCommand(@$"
//            Create Table {temporaryTableName} (
//                Id              Int         Not Null,
//                UpdateDate      DateTime    Not Null,
//                Stock           Int         Not Null,
//                TenantId        Int         Not Null,
//                DomainId        Int         Not Null,
//                CompanyId       Int         Not Null
//            )", sqlConnection))
//                {
//                    try
//                    {
//                        sqlCommand.ExecuteNonQuery();
//                    }
//                    catch (Exception)
//                    {
//                        sqlConnection.Close();
//                        throw;
//                    }
//                }

//                /* Prepare bulk data. */
//                DataTable dataTable = new("");
//                dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
//                dataTable.Columns.Add(new DataColumn("UpdateDate", typeof(DateTime)));
//                dataTable.Columns.Add(new DataColumn("Stock", typeof(int)));
//                dataTable.Columns.Add(new DataColumn("TenantId", typeof(int)));
//                dataTable.Columns.Add(new DataColumn("DomainId", typeof(int)));
//                dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));

//                foreach (var theProductInformationMarketplace in productInformationMarketplaces)
//                {
//                    var dataRow = dataTable.NewRow();
//                    dataRow["Id"] = theProductInformationMarketplace.Id;
//                    dataRow["UpdateDate"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyStock.UpdateDate;
//                    dataRow["Stock"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyStock.Stock;
//                    dataRow["TenantId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyStock.TenantId;
//                    dataRow["DomainId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyStock.DomainId;
//                    dataRow["CompanyId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyStock.CompanyId;

//                    dataTable.Rows.Add(dataRow);
//                }

//                /* Bulk insert to temporary table. */
//                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
//                {
//                    bulkcopy.BulkCopyTimeout = 660;
//                    bulkcopy.DestinationTableName = temporaryTableName;
//                    try
//                    {
//                        bulkcopy.WriteToServer(dataTable);
//                    }
//                    catch (Exception)
//                    {
//                        sqlConnection.Close();
//                        throw;
//                    }
//                    bulkcopy.Close();
//                }

//                /* Read data */
//                try
//                {
//                    using (var sqlCommand = new SqlCommand(@$"
//Delete  PIMLDS
//From    ProductInformationMarketplaceLastDirtyStocks As PIMLDS
//Join    {temporaryTableName}
//On      PIMLDS.Id = {temporaryTableName}.Id

//Insert Into ProductInformationMarketplaceLastDirtyStocks
//Select  *
//From    {temporaryTableName}

//Drop table {temporaryTableName}", sqlConnection))
//                    {
//                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();
//                    }
//                }
//                catch (Exception)
//                {
//                    sqlConnection.Close();
//                    throw;
//                }

//                if (sqlConnection.State != ConnectionState.Closed)
//                    sqlConnection.Close();
//            }
        }

        public void BulkPriceLastUpdate(List<ProductInformationMarketplace> productInformationMarketplaces)
        {
            throw new Exception("ProductInformationMarketplaceLastDirtyPrice silindi");

            //            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            //            {
            //                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

            //                sqlConnection.Open();

            //                /* Create temporary table */
            //                using (var sqlCommand = new SqlCommand(@$"
            //            Create Table {temporaryTableName} (
            //                Id              Int         Not Null,
            //                UpdateDate      DateTime    Not Null,
            //                ListUnitPrice   Decimal     Not Null,
            //                UnitPrice       Decimal     Not Null,
            //                TenantId        Int         Not Null,
            //                DomainId        Int         Not Null,
            //                CompanyId       Int         Not Null
            //            )", sqlConnection))
            //                {
            //                    try
            //                    {
            //                        sqlCommand.ExecuteNonQuery();
            //                    }
            //                    catch (Exception)
            //                    {
            //                        sqlConnection.Close();
            //                        throw;
            //                    }
            //                }

            //                /* Prepare bulk data. */
            //                DataTable dataTable = new("");
            //                dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
            //                dataTable.Columns.Add(new DataColumn("UpdateDate", typeof(DateTime)));
            //                dataTable.Columns.Add(new DataColumn("ListUnitPrice", typeof(decimal)));
            //                dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
            //                dataTable.Columns.Add(new DataColumn("TenantId", typeof(int)));
            //                dataTable.Columns.Add(new DataColumn("DomainId", typeof(int)));
            //                dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));

            //                foreach (var theProductInformationMarketplace in productInformationMarketplaces)
            //                {
            //                    var dataRow = dataTable.NewRow();
            //                    dataRow["Id"] = theProductInformationMarketplace.Id;
            //                    dataRow["UpdateDate"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.UpdateDate;
            //                    dataRow["ListUnitPrice"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.ListUnitPrice;
            //                    dataRow["UnitPrice"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.UnitPrice;
            //                    dataRow["TenantId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.TenantId;
            //                    dataRow["DomainId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.DomainId;
            //                    dataRow["CompanyId"] = theProductInformationMarketplace.ProductInformationMarketplaceLastDirtyPrice.CompanyId;

            //                    dataTable.Rows.Add(dataRow);
            //                }

            //                /* Bulk insert to temporary table. */
            //                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
            //                {
            //                    bulkcopy.BulkCopyTimeout = 660;
            //                    bulkcopy.DestinationTableName = temporaryTableName;
            //                    try
            //                    {
            //                        bulkcopy.WriteToServer(dataTable);
            //                    }
            //                    catch (Exception)
            //                    {
            //                        sqlConnection.Close();
            //                        throw;
            //                    }
            //                    bulkcopy.Close();
            //                }

            //                /* Read data */
            //                try
            //                {
            //                    using (var sqlCommand = new SqlCommand(@$"
            //Delete  PIMLDS
            //From    ProductInformationMarketplaceLastDirtyPrices As PIMLDS
            //Join    {temporaryTableName}
            //On      PIMLDS.Id = {temporaryTableName}.Id

            //Insert Into ProductInformationMarketplaceLastDirtyPrices
            //Select  *
            //From    {temporaryTableName}

            //Drop table {temporaryTableName}", sqlConnection))
            //                    {
            //                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();
            //                    }
            //                }
            //                catch (Exception)
            //                {
            //                    sqlConnection.Close();
            //                    throw;
            //                }

            //                if (sqlConnection.State != ConnectionState.Closed)
            //                    sqlConnection.Close();
            //            }
        }

        public async Task PriceLogAsync(string marketplaceId, List<int> productInformationMarketplaceIds)
        {
            var connectionString = base._dbContext.Database.GetConnectionString();

            var tableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

            #region Create Temporary Table
            using (SqlConnection sqlConnection = new(connectionString))
            {
                await sqlConnection.ExecuteAsync(@$"
Create Table {tableName} 
(
    ProductInformationMarketplaceId Int Not Null
)");
            }
            #endregion

            #region Prepare Data Table
            DataTable dataTable = new("");
            dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

            foreach (var _ in productInformationMarketplaceIds)
            {
                var row = dataTable.NewRow();
                row["ProductInformationMarketplaceId"] = _;

                dataTable.Rows.Add(row);
            }
            #endregion

            #region Bulk Copy
            using (SqlConnection sqlConnection = new(connectionString))
            {
                await sqlConnection.OpenAsync();

                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = tableName;
                    bulkcopy.ColumnMappings.Add("ProductInformationMarketplaceId", "ProductInformationMarketplaceId");
                    bulkcopy.WriteToServer(dataTable);
                    bulkcopy.Close();
                }
            }
            #endregion

            #region Delete Insert Drop
            using (SqlConnection sqlConnection = new(connectionString))
            {
                try
                {
                    await sqlConnection.ExecuteAsync(@$"
Delete  L
From    ProductInformationMarketplacePriceLogs As L
Join    {tableName} As T
On      L.ProductInformationMarketplaceId = T.ProductInformationMarketplaceId
        And L.MarketplaceId = '{marketplaceId}'

Insert Into ProductInformationMarketplacePriceLogs
Select '{marketplaceId}', ProductInformationMarketplaceId, GetDate() From {tableName}

Drop Table {tableName}");
                }
                catch (Exception e)
                {

                }
            }
            #endregion
        }

        public async Task StockLogAsync(string marketplaceId, List<int> productInformationMarketplaceIds)
        {
            var connectionString = base._dbContext.Database.GetConnectionString();

            var tableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

            #region Create Temporary Table
            using (SqlConnection sqlConnection = new(connectionString))
            {
                await sqlConnection.ExecuteAsync(@$"
Create Table {tableName} 
(
    ProductInformationMarketplaceId Int Not Null
)");
            }
            #endregion

            #region Prepare Data Table
            DataTable dataTable = new("");
            dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

            foreach (var _ in productInformationMarketplaceIds)
            {
                var row = dataTable.NewRow();
                row["ProductInformationMarketplaceId"] = _;

                dataTable.Rows.Add(row);
            }
            #endregion

            #region Bulk Copy
            using (SqlConnection sqlConnection = new(connectionString))
            {
                sqlConnection.Open();

                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = tableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception e)
                    {

                    }
                    bulkcopy.Close();
                }
            }
            #endregion

            #region Delete Insert Drop
            using (SqlConnection sqlConnection = new(connectionString))
            {
                try
                {
                    await sqlConnection.ExecuteAsync(@$"
Delete  L
From    ProductInformationMarketplaceStockLogs As L
Join    {tableName} As T
On      L.ProductInformationMarketplaceId = T.ProductInformationMarketplaceId
        And L.MarketplaceId = '{marketplaceId}'

Insert Into ProductInformationMarketplaceStockLogs
Select '{marketplaceId}', ProductInformationMarketplaceId, GetDate() From {tableName}

Drop Table {tableName}");
                }
                catch (Exception e)
                {
                }
            }
            #endregion
        }
        #endregion
    }
}