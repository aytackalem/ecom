using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class CategoryProductRepository : RepositoryBase<CategoryProduct, Int32>, ICategoryProductRepository
    {
        #region Constructor
        public CategoryProductRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public CategoryProduct Read(Int32 id)
        {
            return base
                .DbSet()
                .Include(c => c.Category)
                .Include(p => p.Product)
                .FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}