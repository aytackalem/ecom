using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class CategoryTranslationBreadcrumbRepository : RepositoryBase<CategoryTranslationBreadcrumb, Int32>, ICategoryTranslationBreadcrumbRepository
    {
        #region Constructor
        public CategoryTranslationBreadcrumbRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public CategoryTranslationBreadcrumb Read(Int32 id)
        {
            return base
                .DbSet()
                .Include(c => c.CategoryTranslation)
                .FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}