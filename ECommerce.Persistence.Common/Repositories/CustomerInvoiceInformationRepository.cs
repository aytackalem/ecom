using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class CustomerInvoiceInformationRepository : RepositoryBase<CustomerInvoiceInformation, Int32>, ICustomerInvoiceInformationRepository
	{
		#region Constructor
		public CustomerInvoiceInformationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public CustomerInvoiceInformation Read(Int32 id)
		{
			return base.DbSet().FirstOrDefault(e => e.Id.Equals(id));
		}
		#endregion
	}
}