using Dapper;
using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Interfaces.Repositories.Base;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Repository
{
    public partial class MarketplaceConfigurationRepository : RepositoryBase<MarketplaceConfiguration, int>, IMarketplaceConfigurationRepository
    {
        #region Constructors
        public MarketplaceConfigurationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        public MarketplaceConfiguration Read(int id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public async Task<List<MarketplaceConfiguration>> ReadByQueueIdAsync(int queueId)
        {
            List<MarketplaceConfiguration> list = new();
            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
Select	MC.* 
From	MarketPlaceConfigurations As MC With(NoLock)
Join	Tenants T 
On      T.Id = MC.TenantId
Where	QueueId = @QueueId

Select	MC.*
From	MarketplaceCompanies As MC With(NoLock)
Join	Tenants T 
On      T.Id = MC.TenantId
Where	QueueId = @QueueId";

                try
                {
                    var results = await sqlConnection.QueryMultipleAsync(query, new { QueueId = queueId });
                    var marketplaceConfigurations = (await results.ReadAsync<MarketplaceConfiguration>()).ToList();
                    var marketplaceCompanies = (await results.ReadAsync<MarketplaceCompany>()).ToList();

                    marketplaceConfigurations.ForEach(theMarketplaceConfiguration =>
                    {
                        theMarketplaceConfiguration.MarketplaceCompany = marketplaceCompanies.FirstOrDefault(mc => mc.Id == theMarketplaceConfiguration.MarketplaceCompanyId);
                    });

                    list = marketplaceConfigurations;
                }
                catch
                {
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }
            return list;
        }
        #endregion
    }
}