using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository;

public class WholesaleOrderTypeRepository : RepositoryBase<WholesaleOrderType, String>, IWholesaleOrderTypeRepository
{
    #region Constructor
    public WholesaleOrderTypeRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
    {
    }
    #endregion

    #region Method
    public WholesaleOrderType Read(string id)
    {
        return base.DbSet().FirstOrDefault(v => v.Id == id);
    }
    #endregion
}