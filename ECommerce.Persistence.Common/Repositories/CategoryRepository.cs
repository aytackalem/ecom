using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class CategoryRepository : RepositoryBase<Category, Int32>, ICategoryRepository
    {
        #region Constructor
        public CategoryRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Category Read(Int32 id)
        {
            return base.DbSet().Include(c => c.CategoryTranslations).FirstOrDefault(c => c.Id == id);
        }

        public Category ReadAll(Int32 id)
        {
            return base
                .DbSet()
                .Include(c => c.CategoryTranslations)
                    .ThenInclude(x => x.CategoryContentTranslation)
                .Include(c => c.CategoryTranslations)
                    .ThenInclude(x => x.CategorySeoTranslation)
                .Include(c => c.CategoryTranslations)
                    .ThenInclude(x => x.CategoryTranslationBreadcrumb)
                .FirstOrDefault(c => c.Id == id);
        }


        public void BulkInsertMapping(List<Category> categories)
        {

            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var categoryCopy = new List<Category>();
                categoryCopy.AddRange(categories);

                _dbContext.BulkInsert<Category>(categoryCopy, new BulkConfig { SetOutputIdentity = true });

                int categoryIndex = categoryCopy[0].Id;
                categories.ForEach(x =>
                {
                    x.Id = categoryIndex;
                    categoryIndex++;
                });

                var categoryTranslations = new List<CategoryTranslation>();

                foreach (var cLoop in categories)
                {

                    foreach (var pmLoop in cLoop.CategoryTranslations)
                    {
                        pmLoop.CategoryId = cLoop.Id;
                        pmLoop.Url = $"{pmLoop.Name.GenerateUrl()}-{cLoop.Id}-c";
                    }
                    categoryTranslations.AddRange(cLoop.CategoryTranslations);

                }

                _dbContext.BulkInsert(categoryTranslations, new BulkConfig { SetOutputIdentity = true });


                var categorySeoTranslations = new List<CategorySeoTranslation>();
                var categoryContentTranslations = new List<CategoryContentTranslation>();


                foreach (var ct in categoryTranslations)
                {
                    ct.CategorySeoTranslation.Id = ct.Id;
                    ct.CategoryContentTranslation.Id = ct.Id;

                    categorySeoTranslations.Add(ct.CategorySeoTranslation);
                    categoryContentTranslations.Add(ct.CategoryContentTranslation);
                }
                _dbContext.BulkInsert(categorySeoTranslations, new BulkConfig { SetOutputIdentity = true });
                _dbContext.BulkInsert(categoryContentTranslations, new BulkConfig { SetOutputIdentity = true });

                transaction.Commit();
            }
        }
        #endregion
    }
}