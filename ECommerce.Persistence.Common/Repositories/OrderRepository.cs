using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class OrderRepository : RepositoryBase<Order, Int32>, IOrderRepository
    {
        #region Constructors
        public OrderRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        public Order Read(Int32 id)
        {
            return base
                .DbSet()
                .Include(s => s.OrderSource)
                .Include(c => c.Customer.CustomerContact)
                .Include(x => x.OrderType.OrderTypeTranslations)
                .Include(c => c.Marketplace)
                .Include(c => c.Application)
                .Include(c => c.OrderShipments)
                .ThenInclude(x => x.ShipmentCompany)
                .Include(c => c.Payments)
                .ThenInclude(c => c.PaymentType.PaymentTypeTranslations)
                .Include(c => c.OrderNotes)
                .Include(c => c.OrderDetails)
                .ThenInclude(c => c.ProductInformation)
                .ThenInclude(c => c.ProductInformationVariants)
                .ThenInclude(c => c.VariantValue.VariantValueTranslations)
                .Include(c => c.OrderDetails)
                .ThenInclude(c => c.ProductInformation)
                .ThenInclude(c => c.ProductInformationTranslations)
                .Include(c => c.OrderDetails)
                .ThenInclude(c => c.ProductInformation)
                .ThenInclude(c => c.ProductInformationPhotos)
                .Include(c => c.OrderInvoiceInformation.Neighborhood.District.City.Country)
                .Include(c => c.OrderDeliveryAddress.Neighborhood.District.City.Country)
                .FirstOrDefault(x => x.Id == id);
        }

        public bool UpdateOrderType(int id, string orderTypeId)
        {
            bool updated = false;

            SqlConnection sqlConnection = null;

            ExceptionHandler.Handle(() =>
            {
                using (sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update Orders Set OrderTypeId = @OrderTypeId Where Id = @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@OrderTypeId", orderTypeId);

                        sqlConnection.Open();

                        var affectedRows = sqlCommand.ExecuteNonQuery();

                        updated = affectedRows > 0;
                    }
                }
            }, (exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
            return updated;
        }

        public bool UpdateOrderType(int id, string orderTypeId, string packageNumber, string trackingCode)
        {
            bool updated = false;

            SqlConnection sqlConnection = null;

            ExceptionHandler.Handle(() =>
            {
                using (sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("Update Orders Set OrderTypeId = @OrderTypeId Where Id = @Id;Update OrderShipments Set PackageNumber = @PackageNumber, TrackingCode = @TrackingCode Where OrderId = @Id", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@OrderTypeId", orderTypeId);
                        sqlCommand.Parameters.AddWithValue("@PackageNumber", packageNumber);
                        sqlCommand.Parameters.AddWithValue("@TrackingCode", trackingCode);

                        sqlConnection.Open();

                        var affectedRows = sqlCommand.ExecuteNonQuery();

                        updated = affectedRows > 0;
                    }
                }
            }, (exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
            return updated;
        }

        public void BulkInsertMapping(List<Order> orders)
        {
            if (orders.Count > 0)
            {


                using (var transaction = _dbContext.Database.BeginTransaction())
                {

                    var customerCopy = new List<Customer>();
                    var customers = orders.Select(x => x.Customer).ToList();
                    customerCopy.AddRange(customers);

                    _dbContext.BulkInsert<Customer>(customerCopy, new BulkConfig { SetOutputIdentity = true });


                    var customerContacts = new List<CustomerContact>();
                    var customerAddresses = new List<CustomerAddress>();

                    int customerIndex = customerCopy[0].Id;
                    orders.ForEach(x =>
                    {
                        x.CustomerId = customerIndex;
                        x.Customer.CustomerContact.Id = customerIndex;
                        foreach (var caLoop in x.Customer.CustomerAddresses)
                        {
                            caLoop.CustomerId = customerIndex;
                        }
                        customerContacts.Add(x.Customer.CustomerContact);
                        customerAddresses.AddRange(x.Customer.CustomerAddresses);

                        customerIndex++;

                    });
                    _dbContext.BulkInsert(customerContacts, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(customerAddresses, new BulkConfig { SetOutputIdentity = true });



                    var orderCopy = new List<Order>();
                    orderCopy.AddRange(orders);

                    _dbContext.BulkInsert<Order>(orderCopy, new BulkConfig { SetOutputIdentity = true });

                    int orderIndex = orderCopy[0].Id;
                    orders.ForEach(x =>
                    {
                        x.Id = orderIndex;
                        orderIndex++;
                    });

                    var orderDetails = new List<OrderDetail>();
                    var orderShipments = new List<OrderShipment>();
                    var orderDeliveryAddress = new List<OrderDeliveryAddress>();
                    var orderInvoiceInformations = new List<OrderInvoiceInformation>();
                    var orderTechnicInformations = new List<OrderTechnicInformation>();
                    var orderNotes = new List<OrderNote>();

                    foreach (var odLoop in orders)
                    {
                        foreach (var pmLoop in odLoop.OrderDetails)
                        {
                            pmLoop.OrderId = odLoop.Id;
                        }
                        orderDetails.AddRange(odLoop.OrderDetails);


                        foreach (var osLoop in odLoop.OrderShipments)
                        {
                            osLoop.OrderId = odLoop.Id;
                        }
                        orderShipments.AddRange(odLoop.OrderShipments);


                        odLoop.OrderDeliveryAddress.Id = odLoop.Id;
                        orderDeliveryAddress.Add(odLoop.OrderDeliveryAddress);

                        odLoop.OrderInvoiceInformation.Id = odLoop.Id;
                        orderInvoiceInformations.Add(odLoop.OrderInvoiceInformation);


                        odLoop.OrderTechnicInformation.Id = odLoop.Id;
                        orderTechnicInformations.Add(odLoop.OrderTechnicInformation);


                        foreach (var onLoop in odLoop.OrderNotes)
                        {
                            onLoop.OrderId = odLoop.Id;
                        }
                        orderNotes.AddRange(odLoop.OrderNotes);

                    }

                    _dbContext.BulkInsert(orderDetails, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(orderShipments, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(orderDeliveryAddress, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(orderInvoiceInformations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(orderTechnicInformations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(orderNotes, new BulkConfig { SetOutputIdentity = true });




                    transaction.Commit();
                }
            }
        }

        public int DeliveredBulkCompare(List<Order> orders, string orderTypeId, string[] notOrderTypeIds = null)
        {
            Int32 affectedRows = 0;
            String tableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";
            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                try
                {
                    #region Prepare Data Table
                    DataTable dataTable = new("");
                    dataTable.Columns.Add(new DataColumn("TenantId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("DomainId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("MarketplaceId", typeof(string)));
                    dataTable.Columns.Add(new DataColumn("MarketplaceOrderNumber", typeof(string)));

                    var dataColumn = new DataColumn("TrackingCode", typeof(string));
                    dataColumn.AllowDBNull = true;

                    dataTable.Columns.Add(dataColumn);

                    foreach (var theOrder in orders)
                    {
                        if (theOrder.TenantId <= 0 ||
                            theOrder.DomainId <= 0 ||
                            theOrder.CompanyId <= 0 ||
                            string.IsNullOrEmpty(theOrder.MarketplaceId) ||
                            string.IsNullOrEmpty(theOrder.MarketplaceOrderNumber))
                            continue;

                        var row = dataTable.NewRow();
                        row["TenantId"] = Convert.ToInt32(theOrder.TenantId);
                        row["DomainId"] = Convert.ToDecimal(theOrder.DomainId);
                        row["CompanyId"] = Convert.ToDecimal(theOrder.CompanyId);
                        row["MarketplaceId"] = theOrder.MarketplaceId;
                        row["MarketplaceOrderNumber"] = theOrder.MarketplaceOrderNumber;
                        row["TrackingCode"] = theOrder.OrderShipments?.Count > 0 ?
                                              theOrder.OrderShipments[0].TrackingCode :
                                              DBNull.Value;

                        dataTable.Rows.Add(row);
                    }
                    #endregion

                    using var command = new SqlCommand("", sqlConnection);
                    sqlConnection.Open();

                    //Creating temp table on database
                    command.CommandText = @$"
Create Table {tableName} (
    TenantId Int Not Null,
    DomainId Int Not Null,
    CompanyId Int Not Null,
    MarketplaceId NVarChar(10) Not Null,
    MarketplaceOrderNumber NVarChar(100) Not Null,
    TrackingCode NVarChar(100)
)";
                    var i = command.ExecuteNonQuery();

                    //Bulk insert into temp table
                    using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = tableName;
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    // Updating destination table, and dropping temp table
                    command.CommandTimeout = 300;
                    command.CommandText = @$"
                    Update      O 
                    Set         O.OrderTypeId = '{orderTypeId}'
                    From        Orders As O
                    Inner Join  {tableName} As Temp 
                    On          O.TenantId = Temp.TenantId
                                And O.DomainId = Temp.DomainId         
                                And O.CompanyId = Temp.CompanyId
                                And O.MarketplaceId = Temp.MarketplaceId
                                And O.MarketplaceOrderNumber = Temp.MarketplaceOrderNumber
                    Inner Join  OrderShipments As OS
                    On          OS.OrderId = O.Id
                                And OS.TenantId = Temp.TenantId
                                And OS.DomainId = Temp.DomainId         
                                And OS.CompanyId = Temp.CompanyId
                                And (OS.TrackingCode = Temp.TrackingCode Or Temp.TrackingCode Is Null)
                    {(notOrderTypeIds != null ? $"Where       O.OrderTypeId Not In ({string.Join(",", notOrderTypeIds.Select(x => $"'{x}'"))})" : "")}

                    Drop Table  {tableName};";
                    affectedRows = command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            return affectedRows;
        }

        public int CanceledBulkCompare(List<Order> orders)
        {
            Int32 affectedRows = 0;
            String tableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {
                try
                {
                    #region Prepare Data Table
                    DataTable dataTable = new("");
                    dataTable.Columns.Add(new DataColumn("TenantId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("DomainId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("CompanyId", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("MarketplaceId", typeof(string)));
                    dataTable.Columns.Add(new DataColumn("MarketplaceOrderNumber", typeof(string)));

                    var dataColumn = new DataColumn("TrackingCode", typeof(string));
                    dataColumn.AllowDBNull = true;

                    dataTable.Columns.Add(dataColumn);

                    foreach (var theOrder in orders)
                    {
                        if (theOrder.TenantId <= 0 ||
                            theOrder.DomainId <= 0 ||
                            theOrder.CompanyId <= 0 ||
                            string.IsNullOrEmpty(theOrder.MarketplaceId) ||
                            string.IsNullOrEmpty(theOrder.MarketplaceOrderNumber))
                            continue;

                        var row = dataTable.NewRow();
                        row["TenantId"] = Convert.ToInt32(theOrder.TenantId);
                        row["DomainId"] = Convert.ToDecimal(theOrder.DomainId);
                        row["CompanyId"] = Convert.ToDecimal(theOrder.CompanyId);
                        row["MarketplaceId"] = theOrder.MarketplaceId;
                        row["MarketplaceOrderNumber"] = theOrder.MarketplaceOrderNumber;
                        row["TrackingCode"] = theOrder.OrderShipments?.Count > 0 ?
                                              theOrder.OrderShipments[0].TrackingCode :
                                              DBNull.Value;

                        dataTable.Rows.Add(row);
                    }
                    #endregion

                    using var command = new SqlCommand("", sqlConnection);
                    sqlConnection.Open();

                    //Creating temp table on database
                    command.CommandText = @$"
Create Table {tableName} (
    TenantId Int Not Null,
    DomainId Int Not Null,
    CompanyId Int Not Null,
    MarketplaceId NVarChar(10) Not Null,
    MarketplaceOrderNumber NVarChar(100) Not Null,
    TrackingCode NVarChar(100)
)";
                    var i = command.ExecuteNonQuery();

                    //Bulk insert into temp table
                    using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = tableName;
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    // Updating destination table, and dropping temp table
                    command.CommandTimeout = 300;
                    command.CommandText = @$"
                    Select      O.Id
                    Into        #TemporaryCancelled
                    From        Orders As O
                    Inner Join  {tableName} As Temp 
                    On          O.TenantId = Temp.TenantId
                                And O.DomainId = Temp.DomainId         
                                And O.CompanyId = Temp.CompanyId
                                And O.MarketplaceId = Temp.MarketplaceId
                                And O.MarketplaceOrderNumber = Temp.MarketplaceOrderNumber
                    Inner Join  OrderShipments As OS
                    On          OS.OrderId = O.Id
                                And OS.TenantId = Temp.TenantId
                                And OS.DomainId = Temp.DomainId         
                                And OS.CompanyId = Temp.CompanyId
                                And (OS.TrackingCode = Temp.TrackingCode Or Temp.TrackingCode Is Null)
                                And O.OrderTypeId <> 'IP'
                                And O.OrderTypeId <> 'IA'

                    Update      O
                    Set         O.OrderTypeId = 'IP'
                    From        Orders As O
                    Join        #TemporaryCancelled As T
                    On          O.Id = T.Id

                    Update	[PI]
                    Set     [PI].Stock = [PI].Stock + OD.Quantity
                    From	ProductInformations As [PI]
                    Join	OrderDetails As OD
                    On		[PI].Id = OD.ProductInformationId
                    Join	Orders As O
                    On		OD.OrderId = O.Id
                    Join    #TemporaryCancelled As T
                    On      O.Id = T.Id

                    Update	PIM
                    Set     PIM.DirtyStock = 1
                    From	ProductInformationMarketplaces As PIM
                    Join	ProductInformations As [PI]
                    On		[PI].Id = [PIM].ProductInformationId
                    Join	OrderDetails As OD
                    On		[PI].Id = OD.ProductInformationId
                    Join	Orders As O
                    On		OD.OrderId = O.Id
                    Join    #TemporaryCancelled As T
                    On      O.Id = T.Id

                    Drop Table  #TemporaryCancelled
                    Drop Table  {tableName}";
                    affectedRows = command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            return affectedRows;
        }
        #endregion
    }
}