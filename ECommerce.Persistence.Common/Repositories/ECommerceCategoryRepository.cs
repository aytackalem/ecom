using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities.Companyable;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
	public partial class ECommerceCategoryRepository : RepositoryBase<ECommerceCategory, long>, IECommerceCategoryRepository
    {
		#region Constructor
		public ECommerceCategoryRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public ECommerceCategory Read(long id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
        #endregion
    }
}