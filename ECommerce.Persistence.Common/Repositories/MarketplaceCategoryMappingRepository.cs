using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class MarketplaceCategoryMappingRepository : RepositoryBase<MarketplaceCategoryMapping, int>, IMarketplaceCategoryMappingRepository
	{
		#region Constructor
		public MarketplaceCategoryMappingRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public MarketplaceCategoryMapping Read(int id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}