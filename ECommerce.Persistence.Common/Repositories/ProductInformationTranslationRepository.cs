using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ProductInformationTranslationRepository : RepositoryBase<Domain.Entities.ProductInformationTranslation, int>, IProductInformationTranslationRepository
	{
		#region Constructor
		public ProductInformationTranslationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public Domain.Entities.ProductInformationTranslation Read(int id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}