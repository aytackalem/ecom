using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System; using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ShoppingCartDiscountedProductSettingRepository : RepositoryBase<ShoppingCartDiscountedProductSetting, Int32>, IShoppingCartDiscountedProductSettingRepository
	{
		#region Constructor
		public ShoppingCartDiscountedProductSettingRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public ShoppingCartDiscountedProductSetting Read(Int32 id)
		{
			return base.DbSet().FirstOrDefault(e => e.Id.Equals(id));
		}
		#endregion
	}
}