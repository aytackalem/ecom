using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class WholesaleOrderRepository : RepositoryBase<WholesaleOrder, int>, IWholesaleOrderRepository
    {
        #region Constructor
        public WholesaleOrderRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public WholesaleOrder Read(int id)
        {
            return base.DbSet().FirstOrDefault(v => v.Id == id);
        }
        #endregion
    }
}