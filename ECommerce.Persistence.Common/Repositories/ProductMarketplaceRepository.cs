using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using System;
using ECommerce.Application.Common.Interfaces.Helpers;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Dapper;

namespace ECommerce.Repository
{
    public partial class ProductMarketplaceRepository : RepositoryBase<ProductMarketplace, Int32>, IProductMarketplaceRepository
    {
        #region Constructor
        public ProductMarketplaceRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public ProductMarketplace Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}