using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class TenantRepository : RepositoryBase<Tenant, Int32>, ITenantRepository
    {
        #region Constructor
        public TenantRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Tenant Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}