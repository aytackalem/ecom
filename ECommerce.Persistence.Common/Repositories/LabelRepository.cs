using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class LabelRepository : RepositoryBase<Label, Int32>, ILabelRepository
	{
		#region Constructor
		public LabelRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public Label Read(Int32 id)
		{
			return base.DbSet().Include(c => c.LabelTranslations).FirstOrDefault(c => c.Id == id);
		}
		#endregion
	}
}