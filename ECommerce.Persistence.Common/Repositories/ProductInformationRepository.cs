using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;
using EFCore.BulkExtensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Data;

namespace ECommerce.Repository
{
    public partial class ProductInformationRepository : RepositoryBase<ProductInformation, Int32>, IProductInformationRepository
    {
        #region Constructors
        public ProductInformationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        public ProductInformation Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public void BulkInsertMapping(List<ProductInformation> productInformations)
        {
            if (productInformations.Count > 0)
            {
                using (var transaction = _dbContext.Database.BeginTransaction())
                {


                    _dbContext.BulkInsert(productInformations, new BulkConfig { SetOutputIdentity = true });

                    var productInformationVariants = new List<ProductInformationVariant>();
                    var productInformationPrices = new List<ProductInformationPrice>();
                    var productInformationTranslations = new List<ProductInformationTranslation>();
                    var productInformationPhotos = new List<ProductInformationPhoto>();
                    var productInformationMarketplaces = new List<ProductInformationMarketplace>();

                    foreach (var productInformation in productInformations)
                    {
                        foreach (var productInformationVariant in productInformation.ProductInformationVariants)
                        {
                            productInformationVariant.ProductInformationId = productInformation.Id;
                        }
                        productInformationVariants.AddRange(productInformation.ProductInformationVariants);

                        foreach (var productInformationPrice in productInformation.ProductInformationPriceses)
                        {
                            productInformationPrice.ProductInformationId = productInformation.Id;
                        }
                        productInformationPrices.AddRange(productInformation.ProductInformationPriceses);


                        foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                        {
                            productInformationTranslation.ProductInformationId = productInformation.Id;
                        }
                        productInformationTranslations.AddRange(productInformation.ProductInformationTranslations);


                        foreach (var productInformationTranslation in productInformation.ProductInformationPhotos)
                        {
                            productInformationTranslation.ProductInformationId = productInformation.Id;
                        }
                        productInformationPhotos.AddRange(productInformation.ProductInformationPhotos);


                    }

                    _dbContext.BulkInsert(productInformationVariants, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationPrices, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationTranslations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationPhotos, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationMarketplaces, new BulkConfig { SetOutputIdentity = true });

                    var productInformationSeoTranslations = new List<ProductInformationSeoTranslation>();
                    var productInformationContentTranslations = new List<ProductInformationContentTranslation>();
                    var productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();

                    foreach (var pm in productInformationMarketplaces)
                    {
                        foreach (var productInformationMarketplaceVariantValue in pm.ProductInformationMarketplaceVariantValues)
                        {
                            productInformationMarketplaceVariantValue.ProductInformationMarketplaceId = pm.Id;
                        }
                        productInformationMarketplaceVariantValues.AddRange(pm.ProductInformationMarketplaceVariantValues);
                    }
                    _dbContext.BulkInsert(productInformationMarketplaceVariantValues, new BulkConfig { SetOutputIdentity = true });

                    foreach (var pt in productInformationTranslations)
                    {
                        pt.ProductInformationSeoTranslation.Id = pt.Id;
                        pt.ProductInformationContentTranslation.Id = pt.Id;

                        productInformationSeoTranslations.Add(pt.ProductInformationSeoTranslation);
                        productInformationContentTranslations.Add(pt.ProductInformationContentTranslation);
                    }
                    _dbContext.BulkInsert(productInformationSeoTranslations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationContentTranslations, new BulkConfig { SetOutputIdentity = true });

                    transaction.Commit();
                }
            }
        }
        #endregion
    }
}