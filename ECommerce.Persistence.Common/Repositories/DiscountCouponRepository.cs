using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class DiscountCouponRepository : RepositoryBase<DiscountCoupon, Int32>, IDiscountCouponRepository
    {
        #region Constructor
        public DiscountCouponRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public DiscountCoupon Read(Int32 id)
        {
            return base.DbSet().Include(c => c.DisountCouponSetting).FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}