using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class MarketPlaceCompanyRepository : RepositoryBase<MarketplaceCompany, int>, IMarketPlaceCompanyRepository
	{
		#region Constructor
		public MarketPlaceCompanyRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public MarketplaceCompany Read(int id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}