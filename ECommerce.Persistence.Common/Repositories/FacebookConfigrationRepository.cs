using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class FacebookConfigrationRepository : RepositoryBase<FacebookConfigration, Int32>, IFacebookConfigrationRepository
    {
        #region Constructor
        public FacebookConfigrationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public FacebookConfigration Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}