using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System; using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;
using System.Collections.Generic;
using EFCore.BulkExtensions;

namespace ECommerce.Repository
{
    public partial class VariantValueRepository : RepositoryBase<VariantValue, Int32>, IVariantValueRepository
    {
        #region Constructor
        public VariantValueRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public VariantValue Read(Int32 id)
        {
            return base.DbSet().Include(vv => vv.VariantValueTranslations).FirstOrDefault(vv => vv.Id == id);
        }

        public void BulkInsertMapping(List<VariantValue> variantValues)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var VariantValueCopy = new List<VariantValue>();
                VariantValueCopy.AddRange(variantValues);

                _dbContext.BulkInsert<VariantValue>(VariantValueCopy, new BulkConfig { SetOutputIdentity = true });

                int variantValueIndex = VariantValueCopy[0].Id;
                variantValues.ForEach(x =>
                {
                    x.Id = variantValueIndex;
                    variantValueIndex++;
                });

                var variantValueTranslations = new List<VariantValueTranslation>();

                foreach (var vLoop in variantValues)
                {

                    foreach (var pmLoop in vLoop.VariantValueTranslations)
                    {
                        pmLoop.VariantValueId = vLoop.Id;
                    }
                    variantValueTranslations.AddRange(vLoop.VariantValueTranslations);

                }

                _dbContext.BulkInsert(variantValueTranslations, new BulkConfig { SetOutputIdentity = true });

                transaction.Commit();
            }
        }
        #endregion
    }
}