using System;
using System.Linq;
using System.Collections.Generic;
using ECommerce.Repository.Base;
using ECommerce.Persistence.Context;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Repository
{
    public partial class FeedRepository : RepositoryBase<Feed, Int32>, IFeedRepository
    {
        #region Constructor
        public FeedRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Feed Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}