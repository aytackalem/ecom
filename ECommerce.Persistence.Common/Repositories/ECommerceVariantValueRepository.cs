using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities.Companyable;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System.Linq;

namespace ECommerce.Repository
{
	public partial class ECommerceVariantValueRepository : RepositoryBase<ECommerceVariantValue, long>, IECommerceVariantValueRepository
    {
		#region Constructor
		public ECommerceVariantValueRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public ECommerceVariantValue Read(long id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
        #endregion
    }
}