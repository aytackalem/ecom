using System; using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;
using System.Collections.Generic;
using ECommerce.Repository.Base;
using ECommerce.Persistence.Context;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories;

namespace ECommerce.Repository
{
	public partial class PaymentTypeTranslationRepository : RepositoryBase<PaymentTypeTranslation, Int16>, IPaymentTypeTranslationRepository
	{
		#region Constructor
		public PaymentTypeTranslationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public PaymentTypeTranslation Read(Int16 id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}