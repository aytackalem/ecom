using System; using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;
using System.Collections.Generic;
using ECommerce.Repository.Base;
using ECommerce.Persistence.Context;
using ECommerce.Domain.Entities;
using ECommerce.Application.Common.Interfaces.Repositories;

namespace ECommerce.Repository
{
	public partial class OrderSmsRepository : RepositoryBase<OrderSms, Int64>, IOrderSmsRepository
	{
		#region Constructor
		public OrderSmsRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public OrderSms Read(Int64 id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}