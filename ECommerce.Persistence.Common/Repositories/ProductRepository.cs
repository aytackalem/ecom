using Dapper;
using ECommerce.Application.Common.DataTransferObjects.MarketplacesV2;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Repository
{
    public partial class ProductRepository : RepositoryBase<Product, Int32>, IProductRepository
    {
        #region Constructors
        public ProductRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Methods
        #region Dirty Product
        public async Task<Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>> ReadDirtyProductsAsync(int queueId, string marketplaceId)
        {
            var tuple = new Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>(
                new List<DirtyProduct>(),
                new List<DirtyProductPhoto>(),
                new List<DirtyProductMarketplaceVariantValue>());

            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
With T As (
	Select	PIM.Id As ProductInformationMarketplaceId
			,PIM.ProductInformationId
			,PIM.Opened
			,PIM.AccountingPrice
			,PIM.Barcode
			,PIM.UUId As ProductInformationMarketplaceUUId
			,PIM.StockCode
			,PIM.UnitPrice
			,PIM.ListUnitPrice
			,PIM.TenantId
			,PIM.DomainId
			,PIM.CompanyId
			,PIM.MarketplaceId	
	From	ProductInformationMarketplaces As PIM	
	Join    Tenants As T
	On      PIM.TenantId = T.Id
			And T.QueueId = @QueueId
	Join	MarketplaceCompanies As MC
	On		MC.MarketplaceId = @MarketplaceId
			And MC.TenantId = PIM.TenantId
            And MC.DomainId = PIM.DomainId
			And MC.CompanyId = PIM.CompanyId
			And MC.CreateProduct = 1
	Where	PIM.DirtyProductInformation = 1
			And PIM.MarketplaceId = @MarketplaceId
			And PIM.Barcode Is Not Null
			And PIM.Barcode <> ''
			And PIM.Active = 1
			And PIM.ListUnitPrice > 0
			And PIM.UnitPrice > 0
			And PIM.TenantId = T.Id
            And PIM.DisabledUpdateProduct = 0
), T2 As (
	Select		T.*,
				[PI].ProductId,
				[PI].Stock,
                [PI].SkuCode,
				IsNull(PT.[Name], '') As ProductName
	From		T
	Join		ProductInformations As [PI] With(NoLock)
	On			T.ProductInformationId = [PI].Id
	Join		Products As P With(NoLock)
	On			[PI].ProductId = P.Id
                And P.IsOnlyHidden = 0
	Left Join	ProductTranslations As PT With(NoLock)
	On			PT.ProductId = P.Id
				And PT.LanguageId = 'TR'
), T3 As (
	Select	T2.ProductInformationMarketplaceId,
			T2.ProductInformationId,
			T2.Barcode,
			T2.ProductInformationMarketplaceUUId,
			T2.StockCode,
			T2.UnitPrice,
			T2.ListUnitPrice,
			T2.MarketplaceId,
			T2.ProductId,
			T2.Stock,
            T2.ProductName,
            T2.TenantId,
            T2.DomainId,
            T2.CompanyId,
            T2.Opened,
            T2.SkuCode,
			PIP.VatRate,
			PIT.[Name] As ProductInformationName,
			PIT.SubTitle,
			PICT.Content,
            ISNULL(PIT.VariantValuesDescription,'') AS VariantValuesDescription

	From	T2
	Join	ProductInformationTranslations As PIT With(NoLock)
	On		T2.ProductInformationId = PIT.ProductInformationId
			And PIT.LanguageId = 'TR'
	Join	ProductInformationPriceses As PIP With(NoLock)
	On		T2.ProductInformationId = PIP.ProductInformationId
			And PIP.CurrencyId = 'TL'
	Join	ProductInformationContentTranslations As PICT With(NoLock)
	On		PICT.Id = PIT.Id
)
Select	T3.*
		,PM.MarketplaceBrandCode
		,PM.MarketplaceBrandName
		,PM.MarketplaceCategoryCode
		,PM.MarketplaceCategoryName
        ,PM.SellerCode
        ,PM.UUId As ProductMarketplaceUUId
        ,PM.DeliveryDay
Into	#T
From	T3
Join	ProductMarketplaces As PM With(NoLock)
On		T3.ProductId = PM.ProductId
		And PM.MarketplaceId = @MarketplaceId
		And PM.TenantId = T3.TenantId
		And PM.DomainId = T3.DomainId
		And PM.CompanyId = T3.CompanyId

Select  *
From    #T

Select		DS.ImageUrl + '/product/' + PIP.[FileName] As [FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder
From		ProductInformationPhotos PIP With(NoLock)
Join		DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId
Where		PIP.Deleted = 0
			And PIP.ProductInformationId In (
				Select	ProductInformationId
				From	#T
			)
Group By	PIP.[FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder,
			DS.ImageUrl,
            PIP.Id
Order By	PIP.ProductInformationId,
			PIP.DisplayOrder,
            PIP.Id

Select	*
From	ProductInformationMarketplaceVariantValues As PIMVV
Where	PIMVV.ProductInformationMarketplaceId In (
			Select	ProductInformationMarketplaceId
			From	#T
		)

Drop Table	#T";
#warning Remove

                try
                {
                    var gridReader = await sqlConnection.QueryMultipleAsync(query, new
                    {
                        QueueId = queueId,
                        MarketplaceId = marketplaceId
                    });
                    tuple.Item1.AddRange(gridReader.Read<DirtyProduct>());
                    tuple.Item2.AddRange(gridReader.Read<DirtyProductPhoto>());
                    tuple.Item3.AddRange(gridReader.Read<DirtyProductMarketplaceVariantValue>());
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }

            return tuple;
        }

        public async Task<Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>> ReadDirtyProductsAsync(int queueId, bool opened, string marketplaceId)
        {
            var tuple = new Tuple<List<DirtyProduct>, List<DirtyProductPhoto>, List<DirtyProductMarketplaceVariantValue>>(
                new List<DirtyProduct>(),
                new List<DirtyProductPhoto>(),
                new List<DirtyProductMarketplaceVariantValue>());

            using (var sqlConnection = new SqlConnection(this._dbContext.Database.GetConnectionString()))
            {
                var query = @"
With T As (
	Select	PIM.Id As ProductInformationMarketplaceId
			,PIM.ProductInformationId
			,PIM.Opened
			,PIM.AccountingPrice
			,PIM.Barcode
			,PIM.UUId As ProductInformationMarketplaceUUId
			,PIM.StockCode
			,PIM.UnitPrice
			,PIM.ListUnitPrice
			,PIM.TenantId
			,PIM.DomainId
			,PIM.CompanyId
			,PIM.MarketplaceId
	From	ProductInformationMarketplaces As PIM	
	Join    Tenants As T
	On      PIM.TenantId = T.Id
			And T.QueueId = @QueueId
	Join	MarketplaceCompanies As MC
	On		MC.MarketplaceId = @MarketplaceId
			And MC.TenantId = PIM.TenantId
            And MC.DomainId = PIM.DomainId
			And MC.CompanyId = PIM.CompanyId
			And MC.CreateProduct = 1
	Where	PIM.DirtyProductInformation = 1
			And PIM.MarketplaceId = @MarketplaceId
			And PIM.Barcode Is Not Null
			And PIM.Barcode <> ''
			And PIM.Active = 1
			And PIM.ListUnitPrice > 0
			And PIM.UnitPrice > 0
            And PIM.Opened = @Opened
			And PIM.TenantId = T.Id
            And PIM.DisabledUpdateProduct = 0
), T2 As (
	Select		T.*,
				[PI].ProductId,
				[PI].Stock,
                [PI].SkuCode,
				IsNull(PT.[Name], '') As ProductName
	From		T
	Join		ProductInformations As [PI] With(NoLock)
	On			T.ProductInformationId = [PI].Id
	Join		Products As P With(NoLock)
	On			[PI].ProductId = P.Id
                And P.IsOnlyHidden = 0
	Left Join	ProductTranslations As PT With(NoLock)
	On			PT.ProductId = P.Id
				And PT.LanguageId = 'TR'
), T3 As (
	Select	T2.ProductInformationMarketplaceId,
			T2.ProductInformationId,
			T2.Barcode,
			T2.ProductInformationMarketplaceUUId,
			T2.StockCode,
			T2.UnitPrice,
			T2.ListUnitPrice,
			T2.MarketplaceId,
			T2.ProductId,
			T2.Stock,
            T2.ProductName,
            T2.TenantId,
            T2.DomainId,
            T2.CompanyId,
            T2.Opened,
            T2.SkuCode,
			PIP.VatRate,
			PIT.[Name] As ProductInformationName,
			PIT.SubTitle,
			PICT.Content,
            ISNULL(PIT.VariantValuesDescription,'') AS VariantValuesDescription

	From	T2
	Join	ProductInformationTranslations As PIT With(NoLock)
	On		T2.ProductInformationId = PIT.ProductInformationId
			And PIT.LanguageId = 'TR'
	Join	ProductInformationPriceses As PIP With(NoLock)
	On		T2.ProductInformationId = PIP.ProductInformationId
			And PIP.CurrencyId = 'TL'
	Join	ProductInformationContentTranslations As PICT With(NoLock)
	On		PICT.Id = PIT.Id
)
Select	T3.*
		,PM.MarketplaceBrandCode
		,PM.MarketplaceBrandName
		,PM.MarketplaceCategoryCode
		,PM.MarketplaceCategoryName
        ,PM.SellerCode
        ,PM.UUId As ProductMarketplaceUUId
        ,PM.DeliveryDay
Into	#T
From	T3
Join	ProductMarketplaces As PM With(NoLock)
On		T3.ProductId = PM.ProductId
		And PM.MarketplaceId = @MarketplaceId
		And PM.TenantId = T3.TenantId        
		And PM.DomainId = T3.DomainId
		And PM.CompanyId = T3.CompanyId

Select  *
From    #T

Select		DS.ImageUrl + '/product/' + PIP.[FileName] As [FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder
From		ProductInformationPhotos PIP With(NoLock)
Join		DomainSettings As DS With(NoLock)
On			DS.Id = PIP.DomainId
Where		PIP.Deleted = 0
			And PIP.ProductInformationId In (
				Select	ProductInformationId
				From	#T
			)
Group By	PIP.[FileName],
			PIP.ProductInformationId,
			PIP.DisplayOrder,
			DS.ImageUrl,
            PIP.Id
Order By	PIP.ProductInformationId,
			PIP.DisplayOrder,
            PIP.Id

Select	*
From	ProductInformationMarketplaceVariantValues As PIMVV
Where	PIMVV.ProductInformationMarketplaceId In (
			Select	ProductInformationMarketplaceId
			From	#T
		)

Drop Table	#T";
#warning Remove

                try
                {
                    var gridReader = await sqlConnection.QueryMultipleAsync(query, new
                    {
                        QueueId = queueId,
                        Opened = opened,
                        MarketplaceId = marketplaceId
                    }, null, 600);
                    tuple.Item1.AddRange(gridReader.Read<DirtyProduct>());
                    tuple.Item2.AddRange(gridReader.Read<DirtyProductPhoto>());
                    tuple.Item3.AddRange(gridReader.Read<DirtyProductMarketplaceVariantValue>());
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }

            return tuple;
        }

        public bool UpdateDirtyProduct(List<int> productInformationMarketplaceIds, bool dirtyProductInformation)
        {
            bool success = false;

            using (var sqlConnection = new SqlConnection(base._dbContext.Database.GetConnectionString()))
            {

                var temporaryTableName = $"_{Guid.NewGuid().ToString().Replace("-", "")}";

                sqlConnection.Open();

                /* Create temporary table */
                using (var sqlCommand = new SqlCommand(@$"
            Create Table {temporaryTableName} (
                ProductInformationMarketplaceId   Int   Not Null,
            )", sqlConnection))
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                }

                /* Prepare bulk data. */
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));


                foreach (var id in productInformationMarketplaceIds)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["ProductInformationMarketplaceId"] = id;

                    dataTable.Rows.Add(dataRow);
                }

                /* Bulk insert to temporary table. */
                using (var bulkcopy = new SqlBulkCopy(sqlConnection))
                {
                    bulkcopy.BulkCopyTimeout = 660;
                    bulkcopy.DestinationTableName = temporaryTableName;
                    try
                    {
                        bulkcopy.WriteToServer(dataTable);
                    }
                    catch (Exception)
                    {
                        sqlConnection.Close();
                        throw;
                    }
                    bulkcopy.Close();
                }

                /* Read data */
                try
                {
                    using (var sqlCommand = new SqlCommand(@$"
            Update  ProductInformationMarketplaces
            Set     DirtyProductInformation = @DirtyProductInformation
            From    ProductInformationMarketplaces As PIM
            Join	{temporaryTableName} As T  On T.ProductInformationMarketplaceId = PIM.Id
            
            
            Drop table {temporaryTableName}", sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DirtyProductInformation", dirtyProductInformation);

                        var affectedRowsCount = sqlCommand.ExecuteNonQuery();

                        success = affectedRowsCount == productInformationMarketplaceIds.Count;
                    }
                }
                catch (Exception)
                {
                    sqlConnection.Close();
                    throw;
                }

                if (sqlConnection.State != ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return success;
        }

        //TO DO: UpdateDirtyProduct Id

        //TO DO: UpdateDirtyProduct List<Id>

        //TO DO: UpdateDirtyListStockByBarcode
        #endregion

        public Product Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public void BulkInsertMapping(List<Product> products)
        {
            if (products.Count > 0)
            {
                using (var transaction = _dbContext.Database.BeginTransaction())
                {
                    var productsCopy = new List<Product>();
                    productsCopy.AddRange(products);

                    _dbContext.BulkInsert<Product>(productsCopy, new BulkConfig { SetOutputIdentity = true });

                    int productIndex = productsCopy[0].Id;
                    products.ForEach(x =>
                    {
                        x.Id = productIndex;
                        productIndex++;
                    });

                    var productInformations = new List<ProductInformation>();
                    var categoryProducts = new List<CategoryProduct>();
                    var productMarketplaces = new List<ProductMarketplace>();
                    var productTranslations = new List<ProductTranslation>();

                    foreach (var product in products)
                    {

                        foreach (var pmLoop in product.ProductTranslations)
                        {
                            pmLoop.ProductId = product.Id;
                        }
                        productTranslations.AddRange(product.ProductTranslations);


                        foreach (var productInformation in product.ProductInformations)
                        {
                            productInformation.ProductId = product.Id;
                        }
                        productInformations.AddRange(product.ProductInformations);

                        foreach (var categoryProduct in product.CategoryProducts)
                        {
                            categoryProduct.ProductId = product.Id;
                        }
                        categoryProducts.AddRange(product.CategoryProducts);


                        if (product.ProductMarketplaces != null)
                        {

                            foreach (var pm in product.ProductMarketplaces)
                            {
                                pm.ProductId = product.Id;
                            }
                            productMarketplaces.AddRange(product.ProductMarketplaces);
                        }

                    }

                    _dbContext.BulkInsert(productInformations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(categoryProducts, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productTranslations, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productMarketplaces, new BulkConfig { SetOutputIdentity = true });


                    var productInformationVariants = new List<ProductInformationVariant>();
                    var productInformationPrices = new List<ProductInformationPrice>();
                    var productInformationTranslations = new List<ProductInformationTranslation>();
                    var productInformationPhotos = new List<ProductInformationPhoto>();
                    var productInformationMarketplaces = new List<ProductInformationMarketplace>();

                    foreach (var productInformation in productInformations)
                    {
                        if (productInformation.ProductInformationVariants != null)
                        {

                            foreach (var productInformationVariant in productInformation.ProductInformationVariants)
                            {
                                productInformationVariant.VariantValue = null;
                                productInformationVariant.ProductInformationId = productInformation.Id;
                            }
                            productInformationVariants.AddRange(productInformation.ProductInformationVariants);
                        }

                        foreach (var productInformationPrice in productInformation.ProductInformationPriceses)
                        {
                            productInformationPrice.ProductInformationId = productInformation.Id;
                        }
                        productInformationPrices.AddRange(productInformation.ProductInformationPriceses);


                        foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                        {
                            productInformationTranslation.Url = $"{productInformationTranslation.Name.GenerateUrl()}-{productInformation.Id}-p";
                            productInformationTranslation.ProductInformationId = productInformation.Id;
                        }
                        productInformationTranslations.AddRange(productInformation.ProductInformationTranslations);


                        foreach (var productInformationTranslation in productInformation.ProductInformationPhotos)
                        {
                            productInformationTranslation.ProductInformationId = productInformation.Id;
                        }
                        productInformationPhotos.AddRange(productInformation.ProductInformationPhotos);

                        if (productInformation.ProductInformationMarketplaces != null)
                        {

                            foreach (var productInformationMarketplace in productInformation.ProductInformationMarketplaces)
                            {
                                productInformationMarketplace.ProductInformationId = productInformation.Id;
                            }
                            productInformationMarketplaces.AddRange(productInformation.ProductInformationMarketplaces);
                        }
                    }

                    var page = (productInformationTranslations.Count / 500) + 1;
                    for (int i = 0; i < page; i++)
                    {
                        var _productInformationTranslations = productInformationTranslations.Skip(i * 500).Take(500).ToList();
                        _dbContext.BulkInsert(_productInformationTranslations, new BulkConfig { SetOutputIdentity = true });
                    }




                    _dbContext.BulkInsert(productInformationVariants, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationPrices, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationPhotos, new BulkConfig { SetOutputIdentity = true });
                    _dbContext.BulkInsert(productInformationMarketplaces, new BulkConfig { SetOutputIdentity = true });

                    var productInformationSeoTranslations = new List<ProductInformationSeoTranslation>();
                    var productInformationContentTranslations = new List<ProductInformationContentTranslation>();
                    var productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();

                    foreach (var pm in productInformationMarketplaces)
                    {
                        if (pm.ProductInformationMarketplaceVariantValues != null)
                        {

                            foreach (var productInformationMarketplaceVariantValue in pm.ProductInformationMarketplaceVariantValues)
                            {
                                productInformationMarketplaceVariantValue.ProductInformationMarketplaceId = pm.Id;
                            }
                            productInformationMarketplaceVariantValues.AddRange(pm.ProductInformationMarketplaceVariantValues);
                        }
                    }
                    _dbContext.BulkInsert(productInformationMarketplaceVariantValues, new BulkConfig { SetOutputIdentity = true });

                    foreach (var pt in productInformationTranslations)
                    {
                        pt.ProductInformationSeoTranslation.Id = pt.Id;
                        pt.ProductInformationContentTranslation.Id = pt.Id;

                        productInformationSeoTranslations.Add(pt.ProductInformationSeoTranslation);
                        productInformationContentTranslations.Add(pt.ProductInformationContentTranslation);
                    }
                    _dbContext.BulkInsert(productInformationSeoTranslations, new BulkConfig { SetOutputIdentity = true });

                    var productInformationContenPage = (productInformationContentTranslations.Count / 1000) + 1;
                    for (int i = 0; i < productInformationContenPage; i++)
                    {
                        var _productInformationContentTranslations = productInformationContentTranslations.Skip(i * 1000).Take(1000).ToList();
                        _dbContext.BulkInsert(_productInformationContentTranslations, new BulkConfig { SetOutputIdentity = true });
                    }


                    transaction.Commit();
                }
            }
        }
        #endregion
    }
}