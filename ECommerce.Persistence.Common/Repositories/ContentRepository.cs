using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ContentRepository : RepositoryBase<Content, Int32>, IContentRepository
    {
        #region Constructor
        public ContentRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Content Read(Int32 id)
        {
            return base
                .DbSet()
                .Include(c => c.ContentTranslations)
                .ThenInclude(x => x.ContentTranslationContent)
                .Include(c => c.ContentTranslations)
                .ThenInclude(x => x.ContentSeoTranslation)
                .FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}