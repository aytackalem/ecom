using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Domain.Entities.Base;
using ECommerce.Persistence.Context;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Repository.Base
{
    public abstract class RepositoryBase<TEntity, TId> where TEntity : EntityBase<TId>
    {
        #region Fields
        protected DbContext _dbContext;

        private DbSet<TEntity> _dbSet;

        protected readonly ITenantFinder _tenantFinder;

        protected readonly IDomainFinder _domainFinder;

        protected readonly ICompanyFinder _companyFinder;

        #endregion

        #region Constructors
        public RepositoryBase(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder)
        {
            #region Fields
            this._dbContext = dbContext;
            this._dbSet = _dbContext.Set<TEntity>();
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public Boolean Create(TEntity entity)
        {
            _dbSet.Add(entity);

            var entries = _dbContext.ChangeTracker.Entries();
            foreach (var eLoop in entries)
            {
                if (eLoop.State == EntityState.Added || eLoop.State == EntityState.Modified)
                {
                    #region Check Interfaces
                    var entityType = eLoop.Entity.GetType();


                    if (entityType.IsAssignableTo(typeof(ITenantable)))
                    {
                        var tenantId = this._tenantFinder.FindId();
                        ((ITenantable)eLoop.Entity).TenantId = tenantId;
                    }

                    if (entityType.IsAssignableTo(typeof(IDomainable)))
                    {
                        var domainId = this._domainFinder.FindId();
                        ((IDomainable)eLoop.Entity).DomainId = domainId;
                    }

                    if (entityType.IsAssignableTo(typeof(ICompanyable)))
                    {
                        var companyId = this._companyFinder.FindId();
                        ((ICompanyable)eLoop.Entity).CompanyId = companyId;
                    }

                    #endregion
                }
            }

            return _dbContext.SaveChanges() > 0;
        }

        public async Task<Boolean> CreateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            await _dbSet.AddAsync(entity);

            var entries = _dbContext.ChangeTracker.Entries();
            foreach (var eLoop in entries)
            {
                if (eLoop.State == EntityState.Added || eLoop.State == EntityState.Modified)
                {
                    #region Check Interfaces
                    var entityType = eLoop.Entity.GetType();

                    if (entityType.IsAssignableTo(typeof(ITenantable)))
                    {
                        var tenantId = this._tenantFinder.FindId();
                        ((ITenantable)eLoop.Entity).TenantId = tenantId;
                    }

                    if (entityType.IsAssignableTo(typeof(IDomainable)))
                    {
                        var domainId = this._domainFinder.FindId();
                        ((IDomainable)eLoop.Entity).DomainId = domainId;
                    }

                    if (entityType.IsAssignableTo(typeof(ICompanyable)))
                    {
                        var companyId = this._companyFinder.FindId();
                        ((ICompanyable)eLoop.Entity).CompanyId = companyId;
                    }

                    #endregion
                }
            }

            return await _dbContext.SaveChangesAsync(cancellationToken) > 0;
        }

        public Boolean BulkInsert(List<TEntity> entities)
        {

            #region Check Interfaces
            var entityType = typeof(TEntity);

            if (entityType.IsAssignableTo(typeof(ITenantable)) && entities.Any(e => ((ITenantable)e).TenantId <= 0))
            {
                var tenantId = this._tenantFinder.FindId();
                entities.ForEach(e => ((ITenantable)e).TenantId = tenantId);
            }

            if (entityType.IsAssignableTo(typeof(IDomainable)) && entities.Any(e => ((IDomainable)e).DomainId <= 0))
            {
                var domainId = this._domainFinder.FindId();
                entities.ForEach(e => ((IDomainable)e).DomainId = domainId);
            }

            if (entityType.IsAssignableTo(typeof(ICompanyable)) && entities.Any(e => ((ICompanyable)e).CompanyId <= 0))
            {
                var companyId = this._companyFinder.FindId();
                entities.ForEach(e => ((ICompanyable)e).CompanyId = companyId);
            }

            #endregion


            _dbContext.BulkInsert(entities);
            return true;
        }


        public Boolean Update(TEntity entity)
        {
            _dbContext.Entry<TEntity>(entity).State = EntityState.Modified;

            var entries = _dbContext.ChangeTracker.Entries();
            foreach (var eLoop in entries)
            {
                if (eLoop.State == EntityState.Added || eLoop.State == EntityState.Modified)
                {
                    #region Check Interfaces
                    var entityType = eLoop.Entity.GetType();

                    if (entityType.IsAssignableTo(typeof(ITenantable)))
                    {
                        var tenantId = this._tenantFinder.FindId();
                        ((ITenantable)eLoop.Entity).TenantId = tenantId;
                    }

                    if (entityType.IsAssignableTo(typeof(IDomainable)))
                    {
                        var domainId = this._domainFinder.FindId();
                        ((IDomainable)eLoop.Entity).DomainId = domainId;
                    }

                    if (entityType.IsAssignableTo(typeof(ICompanyable)))
                    {
                        var companyId = this._companyFinder.FindId();
                        ((ICompanyable)eLoop.Entity).CompanyId = companyId;
                    }

                    #endregion
                }
            }

            return _dbContext.SaveChanges() > 0;
        }

        public async Task<Boolean> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            _dbContext.Entry<TEntity>(entity).State = EntityState.Modified;

            var entries = _dbContext.ChangeTracker.Entries();
            foreach (var eLoop in entries)
            {
                if (eLoop.State == EntityState.Added || eLoop.State == EntityState.Modified)
                {
                    #region Check Interfaces
                    var entityType = eLoop.Entity.GetType();

                    if (entityType.IsAssignableTo(typeof(ITenantable)))
                    {
                        var tenantId = this._tenantFinder.FindId();
                        ((ITenantable)eLoop.Entity).TenantId = tenantId;
                    }

                    if (entityType.IsAssignableTo(typeof(IDomainable)))
                    {
                        var domainId = this._domainFinder.FindId();
                        ((IDomainable)eLoop.Entity).DomainId = domainId;
                    }

                    if (entityType.IsAssignableTo(typeof(ICompanyable)))
                    {
                        var companyId = this._companyFinder.FindId();
                        ((ICompanyable)eLoop.Entity).CompanyId = companyId;
                    }

                    #endregion
                }
            }

            return await _dbContext.SaveChangesAsync(cancellationToken) > 0;
        }

        public Boolean Update(List<TEntity> entity)
        {

            #region Check Interfaces
            var entityType = typeof(TEntity);

            if (entityType.IsAssignableTo(typeof(ITenantable)))
            {
                var tenantId = this._tenantFinder.FindId();
                entity.ForEach(e => ((ITenantable)e).TenantId = tenantId);
            }

            if (entityType.IsAssignableTo(typeof(IDomainable)))
            {
                var domainId = this._domainFinder.FindId();
                entity.ForEach(e => ((IDomainable)e).DomainId = domainId);
            }

            if (entityType.IsAssignableTo(typeof(ICompanyable)))
            {
                var companyId = this._companyFinder.FindId();
                entity.ForEach(e => ((ICompanyable)e).CompanyId = companyId);
            }

            #endregion


            _dbContext.BulkUpdate(entity);
            return true;
        }

        public void Reload(TEntity entity)
        {
            _dbContext.Entry<TEntity>(entity).State = EntityState.Detached;
        }

        public virtual IQueryable<TEntity> DbSet()
        {
            IQueryable<TEntity> iQueryable = _dbSet;

            #region Check Interfaces
            var entityType = typeof(TEntity);

            if (entityType.IsAssignableTo(typeof(ITenantable)))
            {
                var tenantId = this._tenantFinder.FindId();
                iQueryable = iQueryable.Cast<ITenantable>().Where(x => x.TenantId == tenantId).Cast<TEntity>();
            }

            if (entityType.IsAssignableTo(typeof(IDomainable)))
            {
                var domainId = this._domainFinder.FindId();
                iQueryable = iQueryable.Cast<IDomainable>().Where(x => x.DomainId == domainId).Cast<TEntity>();
            }

            if (entityType.IsAssignableTo(typeof(ICompanyable)))
            {
                var companyId = this._companyFinder.FindId();
                iQueryable = iQueryable.Cast<ICompanyable>().Where(x => x.CompanyId == companyId).Cast<TEntity>();
            }

            if (entityType.IsAssignableTo(typeof(IDeletable)))
                iQueryable = iQueryable.Cast<IDeletable>().Where(x => x.Deleted == false).Cast<TEntity>();
            #endregion

            return iQueryable;
        }

        protected DbSet<TEntity> OriginalDbSet()
        {
            return this._dbSet;
        }
        #endregion
    }
}