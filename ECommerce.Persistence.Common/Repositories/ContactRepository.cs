using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ContactRepository : RepositoryBase<Contact, Int32>, IContactRepository
    {
        #region Constructor
        public ContactRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Contact Read(Int32 id)
        {
            return base.DbSet().Include(c => c.ContactTranslations).FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}