using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class ExpenseRepository : RepositoryBase<Expense, Int32>, IExpenseRepository
    {
        #region Constructor
        public ExpenseRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Expense Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(e => e.Id == id);
        }
        #endregion
    }
}