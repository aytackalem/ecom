using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class BrandRepository : RepositoryBase<Brand, Int32>, IBrandRepository
    {
        #region Constructor
        public BrandRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Brand Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(e => e.Id == id);
        }


        public void BulkInsertMapping(List<Brand> brands)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var brandCopy = new List<Brand>();
                brandCopy.AddRange(brands);

                _dbContext.BulkInsert<Brand>(brands, new BulkConfig { SetOutputIdentity = true });

                //int brandIndex = brandCopy[0].Id;
                //brandCopy.ForEach(x =>
                //{
                //    x.Id = brandIndex;
                //    brandIndex++;
                //});

                //foreach (var bLoop in brands)
                //{
                //    bLoop.Url = $"{bLoop.Name.GenerateUrl()}-{bLoop.Id}-b";
                //    _dbContext.Update<Brand>(bLoop);
                //}

                transaction.Commit();

            }
        }
        #endregion
    }
}