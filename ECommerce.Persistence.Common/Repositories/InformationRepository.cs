using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class InformationRepository : RepositoryBase<Information, Int32>, IInformationRepository
    {
        #region Constructor
        public InformationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Information Read(Int32 id)
        {
            return base
                .DbSet()
                .Include(c => c.InformationTranslations)
                .ThenInclude(x => x.InformationTranslationContent)
                .Include(c => c.InformationTranslations)
                .ThenInclude(x => x.InformationSeoTranslation)
                .FirstOrDefault(c => c.Id == id);
        }
        #endregion
    }
}