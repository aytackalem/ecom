﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class UpsCityRepository : RepositoryBase<UpsCity, Int32>, IUpsCityRepository
    {
        #region Constructor
        public UpsCityRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public UpsCity Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(vv => vv.Id == id);
        }

        public UpsCity Read(string name)
        {
            if (name == "Bingöl")
            {
                name = "BINGOL";
            }
            else if (name == "Çanakkale")
            {
                name = "CANAKKALE";
            }
            else if (name == "Çankiri")
            {
                name = "CANKIRI";
            }
            else if (name == "Düzce")
            {
                name = "DUZCE";
            }
            else if (name == "Eskisehir")
            {
                name = "ESKİŞEHİR";
            }
            else if (name == "Gümüshane")
            {
                name = "GÜMÜŞHANE";
            }
            else if (name == "Karabük")
            {
                name = "KARABUK";
            }
            else if (name == "Kütahya")
            {
                name = "KUTAHYA";
            }
            else if (name == "Tekirdag")
            {
                name = "TEKIRDAĞ";
            }

            return base.DbSet().FirstOrDefault(ds => EF.Functions.Collate(ds.Name.ToLower(), "Turkish_CI_AS") == EF.Functions.Collate(name.ToLower(), "Turkish_CI_AS"));
        }
        #endregion
    }
}