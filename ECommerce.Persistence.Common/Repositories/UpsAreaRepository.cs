using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class UpsAreaRepository : RepositoryBase<UpsArea, Int32>, IUpsAreaRepository
    {
        #region Constructor
        public UpsAreaRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public UpsArea Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(vv => vv.Id == id);
        }

        public UpsArea Read(string name, int upsCityId)
        {
            var upsArea = base.DbSet().FirstOrDefault(ds => ds.Name.ToLower().Contains(name.ToLower()) && ds.UpsCityId == upsCityId);

            if (upsArea == null)
            {
                var districtName = name.ReplaceChar();
                upsArea = base.DbSet().FirstOrDefault(ds => ds.Name.ToLower().Contains(districtName.ToLower()) && ds.UpsCityId == upsCityId);

                if (upsArea == null)
                {
                    upsArea = base.DbSet().Where(ds => ds.UpsCityId == upsCityId).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == districtName);
                }

            }

            return upsArea;

        }
        #endregion
    }
}