using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class DistrictRepository : RepositoryBase<District, Int32>, IDistrictRepository
    {
        #region Constructor
        public DistrictRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public District Read(Int32 id)
        {
            return base.DbSet().FirstOrDefault(x => x.Id == id);
        }

        public void BulkInsertMapping(List<District> districts)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var districtsCopy = new List<District>();
                districtsCopy.AddRange(districts);

                _dbContext.BulkInsert<District>(districtsCopy, new BulkConfig { SetOutputIdentity = true });

                int districtIndex = districtsCopy[0].Id;
                districts.ForEach(x =>
                {
                    x.Id = districtIndex;
                    districtIndex++;
                });

                var neighborhoods = new List<Neighborhood>();

                foreach (var district in districts)
                {
                    foreach (var nb in district.Neighborhoods)
                    {
                        nb.DistrictId = district.Id;
                    }
                    neighborhoods.AddRange(district.Neighborhoods);

                }

                _dbContext.BulkInsert(neighborhoods, new BulkConfig { SetOutputIdentity = true });



                transaction.Commit();
            }
        }

        #endregion
    }
}