using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class VariantRepository : RepositoryBase<Variant, int>, IVariantRepository
    {
        #region Constructor
        public VariantRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
        {
        }
        #endregion

        #region Method
        public Variant Read(int id)
        {
            return base.DbSet().Include(v => v.VariantTranslations).FirstOrDefault(v => v.Id == id);
        }

        public void BulkInsertMapping(List<Variant> variants)
        {

            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var variantCopy = new List<Variant>();
                variantCopy.AddRange(variants);

                _dbContext.BulkInsert<Variant>(variantCopy, new BulkConfig { SetOutputIdentity = true });

                int variantIndex = variantCopy[0].Id;
                variants.ForEach(x =>
                {
                    x.Id = variantIndex;
                    variantIndex++;
                });

                var variantTranslations = new List<VariantTranslation>();

                foreach (var vLoop in variants)
                {

                    foreach (var pmLoop in vLoop.VariantTranslations)
                    {
                        pmLoop.VariantId = vLoop.Id;
                    }
                    variantTranslations.AddRange(vLoop.VariantTranslations);

                }

                _dbContext.BulkInsert(variantTranslations, new BulkConfig { SetOutputIdentity = true });

                transaction.Commit();
            }
        }
        #endregion
    }
}