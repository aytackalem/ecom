//using ECommerce.Application.Common.Interfaces.Helpers;
//using ECommerce.Application.Common.Interfaces.Repositories;
//using ECommerce.Domain.Entities;
//using ECommerce.Persistence.Context;
//using ECommerce.Repository.Base;
//using System;
//using System.Linq;

//namespace ECommerce.Repository
//{
//    public partial class MarketplaceVariantValueRepository : RepositoryBase<MarketplaceVariantValue, string>, IMarketplaceVariantValueRepository
//    {
//        #region Constructor
//        public MarketplaceVariantValueRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
//        {
//        }
//        #endregion

//        #region Method
//        public MarketplaceVariantValue Read(string id)
//        {
//            return base.DbSet().FirstOrDefault(x => x.Id == id);
//        }
//        #endregion
//    }
//}