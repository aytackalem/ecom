using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class EmailProviderConfigurationRepository : RepositoryBase<EmailProviderConfiguration, int>, IEmailProviderConfigurationRepository
	{
		#region Constructor
		public EmailProviderConfigurationRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public EmailProviderConfiguration Read(int id)
		{
			return base.DbSet().FirstOrDefault(x=>x.Id==id);
		}
		#endregion
	}
}