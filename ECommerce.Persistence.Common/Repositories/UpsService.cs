﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;

namespace ECommerce.Repository
{
    public class UpsService : IUpsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpsService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public DataResponse<UpsAddress> Read(string cityName, string districtName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<UpsAddress>>(result =>
            {

                var upsCity = _unitOfWork.UpsCityRepository.Read(cityName);
                if (upsCity == null)
                {
                    result.Message = "Ups İlçe bulunamadı.";
                    result.Success = false;
                    return;
                }

                if (districtName.Contains(cityName)) districtName = districtName.Replace(cityName, "").Replace("/", "").Replace("\\", "").Trim();

                var upsArea = _unitOfWork.UpsAreaRepository.Read(districtName, upsCity.Id);

                if (upsArea == null)
                {
                    result.Message = "Ups İlçe bulunamadı.";
                    result.Success = false;
                    return;
                }

                result.Success = true;
                result.Data = new UpsAddress
                {
                    AreaCode = upsArea.Id,
                    CityCode = upsCity.Id
                };

            });
        }
    }
}
