using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System; using ECommerce.Application.Common.Interfaces.Helpers;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class OrderDeliveryAddressRepository : RepositoryBase<OrderDeliveryAddress, Int32>, IOrderDeliveryAddressRepository
	{
		#region Constructor
		public OrderDeliveryAddressRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public OrderDeliveryAddress Read(Int32 id)
		{
			return base.DbSet().FirstOrDefault(x => x.Id == id);
		}
		#endregion
	}
}