using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Repositories;
using ECommerce.Domain.Entities;
using ECommerce.Persistence.Context;
using ECommerce.Repository.Base;
using System;
using System.Linq;

namespace ECommerce.Repository
{
    public partial class CustomerUserRepository : RepositoryBase<CustomerUser, Int32>, ICustomerUserRepository
	{
		#region Constructor
		public CustomerUserRepository(ApplicationDbContext dbContext, ITenantFinder tenantFinder, IDomainFinder domainFinder,ICompanyFinder companyFinder) : base(dbContext, tenantFinder, domainFinder, companyFinder)
		{
		}
		#endregion

		#region Method
		public CustomerUser Read(Int32 id)
		{
			return base.DbSet().FirstOrDefault(x=>x.Id==id);
		}
		#endregion
	}
}