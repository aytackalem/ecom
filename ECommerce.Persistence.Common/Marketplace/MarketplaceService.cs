﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters;
using ECommerce.Application.Common.Parameters.Product;
using ECommerce.Application.Common.Parameters.Stock;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.Marketplaces;
using ECommerce.Domain.Entities;
using ECommerce.Domain.Entities.Companyable;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.Marketplace
{
    public class MarketplaceService : IMarketplaceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private IServiceProvider _serviceProvider;

        private readonly IExternalCache _externalCache;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private Domain.Entities.Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Domain.Entities.Company _company;

        private readonly MarketplaceResolver _marketplaceResolver;

        private List<MarketplaceCompany> _marketplaceCompanies;
        #endregion

        #region Constructors
        public MarketplaceService(IUnitOfWork unitOfWork, IServiceProvider serviceProvider, IExternalCache externalCache, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, MarketplaceResolver marketplaceResolver)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._serviceProvider = serviceProvider;
            this._externalCache = externalCache;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._marketplaceResolver = marketplaceResolver;
            #endregion
            if (this._tenantFinder.FindId() > 0)
            {


                this._tenant = this
                    ._unitOfWork
                    .TenantRepository
                    .DbSet()
                    .Include(t => t.TenantSetting)
                    .First(t => t.Id == this._tenantFinder.FindId());
                this._domain = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting).First(d => d.Id == this._domainFinder.FindId());
                this._company = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(c => c.CompanySetting).First(c => c.Id == this._companyFinder.FindId());
            }
        }
        #endregion

        #region Methods
        #region Product Methods

        public DataResponse<List<MarketplacePriceStockResponse>> UpdatePrice(ProductInformationMarketplace productInformationMarketplace, string marketplaceId = null)
        {
            var response = new DataResponse<List<MarketplacePriceStockResponse>>()
            {
                Success = true,
                Data = new()
            };

            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!marketplaceCompany.UpdatePrice) return;

                if (productInformationMarketplace.MarketplaceId != marketplaceCompany.MarketplaceId) return;

                StockRequest request = new()
                {
                    UUId = productInformationMarketplace
                        .ProductInformation
                        .Product
                        .ProductMarketplaces
                        .FirstOrDefault(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)
                        ?.UUId,
                    MarketPlaceId = marketplaceId,
                    Configurations = marketplaceCompany
                        .MarketplaceConfigurations
                        .ToDictionary(mc => mc.Key, mc => mc.Value),
                    PriceStock = new PriceStock
                    {
                        StockCode = productInformationMarketplace.StockCode,
                        SkuCode = productInformationMarketplace.ProductInformation.SkuCode,
                        ListPrice = productInformationMarketplace.ListUnitPrice,
                        SalePrice = productInformationMarketplace.UnitPrice,
                        Barcode = productInformationMarketplace.Barcode,
                        UUId = productInformationMarketplace.UUId
                    }
                };
                var updatePrice = marketplace.UpdatePrice(request);
                if (updatePrice.Success && !string.IsNullOrEmpty(updatePrice.Data))
                {
                    response.Data.Add(new MarketplacePriceStockResponse
                    {
                        MarketplaceId = marketplaceCompany.MarketplaceId,
                        RequestId = updatePrice.Data
                    });
                }
            }, marketplaceId);

            return response;
        }

        public DataResponse<List<MarketplacePriceStockResponse>> UpdateStock(ProductInformationMarketplace productInformationMarketplace, string marketplaceId = null)
        {
            var response = new DataResponse<List<MarketplacePriceStockResponse>>()
            {
                Success = true,
                Data = new()
            };

            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!marketplaceCompany.UpdateStock) return;

                if (productInformationMarketplace.MarketplaceId != marketplaceCompany.MarketplaceId) return;

                StockRequest request = new()
                {
                    MarketPlaceId = marketplaceId,
                    Configurations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                    UUId = productInformationMarketplace.ProductInformation.Product.ProductMarketplaces.FirstOrDefault(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)?.UUId,
                    UpdatePrice = marketplaceCompany.UpdatePrice,
                    PriceStock = new PriceStock
                    {
                        StockCode = productInformationMarketplace.StockCode,
                        SkuCode = productInformationMarketplace.ProductInformation.SkuCode,
                        ListPrice = productInformationMarketplace.ListUnitPrice,
                        SalePrice = productInformationMarketplace.UnitPrice,
                        UUId = productInformationMarketplace.UUId,
                        Barcode = productInformationMarketplace.Barcode,
                        Quantity = productInformationMarketplace.ProductInformation.Stock < 0 ? 0 : productInformationMarketplace.ProductInformation.Stock
                    }
                };
                var updateStock = marketplace.UpdateStock(request);
                if (updateStock.Success && !string.IsNullOrEmpty(updateStock.Data))
                {
                    response.Data.Add(new MarketplacePriceStockResponse
                    {
                        MarketplaceId = marketplaceCompany.MarketplaceId,
                        RequestId = updateStock.Data
                    });
                }

            }, marketplaceId);

            return response;
        }

        public DataResponse<List<ProductItem>> GetProducts(string marketplaceId = null)
        {

            var response = new DataResponse<List<ProductItem>>();

            response.Data = new List<ProductItem>();


            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;


                var getProducts = marketplace.GetProducts();

                response.Data.AddRange(getProducts.Data);
                response.Success = true;

            }, marketplaceId);

            return response;
        }

        public DataResponse<List<MarketplaceProductStatus>> GetProductsStatus(string marketplaceId = null)
        {
            var response = new DataResponse<List<MarketplaceProductStatus>>() { Data = new(), Success = true };

            this.ParallelLoop((marketplace, marketplaceCompany) =>
            {
                var marketplaceResponse = marketplace.GetProductsStatus();
                if (marketplaceResponse.Success)
                    response.Data.AddRange(marketplaceResponse.Data);
            }, marketplaceId);

            return response;
        }

        public DataResponse<bool> StockCheck(string batchId, string marketplaceId = null)
        {
            var response = new DataResponse<bool>();
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace is IBatchable)
                {
                    var checkResponse = (marketplace as IBatchable).StockCheck(batchId);
                    response.Data = checkResponse.Data;
                    response.Success = checkResponse.Success;
                    response.Message = checkResponse.Message;
                }
            }, marketplaceId);
            return response;
        }

        public DataResponse<bool> PriceCheck(string batchId, string marketplaceId = null)
        {
            var response = new DataResponse<bool>();
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace is IBatchable)
                {
                    var checkResponse = (marketplace as IBatchable).PriceCheck(batchId);
                    response.Data = checkResponse.Data;
                    response.Success = checkResponse.Success;
                    response.Message = checkResponse.Message;
                }
            }, marketplaceId);
            return response;
        }

        public DataResponse<bool> ProductUpsertCheck(string batchId, string marketplaceId = null)
        {
            var response = new DataResponse<bool>();
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace is IBatchable)
                {
                    var checkResponse = (marketplace as IBatchable).CreateCheck(batchId);
                    response.Data = checkResponse.Data;
                    response.Success = checkResponse.Success;
                    response.Message = checkResponse.Message;
                }
            }, marketplaceId);
            return response;
        }
        #endregion

        #region Category Methods
        //public Response GetCategories(string marketplaceId = null)
        //{
        //    this.Loop((marketplace, marketplaceCompany) =>
        //    {
        //        if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
        //            return;

        //        var configrations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value);

        //        var response = marketplace.GetCategories(configrations);
        //        if (response.Success)
        //        {
        //            var marketplaceCategories = new List<ECommerceCategory>();

        //            try
        //            {
        //                var _marketplaceCategories = this
        //                    ._unitOfWork
        //                    .MarketplaceCategoryRepository
        //                    .DbSet()
        //                    .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId).Select(x => x.Id)
        //                    .ToList();

        //                marketplaceCategories = response
        //                    .Data
        //                    .Where(mpcLoop => !_marketplaceCategories.Any(y => y == $"{marketplaceCompany.MarketplaceId}-{mpcLoop.Code}"))
        //                    .GroupBy(x => x.Code).Select(x => x.First())
        //                    .Select(mpcLoop => new Domain.Entities.MarketplaceCategory
        //                    {
        //                        Id = $"{marketplaceCompany.MarketplaceId}-{mpcLoop.Code}",
        //                        MarketplaceId = marketplaceCompany.MarketplaceId,
        //                        Name = mpcLoop.Name,
        //                        ParentId = string.IsNullOrEmpty(mpcLoop.ParentCategoryCode)
        //                            ? null
        //                            : $"{marketplaceCompany.MarketplaceId}-{mpcLoop.ParentCategoryCode}",
        //                    })
        //                    .ToList();

        //                _unitOfWork.MarketplaceCategoryRepository.BulkInsert(marketplaceCategories);
        //            }
        //            catch (Exception e)
        //            {
        //            }

        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVarians Added");

        //            try
        //            {
        //                var _earketplaceVariantIds = this
        //                    ._unitOfWork
        //                    .MarketplaceVariantRepository
        //                    .DbSet()
        //                    .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId).Select(x => x.Id)
        //                    .ToList();

        //                var marketplaceVarians = response
        //                    .Data
        //                    .SelectMany(x => x.CategoryAttributes)
        //                    .OrderByDescending(x => x.Mandatory)
        //                    .GroupBy(x => x.Code).Select(x => x.First())
        //                    .Where(x => x.Code != null)
        //                    .Where(mpcLoop => !_earketplaceVariantIds.Any(y => y == $"{marketplaceCompany.MarketplaceId}-{mpcLoop.Code}"))
        //                    .Select(caLoop => new Domain.Entities.MarketplaceVariant
        //                    {
        //                        Id = $"{marketplaceCompany.MarketplaceId}-{caLoop.Code}",
        //                        Name = caLoop.Name,
        //                        MarketplaceId = marketplaceCompany.MarketplaceId,
        //                        AllowCustom = caLoop.AllowCustom,
        //                        Mandatory = caLoop.Mandatory,
        //                        MultiValue = caLoop.MultiValue
        //                    }).ToList();

        //                this._unitOfWork.MarketplaceVariantRepository.BulkInsert(marketplaceVarians);
        //            }
        //            catch (Exception e)
        //            {

        //            }

        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVarians Added");


        //            try
        //            {
        //                var _marketplaceVariantValueIds = this
        //                    ._unitOfWork
        //                    .MarketplaceVariantValueRepository
        //                    .DbSet()
        //                    .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)
        //                    .Select(x => x.Id)
        //                    .ToList();

        //                var marketplaceVariantValues = response
        //                    .Data
        //                    .SelectMany(x => x.CategoryAttributes)
        //                    .SelectMany(x => x.CategoryAttributesValues)
        //                    .GroupBy(x => new { x.Code }).Select(x => x.First())
        //                    .Where(x => x.Code != null)
        //                    .Where(mpcLoop => !_marketplaceVariantValueIds.Any(y => y == $"{marketplaceCompany.MarketplaceId}-{mpcLoop.Code}"))
        //                    .Select(caLoop => new Domain.Entities.MarketplaceVariantValue
        //                    {
        //                        Id = $"{marketplaceCompany.MarketplaceId}-{caLoop.Code}",
        //                        Value = caLoop.Name,
        //                        MarketplaceId = marketplaceCompany.MarketplaceId
        //                    })
        //                    .ToList();

        //                marketplaceVariantValues = marketplaceVariantValues
        //                    .Where(x => !x.Id.Contains("-m2") && !x.Id.Contains("m²") && !x.Id.Contains("m³"))
        //                    .GroupBy(x => x.Id.ToLower()).Select(x => x.First())
        //                    .ToList();

        //                this.
        //                    _unitOfWork.
        //                    MarketplaceVariantValueRepository
        //                    .BulkInsert(marketplaceVariantValues);
        //            }
        //            catch (Exception ex)
        //            {


        //            }

        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVariantValues Added");

        //            var _marketplaceVariantIds = this
        //                ._unitOfWork
        //                .MarketplaceCategoriesMarketplaceVariantRepository
        //                .DbSet()
        //                .Where(y => y.MarketplaceCategory.MarketplaceId == marketplaceCompany.MarketplaceId)
        //                .Select(x => new ECommerceVariant
        //                {
        //                    MarketplaceCategoryId = x.MarketplaceCategoryId,
        //                    MarketplaceVariantId = x.MarketplaceVariantId
        //                })
        //                .ToList();

        //            var marketplaceCategoriesMarketplaceVariants = response
        //                .Data
        //                .SelectMany(x => x.CategoryAttributes)
        //                .Where(x => x.Code != null)
        //                .Where(x => !_marketplaceVariantIds.Any(y =>
        //                    y.MarketplaceCategoryId == $"{marketplaceCompany.MarketplaceId}-{x.CategoryCode}"
        //                    && y.MarketplaceVariantId == $"{marketplaceCompany.MarketplaceId}-{x.Code}"))
        //                .Select(x => new Domain.Entities.MarketplaceCategoriesMarketplaceVariant
        //                {
        //                    MarketplaceCategoryId = $"{marketplaceCompany.MarketplaceId}-{x.CategoryCode}",
        //                    MarketplaceVariantId = $"{marketplaceCompany.MarketplaceId}-{x.Code}",
        //                    AllowCustom = x.AllowCustom,
        //                    Mandatory = x.Mandatory,
        //                    MultiValue = x.MultiValue,
        //                    Variantable = x.Variantable
        //                })
        //                .ToList();

        //            this
        //                ._unitOfWork
        //                .MarketplaceCategoriesMarketplaceVariantRepository
        //                .BulkInsert(marketplaceCategoriesMarketplaceVariants);

        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceCategoriesMarketplaceVariants Added");

        //            var _marketplaceVariantsMarketplaceVariantValueIds = this
        //                ._unitOfWork
        //                .MarketplaceVariantsMarketplaceVariantValueRepository
        //                .DbSet()
        //                .Where(x => x.MarketplaceVariant.MarketplaceId == marketplaceCompany.Marketplace.Id)
        //                .Select(x => new MarketplaceVariantsMarketplaceVariantValue
        //                {
        //                    MarketplaceVariantValueId = x.MarketplaceVariantValueId,
        //                    MarketplaceVariantId = x.MarketplaceVariantId
        //                })
        //                .ToList();

        //            var marketplaceVariantsMarketplaceVariantValues = response.Data
        //               .SelectMany(x => x.CategoryAttributes)
        //               .SelectMany(x => x.CategoryAttributesValues)
        //               .GroupBy(x => new { x.VarinatCode, x.Code })
        //                .Where(x => x.Key.Code != null && x.Key.VarinatCode != null)
        //                 .Where(x => !_marketplaceVariantsMarketplaceVariantValueIds.Any(y =>
        //                    y.MarketplaceVariantValueId == $"{marketplaceCompany.MarketplaceId}-{x.Key.Code}"
        //                    && y.MarketplaceVariantId == $"{marketplaceCompany.MarketplaceId}-{x.Key.VarinatCode}"))
        //               .Select(x => new Domain.Entities.MarketplaceVariantsMarketplaceVariantValue
        //               {
        //                   MarketplaceVariantId = $"{marketplaceCompany.MarketplaceId}-{x.Key.VarinatCode}",
        //                   MarketplaceVariantValueId = $"{marketplaceCompany.MarketplaceId}-{x.Key.Code}"
        //               }).ToList();

        //            _unitOfWork.MarketplaceVariantsMarketplaceVariantValueRepository.BulkInsert(marketplaceVariantsMarketplaceVariantValues);
        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVariantsMarketplaceVariantValues Added");


        //            var _marketplaceCategoriesMarketplaceVariants = _unitOfWork.MarketplaceCategoriesMarketplaceVariantRepository.DbSet().Where(x => x.MarketplaceCategory.MarketplaceId == marketplaceCompany.MarketplaceId).ToList();


        //            var _marketplaceCategoriesMarketplaceVariantValues = new List<MarketplaceCategoriesMarketplaceVariantValue>();
        //            foreach (var mcvLoop in response.Data)
        //            {
        //                var anyMarketplaceCategory = marketplaceCategories.Any(x => x.Id == $"{marketplaceCompany.MarketplaceId}-{mcvLoop.Code}");
        //                if (anyMarketplaceCategory)
        //                {

        //                    foreach (var caLoop in mcvLoop.CategoryAttributes)
        //                    {

        //                        var marketplaceCategoriesMarketplaceVariantId = _marketplaceCategoriesMarketplaceVariants.FirstOrDefault(mcv =>
        //                     mcv.MarketplaceCategoryId == $"{marketplaceCompany.MarketplaceId}-{caLoop.CategoryCode}" && mcv.MarketplaceVariantId == $"{marketplaceCompany.MarketplaceId}-{caLoop.Code}")?.Id;

        //                        foreach (var attLoop in caLoop.CategoryAttributesValues)
        //                        {

        //                            _marketplaceCategoriesMarketplaceVariantValues.Add(new MarketplaceCategoriesMarketplaceVariantValue
        //                            {
        //                                MarketplaceCategoriesMarketplaceVariantId = marketplaceCategoriesMarketplaceVariantId.Value,
        //                                MarketplaceVariantValueId = $"{marketplaceCompany.MarketplaceId}-{attLoop.Code}"
        //                            });

        //                        }
        //                    }


        //                }


        //            }


        //            _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.BulkInsert(_marketplaceCategoriesMarketplaceVariantValues);
        //            System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} MarketplaceCategoriesMarketplaceVariantValues Added");
        //        }
        //    }, marketplaceId);
        //    return null;
        //}
        #endregion

        #region Order Methods
        public DataResponse<OrderResponse> GetDeliveredOrders(string marketplaceId = null)
        {
            DataResponse<OrderResponse> response = new() { Data = new() { Orders = new() }, Success = true };
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace == null)
                    return;

                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                var request = new OrderRequest
                {
                    Configurations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                    MarketplaceId = marketplaceId
                };

                var marketplaceResponse = marketplace.GetDeliveredOrders(request);

                if (marketplaceResponse.Success)
                    response.Data.Orders.AddRange(marketplaceResponse.Data.Orders);
            }, marketplaceId);
            return response;
        }

        public DataResponse<OrderResponse> GetCancelledOrders(string marketplaceId = null)
        {
            DataResponse<OrderResponse> response = new() { Data = new() { Orders = new() }, Success = true };
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace == null)
                    return;

                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                var request = new OrderRequest
                {
                    Configurations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                    MarketplaceId = marketplaceId
                };

                var marketplaceResponse = marketplace.GetCancelledOrders(request);

                if (marketplaceResponse.Success && marketplaceResponse.Data != null && marketplaceResponse.Data.Orders != null)
                {
                    response.Data.Orders.AddRange(marketplaceResponse.Data.Orders);
                }



            });
            return response;
        }

        public DataResponse<OrderResponse> GetReturnedOrders(string marketplaceId = null)
        {
            DataResponse<OrderResponse> response = new() { Data = new() { Orders = new() }, Success = true };
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplace == null)
                    return;

                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                var request = new OrderRequest
                {
                    Configurations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                    MarketplaceId = marketplaceId
                };

                var marketplaceResponse = marketplace.GetReturnedOrders(request);

                if (marketplaceResponse.Success && marketplaceResponse.Data != null && marketplaceResponse.Data.Orders != null)
                {
                    response.Data.Orders.AddRange(marketplaceResponse.Data.Orders);
                }

            });
            return response;
        }

        public void RawOrders(string marketplaceId = null)
        {
            /* Fix: Her tenant farklı order source kayıtlarına sahip olduğundan bu kısma ihtiyaç duyulmuştur. */
            //var orderSource = this._unitOfWork.OrderSourceRepository.DbSet().FirstOrDefault(os => os.Name == "Pazaryeri");

            this.ParallelLoop((marketplace, marketplaceCompany) =>
            {
                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                /* Sipariş çekme onayı kontrol edilir. */
                if (!marketplaceCompany.ReadOrder)
                    return;

                var marketplaceResponse = marketplace.GetRawOrders(new RawOrderRequest());

                if (marketplaceResponse.Success)
                    Parallel.ForEach(marketplaceResponse.Data.Orders, theOrder =>
                    {
                        var tenantId = this._tenantFinder.FindId();
                        var domainId = this._domainFinder.FindId();
                        var companyId = this._companyFinder.FindId();

                        theOrder.OrderSourceId = OrderSources.Pazaryeri; //orderSource.Id;
                        theOrder.MarketplaceCompanyId = marketplaceCompany.Id;
                        theOrder.TenantId = tenantId;
                        theOrder.DomainId = domainId;
                        theOrder.CompanyId = companyId;

                        var key = $"{tenantId}_{domainId}_{companyId}_Orders_{theOrder.UId}";

                        if (this._externalCache.ContainsKey(key) == false)
                        {
                            var value = JsonConvert.SerializeObject(theOrder);

                            this._externalCache.SetEntry(key, value);

                            //TO DO: Stock operation
                            this._externalCache.SetEntry(key.Replace("_Orders_", "_Stocks_"), value);
                        }
                    });
            }, marketplaceId);
        }

        public void ProcessRawOrders(string marketplaceId = null)
        {
            this.ParallelLoop((marketplace, marketplaceCompany) =>
            {

                if (marketplaceCompany.ReadOrder == false)
                    return;

                var tenantId = this._tenantFinder.FindId();
                var domainId = this._domainFinder.FindId();
                var companyId = this._companyFinder.FindId();
                var marketplaceId = marketplaceCompany.MarketplaceId;
                var keyPrefix = $"{tenantId}_{domainId}_{companyId}_Orders_{marketplaceId}";
                var keys = this._externalCache.GetAllKeys().Where(k => k.StartsWith(keyPrefix));

                Parallel.ForEach(keys, theKey =>
                {
                    var value = this._externalCache.GetValue(theKey);
                    var order = JsonConvert.DeserializeObject<Application.Common.Wrappers.Marketplaces.Order.Order>(value);

                    ExceptionHandler.Handle(() =>
                    {
                        using (var scope = this._serviceProvider.CreateScope())
                        {
                            var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                            /* Inject tenant, domain, company. */
                            if (scope.ServiceProvider.GetService<ITenantFinder>() is not ISetableTenantFinder tenantFinder ||
                                scope.ServiceProvider.GetService<IDomainFinder>() is not ISetableDomainFinder domainFinder ||
                                scope.ServiceProvider.GetService<ICompanyFinder>() is not ISetableCompanyFinder companyFinder)
                                throw new Exception("Tenant finder or domain finder or company finder is setable.");

                            tenantFinder.SetId(tenantId);
                            domainFinder.SetId(domainId);
                            companyFinder.SetId(companyId);

                            var created = this.CreateRawOrder(unitOfWork, order);
                            if (created)
                            {
                                this._externalCache.RemoveEntry(theKey);

                                //TO DO: Marketplace request operation
                                this._externalCache.SetEntry(theKey.Replace("_Orders_", "_Pickings_"), JsonConvert.SerializeObject(order));

                                Console.WriteLine("OK");
                            }
                        }
                    }, (exception) =>
                    {
                        Console.WriteLine("Exception");
                    });
                });

            }, marketplaceId);
        }

        public void ProcessOrderTypeUpdates(string marketplaceId = null)
        {
            this.ParallelLoop((marketplace, marketplaceCompany) =>
            {
                var tenantId = this._tenantFinder.FindId();
                var domainId = this._domainFinder.FindId();
                var companyId = this._companyFinder.FindId();
                var marketplaceId = marketplaceCompany.MarketplaceId;
                var keyPrefix = $"{tenantId}_{domainId}_{companyId}_Pickings_{marketplaceId}";
                var keys = this._externalCache.GetAllKeys().Where(k => k.StartsWith(keyPrefix));
                Parallel.ForEach(keys, theKey =>
                {
                    var value = this._externalCache.GetValue(theKey);
                    var order = JsonConvert.DeserializeObject<Application.Common.Wrappers.Marketplaces.Order.Order>(value);

                    var orderTypeUpdateResponse = marketplace.OrderTypeUpdate(new OrderTypeUpdateRequest
                    {
                        Order = order,
                        MarketplaceStatus = Application.Common.Parameters.Enums.MarketplaceStatus.Picking
                    });

                    if (orderTypeUpdateResponse.Success)
                    {
                        ExceptionHandler.Handle(() =>
                        {
                            using (var scope = this._serviceProvider.CreateScope())
                            {
                                /* Inject tenant, domain, company. */
                                if (scope.ServiceProvider.GetService<ITenantFinder>() is not ISetableTenantFinder tenantFinder ||
                                    scope.ServiceProvider.GetService<IDomainFinder>() is not ISetableDomainFinder domainFinder ||
                                    scope.ServiceProvider.GetService<ICompanyFinder>() is not ISetableCompanyFinder companyFinder)
                                    throw new Exception("Tenant finder or domain finder or company finder is setable.");

                                tenantFinder.SetId(tenantId);
                                domainFinder.SetId(domainId);
                                companyFinder.SetId(companyId);

                                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                                var updated = false;
                                if (orderTypeUpdateResponse.Data != null &&
                                    !string.IsNullOrEmpty(orderTypeUpdateResponse.Data.TrackingCode) &&
                                    !string.IsNullOrEmpty(orderTypeUpdateResponse.Data.PackageNumber))
                                {
                                    updated = unitOfWork.OrderRepository.UpdateOrderType(
                                        order.Id,
                                        order.MutualBarcode ? OrderTypes.Beklemede : OrderTypes.OnaylanmisSiparis,
                                        orderTypeUpdateResponse.Data.PackageNumber,
                                        orderTypeUpdateResponse.Data.TrackingCode);

                                    order.OrderShipment.TrackingCode = orderTypeUpdateResponse.Data.TrackingCode;
                                    order.OrderShipment.PackageNumber = orderTypeUpdateResponse.Data.PackageNumber;
                                }
                                else
                                    updated = unitOfWork.OrderRepository.UpdateOrderType(
                                        order.Id,
                                        OrderTypes.OnaylanmisSiparis);

                                if (updated)
                                {
                                    this._externalCache.RemoveEntry(theKey);

                                    if (order.MutualBarcode)
                                        this._externalCache.SetEntry(theKey.Replace("_Pickings_", "_MutualBarcodes_"), JsonConvert.SerializeObject(order));

                                    Console.WriteLine("OK");
                                }
                            }
                        }, (exception) =>
                        {
                            Console.WriteLine("Exception");
                        });
                    }
                });
            }, marketplaceId);
        }

        public void ProcessMutualBarcodes(string marketplaceId = null)
        {
            this.ParallelLoop((marketplace, marketplaceCompany) =>
            {
                if (marketplace is not IMutualBarcodable)
                    return;

                var tenantId = this._tenantFinder.FindId();
                var domainId = this._domainFinder.FindId();
                var companyId = this._companyFinder.FindId();
                var marketplaceId = marketplaceCompany.MarketplaceId;
                var keyPrefix = $"{tenantId}_{domainId}_{companyId}_MutualBarcodes_{marketplaceId}";
                var keys = this._externalCache.GetAllKeys().Where(k => k.StartsWith(keyPrefix));
                Parallel.ForEach(keys, theKey =>
                {
                    var value = this._externalCache.GetValue(theKey);
                    var order = JsonConvert.DeserializeObject<Application.Common.Wrappers.Marketplaces.Order.Order>(value);

                    var orderTypeUpdateResponse = (marketplace as IMutualBarcodable).GetMutualBarcode(new MutualBarcodeRequest
                    {
                        PackageNumber = order.OrderShipment.PackageNumber
                    });

                    if (orderTypeUpdateResponse.Success)
                    {

                        ExceptionHandler.Handle(() =>
                        {
                            using (var scope = this._serviceProvider.CreateScope())
                            {
                                /* Inject tenant, domain, company. */
                                if (scope.ServiceProvider.GetService<ITenantFinder>() is not ISetableTenantFinder tenantFinder ||
                                    scope.ServiceProvider.GetService<IDomainFinder>() is not ISetableDomainFinder domainFinder ||
                                    scope.ServiceProvider.GetService<ICompanyFinder>() is not ISetableCompanyFinder companyFinder)
                                    throw new Exception("Tenant finder or domain finder or company finder is setable.");

                                tenantFinder.SetId(tenantId);
                                domainFinder.SetId(domainId);
                                companyFinder.SetId(companyId);

                                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                                var updated = unitOfWork
                                    .OrderShipmentRepository
                                    .UpdateOrderType(order.Id, orderTypeUpdateResponse.Data);
                                updated = unitOfWork
                                    .OrderRepository
                                    .UpdateOrderType(order.Id, OrderTypes.OnaylanmisSiparis);
                                if (updated)
                                {
                                    this._externalCache.RemoveEntry(theKey);

                                    Console.WriteLine("OK");
                                }
                            }
                        }, (exception) =>
                        {
                            Console.WriteLine("Exception");
                        });

                    }
                });
            }, marketplaceId);
        }

        public DataResponse<OrderResponse> GetOrders(DateTime? startDate = null, DateTime? endDate = null, string marketplaceId = null, string marketplaceOrderNumber = null)
        {
            /* Fix: Her tenant farklı order source kayıtlarına sahip olduğundan bu kısma ihtiyaç duyulmuştur. */
            var orderSources = this._unitOfWork.OrderSourceRepository.DbSet().ToList();

            DataResponse<OrderResponse> response = new() { Success = true, Data = new() { Orders = new() } };
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                /* Pazaryeri siparis okuma izni kontrol ediliyor. */
                if (!marketplaceCompany.ReadOrder)
                    return;

                var request = new OrderRequest
                {
                    Configurations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value),
                    MarketplaceId = marketplaceId,
                    StartDate = startDate,
                    EndDate = endDate,
                    MarketplaceOrderCode = marketplaceOrderNumber
                };

                var marketplaceResponse = marketplace.GetOrders(request);

                if (marketplaceResponse.Success && marketplaceResponse.Data != null && marketplaceResponse.Data.Orders != null)
                {
                    marketplaceResponse.Data.Orders.ForEach(x =>
                    {

                        var orderSource = orderSources.FirstOrDefault(os => os.Name == "Pazaryeri");

                        if (x.OrderSource == "WB")
                        {
                            orderSource = orderSources.FirstOrDefault(os => os.Name == "Web");
                        }

                        x.OrderSourceId = orderSource.Id;
                        x.MarketplaceCompanyId = marketplaceCompany.Id;
                    });
                    response.Data.Orders.AddRange(marketplaceResponse.Data.Orders);
                }

            }, marketplaceId);
            return response;
        }

        public Response PostInvoice(string shipmentPackageId, string invoiceLink, string marketplaceId = null)
        {
            Response respone = null;

            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                var marketplaceResponse = marketplace.PostInvoice(shipmentPackageId, invoiceLink);

            }, marketplaceId);

            return respone;
        }

        public DataResponse<CheckPreparingResponse> CheckPreparingOrder(OrderRequest orderRequest)
        {
            return new DataResponse<CheckPreparingResponse>();
        }

        public Response OrderTypeUpdate(OrderPickingRequest request)
        {
            Response respone = null;
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplaceCompany.MarketplaceId == request.MarketPlaceId)
                {
                    respone = marketplace.OrderTypeUpdate(request);
                }
            });
            return respone;
        }

        public DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> GetChangableCargoCompanies(string marketplaceId, string packageNumber)
        {
            if (string.IsNullOrEmpty(marketplaceId) || string.IsNullOrEmpty(packageNumber))
                return new DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>>
                {
                    Message = "Lütfen tüm parametreleri eksiksiz gönderin."
                };

            DataResponse<List<Application.Common.DataTransferObjects.KeyValue<string, string>>> dataResponse = null;
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplaceCompany.MarketplaceId == marketplaceId)
                {
                    dataResponse = ((ICargoChangable)marketplace).GetChangableCargoCompanies(packageNumber);
                }
            });
            return dataResponse;
        }

        public Response ChangeCargoCompany(string marketplaceId, string marketplaceOrderNumber, string packageNumber, string cargoCompanyId)
        {
            if (string.IsNullOrEmpty(marketplaceId) || string.IsNullOrEmpty(marketplaceOrderNumber) || string.IsNullOrEmpty(packageNumber) || string.IsNullOrEmpty(cargoCompanyId))
                return new Response
                {
                    Message = "Lütfen tüm parametreleri eksiksiz gönderin."
                };

            Response response = new();
            this.Loop((marketplace, marketplaceCompany) =>
            {
                var dataResponse = ((ICargoChangable)marketplace).ChangeCargoCompany(packageNumber, cargoCompanyId);
                if (dataResponse.Success)
                {
                    var order = this
                        ._unitOfWork
                        .OrderRepository
                        .DbSet()
                        .Include(o => o.OrderShipments)
                        .FirstOrDefault(o => o.MarketplaceId == marketplaceId &&
                                             o.MarketplaceOrderNumber == marketplaceOrderNumber &&
                                             o.OrderShipments.Any(os => os.PackageNumber == packageNumber));

                    if (dataResponse.Data.Retake)
                    {
                        var getOrdersResponse = marketplace
                            .GetOrders(new OrderRequest
                            {
                                MarketplaceOrderCode = marketplaceOrderNumber
                            });

                        if (getOrdersResponse.Success)
                        {
                            order.OrderShipments[0].TrackingCode = getOrdersResponse.Data.Orders.First().OrderShipment.TrackingCode;
                        }
                    }

                    order.OrderShipments[0].ShipmentCompanyId = dataResponse.Data.ShipmentCompanyId;
                    order.OrderShipments[0].TrackingBase64 = null;

                    this._unitOfWork.OrderRepository.Update(order);

                    response.Message = "Kargo firması değiştirilmiştir.";
                    response.Success = true;
                }
            }, marketplaceId);
            return response;
        }

        public Response Unpack(string marketplaceId, string marketplaceOrderNumber, string packageNumber)
        {
            if (string.IsNullOrEmpty(marketplaceId) || string.IsNullOrEmpty(marketplaceOrderNumber) || string.IsNullOrEmpty(packageNumber))
                return new Response
                {
                    Message = "Lütfen tüm parametreleri eksiksiz gönderin."
                };

            Response response = null;
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (marketplaceCompany.MarketplaceId == marketplaceId)
                {
                    response = ((IPackable)marketplace).Unpack(packageNumber);
                    if (response.Success)
                    {
                        var order = this
                            ._unitOfWork
                            .OrderRepository
                            .DbSet()
                            .FirstOrDefault(o => o.MarketplaceId == marketplaceId &&
                                                 o.MarketplaceOrderNumber == marketplaceOrderNumber &&
                                                 o.OrderShipments.Any(os => os.PackageNumber == packageNumber));
                        order.OrderTypeId = OrderTypes.Iptal;

                        this._unitOfWork.OrderRepository.Update(order);
                    }
                }
            });
            return response;
        }
        #endregion

        #region Brand Methods
        public DataResponse<List<ECommerce.Application.Common.DataTransferObjects.KeyValue<string, string>>> BrandReadAsKeyValue(string q, string marketplaceId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ECommerce.Application.Common.DataTransferObjects.KeyValue<string, string>>>>((response) =>
            {
                IMarketplace marketplace = this._marketplaceResolver(marketplaceId);

                var configurations = this._unitOfWork.MarketplaceConfigurationRepository.DbSet().Where(x => x.MarketplaceCompany.MarketplaceId == marketplaceId).ToList();

                marketplace.BindConfiguration(configurations);

                var dataResponse = marketplace.BrandReadAsKeyValue(q, null);
                if (dataResponse.Success && dataResponse.Data != null)
                {
                    response.Data = dataResponse.Data;
                    response.Success = true;
                }
            });
        }
        #endregion

        public List<MarketplaceCompany> GetMarketplaceCompanies()
        {
            FillMarketplaceCompanies();

            return this._marketplaceCompanies;
        }

        public List<MarketplaceConfiguration> GetMarketplaceConfigurations(string marketplaceId)
        {
            FillMarketplaceCompanies();

            return this
                ._marketplaceCompanies
                .FirstOrDefault(mc => mc.MarketplaceId == marketplaceId)
                .MarketplaceConfigurations;
        }

        public MarketplaceConfiguration GetMarketplaceConfiguration(string marketplaceId, string key)
        {
            FillMarketplaceCompanies();

            return this
                ._marketplaceCompanies
                .FirstOrDefault(mc => mc.MarketplaceId == marketplaceId)
                .MarketplaceConfigurations
                .FirstOrDefault(x => x.Key == key);
        }
        #endregion

        #region Helper Methods
        private void FillMarketplaceCompanies(string marketplaceId = null)
        {
            this._marketplaceCompanies = this
                ._unitOfWork
                .MarketplaceCompanyRepository
                .DbSet()
                .Include(x => x.Marketplace)
                .Include(x => x.MarketplaceConfigurations)
                .Where(x => x.Active && (marketplaceId == null || x.MarketplaceId == marketplaceId))
                .ToList();
        }

        private bool CreateRawOrder(IUnitOfWork unitOfWork, Application.Common.Wrappers.Marketplaces.Order.Order theOrder)
        {
            bool created = false;

            #region Check Duplicate Order
            var duplicateOrder = unitOfWork
                            .OrderRepository
                            .DbSet()
                            .Any(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                      x.MarketplaceOrderNumber == theOrder.OrderCode &&
                                      (
                                        string.IsNullOrEmpty(theOrder.OrderShipment.TrackingCode) ||
                                        x.OrderShipments.Any(x => x.TrackingCode == theOrder.OrderShipment.TrackingCode)
                                      ));
            if (duplicateOrder)
            {
                XConsole.Warning("Duplicate order.");
                return true;
            }
            #endregion

            #region Find Delivery Neighborhood
            var deliveryFindNeighborhoodId = FindNeighborhoodId(
                            unitOfWork,
                            theOrder.OrderDeliveryAddress.City,
                            theOrder.OrderDeliveryAddress.District,
                            theOrder.OrderDeliveryAddress.Neighborhood);
            if (!deliveryFindNeighborhoodId.Success)
            {
                XConsole.Error("Delivery neighborhood not found.");
                return false;
            }
            #endregion

            #region Find Invoice Neighborhood
            var invoiceFindNeighborhoodId = FindNeighborhoodId(
                            unitOfWork,
                            theOrder.OrderInvoiceAddress.City,
                            theOrder.OrderInvoiceAddress.District,
                            theOrder.OrderInvoiceAddress.Neighborhood);
            if (!invoiceFindNeighborhoodId.Success)
            {
                XConsole.Error("Invoice neighborhood not found.");
                return false;
            }
            #endregion

            #region Product Create
            var orderDetails = new List<ECommerce.Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

            var orderSkip = false;

            foreach (var theOrderDetail in theOrder.OrderDetails)
            {
                #region Find Product Information
                /* İlgili pazaryeri için stok kodu ve barkod alanları kullanılarak ürün eşleşmesi bulunur. */
                ProductInformationMarketplace? productInformationMarketplace = null;

                /* Stok kodundan ürün sorgulanır. */
                if (productInformationMarketplace == null &&
                    !string.IsNullOrEmpty(theOrderDetail.Product.StockCode) &&
                    !string.IsNullOrWhiteSpace(theOrderDetail.Product.StockCode))
                    productInformationMarketplace = this
                        ._unitOfWork
                        .ProductInformationMarketplaceRepository
                        .DbSet()
                        .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                        .FirstOrDefault(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                             x.StockCode.ToLower() == theOrderDetail.Product.StockCode.ToLower());

                /* Barkod'dan ürün sorgulanır. */
                if (productInformationMarketplace == null &&
                    !string.IsNullOrEmpty(theOrderDetail.Product.Barcode) &&
                    !string.IsNullOrWhiteSpace(theOrderDetail.Product.Barcode))
                    productInformationMarketplace = this
                        ._unitOfWork
                        .ProductInformationMarketplaceRepository
                        .DbSet()
                        .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                        .FirstOrDefault(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                             x.Barcode.ToLower() == theOrderDetail.Product.Barcode.ToLower());
                #endregion

                var productInformationId = 0;
                var unitCost = 0m;
                if (productInformationMarketplace == null)
                {
                    if (this._company.CompanySetting.CreateNonexistProduct)
                    {
                        //TO DO: Create Nonexist Product
                        var product = new Product
                        {
                            Active = false,
                            BrandId = _unitOfWork.BrandRepository.DbSet().FirstOrDefault().Id,
                            CategoryId = _unitOfWork.CategoryRepository.DbSet().FirstOrDefault().Id,
                            //GroupId = _group.Id,
                            SupplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id,

                            CreatedDate = DateTime.Now,
                            IsOnlyHidden = true,
                            ProductTranslations = new List<ProductTranslation>
                                {
                                        new ProductTranslation
                                        {
                                            LanguageId="TR",
                                            CreatedDate=DateTime.Now,

                                            Name=theOrderDetail.Product.Name
                                        }
                                },
                            ProductInformations = new List<ProductInformation>
                                {
                                        new ProductInformation
                                        {
                                            Barcode=theOrderDetail.Product.Barcode,

                                            StockCode=theOrderDetail.Product.StockCode,
                                            IsSale=false,
                                            CreatedDate=DateTime.Now,
                                            Stock=0,

                                            ProductInformationTranslations=new List<ProductInformationTranslation>
                                            {
                                                new ProductInformationTranslation{
                                                    CreatedDate=DateTime.Now,
                                                    LanguageId="TR",

                                                    Name=theOrderDetail.Product.Name,
                                                    Url="",
                                                    SubTitle=""}
                                            },
                                            ProductInformationPriceses=new List<ProductInformationPrice>
                                            {
                                                new ProductInformationPrice
                                                {

                                                    CreatedDate=DateTime.Now,
                                                    CurrencyId="TL",
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    VatRate=Convert.ToDecimal(theOrderDetail.TaxRate),
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    UnitCost=0
                                                }
                                            },
                                            ProductInformationMarketplaces = new()
                                            {
                                                new()
                                                {
                                                    MarketplaceId=theOrder.MarketplaceId,
                                                    Active=true,
                                                    Barcode=theOrderDetail.Product.Barcode,
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    //CreatedDate=DateTime.Now.Date,
                                                    //
                                                    StockCode=Guid.NewGuid().ToString()
                                                }
                                            }
                                        }
                                }
                        };
                        unitOfWork.ProductRepository.Create(product);

                        productInformationId = product.ProductInformations[0].Id;
                        unitCost = 0;
                    }
                    else
                    {
                        orderSkip = true;
                        break;
                    }
                }
                else
                {
                    productInformationId = productInformationMarketplace.ProductInformationId;
                    unitCost = productInformationMarketplace
                      .ProductInformation
                      .ProductInformationPriceses
                      .FirstOrDefault()
                      .UnitCost;
                }

                theOrderDetail.UnitCost = unitCost;
                theOrderDetail.Product.ProductInformationId = productInformationId;

                var averageCommission = this.GetMarketplaceConfiguration(theOrder.MarketplaceId, "AverageCommission");

                var unitCommissionAmount = 0.10m;
                decimal.TryParse(averageCommission?.Value, out unitCommissionAmount);
                if (theOrderDetail.UnitCommissionAmount == 0 && unitCommissionAmount > 0)
                {
                    theOrderDetail.UnitCommissionAmount = theOrderDetail.UnitPrice * unitCommissionAmount;
                }

                if (productInformationMarketplace?.ProductInformation?.Type == "PC")
                {
                    var productInformationCombines = unitOfWork
                        .ProductInformationCombineRepository
                        .DbSet()
                        .Include(x => x.ProductInformation)
                        .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                        .Where(x => x.ProductInformationId == productInformationMarketplace.ProductInformationId)
                        .ToList();

                    var totalQuantity = productInformationCombines.Sum(x => x.Quantity);
                    var ratioListPrice = theOrderDetail.ListPrice / totalQuantity;
                    var ratioUnitPrice = theOrderDetail.UnitPrice / totalQuantity;
                    var ratioUnitDiscount = theOrderDetail.UnitDiscount / totalQuantity;
                    var ratioUnitCommissionAmount = theOrderDetail.UnitCommissionAmount / totalQuantity;

                    foreach (var picLoop in productInformationCombines)
                    {
                        var productInformationPriceses = picLoop.ProductInformation.ProductInformationPriceses.FirstOrDefault();

                        var orderDetail = new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                        {
                            ListPrice = ratioListPrice,
                            Quantity = picLoop.Quantity * theOrderDetail.Quantity,
                            UnitPrice = ratioUnitPrice,
                            TaxRate = Convert.ToDouble(productInformationPriceses.VatRate),
                            UnitCost = productInformationPriceses.UnitCost,
                            Payor = false,
                            UnitDiscount = ratioUnitDiscount,
                            UnitCommissionAmount = ratioUnitCommissionAmount,
                            Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                            {
                                ProductInformationId = picLoop.ContainProductInformationId
                            }
                        };

                        orderDetails.Add(orderDetail);
                    }
                }
                else
                    orderDetails.Add(theOrderDetail);
            }

            if (orderSkip)
            {
                XConsole.Error("Product not found.");
                return false;
            }
            #endregion

            #region Check And Create Shipment Company
            var _shipmentCompany = unitOfWork
                            .ShipmentCompanyRepository
                            .DbSet()
                            .FirstOrDefault(x => x.Id == theOrder.OrderShipment.ShipmentCompanyId);
            if (_shipmentCompany == null)
                unitOfWork.ShipmentCompanyRepository.Create(new ShipmentCompany
                {
                    Id = theOrder.OrderShipment.ShipmentCompanyId,
                    Name = theOrder.OrderShipment.ShipmentCompanyId
                });
            #endregion

            #region Create Order
            var orderEntity = new Domain.Entities.Order
            {
                Customer = new Customer
                {
                    Name = theOrder.Customer.FirstName.ToUpperInvariant(),
                    Surname = theOrder.Customer.LastName.ToUpperInvariant(),
                    CustomerContact = new CustomerContact
                    {
                        Phone = theOrder.Customer.Phone.ToNumber(),
                        Mail = theOrder.Customer.Mail,
                        TaxNumber = theOrder.Customer.TaxNumber,
                        TaxOffice = theOrder.Customer.TaxAdministration,
                        CreatedDate = DateTime.Now
                    },
                    CustomerAddresses = new List<CustomerAddress>
                        {
                                new CustomerAddress
                                {
                                    NeighborhoodId = deliveryFindNeighborhoodId.Data,
                                    Address = theOrder.OrderDeliveryAddress.Address,
                                    Recipient = $"{theOrder.Customer.FirstName} {theOrder.Customer.LastName}",
                                    CreatedDate = DateTime.Now
                                }
                        },
                    CreatedDate = DateTime.Now
                },
                CurrencyId = "TL",
                LanguageId = "TR",
                OrderTypeId = theOrder.OrderTypeId,
                OrderSourceId = theOrder.OrderSourceId,
                OrderShipments = new List<Domain.Entities.OrderShipment>
                    {
                            new Domain.Entities.OrderShipment
                            {
                                TrackingUrl = theOrder.OrderShipment.TrackingUrl,
                                TrackingCode = theOrder.OrderShipment.TrackingCode,
                                TrackingBase64 = theOrder.OrderShipment.TrackingBase64,
                                ShipmentCompanyId = _shipmentCompany.Id,
                                CreatedDate = DateTime.Now,
                                Payor = theOrder.OrderShipment.Payor,
                                PackageNumber = theOrder.OrderShipment.PackageNumber
                            }
                    },
                ListTotal = theOrder.ListTotalAmount,
                Total = theOrder.TotalAmount,
                VatExcTotal = theOrder.VatExcTotal,
                VatExcDiscount = theOrder.VatExcDiscount,
                VatExcListTotal = theOrder.VatExcListTotal,
                Discount = theOrder.Discount,
                CreatedDate = DateTime.Now,
                CargoFee = theOrder.CargoFee,
                SurchargeFee = 0,
                OrderDeliveryAddress = new Domain.Entities.OrderDeliveryAddress
                {
                    Recipient = $"{theOrder.OrderDeliveryAddress.FirstName} {theOrder.OrderDeliveryAddress.LastName}",
                    CreatedDate = DateTime.Now,
                    PhoneNumber = theOrder.OrderDeliveryAddress.Phone.ToNumber(),
                    Address = theOrder.OrderDeliveryAddress.Address,
                    NeighborhoodId = deliveryFindNeighborhoodId.Data
                },
                OrderInvoiceInformation = new OrderInvoiceInformation
                {
                    CreatedDate = DateTime.Now,
                    NeighborhoodId = invoiceFindNeighborhoodId.Data,
                    Address = theOrder.OrderInvoiceAddress.Address,
                    Phone = theOrder.OrderInvoiceAddress.Phone.ToNumber(),
                    TaxNumber = theOrder.OrderInvoiceAddress.TaxNumber,
                    TaxOffice = theOrder.OrderInvoiceAddress.TaxOffice,
                    FirstName = theOrder.OrderInvoiceAddress.FirstName,
                    LastName = theOrder.OrderInvoiceAddress.LastName,
                    Mail = theOrder.OrderInvoiceAddress.Email
                },
                OrderTechnicInformation = new OrderTechnicInformation
                {
                    Browser = "",
                    IpAddress = "",
                    Platform = "",
                },
                OrderDetails = orderDetails.Select(x => new ECommerce.Domain.Entities.OrderDetail
                {
                    ListUnitPrice = x.ListPrice,
                    UnitDiscount = x.UnitDiscount,
                    ProductInformationId = x.Product.ProductInformationId,
                    Quantity = x.Quantity,
                    UnitCost = x.UnitCost,
                    UnitPrice = x.UnitPrice,
                    VatRate = Convert.ToDecimal(x.TaxRate),
                    CreatedDate = DateTime.Now,
                    Payor = x.Payor,
                    UnitCargoFee = theOrder.CargoFee > 0 ? theOrder.CargoFee / orderDetails.Sum(od => od.Quantity) : 0,
                    UnitCommissionAmount = x.UnitCommissionAmount,
                    VatExcUnitCost = x.VatExcUnitCost,
                    VatExcListUnitPrice = x.VatExcListUnitPrice,
                    VatExcUnitDiscount = x.VatExcUnitDiscount,
                    VatExcUnitPrice = x.VatExcUnitPrice
                }).ToList(),
                MarketplaceId = theOrder.MarketplaceId,
                MarketplaceOrderNumber = theOrder.OrderCode,
                CommissionAmount = theOrder.CommissionAmount,
                OrderDate = theOrder.OrderDate,
                EstimatedPackingDate = DateTime.Now,
                Application = null
            };

            if (!string.IsNullOrEmpty(theOrder.OrderNote))
            {
                orderEntity.OrderNotes = new List<OrderNote>
                    {
                            new OrderNote
                            {
                                CreatedDate = DateTime.Now,
                                Note = theOrder.OrderNote
                            }
                    };
            }

            created = unitOfWork.OrderRepository.Create(orderEntity);

            if (created)
                theOrder.Id = orderEntity.Id;
            #endregion

            return created;
        }

        private bool CreateOrder(Application.Common.Wrappers.Marketplaces.Order.Order theOrder)
        {
            bool created = false;

            using (var scope = this._serviceProvider.CreateScope())
            {

                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                #region Check Duplicate Order
                var duplicateOrder = unitOfWork
                                .OrderRepository
                                .DbSet()
                                .Any(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                          x.MarketplaceOrderNumber == theOrder.OrderCode &&
                                          (
                                            string.IsNullOrEmpty(theOrder.OrderShipment.TrackingCode) ||
                                            x.OrderShipments.Any(x => x.TrackingCode == theOrder.OrderShipment.TrackingCode)
                                          ));
                if (duplicateOrder)
                {
                    XConsole.Warning("Duplicate order.");
                    return true;
                }
                #endregion

                #region Find Delivery Neighborhood
                var deliveryFindNeighborhoodId = FindNeighborhoodId(
                                unitOfWork,
                                theOrder.OrderDeliveryAddress.City,
                                theOrder.OrderDeliveryAddress.District,
                                theOrder.OrderDeliveryAddress.Neighborhood);
                if (!deliveryFindNeighborhoodId.Success)
                {
                    XConsole.Error("Delivery neighborhood not found.");
                    return false;
                }
                #endregion

                #region Find Invoice Neighborhood
                var invoiceFindNeighborhoodId = FindNeighborhoodId(
                                unitOfWork,
                                theOrder.OrderInvoiceAddress.City,
                                theOrder.OrderInvoiceAddress.District,
                                theOrder.OrderInvoiceAddress.Neighborhood);
                if (!invoiceFindNeighborhoodId.Success)
                {
                    XConsole.Error("Invoice neighborhood not found.");
                    return false;
                }
                #endregion

                #region Product Create
                var orderDetails = new List<Application.Common.Wrappers.Marketplaces.Order.OrderDetail>();

                var orderSkip = false;

                foreach (var theOrderDetail in theOrder.OrderDetails)
                {
                    #region Find Product Information
                    /* İlgili pazaryeri için stok kodu ve barkod alanları kullanılarak ürün eşleşmesi bulunur. */
                    ProductInformationMarketplace? productInformationMarketplace = null;

                    /* Stok kodundan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.StockCode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.StockCode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .FirstOrDefault(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                                 x.StockCode.ToLower() == theOrderDetail.Product.StockCode.ToLower());

                    /* Barkod'dan ürün sorgulanır. */
                    if (productInformationMarketplace == null &&
                        !string.IsNullOrEmpty(theOrderDetail.Product.Barcode) &&
                        !string.IsNullOrWhiteSpace(theOrderDetail.Product.Barcode))
                        productInformationMarketplace = this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Include(pim => pim.ProductInformation.ProductInformationPriceses.Where(pip => pip.CurrencyId == "TL"))
                            .FirstOrDefault(x => x.MarketplaceId == theOrder.MarketplaceId &&
                                                 x.Barcode.ToLower() == theOrderDetail.Product.Barcode.ToLower());
                    #endregion

                    var productInformationId = 0;
                    var unitCost = 0m;
                    if (productInformationMarketplace == null)
                    {
                        if (this._company.CompanySetting.CreateNonexistProduct)
                        {
                            //TO DO: Create Nonexist Product
                            var product = new Product
                            {
                                Active = false,
                                BrandId = _unitOfWork.BrandRepository.DbSet().FirstOrDefault().Id,
                                CategoryId = _unitOfWork.CategoryRepository.DbSet().FirstOrDefault().Id,
                                //GroupId = _group.Id,
                                SupplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id,

                                CreatedDate = DateTime.Now,
                                IsOnlyHidden = true,
                                ProductTranslations = new List<ProductTranslation>
                                {
                                        new ProductTranslation
                                        {
                                            LanguageId="TR",
                                            CreatedDate=DateTime.Now,

                                            Name=theOrderDetail.Product.Name
                                        }
                                },
                                ProductInformations = new List<ProductInformation>
                                {
                                        new ProductInformation
                                        {
                                            Barcode=theOrderDetail.Product.Barcode,

                                            StockCode=theOrderDetail.Product.StockCode,
                                            IsSale=false,
                                            CreatedDate=DateTime.Now,
                                            Stock=0,

                                            ProductInformationTranslations=new List<ProductInformationTranslation>
                                            {
                                                new ProductInformationTranslation{
                                                    CreatedDate=DateTime.Now,
                                                    LanguageId="TR",

                                                    Name=theOrderDetail.Product.Name,
                                                    Url="",
                                                    SubTitle=""}
                                            },
                                            ProductInformationPriceses=new List<ProductInformationPrice>
                                            {
                                                new ProductInformationPrice
                                                {

                                                    CreatedDate=DateTime.Now,
                                                    CurrencyId="TL",
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    VatRate=Convert.ToDecimal(theOrderDetail.TaxRate),
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    UnitCost=0
                                                }
                                            },
                                            ProductInformationMarketplaces = new()
                                            {
                                                new()
                                                {
                                                    MarketplaceId=theOrder.MarketplaceId,
                                                    Active=true,
                                                    Barcode=theOrderDetail.Product.Barcode,
                                                    ListUnitPrice=theOrderDetail.ListPrice,
                                                    UnitPrice=theOrderDetail.UnitPrice,
                                                    //CreatedDate=DateTime.Now.Date,
                                                    //
                                                    StockCode=Guid.NewGuid().ToString()
                                                }
                                            }
                                        }
                                }
                            };
                            unitOfWork.ProductRepository.Create(product);

                            productInformationId = product.ProductInformations[0].Id;
                            unitCost = 0;
                        }
                        else
                        {
                            orderSkip = true;
                            break;
                        }
                    }
                    else
                    {
                        productInformationId = productInformationMarketplace.ProductInformationId;
                        unitCost = productInformationMarketplace
                          .ProductInformation
                          .ProductInformationPriceses
                          .FirstOrDefault()
                          .UnitCost;
                    }

                    theOrderDetail.UnitCost = unitCost;
                    theOrderDetail.Product.ProductInformationId = productInformationId;

                    var averageCommission = this.GetMarketplaceConfiguration(theOrder.MarketplaceId, "AverageCommission");

                    var unitCommissionAmount = 0.10m;
                    decimal.TryParse(averageCommission?.Value, out unitCommissionAmount);
                    if (theOrderDetail.UnitCommissionAmount == 0 && unitCommissionAmount > 0)
                    {
                        theOrderDetail.UnitCommissionAmount = theOrderDetail.UnitPrice * unitCommissionAmount;
                    }

                    if (productInformationMarketplace?.ProductInformation?.Type == "PC")
                    {
                        var productInformationCombines = unitOfWork
                            .ProductInformationCombineRepository
                            .DbSet()
                            .Include(x => x.ProductInformation)
                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                            .Where(x => x.ProductInformationId == productInformationMarketplace.ProductInformationId)
                            .ToList();

                        var totalQuantity = productInformationCombines.Sum(x => x.Quantity);
                        var ratioListPrice = theOrderDetail.ListPrice / totalQuantity;
                        var ratioUnitPrice = theOrderDetail.UnitPrice / totalQuantity;
                        var ratioUnitDiscount = theOrderDetail.UnitDiscount / totalQuantity;
                        var ratioUnitCommissionAmount = theOrderDetail.UnitCommissionAmount / totalQuantity;

                        foreach (var picLoop in productInformationCombines)
                        {
                            var productInformationPriceses = picLoop.ProductInformation.ProductInformationPriceses.FirstOrDefault();

                            var orderDetail = new Application.Common.Wrappers.Marketplaces.Order.OrderDetail
                            {
                                ListPrice = ratioListPrice,
                                Quantity = picLoop.Quantity * theOrderDetail.Quantity,
                                UnitPrice = ratioUnitPrice,
                                TaxRate = Convert.ToDouble(productInformationPriceses.VatRate),
                                UnitCost = productInformationPriceses.UnitCost,
                                Payor = false,
                                UnitDiscount = ratioUnitDiscount,
                                UnitCommissionAmount = ratioUnitCommissionAmount,
                                Product = new Application.Common.Wrappers.Marketplaces.Order.Product
                                {
                                    ProductInformationId = picLoop.ContainProductInformationId
                                }
                            };

                            orderDetails.Add(orderDetail);
                        }
                    }
                    else
                        orderDetails.Add(theOrderDetail);
                }

                if (orderSkip)
                {
                    XConsole.Error("Product not found.");
                    return false;
                }
                #endregion

                #region Check And Create Shipment Company
                var _shipmentCompany = unitOfWork
                                .ShipmentCompanyRepository
                                .DbSet()
                                .FirstOrDefault(x => x.Id == theOrder.OrderShipment.ShipmentCompanyId);
                if (_shipmentCompany == null)
                    unitOfWork.ShipmentCompanyRepository.Create(new ShipmentCompany
                    {
                        Id = theOrder.OrderShipment.ShipmentCompanyId,
                        Name = theOrder.OrderShipment.ShipmentCompanyId
                    });
                #endregion

                #region Create Order
                var orderEntity = new Domain.Entities.Order
                {
                    Customer = new Customer
                    {
                        Name = theOrder.Customer.FirstName.ToUpperInvariant(),
                        Surname = theOrder.Customer.LastName.ToUpperInvariant(),
                        CustomerContact = new CustomerContact
                        {
                            Phone = theOrder.Customer.Phone.ToNumber(),
                            Mail = theOrder.Customer.Mail,
                            TaxNumber = theOrder.Customer.TaxNumber,
                            TaxOffice = theOrder.Customer.TaxAdministration,
                            CreatedDate = DateTime.Now
                        },
                        CustomerAddresses = new List<CustomerAddress>
                        {
                                new CustomerAddress
                                {
                                    NeighborhoodId = deliveryFindNeighborhoodId.Data,
                                    Address = theOrder.OrderDeliveryAddress.Address,
                                    Recipient = $"{theOrder.Customer.FirstName} {theOrder.Customer.LastName}",
                                    CreatedDate = DateTime.Now
                                }
                        },
                        CreatedDate = DateTime.Now
                    },
                    CurrencyId = "TL",
                    LanguageId = "TR",
                    OrderTypeId = theOrder.OrderTypeId,
                    OrderSourceId = theOrder.OrderSourceId,
                    OrderShipments = new List<Domain.Entities.OrderShipment>
                    {
                            new Domain.Entities.OrderShipment
                            {
                                TrackingUrl = theOrder.OrderShipment.TrackingUrl,
                                TrackingCode = theOrder.OrderShipment.TrackingCode,
                                TrackingBase64 = theOrder.OrderShipment.TrackingBase64,
                                ShipmentCompanyId = _shipmentCompany.Id,
                                CreatedDate = DateTime.Now,
                                Payor = theOrder.OrderShipment.Payor,
                                PackageNumber = theOrder.OrderShipment.PackageNumber
                            }
                    },
                    ListTotal = theOrder.ListTotalAmount,
                    Total = theOrder.TotalAmount,
                    VatExcTotal = theOrder.VatExcTotal,
                    VatExcDiscount = theOrder.VatExcDiscount,
                    VatExcListTotal = theOrder.VatExcListTotal,
                    Discount = theOrder.Discount,
                    CreatedDate = DateTime.Now,
                    CargoFee = theOrder.CargoFee,
                    SurchargeFee = 0,
                    OrderDeliveryAddress = new Domain.Entities.OrderDeliveryAddress
                    {
                        Recipient = $"{theOrder.OrderDeliveryAddress.FirstName} {theOrder.OrderDeliveryAddress.LastName}",
                        CreatedDate = DateTime.Now,
                        PhoneNumber = theOrder.OrderDeliveryAddress.Phone.ToNumber(),
                        Address = theOrder.OrderDeliveryAddress.Address,
                        NeighborhoodId = deliveryFindNeighborhoodId.Data
                    },
                    OrderInvoiceInformation = new OrderInvoiceInformation
                    {
                        CreatedDate = DateTime.Now,
                        NeighborhoodId = invoiceFindNeighborhoodId.Data,
                        Address = theOrder.OrderInvoiceAddress.Address,
                        Phone = theOrder.OrderInvoiceAddress.Phone.ToNumber(),
                        TaxNumber = theOrder.OrderInvoiceAddress.TaxNumber,
                        TaxOffice = theOrder.OrderInvoiceAddress.TaxOffice,
                        FirstName = theOrder.OrderInvoiceAddress.FirstName,
                        LastName = theOrder.OrderInvoiceAddress.LastName,
                        Mail = theOrder.OrderInvoiceAddress.Email
                    },
                    OrderTechnicInformation = new OrderTechnicInformation
                    {
                        Browser = "",
                        IpAddress = "",
                        Platform = "",
                    },
                    OrderDetails = orderDetails.Select(x => new ECommerce.Domain.Entities.OrderDetail
                    {
                        ListUnitPrice = x.ListPrice,
                        UnitDiscount = x.UnitDiscount,
                        ProductInformationId = x.Product.ProductInformationId,
                        Quantity = x.Quantity,
                        UnitCost = x.UnitCost,
                        UnitPrice = x.UnitPrice,
                        VatRate = Convert.ToDecimal(x.TaxRate),
                        CreatedDate = DateTime.Now,
                        Payor = x.Payor,
                        UnitCargoFee = theOrder.CargoFee > 0 ? theOrder.CargoFee / orderDetails.Sum(od => od.Quantity) : 0,
                        UnitCommissionAmount = x.UnitCommissionAmount,
                        VatExcUnitCost = x.VatExcUnitCost,
                        VatExcListUnitPrice = x.VatExcListUnitPrice,
                        VatExcUnitDiscount = x.VatExcUnitDiscount,
                        VatExcUnitPrice = x.VatExcUnitPrice
                    }).ToList(),
                    MarketplaceId = theOrder.MarketplaceId,
                    MarketplaceOrderNumber = theOrder.OrderCode,
                    CommissionAmount = theOrder.CommissionAmount,
                    OrderDate = theOrder.OrderDate,
                    EstimatedPackingDate = DateTime.Now,
                    Application = null
                };

                if (!string.IsNullOrEmpty(theOrder.OrderNote))
                {
                    orderEntity.OrderNotes = new List<OrderNote>
                    {
                            new OrderNote
                            {
                                CreatedDate = DateTime.Now,
                                Note = theOrder.OrderNote
                            }
                    };
                }

                created = unitOfWork.OrderRepository.Create(orderEntity);

                if (created)
                    theOrder.Id = orderEntity.Id;
                #endregion

            }

            return created;
        }

        #region Category Methods
        public Response GetCategories(string marketplaceId = null)
        {
            this.Loop((marketplace, marketplaceCompany) =>
            {
                if (!string.IsNullOrEmpty(marketplaceId) && marketplaceId != marketplaceCompany.MarketplaceId)
                    return;

                var configrations = marketplaceCompany.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value);

                var response = marketplace.GetCategories(configrations);
                if (response.Success)
                {
                    var eCommerceCategory = new List<ECommerceCategory>();

                    try
                    {
                        var _eCommerceCategories = this
                            ._unitOfWork
                            .ECommerceCategoryRepository
                            .DbSet()
                            .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)
                            .ToList();

                        eCommerceCategory = response
                            .Data
                            .Where(mpcLoop => !_eCommerceCategories.Any(y => y.Code == mpcLoop.Code))
                            .GroupBy(x => x.Code).Select(x => x.First())
                            .Select(mpcLoop => new ECommerce.Domain.Entities.Companyable.ECommerceCategory
                            {
                                MarketplaceId = marketplaceCompany.MarketplaceId,
                                Name = mpcLoop.Name,
                                Code = mpcLoop.Code,
                                ParentId = string.IsNullOrEmpty(mpcLoop.ParentCategoryCode)
                                    ? null
                                    : $"{marketplaceCompany.MarketplaceId}-{mpcLoop.ParentCategoryCode}"
                            })
                            .ToList();

                        _unitOfWork.ECommerceCategoryRepository.BulkInsert(eCommerceCategory);
                    }
                    catch (Exception e)
                    {
                    }

                    System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVarians Added");

                    try
                    {
                        var _earketplaceVariantIds = this
                            ._unitOfWork
                            .ECommerceVariantRepository
                            .DbSet()
                            .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)
                            .ToList();

                        var eCommerceVariants = response
                            .Data
                            .SelectMany(x => x.CategoryAttributes)
                            .OrderByDescending(x => x.Mandatory)
                            .GroupBy(x => new { x.Code, x.CategoryCode }).Select(x => x.First())
                            .Where(x => x.Code != null)
                            .Where(mpcLoop => !_earketplaceVariantIds.Any(y => y.Code == mpcLoop.Code && y.ECommerceCategoryCode == mpcLoop.CategoryCode))
                            .Select(caLoop => new ECommerce.Domain.Entities.Companyable.ECommerceVariant
                            {
                                Name = caLoop.Name,
                                Code = caLoop.Code,
                                ECommerceCategoryCode = caLoop.CategoryCode,
                                Variantable = caLoop.Variantable,
                                MarketplaceId = marketplaceCompany.MarketplaceId,
                                AllowCustom = caLoop.AllowCustom,
                                Mandatory = caLoop.Mandatory,
                                MultiValue = caLoop.MultiValue
                            }).ToList();

                        this._unitOfWork.ECommerceVariantRepository.BulkInsert(eCommerceVariants);
                    }
                    catch (Exception e)
                    {

                    }


                    System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVariantValues Added");



                    System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceCategoriesMarketplaceVariants Added");

                    var _marketplaceVariantsMarketplaceVariantValueIds = this
                        ._unitOfWork
                        .ECommerceVariantValueRepository
                        .DbSet()
                        .Where(x => x.MarketplaceId == marketplaceCompany.MarketplaceId)
                        .Select(x => new ECommerce.Domain.Entities.Companyable.ECommerceVariantValue
                        {
                            ECommerceVariantCode = x.ECommerceVariantCode,
                            Code = x.Code,
                        })
                        .ToList();

                    var marketplaceVariantsMarketplaceVariantValues = response.Data
                       .SelectMany(x => x.CategoryAttributes)
                       .SelectMany(x => x.CategoryAttributesValues)
                       .GroupBy(x => new { x.VarinatCode, x.Code })
                        .Where(x => x.Key.Code != null && x.Key.VarinatCode != null)
                         .Where(x => !_marketplaceVariantsMarketplaceVariantValueIds.Any(y =>
                            y.Code == x.Key.Code
                            && y.ECommerceVariantCode == x.Key.VarinatCode))
                       .Select(x => new ECommerce.Domain.Entities.Companyable.ECommerceVariantValue
                       {
                           ECommerceVariantCode = x.Key.VarinatCode,
                           Code = x.Key.Code,
                           MarketplaceId = marketplaceCompany.MarketplaceId,
                           Name = x.First().Name
                       }).ToList();

                    _unitOfWork.ECommerceVariantValueRepository.BulkInsert(marketplaceVariantsMarketplaceVariantValues);
                    System.Console.WriteLine($"{marketplaceCompany.MarketplaceId} marketplaceVariantsMarketplaceVariantValues Added");




                }
            }, marketplaceId);
            return null;
        }
        #endregion
        private DataResponse<int> FindNeighborhoodId(IUnitOfWork unitOfWork, string cityName, string districtName, string neighborhoodName)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                #region Find Address
                cityName = cityName.Trim();

                if (cityName.ToLower() == "afyonkarahisar")
                    cityName = "Afyon";

                if (cityName.ReplaceChar().ToLower() == "adapazari")
                {
                    cityName = "sakarya";
                    districtName = "adapazari";
                }

                if (cityName.ReplaceChar().ToLower() == "icel")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }


                if (cityName.ReplaceChar().ToLower() == "Afyon")
                {
                    cityName = "Mersin";
                    districtName = "icel";
                }

                var city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == cityName.ReplaceChar());

                if (city == null)
                {
                    city = unitOfWork.CityRepository.DbSet().Where(x => x.CountryId == 1).FirstOrDefault();
                }

                if (districtName == "Eyüpsultan" && city.Id == 1)
                    districtName = "Eyüp";

                else if (districtName == "Kahramankazan" && city.Id == 8)
                    districtName = "Kazan";

                else if (districtName == "Merkez" && city.Id == 32)
                    districtName = "Palandöken";

                else if (districtName == "Merkez" && city.Id == 26)
                    districtName = "Merkezefendi";

                else if (districtName == "Merkez" && city.Id == 63)
                    districtName = "Altınordu";

                else if (districtName == "Merkez" && city.Id == 58)
                    districtName = "Yenişehir";

                else if (districtName == "Merkez" && city.Id == 42)
                    districtName = "Onikişubat";

                else if (districtName == "Mustafa k. paşa" && city.Id == 22)
                    districtName = "Mustafakemalpaşa";

                else if (districtName == "Marmara Ereğlisi" && city.Id == 73)
                    districtName = "Marmaraereğlisi";

                else if (districtName == "Beşikdözü" && city.Id == 75)
                    districtName = "Beşikdüzü";

                else if (districtName == "Merkez" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Lara Kundu" && city.Id == 9)
                    districtName = "Aksu";

                else if (districtName == "Merkez" && city.Id == 71)
                    districtName = "Karaköprü";

                else if (districtName == "Merkez" && city.Id == 73)
                    districtName = "Süleymanpaşa";

                else if (districtName == "Merkez" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 78)
                    districtName = "Tuşba";

                else if (districtName == "Merkez" && city.Id == 12)
                    districtName = "Efeler";

                else if (districtName == "Merkez" && city.Id == 56)
                    districtName = "Yunusemre";

                else if (districtName == "Merkez" && city.Id == 55)
                    districtName = "Battalgazi";

                else if (districtName == "Merkez" && city.Id == 33)
                    districtName = "Odunpazarı";

                else if (districtName == "Yenişehir" && city.Id == 57)
                    districtName = "Artuklu";

                else if (districtName == "Merkez" && city.Id == 59)
                    districtName = "Menteşe";

                else if (districtName == "Merkez" && city.Id == 75)
                    districtName = "Ortahisar";

                else if (districtName == "Baraj Yolu" && city.Id == 2)
                    districtName = "Seyhan";

                else if (districtName == "Merkez" && city.Id == 34)
                    districtName = "Şahinbey";

                else if (districtName == "Merkez" && city.Id == 41)
                    districtName = "Konak";

                else if (districtName == "Ondokuz Mayıs" && city.Id == 67)
                    districtName = "19 Mayıs";

                else if (districtName == "Merkez" && city.Id == 67)
                    districtName = "İlkadım";

                else if (districtName == "Kuruçaşile" && city.Id == 14)
                    districtName = "Kurucaşile";

                else if (districtName == "İçel" && city.Id == 58)
                    districtName = "Silifke";

                else if (districtName == "Ofis" && city.Id == 27)
                    districtName = "Yenişehir";

                else if (districtName == "Gördeş" && city.Id == 56)
                    districtName = "Gördes";

                else if (districtName == "Sultanhanı" && city.Id == 6)
                    districtName = "Merkez";

                else if (districtName == "Merkez" && city.Id == 22)
                    districtName = "Osmangazi";

                else if (districtName == "Merkez" && city.Id == 66)
                    districtName = "Adapazarı";

                var district = unitOfWork.DistrictRepository.DbSet().Where(x => x.CityId == city.Id).ToList().FirstOrDefault(x => x.Name.ReplaceChar() == districtName.ReplaceChar());


                if (district == null)
                {
                    district = unitOfWork.DistrictRepository.DbSet().FirstOrDefault(x => x.CityId == city.Id);
                }

                var _neighborhoodName = neighborhoodName.ReplaceChar().Split(new string[] { "mah" }, StringSplitOptions.TrimEntries)[0];

                var neighborhood = unitOfWork.NeighborhoodRepository.DbSet()
                    .Where(x => x.DistrictId == district.Id)
                    .ToList()
                    .FirstOrDefault(x => x.Name.ReplaceChar() == $"{_neighborhoodName} mah");
                if (neighborhood == null)
                {
                    neighborhood = unitOfWork.NeighborhoodRepository.DbSet().FirstOrDefault(x => x.DistrictId == district.Id);
                }

                response.Data = neighborhood.Id;
                response.Success = true;
                #endregion

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        private void Loop(Action<IMarketplace, MarketplaceCompany> action, string marketplaceId = null)
        {
            FillMarketplaceCompanies(marketplaceId);

            this._marketplaceCompanies.ForEach(theMarketplaceCompany =>
            {
                IMarketplace marketplace = this._marketplaceResolver(theMarketplaceCompany.MarketplaceId);
                if (marketplace != null && theMarketplaceCompany.MarketplaceConfigurations != null)
                {
                    marketplace.BindConfiguration(theMarketplaceCompany.MarketplaceConfigurations);
                    action(marketplace, theMarketplaceCompany);
                }
            });
        }

        private void ParallelLoop(Action<IMarketplace, MarketplaceCompany> action, string marketplaceId = null)
        {
            FillMarketplaceCompanies(marketplaceId);

            Parallel.ForEach(this._marketplaceCompanies, theMarketplaceCompany =>
            {
                IMarketplace marketplace = this._marketplaceResolver(theMarketplaceCompany.MarketplaceId);
                if (marketplace != null && theMarketplaceCompany.MarketplaceConfigurations != null)
                {
                    marketplace.BindConfiguration(theMarketplaceCompany.MarketplaceConfigurations);
                    action(marketplace, theMarketplaceCompany);
                }
            });
        }



        #endregion
    }
}