﻿using ECommerce.Application.Common.Interfaces.Helpers;
using System;

namespace ECommerce.Persistence.Common.Helpers
{
    public class RandomHelper : IRandomHelper
    {
        public string GenerateNumbers(int min, int max)
        {
            var random = new Random();
            string value = random.Next(min, max).ToString();

            return value;
        }
    }
}
