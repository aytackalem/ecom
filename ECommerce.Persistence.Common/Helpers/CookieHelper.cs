﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Helpers;
using Helpy.Shared.Crypto;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ECommerce.Persistence.Common.Helpers
{
    public class CookieHelper : ICookieHelper
    {
        #region Fields
        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public CookieHelper(IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public List<KeyValue<string, string>> Read(string name)
        {
            var cookie = this._httpContextAccessor.HttpContext.Request.Cookies[name];

            var response = CryptoHelper.Decrypt(cookie);

            return JsonConvert.DeserializeObject<List<KeyValue<string, string>>>(response);
        }

        public void Write(string name, List<KeyValue<string, string>> keyValues, DateTime? expires = null)
        {
            var response = CryptoHelper.Encrypt(JsonConvert.SerializeObject(keyValues));

            var secure = false;

#if !DEBUG
                     secure = true;
#endif
            var cookieOptions = new CookieOptions
            {
                Secure = secure,
                HttpOnly = true
            };

            if (expires.HasValue)
                cookieOptions.Expires = new System.DateTimeOffset(expires.Value);

            this._httpContextAccessor.HttpContext.Response.Cookies.Append(name, response, cookieOptions);
        }

        public void Delete(string name)
        {
            this._httpContextAccessor.HttpContext.Response.Cookies.Delete(name);
        }

        public bool Exist(string name)
        {
            return !string.IsNullOrEmpty(this._httpContextAccessor.HttpContext.Request.Cookies[name]);
        }
        #endregion
    }
}
