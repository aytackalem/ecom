﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.Helpers
{
    public class HttpHelper : IHttpHelper
    {
        #region Constructor
        public HttpHelper()
        {
        }
        #endregion

        #region Method
        public TResponse PostForm<TResponse>(string request, string url, string authType, string token, string accept) where TResponse : class
        {
            try
            {
                using (var client = new HttpClient())
                {
                    if (!string.IsNullOrEmpty(authType) && !string.IsNullOrEmpty(token))
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                    if (!string.IsNullOrEmpty(accept))
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    client.DefaultRequestHeaders.Add("User-Agent", "helpy_dev");


                    using (var content = new MultipartFormDataContent("------" + DateTime.Now.Ticks.ToString("x")))
                    {

                        var streamContent = new StreamContent(GenerateStreamFromString(request));

                        content.Add(streamContent, "file", "product.json");
                        content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            FileName = "product.json",
                            Name = "file"
                        };

                        using HttpResponseMessage res = client.PostAsync(url, content).Result;
                        string responseString = res.Content.ReadAsStringAsync().Result;
                        if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                        return JsonConvert.DeserializeObject<TResponse>(responseString);
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public TResponse Post<TRequest, TResponse>(TRequest request, string url, string authType, string token, bool? log, string logFileName = null) where TRequest : class where TResponse : class
        {
            try
            {
                string requestString = JsonConvert.SerializeObject(request);
                var guid = Guid.NewGuid().ToString();

                if (log.HasValue && log.Value)
                {
                    var path = @$"C:\Request\";
                    System.IO.Directory.CreateDirectory(path);
                    System.IO.File.WriteAllText(@$"{path}\{logFileName ?? guid + ".txt"}", requestString);
                }


                using HttpClient client = new();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);


                client.DefaultRequestHeaders.Add("User-Agent", "helpy_dev");

                HttpRequestMessage httpRequestMessage = new(HttpMethod.Post, url);
                httpRequestMessage.Content = new StringContent(requestString, Encoding.UTF8, "application/json");

                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.SendAsync(httpRequestMessage).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;

                if (log.HasValue && log.Value)
                {
                    var path = @$"C:\Responses\";
                    System.IO.Directory.CreateDirectory(path);
                    System.IO.File.WriteAllText(@$"{path}\{logFileName ?? Guid.NewGuid().ToString() + ".txt"}", responseString);
                }

                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TResponse Post<TResponse>(string url, string authType, string token) where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);


                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PostAsync(url, null).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Post(string url, string body, string authType, string token)
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

            }
            catch (Exception ex)
            {
            }
        }

        public TResponse Post<TResponse>(string url, List<KeyValuePair<string, string>> data) where TResponse : class
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromDays(1);
                    using HttpResponseMessage res = client.PostAsync(url, new FormUrlEncodedContent(data)).Result;
                    using HttpContent content = res.Content;
                    string responseString = content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<TResponse>(responseString);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TResponse Post<TRequest, TResponse>(TRequest request, string url, string authType, string token, Dictionary<string, string> customHeaders) where TRequest : class where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                foreach (var chLoop in customHeaders)
                    client.DefaultRequestHeaders.Add(chLoop.Key, chLoop.Value);

                string requestString = JsonConvert.SerializeObject(request);
                var contentData = new StringContent(requestString, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PostAsync(url, contentData).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TResponse Get<TResponse>(string url, string authType, string token) where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();
                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                client.DefaultRequestHeaders.Add("User-Agent", "helpy_dev");

                HttpResponseMessage res = client.GetAsync(url).Result;
                HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TResponse Get<TResponse>(string url, string authType, string token, Dictionary<string, string> customHeaders) where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();
                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                foreach (var chLoop in customHeaders)
                    client.DefaultRequestHeaders.Add(chLoop.Key, chLoop.Value);

                HttpResponseMessage res = client.GetAsync(url).Result;
                HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TResponse Put<TRequest, TResponse>(TRequest request, string url, string authType, string token) where TRequest : class where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                string requestString = JsonConvert.SerializeObject(request);
                var contentData = new StringContent(requestString, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PutAsync(url, contentData).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }










        public TResponse Put<TRequest, TResponse>(TRequest request, string url, string authType, string token, Dictionary<string, string> customHeaders) where TRequest : class where TResponse : class
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                foreach (var chLoop in customHeaders)
                    client.DefaultRequestHeaders.Add(chLoop.Key, chLoop.Value);


                string requestString = JsonConvert.SerializeObject(request);
                var contentData = new StringContent(requestString, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PutAsync(url, contentData).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(responseString)) throw new Exception("RESPONSE-EMPTY");

                return JsonConvert.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Put<TRequest>(TRequest request, string url, string authType, string token) where TRequest : class
        {
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                string requestString = JsonConvert.SerializeObject(request);
                var contentData = new StringContent(requestString, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PutAsync(url, contentData).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;

                return res.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public void Post(string url, string authType, string token)
        //{
        //    try
        //    {
        //        using HttpClient client = new HttpClient();

        //        if (!string.IsNullOrEmpty(authType))
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

        //        client.Timeout = TimeSpan.FromDays(1);
        //        using HttpResponseMessage res = client.PostAsync(url, null).Result;
        //        using HttpContent content = res.Content;
        //        string responseString = content.ReadAsStringAsync().Result;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        public bool Post(string url, string authType, string token)
        {
            var posted = false;
            try
            {
                using HttpClient client = new HttpClient();

                if (!string.IsNullOrEmpty(authType))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authType, token);

                client.Timeout = TimeSpan.FromDays(1);
                using HttpResponseMessage res = client.PostAsync(url, null).Result;
                using HttpContent content = res.Content;
                string responseString = content.ReadAsStringAsync().Result;

                posted = res.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
            }
            return posted;
        }
        #endregion

        #region Helper Method
        public Stream GenerateStreamFromString(string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
    }
}
