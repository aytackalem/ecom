﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Parameters.HttpHelperV3.Base;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using Microsoft.AspNetCore.Http;
using Microsoft.Graph;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.Helpers
{
    public class HttpHelperV3 : IHttpHelperV3
    {
        #region Methods
        public async Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3Request request)
        {
            HttpHelperV3Response<TContent> response = new();
            await this.Template(request, async httpClient =>
            {
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri));

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    WriteErrorLog(request.RequestUri, string.Empty, responseJson, httpResponseMessage.StatusCode.ToString());
                }

                if (httpResponseMessage.IsSuccessStatusCode)
                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
            });
            return response;
        }

        public async Task<HttpHelperV3Response<TContent>> SendAsync<TRequest, TContent>(HttpHelperV3Request<TRequest> request)
        {
            HttpHelperV3Response<TContent> response = new();
            await this.Template(request, async httpClient =>
            {

                var requestJson = JsonConvert.SerializeObject(request.Request);
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new StringContent(
                            requestJson,
                            Encoding.UTF8,
                         string.IsNullOrEmpty(request.ContentType) ? "application/json" : request.ContentType)
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    WriteErrorLog(request.RequestUri, requestJson, responseJson, httpResponseMessage.StatusCode.ToString());
                }

                response.ContentString = responseJson;

                if (httpResponseMessage.IsSuccessStatusCode)
                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
            });
            return response;
        }

        public async Task<HttpHelperV3Response<TOkContent, TBadRequestContent>> SendAsync<TRequest, TOkContent, TBadRequestContent>(HttpHelperV3Request<TRequest> request)
        {
            HttpHelperV3Response<TOkContent, TBadRequestContent> response = new();
            await this.Template(request, async httpClient =>
            {
                var requestJson = JsonConvert.SerializeObject(request.Request);
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new StringContent(
                            requestJson,
                            Encoding.UTF8,
                            "application/json")
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    WriteErrorLog(request.RequestUri, requestJson, responseJson, httpResponseMessage.StatusCode.ToString());
                }

                response.ContentString = responseJson;

                if (httpResponseMessage.IsSuccessStatusCode)
                    response.Content = JsonConvert.DeserializeObject<TOkContent>(responseJson);
                else if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    response.ContentError = JsonConvert.DeserializeObject<TBadRequestContent>(responseJson);
            });
            return response;
        }

        public async Task<HttpHelperV3Response> SendAsync<TRequest>(HttpHelperV3Request<TRequest> request)
        {
            HttpHelperV3Response response = new();
            await this.Template(request, async httpClient =>
            {
                var requestJson = JsonConvert.SerializeObject(request.Request);
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new StringContent(
                            requestJson,
                            Encoding.UTF8,
                            "application/json")
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    WriteErrorLog(request.RequestUri, requestJson, responseJson, httpResponseMessage.StatusCode.ToString());
                }
            });
            return response;
        }

        public async Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3JsonFileRequest request)
        {
            HttpHelperV3Response<TContent> response = new();
            await this.Template(request, async httpClient =>
            {
                using (MultipartFormDataContent content = new("------" + DateTime.Now.Ticks.ToString("x")))
                {
                    StreamContent streamContent = new(GenerateStreamFromString(request.Request));

                    content.Add(streamContent, "file", request.FileName);
                    content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        FileName = request.FileName,
                        Name = "file"
                    };

                    var httpRequestMessage = new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                    {
                        Content = content
                    };
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

                    string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                    response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
                    response.HttpStatusCode = httpResponseMessage.StatusCode;
                }
            });
            return response;
        }

        public async Task<HttpHelperV3Response<TContent>> SendAsync<TContent>(HttpHelperV3FormUrlEncodedRequest request) where TContent : class
        {
            HttpHelperV3Response<TContent> response = new();
            await this.Template(request, async httpClient =>
            {
                var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(request.HttpMethod, request.RequestUri)
                {
                    Content = new FormUrlEncodedContent(request.Data)
                });

                response.HttpStatusCode = httpResponseMessage.StatusCode;

                string responseJson = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    WriteErrorLog(request.RequestUri, string.Empty, responseJson, httpResponseMessage.StatusCode.ToString());
                }

                if (httpResponseMessage.IsSuccessStatusCode)
                    if (typeof(TContent) == typeof(string))
                        response.Content = responseJson as TContent;
                    else
                        response.Content = JsonConvert.DeserializeObject<TContent>(responseJson);
            });
            return response;
        }
        #endregion

        #region Helper Methods
        private async Task Template(HttpHelperV3RequestBase request, Func<HttpClient, Task> func)
        {
            using (var httpClient = new HttpClient())
            {
                if (request.AuthorizationType.HasValue)
                    switch (request.AuthorizationType.Value)
                    {
                        case AuthorizationType.Basic:
                            httpClient.DefaultRequestHeaders.Authorization = new(request.AuthorizationType.ToString(),
                                                                                 request.Authorization);
                            break;
                        case AuthorizationType.Bearer:
                            httpClient.DefaultRequestHeaders.Authorization = new(request.AuthorizationType.ToString(),
                                                                                 request.Authorization);
                            break;
                        case AuthorizationType.XApiKey:
                            httpClient.DefaultRequestHeaders.Add("x-api-key", request.Authorization);
                            break;
                    }

                if (string.IsNullOrEmpty(request.Agent) == false)
                    httpClient.DefaultRequestHeaders.Add("User-Agent", request.Agent);

                if (string.IsNullOrEmpty(request.Accept) == false)
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(request.Accept));

                if (request.Headers?.Count > 0)
                    foreach (var theHeader in request.Headers)
                        httpClient.DefaultRequestHeaders.Add(theHeader.Key, theHeader.Value);

                await func(httpClient);
            }
        }

        private Stream GenerateStreamFromString(string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private void WriteErrorLog(string url, string request, string response, string code)
        {
            System.IO.File.WriteAllText(@$"C:\Logs\Processor\Errors\{Guid.NewGuid()}.txt", $"Url: {url}\nRequest: {request}\nResponse: {response}\nCode: {code}");
        }
        #endregion
    }
}
