﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace ECommerce.Persistence.Common.Helpers
{
    public class LocalizationHelper : ILocalizationHelper
    {
        #region Fields
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly ISettingService _settingService;

        private readonly ICookieHelper _cookieHelper;
        #endregion

        #region Constructors
        public LocalizationHelper(IHttpContextAccessor httpContextAccessor, ISettingService settingService, ICookieHelper cookieHelper)
        {
            #region Fields
            this._httpContextAccessor = httpContextAccessor;
            this._settingService = settingService;
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public Currency GetCurrency()
        {
            var currencyId = this._settingService.DefaultCurrencyId;

            if (this._cookieHelper.Exist("Currency"))
                currencyId = this._cookieHelper.Read("Currency")[0].Value;

            return new Currency { Id = currencyId };
        }

        public Language GetLanguage()
        {
            var languageId = this._settingService.DefaultLanguageId;

            var routeValue = this._httpContextAccessor.HttpContext.GetRouteValue("l");
            if (routeValue != null)
                languageId = routeValue.ToString().ToUpper();

            return new Language { Id = languageId };
        }
        #endregion
    }
}
