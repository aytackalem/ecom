﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Common.Helpers
{
    public class ImageHelper : IImageHelper
    {
        #region Methods
        public bool Save(string base64, string fileName)
        {
            var success = false;

            try
            {
                var bytes = Convert.FromBase64String(base64.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)[1]);
                using (var memoryStream = new MemoryStream(bytes))
                {
                    using (var image = Image.FromStream(memoryStream))
                    {
                        image.Save(fileName);
                        success = true;
                    }
                }
            }
            catch
            {
            }

            return success;
        }

        public string Resize(string base64)
        {
            using (var image = Bitmap.FromStream(new MemoryStream(Convert.FromBase64String(base64.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)[1]))))
            {
                int sourceWidth = image.Width;
                int sourceHeight = image.Height;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                //Calulate  width with new desired size  
                nPercentW = ((float)1200 / (float)sourceWidth);
                //Calculate height with new desired size  
                nPercentH = ((float)1600 / (float)sourceHeight);
                if (nPercentH < nPercentW)
                    nPercent = nPercentH;
                else
                    nPercent = nPercentW;
                //New Width  
                int destWidth = (int)(sourceWidth * nPercent);
                //New Height  
                int destHeight = (int)(sourceHeight * nPercent);
                Bitmap b = new Bitmap(destWidth, destHeight);
                //b.SetResolution(72F, 72F);
                Graphics g = Graphics.FromImage(b);
                g.InterpolationMode = InterpolationMode.Default;
                g.PixelOffsetMode = PixelOffsetMode.Default;
                g.SmoothingMode = SmoothingMode.Default;
                g.CompositingMode = CompositingMode.SourceCopy;
                // Draw image with new width and height  
                g.DrawImage(image, 0, 0, destWidth, destHeight);
                g.Dispose();

                MemoryStream memoryStream = new MemoryStream();
                b.Save(memoryStream, ImageFormat.Jpeg);
                var bytes = memoryStream.ToArray();

                return Convert.ToBase64String(bytes);
            }
        }

        public string Resize(IFormFile formFile)
        {
            using (var image = Image.FromStream(formFile.OpenReadStream()))
            {
                if (image.PropertyIdList.Contains(0x0112))
                {
                    int rotationValue = image.GetPropertyItem(0x0112).Value[0];
                    switch (rotationValue)
                    {
                        case 1: // landscape, do nothing
                            break;

                        case 8: // rotated 90 right
                                // de-rotate:
                            image.RotateFlip(rotateFlipType: RotateFlipType.Rotate270FlipNone);
                            break;

                        case 3: // bottoms up
                            image.RotateFlip(rotateFlipType: RotateFlipType.Rotate180FlipNone);
                            break;

                        case 6: // rotated 90 left
                            image.RotateFlip(rotateFlipType: RotateFlipType.Rotate90FlipNone);
                            break;
                    }
                }
                

                //float b = 1.1F;
                //ColorMatrix cm = new ColorMatrix(new float[][]
                //    {
                //    new float[] {b, 0, 0, 0, 0},
                //    new float[] {0, b, 0, 0, 0},
                //    new float[] {0, 0, b, 0, 0},
                //    new float[] {0, 0, 0, 1, 0},
                //    new float[] {0, 0, 0, 0, 1},
                //    });
                //ImageAttributes attributes = new ImageAttributes();
                //attributes.SetColorMatrix(cm);

                Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);

                Bitmap bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format24bppRgb);
                bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                Graphics graphics = Graphics.FromImage(bitmap);
                graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
                graphics.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel/*, attributes*/);
                graphics.Dispose();

                MemoryStream memoryStream = new MemoryStream();
                bitmap.Save(memoryStream, ImageFormat.Jpeg);
                var bytes = memoryStream.ToArray();

                return Convert.ToBase64String(bytes);
            }
        }

        //public bool SaveCompressed(string base64, int imageQuality, string fileName)
        //{
        //    var success = false;

        //    try
        //    {
        //        using (var image = Image.FromStream(new MemoryStream(Convert.FromBase64String(base64.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)[1]))))
        //        {
        //            int sourceWidth = image.Width;
        //            int sourceHeight = image.Height;

        //            float nPercent = 0;
        //            float nPercentW = 0;
        //            float nPercentH = 0;
        //            //Calulate  width with new desired size  
        //            nPercentW = ((float)1200 / (float)sourceWidth);
        //            //Calculate height with new desired size  
        //            nPercentH = ((float)1600 / (float)sourceHeight);
        //            if (nPercentH < nPercentW)
        //                nPercent = nPercentH;
        //            else
        //                nPercent = nPercentW;
        //            //New Width  
        //            int destWidth = (int)(sourceWidth * nPercent);
        //            //New Height  
        //            int destHeight = (int)(sourceHeight * nPercent);
        //            Bitmap b = new Bitmap(destWidth, destHeight);
        //            //b.SetResolution(72F, 72F);
        //            Graphics g = Graphics.FromImage(b);
        //            g.InterpolationMode = InterpolationMode.Default;
        //            g.PixelOffsetMode = PixelOffsetMode.Default;
        //            g.SmoothingMode = SmoothingMode.Default;
        //            g.CompositingMode = CompositingMode.SourceCopy;
        //            // Draw image with new width and height  
        //            g.DrawImage(image, 0, 0, destWidth, destHeight);
        //            g.Dispose();




        //            ImageCodecInfo jpegCodec = null;

        //            //Set quality factor for compression
        //            EncoderParameter imageQualitysParameter = new EncoderParameter(Encoder.Quality, imageQuality);

        //            //List all avaible codecs (system wide)
        //            ImageCodecInfo[] alleCodecs = ImageCodecInfo.GetImageEncoders();

        //            EncoderParameters codecParameter = new EncoderParameters(1);
        //            codecParameter.Param[0] = imageQualitysParameter;

        //            //Find and choose JPEG codec
        //            for (int i = 0; i < alleCodecs.Length; i++)
        //            {
        //                if (alleCodecs[i].MimeType == "image/jpeg")
        //                {
        //                    jpegCodec = alleCodecs[i];
        //                    break;
        //                }
        //            }

        //            b.Save(fileName, jpegCodec, codecParameter);
        //        }
        //    }
        //    catch
        //    {
        //    }

        //    return success;
        //}
        #endregion
    }
}
