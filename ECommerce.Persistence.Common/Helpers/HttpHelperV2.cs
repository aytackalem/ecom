﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Parameters.HttpHelperV3;
using ECommerce.Application.Common.Wrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.Helpers
{
    public class HttpHelperV2 : IHttpHelperV2
    {
        #region Methods
        public async Task<HttpResultResponse<string>> SendAsync(string requestUri, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null, string agent = null, bool? log = null, string logFileName = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<string>>(async response =>
            {

                 using (var httpClient = new HttpClient())
                    {
                        if (authorizationType.HasValue)
                        {
                            switch (authorizationType.Value)
                            {
                                case AuthorizationType.Basic:
                                    httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                    break;
                                case AuthorizationType.Bearer:
                                    httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                    break;
                                case AuthorizationType.XApiKey:
                                    httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(agent))
                            httpClient.DefaultRequestHeaders.Add("User-Agent", agent);


                        var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri));
                        response.RequestUri = requestUri;
                        response.HttpStatusCode = httpResponseMessage.StatusCode;

                        if (httpResponseMessage.IsSuccessStatusCode == false)
                            return;
                        else
                            response.Success = true;

                        response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                        if (log.HasValue && log.Value)
                        {
                            var path = @$"C:\Responses\";
                            System.IO.Directory.CreateDirectory(path);
                            await System.IO.File.WriteAllTextAsync(@$"{path}\{logFileName ?? Guid.NewGuid().ToString() + ".txt"}", response.ContentString);
                        }

                        if (response.ContentIsNullOrEmpty == true)
                            return;


                    }
               
            });
        }

        public async Task<HttpResultResponse<string>> SendJsonAsync<TRequestObject>(string requestUri, TRequestObject requestObject, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<string>>(async response =>
            {
                using (var httpClient = new HttpClient())
                {
                    if (authorizationType.HasValue)
                    {
                        switch (authorizationType.Value)
                        {
                            case AuthorizationType.Basic:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.Bearer:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.XApiKey:
                                httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                break;
                        }
                    }

                    var requestObjectJson = JsonConvert.SerializeObject(requestObject);

                    System.IO.File.WriteAllText(@$"C:\Queues\1\IK\Jsons\{Guid.NewGuid()}.json", requestObjectJson);

                    var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri)
                    {
                        Content = new StringContent(
                            requestObjectJson,
                            Encoding.UTF8,
                            "application/json")
                    });

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        response.Message = await httpResponseMessage.Content.ReadAsStringAsync();
                        return;
                    }
                    else
                        response.Success = true;

                    response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                    if (response.ContentIsNullOrEmpty == true)
                        return;

                    response.ContentObject = response.ContentString;
                }
            });
        }

        public async Task<HttpResultResponse<TContentObject>> SendJsonAsync<TRequestObject, TContentObject>(string requestUri, TRequestObject requestObject, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<TContentObject>>(async response =>
            {
                using (var httpClient = new HttpClient())
                {
                    if (authorizationType.HasValue)
                    {
                        switch (authorizationType.Value)
                        {
                            case AuthorizationType.Basic:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.Bearer:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.XApiKey:
                                httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                break;
                        }
                    }

                    httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("(Helpy)"));

                    var requestObjectJson = JsonConvert.SerializeObject(requestObject);
                    var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri)
                    {
                        Content = new StringContent(
                            requestObjectJson,
                            Encoding.UTF8,
                            "application/json")
                    });

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        response.Message = await httpResponseMessage.Content.ReadAsStringAsync();
                        return;
                    }
                    else
                        response.Success = true;

                    response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                    if (response.ContentIsNullOrEmpty == true)
                        return;

                    response.ContentObject = JsonConvert.DeserializeObject<TContentObject>(response.ContentString);
                }
            });
        }

        public async Task<HttpResultResponse<TContentObject>> SendJsonAsync<TContentObject>(string requestUri, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<TContentObject>>(async response =>
            {
                using (var httpClient = new HttpClient())
                {
                    if (authorizationType.HasValue)
                    {
                        switch (authorizationType.Value)
                        {
                            case AuthorizationType.Basic:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.Bearer:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.XApiKey:
                                httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                break;
                        }
                    }

                    var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri));

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    if (httpResponseMessage.IsSuccessStatusCode == false)
                        return;
                    else
                        response.Success = true;

                    response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                    if (response.ContentIsNullOrEmpty == true)
                        return;

                    response.ContentObject = JsonConvert.DeserializeObject<TContentObject>(response.ContentString);
                }
            });
        }

        public async Task<HttpResultResponse<TContentObject>> SendJsonFileAsync<TContentObject>(string requestUri, string requestObject, string fileName, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<TContentObject>>(async response =>
            {
                using (var httpClient = new HttpClient())
                {
                    if (authorizationType.HasValue)
                    {
                        switch (authorizationType.Value)
                        {
                            case AuthorizationType.Basic:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.Bearer:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.XApiKey:
                                httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                break;
                        }
                    }

                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage httpResponseMessage = null;

                    MultipartFormDataContent content = null;
                    using (content = new MultipartFormDataContent("------" + DateTime.Now.Ticks.ToString("x")))
                    {
                        var streamContent = new StreamContent(GenerateStreamFromString(requestObject));

                        content.Add(streamContent, "file", fileName);
                        content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            FileName = fileName,
                            Name = "file"
                        };

                        httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri)
                        {
                            Content = content
                        });
                    }

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    if (httpResponseMessage.IsSuccessStatusCode == false)
                        return;
                    else
                        response.Success = true;

                    response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                    if (response.ContentIsNullOrEmpty == true)
                        return;

                    response.ContentObject = JsonConvert.DeserializeObject<TContentObject>(response.ContentString);
                }
            });
        }

        public async Task<HttpResultResponse<TContentObject>> SendFormUrlEncodedAsync<TContentObject>(string requestUri, List<KeyValuePair<string, string>> formItems, HttpMethod httpMethod, AuthorizationType? authorizationType = null, string authorization = null)
        {
            return await ExceptionHandlerV2.ResponseHandleAsync<HttpResultResponse<TContentObject>>(async response =>
            {
                using (var httpClient = new HttpClient())
                {
                    if (authorizationType.HasValue)
                    {
                        switch (authorizationType.Value)
                        {
                            case AuthorizationType.Basic:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.Bearer:
                                httpClient.DefaultRequestHeaders.Authorization = new(authorizationType.ToString(), authorization);
                                break;
                            case AuthorizationType.XApiKey:
                                httpClient.DefaultRequestHeaders.Add("x-api-key", authorization);
                                break;
                        }
                    }

                    var httpResponseMessage = await httpClient.SendAsync(new HttpRequestMessage(httpMethod, requestUri)
                    {
                        Content = new FormUrlEncodedContent(formItems)
                    });

                    response.HttpStatusCode = httpResponseMessage.StatusCode;

                    if (httpResponseMessage.IsSuccessStatusCode == false)
                        return;
                    else
                        response.Success = true;

                    response.ContentString = await httpResponseMessage.Content.ReadAsStringAsync();

                    if (response.ContentIsNullOrEmpty == true)
                        return;

                    response.ContentObject = JsonConvert.DeserializeObject<TContentObject>(response.ContentString);
                }
            });
        }
        #endregion

        #region Helper Method
        private Stream GenerateStreamFromString(string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
    }
}
