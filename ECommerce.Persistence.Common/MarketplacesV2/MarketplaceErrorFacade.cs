﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Persistence.Common.Helpers;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2
{
    public class MarketplaceErrorFacade : IMarketplaceErrorFacade
    {
        #region Methods
        public async Task SendNotification(MarketplaceRequestNotification notification)
        {
            var subject = $"Error! {notification.JobName} - {notification.Command}";
            var text = $"{notification.ErrorText} QId: {notification.QueueId}. MId: {notification.MarketplaceId}.";
            if (string.IsNullOrEmpty(notification.RequestId) == false)
                text += $" RId: {notification.RequestId}.";
            if (notification.TenantId > 0)
                text += $" TId: {notification.TenantId}.";
            if (notification.DomainId > 0)
                text += $" DId: {notification.DomainId}.";
            if (notification.CompanyId > 0)
                text += $" CId: {notification.CompanyId}.";

            /* Only Mail */
            Task[] tasks = new Task[1];
            tasks[0] = Task.Run(() => SystemAlertHelper.SendMail(subject, text, notification.Attachments));
            await Task.WhenAll(tasks);

            /* Mail And Sms */
            //Task[] tasks = new Task[2];
            //tasks[0] = Task.Run(() => SystemAlertHelper.SendMail(subject, text));
            //tasks[1] = Task.Run(() => SystemAlertHelper.SendSms(text));
            //await Task.WhenAll(tasks);
        }

        public async Task SendNotification(MarketplaceRequestExceptionNotification notification)
        {
            var subject = $"Error! {notification.JobName} - {notification.Command}";
            var text = $"DbName: {notification.DbName}. Queue Id: {notification.QueueId}. Marketplace Id: {notification.MarketplaceId}. {notification.ErrorText}";

            Task[] tasks = new Task[1];
            tasks[0] = Task.Run(() => SystemAlertHelper.SendMail(subject, text));
            await Task.WhenAll(tasks);
        }

        public async Task SendNotification(MarketplaceRequestItemExceptionNotification notification)
        {
            var subject = $"Error! {notification.JobName} - {notification.Command}";
            var text = $"{notification.ErrorText} Queue Id: {notification.QueueId}. Request Id: {notification.RequestId}. Marketplace Id: {notification.MarketplaceId}.";

            Task[] tasks = new Task[1];
            tasks[0] = Task.Run(() => SystemAlertHelper.SendMail(subject, text));
            await Task.WhenAll(tasks);
        }
        #endregion
    }
}
