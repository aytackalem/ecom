﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Checker
{
    public class MarketplaceRequestCheckerCreateProduct : IMarketplaceRequestChecker
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2CreateProductTrackableResolver _marketplaceV2ProductTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestCheckerCreateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2CreateProductTrackableResolver marketplaceV2ProductTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2ProductTrackableResolver = marketplaceV2ProductTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task CheckAsync(MarketplaceRequestCheckerRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceCreateProductRequests = await this._marketplaceRequestReader.ReadAsync(new MarketplaceRequestReaderRequest<MarketplaceRequestCreateProduct>
                {
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId
                });

                if (marketplaceCreateProductRequests.HasItem() == false)
                    return;

                var marketplace = this._marketplaceV2ProductTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceCreateProductRequests.Count; i++)
                {
                    var marketplaceCreateProductRequest = marketplaceCreateProductRequests[i];

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.CheckAsync(marketplaceCreateProductRequest);

                        await this.NotificationAsync(marketplaceCreateProductRequest, httpHelperV3Response);

                        this.Update(marketplaceCreateProductRequest, httpHelperV3Response);

                        this.Delete(marketplaceCreateProductRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Checker",
                            Command = "Create Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceCreateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Checker",
                    Command = "Create Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {

            var _ = httpHelperV3Response
                .Content
                .TrackingResponseItems
                .Where(upri => upri.Messages.HasItem())
                .ToList();

            foreach (var theItem in _)
            {
                var marketplaceRequestCreateProductItem = marketplaceCreateProductRequest
                    .Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaces.Any(pim => pim.ProductInformationMarketplaceId == theItem.ProductInformationMarketplaceId));

                var productInformationMarketplace = marketplaceRequestCreateProductItem
                    .ProductInformationMarketplaces
                    .FirstOrDefault(pim => pim.ProductInformationMarketplaceId == theItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "CPR",
                    Title = "Ürün Yükleme",
                    ProductInformationMarketplaceId = theItem.ProductInformationMarketplaceId,
                    Message = string.Join(" ", theItem.Messages),
                    MarketplaceId = marketplaceCreateProductRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplaceCreateProductRequest.MarketplaceRequestHeader.CompanyId,
                    Barcode = theItem.Barcode,
                    StockCode = theItem.StockCode,
                    CreatedDateTime = DateTime.Now,
                    ProductId = marketplaceRequestCreateProductItem != null
                                        ? marketplaceRequestCreateProductItem.ProductId
                                        : 0,
                    PhotoUrl = productInformationMarketplace.ProductInformationPhotos.HasItem()
                                        ? productInformationMarketplace.ProductInformationPhotos.FirstOrDefault()?.Url
                                        : null,
                    ModelCode = marketplaceRequestCreateProductItem?.SellerCode,
                    ProductInformationId = productInformationMarketplace != null
                                        ? productInformationMarketplace.ProductInformationId
                                        : 0,
                    ProductInformationName = $"{productInformationMarketplace?.ProductInformationName} {productInformationMarketplace?.VariantValuesDescription}"
                });

            }

        }

        private void Update(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK &&
                httpHelperV3Response.Content.Done)
            {
                #region Update Product Information Marketplace UUId
                if (httpHelperV3Response.Content.IncludeUUId)
                    this
                        ._unitOfWork
                        .ProductInformationMarketplaceRepository
                        .UpdateUUId(httpHelperV3Response
                            .Content
                            .TrackingResponseItems
                            .Select(tri => new Application.Common.Parameters.ProductInformationMarketplaces.UpdateUUIdRequest
                            {
                                Id = tri.ProductInformationMarketplaceId,
                                UUId = tri.ProductInformationMarketplaceUUId
                            })
                            .ToList());
                #endregion

                #region Update Opened / Update Url
                if (httpHelperV3Response.Content.IncludeUrl)
                {
                    var productInformationMarketplaceIds = httpHelperV3Response
                        .Content
                        .TrackingResponseItems
                        .Where(upri => upri.Success)
                        .ToDictionary(upri => upri.ProductInformationMarketplaceId, upri => upri.Url);

                    if (productInformationMarketplaceIds.Any())
                        this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .UpdateOpenedUrl(productInformationMarketplaceIds, true);
                }
                else
                {
                    var productInformationMarketplaceIds = httpHelperV3Response
                        .Content
                        .TrackingResponseItems
                        .Where(upri => upri.Success)
                        .Select(upri => upri.ProductInformationMarketplaceId)
                        .ToList();

                    if (productInformationMarketplaceIds.HasItem())
                        this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .UpdateOpened(productInformationMarketplaceIds, true);
                }
                #endregion
            }
        }

        private void Delete(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK && httpHelperV3Response.Content.Done)
                File.Delete(marketplaceCreateProductRequest.Path);
        }
        #endregion
    }
}
