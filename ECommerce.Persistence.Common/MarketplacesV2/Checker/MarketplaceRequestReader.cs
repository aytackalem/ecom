﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Checker
{
    public class MarketplaceRequestReader : IMarketplaceRequestReader
    {
        private readonly IDbNameFinder _dbNameFinder;

        public MarketplaceRequestReader(IDbNameFinder dbNameFinder)
        {
            _dbNameFinder = dbNameFinder;
        }



        #region Methods
        public async Task<List<MarketplaceRequestCreateProduct>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestCreateProduct> request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.CreateProductBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.QueueId,
                    request.MarketplaceId);

            if (Directory.Exists(path) == false)
                return new List<MarketplaceRequestCreateProduct>();

            var files = Directory.GetFiles(path);

            List<MarketplaceRequestCreateProduct> marketplaceRequestCreateProducts = new();

            foreach (var theFile in files)
            {
                var content = await File.ReadAllTextAsync(theFile);
                marketplaceRequestCreateProducts.Add(JsonConvert.DeserializeObject<MarketplaceRequestCreateProduct>(content));
            }

            return marketplaceRequestCreateProducts;
        }

        public async Task<List<MarketplaceRequestUpdateProduct>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdateProduct> request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateProductBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.QueueId,
                    request.MarketplaceId);

            if (Directory.Exists(path) == false)
                return new List<MarketplaceRequestUpdateProduct>();

            var files = Directory.GetFiles(path);

            List<MarketplaceRequestUpdateProduct> marketplaceRequestUpdateProducts = new();

            foreach (var theFile in files)
            {
                var content = await File.ReadAllTextAsync(theFile);
                marketplaceRequestUpdateProducts.Add(JsonConvert.DeserializeObject<MarketplaceRequestUpdateProduct>(content));
            }

            return marketplaceRequestUpdateProducts;
        }

        public async Task<List<MarketplaceRequestUpdatePrice>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdatePrice> request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdatePriceBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.QueueId,
                    request.MarketplaceId);

            if (Directory.Exists(path) == false)
                return new List<MarketplaceRequestUpdatePrice>();

            var files = Directory.GetFiles(path);

            List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices = new();

            foreach (var theFile in files)
            {
                var content = await File.ReadAllTextAsync(theFile);
                marketplaceRequestUpdatePrices.Add(JsonConvert.DeserializeObject<MarketplaceRequestUpdatePrice>(content));
            }

            return marketplaceRequestUpdatePrices;
        }

        public async Task<List<MarketplaceRequestUpdateStock>> ReadAsync(MarketplaceRequestReaderRequest<MarketplaceRequestUpdateStock> request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateStockBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.QueueId,
                    request.MarketplaceId);

            if (Directory.Exists(path) == false)
                return new List<MarketplaceRequestUpdateStock>();

            var files = Directory.GetFiles(path);

            List<MarketplaceRequestUpdateStock> marketplaceRequestUpdatePrices = new();

            foreach (var theFile in files)
            {
                var content = await File.ReadAllTextAsync(theFile);
                marketplaceRequestUpdatePrices.Add(JsonConvert.DeserializeObject<MarketplaceRequestUpdateStock>(content));
            }

            return marketplaceRequestUpdatePrices;
        }
        #endregion
    }
}
