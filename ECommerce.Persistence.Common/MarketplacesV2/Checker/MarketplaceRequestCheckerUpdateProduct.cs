﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Checker
{
    public class MarketplaceRequestCheckerUpdateProduct : IMarketplaceRequestChecker
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2UpdateProductTrackableResolver _marketplaceV2ProductTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestCheckerUpdateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2UpdateProductTrackableResolver marketplaceV2ProductTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2ProductTrackableResolver = marketplaceV2ProductTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task CheckAsync(MarketplaceRequestCheckerRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceUpdateProductRequests = await this._marketplaceRequestReader.ReadAsync(
                    new MarketplaceRequestReaderRequest<MarketplaceRequestUpdateProduct>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplaceUpdateProductRequests.HasItem() == false)
                    return;

                var marketplace = this._marketplaceV2ProductTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceUpdateProductRequests.Count; i++)
                {
                    var marketplaceUpdateProductRequest = marketplaceUpdateProductRequests[i];

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.CheckAsync(marketplaceUpdateProductRequest);

                        await this.NotificationAsync(marketplaceUpdateProductRequest, httpHelperV3Response);

                        this.Delete(marketplaceUpdateProductRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Checker",
                            Command = "Update Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceUpdateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Checker",
                    Command = "Update Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestUpdateProduct marketplaceUpdateProductRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {

            var _ = httpHelperV3Response
                .Content
                .TrackingResponseItems
                .Where(upri => upri.Messages.HasItem()).ToList();

            foreach (var theItem in _)
            {
                var marketplaceRequestUpdateProductItem = marketplaceUpdateProductRequest
                    .Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaces.Any(pim => pim.ProductInformationMarketplaceId == theItem.ProductInformationMarketplaceId));

                var productInformationMarketplace = marketplaceRequestUpdateProductItem
                    .ProductInformationMarketplaces
                    .FirstOrDefault(pim => pim.ProductInformationMarketplaceId == theItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "UPR",
                    Title = "Ürün Güncelleme",
                    ProductInformationMarketplaceId = theItem.ProductInformationMarketplaceId,
                    Message = string.Join(" ", theItem.Messages),
                    MarketplaceId = marketplaceUpdateProductRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplaceUpdateProductRequest.MarketplaceRequestHeader.CompanyId,
                    Barcode = theItem.Barcode,
                    StockCode = theItem.StockCode,
                    CreatedDateTime = DateTime.Now,
                    ProductId = marketplaceRequestUpdateProductItem != null 
                        ? marketplaceRequestUpdateProductItem.ProductId 
                        : 0,
                    PhotoUrl = productInformationMarketplace.ProductInformationPhotos.HasItem() 
                        ? productInformationMarketplace.ProductInformationPhotos.FirstOrDefault()?.Url 
                        : null,
                    ModelCode = marketplaceRequestUpdateProductItem?.SellerCode,
                    ProductInformationId = productInformationMarketplace != null 
                        ? productInformationMarketplace.ProductInformationId 
                        : 0,
                    ProductInformationName = $"{productInformationMarketplace?.ProductInformationName} {productInformationMarketplace?.VariantValuesDescription}"
                });
            }

        }

        private void Delete(MarketplaceRequestUpdateProduct marketplaceUpdateProductRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceUpdateProductRequest.Path);
        }
        #endregion
    }
}
