﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Checker
{
    public class MarketplaceRequestCheckerUpdatePrice : IMarketplaceRequestChecker
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2StockPriceTrackableResolver _marketplaceV2StockPriceTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestCheckerUpdatePrice(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2StockPriceTrackableResolver marketplaceV2StockPriceTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2StockPriceTrackableResolver = marketplaceV2StockPriceTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task CheckAsync(MarketplaceRequestCheckerRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplacePriceRequests = await this._marketplaceRequestReader.ReadAsync(
                    new MarketplaceRequestReaderRequest<MarketplaceRequestUpdatePrice>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplacePriceRequests.HasItem() == false)
                    return;

                var marketplace = this._marketplaceV2StockPriceTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplacePriceRequests.Count; i++)
                {
                    var marketplacePriceRequest = marketplacePriceRequests[i];

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.CheckAsync(marketplacePriceRequest);
                        
                        await this.NotificationAsync(marketplacePriceRequest, httpHelperV3Response);
                        
                        this.Update(marketplacePriceRequest, httpHelperV3Response);
                        
                        this.Delete(marketplacePriceRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Checker",
                            Command = "Update Price Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplacePriceRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Checker",
                    Command = "Update Price",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestUpdatePrice marketplacePriceRequest, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {

            var _ = httpHelperV3Response.Content.TrackingResponseItems.Where(upri => upri.Messages.HasItem()).ToList();
            foreach (var item in _)
            {
                var invalidItem = marketplacePriceRequest.Items.FirstOrDefault(x => x.ProductInformationMarketplaceId == item.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "UP",
                    Title = "Fiyat",
                    ProductInformationMarketplaceId = item.ProductInformationMarketplaceId,
                    Message = string.Join(" ", item.Messages),
                    MarketplaceId = marketplacePriceRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplacePriceRequest.MarketplaceRequestHeader.CompanyId,
                    Barcode = item.Barcode,
                    CreatedDateTime = DateTime.Now,                   
                    StockCode = item.StockCode,
                    ProductId = invalidItem?.ProductId ?? 0,
                    PhotoUrl = invalidItem?.PhotoUrl,
                    ModelCode = invalidItem?.SellerCode,
                    ProductInformationId = invalidItem != null ? invalidItem.ProductInformationId : 0,
                    ProductInformationName = $"{invalidItem?.ProductInformationName} {invalidItem?.VariantValuesDescription}"
                });
            }

        }

        private void Update(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {

        }

        private void Delete(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<TrackingResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceRequestUpdatePrice.Path);
        }
        #endregion
    }
}
