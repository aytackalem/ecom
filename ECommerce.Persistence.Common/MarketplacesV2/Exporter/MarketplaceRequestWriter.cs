﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Exporter
{
    public class MarketplaceRequestWriter : IMarketplaceRequestWriter
    {
        #region Fields
        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestWriter(IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestCreateProduct> request)
        {
            foreach (var theRequest in request.MarketplaceRequests)
            {
                var path = string.Format(
                    MarketplaceRequestFileTemplates.CreateProductDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId);

                Directory.CreateDirectory(path);

                path = string.Format(
                    MarketplaceRequestFileTemplates.CreateProductTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId,
                    theRequest.Id);

                theRequest.Path = path;

                await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(theRequest));
            }
        }

        public async Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdateProduct> request)
        {
            foreach (var theRequest in request.MarketplaceRequests)
            {
                var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateProductDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId);

                Directory.CreateDirectory(path);

                path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateProductTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId,
                    theRequest.Id);

                theRequest.Path = path;

                await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(theRequest));
            }
        }

        public async Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdateStock> request)
        {
            foreach (var theRequest in request.MarketplaceRequests)
            {
                var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateStockDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId);

                Directory.CreateDirectory(path);

                path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateStockTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId,
                    theRequest.Id);

                theRequest.Path = path;

                await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(theRequest));
            }
        }

        public async Task WriteAsync(MarketplaceRequestWriterRequest<MarketplaceRequestUpdatePrice> request)
        {
            foreach (var theRequest in request.MarketplaceRequests)
            {
                var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdatePriceDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId);

                Directory.CreateDirectory(path);

                path = string.Format(
                    MarketplaceRequestFileTemplates.UpdatePriceTemplate,
                    this._dbNameFinder.FindName(),
                    theRequest.MarketplaceRequestHeader.QueueId,
                    theRequest.MarketplaceRequestHeader.MarketplaceId,
                    theRequest.MarketplaceRequestHeader.TenantId,
                    theRequest.MarketplaceRequestHeader.DomainId,
                    theRequest.MarketplaceRequestHeader.CompanyId,
                    theRequest.Id);

                theRequest.Path = path;

                await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(theRequest));
            }
        }
        #endregion
    }
}
