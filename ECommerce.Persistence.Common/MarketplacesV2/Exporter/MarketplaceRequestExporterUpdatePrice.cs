﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Exporter
{
    public class MarketplaceRequestExporterUpdatePrice : IMarketplaceRequestExporter
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestWriter _marketplaceRequestExportWriter;

        private readonly MarketplaceV2StockPriceExportableResolver _marketplaceV2Resolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestExporterUpdatePrice(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestWriter marketplaceRequestExportWriter, MarketplaceV2StockPriceExportableResolver marketplaceV2Resolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestExportWriter = marketplaceRequestExportWriter;
            this._marketplaceV2Resolver = marketplaceV2Resolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ExportAsync(MarketplaceRequestExporterRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceRequestUpdatePrices = await this.Get(request);
                if (marketplaceRequestUpdatePrices.HasItem() == false)
                    return;

                var marketplace = this._marketplaceV2Resolver(request.MarketplaceId);
                marketplace.Validate(marketplaceRequestUpdatePrices);

                await this.PriceNotificationAsync(marketplaceRequestUpdatePrices);

                this.UpdateDirty(marketplaceRequestUpdatePrices);

                if (marketplace.StockPriceLoggable)
                    await this.LogAsync(request.MarketplaceId, marketplaceRequestUpdatePrices);

                marketplaceRequestUpdatePrices = marketplace.Split(marketplaceRequestUpdatePrices);

                await this.WriteAsync(marketplaceRequestUpdatePrices);
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Exporter",
                    Command = "Update Price",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task<List<MarketplaceRequestUpdatePrice>> Get(MarketplaceRequestExporterRequest request)
        {
            var marketplaceConfigurations = await this
                ._unitOfWork
                .MarketplaceConfigurationRepository
                .ReadByQueueIdAsync(request.QueueId);

            var dirtyPrices = await this
                ._unitOfWork
                .ProductInformationMarketplaceRepository
                .ReadDirtyPricesAsync(request.QueueId, request.MarketplaceId);

            return dirtyPrices
                .GroupBy(ds => new
                {
                    ds.TenantId,
                    ds.DomainId,
                    ds.CompanyId,
                    ds.MarketplaceId
                })
                .Select(ds => new MarketplaceRequestUpdatePrice
                {
                    MarketplaceRequestHeader = new()
                    {
                        QueueId = request.QueueId,
                        TenantId = ds.Key.TenantId,
                        DomainId = ds.Key.DomainId,
                        CompanyId = ds.Key.CompanyId,
                        MarketplaceId = ds.Key.MarketplaceId,
                        MarketplaceConfigurations = marketplaceConfigurations
                            .Where(mc => mc.CompanyId == ds.Key.CompanyId &&
                                         mc.MarketplaceCompany.MarketplaceId == ds.Key.MarketplaceId)
                            .ToDictionary(mc => mc.Key, mc => mc.Value)
                    },
                    Items = ds
                        .Select(i => new MarketplaceRequestUpdatePriceItem
                        {
                            Barcode = i.Barcode,
                            ListUnitPrice = i.ListUnitPrice,
                            UnitPrice = i.UnitPrice,
                            Stock = i.Stock < 0 ? 0 : i.Stock,
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            SkuCode = i.SkuCode,
                            StockCode = i.StockCode,
                            VatRate = i.VatRate,
                            ProductInformationUUId = i.ProductInformationUUId,
                            ProductUUId = i.ProductUUId,
                            LastRequestDateTime = i.LastRequestDateTime,
                            PhotoUrl = i.PhotoUrl,
                            ProductId = i.ProductId,
                            ProductInformationId = i.ProductInformationId,
                            ProductInformationName = i.ProductInformationName,
                            VariantValuesDescription = i.VariantValuesDescription,
                            SellerCode = i.SellerCode
                        })
                        .ToList()
                })
                .ToList();
        }

        private async Task WriteAsync(List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices)
        {
            await this._marketplaceRequestExportWriter.WriteAsync(
                        new MarketplaceRequestWriterRequest<MarketplaceRequestUpdatePrice>
                        {
                            MarketplaceRequests = marketplaceRequestUpdatePrices
                        });
        }

        private void UpdateDirty(List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices)
        {
            List<int> productInformationMarketplaceIds = new();

            productInformationMarketplaceIds.AddRange(marketplaceRequestUpdatePrices.SelectMany(mpr => mpr.Items.Select(i => i.ProductInformationMarketplaceId)).ToList());

            if (marketplaceRequestUpdatePrices.Any(mrup => mrup.InvalidItems.HasItem()))
                productInformationMarketplaceIds.AddRange(marketplaceRequestUpdatePrices.SelectMany(mpr => mpr.InvalidItems.Select(i => i.ProductInformationMarketplaceId)).ToList());

            this._unitOfWork.ProductInformationMarketplaceRepository.UpdateDirtyPrice(productInformationMarketplaceIds, false);
        }

        private async Task PriceNotificationAsync(List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices)
        {
            var _ = marketplaceRequestUpdatePrices.Where(mrup => mrup.InvalidItems.HasItem()).ToList();
            foreach (var theMarketplaceRequestUpdatePrice in _)
            {
                foreach (var theInvalidItem in theMarketplaceRequestUpdatePrice.InvalidItems)
                {
                    await this._notificationService.PostAsync(new()
                    {
                        Type = "UP",
                        Title = "Fiyat",
                        ProductId = theInvalidItem.ProductId,
                        ModelCode = theInvalidItem.SellerCode,
                        ProductInformationId = theInvalidItem.ProductInformationId,
                        ProductInformationName = theInvalidItem.ProductInformationName,
                        Message = theInvalidItem.ErrorMessage,
                        ProductInformationMarketplaceId = theInvalidItem.ProductInformationMarketplaceId,
                        MarketplaceId = theMarketplaceRequestUpdatePrice.MarketplaceRequestHeader.MarketplaceId,
                        CompanyId = theMarketplaceRequestUpdatePrice.MarketplaceRequestHeader.CompanyId,
                        Brand = this._dbNameFinder.FindName(),
                        CreatedDateTime = DateTime.Now,
                        Barcode = theInvalidItem.Barcode,
                        StockCode = theInvalidItem.StockCode,
                        PhotoUrl = theInvalidItem.PhotoUrl
                    });
                }
            }
        }

        private async Task LogAsync(string marketplaceId, List<MarketplaceRequestUpdatePrice> marketplaceRequestUpdatePrices)
        {
            var _ = marketplaceRequestUpdatePrices
                .SelectMany(mrup => mrup.Items.Select(i => i.ProductInformationMarketplaceId))
                .ToList();
            if (_.Count > 0)
                await this._unitOfWork.ProductInformationMarketplaceRepository.PriceLogAsync(marketplaceId, _);
        }
        #endregion
    }
}
