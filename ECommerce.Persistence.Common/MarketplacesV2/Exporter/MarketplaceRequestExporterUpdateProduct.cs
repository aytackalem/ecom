﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Exporter
{
    public class MarketplaceRequestExporterUpdateProduct : IMarketplaceRequestExporter
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestWriter _marketplaceRequestExportWriter;

        private readonly MarketplaceV2UpdateProductExportableResolver _marketplaceV2Resolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestExporterUpdateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestWriter marketplaceRequestExportWriter, MarketplaceV2UpdateProductExportableResolver marketplaceV2Resolver, IMarketplaceErrorFacade marketplaceErrorFacade,
            IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestExportWriter = marketplaceRequestExportWriter;
            this._marketplaceV2Resolver = marketplaceV2Resolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ExportAsync(MarketplaceRequestExporterRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplace = this._marketplaceV2Resolver(request.MarketplaceId);

                var marketplaceRequestUpdateProducts = await this.Get(request);
                if (marketplaceRequestUpdateProducts.HasItem() == false)
                    return;

                marketplace.Validate(marketplaceRequestUpdateProducts);

                await this.ValidationNotificationAsync(marketplaceRequestUpdateProducts);

                this.UpdateDirty(marketplaceRequestUpdateProducts);

                marketplaceRequestUpdateProducts = marketplace.Split(marketplaceRequestUpdateProducts);

                await this.Write(marketplaceRequestUpdateProducts);
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Exporter",
                    Command = "Update Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task<List<MarketplaceRequestUpdateProduct>> Get(MarketplaceRequestExporterRequest request)
        {
            var marketplaceConfigurations = await this
                ._unitOfWork
                .MarketplaceConfigurationRepository
                .ReadByQueueIdAsync(request.QueueId);

            var tupple = await this
                ._unitOfWork
                .ProductRepository
                .ReadDirtyProductsAsync(
                    request.QueueId,
                    true,
                    request.MarketplaceId);

            return tupple
                .Item1
                .GroupBy(item => new
                {
                    item.TenantId,
                    item.DomainId,
                    item.CompanyId,
                    item.MarketplaceId
                })
                .Select(groupedItem => new MarketplaceRequestUpdateProduct
                {
                    MarketplaceRequestHeader = new MarketplaceRequestHeader
                    {
                        QueueId = request.QueueId,
                        TenantId = groupedItem.Key.TenantId,
                        DomainId = groupedItem.Key.DomainId,
                        CompanyId = groupedItem.Key.CompanyId,
                        MarketplaceId = groupedItem.Key.MarketplaceId,
                        MarketplaceConfigurations = marketplaceConfigurations
                            .Where(mc => mc.CompanyId == groupedItem.Key.CompanyId &&
                                         mc.MarketplaceCompany.MarketplaceId == groupedItem.Key.MarketplaceId)
                            .ToDictionary(mc => mc.Key, mc => mc.Value)
                    },
                    Items = groupedItem
                        .GroupBy(gi => new
                        {
                            gi.MarketplaceBrandCode,
                            gi.MarketplaceBrandName,
                            gi.MarketplaceCategoryCode,
                            gi.MarketplaceCategoryName,
                            gi.ProductId,
                            gi.ProductName,
                            gi.DeliveryDay,
                            gi.SellerCode,
                            gi.ProductMarketplaceUUId,
                            gi.ProductMarketplaceId
                        })
                        .Select(pm => new MarketplaceRequestUpdateProductItem
                        {
                            DimensionalWeight = 1,
                            ProductMarketplaceId = pm.Key.ProductMarketplaceId,
                            BrandId = pm.Key.MarketplaceBrandCode,
                            BrandName = pm.Key.MarketplaceBrandName,
                            CategoryCode = pm.Key.MarketplaceCategoryCode,
                            CategoryName = pm.Key.MarketplaceCategoryName,
                            ProductId = pm.Key.ProductId,
                            ProductName = pm.Key.ProductName,
                            DeliveryDay = pm.Key.DeliveryDay,
                            SellerCode = pm.Key.SellerCode,
                            ProductMarketplaceUUId = pm.Key.ProductMarketplaceUUId,
                            ProductInformationMarketplaces = pm
                                .GroupBy(i => new
                                {
                                    i.ProductInformationId,
                                    i.ProductInformationMarketplaceId,
                                    i.Barcode,
                                    i.StockCode,
                                    i.ListUnitPrice,
                                    i.UnitPrice,
                                    i.Stock,
                                    i.SkuCode,
                                    i.VatRate,
                                    i.Opened,
                                    i.ProductInformationName,
                                    i.SubTitle,
                                    i.ProductInformationMarketplaceUUId
                                })
                                .Select(pim => new ProductInformationMarketplace
                                {
                                    Description = string.IsNullOrEmpty(pim.First().Content)
                                        ? pim.Key.ProductInformationName
                                        : pim.First().Content,
                                    ProductInformationId = pim.Key.ProductInformationId,
                                    ProductInformationMarketplaceId = pim.Key.ProductInformationMarketplaceId,
                                    Barcode = pim.Key.Barcode,
                                    StockCode = pim.Key.StockCode,
                                    SkuCode = pim.Key.SkuCode,
                                    ListUnitPrice = pim.Key.ListUnitPrice,
                                    UnitPrice = pim.Key.UnitPrice,
                                    Stock = pim.Key.Stock < 0 ? 0 : pim.Key.Stock,
                                    VatRate = pim.Key.VatRate,
                                    Opened = pim.Key.Opened,
                                    ProductInformationName = pim.Key.ProductInformationName,
                                    Subtitle = pim.Key.SubTitle,
                                    VariantValuesDescription = pim.First().VariantValuesDescription,
                                    ProductInformationUUId = pim.Key.ProductInformationMarketplaceUUId,
                                    ProductInformationPhotos = tupple
                                        .Item2
                                        .Where(i2 => i2.ProductInformationId == pim.Key.ProductInformationId)
                                        .OrderBy(i2 => i2.DisplayOrder)
                                        .Select(i2 => new ProductInformationPhoto
                                        {
                                            DisplayOrder = i2.DisplayOrder,
                                            Url = i2.FileName
                                        })
                                        .ToList(),
                                    ProductInformationMarketplaceVariantValues = tupple
                                        .Item3
                                        .Where(i3 => i3.ProductInformationMarketplaceId == pim.Key.ProductInformationMarketplaceId)
                                        .Select(i3 => new ProductInformationMarketplaceVariantValue
                                        {
                                            MarketplaceVarinatCode = i3.MarketplaceVariantCode,
                                            MarketplaceVarinatName = i3.MarketplaceVariantName,
                                            MarketplaceVariantValueCode = i3.MarketplaceVariantValueCode,
                                            MarketplaceVariantValueName = i3.MarketplaceVariantValueName,
                                            AllowCustom = i3.AllowCustom,
                                            Mandatory = i3.Mandatory,
                                            Varinater = i3.Varianter
                                        })
                                        .ToList()
                                })
                                .ToList()
                        })
                        .ToList()
                })
                .ToList();
        }

        private async Task Write(List<MarketplaceRequestUpdateProduct> marketplaceRequestUpdateProducts)
        {
            await this._marketplaceRequestExportWriter.WriteAsync(
                        new MarketplaceRequestWriterRequest<MarketplaceRequestUpdateProduct>
                        {
                            MarketplaceRequests = marketplaceRequestUpdateProducts
                        });
        }

        private void UpdateDirty(List<MarketplaceRequestUpdateProduct> marketplaceRequestUpdateProducts)
        {
            List<int> productInformationMarketplaceIds = new();

            productInformationMarketplaceIds.AddRange(marketplaceRequestUpdateProducts.SelectMany(mupr =>
                        mupr.Items.SelectMany(i =>
                            i.ProductInformationMarketplaces.Select(pim => pim.ProductInformationMarketplaceId))));

            if (marketplaceRequestUpdateProducts.Any(mcpr => mcpr.InvalidItems.HasItem()))
                productInformationMarketplaceIds.AddRange(marketplaceRequestUpdateProducts.SelectMany(mupr =>
                        mupr.InvalidItems.SelectMany(i =>
                            i.ProductInformationMarketplaces.Select(pim => pim.ProductInformationMarketplaceId))));

            this._unitOfWork.ProductRepository.UpdateDirtyProduct(productInformationMarketplaceIds, false);
        }

        private async Task ValidationNotificationAsync(List<MarketplaceRequestUpdateProduct> marketplaceRequestUpdateProducts)
        {
            var _ = marketplaceRequestUpdateProducts.Where(mrup => mrup.InvalidItems.HasItem()).ToList();
            foreach (var theMarketplaceRequestUpdateProduct in _)
            {
                foreach (var theInvalidItem in theMarketplaceRequestUpdateProduct.InvalidItems)
                {
                    foreach (var theProductInformationMarketplace in theInvalidItem.ProductInformationMarketplaces)
                    {
                        await this._notificationService.PostAsync(new()
                        {
                            Type = "UPR",
                            Title = "Ürün Güncelleme",
                            Message = theInvalidItem.ErrorMessage,
                            ProductInformationMarketplaceId = theProductInformationMarketplace.ProductInformationMarketplaceId,
                            MarketplaceId = theMarketplaceRequestUpdateProduct.MarketplaceRequestHeader.MarketplaceId,
                            CompanyId = theMarketplaceRequestUpdateProduct.MarketplaceRequestHeader.CompanyId,
                            Brand = this._dbNameFinder.FindName(),
                            CreatedDateTime = DateTime.Now,
                            ProductId = theInvalidItem.ProductId,
                            PhotoUrl = theProductInformationMarketplace.ProductInformationPhotos.Count > 0 ? theProductInformationMarketplace.ProductInformationPhotos.FirstOrDefault().Url : "",
                            Barcode = theProductInformationMarketplace.Barcode,
                            StockCode = theProductInformationMarketplace.StockCode,
                            ModelCode = theInvalidItem.SellerCode,
                            ProductInformationId = theInvalidItem.ProductMarketplaceId,
                            ProductInformationName = $"{theProductInformationMarketplace.ProductInformationName} {theProductInformationMarketplace.VariantValuesDescription}"
                        });
                    }
                }
            }
        }

        private async Task LogAsync(List<MarketplaceRequestUpdateProduct> marketplaceRequestUpdateProducts)
        {

        }
        #endregion
    }
}
