﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Exporter
{
    public class StockRequest
    {
        public StockRequestHeader Header { get; set; }

        public List<StockRequestItem> Items { get; set; }
    }

    public class StockRequestHeader
    {
        public string MarketplaceId { get; set; }

        public Dictionary<string, string> Configurations { get; set; }
    }

    public class StockRequestItem
    {
        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public int Stock { get; set; }
    }

    public class MarketplaceRequestExporterUpdateStock : IMarketplaceRequestExporter
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestWriter _marketplaceRequestExportWriter;

        private readonly MarketplaceV2StockPriceExportableResolver _marketplaceV2Resolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IHttpHelperV3 _httpHelperV3;
        #endregion

        #region Constructors
        public MarketplaceRequestExporterUpdateStock(IHttpHelperV3 httpHelperV3, INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestWriter marketplaceRequestExportWriter, MarketplaceV2StockPriceExportableResolver marketplaceV2Resolver, IMarketplaceErrorFacade marketplaceErrorFacade,
            IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._httpHelperV3 = httpHelperV3;
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestExportWriter = marketplaceRequestExportWriter;
            this._marketplaceV2Resolver = marketplaceV2Resolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ExportAsync(MarketplaceRequestExporterRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceRequestUpdateStocks = await this.Get(request);
                if (marketplaceRequestUpdateStocks.HasItem() == false)
                    return;

                var marketplace = this._marketplaceV2Resolver(request.MarketplaceId);
                marketplace.Validate(marketplaceRequestUpdateStocks);

                await this.ValidationNotificationAsync(marketplaceRequestUpdateStocks);

                this.UpdateDirty(marketplaceRequestUpdateStocks);

                if (marketplace.StockPriceLoggable)
                    await this.LogAsync(request.MarketplaceId, marketplaceRequestUpdateStocks);

                marketplaceRequestUpdateStocks = marketplace.Split(marketplaceRequestUpdateStocks);

                await this.WriteAsync(marketplaceRequestUpdateStocks);
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Exporter",
                    Command = "Update Stock",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task<List<MarketplaceRequestUpdateStock>> Get(MarketplaceRequestExporterRequest request)
        {
            var marketplaceConfigurations = await this
                ._unitOfWork
                .MarketplaceConfigurationRepository
                .ReadByQueueIdAsync(request.QueueId);

            var dirtyStocks = await this
                ._unitOfWork
                .ProductInformationMarketplaceRepository
                .ReadDirtyStocksAsync(request.QueueId, request.MarketplaceId);

            return dirtyStocks
                .GroupBy(ds => new
                {
                    ds.TenantId,
                    ds.DomainId,
                    ds.CompanyId,
                    ds.MarketplaceId,
                    ds.MarketplaceCompanyId
                })
                .Select(ds => new MarketplaceRequestUpdateStock
                {
                    MarketplaceRequestHeader = new()
                    {
                        QueueId = request.QueueId,
                        TenantId = ds.Key.TenantId,
                        DomainId = ds.Key.DomainId,
                        CompanyId = ds.Key.CompanyId,
                        MarketplaceId = ds.Key.MarketplaceId,
                        MarketplaceConfigurations = marketplaceConfigurations
                                .Where(mc => mc.CompanyId == ds.Key.CompanyId &&
                                             mc.MarketplaceCompany.MarketplaceId == ds.Key.MarketplaceId)
                                .ToDictionary(mc => mc.Key, mc => mc.Value)
                    },
                    Items = ds
                        .Select(i => new MarketplaceRequestUpdateStockItem
                        {
                            Barcode = i.Barcode,
                            ListUnitPrice = i.ListUnitPrice,
                            UnitPrice = i.UnitPrice,
                            ProductInformationMarketplaceId = i.ProductInformationMarketplaceId,
                            SkuCode = i.SkuCode,
                            StockCode = i.StockCode,
                            Stock = i.Stock < 0 ? 0 : i.Stock,
                            VatRate = i.VatRate,
                            ProductInformationUUId = i.ProductInformationUUId,
                            ProductUUId = i.ProductUUId,
                            UpdatePrice = i.UpdatePrice,
                            LastRequestDateTime = i.LastRequestDateTime,
                            PhotoUrl = i.PhotoUrl,
                            ProductId = i.ProductId,
                            ProductInformationId = i.ProductInformationId,
                            ProductInformationName = i.ProductInformationName,
                            VariantValuesDescription = i.VariantValuesDescription,
                            SellerCode = i.SellerCode
                        })
                        .ToList()
                })
                .ToList();
        }

        private async Task WriteAsync(List<MarketplaceRequestUpdateStock> marketplaceRequestUpdateStocks)
        {
            //foreach (var item in marketplaceRequestUpdateStocks)
            //{
            //    await this._httpHelperV3.SendAsync<StockRequest, HttpHelperV3Response>(new Application.Common.Parameters.HttpHelperV3.HttpHelperV3Request<StockRequest>
            //    {
            //        HttpMethod = System.Net.Http.HttpMethod.Post,
            //        RequestUri = "http://localhost:5900/Stock",
            //        Request = new StockRequest
            //        {
            //            Header = new StockRequestHeader
            //            {
            //                MarketplaceId = item.MarketplaceRequestHeader.MarketplaceId,
            //                Configurations = item.MarketplaceRequestHeader.MarketplaceConfigurations
            //            },
            //            Items = item.Items.Select(i => new StockRequestItem
            //            {
            //                Barcode = i.Barcode,
            //                Stock = i.Stock,
            //                StockCode = i.StockCode
            //            }).ToList()
            //        }
            //    });
            //}

            await this._marketplaceRequestExportWriter.WriteAsync(
                        new MarketplaceRequestWriterRequest<MarketplaceRequestUpdateStock>
                        {
                            MarketplaceRequests = marketplaceRequestUpdateStocks
                        });
        }

        private void UpdateDirty(List<MarketplaceRequestUpdateStock> marketplaceRequestUpdateStocks)
        {
            List<int> productInformationMarketplaceIds = new();

            productInformationMarketplaceIds.AddRange(marketplaceRequestUpdateStocks.SelectMany(msr => msr.Items.Select(i => i.ProductInformationMarketplaceId)).ToList());

            if (marketplaceRequestUpdateStocks.Any(mrus => mrus.InvalidItems.HasItem()))
                productInformationMarketplaceIds.AddRange(marketplaceRequestUpdateStocks.SelectMany(msr => msr.InvalidItems.Select(i => i.ProductInformationMarketplaceId)).ToList());

            this._unitOfWork.ProductInformationMarketplaceRepository.UpdateDirtyStock(productInformationMarketplaceIds, false);
        }

        private async Task ValidationNotificationAsync(List<MarketplaceRequestUpdateStock> marketplaceRequestUpdateStocks)
        {
            var _ = marketplaceRequestUpdateStocks.Where(mrup => mrup.InvalidItems.HasItem()).ToList();
            foreach (var theMarketplaceRequestUpdateStock in _)
            {
                foreach (var theInvalidItem in theMarketplaceRequestUpdateStock.InvalidItems)
                {
                    await this._notificationService.PostAsync(new()
                    {
                        Type = "US",
                        Title = "Stok",
                        Message = theInvalidItem.ErrorMessage,
                        ProductInformationMarketplaceId = theInvalidItem.ProductInformationMarketplaceId,
                        MarketplaceId = theMarketplaceRequestUpdateStock.MarketplaceRequestHeader.MarketplaceId,
                        CompanyId = theMarketplaceRequestUpdateStock.MarketplaceRequestHeader.CompanyId,
                        Brand = this._dbNameFinder.FindName(),
                        Barcode = theInvalidItem.Barcode,
                        CreatedDateTime = DateTime.Now,
                        StockCode = theInvalidItem.StockCode,
                        PhotoUrl = theInvalidItem.PhotoUrl,
                        ModelCode = theInvalidItem.SellerCode,
                        ProductId = theInvalidItem.ProductId,
                        ProductInformationId = theInvalidItem.ProductInformationId,
                        ProductInformationName = $"{theInvalidItem.ProductInformationName} {theInvalidItem.VariantValuesDescription}"
                    });
                }
            }
        }

        private async Task LogAsync(string marketplaceId, List<MarketplaceRequestUpdateStock> marketplaceRequestUpdateStocks)
        {
            var _ = marketplaceRequestUpdateStocks
                .SelectMany(mrus => mrus.Items.Select(i => i.ProductInformationMarketplaceId))
                .ToList();
            if (_.Count > 0)
                await this._unitOfWork.ProductInformationMarketplaceRepository.StockLogAsync(marketplaceId, _);
        }
        #endregion
    }
}
