﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Exporter.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Exporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Exporter
{
    public class MarketplaceRequestExporterCreateProduct : IMarketplaceRequestExporter
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestWriter _marketplaceRequestExportWriter;

        private readonly MarketplaceV2CreateProductExportableResolver _marketplaceV2Resolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestExporterCreateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestWriter marketplaceRequestExportWriter, MarketplaceV2CreateProductExportableResolver marketplaceV2Resolver, IMarketplaceErrorFacade marketplaceErrorFacade,
            IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestExportWriter = marketplaceRequestExportWriter;
            this._marketplaceV2Resolver = marketplaceV2Resolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ExportAsync(MarketplaceRequestExporterRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplace = this._marketplaceV2Resolver(request.MarketplaceId);

                var marketplaceRequestCreateProducts = await this.Get(request, marketplace.IsUpsert);
                if (marketplaceRequestCreateProducts.HasItem() == false)
                    return;

                marketplace.Validate(marketplaceRequestCreateProducts);

                await this.ValidationNotificationAsync(marketplaceRequestCreateProducts);

                this.UpdateDirty(marketplaceRequestCreateProducts);

                marketplaceRequestCreateProducts = marketplace.Split(marketplaceRequestCreateProducts);

                await this.Write(marketplaceRequestCreateProducts);
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Exporter",
                    Command = "Create Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task<List<MarketplaceRequestCreateProduct>> Get(MarketplaceRequestExporterRequest request, bool IsUpsert)
        {
            var marketplaceConfigurations = await this
                ._unitOfWork
                .MarketplaceConfigurationRepository
                .ReadByQueueIdAsync(request.QueueId);

            Tuple<List<Application.Common.DataTransferObjects.MarketplacesV2.DirtyProduct>,
                  List<Application.Common.DataTransferObjects.MarketplacesV2.DirtyProductPhoto>,
                  List<Application.Common.DataTransferObjects.MarketplacesV2.DirtyProductMarketplaceVariantValue>> tupple = null;

            if (IsUpsert)
                tupple = await this._unitOfWork.ProductRepository.ReadDirtyProductsAsync(request.QueueId, request.MarketplaceId);
            else
                tupple = await this._unitOfWork.ProductRepository.ReadDirtyProductsAsync(request.QueueId, false, request.MarketplaceId);

            return tupple
                .Item1
                .GroupBy(item => new
                {
                    item.MarketplaceId,
                    item.TenantId,
                    item.DomainId,
                    item.CompanyId
                })
                .Select(groupedItem => new MarketplaceRequestCreateProduct
                {
                    MarketplaceRequestHeader = new MarketplaceRequestHeader
                    {
                        QueueId = request.QueueId,
                        TenantId = groupedItem.Key.TenantId,
                        DomainId = groupedItem.Key.DomainId,
                        CompanyId = groupedItem.Key.CompanyId,
                        MarketplaceId = groupedItem.Key.MarketplaceId,
                        MarketplaceConfigurations = marketplaceConfigurations
                            .Where(mc => mc.CompanyId == groupedItem.Key.CompanyId &&
                                         mc.MarketplaceCompany.MarketplaceId == groupedItem.Key.MarketplaceId)
                            .ToDictionary(mc => mc.Key, mc => mc.Value)
                    },
                    Items = groupedItem
                        .GroupBy(gi => new
                        {
                            gi.MarketplaceBrandCode,
                            gi.MarketplaceBrandName,
                            gi.MarketplaceCategoryCode,
                            gi.MarketplaceCategoryName,
                            gi.ProductId,
                            gi.ProductName,
                            gi.DeliveryDay,
                            gi.SellerCode,
                            gi.ProductMarketplaceUUId,
                            gi.ProductMarketplaceId
                        })
                        .Select(pm => new MarketplaceRequestCreateProductItem
                        {
                            DimensionalWeight = 1,
                            ProductMarketplaceId = pm.Key.ProductMarketplaceId,
                            BrandCode = pm.Key.MarketplaceBrandCode,
                            BrandName = pm.Key.MarketplaceBrandName?.Replace("\n", ""),
                            CategoryCode = pm.Key.MarketplaceCategoryCode,
                            CategoryName = pm.Key.MarketplaceCategoryName?.Replace("\n", ""),
                            ProductId = pm.Key.ProductId,
                            ProductName = pm.Key.ProductName?.Replace("\n", ""),
                            DeliveryDay = pm.Key.DeliveryDay,
                            SellerCode = pm.Key.SellerCode,
                            ProductMarketplaceUUId = pm.Key.ProductMarketplaceUUId,
                            ProductInformationMarketplaces = pm
                                .GroupBy(i => new
                                {
                                    i.ProductInformationId,
                                    i.ProductInformationMarketplaceId,
                                    i.Barcode,
                                    i.StockCode,
                                    i.SkuCode,
                                    i.ListUnitPrice,
                                    i.UnitPrice,
                                    i.Stock,
                                    i.VatRate,
                                    i.Opened,
                                    i.ProductInformationName,
                                    i.SubTitle,
                                    i.ProductInformationMarketplaceUUId
                                })
                                .Select(pim => new ProductInformationMarketplace
                                {
                                    Description = string.IsNullOrEmpty(pim.First().Content)
                                        ? pim.Key.ProductInformationName?.Replace("\n", "")
                                        : pim.First().Content,
                                    ProductInformationId = pim.Key.ProductInformationId,
                                    ProductInformationMarketplaceId = pim.Key.ProductInformationMarketplaceId,
                                    Barcode = pim.Key.Barcode,
                                    StockCode = pim.Key.StockCode,
                                    SkuCode = pim.Key.SkuCode,
                                    ListUnitPrice = pim.Key.ListUnitPrice,
                                    UnitPrice = pim.Key.UnitPrice,
                                    Stock = pim.Key.Stock < 0 ? 0 : pim.Key.Stock,
                                    VatRate = pim.Key.VatRate,
                                    Opened = pim.Key.Opened,
                                    ProductInformationName = pim.Key.ProductInformationName?.Replace("\n", ""),
                                    VariantValuesDescription = pim.First().VariantValuesDescription,
                                    Subtitle = pim.Key.SubTitle?.Replace("\n", ""),
                                    ProductInformationUUId = pim.Key.ProductInformationMarketplaceUUId,
                                    ProductInformationPhotos = tupple
                                        .Item2
                                        .Where(i2 => i2.ProductInformationId == pim.Key.ProductInformationId)
                                        .OrderBy(i2 => i2.DisplayOrder)
                                        .Select(i2 => new ProductInformationPhoto
                                        {
                                            DisplayOrder = i2.DisplayOrder,
                                            Url = i2.FileName
                                        })
                                        .ToList(),
                                    ProductInformationMarketplaceVariantValues = tupple
                                        .Item3
                                        .Where(i3 => i3.ProductInformationMarketplaceId == pim.Key.ProductInformationMarketplaceId)
                                        .Select(i3 => new ProductInformationMarketplaceVariantValue
                                        {
                                            MarketplaceVariantValueCode = i3.MarketplaceVariantValueCode,
                                            MarketplaceVariantValueName = i3.MarketplaceVariantValueName,
                                            MarketplaceVarinatCode = i3.MarketplaceVariantCode,
                                            MarketplaceVarinatName = i3.MarketplaceVariantName,
                                            AllowCustom = i3.AllowCustom,
                                            Mandatory = i3.Mandatory,
                                            Varinater = i3.Varianter
                                        })
                                        .ToList()
                                })
                                .ToList()
                        })
                        .ToList()
                })
                .ToList();
        }

        private async Task Write(List<MarketplaceRequestCreateProduct> marketplaceRequestCreateProducts)
        {
            await this._marketplaceRequestExportWriter.WriteAsync(
                    new MarketplaceRequestWriterRequest<MarketplaceRequestCreateProduct>
                    {
                        MarketplaceRequests = marketplaceRequestCreateProducts
                    });
        }

        private void UpdateDirty(List<MarketplaceRequestCreateProduct> marketplaceRequestCreateProducts)
        {
            List<int> productInformationMarketplaceIds = new();

            productInformationMarketplaceIds.AddRange(marketplaceRequestCreateProducts.SelectMany(mcpr =>
                mcpr.Items.SelectMany(i =>
                        i.ProductInformationMarketplaces.Select(pim => pim.ProductInformationMarketplaceId))));

            if (marketplaceRequestCreateProducts.Any(mcpr => mcpr.InvalidItems.HasItem()))
                productInformationMarketplaceIds.AddRange(marketplaceRequestCreateProducts.Where(mcpr => mcpr.InvalidItems.HasItem()).SelectMany(mcpr =>
                    mcpr.InvalidItems.SelectMany(i =>
                            i.ProductInformationMarketplaces.Select(pim => pim.ProductInformationMarketplaceId))));

            this._unitOfWork.ProductRepository.UpdateDirtyProduct(productInformationMarketplaceIds, false);
        }

        private async Task ValidationNotificationAsync(List<MarketplaceRequestCreateProduct> marketplaceRequestCreateProducts)
        {
            var _ = marketplaceRequestCreateProducts.Where(mrup => mrup.InvalidItems.HasItem()).ToList();
            foreach (var theMarketplaceRequestCreateProduct in _)
            {
                foreach (var theInvalidItem in theMarketplaceRequestCreateProduct.InvalidItems)
                {
                    foreach (var theProductInformationMarketplace in theInvalidItem.ProductInformationMarketplaces)
                    {
                        await this._notificationService.PostAsync(new()
                        {
                            Type = "CPR",
                            Title = "Ürün Yükleme",
                            Message = theInvalidItem.ErrorMessage,
                            ProductInformationMarketplaceId = theProductInformationMarketplace.ProductInformationMarketplaceId,
                            MarketplaceId = theMarketplaceRequestCreateProduct.MarketplaceRequestHeader.MarketplaceId,
                            CompanyId = theMarketplaceRequestCreateProduct.MarketplaceRequestHeader.CompanyId,
                            Brand = this._dbNameFinder.FindName(),
                            ProductId = theInvalidItem.ProductId,
                            PhotoUrl = theProductInformationMarketplace?.ProductInformationPhotos?.FirstOrDefault()?.Url,
                            ProductInformationName = $"{theProductInformationMarketplace.ProductInformationName} {theProductInformationMarketplace.VariantValuesDescription}",
                            ProductInformationId = theProductInformationMarketplace.ProductInformationId,
                            ModelCode = theInvalidItem.SellerCode ?? theProductInformationMarketplace.StockCode,
                            Barcode = theProductInformationMarketplace.Barcode,
                            StockCode = theProductInformationMarketplace.StockCode,
                            CreatedDateTime = DateTime.Now
                        });
                    }
                }
            }
        }

        private async Task LogAsync(List<MarketplaceRequestCreateProduct> marketplaceRequestCreateProducts)
        {

        }
        #endregion
    }
}
