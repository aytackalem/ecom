﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorTrackableUpdatePrice : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly IMarketplaceRequestWriter _marketplaceRequestWriter;

        private readonly MarketplaceV2StockPriceTrackableResolver _marketplaceV2StockPriceTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorTrackableUpdatePrice(IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, IMarketplaceRequestWriter marketplaceRequestWriter, MarketplaceV2StockPriceTrackableResolver marketplaceV2StockPriceTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder, INotificationService notificationService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceRequestWriter = marketplaceRequestWriter;
            this._marketplaceV2StockPriceTrackableResolver = marketplaceV2StockPriceTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            _dbNameFinder = dbNameFinder;
            _notificationService = notificationService;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplacePriceRequests = await _marketplaceRequestReader.ReadAsync(new MarketplaceRequestReaderRequest<MarketplaceRequestUpdatePrice>
                {
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId
                });

                if (marketplacePriceRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2StockPriceTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplacePriceRequests.Count; i++)
                {
                    var marketplacePriceRequest = marketplacePriceRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerPriceRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerPriceRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var trackableResponse = await marketplace.SendAsync(marketplacePriceRequest);

                        await this.NotificationAsync(marketplacePriceRequest, trackableResponse);

                        this.Delete(marketplacePriceRequest, trackableResponse);

                        await this.WriteAsync(marketplacePriceRequest, trackableResponse);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Trackable Update Price Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplacePriceRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Trackable Update Price",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task WriteAsync(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                marketplaceRequestUpdatePrice.TrackingId = httpHelperV3Response.Content.TrackingId;

                await this._marketplaceRequestWriter.WriteAsync(marketplaceRequestUpdatePrice);
            }
        }

        private async Task NotificationAsync(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest &&
                httpHelperV3Response.Content.Messages.HasItem())
            {
                var message = string.Join(" ", httpHelperV3Response.Content.Messages);

                foreach (var theItem in marketplaceRequestUpdatePrice.Items)
                {
                    await this._notificationService.PostAsync(new()
                    {
                        Type = "UP",
                        Title = "Fiyat",
                        ProductInformationMarketplaceId = theItem.ProductInformationMarketplaceId,
                        Message = message,
                        MarketplaceId = marketplaceRequestUpdatePrice.MarketplaceRequestHeader.MarketplaceId,
                        Brand = this._dbNameFinder.FindName(),
                        CompanyId = marketplaceRequestUpdatePrice.MarketplaceRequestHeader.CompanyId,
                        CreatedDateTime = DateTime.Now,
                        PhotoUrl = theItem.PhotoUrl,
                        Barcode = theItem.StockCode,
                        StockCode = theItem.Barcode,
                        ProductId = theItem.ProductId,
                        ModelCode = theItem.SellerCode,
                        ProductInformationName = $"{theItem.ProductInformationName} {theItem.VariantValuesDescription}",
                        ProductInformationId = theItem.ProductInformationId
                    });
                }
            }
        }

        private void Delete(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceRequestUpdatePrice.Path);
        }
        #endregion
    }
}
