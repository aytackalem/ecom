﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using Microsoft.Graph.SecurityNamespace;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestWriter : IMarketplaceRequestWriter
    {
        private readonly IDbNameFinder _dbNameFinder;

        public MarketplaceRequestWriter(IDbNameFinder dbNameFinder)
        {
            _dbNameFinder = dbNameFinder;
        }



        #region Methods
        public async Task WriteAsync(MarketplaceRequestCreateProduct request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.CreateProductBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId);

            Directory.CreateDirectory(path);

            path = string.Format(
                    MarketplaceRequestFileTemplates.CreateProductBatchTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId,
                    request.Id);

            request.Path = path;

            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(request));
        }

        public async Task WriteAsync(MarketplaceRequestUpdateProduct request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateProductBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId);

            Directory.CreateDirectory(path);

            path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateProductBatchTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId,
                    request.Id);

            request.Path = path;

            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(request));
        }

        public async Task WriteAsync(MarketplaceRequestUpdateStock request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateStockBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId);

            Directory.CreateDirectory(path);

            path = string.Format(
                    MarketplaceRequestFileTemplates.UpdateStockBatchTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId,
                    request.Id);

            request.Path = path;

            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(request));
        }

        public async Task WriteAsync(MarketplaceRequestUpdatePrice request)
        {
            var path = string.Format(
                    MarketplaceRequestFileTemplates.UpdatePriceBatchDirectoryTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId);

            Directory.CreateDirectory(path);

            path = string.Format(
                    MarketplaceRequestFileTemplates.UpdatePriceBatchTemplate,
                    this._dbNameFinder.FindName(),
                    request.MarketplaceRequestHeader.QueueId,
                    request.MarketplaceRequestHeader.MarketplaceId,
                    request.MarketplaceRequestHeader.TenantId,
                    request.MarketplaceRequestHeader.DomainId,
                    request.MarketplaceRequestHeader.CompanyId,
                    request.Id);

            request.Path = path;

            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(request));
        }
        #endregion
    }
}
