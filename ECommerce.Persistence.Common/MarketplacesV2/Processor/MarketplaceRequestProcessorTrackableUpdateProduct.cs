﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorTrackableUpdateProduct : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly IMarketplaceRequestWriter _marketplaceRequestWriter;

        private readonly MarketplaceV2UpdateProductTrackableResolver _marketplaceV2ProductTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorTrackableUpdateProduct(IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, IMarketplaceRequestWriter marketplaceRequestWriter, MarketplaceV2UpdateProductTrackableResolver marketplaceV2ProductTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder, INotificationService notificationService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceRequestWriter = marketplaceRequestWriter;
            this._marketplaceV2ProductTrackableResolver = marketplaceV2ProductTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            _dbNameFinder = dbNameFinder;
            _notificationService = notificationService;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceUpdateProductRequests = await _marketplaceRequestReader.ReadAsync(
                    new MarketplaceRequestReaderRequest<MarketplaceRequestUpdateProduct>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplaceUpdateProductRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2ProductTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceUpdateProductRequests.Count; i++)
                {
                    var marketplaceUpdateProductRequest = marketplaceUpdateProductRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerUpdateProductRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerUpdateProductRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var trackableResponse = await marketplace.SendAsync(marketplaceUpdateProductRequest);

                        await this.NotificationAsync(marketplaceUpdateProductRequest, trackableResponse);

                        this.Delete(marketplaceUpdateProductRequest, trackableResponse);

                        await this.WriteAsync(marketplaceUpdateProductRequest, trackableResponse);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Trackable Update Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceUpdateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Trackable Update Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private void Log(MarketplaceRequestUpdateProduct marketplaceRequestUpdateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {

        }

        private async Task NotificationAsync(MarketplaceRequestUpdateProduct marketplaceRequestUpdateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest &&
                httpHelperV3Response.Content.Messages.HasItem())
            {
                var message = string.Join(" ", httpHelperV3Response.Content.Messages);

                foreach (var theItem in marketplaceRequestUpdateProduct.Items)
                {
                    foreach (var theProductInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        await this._notificationService.PostAsync(new()
                        {
                            Type = "UPR",
                            Title = "Ürün Güncelleme",
                            ProductInformationMarketplaceId = theProductInformationMarketplace.ProductInformationMarketplaceId,
                            Message = message,
                            MarketplaceId = marketplaceRequestUpdateProduct.MarketplaceRequestHeader.MarketplaceId,
                            Brand = this._dbNameFinder.FindName(),
                            CompanyId = marketplaceRequestUpdateProduct.MarketplaceRequestHeader.CompanyId,
                            CreatedDateTime = DateTime.Now,
                            PhotoUrl = theProductInformationMarketplace.ProductInformationPhotos.Count > 0 ? theProductInformationMarketplace.ProductInformationPhotos.FirstOrDefault().Url : "",
                            Barcode = theProductInformationMarketplace.Barcode,
                            ProductId = theItem.ProductId,
                            StockCode = theProductInformationMarketplace.StockCode,
                            ProductInformationId = theProductInformationMarketplace.ProductInformationId,
                            ModelCode = theItem.SellerCode,
                            ProductInformationName = $"{theProductInformationMarketplace.ProductInformationName} {theProductInformationMarketplace.VariantValuesDescription}"
                        });
                    }
                }
            }
        }

        private async Task WriteAsync(MarketplaceRequestUpdateProduct marketplaceRequestUpdateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                marketplaceRequestUpdateProduct.TrackingId = httpHelperV3Response.Content.TrackingId;

                await this._marketplaceRequestWriter.WriteAsync(marketplaceRequestUpdateProduct);
            }
        }

        private void Delete(MarketplaceRequestUpdateProduct marketplaceRequestUpdateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceRequestUpdateProduct.Path);
        }
        #endregion
    }
}
