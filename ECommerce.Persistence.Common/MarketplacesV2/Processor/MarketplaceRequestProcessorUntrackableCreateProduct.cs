﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorUntrackableCreateProduct : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2CreateProductUntrackableResolver _marketplaceV2ProductUntrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorUntrackableCreateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2CreateProductUntrackableResolver marketplaceV2ProductUntrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2ProductUntrackableResolver = marketplaceV2ProductUntrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceCreateProductRequests = await _marketplaceRequestReader
                    .ReadAsync(new MarketplaceRequestReaderRequest<MarketplaceRequestCreateProduct>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplaceCreateProductRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2ProductUntrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceCreateProductRequests.Count; i++)
                {
                    var marketplaceCreateProductRequest = marketplaceCreateProductRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerCreateProductRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerCreateProductRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.SendAsync(marketplaceCreateProductRequest);

                        await this.NotificationAsync(marketplaceCreateProductRequest, httpHelperV3Response);

                        this.Update(marketplaceCreateProductRequest, httpHelperV3Response);

                        this.Delete(marketplaceCreateProductRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Untrackable Create Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceCreateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Untrackable Create Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<UntrackableCreateProductProcessResponse> httpHelperV3Response)
        {
            var _ = httpHelperV3Response.Content.UntrackableProcessResponseItems.Where(upri => upri.Messages.HasItem()).ToList();
            foreach (var theResponseItem in _)
            {
                var item = marketplaceCreateProductRequest
                    .Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaces.HasItem()
                        && i
                            .ProductInformationMarketplaces
                            .Any(pim => pim.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId));

                var productInformationMarketplace = item
                    ?.ProductInformationMarketplaces
                    .FirstOrDefault(pim => pim.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "CPR",
                    Title = "Ürün Yükleme",
                    ProductInformationMarketplaceId = theResponseItem.ProductInformationMarketplaceId,
                    Message = string.Join(" ", theResponseItem.Messages),
                    MarketplaceId = marketplaceCreateProductRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplaceCreateProductRequest.MarketplaceRequestHeader.CompanyId,
                    CreatedDateTime = DateTime.Now,
                    PhotoUrl = productInformationMarketplace?.ProductInformationPhotos?.Count > 0 ? productInformationMarketplace.ProductInformationPhotos.FirstOrDefault().Url : "",
                    Barcode = productInformationMarketplace?.Barcode,
                    ProductInformationName = productInformationMarketplace?.ProductInformationName,
                    ModelCode = item?.SellerCode,
                    ProductInformationId = productInformationMarketplace?.ProductInformationId ?? 0,
                    ProductId = item?.ProductId ?? 0,
                    StockCode = productInformationMarketplace?.StockCode
                });
            }

        }

        private void Update(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<UntrackableCreateProductProcessResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                if (httpHelperV3Response.Content.IncludeUrl)
                {
                    var productInformationMarketplaceIds = httpHelperV3Response
                        .Content
                        .UntrackableProcessResponseItems
                        .Where(upri => upri.Success)
                        .ToDictionary(upri => upri.ProductInformationMarketplaceId, upri => upri.Url);

                    this
                        ._unitOfWork
                        .ProductInformationMarketplaceRepository
                        .UpdateOpenedUrl(productInformationMarketplaceIds, true, httpHelperV3Response.Content.MarkDirtyStock, httpHelperV3Response.Content.MarkDirtyPrice);
                }
                else
                {
                    var productInformationMarketplaceIds = httpHelperV3Response
                        .Content
                        .UntrackableProcessResponseItems
                        .Where(upri => upri.Success)
                        .Select(upri => upri.ProductInformationMarketplaceId)
                        .ToList();

                    this
                        ._unitOfWork
                        .ProductInformationMarketplaceRepository
                        .UpdateOpened(productInformationMarketplaceIds, true);
                }
            }
        }

        private void Delete(MarketplaceRequestCreateProduct marketplaceCreateProductRequest, HttpHelperV3Response<UntrackableCreateProductProcessResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceCreateProductRequest.Path);
        }
        #endregion
    }
}
