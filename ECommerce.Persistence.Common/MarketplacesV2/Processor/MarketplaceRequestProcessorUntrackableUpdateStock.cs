﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorUntrackableUpdateStock : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2StockPriceUntrackableResolver _marketplaceV2StockPriceUntrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorUntrackableUpdateStock(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2StockPriceUntrackableResolver marketplaceV2StockPriceUntrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2StockPriceUntrackableResolver = marketplaceV2StockPriceUntrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                await Console.Out.WriteLineAsync("Reading requests...");

                var marketplaceStockRequests = await _marketplaceRequestReader
                    .ReadAsync(
                        new MarketplaceRequestReaderRequest<MarketplaceRequestUpdateStock>
                        {
                            QueueId = request.QueueId,
                            MarketplaceId = request.MarketplaceId
                        });

                await Console.Out.WriteLineAsync($"{marketplaceStockRequests.Count} requests found.");

                if (marketplaceStockRequests.HasItem() == false)
                {
                    return;
                }

                var marketplace = _marketplaceV2StockPriceUntrackableResolver(request.MarketplaceId);

                await Console.Out.WriteLineAsync(marketplace is null ? "Marketplace not found!" : "Marketplace found...");

                if (marketplace is null)
                {
                    return;
                }

                for (int i = 0; i < marketplaceStockRequests.Count; i++)
                {
                    await Console.Out.WriteLineAsync($"Request {i + 1} processing...");

                    var marketplaceStockRequest = marketplaceStockRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerStockRequest.HasValue)
                    {
                        await Console.Out.WriteLineAsync($"Waiting for {marketplace.WaitingSecondPerStockRequest.Value} seconds...");

                        Thread.Sleep(1000 * marketplace.WaitingSecondPerStockRequest.Value);
                    }

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        await Console.Out.WriteLineAsync($"Request {i + 1} sending...");

                        var httpHelperV3Response = await marketplace.SendAsync(marketplaceStockRequest);

                        await Console.Out.WriteLineAsync($"Request {i + 1} sent...");
                        await Console.Out.WriteLineAsync($"HttpStatusCode:\t{httpHelperV3Response.HttpStatusCode}");
                        await Console.Out.WriteLineAsync($"Content String:\t{httpHelperV3Response.ContentString}");

                        await Console.Out.WriteLineAsync($"Request {i + 1} notifing...");
                        await this.NotificationAsync(marketplaceStockRequest, httpHelperV3Response);
                        await Console.Out.WriteLineAsync($"Request {i + 1} notified...");

                        await Console.Out.WriteLineAsync($"Request {i + 1} updating...");
                        this.Update(marketplaceStockRequest, httpHelperV3Response);
                        await Console.Out.WriteLineAsync($"Request {i + 1} updated...");

                        await Console.Out.WriteLineAsync($"Request {i + 1} deleting...");
                        this.Delete(marketplaceStockRequest, httpHelperV3Response);
                        await Console.Out.WriteLineAsync($"Request {i + 1} deleted...");
                    }, async (e) =>
                    {
                        await Console.Out.WriteLineAsync("ERROR!!!");
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Untrackable Update Stock Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceStockRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Untrackable Update Stock",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestUpdateStock marketplaceRequestUpdateStock, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
            var _ = httpHelperV3Response.Content.UntrackableProcessResponseItems.Where(upri => upri.Messages.HasItem()).ToList();
            foreach (var theResponseItem in _)
            {
                var item = marketplaceRequestUpdateStock
                    ?.Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "US",
                    Title = "Stok",
                    ProductInformationMarketplaceId = theResponseItem.ProductInformationMarketplaceId,
                    Message = string.Join(" ", theResponseItem.Messages),
                    MarketplaceId = marketplaceRequestUpdateStock.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplaceRequestUpdateStock.MarketplaceRequestHeader.CompanyId,
                    CreatedDateTime = DateTime.Now,
                    PhotoUrl = item?.PhotoUrl,
                    Barcode = item?.Barcode,
                    StockCode = item?.StockCode,
                    ProductId = item?.ProductId ?? 0,
                    ProductInformationId = item?.ProductInformationId ?? 0,
                    ModelCode = item?.SellerCode,
                    ProductInformationName = $"{item?.ProductInformationName} {item?.VariantValuesDescription}"
                });
            }

        }

        private void Update(MarketplaceRequestUpdateStock marketplaceRequestUpdateStock, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
        }

        private void Delete(MarketplaceRequestUpdateStock marketplaceStockRequest, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceStockRequest.Path);
        }
        #endregion
    }
}
