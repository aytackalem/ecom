﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorTrackableCreateProduct : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly IMarketplaceRequestWriter _marketplaceRequestWriter;

        private readonly MarketplaceV2CreateProductTrackableResolver _marketplaceV2ProductTrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorTrackableCreateProduct(IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, IMarketplaceRequestWriter marketplaceRequestWriter, MarketplaceV2CreateProductTrackableResolver marketplaceV2ProductTrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder, INotificationService notificationService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceRequestWriter = marketplaceRequestWriter;
            this._marketplaceV2ProductTrackableResolver = marketplaceV2ProductTrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            _dbNameFinder = dbNameFinder;
            _notificationService = notificationService;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceCreateProductRequests = await _marketplaceRequestReader.ReadAsync(
                    new MarketplaceRequestReaderRequest<MarketplaceRequestCreateProduct>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplaceCreateProductRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2ProductTrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceCreateProductRequests.Count; i++)
                {
                    var marketplaceCreateProductRequest = marketplaceCreateProductRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerCreateProductRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerCreateProductRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var trackableResponse = await marketplace.SendAsync(marketplaceCreateProductRequest);

                        await this.NotificationAsync(marketplaceCreateProductRequest, trackableResponse);

                       this.Delete(marketplaceCreateProductRequest, trackableResponse);

                        await this.WriteAsync(marketplaceCreateProductRequest, trackableResponse);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Trackable Create Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceCreateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Trackable Create Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private void Log(MarketplaceRequestCreateProduct marketplaceRequestCreateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {

        }

        private async Task NotificationAsync(MarketplaceRequestCreateProduct marketplaceRequestCreateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest && httpHelperV3Response.Content != null &&
                httpHelperV3Response.Content.Messages.HasItem())
            {
                var message = string.Join(" ", httpHelperV3Response.Content.Messages);

                foreach (var theItem in marketplaceRequestCreateProduct.Items)
                {
                    foreach (var theProductInformationMarketplace in theItem.ProductInformationMarketplaces)
                    {
                        await this._notificationService.PostAsync(new()
                        {
                            Type = "CPR",
                            Title = "Ürün Yükleme",
                            ProductInformationMarketplaceId = theProductInformationMarketplace.ProductInformationMarketplaceId,
                            Message = message,
                            MarketplaceId = marketplaceRequestCreateProduct.MarketplaceRequestHeader.MarketplaceId,
                            Brand = this._dbNameFinder.FindName(),
                            CompanyId = marketplaceRequestCreateProduct.MarketplaceRequestHeader.CompanyId,
                            CreatedDateTime = DateTime.Now,
                            PhotoUrl = theProductInformationMarketplace.ProductInformationPhotos.Count > 0 ? theProductInformationMarketplace.ProductInformationPhotos.FirstOrDefault().Url : "",
                            Barcode = theProductInformationMarketplace.Barcode,
                            ProductId = theItem.ProductId,
                            StockCode = theProductInformationMarketplace.StockCode,
                            ProductInformationName = $"{theProductInformationMarketplace.ProductInformationName} {theProductInformationMarketplace.VariantValuesDescription}",
                            ModelCode = theItem.SellerCode,
                            ProductInformationId = theProductInformationMarketplace.ProductInformationId
                        });
                    }
                }
            }
        }

        private async Task WriteAsync(MarketplaceRequestCreateProduct marketplaceRequestCreateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            if (httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.OK || httpHelperV3Response.HttpStatusCode == System.Net.HttpStatusCode.Created)
            {
                marketplaceRequestCreateProduct.TrackingId = httpHelperV3Response.Content.TrackingId;

                await this._marketplaceRequestWriter.WriteAsync(marketplaceRequestCreateProduct);
            }
        }

        private void Delete(MarketplaceRequestCreateProduct marketplaceRequestCreateProduct, HttpHelperV3Response<TrackableResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceRequestCreateProduct.Path);
        }
        #endregion
    }
}
