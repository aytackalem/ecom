﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorUntrackableUpdateProduct : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2UpdateProductUntrackableResolver _marketplaceV2ProductUntrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorUntrackableUpdateProduct(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2UpdateProductUntrackableResolver marketplaceV2ProductUntrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2ProductUntrackableResolver = marketplaceV2ProductUntrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplaceUpdateProductRequests = await _marketplaceRequestReader.ReadAsync(new MarketplaceRequestReaderRequest<MarketplaceRequestUpdateProduct>
                {
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId
                });

                if (marketplaceUpdateProductRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2ProductUntrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplaceUpdateProductRequests.Count; i++)
                {
                    var marketplaceUpdateProductRequest = marketplaceUpdateProductRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerUpdateProductRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerUpdateProductRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.SendAsync(marketplaceUpdateProductRequest);

                        await this.NotificationAsync(marketplaceUpdateProductRequest, httpHelperV3Response);

                        this.Delete(marketplaceUpdateProductRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Untrackable Update Product Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplaceUpdateProductRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Untrackable Update Product",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestUpdateProduct marketplaceUpdateProductRequest, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
            var _ = httpHelperV3Response.Content.UntrackableProcessResponseItems.Where(upri => upri.Messages.HasItem()).ToList();
            foreach (var theResponseItem in _)
            {
                var item = marketplaceUpdateProductRequest
                    .Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaces.HasItem()
                        && i
                            .ProductInformationMarketplaces
                            .Any(pim => pim.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId));

                var productInformationMarketplace = item
                    ?.ProductInformationMarketplaces
                    .FirstOrDefault(pim => pim.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "UPR",
                    Title = "Ürün Güncelleme",
                    ProductInformationMarketplaceId = theResponseItem.ProductInformationMarketplaceId,                    
                    Message = string.Join(" ", theResponseItem.Messages),
                    MarketplaceId = marketplaceUpdateProductRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplaceUpdateProductRequest.MarketplaceRequestHeader.CompanyId,
                    CreatedDateTime = DateTime.Now,
                    PhotoUrl = productInformationMarketplace?.ProductInformationPhotos?.Count > 0 ? productInformationMarketplace.ProductInformationPhotos.FirstOrDefault().Url : "",
                    Barcode = productInformationMarketplace?.Barcode,
                    ProductInformationName = productInformationMarketplace?.ProductInformationName,
                    ModelCode = item?.SellerCode,
                    ProductInformationId = productInformationMarketplace?.ProductInformationId ?? 0,
                    ProductId = item?.ProductId ?? 0,
                    StockCode = productInformationMarketplace?.StockCode
                });
            }

        }

        private void Delete(MarketplaceRequestUpdateProduct marketplaceUpdateProductRequest, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceUpdateProductRequest.Path);
        }
        #endregion
    }
}
