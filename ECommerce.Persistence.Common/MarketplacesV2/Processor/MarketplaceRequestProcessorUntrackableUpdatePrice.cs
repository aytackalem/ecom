﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Resolvers;
using ECommerce.Application.Common.Interfaces.Notification;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.MarketplacesV2;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Common.Wrappers.HttpHelperV3;
using ECommerce.Application.Common.Wrappers.MarketplacesV2;
using ECommerce.Domain.Entities.Companyable;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ECommerce.Persistence.Common.MarketplacesV2.Processor
{
    public class MarketplaceRequestProcessorUntrackableUpdatePrice : IMarketplaceRequestProcessor
    {
        #region Fields
        private readonly INotificationService _notificationService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMarketplaceRequestReader _marketplaceRequestReader;

        private readonly MarketplaceV2StockPriceUntrackableResolver _marketplaceV2StockPriceUntrackableResolver;

        private readonly IMarketplaceErrorFacade _marketplaceErrorFacade;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public MarketplaceRequestProcessorUntrackableUpdatePrice(INotificationService notificationService, IUnitOfWork unitOfWork, IMarketplaceRequestReader marketplaceRequestReader, MarketplaceV2StockPriceUntrackableResolver marketplaceV2StockPriceUntrackableResolver, IMarketplaceErrorFacade marketplaceErrorFacade, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._notificationService = notificationService;
            this._unitOfWork = unitOfWork;
            this._marketplaceRequestReader = marketplaceRequestReader;
            this._marketplaceV2StockPriceUntrackableResolver = marketplaceV2StockPriceUntrackableResolver;
            this._marketplaceErrorFacade = marketplaceErrorFacade;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task ProcessAsync(MarketplaceRequestProcessorRequest request)
        {
            await ExceptionHandlerV2.HandleAsync(async () =>
            {
                var marketplacePriceRequests = await _marketplaceRequestReader.ReadAsync(
                    new MarketplaceRequestReaderRequest<MarketplaceRequestUpdatePrice>
                    {
                        QueueId = request.QueueId,
                        MarketplaceId = request.MarketplaceId
                    });

                if (marketplacePriceRequests.HasItem() == false)
                    return;

                var marketplace = _marketplaceV2StockPriceUntrackableResolver(request.MarketplaceId);
                for (int i = 0; i < marketplacePriceRequests.Count; i++)
                {
                    var marketplacePriceRequest = marketplacePriceRequests[i];

                    if (i > 0 && marketplace.WaitingSecondPerPriceRequest.HasValue)
                        Thread.Sleep(1000 * marketplace.WaitingSecondPerPriceRequest.Value);

                    await ExceptionHandlerV2.HandleAsync(async () =>
                    {
                        var httpHelperV3Response = await marketplace.SendAsync(marketplacePriceRequest);

                        await this.NotificationAsync(marketplacePriceRequest, httpHelperV3Response);

                        this.Update(marketplacePriceRequest, httpHelperV3Response);

                        this.Delete(marketplacePriceRequest, httpHelperV3Response);
                    }, async (e) =>
                    {
                        await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestItemExceptionNotification
                        {
                            JobName = "Processor",
                            Command = "Untrackable Update Price Item",
                            DbName = _dbNameFinder.FindName(),
                            QueueId = request.QueueId,
                            RequestId = marketplacePriceRequest.Id.ToString(),
                            MarketplaceId = request.MarketplaceId,
                            ErrorText = e.Message
                        });
                    });
                }
            }, async (e) =>
            {
                await this._marketplaceErrorFacade.SendNotification(new MarketplaceRequestExceptionNotification
                {
                    JobName = "Processor",
                    Command = "Untrackable Update Price",
                    DbName = _dbNameFinder.FindName(),
                    QueueId = request.QueueId,
                    MarketplaceId = request.MarketplaceId,
                    ErrorText = e.Message
                });
            });
        }
        #endregion

        #region Helper Methods
        private async Task NotificationAsync(MarketplaceRequestUpdatePrice marketplacePriceRequest, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {

            var _ = httpHelperV3Response.Content.UntrackableProcessResponseItems.Where(upri => upri.Messages.HasItem()).ToList();
            foreach (var theResponseItem in _)
            {
                var item = marketplacePriceRequest
                    ?.Items
                    .FirstOrDefault(i => i.ProductInformationMarketplaceId == theResponseItem.ProductInformationMarketplaceId);

                await this._notificationService.PostAsync(new()
                {
                    Type = "UP",
                    Title = "Fiyat",
                    ProductInformationMarketplaceId = theResponseItem.ProductInformationMarketplaceId,
                    Message = string.Join(" ", theResponseItem.Messages),
                    MarketplaceId = marketplacePriceRequest.MarketplaceRequestHeader.MarketplaceId,
                    Brand = this._dbNameFinder.FindName(),
                    CompanyId = marketplacePriceRequest.MarketplaceRequestHeader.CompanyId,
                    CreatedDateTime = DateTime.Now,
                    PhotoUrl = item?.PhotoUrl,
                    Barcode = item?.Barcode,
                    StockCode = item?.StockCode,
                    ProductId = item?.ProductId ?? 0,
                    ProductInformationId = item?.ProductInformationId ?? 0,
                    ModelCode = item?.SellerCode,
                    ProductInformationName = $"{item?.ProductInformationName} {item?.VariantValuesDescription}"
                });
            }

        }

        private void Update(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {

        }

        private void Delete(MarketplaceRequestUpdatePrice marketplaceRequestUpdatePrice, HttpHelperV3Response<UntrackableProcessResponse> httpHelperV3Response)
        {
            File.Delete(marketplaceRequestUpdatePrice.Path);
        }
        #endregion
    }
}
