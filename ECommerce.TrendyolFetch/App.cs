﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ECommerce.TrendyolFetch
{
    public class App : IApp
    {
        private readonly IUnitOfWork _unitOfWork;

        public App(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }

        public void Run()
        {
            var a = string.Join("\n", this._unitOfWork.MarketplaceCompanyRepository.DbSet().Include(x => x.Marketplace).Include(x => x.MarketplaceConfigurations).ToList().SelectMany(x => x.MarketplaceConfigurations.Select(y => $"{x.Marketplace.Name} {y.Key}: {y.Value}")));

            var b = string.Join("\n", this._unitOfWork.AccountingCompanyConfigurationRepository.DbSet().ToList().Select(x => $"{x.Key}: {x.Value}"));

            CategoryRoot root = null;

            using (var httpClient = new HttpClient())
            {
                httpClient
                    .DefaultRequestHeaders
                    .Authorization = new AuthenticationHeaderValue(
                            "Basic",
                            Convert.ToBase64String(Encoding.UTF8.GetBytes($"M8dmuma1PgcSPxGitzem:W2U410btxIbYeEY3Li1b"))
                        );
                var httpResponseMessage = httpClient.GetAsync($"https://api.trendyol.com/sapigw/product-categories").Result;
                var content = httpResponseMessage.Content;
                var responseString = content.ReadAsStringAsync().Result;

                try
                {
                    root = JsonConvert.DeserializeObject<CategoryRoot>(responseString);
                }
                catch (Exception e)
                {
                }
            }

            if (root != null)
            {
                var category = root.categories.FirstOrDefault(r => r.id == 368);
                if (category != null)
                {
                    Recursive(category);
                }
            }
        }

        private void Recursive(SubCategory category)
        {
            if (category.subCategories == null || category.subCategories.Count == 0)
            {
                CategoryAttributeRoot categoryAttributeRoot = null;

                using (var httpClient = new HttpClient())
                {
                    httpClient
                        .DefaultRequestHeaders
                        .Authorization = new AuthenticationHeaderValue(
                                "Basic",
                                Convert.ToBase64String(Encoding.UTF8.GetBytes($"M8dmuma1PgcSPxGitzem:W2U410btxIbYeEY3Li1b"))
                            );
                    var httpResponseMessage = httpClient.GetAsync($"https://api.trendyol.com/sapigw/product-categories/{category.id}/attributes").Result;
                    var content = httpResponseMessage.Content;
                    var responseString = content.ReadAsStringAsync().Result;

                    try
                    {
                        categoryAttributeRoot = JsonConvert.DeserializeObject<CategoryAttributeRoot>(responseString);
                    }
                    catch (Exception e)
                    {
                    }
                }

                //var marketplaceCategory = new Domain.Entities.MarketPlaceCategory
                //{
                //    Name = category.name,
                //    Code = category.id.ToString(),
                //    ParentCode = category.parentId.ToString(),
                //    MarketPlaceId = "TY"
                //};

                //if (categoryAttributeRoot != null)
                //{
                //    marketplaceCategory.MarketplaceCategoryVariants = categoryAttributeRoot
                //        .categoryAttributes
                //        .Select(x => new Domain.Entities.MarketPlaceCategoryVariant
                //        {
                //            Code = x.attribute.id.ToString(),
                //            Name = x.attribute.name,
                //            Mandatory = x.required,
                //            MarketplaceCategoryVariantValues = x
                //                .attributeValues
                //                .Select(y => new Domain.Entities.MarketPlaceCategoryVariantValue
                //                {
                //                    Code = y.id.ToString(),
                //                    Value = y.name
                //                })
                //                .ToList()
                //        })
                //        .ToList();
                //}

                //this
                //    ._unitOfWork.MarketplaceCategoryRepository
                //    .Create(marketplaceCategory);

                //TO DO: Leaf
            }

            foreach (var scLoop in category.subCategories)
            {
                Recursive(scLoop);
            }
        }
    }

    public class CategoryRoot
    {
        public List<SubCategory> categories { get; set; }
    }

    public class SubCategory
    {
        public int id { get; set; }
        public string name { get; set; }
        public int? parentId { get; set; }
        public List<SubCategory> subCategories { get; set; }
    }

    public class Attribute
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class AttributeValue
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class CategoryAttribute
    {
        public bool allowCustom { get; set; }
        public Attribute attribute { get; set; }
        public List<AttributeValue> attributeValues { get; set; }
        public int categoryId { get; set; }
        public bool required { get; set; }
        public bool varianter { get; set; }
        public bool slicer { get; set; }
    }

    public class CategoryAttributeRoot
    {
        public int id { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public List<CategoryAttribute> categoryAttributes { get; set; }
    }
}
