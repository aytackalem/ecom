﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request.Base
{
    public interface IConfigurableRequest
    {
        #region Properties
        public Dictionary<string, string> Configurations { get; set; }
        #endregion
    }
}
