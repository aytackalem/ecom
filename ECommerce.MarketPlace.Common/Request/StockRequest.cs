﻿using ECommerce.MarketPlace.Common.Request.Stock;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request
{
    public class StockRequest
    {

        public Dictionary<string, string> Configurations { get; set; }

        public PriceStock PriceStock { get; set; }

        public string UUId { get; set; }

        public string MarketPlaceId { get; set; }

    }
}
