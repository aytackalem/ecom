﻿using ECommerce.MarketPlace.Common.Request.Base;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request
{
    public class OrderRequest : IConfigurableRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        /// <summary>
        /// Market yerinde sipariş numarasına göre filtreleme yapar
        /// </summary>
        public string MarketPlaceOrderCode { get; set; }

        /// <summary>
        /// Checkprepring için kulanılır
        /// </summary>
        public string TrackingCode { get; set; }

        /// <summary>
        /// Hangi Market yerinin olduğunu belirtir.
        /// </summary>
        public string MarketPlaceId { get; set; }
        #endregion
    }
}
