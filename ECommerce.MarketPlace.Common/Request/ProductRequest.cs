﻿using ECommerce.MarketPlace.Common.Request.Base;
using ECommerce.MarketPlace.Common.Request.Product;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request
{
    /// <summary>
    /// Ürün yükleme için kullanılan model listesi
    /// </summary>
    public class ProductRequest : IConfigurableRequest
    {
        #region Properties
        public string MarketPlaceId { get; set; }

        public Dictionary<string, string> Configurations { get; set; }

        public ProductItem ProductItem { get; set; }
        #endregion
    }
}
