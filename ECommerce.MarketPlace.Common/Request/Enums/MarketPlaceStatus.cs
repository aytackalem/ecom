﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Enums
{
    public enum MarketPlaceStatus
    {
        /// <summary>
        /// Sipariş Çekildi
        /// </summary>
        Picking = 1,

        /// <summary>
        /// Sipariş Paketlendi
        /// </summary>
        Packaging = 2

    }
}
