﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request
{
    /// <summary>
    /// Ürün datalarını çektiğimiz model listesi
    /// </summary>
    public class ProductFilterRequest
    {
        public Dictionary<string, string> Configurations { get; set; }

        public string Barcode { get; set; }

        public string Stockcode { get; set; }

        public int Count { get; set; }

    }
}
