﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Product
{
    public class CategoryAttribute
    {
        #region Property
        public string AttributeCode { get; set; }

        public string AttributeName { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public string AttributeValueCode { get; set; }

        public string AttributeValueName { get; set; }
        #endregion

        #region Navigation Property
        public List<CategoryAttributeValue> CategoryAttributeValues { get; set; }
        #endregion
    }
}
