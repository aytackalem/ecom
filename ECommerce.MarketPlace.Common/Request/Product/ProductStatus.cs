﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Product
{
    public class ProductStatus
    {
        public string ProviderId { get; set; }

        public bool Active { get; set; }
    }
}
