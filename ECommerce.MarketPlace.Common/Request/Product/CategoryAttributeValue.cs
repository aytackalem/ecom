﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Product
{
    public class CategoryAttributeValue
    {
        #region Property
        public string Code { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
