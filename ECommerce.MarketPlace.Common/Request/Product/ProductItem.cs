﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request.Product
{
    public class ProductItem
    {
        public string UUId { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Varianların tamımının ortak ürün satış kodu
        /// </summary>
        public string SellerCode { get; set; }

        /// <summary>
        /// Ürün markası
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Ürün Marka Id'si
        /// </summary>
        public string BrandId { get; set; }

   

        /// <summary>
        /// Pazar yerine göre ürün kategorisi
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// Ürün kargo verilme süresi gün olarak
        /// </summary>
        public int PreparingDayCount { get; set; }

        /// <summary>
        /// Ürün Desi Değeri
        /// </summary>
        public double DimensionalWeight { get; set; }
        
        /// <summary>
        /// Variant'a göre ürün listesi
        /// </summary>
        public List<StockItem> StockItems { get; set; }

    }
}
