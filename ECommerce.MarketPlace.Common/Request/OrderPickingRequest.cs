﻿using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Request.Order;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Request
{
    public class OrderPickingRequest
    {
        #region Properties
        /// <summary>
        /// Market yerninin configrasyon bilgilerini içerir
        /// </summary>
        public Dictionary<string, string> Configurations { get; set; }

        public string MarketPlaceId { get; set; }
        #endregion

        #region Navigation Properties
        public MarketPlaceStatus Status { get; set; }

        /// <summary>
        /// Pazaryerlerinden dönen sipariş detay dataları
        /// </summary>
        public OrderPicking OrderPicking { get; set; }
        #endregion
    }
}
