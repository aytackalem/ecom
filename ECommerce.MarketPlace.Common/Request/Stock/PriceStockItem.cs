﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Stock
{
    public class PriceStockItem
    {

        #region Properties
        public string Barcode { get; set; }

        public int Quantity { get; set; }

        public decimal? ListPrice { get; set; }

        public decimal? SalePrice { get; set; }

        public string StockCode { get; set; }
        #endregion
    }
}
