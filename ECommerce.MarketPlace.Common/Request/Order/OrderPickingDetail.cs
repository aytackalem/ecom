﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Request.Order
{
    /// <summary>
    /// Pazaryerlerinden dönen sipariş detay dataları
    /// </summary>
    public class OrderPickingDetail
    {
        public string Id { get; set; }

        public int Quantity { get; set; }
    }
}
