﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;

namespace ECommerce.MarketPlace.Common.Facade
{
    public interface IOrderFacade
    {
        #region Methods
        DataResponse<OrderResponse> GetDelivered(OrderRequest request);

        DataResponse<OrderResponse> GetCancelled(OrderRequest request);

        DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest);

        DataResponse<OrderResponse> Get(OrderRequest request);

        DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest request);

        Application.Common.Wrappers.Response OrderTypeUpdate(OrderPickingRequest request);
        #endregion
    }
}
