﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Request;

namespace ECommerce.MarketPlace.Common.Facade
{
    public interface IProductFacade
    {
        Application.Common.Wrappers.Response Create(ProductRequest productRequest);        

        Application.Common.Wrappers.Response UpdateStock(StockRequest stockRequest);

        Application.Common.Wrappers.Response UpdatePrice(StockRequest stockRequest);
    }
}
