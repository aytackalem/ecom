﻿using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Response;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Facade
{
    public interface ICategoryFacade
    {
        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations);
        #endregion
    }
}
