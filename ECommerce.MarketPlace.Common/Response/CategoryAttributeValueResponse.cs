﻿namespace ECommerce.MarketPlace.Common.Response
{
    public class CategoryAttributeValueResponse
    {
        #region Properties
        public string VarinatCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        #endregion
    }
}
