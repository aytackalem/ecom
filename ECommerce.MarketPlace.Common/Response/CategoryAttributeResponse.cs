﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Response
{
    public class CategoryAttributeResponse
    {
        public CategoryAttributeResponse()
        {
            CategoryAttributesValues = new List<CategoryAttributeValueResponse>();
        }

        #region Properties
        public string CategoryCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool AllowCustom { get; set; }

        public bool MultiValue { get; set; }

        public bool Mandatory { get; set; }
        #endregion

        #region Navigation Properties
        public List<CategoryAttributeValueResponse> CategoryAttributesValues { get; set; }
        #endregion
    }
}
