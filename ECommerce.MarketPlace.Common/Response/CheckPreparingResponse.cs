﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response
{
    public class CheckPreparingResponse
    {
        public bool IsCancel { get; set; }
    }
}
