﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Common.Response
{
    public class OrderResponse
    {
        #region Property
        public List<Order.Order> Orders { get; set; }
        #endregion
    }
}
