﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response.Order
{
    public class Product
    {
        #region Property
        public string Barcode { get; set; }

        public string Name { get; set; }

        public string StockCode { get; set; }

        /// <summary>
        /// ürünü yükledikten sonra maplenir
        /// </summary>
        public int ProductInformationId { get; set; }
        #endregion
    }
}
