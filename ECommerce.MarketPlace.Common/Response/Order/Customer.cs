﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response.Order
{
    public class Customer
    {
        #region Property
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdentityNumber { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }

        public string TaxNumber { get; set; }

        public string SourceReferenceId { get; set; }

        public string TaxAdministration { get; set; }
        #endregion
    }
}
