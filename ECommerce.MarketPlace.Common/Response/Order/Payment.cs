﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response.Order
{
    public class Payment
    {
        #region Property
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public string CurrencyId { get; set; }
        public string PaymentTypeId { get; set; }
        #endregion
    }
}
