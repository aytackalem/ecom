﻿using ECommerce.MarketPlace.Common.Request.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response.Order
{
    public class Order
    {
        #region Property
        public string OrderCode { get; set; }

        public decimal CargoFee { get; set; }

        public decimal SurchargeFee { get; set; }

        public string Source { get; set; }

        public string OrderNote { get; set; }

        public DateTime OrderDate { get; set; }

        public string CurrencyId { get; set; }

        public string ProviderId { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal ListTotalAmount { get; set; }

        public string Barcode { get; set; }

        public decimal SurchargeAmount { get; set; }

        public decimal ShippingCost { get; set; }

        public string Shipper { get; set; }

        public decimal Discount { get; set; }

        /// <summary>
        /// Siparişlerin kdv hariç indirim hariç toplam tutarı.
        /// </summary>
        public decimal VatExcListTotal { get => this.ListTotalAmount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Sipariş kdv hariç toplam tutarı.
        /// </summary>
        public decimal VatExcTotal { get => this.TotalAmount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Sipariş kdv hariç toplam indirimi.
        /// </summary>
        public decimal VatExcDiscount { get => this.Discount / (1 + (this.OrderDetails.Sum(x => Convert.ToDecimal(x.TaxRate)) / this.OrderDetails.Count)); }

        /// <summary>
        /// Siparişin toplam komiston tutarı
        /// </summary>
        public decimal CommissionAmount { get => this.OrderDetails.Sum(x => Convert.ToDecimal(x.UnitCommissionAmount) * x.Quantity); }

        public string MarketPlaceId { get; set; }

        public int OrderSourceId { get; set; }

        public string OrderTypeId { get; set; }
        #endregion

        #region Navigation Property
        public Customer Customer { get; set; }

        public OrderDeliveryAddress OrderDeliveryAddress { get; set; }

        public OrderInvoiceAddress OrderInvoiceAddress { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        public OrderShipment OrderShipment { get; set; }

        public List<Payment> Payments { get; set; }

        /// <summary>
        /// Statüs atlatmak için pazaryerlerine ait bilgiler girilir.
        /// </summary>
        public OrderPicking OrderPicking { get; set; }
        #endregion
    }
}
