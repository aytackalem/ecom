﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response.ProductFilter
{
    public class ProductFilterPriceItem
    {
        public int PriceGroupId { get; set; }

        public string ProviderId { get; set; }

        public string SellerStockCode { get; set; }

        public decimal ListPrice { get; set; }

        public decimal SalePrice { get; set; }
    }
}
