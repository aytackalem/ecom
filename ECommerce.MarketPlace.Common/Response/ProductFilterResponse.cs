﻿using ECommerce.MarketPlace.Common.Response.ProductFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Common.Response
{
    public class ProductFilterResponse
    {
        public List<ProductFilterItem> ProductItems { get; set; }
    }
}
