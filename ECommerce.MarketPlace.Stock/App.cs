﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Stock.Base;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Stock
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IProductFacade _productFacade;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IProductFacade productFacade)
        {
            _unitOfWork = unitOfWork;
            _productFacade = productFacade;
        }
        #endregion

        #region Methods
        public void Run()
        {
            var marketPlaces = _unitOfWork
                .MarketplaceCompanyRepository
                .DbSet()
                .Include(x => x.MarketplaceConfigurations)
                .Include(x => x.Marketplace)
                .Where(x => x.Active)
                .ToList();

            var productInformationMarketplaces = _unitOfWork
                  .ProductInformationMarketplaceRepository
                  .DbSet()
                  .Include(x => x.ProductInformation)
                  .ToList();



            foreach (var mpLoop in marketPlaces)
            {
                StockRequest request = new()
                {
                    MarketPlaceId = mpLoop.Marketplace.Id,
                    Configurations = mpLoop
                        .MarketplaceConfigurations
                        .ToDictionary(mc => mc.Key, mc => mc.Value)
                };

                var productInformationMarketProducts = productInformationMarketplaces
                    .Where(x => x.MarketplaceCompanyId == mpLoop.Id)
                    .ToList();
                foreach (var productInformationMarketplace in productInformationMarketProducts)
                {

                    request.PriceStock = new Common.Request.Stock.PriceStock();
                    request.PriceStock = new Common.Request.Stock.PriceStock
                    {
                        StockCode = productInformationMarketplace.StockCode,
                        SkuCode = productInformationMarketplace.ProductInformation.SkuCode,
                        ListPrice = null,
                        SalePrice = null,
                        Barcode = productInformationMarketplace.Barcode,
                        Quantity = productInformationMarketplace.ProductInformation.Stock < 0 ? 0 : productInformationMarketplace.ProductInformation.Stock
                    };
                    var response = _productFacade.UpdateStock(request);
                    if (response.Success)
                    {
                        productInformationMarketplace.ProductInformation.DirtyStock = false;

                    }
                }
            }


        }
        #endregion
    }
}
