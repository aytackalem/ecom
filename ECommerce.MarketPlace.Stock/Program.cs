﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Operation;
using ECommerce.MarketPlace.Stock.Base;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.IO;

namespace ECommerce.MarketPlace.Stock
{
    class Program
    {
        #region Methods
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(appConfig.Tenant.EcommerceConnectionStrings);
            });

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddSingleton<IProductFacade, ProductProxy>();

            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();

            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinderFinder>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ICacheHandler, CacheHandler>();

            serviceCollection.AddInfrastructureServices();
            serviceCollection.AddMemoryCache();

            return serviceCollection.BuildServiceProvider();
        }

        #endregion
    }
}
