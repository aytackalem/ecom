﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace ECommerce.CicekSepetiFetch
{
    public class Attribute
    {
        public int parentId { get; set; }
        public string parentName { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int textLength { get; set; }
    }

    public class Product
    {
        public string productName { get; set; }
        public string productCode { get; set; }
        public string stockCode { get; set; }
        public bool isActive { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public string mainProductCode { get; set; }
        public string productStatusType { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public object mediaLink { get; set; }
        public int deliveryMessageType { get; set; }
        public int deliveryType { get; set; }
        public bool isUseStockQuantity { get; set; }
        public int stockQuantity { get; set; }
        public double salesPrice { get; set; }
        public double listPrice { get; set; }
        public string barcode { get; set; }
        public string commissionRate { get; set; }
        public int numberOfFavorites { get; set; }
        public List<string> images { get; set; }
        public List<Attribute> attributes { get; set; }
    }

    public class Root
    {
        public int totalCount { get; set; }
        public List<Product> products { get; set; }
    }

    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static void Main(string[] args)
        {
            List<Product> products = new List<Product>();

            int totalCount = 0;
            int page = 0;

            WebClient webClient = new WebClient();
            webClient.Headers.Add("x-api-key", "2HB7FRfNgG5eex40kvm2N9RtJhg6S7vc3GBTEqif");

            do
            {

                Console.WriteLine(page);

                var address = $"https://apis.ciceksepeti.com/api/v1/Products?ProductStatus=3&PageSize=36&Page={page}";
                var json = webClient.DownloadString(address);

                var root = JsonConvert.DeserializeObject<Root>(json);

                products.AddRange(root.products);

                totalCount = root.totalCount;

                page++;

            } while (page < Math.Ceiling(totalCount / 36M));

            products.RemoveAll(p => string.IsNullOrEmpty(p.mainProductCode));

            Products(products);
            Categories(products);
            CategoryAttributes(products);
            CategoryAttributeValues(products);
            ProductCategoryAttributeValue(products);
        }

        static void Products(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("ProductName", typeof(string));
            dataTable.Columns.Add("ProductCode", typeof(string));
            dataTable.Columns.Add("StockCode", typeof(string));
            dataTable.Columns.Add("Barcode", typeof(string));
            dataTable.Columns.Add("CategoryId", typeof(int));
            dataTable.Columns.Add("MainProductCode", typeof(string));
            dataTable.Columns.Add("IsActive", typeof(bool));

            products
                .Select(p => new
                {
                    ProductName = p.productName,
                    ProductCode = p.productCode,
                    StockCode = p.stockCode,
                    Barcode = p.barcode,
                    CategoryId = p.categoryId,
                    MainProductCode = p.mainProductCode,
                    IsActive = p.isActive
                })
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.ProductName;
                    dataRow[1] = string.IsNullOrEmpty(p.ProductCode) ? DBNull.Value : p.ProductCode;
                    dataRow[2] = string.IsNullOrEmpty(p.StockCode) ? DBNull.Value : p.StockCode;
                    dataRow[3] = string.IsNullOrEmpty(p.Barcode) ? DBNull.Value : p.Barcode;
                    dataRow[4] = p.CategoryId;
                    dataRow[5] = p.MainProductCode;
                    dataRow[6] = p.IsActive;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "CSProducts";

                sqlBulkCopy.ColumnMappings.Add("ProductName", "ProductName");
                sqlBulkCopy.ColumnMappings.Add("ProductCode", "ProductCode");
                sqlBulkCopy.ColumnMappings.Add("StockCode", "StockCode");
                sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");
                sqlBulkCopy.ColumnMappings.Add("CategoryId", "CategoryId");
                sqlBulkCopy.ColumnMappings.Add("MainProductCode", "MainProductCode");
                sqlBulkCopy.ColumnMappings.Add("IsActive", "IsActive");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void Categories(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .Select(p => new
                {
                    Id = p.categoryId,
                    Name = p.categoryName
                })
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.Id;
                    dataRow[1] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "CSCategories";

                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void CategoryAttributes(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("CategoryId", typeof(int));
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            products
                .SelectMany(p => p.attributes.Select(a => new
                {
                    CategoryId = p.categoryId,
                    Id = a.parentId,
                    Name = a.parentName
                }))
                .Distinct()
                .ToList()
                .ForEach(p =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = p.CategoryId;
                    dataRow[1] = p.Id;
                    dataRow[2] = p.Name;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "CSCategoryAttributes";

                sqlBulkCopy.ColumnMappings.Add("CategoryId", "CategoryId");
                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void CategoryAttributeValues(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("CategoryAttributeId", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Type", typeof(string));

            products
                .SelectMany(p => p.attributes.Select(a => new
                {
                    Id = a.id,
                    CategoryAttributeId = a.parentId,
                    Name = a.name,
                    Type = a.type
                }))
                .Distinct()
                .ToList()
                .ForEach(a =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = a.Id;
                    dataRow[1] = a.CategoryAttributeId;
                    dataRow[2] = a.Name;
                    dataRow[3] = a.Type;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "CSCategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeId", "CategoryAttributeId");
                sqlBulkCopy.ColumnMappings.Add("Name", "Name");
                sqlBulkCopy.ColumnMappings.Add("Type", "Type");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }

        static void ProductCategoryAttributeValue(List<Product> products)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("MainProductCode", typeof(string));
            dataTable.Columns.Add("CSCategoryAttributeValueId", typeof(int));

            products
                .SelectMany(p => p.attributes.Select(a => new
                {
                    MainProductCode = p.mainProductCode,
                    CSCategoryAttributeValueId = a.id
                }))
                .Distinct()
                .ToList()
                .ForEach(a =>
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = a.MainProductCode;
                    dataRow[1] = a.CSCategoryAttributeValueId;

                    dataTable.Rows.Add(dataRow);
                });

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
                sqlBulkCopy.DestinationTableName = "CSProductCategoryAttributeValues";

                sqlBulkCopy.ColumnMappings.Add("MainProductCode", "MainProductCode");
                sqlBulkCopy.ColumnMappings.Add("CSCategoryAttributeValueId", "CSCategoryAttributeValueId");

                try
                {
                    sqlConnection.Open();

                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
