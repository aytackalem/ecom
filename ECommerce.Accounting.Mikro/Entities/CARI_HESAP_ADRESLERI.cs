using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Accounting.Mikro.Entities
{
    [Table("CARI_HESAP_ADRESLERI")]
	public partial class CARI_HESAP_ADRESLERI
	{
		#region Property
		public Guid adr_Guid { get; set; }

		public Int16 adr_DBCno { get; set; }

		public Int32 adr_SpecRECno { get; set; }

		public Boolean adr_iptal { get; set; }

		public Int16 adr_fileid { get; set; }

		public Boolean adr_hidden { get; set; }

		public Boolean adr_kilitli { get; set; }

		public Boolean adr_degisti { get; set; }

		public Int32 adr_checksum { get; set; }

		public Int16 adr_create_user { get; set; }

		public DateTime adr_create_date { get; set; }

		public Int16 adr_lastup_user { get; set; }

		public DateTime adr_lastup_date { get; set; }

		public String adr_special1 { get; set; }

		public String adr_special2 { get; set; }

		public String adr_special3 { get; set; }

		public String adr_cari_kod { get; set; }

		public Int32 adr_adres_no { get; set; }

		public Boolean adr_aprint_fl { get; set; }

		public String adr_cadde { get; set; }

		public String adr_mahalle { get; set; }

		public String adr_sokak { get; set; }

		public String adr_Semt { get; set; }

		public String adr_Apt_No { get; set; }

		public String adr_Daire_No { get; set; }

		public String adr_posta_kodu { get; set; }

		public String adr_ilce { get; set; }

		public String adr_il { get; set; }

		public String adr_ulke { get; set; }

		public String adr_Adres_kodu { get; set; }

		public String adr_tel_ulke_kodu { get; set; }

		public String adr_tel_bolge_kodu { get; set; }

		public String adr_tel_no1 { get; set; }

		public String adr_tel_no2 { get; set; }

		public String adr_tel_faxno { get; set; }

		public String adr_tel_modem { get; set; }

		public String adr_yon_kodu { get; set; }

		public Int16 adr_uzaklik_kodu { get; set; }

		public String adr_temsilci_kodu { get; set; }

		public String adr_ozel_not { get; set; }

		public Byte adr_ziyaretperyodu { get; set; }

		public Double adr_ziyaretgunu { get; set; }

		public Double adr_gps_enlem { get; set; }

		public Double adr_gps_boylam { get; set; }

		public Byte adr_ziyarethaftasi { get; set; }

		public Boolean adr_ziygunu2_1 { get; set; }

		public Boolean adr_ziygunu2_2 { get; set; }

		public Boolean adr_ziygunu2_3 { get; set; }

		public Boolean adr_ziygunu2_4 { get; set; }

		public Boolean adr_ziygunu2_5 { get; set; }

		public Boolean adr_ziygunu2_6 { get; set; }

		public Boolean adr_ziygunu2_7 { get; set; }

		public String adr_efatura_alias { get; set; }

		public String adr_eirsaliye_alias { get; set; }
		#endregion
	}
}