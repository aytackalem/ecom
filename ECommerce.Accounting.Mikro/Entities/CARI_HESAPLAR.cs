using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Accounting.Mikro.Entities
{
    [Table("CARI_HESAPLAR")]
	public partial class CARI_HESAPLAR
	{
		#region Property
		public Guid cari_Guid { get; set; }

		public Int16 cari_DBCno { get; set; }

		public Int32 cari_SpecRECno { get; set; }

		public Boolean cari_iptal { get; set; }

		public Int16 cari_fileid { get; set; }

		public Boolean cari_hidden { get; set; }

		public Boolean cari_kilitli { get; set; }

		public Boolean cari_degisti { get; set; }

		public Int32 cari_checksum { get; set; }

		public Int16 cari_create_user { get; set; }

		public DateTime cari_create_date { get; set; }

		public Int16 cari_lastup_user { get; set; }

		public DateTime cari_lastup_date { get; set; }

		public String cari_special1 { get; set; }

		public String cari_special2 { get; set; }

		public String cari_special3 { get; set; }

		public String cari_kod { get; set; }

		public String cari_unvan1 { get; set; }

		public String cari_unvan2 { get; set; }

		public Byte cari_hareket_tipi { get; set; }

		public Byte cari_baglanti_tipi { get; set; }

		public Byte cari_stok_alim_cinsi { get; set; }

		public Byte cari_stok_satim_cinsi { get; set; }

		public String cari_muh_kod { get; set; }

		public String cari_muh_kod1 { get; set; }

		public String cari_muh_kod2 { get; set; }

		public Byte cari_doviz_cinsi { get; set; }

		public Byte cari_doviz_cinsi1 { get; set; }

		public Byte cari_doviz_cinsi2 { get; set; }

		public Double cari_vade_fark_yuz { get; set; }

		public Double cari_vade_fark_yuz1 { get; set; }

		public Double cari_vade_fark_yuz2 { get; set; }

		public Byte cari_KurHesapSekli { get; set; }

		public String cari_vdaire_adi { get; set; }

		public String cari_vdaire_no { get; set; }

		public String cari_sicil_no { get; set; }

		public String cari_VergiKimlikNo { get; set; }

		public Int32 cari_satis_fk { get; set; }

		public Byte cari_odeme_cinsi { get; set; }

		public Byte cari_odeme_gunu { get; set; }

		public Int32 cari_odemeplan_no { get; set; }

		public Int32 cari_opsiyon_gun { get; set; }

		public Byte cari_cariodemetercihi { get; set; }

		public Int32 cari_fatura_adres_no { get; set; }

		public Int32 cari_sevk_adres_no { get; set; }

		public String cari_banka_tcmb_kod1 { get; set; }

		public String cari_banka_tcmb_subekod1 { get; set; }

		public String cari_banka_tcmb_ilkod1 { get; set; }

		public String cari_banka_hesapno1 { get; set; }

		public String cari_banka_swiftkodu1 { get; set; }

		public String cari_banka_tcmb_kod2 { get; set; }

		public String cari_banka_tcmb_subekod2 { get; set; }

		public String cari_banka_tcmb_ilkod2 { get; set; }

		public String cari_banka_hesapno2 { get; set; }

		public String cari_banka_swiftkodu2 { get; set; }

		public String cari_banka_tcmb_kod3 { get; set; }

		public String cari_banka_tcmb_subekod3 { get; set; }

		public String cari_banka_tcmb_ilkod3 { get; set; }

		public String cari_banka_hesapno3 { get; set; }

		public String cari_banka_swiftkodu3 { get; set; }

		public String cari_banka_tcmb_kod4 { get; set; }

		public String cari_banka_tcmb_subekod4 { get; set; }

		public String cari_banka_tcmb_ilkod4 { get; set; }

		public String cari_banka_hesapno4 { get; set; }

		public String cari_banka_swiftkodu4 { get; set; }

		public String cari_banka_tcmb_kod5 { get; set; }

		public String cari_banka_tcmb_subekod5 { get; set; }

		public String cari_banka_tcmb_ilkod5 { get; set; }

		public String cari_banka_hesapno5 { get; set; }

		public String cari_banka_swiftkodu5 { get; set; }

		public String cari_banka_tcmb_kod6 { get; set; }

		public String cari_banka_tcmb_subekod6 { get; set; }

		public String cari_banka_tcmb_ilkod6 { get; set; }

		public String cari_banka_hesapno6 { get; set; }

		public String cari_banka_swiftkodu6 { get; set; }

		public String cari_banka_tcmb_kod7 { get; set; }

		public String cari_banka_tcmb_subekod7 { get; set; }

		public String cari_banka_tcmb_ilkod7 { get; set; }

		public String cari_banka_hesapno7 { get; set; }

		public String cari_banka_swiftkodu7 { get; set; }

		public String cari_banka_tcmb_kod8 { get; set; }

		public String cari_banka_tcmb_subekod8 { get; set; }

		public String cari_banka_tcmb_ilkod8 { get; set; }

		public String cari_banka_hesapno8 { get; set; }

		public String cari_banka_swiftkodu8 { get; set; }

		public String cari_banka_tcmb_kod9 { get; set; }

		public String cari_banka_tcmb_subekod9 { get; set; }

		public String cari_banka_tcmb_ilkod9 { get; set; }

		public String cari_banka_hesapno9 { get; set; }

		public String cari_banka_swiftkodu9 { get; set; }

		public String cari_banka_tcmb_kod10 { get; set; }

		public String cari_banka_tcmb_subekod10 { get; set; }

		public String cari_banka_tcmb_ilkod10 { get; set; }

		public String cari_banka_hesapno10 { get; set; }

		public String cari_banka_swiftkodu10 { get; set; }

		public Byte cari_EftHesapNum { get; set; }

		public String cari_Ana_cari_kodu { get; set; }

		public String cari_satis_isk_kod { get; set; }

		public String cari_sektor_kodu { get; set; }

		public String cari_bolge_kodu { get; set; }

		public String cari_grup_kodu { get; set; }

		public String cari_temsilci_kodu { get; set; }

		public String cari_muhartikeli { get; set; }

		public Boolean cari_firma_acik_kapal { get; set; }

		public Boolean cari_BUV_tabi_fl { get; set; }

		public Boolean cari_cari_kilitli_flg { get; set; }

		public Boolean cari_etiket_bas_fl { get; set; }

		public Boolean cari_Detay_incele_flg { get; set; }

		public Boolean cari_efatura_fl { get; set; }

		public Double cari_POS_ongpesyuzde { get; set; }

		public Double cari_POS_ongtaksayi { get; set; }

		public Double cari_POS_ongIskOran { get; set; }

		public DateTime cari_kaydagiristarihi { get; set; }

		public Double cari_KabEdFCekTutar { get; set; }

		public Byte cari_hal_caritip { get; set; }

		public Double cari_HalKomYuzdesi { get; set; }

		public Int16 cari_TeslimSuresi { get; set; }

		public String cari_wwwadresi { get; set; }

		public String cari_EMail { get; set; }

		public String cari_CepTel { get; set; }

		public Int32 cari_VarsayilanGirisDepo { get; set; }

		public Int32 cari_VarsayilanCikisDepo { get; set; }

		public Boolean cari_Portal_Enabled { get; set; }

		public String cari_Portal_PW { get; set; }

		public Int32 cari_BagliOrtaklisa_Firma { get; set; }

		public String cari_kampanyakodu { get; set; }

		public Boolean cari_b_bakiye_degerlendirilmesin_fl { get; set; }

		public Boolean cari_a_bakiye_degerlendirilmesin_fl { get; set; }

		public Boolean cari_b_irsbakiye_degerlendirilmesin_fl { get; set; }

		public Boolean cari_a_irsbakiye_degerlendirilmesin_fl { get; set; }

		public Boolean cari_b_sipbakiye_degerlendirilmesin_fl { get; set; }

		public Boolean cari_a_sipbakiye_degerlendirilmesin_fl { get; set; }

		public String cari_AvmBilgileri1KiraKodu { get; set; }

		public Byte cari_AvmBilgileri1TebligatSekli { get; set; }

		public String cari_AvmBilgileri2KiraKodu { get; set; }

		public Byte cari_AvmBilgileri2TebligatSekli { get; set; }

		public String cari_AvmBilgileri3KiraKodu { get; set; }

		public Byte cari_AvmBilgileri3TebligatSekli { get; set; }

		public String cari_AvmBilgileri4KiraKodu { get; set; }

		public Byte cari_AvmBilgileri4TebligatSekli { get; set; }

		public String cari_AvmBilgileri5KiraKodu { get; set; }

		public Byte cari_AvmBilgileri5TebligatSekli { get; set; }

		public String cari_AvmBilgileri6KiraKodu { get; set; }

		public Byte cari_AvmBilgileri6TebligatSekli { get; set; }

		public String cari_AvmBilgileri7KiraKodu { get; set; }

		public Byte cari_AvmBilgileri7TebligatSekli { get; set; }

		public String cari_AvmBilgileri8KiraKodu { get; set; }

		public Byte cari_AvmBilgileri8TebligatSekli { get; set; }

		public String cari_AvmBilgileri9KiraKodu { get; set; }

		public Byte cari_AvmBilgileri9TebligatSekli { get; set; }

		public String cari_AvmBilgileri10KiraKodu { get; set; }

		public Byte cari_AvmBilgileri10TebligatSekli { get; set; }

		public Boolean cari_KrediRiskTakibiVar_flg { get; set; }

		public String cari_ufrs_fark_muh_kod { get; set; }

		public String cari_ufrs_fark_muh_kod1 { get; set; }

		public String cari_ufrs_fark_muh_kod2 { get; set; }

		public Byte cari_odeme_sekli { get; set; }

		public String cari_TeminatMekAlacakMuhKodu { get; set; }

		public String cari_TeminatMekAlacakMuhKodu1 { get; set; }

		public String cari_TeminatMekAlacakMuhKodu2 { get; set; }

		public String cari_TeminatMekBorcMuhKodu { get; set; }

		public String cari_TeminatMekBorcMuhKodu1 { get; set; }

		public String cari_TeminatMekBorcMuhKodu2 { get; set; }

		public String cari_VerilenDepozitoTeminatMuhKodu { get; set; }

		public String cari_VerilenDepozitoTeminatMuhKodu1 { get; set; }

		public String cari_VerilenDepozitoTeminatMuhKodu2 { get; set; }

		public String cari_AlinanDepozitoTeminatMuhKodu { get; set; }

		public String cari_AlinanDepozitoTeminatMuhKodu1 { get; set; }

		public String cari_AlinanDepozitoTeminatMuhKodu2 { get; set; }

		public Byte cari_def_efatura_cinsi { get; set; }

		public Boolean cari_otv_tevkifatina_tabii_fl { get; set; }

		public String cari_KEP_adresi { get; set; }

		public DateTime cari_efatura_baslangic_tarihi { get; set; }

		public String cari_mutabakat_mail_adresi { get; set; }

		public String cari_mersis_no { get; set; }

		public String cari_istasyon_cari_kodu { get; set; }

		public Boolean cari_gonderionayi_sms { get; set; }

		public Boolean cari_gonderionayi_email { get; set; }

		public Boolean cari_eirsaliye_fl { get; set; }

		public DateTime cari_eirsaliye_baslangic_tarihi { get; set; }

		public String cari_vergidairekodu { get; set; }

		public Boolean cari_CRM_sistemine_aktar_fl { get; set; }

		public String cari_efatura_xslt_dosya { get; set; }

		public String cari_pasaport_no { get; set; }

		public Byte cari_kisi_kimlik_bilgisi_aciklama_turu { get; set; }

		public String cari_kisi_kimlik_bilgisi_diger_aciklama { get; set; }

		public String cari_uts_kurum_no { get; set; }

		public Boolean cari_kamu_kurumu_fl { get; set; }

		public String cari_earsiv_xslt_dosya { get; set; }

		public Boolean cari_Perakende_fl { get; set; }

		public Boolean cari_yeni_dogan_mi { get; set; }

		public String cari_eirsaliye_xslt_dosya { get; set; }

		public Byte cari_def_eirsaliye_cinsi { get; set; }
		#endregion
	}
}