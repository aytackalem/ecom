using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce.Accounting.Mikro.Entities
{
    [Table("EVRAK_ACIKLAMALARI")]
	public partial class EVRAK_ACIKLAMALARI
	{
		#region Property
		public Guid egk_Guid { get; set; }

		public Int16 egk_DBCno { get; set; }

		public Int32 egk_SpecRECno { get; set; }

		public Boolean egk_iptal { get; set; }

		public Int16 egk_fileid { get; set; }

		public Boolean egk_hidden { get; set; }

		public Boolean egk_kilitli { get; set; }

		public Boolean egk_degisti { get; set; }

		public Int32 egk_checksum { get; set; }

		public Int16 egk_create_user { get; set; }

		public DateTime egk_create_date { get; set; }

		public Int16 egk_lastup_user { get; set; }

		public DateTime egk_lastup_date { get; set; }

		public String egk_special1 { get; set; }

		public String egk_special2 { get; set; }

		public String egk_special3 { get; set; }

		public Int16 egk_dosyano { get; set; }

		public Byte egk_hareket_tip { get; set; }

		public Byte egk_evr_tip { get; set; }

		public String egk_evr_seri { get; set; }

		public Int32 egk_evr_sira { get; set; }

		public String egk_evr_ustkod { get; set; }

		public Int16 egk_evr_doksayisi { get; set; }

		public String egk_evracik1 { get; set; }

		public String egk_evracik2 { get; set; }

		public String egk_evracik3 { get; set; }

		public String egk_evracik4 { get; set; }

		public String egk_evracik5 { get; set; }

		public String egk_evracik6 { get; set; }

		public String egk_evracik7 { get; set; }

		public String egk_evracik8 { get; set; }

		public String egk_evracik9 { get; set; }

		public String egk_evracik10 { get; set; }

		public Double egk_sipgenkarorani { get; set; }

		public String egk_kargokodu { get; set; }

		public String egk_kargono { get; set; }

		public DateTime egk_tesaltarihi { get; set; }

		public String egk_tesalkisi { get; set; }

		public Int16 egk_prevwiewsayisi { get; set; }

		public Int16 egk_emailsayisi { get; set; }

		public Boolean egk_Evrakopno_verildi_fl { get; set; }
		#endregion
	}
}