﻿using ECommerce.Accounting.Base;
using ECommerce.Accounting.Common;
using ECommerce.Accounting.Mikro.DataTransferObjects;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;

namespace ECommerce.Accounting.Mikro
{
    public class Mikro : IAccounting
    {
        #region Fields

        #endregion

        #region Constructors

        #endregion

        #region Methods
        public DataResponse<AccountingInfo> Create(Order order)
        {
            return ExceptionHandler.ResultHandle<DataResponse<AccountingInfo>>((response) =>
            {
                using (var mikroUnitOfWork = new UnitOfWork.UnitOfWork(order.Company.MikroOfficialConnectionString))
                {
                    try
                    {
                        mikroUnitOfWork.BeginTransaction();

                        #region Check Corporate Customer
                        InvoiceInfo invoiceInfo = new InvoiceInfo
                        {
                            CurrentCode = $"120.{company.MikroOfficialConnectionString}.{order.Customer.Id}", //Bireysel Futura Kodu
                            InvoiceSerie = company.MikroOfficialInvoiceSerie,//E-Arsiv Fatura Serisi
                            IsEinvoice = false, //E-Fatura mı E-Ariv olduğu anlaşılır
                            IsBrokerage = isBrokerage
                        };

                        if (string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxOffice) &&
                            string.IsNullOrEmpty(order.OrderInvoiceInformation.TaxNumber))
                        {

                        } 
                        #endregion

                        var invoiceInfo = GetCurrentCode(order.Customer, order.Company, order.ResponsibilityCenter.IsBrokerage, mikroUnitOfWork);
                        if (invoiceInfo.Failed)
                        {
                            throw new Exception(invoiceInfo.Message);
                        }

                        #region Current Account / Cari Hesaplar
                        var currentAccount = mikroUnitOfWork.CARI_HESAPLARRepository.ReadByCariKod(invoiceInfo.Data.CurrentCode);

                        var taxInfo = GetTaxInfo(order);
                        if (currentAccount == null)
                        {

                            currentAccount = new CARI_HESAPLAR
                            {
                                cari_Guid = Guid.NewGuid(),
                                cari_fileid = 31,
                                cari_create_user = 3,
                                cari_create_date = DateTime.Now.Date,
                                cari_lastup_user = 3,
                                cari_lastup_date = DateTime.Now.Date,
                                cari_kod = invoiceInfo.Data.CurrentCode,
                                cari_unvan1 = order.Customer.FirstName,
                                cari_unvan2 = order.Customer.LastName,
                                cari_muh_kod = "120.01.#SOM#",
                                cari_doviz_cinsi1 = 255,
                                cari_doviz_cinsi2 = 255,
                                cari_vade_fark_yuz = 25,
                                cari_KurHesapSekli = 1,
                                cari_satis_fk = 1,
                                cari_fatura_adres_no = 1,
                                cari_sevk_adres_no = 1,
                                cari_EftHesapNum = 1,
                                cari_kaydagiristarihi = DateTime.Now.Date,
                                cari_TeminatMekAlacakMuhKodu = "910",
                                cari_TeminatMekBorcMuhKodu = "912",
                                cari_VerilenDepozitoTeminatMuhKodu = "226",
                                cari_AlinanDepozitoTeminatMuhKodu = "326",
                                cari_efatura_baslangic_tarihi = new DateTime(1899, 12, 31),
                                cari_eirsaliye_baslangic_tarihi = new DateTime(1899, 12, 31),
                                cari_vdaire_adi = taxInfo.OfficeName,
                                cari_vdaire_no = taxInfo.Number,
                                cari_vergidairekodu = taxInfo.OfficeCode,
                                cari_efatura_fl = invoiceInfo.Data.IsEinvoice,
                                cari_TeminatMekAlacakMuhKodu1 = string.Empty,
                                cari_TeminatMekAlacakMuhKodu2 = string.Empty,
                                cari_TeminatMekBorcMuhKodu1 = string.Empty,
                                cari_TeminatMekBorcMuhKodu2 = string.Empty,
                                cari_VergiKimlikNo = string.Empty,
                                cari_special1 = string.Empty,
                                cari_special2 = string.Empty,
                                cari_special3 = string.Empty,
                                cari_muh_kod1 = string.Empty,
                                cari_muh_kod2 = string.Empty,
                                cari_sicil_no = string.Empty,
                                cari_banka_tcmb_kod1 = string.Empty,
                                cari_banka_tcmb_subekod1 = string.Empty,
                                cari_banka_tcmb_ilkod1 = string.Empty,
                                cari_banka_hesapno1 = string.Empty,
                                cari_banka_swiftkodu1 = string.Empty,
                                cari_banka_tcmb_kod2 = string.Empty,
                                cari_banka_tcmb_subekod2 = string.Empty,
                                cari_banka_tcmb_ilkod2 = string.Empty,
                                cari_banka_hesapno2 = string.Empty,
                                cari_banka_swiftkodu2 = string.Empty,
                                cari_banka_tcmb_kod3 = string.Empty,
                                cari_banka_tcmb_subekod3 = string.Empty,
                                cari_banka_tcmb_ilkod3 = string.Empty,
                                cari_banka_hesapno3 = string.Empty,
                                cari_banka_swiftkodu3 = string.Empty,
                                cari_banka_tcmb_kod4 = string.Empty,
                                cari_banka_tcmb_subekod4 = string.Empty,
                                cari_banka_tcmb_ilkod4 = string.Empty,
                                cari_banka_hesapno4 = string.Empty,
                                cari_banka_swiftkodu4 = string.Empty,
                                cari_banka_tcmb_kod5 = string.Empty,
                                cari_banka_tcmb_subekod5 = string.Empty,
                                cari_banka_tcmb_ilkod5 = string.Empty,
                                cari_banka_hesapno5 = string.Empty,
                                cari_banka_swiftkodu5 = string.Empty,
                                cari_banka_tcmb_kod6 = string.Empty,
                                cari_banka_tcmb_subekod6 = string.Empty,
                                cari_banka_tcmb_ilkod6 = string.Empty,
                                cari_banka_hesapno6 = string.Empty,
                                cari_banka_swiftkodu6 = string.Empty,
                                cari_banka_tcmb_kod7 = string.Empty,
                                cari_banka_tcmb_subekod7 = string.Empty,
                                cari_banka_tcmb_ilkod7 = string.Empty,
                                cari_banka_hesapno7 = string.Empty,
                                cari_banka_swiftkodu7 = string.Empty,
                                cari_banka_tcmb_kod8 = string.Empty,
                                cari_banka_tcmb_subekod8 = string.Empty,
                                cari_banka_tcmb_ilkod8 = string.Empty,
                                cari_banka_hesapno8 = string.Empty,
                                cari_banka_swiftkodu8 = string.Empty,
                                cari_banka_tcmb_kod9 = string.Empty,
                                cari_banka_tcmb_subekod9 = string.Empty,
                                cari_banka_tcmb_ilkod9 = string.Empty,
                                cari_banka_hesapno9 = string.Empty,
                                cari_banka_swiftkodu9 = string.Empty,
                                cari_banka_tcmb_kod10 = string.Empty,
                                cari_banka_tcmb_subekod10 = string.Empty,
                                cari_banka_tcmb_ilkod10 = string.Empty,
                                cari_banka_hesapno10 = string.Empty,
                                cari_banka_swiftkodu10 = string.Empty,
                                cari_Ana_cari_kodu = string.Empty,
                                cari_satis_isk_kod = string.Empty,
                                cari_sektor_kodu = string.Empty,
                                cari_bolge_kodu = string.Empty,
                                cari_grup_kodu = string.Empty,
                                cari_temsilci_kodu = string.Empty,
                                cari_muhartikeli = string.Empty,
                                cari_wwwadresi = string.Empty,
                                cari_EMail = string.IsNullOrEmpty(order.Customer.Mail) ? "earsivmusteri@sinoz.com.tr" : order.Customer.Mail,
                                cari_CepTel = string.Empty,
                                cari_Portal_PW = string.Empty,
                                cari_kampanyakodu = string.Empty,
                                cari_AvmBilgileri1KiraKodu = string.Empty,
                                cari_AvmBilgileri2KiraKodu = string.Empty,
                                cari_AvmBilgileri3KiraKodu = string.Empty,
                                cari_AvmBilgileri4KiraKodu = string.Empty,
                                cari_AvmBilgileri5KiraKodu = string.Empty,
                                cari_AvmBilgileri6KiraKodu = string.Empty,
                                cari_AvmBilgileri7KiraKodu = string.Empty,
                                cari_AvmBilgileri8KiraKodu = string.Empty,
                                cari_AvmBilgileri9KiraKodu = string.Empty,
                                cari_AvmBilgileri10KiraKodu = string.Empty,
                                cari_ufrs_fark_muh_kod = string.Empty,
                                cari_ufrs_fark_muh_kod1 = string.Empty,
                                cari_ufrs_fark_muh_kod2 = string.Empty,
                                cari_mutabakat_mail_adresi = string.Empty,
                                cari_mersis_no = string.Empty,
                                cari_VerilenDepozitoTeminatMuhKodu1 = string.Empty,
                                cari_VerilenDepozitoTeminatMuhKodu2 = string.Empty,
                                cari_AlinanDepozitoTeminatMuhKodu1 = string.Empty,
                                cari_AlinanDepozitoTeminatMuhKodu2 = string.Empty,
                                cari_KEP_adresi = string.Empty,
                                cari_istasyon_cari_kodu = string.Empty,
                                cari_efatura_xslt_dosya = string.Empty,
                                cari_Perakende_fl = false

                            };
                            if (!Create(() => mikroUnitOfWork.CARI_HESAPLARRepository.Create(currentAccount), dataResult, "CARI HESAPLAR", mikroUnitOfWork))
                                throw new Exception("Cari Hesaplar not inserted.");

                        }
                        else
                        {
                            mikroUnitOfWork.CARI_HESAPLARRepository.Update(invoiceInfo.Data.CurrentCode, order.Customer.FirstName, order.Customer.LastName, !string.IsNullOrEmpty(order.Customer.Mail) ? currentAccount.cari_EMail : order.Customer.Mail, taxInfo.OfficeName, taxInfo.Number, invoiceInfo.Data.IsEinvoice);
                        }
                        #endregion

                        #region Current Account Address / Cari Hesap Adresleri
                        Address address = GetAddress(order.InvoiceAddress);

                        var adr_adress_number = mikroUnitOfWork.CARI_HESAP_ADRESLERIRepository.GetLastAdrAdresNo(currentAccount.cari_kod);
                        adr_adress_number++;

                        var currentAccountAddress = new CARI_HESAP_ADRESLERI
                        {
                            adr_Guid = Guid.NewGuid(),
                            adr_create_user = 3,
                            adr_create_date = DateTime.Now.Date,
                            adr_lastup_user = 3,
                            adr_lastup_date = DateTime.Now.Date,
                            adr_cari_kod = currentAccount.cari_kod,
                            adr_cadde = address.Lines[0],
                            adr_mahalle = order.InvoiceAddress.Neighborhood.Name,
                            adr_sokak = address.Lines[2],
                            adr_Semt = order.InvoiceAddress.Neighborhood.District.Name,
                            adr_ulke = order.InvoiceAddress.Neighborhood.District.City.Country.Name,
                            adr_il = order.InvoiceAddress.Neighborhood.District.City.Name,
                            adr_tel_no1 = order.Customer.Phone,
                            adr_adres_no = adr_adress_number
                        };

                        if (!Create(() => mikroUnitOfWork.CARI_HESAP_ADRESLERIRepository.Create(currentAccountAddress), dataResult, "CARI HESAP ADRESLERI", mikroUnitOfWork))
                            throw new Exception("Cari Hesap Adresleri not inserted.");
                        #endregion

                        #region Current Account Transaction / Cari Hesap Hareketleri

                        var cha_document_number = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira(invoiceInfo.Data.InvoiceSerie);
                        cha_document_number++;

                        var sign = $"{order.Company.InvoicePrefix}{order.OrderCode}";
                        var isSign = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.ReadBySign(sign);
                        if (isSign)
                            throw new Exception("Cari Hesap Hareketleri not sign.");

                        #region Brokerage
                        /*
                         * Sirkete ait sanal poslar icin cha_cari_cins degeri 2 olarak belirlenir. Bu durum muhasebesel olarak "Bankadan Kapanacak" statusune denk gelir. PayU gibi araci kurumlar icin bu durum gecerli degildir.
                         */
                        byte cha_cari_cins = 0;
                        byte cha_tpoz = 0;
                        byte cha_grupno = 0;
                        string cha_kod = invoiceInfo.Data.CurrentCode;
                        if (!invoiceInfo.Data.IsBrokerage)
                        {
                            if (order.ResponsibilityCenter.PaymentTypeId == "OO" ||
                                order.ResponsibilityCenter.PaymentTypeId == "KKK")
                            {
                                cha_cari_cins = 2;
                                cha_tpoz = 1;
                                cha_grupno = 1;
                                cha_kod = order.ResponsibilityCenter.AccountNumber;
                            }
                            else if (order.ResponsibilityCenter.PaymentTypeId == "KNO")
                            {
                                cha_cari_cins = 4;
                                cha_tpoz = 1;
                                cha_kod = order.ResponsibilityCenter.AccountNumber;
                            }
                        }
                        #endregion

                        var accountTransactionId = Guid.NewGuid();

                        #region Abroad Invoice
                        byte cha_cinsi = 6;
                        byte cha_ticaret_turu = 0;
                        bool abroadInvoice = Convert.ToInt32(order.InvoiceAddress.Neighborhood.District.City.Country.Id) > 1;
                        string cha_EXIMkodu = string.Empty;
                        if (abroadInvoice)
                        {
                            cha_cinsi = 29;
                            cha_ticaret_turu = 3;
                            cha_EXIMkodu = invoiceInfo.Data.CurrentCode;

                            var currentAccountTransactionAppendix = new CARI_HESAP_HAREKETLERI_EK
                            {
                                chaek_Guid = Guid.NewGuid(),
                                chaek_fileid = 575,
                                chaek_create_user = 1,
                                chaek_create_date = DateTime.Now,
                                chaek_lastup_user = 1,
                                chaek_lastup_date = DateTime.Now,
                                chaek_special1 = string.Empty,
                                chaek_special2 = string.Empty,
                                chaek_special3 = string.Empty,
                                chaek_related_uid = accountTransactionId,
                                cha_Istisna1 = "301",
                                cha_Istisna2 = "301",
                                cha_Istisna3 = "301",
                                cha_Istisna4 = "301",
                                cha_Istisna5 = "301",
                                cha_Istisna6 = "301",
                                cha_Istisna7 = "301",
                                cha_Istisna8 = "301",
                                cha_Istisna9 = "301",
                                cha_Istisna10 = "301",
                                cha_ozel_matrah_kodu = string.Empty,
                                cha_yolcuberaber_kodu = string.Empty,
                                cha_yetkiliaracikurumkodu = string.Empty,
                                cha_EArsiv_unvani_ad = string.Empty,
                                cha_EArsiv_unvani_soyad = string.Empty,
                                cha_EArsiv_daire_adi = string.Empty,
                                cha_EArsiv_Vkn = string.Empty,
                                cha_EArsiv_ulke = string.Empty,
                                cha_EArsiv_Il = string.Empty,
                                cha_EArsiv_tel_ulke_kod = string.Empty,
                                cha_EArsiv_tel_bolge_kod = string.Empty,
                                cha_EArsiv_tel_no = string.Empty,
                                cha_EArsiv_mail = string.Empty,
                                cha_EArsiv_cadde = string.Empty,
                                cha_EArsiv_sokak = string.Empty,
                                cha_EArsiv_Ilce = string.Empty,
                                cha_EArsiv_Ceptel = string.Empty
                            };

                            if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERI_EKRepository.Create(currentAccountTransactionAppendix), dataResult, "CARI HESAP ADRESLERI EK", mikroUnitOfWork))
                                throw new Exception("Cari Hesap Adresleri EK not inserted.");
                        }
                        #endregion

                        var currentAccountTransaction = new CARI_HESAP_HAREKETLERI
                        {
                            cha_Guid = accountTransactionId,
                            cha_fileid = 51,
                            cha_create_date = DateTime.Now,
                            cha_lastup_date = DateTime.Now,
                            cha_special1 = string.Empty,
                            cha_special2 = string.Empty,
                            cha_special3 = string.Empty,
                            cha_evrak_tip = 63, //egk_evr_tip Evrak Aciklamalari Icin
                            cha_evrakno_seri = invoiceInfo.Data.InvoiceSerie,
                            cha_evrakno_sira = cha_document_number,
                            cha_tarihi = DateTime.Now.Date,
                            cha_tpoz = cha_tpoz,
                            cha_belge_no = string.Empty,
                            cha_belge_tarih = DateTime.Now.Date,
                            cha_aciklama = !String.IsNullOrEmpty(order.Customer.CorporateCompanyName) ? order.Customer.CorporateCompanyName : currentAccount.cari_unvan1,
                            cha_satici_kodu = string.Empty,
                            cha_EXIMkodu = cha_EXIMkodu,
                            cha_projekodu = order.ResponsibilityCenter.Name,
                            cha_yat_tes_kodu = string.Empty,
                            cha_cari_cins = cha_cari_cins,
                            cha_kod = cha_kod,
                            cha_ciro_cari_kodu = invoiceInfo.Data.CurrentCode,
                            cha_d_kur = 1,
                            cha_altd_kur = 5,
                            cha_grupno = cha_grupno,
                            cha_srmrkkodu = order.ResponsibilityCenter.Name,
                            cha_karsid_kur = 1,
                            cha_karsisrmrkkodu = string.Empty,
                            cha_fis_tarih = DateTime.Now.Date,
                            cha_fis_sirano = 0,
                            cha_reftarihi = new DateTime(1899, 12, 30),
                            cha_vardiya_tarihi = new DateTime(1899, 12, 30),
                            cha_uuid = Guid.NewGuid().ToString(),
                            cha_adres_no = 1,
                            cha_ilk_belge_tarihi = new DateTime(1899, 12, 30),
                            cha_HareketGrupKodu1 = string.Empty,
                            cha_HareketGrupKodu2 = sign,
                            cha_HareketGrupKodu3 = string.Empty,
                            cha_kasa_hizkod = string.Empty,
                            cha_vade = currentAccount.cari_odemeplan_no,
                            cha_cinsi = cha_cinsi,
                            cha_ticaret_turu = cha_ticaret_turu,
                            cha_vergisiz_fl = abroadInvoice,
                            cha_oivergisiz_fl = abroadInvoice,
                            cha_otvvergisiz_fl = abroadInvoice

                        };

                        int wareHouseId = 1;
                        #region Get WareHouse
                        if (order.WareHouseId > 0)
                        {
                            wareHouseId = Convert.ToInt32(order.WareHouse.ErpNo);
                        }
                        #endregion

                        #region Get Price Infos
                        var priceInfos = new List<PriceInfoDto>();
                        foreach (var orderDetail in order.OrderDetails)
                            priceInfos.Add(mikroUnitOfWork.STOKLARRepository.ReadByStokKod(orderDetail.StockCode));
                        #endregion

                        var stokTransactions = new List<STOK_HAREKETLERI>();

                        if (order.SourceId == "TY" || order.SourceId == "HB")
                        {
                            var sth_row_number = 0;
                            foreach (var orderDetail in order.OrderDetails)
                            {
                                var priceInfo = priceInfos.First(pi => pi.StokKod == orderDetail.StockCode);

                                var vatRate = priceInfo.VatType == 4 ? 1.18M : 1.08M;
                                var vat = orderDetail.UnitPrice - (orderDetail.UnitPrice / vatRate);
                                var sip_quantity = orderDetail.Quantity;
                                var sip_amount = (orderDetail.UnitPrice - vat) * orderDetail.Quantity;
                                var sth_discount_1 = 0f;
                                var sip_vergi = vat * orderDetail.Quantity;
                                var sth_vergi_pntr = priceInfo.VatType;
                                byte sth_disticaret_turu = 0;

                                /*
                                 * Bu kod hediye verilen ürünler için yazılmıştır. Ürününün bedelsiz çıkışı istenildiğinden, birim fiyat mikrodan alınır ve o oranda indirim uygulanarak ürünün bedelsiz çıkışı sağlanır.
                                 */
                                if (orderDetail.UnitPrice == 0)
                                {
                                    sip_amount = (decimal)priceInfo.TaxExcludedUnitPrice * orderDetail.Quantity;
                                    sip_vergi = 0;
                                    sth_vergi_pntr = priceInfo.VatType;
                                    sth_discount_1 = (float)sip_amount;
                                }

                                stokTransactions.Add(new STOK_HAREKETLERI
                                {
                                    sth_Guid = Guid.NewGuid(),
                                    sth_DBCno = 0,
                                    sth_SpecRECno = 0,
                                    sth_iptal = false,
                                    sth_fileid = 16,
                                    sth_hidden = false,
                                    sth_kilitli = false,
                                    sth_degisti = false,
                                    sth_checksum = 0,
                                    sth_create_user = 3,
                                    sth_create_date = DateTime.Now,
                                    sth_lastup_user = 2,
                                    sth_lastup_date = DateTime.Now,
                                    sth_special1 = string.Empty,
                                    sth_special2 = string.Empty,
                                    sth_special3 = string.Empty,
                                    sth_firmano = 0,
                                    sth_subeno = 0,
                                    sth_tarih = DateTime.Now.Date,
                                    sth_tip = 1,
                                    sth_cins = 0,
                                    sth_normal_iade = 0,
                                    sth_evraktip = 4,
                                    sth_evrakno_seri = invoiceInfo.Data.InvoiceSerie,
                                    sth_evrakno_sira = cha_document_number,
                                    sth_satirno = sth_row_number,
                                    sth_belge_no = string.Empty,
                                    sth_belge_tarih = DateTime.Now.Date,
                                    sth_stok_kod = orderDetail.StockCode,
                                    sth_isk_mas1 = 0,
                                    sth_isk_mas2 = 1,
                                    sth_isk_mas3 = 1,
                                    sth_isk_mas4 = 1,
                                    sth_isk_mas5 = 1,
                                    sth_isk_mas6 = 1,
                                    sth_isk_mas7 = 1,
                                    sth_isk_mas8 = 1,
                                    sth_isk_mas9 = 1,
                                    sth_isk_mas10 = 1,
                                    sth_sat_iskmas1 = false,
                                    sth_sat_iskmas2 = false,
                                    sth_sat_iskmas3 = false,
                                    sth_sat_iskmas4 = false,
                                    sth_sat_iskmas5 = false,
                                    sth_sat_iskmas6 = false,
                                    sth_sat_iskmas7 = false,
                                    sth_sat_iskmas8 = false,
                                    sth_sat_iskmas9 = false,
                                    sth_sat_iskmas10 = false,
                                    sth_pos_satis = 0,
                                    sth_promosyon_fl = false,
                                    sth_cari_cinsi = 0,
                                    sth_cari_kodu = invoiceInfo.Data.CurrentCode,
                                    sth_cari_grup_no = 0,
                                    sth_isemri_gider_kodu = string.Empty,
                                    sth_plasiyer_kodu = string.Empty,
                                    sth_har_doviz_cinsi = 0,
                                    sth_har_doviz_kuru = 1,
                                    sth_alt_doviz_kuru = 5,
                                    sth_stok_doviz_cinsi = 0,
                                    sth_stok_doviz_kuru = 1,
                                    sth_miktar = orderDetail.Quantity,
                                    sth_miktar2 = orderDetail.Quantity,
                                    sth_birim_pntr = 1,
                                    sth_tutar = (double)sip_amount,
                                    sth_iskonto1 = sth_discount_1,
                                    sth_iskonto2 = 0,
                                    sth_iskonto3 = 0,
                                    sth_iskonto4 = 0,
                                    sth_iskonto5 = 0,
                                    sth_iskonto6 = 0,
                                    sth_masraf1 = 0,
                                    sth_masraf2 = 0,
                                    sth_masraf3 = 0,
                                    sth_masraf4 = 0,
                                    sth_vergi_pntr = sth_vergi_pntr,
                                    sth_vergi = (double)sip_vergi,
                                    sth_masraf_vergi_pntr = 0,
                                    sth_masraf_vergi = 0,
                                    sth_netagirlik = 0,
                                    sth_aciklama = order.MarketOrderCode,
                                    sth_fat_uid = currentAccountTransaction.cha_Guid,
                                    sth_giris_depo_no = wareHouseId,
                                    sth_cikis_depo_no = wareHouseId,
                                    sth_malkbl_sevk_tarihi = DateTime.Now.Date,
                                    sth_cari_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_stok_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_fis_tarihi = DateTime.Now,
                                    sth_maliyet_ana = 0,
                                    sth_maliyet_alternatif = 0,
                                    sth_maliyet_orjinal = 0,
                                    sth_parti_kodu = string.Empty,
                                    sth_lot_no = 0,
                                    sth_proje_kodu = order.ResponsibilityCenter.Name,
                                    sth_exim_kodu = cha_EXIMkodu,
                                    sth_otv_pntr = 0,
                                    sth_otv_vergi = 0,
                                    sth_brutagirlik = 0,
                                    sth_otvtutari = 0,
                                    sth_oiv_pntr = 0,
                                    sth_oiv_vergi = 0,
                                    sth_fiyat_liste_no = 1,
                                    sth_oivtutari = 0,
                                    sth_Tevkifat_turu = 0,
                                    sth_nakliyedeposu = 0,
                                    sth_nakliyedurumu = 0,
                                    sth_taxfree_fl = false,
                                    sth_ilave_edilecek_kdv = 0,
                                    sth_ismerkezi_kodu = string.Empty,
                                    sth_HareketGrupKodu1 = string.Empty,
                                    sth_HareketGrupKodu2 = string.Empty,
                                    sth_HareketGrupKodu3 = string.Empty,
                                    sth_Olcu1 = 0,
                                    sth_Olcu2 = 0,
                                    sth_Olcu3 = 0,
                                    sth_Olcu4 = 0,
                                    sth_Olcu5 = 0,
                                    sth_FormulMiktarNo = 0,
                                    sth_FormulMiktar = 0,
                                    sth_eirs_senaryo = 0,
                                    sth_eirs_tipi = 0,
                                    sth_odeme_op = currentAccount.cari_odemeplan_no,
                                    sth_disticaret_turu = sth_disticaret_turu,
                                    sth_vergisiz_fl = abroadInvoice,
                                    sth_otvvergisiz_fl = abroadInvoice,
                                    sth_oivvergisiz_fl = abroadInvoice,

                                });
                                sth_row_number++;
                            }
                        }
                        else
                        {
                            #region Discount
                            float mikroAmount = order.OrderDetails.Sum(od => od.Quantity * priceInfos.First(pi => pi.StokKod == od.StockCode).TaxIncludedUnitPrice);

                            var discount = new Discount((float)(order.Amount), mikroAmount);
                            #endregion

                            var sth_row_number = 0;
                            foreach (var orderDetail in order.OrderDetails)
                            {
                                var priceInfo = priceInfos.First(pi => pi.StokKod == orderDetail.StockCode);
                                var sip_quantity = orderDetail.Quantity;
                                var sip_amount = priceInfo.TaxExcludedUnitPrice * orderDetail.Quantity;
                                var sth_discount_1 = 0f;
                                var sip_vergi = priceInfo.Vat * orderDetail.Quantity;
                                var sth_vergi_pntr = priceInfo.VatType;
                                byte sth_disticaret_turu = 0;

                                /*Yurt dışı siparişler için vergi hesaplanmıyor*/
                                if (abroadInvoice)
                                {
                                    sth_disticaret_turu = 3;
                                    sip_vergi = 0;
                                    sth_vergi_pntr = 0;
                                    sip_amount = priceInfo.TaxIncludedUnitPrice * orderDetail.Quantity;

                                }

                                if (discount.Rate > 0f)
                                {
                                    //sip_amount = (priceInfo.TaxIncludedUnitPrice * discount.Rate) * orderDetail.Quantity;
                                    sth_discount_1 = sip_amount * discount.Rate;
                                    sip_vergi -= sip_vergi * discount.Rate;
                                }

                                stokTransactions.Add(new STOK_HAREKETLERI
                                {
                                    sth_Guid = Guid.NewGuid(),
                                    sth_DBCno = 0,
                                    sth_SpecRECno = 0,
                                    sth_iptal = false,
                                    sth_fileid = 16,
                                    sth_hidden = false,
                                    sth_kilitli = false,
                                    sth_degisti = false,
                                    sth_checksum = 0,
                                    sth_create_user = 3,
                                    sth_create_date = DateTime.Now,
                                    sth_lastup_user = 2,
                                    sth_lastup_date = DateTime.Now,
                                    sth_special1 = string.Empty,
                                    sth_special2 = string.Empty,
                                    sth_special3 = string.Empty,
                                    sth_firmano = 0,
                                    sth_subeno = 0,
                                    sth_tarih = DateTime.Now.Date,
                                    sth_tip = 1,
                                    sth_cins = 0,
                                    sth_normal_iade = 0,
                                    sth_evraktip = 4,
                                    sth_evrakno_seri = invoiceInfo.Data.InvoiceSerie,
                                    sth_evrakno_sira = cha_document_number,
                                    sth_satirno = sth_row_number,
                                    sth_belge_no = string.Empty,
                                    sth_belge_tarih = DateTime.Now.Date,
                                    sth_stok_kod = orderDetail.StockCode,
                                    sth_isk_mas1 = 0,
                                    sth_isk_mas2 = 1,
                                    sth_isk_mas3 = 1,
                                    sth_isk_mas4 = 1,
                                    sth_isk_mas5 = 1,
                                    sth_isk_mas6 = 1,
                                    sth_isk_mas7 = 1,
                                    sth_isk_mas8 = 1,
                                    sth_isk_mas9 = 1,
                                    sth_isk_mas10 = 1,
                                    sth_sat_iskmas1 = false,
                                    sth_sat_iskmas2 = false,
                                    sth_sat_iskmas3 = false,
                                    sth_sat_iskmas4 = false,
                                    sth_sat_iskmas5 = false,
                                    sth_sat_iskmas6 = false,
                                    sth_sat_iskmas7 = false,
                                    sth_sat_iskmas8 = false,
                                    sth_sat_iskmas9 = false,
                                    sth_sat_iskmas10 = false,
                                    sth_pos_satis = 0,
                                    sth_promosyon_fl = false,
                                    sth_cari_cinsi = 0,
                                    sth_cari_kodu = invoiceInfo.Data.CurrentCode,
                                    sth_cari_grup_no = 0,
                                    sth_isemri_gider_kodu = string.Empty,
                                    sth_plasiyer_kodu = string.Empty,
                                    sth_har_doviz_cinsi = 0,
                                    sth_har_doviz_kuru = 1,
                                    sth_alt_doviz_kuru = 5,
                                    sth_stok_doviz_cinsi = 0,
                                    sth_stok_doviz_kuru = 1,
                                    sth_miktar = orderDetail.Quantity,
                                    sth_miktar2 = orderDetail.Quantity,
                                    sth_birim_pntr = 1,
                                    sth_tutar = sip_amount,
                                    sth_iskonto1 = sth_discount_1,
                                    sth_iskonto2 = 0,
                                    sth_iskonto3 = 0,
                                    sth_iskonto4 = 0,
                                    sth_iskonto5 = 0,
                                    sth_iskonto6 = 0,
                                    sth_masraf1 = 0,
                                    sth_masraf2 = 0,
                                    sth_masraf3 = 0,
                                    sth_masraf4 = 0,
                                    sth_vergi_pntr = sth_vergi_pntr,
                                    sth_vergi = sip_vergi,
                                    sth_masraf_vergi_pntr = 0,
                                    sth_masraf_vergi = 0,
                                    sth_netagirlik = 0,
                                    sth_aciklama = order.MarketOrderCode,
                                    sth_fat_uid = currentAccountTransaction.cha_Guid,
                                    sth_giris_depo_no = wareHouseId,
                                    sth_cikis_depo_no = wareHouseId,
                                    sth_malkbl_sevk_tarihi = DateTime.Now.Date,
                                    sth_cari_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_stok_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_fis_tarihi = DateTime.Now,
                                    sth_maliyet_ana = 0,
                                    sth_maliyet_alternatif = 0,
                                    sth_maliyet_orjinal = 0,
                                    sth_parti_kodu = string.Empty,
                                    sth_lot_no = 0,
                                    sth_proje_kodu = order.ResponsibilityCenter.Name,
                                    sth_exim_kodu = cha_EXIMkodu,
                                    sth_otv_pntr = 0,
                                    sth_otv_vergi = 0,
                                    sth_brutagirlik = 0,
                                    sth_otvtutari = 0,
                                    sth_oiv_pntr = 0,
                                    sth_oiv_vergi = 0,
                                    sth_fiyat_liste_no = 1,
                                    sth_oivtutari = 0,
                                    sth_Tevkifat_turu = 0,
                                    sth_nakliyedeposu = 0,
                                    sth_nakliyedurumu = 0,
                                    sth_taxfree_fl = false,
                                    sth_ilave_edilecek_kdv = 0,
                                    sth_ismerkezi_kodu = string.Empty,
                                    sth_HareketGrupKodu1 = string.Empty,
                                    sth_HareketGrupKodu2 = string.Empty,
                                    sth_HareketGrupKodu3 = string.Empty,
                                    sth_Olcu1 = 0,
                                    sth_Olcu2 = 0,
                                    sth_Olcu3 = 0,
                                    sth_Olcu4 = 0,
                                    sth_Olcu5 = 0,
                                    sth_FormulMiktarNo = 0,
                                    sth_FormulMiktar = 0,
                                    sth_eirs_senaryo = 0,
                                    sth_eirs_tipi = 0,
                                    sth_odeme_op = currentAccount.cari_odemeplan_no,
                                    sth_disticaret_turu = sth_disticaret_turu,
                                    sth_vergisiz_fl = abroadInvoice,
                                    sth_otvvergisiz_fl = abroadInvoice,
                                    sth_oivvergisiz_fl = abroadInvoice,

                                });
                                sth_row_number++;
                            }

                            #region Cargo Fee
                            if (order.ShipmentFee > 0)
                            {
                                byte sth_vergi_pntr = 4;
                                var sth_vergi = TaxAmount(TaxExcludedAmount((float)order.ShipmentFee));
                                var sth_tutar = TaxExcludedAmount((float)order.ShipmentFee);
                                byte sth_disticaret_turu = 0;
                                if (abroadInvoice)
                                {
                                    sth_disticaret_turu = 3;
                                    sth_vergi_pntr = 0;
                                    sth_vergi = 0;
                                    sth_tutar = (float)order.ShipmentFee;
                                }


                                stokTransactions.Add(new STOK_HAREKETLERI
                                {
                                    sth_Guid = Guid.NewGuid(),
                                    sth_DBCno = 0,
                                    sth_SpecRECno = 0,
                                    sth_iptal = false,
                                    sth_fileid = 16,
                                    sth_hidden = false,
                                    sth_kilitli = false,
                                    sth_degisti = false,
                                    sth_checksum = 0,
                                    sth_create_user = 3,
                                    sth_create_date = DateTime.Now,
                                    sth_lastup_user = 2,
                                    sth_lastup_date = DateTime.Now,
                                    sth_special1 = string.Empty,
                                    sth_special2 = string.Empty,
                                    sth_special3 = string.Empty,
                                    sth_firmano = 0,
                                    sth_subeno = 0,
                                    sth_tarih = DateTime.Now.Date,
                                    sth_tip = 1,
                                    sth_cins = 0,
                                    sth_normal_iade = 0,
                                    sth_evraktip = 4,
                                    sth_evrakno_seri = invoiceInfo.Data.InvoiceSerie,
                                    sth_evrakno_sira = cha_document_number,
                                    sth_satirno = sth_row_number,
                                    sth_belge_no = string.Empty,
                                    sth_belge_tarih = DateTime.Now.Date,
                                    sth_stok_kod = "STKARGO",
                                    sth_isk_mas1 = 0,
                                    sth_isk_mas2 = 1,
                                    sth_isk_mas3 = 1,
                                    sth_isk_mas4 = 1,
                                    sth_isk_mas5 = 1,
                                    sth_isk_mas6 = 1,
                                    sth_isk_mas7 = 1,
                                    sth_isk_mas8 = 1,
                                    sth_isk_mas9 = 1,
                                    sth_isk_mas10 = 1,
                                    sth_sat_iskmas1 = false,
                                    sth_sat_iskmas2 = false,
                                    sth_sat_iskmas3 = false,
                                    sth_sat_iskmas4 = false,
                                    sth_sat_iskmas5 = false,
                                    sth_sat_iskmas6 = false,
                                    sth_sat_iskmas7 = false,
                                    sth_sat_iskmas8 = false,
                                    sth_sat_iskmas9 = false,
                                    sth_sat_iskmas10 = false,
                                    sth_pos_satis = 0,
                                    sth_promosyon_fl = false,
                                    sth_cari_cinsi = 0,
                                    sth_cari_kodu = invoiceInfo.Data.CurrentCode,
                                    sth_cari_grup_no = 0,
                                    sth_isemri_gider_kodu = string.Empty,
                                    sth_plasiyer_kodu = string.Empty,
                                    sth_har_doviz_cinsi = 0,
                                    sth_har_doviz_kuru = 1,
                                    sth_alt_doviz_kuru = 5,
                                    sth_stok_doviz_cinsi = 0,
                                    sth_stok_doviz_kuru = 1,
                                    sth_miktar = 1,
                                    sth_miktar2 = 1,
                                    sth_birim_pntr = 1,
                                    sth_tutar = sth_tutar,
                                    sth_iskonto1 = 0,
                                    sth_iskonto2 = 0,
                                    sth_iskonto3 = 0,
                                    sth_iskonto4 = 0,
                                    sth_iskonto5 = 0,
                                    sth_iskonto6 = 0,
                                    sth_masraf1 = 0,
                                    sth_masraf2 = 0,
                                    sth_masraf3 = 0,
                                    sth_masraf4 = 0,
                                    sth_vergi_pntr = sth_vergi_pntr,
                                    sth_vergi = sth_vergi,
                                    sth_masraf_vergi_pntr = 0,
                                    sth_masraf_vergi = 0,
                                    sth_netagirlik = 0,
                                    sth_odeme_op = 0,
                                    sth_aciklama = string.Empty,
                                    sth_fat_uid = currentAccountTransaction.cha_Guid,
                                    sth_giris_depo_no = wareHouseId,
                                    sth_cikis_depo_no = wareHouseId,
                                    sth_malkbl_sevk_tarihi = DateTime.Now.Date,
                                    sth_cari_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_stok_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_fis_tarihi = DateTime.Now,
                                    sth_maliyet_ana = 0,
                                    sth_maliyet_alternatif = 0,
                                    sth_maliyet_orjinal = 0,
                                    sth_parti_kodu = string.Empty,
                                    sth_lot_no = 0,
                                    sth_proje_kodu = order.ResponsibilityCenter.Name,
                                    sth_exim_kodu = string.Empty,
                                    sth_otv_pntr = 0,
                                    sth_otv_vergi = 0,
                                    sth_brutagirlik = 0,
                                    sth_otvtutari = 0,
                                    sth_oiv_pntr = 0,
                                    sth_oiv_vergi = 0,
                                    sth_fiyat_liste_no = 1,
                                    sth_oivtutari = 0,
                                    sth_Tevkifat_turu = 0,
                                    sth_nakliyedeposu = 0,
                                    sth_nakliyedurumu = 0,
                                    sth_taxfree_fl = false,
                                    sth_ilave_edilecek_kdv = 0,
                                    sth_ismerkezi_kodu = string.Empty,
                                    sth_HareketGrupKodu1 = string.Empty,
                                    sth_HareketGrupKodu2 = string.Empty,
                                    sth_HareketGrupKodu3 = string.Empty,
                                    sth_Olcu1 = 0,
                                    sth_Olcu2 = 0,
                                    sth_Olcu3 = 0,
                                    sth_Olcu4 = 0,
                                    sth_Olcu5 = 0,
                                    sth_FormulMiktarNo = 0,
                                    sth_FormulMiktar = 0,
                                    sth_eirs_senaryo = 0,
                                    sth_eirs_tipi = 0,
                                    sth_disticaret_turu = sth_disticaret_turu,
                                    sth_vergisiz_fl = abroadInvoice,
                                    sth_otvvergisiz_fl = abroadInvoice,
                                    sth_oivvergisiz_fl = abroadInvoice,
                                });
                                sth_row_number++;
                            }
                            #endregion

                            #region Payment Type Fee
                            if (order.PaymentFee > 0)
                            {
                                byte sth_vergi_pntr = 4;
                                var sth_vergi = TaxAmount(TaxExcludedAmount((float)order.PaymentFee));
                                var sth_tutar = TaxExcludedAmount((float)order.PaymentFee);
                                byte sth_disticaret_turu = 0;

                                if (abroadInvoice)
                                {
                                    sth_disticaret_turu = 3;
                                    sth_vergi_pntr = 0;
                                    sth_vergi = 0;
                                    sth_tutar = (float)order.PaymentFee;
                                }


                                stokTransactions.Add(new STOK_HAREKETLERI
                                {
                                    sth_Guid = Guid.NewGuid(),
                                    sth_DBCno = 0,
                                    sth_SpecRECno = 0,
                                    sth_iptal = false,
                                    sth_fileid = 16,
                                    sth_hidden = false,
                                    sth_kilitli = false,
                                    sth_degisti = false,
                                    sth_checksum = 0,
                                    sth_create_user = 3,
                                    sth_create_date = DateTime.Now,
                                    sth_lastup_user = 2,
                                    sth_lastup_date = DateTime.Now,
                                    sth_special1 = string.Empty,
                                    sth_special2 = string.Empty,
                                    sth_special3 = string.Empty,
                                    sth_firmano = 0,
                                    sth_subeno = 0,
                                    sth_tarih = DateTime.Now.Date,
                                    sth_tip = 1,
                                    sth_cins = 0,
                                    sth_normal_iade = 0,
                                    sth_evraktip = 4,
                                    sth_evrakno_seri = invoiceInfo.Data.InvoiceSerie,
                                    sth_evrakno_sira = cha_document_number,
                                    sth_satirno = sth_row_number,
                                    sth_belge_no = string.Empty,
                                    sth_belge_tarih = DateTime.Now.Date,
                                    sth_stok_kod = "STOB",
                                    sth_isk_mas1 = 0,
                                    sth_isk_mas2 = 1,
                                    sth_isk_mas3 = 1,
                                    sth_isk_mas4 = 1,
                                    sth_isk_mas5 = 1,
                                    sth_isk_mas6 = 1,
                                    sth_isk_mas7 = 1,
                                    sth_isk_mas8 = 1,
                                    sth_isk_mas9 = 1,
                                    sth_isk_mas10 = 1,
                                    sth_sat_iskmas1 = false,
                                    sth_sat_iskmas2 = false,
                                    sth_sat_iskmas3 = false,
                                    sth_sat_iskmas4 = false,
                                    sth_sat_iskmas5 = false,
                                    sth_sat_iskmas6 = false,
                                    sth_sat_iskmas7 = false,
                                    sth_sat_iskmas8 = false,
                                    sth_sat_iskmas9 = false,
                                    sth_sat_iskmas10 = false,
                                    sth_pos_satis = 0,
                                    sth_promosyon_fl = false,
                                    sth_cari_cinsi = 0,
                                    sth_cari_kodu = invoiceInfo.Data.CurrentCode,
                                    sth_cari_grup_no = 0,
                                    sth_isemri_gider_kodu = string.Empty,
                                    sth_plasiyer_kodu = string.Empty,
                                    sth_har_doviz_cinsi = 0,
                                    sth_har_doviz_kuru = 1,
                                    sth_alt_doviz_kuru = 5,
                                    sth_stok_doviz_cinsi = 0,
                                    sth_stok_doviz_kuru = 1,
                                    sth_miktar = 1,
                                    sth_miktar2 = 1,
                                    sth_birim_pntr = 1,
                                    sth_tutar = sth_tutar,
                                    sth_iskonto1 = 0,
                                    sth_iskonto2 = 0,
                                    sth_iskonto3 = 0,
                                    sth_iskonto4 = 0,
                                    sth_iskonto5 = 0,
                                    sth_iskonto6 = 0,
                                    sth_masraf1 = 0,
                                    sth_masraf2 = 0,
                                    sth_masraf3 = 0,
                                    sth_masraf4 = 0,
                                    sth_vergi_pntr = sth_vergi_pntr,
                                    sth_vergi = sth_vergi,
                                    sth_masraf_vergi_pntr = 0,
                                    sth_masraf_vergi = 0,
                                    sth_netagirlik = 0,
                                    sth_odeme_op = 0,
                                    sth_aciklama = string.Empty,
                                    sth_fat_uid = currentAccountTransaction.cha_Guid,
                                    sth_giris_depo_no = wareHouseId,
                                    sth_cikis_depo_no = wareHouseId,
                                    sth_malkbl_sevk_tarihi = DateTime.Now.Date,
                                    sth_cari_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_stok_srm_merkezi = order.ResponsibilityCenter.Name,
                                    sth_fis_tarihi = DateTime.Now,
                                    sth_maliyet_ana = 0,
                                    sth_maliyet_alternatif = 0,
                                    sth_maliyet_orjinal = 0,
                                    sth_parti_kodu = string.Empty,
                                    sth_lot_no = 0,
                                    sth_proje_kodu = order.ResponsibilityCenter.Name,
                                    sth_exim_kodu = string.Empty,
                                    sth_otv_pntr = 0,
                                    sth_otv_vergi = 0,
                                    sth_brutagirlik = 0,
                                    sth_otvtutari = 0,
                                    sth_oiv_pntr = 0,
                                    sth_oiv_vergi = 0,
                                    sth_fiyat_liste_no = 1,
                                    sth_oivtutari = 0,
                                    sth_Tevkifat_turu = 0,
                                    sth_nakliyedeposu = 0,
                                    sth_nakliyedurumu = 0,
                                    sth_taxfree_fl = false,
                                    sth_ilave_edilecek_kdv = 0,
                                    sth_ismerkezi_kodu = string.Empty,
                                    sth_HareketGrupKodu1 = string.Empty,
                                    sth_HareketGrupKodu2 = string.Empty,
                                    sth_HareketGrupKodu3 = string.Empty,
                                    sth_Olcu1 = 0,
                                    sth_Olcu2 = 0,
                                    sth_Olcu3 = 0,
                                    sth_Olcu4 = 0,
                                    sth_Olcu5 = 0,
                                    sth_FormulMiktarNo = 0,
                                    sth_FormulMiktar = 0,
                                    sth_eirs_senaryo = 0,
                                    sth_eirs_tipi = 0,
                                    sth_disticaret_turu = sth_disticaret_turu,
                                    sth_vergisiz_fl = abroadInvoice,
                                    sth_otvvergisiz_fl = abroadInvoice,
                                    sth_oivvergisiz_fl = abroadInvoice,
                                });
                                sth_row_number++;
                            }
                            #endregion
                        }

                        /* %8 Kdv */
                        currentAccountTransaction.cha_vergi3 = stokTransactions.Where(st => st.sth_vergi_pntr == 3).Sum(sh => sh.sth_vergi);
                        /* %18 Kdv */
                        currentAccountTransaction.cha_vergi4 = stokTransactions.Where(st => st.sth_vergi_pntr == 4).Sum(sh => sh.sth_vergi);
                        currentAccountTransaction.cha_aratoplam = stokTransactions.Sum(sh => sh.sth_tutar);
                        currentAccountTransaction.cha_meblag = stokTransactions.Sum(sh => (sh.sth_tutar - sh.sth_iskonto1) + sh.sth_vergi);
                        currentAccountTransaction.cha_ft_iskonto1 = stokTransactions.Sum(sh => sh.sth_iskonto1);

                        #region Insert
                        if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(currentAccountTransaction), dataResult, "CARI HESAP Hareketleri", mikroUnitOfWork))
                            throw new Exception("Cari Hesap hareketleri not inserted.");

                        foreach (var stokTransaction in stokTransactions)
                        {
                            stokTransaction.sth_fat_uid = currentAccountTransaction.cha_Guid;

                            if (!Create(() => mikroUnitOfWork.STOK_HAREKETLERIRepository.Create(stokTransaction), dataResult, "STOK HESAP Hareketleri", mikroUnitOfWork))
                                throw new Exception("Stok hareketleri not inserted.");
                        }
                        #endregion
                        #endregion

                        #region Document Description / Evrak Aciklamalari
                        var documentDescription = GetDocumentDescription(order.Amount, order.ResponsibilityCenter, order.SourceId,
                            order.Company);


                        if (!Create(() => mikroUnitOfWork.EVRAK_ACIKLAMALARIRepository.Create(new EVRAK_ACIKLAMALARI
                        {
                            egk_Guid = Guid.NewGuid(),
                            egk_evr_tip = 63,
                            egk_fileid = 66,
                            egk_create_user = 1,
                            egk_create_date = DateTime.Now.Date,
                            egk_lastup_user = 1,
                            egk_lastup_date = DateTime.Now.Date,
                            egk_dosyano = 51,
                            egk_evr_seri = invoiceInfo.Data.InvoiceSerie,
                            egk_evr_sira = cha_document_number,
                            egk_tesaltarihi = new DateTime(1899, 12, 30),
                            egk_evracik1 = documentDescription.Lines[0],
                            egk_evracik2 = documentDescription.Lines[1],
                            egk_evracik3 = documentDescription.Lines[2],
                            egk_evracik4 = documentDescription.Lines[3],
                            egk_evracik5 = documentDescription.Lines[4],
                            egk_evracik6 = documentDescription.Lines[5],
                            egk_evracik7 = documentDescription.Lines[6],
                            egk_evracik8 = documentDescription.Lines[7],
                            egk_evracik9 = documentDescription.Lines[8],
                            egk_evracik10 = documentDescription.Lines[9],
                            egk_kargokodu = string.Empty,
                            egk_kargono = string.Empty,
                            egk_tesalkisi = string.Empty,
                            egk_special1 = string.Empty,
                            egk_special2 = string.Empty,
                            egk_special3 = string.Empty,
                            egk_evr_ustkod = string.Empty
                        }), dataResult, "Evrak Açıklaması", mikroUnitOfWork))
                            throw new Exception("Evrak Açıklamaları not inserted.");
                        #endregion

                        #region Current Account Transaction / Cari Hesap Hareketleri
                        /*
                         * Aracilik hizmeti yapilan durumlarda virman islemi gerceklestirilir.
                         */
                        if (invoiceInfo.Data.IsBrokerage)
                        {
                            var cha_document_number2 = mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.GetEvrakNoSira("ZX", 33);
                            cha_document_number2++;


                            if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(new CARI_HESAP_HAREKETLERI
                            {
                                cha_Guid = Guid.NewGuid(),
                                cha_cari_cins = 0,
                                cha_belge_no = $"{order.Company.InvoicePrefix}{order.OrderCode}",
                                cha_kasa_hizmet = 0,
                                cha_kasa_hizkod = String.Empty,
                                cha_evrakno_seri = "ZX",
                                cha_iptal = false,
                                cha_fileid = 51,
                                cha_hidden = false,
                                cha_kilitli = false,
                                cha_degisti = false,
                                cha_create_user = 1,
                                cha_create_date = DateTime.Now.Date,
                                cha_lastup_user = 1,
                                cha_lastup_date = DateTime.Now.Date,
                                cha_special1 = string.Empty,
                                cha_special2 = string.Empty,
                                cha_special3 = string.Empty,
                                cha_evrak_tip = 33,
                                cha_evrakno_sira = cha_document_number2,
                                cha_satir_no = 0,
                                cha_tarihi = DateTime.Now.Date,
                                cha_cinsi = 5,
                                cha_belge_tarih = DateTime.Now.Date,
                                cha_aciklama = $"{order.Customer.FirstName} {order.Customer.LastName}".Length > 40 ? $"{order.Customer.FirstName} {order.Customer.LastName}".Substring(0, 40) : $"{order.Customer.FirstName} {order.Customer.LastName}",
                                cha_satici_kodu = string.Empty,
                                cha_EXIMkodu = string.Empty,
                                cha_projekodu = string.Empty,
                                cha_yat_tes_kodu = string.Empty,
                                cha_kod = order.ResponsibilityCenter.AccountNumber,
                                cha_ciro_cari_kodu = string.Empty,
                                cha_d_kur = 1,
                                cha_altd_kur = 5,
                                cha_srmrkkodu = order.ResponsibilityCenter.Name,
                                cha_karsid_kur = 1,
                                cha_karsisrmrkkodu = string.Empty,
                                cha_miktari = 0,
                                cha_meblag = (float)(order.Amount + order.PaymentFee + order.ShipmentFee),
                                cha_fis_tarih = new DateTime(1900, 01, 01),
                                cha_trefno = string.Empty,
                                cha_reftarihi = new DateTime(1900, 01, 01),
                                cha_vardiya_tarihi = new DateTime(1900, 01, 01),
                                cha_diger_belge_adi = string.Empty,
                                cha_uuid = string.Empty,
                                cha_ilk_belge_tarihi = new DateTime(1900, 01, 01),
                                cha_HareketGrupKodu1 = string.Empty,
                                cha_HareketGrupKodu2 = string.Empty,
                                cha_HareketGrupKodu3 = string.Empty,
                            }), dataResult, "CARI HESAPLAR Hareketleri", mikroUnitOfWork))
                                throw new Exception("Cari Hesap Hareketleri not inserted.");


                            if (!Create(() => mikroUnitOfWork.CARI_HESAP_HAREKETLERIRepository.Create(new CARI_HESAP_HAREKETLERI
                            {
                                cha_Guid = Guid.NewGuid(),
                                cha_belge_no = $"{order.Company.InvoicePrefix}{order.OrderCode}",
                                cha_kasa_hizmet = 0,
                                cha_kasa_hizkod = String.Empty,
                                cha_evrakno_seri = "ZX",
                                cha_iptal = false,
                                cha_fileid = 51,
                                cha_tip = 1,
                                cha_hidden = false,
                                cha_kilitli = false,
                                cha_degisti = false,
                                cha_create_user = 1,
                                cha_create_date = DateTime.Now.Date,
                                cha_lastup_user = 1,
                                cha_lastup_date = DateTime.Now.Date,
                                cha_special1 = string.Empty,
                                cha_special2 = string.Empty,
                                cha_special3 = string.Empty,
                                cha_evrak_tip = 33,
                                cha_evrakno_sira = cha_document_number2,
                                cha_satir_no = 1,
                                cha_tarihi = DateTime.Now.Date,
                                cha_cinsi = 5,
                                cha_belge_tarih = DateTime.Now.Date,
                                cha_aciklama = string.Empty,
                                cha_satici_kodu = string.Empty,
                                cha_EXIMkodu = string.Empty,
                                cha_projekodu = string.Empty,
                                cha_yat_tes_kodu = string.Empty,
                                cha_kod = currentAccount.cari_kod,
                                cha_ciro_cari_kodu = string.Empty,
                                cha_d_kur = 1,
                                cha_altd_kur = 5,
                                cha_srmrkkodu = order.ResponsibilityCenter.Name,
                                cha_karsid_kur = 1,
                                cha_karsisrmrkkodu = string.Empty,
                                cha_miktari = 0,
                                cha_meblag = (float)(order.Amount + order.PaymentFee + order.ShipmentFee),
                                cha_fis_tarih = new DateTime(1900, 01, 01),
                                cha_trefno = string.Empty,
                                cha_reftarihi = new DateTime(1900, 01, 01),
                                cha_vardiya_tarihi = new DateTime(1900, 01, 01),
                                cha_diger_belge_adi = string.Empty,
                                cha_uuid = string.Empty,
                                cha_ilk_belge_tarihi = new DateTime(1900, 01, 01),
                                cha_HareketGrupKodu1 = string.Empty,
                                cha_HareketGrupKodu2 = string.Empty,
                                cha_HareketGrupKodu3 = string.Empty,
                            }), dataResult, "CARI HESAPLAR Hareketleri", mikroUnitOfWork))
                                throw new Exception("Cari Hesap Hareketleri not inserted.");


                        }
                        #endregion

                        dataResult.Data = new SalesInvoice
                        {
                            InvoiceId = $"{invoiceInfo.Data.InvoiceSerie}{cha_document_number}",
                            AccountTransactionId = accountTransactionId,
                            InvoiceAmount = Convert.ToDecimal(currentAccountTransaction.cha_meblag)
                        };

                        mikroUnitOfWork.CommitTransaction();

                    }
                    catch (Exception ex)
                    {
                        dataResult.Failed = true;
                        dataResult.Title = "Hata";
                        dataResult.Message = $"Fatura kaydedilirken bir hata oluştu sistem yetkilisine haber veriniz." + ex.Message;
                        mikroUnitOfWork.RollbackTransaction();
                    }
                }
            });
        }

        public DataResponse<AccountingCancel> Cancel(CancelOrder cancelOrder)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Cari Kodunu buluan method
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        private InvoiceInfo GetCurrentCode(CustomerModel customer, CompanyModel company, bool isBrokerage, UnitOfWork.UnitOfWork mikroUnitOfWork)
        {
            var invoiceInfo = new InvoiceInfo
            {
                CurrentCode = $"120.{company.AccountCustomerPrefix}.{customer.CustomerCode}", //Bireysel Futura Kodu
                InvoiceSerie = company.MikroOfficialInvoiceSerie,//E-Arsiv Fatura Serisi
                IsEinvoice = false, //E-Fatura mı E-Ariv olduğu anlaşılır
                IsBrokerage = isBrokerage
            };

            if (customer.CorporateInvoice)//Kurumsal Fatura
            {
                if (!string.IsNullOrEmpty(customer.TaxNumber) && !string.IsNullOrEmpty(customer.TaxOffice))//Eğer Müşteri Kurumsal faturayı Seçipte Vergi numarasını ve vergi dairesini girmesse fatura kesilmez
                {
                    //Eğer vergi numarasından vadeli kurumsal cari bulursak ona göre işlemler yapılıyor.
                    var currentAccount = mikroUnitOfWork.CARI_HESAPLARRepository.ReadByTaxNumber(customer.TaxNumber);
                    if (currentAccount != null)
                    {
                        invoiceInfo.CurrentCode = currentAccount.cari_kod;
                        invoiceInfo.IsEinvoice = currentAccount.cari_efatura_fl;
                        invoiceInfo.IsBrokerage = false;//Vadeli çalışığımız carilerde virman olmayacak

                        customer.TaxOffice = !string.IsNullOrEmpty(currentAccount.cari_vdaire_adi) ? currentAccount.cari_vdaire_adi : customer.TaxOffice;
                        customer.FirstName = currentAccount.cari_unvan1;
                        customer.LastName = currentAccount.cari_unvan2;
                    }
                    else
                    {
                        customer.FirstName = customer.CorporateCompanyName;
                        customer.LastName = string.Empty;
                        invoiceInfo.CurrentCode = $"120.{company.AccountCompanyPrefix}.{customer.CustomerCode}"; //Kurumsal Futura Kodu

                    }

                    if (customer.IsEinvoice)
                    {
                        invoiceInfo.IsEinvoice = true;
                        invoiceInfo.InvoiceSerie = company.MikroOfficialEInvoiceSerie;//E-Fatura Fatura Serisi
                    }
                }
            }

            return invoiceInfo;
        }

        /// <summary>
        /// Adrese göre tc kimlik belirleyen method
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private TaxInfo GetTaxInfo(OrderModel order)
        {
            if (order.InvoiceAddress != null && order.InvoiceAddress.Neighborhood.District.City.Country.Id != "1")
                return new TaxInfo
                {
                    Number = "2222222222",
                    OfficeName = "NİHAİ TÜKETİCİ",
                    OfficeCode = "998"
                };

            if (order.Customer.CorporateInvoice && !string.IsNullOrEmpty(order.Customer.TaxNumber) && !string.IsNullOrEmpty(order.Customer.TaxOffice))
                return new TaxInfo
                {
                    Number = order.Customer.TaxNumber,
                    OfficeName = order.Customer.TaxOffice,
                    OfficeCode = string.Empty
                };

            return new TaxInfo
            {
                Number = "33333333333",
                OfficeName = "NİHAİ TÜKETİCİ",
                OfficeCode = "999"
            };
        }
        #endregion
    }
}
