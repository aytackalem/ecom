﻿namespace ECommerce.Accounting.Mikro.DataTransferObjects
{
    public class CustomerModel
    {
        #region Property
        public string CustomerCode { get; set; }

        public string CompanyId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }
        /// <summary>
        /// Vergi Dairesi
        /// </summary>
        public string TaxOffice { get; set; }
        /// <summary>
        /// Vergi Numarası
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// E Faturamı E Arsiv olduğunu anlıyoruz. E-Fature gelirse true gelir.
        /// </summary>
        public bool IsEinvoice { get; set; }

        /// <summary>
        /// Bireysel mi kurumsal mı olduğunu anlıyoruz Kurumsal gelirse true gelir.
        /// </summary>
        public bool CorporateInvoice { get; set; }

        /// <summary>
        /// Kurumsal faturada dolu gelicek faturayı kesen müşterinin cari ünvanı
        /// </summary>
        public string CorporateCompanyName { get; set; }
        #endregion

        #region Navigation Property
        public CompanyModel Company { get; set; }
        #endregion
    }
}
