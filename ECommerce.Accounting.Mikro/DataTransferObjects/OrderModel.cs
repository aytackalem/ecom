﻿using System.Text.Json.Serialization;

namespace ECommerce.Accounting.Mikro.DataTransferObjects
{
    public class OrderModel
    {
        #region Property

        public int OrderCode { get; set; }

        public string MarketOrderCode { get; set; }

        public string ShippingBarcode { get; set; }

        public string CompanyId { get; set; }

        public string ShipmentCompanyId { get; set; }

        public string SourceId { get; set; }

        public decimal Amount { get; set; }
        /// <summary>
        /// Musahabeye atılan fatura tutarını gösterir
        /// </summary>
        public decimal InvoiceAmount { get; set; }

        public decimal PaymentFee { get; set; }

        public decimal ShipmentFee { get; set; }

        public string OrderTypeId { get; set; }

        public string CustomerId { get; set; }        

        public DateTime CreatedDate { get; set; }

        public DateTime OrderDate { get; set; }

        [JsonIgnore]
        public string InvoiceNumber { get; set; }
        [JsonIgnore]
        public int WareHouseId { get; set; }
        [JsonIgnore]
        public int? ResponsibilityCenterId { get; set; }
        [JsonIgnore]
        public Guid? AccountTransactionId { get; set; }
        #endregion

        #region Navigation Property
        public WareHouseModel WareHouse { get; set; }

        public OrderTypeModel OrderType { get; set; }

        public CompanyModel Company { get; set; }

        public ShipmentCompanyModel ShipmentCompany { get; set; }

        public SourceModel Source { get; set; }

        public InvoiceAddressModel InvoiceAddress { get; set; }

        public CustomerModel Customer { get; set; }

        public ResponsibilityCenterModel ResponsibilityCenter { get; set; }

        public List<OrderDetailModel> OrderDetails { get; set; }

        public List<PaymentModel> Payments { get; set; }

        public List<OrderHistoryModel> OrderHistories { get; set; }
        #endregion
    }
}
