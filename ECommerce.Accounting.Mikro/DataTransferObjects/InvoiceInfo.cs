﻿namespace ECommerce.Accounting.Mikro.DataTransferObjects
{
    public class InvoiceInfo
    {
        #region Properties
        /// <summary>
        ///Fature E-fature ise (MikroOfficialEInvoiceSerie) seri gelir E-Arsivse (MikroOfficialInvoiceSerie) seri gelir
        /// </summary>
        public string InvoiceSerie { get; set; }

        public string CurrentCode { get; set; }

        public bool IsEinvoice { get; set; }

        /// <summary>
        /// Virman mı ZX atılacak mı
        /// </summary>
        public bool IsBrokerage { get; set; }
        #endregion
    }
}
