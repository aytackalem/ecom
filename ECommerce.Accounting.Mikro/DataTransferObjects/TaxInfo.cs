﻿namespace ECommerce.Accounting.Mikro.DataTransferObjects
{
    public class TaxInfo
    {
        #region Properties
        public string Number { get; set; }

        public string OfficeName { get; set; }

        public string OfficeCode { get; set; }
        #endregion
    }
}
