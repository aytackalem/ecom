using Dapper;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Accounting.Mikro.Repository.Base;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository
{
    public partial class CARI_HESAP_HAREKETLERI_EKRepository : RepositoryBase<CARI_HESAP_HAREKETLERI_EK, Guid>, ICARI_HESAP_HAREKETLERI_EKRepository
	{
		#region Constructor
		public CARI_HESAP_HAREKETLERI_EKRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<CARI_HESAP_HAREKETLERI_EK> Read()
		{
			return _dbConnection.Query<CARI_HESAP_HAREKETLERI_EK>(sql: $"Select * From [CARI_HESAP_HAREKETLERI_EK] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public CARI_HESAP_HAREKETLERI_EK Read(Guid chaek_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<CARI_HESAP_HAREKETLERI_EK>(sql: $"Select * From [CARI_HESAP_HAREKETLERI_EK] With(NoLock) Where [chaek_Guid] = @chaek_Guid", transaction: _dbTransaction, param: new { chaek_Guid = chaek_Guid });
		}

		#endregion
	}
}