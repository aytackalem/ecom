using Dapper;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Accounting.Mikro.Repository.Base;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository
{
    public partial class EVRAK_ACIKLAMALARIRepository : RepositoryBase<EVRAK_ACIKLAMALARI, Guid>, IEVRAK_ACIKLAMALARIRepository
	{
		#region Constructor
		public EVRAK_ACIKLAMALARIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<EVRAK_ACIKLAMALARI> Read()
		{
			return _dbConnection.Query<EVRAK_ACIKLAMALARI>(sql: $"Select * From [EVRAK_ACIKLAMALARI] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public EVRAK_ACIKLAMALARI Read(Guid egk_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<EVRAK_ACIKLAMALARI>(sql: $"Select * From [EVRAK_ACIKLAMALARI] With(NoLock) Where [egk_Guid] = @egk_Guid", transaction: _dbTransaction, param: new { egk_Guid = egk_Guid });
		}

		#endregion
	}
}