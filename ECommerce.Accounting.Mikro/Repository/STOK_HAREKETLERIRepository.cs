using Dapper;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Accounting.Mikro.Repository.Base;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository
{
    public partial class STOK_HAREKETLERIRepository : RepositoryBase<STOK_HAREKETLERI, Guid>, ISTOK_HAREKETLERIRepository
	{
		#region Constructor
		public STOK_HAREKETLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<STOK_HAREKETLERI> Read()
		{
			return _dbConnection.Query<STOK_HAREKETLERI>(sql: $"Select * From [STOK_HAREKETLERI] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public STOK_HAREKETLERI Read(Guid sth_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<STOK_HAREKETLERI>(sql: $"Select * From [STOK_HAREKETLERI] With(NoLock) Where [sth_Guid] = @sth_Guid", transaction: _dbTransaction, param: new { sth_Guid = sth_Guid });
		}

		#endregion
	}
}