using Dapper;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Accounting.Mikro.Repository.Base;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository
{
    public partial class CARI_HESAP_HAREKETLERIRepository : RepositoryBase<CARI_HESAP_HAREKETLERI, Guid>, ICARI_HESAP_HAREKETLERIRepository
	{
		#region Constructor
		public CARI_HESAP_HAREKETLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<CARI_HESAP_HAREKETLERI> Read()
		{
			return _dbConnection.Query<CARI_HESAP_HAREKETLERI>(sql: $"Select * From [CARI_HESAP_HAREKETLERI] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public CARI_HESAP_HAREKETLERI Read(Guid cha_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<CARI_HESAP_HAREKETLERI>(sql: $"Select * From [CARI_HESAP_HAREKETLERI] With(NoLock) Where [cha_Guid] = @cha_Guid", transaction: _dbTransaction, param: new { cha_Guid = cha_Guid });
		}

		#endregion
	}
}