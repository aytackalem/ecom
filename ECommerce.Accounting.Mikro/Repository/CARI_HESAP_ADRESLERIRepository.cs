using Dapper;
using ECommerce.Accounting.Mikro.Entities;
using ECommerce.Accounting.Mikro.Repository.Base;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository
{
    public partial class CARI_HESAP_ADRESLERIRepository : RepositoryBase<CARI_HESAP_ADRESLERI, Guid>, ICARI_HESAP_ADRESLERIRepository
	{
		#region Constructor
		public CARI_HESAP_ADRESLERIRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
		{
		}
		#endregion

		#region Method
		public List<CARI_HESAP_ADRESLERI> Read()
		{
			return _dbConnection.Query<CARI_HESAP_ADRESLERI>(sql: $"Select * From [CARI_HESAP_ADRESLERI] With(NoLock)", transaction: _dbTransaction).ToList();
		}

		public CARI_HESAP_ADRESLERI Read(Guid adr_Guid)
		{
			return _dbConnection.QueryFirstOrDefault<CARI_HESAP_ADRESLERI>(sql: $"Select * From [CARI_HESAP_ADRESLERI] With(NoLock) Where [adr_Guid] = @adr_Guid", transaction: _dbTransaction, param: new { adr_Guid = adr_Guid });
		}

		#endregion
	}
}