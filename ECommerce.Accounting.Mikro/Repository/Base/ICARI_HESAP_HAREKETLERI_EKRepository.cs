using ECommerce.Accounting.Mikro.Entities;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public interface ICARI_HESAP_HAREKETLERI_EKRepository : IRepository<CARI_HESAP_HAREKETLERI_EK, Guid>
	{
		#region Method
		List<CARI_HESAP_HAREKETLERI_EK> Read();

		CARI_HESAP_HAREKETLERI_EK Read(Guid chaek_Guid);

		#endregion
	}
}