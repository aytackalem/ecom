using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public abstract class RepositoryBase<TEntity, TId> where TEntity : class
    {
        #region Field
        protected IDbConnection _dbConnection;

        protected IDbTransaction _dbTransaction;
        #endregion

        #region Constructor
        public RepositoryBase(IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            #region Field
            _dbConnection = dbConnection;
            _dbTransaction = dbTransaction;
            #endregion
        }
        #endregion

        #region Method
        public Boolean Create(TEntity entity)
        {
            _dbConnection.Insert<TEntity>(entityToInsert: entity, transaction: _dbTransaction);
            return true;
        }

        public Boolean Update(TEntity entity)
        {
            return _dbConnection.Update(entityToUpdate: entity, transaction: _dbTransaction);
        }

        protected IEnumerable<D> Query<D>(string sql, object param = null)
        {
            if (string.IsNullOrEmpty(sql))
                throw new ArgumentNullException("Sql cannot be null or empty.");

            return _dbConnection.Query<D>(sql: sql, param: param, transaction: _dbTransaction);
        }

        protected D QueryFirstOrDefault<D>(string sql, object param = null)
        {
            if (string.IsNullOrEmpty(sql))
                throw new ArgumentNullException("Sql cannot be null or empty.");

            return _dbConnection.QueryFirstOrDefault<D>(sql: sql, param: param, transaction: _dbTransaction);
        }
        #endregion
    }
}