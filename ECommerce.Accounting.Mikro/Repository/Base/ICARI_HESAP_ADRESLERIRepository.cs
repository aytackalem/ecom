using ECommerce.Accounting.Mikro.Entities;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public partial interface ICARI_HESAP_ADRESLERIRepository : IRepository<CARI_HESAP_ADRESLERI, Guid>
	{
		#region Method
		List<CARI_HESAP_ADRESLERI> Read();

		CARI_HESAP_ADRESLERI Read(Guid adr_Guid);

		#endregion
	}
}