using ECommerce.Accounting.Mikro.Entities;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public interface IEVRAK_ACIKLAMALARIRepository : IRepository<EVRAK_ACIKLAMALARI, Guid>
	{
		#region Method
		List<EVRAK_ACIKLAMALARI> Read();

		EVRAK_ACIKLAMALARI Read(Guid egk_Guid);

		#endregion
	}
}