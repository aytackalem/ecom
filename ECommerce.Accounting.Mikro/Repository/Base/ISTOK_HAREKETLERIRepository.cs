using ECommerce.Accounting.Mikro.Entities;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public partial interface ISTOK_HAREKETLERIRepository : IRepository<STOK_HAREKETLERI, Guid>
	{
		#region Method
		List<STOK_HAREKETLERI> Read();

		STOK_HAREKETLERI Read(Guid sth_Guid);

		#endregion
	}
}