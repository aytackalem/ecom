using ECommerce.Accounting.Mikro.Entities;

namespace ECommerce.Accounting.Mikro.Repository.Base
{
    public partial interface ICARI_HESAP_HAREKETLERIRepository : IRepository<CARI_HESAP_HAREKETLERI, Guid>
	{
		#region Method
		List<CARI_HESAP_HAREKETLERI> Read();

		CARI_HESAP_HAREKETLERI Read(Guid cha_Guid);

		#endregion
	}
}