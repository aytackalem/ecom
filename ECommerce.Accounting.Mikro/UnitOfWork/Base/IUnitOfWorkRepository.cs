using ECommerce.Accounting.Mikro.Repository.Base;

namespace ECommerce.Accounting.Mikro.UnitOfWork.Base
{
    public interface IUnitOfWork : IDisposable
    {
        #region Property
        public ICARI_HESAP_HAREKETLERIRepository CARI_HESAP_HAREKETLERIRepository { get; }

        public ICARI_HESAPLARRepository CARI_HESAPLARRepository { get; }

        public ICARI_HESAP_ADRESLERIRepository CARI_HESAP_ADRESLERIRepository { get; }

        public IEVRAK_ACIKLAMALARIRepository EVRAK_ACIKLAMALARIRepository { get; }

        public ISTOK_HAREKETLERIRepository STOK_HAREKETLERIRepository { get; }

        public ICARI_HESAP_HAREKETLERI_EKRepository CARI_HESAP_HAREKETLERI_EKRepository { get; }

        #endregion

        #region Method
        public void BeginTransaction();

        public void CommitTransaction();

        public void RollbackTransaction();
        #endregion
    }
}