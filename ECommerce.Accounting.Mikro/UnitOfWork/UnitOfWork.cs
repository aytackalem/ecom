using ECommerce.Accounting.Mikro.Repository;
using ECommerce.Accounting.Mikro.Repository.Base;
using ECommerce.Accounting.Mikro.UnitOfWork.Base;
using System.Data;
using System.Data.SqlClient;

namespace ECommerce.Accounting.Mikro.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Field
        protected IDbConnection _dbConnection;

        protected IDbTransaction _dbTransaction;

        private ICARI_HESAP_HAREKETLERIRepository _CARI_HESAP_HAREKETLERIRepository;

        private ICARI_HESAPLARRepository _CARI_HESAPLARRepository;

        private ICARI_HESAP_ADRESLERIRepository _CARI_HESAP_ADRESLERIRepository;

        private IEVRAK_ACIKLAMALARIRepository _EVRAK_ACIKLAMALARIRepository;

        private ISTOK_HAREKETLERIRepository _STOK_HAREKETLERIRepository;

        private ICARI_HESAP_HAREKETLERI_EKRepository _CARI_HESAP_HAREKETLERI_EKRepository;

        private readonly string _connectionString;

        #endregion

        #region Constructor
        public UnitOfWork(string connectionString)
        {
            #region Field
            _connectionString = connectionString;
            _dbConnection = new SqlConnection(connectionString);
            #endregion
        }
        #endregion

        #region Properties
        public ICARI_HESAP_HAREKETLERIRepository CARI_HESAP_HAREKETLERIRepository
        {
            get
            {
                if (_CARI_HESAP_HAREKETLERIRepository == null)
                    _CARI_HESAP_HAREKETLERIRepository = new CARI_HESAP_HAREKETLERIRepository(_dbConnection, _dbTransaction);

                return _CARI_HESAP_HAREKETLERIRepository;
            }
        }

        public ICARI_HESAPLARRepository CARI_HESAPLARRepository
        {
            get
            {
                if (_CARI_HESAPLARRepository == null)
                    _CARI_HESAPLARRepository = new CARI_HESAPLARRepository(_dbConnection, _dbTransaction);

                return _CARI_HESAPLARRepository;
            }
        }

        public ICARI_HESAP_ADRESLERIRepository CARI_HESAP_ADRESLERIRepository
        {
            get
            {
                if (_CARI_HESAP_ADRESLERIRepository == null)
                    _CARI_HESAP_ADRESLERIRepository = new CARI_HESAP_ADRESLERIRepository(_dbConnection, _dbTransaction);

                return _CARI_HESAP_ADRESLERIRepository;
            }
        }

        public IEVRAK_ACIKLAMALARIRepository EVRAK_ACIKLAMALARIRepository
        {
            get
            {
                if (_EVRAK_ACIKLAMALARIRepository == null)
                    _EVRAK_ACIKLAMALARIRepository = new EVRAK_ACIKLAMALARIRepository(_dbConnection, _dbTransaction);

                return _EVRAK_ACIKLAMALARIRepository;
            }
        }

        public ISTOK_HAREKETLERIRepository STOK_HAREKETLERIRepository
        {
            get
            {
                if (_STOK_HAREKETLERIRepository == null)
                    _STOK_HAREKETLERIRepository = new STOK_HAREKETLERIRepository(_dbConnection, _dbTransaction);

                return _STOK_HAREKETLERIRepository;
            }
        }


        public ICARI_HESAP_HAREKETLERI_EKRepository CARI_HESAP_HAREKETLERI_EKRepository
        {
            get
            {
                if (_CARI_HESAP_HAREKETLERI_EKRepository == null)
                    _CARI_HESAP_HAREKETLERI_EKRepository = new CARI_HESAP_HAREKETLERI_EKRepository(_dbConnection, _dbTransaction);

                return _CARI_HESAP_HAREKETLERI_EKRepository;
            }
        }
        #endregion

        #region Method
        public void BeginTransaction()
        {
            _dbConnection.Open();
            _dbTransaction = _dbConnection.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        public void CommitTransaction()
        {
            if (_dbTransaction != null)
            {
                _dbTransaction.Commit();
                _dbConnection.Close();
            }
        }

        public void RollbackTransaction()
        {
            if (_dbTransaction != null)
            {
                _dbTransaction.Rollback();
                _dbConnection.Close();
            }
        }
        #endregion

        #region IDisposable
        bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                if (_CARI_HESAP_HAREKETLERIRepository != null)
                {
                    _CARI_HESAP_HAREKETLERIRepository = null;
                }

                if (_CARI_HESAPLARRepository != null)
                {
                    _CARI_HESAPLARRepository = null;
                }

                if (_CARI_HESAP_ADRESLERIRepository != null)
                {
                    _CARI_HESAP_ADRESLERIRepository = null;
                }

                if (_EVRAK_ACIKLAMALARIRepository != null)
                {
                    _EVRAK_ACIKLAMALARIRepository = null;
                }

                if (_STOK_HAREKETLERIRepository != null)
                {
                    _STOK_HAREKETLERIRepository = null;
                }
            }

            disposed = true;
        }
        #endregion

        #region Destructor
        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}