﻿using Microsoft.AspNetCore.Builder;

namespace Helpy.Middlewares
{
    public static class MiddlewareExtension
    {
        public static IApplicationBuilder UseDomainIdCheck(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<DomainIdCheckMiddleware>();
        }
    }
}
