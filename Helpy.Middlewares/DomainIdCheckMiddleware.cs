﻿using Helpy.Responses;
using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace Helpy.Middlewares
{
    public class DomainIdCheckMiddleware
    {
        private readonly RequestDelegate _next;
        public DomainIdCheckMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var contains = httpContext.Request.Headers.ContainsKey("DomainId");
            if (contains == false)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync(JsonSerializer.Serialize(new Response(ResponseCodes.Bad)));
                return;
            }

            if (int.TryParse(httpContext.Request.Headers["DomainId"].ToString(), out int domainId) == false)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync(JsonSerializer.Serialize(new Response(ResponseCodes.Bad)));
                return;
            }

            await _next.Invoke(httpContext);
        }
    }
}
