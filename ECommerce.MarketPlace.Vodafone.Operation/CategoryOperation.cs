﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Vodafone.Common.Request.Token;
using ECommerce.MarketPlace.Vodafone.Common.Response.Category;
using ECommerce.MarketPlace.Vodafone.Common.Response.CategoryAttribute;
using ECommerce.MarketPlace.Vodafone.Common.Response.Token;

namespace ECommerce.MarketPlace.Vodafone.Operation
{
    public class CategoryOperation : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private const string _baseUrl = "https://vfmallapi.vodafone.com.tr/vfmallapi";
        #endregion

        #region Constructors
        public CategoryOperation(IHttpHelper httpHelper)
        {
            #region Fields
            _httpHelper = httpHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var tokenResponse = this.GetToken();
                tokenResponse.token = tokenResponse.token.Replace("bearer ", "");

                response.Data = new List<CategoryResponse>();

                var page = 0;
                var size = 100;
                var categoryUrl = $"{_baseUrl}/getVfMallCategories?page={page}&size={size}";

                VodafoneCategoryResponse vodafoneCategoryResponse = null;
                do
                {
                    vodafoneCategoryResponse = this
                        ._httpHelper
                        .Get<VodafoneCategoryResponse>(categoryUrl, "Bearer", tokenResponse.token);

                    if (vodafoneCategoryResponse.Result.Result.ToLower() == "success" &&
                        vodafoneCategoryResponse.Result.ResultCode == "0")
                    {
                        vodafoneCategoryResponse.GetCategories.Categories.ForEach(c =>
                        {
                            if (c.Name.Contains("Güneş Koruyucuları") || c.Name.Contains("Yüz Nemlendiriciler") || c.Name.Contains("Vücut Nemlendirici Krem, Losyon"))
                                response.Data.Add(new CategoryResponse
                                {
                                    Code = c.Id.ToString(),
                                    Name = c.Name,
                                    ParentCategoryCode = c.ParentId,
                                });
                        });
                    }

                    page++;
                    categoryUrl = $"{_baseUrl}/getVfMallCategories?page={page}&size={size}";
                } while (vodafoneCategoryResponse.GetCategories.TotalPages > page);

                response.Data.ForEach(d =>
                {
                    var categoryAttributeUrl = $"{_baseUrl}/getVfMallCategoryAttributes?id={d.Code}";
                    var vodafoneCategoryAttributeResponse = this
                        ._httpHelper
                        .Get<VodafoneCategoryAttributeResponse>(categoryAttributeUrl, "Bearer", tokenResponse.token);

                    if (vodafoneCategoryAttributeResponse.Result.Result.ToLower() == "success" &&
                        vodafoneCategoryAttributeResponse.Result.ResultCode == "0")
                    {
                        d.CategoryAttributes = vodafoneCategoryAttributeResponse
                            .ExtensionList
                            .First()
                            .Parameters
                            .Select(p => new CategoryAttributeResponse
                            {
                                Code = p.Id,
                                Name = p.Name,
                                CategoryAttributesValues = p
                                    .Value
                                    .Select(v => new CategoryAttributeValueResponse
                                    {
                                        Code = null,
                                        Name = v
                                    })
                                    .ToList()
                            })
                            .ToList();
                    }
                });

                response.Success = true;
            });
        }
        #endregion

        #region Helper Methods
        private TokenResponse GetToken()
        {
            return this._httpHelper.Post<TokenRequest, TokenResponse>(new TokenRequest
            {
                integratorCode = null,
                password = "Selim123456!",
                username = "selim.corekci@biolog.com.tr"
            }, $"{_baseUrl}/createVfMallToken", null, null);
        }
        #endregion
    }
}
