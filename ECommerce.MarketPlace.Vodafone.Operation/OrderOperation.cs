﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Vodafone.Common.Request.OrderStatus;
using ECommerce.MarketPlace.Vodafone.Common.Request.Shipment;
using ECommerce.MarketPlace.Vodafone.Common.Request.Token;
using ECommerce.MarketPlace.Vodafone.Common.Response.Orders;
using ECommerce.MarketPlace.Vodafone.Common.Response.OrderStatus;
using ECommerce.MarketPlace.Vodafone.Common.Response.Shipment;
using ECommerce.MarketPlace.Vodafone.Common.Response.Token;

namespace ECommerce.MarketPlace.Vodafone.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        private const string _baseUrl = "https://vfmallapi.vodafone.com.tr/vfmallapi";

        public readonly IHttpHelper _httpHelper;
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
        }
        #endregion

        #region Methods
        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            OrderResponse response = new()
            {
                Orders = new List<MarketPlace.Common.Response.Order.Order>()
            };

            var vodafoneOrders = new List<VodafoneOrder>();

            #region Fetch Data
            var tokenResponse = this.GetToken();
            tokenResponse.token = tokenResponse.token.Replace("bearer ", "");

            var page = 0;

            VodafoneOrderResponse vodafoneOrderResponse = null;

            do
            {
                var url = $"{_baseUrl}/getVfMallOrders?status=PENDING&shipmentStatus=UNPACKED&page={page}";

                vodafoneOrderResponse = _httpHelper.Get<VodafoneOrderResponse>(url, "Bearer", tokenResponse.token);

                if (vodafoneOrderResponse != null &&
                    vodafoneOrderResponse.Result.Result.ToLower() == "success" &&
                    vodafoneOrderResponse.Result.ResultCode == "0")
                {
                    vodafoneOrders.AddRange(vodafoneOrderResponse.Orders);
                }

                page++;
            } while (vodafoneOrderResponse?.totalPages > page);
            #endregion

            var groupedVodafoneOrders = vodafoneOrders.GroupBy(vo => vo.id);
            foreach (var gvoLoop in groupedVodafoneOrders)
            {
                foreach (var item in gvoLoop)
                {
                    var r = this._httpHelper.Post<VodafoneOrderStatusRequest, VodafoneOrderStatusResponse>(
                        new VodafoneOrderStatusRequest
                        {
                            orderId = item.id,
                            newStatus = "PREPARING"
                        },
                        $"{_baseUrl}/v2/updateVfMallOrderStatus",
                        "Bearer",
                        tokenResponse.token);
                }

                var url = $"{_baseUrl}/v2/shipVfMallOrders";

                var vodafoneShipmentResponse = this
                    ._httpHelper
                    .Post<VodafoneShipmentRequest, VodafoneShipmentResponse>(new VodafoneShipmentRequest
                    {
                        orderIds = gvoLoop.Select(x => x.id).ToList()
                    }, url, "Bearer", tokenResponse.token);

                var orderDeliveryAddress = gvoLoop.First().customerContactInfoList.First(cci => cci.type == "DELIVERY");
                var orderInvoiceAddress = gvoLoop.First().customerContactInfoList.First(cci => cci.type == "INVOICE");
                var shipmentCompanyId = this.FindShipmentCompanyId(gvoLoop.First().cargoCompanyId);

                response.Orders.Add(new MarketPlace.Common.Response.Order.Order
                {
                    OrderSourceId = OrderSources.Pazaryeri,
                    MarketPlaceId = Marketplaces.Vodafone,
                    CurrencyId = "TL",
                    TotalAmount = gvoLoop.Sum(x => x.salePrice),
                    ListTotalAmount = gvoLoop.Sum(x => x.listPrice),
                    Discount = gvoLoop.Sum(x => x.discount),
                    ShippingCost = 0,
                    OrderTypeId = "OS",
                    OrderPicking = new MarketPlace.Common.Request.Order.OrderPicking { MarketPlaceOrderId = gvoLoop.First().id },
                    Source = "VO",
                    OrderCode = gvoLoop.First().id,
                    OrderDate = DateTime.Parse(gvoLoop.First().created.Value),
                    OrderShipment = new MarketPlace.Common.Response.Order.OrderShipment
                    {
                        TrackingCode = gvoLoop.First().barcode,
                        ShipmentTypeId = "PND",
                        ShipmentCompanyId = shipmentCompanyId
                    },
                    Customer = new MarketPlace.Common.Response.Order.Customer
                    {
                        FirstName = orderInvoiceAddress.FullName,
                        LastName = orderInvoiceAddress.FullName,
                        IdentityNumber = "11111111111",
                        Mail = orderInvoiceAddress.Email,
                        Phone = orderInvoiceAddress.PhoneNumber
                    },
                    OrderDeliveryAddress = new MarketPlace.Common.Response.Order.OrderDeliveryAddress
                    {
                        Address = orderDeliveryAddress.address,
                        City = orderDeliveryAddress.City,
                        District = orderDeliveryAddress.Town,
                        Email = orderDeliveryAddress.Email,
                        Phone = orderDeliveryAddress.PhoneNumber
                    },
                    OrderInvoiceAddress = new MarketPlace.Common.Response.Order.OrderInvoiceAddress
                    {
                        Address = orderInvoiceAddress.address,
                        City = orderInvoiceAddress.City,
                        District = orderInvoiceAddress.Town,
                        Email = orderInvoiceAddress.Email,
                        Phone = orderInvoiceAddress.PhoneNumber
                    },
                    OrderDetails = gvoLoop
                        .GroupBy(x => x.barcode)
                        .Select(x => new MarketPlace.Common.Response.Order.OrderDetail
                        {
                            TaxRate = Convert.ToDouble(x.First().vatRate) / 100,
                            UnitPrice = x.First().salePrice,
                            ListPrice = x.First().listPrice,
                            UnitDiscount = x.First().discount,
                            Product = new MarketPlace.Common.Response.Order.Product()
                            {
                                Barcode = x.First().barcode.ToUpper(),
                                Name = x.First().offeringName,
                                StockCode = x.First().stockCode
                            },
                            Quantity = 1
                        })
                        .ToList()
                });
            }

            return new DataResponse<OrderResponse>
            {
                Data = response
            };
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public Application.Common.Wrappers.Response OrderTypeUpdate(OrderPickingRequest request)
        {
            var tokenResponse = this.GetToken();
            tokenResponse.token = tokenResponse.token.Replace("bearer ", "");

            _httpHelper.Post<VodafoneOrderStatusRequest, VodafoneOrderStatusResponse>(new VodafoneOrderStatusRequest
            {
                newStatus = request.Status == MarketPlaceStatus.Picking ? "PREPARING" : "COMPLETED",
                orderId = request.OrderPicking.MarketPlaceOrderId
            }, $"{_baseUrl}/v2/updateVfMallOrderStatus", "Bearer", tokenResponse.token);

            return new Application.Common.Wrappers.Response { };
        }
        #endregion

        #region Helper Methods
        private TokenResponse GetToken()
        {
            return this._httpHelper.Post<TokenRequest, TokenResponse>(new TokenRequest
            {
                integratorCode = null,
                password = "Kozmo2022!2008",//"SİNnoz2022!.",
                username = "info@kozmobox.com"//"pazaryeri@sinoz.com.tr"
            }, $"{_baseUrl}/createVfMallToken", null, null);
        }

        private string FindShipmentCompanyId(string cargoCompanyId)
        {
            switch (cargoCompanyId)
            {
                case "601be721a23ffc44f4864240":
                    return ShipmentCompanies.MngKargo;
                case "6183b250026e19ca7df748e8":
                    return ShipmentCompanies.ArasKargo;
                case "601be73da23ffc44f4864242":
                    return ShipmentCompanies.YurtiçiKargo;
                case "601be73da23ffc44f4864243":
                    return ShipmentCompanies.PttKargo;
                case "60d43f7a026e19ca7dfb29bf":
                    return ShipmentCompanies.Ups;
                case "60d44687026e19ca7dfbf337":
                    return ShipmentCompanies.HepsiJet;
                default:
                    throw new Exception("Unsupported shipment company.");
            }
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}