﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Stock;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.ProductFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECommerce.MarketPlace.Omni.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        public readonly string _apiUrl;

        #endregion

        #region Constructors
        public ProductOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
            _apiUrl = "http://localhost:11201";
        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var _productResponse = this._httpHelper.Post<ProductRequest, Response>(productRequest, $"{_apiUrl}/Product/Create", null, null);

                response.Success = _productResponse.Success;
                response.Message = _productResponse.Message;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var _stockResponse = this._httpHelper.Post<StockRequest, Response>(stockRequest, $"{_apiUrl}/Product/UpdateStock", null, null);
                response.Success = _stockResponse.Success;
                response.Message = _stockResponse.Message;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        private DataResponse<ProductFilterResponse> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductFilterResponse>>((response) =>
            {
              

                response.Success = true;
            });
        }
        #endregion
    }
}
