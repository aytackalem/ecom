﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Common.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECommerce.MarketPlace.Common.Request.Enums;

namespace ECommerce.MarketPlace.Omni.Operation
{
    public class OrderOperation : IOrderFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        public readonly string _apiUrl;
        #endregion

        #region Constructors
        public OrderOperation(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
            _apiUrl = "http://localhost:11201";

        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
             {

                 var _orderResponse = this._httpHelper.Post<OrderRequest, DataResponse<OrderResponse>>(orderRequest, $"{_apiUrl}/Order/Get", null, null);

                 response.Data = _orderResponse.Data;
                 response.Success = _orderResponse.Success;
                 response.Message = _orderResponse.Message;
             }, (response, exception) =>
             {
                 response.Success = false;
                 response.Message = "";
             });
        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {

                var _checkPreparingResponse = this._httpHelper.Post<OrderRequest, DataResponse<CheckPreparingResponse>>(orderRequest, $"{_apiUrl}/Order/CheckPreparing", null, null);

                response.Data = _checkPreparingResponse.Data;
                response.Success = _checkPreparingResponse.Success;
                response.Message = _checkPreparingResponse.Message;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Omni ile bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
#if !DEBUG


                switch (orderPackaging.Status)
                {
                    case MarketPlaceStatus.Picking:
                        orderPackaging.Configurations.Add("StatusId", "0");
                        break;
                    case MarketPlaceStatus.Packaging:
                        orderPackaging.Configurations.Add("StatusId", "12");
                        break;
                }

                var _orderSourceUpdate = this._httpHelper.Post<OrderPickingRequest, Response>(orderPackaging, $"{_apiUrl}/Order/OrderSourceUpdate", null, null);
                response.Success = _orderSourceUpdate.Success;
                response.Message = _orderSourceUpdate.Message; 
#endif


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
