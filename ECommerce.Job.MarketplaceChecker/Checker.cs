﻿    using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Checker;

namespace ECommerce.Job.MarketplaceChecker
{
    public class Checker
    {
        #region Fields
        private readonly MarketplaceRequestCheckerResolver _marketplaceRequestCheckerResolver;
        #endregion

        #region Constructors
        public Checker(MarketplaceRequestCheckerResolver marketplaceRequestCheckerResolver)
        {
            #region Fields
            this._marketplaceRequestCheckerResolver = marketplaceRequestCheckerResolver;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(string[] args)
        {
            var queueId = int.Parse(args[0]);
            var requestType = args[1];
            var marketplaceId = args[2];

            var marketplaceRequestChecker = this._marketplaceRequestCheckerResolver(requestType);
            await marketplaceRequestChecker.CheckAsync(new MarketplaceRequestCheckerRequest
            {
                QueueId = queueId,
                MarketplaceId = marketplaceId
            });
        }
        #endregion
    }
}
