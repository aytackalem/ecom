﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Checker.Base;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Job.MarketplaceChecker.Helpers;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.MarketplacesV2;
using ECommerce.Persistence.Common.MarketplacesV2.Checker;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.MarketplaceChecker
{
    internal class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IConfiguration>(p => configuration);


            serviceCollection.AddSingleton<IDbService, DbService>();

            serviceCollection.AddDbContext<ApplicationDbContext>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ITenantFinder>(tf => null);

            serviceCollection.AddScoped<IDomainFinder>(df => null);

            serviceCollection.AddScoped<ICompanyFinder>(cf => null);

            serviceCollection.AddScoped<IDbNameFinder, DbNameFinder>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IMarketplaceErrorFacade, MarketplaceErrorFacade>();

            #region Marketplace Request Checker Injections
            serviceCollection.AddTransient<MarketplaceRequestCheckerCreateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestCheckerUpdateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestCheckerUpdatePrice>();

            serviceCollection.AddTransient<MarketplaceRequestCheckerUpdateStock>();

            serviceCollection.AddTransient<MarketplaceRequestCheckerResolver>(serviceProvider => requestType =>
            {
                IMarketplaceRequestChecker p = requestType switch
                {
                    "CreateProduct" => serviceProvider.GetService<MarketplaceRequestCheckerCreateProduct>(),
                    "UpdateProduct" => serviceProvider.GetService<MarketplaceRequestCheckerUpdateProduct>(),
                    "UpdatePrice" => serviceProvider.GetService<MarketplaceRequestCheckerUpdatePrice>(),
                    "UpdateStock" => serviceProvider.GetService<MarketplaceRequestCheckerUpdateStock>(),
                    _ => null
                };
                return p;
            });
            #endregion

            serviceCollection.AddTransient<IMarketplaceRequestReader, MarketplaceRequestReader>();

            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddScoped<Checker>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            #region Checks
#if DEBUG
            args = new string[] { "1", "UpdateStock", "BY" };
#endif

            if (args.Length != 3)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Checker", "Please check jos argumants.", new string[] { });
                return;
            }

            if (int.TryParse(args[0], out int i) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Checker", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "UpdatePrice", "UpdateStock", "CreateProduct", "UpdateProduct" }.Contains(args[1]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Checker", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "CS", "HB", "M", "TY", "TYE", "PA", "IS", "MN", "N11V2", "LCW", "BY" }.Contains(args[2]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Checker", "Please check jos argumants.", new string[] { });
                return;
            }
            #endregion

            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);

            var dbNames = new List<string>();

            using (var scope = serviceProvider.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = await dbService.GetNamesAsync();
                if (getNamesResponse.Success == false)
                {
                    SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Db names not fetched.");
                    return;
                }

                dbNames = getNamesResponse.Data;
            }

            if (dbNames.HasItem())
                foreach (var theDbName in dbNames)
                {
#if DEBUG
                    if (theDbName != "Mizalle")
                        continue;
#endif
                    using (var scope = serviceProvider.CreateScope())
                    {
                        var dbNameFinder = scope.ServiceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(theDbName);

                        await scope.ServiceProvider.GetRequiredService<Checker>().RunAsync(args);
                    }
                }
        }
        #endregion
    }
}