﻿using System.Drawing;
using System.Drawing.Printing;
using Helpy.Shared.Print.Base;

namespace Helpy.Shared.Print
{
    public class Page : CompositeElement
    {
        public Page(List<Element> elements)
        {
            Elements.AddRange(elements);
        }

        public int MarginTop { get; set; }

        public int MarginLeft { get; set; }

        public bool Infinite { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;

            y = MarginTop;
            x = MarginLeft;

            for (int i = 0; i < Elements.Count; i++)
            {
                Elements[i].Print(e, x, y);

                y += Elements[i].Height;

                if (Elements[i] is Row row)
                    y += row.MarginBottom;
            }
        }
    }
}