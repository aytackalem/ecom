﻿using System.Drawing.Printing;

namespace Helpy.Shared.Print.Base
{
    public abstract class Element
    {
        public int Height { get; set; }

        public int Width { get; set; }

        public abstract void Print(PrintPageEventArgs e, int x, int y);
    }
}