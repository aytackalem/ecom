﻿using System.Drawing;
using System.Drawing.Printing;
using Helpy.Shared.Print.Base;

namespace Helpy.Shared.Print
{
    public class Cell : CompositeElement
    {
        public Cell(int width, int height, Element element)
        {
            this.Width = width;
            this.Height = height;
            Elements.Add(element);
        }

        public bool Border { get; set; }

        public bool Dashed { get; set; }

        public bool AlignCenterChild { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            if (Border)
            {
                Pen pen = new(Brushes.Black, 0.2F);
                if (Dashed)
                    pen.DashPattern = new float[] { 3F, 3F, };

                e.Graphics.DrawLine(pen, x, y, x + Width, y);
                e.Graphics.DrawLine(pen, x, y + Height, x + Width, y + Height);
                e.Graphics.DrawLine(pen, x, y, x, y + Height);
                e.Graphics.DrawLine(pen, x + Width, y, x + Width, y + Height);
            }

            for (int i = 0; i < Elements.Count; i++)
            {
                if (AlignCenterChild)
                {
                    var marginLeft = (Width - Elements[i].Width) / 2;
                    x += marginLeft;
                }

                Elements[i].Print(e, x, y);
            }
        }
    }
}