﻿using System.Drawing;
using System.Drawing.Printing;
using Helpy.Shared.Print.Base;

namespace Helpy.Shared.Print
{
    public class Picture : Element
    {
        public Picture(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public string Src { get; set; }

        public override void Print(PrintPageEventArgs e, int x, int y)
        {
            var image = Image.FromFile(Src);

            e.Graphics.DrawImage(image, x, y, Width, Height /*new RectangleF(x, y, Width, Height)*/);
        }
    }
}