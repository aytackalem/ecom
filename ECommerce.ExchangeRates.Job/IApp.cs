﻿namespace ECommerce.ExchangeRates.Job
{
    public interface IApp
    {
        #region Methods
        void Run(); 
        #endregion
    }
}
