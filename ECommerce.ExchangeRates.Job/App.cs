﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using System.Xml;

namespace ECommerce.ExchangeRates.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            string url = "http://www.tcmb.gov.tr/kurlar/today.xml";
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(url);

            string usd = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/BanknoteSelling").InnerXml;
            //string eur = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='EUR']/BanknoteSelling").InnerXml;

            var exchangeRate = this
                ._unitOfWork
                .ExchangeRateRepository
                .DbSet()
                .Where(er => er.CreatedDate.Date == DateTime.Now.Date && er.CurrencyId == "USD")
                .FirstOrDefault();
            if (exchangeRate == null)
            {
                this
                    ._unitOfWork
                    .ExchangeRateRepository
                    .Create(new Domain.Entities.ExchangeRate
                    {
                        CreatedDate = DateTime.Now,
                        CurrencyId = "USD",
                        Rate = decimal.Parse(usd)
                    });
            }
            else
            {
                exchangeRate.Rate = decimal.Parse(usd);

                this
                    ._unitOfWork
                    .ExchangeRateRepository
                    .Update(exchangeRate);
            }

            //this
            //    ._unitOfWork
            //    .ExchangeRateRepository
            //    .Create(new Domain.Entities.ExchangeRate
            //    {
            //        CreatedDate = DateTime.Now,
            //        CurrencyId = "EUR",
            //        Rate = decimal.Parse(eur)
            //    });
        }
        #endregion
    }
}
