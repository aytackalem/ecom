﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ECommerce.Sitemap.Job
{
    class Url
    {
        public string Loc { get; set; }

        public DateTime LastMod { get; set; }

        public double Priority { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            File.WriteAllText(@"C:\sitemap\sitemap.xml", @"<?xml version=""1.0"" encoding=""UTF-8""?>
<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">
    <sitemap>
        <loc>http://www.example.com/sitemap0.xml</loc>
    </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap1.xml</loc>
    </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap2.xml</loc>
    </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap3.xml</loc>
    </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap4.xml</loc>
      </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap5.xml</loc>
      </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap6.xml</loc>
      </sitemap>
    <sitemap>
        <loc>http://www.example.com/sitemap7.xml</loc>
    </sitemap>
</sitemapindex>");

            string connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

            List<Url> urls = new();

            urls.Add(new Url { Loc = "https://www.fidanistanbul.com", LastMod = new DateTime(2021, 01, 14), Priority = 1 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/fidan-bul", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/contact", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/faq", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/about", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/registercontract", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/reset", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/kvkk", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/salesassociate", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/error", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/home/paymentinfo", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });
            urls.Add(new Url { Loc = "https://www.fidanistanbul.com/account/register", LastMod = new DateTime(2021, 01, 14), Priority = 0.80 });

            Write(@"C:\sitemap\sitemap0.xml", urls);

            urls = new();

            /* Products */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                var query = "Select [Url], [CreatedDate] From ProductInformationTranslations With(NoLock)";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            urls.Add(new Url
                            {
                                Loc = sqlDataReader.GetString(0),
                                LastMod = sqlDataReader.GetDateTime(1),
                                Priority = 0.64
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            Write(@"C:\sitemap\sitemap1.xml", urls);

            urls = new();

            /* Categories */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                var query = @"
Select		CT.[Url], CT.[CreatedDate], Count(0)
From		Categories As C
Left Join	CategoryProducts AS CP
On			C.Id = CP.CategoryId
			And CP.Active = 1
Join		CategoryTranslations As CT
On			C.Id = CT.CategoryId
Where		C.Active = 1
Group By	CT.[Url], CT.[CreatedDate]
Order By	Count(0) Desc";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            var loc = sqlDataReader.GetString(0);
                            var lastMod = sqlDataReader.GetDateTime(1);

                            urls.Add(new Url
                            {
                                Loc = loc,
                                LastMod = lastMod,
                                Priority = 0.80
                            });

                            var recordsCount = sqlDataReader.GetInt32(2);
                            var pagesCount = Math.Ceiling(recordsCount / 20M);
                            for (int i = 0; i < pagesCount; i++)
                            {
                                if (i == 0) continue;

                                urls.Add(new Url
                                {
                                    Loc = loc + $"?p={i}&amp;pc=20&amp;st=0",
                                    LastMod = lastMod,
                                    Priority = 0.80
                                });
                            }
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            Write(@"C:\sitemap\sitemap2.xml", urls);

            urls = new();

            /* Groups */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                var query = @"
Select		GT.[Url], GT.[CreatedDate], Count(0)
From		GroupTranslations AS GT With(NoLock)
Left Join	Products As P With(NoLock)
On			GT.GroupId = P.GroupId
Group By	GT.[Url], GT.[CreatedDate]
Order By	Count(0) Desc";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            var loc = sqlDataReader.GetString(0);
                            var lastMod = sqlDataReader.GetDateTime(1);

                            urls.Add(new Url
                            {
                                Loc = loc,
                                LastMod = lastMod,
                                Priority = 0.80
                            });

                            var recordsCount = sqlDataReader.GetInt32(2);
                            var pagesCount = Math.Ceiling(recordsCount / 20M);
                            for (int i = 0; i < pagesCount; i++)
                            {
                                if (i == 0) continue;

                                urls.Add(new Url
                                {
                                    Loc = loc + $"?p={i}&amp;pc=20&amp;st=0",
                                    LastMod = lastMod,
                                    Priority = 0.80
                                });
                            }
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            Write(@"C:\sitemap\sitemap3.xml", urls);

            urls = new();

            /* Informations */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                var query = "Select [Url], [CreatedDate] From InformationTranslations With(NoLock)";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            urls.Add(new Url
                            {
                                Loc = sqlDataReader.GetString(0),
                                LastMod = sqlDataReader.GetDateTime(1),
                                Priority = 0.80
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            Write(@"C:\sitemap\sitemap4.xml", urls);

            urls = new();

            /* Contents */
            using (SqlConnection sqlConnection = new(connectionString))
            {
                var query = "Select [Url], [CreatedDate] From ContentTranslations With(NoLock)";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            urls.Add(new Url
                            {
                                Loc = sqlDataReader.GetString(0),
                                LastMod = sqlDataReader.GetDateTime(1),
                                Priority = 0.80
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            Write(@"C:\sitemap\sitemap5.xml", urls);

            /* Geolocations */
            var keywords = new string[] { "-fidanlik-", "-fidancilik-", "-gul-fidani-" };

            int sitemapNo = 5;
            foreach (var kLoop in keywords)
            {
                urls = new();

                using (SqlConnection sqlConnection = new(connectionString))
                {
                    var query = @"
Select	Url + @Keyword + Cast(Id As NVarChar(Max)) + '-gcc'
From	OldCities

Union

Select	C.Url + '-' + D.Url + @Keyword + Cast(C.Id As NVarChar(Max)) + '-' + Cast(D.Id As NVarChar(Max)) + '-gcd'
From	OldCities As C
Join	OldDistricts As D
On		C.Id = D.OldCityId

Union

Select	C.Url + '-' + D.Url + '-' + N.Url + @Keyword + Cast(C.Id As NVarChar(Max)) + '-' + Cast(D.Id As NVarChar(Max)) + '-' + Cast(N.Id As NVarChar(Max)) + '-gcn'
From	OldCities As C
Join	OldDistricts As D
On		C.Id = D.OldCityId
Join	OldNeighborhoods As N
On		D.Id = N.OldDistrictId";

                    try
                    {
                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlCommand.Parameters.AddWithValue("@Keyword", kLoop);

                            sqlConnection.Open();

                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                urls.Add(new Url
                                {
                                    Loc = sqlDataReader.GetString(0),
                                    LastMod = new DateTime(2022, 2, 17),
                                    Priority = 0.80
                                });
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

                Write($@"C:\sitemap\sitemap{sitemapNo}.xml", urls);

                sitemapNo++;
            }
        }

        static void Write(string path, List<Url> urls)
        {
            var content = @$"<?xml version=""1.0"" encoding=""UTF-8""?>
<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">
  {string.Join("", urls.Select(u => $"<url><loc>https://www.fidanistanbul.com/{u.Loc}</loc><lastmod>{u.LastMod.ToString("yyyy-MM-dd")}</lastmod><priority>{u.Priority}</priority></url>").ToList())}
</urlset>";

            File.WriteAllText(path, content);
        }
    }
}
