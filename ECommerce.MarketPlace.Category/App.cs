﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using ECommerce.MarketPlace.Category.Base;
using ECommerce.MarketPlace.Common.Facade;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.MarketPlace.Category
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICategoryFacade _categoryFacade;


        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ICategoryFacade categoryFacade)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._categoryFacade = categoryFacade;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var marketPlaces =
                _unitOfWork
                .MarketplaceCompanyRepository
                .DbSet()
                .Include(x => x.MarketplaceConfigurations)
                .Where(x => x.MarketplaceId == "CS")
                .ToList();




            foreach (var mpLoop in marketPlaces)
            {

                System.Console.WriteLine($"{mpLoop.Id} Başladı");
                var configurations = mpLoop.MarketplaceConfigurations.ToDictionary(mc => mc.Key, mc => mc.Value);
                configurations.Add("MarketPlaceId", mpLoop.MarketplaceId);

                var response = _categoryFacade.Get(configurations);
                if (response.Success)
                {

                    var marketplaceCategories =
                        response.Data.Select(mpcLoop => new Domain.Entities.MarketplaceCategory
                        {
                            Id = $"{mpLoop.MarketplaceId}-{mpcLoop.Code}",
                            MarketplaceId = mpLoop.MarketplaceId,
                            Name = mpcLoop.Name,
                            ParentId = mpcLoop.ParentCategoryCode
                        }).ToList();

                    _unitOfWork.MarketplaceCategoryRepository.BulkInsert(marketplaceCategories);
                    System.Console.WriteLine($"{mpLoop.Id} marketplaceCategories Added");


                    var marketplaceVarians = response.Data
                        .SelectMany(x => x.CategoryAttributes)
                        .OrderByDescending(x => x.Mandatory)
                        .GroupBy(x => x.Code).Select(x => x.First())
                        .Where(x => x.Code != null)
                        .Select(caLoop => new Domain.Entities.MarketplaceVariant
                        {
                            Id = $"{mpLoop.MarketplaceId}-{caLoop.Code}",
                            Name = caLoop.Name,
                            MarketplaceId = mpLoop.MarketplaceId,
                            AllowCustom = caLoop.AllowCustom,
                            Mandatory = caLoop.Mandatory,
                            MultiValue = caLoop.MultiValue
                        }).ToList();

                    _unitOfWork.MarketplaceVariantRepository.BulkInsert(marketplaceVarians);

                    System.Console.WriteLine($"{mpLoop.Id} marketplaceVarians Added");


                    try
                    {
                        var marketplaceVariantValues = response.Data.SelectMany(x => x.CategoryAttributes).SelectMany(x => x.CategoryAttributesValues)
                                 .GroupBy(x => new { x.Code }).Select(x => x.First())
                                  .Where(x => x.Code != null)
                                    .Select(caLoop => new Domain.Entities.MarketplaceVariantValue
                                    {
                                        Id = $"{mpLoop.MarketplaceId}-{caLoop.Code}",
                                        Value = caLoop.Name,
                                        MarketplaceId = mpLoop.MarketplaceId
                                    }).ToList();

                        marketplaceVariantValues = marketplaceVariantValues.GroupBy(x => x.Id).Select(x => x.First()).ToList();

                        _unitOfWork.MarketplaceVariantValueRepository.BulkInsert(marketplaceVariantValues);
                    }
                    catch (Exception ex)
                    {


                    }

                    System.Console.WriteLine($"{mpLoop.Id} marketplaceVariantValues Added");




                    var marketplaceCategoriesMarketplaceVariants = response.Data
                          .SelectMany(x => x.CategoryAttributes)
                           .Where(x => x.Code != null)
                          .Select(x => new Domain.Entities.MarketplaceCategoriesMarketplaceVariant
                          {
                              MarketplaceCategoryId = $"{mpLoop.MarketplaceId}-{x.CategoryCode}",
                              MarketplaceVariantId = $"{mpLoop.MarketplaceId}-{x.Code}",
                              AllowCustom = x.AllowCustom,
                              Mandatory = x.Mandatory,
                              MultiValue = x.MultiValue
                          }).ToList();

                    _unitOfWork.MarketplaceCategoriesMarketplaceVariantRepository.BulkInsert(marketplaceCategoriesMarketplaceVariants);
                    System.Console.WriteLine($"{mpLoop.Id} marketplaceCategoriesMarketplaceVariants Added");



                    var marketplaceVariantsMarketplaceVariantValues = response.Data
                       .SelectMany(x => x.CategoryAttributes)
                       .SelectMany(x => x.CategoryAttributesValues)
                       .GroupBy(x => new { x.VarinatCode, x.Code })
                        .Where(x => x.Key.Code != null && x.Key.VarinatCode != null)
                       .Select(x => new Domain.Entities.MarketplaceVariantsMarketplaceVariantValue
                       {
                           MarketplaceVariantId = $"{mpLoop.MarketplaceId}-{x.Key.VarinatCode}",
                           MarketplaceVariantValueId = $"{mpLoop.MarketplaceId}-{x.Key.Code}"
                       }).ToList();

                    _unitOfWork.MarketplaceVariantsMarketplaceVariantValueRepository.BulkInsert(marketplaceVariantsMarketplaceVariantValues);
                    System.Console.WriteLine($"{mpLoop.Id} marketplaceVariantsMarketplaceVariantValues Added");


                    var _marketplaceCategoriesMarketplaceVariants = _unitOfWork.MarketplaceCategoriesMarketplaceVariantRepository.DbSet().Where(x => x.MarketplaceCategory.MarketplaceId == mpLoop.MarketplaceId).ToList();


                    var _marketplaceCategoriesMarketplaceVariantValues = new List<MarketplaceCategoriesMarketplaceVariantValue>();
                    foreach (var mcvLoop in response.Data)
                    {
                        foreach (var caLoop in mcvLoop.CategoryAttributes)
                        {


                            var marketplaceCategoriesMarketplaceVariantId = _marketplaceCategoriesMarketplaceVariants.FirstOrDefault(mcv =>
                             mcv.MarketplaceCategoryId == $"{mpLoop.MarketplaceId}-{caLoop.CategoryCode}" && mcv.MarketplaceVariantId == $"{mpLoop.MarketplaceId}-{caLoop.Code}")?.Id;

                            foreach (var attLoop in caLoop.CategoryAttributesValues)
                            {
                                _marketplaceCategoriesMarketplaceVariantValues.Add(new MarketplaceCategoriesMarketplaceVariantValue
                                {
                                    MarketplaceCategoriesMarketplaceVariantId = marketplaceCategoriesMarketplaceVariantId.Value,
                                    MarketplaceVariantValueId = $"{mpLoop.MarketplaceId}-{attLoop.Code}"
                                });

                            }


                        }

                    }


                    _unitOfWork.MarketplaceCategoriesMarketplaceVariantValueRepository.BulkInsert(_marketplaceCategoriesMarketplaceVariantValues);
                    System.Console.WriteLine($"{mpLoop.Id} MarketplaceCategoriesMarketplaceVariantValues Added");

                }
            }
        }
        #endregion
    }
}