﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Interfaces.Utilities;
using ECommerce.Persistence.Client.Caching;
using ECommerce.Persistence.Client.Helpers;
using ECommerce.Persistence.Client.Services;
using ECommerce.Persistence.Client.Utilities;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace ECommerce.Persistence.Client
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            #region Tenant Dependencies
            serviceCollection.AddSingleton<ITenantFinder, AppConfigTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, AppConfigDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, AppConfigCompanyFinder>();

            serviceCollection.AddSingleton<IDbNameFinder, AppConfigDbNameFinder>();
            #endregion

            #region Config And Setting Dependencies
            serviceCollection.AddTransient<ISettingService, SettingService>();
            #endregion

            #region DbContext Dependencies
            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });
            #endregion

            serviceCollection.AddMemoryCache();

            serviceCollection.AddLocalization(localizationOption =>
            {
                localizationOption.ResourcesPath = "Resources";
            });

            serviceCollection.Configure<RequestLocalizationOptions>(configureOption =>
            {
                var supportedCultures = new[] {
                    new CultureInfo("TR") 
                };

                configureOption.DefaultRequestCulture = new RequestCulture("TR", "TR");
                configureOption.SupportedCultures = supportedCultures;
                configureOption.SupportedUICultures = supportedCultures;
            });

            serviceCollection.AddTransient<IPosService, PosService>();

            serviceCollection.AddSingleton<ICacheHandler, CacheHandler>();

            serviceCollection.AddSingleton<IRedirectService, RedirectService>();

            serviceCollection.AddTransient<IFileCaching, FileCaching>();

            serviceCollection.AddTransient<ICaptchaService, CaptchaService>();

            serviceCollection.AddTransient<IImageHelper, ImageHelper>();

            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddTransient<ILocalizationHelper,ECommerce.Persistence.Client.Helpers.LocalizationHelper>();

            serviceCollection.AddTransient<ICookieHelper, CookieHelper>();

            serviceCollection.AddTransient<IAuthUtility<CustomerUser>, AuthUtility>();

            serviceCollection.AddTransient<IShoppingCartService, ShoppingCartService>();

            serviceCollection.AddTransient<ICustomerUserService, CustomerUserService>();

            serviceCollection.AddTransient<ICategoryService, CategoryService>();

            serviceCollection.AddTransient<IBrandService, BrandService>();

            serviceCollection.AddTransient<IProductService, ProductService>();

            serviceCollection.AddTransient<ICompanyService, CompanyService>();

            serviceCollection.AddTransient<IShowcaseService, ShowcaseService>();

            serviceCollection.AddTransient<IFrequentlyAskedQuestionService, FrequentlyAskedQuestionService>();

            serviceCollection.AddTransient<IOrderService, OrderService>();

            serviceCollection.AddTransient<ICityService, CityService>();

            serviceCollection.AddTransient<IPaymentTypeService, PaymentTypeService>();

            serviceCollection.AddTransient<INeighborhoodService, NeighborhoodService>();

            serviceCollection.AddTransient<INewBulletinService, NewBulletinService>();

            serviceCollection.AddTransient<IContactFormService, ContactFormService>();

            serviceCollection.AddTransient<IDistrictService, DistrictService>();

            serviceCollection.AddTransient<IInformationService, InformationService>();

            serviceCollection.AddTransient<IContentService, ContentService>();

            serviceCollection.AddTransient<ISearchService, SearchService>();

            serviceCollection.AddTransient<IDiscountCouponService, DiscountCouponService>();

            serviceCollection.AddTransient<IGetXPayYSettingService, GetXPayYSettingService>();

            serviceCollection.AddTransient<IShoppingCartDiscountedService, ShoppingCartDiscountedService>();

            serviceCollection.AddTransient<IShoppingCartDiscountedProductService, ShoppingCartDiscountedProductService>();

            serviceCollection.AddTransient<IProductInformationCommentService, ProductInformationCommentService>();

            serviceCollection.AddTransient<IConfigrationService, ConfigrationService>();

            serviceCollection.AddTransient<IPaymentService, PaymentService>();

            serviceCollection.AddTransient<IPropertyService, PropertyService>();

            serviceCollection.AddTransient<IProductInformationSubscriptionService, ProductInformationSubscriptionService>();

            serviceCollection.AddTransient<IEmailTemplateService, EmailTemplateService>();

            serviceCollection.AddTransient<ICustomerAddressService, CustomerAddressService>();

            serviceCollection.AddTransient<ICustomerInvoiceInformationService, CustomerInvoiceInformationService>();

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();

            serviceCollection.AddTransient<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddTransient<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddTransient<IProductInformationStockService, ProductInformationStockService>();

            serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<ISmsService, SmsProviderService>();

            serviceCollection.AddTransient<IEmailService, EmailService>();
        }
        #endregion
    }
}
