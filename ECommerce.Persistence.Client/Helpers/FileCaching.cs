﻿using ECommerce.Application.Client.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.IO;

namespace ECommerce.Persistence.Client.Helpers
{
    public class FileCaching : IFileCaching
    {
        #region Fields
        protected readonly IConfiguration _configuration;

        private readonly IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public FileCaching(IConfiguration configuration, IDomainFinder domainFinder)
        {
            #region Fields
            this._configuration = configuration;
            this._domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods
        public T Read<T>(string key)
        {
            var path = this.GetPath(key);

            return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
        }

        public void Write<T>(string key, T t)
        {
            var path = this.GetPath(key);

            File.WriteAllText(path, JsonConvert.SerializeObject(t));
        }

        public bool Contains(string key)
        {
            var path = this.GetPath(key);

            return File.Exists(path);
        }
        #endregion

        #region Helper Methods
        private string GetPath(string key)
        {
            var uuid = !string.IsNullOrEmpty(this._configuration.GetSection("UUId").Value) ? this._configuration.GetSection("UUId").Value : "";


            var path = @$"c:\caching\{uuid}-{this._domainFinder.FindId()}";

            if (key.EndsWith("b"))
                path += @"\b";
            else if (key.EndsWith("c"))
                path += @"\c";
            else if (key.EndsWith("g"))
                path += @"\g";
            else if (key.EndsWith("i"))
                path += @"\i";
            else if (key.EndsWith("p"))
                path += @"\p";
            else if (key.EndsWith("co"))
                path += @"\co";

            Directory.CreateDirectory(path);

            return path + @"\" + key;
        }
        #endregion
    }
}
