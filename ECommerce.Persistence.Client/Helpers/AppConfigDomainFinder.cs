﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Configuration;
using System;

namespace ECommerce.Persistence.Client.Helpers
{
    public class AppConfigDomainFinder : IDomainFinder
    {
        #region Fields
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public AppConfigDomainFinder(IConfiguration configuration)
        {
            #region Fields
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return Convert.ToInt32(this._configuration.GetSection("DomainId").Value);
        }

        public void Set(int id)
        {
          

        }
        #endregion
    }
}
