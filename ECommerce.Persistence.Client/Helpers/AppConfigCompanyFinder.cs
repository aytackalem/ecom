﻿using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Configuration;
using System;

namespace ECommerce.Persistence.Client.Helpers
{
    public class AppConfigCompanyFinder : ICompanyFinder
    {
        #region Fields
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public AppConfigCompanyFinder(IConfiguration configuration)
        {
            #region Fields
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return Convert.ToInt32(this._configuration.GetSection("CompanyId").Value);

        }

        public void Set(int id)
        {
        }
        #endregion
    }
}