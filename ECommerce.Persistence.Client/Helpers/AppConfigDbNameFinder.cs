﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace ECommerce.Persistence.Client.Helpers
{
    public class AppConfigDbNameFinder : IDbNameFinder
    {
    
        #region Fields
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public AppConfigDbNameFinder(IConfiguration configuration)
        {
            #region Fields
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public void Set(string dbName)
        {
        }

        public string FindName()
        {
            return this._configuration.GetSection("DbName").Value;
        }
        #endregion
    }
}
