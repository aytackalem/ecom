﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ProductService : IProductService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly Currency _currency;

        private readonly ICacheHandler _cacheHandler;

        protected readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public ProductService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ICacheHandler cacheHandler, IConfiguration configuration)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._currency = this._localizationHelper.GetCurrency();
            this._cacheHandler = cacheHandler;
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<ProductInformation> ReadInfo(int productInformationId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                    .Include(pi => pi.Product)
                    .Where(pi => pi.Product.Active && pi.Id == productInformationId && pi.Stock > 0)
                    .FirstOrDefault();

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }
                else
                {
                    var entityTranslation = entity
                        .ProductInformationTranslations
                        .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                    var entityPrice = entity
                        .ProductInformationPriceses
                        .FirstOrDefault(pip => pip.CurrencyId == this._currency.Id);

                    var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

                    var model = new ProductInformation
                    {
                        Id = entity.Id,
                        Barcode = entity.Barcode,
                        Stock = entity.Stock,
                        VirtualStock = entity.VirtualStock,
                        StockCode = entity.StockCode,
                        IsSale = entity.IsSale,
                        Payor = entity.Payor,
                        Name = entityTranslation.Name,
                        VariantValuesDescription = entityTranslation.VariantValuesDescription,
                        Url = entityTranslation.Url,
                        SubTitle = entityTranslation.SubTitle,
                        ProductInformationPrice = new ProductInformationPrice
                        {
                            ListUnitPrice = entityPrice.ListUnitPrice,
                            UnitPrice = entityPrice.UnitPrice,
                            VatRate = entityPrice.VatRate,
                            UnitCost = entityPrice.UnitCost
                        },
                        ProductInformationPhotos = entity
                                    .ProductInformationPhotos
                                    .Select(pip => new ProductInformationPhoto
                                    {
                                        FileName = $@"{productImageUrl}/{pip.FileName}"
                                    })
                                    .ToList(),
                        Product = new Product
                        {
                            Id = entity.Product.Id,
                            CategoryId = entity.Product.CategoryId,
                            BrandId = entity.Product.BrandId
                        }
                    };

                    model.ProductInformationPrice.UnitPrice = decimal.Round(model.ProductInformationPrice.UnitPrice, 2);

                    response.Data = model;
                    response.Success = true;
                    response.Message = "Ürün başarılı bir şekilde getirildi.";
                }
            }, (response, exception) => { });
            }, $"{string.Format(CacheKey.ProductinformationReadInfo, productInformationId)}", 10);
        }

        public DataResponse<ProductInformation> ReadVariants(int productInformationId)
        {

            return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {

                var productInformation = _unitOfWork
                                         .ProductInformationRepository
                                         .DbSet()
                                         .Include(x => x.Product)
                                         .Include(x => x.ProductInformationComments)
                                         .Include(x => x.ProductInformationPhotos.Where(x => !x.Deleted))
                                         .Include(x => x.ProductInformationTranslations)
                                         .ThenInclude(x => x.ProductInformationTranslationBreadcrumb)
                                         .ThenInclude(x => x.ProductInformationTranslation.ProductInformationContentTranslation)
                                         .Include(x => x.ProductInformationPriceses)
                                         .FirstOrDefault(x => x.Id == productInformationId && x.Stock > 0);

                if (productInformation == null)
                {
                    response.Success = false;
                    response.Message = "Ürün bulunamadı";
                    return;
                }



                var entityPrice = productInformation
                .ProductInformationPriceses
                .FirstOrDefault(pip => pip.CurrencyId == this._currency.Id);

                var entityTranslation = productInformation
                     .ProductInformationTranslations
                     .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

                var model = new ProductInformation
                {
                    Id = productInformation.Id,
                    Barcode = productInformation.Barcode,
                    Stock = productInformation.Stock,
                    VirtualStock = productInformation.VirtualStock,
                    IsSale = productInformation.IsSale,
                    Payor = productInformation.Payor,
                    StockCode = productInformation.StockCode,
                    Name = entityTranslation.Name,
                    VariantValuesDescription = entityTranslation.VariantValuesDescription,
                    Url = entityTranslation.Url,
                    SubTitle = entityTranslation.SubTitle,
                    ProductId = productInformation.ProductId,
                    ProductInformationPrice = new ProductInformationPrice
                    {
                        ListUnitPrice = entityPrice.ListUnitPrice,
                        UnitPrice = entityPrice.UnitPrice
                    },
                    ProductInformationPhotos = productInformation.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                    {
                        FileName = $@"{productImageUrl}/{pip.FileName}"
                    }).ToList(),
                    ProductInformationBreadcrumb = new ProductInformationBreadcrumb
                    {
                        Html = entityTranslation.ProductInformationTranslationBreadcrumb.Html
                    },
                    ProductInformationContent = new ProductInformationContent
                    {
                        Content = entityTranslation.ProductInformationContentTranslation != null ? entityTranslation.ProductInformationContentTranslation.Content : "",
                        Description = entityTranslation.ProductInformationContentTranslation != null ? entityTranslation.ProductInformationContentTranslation.Description : ""
                    },
                    ProductInformationSeo = new ProductInformationSeo
                    {
                        Title = entityTranslation.ProductInformationSeoTranslation?.Title,
                        MetaDescription = entityTranslation.ProductInformationSeoTranslation?.MetaDescription,
                        MetaKeywords = entityTranslation.ProductInformationSeoTranslation?.MetaKeywords
                    },
                    ProductInformationComments = productInformation.ProductInformationComments.Select(pip => new ProductInformationComment
                    {
                        FullName = pip.FullName,
                        Comment = pip.Comment,
                        Rating = pip.Rating,
                        CreatedDate = pip.CreatedDate
                    }).ToList(),
                };

                var variants = Variants(model);
                if (!variants.Success)
                {
                    response.Message = variants.Message;
                    response.Success = false;
                    return;
                }

                model.Variants = variants.Data;
                response.Data = model;
                response.Success = true;
                response.Message = "Ürün başarılı bir şekilde getirildi.";

            }, (response, exception) => { });


        }



        public DataResponse<ProductInformation> Read(int productInformationId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<ProductInformation>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(p => p.Product.Category.CategoryTranslations)
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                    .Include(pi => pi.ProductInformationTranslations)
                    .ThenInclude(pit => pit.ProductInformationSeoTranslation)
                    .Include(pi => pi.ProductInformationTranslations)
                    .ThenInclude(pit => pit.ProductInformationContentTranslation)
                    .Include(pi => pi.ProductInformationTranslations)
                    .ThenInclude(pi => pi.ProductInformationTranslationBreadcrumb)
                    .Include(pi => pi.ProductInformationComments.Where(x => x.Active && x.Approved).OrderByDescending(x => x.Id).Take(3))
                    .FirstOrDefault(p => p.Product.Active && p.Id == productInformationId && p.Active && p.Stock > 0);

                if (entity == null)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }
                else
                {
                    var productProperties = this
                          ._unitOfWork
                          .ProductPropertyRepository
                          .DbSet()
                          .Include(pp => pp.PropertyValue.PropertyValueTranslations)
                          .Include(pp => pp.PropertyValue.Property.ProductPropertyTranslations)
                          .Where(pp => pp.ProductId == entity.ProductId && pp.Active && !pp.PropertyValue.Property.Deleted)
                          .ToList();

                    var productLabels = this
                        ._unitOfWork
                        .ProductLabelRepository
                        .DbSet()
                        .Where(pl => pl.Active && !pl.Label.Deleted)
                        .Include(pl => pl.Label.LabelTranslations)
                        .Where(pl => pl.ProductId == entity.ProductId)
                        .OrderBy(x => x.Label.DisplayOrder)
                        .ToList();


                    var entityTranslation = entity.ProductInformationTranslations
                                                             .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                    var entityPrice = entity
                                         .ProductInformationPriceses
                                         .FirstOrDefault(pip => pip.CurrencyId == this._currency.Id);


                    var entityCategory = entity
                                         .Product
                                         .Category
                                         .CategoryTranslations
                                         .FirstOrDefault(pip => pip.LanguageId == this._language.Id);


                    var productinformationVariants = this
                                                  ._unitOfWork
                                                  .ProductInformationRepository
                                                  .DbSet()
                                                  .Include(x => x.ProductInformationTranslations)
                                                  .Where(pl => pl.ProductId == entity.ProductId && pl.Active)
                                                  .ToList();

                    var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

                    var model = new ProductInformation
                    {
                        Id = entity.Id,
                        Barcode = entity.Barcode,
                        Stock = entity.Stock,
                        VirtualStock = entity.VirtualStock,
                        StockCode = entity.StockCode,
                        IsSale = entity.IsSale,
                        Payor = entity.Payor,
                        Name = entityTranslation.Name,
                        VariantValuesDescription = entityTranslation.VariantValuesDescription,
                        Url = entityTranslation.Url,
                        SubTitle = entityTranslation.SubTitle,
                        ProductId = entity.ProductId,
                        ProductInformationPrice = new ProductInformationPrice
                        {
                            ListUnitPrice = entityPrice.ListUnitPrice,
                            UnitPrice = entityPrice.UnitPrice
                        },
                        ProductInformationSeo = new ProductInformationSeo
                        {
                            Title = entityTranslation.ProductInformationSeoTranslation.Title,
                            MetaDescription = entityTranslation.ProductInformationSeoTranslation.MetaDescription,
                            MetaKeywords = entityTranslation.ProductInformationSeoTranslation.MetaKeywords
                        },
                        ProductInformationContent = new ProductInformationContent
                        {
                            Description = entityTranslation.ProductInformationContentTranslation?.Description,
                            Content = entityTranslation.ProductInformationContentTranslation?.Content,
                            ExpertOpinion = entityTranslation.ProductInformationContentTranslation?.ExpertOpinion
                        },
                        ProductInformationBreadcrumb = new ProductInformationBreadcrumb
                        {
                            Html = entityTranslation.ProductInformationTranslationBreadcrumb.Html
                        },
                        ProductInformationComments = entity.ProductInformationComments.Select(pip => new ProductInformationComment
                        {
                            FullName = pip.FullName,
                            Comment = pip.Comment,
                            Rating = pip.Rating,
                            CreatedDate = pip.CreatedDate
                        }).ToList(),
                        ProductInformationPhotos = entity.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                        {
                            FileName = $@"{productImageUrl}/{pip.FileName}"

                        }).ToList(),
                        Product = new Product
                        {
                            ProductLabels = productLabels.Select(pl => new ProductLabel
                            {
                                Id = pl.Id,
                                Value = pl.Label.LabelTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Value

                            }).ToList(),
                            ProductProperties = productProperties.Where(x => x.Active).Select(x => new ProductProperty
                            {
                                PropertyValueId = x.PropertyValueId,
                                Property = x.PropertyValue.Property.ProductPropertyTranslations.FirstOrDefault().Name,
                                Value = x.PropertyValue.PropertyValueTranslations.FirstOrDefault(x => x.LanguageId == this._language.Id).Value
                            }).ToList(),
                            Id = entity.Id,
                            CategoryId = entity.Product.CategoryId,
                            BrandId = entity.Product.BrandId,
                            Categories = new List<Category>
                            {
                                new Category
                                {
                                Id = entityCategory.Id,
                                Name = entityCategory?.Name,
                                Url = entityCategory?.Url
                                }
                            },
                            ProductInformations = productinformationVariants.Select(x => new ProductInformation
                            {
                                Id = x.Id,
                                Url = x.ProductInformationTranslations
                                                             .FirstOrDefault(pit => pit.LanguageId == this._language.Id)?.Url,
                                VariantValuesDescription = x.ProductInformationTranslations
                                                             .FirstOrDefault(pit => pit.LanguageId == this._language.Id)?.VariantValuesDescription
                            }).ToList()
                        }
                    };

                    model.ProductInformationPrice.UnitPrice = decimal.Round(model.ProductInformationPrice.UnitPrice, 2);

                    var variants = Variants(model);
                    if (variants.Success)
                    {
                        model.Variants = variants.Data;
                    }












                    response.Data = model;
                    response.Success = true;
                    response.Message = "Ürün başarılı bir şekilde getirildi.";
                }
            }, (response, exception) => { });
        }

        public PagedResponse<ProductInformation> InfiniteReadByCategoryId(int categoryId, int page, int pageRecordsCount, int sorting)
        {
            return this.Read((queryable) =>
            {
                return queryable.Where(p => p.Product.CategoryProducts.Any(cp => cp.CategoryId == categoryId) || p.CategoryProducts.Any(cp => cp.CategoryId == categoryId));
            }, page, pageRecordsCount, sorting);
        }

        public PagedResponse<ProductInformation> InfiniteReadByCategoryId(int categoryId, int page, int pageRecordsCount, int sorting, List<int> propertyValueIds)
        {
            return this.Read((queryable) =>
            {
                return queryable.Where(p => p.Product.CategoryProducts.Any(cp => cp.CategoryId == categoryId));
            }, (queryable) =>
            {
                return queryable.Where(pp => pp.CategoryProducts.Any(cp => cp.CategoryId == categoryId));
            }, page, pageRecordsCount, sorting, propertyValueIds);
        }

        public PagedResponse<ProductInformation> InfiniteReadByKeyword(int page, int pageRecordsCount, int sorting, string keyword)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {
                var queryable = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(p => p.Product.Active && p.ProductInformationTranslations.Any(pt =>
                    EF.Functions.Collate(pt.Name, "Turkish_CI_AS").Contains(keyword)
                    ) && p.Photoable);

                response.RecordsCount = queryable.Count();
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;

                if (response.PageRecordsCount == 0)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                queryable = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(p => p.Product.ProductLabels.Where(x => x.Active))
                    .ThenInclude(x => x.Label.LabelTranslations.OrderBy(x => x.Label.DisplayOrder).Where(x => x.Label.Active && !x.Label.Deleted))
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                    .Where(p => p.ProductInformationPriceses.Any(y => y.ListUnitPrice > 0 && y.UnitPrice > 0) && p.Active && p.Product.Active && p.ProductInformationTranslations.Any(pt => EF.Functions.Collate(pt.Name, "Turkish_CI_AS").Contains(keyword)) && p.Photoable && p.Stock > 0);


                queryable = Sorting(queryable, sorting);

                var entities = queryable
                    .Skip(pageRecordsCount * page)
                    .Take(pageRecordsCount)
                    .ToList();

                var models = entities.Select(e => this.Map(e)).ToList();

                response.Data = models;
                response.Success = true;
                response.Message = "Ürünler başarılı bir şekilde getirildi.";

            }, (response, exception) => { });
        }

        public PagedResponse<ProductInformation> InfiniteReadByProperties(int page, int pageRecordsCount, int sorting, List<int> propertyValueIds)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {

                var queryable = this
                         ._unitOfWork
                         .ProductRepository
                         .DbSet()
                         .Include(x => x.ProductProperties)
                         .Where(p => p.ProductProperties.Count(ppp => propertyValueIds.Contains(ppp.PropertyValueId) && ppp.Active && !ppp.PropertyValue.Property.Deleted) == propertyValueIds.Count);


                response.RecordsCount = queryable
                                        .SelectMany(x => x.ProductInformations)
                                        .GroupBy(pp => pp.Id)
                                        .Select(pp => pp.Key)
                                        .Count();
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;

                if (response.PageRecordsCount == 0)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }


                var productInformations = queryable
                        .Where(p => p.Active)
                        .SelectMany(x => x.ProductInformations);




                var productInformationIds = productInformations
                        .GroupBy(x => x.Id)
                        .Select(x => x.Key)
                        .ToList();

                var queryable2 = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(p => p.Product.ProductLabels.Where(x => x.Active))
                    .ThenInclude(x => x.Label.LabelTranslations.Where(x => x.Label.Active && !x.Label.Deleted))
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                    .Where(p => p.ProductInformationPriceses.Any(y => y.ListUnitPrice > 0 && y.UnitPrice > 0) && p.ProductInformationPriceses.Any(y => y.ListUnitPrice > 0 && y.UnitPrice > 0) && productInformationIds.Contains(p.Id) && p.Stock > 0);

                queryable2 = Sorting(queryable2, sorting);



                var entities = queryable2
                              .Skip(pageRecordsCount * page)
                              .Take(pageRecordsCount)
                              .ToList();

                var models = entities.Select(e => this.Map(e)).ToList();


                response.Data = models;
                response.Success = true;
                response.Message = "Ürünler başarılı bir şekilde getirildi.";
            }, (response, exception) => { });
        }

        public PagedResponse<ProductInformation> InfiniteReadByBrandId(int brandId, int page, int pageRecordsCount, int sorting)
        {
            return this.Read((queryable) =>
            {
                return queryable.Where(p => p.Product.BrandId == brandId);
            }, page, pageRecordsCount, sorting);
        }

        public PagedResponse<ProductInformation> InfiniteReadByBrandId(int brandId, int page, int pageRecordsCount, int sorting, List<int> propertyValueIds)
        {
            return this.Read((queryable) =>
            {
                return queryable.Where(p => p.Product.BrandId == brandId);
            }, (queryable) =>
            {
                return queryable.Where(pp => pp.BrandId == brandId);
            }, page, pageRecordsCount, sorting, propertyValueIds);
        }

        public PagedResponse<ProductInformation> InfiniteReadByDiscount(int discount, int page, int pageRecordsCount, int sorting)
        {
            return this.Read((queryable) =>
            {
                return queryable.Where(p => Math.Round(100 - (p.ProductInformationPriceses.FirstOrDefault(y => y.CurrencyId == _currency.Id).UnitPrice * 100 / p.ProductInformationPriceses.FirstOrDefault(y => y.CurrencyId == _currency.Id).ListUnitPrice)) >= discount);
            }, page, pageRecordsCount, sorting);
        }
        #endregion

        #region Private Methods

        private DataResponse<List<Variant>> Variants(ProductInformation productInformation)
        {

            return ExceptionHandler.ResultHandle<DataResponse<List<Variant>>>((response) =>
            {

                var productinformationVariants = this
                                                ._unitOfWork
                                                .ProductInformationRepository
                                                .DbSet()
                                                .Include(x => x.ProductInformationVariants)
                                                .ThenInclude(x => x.VariantValue.VariantValueTranslations)
                                                 .Include(x => x.ProductInformationVariants)
                                                .ThenInclude(x => x.VariantValue.Variant.VariantTranslations)
                                                .Include(x => x.ProductInformationPhotos.Where(x => !x.Deleted))
                                                .Include(x => x.ProductInformationTranslations)
                                                .Where(pl => pl.ProductId == productInformation.ProductId && pl.Active)
                                                .ToList();

                var productinformationVariant = productinformationVariants.FirstOrDefault(x => x.Id == productInformation.Id);


                var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

                var groupVariants = productinformationVariants.SelectMany(x => x.ProductInformationVariants).GroupBy(x =>
                new
                {
                    x.VariantValue.VariantId,
                    x.VariantValue.Variant.VariantTranslations.FirstOrDefault(x => x.LanguageId == this._language.Id).Name
                }).Select(x => new Variant
                {
                    Id = x.Key.VariantId,
                    Name = x.Key.Name,
                    Photoable = x.Count() > 0 ? x.FirstOrDefault().VariantValue.Variant.Photoable : false,
                    ShowVariant = x.Count() > 0 ? x.FirstOrDefault().VariantValue.Variant.ShowVariant : false,
                    VariantValues = x.Select(y => new VariantValue
                    {
                        Id = y.VariantValueId,
                        Value = y.VariantValue.VariantValueTranslations[0].Value,
                        ImageUrl = y.ProductInformation.ProductInformationPhotos.FirstOrDefault() != null ? $@"{productImageUrl}/{y.ProductInformation.ProductInformationPhotos.FirstOrDefault().FileName}" : ""
                    }).ToList()
                }).ToList();

                var variants = new List<Variant>();

                foreach (var gvLoop in groupVariants)
                {
                    var variantCombine = new Variant
                    {
                        Id = gvLoop.Id,
                        Name = gvLoop.Name,
                        Photoable = gvLoop.Photoable,
                        VariantValues = new List<VariantValue>()
                    };

                    var groupVariantValues = gvLoop.VariantValues.GroupBy(x => x.Id).Select(x => x.First()).ToList();

                    if (groupVariantValues.Count == 1 && !gvLoop.ShowVariant) continue;


                    foreach (var gv in groupVariantValues)
                    {
                        variantCombine.VariantValues.Add(new VariantValue
                        {
                            Id = gv.Id,
                            Value = gv.Value,
                            ImageUrl = gv.ImageUrl,
                        });
                    }


                    variants.Add(variantCombine);
                }

                foreach (var vLoop in variants)
                {

                    var variantIds = variants.Where(x => x.Id != vLoop.Id).Select(x => x.Id);

                    var _productinformationVariants = productinformationVariant.ProductInformationVariants.Where(x => variantIds.Contains(x.VariantValue.VariantId)).Select(x => x.VariantValueId).ToList();

                    foreach (var vvLoop in vLoop.VariantValues)
                    {

                        var piVariants = productinformationVariants.Where(x => x.ProductInformationVariants.Any(y => y.VariantValueId == vvLoop.Id));

                        foreach (var variantValueId in _productinformationVariants)
                        {
                            piVariants = piVariants.Where(x => x.ProductInformationVariants.Any(y => y.VariantValueId == variantValueId));
                        }

                        var piVarinat = piVariants.FirstOrDefault();

                        if (piVarinat != null)
                        {
                            vvLoop.ProductInformationId = piVarinat.Id;
                            vvLoop.Selected = piVarinat.Id == productInformation.Id;
                            vvLoop.Sale = piVarinat.IsSale && (piVarinat.Stock + piVarinat.VirtualStock) > 0;
                            vvLoop.Active = true;
                            vvLoop.Url = piVarinat.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Url;
                        }
                        else if (vLoop.Photoable)
                        {
                            var firstPhotoTable = productinformationVariants.FirstOrDefault(x => x.ProductInformationVariants.Any(y => y.VariantValueId == vvLoop.Id));

                            vvLoop.ProductInformationId = firstPhotoTable.Id;
                            vvLoop.Selected = firstPhotoTable.Id == productInformation.Id;
                            vvLoop.Sale = firstPhotoTable.IsSale && (firstPhotoTable.Stock + firstPhotoTable.VirtualStock) > 0;
                            vvLoop.Active = true;
                            vvLoop.Url = firstPhotoTable.ProductInformationTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Url;
                        }

                    }
                }


                response.Data = variants.OrderBy(x => x.Id).ToList();
                response.Success = true;
                response.Message = "Ürün başarılı bir şekilde getirildi.";

            }, (response, exception) => { });


        }

        private PagedResponse<ProductInformation> Read(Func<IQueryable<Domain.Entities.ProductInformation>, IQueryable<Domain.Entities.ProductInformation>> func, int page, int pageRecordsCount, int sorting)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {
                var queryable = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Where(p => p.Product.Active && p.Photoable && p.Active && p.Stock > 0);

                response.RecordsCount = func(queryable).Count();
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;

                if (response.PageRecordsCount == 0)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                queryable = this
                        ._unitOfWork
                        .ProductInformationRepository
                        .DbSet()
                        .Include(p => p.Product.ProductLabels.Where(x => x.Active))
                        .ThenInclude(x => x.Label.LabelTranslations.Where(x => x.Label.Active && !x.Label.Deleted))
                        .Include(pi => pi.ProductInformationPriceses)
                        .Include(pi => pi.ProductInformationTranslations)
                        .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                        .Where(p => p.Product.Active && p.Photoable && p.Active && p.ProductInformationPriceses.Any(y => y.ListUnitPrice > 0 && y.UnitPrice > 0) && p.Stock > 0);


                queryable = Sorting(queryable, sorting);

                var entities = func(queryable)
                    .Skip(pageRecordsCount * page)
                    .Take(pageRecordsCount)
                    .OrderByDescending(x => x.Id)
                    .ToList();

                var models = entities.Select(e => this.Map(e)).ToList();

                response.Data = models;
                response.Success = true;
                response.Message = "Ürünler başarılı bir şekilde getirildi.";
            }, (response, exception) => { });
        }

        private PagedResponse<ProductInformation> Read(Func<IQueryable<Domain.Entities.ProductInformation>, IQueryable<Domain.Entities.ProductInformation>> func, Func<IQueryable<Domain.Entities.Product>, IQueryable<Domain.Entities.Product>> func2, int page, int pageRecordsCount, int sorting, List<int> propertyValueIds)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformation>>((response) =>
            {
                var queryable = this
                                ._unitOfWork
                                .ProductRepository
                                .DbSet()
                                .Include(x => x.ProductProperties)
                                .Where(p => p.ProductProperties.Count(ppp => propertyValueIds.Contains(ppp.PropertyValueId) && ppp.Active && !ppp.PropertyValue.Property.Deleted) == propertyValueIds.Count);

                queryable = func2(queryable);

                response.RecordsCount = queryable
                    .SelectMany(x => x.ProductInformations)
                    .GroupBy(pp => pp.Id)
                    .Select(pp => pp.Key)
                    .Count();
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;

                if (response.PageRecordsCount == 0)
                {
                    response.Message = $"Ürün bulunamadı.";
                    response.Success = false;
                    return;
                }

                queryable = func2(queryable);

                var productInformations = queryable
                        .Where(p => p.Active)
                        .SelectMany(x => x.ProductInformations);


                var productInformationIds = productInformations
                        .GroupBy(x => x.Id)
                        .Select(x => x.Key)
                        .ToList();

                var queryable2 = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(p => p.Product.ProductLabels.Where(x => x.Active))
                    .ThenInclude(x => x.Label.LabelTranslations.Where(x => x.Label.Active && !x.Label.Deleted))
                    .Include(pi => pi.ProductInformationPriceses)
                    .Include(pi => pi.ProductInformationTranslations)
                    .Include(pi => pi.ProductInformationPhotos.Where(x => !x.Deleted))
                    .Where(p => productInformationIds.Contains(p.Id) && p.Photoable && p.Active && p.Stock > 0);


                queryable2 = Sorting(queryable2, sorting);


                var entities = func(queryable2)
                               .Skip(pageRecordsCount * page)
                               .Take(pageRecordsCount)
                               .ToList();

                var models = entities.Select(e => this.Map(e)).ToList();

                response.Data = models;
                response.Success = true;
                response.Message = "Ürünler başarılı bir şekilde getirildi.";
            }, (response, exception) => { });
        }

        private IOrderedQueryable<Domain.Entities.ProductInformation> Sorting(IQueryable<Domain.Entities.ProductInformation> queryable, int sorting)
        {
            switch (sorting)
            {
                case 1:
                    return queryable.OrderBy(p => p.ProductInformationPriceses.FirstOrDefault(y => y.CurrencyId == this._currency.Id).UnitPrice);
                case 2:
                    return queryable.OrderByDescending(p => p.ProductInformationPriceses.FirstOrDefault(y => y.CurrencyId == this._currency.Id).UnitPrice);
                case 3:
                    return queryable.OrderBy(p => p.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == this._language.Id).Name);
                case 4:
                    return queryable.OrderByDescending(p => p.ProductInformationTranslations.FirstOrDefault(y => y.LanguageId == this._language.Id).Name);
                default:
                    return queryable.OrderBy(p => !p.IsSale);
            }
        }

        private ProductInformation Map(Domain.Entities.ProductInformation productInformation)
        {
            var entityPrice = productInformation
                  .ProductInformationPriceses
                  .FirstOrDefault(pip => pip.CurrencyId == this._currency.Id);

            var entityTranslation = productInformation
                 .ProductInformationTranslations
                 .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

            var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

            var model = new ProductInformation
            {
                Id = productInformation.Id,
                Barcode = productInformation.Barcode,
                Stock = productInformation.Stock,
                VirtualStock = productInformation.VirtualStock,
                IsSale = productInformation.IsSale,
                Payor = productInformation.Payor,
                StockCode = productInformation.StockCode,
                Name = entityTranslation.Name,
                VariantValuesDescription = entityTranslation.VariantValuesDescription,
                Url = entityTranslation.Url,
                SubTitle = entityTranslation.SubTitle,
                ProductInformationPrice = new ProductInformationPrice
                {
                    ListUnitPrice = entityPrice.ListUnitPrice,
                    UnitPrice = entityPrice.UnitPrice
                },
                ProductInformationPhotos = productInformation.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                {
                    FileName = $@"{productImageUrl}/{pip.FileName}"
                }).ToList(),
                ProductInformationContent = new ProductInformationContent
                {
                    Content = entityTranslation.ProductInformationContentTranslation != null ? entityTranslation.ProductInformationContentTranslation.Content : "",
                    Description = entityTranslation.ProductInformationContentTranslation != null ? entityTranslation.ProductInformationContentTranslation.Description : ""
                },
                Product = new Product
                {
                    Id = productInformation.Product.Id,
                    BrandId = productInformation.Product.BrandId,
                    CategoryId = productInformation.Product.CategoryId,
                    ProductLabels = productInformation.Product?.ProductLabels.OrderBy(x => x.Label.DisplayOrder).Select(pl => new ProductLabel
                    {
                        Id = pl.Id,
                        Value = pl.Label.LabelTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Value

                    }).ToList()

                }


            };

            return model;
        }
        #endregion
    }
}
