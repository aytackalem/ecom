﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CustomerAddressService : ICustomerAddressService
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CustomerAddressService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CustomerAddress>> Read(int customerUserId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CustomerAddress>>>((response) =>
            {
                var CustomerAddresses = this
                    ._unitOfWork
                    .CustomerAddressRepository
                    .DbSet()
                    .Where(c => c.CustomerId == customerUserId && !c.Deleted)
                    .OrderBy(x => x.Id);


                response.Data = CustomerAddresses.Select(cad => new CustomerAddress
                {
                    Id = cad.Id,
                    Address = cad.Address,
                    NeighborhoodId = cad.NeighborhoodId,
                    Title = cad.Title,
                    Phone = cad.Phone,
                    Default = cad.Default
                }).ToList();

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public DataResponse<CustomerAddress> Read(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerAddress>>((response) =>
            {
                var customerAddress = this
                    ._unitOfWork
                    .CustomerAddressRepository
                    .DbSet()
                    .Include(x => x.Neighborhood.District.City)
                    .FirstOrDefault(c => c.CustomerId == customerUserId && c.Id == id && !c.Deleted);

                if (customerAddress == null)
                {

                    response.Message = "Size ait böyle bir adres bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }


                response.Data = new CustomerAddress
                {
                    Id = customerAddress.Id,
                    Address = customerAddress.Address,
                    NeighborhoodId = customerAddress.NeighborhoodId,
                    Title = customerAddress.Title,
                    Phone = customerAddress.Phone,
                    Recipient = customerAddress.Recipient,
                    Default = customerAddress.Default,
                    Neighborhood = new Neighborhood
                    {
                        Id = customerAddress.Neighborhood.Id,
                        District = new District
                        {
                            Id = customerAddress.Neighborhood.DistrictId,
                            City = new City
                            {
                                Id = customerAddress.Neighborhood.District.CityId,
                                Country = new Country
                                {
                                    Id = customerAddress.Neighborhood.District.City.CountryId
                                }
                            }
                        }
                    }
                };

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Delete(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var customerAddress = this
                    ._unitOfWork
                    .CustomerAddressRepository
                    .DbSet()
                    .FirstOrDefault(c => c.CustomerId == customerUserId && c.Id == id && !c.Deleted);

                if (customerAddress == null)
                {

                    response.Message = "Size ait böyle bir adres bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }


                customerAddress.Deleted = true;
                customerAddress.DeletedDateTime = DateTime.Now;


                var updated = _unitOfWork
                     .CustomerAddressRepository.Update(customerAddress);
                if (!updated)
                {
                    response.Message = "Adres silinemedi lütfen daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }

                response.Message = "Adresiniz silinmiştir.";
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Default(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var customerAddress = this
                    ._unitOfWork
                    .CustomerAddressRepository
                    .DbSet()
                    .Where(c => c.CustomerId == customerUserId && !c.Deleted).ToList();

                if (!customerAddress.Any(x => x.Id == id))
                {

                    response.Message = "Size ait böyle bir adres bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }

                customerAddress.ForEach(c =>
                {
                    c.Default = false;
                    if (c.Id == id)
                    {
                        c.Default = true;
                    }
                });

                var updated = _unitOfWork
                     .CustomerAddressRepository.Update(customerAddress);

                if (!updated)
                {
                    response.Message = "Adres seçilemedi daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }

                response.Message = "Adresiniz güncellenmiştir.";
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Upsert(CustomerAddress dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (dto.Id > 0)
                {
                    var customerAddress = this
                    ._unitOfWork
                    .CustomerAddressRepository
                    .DbSet()
                    .FirstOrDefault(c => c.CustomerId == dto.CustomerId && c.Id == dto.Id && !c.Deleted);

                    if (customerAddress == null)
                    {

                        response.Message = "Size ait böyle bir adres bulunamadı lütfen daha sonra tekrar deneyiniz.";
                        response.Success = false;
                        return;
                    }


                    customerAddress.NeighborhoodId = dto.NeighborhoodId;
                    customerAddress.Title = dto.Title.CleanParameter();
                    customerAddress.Phone = dto.Phone.ToNumber();
                    customerAddress.Recipient = dto.Recipient.CleanParameter();
                    customerAddress.Address = dto.Address.CleanParameter();

                    var updated = _unitOfWork
                         .CustomerAddressRepository.Update(customerAddress);
                    if (!updated)
                    {
                        response.Message = "Adres güncellenemedi lütfen daha sonra tekrar deneyin.";
                        response.Success = false;
                        return;
                    }
                }
                else
                {
                    var customerAddressformationAny = this
                            ._unitOfWork
                            .CustomerAddressRepository
                            .DbSet()
                            .Any(c => c.CustomerId == dto.CustomerId && !c.Deleted);

                    var created = _unitOfWork
                      .CustomerAddressRepository
                      .Create(new Domain.Entities.CustomerAddress
                      {
                          Address = dto.Address.CleanParameter(),
                          CreatedDate = DateTime.Now,
                          CustomerId = dto.CustomerId,
                          NeighborhoodId = dto.NeighborhoodId,
                          Phone = dto.Phone.ToNumber(),
                          Recipient = dto.Recipient.CleanParameter(),
                          Title = dto.Title.CleanParameter(),
                          Default = !customerAddressformationAny
                      });

                    if (!created)
                    {
                        response.Message = "Adres kaydedilemedi lütfen daha sonra tekrar deneyin.";
                        response.Success = false;
                        return;
                    }
                }

                response.Success = true;
                response.Message = "Adresiniz güncellendi.";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }
        #endregion
    }
}
