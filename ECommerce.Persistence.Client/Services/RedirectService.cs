﻿using ECommerce.Application.Client.Interfaces.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class RedirectService : IRedirectService
    {
        #region Fields
        private Dictionary<string, string> _urls = null;
        #endregion

        #region Constructors
        public RedirectService()
        {
            if (this._urls == null)
            {
                this._urls = new Dictionary<string, string>();

                string _301FilePath = @"C:\301.txt";
                if (File.Exists(_301FilePath))
                {

                    var value = File.ReadAllText(_301FilePath);
                    var keyValues = value.Split("\r\n");

                    foreach (var kvLoop in keyValues)
                    {
                        var keyValue = kvLoop.Split("\t");

                        this._urls.Add(keyValue[0].Replace("\"", ""), keyValue[1].Replace("\"", ""));
                    }

                }
            }
        }
        #endregion

        #region Methods
        public string Find(string path)
        {
            var redirectPath = $"/Home/Error?path={path}";

            if (this._urls.ContainsKey(path))
                redirectPath = this._urls[path];

            return redirectPath;
        }
        #endregion
    }
}
