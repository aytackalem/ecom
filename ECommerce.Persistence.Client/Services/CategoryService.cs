﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CategoryService : ICategoryService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        protected readonly IConfiguration _configuration;

        protected readonly ISettingService _settingService;

        #endregion

        #region Constructors
        public CategoryService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, IConfiguration configuration, ISettingService settingService)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._configuration = configuration;
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Category> ReadInfo(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Category>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Include(c => c.CategoryTranslations)
                    .FirstOrDefault(c => c.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kategori bulunamadı.";
                }
                else
                {
                    var entityTranslation = entity
                                            .CategoryTranslations
                                            .FirstOrDefault(ct => ct.LanguageId == this._language.Id);
                    if (entityTranslation == null)
                    {
                        response.Success = false;
                        response.Message = "Kategori bilgileri eksik.";
                    }
                    else
                    {
                        response.Data = new Category
                        {
                            Id = entity.Id,
                            Name = entityTranslation?.Name,
                            Url = entityTranslation?.Url
                        };
                        response.Success = true;
                        response.Message = "Kategori başarılı bir şekilde getirildi.";
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Kategori başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<Category> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Category>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CategoryRepository
                    .DbSet()
                    .Include(c => c.CategoryTranslations)
                    .ThenInclude(ct => ct.CategoryTranslationBreadcrumb)
                    .Include(c => c.CategoryTranslations)
                    .ThenInclude(ct => ct.CategoryContentTranslation)
                    .Include(c => c.CategoryTranslations)
                    .ThenInclude(ct => ct.CategorySeoTranslation)
                    .FirstOrDefault(c => c.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kategori bulunamadı.";
                }
                else
                {
                    var entityTranslation = entity
                                            .CategoryTranslations
                                            .FirstOrDefault(ct => ct.LanguageId == this._language.Id);
                    if (entityTranslation == null)
                    {
                        response.Success = false;
                        response.Message = "Kategori bilgileri eksik.";
                    }
                    else
                    {
                        response.Data = new Category
                        {
                            Id = entity.Id,
                            ParentCategoryId = entity.ParentCategoryId,
                            Name = entityTranslation?.Name,
                            Url = entityTranslation?.Url,
                            CategoryBreadcrumb = new CategoryBreadcrumb
                            {
                                Html = entityTranslation.CategoryTranslationBreadcrumb.Html
                            },
                            CategoryContent = new CategoryContent
                            {
                                Content = entityTranslation.CategoryContentTranslation.Content
                            },
                            CategorySeo = new CategorySeo
                            {
                                MetaDescription = entityTranslation.CategorySeoTranslation.MetaDescription,
                                MetaKeywords = entityTranslation.CategorySeoTranslation.MetaKeywords,
                                Title = entityTranslation.CategorySeoTranslation.Title
                            }
                        };
                        response.Success = true;
                        response.Message = "Kategori başarılı bir şekilde getirildi.";
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Kategori başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<List<Category>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<Category>>>((response) =>
                {
                    var entities = this
                        ._unitOfWork
                        .CategoryRepository
                        .DbSet()
                        .Where(x => x.Active)
                        .Include(c => c.CategoryTranslations)
                        .OrderBy(c => c.SortNumber)
                        .ToList();
                        

                    var models = new List<Category>();

                    var parentEntites = entities.Where(e => e.ParentCategoryId == null);

                    var categoryImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("CategoryImageUrl").Value) ? this._configuration.GetSection("CategoryImageUrl").Value : "";

                    foreach (var peLoop in parentEntites)
                    {
                        var entityTranslation = peLoop
                            .CategoryTranslations
                            .FirstOrDefault(ct => ct.LanguageId == this._language.Id);
                        var model = new Category
                        {
                            Id = peLoop.Id,
                            Name = entityTranslation.Name,
                            Url = entityTranslation.Url,
                            IconUrl = $@"{categoryImageUrl}/{peLoop.Id}/{peLoop.FileName}",
                            ChildCategories = new List<Category>()
                        };
                        this.BindChilds(entities, model);
                        models.Add(model);
                    }

                    response.Data = models;
                    response.Success = true;
                    response.Message = "Kategoriler başarılı bir şekilde getirildi.";
                }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Kategori başarılı bir şekilde getirilemedi.";
                });
            }, CacheKey.Categories);
        }

        public DataResponse<List<Property>> ReadCategoryProperties(int id)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                SqlConnection sqlConnection = null;

                return ExceptionHandler.ResultHandle<DataResponse<List<Property>>>((response) =>
                {
                    List<Property> models = new();

                    using (sqlConnection = new(this._settingService.ConnectionString))
                    {
                        var query = @"
Select		PVT.PropertyValueId
			,PVT.[Value]
			,PT.PropertyId
			,PT.[Name]

From		CategoryProducts AS CP With(NoLock)

Join		Products AS P With(NoLock)
On			CP.ProductId = P.Id
			And P.Active = 1

Join		ProductProperties AS PP With(NoLock)
On			PP.ProductId = P.Id
			And PP.Active = 1

Join		PropertyValues As PV (NoLock)
On			PP.PropertyValueId = PV.Id

Join		PropertyValueTranslations AS PVT
On			PVT.PropertyValueId = PP.PropertyValueId
			And PVT.LanguageId = @LanguageId

Join		Properties As P2 With(NoLock)
On			P2.Id = PV.PropertyId

Join		PropertyTranslations AS PT With(NoLock)
On			PT.PropertyId = P2.Id
			And PT.LanguageId = @LanguageId

Where		CP.CategoryId = @CategoryId And P2.Deleted=0

Group By	PVT.PropertyValueId
			,PVT.[Value]
			,PT.PropertyId
			,PT.[Name]";

                        using (SqlCommand sqlCommand = new(query, sqlConnection))
                        {
                            sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);
                            sqlCommand.Parameters.AddWithValue("@CategoryId", id);

                            sqlConnection.Open();

                            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                            while (sqlDataReader.Read())
                            {
                                var propertyValueId = sqlDataReader.GetInt32(0);
                                var value = sqlDataReader.GetString(1);
                                var propertyId = sqlDataReader.GetInt32(2);
                                var name = sqlDataReader.GetString(3);

                                if (!models.Any(d => d.Id == propertyId))
                                    models.Add(new Property
                                    {
                                        Id = propertyId,
                                        Name = name,
                                        PropertyValues = new List<PropertyValue>()
                                    });

                                models.Single(d => d.Id == propertyId).PropertyValues.Add(new PropertyValue
                                {
                                    Id = propertyValueId,
                                    Value = value
                                });
                            }
                        }
                    }

                    sqlConnection.Close();

                    response.Data = models;
                    response.Success = true;
                    response.Message = "Kategori özellikleri başarılı bir şekilde getirildi.";
                }, (response, exception) =>
                {
                    if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                        sqlConnection.Close();

                    response.Success = false;
                    response.Message = "Kategori özellikleri başarılı bir şekilde getirilemedi.";
                });
            }, $"{CacheKey.CategoryProperties}_{id}", 30);
        }

        /// <summary>
        /// Ürün Labellarinden ilk 7 tanesi ekrana basılır
        /// Şimdlik Kullanılmayacak
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<List<ProductLabel>> ReadCategoryLabels(int id)
        {
            return new DataResponse<List<ProductLabel>>();

            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<ProductLabel>>>((response) =>
            {
                List<ProductLabel> models = new();

                using (sqlConnection = new(this._settingService.ConnectionString))
                {
                    var query = @"Select		TOP 7
			TT.LabelId,
			TT.Value
From		CategoryProducts AS CP With(NoLock)

Join		Products AS P With(NoLock)
On			CP.ProductId = P.Id
			And P.Active = 1

Join		ProductLabels AS PT With(NoLock)
On			PT.ProductId = P.Id
			And PT.Active = 1
			

Join		Labels AS TG With(NoLock)
On			TG.Id = PT.LabelId
			And TG.Active = 1
			And TG.Deleted=0

Join		LabelTranslations AS TT With(NoLock)
On			TT.LabelId = TG.Id
			And TT.LanguageId = @LanguageId

Where		CP.CategoryId = @CategoryId
			

Group By	TT.LabelId,
			TT.Value
";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);
                        sqlCommand.Parameters.AddWithValue("@CategoryId", id);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            var id = sqlDataReader.GetInt32(0);
                            var value = sqlDataReader.GetString(1);
                            models.Add(new ProductLabel
                            {
                                Id = id,
                                Value = value
                            });
                        }
                    }
                }

                sqlConnection.Close();
                response.Data = models;
                response.Success = true;
                response.Message = "Kategori özellikleri başarılı bir şekilde getirildi.";
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

                response.Success = false;
                response.Message = "Kategori özellikleri başarılı bir şekilde getirilemedi.";
            });
        }
        #endregion

        #region Private Methods
        private void BindChilds(List<Domain.Entities.Category> entities, Category model)
        {
            var childEntities = entities.Where(e => e.ParentCategoryId == model.Id);
            foreach (var ceLoop in childEntities)
            {
                var entityTranslation = ceLoop
                        .CategoryTranslations
                        .FirstOrDefault(ct => ct.LanguageId == this._language.Id);
                var childModel = new Category
                {
                    Id = ceLoop.Id,
                    Name = entityTranslation.Name,
                    Url = entityTranslation.Url,
                    ChildCategories = new List<Category>()
                };
                this.BindChilds(entities, childModel);
                model.ChildCategories.Add(childModel);
            }
        }
        #endregion
    }
}
