﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ShoppingCartDiscountedProductService : IShoppingCartDiscountedProductService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly Currency _currency;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public ShoppingCartDiscountedProductService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ISettingService settingService)
        {
            _unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._currency = this._localizationHelper.GetCurrency();
            this._settingService = settingService;
        }
        #endregion

        #region Methods
        public DataResponse<List<ShoppingCartDiscountedProduct>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<ShoppingCartDiscountedProduct>>>((response) =>
            {

                var shoppingCartDiscountedProducts = this
                          ._unitOfWork
                          .ShoppingCartDiscountedProductRepository
                          .DbSet()
                          .Include(p => p.ShoppingCartDiscountedProductSetting)
                          .Include(p => p.Product)
                          .Include(p => p.Product.ProductLabels.Where(x => x.Active))
                           .ThenInclude(x => x.Label.LabelTranslations.OrderBy(x => x.Label.DisplayOrder).Where(x => x.Label.Active && !x.Label.Deleted))
                           .Include(p => p.Product)
                           .ThenInclude(p => p.ProductInformations)
                           .ThenInclude(pi => pi.ProductInformationPriceses)
                          .Include(p => p.Product)
                          .ThenInclude(p => p.ProductInformations)
                          .ThenInclude(pi => pi.ProductInformationTranslations)
                          .Include(p => p.Product)
                          .ThenInclude(p => p.ProductInformations)
                          .ThenInclude(pi => pi.ProductInformationPhotos)
                          .Include(p => p.Product)
                          .ThenInclude(p => p.ProductInformations)
                          .ThenInclude(p => p.ProductInformationVariants)
                          .ThenInclude(p => p.VariantValue.VariantValueTranslations)
                          .Where(p => p.Product.Active && !p.Deleted && p.Active && p.Product.ProductInformations.Any(y => y.ProductInformationPriceses.Any(pp => pp.ListUnitPrice > 0 && pp.UnitPrice > 0))).ToList();

                var models = shoppingCartDiscountedProducts.Select(e => this.Map(e)).ToList();
                response.Data = models;
                response.Success = true;
            }, (response, exception) =>
            {
            });
            }, $"{CacheKey.ShoppingCartDiscountedProducts}", 10);
        }
        #endregion


        private ShoppingCartDiscountedProduct Map(Domain.Entities.ShoppingCartDiscountedProduct shoppingCartDiscountedProduct)
        {


            var productInformation = shoppingCartDiscountedProduct.Product.ProductInformations[0];

            var entityPrice = productInformation
                   .ProductInformationPriceses
                   .FirstOrDefault(pip => pip.CurrencyId == this._currency.Id);

            var entityTranslation = productInformation
                 .ProductInformationTranslations
                 .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

            var model = new ShoppingCartDiscountedProduct
            {
                ProductInformation = new ProductInformation
                {
                    Id = productInformation.Id,
                    Barcode = productInformation.Barcode,
                    Stock = productInformation.Stock,
                    VirtualStock = productInformation.VirtualStock,
                    IsSale = productInformation.IsSale,
                    Payor = productInformation.Payor,
                    StockCode = productInformation.StockCode,
                    Name = $"{entityTranslation.Name} ({(productInformation.ProductInformationVariants.Count == 0 ? "" : $"({string.Join(", ", productInformation.ProductInformationVariants.Select(piv => piv.VariantValue.VariantValueTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Value))})")})",
                    Url = entityTranslation.Url,
                    SubTitle = entityTranslation.SubTitle,
                    ProductInformationPrice = new ProductInformationPrice
                    {
                        ListUnitPrice = entityPrice.ListUnitPrice,
                        UnitPrice = entityPrice.UnitPrice,
                        UnitCost = entityPrice.UnitCost,
                        VatRate = entityPrice.VatRate
                    },
                    ProductInformationPhotos = productInformation.ProductInformationPhotos.Select(pip => new ProductInformationPhoto
                    {
                        FileName = $@"{this._settingService.ProductImageUrl}/{pip.ProductInformationId}/{pip.FileName}"
                    }).ToList(),
                    Product = new Product
                    {
                        ProductLabels = shoppingCartDiscountedProduct.Product
                        .ProductLabels
                        .Select(pt =>
                        {
                            var entityTagTranslation = pt
                                           .Label.LabelTranslations
                                           .FirstOrDefault(pit => pit.LanguageId == this._language.Id);
                            return new ProductLabel
                            {
                                Value = entityTagTranslation.Value
                            };
                        }).ToList(),
                        Id = shoppingCartDiscountedProduct.Product.Id,
                        BrandId = shoppingCartDiscountedProduct.Product.BrandId,
                        CategoryId = shoppingCartDiscountedProduct.Product.CategoryId,
                    }
                }
            };

            if (shoppingCartDiscountedProduct.ShoppingCartDiscountedProductSetting.DiscountIsRate)
            {
                model.ProductInformation.ProductInformationPrice.UnitPrice -= model.ProductInformation.ProductInformationPrice.UnitPrice * shoppingCartDiscountedProduct.ShoppingCartDiscountedProductSetting.Discount;
            }
            else
            {
                model.ProductInformation.ProductInformationPrice.UnitPrice -= shoppingCartDiscountedProduct.ShoppingCartDiscountedProductSetting.Discount;
            }

            model.ProductInformation.ProductInformationPrice.UnitPrice = decimal.Round(model.ProductInformation.ProductInformationPrice.UnitPrice, 2);

            return model;
        }


    }
}
