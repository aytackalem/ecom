﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class PropertyService : IPropertyService
    {
        #region Fields
        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly ICacheHandler _cacheHandler;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;

        #endregion

        #region Constructors
        public PropertyService(ILocalizationHelper localizationHelper, ICacheHandler cacheHandler, IConfiguration configuration, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._cacheHandler = cacheHandler;
            this._configuration = configuration;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<Property>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<Property>>>((response) =>
                {
                    var properties = this.ReadProperties();
                    var propertyValues = this.ReadPropertyValues();

                    properties.ForEach(p => p.PropertyValues = propertyValues.Where(pv => pv.PropertyId == p.Id).ToList());

                    response.Data = properties;
                    response.Success = true;
                    response.Message = "Özellikler başarılı bir şekilde getirildi.";
                }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Özellikler başarılı bir şekilde getirilemedi.";
                });
            }, CacheKey.Properties, 30);
        }
        #endregion

        #region Helper Methods
        private List<Property> ReadProperties()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ObjectHandle<List<Property>>((obj) =>
            {
                using (sqlConnection =new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var query = @"
Select	P.Id
		,PT.[Name]
From	Properties As P
Join	PropertyTranslations As PT
On		P.Id = PT.PropertyId
		And PT.LanguageId = @LanguageId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            obj.Add(new Property
                            {
                                Id = sqlDataReader.GetInt32(0),
                                Name = sqlDataReader.GetString(1),
                                PropertyValues = new List<PropertyValue>()
                            });
                        }
                    }
                }

                sqlConnection.Close();

            }, (obj, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }

        private List<PropertyValue> ReadPropertyValues()
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ObjectHandle<List<PropertyValue>>((obj) =>
            {
                using (sqlConnection = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var query = @"
Select	PV.Id
		,PV.PropertyId
		,PVT.[Value]
From	PropertyValues As PV
Join	PropertyValueTranslations As PVT
On		PV.Id = PVT.PropertyValueId
		And PVT.LanguageId = @LanguageId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            obj.Add(new PropertyValue
                            {
                                Id = sqlDataReader.GetInt32(0),
                                PropertyId = sqlDataReader.GetInt32(1),
                                Value = sqlDataReader.GetString(2),
                            });
                        }
                    }
                }

                sqlConnection.Close();

            }, (obj, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }
        #endregion
    }
}
