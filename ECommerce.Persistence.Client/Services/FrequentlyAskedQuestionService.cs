﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class FrequentlyAskedQuestionService : IFrequentlyAskedQuestionService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly Language _language;

        private readonly ILocalizationHelper _localizationHelper;

        #endregion

        #region Constructors
        public FrequentlyAskedQuestionService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            #endregion
        }
        #endregion

        #region Methods



        public DataResponse<List<FrequentlyAskedQuestion>> Read()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<FrequentlyAskedQuestion>>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .FrequentlyAskedQuestionRepository
                    .DbSet()
                    .Include(dc => dc.FrequentlyAskedQuestionTranslations.Where(ft => ft.LanguageId == this._language.Id))
                    .Where(x => !x.Deleted);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sıkça sorulan sorular bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = entity.Select(x => new FrequentlyAskedQuestion
                    {
                        Header = x.FrequentlyAskedQuestionTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Header,
                        Description = x.FrequentlyAskedQuestionTranslations.FirstOrDefault(pit => pit.LanguageId == this._language.Id).Description
                    }).ToList();

                    response.Success = true;
                    response.Message = "Sıkça sorulan sorular başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Sıkça sorulan sorular başarılı bir şekilde getirilemedi.";
            });
        }
        #endregion
    }
}
