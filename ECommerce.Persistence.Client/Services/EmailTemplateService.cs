﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly Configuration _configuration;

        private readonly Language _language;

        #endregion

        #region Constructors
        public EmailTemplateService(IUnitOfWork unitOfWork, IConfigrationService configrationService, ILocalizationHelper localizationHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._configuration = configrationService.Read().Data;
            this._language = localizationHelper.GetLanguage();
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<EmailTemplate> ResetPasswordTemplate(int customerId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailTemplate>>((response) =>
            {
                var emailTemplate = new EmailTemplate();

                var customer = _unitOfWork
                   .CustomerRepository
                   .DbSet()
                   .Include(x => x.CustomerContact)
                   .Include(x => x.CustomerAddresses)
                   .Include(x => x.CustomerUser)
                   .FirstOrDefault(x => x.Id == customerId);


                var logo = $"{this._configuration.WebUrl}/themes/green/imgs/theme/logo.png";
                var forgotUrl = $"{this._configuration.WebUrl}/Account/ResetPassword?rpd={customer.CustomerUser.Token}";

                emailTemplate.Subject = "Şifre Yenileme";
                emailTemplate.Template = @$"<html>
    <head>
        <title>Şifre Yenileme</title>
    </head>
    <body>
        <table
            style=""width:600px !important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;margin:auto;padding:0;border:0;font-size:16px""
            cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
            <tbody>
                <tr>
                    <td valign=""top"" bgcolor=""#e5e5e5"" align=""center"" style=""vertical-align:top"">
                        <table width=""720"" cellspacing=""0"" cellpadding=""0"" border=""0"">
                            <tbody>
        
                                <tr>
                                    <td>
                                        <table style=""border-spacing:0;border-collapse:collapse;width:100%"" cellspacing=""0""
                                            cellpadding=""0"" border=""0"">
                                            <tbody>
                                                <tr>
                                                    <td align=""center"" bgcolor=""#f5f5f5"">
                                                        <a href=""{this._configuration.WebUrl}"" target=""_blank"">
                                                            <img width=""280"" src=""{logo}""
                                                                alt=""{_configuration.WebShortName.Replace(" ", "")}""
                                                                style=""outline:none;text-decoration:none;display:block""
                                                                border=""0"">
                                                        </a>
                                                    </td>
        
        
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
        
        
                                <tr height=""385"">
                                    <td bgcolor=""#ffffff"" style=""padding:2em 5%;border:1px solid #ecebec;"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        style=""font-size:25px;font-family:'AvenirNext',Arial,Helvetica,sans-serif;color:green;text-align:center"">
                                                       Şifre Yenileme</td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        style=""font-size:15px;color:#484848;text-align:center;padding:1.5em 0 0;font-family:Arial,Helvetica,sans-serif"">
                                                        Sayın {customer.Name.ToUpper()} {customer.Surname.ToUpper()}, şifre yenileme talebinizi aşağıdaki butona tıklayarak yapabilirsiniz.
                                                    </td>
                                                </tr>
        
                                                <tr>
                                                    <td style=""padding:.5em 0 0"">
                                                        <table align=""center"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                            <tbody>
                                                                <tr>
                                                                    <td width=""100%"" height=""56"" align=""center"">
                                                                        <div>
                                                                            <a href=""{forgotUrl}""
                                                                                style=""background-color:#f5f5f5;height:56px;font-family:monospace;text-align:center;line-height:56px;color:#484848;text-decoration:none;display:block;padding-left:32px;padding-right:32px;border-radius:28px;font-size:21px;white-space:nowrap""
                                                                                target=""_blank""
                                                                                data-saferedirecturl=""{forgotUrl}"">Güvenli Şifre Yenileme
                                                                              </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor=""#f5f5f5"" style=""padding:0 5%"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <hr noshade="""" color=""#E5E5E5""
                                                            style=""border:0;height:1px;font-size:0;line-height:0"">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor=""#f5f5f5"" style=""padding:0 5%;border-bottom:0.0625em solid #c4c4c4"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td align=""center""
                                                        style=""font-family:Arial,Helvetica,sans-serif;color:#484848;padding:2em 0 1em"">
                                                        <strong style=""font-size:0.8125em"">- BİZİ TAKİP EDİN -</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"">
                                                        <table cellspacing=""0"" cellpadding=""0"" border=""0"">
                                                            <tbody>
                                                                <tr>
             {(!string.IsNullOrEmpty(this._configuration.FacebookUrl) ? @$"<td style=""padding:0 0.5em"">
                                                             <a href=""{this._configuration.FacebookUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.FacebookUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/face.png""
                                                                                alt=""Facebook"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}

    {(!string.IsNullOrEmpty(this._configuration.İnstagramUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.İnstagramUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.İnstagramUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/insta.png""
                                                                                alt=""instagram"" width=""32"" height=""32"" border=""0"">
                                                                        </a>
                                                                    </td>" : "")}


    {(!string.IsNullOrEmpty(this._configuration.YoutubeUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.YoutubeUrl}""
                                                                            style=""text-decoration:none;display:block""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.YoutubeUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/youtube.png""
                                                                                alt=""Instagram"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}                                                                   
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"" style=""padding:2.5em 5% 3em"">
                                                        <table border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=""center"" style=""padding-bottom:0.5em"" valign=""middle"">
                                                                        <strong style=""font-family:Arial,Helvetica,sans-serif;font-size:0.6875em;color:#484848"">{this._configuration.WebShortName}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
        
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>";
                response.Data = emailTemplate;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Email template getirilemedi.";
            });
        }

        public DataResponse<EmailTemplate> OrderTransferTemplate(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailTemplate>>((response) =>
            {
                var emailTemplate = new EmailTemplate();

                var order = _unitOfWork
                 .OrderRepository
                 .DbSet()
                 .Include(o => o.OrderDetails)
                 .ThenInclude(pi => pi.ProductInformation.ProductInformationTranslations)
                 .Include(o => o.Customer.CustomerContact)
                 .Include(od => od.OrderDeliveryAddress.Neighborhood.District.City.Country)
                 .Include(od => od.OrderInvoiceInformation.Neighborhood.District.City.Country)
                 .FirstOrDefault(x => x.Id == orderId);





                var logo = $"{this._configuration.WebUrl}/themes/green/imgs/theme/logo.png";

                var orderDetailHtml = "";
                foreach (var odLoop in order.OrderDetails)
                {
                    var entityTranslation = odLoop.ProductInformation
              .ProductInformationTranslations
              .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                    orderDetailHtml += @$"<tr>
                                            <td  align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {entityTranslation.Name} </td>
                                            <td  width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td  width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {odLoop.Quantity}</td>
                                        </tr>";
                }


                emailTemplate.Subject = "Sipariş Özeti";
                emailTemplate.Template = @$"<html>

<head></head>

<body>

    <tbody>
        <tr>
            <td style=""width:660px;max-width:660px;min-width:660px"" align=""center"">
                <!--Logo Banner-->
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td style=""border-bottom:1px dotted #cccccc"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <th align=""center"" valign=""middle""
                                                style=""font-weight:normal;vertical-align:middle"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""center"" style=""padding:7px 0"">
                                                                <a href=""{_configuration.WebUrl}"" target=""_blank""
                                                                    data-saferedirecturl=""logo link""><img
                                                                        src=""{logo}""
                                                                        width=""180"" alt="""" border=""0""
                                                                        style=""display:block""></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>

                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--Logo Banner-->
                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""7"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

                <!--Müşteri Bilgileri ve Teşekkürler-->

                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" bgcolor=""#ffffff"">
                    <tbody>
                        <tr>
                            <td align=""left"" valign=""middle"" width=""602"" height=""40""
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:#484848;padding:5px 0 5px 10px"">
                                <span>
                                    Merhaba {order.Customer.Name.ToUpper()} {order.Customer.Surname.ToUpper()}, {_configuration.WebShortName}'dan vermiş olduğunuz sipariş alındı.
                                </span>
                                <br>
                            </td>


                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" bgcolor=""#f3f3f4"">
                    <tbody>
                        <tr>
                            <td align=""left""
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#484848;padding:5px 10px"">
                                <span>Sipariş</span>
                                Numaranız: &nbsp; <font style=""font-weight:bold;color:darkgreen"">{order.Id}</font>
                            </td>
                        </tr>
                    </tbody>
                </table>





                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td align=""left""
                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:15px 10px"">
                                Banka ödemenizin ardından siparişiniz işleme alınacaktır. Banka bilgilerini aşağıda
                                bulabilirsiniz.
                                Siparişiniz kargoya verildiğinde ayrıca bilgi vereceğiz. </td>
                        </tr>
                    </tbody>
                </table>
                <!--Müşteri Bilgileri ve Teşekkürler-->

                <!--ürün adet fiyat-->
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <th width=""640"" align=""left"" valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Ürün </td>
                                            <td bgcolor=""#f3f3f4"" width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td bgcolor=""#f3f3f4"" width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Adet</td>
                                        </tr>
                                        <!--Ürün Bilgileri-->

                                        <!--Foreach ile çekilecek ürün bilgisi-->
                                        {orderDetailHtml}
                                        <!--Foreach ile çekilecek ürün bilgisi-->

                                        <!--Ürün Bilgileri-->
                                        <!--Sipariş Özetleri-->
                                        <tr>
                                            <td align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px;text-align: right"">
                                                Toplam:</td>
                                            <td width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {order.Total} {order.CurrencyId}</td>
                                        </tr>
                                        <!--Sipariş Özetleri-->
                                        <!--ürün adet fiyat-->

                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0""
                    class=""m_6122722835262201619content-wrapper"" align=""center"">
                    <tbody>
                        <tr>
                            <td
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:darkgreen;padding:0 10px 10px 10px"">
                                Teslimat Bilgileri </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0""
                    class=""m_6122722835262201619content-wrapper"" align=""center"">
                    <tbody>
                        <tr>
                            <th class=""m_6122722835262201619column"" align=""center"" valign=""top""
                                style=""font-weight:normal;vertical-align:top"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <th class=""m_6122722835262201619column"" width=""50%"" align=""left""
                                                valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                                Teslimat Adresi </td>
                                                        </tr>
                                                        <tr>
                                                            <td align=""left"" valign=""top""
                                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                                {order.OrderDeliveryAddress.Recipient} <br>{order.OrderDeliveryAddress.Address}<br>{order.OrderDeliveryAddress.Neighborhood.District.Name}<br>{order.OrderDeliveryAddress.Neighborhood.District.City.Name}<br>
                                      {order.OrderDeliveryAddress.Neighborhood.District.City.Country.Name}<br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th class=""m_6122722835262201619column"" width=""50%"" align=""center""
                                                valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                                Fatura Adresi </td>
                                                        </tr>
                                                        <tr>
                                                            <td align=""left"" valign=""top""
                                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                                {order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}<br>{order.OrderInvoiceInformation.Address}<br>{order.OrderInvoiceInformation.Neighborhood.District.Name}<br>{order.OrderInvoiceInformation.Neighborhood.District.City.Name}<br>{order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name}<br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>

                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px;border-top:1px dotted #ccc"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:darkgreen;padding:0 10px 10px 10px"">
                                Havale Bilgileri </td>
                        </tr>
                    </tbody>
                </table>

                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>

                            <th align=""left"" valign=""top"" style=""font-weight:normal;"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Alıcı Adı </td>
                                        </tr>
                                        <tr>
                                            <td align=""left"" valign=""top""
                                                style=""font-family:Arial;font-size:13px;line-height:30px;color:#484848;padding:10px"">
                                                {_configuration.Company}</td>
                                        </tr>
                                    </tbody>

                                </table>
                            </th>
                            <th align=""center"" valign=""top"" style=""font-weight:normal;"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding: 13px 10px 6px 10px;"">
                                                Banka </td>
                                        </tr>
                                 <tr>
                                <td align=""left"" valign=""top""
                                style=""font-family:Arial;font-size:13px;line-height:30px;color:#484848;padding:10px"">
                                {_configuration.IbanBank}</td>
                             </tr>
                                    </tbody>
                                </table>
                            </th>

                            <th align=""right"" valign=""top"" style=""font-weight:normal"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Iban
                                            </td>
                                        </tr>
                                          <tr>
                                <td align=""left"" valign=""top""
                                style=""font-family:Arial;font-size:13px;line-height:30px;color:#484848;padding:10px"">
                                {_configuration.Iban}</td>
                             </tr>
                                    </tbody>

                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>

                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">

                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""30"" style=""font-size:1px;line-height:1px;border-top:1px dotted #ccc"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td bgcolor=""#f5f5f5"" style=""padding:0 5%;border-bottom:0.0625em solid #c4c4c4"">
                                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td align=""center""
                                                        style=""font-family:Arial,Helvetica,sans-serif;color:#484848;padding:2em 0 1em"">
                                                        <strong style=""font-size:0.8125em"">- BİZİ TAKİP EDİN -</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"">
                                                        <table cellspacing=""0"" cellpadding=""0"" border=""0"">
                                                            <tbody>
                                                                <tr>
             {(!string.IsNullOrEmpty(this._configuration.FacebookUrl) ? @$"<td style=""padding:0 0.5em"">
                                                             <a href=""{this._configuration.FacebookUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.FacebookUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/face.png""
                                                                                alt=""Facebook"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}

    {(!string.IsNullOrEmpty(this._configuration.İnstagramUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.İnstagramUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.İnstagramUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/insta.png""
                                                                                alt=""instagram"" width=""32"" height=""32"" border=""0"">
                                                                        </a>
                                                                    </td>" : "")}


    {(!string.IsNullOrEmpty(this._configuration.YoutubeUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.YoutubeUrl}""
                                                                            style=""text-decoration:none;display:block""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.YoutubeUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/youtube.png""
                                                                                alt=""Instagram"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}                                                                   
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"" style=""padding:2.5em 5% 3em"">
                                                        <table border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=""center"" style=""padding-bottom:0.5em"" valign=""middle"">
                                                                        <strong style=""font-family:Arial,Helvetica,sans-serif;font-size:0.6875em;color:#484848"">{this._configuration.WebShortName}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                            </td>
                        </tr>

                    </tbody>
                </table>

                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""30"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

        </tr>
    </tbody>

</body>

</html>";
                response.Data = emailTemplate;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Email template getirilemedi.";
            });
        }

        public DataResponse<EmailTemplate> NewOrderTemplate(int orderId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailTemplate>>((response) =>
            {
                var emailTemplate = new EmailTemplate();

                var order = _unitOfWork
                 .OrderRepository
                 .DbSet()
                 .Include(o => o.OrderDetails)
                 .ThenInclude(pi => pi.ProductInformation.ProductInformationTranslations)
                 .Include(o => o.Customer.CustomerContact)
                 .Include(od => od.OrderDeliveryAddress.Neighborhood.District.City.Country)
                 .Include(od => od.OrderInvoiceInformation.Neighborhood.District.City.Country)
                 .Include(p => p.Payments)
                 .FirstOrDefault(x => x.Id == orderId);

                var payment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "OO");


                var logo = $"{this._configuration.WebUrl}/themes/green/imgs/theme/logo.png";

                var orderDetailHtml = "";
                foreach (var odLoop in order.OrderDetails)
                {
                    var entityTranslation = odLoop.ProductInformation
              .ProductInformationTranslations
              .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                    orderDetailHtml += @$"<tr>
                                            <td  align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {entityTranslation.Name} </td>
                                            <td  width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td  width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {odLoop.Quantity}</td>
                                        </tr>";
                }


                emailTemplate.Subject = "Sipariş Özeti";
                emailTemplate.Template = @$"<html>

<head>
</head>

<body>
    <tbody>
        <tr>
            <td style=""width:660px;max-width:660px;min-width:660px"" align=""center"">
                <!--Logo Banner-->
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td>
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <th align=""center"" valign=""middle""
                                                style=""font-weight:normal;vertical-align:middle"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""center"" style=""padding:7px 0"">
                                                                <a href=""{_configuration.WebUrl}"" target=""_blank""
                                                                    data-saferedirecturl=""logo link""><img
                                                                        src=""{logo}""
                                                                        width=""180"" alt="""" border=""0""
                                                                        style=""display:block""></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--Logo Banner-->
                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""7"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <!--Müşteri Bilgileri ve Teşekkürler-->
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" bgcolor=""#ffffff"">
                    <tbody>
                        <tr>
                            <td align=""left"" valign=""middle"" width=""602"" height=""60""
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:#484848;padding:5px 0 5px 10px"">
                                <span> Sevgili {order.Customer.Name.ToUpper()} {order.Customer.Surname.ToUpper()}, siparişiniz başarılı bir şekilde oluşturuldu.</span> </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" bgcolor=""#f3f3f4"">
                    <tbody>
                        <tr>
                            <td align=""left""
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#484848;padding:20px 10px"">
                                <span>Sipariş</span> Numaranız: &nbsp; <font style=""font-weight:bold;color:#ef8f0a"">
                                    {order.Id}</font>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td align=""left""
                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:15px 10px"">
                                Bir sonraki alışverişinde görüşmek üzere.<br> Bu süreçte aklınıza takılan bir soru olur ise 
                                <a href=""{_configuration.WebUrl}/Home/Faq"">Sıkça Sorulan Sorular</a>  sayfamızdan destek alabilirsiniz.<br>  <br> {_configuration.WebShortName} {_configuration.PhoneNumber} </td>
                        </tr>
                    </tbody>
                </table>
                <!--Müşteri Bilgileri ve Teşekkürler-->
                <!--ürün adet fiyat-->
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <th width=""640"" align=""left"" valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Ürün</td>
                                            <td bgcolor=""#f3f3f4"" width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td bgcolor=""#f3f3f4"" width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Adet</td>
                                        </tr>
                                        <!--Ürün Bilgileri-->
                                        <!--Foreach ile çekilecek ürün bilgisi--> 
                                            {orderDetailHtml}
                                        <!--Foreach ile çekilecek ürün bilgisi-->
                                        <!--Ürün Bilgileri-->
                                        <!--Sipariş Özetleri-->
                                        <tr>
                                            <td align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                            </td>
                                            <td width=""35"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px;text-align: right"">
                                                Toplam:</td>
                                            <td width=""70"" align=""center""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                {order.Total} {order.CurrencyId}</td>
                                        </tr>
                                        <!--Sipariş Özetleri-->
                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0""
                    class=""m_6122722835262201619content-wrapper"" align=""center"">
                    <tbody>
                        <tr>
                            <td
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:#ef8f0a;padding:0 10px 10px 10px"">
                                Teslimat Bilgileri </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0""
                    class=""m_6122722835262201619content-wrapper"" align=""center"">
                    <tbody>
                        <tr>
                            <th class=""m_6122722835262201619column"" align=""center"" valign=""top""
                                style=""font-weight:normal;vertical-align:top"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <th class=""m_6122722835262201619column"" width=""50%"" align=""left""
                                                valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                                Teslimat Adresi </td>
                                                        </tr>
                                                        <tr>
                                                            <td align=""left"" valign=""top""
                                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                                    {order.OrderDeliveryAddress.Recipient} <br>{order.OrderDeliveryAddress.Address}<br>{order.OrderDeliveryAddress.Neighborhood.District.Name}<br>{order.OrderDeliveryAddress.Neighborhood.District.City.Name}<br>
                                      {order.OrderDeliveryAddress.Neighborhood.District.City.Country.Name}<br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th class=""m_6122722835262201619column"" width=""50%"" align=""center""
                                                valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                                Fatura Adresi </td>
                                                        </tr>
                                                        <tr>
                                                            <td align=""left"" valign=""top""
                                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                                    {order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName} <br>{order.OrderInvoiceInformation.Address}<br>{order.OrderInvoiceInformation.Neighborhood.District.Name}<br>{order.OrderInvoiceInformation.Neighborhood.District.City.Name}<br>
                                      {order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name}<br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px;border-top:1px dotted #ccc"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""15"" style=""font-size:1px;line-height:1px"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td
                                style=""font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;color:#ef8f0a;padding:0 10px 10px 10px"">
                                Ödeme Bilgileri </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <th align=""left"" valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Ödeme Şekli</td>
                                        </tr>
                                        <tr>
                                            <td align=""left"" valign=""top""
                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                Kredi Kartı</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                            <th align=""center"" valign=""top"" style=""font-weight:normal;vertical-align:top"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" border=""0"">
                                    <tbody>
                                        <tr>
                                            <td bgcolor=""#f3f3f4"" align=""left""
                                                style=""border-top:1px dotted #ccc;border-bottom:1px dotted #ccc;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:13px;padding:10px"">
                                                Tutar </td>
                                        </tr>
                                        <tr>
                                            <td align=""left"" valign=""top""
                                                style=""font-family:Arial;font-size:13px;line-height:18px;color:#484848;padding:10px"">
                                                {payment?.Amount} {order.CurrencyId}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center""> </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""30"" style=""font-size:1px;line-height:1px;border-top:1px dotted #ccc"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">
                    <tbody>
                        <tr>
                            <td bgcolor=""#f5f5f5"" style=""padding:0 5%;border-bottom:0.0625em solid #c4c4c4"">
                                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                    <tbody>
                                        <tr>
                                            <td align=""center""
                                                style=""font-family:Arial,Helvetica,sans-serif;color:#484848;padding:2em 0 1em"">
                                                <strong style=""font-size:0.8125em"">- BİZİ TAKİP EDİN -</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align=""center"">
                                                <table cellspacing=""0"" cellpadding=""0"" border=""0"">
                                                    <tbody>
                                                        <tr>
                                                      {(!string.IsNullOrEmpty(this._configuration.FacebookUrl) ? @$"<td style=""padding:0 0.5em"">
                                                             <a href=""{this._configuration.FacebookUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.FacebookUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/face.png""
                                                                                alt=""Facebook"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}

    {(!string.IsNullOrEmpty(this._configuration.İnstagramUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.İnstagramUrl}""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.İnstagramUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/insta.png""
                                                                                alt=""instagram"" width=""32"" height=""32"" border=""0"">
                                                                        </a>
                                                                    </td>" : "")}


    {(!string.IsNullOrEmpty(this._configuration.YoutubeUrl) ? @$"<td style=""padding:0 0.5em"">
                                                                        <a href=""{this._configuration.YoutubeUrl}""
                                                                            style=""text-decoration:none;display:block""
                                                                            target=""_blank""
                                                                            data-saferedirecturl=""{this._configuration.YoutubeUrl}"">
                                                                            <img src=""https://snzimg.g.lyrcdn.com/Default/youtube.png""
                                                                                alt=""Instagram"" width=""32"" height=""32""
                                                                                border=""0"">
                                                                        </a>
                                                                    </td>" : "")}
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align=""center"" style=""padding:2.5em 5% 3em"">
                                                <table border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                    <tbody>
                                                        <tr>
                                                            <td align=""center"" style=""padding-bottom:0.5em"" valign=""middle"">
                                                                <strong style=""font-family:Arial,Helvetica,sans-serif;font-size:0.6875em;color:#484848"">{_configuration.WebShortName}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                    <tbody>
                        <tr style=""font-size:1px;line-height:1px"">
                            <td height=""30"" style=""font-size:1px;line-height:1px;"">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
        </tr>
    </tbody>
</body>

</html>";
                response.Data = emailTemplate;
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Email template getirilemedi.";
            });
        }

        public DataResponse<EmailTemplate> NewBulletinTemplate(int informationId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<EmailTemplate>>((response) =>
            {

                var information = this._unitOfWork.InformationRepository
              .DbSet()
              .Include(p => p.InformationTranslations)
              .ThenInclude(x => x.InformationTranslationContent)
              .FirstOrDefault(x => x.Id == informationId);


                var informationTranslation = information.InformationTranslations.FirstOrDefault(x => x.LanguageId == "TR");

                var emailTemplate = new EmailTemplate();
                var logo = $"{this._configuration.WebUrl}/themes/green/imgs/theme/logo.png";

                var html = informationTranslation.InformationTranslationContent.Html;
                html = html.Length > 200 ? html.Substring(0, 200) + "..." : "";

                emailTemplate.Subject = informationTranslation.Header;
                emailTemplate.Template = @$"<html>
    <head>
        <title>Şifre Yenileme</title>
    </head>
    <body>
        <table
            style=""width:600px !important;height:100%!important;line-height:100%!important;border-spacing:0;border-collapse:collapse;margin:auto;padding:0;border:0;font-size:16px""
            cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
            <tbody>
                <tr>
                    <td valign=""top"" bgcolor=""#e5e5e5"" align=""center"" style=""vertical-align:top"">
                        <table width=""720"" cellspacing=""0"" cellpadding=""0"" border=""0"">
                            <tbody>
        
                                <tr>
                                    <td>
                                        <table style=""border-spacing:0;border-collapse:collapse;width:100%"" cellspacing=""0""
                                            cellpadding=""0"" border=""0"">
                                            <tbody>
                                                <tr>
                                                    <td align=""center"" bgcolor=""#f5f5f5"">
                                                        <a href=""{_configuration.WebUrl}"" target=""_blank"">
                                                            <img width=""280"" src=""{logo}""
                                                                alt=""""
                                                                style=""outline:none;text-decoration:none;display:block""
                                                                border=""0"">
                                                        </a>
                                                    </td>
        
        
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
        
        
                                <tr height=""385"">
                                    <td bgcolor=""#ffffff"" style=""padding:2em 5%;border:1px solid #ecebec;"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        style=""font-size:25px;font-family:'AvenirNext',Arial,Helvetica,sans-serif;color:green;text-align:center"">
                                                       {informationTranslation.Header}</td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        style=""font-size:15px;color:#484848;text-align:center;padding:1.5em 0 0;font-family:Arial,Helvetica,sans-serif;line-height: 34px"">
                                                        {html}
                                                    </td>
                                                </tr>
        
                                                <tr>
                                                    <td style=""padding:.5em 0 0"">
                                                        <table align=""center"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                            <tbody>
                                                                <tr>
                                                                    <td width=""100%"" height=""56"" align=""center"">
                                                                        <div>
                                                                            <a href=""{_configuration.WebUrl}/{informationTranslation.Url}""
                                                                                style=""background-color:#f5f5f5;height:56px;font-family:monospace;text-align:center;line-height:56px;color:#484848;text-decoration:none;display:block;padding-left:32px;padding-right:32px;border-radius:28px;font-size:21px;white-space:nowrap""
                                                                                target=""_blank""
                                                                                data-saferedirecturl=""{_configuration.WebUrl}/{informationTranslation.Url}"">Haberi detaylı oku.
                                                                              </a>
        
        
                                                                        </div>
        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
        
        
        
                                <tr>
                                    <td bgcolor=""#f5f5f5"" style=""padding:0 5%"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <hr noshade="""" color=""#E5E5E5""
                                                            style=""border:0;height:1px;font-size:0;line-height:0"">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor=""#f5f5f5"" style=""padding:0 5%;border-bottom:0.0625em solid #c4c4c4"">
                                        <table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                            <tbody>
                                                <tr>
                                                    <td align=""center""
                                                        style=""font-family:Arial,Helvetica,sans-serif;color:#484848;padding:2em 0 1em"">
                                                        <strong style=""font-size:0.8125em"">- BİZİ TAKİP EDİN -</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"">
                                                        <table cellspacing=""0"" cellpadding=""0"" border=""0"">
                                                            <tbody>
                                                                <tr>
                                                                   
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align=""center"" style=""padding:2.5em 5% 3em"">
                                                        <table border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=""center"" style=""padding-bottom:0.5em"" valign=""middle"">
                                                                        <strong style=""font-family:Arial,Helvetica,sans-serif;font-size:0.6875em;color:#484848"">{_configuration.WebShortName}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
         {(!string.IsNullOrEmpty(this._configuration.FacebookUrl) ? @$"
                                                                    <td style="" padding:0 0.5em"">
                                                                        <a href="" {this._configuration.FacebookUrl}""
                                                                           target="" _blank""
                                                                           data-saferedirecturl="" {this._configuration.FacebookUrl}"">
                                                                            <img src="" https: //snzimg.g.lyrcdn.com/Default/face.png""
                                                                                 alt="" Facebook"" width="" 32"" height="" 32""
                                                                                 border="" 0"">
                                                                        </a>
                                                                    </td>" : "")}

                                                                    {(!string.IsNullOrEmpty(this._configuration.İnstagramUrl) ? @$"
                                                                    <td style="" padding:0 0.5em"">
                                                                        <a href="" {this._configuration.İnstagramUrl}""
                                                                           target="" _blank""
                                                                           data-saferedirecturl="" {this._configuration.İnstagramUrl}"">
                                                                            <img src="" https: //snzimg.g.lyrcdn.com/Default/insta.png""
                                                                                 alt="" instagram"" width="" 32"" height="" 32"" border="" 0"">
                                                                        </a>
                                                                    </td>" : "")}


                                                                    {(!string.IsNullOrEmpty(this._configuration.YoutubeUrl) ? @$"
                                                                    <td style="" padding:0 0.5em"">
                                                                        <a href="" {this._configuration.YoutubeUrl}""
                                                                           style="" text-decoration:none;display:block""
                                                                           target="" _blank""
                                                                           data-saferedirecturl="" {this._configuration.YoutubeUrl}"">
                                                                            <img src="" https: //snzimg.g.lyrcdn.com/Default/youtube.png""
                                                                                 alt="" Instagram"" width="" 32"" height="" 32""
                                                                                 border="" 0"">
                                                                        </a>
                                                                    </td>" : "")}
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
        
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>";


                response.Data = emailTemplate;
                response.Success = true;
            }, (response, exception) =>
                  {
                      response.Success = false;
                      response.Message = "Email template getirilemedi.";
                  });
        }
        #endregion
    }
}
