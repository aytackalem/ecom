﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class MoneyPointService : IMoneyPointService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MoneyPointService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods

        #endregion
    }
}
