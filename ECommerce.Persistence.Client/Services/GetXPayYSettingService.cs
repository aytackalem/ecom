﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class GetXPayYSettingService : IGetXPayYSettingService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors
        public GetXPayYSettingService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<GetXPayYSetting> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<GetXPayYSetting>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .GetXPayYSettingRepository
                    .DbSet()
                    .FirstOrDefault(x => !x.Deleted && x.Active && x.StartDate.Date < DateTime.Now.Date && x.EndDate.Date > DateTime.Now.Date);
                if (entity == null)
                {
                    response.Data = new GetXPayYSetting();
                    response.Success = true;
                    response.Message = "X Al Y öde ayarları bulunamadı.";
                    return;
                }

                response.Data = new GetXPayYSetting
                {
                    X = entity.X,
                    XLimit = entity.XLimit,
                    Y = entity.Y,
                    Active = entity.Active
                };
                response.Success = true;
                response.Message = "İndirim kuponu başarılı bir şekilde getirildi.";
            });
            }, $"{CacheKey.GetXPayYSettings}", 10);
        }
        #endregion
    }
}
