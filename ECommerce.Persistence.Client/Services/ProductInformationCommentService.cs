﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ProductInformationCommentService : IProductInformationCommentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public ProductInformationCommentService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(ProductInformationComment productInformationComment)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var productInformationCommentCount = _unitOfWork.ProductInformationCommentRepository.DbSet().Count(x => x.CreatedDate.Date == DateTime.Now.Date && x.IpAddress == _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString());

                if (productInformationCommentCount >= 20)
                {
                    response.Success = false;
                    response.Message = "Günlük yorum limitiniz 20'dir. Arttırmak isterseniz müşteri iletişim formundan talepte bulunabilirsiniz.";
                    return;
                }


                var _productInformationComment = new Domain.Entities.ProductInformationComment
                {
                    ProductInformationId = productInformationComment.ProductInformationId,
                    FullName = productInformationComment.FullName.CleanParameter(),
                    Comment = productInformationComment.Comment.CleanParameter(),
                    IpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                    Rating = productInformationComment.Rating,
                    Active = true,
                    Approved = false,
                    CreatedDate = DateTime.Now
                };
                this._unitOfWork.ProductInformationCommentRepository.Create(_productInformationComment);
                response.Success = true;
            });
        }

        public PagedResponse<ProductInformationComment> InfiniteRead(int productInformationId, int page, int pageRecordsCount)
        {
            return ExceptionHandler.ResultHandle<PagedResponse<ProductInformationComment>>((response) =>
            {
                var queryable = this
                    ._unitOfWork
                    .ProductInformationCommentRepository
                    .DbSet()
                    .Where(p => p.ProductInformationId == productInformationId && p.Active && p.Approved);

                response.RecordsCount = queryable.Count();
                response.Page = page;
                response.PageRecordsCount = pageRecordsCount;

                if (response.PageRecordsCount == 0)
                {
                    response.Message = $"Yorum bulunamadı.";
                    response.Success = false;
                    return;
                }

                response.Data = queryable
                                .Skip(pageRecordsCount * page)
                                .Take(pageRecordsCount)
                                .Select(q => new ProductInformationComment
                                {
                                    FullName = q.FullName,
                                    Comment = q.Comment,
                                    Rating = q.Rating,
                                    CreatedDate = q.CreatedDate
                                })
                                .ToList();
            });
        }
        #endregion
    }
}
