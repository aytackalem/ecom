﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class DiscountCouponService : IDiscountCouponService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICookieHelper _cookieHelper;

        private const string _cookieName = "DiscountCoupon";

        #endregion

        #region Constructors
        public DiscountCouponService(IUnitOfWork unitOfWork, ICookieHelper cookieHelper)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cookieHelper = cookieHelper;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<DiscountCoupon> Read(string code)
        {
            return ExceptionHandler.ResultHandle<DataResponse<DiscountCoupon>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .DiscountCouponRepository
                    .DbSet()
                    .Include(dc => dc.DisountCouponSetting)
                    .FirstOrDefault(dc => dc.Code == code && dc.Active && (!dc.DisountCouponSetting.SingleUse || (dc.DisountCouponSetting.SingleUse && !dc.DisountCouponSetting.Used)));
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "İndirim kuponu bulunamadı.";
                    return;
                }

                response.Data = new DiscountCoupon
                {
                    Id = entity.Id,
                    Code = code,
                    DisountCouponSetting = new DiscountCouponSetting
                    {
                        Discount = entity.DisountCouponSetting.Discount,
                        DiscountIsRate = entity.DisountCouponSetting.DiscountIsRate,
                        Limit = entity.DisountCouponSetting.Limit,
                        SingleUse = entity.DisountCouponSetting.SingleUse
                    }
                };
                response.Success = true;
                response.Message = "İndirim kuponu başarılı bir şekilde getirildi.";
            });
        }

        public Response Clear()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Delete(_cookieName);
                response.Success = true;
            }, (response, exception) =>
            {
            });
        }

        public Response Set(string code)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Write(_cookieName, new List<KeyValue<string, string>>
                {
                    new KeyValue<string, string> { Key = "Code", Value = code }
                });
            }, (response, exception) =>
            {
            });
        }


        #endregion
    }
}
