﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CompanyService : ICompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        protected readonly ICompanyFinder _companyFinder;

        protected readonly IConfiguration _configuration;


        #endregion

        #region Constructors
        public CompanyService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler, ICompanyFinder companyFinder, IConfiguration configuration)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._companyFinder = companyFinder;
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<Company> Read()
        {
            var companyId = this._companyFinder.FindId();

            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<Company>>((response) =>
                {
                    var company = _unitOfWork.CompanyRepository.DbSet().Select(x => new Company
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CompanyContact = new CompanyContact
                        {
                            Address = x.CompanyContact.Address,
                            Email = x.CompanyContact.Email,
                            PhoneNumber = x.CompanyContact.PhoneNumber,
                            TaxNumber = x.CompanyContact.TaxNumber,
                            TaxOffice = x.CompanyContact.TaxOffice,
                            WebUrl = !string.IsNullOrEmpty(this._configuration.GetSection("WebUrl").Value) ? this._configuration.GetSection("WebUrl").Value : "",
                            Instagram = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "InstagramUrl").Value,
                            Youtube = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "YoutubeUrl").Value,
                            Facebook = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "FacebookUrl").Value,
                            Iban = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "Iban").Value,
                            IbanType = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "IbanBank").Value,
                            IbanNameSurname = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "IbanNameSurname").Value,
                            CargoLimit = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "CargoLimit").Value,
                            CargoAmount = x.CompanyConfigurations.FirstOrDefault(x => x.Key == "CargoAmount").Value
                        }
                    })
                         .FirstOrDefault(x => x.Id == companyId);

                    response.Data = company;
                    response.Success = true;
                }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Firma bulunamadı";
                });
            }, string.Format(CacheKey.Company, companyId));
        }

        #endregion
    }
}
