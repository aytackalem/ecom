﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class DistrictService : IDistrictService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public DistrictService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;

            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<District> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<District>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .DistrictRepository
                    .DbSet()
                    .FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "İlçe bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = new District
                    {
                        Id = id,
                        Name = entity.Name,
                        Url = entity.Name.ToLowerNormalize()
                    };
                    response.Success = true;
                    response.Message = "İlçe başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İlçe başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<List<District>> ReadByCityId(int cityId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<District>>>((response) =>
            {
                var entities = this._unitOfWork.DistrictRepository.DbSet().Where(n => n.CityId == cityId).OrderBy(n => n.Name).ToList();
                if (entities?.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "İlçe bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = entities
                        .Select(e => new District
                        {
                            Id = e.Id,
                            Name = e.Name,
                            Url = e.Name.ToLowerNormalize()
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "İlçeler başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İlçeler başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.DistrictsByCityId, cityId));
        }
        #endregion
    }
}
