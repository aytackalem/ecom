﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ContactFormService : IContactFormService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly IEmailService _emailService;

        private readonly Configuration _configuration;
        #endregion

        #region Constructors
        public ContactFormService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, IEmailService emailService, IConfigrationService configrationService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._emailService = emailService;
            this._configuration = configrationService.Read().Data;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(ContactForm contactForm)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var body = $@"<h1>İletişim Formu</h1>
                              <div>Ad Soyad: {contactForm.Name.CleanParameter()}</div>
                              <div>Telefon: {contactForm.PhoneNumber.ToNumber()}</div>
                              <div>Konu: {contactForm.Subject.CleanParameter()}</div>
                              <div>Mesaj: {contactForm.Message.CleanParameter()}</div>";

                var emails = new List<string>();
                emails.Add("bilgi@fidanistanbul.com");
                emails.Add(contactForm.Mail.CleanParameter());

                this._emailService.Send(emails, $"{this._configuration.WebShortName} İletişim Formu", body);

                response.Success = true;
                response.Message = "İletişim formunuz gönderilmiştir. En kısa sürede sizinle iletişim kuracağız.";
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Mail gönderilemedi. Lütfen daha sonra tekrar deneyiniz";
            });
        }
        #endregion
    }
}
