﻿using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ShowcaseService : IShowcaseService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        protected readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public ShowcaseService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler, ILocalizationHelper localizationHelper, IConfiguration configuration)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._configuration = configuration;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Showcase> Read(int? brandId = null, int? categoryId = null)
        {

            return ExceptionHandler.ResultHandle<DataResponse<Showcase>>(response =>
        {
            var entity = this
                ._unitOfWork
                .ShowcaseRepository
                .DbSet()
                .FirstOrDefault(s => s.BrandId == brandId && s.CategoryId == categoryId);

            if (entity == null)
            {
                response.Success = false;
                response.Message = "Vitrin bulunamadı.";
            }
            else
            {
                response.Data = new Showcase
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    RowComponents = JsonConvert.DeserializeObject<List<RowComponent>>(entity.Json)
                };

                var showcaseImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ShowcaseImageUrl").Value) ? this._configuration.GetSection("ShowcaseImageUrl").Value : "";
                var productImageUrl = !string.IsNullOrEmpty(this._configuration.GetSection("ProductImageUrl").Value) ? this._configuration.GetSection("ProductImageUrl").Value : "";

                response.Data.RowComponents.ForEach(rc =>
                {
                    rc.ColumnComponents.ForEach(cc =>
                    {
                        cc.ProductComponents.ForEach(pc =>
                        {
                            pc.FileName = $@"{productImageUrl}/{pc.FileName}";
                        });

                        cc.BannerComponents.ForEach(bc =>
                        {
                            bc.FileName = $@"{showcaseImageUrl}/banner/{bc.FileName}";
                        });

                        cc.SliderComponents.ForEach(sc =>
                        {
                            sc.ProductComponents.ForEach(pc =>
                            {
                                pc.FileName = $@"{productImageUrl}/{pc.FileName}";
                            });

                            sc.BannerComponents.ForEach(bc =>
                            {
                                bc.FileName = $@"{showcaseImageUrl}/banner/{bc.FileName}";
                            });
                        });
                    });
                });

                response.Success = true;
                response.Message = "Vitrin başarılı bir şekilde getirildi.";
            }
        }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Vitrin başarılı bir şekilde getirilemedi.";
            });

        }
        #endregion
    }
}
