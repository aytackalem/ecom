﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class PaymentTypeService : IPaymentTypeService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public PaymentTypeService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<PaymentType>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<PaymentType>>>((response) =>
                {
                    var entities = this
                        ._unitOfWork
                        .PaymentTypeDomainRepository
                        .DbSet()
                        .Include(pt => pt.PaymentType)
                        .ThenInclude(pt => pt.PaymentTypeTranslations)
                        .Include(x=>x.PaymentTypeSetting)
                        .Where(c => c.Active)
                        .ToList();

                    if (entities?.Count == 0)
                    {
                        response.Success = false;
                        response.Message = "ödeme tipi bulunamadı.";
                    }
                    else
                    {
                        response.Data = entities
                            .Select(e =>
                            {
                                var entitytranslation = e.PaymentType.PaymentTypeTranslations                                    
                                    .FirstOrDefault(pit => pit.LanguageId == this._language.Id);

                                return new PaymentType
                                {
                                    Id = e.PaymentType.Id,
                                    Name = entitytranslation.Name,
                                    PaymentTypeSetting = new PaymentTypeSetting
                                    {
                                        Cost = e.PaymentTypeSetting.Cost,
                                        CostIsRate = e.PaymentTypeSetting.CostIsRate,
                                        Discount = e.PaymentTypeSetting.Discount,
                                        DiscountIsRate = e.PaymentTypeSetting.DiscountIsRate
                                    }
                                };
                            })
                            .ToList();

                        response.Success = true;
                        response.Message = "ödeme tipleri başarılı bir şekilde getirildi.";
                    }
                }, (response, exception) =>
                  {
                      response.Success = false;
                      response.Message = "Ödeme tipleri başarılı bir şekilde getirilemedi.";
                  });
            }, CacheKey.PaymentTypes);
        }
        #endregion
    }
}
