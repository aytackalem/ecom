﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class NewBulletinService : INewBulletinService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        #region Constructors
        public NewBulletinService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, IHttpContextAccessor httpContextAccessor)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._httpContextAccessor = httpContextAccessor;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(NewBulletin newBulletin)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var exsist = _unitOfWork.NewBulletinRepository.DbSet().Any(x =>
                x.Mail.ToLower() == newBulletin.Mail.ToLower()
                && !x.Deleted
                && x.Active
                );

                if (exsist)
                {
                    response.Success = true;
                    response.Message = "Mail adresiniz bültene eklenmiştir.";
                    return;
                }

                var newBulletinCount = _unitOfWork.NewBulletinRepository.DbSet().Count(x => x.CreatedDate.Date == DateTime.Now.Date && x.IpAddress == _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString());

                if (newBulletinCount >= 5)
                {
                    response.Success = false;
                    response.Message = "Günlük bülten limitiniz 5'dir. Arttırmak isterseniz müşteri iletişim formundan talepte bulunabilirsiniz.";
                    return;
                }



                var create = _unitOfWork.NewBulletinRepository.Create(new Domain.Entities.NewBulletin
                {
                    Mail = newBulletin.Mail,
                    Active = true,
                    Deleted = false,
                    CreatedDate = DateTime.Now,
                    IpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString()
                });

                if (!create)
                {
                    response.Success = false;
                    response.Message = "Mail adresiniz eklenirken bir hata oluştu daha sonra tekrar deneyiniz.";
                    return;
                }

                response.Success = true;
                response.Message = "Mail adresiniz bültene eklenmiştir.";
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Mail adresiniz eklenirken bir hata oluştu daha sonra tekrar deneyiniz.";
            });
        }
        #endregion
    }
}
