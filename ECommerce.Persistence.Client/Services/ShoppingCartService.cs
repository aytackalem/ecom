﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.Parameters.ShoppingCarts;
using ECommerce.Application.Client.ViewModels.ShoppingCarts;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ShoppingCartService : IShoppingCartService
    {
        #region Fields
        private readonly ICookieHelper _cookieHelper;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly Currency _currency;

        private readonly Configuration _configuration;

        private readonly IShoppingCartDiscountedProductService _shoppingCartDiscountedProductService;

        private readonly IProductService _productService;

        private readonly IGetXPayYSettingService _getXPayYSettingService;

        #endregion

        #region Constructors
        public ShoppingCartService(ICookieHelper cookieHelper, ILocalizationHelper localizationHelper, IShoppingCartDiscountedProductService shoppingCartDiscountedProductService,
            IProductService productService, IGetXPayYSettingService getXPayYSettingService, IConfigrationService configrationService)
        {
            #region Fields
            this._cookieHelper = cookieHelper;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._currency = this._localizationHelper.GetCurrency();

            this._shoppingCartDiscountedProductService = shoppingCartDiscountedProductService;
            this._productService = productService;
            this._getXPayYSettingService = getXPayYSettingService;

            var configration = configrationService.Read();
            if (configration != null)
            {
                _configuration = configration.Data;
            }
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<ShoppingCartItem>> Mapping(List<ShoppingCartItem> shoppingCartItems)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ShoppingCartItem>>>((response) =>
            {
                var shoppingCartDiscountedProducts = _shoppingCartDiscountedProductService.Read();
                foreach (var dLoop in shoppingCartItems)
                {
                    if (dLoop.ShoppingCartDiscount)
                    {
                        var shoppingCartDiscountedProduct = shoppingCartDiscountedProducts
                            .Data
                            .FirstOrDefault(x => x.ProductInformation.Id == dLoop.ProductInformationId);

                        dLoop.ProductInformation = shoppingCartDiscountedProduct.ProductInformation;
                    }
                    else
                    {
                        var productResponse = this._productService.ReadInfo(dLoop.ProductInformationId);
                        if (!productResponse.Success)
                        {
                            response.Success = false;
                            response.Message = "Ürün bulunamadı";
                            return;
                        }

                        dLoop.ProductInformation = productResponse.Data;
                    }
                    dLoop.ProductInformation.ProductInformationPrice.TotalUnitPrice = dLoop.ProductInformation.ProductInformationPrice.UnitPrice * dLoop.Quantity;
                }

                var getXPayYSettingResponse = this._getXPayYSettingService.Read();
                if (getXPayYSettingResponse.Success && getXPayYSettingResponse.Data.Active)
                {
                    var getXPayYSetting = getXPayYSettingResponse.Data;
                    //TO DO: Limit üstü kalan ürünleri bul adet bazında kontrol et
                    if (shoppingCartItems.Count(sci => sci.ProductInformation.ProductInformationPrice.UnitPrice >= getXPayYSetting.XLimit) >= getXPayYSetting.X)
                    {
                        var shoppingCartItem = shoppingCartItems
                             .OrderBy(sci => sci.ProductInformation.ProductInformationPrice.UnitPrice)
                             .FirstOrDefault();
                        shoppingCartItem.GetXPayYGift = true;
                        shoppingCartItem.GetXPayYCount = getXPayYSetting.X - getXPayYSetting.Y;
                        shoppingCartItem.ProductInformation.ProductInformationPrice.TotalUnitPrice -= shoppingCartItem.ProductInformation.ProductInformationPrice.UnitPrice;
                    }
                }

                response.Data = shoppingCartItems;
                response.Success = true;
                response.Message = "Sepet verileri başarılı bir şekilde getirildi.";
            }, (response, exception) =>
            {
            });
        }
        public DataResponse<List<ShoppingCartItem>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ShoppingCartItem>>>((response) =>
            {
                var shoppingCartItems = this.Read();
                var result = this.Mapping(shoppingCartItems);

                response.Data = result.Data;
                response.Message = result.Message;
                response.Success = result.Success;
            }, (response, exception) =>
            {

            });
        }

        public Create Add(AddRequest addRequest)
        {
            List<ShoppingCartItem> shoppingCartItems;
            if (!this._cookieHelper.Exist(_configuration.ShoppingCartCookie))
                shoppingCartItems = new List<ShoppingCartItem>();
            else
                shoppingCartItems = this.Read();

            if (addRequest.Quantity <= 0)
            {
                addRequest.Quantity = 1;
            }


            if (shoppingCartItems.Any(sci => sci.ProductInformationId == addRequest.ProductInformationId && sci.ShoppingCartDiscount == addRequest.ShoppingCartDiscount))
            {
                var shoppingCartItem = shoppingCartItems.FirstOrDefault(sci => sci.ProductInformationId == addRequest.ProductInformationId);
                shoppingCartItem.Quantity += addRequest.Quantity;
            }
            else
            {
                shoppingCartItems.Add(new ShoppingCartItem
                {
                    ProductInformationId = addRequest.ProductInformationId,
                    Quantity = addRequest.Quantity,
                    ShoppingCartDiscount = addRequest.ShoppingCartDiscount
                });
            }

            this.Write(shoppingCartItems);

            return new Create { Success = true, Message = "Başarılı", Count = shoppingCartItems.Count, ShoppingCartItems = shoppingCartItems };
        }

        public Response Clear()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Delete(_configuration.ShoppingCartCookie);
                response.Success = true;
            }, (response, exception) =>
            {
            });
        }

        public DataResponse<int> Count()
        {
            if (!this._cookieHelper.Exist(_configuration.ShoppingCartCookie))
                return new DataResponse<int> { Data = 0, Success = true };
            else
            {
                return new DataResponse<int>
                {
                    Data = this._cookieHelper.Read(_configuration.ShoppingCartCookie).Count,
                    Success = true
                };
            }
        }

        public Response Remove(RemoveRequest removeRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var shoppingCartItems = this.Read();
                shoppingCartItems.Remove(shoppingCartItems.First(sci => sci.ProductInformationId == removeRequest.ProductInformationId));

                this.Write(shoppingCartItems);

                response.Success = true;

            }, (response, exception) =>
            {
            });
        }

        public Response Update(UpdateRequest updateRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (updateRequest == null)
                {
                    response.Message = "Yanlış değer girdiniz girilen değeri tekrar kontrol ediniz.";
                    response.Success = false;
                    return;
                }

                var shoppingCartItems = this.Read();

                if (updateRequest.Quantity <= 0)
                {
                    updateRequest.Quantity = 1;
                }

                shoppingCartItems.FirstOrDefault(sci => sci.ProductInformationId == updateRequest.ProductInformationId && sci.ShoppingCartDiscount == updateRequest.ShoppingCartDiscount).Quantity = updateRequest.Quantity;

                this.Write(shoppingCartItems);

                response.Success = true;
            });
        }
        #endregion

        #region Private Methods
        private List<ShoppingCartItem> Read()
        {
            return this._cookieHelper
                    .Read(_configuration.ShoppingCartCookie)
                    .Select(kv => JsonConvert.DeserializeObject<ShoppingCartItem>(kv.Value))
                    .ToList();
        }

        private void Write(List<ShoppingCartItem> shoppingCartItems)
        {
            this._cookieHelper
                .Write(_configuration.ShoppingCartCookie, shoppingCartItems.Select(sci => new KeyValue<string, string>
                {
                    Key = sci.ProductInformationId.ToString(),
                    Value = JsonConvert.SerializeObject(sci)
                })
                .ToList());
        }
        #endregion
    }
}
