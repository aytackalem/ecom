﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ConfigrationService : IConfigrationService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly ICompanyService _companyService;
        #endregion

        #region Constructors
        public ConfigrationService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ICompanyService companyService)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._companyService = companyService;
            #endregion
        }
        #endregion

        #region Methods     


        public DataResponse<Configuration> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<Configuration>>((response) =>
                {
                    var configrations = this
                        ._unitOfWork
                        .ConfigurationRepository
                        .DbSet()
                        .OrderByDescending(x => x.Id)
                        .Select(c => new KeyValue<string, string>
                        {
                            Key = c.Key,
                            Value = c.Value
                        })
                        .ToList();
                    var _company = this._companyService.Read();

                    var configration = new Configuration();

                    var cargoLimit = 300m;
                    decimal.TryParse(_company.Data.CompanyContact.CargoLimit, out cargoLimit);

                    var cargoAmount = 0m;
                    decimal.TryParse(_company.Data.CompanyContact.CargoAmount, out cargoAmount);

                    var phoneNumber = configrations.FirstOrDefault(x => x.Key == "PhoneNumber");
                    var webUrl = configrations.FirstOrDefault(x => x.Key == "WebUrl");
                    var shoppingCartCookie = configrations.FirstOrDefault(x => x.Key == "ShoppingCartCookie");
                    var orderCookie = configrations.FirstOrDefault(x => x.Key == "OrderCookie");
                    var company = configrations.FirstOrDefault(x => x.Key == "Company");
                    var iban = configrations.FirstOrDefault(x => x.Key == "Iban");
                    var youtubeUrl = configrations.FirstOrDefault(x => x.Key == "YoutubeUrl");
                    var instagramUrl = configrations.FirstOrDefault(x => x.Key == "InstagramUrl");
                    var facebookUrl = configrations.FirstOrDefault(x => x.Key == "FacebookUrl");
                    var webShortName = configrations.FirstOrDefault(x => x.Key == "WebShortName");
                    var ibanBank = configrations.FirstOrDefault(x => x.Key == "IbanBank");



                    configration.CargoLimit = cargoLimit;
                    configration.CargoAmount = cargoAmount;
                    configration.PhoneNumber = phoneNumber != null ? phoneNumber.Value : "";
                    configration.ShoppingCartCookie = $"ShoppingCartCookie_{(shoppingCartCookie != null ? shoppingCartCookie.Value : "")}";
                    configration.OrderCookie = $"OrderCookie_{(orderCookie != null ? orderCookie.Value : "")}";
                    configration.WebUrl = webUrl != null ? webUrl.Value : "";
                    configration.Iban = iban != null ? iban.Value : "";
                    configration.Company = company != null ? company.Value : "";
                    configration.YoutubeUrl = youtubeUrl != null ? youtubeUrl.Value : "";
                    configration.İnstagramUrl = instagramUrl != null ? instagramUrl.Value : "";
                    configration.FacebookUrl = facebookUrl != null ? facebookUrl.Value : "";
                    configration.WebShortName = webShortName != null ? webShortName.Value : ""; ;
                    configration.IbanBank = ibanBank != null ? ibanBank.Value : ""; ;
                    configration.IbanBank = ibanBank != null ? ibanBank.Value : ""; ;

                    response.Data = configration;
                    response.Success = true;
                    response.Message = "Konfigurasyon başarılı bir şekilde getirildi.";

                }, (response, exception) =>
                {
                    response.Success = true;
                    response.Data = new Configuration
                    {
                        CargoLimit = 300,
                        ShoppingCartCookie = "ShoppingCartCookie_1",
                        OrderCookie = "OrderCookie_1",
                        WebUrl = "",
                        PhoneNumber = "",
                        Company = "",
                        Iban = "",
                        FacebookUrl = "",
                        YoutubeUrl = "",
                        İnstagramUrl = "",
                        WebShortName = ""
                    };
                    response.Message = "Konfigurasyon bulunamadı.";
                });
            }, CacheKey.Configurations);
        }
        #endregion
    }
}
