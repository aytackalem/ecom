﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ProductInformationSubscriptionService : IProductInformationSubscriptionService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ProductInformationSubscriptionService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public Response Create(ProductInformationSubscription productInformationSubscription)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var exsist = this._unitOfWork.ProductInformationSubscriptionRepository.DbSet()
                   .Any(x =>
                        x.PhoneNumber == productInformationSubscription.PhoneNumber.ToNumber() &&
                        x.ProductInformationId == productInformationSubscription.ProductInformationId &&
                        !x.IsSend
                   );
                if (!exsist)
                {
                    var entity = new Domain.Entities.ProductInformationSubscription
                    {
                        CreatedDate = DateTime.Now,
                        IsSend = false,
                        PhoneNumber = productInformationSubscription.PhoneNumber.ToNumber(),
                        ProductInformationId = productInformationSubscription.ProductInformationId
                    };
                    this._unitOfWork.ProductInformationSubscriptionRepository.Create(entity);
                    response.Success = true;
                }
                else
                {
                    response.Message = "Telefon numaranız bu ürün için sistemde kayıtlı.";
                    response.Success = false;
                }

            });
        }

        #endregion
    }
}
