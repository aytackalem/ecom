﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class NeighborhoodService : INeighborhoodService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public NeighborhoodService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Neighborhood> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Neighborhood>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .NeighborhoodRepository
                    .DbSet()
                    .FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Mahalle bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = new Neighborhood
                    {
                        Id = id,
                        Name = entity.Name,
                        Url = entity.Name.ToLowerNormalize()
                    };
                    response.Success = true;
                    response.Message = "Mahalle başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Mahalle başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<List<Neighborhood>> ReadByDistrictId(int districtId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<Neighborhood>>>((response) =>
            {
                var entities = this._unitOfWork.NeighborhoodRepository.DbSet().Where(n => n.DistrictId == districtId).OrderBy(n => n.Name).ToList();
                if (entities?.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "Mahalle bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = entities
                        .Select(e => new Neighborhood
                        {
                            Id = e.Id,
                            Name = e.Name,
                            Url = e.Name.ToLowerNormalize()
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Mahalleler başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Mahalleler başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.NeighborhoodsByDistrictId, districtId));
        }
        #endregion
    }
}
