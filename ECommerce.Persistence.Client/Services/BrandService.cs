﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class BrandService : IBrandService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public BrandService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, IConfiguration configuration, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._configuration = configuration;
            this._cacheHandler = cacheHandler;
            this._dbNameFinder = dbNameFinder;
            #endregion
        }
        #endregion

        #region Methods

        public DataResponse<List<Brand>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<Brand>>>((response) =>
            {
                var brands = this._unitOfWork.BrandRepository.DbSet().ToList();



                response.Data = brands.Select(x => new Brand
                {
                    Id = x.Id,
                    Name = x.Name,
                    Url = x.Url
                }).ToList();

                response.Success = true;
                response.Message = "Marka başarılı bir şekilde getirildi.";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Kategori başarılı bir şekilde getirilemedi.";
            });
            }, CacheKey.Brands);
        }

        public DataResponse<Brand> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Brand>>((response) =>
            {
                var entity = this._unitOfWork.BrandRepository.Read(id);
                if (entity == null)
                {
                    response.Success = true;
                    response.Message = "Marka bulunamadı.";
                }
                else
                {
                    response.Data = new Brand
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Url = entity.Url
                    };
                    response.Success = true;
                    response.Message = "Marka başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Marka başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<List<Property>> ReadBrandProperties(int id)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Property>>>((response) =>
            {
                List<Property> models = new();

                using (sqlConnection =new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var query = @"
Select		PVT.PropertyValueId
			,PVT.[Value]
			,PT.PropertyId
			,PT.[Name]

From		Products AS P With(NoLock)

Join		ProductProperties AS PP With(NoLock)
On			PP.ProductId = P.Id
			And PP.Active = 1

Join		PropertyValues As PV (NoLock)
On			PP.PropertyValueId = PV.Id

Join		PropertyValueTranslations AS PVT
On			PVT.PropertyValueId = PP.PropertyValueId
			And PVT.LanguageId = @LanguageId

Join		Properties As P2 With(NoLock)
On			P2.Id = PV.PropertyId

Join		PropertyTranslations AS PT With(NoLock)
On			PT.PropertyId = P2.Id
			And PT.LanguageId = @LanguageId

Where		P.Active = 1
			And P.BrandId = @BrandId And P2.Deleted=0

Group By	PVT.PropertyValueId
			,PVT.[Value]
			,PT.PropertyId
			,PT.[Name]";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);
                        sqlCommand.Parameters.AddWithValue("@BrandId", id);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            var propertyValueId = sqlDataReader.GetInt32(0);
                            var value = sqlDataReader.GetString(1);
                            var propertyId = sqlDataReader.GetInt32(2);
                            var name = sqlDataReader.GetString(3);

                            if (!models.Any(d => d.Id == propertyId))
                                models.Add(new Property
                                {
                                    Id = propertyId,
                                    Name = name,
                                    PropertyValues = new List<PropertyValue>()
                                });

                            models.Single(d => d.Id == propertyId).PropertyValues.Add(new PropertyValue
                            {
                                Id = propertyValueId,
                                Value = value
                            });
                        }
                    }
                }

                sqlConnection.Close();

                response.Data = models;
                response.Success = true;
                response.Message = "Marka özellikleri başarılı bir şekilde getirildi.";
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

                response.Success = false;
                response.Message = "Marka özellikleri başarılı bir şekilde getirilemedi.";
            });
        }

        public DataResponse<List<ProductLabel>> ReadBrandLabels(int id)
        {
            return new DataResponse<List<ProductLabel>>();

            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<ProductLabel>>>((response) =>
            {
                List<ProductLabel> models = new();

                using (sqlConnection =new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var query = @"
Select		TOP 7
			TT.LabelId,
			TT.Value
From		Products AS P With(NoLock)

Join		ProductLabels AS PT With(NoLock)
On			PT.ProductId = P.Id
			And PT.Active = 1
			

Join		Labels AS TG With(NoLock)
On			TG.Id = PT.LabelId
			And TG.Active = 1
			And TG.Deleted=0

Join		LabelTranslations AS TT With(NoLock)
On			TT.LabelId = TG.Id
			And TT.LanguageId = @LanguageId

Where		P.BrandId = @BrandId		

Group By	TT.LabelId,
			TT.Value";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@LanguageId", this._language.Id);
                        sqlCommand.Parameters.AddWithValue("@BrandId", id);

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            var id = sqlDataReader.GetInt32(0);
                            var value = sqlDataReader.GetString(1);
                            models.Add(new ProductLabel
                            {
                                Id = id,
                                Value = value
                            });
                        }
                    }
                }

                sqlConnection.Close();

                response.Data = models;
                response.Success = true;
                response.Message = "Group özellikleri başarılı bir şekilde getirildi.";
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

                response.Success = false;
                response.Message = "Group özellikleri başarılı bir şekilde getirilemedi.";
            });
        }


        #endregion
    }
}
