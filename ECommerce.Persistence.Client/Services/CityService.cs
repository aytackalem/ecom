﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CityService : ICityService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public CityService(IUnitOfWork unitOfWork, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<City> Read(int id)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<City>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CityRepository
                    .DbSet()
                    .FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "İl bilgisi bulunamadı.";
                }
                else
                {
                    response.Data = new City
                    {
                        Id = id,
                        Name = entity.Name,
                        Url = entity.Name.ToLowerNormalize()
                    };
                    response.Success = true;
                    response.Message = "İl başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İl başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.City, id));
        }

        public DataResponse<List<City>> ReadByCountryId(int countryId)
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<City>>>((response) =>
            {
                var entities = this._unitOfWork.CityRepository.DbSet().Where(c => c.CountryId == countryId).ToList();
                if (entities?.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Şehir bulunamadı.";
                }
                else
                {
                    response.Data = entities
                        .Select(e => new City
                        {
                            Id = e.Id,
                            Name = e.Name,
                            Url = e.Name.ToLowerNormalize()
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Şehir başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Şehir başarılı bir şekilde getirilemedi.";
            });
            }, string.Format(CacheKey.CitiesByCountryId, countryId));
        }
        #endregion
    }
}
