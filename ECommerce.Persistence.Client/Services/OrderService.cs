﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Client.ViewModels.Orders;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Payment;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class OrderService : IOrderService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IPosService _posService;

        private readonly ICookieHelper _cookieHelper;

        private readonly Configuration _configuration;

        private readonly ECommerce.Application.Common.DataTransferObjects.Language _language;

        private readonly ECommerce.Application.Common.DataTransferObjects.Currency _currency;

        private readonly IProductInformationStockService _productInformationStockService;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public OrderService(IUnitOfWork unitOfWork, IPosService posService, ILocalizationHelper localizationHelper, ICookieHelper cookieHelper, IConfigrationService configrationService, IHttpContextAccessor httpContextAccessor, IProductInformationStockService productInformationStockService, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._httpContextAccessor = httpContextAccessor;
            this._posService = posService;
            this._cookieHelper = cookieHelper;
            this._currency = localizationHelper.GetCurrency();
            this._language = localizationHelper.GetLanguage();
            this._productInformationStockService = productInformationStockService;

            var configration = configrationService.Read();
            if (configration != null)
            {
                _configuration = configration.Data;
            }
            this._settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public Create Create(ECommerce.Application.Client.DataTransferObjects.Order order)
        {
            return ExceptionHandler.ResultHandle<Create>((response) =>
            {
                var orderCount = _unitOfWork.OrderTechnicInformationRepository.DbSet().Count(x => x.Order.CreatedDate.Date == DateTime.Now.Date && x.IpAddress == _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString());

                if (order.OrderDetails.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Sepetinizde ürün bulunmamaktadır lütfen sepetinize ürün ekleyiniz.";
                    return;
                }

                if (orderCount >= 20)
                {
                    response.Success = false;
                    response.Message = "Günlük sipariş limitiniz 20'dir. Arttırmak isterseniz müşteri iletişim formundan talepte bulunabilirsiniz.";
                    return;
                }


                var customerId = 0;
                var customerName = string.Empty;
                if (order.Customer?.CustomerUser != null) //Müşteri login ise
                {
                    var customerAddress = this
                           ._unitOfWork
                           .CustomerAddressRepository
                           .DbSet()
                           .FirstOrDefault(c => c.CustomerId == order.Customer.CustomerUser.Id && c.Id == order.CustomerAddresId && !c.Deleted);

                    if (customerAddress == null)
                    {
                        response.Success = false;
                        response.Message = "Size ait bir adres bilgisi bulamadık lütfen tekrar adres bilgisi seçiniz ve tekrar sipariş veriniz.";
                        return;
                    }
                    var recipient = customerAddress.Recipient.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (string.IsNullOrEmpty(customerAddress.Recipient.Trim()) || string.IsNullOrEmpty(recipient[0]))
                    {
                        response.Success = false;
                        response.Message = "Lütfen teslimat adresi kısmında adınızı ve soyadınızı eksiksiz doldurunuz";
                        return;
                    }

                    customerId = order.Customer.CustomerUser.Id;
                    order.OrderDeliveryAddress = new OrderDeliveryAddress();
                    order.OrderDeliveryAddress.Recipient = customerAddress.Recipient;
                    order.OrderDeliveryAddress.PhoneNumber = customerAddress.Phone;
                    order.OrderDeliveryAddress.Address = customerAddress.Address;
                    order.OrderDeliveryAddress.NeighborhoodId = customerAddress.NeighborhoodId;
                    order.OrderDeliveryAddress.Mail = order.Customer.CustomerUser.Customer.CustomerContact.Mail;


                    if (order.Differentaddress)
                    {

                        var customerInvoiceInformation = this
                          ._unitOfWork
                          .CustomerInvoiceInformationRepository
                          .DbSet()
                          .FirstOrDefault(c => c.CustomerId == order.Customer.CustomerUser.Id && c.Id == order.CustomerInvoiceInformationId && !c.Deleted);

                        if (customerAddress == null)
                        {
                            response.Success = false;
                            response.Message = "Size ait bir fatura adres bilgisi bulamadık lütfen tekrar fatura bilgisi seçiniz ve tekrar sipariş veriniz.";
                            return;
                        }
                        order.OrderInvoiceInformation = new OrderInvoiceInformation();
                        order.OrderInvoiceInformation.FirstName = customerInvoiceInformation.FirstName;
                        order.OrderInvoiceInformation.LastName = customerInvoiceInformation.LastName;
                        order.OrderInvoiceInformation.Phone = customerInvoiceInformation.Phone;
                        order.OrderInvoiceInformation.Address = customerInvoiceInformation.Address;
                        order.OrderInvoiceInformation.NeighborhoodId = customerInvoiceInformation.NeighborhoodId;
                        order.OrderInvoiceInformation.TaxNumber = customerInvoiceInformation.TaxNumber;
                        order.OrderInvoiceInformation.TaxOffice = customerInvoiceInformation.TaxOffice;
                        order.OrderInvoiceInformation.Mail = customerInvoiceInformation.Mail;
                    }
                    else
                    {

                        order.OrderInvoiceInformation.FirstName = recipient[0];
                        order.OrderInvoiceInformation.LastName = recipient.Length > 1 ? recipient[1] : recipient[0];
                        order.OrderInvoiceInformation.Phone = customerAddress.Phone;
                        order.OrderInvoiceInformation.Address = customerAddress.Address;
                        order.OrderInvoiceInformation.NeighborhoodId = customerAddress.NeighborhoodId;
                        order.OrderInvoiceInformation.Mail = order.Customer.CustomerUser.Customer.CustomerContact.Mail;
                        order.OrderInvoiceInformation.TaxNumber = null;
                        order.OrderInvoiceInformation.TaxOffice = null;
                    }


                }
                else
                {
                    var firstName = order.OrderDeliveryAddress.FirstName.CleanParameter();
                    var lastName = string.IsNullOrEmpty(order.OrderDeliveryAddress.LastName.CleanParameter()) ? firstName : order.OrderDeliveryAddress.LastName.CleanParameter();

                    if (string.IsNullOrEmpty(firstName.Trim()))
                    {
                        response.Success = false;
                        response.Message = "Lütfen adınızı ve soyadınızı boş bırakmayınız.";
                        return;
                    }

                    #region Clean Parameters

                    order.OrderDeliveryAddress.Recipient = order.OrderDeliveryAddress.Recipient.CleanParameter();
                    order.OrderDeliveryAddress.PhoneNumber = order.OrderDeliveryAddress.PhoneNumber.ToNumber();
                    order.OrderDeliveryAddress.Address = order.OrderDeliveryAddress.Address.CleanParameter();
                    order.OrderDeliveryAddress.Mail = order.OrderDeliveryAddress.Mail.CleanParameter();

                    if (order.Differentaddress)
                    {
                        var invoiceFirstName = order.OrderInvoiceInformation.FirstName.CleanParameter();
                        var invoiceFirstLastName = string.IsNullOrEmpty(order.OrderInvoiceInformation.LastName.Trim()) ? invoiceFirstName : order.OrderInvoiceInformation.LastName.CleanParameter();


                        if (string.IsNullOrEmpty(invoiceFirstName.Trim()))
                        {
                            response.Success = false;
                            response.Message = "Lütfen fatura alanındaki adınızı ve soyadınızı boş bırakmayınız.";
                            return;
                        }

                        order.OrderInvoiceInformation.FirstName = invoiceFirstName;
                        order.OrderInvoiceInformation.LastName = invoiceFirstLastName;
                        order.OrderInvoiceInformation.Phone = order.OrderInvoiceInformation.Phone.ToNumber();
                        order.OrderInvoiceInformation.Address = order.OrderInvoiceInformation.Address.CleanParameter();
                        order.OrderInvoiceInformation.NeighborhoodId = order.OrderInvoiceInformation.NeighborhoodId;
                        order.OrderInvoiceInformation.TaxNumber = order.OrderInvoiceInformation.TaxNumber.CleanParameter();
                        order.OrderInvoiceInformation.TaxOffice = order.OrderInvoiceInformation.TaxOffice.CleanParameter();
                        order.OrderInvoiceInformation.Mail = order.OrderInvoiceInformation.Mail.CleanParameter();
                    }
                    else
                    {
                        order.OrderInvoiceInformation.FirstName = firstName;
                        order.OrderInvoiceInformation.LastName = lastName;
                        order.OrderInvoiceInformation.Phone = order.OrderDeliveryAddress.PhoneNumber.ToNumber();
                        order.OrderInvoiceInformation.Address = order.OrderDeliveryAddress.Address.CleanParameter();
                        order.OrderInvoiceInformation.NeighborhoodId = order.OrderDeliveryAddress.NeighborhoodId;
                        order.OrderInvoiceInformation.TaxNumber = null;
                        order.OrderInvoiceInformation.TaxOffice = null;
                        order.OrderInvoiceInformation.Mail = order.OrderDeliveryAddress.Mail.CleanParameter();
                    }

                    #endregion

                    var eCustomer = new Domain.Entities.Customer
                    {
                        Name = order.OrderDeliveryAddress.FirstName,
                        Surname = order.OrderDeliveryAddress.LastName,
                        CustomerContact = new Domain.Entities.CustomerContact
                        {
                            Phone = order.OrderDeliveryAddress.PhoneNumber,
                            Mail = order.OrderDeliveryAddress.Mail,
                            TaxNumber = null,
                            TaxOffice = null,
                            CreatedDate = DateTime.Now
                        },
                    };
                    _unitOfWork.CustomerRepository.Create(eCustomer);
                    customerId = eCustomer.Id;
                }


                #region OrderShipment
                var orderShipments = new List<Domain.Entities.OrderShipment>();

                var shipmentCompanyCompany = _unitOfWork.ShipmentCompanyCompanyRepository.DbSet().FirstOrDefault();

                var shipmentCompanyId = "ARS";

                if (shipmentCompanyCompany != null)
                {
                    shipmentCompanyId = shipmentCompanyCompany.ShipmentCompanyId;
                }

                if (order.OrderDetails.Any(x => x.Payor))
                {
                    orderShipments.Add(new Domain.Entities.OrderShipment
                    {
                        TrackingUrl = "",
                        TrackingCode = "",
                        ShipmentCompanyId = shipmentCompanyId,
                        CreatedDate = DateTime.Now,
                        Payor = true
                    });
                }
                if (order.OrderDetails.Any(x => !x.Payor))
                {
                    orderShipments.Add(new Domain.Entities.OrderShipment
                    {
                        TrackingUrl = "",
                        TrackingCode = "",
                        ShipmentCompanyId = shipmentCompanyId,
                        CreatedDate = DateTime.Now,
                        Payor = false
                    });
                }
                #endregion

                var orderSource = _unitOfWork.OrderSourceRepository.DbSet().FirstOrDefault(x => x.Name == "Web");
                var paymentTypeIds = order.Payments.Select(x => x.PaymentTypeId);

                var paymentTypes = _unitOfWork.PaymentTypeDomainRepository
                .DbSet()
                .Include(x => x.PaymentTypeSetting)
                .Where(x => paymentTypeIds.Contains(x.PaymentTypeId))
                .ToList();

                var surchargeFee = paymentTypes.Sum(x => x.PaymentTypeSetting.Cost);

                order.Total += surchargeFee;


                var eOrder = new Domain.Entities.Order
                {
                    CustomerId = customerId,
                    CurrencyId = this._currency.Id,
                    ApplicationId = "WB",
                    LanguageId = this._language.Id,
                    ListTotal = order.ListTotal,
                    Total = order.Total,
                    ExchangeRate = 1,
                    Discount = order.Discount,
                    VatExcDiscount = order.VatExcDiscount,
                    VatExcListTotal = order.VatExcListTotal,
                    VatExcTotal = order.VatExcTotal,
                    ShoppingCartDiscount = order.ShoppingCartDiscount,
                    CommissionAmount = 0,
                    CargoFee = order.CargoFee,
                    SurchargeFee = surchargeFee,
                    OrderTypeId = "BE",
                    OrderSourceId = orderSource.Id,
                    OrderDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    EstimatedPackingDate = DateTime.Now,
                    OrderDeliveryAddress = new Domain.Entities.OrderDeliveryAddress
                    {
                        Recipient = order.OrderDeliveryAddress.Recipient,
                        CreatedDate = DateTime.Now,
                        Address = order.OrderDeliveryAddress.Address,
                        NeighborhoodId = order.OrderDeliveryAddress.NeighborhoodId,
                        PhoneNumber = order.OrderDeliveryAddress.PhoneNumber
                    },
                    OrderInvoiceInformation = new Domain.Entities.OrderInvoiceInformation
                    {
                        CreatedDate = DateTime.Now,
                        NeighborhoodId = order.OrderInvoiceInformation.NeighborhoodId,
                        Address = order.OrderInvoiceInformation.Address,
                        Mail = order.OrderInvoiceInformation.Mail,
                        Phone = order.OrderInvoiceInformation.Phone,
                        TaxNumber = order.OrderInvoiceInformation.TaxNumber,
                        TaxOffice = order.OrderInvoiceInformation.TaxOffice,
                        FirstName = order.OrderInvoiceInformation.FirstName,
                        LastName = order.OrderInvoiceInformation.LastName
                    },
                    OrderNotes = new List<Domain.Entities.OrderNote>
                    {
                        new Domain.Entities.OrderNote
                        {
                            Note=order.OrderNote.Note,
                            CreatedDate=DateTime.Now
                        }
                    },
                    OrderShipments = orderShipments,
                    OrderTechnicInformation = new Domain.Entities.OrderTechnicInformation
                    {
                        Browser = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString(),
                        IpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                        Platform = "",
                    },
                    OrderDetails = order.OrderDetails.Select(x => new Domain.Entities.OrderDetail
                    {
                        ListUnitPrice = x.ListUnitPrice,
                        ProductInformationId = x.ProductInformationId,
                        Quantity = x.Quantity,
                        UnitCost = x.UnitCost,
                        UnitPrice = x.UnitPrice,
                        VatRate = x.VatRate,
                        Payor = x.Payor,
                        UnitCargoFee = 0,
                        UnitDiscount = x.UnitDiscount,
                        UnitCommissionAmount = 0,
                        VatExcListUnitPrice = x.VatExcListUnitPrice,
                        VatExcUnitCost = x.VatExcUnitCost,
                        VatExcUnitDiscount = x.VatExcUnitDiscount,
                        VatExcUnitPrice = x.VatExcUnitPrice,
                        CreatedDate = DateTime.Now
                    }).ToList(),
                    MarketplaceId = null,
                    Application = null,
                    Currency = null,
                    Customer = null,
                    Language = null,
                    Marketplace = null
                };

                #region Payment
                /*
                          * Eğer sipariş içerisinde para puan kullanılmış ise kontroller sağlanarak ödenecek tutardan düşülür.
                       */
                var moneyPointAmount = 0M;
                var moneyPoint = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "MP");
                if (moneyPoint != null)
                {


                    moneyPointAmount = moneyPoint.Amount <= order.Customer.CustomerUser.MoneyPointAmount
                    ? moneyPoint.Amount
                    : order.Customer.CustomerUser.MoneyPointAmount;

                    if (moneyPointAmount <= 0)
                    {
                        moneyPointAmount = 0;
                        order.Payments.Remove(moneyPoint);
                    }
                }
                /*
                    * Eğer sipariş içerisinde indirim kuponu kullanılmış ise kontroller sağlanarak ödenecek tutardan düşülür.
                */
                var discountCouponAmount = 0M;
                var discountCoupon = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "IK");
                if (discountCoupon != null)
                    discountCouponAmount = discountCoupon.Amount;

                eOrder.Payments = order
                    .Payments
                    .Select(p =>
                    {
                        var paid = true;
                        var amount = p.Amount;
                        if (p.PaymentTypeId != "MP" && p.PaymentTypeId != "IK")
                        {
                            //Kapıda ödeme maliyeti
                            var cost = paymentTypes.FirstOrDefault(x => x.PaymentTypeId == p.PaymentTypeId).PaymentTypeSetting.Cost;

                            amount = order.Total  - (moneyPointAmount + discountCouponAmount);
                            paid = false;
                        }


                        return new Domain.Entities.Payment
                        {
                            PaymentTypeId = p.PaymentTypeId,
                            Amount = amount,
                            CreatedDate = DateTime.Now,
                            DiscountCouponId = p.DiscountCouponId,
                            Paid = paid
                        };
                    })
                    .ToList();
                #endregion

                _unitOfWork.OrderRepository.Create(eOrder);

                #region Money Point
                /*
                     * Siparişi veren müşteri üye ise para puana hak kazanır.
                 */
                if (order.Customer?.CustomerUser != null)
                {
                    var moneyPointSetting = _unitOfWork.MoneyPointSettingRepository.DbSet().FirstOrDefault();
                    if (moneyPointSetting != null)
                    {
                        if (eOrder.Total >= moneyPointSetting.Limit)
                        {
                            var earnigAmount = moneyPointSetting.EarningIsRate
                                ? eOrder.Total * moneyPointSetting.Earning
                                : moneyPointSetting.Earning;

                            _unitOfWork.MoneyPointRepository.Create(new Domain.Entities.MoneyPoint
                            {
                                Id = eOrder.Id,
                                OrderId = eOrder.Id,
                                CustomerUserId = order.Customer.CustomerUser.Id,
                                CreatedDate = DateTime.Now,
                                Amount = earnigAmount,
                                Approved = false
                            });
                        }
                    }
                }


                /*
                     * Eğer para puan kullanımı var ise müşteri hesabından kullanılan tutar düşülür.
                 */
                if (moneyPointAmount > 0)
                {
                    var customerUser = this
                        ._unitOfWork
                        .CustomerUserRepository
                        .Read(order.Customer.CustomerUser.Id);
                    customerUser.MoneyPointAmount -= moneyPointAmount;

                    this._unitOfWork.CustomerUserRepository.Update(customerUser);
                }
                #endregion

                var onlinePayment = order.Payments.FirstOrDefault(x => x.PaymentTypeId == "OO");

                if (onlinePayment != null)
                {
                    var neighborhood = _unitOfWork.NeighborhoodRepository.DbSet().Include(x => x.District).ThenInclude(x => x.City).FirstOrDefault(x => x.Id == order.OrderDeliveryAddress.NeighborhoodId);

                    var onlinePaymentAmount = eOrder.Payments.FirstOrDefault(x => x.PaymentTypeId == "OO").Amount;
#if DEBUG
                    onlinePaymentAmount = 1;
#endif

                    var posServiceResult = this
                        ._posService
                        .Pay3d(new Application.Common.DataTransferObjects.CreditCardPayment
                        {
                            Amount = onlinePaymentAmount,
                            CustomerAddress = order.OrderDeliveryAddress.Address,
                            CustomerCityName = neighborhood.District.City.Name,
                            CustomerCountryName = "Türkiye",
                            CustomerEmail = order.OrderDeliveryAddress.Mail,
                            Number = onlinePayment.CreditCard.Number.ToNumber(),
                            Cvv = onlinePayment.CreditCard.Cvv,
                            ExpireMonth = onlinePayment.CreditCard.ExpireMonth,
                            ExpireYear = onlinePayment.CreditCard.ExpireYear,
                            OrderId = eOrder.Id,
                            PaymentId = eOrder.Payments.FirstOrDefault(x => x.PaymentTypeId == "OO").Id,
                            CustomerId = eOrder.CustomerId,
                            FirstName = eOrder.OrderInvoiceInformation.FirstName.Trim(),
                            LastName = eOrder.OrderInvoiceInformation.LastName.Trim(),
                            Phone = order.OrderDeliveryAddress.PhoneNumber
                        });



                    if (!posServiceResult.Success)
                    {
                        response.Success = posServiceResult.Success;
                        response.Message = posServiceResult.Message;
                        return;
                    }

                    response.IsOnlinePayment = true;
                    response.IsThreeD = true;
                    response.ThreeDRedirect = posServiceResult.Data.ThreeDRedirect;
                    response.ThreeDResponse = posServiceResult.Data.Content;
                    response.AllowFrame = posServiceResult.Data.AllowFrame;
                }
                else //Eğer ödeme kredi kartı ile ödeme değilse stokdan düşülür
                {
                    if (this._settingService.StockControl)
                        eOrder.OrderDetails.ForEach(odLoop => this._productInformationStockService.DecreaseStock(odLoop.ProductInformationId, odLoop.Quantity));

                }



                var payment = eOrder.Payments.FirstOrDefault(x => x.PaymentTypeId != "IK" && x.PaymentTypeId != "MP");

                /*
                 Son Sipariş Verileri Cookie atılıyor Order done ekranında 1 kez gösterilmek üzere sonrasında siliniyor
                 */
                this.Add(new LastOrderItem
                {
                    Id = eOrder.Id,
                    Amount = decimal.Round(payment.Amount, 2),
                    PaymentTypeId = payment.PaymentTypeId,
                    CustomerName = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}",
                    PhoneNumber = order.OrderDeliveryAddress.PhoneNumber,
                    Mail = order.OrderDeliveryAddress.Mail
                });

                response.Success = true;
            }, (response, exception) =>
                    {
                        response.Success = false;
                        response.Message = "Ödeme alınırken bir hata ile karşılaşışdı lütfen daha sonra deneyin.";
                    });
        }

        public DataResponse<List<ECommerce.Application.Client.DataTransferObjects.Order>> ReadByCustomerId(int customerId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ECommerce.Application.Client.DataTransferObjects.Order>>>((response) =>
            {
                var entities = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.OrderDeliveryAddress)
                    .Include(o => o.OrderDetails)
                    .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderType.OrderTypeTranslations)
                    .Where(i => i.CustomerId == customerId)
                    .ToList();

                if (entities == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                }

                response.Data = entities.Select(o => new ECommerce.Application.Client.DataTransferObjects.Order
                {
                    Id = o.Id,
                    CreatedDate = o.CreatedDate,
                    Total = o.Total,
                    OrderDeliveryAddress = new ECommerce.Application.Client.DataTransferObjects.OrderDeliveryAddress
                    {
                        Recipient = o.OrderDeliveryAddress.Recipient
                    },
                    OrderType = new OrderType
                    {
                        Name = o.OrderType.OrderTypeTranslations.FirstOrDefault(ott => ott.LanguageId == "TR").Name,
                        OrderTypeTranslation = new OrderTypeTranslation
                        {
                            Name = o.OrderType.OrderTypeTranslations.FirstOrDefault(x => x.LanguageId == "TR").Name
                        }
                    },
                    OrderDetails = o.OrderDetails.Select(od => new OrderDetail
                    {
                        Quantity = od.Quantity,
                        ProductInformation = new ProductInformation
                        {
                            Name = od.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name
                        }
                    }).ToList()
                }).ToList();

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Siparişler başarılı bir şekilde okunamadı.";
            });
        }

        public DataResponse<LastOrderItem> GetLastOrder()
        {
            return ExceptionHandler.ResultHandle<DataResponse<LastOrderItem>>((response) =>
            {
                var orderCookie = this._cookieHelper.Read(_configuration.OrderCookie);

                if (orderCookie == null || orderCookie.Count == 0)
                {
                    response.Success = false;
                    response.Message = "Son sipariş getirilemedi";
                    return;
                }

                response.Data = JsonConvert.DeserializeObject<LastOrderItem>(orderCookie.FirstOrDefault().Value);
                response.Success = true;
            });
        }

        public Response Clear()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Delete(_configuration.OrderCookie);
                response.Success = true;
            }, (response, exception) =>
            {
            });
        }

        private Response Add(LastOrderItem lastOrderItem)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                this._cookieHelper.Write(_configuration.OrderCookie, new List<ECommerce.Application.Common.DataTransferObjects.KeyValue<string, string>> { new ECommerce.Application.Common.DataTransferObjects.KeyValue<string, string>
                {
                    Key = "Order",
                    Value = JsonConvert.SerializeObject(lastOrderItem)
                } });

                response.Success = true;

            }, (response, exception) =>
            {
            });
        }
        #endregion
    }
}
