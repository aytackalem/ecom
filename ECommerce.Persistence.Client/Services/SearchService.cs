﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class SearchService : ISearchService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly ISettingService _settingService;

        private readonly IConfiguration _configuration;

        protected readonly IDomainFinder _domainFinder;

        private readonly IDbNameFinder _dbNameFinder;
        #endregion

        #region Constructors
        public SearchService(IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ISettingService settingService, IConfiguration configuration, IDomainFinder domainFinder, IDbNameFinder dbNameFinder)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._settingService = settingService;
            this._configuration = configuration;
            this._domainFinder = domainFinder;
            this._dbNameFinder = dbNameFinder;

            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<Search>> ReadInfo(string keyword)
        {
            SqlConnection sqlConnection = null;


            return ExceptionHandler.ResultHandle<DataResponse<List<Search>>>((response) =>
            {
               
                var models = new List<Search>();
                using (sqlConnection = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    var query = $@"
SELECT 
Top 5 
			NAME + ' '+ + ISNULL(P.VariantValuesDescription,''),
			Url,
			'Product' AS TYPE,
			ISNULL('{_settingService.ProductImageUrl}/'+PP.FileName,'') AS PhotoUrl
FROM ProductInformationTranslations P
LEFT JOIN ProductInformationPhotos PP ON PP.Id = (SELECT TOP 1 Id FROM ProductInformationPhotos WHERE ProductInformationId=P.ProductInformationId) 
INNER JOIN ProductInformations PIN ON PIN.Id=P.ProductInformationId
INNER JOIN Products PT ON PT.Id=PIN.ProductId
Where [Name] Like   N'%' + @SEARCH + '%' COLLATE Turkish_CI_AS  AND PT.Active=1 And PIN.DomainId = @DomainId

UNION ALL

SELECT		Top 5 
			Name,
			Url,
			'Category' AS TYPE,
			'/themes/{_settingService.Theme}/imgs/theme/kategori-search.png' as PhotoUrl
FROM		CategoryTranslations CT
INNER JOIN Categories C ON C.Id=CT.CategoryId
WHERE		[Name] Like  N'%' + @SEARCH + '%' COLLATE Turkish_CI_AS AND C.Active=1 and C.DomainId = @DomainId

UNION ALL


SELECT		Top 5 
			Name,
			Url,
			'Group' AS TYPE,
			'/themes/{_settingService.Theme}/imgs/theme/grup-search.png' as PhotoUrl
FROM		GroupTranslations GT
INNER JOIN Groups G ON GT.GroupId=G.Id
WHERE		[Name] Like  N'%' + @SEARCH + '%' COLLATE Turkish_CI_AS AND G.Active=1 and G.DomainId = @DomainId";

                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Search", keyword);
                        sqlCommand.Parameters.AddWithValue("@DomainId", _domainFinder.FindId());

                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            models.Add(new Search
                            {
                                Name = sqlDataReader.GetString(0),
                                Url = sqlDataReader.GetString(1),
                                Type = sqlDataReader.GetString(2),
                                PhotoUrl = sqlDataReader.GetString(3)
                            });

                        }
                    }
                }

                sqlConnection.Close();

                if (models?.Count == 0)
                {
                    response.Success = true;
                    response.Message = "Sonuç bulunamadı.";
                }
                else
                {
                    response.Data = models;
                    response.Success = true;
                    response.Message = "Sonuç başarılı bir şekilde getirildi.";
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();

                response.Success = false;
                response.Message = "Sonuç başarılı bir şekilde getirilemedi.";
            });
        }
        #endregion
    }
}
