﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CustomerInvoiceInformationService : ICustomerInvoiceInformationService
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CustomerInvoiceInformationService(IUnitOfWork unitOfWork)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<List<CustomerInvoiceInformation>> Read(int customerUserId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CustomerInvoiceInformation>>>((response) =>
            {
                var CustomerInvoiceInformationes = this
                    ._unitOfWork
                    .CustomerInvoiceInformationRepository
                    .DbSet()
                    .Where(c => c.CustomerId == customerUserId && !c.Deleted)
                    .OrderBy(x => x.Id);


                response.Data = CustomerInvoiceInformationes.Select(cad => new CustomerInvoiceInformation
                {
                    Id = cad.Id,
                    Address = cad.Address,
                    NeighborhoodId = cad.NeighborhoodId,
                    Title = cad.Title,
                    Phone = cad.Phone,
                    Default = cad.Default,
                    FirstName = cad.FirstName,
                    LastName = cad.LastName,
                    Mail = cad.Mail,
                    TaxNumber = cad.TaxNumber,
                    TaxOffice = cad.TaxOffice
                }).ToList();

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public DataResponse<CustomerInvoiceInformation> Read(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerInvoiceInformation>>((response) =>
            {
                var customerInvoiceInformation = this
                    ._unitOfWork
                    .CustomerInvoiceInformationRepository
                    .DbSet()
                    .Include(x => x.Neighborhood.District.City)
                    .FirstOrDefault(c => c.CustomerId == customerUserId && c.Id == id && !c.Deleted);

                if (customerInvoiceInformation == null)
                {

                    response.Message = "Size ait böyle bir fatura adresi bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }


                response.Data = new CustomerInvoiceInformation
                {
                    Id = customerInvoiceInformation.Id,
                    Address = customerInvoiceInformation.Address,
                    NeighborhoodId = customerInvoiceInformation.NeighborhoodId,
                    Title = customerInvoiceInformation.Title,
                    Phone = customerInvoiceInformation.Phone,
                    FirstName = customerInvoiceInformation.FirstName,
                    LastName = customerInvoiceInformation.LastName,
                    Default = customerInvoiceInformation.Default,
                    Mail = customerInvoiceInformation.Mail,
                    TaxOffice = customerInvoiceInformation.TaxOffice,
                    TaxNumber = customerInvoiceInformation.TaxNumber,
                    Neighborhood = new Neighborhood
                    {
                        Id = customerInvoiceInformation.Neighborhood.Id,
                        District = new District
                        {
                            Id = customerInvoiceInformation.Neighborhood.DistrictId,
                            City = new City
                            {
                                Id = customerInvoiceInformation.Neighborhood.District.CityId,
                                Country = new Country
                                {
                                    Id = customerInvoiceInformation.Neighborhood.District.City.CountryId
                                }
                            }
                        }
                    }
                };

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Delete(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var customerInvoiceInformation = this
                    ._unitOfWork
                    .CustomerInvoiceInformationRepository
                    .DbSet()
                    .FirstOrDefault(c => c.CustomerId == customerUserId && c.Id == id && !c.Deleted);

                if (customerInvoiceInformation == null)
                {

                    response.Message = "Size ait böyle bir fatura adresi bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }


                customerInvoiceInformation.Deleted = true;
                customerInvoiceInformation.DeletedDateTime = DateTime.Now;


                var updated = _unitOfWork
                     .CustomerInvoiceInformationRepository.Update(customerInvoiceInformation);
                if (!updated)
                {
                    response.Message = "Adres silinemedi lütfen daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }

                response.Message = "Adresiniz silinmiştir.";
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Default(int id, int customerUserId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var customerInvoiceInformation = this
                    ._unitOfWork
                    .CustomerInvoiceInformationRepository
                    .DbSet()
                    .Where(c => c.CustomerId == customerUserId && !c.Deleted).ToList();

                if (!customerInvoiceInformation.Any(x => x.Id == id))
                {

                    response.Message = "Size ait böyle bir fatura adresi bulunamadı lütfen daha sonra tekrar deneyiniz.";
                    response.Success = false;
                    return;
                }

                customerInvoiceInformation.ForEach(c =>
                {
                    c.Default = false;
                    if (c.Id == id)
                    {
                        c.Default = true;
                    }
                });

                var updated = _unitOfWork
                     .CustomerInvoiceInformationRepository.Update(customerInvoiceInformation);

                if (!updated)
                {
                    response.Message = "Adres seçilemedi daha sonra tekrar deneyin.";
                    response.Success = false;
                    return;
                }

                response.Message = "Adresiniz güncellenmiştir.";
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }

        public Response Upsert(CustomerInvoiceInformation dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (dto.Id > 0)
                {
                    var customerInvoiceInformation = this
                    ._unitOfWork
                    .CustomerInvoiceInformationRepository
                    .DbSet()
                    .FirstOrDefault(c => c.CustomerId == dto.CustomerId && c.Id == dto.Id && !c.Deleted);

                    if (customerInvoiceInformation == null)
                    {

                        response.Message = "Size ait böyle bir fatura adresi bulunamadı lütfen daha sonra tekrar deneyiniz.";
                        response.Success = false;
                        return;
                    }


                    customerInvoiceInformation.NeighborhoodId = dto.NeighborhoodId;
                    customerInvoiceInformation.Title = dto.Title.CleanParameter();
                    customerInvoiceInformation.Phone = dto.Phone.ToNumber();
                    customerInvoiceInformation.FirstName = dto.FirstName.CleanParameter();
                    customerInvoiceInformation.LastName = dto.LastName.CleanParameter();
                    customerInvoiceInformation.Mail = dto.Mail.CleanParameter();
                    customerInvoiceInformation.Address = dto.Address.CleanParameter();
                    customerInvoiceInformation.TaxNumber = dto.TaxNumber.ToNumber();
                    customerInvoiceInformation.TaxOffice = dto.TaxOffice.CleanParameter();

                    var updated = _unitOfWork
                         .CustomerInvoiceInformationRepository.Update(customerInvoiceInformation);
                    if (!updated)
                    {
                        response.Message = "Adres güncellenemedi lütfen daha sonra tekrar deneyin.";
                        response.Success = false;
                        return;
                    }
                }
                else
                {
                    var customerInvoiceInformationAny = this
                            ._unitOfWork
                            .CustomerInvoiceInformationRepository
                            .DbSet()
                            .Any(c => c.CustomerId == dto.CustomerId && !c.Deleted);

                    var created = _unitOfWork
                      .CustomerInvoiceInformationRepository
                      .Create(new Domain.Entities.CustomerInvoiceInformation
                      {
                          Address = dto.Address.CleanParameter(),
                          CreatedDate = DateTime.Now,
                          CustomerId = dto.CustomerId,
                          NeighborhoodId = dto.NeighborhoodId,
                          Phone = dto.Phone.ToNumber(),
                          Mail = dto.Mail.CleanParameter(),
                          FirstName = dto.FirstName.CleanParameter(),
                          LastName = dto.LastName.CleanParameter(),
                          Title = dto.Title.CleanParameter(),
                          TaxOffice = dto.TaxOffice.CleanParameter(),
                          TaxNumber = dto.TaxNumber.ToNumber(),
                          Default = !customerInvoiceInformationAny
                      });

                    if (!created)
                    {
                        response.Message = "Adres kaydedilemedi lütfen daha sonra tekrar deneyin.";
                        response.Success = false;
                        return;
                    }
                }

                response.Success = true;
                response.Message = "Adresiniz güncellendi.";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri adresi bulunamadı.";
            });
        }
        #endregion
    }
}
