﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class InformationService : IInformationService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;
        #endregion

        #region Constructors
        public InformationService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Information> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Information>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .InformationRepository
                    .DbSet()
                    .Include(i => i.InformationTranslations)
                    .ThenInclude(it => it.InformationTranslationContent)
                    .Include(i => i.InformationTranslations)
                    .ThenInclude(it => it.InformationSeoTranslation)
                    .FirstOrDefault(i => i.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Haber bulunamadı.";
                }
                else
                {
                    var entityTranslation = entity
                                            .InformationTranslations
                                            .FirstOrDefault(ct => ct.LanguageId == this._language.Id);

                    if (entityTranslation == null)
                    {
                        response.Success = false;
                        response.Message = "Haber bilgileri eksik.";
                    }
                    else
                    {
                        response.Data = new Information
                        {
                            Id = entity.Id,
                            Header = entityTranslation.Header,
                            Url = entityTranslation.Url,
                            InformationContent = new InformationContent
                            {
                                Html = entityTranslation.InformationTranslationContent.Html
                            },
                            InformationSeo = new InformationSeo
                            {
                                Title = entityTranslation.InformationSeoTranslation.Title,
                                MetaKeywords = entityTranslation.InformationSeoTranslation.MetaKeywords,
                                MetaDescription = entityTranslation.InformationSeoTranslation.MetaDescription
                            }
                        };
                        response.Success = true;
                        response.Message = "Haberler başarılı bir şekilde getirildi.";
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Haberler bulunamadı.";
            });
        }

        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
                {
                    response.Data = this
                        ._unitOfWork
                        .InformationRepository
                        .DbSet()
                        .Include(i => i.InformationTranslations)
                        .Take(5)
                        .OrderByDescending(x => x.Id)
                        .Select(i => new KeyValue<string, string>
                        {
                            Key = i.InformationTranslations.FirstOrDefault(it => it.LanguageId == this._language.Id).Header,
                            Value = i.InformationTranslations.FirstOrDefault(it => it.LanguageId == this._language.Id).Url
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "Haberler başarılı bir şekilde getirildi.";
                }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Haberler bulunamadı.";
                });
            }, CacheKey.Informations);
        }
        #endregion
    }
}
