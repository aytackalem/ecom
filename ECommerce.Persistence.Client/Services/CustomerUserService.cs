﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class CustomerUserService : ICustomerUserService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISmsService _smsService;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public CustomerUserService(IUnitOfWork unitOfWork, ISmsService smsService, ICacheHandler cacheHandler)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._smsService = smsService;
            this._cacheHandler = cacheHandler;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<CustomerUser> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerUser>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .DbSet()
                    .Include(mu => mu.CustomerRole)
                    .Include(x => x.Customer.CustomerAddresses.Where(x => !x.Deleted).OrderBy(x => x.Id))
                    .ThenInclude(x => x.Neighborhood.District.City.Country)
                    .Include(x => x.Customer.CustomerInvoiceInformations.Where(x => !x.Deleted).OrderBy(x => x.Id))
                    .ThenInclude(x => x.Neighborhood.District.City.Country)
                    .Include(x => x.Customer.CustomerContact)
                    .FirstOrDefault(mu => mu.Id == id);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "kullanıcı bulunamadı.";
                    return;
                }

                response.Data = new CustomerUser
                {
                    Id = entity.Id,
                    ManagerRoleId = entity.CustomerRoleId,
                    CustomerRole = new CustomerRole
                    {
                        Name = entity.CustomerRole.Name
                    },
                    Customer = new Customer
                    {
                        Name = entity.Customer.Name,
                        Surname = entity.Customer.Surname,
                        CustomerContact = new CustomerContact
                        {
                            Mail = entity.Customer.CustomerContact.Mail,
                            Phone = entity.Customer.CustomerContact.Phone,
                            TaxNumber = entity.Customer.CustomerContact.TaxNumber,
                            TaxOffice = entity.Customer.CustomerContact.TaxOffice
                        },
                        CustomerAddresses = entity.Customer.CustomerAddresses.Select(x => new CustomerAddress
                        {
                            Id = x.Id,
                            Title = x.Title,
                            Address = x.Address,
                            Neighborhood = new Neighborhood
                            {
                                Id = x.Id,
                                Name = x.Neighborhood.Name,
                                District = new District
                                {
                                    Id = x.Neighborhood.DistrictId,
                                    Name = x.Neighborhood.District.Name,
                                    City = new City
                                    {
                                        Id = x.Neighborhood.District.CityId,
                                        Name = x.Neighborhood.District.City.Name,
                                        Country = new Country
                                        {
                                            Id = x.Neighborhood.District.City.CountryId,
                                            Name = x.Neighborhood.District.City.Country.Name
                                        }
                                    }
                                }
                            },
                            NeighborhoodId = x.NeighborhoodId,
                            Recipient = x.Recipient,
                            Default = x.Default
                        }).ToList(),
                        CustomerInvoiceInformations = entity.Customer.CustomerInvoiceInformations.Select(x => new CustomerInvoiceInformation
                        {
                            Id = x.Id,
                            Title = x.Title,
                            Address = x.Address,
                            Neighborhood = new Neighborhood
                            {
                                Id = x.Id,
                                Name = x.Neighborhood.Name,
                                District = new District
                                {
                                    Id = x.Neighborhood.DistrictId,
                                    Name = x.Neighborhood.District.Name,
                                    City = new City
                                    {
                                        Id = x.Neighborhood.District.CityId,
                                        Name = x.Neighborhood.District.City.Name,
                                        Country = new Country
                                        {
                                            Id = x.Neighborhood.District.City.CountryId,
                                            Name = x.Neighborhood.District.City.Country.Name
                                        }
                                    }
                                }
                            },
                            NeighborhoodId = x.NeighborhoodId,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            Phone = x.Phone,
                            TaxNumber = x.TaxNumber,
                            TaxOffice = x.TaxOffice,
                            Mail = x.Mail,
                            Default = x.Default
                        }).ToList()
                    },
                    MoneyPointAmount = entity.MoneyPointAmount
                };

                response.Success = true;
            }, (response, exception) => { });
        }

        public DataResponse<CustomerUser> Read(CustomerUser customerUser, bool cache = true)
        {
            var cacheKey = string.Format(CacheKey.CustomerUser, customerUser.Username, customerUser.Password);
            if (!cache)
            {
                return this.ReadByCustomerUser(customerUser);
            }

            return this._cacheHandler.ResponseHandle(() =>
            {
                return this.ReadByCustomerUser(customerUser);
            }, cacheKey, 10);


        }

        public DataResponse<CustomerUserRegister> Create(CustomerUser dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerUserRegister>>((response) =>
            {
                response.Data = new CustomerUserRegister();
                var _ecustromerUser = this._unitOfWork
                     .CustomerUserRepository
                     .DbSet()
                     .Include(x => x.Customer.CustomerContact)
                     .FirstOrDefault(x => x.Username.ToLower() == dto.Username.ToLower());

                var smsProvider = this._unitOfWork
                 .SmsProviderCompanyRepository
                 .DbSet()
                 .FirstOrDefault();

                var activeSms = smsProvider != null;

                if (_ecustromerUser != null)
                {

                    if (_ecustromerUser.Verified)
                    {

                        response.Message = "Kullanıcı sistemde kayıtlı lütfen başka bir kullanıcı adı giriniz!";
                        response.Success = false;
                        return;
                    }

                    dto.Id = _ecustromerUser.Id;
                    _ecustromerUser.Password = dto.Password;
                    _ecustromerUser.Customer.Name = dto.Customer.Name;
                    _ecustromerUser.Customer.Surname = dto.Customer.Surname;
                    _ecustromerUser.Customer.CustomerContact.Phone = dto.Customer.CustomerContact.Phone.ToNumber();
                    _ecustromerUser.Customer.CustomerContact.Mail = dto.Customer.CustomerContact.Mail;

                    this
                    ._unitOfWork
                    .CustomerUserRepository
                    .Update(_ecustromerUser);
                }
                else
                {
                    ///Eğer sms aktif değilse doğrulama yapılır

                    var customerUser = new Domain.Entities.CustomerUser
                    {
                        CustomerRoleId = "DC",
                        Username = dto.Username,
                        Password = dto.Password,
                        CreatedDate = DateTime.Now,
                        Customer = new Domain.Entities.Customer
                        {
                            CreatedDate = DateTime.Now,
                            Name = dto.Customer.Name,
                            Surname = dto.Customer.Surname,
                            CustomerContact = new Domain.Entities.CustomerContact
                            {
                                Phone = dto.Customer.CustomerContact.Phone.ToNumber(),
                                Mail = dto.Customer.CustomerContact.Mail,
                                CreatedDate = DateTime.Now
                            }
                        },
                        MoneyPointAmount = 0M,
                        Verified = activeSms == false
                    };

                    this
                    ._unitOfWork
                    .CustomerUserRepository
                    .Create(customerUser);

                    dto.Id = customerUser.Id;
                    response.Data.Verified = activeSms == false;
                }



                if (activeSms)
                {
                    var phone = dto.Customer.CustomerContact.Phone.ToNumber();

                    var customerTwoFactorCount = TwoFactorCount(phone);
                    if (!customerTwoFactorCount.Success)
                    {

                        response.Success = false;
                        response.Message = customerTwoFactorCount.Message;
                        return;
                    }

                    if (customerTwoFactorCount.Data >= 3)
                    {
                        response.Success = false;
                        response.Message = "Sms gönderimi bu telefon numarası için geçici süreliğine bloke olmuştur. Lütfen daha sonra tekrar deneyiniz.";
                        return;
                    }

                    var random = new Random();
                    var verificationCode = random.Next(1234, 567898).ToString("D6");

                    var twoFactorCreate = TwoFactorCreate(new TwoFactorCustomerUser
                    {
                        Code = verificationCode,
                        CustomerUserId = dto.Id,
                        TryCount = 0,
                        Phone = phone
                    });

                    if (!twoFactorCreate.Success)
                    {
                        response.Success = twoFactorCreate.Success;
                        response.Message = twoFactorCreate.Message;
                        return;
                    }

                    this._smsService.Send(phone, $@"Sayın {dto.Customer.Name} {dto.Customer.Surname}, Tek kullanımlık SMS Şifreniz {verificationCode}. Lütfen şifrenizi kimse ile paylaşmayınız.");

                    response.Message = "Telefon numaranıza sms gönderilmiştir.";
                    response.Data.SmsModal = true;
                }

                response.Success = true;
            });
        }

        public DataResponse<TwoFactorCustomerUser> UserVerification(CustomerUserVerification dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<TwoFactorCustomerUser>>((response) =>
            {

                var _ecustromerUser = this._unitOfWork
                     .CustomerUserRepository
                     .DbSet()
                     .Include(x => x.Customer.CustomerContact)
                     .FirstOrDefault(x => x.Username.ToLower() == dto.Username.ToLower());

                if (_ecustromerUser.Verified)
                {
                    response.Message = "Kullanıcı sistemde kayıtlı lütfen başka bir kullanıcı adı giriniz!";
                    response.Success = false;
                    return;
                }


                var result = new TwoFactorCustomerUser();
                result.CustomerUserId = _ecustromerUser.Id;

                if (!string.IsNullOrEmpty(dto.SmsCode))
                {
                    var twoFactorCustomerUser = ReadTwoFactor(_ecustromerUser.Id);
                    if (twoFactorCustomerUser.Success)
                    {
                        var tryCount = twoFactorCustomerUser.Data.TryCount + 1;


                        var update = TwoFactorUpdate(twoFactorCustomerUser.Data.Id, tryCount);
                        if (!update.Success)
                        {
                            response.Success = update.Success;
                            response.Message = update.Message;
                            return;
                        }
                        if (tryCount > 3)
                        {
                            response.Success = false;
                            response.Message = "Sms doğrulama hakkınız dolmuştur. Doğrulama işlemi için tekrar sms gönderiniz.";
                            return;
                        }
                        else if (twoFactorCustomerUser.Data.Code != dto.SmsCode)
                        {

                            if (tryCount == 3)
                            {
                                response.Success = false;
                                response.Message = "Sms doğrulama hakkınız dolmuştur. Doğrulama işlemi için tekrar sms gönderiniz.";
                                return;
                            }

                            response.Success = false;
                            response.Message = "Telefonunuza gelen doğrulama kodunu yanlış girdiniz, Lütfen kontrol ediniz.";
                            return;
                        }


                        result.SmsVerified = true;
                        _ecustromerUser.Verified = true;
                        this
                           ._unitOfWork
                           .CustomerUserRepository
                           .Update(_ecustromerUser);
                        response.Data = result;
                        response.Message = "Kullanıcı başarılı bir şekilde oluşturuldu.";
                        response.Success = true;
                        return;

                    }

                    response.Success = twoFactorCustomerUser.Success;
                    response.Message = twoFactorCustomerUser.Message;
                    return;

                }

                var customerTwoFactorCount = TwoFactorCount(_ecustromerUser.Customer.CustomerContact.Phone);
                if (!customerTwoFactorCount.Success)
                {
                    response.Success = customerTwoFactorCount.Success;
                    response.Message = customerTwoFactorCount.Message;
                    return;
                }

                if (customerTwoFactorCount.Data >= 3)
                {
                    result.SmsModal = false;
                    response.Data = result;
                    response.Success = true;
                    response.Message = "Sms gönderimi bu telefon numarası için geçici süreliğine bloke olmuştur. Lütfen daha sonra tekrar deneyiniz.";
                    return;
                }


                var random = new Random();
                var verificationCode = random.Next(1234, 567898).ToString("D6");
                result.Phone = _ecustromerUser.Customer.CustomerContact.Phone;
                result.Code = verificationCode;
                var twoFactorCreate = TwoFactorCreate(result);
                if (!twoFactorCreate.Success)
                {
                    response.Success = twoFactorCreate.Success;
                    response.Message = twoFactorCreate.Message;
                    return;
                }

                //this._smsService.Send(result.Phone, $@"Sayın {_ecustromerUser.Customer.Name} {_ecustromerUser.Customer.Surname}, Tek kullanımlık SMS Şifreniz {verificationCode}. Lütfen şifrenizi kimse ile paylaşmayınız.");

                result.SmsModal = true;
                response.Success = true;
                response.Message = "Telefon numaranıza sms gönderilmiştir.";
                response.Data = result;

            });
        }

        public DataResponse<CustomerUser> CreateRecovery(string username)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerUser>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .DbSet()
                    .Include(c => c.Customer.CustomerContact)
                    .FirstOrDefault(mu => mu.Username == username);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kullanıcı bulunamadı.";
                    return;
                }

                if (string.IsNullOrEmpty(entity.Customer.CustomerContact.Mail))
                {
                    response.Success = false;
                    response.Message = "Kullanıcının mail adresi bulunamadı. Lütfen müşteri hizmetleri ile görüşün.";
                    return;
                }

                entity.Token = Guid.NewGuid();
                entity.TokenExpried = DateTime.Now.AddMinutes(15);

                response.Success = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .Update(entity);
                response.Data = new CustomerUser
                {
                    Id = entity.Id,
                    Token = entity.Token.ToString(),
                    Customer = new Customer
                    {
                        Name = entity.Customer.Name,
                        Surname = entity.Customer.Surname,
                        CustomerContact = new CustomerContact
                        {
                            Mail = entity.Customer.CustomerContact.Mail,
                            Phone = entity.Customer.CustomerContact.Phone
                        }
                    }
                };
                response.Message = "E-posta adresine sıfırlama iletisi gönderildi.";
            });
        }

        public DataResponse<CustomerUser> UpdatePassword(CustomerUser dto)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerUser>>((response) =>
            {
                if (string.IsNullOrEmpty(dto.NewPassword))
                {
                    response.Success = false;
                    response.Message = "Şifre boş gönderilemez.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .DbSet()
                    .FirstOrDefault(mu => mu.Id == dto.Id);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kullanıcı bulunamadı.";
                    return;
                }


                entity.Password = dto.NewPassword;

                response.Success = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .Update(entity);

                response.Data = new CustomerUser
                {
                    Username = entity.Username,
                    Password = entity.Password
                };
                response.Success = true;
                response.Message = "Yeni şifre başarılı bir şekilde oluşturuldu.";
            });
        }

        public Response ResetPassword(CustomerUser dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (string.IsNullOrEmpty(dto.Password))
                {
                    response.Success = false;
                    response.Message = "Parolanızı lütfen boş girmeyiniz";
                    return;
                }

                if (string.IsNullOrEmpty(dto.Token))
                {
                    response.Success = false;
                    response.Message = "Lütfen token'ı boş girmeyiniz.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .DbSet()
                    .FirstOrDefault(mu => (string.IsNullOrEmpty(dto.Token) || mu.Token == Guid.Parse(dto.Token)));

                if (entity == null || !entity.TokenExpried.HasValue)
                {
                    response.Success = false;
                    response.Message = "Girdiğiniz token geçersizdir. Yeniden şifremi unuttum işlemi yapınız.";
                    return;
                }

                if (entity.TokenExpried.HasValue && DateTime.Now > entity.TokenExpried.Value)
                {
                    response.Success = false;
                    response.Message = "Girdiğiniz token süreniz dolmuştur. Lütfen yeniden şifremi unuttum işlemi yapınız.";
                    return;
                }


                entity.Token = null;
                entity.TokenExpried = null;
                entity.Password = dto.Password;

                response.Success = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .Update(entity);
                response.Message = "Yeni şifre başarılı bir şekilde oluşturuldu.";
            });
        }

        public Response MemberUpdate(Customer dto)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                if (string.IsNullOrEmpty(dto.Name) || string.IsNullOrEmpty(dto.Surname))
                {
                    response.Success = false;
                    response.Message = "Adınızı veya Soyadınızı boş gönderemezsiniz.";
                    return;
                }
                else if (string.IsNullOrEmpty(dto.CustomerContact.Mail))
                {
                    response.Success = false;
                    response.Message = "Mail adresinizi boş gönderemezsiniz.";
                    return;
                }
                else if (string.IsNullOrEmpty(dto.CustomerContact.Phone))
                {
                    response.Success = false;
                    response.Message = "Telefon numaranızı boş gönderemezsiniz.";
                    return;
                }

                var entity = this
                    ._unitOfWork
                    .CustomerRepository
                    .DbSet()
                    .Include(x => x.CustomerContact)
                    .FirstOrDefault(mu => mu.Id == dto.Id);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kullanıcı bulunamadı.";
                    return;
                }

                entity.Name = dto.Name.CleanParameter();
                entity.Surname = dto.Surname.CleanParameter();
                entity.CustomerContact.Mail = dto.CustomerContact.Mail.CleanParameter();
                entity.CustomerContact.Phone = dto.CustomerContact.Phone.ToNumber();

                response.Success = this
                    ._unitOfWork
                    .CustomerRepository
                    .Update(entity);
                response.Message = "Güncelleme başarılı bir şekilde yapıldı.";
            });
        }
        #endregion

        #region Helper

        private DataResponse<TwoFactorCustomerUser> ReadTwoFactor(int customerUserId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<TwoFactorCustomerUser>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .TwoFactorCustomerUserRepository
                    .DbSet()
                    .OrderByDescending(x => x.Id)
                    .FirstOrDefault(x => x.CustomerUserId == customerUserId);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Doğrulama kodu sistemde bulunamadı lütfen tekrar doğrulama kodu gönderiniz.";
                    return;
                }

                var result = new TwoFactorCustomerUser
                {
                    Id = entity.Id,
                    Phone = entity.Phone,
                    TryCount = entity.TryCount,
                    Code = entity.Code
                };

                response.Success = true;
                response.Data = result;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Giriş yapılamadı.";

            });
        }

        /// <summary>
        /// Müşteriye gelen 3 kere doğrulanmassa hesap 1 saatliğine bloke olur
        /// </summary>
        /// <param name="customerUserId"></param>
        /// <returns></returns>
        private DataResponse<int> TwoFactorCount(string phone)
        {
            return ExceptionHandler.ResultHandle<DataResponse<int>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .TwoFactorCustomerUserRepository
                    .DbSet()
                    .Count(x =>
                     x.Phone == phone
                     && x.CreatedDate > DateTime.Now.AddHours(-1));

                response.Success = true;
                response.Data = entity;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Giriş yapılamadı.";

            });
        }

        private Response TwoFactorCreate(TwoFactorCustomerUser twoFactorCustomerUser)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this
                    ._unitOfWork
                    .TwoFactorCustomerUserRepository
                    .Create(new Domain.Entities.TwoFactorCustomerUser
                    {
                        Code = twoFactorCustomerUser.Code,
                        CustomerUserId = twoFactorCustomerUser.CustomerUserId,
                        TryCount = twoFactorCustomerUser.TryCount,
                        Phone = twoFactorCustomerUser.Phone,
                        CreatedDate = DateTime.Now
                    });
                response.Success = true;
            });
        }

        private Response TwoFactorUpdate(int id, int tryCount)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var twoFactorCustomerUser = this
                      ._unitOfWork
                      .TwoFactorCustomerUserRepository
                      .Read(id);
                twoFactorCustomerUser.TryCount = tryCount;

                response.Success = this
                      ._unitOfWork
                      .TwoFactorCustomerUserRepository
                      .Update(twoFactorCustomerUser);
            });
        }

        private DataResponse<CustomerUser> ReadByCustomerUser(CustomerUser customerUser)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CustomerUser>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .CustomerUserRepository
                    .DbSet()
                    .Include(mu => mu.CustomerRole)
                    .Include(x => x.Customer.CustomerContact)
                    .FirstOrDefault(mu => mu.Username == customerUser.Username && mu.Password == customerUser.Password && mu.Verified);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Kullanıcı adı veya şifre yanlış.";
                    return;
                }

                response.Data = new CustomerUser
                {
                    Id = entity.Id,
                    Username = customerUser.Username,
                    Password = customerUser.Password,
                    ManagerRoleId = entity.CustomerRoleId,
                    Verified = entity.Verified,
                    CustomerRole = new CustomerRole
                    {
                        Name = entity.CustomerRole.Name
                    },
                    Customer = new Customer
                    {
                        Name = entity.Customer.Name,
                        Surname = entity.Customer.Surname,
                        CustomerContact = new CustomerContact
                        {
                            Phone = entity.Customer.CustomerContact.Phone,
                            Mail = entity.Customer.CustomerContact.Mail
                        }
                    },
                    MoneyPointAmount = entity.MoneyPointAmount
                };

                response.Success = true;
            }, (response, exception) => { });
        }
        #endregion
    }
}
