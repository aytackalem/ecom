﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class PaymentService : IPaymentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IProductInformationStockService _productInformationStockService;

        private readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public PaymentService(IUnitOfWork unitOfWork, IProductInformationStockService productInformationStockService, ISettingService settingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._settingService = settingService;
            this._productInformationStockService = productInformationStockService;
            #endregion
        }
        #endregion

        #region Methods
        public Response UpdateSale(int orderId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var order = this._unitOfWork.OrderRepository.DbSet().Include(x => x.OrderDetails).ThenInclude(x => x.ProductInformation).Include(x => x.Payments)
                .FirstOrDefault(x => x.Id == orderId);

                order.Payments.ForEach(pLoop => pLoop.Paid = true);
                order.OrderTypeId = "OS";
                this._unitOfWork.OrderRepository.Update(order);

                if (this._settingService.StockControl)
                    order.OrderDetails.ForEach(odLoop => this._productInformationStockService.DecreaseStock(odLoop.ProductInformationId, odLoop.Quantity));

                response.Success = true;
                response.Message = "Ödeme başarılı bir şekilde güncellendi.";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Ödeme yapıldı ama sistemde bir arıza oluştu lütfen müşteri hizmetlerini arayınız.";
            });
        }

        public DataResponse<Payment> Read(int orderId, string paymentTypeId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Payment>>((response) =>
            {
                var _payment = this._unitOfWork.PaymentRepository.DbSet().FirstOrDefault(x => x.OrderId == orderId && x.PaymentTypeId == paymentTypeId);

                if (_payment == null)
                {
                    response.Success = false;
                    response.Message = "Ödeme bulunamadı.";
                    return;
                }

                var result = new Payment
                {
                    Amount = _payment.Amount,
                    PaymentTypeId = _payment.PaymentTypeId,
                    Paid = _payment.Paid
                };


                response.Data = result;
                response.Success = true;
                response.Message = "Ödeme başarılı bir şekilde güncellendi.";

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Ödeme tipleri başarısız bir şekilde güncellenmedi.";
            });
        }
        #endregion
    }
}
