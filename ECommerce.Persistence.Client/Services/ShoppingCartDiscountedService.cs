﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using System;
using System.Collections.Generic;

namespace ECommerce.Persistence.Client.Services
{
    public class ShoppingCartDiscountedService : IShoppingCartDiscountedService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;

        private readonly Currency _currency;

        protected readonly ITenantFinder _tenantFinder;
        #endregion

        #region Constructors
        public ShoppingCartDiscountedService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper, ITenantFinder tenantFinder)
        {
            _unitOfWork = unitOfWork;
            this._cacheHandler = cacheHandler;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            this._currency = this._localizationHelper.GetCurrency();
            this._tenantFinder = tenantFinder;
        }
        #endregion

        #region Methods
        public DataResponse<List<ShoppingCartDiscounted>> Read()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<ShoppingCartDiscounted>>>((response) =>
            {
                var shoppingCartDiscounteds = new List<ShoppingCartDiscounted>();

#warning Elle caplin.com.tr için statik yazıldı düzeltilecek
                //if (_tenantFinder.FindId() == 1) //Elle caplin.com.tr için statik yazıldı düzeltilecek.
                //{

                //    shoppingCartDiscounteds.Add(new ShoppingCartDiscounted
                //    {
                //        StartDate = DateTime.Now,
                //        EndDate = DateTime.Now.AddDays(2),
                //        ShoppingCartDiscountedSetting = new ShoppingCartDiscountedSetting
                //        {
                //            Discount = 60,
                //            DiscountIsRate = false,
                //            Limit = 355
                //        }
                //    });
                //}
                //shoppingCartDiscounteds.Add(new ShoppingCartDiscounted
                //{
                //    StartDate = DateTime.Now,
                //    EndDate = DateTime.Now.AddDays(2),
                //    ShoppingCartDiscountedSetting = new ShoppingCartDiscountedSetting
                //    {
                //        Discount = 100,
                //        DiscountIsRate = false,
                //        Limit = 400
                //    }
                //});


                response.Data = shoppingCartDiscounteds;
                response.Success = true;
            }, (response, exception) =>
            {
            });
            }, $"{CacheKey.ShoppingCartDiscounteds}", 10);
        }
        #endregion
    }
}
