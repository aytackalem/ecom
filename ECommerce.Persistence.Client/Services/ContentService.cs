﻿using ECommerce.Application.Client.Constants;
using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Client.Services
{
    public class ContentService : IContentService
    {
        #region Fields
        private readonly ICacheHandler _cacheHandler;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ILocalizationHelper _localizationHelper;

        private readonly Language _language;
        #endregion

        #region Constructors
        public ContentService(ICacheHandler cacheHandler, IUnitOfWork unitOfWork, ILocalizationHelper localizationHelper)
        {
            #region Fields
            this._cacheHandler = cacheHandler;
            this._unitOfWork = unitOfWork;
            this._localizationHelper = localizationHelper;
            this._language = this._localizationHelper.GetLanguage();
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Content> Read(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Content>>((response) =>
            {
                var entity = this
                    ._unitOfWork
                    .ContentRepository
                    .DbSet()
                    .Include(c => c.ContentTranslations)
                    .ThenInclude(ct => ct.ContentTranslationContent)
                         .Include(c => c.ContentTranslations)
                    .ThenInclude(ct => ct.ContentSeoTranslation)
                    .FirstOrDefault(c => c.Id == id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "İçerik bulunamadı.";
                }
                else
                {
                    var entityTranslation = entity
                                            .ContentTranslations
                                            .FirstOrDefault(ct => ct.LanguageId == this._language.Id);


                    if (entityTranslation == null)
                    {
                        response.Success = false;
                        response.Message = "İçerik bilgileri eksik.";
                    }
                    else
                    {
                        response.Data = new Content
                        {
                            Id = entity.Id,
                            Header = entityTranslation.Header,
                            Url = entityTranslation.Url,
                            ContentContent = new ContentContent
                            {
                                Html = entityTranslation.ContentTranslationContent.Html
                            },
                            ContentSeo = new ContentSeo
                            {
                                Title = entityTranslation.ContentSeoTranslation.Title,
                                MetaDescription = entityTranslation.ContentSeoTranslation.MetaDescription,
                                MetaKeywords = entityTranslation.ContentSeoTranslation.MetaKeywords
                            }
                        };
                        response.Success = true;
                        response.Message = "İçerikler başarılı bir şekilde getirildi.";
                    }
                }
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "İçerikler bulunamadı.";
            });
        }

        public DataResponse<List<KeyValue<string, string>>> ReadAsKeyValue()
        {
            return this._cacheHandler.ResponseHandle(() =>
            {
                return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
                {
                    response.Data = this
                        ._unitOfWork
                        .ContentRepository
                        .DbSet()
                        .Include(c => c.ContentTranslations)
                        .Take(5)
                        .OrderByDescending(x => x.Id)
                        .Select(c => new KeyValue<string, string>
                        {
                            Key = c.ContentTranslations.FirstOrDefault(ic => ic.LanguageId == this._language.Id).Header,
                            Value = c.ContentTranslations.FirstOrDefault(ic => ic.LanguageId == this._language.Id).Url
                        })
                        .ToList();
                    response.Success = true;
                    response.Message = "İçerikler başarılı bir şekilde getirildi.";
                }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "İçerikler bulunamadı.";
                });
            }, CacheKey.Contents);
        }
        #endregion
    }
}
