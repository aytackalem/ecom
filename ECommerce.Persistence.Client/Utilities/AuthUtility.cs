﻿using ECommerce.Application.Client.DataTransferObjects;
using ECommerce.Application.Client.Interfaces.Services;
using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Utilities;
using ECommerce.Application.Common.Wrappers;
using System.Collections.Generic;

namespace ECommerce.Persistence.Client.Utilities
{
    public class AuthUtility : IAuthUtility<CustomerUser>
    {
        #region Fields
        private readonly ICustomerUserService _customerUserService;

        private readonly ICookieHelper _cookieHelper;

        private readonly IEmailService _emailService;

        private readonly IEmailTemplateService _emailTemplateService;

        #endregion

        #region Constructors
        public AuthUtility(ICustomerUserService customerUserService, ICookieHelper cookieHelper, IEmailService emailService,
            IEmailTemplateService emailTemplateService)
        {
            #region Fields
            this._customerUserService = customerUserService;
            this._cookieHelper = cookieHelper;
            this._emailService = emailService;
            this._emailTemplateService = emailTemplateService;
            #endregion
        }
        #endregion

        #region Methods
        public Response Login(CustomerUser customerUser, bool keepLoggedIn)
        {
            var customerUserResponse = this._customerUserService.Read(customerUser, false);
            if (!customerUserResponse.Success)
                return new Response
                {
                    Success = customerUserResponse.Success,
                    Message = customerUserResponse.Message
                };

            if (!customerUserResponse.Data.Verified)
                return new Response
                {
                    Success = false,
                    Message = "Kullanıcı bulunamadı."
                };


            var cookieResponse = Add(customerUser);

            if (!cookieResponse.Success)
                return new Response
                {
                    Success = cookieResponse.Success,
                    Message = cookieResponse.Message
                };

            return new Response
            {
                Success = true,
                Message = "Giriş başarılı."
            };
        }

        public DataResponse<CustomerUser> GetLogged()
        {
            var cookieResponse = ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>((response) =>
            {
                response.Data = this._cookieHelper.Read("LoginV3");
                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Kullanıcı bilgileri alınamadı.";
            });
            if (!cookieResponse.Success)
                return new DataResponse<CustomerUser>
                {
                    Success = cookieResponse.Success,
                    Message = cookieResponse.Message
                };

            var customerUserResponse = this._customerUserService.Read(new CustomerUser
            {
                Username = cookieResponse.Data[0].Value,
                Password = cookieResponse.Data[1].Value
            });

            if (!customerUserResponse.Success)
                return new DataResponse<CustomerUser>
                {
                    Success = customerUserResponse.Success,
                    Message = customerUserResponse.Message
                };

            return customerUserResponse;
        }

        public Response Logout()
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Delete("LoginV3");

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Çıkış yapılamadı.";
            });
        }

        public Response ForgotPassword(CustomerUser customerUser)
        {

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var customerUserResponse = this._customerUserService.CreateRecovery(customerUser.Username);
                if (!customerUserResponse.Success)
                {

                    response.Success = true;
                    response.Message = "Kullanıcınız sistemde kayıtlıysa e-posta adresinize iletilecektir.";
                    return;
                }

                var emailTemplate = this._emailTemplateService.ResetPasswordTemplate(customerUserResponse.Data.Id);
                if (emailTemplate.Success)
                {
                    this._emailService.Send(new List<string> { customerUserResponse.Data.Customer.CustomerContact.Mail }, emailTemplate.Data.Subject, emailTemplate.Data.Template);
                }

                response.Message = "Kullanıcınız sistemde kayıtlıysa e-posta adresinize iletilecektir.";
                response.Success = true;
            });
        }

        public Response ResetPassword(CustomerUser user)
        {
            return this._customerUserService.ResetPassword(user);
        }

        public Response UpdatePassword(CustomerUser user)
        {

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var updatePassword = this._customerUserService.UpdatePassword(user);

                if (updatePassword.Success)
                {
                    Add(updatePassword.Data);
                }

                response.Message = "Yeni şifre başarılı bir şekilde oluşturuldu";
                response.Success = true;
            });
        }
        #endregion

        private Response Add(CustomerUser customerUser)
        {

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                this._cookieHelper.Write("LoginV3", new List<KeyValue<string, string>>
                {
                    new KeyValue<string, string> { Key = "Username", Value = customerUser.Username },
                    new KeyValue<string, string> { Key = "Password", Value = customerUser.Password }
                });

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Giriş yapılamadı.";
            });
        }

        public Response ChangeDomain(CustomerUser user)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
             

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Giriş yapılamadı.";
            });
        }

        public Response ChangeCompany(int companyId)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Giriş yapılamadı.";
            });
        }
    }
}
