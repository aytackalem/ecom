﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Cancelled.Base
{
    public interface IApp
    {
        void Run();
    }
}
