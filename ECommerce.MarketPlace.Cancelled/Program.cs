﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Operation;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.Extensions.Logging;
using ECommerce.MarketPlace.Cancelled.Base;
using System.Diagnostics;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Persistence.Common.Services;

namespace ECommerce.MarketPlace.Cancelled
{
    class Program
    {

        #region Method
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);



            var configuration = configurationBuilder.Build();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(appConfig.Tenant.EcommerceConnectionStrings);
            });


            serviceCollection.AddMemoryCache();

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddSingleton<ICacheHandler, CacheHandler>();

            serviceCollection.AddSingleton<IOrderFacade, OrderProxy>();

            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();

            serviceCollection.AddScoped<ITenantFinder, AppConfigTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, AppConfigDomainFinder>();

            serviceCollection.AddScoped<ICompanyFinder, AppConfigCompanyFinderFinder>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IProductInformationStockService, ProductInformationStockService>();

            return serviceCollection.BuildServiceProvider();
        }

        #endregion

    }
}
