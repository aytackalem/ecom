﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.Extensions.Options;

namespace ECommerce.MarketPlace.Cancelled
{
    public class AppConfigTenantFinder : ITenantFinder
    {
        #region Fields
        private readonly IOptions<AppConfig> _options;
        #endregion

        #region Constructors
        public AppConfigTenantFinder(IOptions<AppConfig> options)
        {
            this._options = options;
        }
        #endregion

        #region Methods
        public int Find()
        {
            return this._options.Value.Tenant.Id;
        }
        #endregion
    }
}
