﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.MarketPlace.Cancelled.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace ECommerce.MarketPlace.Cancelled
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IProductInformationStockService _productInformationStockService;

        private readonly IOptions<AppConfig> _options;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IProductInformationStockService productInformationStockService, IOptions<AppConfig> options)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._productInformationStockService = productInformationStockService;
            this._options = options;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var marketPlaces =
                _unitOfWork
                .MarketplaceCompanyRepository
                .DbSet()
                .Include(x => x.Marketplace)
                .Include(x => x.MarketplaceConfigurations)
                .Where(x => x.Active)
                .ToList();

            foreach (var mpLoop in marketPlaces)
            {
                if (mpLoop.MarketplaceId != "TY" && mpLoop.MarketplaceId != "HB")
                    continue;

                var request = new Common.Request.OrderRequest
                {
                    MarketPlaceId = mpLoop.Marketplace.Id,
                    Configurations = mpLoop
                        .MarketplaceConfigurations
                        .ToDictionary(mc => mc.Key, mc => mc.Value)
                };

                if (mpLoop.MarketplaceConfigurations.Count > 0)
                {
                    var response = _orderFacade.GetCancelled(request);
                    if (response.Success)
                    {
                        foreach (var odLoop in response.Data.Orders)
                        {
                            var order = _unitOfWork.OrderRepository
                                .DbSet()
                                .Include(o => o.OrderDetails)
                                .FirstOrDefault(x =>
                                x.MarketplaceOrderNumber == odLoop.OrderCode
                                && x.MarketplaceId == mpLoop.Marketplace.Id
                                && (odLoop.OrderShipment == null
                                || x.OrderShipments.Any(x => x.TrackingCode == odLoop.OrderShipment.TrackingCode))
                                && x.OrderTypeId != "IP");

                            if (order != null)
                            {
                                order.OrderTypeId = "IP";
                                _unitOfWork.OrderRepository.Update(order);

                                if (this._options.Value.Tenant.IsStock)
                                    order.OrderDetails.ForEach(odLoop => this._productInformationStockService.IncreaseStock(odLoop.ProductInformationId, odLoop.Quantity));

                                Console.BackgroundColor = ConsoleColor.Green;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{order.MarketplaceOrderNumber}] sipariş numarası iptal edildi.");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine($"[{mpLoop.Marketplace.Name}] [{odLoop.OrderCode}] sipariş sistemde bulunamadı veya sipariş iptal statüsündedir.");
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;

                            }

                        }

                    }
                }
            }


        }
        #endregion
    }
}
