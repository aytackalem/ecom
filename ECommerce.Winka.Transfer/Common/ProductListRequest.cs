﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Winka.Transfer.Common
{
    public class ProductListRequest
    {
        #region Property
        public Dictionary<string, string> Configurations { get; set; }

        public string Barcode { get; set; }

        public string SkuCode { get; set; }

        public int Count { get; set; }
        #endregion
    }
}
