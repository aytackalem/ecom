﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Winka.Transfer.Common
{
    public class DataResult<TData>
    {
        #region Property
        public TData Data { get; set; }

        public bool Failed { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public bool Success { get; set; }
        #endregion
    }
}
