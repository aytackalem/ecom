﻿namespace ECommerce.Winka.Transfer.Base
{
    public interface IApp
    {
        void Run(string dbName,string winkaDbName);
    }
}
