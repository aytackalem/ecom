﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using ECommerce.Winka.Transfer.Base;
using ECommerce.Winka.Transfer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;

namespace ECommerce.Winka.Transfer
{
    public class App : IApp
    {

        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        public readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        private Domain.Entities.Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Domain.Entities.Company _company;

        private readonly MarketplaceResolver _marketplaceResolver;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService _marketplaceCatalogVariantValueService;

        #endregion

        public App(IUnitOfWork unitOfWork, IHttpHelper httpHelper, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IDbNameFinder dbNameFinder,
            IServiceProvider serviceProvider, MarketplaceResolver marketplaceResolver, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService marketplaceCatalogVariantValueService)
        {


            #region Field
            this._unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            this._dbNameFinder = dbNameFinder;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._serviceProvider = serviceProvider;
            this._marketplaceResolver = marketplaceResolver;
            this._marketplaceVariantService = marketplaceVariantService;
            this._marketplaceCatalogVariantValueService = marketplaceCatalogVariantValueService;

            #endregion

        }


        public void Run(string dbName, string winkaDbName)
        {
            ((DynamicTenantFinder)this._tenantFinder)._tenantId = 1;
            this._tenant = new Tenant { Id = 1 };
            ((DynamicDomainFinder)this._domainFinder)._domainId = 1;
            this._domain = new Domain.Entities.Domain { Id = 1 };
            ((DynamicCompanyFinder)this._companyFinder)._companyId = 1;
            this._company = new Domain.Entities.Company { Id = 1 };

            this._dbNameFinder.Set(dbName);


            var connectionString = $"Server=172.31.57.11;Database={dbName};UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";


            Seed(connectionString, dbName, winkaDbName);


            InsertProducts(connectionString, dbName);
            ProductUpdate(connectionString, dbName, winkaDbName);

            ProductMarketplace(connectionString, dbName);
            FullVariantUpdate(connectionString);
            IkasMarketplace();

        }

        private void Seed(string connectionString, string dbName, string winkaDbName)
        {
            Console.WriteLine("Categories");
            #region Category
            var categories = _unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();

            var kategoris = GetDataTable(connectionString, @$"Select * From [{dbName}].[vtmaster].dbo.StokGruplari");
            foreach (DataRow rLoop in kategoris.Rows)
            {
                var categoryId = rLoop["Id"].ToString();

                if (!categories.Any(x => x.Code == categoryId))
                {
                    Domain.Entities.Category category = new()
                    {
                        Code = categoryId,
                        Active = true,
                        CategoryTranslations = new()
                    {
                        new Domain.Entities.CategoryTranslation
                        {
                            Name = rLoop["Adi"].ToString(),
                            LanguageId="TR",
                            Url = categoryId,
                            CategoryContentTranslation=new Domain.Entities.CategoryContentTranslation
                            {
                                Content=string.Empty,
                                CreatedDate=DateTime.Now,

                                DomainId = 1,
                                TenantId=1,
                            },
                            CategorySeoTranslation=new Domain.Entities.CategorySeoTranslation
                            {

                                CreatedDate=DateTime.Now,
                                Title=string.Empty,
                                MetaDescription=string.Empty,
                                MetaKeywords=string.Empty,
                                DomainId = 1,
                                TenantId=1,
                            }
                        }
                    }
                    };
                    this._unitOfWork.CategoryRepository.Create(category);

                }



            }
            #endregion

            #region Color
            var renklers = GetDataTable(connectionString, $"Select Adi From [{dbName}].[vtmaster].[dbo].[Renkler] group by Adi");
            var bedens = GetDataTable(connectionString, $"Select Beden From [{dbName}].[{winkaDbName}].[dbo].[StokRBK] Group By Beden");

            var variants = _unitOfWork.VariantRepository.DbSet().Include(x => x.VariantValues).ThenInclude(x => x.VariantValueTranslations).ToList();//Renk

            foreach (var vLoop in variants)
            {
                var variantValueTranslations = vLoop.VariantValues.SelectMany(x => x.VariantValueTranslations).ToList();

                if (vLoop.Id == 1) //renk
                {
                    foreach (DataRow rLoop in renklers.Rows)
                    {
                        if (!variantValueTranslations.Any(x => x.Value == rLoop["Adi"].ToString()))
                        {

                            vLoop.VariantValues.Add(new Domain.Entities.VariantValue
                            {
                                VariantValueTranslations = new()
                             {
                                    new Domain.Entities.VariantValueTranslation
                                    {
                                        LanguageId = "TR",
                                        Value = rLoop["Adi"].ToString()
                                    }
                             }
                            });
                        }
                    }
                }
                else if (vLoop.Id == 3) //beden
                {

                    foreach (DataRow rLoop in bedens.Rows)
                    {
                        if (!variantValueTranslations.Any(x => x.Value == rLoop["Beden"].ToString()))
                        {

                            vLoop.VariantValues.Add(new Domain.Entities.VariantValue
                            {
                                VariantValueTranslations = new()
                             {
                                    new Domain.Entities.VariantValueTranslation
                                    {
                                        LanguageId = "TR",
                                        Value = rLoop["Beden"].ToString()
                                    }
                             }
                            });
                        }
                    }

                }

                _unitOfWork.VariantRepository.Update(vLoop);
            }

            #endregion
        }

        private Response FullVariantUpdate(string connectionString)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                using (conn = new SqlConnection(connectionString))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();


                    command.CommandTimeout = 300;
                    command.CommandText = @$"
Update	PIT
Set		FullVariantValuesDescription = T.V
From	ProductInformationTranslations As PIT
Join	(
Select		[PI].Id, String_Agg(VT.[Name] + ': ' + VVT.[Value], ', ') As V
From		ProductInformations As [PI]
Join		ProductInformationVariants As PIV
On			[PI].Id = PIV.ProductInformationId
Join		VariantValues As VV
On			PIV.VariantValueId = VV.Id
Join		VariantValueTranslations As VVT
On			VV.Id = VVT.VariantValueId
Join		Variants As V
On			V.Id = VV.VariantId
Join		VariantTranslations As VT
On			V.Id = VT.VariantId
Group By	[PI].Id
) As T
On PIT.ProductInformationId = T.Id
Where  PIT.TenantId=@TenantId
And PIT.DomainId=@DomainId



Update	PIT
Set		VariantValuesDescription = T.V
From	ProductInformationTranslations As PIT
Join	(
Select		[PI].Id, '('+String_Agg( VVT.[Value], ', ') +')' As V
From		ProductInformations As [PI]
Join		ProductInformationVariants As PIV
On			[PI].Id = PIV.ProductInformationId
Join		VariantValues As VV
On			PIV.VariantValueId = VV.Id
Join		VariantValueTranslations As VVT
On			VV.Id = VVT.VariantValueId
Join		Variants As V
On			V.Id = VV.VariantId
Join		VariantTranslations As VT
On			V.Id = VT.VariantId
where		ShowVariant=1
Group By	[PI].Id
) As T
On PIT.ProductInformationId = T.Id
Where  PIT.TenantId=@TenantId
And PIT.DomainId=@DomainId
";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();



                    response.Success = true;

                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private void InsertProducts(string connectionString, string dbName)
        {
            Console.WriteLine("InsertProducts");

            var _categoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().ToList();
            var _marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().ToList();

            var _categories = _unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
            var _variantValues = _unitOfWork.VariantValueRepository.DbSet().Include(x => x.VariantValueTranslations).ToList();

            var textRKBModel = "Select * From [LAFABA].[CodeDB_2].[dbo].[StokRBKModel] Order By Id Desc";
            var textstokRKB = @"Select RKB.Id,RKB.ModelId,RKB.StokId,RKB.Renk,RKB.Beden,RKB.Kavala From [LAFABA].[CodeDB_2].[dbo].[StokRBK] RKB
            Join [LAFABA].[CodeDB_2].[dbo].[Stoklar] S ON S.Id=RKB.StokId
            Where  OzelKod6=5208 and Grubu>0 
            group by RKB.Id,RKB.ModelId,RKB.StokId,RKB.Renk,RKB.Beden,RKB.Kavala";
            var textStok = @"Select S.*,ISNULL(OK.Kod,'') as ShelfCode From [LAFABA].[CodeDB_2].[dbo].[Stoklar] S
                        left Join    [LAFABA].[CodeDB_2].[dbo].[OzelKodlar] As OK With(NoLock)
                         On      S.OzelKod4 = OK.Id
                        Where OzelKod6=5208 and Grubu>0";
            var textBirimler = @"Select * From	[LAFABA].[CodeDB_2].[dbo].[Birimler] BR
Inner join		[LAFABA].[CodeDB_2].[dbo].[Barkodlar] BK ON BR.BarkodId = BK.Id
Inner join		[LAFABA].[CodeDB_2].[dbo].[KDVGruplari] Kdv ON KDV.Grubu=KdvGrubu and Kdv.Tipi=0";
            var textRenkler = "Select * From [LAFABA].[vtmaster].[dbo].[Renkler]";
            var textCinsiyet = "Select Id, Kod From [LAFABA].[CodeDB_2].[dbo].[OzelKodlar] Where Id In (5037, 6629)";
            var textKategori = @"Select * From [LAFABA].[vtmaster].dbo.StokGruplari";



            if (dbName == "Moonsea")
            {
                textRKBModel = "Select * From [Moonsea].[CodeDB_1].[dbo].[StokRBKModel] Order By Id Desc";
                textstokRKB = @"Select RKB.Id,RKB.ModelId,RKB.StokId,RKB.Renk,RKB.Beden,RKB.Kavala From [Moonsea].[CodeDB_1].[dbo].[StokRBK] RKB
            Join [Moonsea].[CodeDB_1].[dbo].[Stoklar] S ON S.Id=RKB.StokId
            Where  OzelKod6=50 --and Grubu>0 
            group by RKB.Id,RKB.ModelId,RKB.StokId,RKB.Renk,RKB.Beden,RKB.Kavala";

                textStok = @"Select S.*,ISNULL(OK.Kod,'') as ShelfCode From [Moonsea].[CodeDB_1].[dbo].[Stoklar] S
                        left Join    [Moonsea].[CodeDB_1].[dbo].[OzelKodlar] As OK With(NoLock)
                         On      S.OzelKod4 = OK.Id
                        Where S.OzelKod6=50 --and Grubu>0";

                textBirimler = @"Select * From	[Moonsea].[CodeDB_1].[dbo].[Birimler] BR
Inner join		[Moonsea].[CodeDB_1].[dbo].[Barkodlar] BK ON BR.BarkodId = BK.Id
Inner join		[Moonsea].[CodeDB_1].[dbo].[KDVGruplari] Kdv ON KDV.Grubu=KdvGrubu and Kdv.Tipi=0";
                textRenkler = "Select * From [Moonsea].[vtmaster].[dbo].[Renkler]";
                textCinsiyet = "Select Id, Kod From [Moonsea].[CodeDB_1].[dbo].[OzelKodlar] Where Id In (5037, 6629)";
                textKategori = @"Select * From [Moonsea].[vtmaster].dbo.StokGruplari";

            }





            var stokRBKModels = GetDataTable(connectionString, textRKBModel);
            var stokRBKs = GetDataTable(connectionString, textstokRKB);


            var stoklars = GetDataTable(connectionString, textStok);
            var birimlers = GetDataTable(connectionString, textBirimler);
            var renklers = GetDataTable(connectionString, textRenkler);
            var cinsiyets = GetDataTable(connectionString, textCinsiyet);
            var kategoris = GetDataTable(connectionString, textKategori);


            var products = new List<Domain.Entities.Product>();

            foreach (DataRow stokRBKModel in stokRBKModels.Rows)
            {
                var modelId = stokRBKModel["Id"];
                var sellerCode = stokRBKModel["Kodu"];



                var stokRBKFirst = stokRBKs.Select($"ModelId = {modelId}").FirstOrDefault();

                if (stokRBKFirst == null) continue;

                var firstStokId = stokRBKFirst["StokId"];

                var firstStoklar = stoklars.Select($"Id = {firstStokId}").FirstOrDefault();

                if (firstStoklar == null) continue;

                var grubu = firstStoklar["Grubu"];
                var kategori = "";
                if (Convert.ToInt32(firstStoklar["Grubu"]) == 0)
                {
                    kategori = "Elbise";
                }
                else
                    kategori = kategoris.Select($"Id = {firstStoklar["Grubu"]}")[0]["Id"].ToString();


                var categoryId = _categories.FirstOrDefault(x => x.Code == kategori).Id;

                string productName = $"{stokRBKModel["Adi"].ToString()}";

                if (Convert.ToInt32(firstStoklar["OzelKod1"]) == 5037 || Convert.ToInt32(firstStoklar["OzelKod1"]) == 6629)
                {
                    productName = $"{cinsiyets.Select($"Id = {firstStoklar["OzelKod1"]}")[0]["Kod"].ToString()} {productName}";

                }

                var product =
                    _unitOfWork
                    .ProductRepository.DbSet()
                    .Include(x => x.ProductTranslations)
                    .Include(x => x.ProductInformations)
                    .ThenInclude(x => x.ProductInformationPhotos)
                    .FirstOrDefault(x => x.SellerCode == sellerCode.ToString() && !x.IsOnlyHidden);

                if (product == null)
                    product = new Domain.Entities.Product
                    {
                        BrandId = 1,
                        CategoryId = categoryId,
                        Active = true,
                        IsOnlyHidden = false,
                        SupplierId = 1,
                        SellerCode = sellerCode.ToString(),
                        CreatedDate = DateTime.Now,
                        TenantId = 1,
                        DomainId = 1,
                        CategoryProducts = new List<Domain.Entities.CategoryProduct>(),
                        ProductInformations = new List<Domain.Entities.ProductInformation>(),
                        ProductTranslations = new List<Domain.Entities.ProductTranslation>
                        {
                            new Domain.Entities.ProductTranslation
                            {
                                TenantId=1,
                                DomainId=1,
                                CreatedDate=DateTime.Now,
                                LanguageId="TR",

                                Name=productName
                            }
                        },
                    };

                var skuCode = "";
                foreach (DataRow stokRBK in stokRBKs.Select($"ModelId = {modelId}"))
                {

                    var stokId = stokRBK["StokId"];

                    var stoklar = stoklars.Select($"Id = {stokId}").FirstOrDefault();
                    var birimler = birimlers.Select($"StokId = {stokId}").FirstOrDefault();

                    if (birimler == null) continue;

                    var barkodu = birimler["Barkodu"].ToString();

                    if (stoklar == null) continue;

                    if (string.IsNullOrEmpty(stoklar["StokAdi"].ToString())) continue;


                    if (product.ProductInformations.Any(x => x.Barcode == barkodu)) continue;



                    var beden = stokRBK["Beden"].ToString();
                    var renk = renklers.Select($"Id = {stokRBK["Renk"]}")[0]["Adi"].ToString();
                    var renkKodu = renklers.Select($"Id = {stokRBK["Renk"]}")[0]["Kodu"].ToString();
                    var stockCode = stoklar["StokKodu"].ToString();


                    skuCode = $"{sellerCode}{renkKodu}";


                    var productInformationPhotos = new List<Domain.Entities.ProductInformationPhoto>();
                    var firstSku = product.ProductInformations.FirstOrDefault(x => x.Photoable && x.SkuCode == skuCode);
                    if (firstSku != null && firstSku.ProductInformationPhotos.Count > 0)
                    {
                        productInformationPhotos.AddRange(
                            firstSku.ProductInformationPhotos.Where(x => !x.Deleted).Select(pp => new ProductInformationPhoto
                            {
                                CreatedDate = DateTime.Now,
                                Deleted = false,
                                DisplayOrder = pp.DisplayOrder,
                                DomainId = pp.DomainId,
                                TenantId = pp.TenantId,
                                FileName = pp.FileName
                            }));
                    }


                    #region ProductInformationVariants
                    var productInformationVariants = new List<Domain.Entities.ProductInformationVariant>();

                    var renkler = _variantValues.SelectMany(x => x.VariantValueTranslations)
                        .FirstOrDefault(x => x.Value == renk);

                    if (!string.IsNullOrEmpty(renk) && renkler != null)
                    {

                        var renkId = renkler.VariantValueId;

                        productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                        {
                            VariantValueId = renkId,
                            CreatedDate = DateTime.Now,
                            TenantId = 1,
                            DomainId = 1,

                        });
                    }


                    var bedenler = _variantValues.SelectMany(x => x.VariantValueTranslations)
                              .FirstOrDefault(x => x.Value == beden);
                    if (!string.IsNullOrEmpty(beden) && bedenler != null)
                    {
                        var bedenId = bedenler.VariantValueId;
                        productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                        {
                            VariantValueId = bedenId,
                            TenantId = 1,
                            DomainId = 1,
                        });


                    }

                    string cinsiyet = "";

                    var unitCost = 0m;
                    var listPrice = 0m;
                    var unitPrice = 0m;

                    if (dbName == "Lafaba")
                    {
                        unitCost = Convert.ToDecimal(birimler["Fiyat1"]);
                        listPrice = Convert.ToDecimal(birimler["Fiyat2"]);
                        unitPrice = Convert.ToDecimal(birimler["Fiyat3"]);

                        if (Convert.ToInt32(stoklar["OzelKod1"]) == 5037 || Convert.ToInt32(stoklar["OzelKod1"]) == 6629)
                        {
                            cinsiyet = cinsiyets.Select($"Id = {stoklar["OzelKod1"]}")[0]["Kod"].ToString();
                            var cinsiyetId = _variantValues.SelectMany(x => x.VariantValueTranslations)
                          .FirstOrDefault(x => x.Value == cinsiyet).VariantValueId;
                            productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                            {
                                VariantValueId = cinsiyetId,
                                TenantId = 1,
                                DomainId = 1,
                            });

                        }
                    }
                    else if (dbName == "Moonsea")
                    {
                        listPrice = Convert.ToDecimal(birimler["Fiyat2"]);
                        unitPrice = Convert.ToDecimal(birimler["Fiyat1"]);

                        var cinsiyetId = _variantValues.SelectMany(x => x.VariantValueTranslations)
                      .FirstOrDefault(x => x.Value == "Kadın").VariantValueId;

                        productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                        {
                            VariantValueId = cinsiyetId,
                            TenantId = 1,
                            DomainId = 1,
                        });
                    }

                    var yasGrubu = _variantValues.SelectMany(x => x.VariantValueTranslations)
                            .FirstOrDefault(x => x.Value == "Yetişkin");
                    if (yasGrubu == null)
                        yasGrubu = _variantValues.SelectMany(x => x.VariantValueTranslations)
                            .FirstOrDefault(x => x.Value == "Genç");

                    if (yasGrubu != null)
                        productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                        {
                            VariantValueId = yasGrubu.VariantValueId,
                            TenantId = 1,
                            DomainId = 1,
                        });
                    #endregion


                    var marketplaceVariantValueMappings = _marketplaceVariantValueMappings.Where(x => productInformationVariants.Any(pv => pv.VariantValueId == x.VariantValueId)).ToList();

                    string productInformationName = $"{cinsiyet} {renk} {stokRBKModel["Adi"].ToString()}".TrimStart();
                    var exsistPhotoable = product.ProductInformations.Any(x => x.GroupId == skuCode);

                    product.ProductInformations.Add(new Domain.Entities.ProductInformation
                    {
                        TenantId = 1,
                        DomainId = 1,
                        ProductId = product.Id,
                        SkuCode = skuCode,
                        Barcode = barkodu,
                        Photoable = !exsistPhotoable,
                        GroupId = skuCode,
                        StockCode = stockCode,
                        ShelfZone = String.Empty,
                        ShelfCode = stoklar["ShelfCode"].ToString(),
                        Stock = 0,
                        Active = true,
                        CreatedDate = DateTime.Now,
                        ProductInformationTranslations = new List<Domain.Entities.ProductInformationTranslation>
                        {
                            new Domain.Entities.ProductInformationTranslation
                            {
                                LanguageId = "TR",
                                Name = productInformationName,
                                DomainId = 1,
                                TenantId = 1,
                                SubTitle = "",
                                Url = "",
                                CreatedDate=DateTime.Now,
                                ProductInformationSeoTranslation=new Domain.Entities.ProductInformationSeoTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    DomainId=1,
                                    TenantId=1
                                },
                                ProductInformationContentTranslation = new Domain.Entities.ProductInformationContentTranslation
                                {
                                  CreatedDate = DateTime.Now,
                                  DomainId = 1,
                                  Content = productName,
                                  Description = productName,
                                  TenantId = 1
                                }
                            }
                        },
                        ProductInformationPriceses = new List<Domain.Entities.ProductInformationPrice>
                        {
                            new Domain.Entities.ProductInformationPrice
                            {
                                CurrencyId = "TL",
                                UnitCost = unitCost,
                                ListUnitPrice = listPrice,
                                UnitPrice = unitPrice,
                                TenantId=1,
                                DomainId=1,
                                CreatedDate= DateTime.Now,
                                VatRate = Convert.ToInt32(birimler["Orani"]) * 0.01m,
                            }
                        },
                        ProductInformationVariants = productInformationVariants,
                        ProductInformationPhotos = productInformationPhotos
                    });
                }


                if (product.ProductInformations.Count > 0)
                {
                    products.Add(product);
                }


            }

            _unitOfWork.ProductRepository.BulkInsertMapping(products.Where(x => x.Id == 0).ToList());
            _unitOfWork.ProductInformationRepository.BulkInsertMapping(products.Where(x => x.Id > 0).SelectMany(x => x.ProductInformations.Where(pi => pi.Id == 0)).ToList());



        }




        private void IkasMarketplace()
        {

            var tenants = this
            ._unitOfWork
            .TenantRepository
            .DbSet()
            .Include(t => t.TenantSetting)
            .ToList();

            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .ToList();

                    foreach (var thecompany in companies)
                    {

                        ((DynamicCompanyFinder)this._companyFinder)._companyId = thecompany.Id;
                        this._domain = theDomain;
                        using (var appScope = this._serviceProvider.CreateScope())
                        {

                            var _marketplaceService = appScope.ServiceProvider.GetRequiredService<IMarketplaceService>();
                            _marketplaceService.GetCategories("IK");

                        }


                    }


                    //


                }
            }
        }

        private async void ProductMarketplace(string connectionString, string dbName)
        {
            Console.WriteLine("ProductMarketplace");
            var tenants = this
          ._unitOfWork
          .TenantRepository
          .DbSet()
          .Include(t => t.TenantSetting)
          .ToList();

            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .ToList();

                    var products = _unitOfWork.ProductRepository.DbSet().AsNoTracking()
                            .Include(x => x.ProductInformations)
                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                             .Include(x => x.ProductInformations)
                             .ThenInclude(x => x.ProductInformationVariants)
                             .ThenInclude(x => x.VariantValue.Variant)
                             .Where(x => !x.IsOnlyHidden
                             && x.ProductInformations.Any(pi => pi.CreatedDate >= DateTime.Now.Date.AddMonths(-1))
                             )
                             .ToList();

                    var textBirimler = @"Select	   S.StokKodu,Fiyat2,Fiyat3,Fiyat7,Fiyat8,Fiyat9,Fiyat10 
                            From	   [LAFABA].[CodeDB_2].[dbo].[Stoklar] S
                            Inner Join [LAFABA].[CodeDB_2].[dbo].[Birimler] BR On S.Id=BR.StokId
                            Where	   S.OzelKod6=5208 and S.Grubu>0";
                    if (dbName == "Moonsea")
                    {
                        textBirimler = @"Select	   S.StokKodu,Fiyat1,Fiyat2,Fiyat3,Fiyat4 
                            From	   [Moonsea].[CodeDB_1].[dbo].[Stoklar] S
                            Inner Join [Moonsea].[CodeDB_1].[dbo].[Birimler] BR On S.Id=BR.StokId
                            Where	   S.OzelKod6=50 --and S.Grubu>0";
                    }


                    var birimlers = GetDataTable(connectionString, textBirimler);

                    foreach (var theCompany in companies)
                    {
                        ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;
                        this._company = theCompany;



                        var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet()
                            .Include(x => x.Marketplace)
                            .Where(x => x.Active && x.Marketplace.Active)
                        .ToList();

                        var marketplaceCategoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().Include(x => x.MarketplaceCategoryVariantValueMappings).ToList();
                        var marketplaceBrandMappings = _unitOfWork.MarketplaceBrandMappingRepository.DbSet().ToList();

                        var marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().ToList();

                        var _productMarketplaces = new List<Domain.Entities.ProductMarketplace>();
                        var _productInformationMarketplaces = new List<Domain.Entities.ProductInformationMarketplace>();
                        var _productInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>();


                        var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet()
                            .Select(x => new
                            {
                                x.ProductId,
                                x.MarketplaceId
                            }).ToList();


                        var productInformationMarketplaces = _unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Select(x => new ECommerce.Domain.Entities.ProductInformationMarketplace
                            {
                                Id = x.Id,
                                ProductInformationId = x.ProductInformationId,
                                MarketplaceId = x.MarketplaceId,
                                ProductInformationMarketplaceVariantValues = x.ProductInformationMarketplaceVariantValues.Select(piv => new ECommerce.Domain.Entities.ProductInformationMarketplaceVariantValue
                                {
                                    MarketplaceVariantCode = piv.MarketplaceVariantCode
                                }).ToList()
                            })
                            .ToList();



                        foreach (var mcLoop in marketplaceCompanies)
                        {
                            var marketplaceCategoryCodes = new Dictionary<string, List<string>>();
                            var _variantValueDictionary = new Dictionary<string, bool>();

                            foreach (var pLoop in products)
                            {

                                #region Mappings




                                var marketplaceCategory = marketplaceCategoryMappings.FirstOrDefault(x =>
                                (mcLoop.MarketplaceId == "TYE" ? x.MarketplaceId == "TY" : x.MarketplaceId == mcLoop.MarketplaceId)
                                && x.CategoryId == pLoop.CategoryId
                                && (!x.CompanyId.HasValue || x.CompanyId == theCompany.Id));

                                var marketplaceBrandMapping = marketplaceBrandMappings.FirstOrDefault(x =>
                                x.BrandId == pLoop.BrandId
                                && (mcLoop.MarketplaceId == "TYE" ? x.MarketplaceId == "TY" : x.MarketplaceId == mcLoop.MarketplaceId));
                                #endregion

                                if (marketplaceCategory != null && marketplaceBrandMapping != null)
                                {
                                    var anyProductMarketPlace = productMarketplaces.Any(x => x.ProductId == pLoop.Id && x.MarketplaceId == mcLoop.MarketplaceId);

                                    if (!anyProductMarketPlace)
                                    {

                                        var sellerCode = pLoop.SellerCode.ToString();
                                        var deliveryDay = 2;

                                        if (dbName == "Moonsea")
                                        {
                                            sellerCode = $"SD/{pLoop.SellerCode.ToString()}";
                                            deliveryDay = 1;
                                        }
                                        else
                                        {
                                            if (mcLoop.CompanyId == 2)
                                            {
                                                sellerCode = $"OZ{pLoop.SellerCode.ToString()}";
                                            }
                                            else if (mcLoop.CompanyId == 3)
                                            {
                                                sellerCode = $"AD{pLoop.SellerCode.ToString()}";
                                            }
                                        }


                                        _productMarketplaces.Add(new Domain.Entities.ProductMarketplace
                                        {
                                            Active = true,
                                            MarketplaceBrandCode = marketplaceBrandMapping.MarketplaceBrandCode,
                                            MarketplaceBrandName = marketplaceBrandMapping.MarketplaceBrandName,
                                            MarketplaceCategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                            MarketplaceCategoryName = !string.IsNullOrEmpty(marketplaceCategory.MarketplaceCategoryName) ? marketplaceCategory.MarketplaceCategoryName.Trim() : string.Empty,
                                            TenantId = theTenant.Id,
                                            DomainId = theDomain.Id,
                                            CompanyId = theCompany.Id,
                                            ProductId = pLoop.Id,
                                            SellerCode = sellerCode,
                                            DeliveryDay = deliveryDay,
                                            UUId = Guid.NewGuid().ToString().ToLower(),
                                            MarketplaceId = mcLoop.MarketplaceId
                                        });
                                    }


                                    foreach (var piLoop in pLoop.ProductInformations)
                                    {
                                        var anyProductInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.ProductInformationId == piLoop.Id && x.MarketplaceId == mcLoop.MarketplaceId);


                                        if (!marketplaceCategoryCodes.ContainsKey(marketplaceCategory.MarketplaceCategoryCode))
                                        {
                                            if (mcLoop.Marketplace.IsECommerce)
                                            {
                                                var eCommerceVariants = _unitOfWork.ECommerceVariantRepository
                                                        .DbSet()
                                                        .Where(x => x.ECommerceCategoryCode == marketplaceCategory.MarketplaceCategoryCode && x.MarketplaceId == mcLoop.MarketplaceId);

                                                marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, eCommerceVariants.Select(x => x.Code).ToList());
                                            }
                                            else
                                            {
                                                var categoryCodes = this._marketplaceVariantService.GetByCategoryCodesAsync(new Application.Common.Parameters.MarketplaceCatalog.VariantService.GetByCategoryCodesParameter
                                                {
                                                    MarketplaceId = mcLoop.MarketplaceId,
                                                    CategoryCodes = new List<string> { marketplaceCategory.MarketplaceCategoryCode }
                                                }).Result;

                                                if (!categoryCodes.Success) continue;

                                                marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, categoryCodes.Data.Items.Select(x => x.Code).ToList());
                                            }


                                        }

                                        if (anyProductInformationMarketplace == null)
                                        {
                                            var birimler = birimlers.Select($"StokKodu = '{piLoop.StockCode}'").FirstOrDefault();

                                            if (birimler == null) continue;

                                            var listPrice = 0m;
                                            var unitPrice = 0m;


                                            var stockCode = $"{piLoop.SkuCode}|{piLoop.StockCode}";
                                            var barcode = piLoop.Barcode;


                                            if (dbName == "Lafaba")
                                            {
                                                listPrice = LafabaListPrice(birimler, theCompany.Id, mcLoop.Id);
                                                unitPrice = LafabaUnitPrice(birimler, theCompany.Id, mcLoop.Id);
                                                if (mcLoop.Id == 7) //ikas
                                                {
                                                    stockCode = piLoop.StockCode;
                                                }
                                                else if (mcLoop.Id == 9) //ofze ikas
                                                {
                                                    stockCode = piLoop.StockCode;
                                                }
                                                else if (mcLoop.Id == 10) //Abiye dolabı ikas
                                                {

                                                    stockCode = piLoop.StockCode;
                                                }
                                                else if (mcLoop.CompanyId == 2) //ofze
                                                {
                                                    barcode = piLoop.Barcode.Replace("6402187", "2391036");
                                                    stockCode = $"OZ{stockCode}";
                                                }
                                            }
                                            else if (dbName == "Moonsea")
                                            {
                                                listPrice = MoonseaListPrice(birimler, theCompany.Id, mcLoop.Id);
                                                unitPrice = MoonseaUnitPrice(birimler, theCompany.Id, mcLoop.Id);
                                            }

                                            if (listPrice == 0 || unitPrice == 0) continue;


                                            var productInformationMarketplace = new Domain.Entities.ProductInformationMarketplace
                                            {
                                                Active = true,
                                                Barcode = barcode,
                                                ListUnitPrice = listPrice,
                                                UnitPrice = unitPrice,
                                                StockCode = stockCode,
                                                AccountingPrice = true,
                                                UUId = Guid.NewGuid().ToString().ToLower(),
                                                TenantId = theTenant.Id,
                                                DomainId = theDomain.Id,
                                                CompanyId = theCompany.Id,
                                                MarketplaceId = mcLoop.MarketplaceId,
                                                ProductInformationId = piLoop.Id,
                                                DirtyProductInformation = true,
                                                ProductInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>()
                                            };

                                            #region Variant Mappings

                                            foreach (var theProductInformationVariant in piLoop.ProductInformationVariants)
                                            {
                                                if (theProductInformationVariant.VariantValue == null)
                                                    continue;

                                                var _marketplaceVariantValueMappings =
                marketplaceVariantValueMappings.Where(mvvm =>
                (mcLoop.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true)
                && marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                && mvvm.MarketplaceId == mcLoop.MarketplaceId)
                .GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();




                                                foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                                {

                                                    /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                                    if (!marketplaceVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY"))
                                                    {
                                                        var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                                        if (!_variantValueDictionary.ContainsKey(code))
                                                        {
                                                            var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                            {
                                                                MarketplaceId = mcLoop.MarketplaceId,
                                                                Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                                CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                                VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                            }).Result;

                                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                        }

                                                        if (_variantValueDictionary[code] == false) continue;
                                                    }

                                                    productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                    {
                                                        AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                                        Mandatory = marketplaceVariantValueMapping.Mandatory,
                                                        Multiple = marketplaceVariantValueMapping.Multiple,
                                                        Varianter = marketplaceVariantValueMapping.Varianter,
                                                        MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                                        MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                                        MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                        MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                                        TenantId = theTenant.Id,
                                                        DomainId = theDomain.Id,
                                                        CompanyId = theCompany.Id
                                                    });
                                                }


                                            }

                                            #endregion

                                            #region Category Mappings
                                            foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                            {
                                                if (productInformationMarketplace
                                  .ProductInformationMarketplaceVariantValues
                                  .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                                  string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                                    continue;

                                                if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY" || mcLoop.MarketplaceId == "PA"))
                                                {
                                                    var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                                    if (!_variantValueDictionary.ContainsKey(code))
                                                    {
                                                        var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                        {
                                                            MarketplaceId = mcLoop.MarketplaceId,
                                                            Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                            CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                            VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                                        }).Result;
                                                        if (!marketplaceCatalogVariantValueService.Success) continue;


                                                        _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                    }

                                                    if (_variantValueDictionary[code] == false) continue;
                                                }

                                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                {
                                                    AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                                    Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                                    Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                                    Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                                    MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                                    MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                                    MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                    MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                                    TenantId = theTenant.Id,
                                                    DomainId = theDomain.Id,
                                                    CompanyId = theCompany.Id
                                                });

                                            }
                                            #endregion

                                            _productInformationMarketplaces.Add(productInformationMarketplace);
                                        }
                                        else
                                        {
                                            #region Variant Mappings
                                            foreach (var theProductInformationVariant in piLoop.ProductInformationVariants)
                                            {

                                                var _marketplaceVariantValueMappings =
                                               marketplaceVariantValueMappings.Where(mvvm =>
                                               (mcLoop.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true)
                                               && marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                                               && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                                               && mvvm.MarketplaceId == mcLoop.MarketplaceId)
                                               .GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();

                                                foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                                {
                                                    if (anyProductInformationMarketplace
                                                        .ProductInformationMarketplaceVariantValues
                                                        .Any(pimvv => pimvv.MarketplaceVariantCode == marketplaceVariantValueMapping.MarketplaceVariantCode))
                                                        continue;


                                                    /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                                    if (!marketplaceVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY"))
                                                    {
                                                        var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                                        if (!_variantValueDictionary.ContainsKey(code))
                                                        {
                                                            var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                            {
                                                                MarketplaceId = mcLoop.MarketplaceId,
                                                                Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                                CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                                VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                            }).Result;

                                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                        }

                                                        if (_variantValueDictionary[code] == false) continue;
                                                    }

                                                    _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                    {
                                                        AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                                        Mandatory = marketplaceVariantValueMapping.Mandatory,
                                                        Multiple = marketplaceVariantValueMapping.Multiple,
                                                        Varianter = marketplaceVariantValueMapping.Varianter,
                                                        ProductInformationMarketplaceId = anyProductInformationMarketplace.Id,
                                                        MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                                        MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                                        MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                        MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                                        TenantId = theTenant.Id,
                                                        DomainId = theDomain.Id,
                                                        CompanyId = theCompany.Id
                                                    });
                                                }
                                            }
                                            #endregion

                                            #region Category Mappings
                                            foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                            {
                                                if (anyProductInformationMarketplace
                                  .ProductInformationMarketplaceVariantValues
                                  .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                                  string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                                    continue;

                                                if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY" || mcLoop.MarketplaceId == "PA"))
                                                {
                                                    var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                                    if (!_variantValueDictionary.ContainsKey(code))
                                                    {
                                                        var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                        {
                                                            MarketplaceId = mcLoop.MarketplaceId,
                                                            Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                            CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                            VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                                        }).Result;
                                                        if (!marketplaceCatalogVariantValueService.Success) continue;


                                                        _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                    }

                                                    if (_variantValueDictionary[code] == false) continue;
                                                }

                                                _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                {
                                                    AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                                    Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                                    Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                                    Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                                    MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                                    MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                                    MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                    MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                                    TenantId = theTenant.Id,
                                                    DomainId = theDomain.Id,
                                                    CompanyId = theCompany.Id
                                                });

                                            }
                                            #endregion
                                        }
                                    }


                                }
                            }
                        }
                        try
                        {
                            _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
                            _unitOfWork.ProductInformationMarketplaceVariantValueRepository.BulkInsert(_productInformationMarketplaceVariantValues);
                            _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);

                            var ProductInformationMarketplaceIds = _productInformationMarketplaceVariantValues.GroupBy(x => x.ProductInformationMarketplaceId).Select(x => x.Key).ToList();
                            BulkUpdateDirtyProductInformation(connectionString, ProductInformationMarketplaceIds);
                            ProductInformationMarketplaceIds.AddRange(_productInformationMarketplaces.Select(x => x.Id).ToList());
                            BulkUpdateDirtyProductInformationIkas(connectionString, ProductInformationMarketplaceIds);

                        }
                        catch (Exception ex)
                        {

                        }







                    }
                }
            }



        }


        private Response BulkUpdateDirtyProductInformationIkas(string connectionString, List<int> productInformationMarketplaceIds)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

                foreach (var theId in productInformationMarketplaceIds)
                {
                    var row = dataTable.NewRow();
                    row["ProductInformationMarketplaceId"] = theId;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkUpdatePim (
    ProductInformationMarketplaceId int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkUpdatePim";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 600;
                    command.CommandText = @"
UPDATE PIM2
SET
DirtyProductInformation=1
From		ProductInformationMarketplaces PIM WITH(NOLOCK)
JOIN		#TmpTableBulkUpdatePim T WITH(NOLOCK) ON T.ProductInformationMarketplaceId=PIM.Id
JOIN		ProductInformations PIN ON PIN.Id=PIM.ProductInformationId
JOIN		ProductInformations PIN2 ON PIN2.ProductId=PIN.ProductId
JOIN		ProductInformationMarketplaces PIM2 ON PIM2.ProductInformationId=PIN2.Id AND PIM2.CompanyId=@CompanyId
Where		PIM.DomainId = @DomainId And PIM.CompanyId  = @CompanyId And PIM2.MarketplaceId='IK'

Drop Table  #TmpTableBulkUpdatePim;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                    var dataReader = command.ExecuteNonQuery();


                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }
        private Response BulkUpdateDirtyProductInformation(string connectionString, List<int> productInformationMarketplaceIds)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

                foreach (var theId in productInformationMarketplaceIds)
                {
                    var row = dataTable.NewRow();
                    row["ProductInformationMarketplaceId"] = theId;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkUpdatePim (
    ProductInformationMarketplaceId int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkUpdatePim";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 600;
                    command.CommandText = @"
Update		PIM
set
DirtyProductInformation = 1
From		ProductInformationMarketplaces PIM WITH(NOLOCK)
JOIN		#TmpTableBulkUpdatePim T WITH(NOLOCK) ON T.ProductInformationMarketplaceId=PIM.Id
Where		PIM.DomainId = @DomainId And PIM.CompanyId  = @CompanyId And PIM.MarketplaceId <> 'IK'

Drop Table  #TmpTableBulkUpdatePim;";

                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();


                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }



        private decimal LafabaListPrice(DataRow birimler, int companyId, int marketplaceCompanyId)
        {

            decimal listPrice = 0;


            if (marketplaceCompanyId == 3)// Hepsiburada
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat9"]);
            }
            if (marketplaceCompanyId == 7 || marketplaceCompanyId == 9 || marketplaceCompanyId == 10)// IKAS
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat2"]);

            }
            if (marketplaceCompanyId == 11)// MODANISA
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat8"]);

            }
            else if (companyId == 1) //Lafaba
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat7"]);

            }
            else if (companyId == 2)//Ofze
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat10"]);

            }
            else if (companyId == 2)//Ofze
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat2"]);

            }
            else if (companyId == 3)//Ofze
            {
                listPrice = Convert.ToDecimal(birimler["Fiyat2"]);

            }
            return listPrice;
        }

        private decimal LafabaUnitPrice(DataRow birimler, int companyId, int marketplaceCompanyId)
        {

            decimal unitPrice = 0;
            if (marketplaceCompanyId == 3)// Hepsiburada
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat9"]);
            }
            if (marketplaceCompanyId == 7 || marketplaceCompanyId == 9 || marketplaceCompanyId == 10)// IKAS
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat3"]);

            }
            if (marketplaceCompanyId == 11)// MODANISA
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat8"]);
            }
            else if (companyId == 1) //Lafaba
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat7"]);

            }
            else if (companyId == 2)//Ofze
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat10"]);

            }
            else if (companyId == 3)//Ofze
            {
                unitPrice = Convert.ToDecimal(birimler["Fiyat10"]);

            }
            return unitPrice;
        }


        private decimal MoonseaListPrice(DataRow birimler, int companyId, int marketplaceCompanyId)
        {

            decimal listPrice = 0;


            listPrice = Convert.ToDecimal(birimler["Fiyat2"]);

            return listPrice;
        }

        private decimal MoonseaUnitPrice(DataRow birimler, int companyId, int marketplaceCompanyId)
        {

            decimal unitPrice = 0;

            unitPrice = Convert.ToDecimal(birimler["Fiyat1"]);


            return unitPrice;
        }

        public DataTable GetDataTable(string connectionString, string selectCommandText)
        {
            DataTable dataTable = new();
            using (SqlDataAdapter sqlDataAdapter = new(selectCommandText, new SqlConnection(connectionString)))
            {
                sqlDataAdapter.Fill(dataTable);
            }
            return dataTable;
        }

        private void ProductUpdate(string connectionString, string dbName, string winkaDbName)
        {
            Console.WriteLine("ProductUpdate");


            var urunAciklamalari = GetDataTable(connectionString, @$"
Select	RefId, Baslik, Deger,RKB.Kodu
From	[{dbName}].vtmaster.dbo.AciklamaBaslik As B
Join	[{dbName}].[{winkaDbName}].dbo.Aciklamalar As A 
On		B.Id = A.BaslikId
Join    [{dbName}].[{winkaDbName}].[dbo].[StokRBKModel] As RKB ON RKB.Id=A.RefId
Where	Durum = 0 AND Tip = 5
ORDER   BY A.BaslikId
");

            var stokRBKModels = GetDataTable(connectionString, $"Select * From [{dbName}].[{winkaDbName}].[dbo].[StokRBKModel] Order By Id Desc");

            var StokRBKs = new List<StokRBK>();

            foreach (DataRow stokRBKModel in stokRBKModels.Rows)
            {
                var modelId = stokRBKModel["Id"];
                var urunAciklamasi = urunAciklamalari.Select($"RefId = {modelId}");
                var description = string.Empty;

                if (dbName == "Lafaba")
                {
                    description = string.Join("", urunAciklamasi.Select(ua =>
                                     ua["Baslik"] != null && (ua["Baslik"].ToString() == "Not:" || ua["Baslik"].ToString() == "Açıklama")
                                         ? $"<div>{ua["Deger"]}</div>"
                                         : $"<div><b>{ua["Baslik"]}</b> {ua["Deger"]}</div>"));
                }
                else
                    description = string.Join("", urunAciklamasi.Select(ua =>
                                    ua["Baslik"] != null && (ua["Baslik"].ToString() == "Not:" || ua["Baslik"].ToString() == "Açıklama")
                                        ? $"{ua["Deger"]}"
                                        : $"{ua["Baslik"]} {ua["Deger"]}"));

                var modelKodu = stokRBKModel["Kodu"];

                StokRBKs.Add(new StokRBK
                {
                    Kodu = modelKodu.ToString(),
                    Description = description
                });

            }
            BulkUpdate(connectionString, StokRBKs);
            //İKAS İÇİN İSİM GÜNCELLEME
            // Select OK.Kod + ' ' + RKB.Adi AS Adi,RKB.Kodu INTO #TEMP_TABLE From  [{dbName}].[{winkaDbName}].[dbo].[StokRBKModel]  RKB 
            //JOIN [{dbName}].[{winkaDbName}].[dbo].[StokRBK] SRKB ON RKB.Id=SRKB.ModelId
            //JOIN [{dbName}].[{winkaDbName}].[dbo].[Stoklar] S ON S.Id=SRKB.StokId
            //JOIN [{dbName}].[{winkaDbName}].[dbo].[OzelKodlar] OK ON OK.Id=S.OzelKod1
            //where  S.OzelKod6=5208 and S.Grubu>0
            //Group by RKB.Adi,S.OzelKod1,RKB.Kodu,OK.Kod



            //UPDATE 
            //		   ProductTranslations 
            //SET		
            //	Name=T.Adi
            //FROM ProductTranslations PT 
            //JOIN Products P ON P.Id=PT.ProductId
            //JOIN #TEMP_TABLE T ON T.Kodu=P.SellerCode COLLATE SQL_Latin1_General_CP1_CI_AS


            //DROP TABLE #TEMP_TABLE
        }



        private Response BulkUpdate(string connectionString, List<StokRBK> stokRBKs)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ModelCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Description", typeof(string)));
                foreach (var stokRBK in stokRBKs)
                {


                    var row = dataTable.NewRow();
                    row["ModelCode"] = stokRBK.Kodu;
                    row["Description"] = stokRBK.Description;
                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    var temporaryTableName = $"Update_{Guid.NewGuid().ToString().Replace("-", "")}";

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @$"
Create Table {temporaryTableName} (
    ModelCode                 NVarChar(200) Not Null,
    Description                 NVarChar(MAX) Not Null

)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = temporaryTableName;
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @$"

    UPDATE [PIM]
    SET
    DirtyProductInformation=1
    From  ProductInformationMarketplaces [PIM]
    Join  ProductInformations [PIN] ON PIN.Id=[PIM].ProductInformationId
    Join  ProductInformationTranslations PT ON PT.ProductInformationId=[PIM].ProductInformationId AND PT.LanguageId='TR'
    Join  ProductInformationContentTranslations PIC ON PIC.Id=PT.Id
    Join  Products P ON P.Id=PIN.ProductId
    Join  {temporaryTableName} U ON P.SellerCode=U.ModelCode
    Where U.[Description]<>'' And PIC.[Content]<>U.[Description]

    UPDATE  PIC
    SET
    PIC.[Content]=U.[Description]
    From  ProductInformationContentTranslations PIC
    Join  ProductInformationTranslations PT ON PT.Id=PIC.Id
    Join  ProductInformations [PIN] ON PIN.Id=PT.ProductInformationId
    Join  Products P ON P.Id=PIN.ProductId
    Join  {temporaryTableName} U ON P.SellerCode=U.ModelCode
    Where U.[Description]<>'' And PIC.[Content]<>U.[Description]

    Drop Table  {temporaryTableName};";

                    var dataReader = command.ExecuteNonQuery();
                }


                response.Success = true;

            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Bilinmeyen bir hata oluştu.";

                    conn?.Close();
                });
        }

    }
}