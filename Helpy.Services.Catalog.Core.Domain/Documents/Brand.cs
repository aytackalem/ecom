﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Brand : DocumentBase
    {
        #region Properties
        public string Name { get; set; }

        public string Slug { get; set; }
        #endregion
    }
}
