﻿namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Shelf
    {
        #region Properties
        public string Zone { get; set; }

        public string Code { get; set; }
        #endregion
    }
}
