﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Product : DocumentBase
    {
        #region Constructors
        public Product()
        {
            #region Properties
            this.ProductInformations = new();
            #endregion
        }
        #endregion

        #region Properties
        public string Name { get; set; }

        public string SellerCode { get; set; }

        public string Slug { get; set; }
        #endregion

        #region Navigation Properties
        public Category Category { get; set; }

        public Brand Brand { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }
        #endregion
    }
}
