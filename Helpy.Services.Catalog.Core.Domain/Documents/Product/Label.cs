﻿namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Label
    {
        #region Properties
        public string Barcode { get; set; }

        public string StockCode { get; set; }

        public string Sku { get; set; }
        #endregion
    }
}
