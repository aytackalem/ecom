﻿namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Price
    {
        #region Properties
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }
        #endregion
    }
}
