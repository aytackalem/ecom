﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class ProductInformation : DocumentBase
    {
        #region Properties
        public string Name { get; set; }

        public string Type { get; set; }

        public int Stock { get; set; }

        public int VirtualStock { get; set; }

        public bool OnSale { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public Label Label { get; set; }

        public Price Price { get; set; }

        public Shelf Shelf { get; set; }

        public List<Variant> Variants { get; set; }
        #endregion
    }
}
