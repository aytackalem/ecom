﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class ProductMarketplace : CompanyDocumentBase
    {
        #region Properties
        public ObjectId ProductId { get; set; }

        public string MarketplaceId { get; set; }

        public string SellerCode { get; set; }

        public int DeliveryDay { get; set; }

        public string UUId { get; set; }
        #endregion

        #region Navigation Properties
        public MarketplaceCategory MarketplaceCategory { get; set; }

        public MarketplaceBrand MarketplaceBrand { get; set; }
        #endregion
    }
}
