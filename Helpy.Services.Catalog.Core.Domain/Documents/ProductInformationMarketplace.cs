﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class ProductInformationMarketplace : CompanyDocumentBase
    {
        #region Properties
        public ObjectId ProductInformationId { get; set; }

        public ObjectId ProductMarketplaceId { get; set; }

        public string MarketplaceId { get; set; }

        public string UUId { get; set; }

        public bool ApplyIntegrationPrice { get; set; }

        public bool Active { get; set; }

        public List<MarketplaceVariant> MarketplaceVariants { get; set; }
        #endregion

        #region Navigation Properties
        public Label Label { get; set; }

        public Price Price { get; set; }

        public MarketplaceInformation MarketplaceInformation { get; set; }

        public UpdateInformation UpdateInformation { get; set; }
        #endregion
    }
}
