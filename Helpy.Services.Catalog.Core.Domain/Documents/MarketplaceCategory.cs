﻿using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class MarketplaceCategory
    {
        #region Properties
        public ObjectId Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
