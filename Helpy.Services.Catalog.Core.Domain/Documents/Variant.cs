﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Variant : DocumentBase
    {
        #region Constructors
        public Variant()
        {
            #region Properties
            this.VariantValues = new();
            #endregion
        } 
        #endregion

        #region Properties
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantValue> VariantValues { get; set; }
        #endregion
    }
}
