﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class Category : DocumentBase
    {
        #region Properties
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string ParentId { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }
        #endregion
    }
}
