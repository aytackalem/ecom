﻿namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class MarketplaceInformation
    {
        #region Properties
        public bool Opened { get; set; }

        public bool OnSale { get; set; }

        public bool Locked { get; set; }

        public string Url { get; set; }
        #endregion
    }
}
