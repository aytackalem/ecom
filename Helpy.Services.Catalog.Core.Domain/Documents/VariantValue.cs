﻿using Helpy.Services.Catalog.Core.Domain.Documents.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class VariantValue : DocumentBase
    {
        #region Properties
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string VariantId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
