﻿namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class UpdateInformation
    {
        #region Properties
        public bool DirtyStock { get; set; }

        public bool DirtyPrice { get; set; }

        public bool Dirty { get; set; }
        #endregion
    }
}
