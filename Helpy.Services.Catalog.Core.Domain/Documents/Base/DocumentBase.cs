﻿using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.Catalog.Core.Domain.Documents.Base
{
    public abstract class DocumentBase
    {
        #region Properties
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        public int DomainId { get; set; }
        #endregion
    }
}
