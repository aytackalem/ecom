﻿using MongoDB.Bson.Serialization.Attributes;

namespace Helpy.Services.Catalog.Core.Domain.Documents.Base
{
    public abstract class CompanyDocumentBase : DocumentBase
    {
        #region Properties
        public int CompanyId { get; set; }
        #endregion
    }
}
