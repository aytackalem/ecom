﻿using MongoDB.Bson;

namespace Helpy.Services.Catalog.Core.Domain.Documents
{
    public class MarketplaceVariant
    {
        #region Properties
        public string Code { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketplaceVariantValue> MarketplaceVariantValues { get; set; }
        #endregion
    }
}
