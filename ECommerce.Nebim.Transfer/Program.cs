﻿
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.MarketplaceCatalog;
using ECommerce.Nebim.Transfer.Base;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using ECommerce.Infrastructure;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Persistence.Common.Marketplace;

namespace ECommerce.Nebim.Transfer
{
    class Program
    {

        static void Main(string[] args)
        {
#if DEBUG
            args = new string[] { "Mizalle", "Stock" };
            //args = new string[] { "Sogo", "Stock" };
#endif

            Console.WriteLine($"{args[0]} {args[1]} Start..");
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run(args[0], args[1]);
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var fileName = Process.GetCurrentProcess().MainModule.FileName;


            ServiceCollection serviceCollection = new ServiceCollection();
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetParent(fileName).FullName)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = configurationBuilder.Build();

            #region Config And Setting Dependencies
            serviceCollection.AddTransient<ISettingService, SettingService>();
            #endregion

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();
            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();
            serviceCollection.AddScoped<IMarketplaceService, MarketplaceService>();
            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddTransient<IHttpHelper, HttpHelper>();
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();
            //serviceCollection.AddTransient<IShipmentProviderService, ShipmentProviderService>();
            serviceCollection.AddTransient<IHttpHelperV3, HttpHelperV3>();
            serviceCollection.AddTransient<IMarketplaceVariantService, MarketplaceVariantService>();

            serviceCollection.AddTransient<IConfiguration>(x => configuration);
            serviceCollection.AddMemoryCache();
            return serviceCollection.BuildServiceProvider();
        }

    }
}