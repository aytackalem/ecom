﻿using ECommerce.Application.Common.Helpers;
using ECommerce.Application.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace ECommerce.Nebim.Transfer
{
    public class DynamicDbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName = string.Empty;

        #endregion

        #region Constructors
        public DynamicDbNameFinder()
        {
       
        }
        #endregion

        #region Methods
        public string FindName()
        {
            return this._dbName;
        }

        public void Set(string dbName)
        {
            this._dbName = dbName;
        }
        #endregion
    }
}
