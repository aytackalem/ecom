﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Nebim.Transfer.Common.Response
{
    public class NebimProduct
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemDesc { get; set; }
        public string CurrencyCode { get; set; }
        public string Barcode { get; set; }
        public string GenderCode { get; set; }

        [JsonProperty("Yaş Grubu")]
        public string AgeGroup { get; set; }
        public string GenderDesc { get; set; }
        public string StockCode { get; set; }
        public string ColorCode { get; set; }
        public string ColorDesc { get; set; }
        public string ItemDimTypeCode { get; set; }
        public string ItemDim1Code { get; set; }
        public string ItemDim1Desc { get; set; }
        public string ItemDim2Code { get; set; }
        public string ItemDim2Desc { get; set; }
        public string ItemDim3Code { get; set; }
        public string ItemDim3Desc { get; set; }
        public string Cat01Code { get; set; }
        public string Cat01Desc { get; set; }
        public string Cat02Code { get; set; }
        public string Cat02Desc { get; set; }
        public string Cat03Code { get; set; }
        public string Cat03Desc { get; set; }
        public string Cat04Code { get; set; }
        public string Cat04Desc { get; set; }
        public string Cat05Code { get; set; }
        public string Cat05Desc { get; set; }

        public string Cat06Code { get; set; }
        public string Cat06Desc { get; set; }

        public string Cat07Code { get; set; }
        public string Cat07Desc { get; set; }

        public string Cat08Code { get; set; }
        public string Cat08Desc { get; set; }

        public string BrandCode { get; set; }
        public string BrandDesc { get; set; }
        public int Qty { get; set; }
        public string Vat { get; set; }
        public double Price1 { get; set; }
        public double Price2 { get; set; }
        public string Price3 { get; set; }
        public string Price4 { get; set; }
        public string Price5 { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public string Image6 { get; set; }
        public string Image7 { get; set; }
        public string Image8 { get; set; }
    }
}
