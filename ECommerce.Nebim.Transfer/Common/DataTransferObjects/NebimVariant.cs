﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Nebim.Transfer.Common.DataTransferObjects
{
    public class NebimVariantValue
    {
        public string VariantCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
    }


    public class NebimVariant
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public List<NebimVariantValue> NebimVariantValues { get; set; }
    }
}
