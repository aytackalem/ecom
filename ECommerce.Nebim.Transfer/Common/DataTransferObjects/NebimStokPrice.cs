﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Nebim.Transfer.Common.DataTransferObjects
{
    public class NebimStokPrice
    {
        public string Barcode { get; set; }
        public int Qty { get; set; }
        public decimal Price1 { get; set; }
        public decimal Price2 { get; set; }
    }
}
