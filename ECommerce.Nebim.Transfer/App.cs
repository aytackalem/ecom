﻿using DocumentFormat.OpenXml.Office2010.Excel;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Marketplaces;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Panel.DataTransferObjects;
using ECommerce.Domain.Entities;
using ECommerce.Nebim.Transfer.Base;
using ECommerce.Nebim.Transfer.Common.DataTransferObjects;
using ECommerce.Nebim.Transfer.Common.Response;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using ECommerce.Application.Common.Extensions;
using System.Web;
using ECommerce.Infrastructure.Marketplaces.TSoft.Response;


namespace ECommerce.Nebim.Transfer
{
    public class App : IApp
    {

        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        public readonly IHttpHelper _httpHelper;

        private readonly IServiceProvider _serviceProvider;

        private Domain.Entities.Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Domain.Entities.Company _company;


        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService _marketplaceCatalogVariantValueService;

        #endregion

        public App(IUnitOfWork unitOfWork, IHttpHelper httpHelper, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IDbNameFinder dbNameFinder,
            IServiceProvider serviceProvider, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService marketplaceCatalogVariantValueService)
        {
            #region Field
            this._unitOfWork = unitOfWork;
            this._httpHelper = httpHelper;
            this._dbNameFinder = dbNameFinder;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._serviceProvider = serviceProvider;
            this._marketplaceVariantService = marketplaceVariantService;
            this._marketplaceCatalogVariantValueService = marketplaceCatalogVariantValueService;

            #endregion
        }


        public void Run(string dbName, string type)
        {
            ((DynamicTenantFinder)this._tenantFinder)._tenantId = 1;
            this._tenant = new Tenant { Id = 1 };
            ((DynamicDomainFinder)this._domainFinder)._domainId = 1;
            this._domain = new Domain.Entities.Domain { Id = 1 };
            ((DynamicCompanyFinder)this._companyFinder)._companyId = 1;
            this._company = new Domain.Entities.Company { Id = 1 };

            this._dbNameFinder.Set(dbName);

            var accountingcompany = _unitOfWork
                     .AccountingCompanyRepository
                     .DbSet()
                     .Include(x => x.AccountingConfigurations)
                     .FirstOrDefault(x => x.AccountingId == "NB");

            if (accountingcompany == null)
            {

                Console.WriteLine("Muhasebe sağlayıcısı bulunamadı.");
                return;
            }
            var configurations = accountingcompany.AccountingConfigurations.ToDictionary(ac => ac.Key, ac => ac.Value);

            string apiUrl = configurations["ApiUrl"];
            string apiPhotoUrl = configurations["ApiPhotoUrl"];

            var login = GetLogin(apiUrl);

            if (login != null)
            {
                var photo = !string.IsNullOrEmpty(apiPhotoUrl);

                var connectionString = $"Server=172.31.57.11;Database={dbName};UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

                if (type == "Product")
                {
                    var products = GetProducts(apiUrl, login.SessionID);

                    if (products != null && products.Count > 0)
                    {
                        products.ForEach(pLoop =>
                        {
                            pLoop.GenderDesc = pLoop.GenderDesc.ToLower().Replace("kadın / kız", "Kadın");
                            pLoop.ColorDesc = pLoop.ColorDesc.FirstLetterUppercase();

                        });

                        Seed(connectionString, dbName, products);

                        InsertProducts(connectionString, dbName, products, photo);

                    }


                    FullVariantUpdate(connectionString);

                    if (photo)
                    {
                        InsertImages(apiPhotoUrl);
                    }

                    ProductMarketplace(connectionString, dbName);

                }
                else if (type == "Stock")
                {
                    var stokPrices = GetNebimStokPrices(apiUrl, login.SessionID);

                    if (stokPrices.Success && stokPrices.Data != null)
                    {
                        BulkStockPriceUpdate(connectionString, stokPrices.Data);
                    }
                }
            }


            var marketplaceCompany = _unitOfWork.MarketplaceCompanyRepository.DbSet().Include(x => x.MarketplaceConfigurations).FirstOrDefault(x => x.MarketplaceId == "TSoft" && x.Active);

            if (marketplaceCompany != null)
            {
                var expirationTime = DateTime.Now.AddDays(-1);
                DateTime.TryParse(marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ExpirationTime").Value, System.Globalization.CultureInfo.GetCultureInfo("tr-TR"), out expirationTime);


                if (expirationTime.AddHours(-6) < DateTime.Now)
                {
                    var url = marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Url").Value;

                    var tokenRequest = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>("user", marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "User").Value),
                    new KeyValuePair<string, string>("pass", marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Password").Value),
                 };

                    var tokenResponse = this._httpHelper.Post<TSoftTokenResponse>($"{url}/rest1/auth/login/helpy", tokenRequest);
                    var token = string.Empty;
                    if (tokenResponse != null && tokenResponse.Success == true)
                    {
                        marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "ExpirationTime").Value = tokenResponse.Data[0].ExpirationTime;
                        marketplaceCompany.MarketplaceConfigurations.FirstOrDefault(mc => mc.Key == "Token").Value = tokenResponse.Data[0].Token;
                        _unitOfWork.MarketplaceCompanyRepository.Update(marketplaceCompany);
                    }
                }

                TSoftMarketplaceCategories();
            }



        }


        private void InsertImages(string photoUrl)
        {
            try
            {
                var domain = _unitOfWork.DomainRepository.DbSet().Include(x => x.DomainSetting).First();

                var path = domain.DomainSetting.ImagePath;
                Console.WriteLine("InsertImages");
                var _productInformationPhotos = _unitOfWork.ProductInformationPhotoRepository.DbSet().Where(x => x.FileName.StartsWith(photoUrl)).ToList();
                var newPath = path.Replace("C:\\inetpub", "\\\\172.31.57.21\\c$\\inetpub");


                Dictionary<string, string> keys = new Dictionary<string, string>();

                var productPhotos = new List<Domain.Entities.ProductInformationPhoto>();

                foreach (var ppLoop in _productInformationPhotos)
                {
                    if (keys.ContainsKey(ppLoop.FileName))
                    {
                        ppLoop.FileName = keys[ppLoop.FileName];
                        Console.WriteLine("Cache'den geldi");
                    }
                    else
                    {

                        var splitPhoto = ppLoop.FileName.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                        var fileName = $"{splitPhoto[splitPhoto.Length - 1]}";
                        var success = Download(newPath, fileName, ppLoop.ProductInformationId, ppLoop.FileName);


                        if (success)
                        {

                            keys.Add(ppLoop.FileName, $"{ppLoop.ProductInformationId}/{fileName}");

                            ppLoop.FileName = $"{ppLoop.ProductInformationId}/{fileName}";
                        }
                        else
                            ppLoop.Deleted = true;
                    }
                }
                _unitOfWork.ProductInformationPhotoRepository.Update(_productInformationPhotos);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                Console.ReadLine();
            }


        }


        private bool Download(string path, string fileName, int productInformationId, string url)
        {
            var success = true;


            string directory = $@"{path}\product\{productInformationId}";

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            string pathUrl = $@"{path}\product\{productInformationId}\{fileName.Replace("/", "\\")}";
            using (WebClient webClient = new WebClient())
            {
                if (!url.StartsWith("http")) url = "http://" + url;

                try
                {
                    webClient.DownloadFile(url, pathUrl);
                    Console.WriteLine("Resim yüklendi");
                }
                catch (Exception e)
                {
                    success = false;
                }
            }

            return success;
        }

        private NebimConnect GetLogin(string apiUrl)
        {
            return this._httpHelper.Get<NebimConnect>($"{apiUrl}/IntegratorService/connect", null, null);

        }

        private List<NebimProduct> GetProducts(string apiUrl, string sessionID)
        {
            return this._httpHelper.Get<List<NebimProduct>>($"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc?%20{{%20%22ProcName%22:%20%22usp_GetProductPriceAndInventory_HELPY%22}}", null, null);

        }

        private void Seed(string connectionString, string dbName, List<NebimProduct> nebimProducts)
        {
            Console.WriteLine("Categories");


            #region Brand

            var nebimBrands = nebimProducts.Where(x => !string.IsNullOrEmpty(x.BrandCode)).GroupBy(x => x.BrandCode.ToUpper()).Select(x => new { x.Key, x.First().BrandDesc }).ToList();
            var brands = _unitOfWork.BrandRepository.DbSet().ToList();

            foreach (var rLoop in nebimBrands)
            {


                if (!brands.Any(x => x.Url == rLoop.Key))
                {
                    Domain.Entities.Brand brand = new()
                    {
                        Active = true,
                        CreatedDate = DateTime.Now,
                        Name = rLoop.BrandDesc,
                        Url = rLoop.Key,

                    };
                    this._unitOfWork.BrandRepository.Create(brand);
                }

            }

            #endregion

            #region Category
            var _nebimCategories = new List<NebimCategory>();


            foreach (var ctLoop in nebimProducts)
            {
                for (int i = 1; i <= 8; i++)
                {
                    if (i == 1 && !string.IsNullOrEmpty(ctLoop.Cat01Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat01Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat01Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat01Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat01Code}"
                        });
                    else if (i == 2 && !string.IsNullOrEmpty(ctLoop.Cat02Code) && !string.IsNullOrEmpty(ctLoop.Cat02Desc))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat02Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat02Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat02Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat02Code}"

                        });
                    else if (i == 3 && !string.IsNullOrEmpty(ctLoop.Cat03Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat03Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat03Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat03Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat03Code}"
                        });
                    else if (i == 4 && !string.IsNullOrEmpty(ctLoop.Cat04Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat04Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat04Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat04Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat04Code}"
                        });
                    else if (i == 5 && !string.IsNullOrEmpty(ctLoop.Cat05Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat05Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat05Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat05Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat05Code}"
                        });
                    else if (i == 6 && !string.IsNullOrEmpty(ctLoop.Cat06Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat06Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat06Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat06Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat06Code}"
                        });
                    else if (i == 7 && !string.IsNullOrEmpty(ctLoop.Cat07Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat07Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat07Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat07Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat07Code}"
                        });
                    else if (i == 8 && !string.IsNullOrEmpty(ctLoop.Cat08Code))
                        _nebimCategories.Add(new NebimCategory
                        {
                            Code = $"{ctLoop.Cat08Code}{ctLoop.GenderCode}",
                            Name = !string.IsNullOrEmpty(ctLoop.Cat08Desc) ? $"{ctLoop.GenderDesc} {ctLoop.Cat08Desc}" : $"{ctLoop.GenderDesc}{ctLoop.Cat08Code}"
                        });
                }
            }

            var nebimCategories = _nebimCategories.GroupBy(x => new
            {
                x.Code
            }).Select(x =>
                x.First()
            ).ToList();

            var categories = _unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();


            foreach (var rLoop in nebimCategories)
            {
                var _category = categories.FirstOrDefault(x => x.Code == rLoop.Code);

                if (_category == null)
                {
                    Domain.Entities.Category category = new()
                    {
                        Code = rLoop.Code,
                        Active = true,
                        CreatedDate = DateTime.Now,
                        CategoryTranslations = new()
                    {
                        new Domain.Entities.CategoryTranslation
                        {
                            Name = rLoop.Name,
                            LanguageId="TR",
                            Url = string.Empty,
                            CategoryContentTranslation=new Domain.Entities.CategoryContentTranslation
                            {
                                Content=string.Empty,
                                CreatedDate=DateTime.Now,

                                DomainId = 1,
                                TenantId=1,
                            },
                            CategorySeoTranslation=new Domain.Entities.CategorySeoTranslation
                            {

                                CreatedDate=DateTime.Now,
                                Title=string.Empty,
                                MetaDescription=string.Empty,
                                MetaKeywords=string.Empty,
                                DomainId = 1,
                                TenantId=1,
                            }
                        }
                    }
                    };
                    this._unitOfWork.CategoryRepository.Create(category);
                }
                else
                {
                    _category.CategoryTranslations[0].Name = rLoop.Name;
                    _unitOfWork.CategoryRepository.Update(_category);
                }


            }
            #endregion

            #region Variant

            var nebimVariants = new List<NebimVariant>();

            var color = new NebimVariant
            {
                Name = "Renk",
                Code = "RNK",
                NebimVariantValues = new List<NebimVariantValue>()
            };

            color.NebimVariantValues.AddRange(nebimProducts.Where(x => !string.IsNullOrEmpty(x.ColorCode)).GroupBy(x => x.ColorCode)
                .Select(x => new NebimVariantValue
                {
                    Code = x.Key,
                    Name = !string.IsNullOrEmpty(x.First().ColorDesc) ? x.First().ColorDesc : $"Renk {x.Key}"
                }).ToList());

            nebimVariants.Add(color);


            var gender = new NebimVariant
            {
                Name = "Cinsiyet",
                Code = "CNT",
                NebimVariantValues = new List<NebimVariantValue>()
            };

            gender.NebimVariantValues.AddRange(nebimProducts.Where(x => !string.IsNullOrEmpty(x.GenderCode)).GroupBy(x => x.GenderCode)
                .Select(x => new NebimVariantValue
                {
                    Code = x.Key,
                    Name = x.First().GenderDesc
                }).ToList());

            nebimVariants.Add(gender);



            var body = new NebimVariant
            {
                Name = "Beden",
                Code = "BDN",
                NebimVariantValues = new List<NebimVariantValue>()
            };

            body.NebimVariantValues.AddRange(nebimProducts.Where(x => !string.IsNullOrEmpty(x.ItemDim1Code)).GroupBy(x => x.ItemDim1Code)
                .Select(x => new NebimVariantValue
                {
                    Code = x.Key,
                    Name = x.First().ItemDim1Desc
                }).ToList());

            nebimVariants.Add(body);

            var ageGroup = new NebimVariant
            {
                Name = "Yaş Grubu",
                Code = "YG",
                NebimVariantValues = new List<NebimVariantValue>()
            };

            ageGroup.NebimVariantValues.AddRange(nebimProducts.Where(x => !string.IsNullOrEmpty(x.AgeGroup)).GroupBy(x => x.AgeGroup)
                .Select(x => new NebimVariantValue
                {
                    Code = x.Key,
                    Name = x.Key
                }).ToList());

            if (ageGroup.NebimVariantValues != null && ageGroup.NebimVariantValues.Count > 0)
                nebimVariants.Add(ageGroup);


            var variants = _unitOfWork.VariantRepository.DbSet().Include(x => x.VariantTranslations).Include(x => x.VariantValues).ThenInclude(x => x.VariantValueTranslations).ToList();//Renk

            foreach (var vLoop in nebimVariants)
            {
                var variant = variants.FirstOrDefault(x => x.Code == vLoop.Code);

                if (variant != null)
                {


                    foreach (var nvvLoop in vLoop.NebimVariantValues)
                    {
                        if (!variant.VariantValues.Any(x => x.Code == nvvLoop.Code))
                        {

                            variant.VariantValues.Add(new Domain.Entities.VariantValue
                            {
                                Code = nvvLoop.Code,
                                VariantValueTranslations = new()
                             {
                                    new Domain.Entities.VariantValueTranslation
                                    {
                                        LanguageId = "TR",
                                        CreatedDate=DateTime.Now,
                                        Value = nvvLoop.Name,
                                    }
}
                            });
                        }
                    }
                    _unitOfWork.VariantRepository.Update(variant);
                }
                else
                {
                    variant = new Domain.Entities.Variant
                    {
                        Code = vLoop.Code,
                        CreatedDate = DateTime.Now,
                        VariantTranslations = new()
                        {
                          new Domain.Entities.VariantTranslation
                                    {
                                        LanguageId = "TR",
                                        CreatedDate=DateTime.Now,
                                        Name = vLoop.Name,
                                    }
                        },
                        VariantValues = new List<Domain.Entities.VariantValue>()
                    };

                    foreach (var nvvLoop in vLoop.NebimVariantValues)
                    {
                        variant.VariantValues.Add(new Domain.Entities.VariantValue
                        {
                            Code = nvvLoop.Code,
                            CreatedDate = DateTime.Now,
                            VariantValueTranslations = new()
                             {
                                    new Domain.Entities.VariantValueTranslation
                                    {
                                        LanguageId = "TR",
                                        CreatedDate=DateTime.Now,
                                        Value = nvvLoop.Name,
                                    }
}
                        });

                    }


                    _unitOfWork.VariantRepository.Create(variant);
                }
            }

            #endregion
        }

        private Response FullVariantUpdate(string connectionString)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                using (conn = new SqlConnection(connectionString))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();


                    command.CommandTimeout = 300;
                    command.CommandText = @$"
Update	PIT
Set		FullVariantValuesDescription = T.V
From	ProductInformationTranslations As PIT
Join	(
Select		[PI].Id, String_Agg(VT.[Name] + ': ' + VVT.[Value], ', ') As V
From		ProductInformations As [PI]
Join		ProductInformationVariants As PIV
On			[PI].Id = PIV.ProductInformationId
Join		VariantValues As VV
On			PIV.VariantValueId = VV.Id
Join		VariantValueTranslations As VVT
On			VV.Id = VVT.VariantValueId
Join		Variants As V
On			V.Id = VV.VariantId
Join		VariantTranslations As VT
On			V.Id = VT.VariantId
Group By	[PI].Id
) As T
On PIT.ProductInformationId = T.Id
Where  PIT.TenantId=@TenantId
And PIT.DomainId=@DomainId



Update	PIT
Set		VariantValuesDescription = T.V
From	ProductInformationTranslations As PIT
Join	(
Select		[PI].Id, '('+String_Agg( VVT.[Value], ', ') +')' As V
From		ProductInformations As [PI]
Join		ProductInformationVariants As PIV
On			[PI].Id = PIV.ProductInformationId
Join		VariantValues As VV
On			PIV.VariantValueId = VV.Id
Join		VariantValueTranslations As VVT
On			VV.Id = VVT.VariantValueId
Join		Variants As V
On			V.Id = VV.VariantId
Join		VariantTranslations As VT
On			V.Id = VT.VariantId
where		ShowVariant=1
Group By	[PI].Id
) As T
On PIT.ProductInformationId = T.Id
Where  PIT.TenantId=@TenantId
And PIT.DomainId=@DomainId
";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();



                    response.Success = true;

                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private void InsertProducts(string connectionString, string dbName, List<NebimProduct> nebimProducts, bool photo)
        {
            Console.WriteLine("InsertProducts");

            var _categoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().ToList();
            var _marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().ToList();

            var _categories = _unitOfWork.CategoryRepository.DbSet().Include(x => x.CategoryTranslations).ToList();
            var _brands = _unitOfWork.BrandRepository.DbSet().ToList();
            var _variantValues = _unitOfWork.VariantValueRepository.DbSet().Include(x => x.VariantValueTranslations).Include(x => x.Variant).ToList();


            var groupNebimProducts = nebimProducts.Where(x => x.Price1 > 0).GroupBy(x => x.ItemCode).Select(x => new { sellerCode = x.Key, nebimProducts = x.ToList() }).ToList();



            var products = new List<Domain.Entities.Product>();

            foreach (var nLoop in groupNebimProducts)
            {


                var nebimProduct = nLoop.nebimProducts.First();

                if (string.IsNullOrEmpty(nebimProduct.ItemName)) continue;


                var code = new List<string>();
                for (int i = 1; i <= 8; i++)
                {
                    if (i == 1 && !string.IsNullOrEmpty(nebimProduct.Cat01Code))
                        code.Add(nebimProduct.Cat01Code);
                    else if (i == 2 && !string.IsNullOrEmpty(nebimProduct.Cat02Code))
                        code.Add(nebimProduct.Cat02Code);
                    else if (i == 3 && !string.IsNullOrEmpty(nebimProduct.Cat03Code))
                        code.Add(nebimProduct.Cat03Code);
                    else if (i == 4 && !string.IsNullOrEmpty(nebimProduct.Cat04Code))
                        code.Add(nebimProduct.Cat04Code);
                    else if (i == 5 && !string.IsNullOrEmpty(nebimProduct.Cat05Code))
                        code.Add(nebimProduct.Cat05Code);
                    else if (i == 6 && !string.IsNullOrEmpty(nebimProduct.Cat06Code))
                        code.Add(nebimProduct.Cat06Code);
                    else if (i == 7 && !string.IsNullOrEmpty(nebimProduct.Cat07Code))
                        code.Add(nebimProduct.Cat07Code);
                    else if (i == 8 && !string.IsNullOrEmpty(nebimProduct.Cat08Code))
                        code.Add(nebimProduct.Cat08Code);

                }

                var categoryCode = code[code.Count - 1] + nebimProduct.GenderCode;

                var _category = _categories.FirstOrDefault(x => x.Code == categoryCode);

                if (_category == null) continue;

                var categoryId = _category.Id;


                var brandId = _brands.FirstOrDefault(x => x.Url == nebimProduct.BrandCode.ToUpper()).Id;

                string productName = nebimProduct.ItemName;


                var product =
                    _unitOfWork
                    .ProductRepository.DbSet()
                    .Include(x => x.ProductTranslations)
                    .Include(x => x.ProductInformations)
                    .ThenInclude(x => x.ProductInformationPhotos)
                    .FirstOrDefault(x => x.SellerCode == nLoop.sellerCode && !x.IsOnlyHidden);

                if (product == null)
                    product = new Domain.Entities.Product
                    {
                        BrandId = brandId,
                        CategoryId = categoryId,
                        Active = true,
                        IsOnlyHidden = false,
                        SupplierId = 1,
                        SellerCode = nLoop.sellerCode,
                        CreatedDate = DateTime.Now,
                        TenantId = 1,
                        AccountingProperty = true,
                        DomainId = 1,
                        CategoryProducts = new List<Domain.Entities.CategoryProduct>(),
                        ProductInformations = new List<Domain.Entities.ProductInformation>(),
                        ProductTranslations = new List<Domain.Entities.ProductTranslation>
                        {
                            new Domain.Entities.ProductTranslation
                            {
                                TenantId=1,
                                DomainId=1,
                                CreatedDate=DateTime.Now,
                                LanguageId="TR",

                                Name=productName
                            }
                        },
                    };

                var skuCode = "";
                foreach (var npLoop in nLoop.nebimProducts)
                {
                    if (string.IsNullOrEmpty(npLoop.ItemName)) continue;

                    if (product.ProductInformations.Any(x => x.Barcode == npLoop.Barcode)) continue;

                    skuCode = $"{npLoop.ItemCode}{npLoop.ColorCode}";

                    var images = new List<string>();
                    for (int i = 1; i <= 8; i++)
                    {
                        if (i == 1 && !string.IsNullOrEmpty(npLoop.Image1))
                            images.Add(npLoop.Image1);
                        else if (i == 2 && !string.IsNullOrEmpty(npLoop.Image2))
                            images.Add(npLoop.Image2);
                        else if (i == 3 && !string.IsNullOrEmpty(npLoop.Image3))
                            images.Add(npLoop.Image3);
                        else if (i == 4 && !string.IsNullOrEmpty(npLoop.Image4))
                            images.Add(npLoop.Image4);
                        else if (i == 5 && !string.IsNullOrEmpty(npLoop.Image5))
                            images.Add(npLoop.Image5);
                        else if (i == 5 && !string.IsNullOrEmpty(npLoop.Image6))
                            images.Add(npLoop.Image6);
                        else if (i == 5 && !string.IsNullOrEmpty(npLoop.Image7))
                            images.Add(npLoop.Image7);
                        else if (i == 5 && !string.IsNullOrEmpty(npLoop.Image8))
                            images.Add(npLoop.Image8);
                    }

                    var productInformationPhotos = new List<Domain.Entities.ProductInformationPhoto>();

                    if (photo)
                    {
                        productInformationPhotos.AddRange(
                            images.Select((pp) => new Domain.Entities.ProductInformationPhoto
                            {
                                FileName = pp,
                                CreatedDate = DateTime.Now,
                                Deleted = false,
                                DomainId = 1,
                                TenantId = 1,
                                DisplayOrder = images.IndexOf(pp)
                            }));
                    }

                    #region ProductInformationVariants

                    var nebimVariantValues = new List<NebimVariantValue>();

                    if (!string.IsNullOrEmpty(npLoop.ColorCode))
                        nebimVariantValues.Add(new NebimVariantValue
                        {
                            VariantCode = "RNK",
                            Code = npLoop.ColorCode,
                            Name = npLoop.ColorDesc
                        });

                    if (!string.IsNullOrEmpty(npLoop.GenderCode))
                        nebimVariantValues.Add(new NebimVariantValue
                        {
                            VariantCode = "CNT",
                            Code = npLoop.GenderCode,
                            Name = npLoop.GenderDesc
                        });


                    if (!string.IsNullOrEmpty(npLoop.AgeGroup))
                        nebimVariantValues.Add(new NebimVariantValue
                        {
                            VariantCode = "YG",
                            Code = npLoop.AgeGroup,
                            Name = npLoop.AgeGroup
                        });

                    for (int i = 1; i <= 3; i++)
                    {
                        if (i == 1 && !string.IsNullOrEmpty(npLoop.ItemDim1Code))
                            nebimVariantValues.Add(new NebimVariantValue
                            {
                                VariantCode = "BDN",
                                Code = npLoop.ItemDim1Code,
                                Name = npLoop.ItemDim1Desc
                            });
                        else if (i == 2 && !string.IsNullOrEmpty(npLoop.ItemDim2Code))
                            nebimVariantValues.Add(new NebimVariantValue
                            {
                                VariantCode = "BDN",
                                Code = npLoop.ItemDim2Code,
                                Name = npLoop.ItemDim2Desc
                            });
                        else if (i == 3 && !string.IsNullOrEmpty(npLoop.ItemDim3Code))
                            nebimVariantValues.Add(new NebimVariantValue
                            {
                                VariantCode = "BDN",
                                Code = npLoop.ItemDim3Code,
                                Name = npLoop.ItemDim3Desc
                            });
                    }

                    var productInformationVariants = new List<Domain.Entities.ProductInformationVariant>();

                    foreach (var nevLoop in nebimVariantValues)
                    {
                        var variantValue = _variantValues
                         .FirstOrDefault(x => x.Code == nevLoop.Code && x.Variant.Code == nevLoop.VariantCode);

                        if (variantValue != null)
                        {
                            productInformationVariants.Add(new Domain.Entities.ProductInformationVariant
                            {
                                VariantValueId = variantValue.Id,
                                TenantId = 1,
                                DomainId = 1,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }
                    #endregion

                    var marketplaceVariantValueMappings = _marketplaceVariantValueMappings.Where(x => productInformationVariants.Any(pv => pv.VariantValueId == x.VariantValueId)).ToList();

                    string productInformationName = npLoop.ItemName;

                    if (!npLoop.ItemName.ToLower().Contains(npLoop.ColorDesc.ToLower()))
                    {
                        productInformationName = $"{npLoop.ColorDesc} {npLoop.ItemName}".TrimStart();
                    }




                    var exsistPhotoable = product.ProductInformations.Any(x => x.GroupId == skuCode);

                    product.ProductInformations.Add(new Domain.Entities.ProductInformation
                    {
                        TenantId = 1,
                        DomainId = 1,
                        ProductId = product.Id,
                        SkuCode = skuCode,
                        Barcode = npLoop.Barcode,
                        Photoable = !exsistPhotoable,
                        GroupId = skuCode,
                        StockCode = npLoop.StockCode,
                        ShelfZone = String.Empty,
                        ShelfCode = "",
                        Stock = npLoop.Qty,
                        Active = true,
                        CreatedDate = DateTime.Now,
                        ProductInformationTranslations = new List<Domain.Entities.ProductInformationTranslation>
                        {
                            new Domain.Entities.ProductInformationTranslation
                            {
                                LanguageId = "TR",
                                Name = productInformationName,
                                SubTitle = productInformationName,
                                Url = "",
                                CreatedDate=DateTime.Now,
                                ProductInformationSeoTranslation=new Domain.Entities.ProductInformationSeoTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    DomainId=1,
                                    TenantId=1
                                },
                                ProductInformationContentTranslation = new Domain.Entities.ProductInformationContentTranslation
                                {
                                  CreatedDate = DateTime.Now,
                                  Content = !string.IsNullOrEmpty(npLoop.ItemDesc) ? npLoop.ItemDesc : productInformationName,
                                  Description = !string.IsNullOrEmpty(npLoop.ItemDesc) ? npLoop.ItemDesc : productInformationName,
                                  DomainId=1,
                                    TenantId=1
                                },
                                DomainId=1,
                                TenantId=1
                            }
                        },
                        ProductInformationPriceses = new List<Domain.Entities.ProductInformationPrice>
                        {
                            new Domain.Entities.ProductInformationPrice
                            {
                                CurrencyId = "TL",
                                UnitCost = 0,
                                ListUnitPrice = Convert.ToDecimal(npLoop.Price1),
                                UnitPrice = Convert.ToDecimal(npLoop.Price2),
                                TenantId=1,
                                DomainId=1,
                                CreatedDate= DateTime.Now,
                                VatRate =( npLoop.Vat!=null ? Convert.ToInt32(npLoop.Vat.Replace("%","")) : 10) * 0.01m,
                            }
                        },
                        ProductInformationVariants = productInformationVariants,
                        ProductInformationPhotos = productInformationPhotos
                    });

                }
                if (product.ProductInformations.Count > 0)
                {
                    products.Add(product);
                }




            }
            _unitOfWork.ProductRepository.BulkInsertMapping(products.Where(x => x.Id == 0).ToList());
            _unitOfWork.ProductInformationRepository.BulkInsertMapping(products.Where(x => x.Id > 0).SelectMany(x => x.ProductInformations.Where(pi => pi.Id == 0)).ToList());

        }

        private DataResponse<List<NebimStokPrice>> GetNebimStokPrices(string apiUrl, string sessionID)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<NebimStokPrice>>>((response) =>
            {
                var nebimProc = new NebimProc
                {
                    ProcName = "usp_GetChangeProductPriceAndInventory_HELPY",
                    Parameters = new List<NebimProcParameter>()
                };

                var nebimStokPriceResponse = _httpHelper.Post<NebimProc, List<NebimStokPrice>>(nebimProc, $"{apiUrl}/(S({sessionID}))/IntegratorService/RunProc", null, null, null, null);


                response.Data = nebimStokPriceResponse;
                response.Success = true;
            }, (response, e) =>
            {
                response.Message = "Bilinmeyen bir hata oluştu.";
            });
        }


        private void TSoftMarketplaceCategories()
        {

            var tenants = this
            ._unitOfWork
            .TenantRepository
            .DbSet()
            .Include(t => t.TenantSetting)
            .ToList();

            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .ToList();

                    foreach (var thecompany in companies)
                    {


                        ((DynamicCompanyFinder)this._companyFinder)._companyId = thecompany.Id;
                        this._domain = theDomain;
                        using (var appScope = this._serviceProvider.CreateScope())
                        {

                            var _marketplaceService = appScope.ServiceProvider.GetRequiredService<IMarketplaceService>();
                            _marketplaceService.GetCategories("TSoft");

                        }


                    }


                    //


                }
            }
        }

        private async void ProductMarketplace(string connectionString, string dbName)
        {
            Console.WriteLine("ProductMarketplace");
            var tenants = this
          ._unitOfWork
          .TenantRepository
          .DbSet()
          .Include(t => t.TenantSetting)
          .ToList();

            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .ToList();

                    var products = _unitOfWork.ProductRepository.DbSet().AsNoTracking()
                            .Include(x => x.ProductInformations)
                            .ThenInclude(x => x.ProductInformationPriceses.Where(x => x.CurrencyId == "TL"))
                             .Include(x => x.ProductInformations)
                             .ThenInclude(x => x.ProductInformationVariants)
                             .ThenInclude(x => x.VariantValue.Variant)
                             .Where(x => !x.IsOnlyHidden
                             && x.ProductInformations.Any(pi => pi.CreatedDate >= DateTime.Now.Date.AddMonths(-1))
                             )
                             .ToList();



                    foreach (var theCompany in companies)
                    {
                        ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;
                        this._company = theCompany;



                        var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet()
                            .Include(x => x.Marketplace)
                            .Where(x => x.Active && x.Marketplace.Active)
                        .ToList();

                        var marketplaceCategoryMappings = _unitOfWork.MarketplaceCategoryMappingRepository.DbSet().Include(x => x.MarketplaceCategoryVariantValueMappings).ToList();
                        var marketplaceBrandMappings = _unitOfWork.MarketplaceBrandMappingRepository.DbSet().ToList();

                        var marketplaceVariantValueMappings = _unitOfWork.MarketplaceVariantValueMappingRepository.DbSet().ToList();


                        var _productMarketplaces = new List<Domain.Entities.ProductMarketplace>();
                        var _productInformationMarketplaces = new List<Domain.Entities.ProductInformationMarketplace>();
                        var _productInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>();


                        var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet()
                            .Select(x => new
                            {
                                x.ProductId,
                                x.MarketplaceId
                            }).ToList();


                        var productInformationMarketplaces = _unitOfWork
                            .ProductInformationMarketplaceRepository
                            .DbSet()
                            .Select(x => new ECommerce.Domain.Entities.ProductInformationMarketplace
                            {
                                Id = x.Id,
                                ProductInformationId = x.ProductInformationId,
                                MarketplaceId = x.MarketplaceId,
                                ProductInformationMarketplaceVariantValues = x.ProductInformationMarketplaceVariantValues.Select(piv => new ECommerce.Domain.Entities.ProductInformationMarketplaceVariantValue
                                {
                                    MarketplaceVariantCode = piv.MarketplaceVariantCode
                                }).ToList()
                            })
                            .ToList();



                        foreach (var mcLoop in marketplaceCompanies)
                        {
                            var marketplaceCategoryCodes = new Dictionary<string, List<string>>();
                            var _variantValueDictionary = new Dictionary<string, bool>();

                            foreach (var pLoop in products)
                            {
                                if (pLoop.Id == 9432)
                                {

                                }

                                #region Mappings




                                var marketplaceCategory = marketplaceCategoryMappings.FirstOrDefault(x =>
                                (mcLoop.MarketplaceId == "TYE" ? x.MarketplaceId == "TY" : x.MarketplaceId == mcLoop.MarketplaceId)
                                && x.CategoryId == pLoop.CategoryId
                                && (!x.CompanyId.HasValue || x.CompanyId == theCompany.Id));

                                var marketplaceBrandMapping = marketplaceBrandMappings.FirstOrDefault(x =>
                                x.BrandId == pLoop.BrandId
                                && (mcLoop.MarketplaceId == "TYE" ? x.MarketplaceId == "TY" : x.MarketplaceId == mcLoop.MarketplaceId));
                                #endregion

                                if (marketplaceCategory != null && marketplaceBrandMapping != null)
                                {
                                    var anyProductMarketPlace = productMarketplaces.Any(x => x.ProductId == pLoop.Id && x.MarketplaceId == mcLoop.MarketplaceId);

                                    if (!anyProductMarketPlace)
                                    {

                                        var sellerCode = pLoop.SellerCode.ToString();
                                        var deliveryDay = 2;


                                        sellerCode = $"{pLoop.SellerCode.ToString()}";



                                        _productMarketplaces.Add(new Domain.Entities.ProductMarketplace
                                        {
                                            Active = true,
                                            MarketplaceBrandCode = marketplaceBrandMapping.MarketplaceBrandCode,
                                            MarketplaceBrandName = marketplaceBrandMapping.MarketplaceBrandName,
                                            MarketplaceCategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                            MarketplaceCategoryName = marketplaceCategory.MarketplaceCategoryName,
                                            TenantId = theTenant.Id,
                                            DomainId = theDomain.Id,
                                            CompanyId = theCompany.Id,
                                            ProductId = pLoop.Id,
                                            SellerCode = sellerCode,
                                            DeliveryDay = deliveryDay,
                                            UUId = Guid.NewGuid().ToString().ToLower(),
                                            MarketplaceId = mcLoop.MarketplaceId
                                        });
                                    }


                                    foreach (var piLoop in pLoop.ProductInformations)
                                    {
                                        var anyProductInformationMarketplace = productInformationMarketplaces.FirstOrDefault(x => x.ProductInformationId == piLoop.Id && x.MarketplaceId == mcLoop.MarketplaceId);


                                        if (!marketplaceCategoryCodes.ContainsKey(marketplaceCategory.MarketplaceCategoryCode))
                                        {
                                            if (mcLoop.Marketplace.IsECommerce)
                                            {
                                                var eCommerceVariants = _unitOfWork.ECommerceVariantRepository
                                                        .DbSet()
                                                        .Where(x => x.ECommerceCategoryCode == marketplaceCategory.MarketplaceCategoryCode && x.MarketplaceId == mcLoop.MarketplaceId);

                                                marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, eCommerceVariants.Select(x => x.Code).ToList());
                                            }
                                            else
                                            {
                                                var categoryCodes = this._marketplaceVariantService.GetByCategoryCodesAsync(new Application.Common.Parameters.MarketplaceCatalog.VariantService.GetByCategoryCodesParameter
                                                {
                                                    MarketplaceId = mcLoop.MarketplaceId,
                                                    CategoryCodes = new List<string> { marketplaceCategory.MarketplaceCategoryCode }
                                                }).Result;

                                                if (!categoryCodes.Success) continue;

                                                marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, categoryCodes.Data.Items.Select(x => x.Code).ToList());
                                            }


                                        }

                                        if (anyProductInformationMarketplace == null)
                                        {


                                            var listPrice = piLoop.ProductInformationPriceses.FirstOrDefault().ListUnitPrice;
                                            var unitPrice = piLoop.ProductInformationPriceses.FirstOrDefault().UnitPrice;


                                            var stockCode = $"{piLoop.StockCode}";
                                            var barcode = piLoop.Barcode;




                                            if (listPrice == 0 || unitPrice == 0) continue;


                                            var productInformationMarketplace = new Domain.Entities.ProductInformationMarketplace
                                            {
                                                Active = true,
                                                Barcode = barcode,
                                                ListUnitPrice = listPrice,
                                                UnitPrice = mcLoop.MarketplaceId == "LCW" ? listPrice : unitPrice,
                                                StockCode = stockCode,
                                                AccountingPrice = true,
                                                UUId = stockCode,
                                                TenantId = theTenant.Id,
                                                DomainId = theDomain.Id,
                                                CompanyId = theCompany.Id,
                                                MarketplaceId = mcLoop.MarketplaceId,
                                                ProductInformationId = piLoop.Id,
                                                DirtyProductInformation = true,
                                                ProductInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>()
                                            };


                                            #region Variant Mappings
                                            foreach (var theProductInformationVariant in piLoop.ProductInformationVariants)
                                            {
                                                if (theProductInformationVariant.VariantValue == null)
                                                    continue;

                                                var _marketplaceVariantValueMappings =
                marketplaceVariantValueMappings.Where(mvvm =>
                (mcLoop.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true)
                && marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                && mvvm.MarketplaceId == mcLoop.MarketplaceId)
                .GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();




                                                foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                                {

                                                    /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                                    if (!marketplaceVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY" || mcLoop.MarketplaceId == "PA"))
                                                    {
                                                        var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                                        if (!_variantValueDictionary.ContainsKey(code))
                                                        {
                                                            var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                            {
                                                                MarketplaceId = mcLoop.MarketplaceId,
                                                                Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                                CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                                VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                            }).Result;

                                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                        }

                                                        if (_variantValueDictionary[code] == false) continue;
                                                    }

                                                    productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                    {
                                                        AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                                        Mandatory = marketplaceVariantValueMapping.Mandatory,
                                                        Multiple = marketplaceVariantValueMapping.Multiple,
                                                        Varianter = marketplaceVariantValueMapping.Varianter,
                                                        MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                                        MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                                        MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                        MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                                        TenantId = theTenant.Id,
                                                        DomainId = theDomain.Id,
                                                        CompanyId = theCompany.Id
                                                    });
                                                }


                                            }
                                            #endregion

                                            #region Category Mappings
                                            foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                            {
                                                if (productInformationMarketplace
                                  .ProductInformationMarketplaceVariantValues
                                  .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                                  string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                                    continue;

                                                if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY" || mcLoop.MarketplaceId == "PA"))
                                                {
                                                    var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                                    if (!_variantValueDictionary.ContainsKey(code))
                                                    {
                                                        var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                        {
                                                            MarketplaceId = mcLoop.MarketplaceId,
                                                            Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                            CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                            VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                                        }).Result;
                                                        if (!marketplaceCatalogVariantValueService.Success) continue;


                                                        _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                    }

                                                    if (_variantValueDictionary[code] == false) continue;
                                                }

                                                productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                {
                                                    AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                                    Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                                    Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                                    Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                                    MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                                    MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                                    MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                    MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                                    TenantId = theTenant.Id,
                                                    DomainId = theDomain.Id,
                                                    CompanyId = theCompany.Id
                                                });

                                            }
                                            #endregion

                                            _productInformationMarketplaces.Add(productInformationMarketplace);

                                        }
                                        else
                                        {
                                            #region Variant Mappings
                                            foreach (var theProductInformationVariant in piLoop.ProductInformationVariants)
                                            {

                                                var _marketplaceVariantValueMappings =
                                               marketplaceVariantValueMappings.Where(mvvm =>
                                               (mcLoop.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true)
                                               && marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                                               && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                                               && mvvm.MarketplaceId == mcLoop.MarketplaceId)
                                               .GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();

                                                foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                                {
                                                    if (anyProductInformationMarketplace
                                                        .ProductInformationMarketplaceVariantValues
                                                        .Any(pimvv => pimvv.MarketplaceVariantCode == marketplaceVariantValueMapping.MarketplaceVariantCode))
                                                        continue;


                                                    /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                                    if (!marketplaceVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY"))
                                                    {
                                                        var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                                        if (!_variantValueDictionary.ContainsKey(code))
                                                        {
                                                            var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                            {
                                                                MarketplaceId = mcLoop.MarketplaceId,
                                                                Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                                CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                                VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                            }).Result;

                                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                        }

                                                        if (_variantValueDictionary[code] == false) continue;
                                                    }

                                                    _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                    {
                                                        AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                                        Mandatory = marketplaceVariantValueMapping.Mandatory,
                                                        Multiple = marketplaceVariantValueMapping.Multiple,
                                                        Varianter = marketplaceVariantValueMapping.Varianter,
                                                        ProductInformationMarketplaceId = anyProductInformationMarketplace.Id,
                                                        MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                                        MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                                        MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                        MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                                        TenantId = theTenant.Id,
                                                        DomainId = theDomain.Id,
                                                        CompanyId = theCompany.Id
                                                    });
                                                }
                                            }
                                            #endregion

                                            #region Category Mappings
                                            foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                            {
                                                if (anyProductInformationMarketplace
                                  .ProductInformationMarketplaceVariantValues
                                  .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                                  string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                                    continue;

                                                if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (mcLoop.MarketplaceId == "TY" || mcLoop.MarketplaceId == "PA"))
                                                {
                                                    var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                                    if (!_variantValueDictionary.ContainsKey(code))
                                                    {
                                                        var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                        {
                                                            MarketplaceId = mcLoop.MarketplaceId,
                                                            Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                            CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                            VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                                        }).Result;
                                                        if (!marketplaceCatalogVariantValueService.Success) continue;


                                                        _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                    }

                                                    if (_variantValueDictionary[code] == false) continue;
                                                }

                                                _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                                {
                                                    AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                                    Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                                    Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                                    Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                                    MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                                    MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                                    MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                    MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                                    TenantId = theTenant.Id,
                                                    DomainId = theDomain.Id,
                                                    CompanyId = theCompany.Id
                                                });

                                            }
                                            #endregion
                                        }
                                    }


                                }
                            }
                        }
                        try
                        {
                            _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
                            _unitOfWork.ProductInformationMarketplaceVariantValueRepository.BulkInsert(_productInformationMarketplaceVariantValues);
                            _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);

                            var ProductInformationMarketplaceIds = _productInformationMarketplaceVariantValues.GroupBy(x => x.ProductInformationMarketplaceId).Select(x => x.Key).ToList();
                            BulkUpdateDirtyProductInformation(connectionString, ProductInformationMarketplaceIds);
                            ProductInformationMarketplaceIds.AddRange(_productInformationMarketplaces.Select(x => x.Id).ToList());


                        }
                        catch (Exception ex)
                        {

                        }







                    }
                }
            }

        }
        private Response BulkUpdateDirtyProductInformationIkas(string connectionString, List<int> productInformationMarketplaceIds)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

                foreach (var theId in productInformationMarketplaceIds)
                {
                    var row = dataTable.NewRow();
                    row["ProductInformationMarketplaceId"] = theId;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkUpdatePim (
    ProductInformationMarketplaceId int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkUpdatePim";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 600;
                    command.CommandText = @"
UPDATE PIM2
SET
DirtyProductInformation=1
From		ProductInformationMarketplaces PIM WITH(NOLOCK)
JOIN		#TmpTableBulkUpdatePim T WITH(NOLOCK) ON T.ProductInformationMarketplaceId=PIM.Id
JOIN		ProductInformations PIN ON PIN.Id=PIM.ProductInformationId
JOIN		ProductInformations PIN2 ON PIN2.ProductId=PIN.ProductId
JOIN		ProductInformationMarketplaces PIM2 ON PIM2.ProductInformationId=PIN2.Id AND PIM2.CompanyId=@CompanyId
Where		PIM.DomainId = @DomainId And PIM.CompanyId  = @CompanyId And PIM2.MarketplaceId='IK'

Drop Table  #TmpTableBulkUpdatePim;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                    var dataReader = command.ExecuteNonQuery();


                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }
        private Response BulkUpdateDirtyProductInformation(string connectionString, List<int> productInformationMarketplaceIds)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

                foreach (var theId in productInformationMarketplaceIds)
                {
                    var row = dataTable.NewRow();
                    row["ProductInformationMarketplaceId"] = theId;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkUpdatePim (
    ProductInformationMarketplaceId int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkUpdatePim";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 600;
                    command.CommandText = @"
Update		PIM
set
DirtyProductInformation = 1
From		ProductInformationMarketplaces PIM WITH(NOLOCK)
JOIN		#TmpTableBulkUpdatePim T WITH(NOLOCK) ON T.ProductInformationMarketplaceId=PIM.Id
Where		PIM.DomainId = @DomainId And PIM.CompanyId  = @CompanyId And PIM.MarketplaceId <> 'IK'

Drop Table  #TmpTableBulkUpdatePim;";

                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    command.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();


                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        public DataTable GetDataTable(string connectionString, string selectCommandText)
        {
            DataTable dataTable = new();
            using (SqlDataAdapter sqlDataAdapter = new(selectCommandText, new SqlConnection(connectionString)))
            {
                sqlDataAdapter.Fill(dataTable);
            }
            return dataTable;
        }

        private void ProductUpdate(string connectionString, string dbName)
        {
            Console.WriteLine("ProductUpdate");

            //BulkUpdate(connectionString, StokRBKs);
        }

        private Response BulkStockPriceUpdate(string connectionString, List<NebimStokPrice> nebimStokPrices)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var deletedBarcodes = nebimStokPrices.GroupBy(x => x.Barcode).Select(x => new { Barcode = x.Key, C = x.Count() }).Where(x => x.C > 1).ToList();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Qty", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Price1", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("Price2", typeof(decimal)));

                foreach (var stokRBK in nebimStokPrices)
                {
                    if (deletedBarcodes.Any(x => x.Barcode == stokRBK.Barcode))
                    {
                        stokRBK.Qty = 0;
                        stokRBK.Price1 = 0;
                        stokRBK.Price2 = 0;
                    }

                    var row = dataTable.NewRow();
                    row["Barcode"] = stokRBK.Barcode;
                    row["Qty"] = stokRBK.Qty;
                    row["Price1"] = stokRBK.Price1;
                    row["Price2"] = stokRBK.Price2;
                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(connectionString))
                {
                    var temporaryTableName = $"_Update_NebimStokPrices_{Guid.NewGuid().ToString().Replace("-", "")}";

                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @$"
        Create Table {temporaryTableName} (
            Barcode    NVarChar(200) Not Null,
            Qty  Int   Not Null,
            Price1   Decimal(18,2) Not Null,
            Price2   Decimal(18,2) Not Null,
        )";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = temporaryTableName;
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @$"

SELECT PIN.Id,ISNULL(Envanter.qty,0) AS  Qty,PIN.Stock,PIN.Barcode INTO #TEMP_TABLE FROM
 ProductInformations PIN 
 JOIN Products P ON P.Id=PIN.ProductId
LEFT JOIN {temporaryTableName} Envanter ON PIN.Barcode=Envanter.Barcode
where  ISNULL(Envanter.Qty,0) != PIN.Stock AND P.AccountingProperty=1

INSERT INTO HelpyNebimStok
Select Qty,Stock,Id,Barcode,getdate() from #TEMP_TABLE

UPDATE 
    ProductInformationMarketplaces 
SET 
     DirtyStock=1
FROM 
ProductInformationMarketplaces M
INNER JOIN #TEMP_TABLE T ON M.ProductInformationId=T.Id

UPDATE 
    ProductInformations 
SET 
    Stock=T.Qty
FROM 
 ProductInformations PIN 
 INNER JOIN #TEMP_TABLE T ON T.Id=PIN.Id


UPDATE 
		   ProductInformationPriceses 
SET			
		   ListUnitPrice=T.Price1,
		   UnitPrice=T.Price2
From	   ProductInformationPriceses PP   --WEB SITESI
Inner Join ProductInformations P ON PP.ProductInformationId=P.Id
Inner Join {temporaryTableName} T ON T.Barcode=P.Barcode 
Where	     
(ListUnitPrice!=CAST(T.Price1 as decimal(18,2)) OR UnitPrice!=CAST(T.Price2 as decimal(18,2)))
And PP.AccountingPrice=1
	   

UPDATE 
		   ProductInformationMarketplaces 
SET 
		   ListUnitPrice=T.Price1,
		   UnitPrice=T.Price2,
		   DirtyPrice=1
From	   ProductInformationMarketplaces PP
Inner Join ProductInformations P ON PP.ProductInformationId=P.Id
Inner Join {temporaryTableName} T ON T.Barcode=P.Barcode 
Where	   PP.CompanyId=1 
		   AND (PP.ListUnitPrice!=CAST(T.Price1 as decimal(18,2)) OR PP.UnitPrice!=CAST(T.Price2 as decimal(18,2)))
		   And PP.AccountingPrice=1
           And PP.MarketplaceId<>'LCW'

		   UPDATE 
		   ProductInformationMarketplaces 
SET 
		   ListUnitPrice=T.Price1,
		   UnitPrice=T.Price1,
		   DirtyPrice=1
From	   ProductInformationMarketplaces PP
Inner Join ProductInformations P ON PP.ProductInformationId=P.Id
Inner Join {temporaryTableName} T ON T.Barcode=P.Barcode 
Where	   PP.CompanyId=1 
		   AND (PP.ListUnitPrice!=CAST(T.Price1 as decimal(18,2)) OR PP.UnitPrice!=CAST(T.Price1 as decimal(18,2)))
		   And PP.AccountingPrice=1
		   And PP.MarketplaceId='LCW'



DROP TABLE #TEMP_TABLE
Drop Table  {temporaryTableName};";

                    var dataReader = command.ExecuteNonQuery();
                }


                response.Success = true;

            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Bilinmeyen bir hata oluştu.";

                    conn?.Close();
                });
        }

    }
}