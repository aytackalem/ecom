﻿namespace ECommerce.Nebim.Transfer.Base
{
    public interface IApp
    {
        void Run(string dbName,string type);
    }
}
