﻿using Helpy.Services.MarketplaceCatalog.Core.Domain.Documents;
using Helpy.Services.MarketplaceCatalog.Persistence;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Diagnostics;

namespace ConsoleApp2
{
    internal class Program
    {
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddPersistenceServices(configuration);

            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
            //var mongoClient = new MongoClient("mongodb://localhost:27017");
            //var mongoDatabase = mongoClient.GetDatabase("MarketplaceCatalog");
            //var variantCollection = mongoDatabase.GetCollection<Variant>("Variants");
            //var valueCollection = mongoDatabase.GetCollection<Value>("Values");

            //var count = variantCollection.CountDocuments(Builders<Variant>.Filter.Empty);
            //for (int i = 0; i < count; i++)
            //{
            //    var variant = variantCollection.Find(Builders<Variant>.Filter.Empty).Skip(i).Limit(1).FirstOrDefault();

            //    var values = variant.VariantValues.Select(vv => new Value
            //    {
            //        Id = $"{variant.MarketplaceId}-{vv.Code}",
            //        Code = vv.Code,
            //        Name = vv.Name,
            //        Len = vv.Name.Length,
            //        MarketplaceId = variant.MarketplaceId,
            //        VariantCode = vv.VariantCode,
            //    });

            //    var k = 0;
            //    foreach (var theValue in values)
            //    {
            //        valueCollection.ReplaceOne(v => v.Id == theValue.Id, theValue, new UpdateOptions { IsUpsert = true });
            //        Console.WriteLine($"{i} {k}");
            //        k++;
            //    }


            //    //valueCollection
            //    //    .UpdateMany(variants.SelectMany(v => v
            //    //        .VariantValues
            //    //        .Select(vv => new Value
            //    //        {
            //    //            Code = vv.Code,
            //    //            Name = vv.Name,
            //    //            MarketplaceId = v.MarketplaceId,
            //    //            VariantCode = vv.VariantCode,
            //    //            Id = ObjectId.GenerateNewId().ToString()
            //    //        })));

            //    Console.WriteLine($"{i}");
            //}



            //var configurationRoot = GetConfiguration();
            //var serviceProvider = ConfigureServiceCollection(configurationRoot);
            //using (var scope = serviceProvider.CreateScope())
            //{
            //    //scope.ServiceProvider.GetRequiredService<>
            //}
        }
    }
}