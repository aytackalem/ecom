﻿using ECommerce.Application.Common.DataTransferObjects.Configs;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Infrastructure.Accounting.Mikro;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Invoice.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();

            var fileName = Process.GetCurrentProcess().MainModule.FileName;

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(fileName).FullName)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.AddInfrastructureServices();

            serviceCollection.Configure<AppConfig>(o => configuration.GetSection("AppConfig").Bind(o));

            serviceCollection.AddSingleton<IApp, App>();
            serviceCollection.AddScoped<IAccountingProvider, MikroProviderService>();
            serviceCollection.AddScoped<ICryptoHelper, CryptoHelper>();
            serviceCollection.AddScoped<ITenantFinder, TenantFinder>();
            serviceCollection.AddScoped<IDomainFinder, DomainFinder>();
            serviceCollection.AddScoped<ICompanyFinder, CompanyFinder>();

            var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(appConfig.Tenant.EcommerceConnectionStrings);
            });
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            return serviceCollection.BuildServiceProvider();
        }
    }
}














//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//public class Root
//{
//    public List<Category> subCategories { get; set; }
//}

//public class Category
//{
//    public int id { get; set; }
//    public string name { get; set; }
//    public int parentId { get; set; }
//    public List<Category> subCategories { get; set; }
//}

//class Program
//{
//    static async Task Main(string[] args)
//    {
//        Root root = null;

//        using (var httpClient = new HttpClient())
//        {
//            httpClient
//                .DefaultRequestHeaders
//                .Authorization = new AuthenticationHeaderValue(
//                        "Basic",
//                        Convert.ToBase64String(Encoding.UTF8.GetBytes($"M8dmuma1PgcSPxGitzem:W2U410btxIbYeEY3Li1b"))
//                    );
//            var httpResponseMessage = await httpClient.GetAsync($"https://api.trendyol.com/sapigw/product-categories");
//            var content = httpResponseMessage.Content;
//            var responseString = await content.ReadAsStringAsync();

//            try
//            {
//                root = JsonConvert.DeserializeObject<Root>(responseString);
//            }
//            catch (Exception e)
//            {
//            }
//        }

//        if (root != null)
//        {
//            var category = root.subCategories.FirstOrDefault(r => r.id == 1070);
//            if (category != null)
//            {
//                Recursive(category);
//            }
//        }
//    }

//    static void Recursive(Category category)
//    {
//        if (category.subCategories == null || category.subCategories.Count == 0)
//        {
//            //TO DO: Leaf

//        }

//        foreach (var scLoop in category.subCategories)
//        {
//            Recursive(scLoop);
//        }
//    }
//}

//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//namespace ECommerce.TrendyolFetch
//{
//    public class Image
//    {
//        public string url { get; set; }
//    }

//    public class Attribute
//    {
//        public int attributeId { get; set; }
//        public string attributeName { get; set; }
//        public int attributeValueId { get; set; }
//        public string attributeValue { get; set; }
//    }

//    public class Content
//    {
//        public string id { get; set; }
//        public bool approved { get; set; }
//        public bool archived { get; set; }
//        public int productCode { get; set; }
//        public string batchRequestId { get; set; }
//        public int supplierId { get; set; }
//        public long createDateTime { get; set; }
//        public long lastUpdateDate { get; set; }
//        public string gender { get; set; }
//        public string brand { get; set; }
//        public string barcode { get; set; }
//        public string title { get; set; }
//        public string categoryName { get; set; }
//        public string productMainId { get; set; }
//        public string description { get; set; }
//        public string stockUnitType { get; set; }
//        public int quantity { get; set; }
//        public double listPrice { get; set; }
//        public double salePrice { get; set; }
//        public int vatRate { get; set; }
//        public double dimensionalWeight { get; set; }
//        public string stockCode { get; set; }
//        public List<Image> images { get; set; }
//        public List<Attribute> attributes { get; set; }
//        public string platformListingId { get; set; }
//        public string stockId { get; set; }
//        public bool hasActiveCampaign { get; set; }
//        public bool locked { get; set; }
//        public int productContentId { get; set; }
//        public int pimCategoryId { get; set; }
//        public int brandId { get; set; }
//        public int version { get; set; }
//        public string color { get; set; }
//        public string size { get; set; }
//        public bool lockedByUnSuppliedReason { get; set; }
//        public bool onsale { get; set; }
//    }

//    public class Root
//    {
//        public int totalElements { get; set; }
//        public int totalPages { get; set; }
//        public int page { get; set; }
//        public int size { get; set; }
//        public List<Content> content { get; set; }
//    }

//    class Program
//    {
//        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

//        static async Task Main(string[] args)
//        {
//            var content = await GetAsync();

//            Products(content);
//            Categories(content);
//            CategoryAttributes(content);
//            CategoryAttributeValues(content);
//            ProductCategoryAttributeValue(content);
//        }

//        static async Task<List<Content>> GetAsync()
//        {
//            List<Content> list = new();

//            int size = 50;
//            int page = 0;
//            int pagesCount = 0;

//            do
//            {
//                using (HttpClient httpClient = new HttpClient())
//                {
//                    httpClient
//                        .DefaultRequestHeaders
//                        .Authorization = new AuthenticationHeaderValue(
//                                "Basic",
//                                Convert.ToBase64String(Encoding.UTF8.GetBytes($"M8dmuma1PgcSPxGitzem:W2U410btxIbYeEY3Li1b"))
//                            );
//                    var httpResponseMessage = await httpClient
//                        .GetAsync($"https://api.trendyol.com/sapigw/suppliers/114231/products?approved=true&page={page}&size={size}");
//                    HttpContent content = httpResponseMessage.Content;
//                    string responseString = await content.ReadAsStringAsync();

//                    Root root = null;
//                    try
//                    {
//                        root = JsonConvert.DeserializeObject<Root>(responseString);
//                    }
//                    catch (Exception e)
//                    {
//                    }

//                    list.AddRange(root.content);

//                    pagesCount = root.totalPages;

//                    page++;

//                    Console.WriteLine(page);
//                }
//            } while (page < pagesCount);

//            return list;
//        }

//        static void Products(List<Content> products)
//        {
//            DataTable dataTable = new();
//            dataTable.Columns.Add("Id", typeof(string));
//            dataTable.Columns.Add("ProductCode", typeof(int));
//            dataTable.Columns.Add("Barcode", typeof(string));
//            dataTable.Columns.Add("Title", typeof(string));
//            dataTable.Columns.Add("CategoryName", typeof(string));
//            dataTable.Columns.Add("ProductMainId", typeof(string));
//            dataTable.Columns.Add("Description", typeof(string));
//            dataTable.Columns.Add("PimCategoryId", typeof(int));

//            products
//                .Select(p => new
//                {
//                    Id = p.id,
//                    ProductCode = p.productCode,
//                    Barcode = p.barcode,
//                    Title = p.title,
//                    CategoryName = p.categoryName,
//                    ProductMainId = p.productMainId,
//                    Description = p.description,
//                    PimCategoryId = p.pimCategoryId
//                })
//                .ToList()
//                .ForEach(p =>
//                {
//                    DataRow dataRow = dataTable.NewRow();
//                    dataRow[0] = p.Id;
//                    dataRow[1] = p.ProductCode;
//                    dataRow[2] = p.Barcode;
//                    dataRow[3] = p.Title;
//                    dataRow[4] = p.CategoryName;
//                    dataRow[5] = p.ProductMainId;
//                    dataRow[6] = p.Description;
//                    dataRow[7] = p.PimCategoryId;

//                    dataTable.Rows.Add(dataRow);
//                });

//            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
//            {
//                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
//                sqlBulkCopy.DestinationTableName = "TYProducts";

//                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
//                sqlBulkCopy.ColumnMappings.Add("ProductCode", "ProductCode");
//                sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");
//                sqlBulkCopy.ColumnMappings.Add("Title", "Title");
//                sqlBulkCopy.ColumnMappings.Add("CategoryName", "CategoryName");
//                sqlBulkCopy.ColumnMappings.Add("ProductMainId", "ProductMainId");
//                sqlBulkCopy.ColumnMappings.Add("Description", "Description");
//                sqlBulkCopy.ColumnMappings.Add("PimCategoryId", "PimCategoryId");

//                try
//                {
//                    sqlConnection.Open();

//                    sqlBulkCopy.WriteToServer(dataTable);
//                }
//                catch (Exception e)
//                {

//                }
//                finally
//                {
//                    sqlConnection.Close();
//                }
//            }
//        }

//        static void Categories(List<Content> products)
//        {
//            DataTable dataTable = new();
//            dataTable.Columns.Add("Id", typeof(int));
//            dataTable.Columns.Add("Name", typeof(string));

//            products
//                .Select(p => new
//                {
//                    Id = p.pimCategoryId,
//                    Name = p.categoryName
//                })
//                .Distinct()
//                .ToList()
//                .ForEach(p =>
//                {
//                    DataRow dataRow = dataTable.NewRow();
//                    dataRow[0] = p.Id;
//                    dataRow[1] = p.Name;

//                    dataTable.Rows.Add(dataRow);
//                });

//            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
//            {
//                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
//                sqlBulkCopy.DestinationTableName = "TYCategories";

//                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
//                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

//                try
//                {
//                    sqlConnection.Open();

//                    sqlBulkCopy.WriteToServer(dataTable);
//                }
//                catch (Exception e)
//                {

//                }
//                finally
//                {
//                    sqlConnection.Close();
//                }
//            }
//        }

//        static void CategoryAttributes(List<Content> products)
//        {
//            DataTable dataTable = new();
//            dataTable.Columns.Add("CategoryId", typeof(int));
//            dataTable.Columns.Add("Id", typeof(int));
//            dataTable.Columns.Add("Name", typeof(string));

//            products
//                .SelectMany(p => p.attributes.Select(a => new
//                {
//                    CategoryId = p.pimCategoryId,
//                    Id = a.attributeId,
//                    Name = a.attributeName
//                }))
//                .Distinct()
//                .ToList()
//                .ForEach(p =>
//                {
//                    DataRow dataRow = dataTable.NewRow();
//                    dataRow[0] = p.CategoryId;
//                    dataRow[1] = p.Id;
//                    dataRow[2] = p.Name;

//                    dataTable.Rows.Add(dataRow);
//                });

//            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
//            {
//                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
//                sqlBulkCopy.DestinationTableName = "TYCategoryAttributes";

//                sqlBulkCopy.ColumnMappings.Add("CategoryId", "CategoryId");
//                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
//                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

//                try
//                {
//                    sqlConnection.Open();

//                    sqlBulkCopy.WriteToServer(dataTable);
//                }
//                catch (Exception e)
//                {

//                }
//                finally
//                {
//                    sqlConnection.Close();
//                }
//            }
//        }

//        static void CategoryAttributeValues(List<Content> products)
//        {
//            DataTable dataTable = new();
//            dataTable.Columns.Add("Id", typeof(int));
//            dataTable.Columns.Add("CategoryAttributeId", typeof(int));
//            dataTable.Columns.Add("Name", typeof(string));

//            products
//                .SelectMany(p => p.attributes.Select(a => new
//                {
//                    Id = a.attributeValueId,
//                    CategoryAttributeId = a.attributeId,
//                    Name = a.attributeValue
//                }))
//                .Distinct()
//                .ToList()
//                .ForEach(a =>
//                {
//                    DataRow dataRow = dataTable.NewRow();
//                    dataRow[0] = a.Id;
//                    dataRow[1] = a.CategoryAttributeId;
//                    dataRow[2] = string.IsNullOrEmpty(a.Name) ? DBNull.Value : a.Name;

//                    dataTable.Rows.Add(dataRow);
//                });

//            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
//            {
//                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
//                sqlBulkCopy.DestinationTableName = "TYCategoryAttributeValues";

//                sqlBulkCopy.ColumnMappings.Add("Id", "Id");
//                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeId", "CategoryAttributeId");
//                sqlBulkCopy.ColumnMappings.Add("Name", "Name");

//                try
//                {
//                    sqlConnection.Open();

//                    sqlBulkCopy.WriteToServer(dataTable);
//                }
//                catch (Exception e)
//                {

//                }
//                finally
//                {
//                    sqlConnection.Close();
//                }
//            }
//        }

//        static void ProductCategoryAttributeValue(List<Content> products)
//        {
//            DataTable dataTable = new();
//            dataTable.Columns.Add("ProductId", typeof(string));
//            dataTable.Columns.Add("CategoryAttributeValueId", typeof(int));
//            dataTable.Columns.Add("CategoryAttributeValue", typeof(string));

//            products
//                .SelectMany(p => p.attributes.Select(a => new
//                {
//                    ProductId = p.id,
//                    CategoryAttributeValueId = a.attributeValueId,
//                    CategoryAttributeValue = a.attributeValue
//                }))
//                .Distinct()
//                .ToList()
//                .ForEach(a =>
//                {
//                    DataRow dataRow = dataTable.NewRow();
//                    dataRow[0] = a.ProductId;
//                    dataRow[1] = a.CategoryAttributeValueId;
//                    dataRow[2] = string.IsNullOrEmpty(a.CategoryAttributeValue) ? DBNull.Value : a.CategoryAttributeValue;

//                    dataTable.Rows.Add(dataRow);
//                });

//            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
//            {
//                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection);
//                sqlBulkCopy.DestinationTableName = "TYProductCategoryAttributeValues";

//                sqlBulkCopy.ColumnMappings.Add("ProductId", "ProductId");
//                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeValueId", "CategoryAttributeValueId");
//                sqlBulkCopy.ColumnMappings.Add("CategoryAttributeValue", "CategoryAttributeValue");

//                try
//                {
//                    sqlConnection.Open();

//                    sqlBulkCopy.WriteToServer(dataTable);
//                }
//                catch (Exception e)
//                {

//                }
//                finally
//                {
//                    sqlConnection.Close();
//                }
//            }
//        }
//    }
//}
