﻿namespace ECommerce.Invoice.Job
{
    public interface IApp
    {
        #region Methods
        void Run(); 
        #endregion
    }
}
