﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace ECommerce.Invoice.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IAccountingProviderService _accountingService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IAccountingProviderService accountingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._accountingService = accountingService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            /* Faturası kesilmiş siparişleri CSV ye aktarım için */
            //var orders2 = this
            //._unitOfWork
            //.OrderRepository
            //.DbSet()
            //.Include(o => o.OrderInvoiceInformation)
            //.Include(o => o.OrderDetails)
            //.Where(o => o.OrderTypeId == "TA")
            //.ToList();

            //string a = string.Join("\n", orders2.Select(o => $"{o.MarketplaceOrderNumber};{o.OrderInvoiceInformation.FirstName};{o.OrderInvoiceInformation.LastName};{o.OrderDetails.Sum(od => od.Quantity * od.VatExcUnitPrice)};{o.OrderDetails.Sum(od => od.Quantity * (od.UnitPrice - od.VatExcUnitPrice))};"));

            var orders = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderInvoiceInformation)
                .Include(o => o.OrderDetails)
                .Where(o => o.OrderTypeId == OrderTypes.TeslimEdildi &&
                            (
                                o.OrderSourceId == OrderSources.Pazaryeri
                            )
                      )
                .ToList();

            StringBuilder stringBuilder = new();

            Console.WriteLine(orders.Count);
            int counter = 0;
            foreach (var oLoop in orders)
            {
                counter++;
                Console.WriteLine(counter);
                /*
                 * Kontroller
                 * 
                 * 1. PSF SF den küçük olamaz.
                 */

                if (oLoop.OrderDetails.Any(od => od.ListUnitPrice < od.UnitPrice))
                {
                    //TO DO: Email gönder
                    continue;
                }

                /*
                 * 2. KDV hesaplaması
                 */
                if (oLoop.OrderDetails.Any(od => od.VatExcListUnitPrice != Math.Round(od.ListUnitPrice / (od.VatRate + 1M), 4) ||
                                                 od.VatExcUnitPrice != Math.Round(od.UnitPrice / (od.VatRate + 1M), 4)))
                {
                    //TO DO: Email gönder
                    continue;
                }

                foreach (var odLoop in oLoop.OrderDetails)
                {
                    if (odLoop.VatExcListUnitPrice != Math.Round(odLoop.ListUnitPrice / (odLoop.VatRate + 1M), 4) ||
                        odLoop.VatExcUnitPrice != Math.Round(odLoop.UnitPrice / (odLoop.VatRate + 1M), 4))
                        continue;
                }

                if (oLoop.OrderDetails.Any(od => od.VatRate < 0.18M))
                {
                    continue;
                }

                var createResponse = this._accountingService.CreateInvoice(oLoop.Id);
                if (createResponse.Success)
                {
                    oLoop.OrderBilling = new Domain.Entities.OrderBilling
                    {
                        CreatedDate = DateTime.Now,
                        InvoiceNumber = createResponse.Data.Guid
                    };
                    oLoop.OrderTypeId = "TA";
                    this._unitOfWork.OrderRepository.Update(oLoop);

                    //File.AppendAllLines($@"D:\Invoices-Sinoz-{DateTime.Now.ToString("dddd-MM-HH")}.txt", new List<string> { $"{oLoop.MarketplaceOrderNumber};{oLoop.OrderInvoiceInformation.FirstName};{oLoop.OrderInvoiceInformation.LastName};{oLoop.OrderDetails.Sum(od => od.Quantity * od.VatExcUnitPrice)};{oLoop.OrderDetails.Sum(od => od.Quantity * (od.UnitPrice - od.VatExcUnitPrice))};{oLoop.OrderBilling.InvoiceNumber}" });

                    this._accountingService.DeleteStockTransfer(oLoop.Id);

                    Console.WriteLine("Success");
                }
                else
                {
                    //TO DO: Email gönder
                }
            }
        }
        #endregion
    }
}
