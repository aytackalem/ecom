﻿namespace Helpy.Responses
{
    public enum ResponseCodes
    {
        Ok = 200,
        Created = 201,
        Bad = 400,
        Error = 500
    }
}
