﻿namespace Helpy.Responses
{
    public class PagingResponse<T> : Response<List<T>>
    {
        public int Page { get; set; }

        public int PageRecordsCount { get; set; }

        public int PagesCount { get; set; }

        public int RecordsCount { get; set; }

        public PagingResponse(ResponseCodes code, string message, int page, int recordsCount, int pageRecordsCount, List<T> data) : this(code, page, recordsCount, pageRecordsCount, data)
        {
            this.Message = message;
        }

        public PagingResponse(ResponseCodes code, int page, int recordsCount, int pageRecordsCount, List<T> data) : base(code, data)
        {
            this.Page = page;
            this.RecordsCount = recordsCount;
            this.PageRecordsCount = pageRecordsCount;

            if (this.RecordsCount == 0 || this.PageRecordsCount == 0)
                return;

            this.PagesCount = Convert.ToInt32(Math.Ceiling(this.RecordsCount / (decimal)this.PageRecordsCount));
        }
    }
}
