﻿namespace Helpy.Responses
{
    public class Response
    {
        public ResponseCodes Code { get; set; }

        public string Message { get; set; }

        public Response(ResponseCodes code, string message) : this(code)
        {
            this.Message = message;
        }

        public Response(ResponseCodes code)
        {
            this.Code = code;
        }
    }
}