﻿namespace Helpy.Responses
{
    public class Response<T> : Response
    {
        public T Data { get; set; }

        public Response(ResponseCodes code, string message, T data) : this(code, data)
        {
            this.Message = message;
        }

        public Response(ResponseCodes code, T data) : base(code)
        {
            this.Data = data;
        }
    }
}