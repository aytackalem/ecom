﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Photo.Job
{
    internal class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IConfiguration>(c => configuration);

            serviceCollection.AddDbContext<ApplicationDbContext>();

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddScoped<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddScoped<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddScoped<ICompanyFinder>(cf => null);

            serviceCollection.AddTransient<IConfiguration>(x => configuration);

            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddScoped<IFtpHelper, FtpHelper>();

            serviceCollection.AddScoped<App>();

            return serviceCollection.BuildServiceProvider();
        }

        static void Main(string[] args)
        {
#if DEBUG
            args = new string[] { "Ebsumu", "ebsumuimg", "erp\\wwwroot\\img\\Ebsumu\\product" };
            //args = new string[] { "Lafaba", "lafabaimg", "Backoffice\\wwwroot\\img\\product" };
            //args = new string[] { "Moonsea", "moonseaimg", "erp\\wwwroot\\img\\moonsea\\product" };
            //args = new string[] { "Lafaba", "CodeDB_2" };
#endif

            ConfigureServiceCollection(GetConfiguration())
                .GetRequiredService<App>()
                .Run(args[0], args[1], args[2]);
        }
        #endregion
    }
}