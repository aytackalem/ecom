﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Parameters.FtpHelper;
using ECommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using System.Text;

namespace ECommerce.Photo.Job
{
    public class App
    {
        #region Fields
        IDbNameFinder _dbNameFinder;

        ITenantFinder _tenantFinder;

        IDomainFinder _domainFinder;

        IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public App(IDbNameFinder dbNameFinder, ITenantFinder tenantFinder, IDomainFinder domainFinder, IUnitOfWork unitOfWork)
        {
            #region Fields
            this._dbNameFinder = dbNameFinder;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._unitOfWork = unitOfWork;
            #endregion
        }
        #endregion

        #region Methods
        public void Run(string dbName, string sourcePath, string targetPath)
        {
            this._dbNameFinder.Set(dbName);
            (this._tenantFinder as DynamicTenantFinder)._tenantId = 1;
            (this._domainFinder as DynamicDomainFinder)._domainId = 1;

            sourcePath = @$"\\172.31.57.21\c$\inetpub\{sourcePath}";
            targetPath = @$"\\172.31.57.21\c$\inetpub\Helpy\{targetPath}";

#if DEBUG
            sourcePath = @"C:\Source";
            targetPath = @"C:\Target\product";
#endif

            var directories = Directory.GetDirectories(sourcePath);
            foreach (var theDirectory in directories)
            {
                var skuCode = Path.GetFileName(theDirectory);

                var productInformations = this
                    ._unitOfWork
                    .ProductInformationRepository
                    .DbSet()
                    .Include(pi => pi.ProductInformationPhotos)
                    .Where(pi => pi.SkuCode == skuCode && pi.Active)
                    .ToList();

                if (productInformations.HasItem())
                {
                    var files = Directory.GetFiles(theDirectory);

                    /* Klasor icerisinde "jpg" disinda bir dosya uzantisi olup/olmadigi kontrol ediliyor. */
                    if (files.Any(f => !f.EndsWith(".jpg")))
                    {
                        /* Klasor icerisindeki "jpg" olmayan dosyalar siliniyor. */
                        var nonImageFiles = files.Where(f => !f.EndsWith(".jpg")).ToArray();
                        for (int i = 0; i < nonImageFiles.Length; i++)
                            File.Delete(nonImageFiles[i]);

                        /* Silme sonrasi dosyalar icin tekrar okuma yapiliyor. */
                        files = Directory.GetFiles(theDirectory);
                    }

                    if (files.Length > 0)
                    {
                        foreach (var theProductInformation in productInformations)
                        {
                            theProductInformation.ProductInformationPhotos = new();
                            theProductInformation.ProductInformationPhotos = files
                                .Select(f =>
                                {
                                    /* 
                                     * - ile split islemi yapildigindan bu degisiklik eklenmistir.
                                     * 
                                     * Test edilen inputlar:
                                     * ASD-1R08-1.jpg
                                     * ASD-ASDAUDY-ASDSA-1R08-22.jpg
                                     * ASD-1R08-SA7UHH-333.jpg
                                     */

                                    int startIndex = -1;
                                    int endIndex = -1;
                                    do
                                    {
                                        startIndex = f.IndexOf("-", startIndex + 1);
                                    } while (startIndex > -1 && f.IndexOf("-", startIndex + 1) > startIndex);

                                    endIndex = f.IndexOf(".", startIndex + 1);

                                    var displayOrderString = f.Substring(startIndex + 1, endIndex - 1 - startIndex);

                                    return new ProductInformationPhoto
                                    {
                                        DisplayOrder = int.Parse(displayOrderString),
                                        FileName = f.Replace(sourcePath, "").TrimStart('\\').Replace("\\", "/"),
                                        CreatedDate = DateTime.Now
                                    };
                                })
                                .ToList();

                            this
                                ._unitOfWork
                                .ProductInformationRepository
                                .Update(theProductInformation);
                        }

                        this
                            ._unitOfWork
                            .ProductInformationMarketplaceRepository
                            .UpdateDirtyProductInformationByProductId(productInformations[0].ProductId);

                        var path = theDirectory.Replace(sourcePath, targetPath);

                        if (Directory.Exists(path))
                            Directory.Delete(path, true);

                        Directory.Move(theDirectory, path);

                        /* Thumb */
                        //var thumbPath = path.Replace("\\product\\", "\\product\\t\\");

                        //if (Directory.Exists(thumbPath))
                        //    Directory.Delete(thumbPath, true);

                        //Directory.CreateDirectory(thumbPath);

                        //var copiedFiles = Directory.GetFiles(path, "*.jpg");

                        //foreach (var theCopiedFile in copiedFiles)
                        //    using (var image = Image.FromFile(theCopiedFile))
                        //    {
                        //        var thumb = image.GetThumbnailImage(image.Width / 10, image.Height / 10, () => false, IntPtr.Zero);
                        //        var fileName = theCopiedFile.Replace("\\product\\", "\\product\\t\\");
                        //        thumb.Save(fileName);
                        //    }
                    }
                }
            }
        }
        #endregion
    }
}
