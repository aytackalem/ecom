﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Pazarama.Common.CategoryAttributes
{
    public class CategoryAttribute
    {
        #region Properties
        public string id { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public List<Attribute> attributes { get; set; } 
        #endregion
    }
}
