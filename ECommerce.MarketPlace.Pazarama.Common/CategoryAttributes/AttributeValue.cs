﻿namespace ECommerce.MarketPlace.Pazarama.Common.CategoryAttributes
{
    public class AttributeValue
    {
        #region Properties
        public string id { get; set; }
        
        public string value { get; set; } 
        #endregion
    }
}
