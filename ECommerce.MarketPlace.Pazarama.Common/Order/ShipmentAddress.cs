﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Order
{
    public class ShipmentAddress
    {
        public string addressId { get; set; }
        public string title { get; set; }
        public string nameSurname { get; set; }
        public string customerEmail { get; set; }
        public string cityName { get; set; }
        public string districtName { get; set; }
        public string neighborhoodName { get; set; }
        public string addressDetail { get; set; }
        public string displayAddressText { get; set; }
        public string phoneNumber { get; set; }
    }
}
