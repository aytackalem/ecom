﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Order
{
    public class OrderRequest
    {
        public int? orderNumber { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int orderStatus { get; set; }
    }
}
