﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Order
{

    public class OrderResponse
    {
        public List<Datum> data { get; set; }
        public bool success { get; set; }
        public string messageCode { get; set; }
        public string message { get; set; }
        public object userMessage { get; set; }
    }

}
