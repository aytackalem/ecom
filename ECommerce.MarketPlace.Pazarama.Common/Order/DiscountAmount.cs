﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Order
{
    public class DiscountAmount
    {
        public double value { get; set; }
        public int valueInt { get; set; }
        public string valueString { get; set; }
        public string currency { get; set; }
    }
}
