﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Pazarama.Common.Categories
{
    public class CategoryResponse
    {
        #region Properties
        public List<Category> data { get; set; }
        #endregion
    }
}
