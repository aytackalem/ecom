﻿using System.Collections.Generic;

namespace ECommerce.MarketPlace.Pazarama.Common.Categories
{
    public class Category
    {
        #region Properties
        public string id { get; set; }
        
        public string parentId { get; set; }
        
        public List<string> parentCategories { get; set; }
        
        public string name { get; set; }
        
        public string displayName { get; set; }
        
        public int displayOrder { get; set; }
        
        public string description { get; set; }
        
        public bool leaf { get; set; }
        #endregion
    }
}
