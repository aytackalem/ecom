﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.CargoTraking
{
    public class CargoTraking
    {
        public long orderNumber { get; set; }
        public Item item { get; set; }
    }
}
