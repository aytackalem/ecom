﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.CargoTraking
{
    public class Item
    {
        public Guid orderItemId { get; set; }
        public int status { get; set; }
        public int deliveryType { get; set; }
        public string shippingTrackingNumber { get; set; }
        public string trackingUrl { get; set; }
        public Guid cargoCompanyId { get; set; }
    }

}
