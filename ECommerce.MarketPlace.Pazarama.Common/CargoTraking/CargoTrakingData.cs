﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.CargoTraking
{
    public class CargoTrakingData
    {
        public bool success { get; set; }
        public object messageCode { get; set; }
        public object message { get; set; }
    }
}
