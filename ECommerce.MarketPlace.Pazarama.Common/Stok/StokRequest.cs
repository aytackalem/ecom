﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Stok
{


    public class Stok
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("stockCount")]
        public int StockCount { get; set; }
    }

    public class StokRequest
    {

        [JsonProperty("items")]
        public List<Stok> Items { get; set; }
    }
}
