﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.Pazarama.Common.Token
{
    public class Data
    {
        public string accessToken { get; set; }
        public object refreshToken { get; set; }
        public int expiresIn { get; set; }
        public string tokenType { get; set; }
        public string scope { get; set; }
    }

    public class Token
    {
        public bool success { get; set; }
        public string messageCode { get; set; }
        public string message { get; set; }
        public string userMessage { get; set; }
        public Data data { get; set; }
    }
}
