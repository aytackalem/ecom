﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Campaign.Job
{
    #region Data Transfer Objects
    abstract class DiscountSetting
    {
        public int Id { get; set; }

        public bool DiscountIsRate { get; set; }

        public decimal Discount { get; set; }
    }

    class EntireDiscountSetting : DiscountSetting
    {
        public decimal Limit { get; set; }
    }

    class CategoryDiscountSetting : DiscountSetting
    {
        public int CategoryId { get; set; }
    }

    class BrandDiscountSetting : DiscountSetting
    {

    }

    class ProductInformationPrice
    {
        public int ProductId { get; set; }

        public int CategoryId { get; set; }

        public int Id { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal? OriginalUnitPrice { get; set; }

        public decimal Discount => Math.Round(100 - (this.UnitPrice * 100 / this.ListUnitPrice));
    }
    #endregion

    #region Calculators
    interface ICalculator
    {
        void Apply(ProductInformationPrice productInformationPrice);
    }

    class EntireDiscountCalculator : ICalculator
    {
        EntireDiscountSetting _entireDiscountSetting;

        CategoryDiscountCalculator _categoryDiscountCalculator;

        public EntireDiscountCalculator(EntireDiscountSetting entireDiscountSetting, CategoryDiscountCalculator categoryDiscountCalculator)
        {
            this._entireDiscountSetting = entireDiscountSetting;
            this._categoryDiscountCalculator = categoryDiscountCalculator;
        }

        public void Apply(ProductInformationPrice productInformationPrice)
        {
            if (this._entireDiscountSetting != null)
                if (this._entireDiscountSetting.DiscountIsRate)
                {
                    if ((productInformationPrice.Discount / 100m) < this._entireDiscountSetting.Discount)
                    {
                        var unitPrice = productInformationPrice.ListUnitPrice - (productInformationPrice.ListUnitPrice * this._entireDiscountSetting.Discount);
                        productInformationPrice.UnitPrice = unitPrice;
                    }
                }
                else
                {
                    productInformationPrice.UnitPrice -= this._entireDiscountSetting.Discount;
                }

            this._categoryDiscountCalculator.Apply(productInformationPrice);
        }
    }

    class CategoryDiscountCalculator : ICalculator
    {
        List<CategoryDiscountSetting> _categoryDiscountSettings;

        BrandDiscountCalculator _brandDiscountCalculator;

        public CategoryDiscountCalculator(List<CategoryDiscountSetting> categoryDiscountSettings, BrandDiscountCalculator brandDiscountCalculator)
        {
            this._categoryDiscountSettings = categoryDiscountSettings;
            this._brandDiscountCalculator = brandDiscountCalculator;
        }

        public void Apply(ProductInformationPrice productInformationPrice)
        {
            var categoryDiscountSetting = this._categoryDiscountSettings.FirstOrDefault(cds => cds.CategoryId == productInformationPrice.CategoryId);

            if (categoryDiscountSetting != null)
                if (categoryDiscountSetting.DiscountIsRate)
                {
                    if ((productInformationPrice.Discount / 100m) < (categoryDiscountSetting.Discount * 100))
                    {
                        var unitPrice = productInformationPrice.ListUnitPrice - (productInformationPrice.ListUnitPrice * categoryDiscountSetting.Discount);
                        productInformationPrice.UnitPrice = unitPrice;
                    }
                }
                else
                {
                    productInformationPrice.UnitPrice -= categoryDiscountSetting.Discount;
                }

            this._brandDiscountCalculator.Apply(productInformationPrice);
        }
    }

    class BrandDiscountCalculator : ICalculator
    {
        BrandDiscountSetting _brandDiscountSetting;

        public BrandDiscountCalculator(BrandDiscountSetting brandDiscountSetting)
        {
            this._brandDiscountSetting = brandDiscountSetting;
        }

        public void Apply(ProductInformationPrice productInformationPrice)
        {
            if (this._brandDiscountSetting != null)
                if (this._brandDiscountSetting.DiscountIsRate)
                {
                    if (productInformationPrice.Discount < this._brandDiscountSetting.Discount)
                    {
                        var unitPrice = productInformationPrice.ListUnitPrice - (productInformationPrice.ListUnitPrice * this._brandDiscountSetting.Discount);
                        productInformationPrice.UnitPrice = unitPrice;
                    }
                }
                else
                {
                    productInformationPrice.UnitPrice -= this._brandDiscountSetting.Discount;
                }
        }
    }
    #endregion

    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static void Main(string[] args)
        {
            EntireDiscountSetting entireDiscountSetting = null;

            /* Entire Discount Setting */
            using (SqlConnection sqlConnection = new(_connectionString))
            {
                var query = @"
Select	ED.Id, EDS.DiscountIsRate, EDS.Discount, EDS.Limit
From	EntireDiscounts As ED With(NoLock)
Join	EntireDiscountSettings As EDS With(NoLock)
On		ED.Id = EDS.Id
Where	GetDate() Between ED.StartDate And ED.EndDate
        And Active=1 
        And Deleted=0";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            entireDiscountSetting = new EntireDiscountSetting
                            {
                                Id = sqlDataReader.GetInt32(0),
                                DiscountIsRate = sqlDataReader.GetBoolean(1),
                                Discount = sqlDataReader.GetDecimal(2),
                                Limit = sqlDataReader.GetDecimal(3)
                            };
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            List<CategoryDiscountSetting> categoryDiscountSettings = new();

            /* Category Discount Setting */
            using (SqlConnection sqlConnection = new(_connectionString))
            {
                var query = @"
Select	Id, DiscountIsRate, Discount, CategoryId
From	CategoryDiscountSettings With(NoLock)
Where	GetDate() Between StartDate And EndDate
        And Active=1
        And Deleted=0";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            categoryDiscountSettings.Add(new CategoryDiscountSetting
                            {
                                Id = sqlDataReader.GetInt32(0),
                                DiscountIsRate = sqlDataReader.GetBoolean(1),
                                Discount = sqlDataReader.GetDecimal(2),
                                CategoryId = sqlDataReader.GetInt32(3),
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            BrandDiscountSetting brandDiscountSetting = null;

            /* Brand Discount Setting */
            using (SqlConnection sqlConnection = new(_connectionString))
            {
                var query = @"
Select	Id, DiscountIsRate, Discount
From	BrandDiscountSettings
Where	GetDate() Between StartDate And EndDate
        And Active=1
        And Deleted=0";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            brandDiscountSetting = new BrandDiscountSetting
                            {
                                Id = sqlDataReader.GetInt32(0),
                                DiscountIsRate = sqlDataReader.GetBoolean(1),
                                Discount = sqlDataReader.GetDecimal(2)
                            };
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            List<ProductInformationPrice> productInformationPrices = new List<ProductInformationPrice>();

            /* Product Information Prices */
            using (SqlConnection sqlConnection = new(_connectionString))
            {
                var query = @"
Select	P.Id As ProductId, P.CategoryId, [PIP].Id, [PIP].ListUnitPrice, [PIP].UnitPrice, [PIP].OriginalUnitPrice
From	Products As P With(NoLock)
Join	ProductInformations As [PI] With(NoLock)
On		P.Id = [PI].ProductId
Join	ProductInformationPriceses As [PIP] With(NoLock)
On		[PI].Id = [PIP].ProductInformationId
Where   P.Active=1
        And P.IsOnlyPanel<>1";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        while (sqlDataReader.Read())
                        {
                            productInformationPrices.Add(new ProductInformationPrice
                            {
                                ProductId = sqlDataReader.GetInt32(0),
                                CategoryId = sqlDataReader.GetInt32(1),
                                Id = sqlDataReader.GetInt32(2),
                                ListUnitPrice = sqlDataReader.GetDecimal(3),
                                UnitPrice = sqlDataReader.GetDecimal(4),
                                OriginalUnitPrice = sqlDataReader["OriginalUnitPrice"] == DBNull.Value ? null : sqlDataReader.GetDecimal(5)
                            });
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            var brandDiscountCalculator = new BrandDiscountCalculator(brandDiscountSetting);
            var categoryDiscountCalculator = new CategoryDiscountCalculator(categoryDiscountSettings, brandDiscountCalculator);
            var entireDiscountCalculator = new EntireDiscountCalculator(entireDiscountSetting, categoryDiscountCalculator);

            DataTable dataTable = new();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("ListUnitPrice", typeof(decimal));
            dataTable.Columns.Add("UnitPrice", typeof(decimal));
            dataTable.Columns.Add("OriginalUnitPrice", typeof(decimal));

            productInformationPrices.ForEach(pip =>
            {
                if (pip.OriginalUnitPrice.HasValue)
                    pip.UnitPrice = pip.OriginalUnitPrice.Value;
                else
                    pip.OriginalUnitPrice = pip.UnitPrice;

                entireDiscountCalculator.Apply(pip);

                if (pip.UnitPrice == pip.OriginalUnitPrice)
                    pip.OriginalUnitPrice = null;

                DataRow dataRow = dataTable.NewRow();
                dataRow[0] = pip.Id;
                dataRow[1] = pip.ListUnitPrice;
                dataRow[2] = pip.UnitPrice;
                dataRow[3] = pip.OriginalUnitPrice.HasValue ? pip.OriginalUnitPrice.Value : DBNull.Value;

                dataTable.Rows.Add(dataRow);
            });

            using (SqlConnection sqlConnection = new(_connectionString))
            {
                var query = @"
Create Table #CalculatorTable
(
    Id Int,
    ListUnitPrice Money,
    UnitPrice Money,
    OriginalUnitPrice Money,
);";

                try
                {
                    using (SqlCommand sqlCommand = new(query, sqlConnection))
                    {
                        sqlConnection.Open();

                        sqlCommand.ExecuteNonQuery();

                        using (SqlBulkCopy sqlBulkCopy = new(sqlConnection))
                        {
                            sqlBulkCopy.DestinationTableName = "#CalculatorTable";
                            sqlBulkCopy.WriteToServer(dataTable);
                            sqlBulkCopy.Close();
                        }

                        sqlCommand.CommandText = @"
Update      ProductInformationPriceses
Set         ListUnitPrice = CT.ListUnitPrice
            ,UnitPrice = CT.UnitPrice
            ,OriginalUnitPrice = CT.OriginalUnitPrice
From        ProductInformationPriceses As PIP
Inner Join  #CalculatorTable As CT
On          PIP.Id = CT.Id;

Drop Table #CalculatorTable;";

                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch
                {
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
