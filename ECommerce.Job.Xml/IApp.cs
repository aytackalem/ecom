﻿using ECommerce.Domain.Entities;

namespace ECommerce.Job.Xml
{
    public interface IApp
    {
        #region Methods
        Task RunAsync(Tenant tLoop, Domain.Entities.Domain dLoop); 
        #endregion
    }
}
