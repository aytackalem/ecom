﻿namespace ECommerce.Job.Xml
{
    public interface IMain
    {
        #region Methods
        Task Run(); 
        #endregion
    }
}
