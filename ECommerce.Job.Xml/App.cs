﻿using ECommerce.Application.Common.DataTransferObjects.XmlService;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace ECommerce.Job.Xml
{
    public class App : IApp
    {
        #region Fields
        private readonly IHttpHelperV2 _httpHelperV2;

        private readonly IXmlService _xmlService;

        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService _marketplaceVariantService;

        private readonly Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService _marketplaceCatalogVariantValueService;

        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IConfiguration configuration, IHttpHelperV2 httpHelperV2, IXmlService xmlService, IDbNameFinder dbNameFinder, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantService marketplaceVariantService, Application.Common.Interfaces.MarketplaceCatalog.IMarketplaceVariantValueService marketplaceCatalogVariantValueService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._configuration = configuration;
            this._companyFinder = companyFinder;
            this._httpHelperV2 = httpHelperV2;
            this._xmlService = xmlService;
            this._dbNameFinder = dbNameFinder;
            this._marketplaceVariantService = marketplaceVariantService;
            this._marketplaceCatalogVariantValueService = marketplaceCatalogVariantValueService;

            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(Tenant tLoop, Domain.Entities.Domain dLoop)
        {



#if DEBUG

            //InsertImages(dLoop.DomainSetting.ImagePath);
            //ProductMarketplace(tLoop, dLoop);
#endif
            /*
             * Ürün kaynağı, "XML" veya "SM" olan ve ayarları içerisinde "Url" anahtar değerine sahip veriler çekilir.
             */
            var productSourceDomains = this
                ._unitOfWork
                .ProductSourceDomainRepository
                .DbSet()
                .Include(psd => psd.ProductSourceConfigurations)
                .Where(psd => psd.Active && psd.ProductSourceConfigurations.Any(psc => psc.Key == "Url")
                              && (psd.ProductSourceId == "XML" || psd.ProductSourceId == "SM"))
                .ToList();





            if (productSourceDomains.Count == 0)
                return;

#if DEBUG


#endif

            List<XmlSource> xmlSources = new();
            foreach (var psd in productSourceDomains)
            {
                var xmlSource = this._xmlService.GetXmlSource(
                    psd.ProductSourceConfigurations.First(psc => psc.Key == "Url").Value,
                    psd.Id,
                    psd.ProductSourceId);
                if (xmlSource == null)
                {
                    var error = $"Company {dLoop.Name} İstek yapılan XML'i sisteme eşleyecek ayarlar eksik";
                    SendErrorMail(error);

                    continue;
                }

                xmlSources.Add(xmlSource);
            }

            if (xmlSources.Count == 0)
                return;

            var tasks = new Task<HttpResultResponse<string>>[xmlSources.Count];

            for (int i = 0; i < xmlSources.Count; i++)
            {


                var uri = new Uri(xmlSources[i].RequestUri);
                var host = uri.Host;
                var localPath = uri.LocalPath.Replace("/", "_");
                var log = false;
                var tenantId = _tenantFinder.FindId();
                var domainId = _domainFinder.FindId();
                var timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                var logFileName = $"{tenantId}_{domainId}_{timeStamp}_{host}_{localPath}_{Guid.NewGuid().ToString().Replace("-", "")}.xml";

                if (xmlSources[i].ProductSourceId == "SM")
                {
                    log = true;
                }

                tasks[i] = this
                    ._httpHelperV2
                    .SendAsync(xmlSources[i].RequestUri, HttpMethod.Get, agent: xmlSources[i].UserAgent, log: log, logFileName: logFileName);

            }

            var httpResultResponses = await Task.WhenAll(tasks);
            List<ParsedXmlItem> parsedXmlItems = new();

            for (int i = 0; i < xmlSources.Count; i++)
                if (httpResultResponses[i].HttpStatusCode == HttpStatusCode.OK && httpResultResponses[i].Success)
                {
                    if (string.IsNullOrEmpty(httpResultResponses[i].ContentString) ||
                        string.IsNullOrWhiteSpace(httpResultResponses[i].ContentString))
                    {
                        var error = $"Company {dLoop.Name} İstek yapılan XML sonucu boş veri dönüyor.";
                        SendErrorMail(error);
                        continue;
                    }

                    var parsed = this._xmlService.Parse(httpResultResponses[i].ContentString, xmlSources[i]);
                    if (parsed.Success == false)
                    {
                        var error = $"Company {dLoop.Name} İstek yapılan XML parse edilemedi..";
                        SendErrorMail(error);
                        continue;
                    }

                    parsedXmlItems.AddRange(parsed.Data);
                }

            parsedXmlItems.RemoveAll(pxi => pxi.GetXmlItemMappingValue("Stock") == "0");
            parsedXmlItems.RemoveAll(pxi => string.IsNullOrEmpty(pxi.GetXmlItemMappingValue("Barcode")));

            if (parsedXmlItems.Count == 0)
                return;

            parsedXmlItems = parsedXmlItems
                .GroupBy(pxi => pxi.GetXmlItemMappingValue("Barcode"))
                .Select(pxi => pxi.First())
                .ToList();

            Insert(tLoop, dLoop, parsedXmlItems);



            ProductMarketplace(tLoop, dLoop);

            RunBrandUrl();

        }
        #endregion

        #region Helper Methods
        private void RunBrandUrl()
        {
            var _brands = this._unitOfWork.BrandRepository.DbSet().Where(x => string.IsNullOrEmpty(x.Url)).ToList();

            if (_brands.Count > 0)
            {
                foreach (var bLoop in _brands)
                {

                    bLoop.Url = $"{bLoop.Name.GenerateUrl()}-{bLoop.Id}-b";


                }
                this._unitOfWork.BrandRepository.Update(_brands);
            }


        }
        private Response Insert(Tenant tLoop, Domain.Entities.Domain dLoop, List<ParsedXmlItem> parsedXmlItems)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                List<Product> products = new();

                List<Variant> variants = new();

                List<Brand> brands = new();

                List<Category> categories = new();

                var supplierId = _unitOfWork.SupplierRepository.DbSet().FirstOrDefault().Id;

                foreach (var theParsedXmlItem in parsedXmlItems)
                {
                    try
                    {
                        var brandName = theParsedXmlItem.GetXmlItemMappingValue("BrandName");
                        var categoryName = theParsedXmlItem.GetXmlItemMappingValue("CategoryName");
                        var barcode = theParsedXmlItem.GetXmlItemMappingValue("Barcode");
                        var stock = theParsedXmlItem.GetXmlItemMappingValue("Stock");
                        var stockCode = theParsedXmlItem.GetXmlItemMappingValue("StockCode");
                        var sellerCode = theParsedXmlItem.GetXmlItemMappingValue("SellerCode");
                        var productName = theParsedXmlItem.GetXmlItemMappingValue("ProductName");
                        var productDescription = theParsedXmlItem.GetXmlItemMappingValue("ProductDescription");
                        var productContent = theParsedXmlItem.GetXmlItemMappingValue("ProductContent");
                        var listUnitPrice = theParsedXmlItem.GetXmlItemMappingValue("ListUnitPrice");
                        var unitPrice = theParsedXmlItem.GetXmlItemMappingValue("UnitPrice");
                        var vatRate = theParsedXmlItem.GetXmlItemMappingValue("VatRate");

                        var elementVariants = theParsedXmlItem
                            .XmlItemMappings
                            .Where(xim => xim.XmlItemType == XmlItemType.Attribute)
                            .Select(xim => new Variant
                            {
                                CreatedDate = DateTime.Now,
                                DomainId = dLoop.Id,

                                Photoable = false,
                                TenantId = tLoop.Id,
                                VariantTranslations = new()
                                {
                                new VariantTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    DomainId = dLoop.Id,
                                    LanguageId = "TR",

                                    TenantId = tLoop.Id,
                                    Name = xim.Key.FirstLetterUppercase()
                                }
                                },
                                VariantValues = new()
                                {
                                new VariantValue
                                {
                                    CreatedDate = DateTime.Now,
                                    DomainId = dLoop.Id,

                                    TenantId = tLoop.Id,
                                    VariantValueTranslations = new()
                                    {
                                        new VariantValueTranslation
                                        {
                                            CreatedDate = DateTime.Now,
                                            DomainId = dLoop.Id,
                                            LanguageId = "TR",

                                            TenantId = tLoop.Id,
                                            Value = xim.Value.FirstLetterUppercase()
                                        }
                                    }
                                }
                                }
                            })
                            .ToList();
                        if (elementVariants != null)
                            foreach (var theElementVariant in elementVariants)
                            {
                                var variantName = theElementVariant.VariantTranslations.First().Name;
                                var variant = variants.FirstOrDefault(v => v.VariantTranslations.First().Name == variantName);
                                if (variant == null)
                                    variants.Add(theElementVariant);
                                else
                                    theElementVariant.VariantValues.ForEach(theVariantValue =>
                                    {
                                        var variantValueValue = theVariantValue.VariantValueTranslations.First().Value;
                                        if (!variant.VariantValues.Any(vv => vv.VariantValueTranslations.First().Value == variantValueValue) && !string.IsNullOrEmpty(variantValueValue))
                                        {

                                            variant.VariantValues.Add(theVariantValue);
                                        }
                                    });
                            }

                        if (!brands.Any(b => b.Name == brandName))
                            brands.Add(new Brand
                            {
                                Active = true,
                                CreatedDate = DateTime.Now,
                                DomainId = dLoop.Id,

                                TenantId = tLoop.Id,
                                Url = String.Empty,
                                Name = brandName
                            });

                        var categoryCode = categoryName.ReplaceChar().Replace(" ", "");
                        if (!categories.Any(c => c.Code == categoryCode))
                            categories.Add(new Category
                            {
                                Active = true,
                                CreatedDate = DateTime.Now,
                                FileName = String.Empty,
                                DomainId = dLoop.Id,

                                SortNumber = 1,
                                TenantId = tLoop.Id,
                                Code = categoryCode,
                                CategoryTranslations = new()
                            {
                                new CategoryTranslation
                                {
                                    CreatedDate = DateTime.Now,
                                    DomainId=dLoop.Id,
                                    IconFileName=String.Empty,
                                    LanguageId="TR",

                                    TenantId=tLoop.Id,
                                    Url=String.Empty,
                                    Name = categoryName,
                                    CategoryContentTranslation=new CategoryContentTranslation
                                    {

                                        TenantId = tLoop.Id,
                                        DomainId = dLoop.Id,
                                        CreatedDate = DateTime.Now,
                                        Content = categoryName
                                    },
                                    CategorySeoTranslation=new CategorySeoTranslation
                                    {

                                        TenantId = tLoop.Id,
                                        DomainId = dLoop.Id,
                                        CreatedDate = DateTime.Now,
                                        MetaDescription = categoryName,
                                        MetaKeywords = categoryName,
                                        Title = categoryName
                                    },
                                    CategoryTranslationBreadcrumb = new()
                                }
                            }
                            });

                        Product product = new()
                        {
                            ProductSourceDomainId = theParsedXmlItem.ProductSourceDomainId,
                            DomainId = dLoop.Id,
                            TenantId = tLoop.Id,

                            CreatedDate = DateTime.Now,
                            SupplierId = supplierId,
                            AccountingProperty = false,
                            CategoryProducts = new List<CategoryProduct>(),
                            ProductTranslations = new()
                            {
                                new ProductTranslation
                                {
                                    Name = productName,

                                    LanguageId = "TR",
                                    TenantId = tLoop.Id,
                                    DomainId = dLoop.Id,
                                    CreatedDate = DateTime.Now
                                }
                            },
                            Active = true,
                            ProductMarketplaces = new List<ProductMarketplace>(),
                            Brand = new()
                            {
                                Name = brandName,
                                Active = true
                            },
                            Category = new Category
                            {
                                Code = categoryCode,
                                CategoryTranslations = new()
                                {
                                    new CategoryTranslation
                                    {
                                        Name = categoryName,

                                    }
                                }
                            },
                            SellerCode = sellerCode,
                            ProductInformations = new()
                            {
                                new ProductInformation
                                {
                                    Active = true,
                                    Barcode = barcode,
                                    StockCode = stockCode,
                                    Stock = int.Parse(stock),
                                    TenantId=tLoop.Id,
                                    DomainId=dLoop.Id,

                                    Photoable=true,
                                    IsSale=true,
                                    Type="PI",
                                    Payor=false,
                                    SkuCode=stockCode,
                                    CreatedDate=DateTime.Now,
                                    GroupId=Guid.NewGuid().ToString(),
                                    ProductInformationTranslations = new()
                                    {
                                        new ProductInformationTranslation
                                        {
                                           CreatedDate = DateTime.Now,
                                            SubTitle=String.Empty,
                                            TenantId=tLoop.Id,
                                            DomainId=dLoop.Id,

                                            LanguageId="TR",
                                            Url=String.Empty,
                                            Name = productName,
                                            ProductInformationSeoTranslation=new ProductInformationSeoTranslation
                                            {
                                            TenantId=tLoop.Id,
                                            DomainId=dLoop.Id,

                                            CreatedDate=DateTime.Now,
                                            MetaDescription = productName,
                                            MetaKeywords = productName,
                                            Title = productName,
                                            },
                                            ProductInformationContentTranslation=new ProductInformationContentTranslation
                                            {
                                                TenantId=tLoop.Id,
                                                DomainId=dLoop.Id,

                                                CreatedDate=DateTime.Now,
                                                Content=productContent,
                                                Description=productDescription,
                                                ExpertOpinion=String.Empty
                                            },
                                            ProductInformationTranslationBreadcrumb=new ProductInformationTranslationBreadcrumb(),
                                        }
                                    },
                                    ProductInformationPriceses = new()
                                    {
                                        new ProductInformationPrice
                                        {
                                            CreatedDate = DateTime.Now,
                                            TenantId=tLoop.Id,
                                            DomainId=dLoop.Id,

                                            AccountingPrice=true,
                                            CurrencyId="TL",
                                            ListUnitPrice = decimal.Parse(listUnitPrice),
                                            UnitPrice = decimal.Parse((string.IsNullOrEmpty(unitPrice) ? null : unitPrice) ?? listUnitPrice),
                                            VatRate = decimal.Parse(vatRate) / 100
                                        }
                                    },
                                    ProductInformationPhotos = theParsedXmlItem
                                        .XmlItemMappings
                                        .Where(xim => xim.TargetPropertyName == "Photos")
                                        .Select(xim => new ProductInformationPhoto
                                        {
                                            CreatedDate = DateTime.Now,
                                            TenantId = tLoop.Id,
                                            DomainId = dLoop.Id,

                                            FileName = xim.Value
                                        })
                                        .ToList(),
                                    ProductInformationVariants = theParsedXmlItem
                                        .XmlItemMappings
                                        .Where(xim => xim.XmlItemType == XmlItemType.Attribute &&
                                           !string.IsNullOrEmpty(xim.Value)
                                           && !string.IsNullOrEmpty(xim.Key)
                                        && string.Equals(xim.Key, "marka", StringComparison.OrdinalIgnoreCase) == false)
                                        .GroupBy(_=> new { _.Key,_.Value}).Select(x=>x.First())
                                        .Select(xim => new ProductInformationVariant
                                        {

                                            CreatedDate = DateTime.Now,
                                            DomainId = dLoop.Id,
                                            TenantId = tLoop.Id,
                                            VariantValue = new VariantValue
                                            {

                                                   CreatedDate = DateTime.Now,
                                                   DomainId = dLoop.Id,
                                                   TenantId = tLoop.Id,
                                                VariantValueTranslations = new List<VariantValueTranslation>
                                                {
                                                    new VariantValueTranslation
                                                    {
                                                        Value = xim.Value.FirstLetterUppercase(),

                                                        CreatedDate = DateTime.Now,
                                                        DomainId = dLoop.Id,
                                                        LanguageId = "TR",
                                                        TenantId = tLoop.Id
                                                    }
                                                },
                                                Variant = new Variant
                                                {
                                                    VariantTranslations = new()
                                                    {
                                                        new VariantTranslation
                                                        {
                                                            Name = xim.Key.FirstLetterUppercase(),

                                                            CreatedDate = DateTime.Now,
                                                            DomainId = dLoop.Id,
                                                            LanguageId = "TR",
                                                            TenantId = tLoop.Id
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                        .ToList()
                                }
                            }
                        };

                        products.Add(product);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                #region Brands
                var brandResponse = this.BulkRead(brands);

                var addBrands = brands
                    .Where(b => !brandResponse.Data.Where(d => d.Id != 0).Select(d => d.Name).Contains(b.Name))
                    .ToList();
                if (addBrands.Count > 0)
                    this._unitOfWork.BrandRepository.BulkInsertMapping(addBrands);

                brandResponse = this.BulkRead(brands);
                #endregion

                #region Categories
                var categoryResponse = this.BulkRead(categories);

                var addCategories = categories
                    .Where(c => !categoryResponse.Data.Where(d => d.Id != 0).Select(d => d.Code).Contains(c.Code))
                    .ToList();
                if (addCategories.Count > 0)
                {
                    this._unitOfWork.CategoryRepository.BulkInsertMapping(addCategories);
                    RunCategoryTranslationBreadcrumb(addCategories);
                }

                categoryResponse = this.BulkRead(categories);
                #endregion

                #region Variants
                variants.RemoveAll(v => v.VariantTranslations.First().Name == "Marka");
                var variantResponse = this.BulkRead(variants);

                var addVariants = variants
                    .Where(v => !variantResponse.Data.Where(d => d.Id != 0).Select(x => x.VariantTranslations.First().Name).Contains(v.VariantTranslations.First().Name))
                    .ToList();
                if (addVariants.Count > 0)
                    this._unitOfWork.VariantRepository.BulkInsertMapping(addVariants);

                variantResponse = this.BulkRead(variants);

                variants.ForEach(theVariant =>
                {
                    var variant = variantResponse
                        .Data
                        .First(d => d.VariantTranslations.First().Name == theVariant.VariantTranslations.First().Name);

                    theVariant.VariantValues.ForEach(theVariantValue => theVariantValue.VariantId = variant.Id);
                });
                #endregion

                #region Variant Values
                var variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());

                var addVariantValues = new List<VariantValue>();
                variants.SelectMany(v => v.VariantValues).ToList().ForEach(theVariantValue =>
                {
                    var variantValue = variantValueResponse
                        .Data
                        .FirstOrDefault(d => d.VariantId == theVariantValue.VariantId && d.VariantValueTranslations.First().Value == theVariantValue.VariantValueTranslations.First().Value);

                    if (variantValue == null)
                    {
                        addVariantValues.Add(theVariantValue);
                    }
                    else if (variantValue.Id == 0)
                        addVariantValues.Add(variantValue);
                });
                if (addVariantValues.Count > 0)
                    this._unitOfWork.VariantValueRepository.BulkInsertMapping(addVariantValues);

                variantValueResponse = this.BulkRead(variants.SelectMany(v => v.VariantValues).ToList());
                #endregion

                #region Products (Create)
                var productResponse = this.BulkRead(products);
                products.ForEach(theProduct =>
                {
                    theProduct.Brand = brandResponse.Data.First(d => d.Name == theProduct.Brand.Name);
                    theProduct.BrandId = theProduct.Brand.Id;

                    theProduct.Category = categoryResponse.Data.First(d => d.Code == theProduct.Category.Code);
                    theProduct.CategoryId = theProduct.Category.Id;

                    theProduct.CategoryProducts.Add(new CategoryProduct
                    {
                        CategoryId = theProduct.Category.Id,
                        CreatedDate = DateTime.Now,
                        DomainId = dLoop.Id,
                        TenantId = tLoop.Id,

                    });

                    var theProductInformation = theProduct.ProductInformations.First();
                    if (theProductInformation.ProductInformationVariants != null)
                    {

                        theProductInformation.ProductInformationVariants.ForEach(theProductInformationVariant =>
                        {
                            try
                            {

                                var variant = variantResponse.Data.First(d => d.VariantTranslations.First().Name == theProductInformationVariant.VariantValue.Variant.VariantTranslations.First().Name);

                                var variantValue = variantValueResponse.Data.First(d => d.VariantId == variant.Id && d.VariantValueTranslations.First().Value == theProductInformationVariant.VariantValue.VariantValueTranslations.First().Value);

                                theProductInformationVariant.VariantValueId = variantValue.Id;
                                theProductInformationVariant.VariantValue.VariantId = variant.Id;
                            }
                            catch (Exception ex)
                            {
                            }
                        });
                    }

                    var productEntity = productResponse
                        .Data
                        .FirstOrDefault(d => d.ProductInformations[0].Barcode == theProduct.ProductInformations[0].Barcode);

                    /*
                     * Ürün daha önceden açılmış ise...
                     */
                    if (productEntity != null)
                    {
                        theProduct.Id = productEntity.Id;
                        theProduct.ProductInformations[0].Id = productEntity.ProductInformations[0].Id;
                    }
                });

                var insertingProducts = products.Where(p => p.Id == 0).ToList();
                if (insertingProducts.Count > 0)
                {
                    this._unitOfWork.ProductRepository.BulkInsertMapping(insertingProducts);

                    RunProductInformationTranslationBreadcrumb(categoryResponse.Data, insertingProducts);
                }
                #endregion

                #region Products (Stok - Price Update)
                var productInformationResponse = this.BulkUpdate(products);

                this.FullVariantUpdate();
                #endregion

                InsertImages(dLoop.DomainSetting.ImagePath);

                #region Update
                var updatingProducts = products.Where(x => x.Id > 0).ToList();
                if (updatingProducts.Count > 0)
                {

                    var _productInformations = this
                        ._unitOfWork
                        .ProductInformationRepository
                        .DbSet()
                        .Include(x => x.Product)
                        .Include(x => x.ProductInformationVariants)
                            .ThenInclude(x => x.VariantValue)
                        .ToList();

                    var _productInformationVariants = new List<ProductInformationVariant>();
                    var _updateProductInformationVariants = new List<ProductInformationVariant>();


                    foreach (var pLoop in updatingProducts)
                    {
                        var pin = _productInformations.FirstOrDefault(x => x.ProductId == pLoop.Id);
                        if (pin != null)
                        {
                            if (pin.Product.AccountingProperty)
                            {

                                if (pLoop.ProductInformations[0].ProductInformationVariants != null)
                                {
                                    var groupProductInformationVariants = pLoop.ProductInformations[0].ProductInformationVariants.GroupBy(x => x.VariantValue.VariantId).Select(x => x.First()).ToList();

                                    foreach (var pvLoop in groupProductInformationVariants)
                                    {
                                        var productInformationVariant = pin.ProductInformationVariants.FirstOrDefault(x => x.VariantValue != null && x.VariantValue.VariantId == pvLoop.VariantValue.VariantId);
                                        if (productInformationVariant != null)
                                        {
                                            if (!pin.ProductInformationVariants.Any(x => pLoop.ProductInformations[0].ProductInformationVariants.Any(y => y.VariantValueId == x.VariantValueId)))
                                            {
                                                productInformationVariant.VariantValueId = pvLoop.VariantValueId;
                                                productInformationVariant.Tenant = null;
                                                productInformationVariant.ProductInformation = null;
                                                productInformationVariant.Domain = null;
                                                productInformationVariant.VariantValue = null;
                                                _updateProductInformationVariants.Add(productInformationVariant);
                                            }
                                        }
                                        else
                                        {
                                            _productInformationVariants.Add(new ProductInformationVariant
                                            {

                                                CreatedDate = DateTime.Now,
                                                DomainId = dLoop.Id,
                                                TenantId = tLoop.Id,
                                                ProductInformationId = pin.Id,
                                                VariantValueId = pvLoop.VariantValueId
                                            });
                                        }
                                    }
                                }

                            }
                        }

                    }

                    _unitOfWork.ProductInformationVariantRepository.Update(_updateProductInformationVariants);
                    _unitOfWork.ProductInformationVariantRepository.BulkInsert(_productInformationVariants);
                }
                #endregion


                response.Success = true;
            }, (response, exception) =>
            {

                var error = $"Insert Company {dLoop.Name} {exception} {exception.InnerException}";
                SendErrorMail(error);

            });
        }

        private void ProductMarketplace(Tenant tLoop, Domain.Entities.Domain dLoop)
        {
            var companies = this
         ._unitOfWork
         .CompanyRepository
         .DbSet()
         .Include(c => c.CompanySetting)
         .ToList();

            foreach (var theCompany in companies)
            {
                ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;

                var _products = _unitOfWork
                    .ProductRepository
                    .DbSet()
                    .Select(x => new Product
                    {

                        Id = x.Id,
                        SellerCode = x.SellerCode,
                        CategoryId = x.CategoryId,
                        BrandId = x.BrandId,
                        ProductInformations = x.ProductInformations.Select(pi => new ProductInformation
                        {
                            Id = pi.Id,
                            Barcode = pi.Barcode,
                            StockCode = pi.StockCode,
                            ProductInformationPriceses = pi.ProductInformationPriceses.Where(x => x.CurrencyId == "TL").Select(pip => new ProductInformationPrice
                            {
                                ListUnitPrice = pip.ListUnitPrice,
                                UnitPrice = pip.UnitPrice,
                            }).ToList(),
                            ProductInformationVariants = pi.ProductInformationVariants.Select(pv => new ProductInformationVariant
                            {
                                VariantValueId = pv.VariantValueId
                            }).ToList()

                        })
                            .ToList()
                    })
                    .ToList();

                var marketplaceCompanies = _unitOfWork.MarketplaceCompanyRepository.DbSet().Include(x => x.Marketplace).Where(x => x.Active).ToList();
                var productMarketplaces = _unitOfWork.ProductMarketplaceRepository.DbSet().Select(x => new ProductMarketplace
                {
                    ProductId = x.ProductId,
                    MarketplaceId = x.MarketplaceId
                })
                    .ToList();

                var productInformationMarketplaces = _unitOfWork
                    .ProductInformationMarketplaceRepository
                    .DbSet()
                    .Select(x => new ProductInformationMarketplace
                    {
                        Id = x.Id,
                        ProductInformationId = x.ProductInformationId,
                        MarketplaceId = x.MarketplaceId,
                        ProductInformationMarketplaceVariantValues = x.ProductInformationMarketplaceVariantValues.Select(piv => new ProductInformationMarketplaceVariantValue
                        {
                            MarketplaceVariantCode = piv.MarketplaceVariantCode
                        }).ToList()
                    })
                    .ToList();

                var _productMarketplaces = new List<Domain.Entities.ProductMarketplace>();
                var _productInformationMarketplaces = new List<Domain.Entities.ProductInformationMarketplace>();
                var _productInformationMarketplaceVariantValues = new List<ProductInformationMarketplaceVariantValue>();
                foreach (var theMarketplaceCompany in marketplaceCompanies)
                {
                    var marketplaceCategoryCodes = new Dictionary<string, List<string>>();
                    var _variantValueDictionary = new Dictionary<string, bool>();


                    var marketplaceCategoryMappings = _unitOfWork
                        .MarketplaceCategoryMappingRepository
                        .DbSet()
                        .Include(x => x.MarketplaceCategoryVariantValueMappings)
                        .Where(mcm => mcm.MarketplaceId == theMarketplaceCompany.MarketplaceId)
                        .ToList();

                    var marketplaceBrandMappings = _unitOfWork
                        .MarketplaceBrandMappingRepository
                        .DbSet()
                        .Where(x => x.MarketplaceId == theMarketplaceCompany.MarketplaceId)
                        .ToList();


                    var marketplaceVariantValueMappings = _unitOfWork
                        .MarketplaceVariantValueMappingRepository
                        .DbSet()
                        .Where(mvvm => mvvm.MarketplaceId == theMarketplaceCompany.MarketplaceId)
                        .ToList();

                    foreach (var pLoop in _products)
                    {
                        Console.WriteLine($"{theMarketplaceCompany.MarketplaceId} {_products.IndexOf(pLoop)}. Ürün");

                        var marketplaceCategory = marketplaceCategoryMappings
                            .FirstOrDefault(mcm => mcm.MarketplaceId == theMarketplaceCompany.MarketplaceId &&
                                                   mcm.CategoryId == pLoop.CategoryId
                                                   && !string.IsNullOrEmpty(mcm.MarketplaceCategoryCode));

                        var anyProductMarketPlace = productMarketplaces.Any(x => x.ProductId == pLoop.Id &&
                                                                                 x.MarketplaceId == theMarketplaceCompany.MarketplaceId);

                        var marketplaceBrand = marketplaceBrandMappings.FirstOrDefault(x => x.BrandId == pLoop.BrandId);

                        var added = true;

                        if (marketplaceCategory != null && marketplaceBrand != null)
                        {
                            if ((theMarketplaceCompany.Marketplace.BrandAutocomplete && string.IsNullOrEmpty(marketplaceBrand.MarketplaceBrandCode)))
                            {
                                added = false;
                            }

                            if (added && !anyProductMarketPlace)
                                _productMarketplaces.Add(new Domain.Entities.ProductMarketplace
                                {
                                    Active = true,
                                    MarketplaceBrandCode = marketplaceBrand.MarketplaceBrandCode,
                                    MarketplaceBrandName = marketplaceBrand.MarketplaceBrandName,
                                    MarketplaceCategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                    TenantId = tLoop.Id,
                                    DomainId = dLoop.Id,
                                    CompanyId = theCompany.Id,
                                    ProductId = pLoop.Id,
                                    SellerCode = pLoop.SellerCode,
                                    MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                    UUId = Guid.NewGuid().ToString().ToLower(),
                                    DeliveryDay = 1
                                });
                        }

                        foreach (var theProductInformation in pLoop.ProductInformations)
                        {
                            var productInformationMarketplace = productInformationMarketplaces
                                .FirstOrDefault(x => x.ProductInformationId == theProductInformation.Id &&
                                                     x.MarketplaceId == theMarketplaceCompany.MarketplaceId);

                            if (marketplaceCategory != null && !marketplaceCategoryCodes.ContainsKey(marketplaceCategory.MarketplaceCategoryCode))
                            {

                                if (theMarketplaceCompany.Marketplace.IsECommerce)
                                {
                                    var eCommerceVariants = _unitOfWork.ECommerceVariantRepository
                                            .DbSet()
                                            .Where(x => x.ECommerceCategoryCode == marketplaceCategory.MarketplaceCategoryCode && x.MarketplaceId == theMarketplaceCompany.MarketplaceId);

                                    marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, eCommerceVariants.Select(x => x.Code).ToList());
                                }
                                else
                                {
                                    var categoryCodes = this._marketplaceVariantService.GetByCategoryCodesAsync(new Application.Common.Parameters.MarketplaceCatalog.VariantService.GetByCategoryCodesParameter
                                    {
                                        MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                        CategoryCodes = new List<string> { marketplaceCategory.MarketplaceCategoryCode }
                                    }).Result;

                                    if (!categoryCodes.Success) continue;

                                    marketplaceCategoryCodes.Add(marketplaceCategory.MarketplaceCategoryCode, categoryCodes.Data.Items.Select(x => x.Code).ToList());
                                }
                            }


                            if (productInformationMarketplace == null)
                            {
                                productInformationMarketplace = new Domain.Entities.ProductInformationMarketplace
                                {
                                    Active = true,
                                    AccountingPrice = true,
                                    DirtyPrice = true,
                                    DirtyStock = true,
                                    DirtyProductInformation = true,
                                    Barcode = theProductInformation.Barcode,
                                    ListUnitPrice = theProductInformation.ProductInformationPriceses.FirstOrDefault().ListUnitPrice,
                                    UnitPrice = theProductInformation.ProductInformationPriceses.FirstOrDefault().UnitPrice,
                                    StockCode = theProductInformation.StockCode,
                                    UUId = Guid.NewGuid().ToString().ToLower(),
                                    TenantId = tLoop.Id,
                                    DomainId = dLoop.Id,
                                    CompanyId = theCompany.Id,
                                    MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                    ProductInformationId = theProductInformation.Id,
                                    ProductInformationMarketplaceVariantValues = new List<Domain.Entities.ProductInformationMarketplaceVariantValue>()
                                };

                                if (marketplaceCategory != null)
                                {
                                    #region Variant Mappings

                                    foreach (var theProductInformationVariant in theProductInformation.ProductInformationVariants)
                                    {

                                        var _marketplaceVariantValueMappings =
                                            marketplaceVariantValueMappings.Where(mvvm =>
                                            (theMarketplaceCompany.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true) &&
                                            marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                                            && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                                            && mvvm.MarketplaceId == theMarketplaceCompany.MarketplaceId
                                            && !string.IsNullOrEmpty(mvvm.MarketplaceVariantValueCode)
                                            && !string.IsNullOrEmpty(mvvm.MarketplaceVariantValueName)
                                        ).GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();

                                        foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                        {


                                            /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                            if (!marketplaceVariantValueMapping.AllowCustom && (theMarketplaceCompany.MarketplaceId == "TY"))
                                            {
                                                var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                                if (!_variantValueDictionary.ContainsKey(code))
                                                {
                                                    var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                    {
                                                        MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                                        Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                        CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                        VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                    }).Result;

                                                    if (!marketplaceCatalogVariantValueService.Success) continue;


                                                    _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                                }

                                                if (_variantValueDictionary[code] == false) continue;
                                            }


                                            productInformationMarketplace
                                            .ProductInformationMarketplaceVariantValues
                                            .Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                            {
                                                AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                                Mandatory = marketplaceVariantValueMapping.Mandatory,
                                                Multiple = marketplaceVariantValueMapping.Multiple,
                                                Varianter = marketplaceVariantValueMapping.Varianter,
                                                MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                                MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                                MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                                TenantId = tLoop.Id,
                                                DomainId = dLoop.Id,
                                                CompanyId = theCompany.Id
                                            });
                                        }
                                    }

                                    #endregion

                                    #region Category Mappings
                                    foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                    {
                                        if (productInformationMarketplace
                          .ProductInformationMarketplaceVariantValues
                          .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                          string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                            continue;

                                        if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (theMarketplaceCompany.MarketplaceId == "TY" || theMarketplaceCompany.MarketplaceId == "PA"))
                                        {
                                            var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                            if (!_variantValueDictionary.ContainsKey(code))
                                            {
                                                var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                {
                                                    MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                                    Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                    CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                    VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                                }).Result;
                                                if (!marketplaceCatalogVariantValueService.Success) continue;


                                                _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                            }

                                            if (_variantValueDictionary[code] == false) continue;
                                        }

                                        productInformationMarketplace.ProductInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                        {
                                            AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                            Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                            Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                            Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                            MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                            MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                            MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                            MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                            TenantId = tLoop.Id,
                                            DomainId = dLoop.Id,
                                            CompanyId = theCompany.Id
                                        });

                                    }
                                    #endregion
                                }
                                _productInformationMarketplaces.Add(productInformationMarketplace);
                            }
                            else if (marketplaceCategory != null)
                            {
                                #region Variant Mappings
                                foreach (var theProductInformationVariant in theProductInformation.ProductInformationVariants)
                                {
                                    var _marketplaceVariantValueMappings =
                                      marketplaceVariantValueMappings.Where(mvvm =>
                                      (theMarketplaceCompany.Marketplace.IsECommerce ? (mvvm.CompanyId == theCompany.Id) : true) &&
                                      marketplaceCategoryCodes[marketplaceCategory.MarketplaceCategoryCode].Contains(mvvm.MarketplaceVariantCode)
                                      && mvvm.VariantValueId == theProductInformationVariant.VariantValueId
                                      && mvvm.MarketplaceId == theMarketplaceCompany.MarketplaceId
                                      && !string.IsNullOrEmpty(mvvm.MarketplaceVariantValueCode)
                                      && !string.IsNullOrEmpty(mvvm.MarketplaceVariantValueName)
                                      ).GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList();

                                    foreach (var marketplaceVariantValueMapping in _marketplaceVariantValueMappings)
                                    {
                                        if (productInformationMarketplace
                                            .ProductInformationMarketplaceVariantValues
                                            .Any(pimvv => pimvv.MarketplaceVariantCode == marketplaceVariantValueMapping.MarketplaceVariantCode))
                                            continue;


                                        /*Bu kodu eklememizin amacı bazı kategorilerde bazen XL bedeni bulunuyorken bazı kategogerilerin bedeninde XL bulunmuyor ama müşteri XL eşlemesi yaptğı için biz otamtik eşliyorduk aslında eşlememesi gerekiyor*/
                                        if (!marketplaceVariantValueMapping.AllowCustom && (theMarketplaceCompany.MarketplaceId == "TY"))
                                        {
                                            var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{marketplaceVariantValueMapping.MarketplaceVariantCode}-{marketplaceVariantValueMapping.MarketplaceVariantValueCode}";

                                            if (!_variantValueDictionary.ContainsKey(code))
                                            {
                                                var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                                {
                                                    MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                                    Code = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                                    CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                    VariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode
                                                }).Result;

                                                if (!marketplaceCatalogVariantValueService.Success) continue;


                                                _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                            }

                                            if (_variantValueDictionary[code] == false) continue;
                                        }


                                        _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                        {
                                            AllowCustom = marketplaceVariantValueMapping.AllowCustom,
                                            Mandatory = marketplaceVariantValueMapping.Mandatory,
                                            Multiple = marketplaceVariantValueMapping.Multiple,
                                            Varianter = marketplaceVariantValueMapping.Varianter,
                                            ProductInformationMarketplaceId = productInformationMarketplace.Id,
                                            MarketplaceVariantCode = marketplaceVariantValueMapping.MarketplaceVariantCode,
                                            MarketplaceVariantName = marketplaceVariantValueMapping.MarketplaceVariantName,
                                            MarketplaceVariantValueCode = marketplaceVariantValueMapping.MarketplaceVariantValueCode,
                                            MarketplaceVariantValueName = marketplaceVariantValueMapping.MarketplaceVariantValueName,
                                            TenantId = tLoop.Id,
                                            DomainId = dLoop.Id,
                                            CompanyId = theCompany.Id
                                        });
                                    }
                                }
                                #endregion

                                #region Category Mappings
                                foreach (var theMarketplaceCategoryVariantValueMapping in marketplaceCategory.MarketplaceCategoryVariantValueMappings)
                                {
                                    if (productInformationMarketplace
                      .ProductInformationMarketplaceVariantValues
                      .Any(pimvv => pimvv.MarketplaceVariantCode == theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode) ||
                      string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode) || string.IsNullOrEmpty(theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName))
                                        continue;

                                    if (!theMarketplaceCategoryVariantValueMapping.AllowCustom && (theMarketplaceCompany.MarketplaceId == "TY" || theMarketplaceCompany.MarketplaceId == "PA"))
                                    {
                                        var code = $"{marketplaceCategory.MarketplaceCategoryCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode}-{theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode}";

                                        if (!_variantValueDictionary.ContainsKey(code))
                                        {
                                            var marketplaceCatalogVariantValueService = this._marketplaceCatalogVariantValueService.GetExsistName(new Application.Common.Parameters.MarketplaceCatalog.VariantValueService.SearchByNameParameters
                                            {
                                                MarketplaceId = theMarketplaceCompany.MarketplaceId,
                                                Code = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                                CategoryCode = marketplaceCategory.MarketplaceCategoryCode,
                                                VariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode
                                            }).Result;
                                            if (!marketplaceCatalogVariantValueService.Success) continue;


                                            _variantValueDictionary.Add(code, marketplaceCatalogVariantValueService.Data);
                                        }

                                        if (_variantValueDictionary[code] == false) continue;
                                    }

                                    _productInformationMarketplaceVariantValues.Add(new Domain.Entities.ProductInformationMarketplaceVariantValue
                                    {
                                        AllowCustom = theMarketplaceCategoryVariantValueMapping.AllowCustom,
                                        Mandatory = theMarketplaceCategoryVariantValueMapping.Mandatory,
                                        Multiple = theMarketplaceCategoryVariantValueMapping.Multiple,
                                        Varianter = theMarketplaceCategoryVariantValueMapping.Varianter,
                                        MarketplaceVariantCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantCode,
                                        MarketplaceVariantName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantName,
                                        MarketplaceVariantValueCode = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueCode,
                                        MarketplaceVariantValueName = theMarketplaceCategoryVariantValueMapping.MarketplaceVariantValueName,
                                        TenantId = tLoop.Id,
                                        DomainId = dLoop.Id,
                                        CompanyId = theCompany.Id
                                    });

                                }
                                #endregion
                            }
                        }


                    }
                }

                _productInformationMarketplaces.ForEach(x =>
              x.ProductInformationMarketplaceVariantValues =
                x.ProductInformationMarketplaceVariantValues.GroupBy(x => x.MarketplaceVariantCode).Select(x => x.First()).ToList());

                _productInformationMarketplaceVariantValues = _productInformationMarketplaceVariantValues.GroupBy(x => new { x.MarketplaceVariantCode, x.ProductInformationMarketplaceId }).Select(x => x.First()).ToList();

                try
                {
                    _unitOfWork.ProductInformationMarketplaceVariantValueRepository.BulkInsert(_productInformationMarketplaceVariantValues);
                    _unitOfWork.ProductMarketplaceRepository.BulkInsert(_productMarketplaces);
                    _unitOfWork.ProductInformationMarketplaceRepository.BulkInsertMapping(_productInformationMarketplaces);
                    BulkUpdateDirtyProductInformation(_productInformationMarketplaceVariantValues.GroupBy(x => x.ProductInformationMarketplaceId).Select(x => x.Key).ToList());
                }
                catch (Exception exception)
                {
                    var error = $"ProductMarketplace Company {dLoop.Name} {exception} {exception.InnerException}";
                    SendErrorMail(error);
                }
            }
        }

        private void InsertImages(string path)
        {
            Console.WriteLine("InsertImages");
            var _productInformationPhotos = _unitOfWork.ProductInformationPhotoRepository.DbSet().Where(x => x.FileName.StartsWith("http")).ToList();
            var newPath = path.Replace("C:\\inetpub", "\\\\172.31.57.21\\c$\\inetpub");

#if DEBUG
            //newPath = path;
#endif



            var productPhotos = new List<Domain.Entities.ProductInformationPhoto>();

            foreach (var ppLoop in _productInformationPhotos)
            {
                var splitPhoto = ppLoop.FileName.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                var fileName = $"{splitPhoto[splitPhoto.Length - 1]}";


                var success = Download(newPath, fileName, ppLoop.ProductInformationId, ppLoop.FileName);

                if (success)
                {
                    ppLoop.FileName = $"{ppLoop.ProductInformationId}/{fileName}";
                }
                else
                    ppLoop.Deleted = true;

            }
            _unitOfWork.ProductInformationPhotoRepository.Update(_productInformationPhotos);


        }

        private DataResponse<List<Brand>> BulkUpdateDirtyProductInformation(List<int> productInformationMarketplaceIds)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Brand>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("ProductInformationMarketplaceId", typeof(int)));

                foreach (var theId in productInformationMarketplaceIds)
                {
                    var row = dataTable.NewRow();
                    row["ProductInformationMarketplaceId"] = theId;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkUpdatePim (
    ProductInformationMarketplaceId int Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkUpdatePim";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Update		PIM
set
DirtyProductInformation = 1
From		ProductInformationMarketplaces PIM WITH(NOLOCK)
JOIN		#TmpTableBulkUpdatePim T WITH(NOLOCK) ON T.ProductInformationMarketplaceId=PIM.Id
Where		PIM.DomainId = @DomainId

Drop Table  #TmpTableBulkUpdatePim;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteNonQuery();


                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Brand>> BulkRead(List<Brand> brands)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Brand>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theBrand in brands)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theBrand.Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkBrand (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkBrand";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(B.Id, 0) As Id
            ,T.Name
From        #TmpTableBulkBrand As T
Left Join   Brands As B With(NoLock)
On          T.Name = B.Name
            And B.DomainId = @DomainId

Drop Table  #TmpTableBulkBrand;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Brand
                        {
                            Id = dataReader.GetInt32("Id"),
                            Name = dataReader.GetString("Name")
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Category>> BulkRead(List<Category> categories)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Category>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Code", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theCategory in categories)
                {
                    var row = dataTable.NewRow();
                    row["Code"] = theCategory.Code;
                    row["Name"] = theCategory.CategoryTranslations.FirstOrDefault().Name;
                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkCategory (
    Code NVarChar(200) Not Null,
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkCategory";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(C.Id, 0) As Id
            ,T.Code,
            T.Name,
            IsNull(CT.Url,'') as Url
From        #TmpTableBulkCategory As T
Left Join   Categories As C With(NoLock)
On          T.Code = C.Code
            And C.DomainId = @DomainId
Left Join   CategoryTranslations CT ON CT.CategoryId=C.Id
            And CT.DomainId = @DomainId
Drop Table  #TmpTableBulkCategory;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Category
                        {
                            Id = dataReader.GetInt32("Id"),
                            Code = dataReader.GetString("Code"),
                            CategoryTranslations = new List<CategoryTranslation>
                            {
                                new CategoryTranslation
                                {
                                    CategoryId=dataReader.GetInt32("Id"),
                                    Name = dataReader.GetString("Name"),
                                    Url=dataReader.GetString("Url")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Product>> BulkRead(List<Product> products)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Product>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));

                foreach (var theProduct in products)
                {
                    var row = dataTable.NewRow();
                    row["Barcode"] = theProduct.ProductInformations.First().Barcode;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkProduct (
    Barcode NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkProduct";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      P.Id,
            [PI].Id As ProductInformationId,
            T.Barcode
From        #TmpTableBulkProduct As T

Inner Join  Products As P With(NoLock)
On          P.DomainId = @DomainId

Inner Join  ProductInformations As [PI] With(NoLock)
On          P.Id = [PI].ProductId
            And [PI].Barcode = T.Barcode
            And [PI].DomainId = @DomainId

Group By    P.Id,
            [PI].Id,
            T.Barcode

Drop Table  #TmpTableBulkProduct;";

                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        response.Data.Add(new Product
                        {
                            Id = dataReader.GetInt32("Id"),
                            ProductInformations = new()
                            {
                                new ProductInformation
                                {
                                    Id = dataReader.GetInt32("ProductInformationId"),
                                    Barcode = dataReader.GetString("Barcode")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Variant>> BulkRead(List<Variant> variants)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Variant>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("Name", typeof(string)));

                foreach (var theVariant in variants)
                {
                    var row = dataTable.NewRow();
                    row["Name"] = theVariant.VariantTranslations.First().Name;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table #TmpTableBulkVariant (
    Name NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTableBulkVariant";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VT.VariantId, 0) As Id
            ,T.Name
From        #TmpTableBulkVariant As T
Left Join   VariantTranslations As VT With(NoLock)
On          T.Name = VT.Name
            And VT.DomainId = @DomainId

Drop Table  #TmpTableBulkVariant;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new Variant
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantTranslations = new List<VariantTranslation>
                            {
                                new VariantTranslation
                                {
                                    Name = dataReader.GetString("Name")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<VariantValue>> BulkRead(List<VariantValue> variantValues)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<VariantValue>>>((response) =>
            {
                response.Data = new();

                DataTable dataTable = new("");
                dataTable.Columns.Add(new DataColumn("VariantId", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Value", typeof(string)));

                foreach (var theVariantValue in variantValues)
                {
                    var row = dataTable.NewRow();
                    row["VariantId"] = theVariantValue.VariantId;
                    row["Value"] = theVariantValue.VariantValueTranslations.First().Value;

                    dataTable.Rows.Add(row);
                }

                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {
                    using var command = new SqlCommand("", conn);
                    conn.Open();

                    command.CommandText = @"
Create Table TmpTableBulkVariantValue (
    VariantId Int Not Null,
    Value NVarChar(200) Not Null
)";
                    var i = command.ExecuteNonQuery();

                    using (var bulkcopy = new SqlBulkCopy(conn))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "TmpTableBulkVariantValue";
                        bulkcopy.WriteToServer(dataTable);
                        bulkcopy.Close();
                    }

                    command.CommandTimeout = 300;
                    command.CommandText = @"
Select      IsNull(VV.Id, 0) As Id
            ,IsNull(VV.VariantId, 0) As VariantId
            ,ISNULL(T.Value,VVT.Value) AS Value
From        VariantValues As VV With(NoLock)

Join		VariantValueTranslations As VVT With(NoLock)
On          VVT.VariantValueId = VV.Id
            And VVT.DomainId = @DomainId

Left Join   TmpTableBulkVariantValue As T
On          T.VariantId = VV.VariantId
            And VV.DomainId = @DomainId
			And VVT.Value = T.Value

Drop Table  TmpTableBulkVariantValue;";
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        response.Data.Add(new VariantValue
                        {
                            Id = dataReader.GetInt32("Id"),
                            VariantId = dataReader.GetInt32("VariantId"),
                            VariantValueTranslations = new List<VariantValueTranslation>
                            {
                                new VariantValueTranslation
                                {
                                    Value = dataReader.GetString("Value")
                                }
                            }
                        });
                    }
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private DataResponse<List<Application.Panel.DataTransferObjects.ProductInformation>> BulkUpdate(List<Product> products)
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Panel.DataTransferObjects.ProductInformation>>>((response) =>
            {

                var groupedProducts = products.GroupBy(p => p.ProductSourceDomainId);
                foreach (var groupedProduct in groupedProducts)
                {
                    DataTable dataTable = new("");
                    dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));
                    dataTable.Columns.Add(new DataColumn("Stock", typeof(int)));
                    dataTable.Columns.Add(new DataColumn("ListUnitPrice", typeof(decimal)));
                    dataTable.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
                    dataTable.Columns.Add(new DataColumn("ProductSourceDomainId", typeof(int)));

                    groupedProduct.ToList().ForEach(theProduct =>
                    {
                        theProduct.ProductInformations.ForEach(theProductInformation =>
                        {
                            var row = dataTable.NewRow();
                            row["Barcode"] = theProductInformation.Barcode;
                            row["Stock"] = theProductInformation.Stock;
                            row["ListUnitPrice"] = theProductInformation.ProductInformationPriceses.First().ListUnitPrice;
                            row["UnitPrice"] = theProductInformation.ProductInformationPriceses.First().UnitPrice;
                            row["ProductSourceDomainId"] = theProduct.ProductSourceDomainId;

                            dataTable.Rows.Add(row);
                        });
                    });

                    using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                    {
                        var temporaryTableName = $"Update_{Guid.NewGuid().ToString().Replace("-", "")}";

                        using var command = new SqlCommand("", conn);
                        conn.Open();

                        command.CommandText = @$"
Create Table {temporaryTableName} (
    Barcode                 NVarChar(200) Not Null,
    Stock                   Int Not Null,
    ListUnitPrice           Decimal(18,2) Not Null,
    UnitPrice               Decimal(18,2) Not Null,
    ProductSourceDomainId   Int Not Null
)";
                        var i = command.ExecuteNonQuery();

                        using (var bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 660;
                            bulkcopy.DestinationTableName = temporaryTableName;
                            bulkcopy.WriteToServer(dataTable);
                            bulkcopy.Close();
                        }

                        command.CommandTimeout = 300;
                        command.CommandText = @$"

Update  P
Set     P.ProductSourceDomainId = @ProductSourceDomainId
From    Products As P

Join    ProductInformations As [PI]
On      [PI].ProductId = P.Id

Join    {temporaryTableName} As T
On      T.Barcode = [PI].Barcode

Where   P.TenantId = @TenantId
        And P.DomainId = @DomainId


--UpdateStock alanı sıfıra çekilir.

Update  [PI]
Set     [PI].UpdateStock = 0
From    ProductInformations As [PI]

Join    Products As P
On      P.Id = [PI].ProductId
        And P.ProductSourceDomainId = @ProductSourceDomainId
        And P.TenantId = @TenantId
        And P.DomainId = @DomainId

Where   [PI].TenantId = @TenantId
        And [PI].DomainId = @DomainId


--Dış kaynaktan gelen değerler UpdateStock alanına yazılır.

Update      [PI]
Set         [PI].UpdateStock = T.Stock
From        ProductInformations As [PI]

Inner Join  Products As P
On          P.Id = [PI].ProductId
            And P.ProductSourceDomainId = @ProductSourceDomainId
            And P.TenantId = @TenantId
            And P.DomainId = @DomainId

Inner Join  {temporaryTableName} As T 
On	        T.Barcode = [PI].Barcode

Where       [PI].TenantId = @TenantId
            And [PI].DomainId = @DomainId


--Dış kaynaktan gelen veri ile hali hazırdaki uyumsuzluğunda DirtyStock alanını 1 olarak set ediyoruz.

Update      PIM
Set         PIM.DirtyStock = 1
From        ProductInformationMarketplaces As PIM

Inner Join	ProductInformations As [PI]
On		    PIM.ProductInformationId = [PI].Id
            And [PI].TenantId = @TenantId
            And [PI].DomainId = @DomainId
            And [PI].Stock <> [PI].UpdateStock

Inner Join  Products As P
On          P.Id = [PI].ProductId
            And P.TenantId = @TenantId
            And P.DomainId = @DomainId
            And P.ProductSourceDomainId = @ProductSourceDomainId


--DirtyStock alanı 1 olan kayıtlarda Stock alanı güncellemesi yapılır.

Update      [PI]
Set         [PI].Stock = [PI].UpdateStock
From        ProductInformations As [PI]

Inner Join  Products As P
On          P.Id = [PI].ProductId
            And [PI].TenantId = @TenantId
            And [PI].DomainId = @DomainId
            And P.ProductSourceDomainId = @ProductSourceDomainId

Where       [PI].Stock <> [PI].UpdateStock
            And [PI].TenantId = @TenantId
            And [PI].DomainId = @DomainId


--Dış kaynaktan fiyat güncellemesine izin verilen manuel girilmiş ürünlere ait verileri güncelleyen ürün.

Update      PIP
Set         PIP.ListUnitPrice = T.ListUnitPrice,
            PIP.UnitPrice = T.UnitPrice
From	    ProductInformationPriceses As PIP

Inner Join	ProductInformations [PI] 
On          [PI].Id = PIP.ProductInformationId

Inner Join  Products As P
On          P.Id = [PI].ProductId
            And [PI].TenantId = @TenantId
            And [PI].DomainId = @DomainId
            And P.ProductSourceDomainId = @ProductSourceDomainId

Join	    {temporaryTableName} T 
On		    T.Barcode = [PI].Barcode

Where	    PIP.TenantId = @TenantId
            And PIP.DomainId = @DomainId
            And (
                    PIP.ListUnitPrice <> T.ListUnitPrice Or 
                    PIP.UnitPrice <> T.UnitPrice
                )
            And PIP.AccountingPrice = 1


--

Update  PIM
Set     PIM.ListUnitPrice = T.ListUnitPrice,
        PIM.UnitPrice = T.UnitPrice,
        PIM.DirtyPrice = 1
From    ProductInformationMarketplaces As PIM

Join    ProductInformations [PI]
On		[PI].Id = PIM.ProductInformationId
        And [PI].TenantId = @TenantId
        And [PI].DomainId = @DomainId

Join    Products As P
On      P.Id = [PI].ProductId
        And P.TenantId = @TenantId
        And P.DomainId = @DomainId
        And P.ProductSourceDomainId = @ProductSourceDomainId

Join    {temporaryTableName} As T
On      T.Barcode = [PI].Barcode

Where   PIM.TenantId = @TenantId
        And PIM.DomainId = @DomainId
        And PIM.AccountingPrice = 1
        And (
                PIM.ListUnitPrice <> T.ListUnitPrice Or 
                PIM.UnitPrice <> T.UnitPrice
            )


Drop Table  {temporaryTableName};";

                        command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                        command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        command.Parameters.AddWithValue("@ProductSourceDomainId", groupedProduct.Key);

                        var dataReader = command.ExecuteNonQuery();
                    }
                }

                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private Response FullVariantUpdate()
        {
            SqlConnection conn = null;

            return ExceptionHandler.ResultHandle<Response>((response) =>
            {


                using (conn = new SqlConnection(string.Format(this._configuration.GetConnectionString("Helpy"), _dbNameFinder.FindName())))
                {

                    using var command = new SqlCommand("", conn);
                    conn.Open();


                    command.CommandTimeout = 300;
                    command.CommandText = @$"
Update	PIT
Set		FullVariantValuesDescription = T.V
From	ProductInformationTranslations As PIT
Join	(
Select		[PI].Id, String_Agg(VT.[Name] + ': ' + VVT.[Value], ', ') As V
From		ProductInformations As [PI]
Join		ProductInformationVariants As PIV
On			[PI].Id = PIV.ProductInformationId
Join		VariantValues As VV
On			PIV.VariantValueId = VV.Id
Join		VariantValueTranslations As VVT
On			VV.Id = VVT.VariantValueId
Join		Variants As V
On			V.Id = VV.VariantId
Join		VariantTranslations As VT
On			V.Id = VT.VariantId
Group By	[PI].Id
) As T
On PIT.ProductInformationId = T.Id
Where  PIT.TenantId=@TenantId
And PIT.DomainId=@DomainId
";

                    command.Parameters.AddWithValue("@TenantId", this._tenantFinder.FindId());
                    command.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());

                    var dataReader = command.ExecuteNonQuery();



                    response.Success = true;

                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bilinmeyen bir hata oluştu.";

                conn?.Close();
            });
        }

        private void RunProductInformationTranslationBreadcrumb(List<Category> categories, List<Product> products)
        {


            var productInformationTranslationBreadcrumbs = new List<ProductInformationTranslationBreadcrumb>();

            foreach (var product in products)
            {
                foreach (var productInformation in product.ProductInformations)
                {
                    foreach (var productInformationTranslation in productInformation.ProductInformationTranslations)
                    {
                        var html = new StringBuilder();
                        html.AppendLine("<ul>");
                        var categoryProducts = RecursiveCategoryProduct(categories, product.CategoryId, false);
                        categoryProducts.Reverse();
                        foreach (var categoryProduct in categoryProducts)
                        {
                            html.AppendLine(categoryProduct);
                        }

                        var productInformationTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(productInformationTranslation.Name.ToLower().ReplaceChar());
                        html.AppendLine($"<li>{productInformationTranslationName}</li>");

                        html.AppendLine("</ul>");
                        productInformationTranslationBreadcrumbs.Add(new ProductInformationTranslationBreadcrumb
                        {
                            Id = productInformationTranslation.Id,
                            CreatedDate = DateTime.Now,

                            Html = html.ToString()
                        });
                    }

                }
            }
            _unitOfWork.ProductInformationTranslationBreadcrumbRepository.BulkInsert(productInformationTranslationBreadcrumbs);
        }

        private void RunCategoryTranslationBreadcrumb(List<Category> categories)
        {

            var CategoryTranslationBreadcrumbs = new List<CategoryTranslationBreadcrumb>();

            foreach (var category in categories)
            {
                foreach (var categoryTranslation in category.CategoryTranslations)
                {
                    var html = new StringBuilder();
                    html.AppendLine("<ul>");
                    var categoryProducts = RecursiveCategoryProduct(categories, category.Id, true);
                    categoryProducts.Reverse();
                    foreach (var categoryProduct in categoryProducts)
                    {
                        html.AppendLine(categoryProduct);
                    }
                    html.AppendLine("</ul>");
                    CategoryTranslationBreadcrumbs.Add(new CategoryTranslationBreadcrumb
                    {
                        Id = categoryTranslation.Id,
                        CreatedDate = DateTime.Now,

                        Html = html.ToString()
                    });
                }
            }
            _unitOfWork.CategoryTranslationBreadcrumbRepository.BulkInsert(CategoryTranslationBreadcrumbs);
        }

        private List<string> RecursiveCategoryProduct(List<Category> categories, int categoryId, bool first)
        {
            var html = new List<string>();

            var category = categories.FirstOrDefault(x => x.Id == categoryId);
            var CategoryTranslation = category.CategoryTranslations.FirstOrDefault();

            var categoryTranslationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CategoryTranslation.Name.ToLower().ReplaceChar());

            if (!first)
                html.Add($"<li><a href=\"{CategoryTranslation.Url}\">{categoryTranslationName}</a> </li>");
            else
                html.Add($"<li>{categoryTranslationName}</li>");


            if (category.ParentCategoryId.HasValue)
            {
                html.AddRange(RecursiveCategoryProduct(categories, category.ParentCategoryId.Value, false));
            }

            return html;
        }

        private void SendErrorMail(string body)
        {
            try
            {
                var fromAddress = new MailAddress("burakzengin1988@gmail.com", "Burak Zengin");
                string fromPassword = "zllhnynhgspirtav";
                string subject = "Simsar Alarm";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage()
                {
                    From = fromAddress,
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    message.To.Add(new MailAddress("burakzengin1988@gmail.com", "Burak Zengin"));
                    message.To.Add(new MailAddress("aytac.kalem12@gmail.com", "Aytaç Kalem"));

                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private bool Download(string path, string fileName, int productInformationId, string url)
        {
            var success = true;


            string directory = $@"{path}\product\{productInformationId}";

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            string pathUrl = $@"{path}\product\{productInformationId}\{fileName.Replace("/", "\\")}";
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, pathUrl);
                    Console.WriteLine("Resim yüklendi");
                }
                catch (Exception e)
                {
                    success = false;
                }
            }

            return success;
        }
        #endregion
    }
}
