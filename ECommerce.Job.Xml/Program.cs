﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplaceCatalog;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure.MarketplaceCatalog;
using ECommerce.Job.Xml;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Accounting
{
    class Program
    {
        #region Methods
        static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfiguration configuration)
        {
            ServiceCollection serviceCollection = new();


            serviceCollection.AddScoped<IApp, App>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddScoped<IDbService, DbService>();

            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddSingleton<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();

            serviceCollection.AddSingleton<IMarketplaceVariantService, MarketplaceVariantService>();
            serviceCollection.AddSingleton<IMarketplaceVariantValueService, MarketplaceVariantValueService>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            }, ServiceLifetime.Scoped);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            serviceCollection.AddScoped<IXmlService, XmlService>();

            serviceCollection.AddScoped<IMain, Main>();


            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main()
        {
            await ConfigureServiceCollection(GetConfiguration()).GetService<IMain>()?.Run();
        }
        #endregion
    }
}