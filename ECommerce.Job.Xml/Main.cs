﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Job.Xml
{
    public class Main : IMain
    {
        #region Fields
        private readonly IDbService _dbService;

        private readonly IServiceProvider _serviceProvider;
        #endregion

        #region Constructors
        public Main(IDbService dbService, IServiceProvider serviceProvider)
        {
            #region Fields
            this._dbService = dbService;
            this._serviceProvider = serviceProvider;
            #endregion
        }
        #endregion

        #region Methods
        public async Task Run()
        {
            var getNameResponse = await this._dbService.GetNamesAsync();
            if (getNameResponse.Success == false)
            {
                //TO DO: Log
            }
            var dbNames = getNameResponse.Data;
            if (dbNames.HasItem())
                foreach (var dbName in dbNames)
                {

#if DEBUG
                    if (dbName != "Simge")
                        continue;
#endif


                    using (var scope = this._serviceProvider.CreateScope())
                    {

                        var dbNameFinder = scope.ServiceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(dbName);

                        var _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                        var tenantFinder = (DynamicTenantFinder)scope.ServiceProvider.GetRequiredService<ITenantFinder>();
                        var domainFinder = (DynamicDomainFinder)scope.ServiceProvider.GetRequiredService<IDomainFinder>();
                        var companyFinder = (DynamicCompanyFinder)scope.ServiceProvider.GetRequiredService<ICompanyFinder>();

                        var tenants = await _unitOfWork.TenantRepository.DbSet().Include(t => t.TenantSetting).ToListAsync();
                        foreach (var theTenant in tenants)
                        {
                            tenantFinder._tenantId = theTenant.Id;

                            var domains = await _unitOfWork.DomainRepository.DbSet().Include(d => d.DomainSetting).ToListAsync();
                            foreach (var theDomain in domains)
                            {
                                domainFinder._domainId = theDomain.Id;


                                var appV2 = scope.ServiceProvider.GetRequiredService<IApp>();
                                await appV2.RunAsync(theTenant, theDomain);

                            }
                        }
                    }

                }
        }
        #endregion
    }
}