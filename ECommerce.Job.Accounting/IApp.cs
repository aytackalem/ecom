﻿namespace ECommerce.Job.Accounting
{
    public interface IApp
    {
        #region Methods
        void Run(string dbName); 
        #endregion
    }
}
