﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.Accounting
{
    public class DynamicCompanyFinder : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Constructors
        public DynamicCompanyFinder()
        {
        }
        #endregion

        #region Methods
        public int FindId()
        {
            return _companyId;
        }

        public void Set(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
