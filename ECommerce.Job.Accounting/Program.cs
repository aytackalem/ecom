﻿using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.EInvoice;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.Accounting
{
    class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IApp, App>();

            serviceCollection.AddScoped<ITenantFinder, DynamicTenantFinder>();

            serviceCollection.AddSingleton<IDomainFinder, DynamicDomainFinder>();

            serviceCollection.AddSingleton<IDbNameFinder, DynamicDbNameFinder>();

            serviceCollection.AddSingleton<ICompanyFinder, DynamicCompanyFinder>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddTransient<IEInvoiceService, EInvoiceService>();

            serviceCollection.AddTransient<IAccountingProviderService, AccountingProviderService>();

            serviceCollection.AddInfrastructureServices(configuration);


            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddTransient<ICacheHandler, CacheHandler>();
            serviceCollection.AddMemoryCache();

            return serviceCollection.BuildServiceProvider();
        }   

        static async Task Main(string[] args)
        {


#if DEBUG
            args = new string[] { "Mizalle" };
#endif

            if (args.Length == 0)
            {

                Console.WriteLine("Stok Transfer Fişi için lütfen bir tane database ismi giriniz");
                Console.Read();
                return;
            }
            Console.WriteLine($"{args[0]} Stok Trasfer Fişi Başladı.");


            ConfigureServiceCollection(GetConfiguration())
                .GetService<IApp>()
                .Run(args[0]);
        }
        #endregion
    }
}