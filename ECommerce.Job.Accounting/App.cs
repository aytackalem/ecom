﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.EInvoice;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ECommerce.Job.Accounting
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly ITenantFinder _tenantFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IAccountingProviderService _accountingProviderService;

        private Tenant _tenant;

        private Domain.Entities.Domain _domain;

        private Company _company;

        private readonly IEInvoiceService _eInvoiceService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, ITenantFinder tenantFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, IDbNameFinder dbNameFinder, IAccountingProviderService accountingProviderService, IEInvoiceService eInvoiceService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._tenantFinder = tenantFinder;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._dbNameFinder = dbNameFinder;
            this._accountingProviderService = accountingProviderService;
            this._eInvoiceService = eInvoiceService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run(string dbName)
        {

            this._dbNameFinder.Set(dbName);



            var tenants = this
                ._unitOfWork
                .TenantRepository
                .DbSet()
                .Include(t => t.TenantSetting)
                .ToList();
            foreach (var theTenant in tenants)
            {
                ((DynamicTenantFinder)this._tenantFinder)._tenantId = theTenant.Id;
                this._tenant = theTenant;

                var domains = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(d => d.DomainSetting)
                    .ToList();
                foreach (var theDomain in domains)
                {
                    ((DynamicDomainFinder)this._domainFinder)._domainId = theDomain.Id;
                    this._domain = theDomain;

                    var companies = this
                        ._unitOfWork
                        .CompanyRepository
                        .DbSet()
                        .Include(c => c.CompanySetting)
                        .ToList();
                    foreach (var theCompany in companies)
                    {
                        ((DynamicCompanyFinder)this._companyFinder)._companyId = theCompany.Id;
                        this._company = theCompany;




                        Console.WriteLine($"Company: {theCompany.Name}.");

                        CancelWarehouseStockTransfer();

                        WarehouseStockTransfer();

                        //Invoice();

                        //Return();
                    }
                }
            }
        }

        /// <summary>
        /// Teslim edilmiş siparişlerin faturalarını muhasebe programına kaydeden metod.
        /// </summary>
        public void Invoice()
        {
            var orders = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderInvoiceInformation)
                .Include(o => o.OrderDetails)
                .Where(o => o.OrderTypeId == OrderTypes.TeslimEdildi &&
                            o.OrderBilling == null &&
                            (
                                o.OrderSourceId == OrderSources.Pazaryeri ||
                                o.OrderSourceId == OrderSources.Temsilci ||
                                o.OrderSourceId == OrderSources.TemsilciInfluencer ||
                                o.OrderSourceId == OrderSources.Diger
                            )
                      )
                .ToList();

            foreach (var oLoop in orders)
            {
                //this._eInvoiceService.CreateInvoice(oLoop.Id);

                /* Hatalara Karşı Kontroller */

                /* 
                 * 1. PSF SF den küçük olamaz.
                 */
                if (oLoop.OrderDetails.Any(od => od.ListUnitPrice < od.UnitPrice))
                {
                    //TO DO: Email gönder
                    continue;
                }

                if (oLoop.OrderDetails.Any(od => od.ListUnitPrice > od.UnitPrice && od.UnitDiscount == 0))
                {
                    //TO DO: Email gönder
                    continue;
                }

                /*
                 * 2. KDV hesaplaması
                 */
                if (oLoop.OrderDetails.Any(od =>
                {
                    var difference = od.VatExcListUnitPrice - Math.Round(od.ListUnitPrice / (od.VatRate + 1M), 4);
                    if (difference != 0 && difference != 0.0001M && difference != -0.0001M)
                        return true;

                    difference = od.VatExcUnitPrice - Math.Round(od.UnitPrice / (od.VatRate + 1M), 4);
                    if (difference != 0 && difference != 0.0001M && difference != -0.0001M)
                        return true;

                    return false;
                }))
                {
                    //TO DO: Email gönder
                    continue;
                }

                var createResponse = this._accountingProviderService.CreateInvoice(oLoop.Id);
                if (createResponse.Success)
                {
                    oLoop.OrderBilling = new Domain.Entities.OrderBilling
                    {
                        CreatedDate = DateTime.Now,
                        InvoiceNumber = createResponse.Data.Guid
                    };
                    oLoop.OrderTypeId = "TA";
                    this._unitOfWork.OrderRepository.Update(oLoop);

                    this._accountingProviderService.DeleteStockTransfer(oLoop.Id);

                    Console.WriteLine($"{orders.Count}/{orders.IndexOf(oLoop)} Success");
                }
                else
                {
                    Console.WriteLine($"{orders.Count}/{orders.IndexOf(oLoop)} {createResponse.Message}");
                    //TO DO: Email gönder
                }
            }
        }

        /// <summary>
        /// İadeleri muhasebe programına kaydeden metod.
        /// </summary>
        public void Return()
        {
            var orderIds = this
                ._unitOfWork
                .OrderBillingRepository
                .DbSet()
                .Where(ob => !string.IsNullOrEmpty(ob.ReturnDescription) && string.IsNullOrEmpty(ob.ReturnNumber))
                .Select(ob => ob.Id)
                .ToList();

            foreach (var oiLoop in orderIds)
            {
                var accountingCancel = this._accountingProviderService.CancelInvoice(oiLoop);
                if (accountingCancel.Success)
                {
                    var orderBilling = this
                        ._unitOfWork
                        .OrderBillingRepository
                        .DbSet()
                        .FirstOrDefault(ob => ob.Id == oiLoop);

                    orderBilling.ReturnNumber = accountingCancel.Data.DocumentNo;

                    this
                        ._unitOfWork
                        .OrderBillingRepository
                        .Update(orderBilling);
                }
            }
        }

        /// <summary>
        /// Yeni siparişleri stok düşümü için muhasebe programına kaydeden metod.
        /// </summary>
        public void WarehouseStockTransfer()
        {
            var lastStockTransferOrderIdResponse = this._accountingProviderService.ReadLastStockTransfer();
            if (!lastStockTransferOrderIdResponse.Success)
                return;

            var lastStockTransferOrderId = lastStockTransferOrderIdResponse.Data;

#warning Stok dusulebilir kaynaklar veya siparise stoktan dus koyulabilir

            //var ids = new int[]
            //{
            //    396
            //};

            var orderIds = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                //.Where(x => ids.Contains(x.Id))
                .Where(o => (o.Id > lastStockTransferOrderId &&
                            o.OrderTypeId != OrderTypes.Iptal &&
                            o.OrderTypeId != OrderTypes.Iade &&
                            o.OrderTypeId != OrderTypes.Tamamlandi &&
                            o.MarketplaceId != "TYE" &&
                            string.IsNullOrEmpty(o.OrderInvoiceInformation.Number) &&
                            (
                                o.OrderSourceId == OrderSources.Pazaryeri ||
                                o.OrderSourceId == OrderSources.Web ||
                                o.OrderSourceId == 6 ||
                                o.OrderSourceId == 7 ||
                                o.OrderSourceId == 8
                            )) || (o.MarketplaceId == "TYE" && new string[] {
                                OrderTypes.OnaylanmisSiparis,
                                OrderTypes.Beklemede
                            }.Contains(o.OrderTypeId))
                      )
                .Select(o => o.Id)
                .ToList();

            Console.WriteLine(orderIds.Count);
            int counter = 0;
            foreach (var oiLoop in orderIds)
            {
                counter++;
                Console.WriteLine(counter);

                var stockTransferResponse = this._accountingProviderService.CreateStockTransfer(oiLoop);
                //if (!stockTransferResponse.Success)
                //    break;
            }
        }

        /// <summary>
        /// İptal siparişleri stok arttırımı için muhasebe programına kaydeden metod.
        /// </summary>
        public void CancelWarehouseStockTransfer()
        {
            var dataResponse = this._accountingProviderService.ReadStockTransfers();
            if (!dataResponse.Success)
                return;

            Console.WriteLine($"Waiting stock: {dataResponse.Data.Count}");

            int counter = 0;
            dataResponse.Data.ForEach(orderId =>
            {
                counter++;
                Console.WriteLine(counter);

                var order = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .FirstOrDefault(o => o.Id == orderId && o.OrderTypeId == "IP");

                if (order != null)
                {
                    var response = this._accountingProviderService.DeleteStockTransfer(orderId);
                    if (response.Success)
                    {
                        Console.WriteLine($"{counter} removed.");
                    }
                }
            });
        }

        /// <summary>
        /// Muhasebe programından stokları alıp sistemi güncelleyen metod.
        /// </summary>
        public void StockUpdate()
        {
            var productInformations = this
                ._unitOfWork
                .ProductInformationRepository
                .DbSet()
                .Include(pi => pi.ProductInformationCombines)
                .ThenInclude(pic => pic.ContainProductInformation)
                .ToList();

            foreach (var piLoop in productInformations.Where(pi => pi.Type == "PI"))
                piLoop.Stock = this._accountingProviderService.ReadStock(piLoop.StockCode, "1").Data;

            var productCombines = productInformations.Where(pi => pi.Type == "PC");
            foreach (var piLoop in productCombines)
            {
                if (piLoop.ProductInformationCombines.Count == 1)
                {
                    piLoop.Stock = piLoop.ProductInformationCombines[0].ContainProductInformation.Stock / piLoop.ProductInformationCombines[0].Quantity;
                }
                else
                {
                    piLoop.Stock = piLoop.ProductInformationCombines.Min(pic => pic.ContainProductInformation.Stock);
                }
            }

            foreach (var piLoop in productInformations)
                this
                    ._unitOfWork
                    .ProductInformationRepository
                    .Update(piLoop);
        }
        #endregion
    }
}
