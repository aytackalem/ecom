﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Subscription.Job.Base
{
    public interface IApp
    {
        void Run();
    }
}
