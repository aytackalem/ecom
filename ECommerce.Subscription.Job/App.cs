﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities;
using ECommerce.Subscription.Job.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Subscription.Job
{
    public class App : IApp
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISmsService _smsService;

        public App(IUnitOfWork unitOfWork, ISmsService smsService)
        {
            #region Field
            this._unitOfWork = unitOfWork;
            this._smsService = smsService;
            #endregion
        }

        public void Run()
        {

            var productInformationSubscriptions = this._unitOfWork.ProductInformationSubscriptionRepository
                .DbSet()
                .Include(p => p.ProductInformation.ProductInformationTranslations)
                .Where(x =>
                       !x.IsSend &&
                       x.ProductInformation.IsSale &&
                       x.ProductInformation.Product.Active).ToList();


            var configrations = this
             ._unitOfWork
             .ConfigurationRepository
             .DbSet()
             .OrderByDescending(x => x.Id)
             .Select(c => new KeyValue<string, string>
             {
                 Key = c.Key,
                 Value = c.Value
             })
             .ToList();



            foreach (var productInformationSubscription in productInformationSubscriptions)
            {
                var productInformationTranslation = productInformationSubscription.ProductInformation.ProductInformationTranslations.FirstOrDefault(x => x.LanguageId == "TR");

                var _phoneNumber = configrations.FirstOrDefault(x => x.Key == "PhoneNumber");
                var _webUrl = configrations.FirstOrDefault(x => x.Key == "WebUrl");

                var productUrl = _webUrl != null ? $"{_webUrl.Value}/{productInformationTranslation.Url}" : "";
                var phoneNumber = _phoneNumber != null ? $"Tel:{_phoneNumber.Value}" : "";

                var message = $"Heyecanla beklediğin {productInformationTranslation.Name} şimdi stoklarda! {(productInformationSubscription.ProductInformation.Payor ? "Online sipariş ve ücretsiz kargo için" : "Online sipariş için")} hemen tıkla {productUrl} {phoneNumber}";


                //var result = this._smsService.Send(productInformationSubscription.PhoneNumber, message);

                //if (result.Success)
                //{
                //    Console.WriteLine($"Send {productInformationSubscription.PhoneNumber} {message}");
                //    productInformationSubscription.IsSend = true;
                //    _unitOfWork.ProductInformationSubscriptionRepository.Update(productInformationSubscription);
                //}
            }

        }


    }
}
