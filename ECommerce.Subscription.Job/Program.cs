﻿using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Infrastructure.Communication;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using ECommerce.Subscription.Job.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ECommerce.Subscription.Job
{
    class Program
    {
        static void Main(string[] args)
        {

            var serviceProvider = ConfigureServiceCollection();
            var app = serviceProvider.GetService<IApp>();
            app.Run();
        }

        static IServiceProvider ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configurationBuilder.Build();

            serviceCollection.AddSingleton<IApp, App>();
            //var appConfig = configuration.GetSection("AppConfig").Get<AppConfig>();

            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Helpy"));
            });
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

           

            return serviceCollection.BuildServiceProvider();
        }
    }
}
