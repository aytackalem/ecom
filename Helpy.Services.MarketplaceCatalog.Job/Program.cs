﻿using AutoMapper;
using Helpy.Services.MarketplaceCatalog.Core.Application.DataTransferObjects;
using Helpy.Services.MarketplaceCatalog.Core.Application.Interfaces.Marketplaces;
using Helpy.Services.MarketplaceCatalog.Persistence;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.DependencyInjection;

namespace Helpy.Services.MarketplaceCatalog.Job
{
    internal class Program
    {
        #region Constants
        private const string _marketplaceCatalogApiBase = "https://marketplacecatalog.helpy.com.tr";
        //private const string _marketplaceCatalogApiBase = "http://localhost:5100";
        #endregion

        #region Methods
        static IServiceProvider ConfigureServiceCollection()
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddInfrustructureServices();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            args = new string[1] { "Category" };

            var serviceProvider = ConfigureServiceCollection();
            using (var scope = serviceProvider.CreateScope())
            {
                var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
                var httpHelper = scope.ServiceProvider.GetRequiredService<IHttpHelper>();

                if (args[0] == "Category")
                {
                    var marketplaces = scope.ServiceProvider.GetServices<IMarketplace>();

                    foreach (var theMarketplace in marketplaces)
                    {
                        /*
                         * 
                         * LCW değilse atla!
                         * 
                         */
                        if (theMarketplace.Id != "BY")
                            continue;

                        List<Core.Application.Wrappers.Marketplaces.VariantValue> marketplaceVariantValues = new();

                        #region Category
                        var getCategoriesReponse = await theMarketplace.GetCategoriesAsync();
                        var postCategoriesResponse = await httpHelper.SendAsync<List<Category>, DataResponse<List<Category>>>(new Shared.Http.HttpRequest<List<Category>>
                        {
                            RequestUri = $"{_marketplaceCatalogApiBase}/Category",
                            HttpMethod = HttpMethod.Post,
                            Request = mapper.Map<List<Category>>(getCategoriesReponse.Data)
                        });
                        if (postCategoriesResponse.Success == false || postCategoriesResponse.Content.Success == false)
                        {
                        }
                        #endregion

                        foreach (var theCategory in getCategoriesReponse.Data)
                        {
                            ///*
                            // * 
                            // * Abiye 5157 değilse atla!
                            // *
                            // */

                            //if(theCategory.Code != "5157")
                            //{
                            //    continue;
                            //}

                            try
                            {
                             
                                #region Variant
                                var getVariantsResponse = await theMarketplace.GetVariantsAsync(theCategory.Code);
                                var postVariantResponse = await httpHelper.SendAsync<List<Variant>, DataResponse<List<Variant>>>(new Shared.Http.HttpRequest<List<Variant>>
                                {
                                    RequestUri = $"{_marketplaceCatalogApiBase}/Variant/Insert",
                                    HttpMethod = HttpMethod.Post,
                                    Request = mapper.Map<List<Variant>>(getVariantsResponse.Data)
                                });
                                if (postVariantResponse.Success == false || postVariantResponse.Content.Success == false)
                                {
                                }
                                #endregion

                                marketplaceVariantValues.AddRange(getVariantsResponse.Data.Where(d => d.VariantValues != null).SelectMany(d => d.VariantValues));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        try
                        {

                            #region Variant Value
                            marketplaceVariantValues = marketplaceVariantValues
                                            .GroupBy(vv => new
                                            {
                                                vv.VariantCode,
                                                vv.Code
                                            })
                                            .Select(vv => new Core.Application.Wrappers.Marketplaces.VariantValue
                                            {
                                                VariantCode = vv.Key.VariantCode,
                                                Code = vv.Key.Code,
                                                Name = vv.First().Name,
                                                CategoryCodes = vv.SelectMany(x => x.CategoryCodes).ToList()
                                            })
                                            .ToList();

                            var variantValues = mapper.Map<List<VariantValue>>(marketplaceVariantValues);
                            variantValues.ForEach(vv => vv.MarketplaceId = theMarketplace.Id);

                            var pagesCount = Math.Ceiling(variantValues.Count / 1000M);

                            for (int i = 0; i < pagesCount; i++)
                            {
                                var postValueResponse = await httpHelper.SendAsync<List<VariantValue>, DataResponse<List<VariantValue>>>(new Shared.Http.HttpRequest<List<VariantValue>>
                                {
                                    RequestUri = $"{_marketplaceCatalogApiBase}/VariantValue/Insert",
                                    HttpMethod = HttpMethod.Post,
                                    Request = variantValues.Skip(i * 1000).Take(1000).ToList()
                                });
                                if (postValueResponse.Success == false || postValueResponse.Content.Success == false)
                                {
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {


                        }
                    }
                }

                if (args[0] == "Brand")
                {
                    var marketplaces = scope.ServiceProvider.GetServices<IBrandDownloadable>();

                    foreach (var theMarketplace in marketplaces)
                    {
                        /*
                         * 
                         * LCW değilse atla!
                         * 
                         */
                        if (theMarketplace.Id != "BY")
                            continue;

                        var getBrandsReponse = await theMarketplace.GetBrandsAsync();
                        if (getBrandsReponse.Success)
                        {
                            var pagesCount = Math.Ceiling(getBrandsReponse.Data.Count / 1000M);
                            for (int i = 0; i < pagesCount; i++)
                            {
                                var brands = mapper.Map<List<Brand>>(getBrandsReponse.Data.Skip(i * 1000).Take(1000));
                                brands.ForEach(b => b.MarketplaceId = theMarketplace.Id);

                                var postBrandsResponse = await httpHelper.SendAsync<List<Brand>, DataResponse<List<Brand>>>(new Shared.Http.HttpRequest<List<Brand>>
                                {
                                    RequestUri = $"{_marketplaceCatalogApiBase}/Brand",
                                    HttpMethod = HttpMethod.Post,
                                    Request = brands
                                });
                            }
                        }
                    }
                }

            }
        }
        #endregion
    }
}