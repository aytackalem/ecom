﻿namespace ECommerce.Shipment.Common
{
    public class ShipmentInfoDetail
    {
        #region Properties
        public string TrackingCode { get; set; }

        public string Weight { get; set; }
        #endregion
    }
}
