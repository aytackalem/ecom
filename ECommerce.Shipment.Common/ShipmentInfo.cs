﻿using System.Collections.Generic;

namespace ECommerce.Shipment.Common
{
    public class ShipmentInfo
    {
        #region Properties
        public string ShipmentCompanyId { get; set; }

        public string TrackingUrl { get; set; }

        public string TrackingBase64 { get; set; }

        public string Barcode { get; set; }
        #endregion

        #region Navigation Properties
        public List<ShipmentInfoDetail> ShipmentInfoDetails { get; set; }
        #endregion
    }
}
