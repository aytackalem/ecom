﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class AccountingService : ServiceBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Helpers.IDomainFinder _domainFinder;

        private readonly Application.Common.Interfaces.Helpers.ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public AccountingService(Application.Common.Interfaces.Helpers.IDomainFinder domainFinder, Application.Common.Interfaces.Helpers.ICompanyFinder companyFinder, IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService) : base(httpHelper, configuration, identityService)
        {
            #region Fields
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<AccountingInfo>> CreateInvoiceAsync(List<int> orderIds)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<AccountingInfo>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<List<int>, DataResponse<AccountingInfo>>(new HttpRequest<List<int>>
                {
                    Timeout = 300,
                    Request = orderIds,
                    HttpMethod = HttpMethod.Post,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Accounting/CreateInvoiceList",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            }, (e) =>
            {
                Directory.CreateDirectory(@"C:\Logs\Helpy\Response");
                File.WriteAllText(@$"C:\Logs\Helpy\Request\{Guid.NewGuid()}.txt", e.Message);
            });
        }


        public async Task<DataResponse<AccountingInfo>> CreateInvoiceAsync(int orderId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<AccountingInfo>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<AccountingInfo>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Accounting/CreateInvoice?OrderId={orderId}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<NoContentResponse>> CreateStockTransferAsync(int orderId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<NoContentResponse>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Post,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Accounting/CreateStockTransfer?OrderId={orderId}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<AccountingCancel>> CancelInvoiceAsync(int orderId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<AccountingCancel>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<AccountingCancel>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Delete,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Accounting/CancelInvoice?OrderId={orderId}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<NoContentResponse>> DeleteStockTransferAsync(int orderId)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<NoContentResponse>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Delete,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Accounting/DeleteStockTransfer?OrderId={orderId}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }
        #endregion
    }
}
