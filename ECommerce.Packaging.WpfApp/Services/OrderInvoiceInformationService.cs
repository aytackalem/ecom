﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class OrderInvoiceInformationService : ServiceBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Helpers.IDomainFinder _domainFinder;

        private readonly Application.Common.Interfaces.Helpers.ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public OrderInvoiceInformationService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService, Application.Common.Interfaces.Helpers.IDomainFinder domainFinder, Application.Common.Interfaces.Helpers.ICompanyFinder companyFinder) : base(httpHelper, configuration, identityService)
        {
            #region Fields
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<NoContentResponse> UpdateAsync(OrderInvoiceInformation orderInvoiceInformation)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<OrderInvoiceInformation, NoContentResponse>(new HttpRequest<OrderInvoiceInformation>
                {
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{base._configuration["ApiUrl"]}/OrderInvoiceInformation",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    },
                    Request = orderInvoiceInformation
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }
        #endregion
    }
}
