﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Response;
using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class IdentityService
    {
        #region Fields
        private const string _identityServerUrl = "https://identity.helpy.com.tr";

        //private const string _identityServerUrl = "http://localhost:5001";

        private Identity _identity = null;
        #endregion

        #region Methods
        public async Task<NoContentResponse> LoginAsync(string username, string password)
        {
            return await ExceptionHandler.HandleAsync<NoContentResponse>(async response =>
            {
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                {
                    response.Message = "Kullanıcı adı veya şifre boş olamaz";
                    response.Success = false;
                    return;
                }
                using (HttpClient _httpClient = new())
                {
                    var disco = await _httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
                    {
                        Address = _identityServerUrl,
                        Policy = new DiscoveryPolicy { RequireHttps = true }
                    });

                    if (disco.IsError)
                    {
                        response.Message = "Bilinmeyen bir hata oluştu.";
                        response.Exception = disco.Exception;
                        return;
                    }

                    var passwordTokenRequest = new PasswordTokenRequest
                    {
                        ClientId = "PackingForUser",
                        ClientSecret = "secret",
                        UserName = username,
                        Password = password,
                        Address = disco.TokenEndpoint
                    };

                    var token = await _httpClient.RequestPasswordTokenAsync(passwordTokenRequest);

                    if (token.IsError)
                    {
                        response.Message = "Kullanıcı adı veya şifre yanlış.";
                        return;
                    }

                    var userInfoRequest = new UserInfoRequest
                    {
                        Token = token.AccessToken,
                        Address = disco.UserInfoEndpoint
                    };

                    var userInfo = await _httpClient.GetUserInfoAsync(userInfoRequest);
                    if (userInfo.IsError)
                    {
                        response.Message = "Bilinmeyen bir hata oluştu.";
                        return;
                    }

                    Identity identity = new()
                    {
                        AccessToken = token.AccessToken,
                        RefreshToken = token.RefreshToken,
                        Expiry = DateTime.Now.AddSeconds(23 * 60 * 60)
                    };

                    this._identity = identity;
                }

                response.Success = true;
            });
        }

        public async Task<DataResponse<Identity>> GetIdentity()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Identity>>(async response =>
            {
                if (this._identity == null)
                {
                    response.Success = false;
                    return;
                }

                if (this._identity.Expiry < DateTime.Now.AddSeconds(60))
                {
                    #region Refresh Token
                    using (HttpClient _httpClient = new())
                    {
                        var disco = await _httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
                        {
                            Address = _identityServerUrl,
                            Policy = new DiscoveryPolicy { RequireHttps = true }
                        });

                        if (disco.IsError)
                        {
                            response.Message = "Bilinmeyen bir hata oluştu.";
                            response.Exception = disco.Exception;
                            return;
                        }

                        var refreshTokenRequest = new RefreshTokenRequest
                        {
                            ClientId = "PackingForUser",
                            ClientSecret = "secret",
                            RefreshToken = this._identity.RefreshToken,
                            Address = disco.TokenEndpoint
                        };

                        var token = await _httpClient.RequestRefreshTokenAsync(refreshTokenRequest);

                        if (token.IsError)
                        {
                            response.Message = "Kullanıcı adı veya şifre yanlış.";
                            return;
                        }

                        var userInfoRequest = new UserInfoRequest
                        {
                            Token = token.AccessToken,
                            Address = disco.UserInfoEndpoint
                        };

                        var userInfo = await _httpClient.GetUserInfoAsync(userInfoRequest);
                        if (userInfo.IsError)
                        {
                            response.Message = "Bilinmeyen bir hata oluştu.";
                            return;
                        }

                        Identity identity = new()
                        {
                            AccessToken = token.AccessToken,
                            RefreshToken = token.RefreshToken,
                            Expiry = DateTime.Now.AddDays(token.ExpiresIn)
                        };

                        this._identity = identity;
                    }
                    #endregion
                }

                response.Data = this._identity;
                response.Success = true;
            });
        }
        #endregion
    }
}
