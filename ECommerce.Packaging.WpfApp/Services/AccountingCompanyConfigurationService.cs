﻿using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.Http.Base;
using Microsoft.Extensions.Configuration;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class AccountingCompanyConfigurationService : ServiceBase
    {
        #region Constructors
        public AccountingCompanyConfigurationService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService) : base(httpHelper, configuration, identityService)
        {
        } 
        #endregion
    }
}
