﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Parameters.Order;
using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class OrderReturnService : ServiceBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Helpers.IDomainFinder _domainFinder;

        private readonly Application.Common.Interfaces.Helpers.ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public OrderReturnService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService, Application.Common.Interfaces.Helpers.IDomainFinder domainFinder, Application.Common.Interfaces.Helpers.ICompanyFinder companyFinder) : base(httpHelper, configuration, identityService)
        {
            #region Fields
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods

        public async Task<DataResponse<NoContentResponse>> UpdateAsync(OrderReturn orderReturn)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<OrderReturn, DataResponse<NoContentResponse>>(new HttpRequest<OrderReturn>
                {
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{base._configuration["ApiUrl"]}/OrderReturn",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Request = orderReturn,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        #endregion
    }
}
