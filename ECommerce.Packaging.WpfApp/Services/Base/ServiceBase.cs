﻿using Helpy.Shared.Http.Base;
using Microsoft.Extensions.Configuration;

namespace ECommerce.Packaging.WpfApp.Services.Base
{
    public abstract class ServiceBase
    {
        #region Fields
        protected readonly IHttpHelper _httpHelper;

        protected readonly IConfiguration _configuration;

        protected readonly IdentityService _identityService;
        #endregion

        #region Constructors
        public ServiceBase(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService)
        {
            #region Fields
            this._httpHelper = httpHelper;
            this._configuration = configuration;
            this._identityService = identityService;
            #endregion
        }
        #endregion
    }
}
