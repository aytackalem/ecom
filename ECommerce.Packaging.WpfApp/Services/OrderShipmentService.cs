﻿using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.Http.Base;
using Microsoft.Extensions.Configuration;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class OrderShipmentService : ServiceBase
    {
        #region Constructors
        public OrderShipmentService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService) : base(httpHelper, configuration, identityService)
        {
        } 
        #endregion
    }
}
