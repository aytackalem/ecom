﻿using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class DomainService : ServiceBase
    {
        #region Constructors
        public DomainService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService) : base(httpHelper, configuration, identityService)
        {
        }
        #endregion

        #region Methods
        public async Task<DataResponse<List<DataTransferObjects.Domain>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<DataTransferObjects.Domain>>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<List<DataTransferObjects.Domain>>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Domain",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }
        #endregion
    }
}
