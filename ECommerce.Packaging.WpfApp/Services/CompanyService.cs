﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class CompanyService : ServiceBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Helpers.IDomainFinder _domainFinder;
        #endregion

        #region Constructors
        public CompanyService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService, Application.Common.Interfaces.Helpers.IDomainFinder domainFinder) : base(httpHelper, configuration, identityService)
        {
            #region Fields
            this._domainFinder = domainFinder;
            #endregion
        }
        #endregion

        #region Methods

        public async Task<DataResponse<List<DataTransferObjects.Company>>> GetAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<Company>>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<List<Company>>>(new HttpRequest
                {
                    Headers = new() { { "DomainId", this._domainFinder.FindId().ToString() } },
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Company",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }
        #endregion
    }
}
