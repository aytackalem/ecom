﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Parameters.Order;
using ECommerce.Packaging.WpfApp.Services.Base;
using Helpy.Shared.ExceptionHandling;
using Helpy.Shared.Http;
using Helpy.Shared.Http.Base;
using Helpy.Shared.Response;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace ECommerce.Packaging.WpfApp.Services
{
    public class OrderService : ServiceBase
    {
        #region Fields
        private readonly Application.Common.Interfaces.Helpers.IDomainFinder _domainFinder;

        private readonly Application.Common.Interfaces.Helpers.ICompanyFinder _companyFinder;
        #endregion

        #region Constructors
        public OrderService(IHttpHelper httpHelper, IConfiguration configuration, IdentityService identityService, Application.Common.Interfaces.Helpers.IDomainFinder domainFinder, Application.Common.Interfaces.Helpers.ICompanyFinder companyFinder) : base(httpHelper, configuration, identityService)
        {
            #region Fields
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            #endregion
        }
        #endregion

        #region Methods
        public async Task<DataResponse<Order>> GetAsync(GetParameter parameter)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<Order>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var requestUri = $"{base._configuration["ApiUrl"]}/Order?OrderId={parameter.OrderId}&PackerBarcode={parameter.PackerBarcode}&ProductBarcode={parameter.ProductBarcode}&OrderFilterType={parameter.OrderFilterType}";

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<Order>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = requestUri,
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<int>> ReserveAsync(ReserveParameter parameter)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<int>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;
                
                var httpResponse = await base._httpHelper.SendAsync<DataResponse<int>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/Reserve?MarketplaceId={parameter.MarketplaceId}&ShipmentCompanyId={parameter.ShipmentCompanyId}&OrderId={parameter.OrderId}&OrderSourceId={parameter.OrderSourceId}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<NoContentResponse>> CancelAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<NoContentResponse>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/Cancel?Id={id}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<NoContentResponse>> CompleteAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<NoContentResponse>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/Complete?Id={id}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<NoContentResponse>> NotAvailableAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<NoContentResponse>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<NoContentResponse>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Put,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/NotAvailable?Id={id}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<bool>> PackedAsync(int id)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<bool>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<bool>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/Packed?Id={id}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });

                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<OrderSummary>> SummaryAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<OrderSummary>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<OrderSummary>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/Summary",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<List<string>>> GetTrendyolEuropePackagesAsync()
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<string>>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<List<string>>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/TrendyolEuropePackages",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }

        public async Task<DataResponse<List<int>>> GetTrendyolEuropeOrderNumbersAsync(string packageNumber)
        {
            return await ExceptionHandler.HandleAsync<DataResponse<List<int>>>(async response =>
            {
                var identityResponse = await base._identityService.GetIdentity();
                var identity = identityResponse.Data;

                var httpResponse = await base._httpHelper.SendAsync<DataResponse<List<int>>>(new HttpRequest
                {
                    HttpMethod = HttpMethod.Get,
                    RequestUri = $"{base._configuration["ApiUrl"]}/Order/TrendyolEuropeOrderNumbers?packagenumber={packageNumber}",
                    AuthorizationType = AuthorizationType.Bearer,
                    Authorization = identity.AccessToken,
                    Headers = new() {
                        { "DomainId", this._domainFinder.FindId().ToString() },
                        { "CompanyId", this._companyFinder.FindId().ToString() }
                    }
                });
                if (httpResponse.Success == true && httpResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    response.Success = httpResponse.Content.Success;
                    response.Data = httpResponse.Content.Data;
                    response.Message = httpResponse.Content.Message;
                }
            });
        }
        #endregion
    }
}
