﻿namespace ECommerce.Packaging.WpfApp.Parameters.Order
{
    public class GetParameter
    {
        #region Properties
        public int OrderId { get; set; }

        public string PackerBarcode { get; set; }

        public string ProductBarcode { get; set; }

        public string OrderFilterType { get; set; }
        #endregion
    }
}
