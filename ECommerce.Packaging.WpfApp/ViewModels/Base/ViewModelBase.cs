﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Services;
using Npgsql.Replication.PgOutput;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Automation;
using System.Windows.Input;
using System.Windows.Threading;

namespace ECommerce.Packaging.WpfApp.ViewModels.Base
{
    public abstract class ViewModelBase : NotifyPropertyChangedBase
    {
        #region Events Delegates
        public delegate void LoginEventHandler(bool login);

        public event LoginEventHandler Login;

        public delegate void ComplateEventHandler();

        public event ComplateEventHandler Complate;
        #endregion

        #region Service Fields
        private readonly IdentityService _identityService;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly OrderService _orderService;

        private readonly DomainService _domainService;

        private readonly CompanyService _companyService;
        #endregion

        #region Constructors
        public ViewModelBase(IdentityService identityService, IDomainFinder domainFinder, ICompanyFinder companyFinder, OrderService orderService, DomainService domainService, CompanyService companyService)
        {
            #region Service Fields
            this._identityService = identityService;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._orderService = orderService;
            this._domainService = domainService;
            this._companyService = companyService;
            #endregion
        }
        #endregion

        #region Filters
        #region Property Fields
        private ObservableCollection<DataTransferObjects.Domain> _domains = new();

        private DataTransferObjects.Domain _selectedDomain;

        private ObservableCollection<Company> _companies = new();

        private Company _selectedCompany;

        private int? _selectedOrderId;
        #endregion

        #region Properties
        public ObservableCollection<DataTransferObjects.Domain> Domains
        {
            get { return _domains; }
            set { _domains = value; OnPropertyChanged(); }
        }

        public DataTransferObjects.Domain SelectedDomain
        {
            get { return _selectedDomain; }
            set
            {
                this._domainFinder.Set(value.Id);

                this.Companies = new ObservableCollection<Company>();

                Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                {
                    var task = await this._companyService.GetAsync();
                    task.Data.ForEach(c => this.Companies.Add(c));

                    this.SelectedCompany = this.Companies.FirstOrDefault();

                    _selectedDomain = value;
                    OnPropertyChanged();
                });
            }
        }

        public ObservableCollection<Company> Companies
        {
            get { return _companies; }
            set { _companies = value; OnPropertyChanged(); }
        }

        public Company SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                this._companyFinder.Set(value.Id);

                _selectedCompany = value;
                OnPropertyChanged();
            }
        }

        public int? SelectedOrderId
        {
            get { return _selectedOrderId; }
            set { _selectedOrderId = value; OnPropertyChanged(); }
        }
        #endregion

        #region Command Fields
        private ICommand _domainSelectionVisibleChangeCommand;

        private ICommand _companySelectionVisibleChangeCommand;

        private ICommand _selectedDomainChangeCommand;

        private ICommand _selectedCompanyChangeCommand;
        #endregion

        #region Commands
        public ICommand DomainSelectionVisibleChangeCommand
        {
            get
            {
                if (this._domainSelectionVisibleChangeCommand == null)
                    this._domainSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        DomainSelectionVisible = !DomainSelectionVisible;
                    });

                return _domainSelectionVisibleChangeCommand;
            }
        }

        public ICommand CompanySelectionVisibleChangeCommand
        {
            get
            {
                if (this._companySelectionVisibleChangeCommand == null)
                    this._companySelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        CompanySelectionVisible = !CompanySelectionVisible;
                    });

                return _companySelectionVisibleChangeCommand;
            }
        }

        public ICommand SelectedDomainChangeCommand
        {
            get
            {
                if (this._selectedDomainChangeCommand == null)
                    this._selectedDomainChangeCommand = new RelayCommand(o =>
                    {
                        SelectedDomain = (DataTransferObjects.Domain)o;
                        DomainSelectionVisible = false;
                    });

                return _selectedDomainChangeCommand;
            }
        }

        public ICommand SelectedCompanyChangeCommand
        {
            get
            {
                if (this._selectedCompanyChangeCommand == null)
                    this._selectedCompanyChangeCommand = new RelayCommand(o =>
                    {
                        SelectedCompany = (Company)o;
                        CompanySelectionVisible = false;
                    });

                return _selectedCompanyChangeCommand;
            }
        }
        #endregion
        #endregion

        #region Visibles
        #region Property Fields
        private bool _loginVisible = true;

        private bool _mainVisible = false;

        private bool _packagingVisible = false;

        private bool _messageVisible = false;

        private bool _printBarcodesVisible = false;

        private bool _domainSelectionVisible = false;

        private bool _companySelectionVisible = false;

        private bool _marketplaceSelectionVisible = false;

        private bool _shipmentCompanySelectionVisible = false;

        private bool _orderSourceSelectionVisible = false;
        #endregion

        #region Properties
        public bool LoginVisible
        {
            get { return _loginVisible; }
            set { _loginVisible = value; OnPropertyChanged(); }
        }

        public bool MainVisible
        {
            get { return _mainVisible; }
            set { _mainVisible = value; OnPropertyChanged(); }
        }

        public bool PackagingVisible
        {
            get { return _packagingVisible; }
            set { _packagingVisible = value; OnPropertyChanged(); }
        }

        public bool MessageVisible
        {
            get { return _messageVisible; }
            set { _messageVisible = value; OnPropertyChanged(); }
        }

        public bool PrintBarcodesVisible
        {
            get { return _printBarcodesVisible; }
            set { _printBarcodesVisible = value; OnPropertyChanged(); }
        }

        public bool DomainSelectionVisible
        {
            get { return _domainSelectionVisible; }
            set { _domainSelectionVisible = value; OnPropertyChanged(); }
        }

        public bool CompanySelectionVisible
        {
            get { return _companySelectionVisible; }
            set { _companySelectionVisible = value; OnPropertyChanged(); }
        }

        public bool MarketplaceSelectionVisible
        {
            get { return _marketplaceSelectionVisible; }
            set { _marketplaceSelectionVisible = value; OnPropertyChanged(); }
        }

        public bool ShipmentCompanySelectionVisible
        {
            get { return _shipmentCompanySelectionVisible; }
            set { _shipmentCompanySelectionVisible = value; OnPropertyChanged(); }
        }

        public bool OrderSourceSelectionVisible
        {
            get { return _orderSourceSelectionVisible; }
            set { _orderSourceSelectionVisible = value; OnPropertyChanged(); }
        }
        #endregion
        #endregion

        #region Message
        #region Property Fields
        private string _message = string.Empty;
        #endregion

        #region Properties
        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged(); }
        }
        #endregion

        #region Command Fields
        private ICommand _closeMessageCommand;
        #endregion

        #region Commands
        public ICommand CloseMessageCommand
        {
            get
            {
                if (this._closeMessageCommand == null)
                    this._closeMessageCommand = new RelayCommand(o =>
                    {
                        this.MessageVisible = false;
                    });

                return this._closeMessageCommand;
            }
        }
        #endregion
        #endregion

        #region Login / Logout
        #region Property Fields

        private string _username = "Moonsea";

        private string _password = "Games*21";
        #endregion

        #region Properties
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(); }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged(); }
        }
        #endregion

        #region Command Fields
        private ICommand _loginCommand;

        private ICommand _logoutCommand;
        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                if (this._loginCommand == null)
                    this._loginCommand = new RelayCommand(async o =>
                    {
                        var loginResponse = await this._identityService.LoginAsync(this.Username, this.Password);
                        if (loginResponse.Success == false)
                        {
                            MessageVisible = true;
                            Message = loginResponse.Message;
                            return;
                        }

                        this.Domains = new();

                        var domainResponse = await this._domainService.GetAsync();
                        domainResponse.Data.ForEach(d => this.Domains.Add(d));

                        if (this.Domains.Any(d => d.Default))
                            this.SelectedDomain = this.Domains.FirstOrDefault(d => d.Default);
                        else
                            this.SelectedDomain = this.Domains.FirstOrDefault();

                        if (this.Login != null)
                            this.Login(true);

                        this.LoginVisible = false;
                        this.MainVisible = true;
                    });

                return _loginCommand;
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                if (this._logoutCommand == null)
                    this._logoutCommand = new RelayCommand(o =>
                    {
                        this.LoginVisible = true;
                        this.MainVisible = false;
                    });

                return _logoutCommand;
            }
        }
        #endregion
        #endregion

        #region Packaging
        #region Property Fields
        private bool _barcodeFocused = false;

        private string _barcode = string.Empty;

        private Order _order;

        private ObservableCollection<OrderDetailViewModel> _orderDetails = null;
        #endregion

        #region Properties
        public bool BarcodeFocused
        {
            get { return _barcodeFocused; }
            set { _barcodeFocused = value; OnPropertyChanged(); }
        }

        public string Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged(); }
        }

        public Order Order
        {
            get { return _order; }
            set { _order = value; OnPropertyChanged(); OnPropertyChanged("MarketplaceIcon"); OnPropertyChanged("ShipmentCompanyIcon"); }
        }

        public ObservableCollection<OrderDetailViewModel> OrderDetails
        {
            get { return _orderDetails; }
            set { _orderDetails = value; OnPropertyChanged(); }
        }

        public string MarketplaceIcon => this.Order != null
            ? $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/{this.Order.MarketPlaceId}.png"
            : $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/HB.png";

        public string ShipmentCompanyIcon => this.Order != null
            ? $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/{this.Order.OrderShipments.First().ShipmentCompanyId}.png"
            : $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/ARS.png";
        #endregion

        #region Command Fields
        protected ICommand _getOrderCommand;

        protected ICommand _cancelOrderCommand;

        private bool _busy = false;

        private bool _completeOrderCommandVisible = false;

        private ICommand _completeOrderCommand;

        private ICommand _notAvailableOrderCommand;

        private ICommand _checkBarcodeCommand;

        private ICommand _printBarcodesCommand;
        #endregion

        #region Commands
        public abstract ICommand GetOrderCommand { get; }

        public abstract ICommand CancelOrderCommand { get; }

        public bool Busy
        {
            get
            {
                return _busy;
            }
            set
            {
                _busy = value;
                OnPropertyChanged();
            }
        }

        public bool CompleteOrderCommandVisible
        {
            get
            {
                return _completeOrderCommandVisible;
            }
            set
            {
                _completeOrderCommandVisible = value;
                OnPropertyChanged();
            }
        }

        public ICommand CompleteOrderCommand
        {
            get
            {
                if (this._completeOrderCommand == null)
                    this._completeOrderCommand = new RelayCommand(async o =>
                    {
                        if (Busy)
                        {
                            return;
                        }

                        Busy = true;
                        CompleteOrderCommandVisible = false;

                        if (this.Order == null)
                        {
                            Busy = false;
                            CompleteOrderCommandVisible = true;

                            return;
                        }

                        if (this.OrderDetails.Any(od => !od.Completed))
                        {
                            this.MessageVisible = true;
                            this.Message = "Adetleri kontrol ediniz.";

                            Busy = false;
                            CompleteOrderCommandVisible = true;

                            return;
                        }

                        var completeResponse = await this._orderService.CompleteAsync(this.Order.Id);
                        if (completeResponse.Success)
                        {
                            PackagingVisible = !PackagingVisible;

                            this.Order = null;

                            if (this.Complate != null)
                                this.Complate();

                            Busy = false;
                            CompleteOrderCommandVisible = true;

                            return;
                        }
                        else
                        {
                            this.MessageVisible = true;
                            this.Message = completeResponse.Message;

                            if (this.Complate != null)
                                this.Complate();
                        }
                    });

                return _completeOrderCommand;
            }
        }

        public ICommand NotAvailableOrderCommand
        {
            get
            {
                if (this._notAvailableOrderCommand == null)
                    this._notAvailableOrderCommand = new RelayCommand(async o =>
                    {
                        var completeResponse = await this._orderService.NotAvailableAsync(this.Order.Id);
                        if (completeResponse.Success)
                        {
                            PackagingVisible = !PackagingVisible;

                            this.Order = null;
                        }
                        else
                        {
                            this.MessageVisible = true;
                            this.Message = completeResponse.Message;
                        }
                    });

                return _notAvailableOrderCommand;
            }
        }

        public ICommand CheckBarcodeCommand
        {
            get
            {
                if (this._checkBarcodeCommand == null)
                    this._checkBarcodeCommand = new RelayCommand(async o =>
                    {
                        if (!this.OrderDetails.Any(od => od.Barcode == this.Barcode))
                        {
                            this.Message = "Hatalı barkod okuttunuz.";
                            this.MessageVisible = true;
                            return;
                        }

                        var orderDetail = this.OrderDetails.First(od => od.Barcode == this.Barcode);
                        if (orderDetail.Completed)
                        {
                            this.Message = "Fazla barkod okuttunuz.";
                            this.MessageVisible = true;
                            return;
                        }
                        else
                            orderDetail.CompletedQuantity++;

                        if (!this.OrderDetails.Any(od => !od.Completed))
                        {
                            var packedResponse = await this._orderService.PackedAsync(this.Order.Id);
                            if (packedResponse.Success && packedResponse.Data)
                            {
                                if (this.Complate != null)
                                    this.Complate();

                                this.MessageVisible = true;
                                this.Message = "Bu sipariş daha önce paketlenmiştir.";
                                this.PackagingVisible = false;
                                this.Order = null;
                                this.OrderDetails = null;
                                return;
                            }

                            if (Busy == false)
                            {
                                Busy = true;
                                CompleteOrderCommandVisible = false;
                            }

                            this.PrintBarcodes();
                            this.PrintBarcodesVisible = true;
                        }

                        Barcode = string.Empty;
                    });

                return _checkBarcodeCommand;
            }
        }

        public ICommand PrintBarcodesCommand
        {
            get
            {
                if (this._printBarcodesCommand == null)
                    this._printBarcodesCommand = new RelayCommand(o =>
                    {
                        this.PrintBarcodes();
                    });

                return _printBarcodesCommand;
            }
        }
        #endregion
        #endregion

        #region Helper Methods
        public abstract void PrintBarcodes();

        public abstract void Initialize();
        #endregion
    }
}