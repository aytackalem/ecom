﻿using System;
using System.Windows.Input;

namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class DialogViewModel
    {
        public string Message { get; private set; }

        public ICommand Command { get; private set; }

        public DialogViewModel(string message, Action<bool> result)
        {
            this.Message = message;
            this.Command = new RelayCommand((o) =>
            {
                var value = Convert.ToBoolean(o);
                result(value);
            });
        }
    }
}
