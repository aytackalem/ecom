﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing;
using ECommerce.Packaging.WpfApp.Printing.Base;
using ECommerce.Packaging.WpfApp.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class TextileBulkInvoicingViewModel : NotifyPropertyChangedBase
    {
        #region Service Fields
        public delegate void LoginEventHandler(bool login);

        public event LoginEventHandler Login;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly IdentityService _identityService;

        private readonly OrderService _orderService;

        private readonly CompanyService _companyService;

        private readonly DomainService _domainService;

        private readonly AccountingService _accountingService;

        private readonly OrderInvoiceInformationService _orderInvoiceInformationService;
        #endregion

        #region Fields
        private string _username = "Lafaba";

        private string _password = "3297723Az*";

        private bool _loginVisible = true;

        private string _message = string.Empty;

        private bool _messageVisible = false;

        private bool _mainVisible = false;

        private ObservableCollection<DataTransferObjects.Domain> _domains = new();

        private DataTransferObjects.Domain _selectedDomain;

        private ObservableCollection<Company> _companies = new();

        private Company _selectedCompany;

        private bool _domainSelectionVisible = false;

        private bool _companySelectionVisible = false;

        private bool _orderIdFocused = false;

        private bool _invoicingVisible = false;

        private string _orderId = string.Empty;

        private ObservableCollection<Order> _orders = new();

        private bool _invoiceSaved = false;
        #endregion

        #region Properties
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(); }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged(); }
        }

        public bool LoginVisible
        {
            get { return _loginVisible; }
            set { _loginVisible = value; OnPropertyChanged(); }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged(); }
        }

        public bool MessageVisible
        {
            get { return _messageVisible; }
            set { _messageVisible = value; OnPropertyChanged(); }
        }

        public bool MainVisible
        {
            get { return _mainVisible; }
            set { _mainVisible = value; OnPropertyChanged(); }
        }

        public ObservableCollection<Order> Orders
        {
            get { return _orders; }
            set
            {
                _orders = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<DataTransferObjects.Domain> Domains
        {
            get { return _domains; }
            set { _domains = value; OnPropertyChanged(); }
        }

        public DataTransferObjects.Domain SelectedDomain
        {
            get { return _selectedDomain; }
            set
            {
                this._domainFinder.Set(value.Id);

                this.Companies = new ObservableCollection<Company>();

                Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                {
                    var task = await this._companyService.GetAsync();
                    task.Data.ForEach(c => this.Companies.Add(c));

                    this.SelectedCompany = this.Companies.FirstOrDefault();

                    _selectedDomain = value;
                    OnPropertyChanged();
                });

            }
        }

        public ObservableCollection<Company> Companies
        {
            get { return _companies; }
            set { _companies = value; OnPropertyChanged(); }
        }

        public Company SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                this._companyFinder.Set(value.Id);

                _selectedCompany = value;
                OnPropertyChanged();
            }
        }

        public bool InvoicingVisible
        {
            get { return _invoicingVisible; }
            set
            {
                _invoicingVisible = value;
                OnPropertyChanged();
            }
        }

        public bool DomainSelectionVisible
        {
            get { return _domainSelectionVisible; }
            set { _domainSelectionVisible = value; OnPropertyChanged(); }
        }

        public bool CompanySelectionVisible
        {
            get { return _companySelectionVisible; }
            set { _companySelectionVisible = value; OnPropertyChanged(); }
        }

        public bool OrderIdFocused
        {
            get { return _orderIdFocused; }
            set { _orderIdFocused = value; OnPropertyChanged(); }
        }

        public string OrderId
        {
            get { return _orderId; }
            set { _orderId = value; OnPropertyChanged(); }
        }
        #endregion

        #region Command Fields
        private ICommand _loginCommand;

        private ICommand _logoutCommand;

        private ICommand _closeMessageCommand;

        private ICommand _domainSelectionVisibleChangeCommand;

        private ICommand _companySelectionVisibleChangeCommand;

        private ICommand _selectedDomainChangeCommand;

        private ICommand _selectedCompanyChangeCommand;

        private ICommand _getOrderCommand;

        private ICommand _deleteOrderCommand;

        private ICommand _invoicingCommand;

        private ICommand _closeCommand;
        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                if (this._loginCommand == null)
                    this._loginCommand = new RelayCommand(async o =>
                    {


                        var loginResponse = await this._identityService.LoginAsync(this.Username, this.Password);
                        if (loginResponse.Success == false)
                        {
                            MessageVisible = true;
                            Message = loginResponse.Message;
                            return;
                        }

                        this.Domains = new();

                        #region Filters
                        var domains = await this._domainService.GetAsync();

                        domains.Data.ForEach(d => this.Domains.Add(d));

                        if (this.Domains.Any(d => d.Default))
                            this.SelectedDomain = this.Domains.FirstOrDefault(d => d.Default);
                        else
                            this.SelectedDomain = this.Domains.FirstOrDefault();

                        if (this.Login != null)
                            this.Login(true);

                        this.LoginVisible = false;
                        this.MainVisible = true;

                        #endregion
                    });

                return _loginCommand;
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                if (this._logoutCommand == null)
                    this._logoutCommand = new RelayCommand(o =>
                    {
                        this.LoginVisible = true;
                        this.MainVisible = false;
                    });

                return _logoutCommand;
            }
        }

        public ICommand DomainSelectionVisibleChangeCommand
        {
            get
            {
                if (this._domainSelectionVisibleChangeCommand == null)
                    this._domainSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        DomainSelectionVisible = !DomainSelectionVisible;
                    });

                return _domainSelectionVisibleChangeCommand;
            }
        }

        public ICommand CompanySelectionVisibleChangeCommand
        {
            get
            {
                if (this._companySelectionVisibleChangeCommand == null)
                    this._companySelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        CompanySelectionVisible = !CompanySelectionVisible;
                    });

                return _companySelectionVisibleChangeCommand;
            }
        }

        public ICommand CloseMessageCommand
        {
            get
            {
                if (this._closeMessageCommand == null)
                    this._closeMessageCommand = new RelayCommand(o =>
                    {
                        this.MessageVisible = false;
                    });

                return this._closeMessageCommand;
            }
        }

        public ICommand SelectedDomainChangeCommand
        {
            get
            {
                if (this._selectedDomainChangeCommand == null)
                    this._selectedDomainChangeCommand = new RelayCommand(o =>
                    {
                        SelectedDomain = (DataTransferObjects.Domain)o;
                        DomainSelectionVisible = false;
                    });

                return _selectedDomainChangeCommand;
            }
        }

        public ICommand SelectedCompanyChangeCommand
        {
            get
            {
                if (this._selectedCompanyChangeCommand == null)
                    this._selectedCompanyChangeCommand = new RelayCommand(o =>
                    {
                        SelectedCompany = (DataTransferObjects.Company)o;
                        CompanySelectionVisible = false;
                    });

                return _selectedCompanyChangeCommand;
            }
        }

        public ICommand GetOrderCommand
        {
            get
            {
                if (this._getOrderCommand == null)
                    this._getOrderCommand = new RelayCommand(async o =>
                    {
                        var orderResponse = await this._orderService.GetAsync(new()
                        {
                            OrderId = int.Parse(this.OrderId),
                            PackerBarcode = "0"
                        });

                        this.OrderId = string.Empty;

                        if (!orderResponse.Success)
                        {
                            MessageVisible = true;
                            Message = orderResponse.Message;
                            return;
                        }

                        if (orderResponse.Data.OrderTypeId == "IP")
                        {
                            MessageVisible = true;
                            Message = "Sipariş iptal edilmiştir.";
                            return;
                        }

                        this.Orders.Add(orderResponse.Data);

                        this.InvoicingVisible = true;
                    });

                return this._getOrderCommand;
            }
        }

        public ICommand DeleteOrderCommand
        {
            get
            {
                if (this._deleteOrderCommand == null)
                    this._deleteOrderCommand = new RelayCommand(o =>
                    {
                        this.Orders.Remove((Order)o);

                        if (this.Orders.Count == 0)
                            this.InvoicingVisible = false;
                    });

                return this._deleteOrderCommand;
            }
        }

        public ICommand InvoicingCommand
        {
            get
            {
                if (this._invoicingCommand == null)
                    this._invoicingCommand = new RelayCommand(async o =>
                    {
                        if (this.Orders.Count == 0)
                            return;

                        if (this._invoiceSaved == false)
                        {

                            var orderIds = this.Orders.Select(theOrder => theOrder.Id).ToList();

                            var accountingResponse = await this._accountingService.CreateInvoiceAsync(orderIds);
                            if (accountingResponse.Success)
                            {
                                this._invoiceSaved = true;

                                foreach (var theOrder in this.Orders)
                                {
                                    theOrder.OrderInvoiceInformation.Number = accountingResponse.Data.Guid;
                                    theOrder.OrderInvoiceInformation.AccountingCompanyId = accountingResponse.Data.AccountingCompanyId;
                                    theOrder.OrderInvoiceInformation.Url = accountingResponse.Data.Url;


                                    var _ = await this._orderInvoiceInformationService.UpdateAsync(theOrder.OrderInvoiceInformation);
                                }

                                this.Print();
                            }
                            else
                            {
                                this.Message = string.IsNullOrEmpty(accountingResponse.Message) ? "Fatura oluşturulamadı." : accountingResponse.Message;
                                this.MessageVisible = true;

                                return;
                            }
                        }
                        else
                            this.Print();
                    });

                return this._invoicingCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this._closeCommand == null)
                    this._closeCommand = new RelayCommand(o =>
                    {
                        this.InvoicingVisible = false;
                        this.Orders = new();
                        this._invoiceSaved = false;
                    });

                return this._closeCommand;
            }
        }
        #endregion

        #region Constructors
        public TextileBulkInvoicingViewModel(IDomainFinder domainFinder, ICompanyFinder companyFinder, OrderService orderService, DomainService domainService, CompanyService companyService, AccountingService accountingService, OrderInvoiceInformationService orderInvoiceInformationService, IdentityService identityService)
        {
            #region Service Fields
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._orderService = orderService;
            this._domainService = domainService;
            this._companyService = companyService;
            this._orderInvoiceInformationService = orderInvoiceInformationService;
            this._accountingService = accountingService;
            this._identityService = identityService;
            #endregion
        }
        #endregion

        #region Helper Methods
        private void Print()
        {
            if (this._invoiceSaved == false)
            {
                this.Message = "Faturalandırma işlemi başarılı değil.";
                this.MessageVisible = true;

                return;
            }

            PrintBase<List<Order>> invoicePrint = new BulkInvoicePrint("Invoice");
            invoicePrint.Print(this.Orders.ToList());
        }
        #endregion
    }
}
