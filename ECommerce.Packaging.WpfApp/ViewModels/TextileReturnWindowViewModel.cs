﻿using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing;
using ECommerce.Packaging.WpfApp.Services;
using Helpy.Shared.Response;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Threading;

namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class TextileReturnWindowViewModel : NotifyPropertyChangedBase
    {
        #region Service Fields
        public delegate void LoginEventHandler(bool login);

        public event LoginEventHandler Login;

        private readonly OrderService _orderService;

        private readonly DomainService _domainService;

        private readonly CompanyService _companyService;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        private readonly AccountingService _accountingService;

        private readonly OrderReturnService _orderReturnService;

        private readonly IdentityService _identityService;
        #endregion

        #region Fields
        private DataResponse<Application.Common.DataTransferObjects.AccountingCancel> _returnResponse = null;
        #endregion

        #region Property Fields
        private bool _orderIdFocused = false;

        private int? _orderId;

        private string _message = string.Empty;

        private bool _messageVisible = false;

        private Order _order;

        private ObservableCollection<TextileReturnOrderDetailViewModel> _orderDetails;

        //private string _username = "Lafaba";

        //private string _password = "3297723Az*";

        private string _username = "Lafaba";

        private string _password = "3297723Az*";


        private bool _printVisible = false;

        private bool _completeVisible = false;

        private bool _loginVisible = true;

        private bool _mainVisible = false;

        private bool _packagingVisible = false;

        private ObservableCollection<DataTransferObjects.Domain> _domains = new();

        private DataTransferObjects.Domain _selectedDomain;

        private ObservableCollection<Company> _companies = new();

        private Company _selectedCompany;

        private bool _reasonSelectionVisible = false;

        private bool _domainSelectionVisible = false;

        private bool _companySelectionVisible = false;

        public string _selectedReason = "";

        public string _paymentSource = "";

        public ObservableCollection<string> _reasons = new ObservableCollection<string>
        {
            "BEDENİ BÜYÜK GELDİ",
            "BEDENİ KÜÇÜK GELDİ",
            "MODELİNİ BEĞENMEDİM",
            "KALİTESİNİ BEĞENMEDİM",
            "TESLİMAT TARİHİ GECİKTİ",
            "KUSURLU ÜRÜN GÖNDERİLDİ",
            "YANLIŞ ÜRÜN GÖNDERİLDİ",
            "ÜRÜN BELİRTİLEN ÖZELLİKLERE SAHİP DEĞİL",
            "FAZLA/YANLIŞ SİPARİŞ VERDİM",
            "DİĞER"
        };

        private bool _barcodeFocused = false;

        private string _barcode = string.Empty;
        #endregion

        #region Properties
        public bool OrderIdFocused
        {
            get { return _orderIdFocused; }
            set { _orderIdFocused = value; OnPropertyChanged(); }
        }

        public int? OrderId
        {
            get { return _orderId; }
            set { _orderId = value; OnPropertyChanged(); }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged(); }
        }

        public bool MessageVisible
        {
            get { return _messageVisible; }
            set { _messageVisible = value; OnPropertyChanged(); }
        }

        public Order Order
        {
            get { return _order; }
            set { _order = value; OnPropertyChanged(); OnPropertyChanged("MarketplaceIcon"); OnPropertyChanged("ShipmentCompanyIcon"); }
        }

        public ObservableCollection<TextileReturnOrderDetailViewModel> OrderDetails
        {
            get { return _orderDetails; }
            set { _orderDetails = value; OnPropertyChanged(); }
        }

        public string MarketplaceIcon => this.Order != null
            ? $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/{this.Order.MarketPlaceId}.png"
            : $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/HB.png";

        public string ShipmentCompanyIcon => this.Order != null
            ? $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/{this.Order.OrderShipments.First().ShipmentCompanyId}.png"
            : $"pack://application:,,,/ECommerce.Packaging.WpfApp;component/Images/ARS.png";

        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(); }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged(); }
        }

        public bool LoginVisible
        {
            get { return _loginVisible; }
            set { _loginVisible = value; OnPropertyChanged(); }
        }

        public bool PrintVisible
        {
            get { return _printVisible; }
            set { _printVisible = value; OnPropertyChanged(); }
        }

        public bool CompleteVisible
        {
            get { return _completeVisible; }
            set { _completeVisible = value; OnPropertyChanged(); }
        }

        public bool MainVisible
        {
            get { return _mainVisible; }
            set { _mainVisible = value; OnPropertyChanged(); }
        }

        public bool PackagingVisible
        {
            get { return _packagingVisible; }
            set { _packagingVisible = value; OnPropertyChanged(); }
        }

        public ObservableCollection<DataTransferObjects.Domain> Domains
        {
            get { return _domains; }
            set { _domains = value; OnPropertyChanged(); }
        }

        public DataTransferObjects.Domain SelectedDomain
        {
            get { return _selectedDomain; }
            set
            {
                this._domainFinder.Set(value.Id);

                this.Companies = new ObservableCollection<Company>();

                Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                {
                    var task = await this._companyService.GetAsync();
                    task.Data.ForEach(c => this.Companies.Add(c));

                    this.SelectedCompany = this.Companies.FirstOrDefault();

                    _selectedDomain = value;
                    OnPropertyChanged();
                });
            }
        }

        public ObservableCollection<Company> Companies
        {
            get { return _companies; }
            set { _companies = value; OnPropertyChanged(); }
        }

        public Company SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                this._companyFinder.Set(value.Id);

                _selectedCompany = value;
                OnPropertyChanged();
            }
        }

        public bool ReasonSelectionVisible
        {
            get { return _reasonSelectionVisible; }
            set { _reasonSelectionVisible = value; OnPropertyChanged(); }
        }

        public bool DomainSelectionVisible
        {
            get { return _domainSelectionVisible; }
            set { _domainSelectionVisible = value; OnPropertyChanged(); }
        }

        public bool CompanySelectionVisible
        {
            get { return _companySelectionVisible; }
            set { _companySelectionVisible = value; OnPropertyChanged(); }
        }

        public bool Return
        {
            get { return this.SelectedReason != "" ? true : false; }
        }

        public string PaymentSource
        {
            get { return _selectedReason; }
            set
            {
                _selectedReason = value;
                OnPropertyChanged();
                OnPropertyChanged("ClearFilterReason");
            }
        }

        public string SelectedReason
        {
            get { return _paymentSource; }
            set
            {
                _paymentSource = value;
                OnPropertyChanged();
                OnPropertyChanged("ClearFilterPaymentSource");
            }
        }


        public ObservableCollection<string> Reasons
        {
            get { return _reasons; }
        }

        public bool BarcodeFocused
        {
            get { return _barcodeFocused; }
            set { _barcodeFocused = value; OnPropertyChanged(); }
        }

        public string Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged(); }
        }

        public bool ClearFilterReason { get { return !string.IsNullOrEmpty(this.SelectedReason); } }
        #endregion

        #region Command Fields
        protected ICommand _getOrderCommand;

        private ICommand _loginCommand;

        private ICommand _logoutCommand;

        private ICommand _reasonSelectionVisibleChangeCommand;

        private ICommand _domainSelectionVisibleChangeCommand;

        private ICommand _companySelectionVisibleChangeCommand;

        private ICommand _selectedDomainChangeCommand;

        private ICommand _selectedCompanyChangeCommand;

        private ICommand _selectedReasonChangeCommand;

        private ICommand _closeMessageCommand;

        private ICommand _cancelOrderCommand;

        private bool _busy = false;

        private ICommand _completeOrderCommand;

        private ICommand _printCommand;

        private ICommand _checkBarcodeCommand;
        #endregion

        #region Commands
        public ICommand GetOrderCommand
        {
            get
            {
                if (this._getOrderCommand == null)
                    this._getOrderCommand = new RelayCommand(async o =>
                    {
                        if (this.Order != null)
                            return;

                        if (!this.OrderId.HasValue)
                        {
                            return;
                        }

                        this._returnResponse = null;

                        OrderDetails = new ObservableCollection<TextileReturnOrderDetailViewModel>();

                        var orderResponse = await this._orderService.GetAsync(new()
                        {
                            OrderId = this.OrderId.Value,
                            PackerBarcode = "0"
                        });

                        if (!orderResponse.Success)
                        {
                            MessageVisible = true;
                            Message = orderResponse.Message;
                            return;
                        }

                        this.Order = orderResponse.Data;
                        this.Order.OrderDetails.ForEach(od =>
                        {
                            var orderReturnDetail = this.Order.OrderReturnDetails.FirstOrDefault(x => x.ProductInformationId == od.ProductInformationId);



                            OrderDetails.Add(new TextileReturnOrderDetailViewModel
                            {
                                ProductInformationId = od.ProductInformationId,
                                Barcode = od.Product.ProductInformation.Barcode,
                                CompletedQuantity = orderReturnDetail != null ? orderReturnDetail.Quantity : 0,
                                Quantity = od.Quantity,
                                Name = od.Product.ProductInformation.Name,
                                PhotoUrl = od.Product.ProductInformation.ProductInformationPhoto.FileName
                            });
                        });

                        PackagingVisible = !PackagingVisible;
                        BarcodeFocused = true;

                        /* Eğer iade faturası daha önceden kesilmiş ise "Tamamla" butonu gizlenir yazdır butonu açılır ve daha önce kesilmiş olan iade faturası çekilir. */
                        //if (!string.IsNullOrEmpty(this.Order.OrderBilling.ReturnNumber))
                        //{
                        //    var _orderResponse = await this._accountingService.CancelInvoiceAsync(this.Order.Id);
                        //    if(_orderResponse.Success)
                        //    {
                        //        this._returnResponse = _orderResponse;
                        //    }
                        //    this.SelectedReason = this.Order.OrderBilling.ReturnDescription;
                        //}

                        this.CompleteVisible = string.IsNullOrEmpty(this.Order.OrderBilling.ReturnNumber);
                        PrintVisible = !string.IsNullOrEmpty(this.Order.OrderBilling.ReturnNumber);
                    });

                return _getOrderCommand;
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                if (this._loginCommand == null)
                    this._loginCommand = new RelayCommand(async o =>
                    {
                        var loginResponse = await this._identityService.LoginAsync(this.Username, this.Password);
                        if (loginResponse.Success == false)
                        {
                            MessageVisible = true;
                            Message = loginResponse.Message;
                            return;
                        }

                        this.Domains = new();

                        #region Filters
                        var domainResponse = await this._domainService.GetAsync();

                        this.Domains = new ObservableCollection<DataTransferObjects.Domain>();
                        domainResponse.Data.ForEach(d => this.Domains.Add(d));

                        if (this.Domains.Any(d => d.Default))
                            this.SelectedDomain = this.Domains.FirstOrDefault(d => d.Default);
                        else
                            this.SelectedDomain = this.Domains.FirstOrDefault();
                        #endregion



                        if (this.Login != null)
                            this.Login(true);

                        this.LoginVisible = false;
                        this.MainVisible = true;
                    });

                return _loginCommand;
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                if (this._logoutCommand == null)
                    this._logoutCommand = new RelayCommand(o =>
                    {
                        this.LoginVisible = true;
                        this.MainVisible = false;
                    });

                return _logoutCommand;
            }
        }

        public ICommand DomainSelectionVisibleChangeCommand
        {
            get
            {
                if (this._domainSelectionVisibleChangeCommand == null)
                    this._domainSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        DomainSelectionVisible = !DomainSelectionVisible;
                    });

                return _domainSelectionVisibleChangeCommand;
            }
        }

        public ICommand CompanySelectionVisibleChangeCommand
        {
            get
            {
                if (this._companySelectionVisibleChangeCommand == null)
                    this._companySelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        CompanySelectionVisible = !CompanySelectionVisible;
                    });

                return _companySelectionVisibleChangeCommand;
            }
        }

        public ICommand ReasonSelectionVisibleChangeCommand
        {
            get
            {
                if (this._reasonSelectionVisibleChangeCommand == null)
                    this._reasonSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        ReasonSelectionVisible = !ReasonSelectionVisible;
                    });

                return _reasonSelectionVisibleChangeCommand;
            }
        }

        public ICommand SelectedDomainChangeCommand
        {
            get
            {
                if (this._selectedDomainChangeCommand == null)
                    this._selectedDomainChangeCommand = new RelayCommand(o =>
                    {
                        SelectedDomain = (DataTransferObjects.Domain)o;
                        DomainSelectionVisible = false;
                    });

                return _selectedDomainChangeCommand;
            }
        }

        public ICommand SelectedReasonChangeCommand
        {
            get
            {
                if (this._selectedReasonChangeCommand == null)
                    this._selectedReasonChangeCommand = new RelayCommand(o =>
                    {
                        SelectedReason = (string)o;
                        ReasonSelectionVisible = false;
                    });

                return _selectedReasonChangeCommand;
            }
        }

        public ICommand SelectedCompanyChangeCommand
        {
            get
            {
                if (this._selectedCompanyChangeCommand == null)
                    this._selectedCompanyChangeCommand = new RelayCommand(o =>
                    {
                        SelectedCompany = (DataTransferObjects.Company)o;
                        CompanySelectionVisible = false;
                    });

                return _selectedCompanyChangeCommand;
            }
        }

        public ICommand CloseMessageCommand
        {
            get
            {
                if (this._closeMessageCommand == null)
                    this._closeMessageCommand = new RelayCommand(o =>
                    {
                        this.MessageVisible = false;
                    });

                return this._closeMessageCommand;
            }
        }

        public ICommand CancelOrderCommand
        {
            get
            {
                if (this._cancelOrderCommand == null)
                    this._cancelOrderCommand = new RelayCommand(o =>
                    {
                        if (this.Order == null)
                            return;

                        PackagingVisible = !PackagingVisible;

                        this._returnResponse = null;
                        this.SelectedReason = null;
                        this.OrderId = null;
                        this.Order = null;
                    });

                return _cancelOrderCommand;
            }
        }

        public bool Busy
        {
            get
            {
                return _busy;
            }
            set
            {
                _busy = value;
                OnPropertyChanged();
            }
        }

        public ICommand CompleteOrderCommand
        {
            get
            {
                if (this._completeOrderCommand == null)
                    this._completeOrderCommand = new RelayCommand(async o =>
                    {
                        if (Busy)
                        {
                            return;
                        }

                        Busy = true;
                        CompleteVisible = false;

                        if (string.IsNullOrEmpty(this.SelectedReason))
                        {
                            this.Message = "Lütfen iade nedeni Giriniz.";
                            this.MessageVisible = true;

                            Busy = false;
                            CompleteVisible = true;

                            return;
                        }

                        if (string.IsNullOrEmpty(this.Order.OrderBilling.ReturnNumber))
                        {
                            var orderReturn = new OrderReturn
                            {
                                Id = this.Order.Id,
                                ReturnDescription = this.SelectedReason,
                                ReturnNumber = this.Order.OrderBilling.ReturnNumber,
                                OrderReturnDetails = OrderDetails
                            .Where(x => x.CompletedQuantity > 0)
                              .Select(od => new OrderReturnDetail
                              {
                                  IsReturn = true,
                                  Quantity = od.CompletedQuantity,
                                  ProductInformationId = od.ProductInformationId,
                                  IsLoss = false
                              })
                              .ToList()
                            };

                            var updateResponse = await this._orderReturnService.UpdateAsync(orderReturn);
                            if (updateResponse.Success == false)
                            {
                                this.Message = "Gider Pusulası oluşturulamadı.";
                                this.MessageVisible = true;

                                Busy = false;
                                CompleteVisible = true;

                                return;
                            }


                            this._returnResponse = await this._accountingService.CancelInvoiceAsync(this.Order.Id);
                            if (this._returnResponse.Success == false)
                            {
                                this.Message = "Gider Pusulası oluşturulamadı.";
                                this.MessageVisible = true;

                                Busy = false;
                                CompleteVisible = true;

                                return;
                            }

                            this.Order.OrderBilling.ReturnDescription = this.SelectedReason;
                            this.Order.OrderBilling.ReturnNumber = this._returnResponse.Data.DocumentNo;

                            orderReturn.ReturnNumber = this._returnResponse.Data.DocumentNo;

                            await this._orderReturnService.UpdateAsync(orderReturn);

                            _order.OrderReturnDetails = OrderDetails
                                .Where(x => x.CompletedQuantity > 0)
                                .Select(rd => new OrderReturnDetail
                                {
                                    ProductInformationId = rd.ProductInformationId,
                                    Quantity = rd.Quantity,
                                    IsLoss = false,
                                    IsReturn = true
                                })
                                .ToList();
                            PackagingVisible = false;
                        }
                        else
                        {
                            this.Message = "Lütfen Helpy Tarafından iade faturasını siliniz.";
                            this.MessageVisible = true;

                            Busy = false;
                            CompleteVisible = true;

                            return;
                        }

                        PrintBarcodes();

                        Busy = false;
                        CompleteVisible = true;
                    });

                return _completeOrderCommand;
            }
        }

        public ICommand PrintCommand
        {
            get
            {
                if (this._printCommand == null)
                    this._printCommand = new RelayCommand(o =>
                    {
                        if (Busy == true)
                        {
                            return;
                        }

                        Busy = true;
                        CompleteVisible = false;

                        PrintBarcodes();

                        Busy = false;
                        CompleteVisible = true;
                    });

                return _printCommand;
            }
        }

        public ICommand CheckBarcodeCommand
        {
            get
            {
                if (this._checkBarcodeCommand == null)
                    this._checkBarcodeCommand = new RelayCommand(o =>
                    {
                        if (!this.OrderDetails.Any(od => od.Barcode == this.Barcode))
                        {
                            this.Message = "Hatalı barkod okuttunuz.";
                            this.MessageVisible = true;
                            return;
                        }

                        var orderDetail = this.OrderDetails.First(od => od.Barcode == this.Barcode);
                        if (orderDetail.Completed)
                        {
                            this.Message = "Fazla barkod okuttunuz.";
                            this.MessageVisible = true;
                            return;
                        }
                        else
                            orderDetail.CompletedQuantity++;

                        this.Barcode = string.Empty;
                    });

                return _checkBarcodeCommand;
            }
        }
        #endregion

        #region Constructors
        public TextileReturnWindowViewModel(IDomainFinder domainFinder, ICompanyFinder companyFinder, OrderService orderService, CompanyService companyService, DomainService domainService, AccountingService accountingService, IdentityService identityService, OrderReturnService orderReturnService)
        {
            #region Service Fields
            this._orderService = orderService;
            this._domainFinder = domainFinder;
            this._companyFinder = companyFinder;
            this._companyService = companyService;
            this._domainService = domainService;
            this._accountingService = accountingService;
            this._identityService = identityService;
            this._orderReturnService = orderReturnService;
            #endregion
        }
        #endregion

        #region Helper Methods
        public void PrintBarcodes()
        {
            if (!string.IsNullOrEmpty(this.Order.OrderBilling.ReturnNumber))
            {
                ReturnPrint returnPrint = new(this.Return, this.SelectedReason, _paymentSource, "Invoice");
                returnPrint.Print(this.Order);

                this.PrintVisible = true;

                if (!PackagingVisible)
                {

                    this._returnResponse = null;
                    this.SelectedReason = null;
                    this.OrderId = null;
                    this.Order = null;

                }
            }
        }
        #endregion
    }
}
