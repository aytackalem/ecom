﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing;
using ECommerce.Packaging.WpfApp.Printing.Base;
using ECommerce.Packaging.WpfApp.Services;
using ECommerce.Packaging.WpfApp.ViewModels.Base;
using Helpy.Shared.Crypto;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Input;

namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class TextileWindowViewModel : ViewModelBase
    {
        #region Service Fields
        private readonly IConfiguration _configuration;

        private readonly OrderService _orderService;

        private readonly AccountingService _accountingService;

        private readonly OrderInvoiceInformationService _orderInvoiceInformationService;

        private readonly OrderShipmentService _orderShipmentService;

        private readonly MarketplaceService _marketplaceService;
        #endregion

        #region Constructors
        public TextileWindowViewModel(IdentityService identityService, IDomainFinder domainFinder, ICompanyFinder companyFinder, OrderService orderService, OrderInvoiceInformationService orderInvoiceInformationService, MarketplaceService marketplaceService, OrderShipmentService orderShipmentService, AccountingService accountingService, DomainService domainService, CompanyService companyService, IConfiguration configuration) : base(identityService, domainFinder, companyFinder, orderService, domainService, companyService)
        {
            #region Service Fields
            this._orderService = orderService;
            this._accountingService = accountingService;
            this._orderInvoiceInformationService = orderInvoiceInformationService;
            this._marketplaceService = marketplaceService;
            this._orderShipmentService = orderShipmentService;
            this._configuration = configuration;
            #endregion

            this.Login += (login) =>
            {
                if (login)
                {
                    var orderFilter = this._configuration.GetValue<string>("OrderFilter");
                    if (string.IsNullOrEmpty(orderFilter) || orderFilter == "PackerBarcode")
                    {
                        this.PackerBarcodeFocused = true;

                        return;
                    }

                    this.ProductBarcodeFocused = true;
                }
            };

            this.Complate += () =>
            {
                this.ProductBarcode = null;
                this.PackerBarcode = null;
                this.Order = null;
                this._invoiceCreated = false;
                this._invoiceSaved = false;

                var orderFilter = this._configuration.GetValue<string>("OrderFilter");
                if (!string.IsNullOrEmpty(orderFilter) || orderFilter == "PackerBarcode")
                {
                    this.PackerBarcodeFocused = false;
                    this.PackerBarcodeFocused = true;
                    return;
                }

                this.ProductBarcodeFocused = false;
                this.ProductBarcodeFocused = true;
            };
        }
        #endregion

        #region Packaging
        #region Fields
        private bool _invoiceCreated = false;

        private bool _invoiceSaved = false;
        #endregion

        #region Property Fields
        private bool _packerBarcodeFocused = false;

        private string _packerBarcode = string.Empty;

        private bool _productBarcodeFocused = false;

        private string _productBarcode = string.Empty;

        private string _selectedOrderFilterType = "Tümü";

        private bool _orderFilterTypeSelectionVisible = false;

        private List<string> _orderFilterTypes = new() { "Tümü", "Tekli", "Çoklu" };

        private ICommand _selectedOrderFilterTypeChangeCommand;

        private ICommand _orderFilterTypeVisibleChangeCommand;
        #endregion

        #region Properties
        public bool PackerBarcodeFocused
        {
            get { return _packerBarcodeFocused; }
            set
            {
                _packerBarcodeFocused = value;
                OnPropertyChanged();
            }
        }

        public string PackerBarcode
        {
            get { return _packerBarcode; }
            set
            {
                _packerBarcode = value;
                OnPropertyChanged();
            }
        }

        public bool ProductBarcodeFocused
        {
            get { return _productBarcodeFocused; }
            set
            {
                _productBarcodeFocused = value;
                OnPropertyChanged();
            }
        }

        public string ProductBarcode
        {
            get { return _productBarcode; }
            set
            {
                _productBarcode = value;
                OnPropertyChanged();
            }
        }

        public string SelectedOrderFilterType
        {
            get { return _selectedOrderFilterType; }
            set
            {
                _selectedOrderFilterType = value;
                OnPropertyChanged();
            }
        }

        public bool OrderFilterTypeSelectionVisible
        {
            get { return _orderFilterTypeSelectionVisible; }
            set
            {
                _orderFilterTypeSelectionVisible = value;
                OnPropertyChanged();
            }
        }

        public List<string> OrderFilterTypes
        {
            get
            {
                return _orderFilterTypes;
            }
            set
            {
                _orderFilterTypes = value;
                OnPropertyChanged();
            }
        }

        public ICommand OrderFilterTypeVisibleChangeCommand
        {
            get
            {
                if (this._orderFilterTypeVisibleChangeCommand == null)
                    this._orderFilterTypeVisibleChangeCommand = new RelayCommand(o =>
                    {
                        OrderFilterTypeSelectionVisible = !OrderFilterTypeSelectionVisible;
                    });

                return _orderFilterTypeVisibleChangeCommand;
            }
        }

        public ICommand SelectedOrderFilterTypeChangeCommand
        {
            get
            {
                if (this._selectedOrderFilterTypeChangeCommand == null)
                    this._selectedOrderFilterTypeChangeCommand = new RelayCommand(o =>
                    {
                        SelectedOrderFilterType = (string)o;
                        OrderFilterTypeSelectionVisible = false;
                    });

                return _selectedOrderFilterTypeChangeCommand;
            }
        }

        public bool IsPackerBarcode => this._configuration.GetValue<string>("OrderFilter") == "PackerBarcode";

        public bool IsProductBarcode => this._configuration.GetValue<string>("OrderFilter") == "ProductBarcode";
        #endregion

        #region Commands
        public override ICommand GetOrderCommand
        {
            get
            {
                if (this._getOrderCommand == null)
                    this._getOrderCommand = new RelayCommand(async o =>
                    {
                        if (this.Order != null)
                            return;

                        this.PrintBarcodesVisible = false;

                        OrderDetails = new();

                        var orderResponse = await this._orderService.GetAsync(new Parameters.Order.GetParameter
                        {
                            OrderId = 0,
                            PackerBarcode = this.PackerBarcode,
                            ProductBarcode = this.ProductBarcode,
                            OrderFilterType = this.SelectedOrderFilterType
                        });

                        if (!orderResponse.Success || orderResponse.Data == null)
                        {
                            MessageVisible = true;
                            Message = orderResponse.Message;
                            return;
                        }

                        this.Order = orderResponse.Data;
                        this.Order.OrderDetails.ForEach(od =>
                        {
                            OrderDetails.Add(new OrderDetailViewModel
                            {
                                Barcode = od.Product.ProductInformation.Barcode,
                                CompletedQuantity = 0,
                                Quantity = od.Quantity,
                                Name = $"{od.Product.ProductInformation.FullName} {od.Product.SellerCode}",
                                PhotoUrl = od.Product.ProductInformation.ProductInformationPhoto.FileName
                            });
                        });

                        PackagingVisible = !PackagingVisible;
                        BarcodeFocused = false;
                        BarcodeFocused = true;

                        if (IsProductBarcode)
                        {
                            this.Barcode = this.ProductBarcode;
                            CheckBarcodeCommand.Execute(null);
                        }
                    });

                return _getOrderCommand;
            }
        }

        public override ICommand CancelOrderCommand
        {
            get
            {
                if (this._cancelOrderCommand == null)
                    this._cancelOrderCommand = new RelayCommand(o =>
                    {
                        if (this.Order == null)
                            return;

                        PackagingVisible = !PackagingVisible;

                        this.PackerBarcode = null;
                        this.ProductBarcode = null;
                        this.Order = null;
                        this._invoiceCreated = false;
                        this._invoiceSaved = false;
                    });

                return _cancelOrderCommand;
            }
        }
        #endregion
        #endregion

        #region Helper Methods
        public override async void PrintBarcodes()
        {
            this.Order.Company = this.SelectedCompany;

            if (this.Order.BulkInvoicing)
            {
                if (this.Order.MarketPlaceId == Marketplaces.TrendyolEurope)
                {
                    PrintBase<Order> trendyolEuropePrint = new TrendyolEuropePrint("LineBarcode");
                    trendyolEuropePrint.Print(this.Order);

                    return;
                }

                PrintBase<Order> orderNumberPrint = new OrderNumberPrint("Barcode");
                orderNumberPrint.Print(this.Order);
            }
            else
            {
                if (string.IsNullOrEmpty(Order.OrderInvoiceInformation.Number) && this._invoiceCreated == false)
                {
                    var _accountingResponse = await this._accountingService.CreateInvoiceAsync(this.Order.Id);

                    if (_accountingResponse.Success == false)
                    {
                        this.Message = _accountingResponse.Message;
                        this.MessageVisible = true;

                        Busy = false;
                        CompleteOrderCommandVisible = true;

                        return;
                    }

                    this._invoiceCreated = true;

                    Order.OrderInvoiceInformation.Number = _accountingResponse.Data.Guid;
                    Order.OrderInvoiceInformation.AccountingCompanyId = _accountingResponse.Data.AccountingCompanyId;
                    Order.OrderInvoiceInformation.Url = !string.IsNullOrEmpty(_accountingResponse.Data.Url) ? _accountingResponse.Data.Url : "";
                    Order.OrderInvoiceInformation.Guid = "";
                    Order.OrderInvoiceInformation.TaxNumber = !string.IsNullOrEmpty(Order.OrderInvoiceInformation.TaxNumber) ? Order.OrderInvoiceInformation.TaxNumber : "";
                    Order.OrderInvoiceInformation.TaxOffice = !string.IsNullOrEmpty(Order.OrderInvoiceInformation.TaxOffice) ? Order.OrderInvoiceInformation.TaxOffice : "";

                }

                if (this._invoiceSaved == false && this._invoiceCreated)
                {
                    var invoiceCreateResponse = await _orderInvoiceInformationService.UpdateAsync(Order.OrderInvoiceInformation);
                    if (invoiceCreateResponse.Success == false)
                    {
                        this.Message = "Fatura kaydedilemedi.";
                        this.MessageVisible = true;

                        Busy = false;
                        CompleteOrderCommandVisible = true;

                        return;
                    }

                    this._invoiceSaved = true;
                }

                Order.EncryptId = CryptoHelper.Encrypt(Order.Id.ToString());


                var trackingBase64 = this.Order.OrderShipments.FirstOrDefault()?.TrackingBase64;

                if (!string.IsNullOrEmpty(trackingBase64))
                {
                    ShipmentPrint sshipmentPrint = new("Barcode");
                    sshipmentPrint.Print(trackingBase64);
                }
                else
                {
                    PrintBase<Order> marketplacePrint = new MarketplacePrint("Barcode", _configuration.GetValue<int>("MarketplacePrintHeight"));
                    marketplacePrint.Print(this.Order);
                }

                if (base.SelectedCompany.Id == 1 && base.SelectedCompany.Name == "Lafaba")//SADECE LAFABADA FATURA FISI CIKACAK
                {
                    PrintBase<Order> invoicePrint = new Invoice80Print("Invoice");
                    invoicePrint.Print(this.Order);
                }

                

                //PrintBase<Order> invoicePrint = new InvoicePrint();
                //invoicePrint.Print(this.Order);
            }

            Busy = false;
            CompleteOrderCommandVisible = true;
        }

        public override void Initialize()
        {
        }
        #endregion
    }
}