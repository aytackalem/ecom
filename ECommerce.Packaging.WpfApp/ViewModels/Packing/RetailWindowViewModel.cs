﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing;
using ECommerce.Packaging.WpfApp.Services;
using ECommerce.Packaging.WpfApp.ViewModels.Base;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;

namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class RetailWindowViewModel : ViewModelBase
    {
        #region Timers
        System.Timers.Timer _timer;
        #endregion

        #region Service Fields
        private readonly OrderService _orderService;

        private readonly OrderSourceService _orderSourceService;

        private readonly ShipmentCompanyCompanyService _shipmentCompanyService;

        private readonly MarketplaceService _marketplaceService;
        #endregion

        #region Property Fields
        private int _todayOrdersCount = 0;

        private int _packedOrdersCount = 0;

        private int _unpackedOrdersCount = 0;
        #endregion

        #region Properties
        public int TodayOrdersCount
        {
            get { return _todayOrdersCount; }
            set { _todayOrdersCount = value; OnPropertyChanged(); }
        }

        public int PackedOrdersCount
        {
            get { return _packedOrdersCount; }
            set { _packedOrdersCount = value; OnPropertyChanged(); }
        }

        public int UnpackedOrdersCount
        {
            get { return _unpackedOrdersCount; }
            set { _unpackedOrdersCount = value; OnPropertyChanged(); }
        }
        #endregion

        #region Constructors
        public RetailWindowViewModel(IDomainFinder filterDomainFinder, ICompanyFinder filterCompanyFinder, IdentityService identityService, OrderService orderService, MarketplaceService marketPlaceService, OrderSourceService orderSourceService, ShipmentCompanyCompanyService shipmentCompanyService, DomainService domainService, CompanyService companyService) : base(identityService, filterDomainFinder, filterCompanyFinder, orderService, domainService, companyService)
        {


            #region Service Fields
            this._orderService = orderService;
            this._orderSourceService = orderSourceService;
            this._shipmentCompanyService = shipmentCompanyService;
            this._marketplaceService = marketPlaceService;
            #endregion

            this.Login += (login) =>
            {
                if (login)
                    this.Initialize();
            };
        }
        #endregion

        #region Filters
        #region Property Fields
        private ObservableCollection<Marketplace> _marketplaces = new();

        private ObservableCollection<OrderSource> _orderSources = null;

        private ObservableCollection<ShipmentCompany> _shipmentCompanies = null;

        private Marketplace _selectedMarketplace;

        private ShipmentCompany _selectedShipmentCompany;

        private OrderSource _selectedOrderSource;
        #endregion

        #region Properties
        public ObservableCollection<Marketplace> Marketplaces
        {
            get { return _marketplaces; }
            set { _marketplaces = value; OnPropertyChanged(); }
        }

        public ObservableCollection<OrderSource> OrderSources
        {
            get { return _orderSources; }
            set { _orderSources = value; OnPropertyChanged(); }
        }

        public ObservableCollection<ShipmentCompany> ShipmentCompanies
        {
            get { return _shipmentCompanies; }
            set { _shipmentCompanies = value; OnPropertyChanged(); }
        }

        public Marketplace SelectedMarketplace
        {
            get { return _selectedMarketplace; }
            set
            {
                _selectedMarketplace = value;
                OnPropertyChanged();
                OnPropertyChanged("ClearFilterMarketplace");
            }
        }

        public ShipmentCompany SelectedShipmentCompany
        {
            get { return _selectedShipmentCompany; }
            set
            {
                _selectedShipmentCompany = value;
                OnPropertyChanged();
                OnPropertyChanged("ClearFilterShipmentCompany");
            }
        }

        public OrderSource SelectedOrderSource
        {
            get { return _selectedOrderSource; }
            set
            {
                _selectedOrderSource = value;
                OnPropertyChanged();
                OnPropertyChanged("ClearFilterOrderSource");
            }
        }

        public bool ClearFilterMarketplace { get { return this.SelectedMarketplace != null; } }

        public bool ClearFilterShipmentCompany { get { return this.SelectedShipmentCompany != null; } }

        public bool ClearFilterOrderSource { get { return this.SelectedOrderSource != null; } }
        #endregion

        #region Command Fields
        private ICommand _marketplaceSelectionVisibleChangeCommand;

        private ICommand _shipmentCompanySelectionVisibleChangeCommand;

        private ICommand _orderSourceSelectionVisibleChangeCommand;

        private ICommand _selectedMarketplaceChangeCommand;

        private ICommand _selectedShipmentCompanyChangeCommand;

        private ICommand _selectedOrderSourceChangeCommand;

        private ICommand _marketplaceSelectionCloseCommand;

        private ICommand _shipmentCompanySelectionCloseCommand;

        private ICommand _orderSourceSelectionCloseCommand;
        #endregion

        #region Commands
        public ICommand MarketplaceSelectionVisibleChangeCommand
        {
            get
            {
                if (this._marketplaceSelectionVisibleChangeCommand == null)
                    this._marketplaceSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        MarketplaceSelectionVisible = !MarketplaceSelectionVisible;
                    });

                return _marketplaceSelectionVisibleChangeCommand;
            }
        }

        public ICommand ShipmentCompanySelectionVisibleChangeCommand
        {
            get
            {
                if (this._shipmentCompanySelectionVisibleChangeCommand == null)
                    this._shipmentCompanySelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        ShipmentCompanySelectionVisible = !ShipmentCompanySelectionVisible;
                    });

                return _shipmentCompanySelectionVisibleChangeCommand;
            }
        }

        public ICommand OrderSourceSelectionVisibleChangeCommand
        {
            get
            {
                if (this._orderSourceSelectionVisibleChangeCommand == null)
                    this._orderSourceSelectionVisibleChangeCommand = new RelayCommand(o =>
                    {
                        OrderSourceSelectionVisible = !OrderSourceSelectionVisible;
                    });

                return _orderSourceSelectionVisibleChangeCommand;
            }
        }

        public ICommand SelectedMarketplaceChangeCommand
        {
            get
            {
                if (this._selectedMarketplaceChangeCommand == null)
                    this._selectedMarketplaceChangeCommand = new RelayCommand(o =>
                    {
                        SelectedMarketplace = (Marketplace)o;
                        MarketplaceSelectionVisible = false;
                    });

                return _selectedMarketplaceChangeCommand;
            }
        }

        public ICommand SelectedShipmentCompanyChangeCommand
        {
            get
            {
                if (this._selectedShipmentCompanyChangeCommand == null)
                    this._selectedShipmentCompanyChangeCommand = new RelayCommand(o =>
                    {
                        SelectedShipmentCompany = (ShipmentCompany)o;
                        ShipmentCompanySelectionVisible = false;
                    });

                return _selectedShipmentCompanyChangeCommand;
            }
        }

        public ICommand SelectedOrderSourceChangeCommand
        {
            get
            {
                if (this._selectedOrderSourceChangeCommand == null)
                    this._selectedOrderSourceChangeCommand = new RelayCommand(o =>
                    {
                        SelectedOrderSource = (OrderSource)o;
                        OrderSourceSelectionVisible = false;
                    });

                return _selectedOrderSourceChangeCommand;
            }
        }

        public ICommand MarketplaceSelectionCloseCommand
        {
            get
            {
                if (this._marketplaceSelectionCloseCommand == null)
                    this._marketplaceSelectionCloseCommand = new RelayCommand(o =>
                    {
                        MarketplaceSelectionVisible = false;
                    });

                return this._marketplaceSelectionCloseCommand;
            }
        }

        public ICommand ShipmentCompanySelectionCloseCommand
        {
            get
            {
                if (this._shipmentCompanySelectionCloseCommand == null)
                    this._shipmentCompanySelectionCloseCommand = new RelayCommand(o =>
                    {
                        ShipmentCompanySelectionVisible = false;
                    });

                return this._shipmentCompanySelectionCloseCommand;
            }
        }

        public ICommand OrderSourceSelectionCloseCommand
        {
            get
            {
                if (this._orderSourceSelectionCloseCommand == null)
                    this._orderSourceSelectionCloseCommand = new RelayCommand(o =>
                    {
                        OrderSourceSelectionVisible = false;
                    });

                return this._orderSourceSelectionCloseCommand;
            }
        }
        #endregion
        #endregion

        #region Packaging
        #region Commands
        public override ICommand GetOrderCommand
        {
            get
            {
                if (this._getOrderCommand == null)
                    this._getOrderCommand = new RelayCommand(async o =>
                    {
                        if (this.Order != null)
                            return;

                        this.PrintBarcodesVisible = false;

                        OrderDetails = new ObservableCollection<OrderDetailViewModel>();

                        var reserveResponse = await this._orderService.ReserveAsync(new()
                        {
                            MarketplaceId = this.SelectedMarketplace != null
                                ? this.SelectedMarketplace.Id
                                : "",
                            OrderId = this.SelectedOrderId != null ? this.SelectedOrderId : null,
                            OrderSourceId = this.SelectedOrderSource != null
                                ? this.SelectedOrderSource.Id
                                : null,
                            ShipmentCompanyId = this.SelectedShipmentCompany != null
                                ? this.SelectedShipmentCompany.Id
                                : ""
                        });

                        if (!reserveResponse.Success)
                        {
                            MessageVisible = true;
                            Message = reserveResponse.Message;
                            return;
                        }

                        if (!reserveResponse.Success)
                        {
                            MessageVisible = true;
                            Message = reserveResponse.Message;
                            return;
                        }

                        var orderResponse = await this._orderService.GetAsync(new()
                        {
                            OrderId = reserveResponse.Data,
                            PackerBarcode = "0"
                        });

                        if (!orderResponse.Success)
                        {
                            MessageVisible = true;
                            Message = orderResponse.Message;
                            return;
                        }

                        this.Order = orderResponse.Data;
                        this
                            .Order
                            .OrderDetails
                            .GroupBy(od => od.Product.ProductInformation.Barcode)
                            .Select(god => new OrderDetailViewModel
                            {
                                Barcode = god.Key,
                                CompletedQuantity = 0,
                                Quantity = god.Sum(x => x.Quantity),
                                Name = god.First().Product.ProductInformation.FullName,
                                PhotoUrl = god.First().Product.ProductInformation.ProductInformationPhoto.FileName
                            })
                            .ToList()
                            .ForEach(odvm => OrderDetails.Add(odvm));

                        PackagingVisible = !PackagingVisible;
                        BarcodeFocused = false;
                        BarcodeFocused = true;
                    });

                return _getOrderCommand;
            }
        }

        public override ICommand CancelOrderCommand
        {
            get
            {
                if (this._cancelOrderCommand == null)
                    this._cancelOrderCommand = new RelayCommand(async o =>
                    {
                        if (this.Order == null)
                            return;

                        var cancelResponse = await this._orderService.CancelAsync(this.Order.Id);
                        if (!cancelResponse.Success)
                        {
                            this.MessageVisible = true;
                            this.Message = cancelResponse.Message;
                            return;
                        }

                        PackagingVisible = !PackagingVisible;

                        this.Order = null;
                    });

                return _cancelOrderCommand;
            }
        }
        #endregion
        #endregion

        #region Helper Methods
        private async void Summary()
        {
            var summaryResponse = await this._orderService.SummaryAsync();

            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                this.TodayOrdersCount = summaryResponse.Data.TodayOrdersCount;
                this.PackedOrdersCount = summaryResponse.Data.PackedOrdersCount;
                this.UnpackedOrdersCount = summaryResponse.Data.UnpackedOrdersCount;
            });

            this._timer.Start();
        }

        public override void PrintBarcodes()
        {
            this.Order.Company = this.SelectedCompany;

            if (!string.IsNullOrEmpty(this.Order.OrderShipments[0].TrackingBase64))
            {
                Base64Print base64Print = new("Invoice");
                base64Print.Print(this.Order.OrderShipments[0].TrackingBase64);
            }
            else
            {
                WaybillPrint waybillPrint = new("Invoice");
                waybillPrint.Print(this.Order);
            }
        }

        public override async void Initialize()
        {
            this.OrderSources = new ObservableCollection<OrderSource>();
            var orderSources = await this._orderSourceService.GetAsync();
            if (orderSources.Success)
                orderSources.Data.ForEach(os => this.OrderSources.Add(os));

            this.ShipmentCompanies = new ObservableCollection<ShipmentCompany>();
            var shipmentCompanies = await this._shipmentCompanyService.GetAsync();
            if (shipmentCompanies.Success)
                shipmentCompanies.Data.ForEach(sc => this.ShipmentCompanies.Add(sc));


            this.Marketplaces = new ObservableCollection<Marketplace>();
            var response = await this._marketplaceService.GetAsync();
            if (response.Success)
                response.Data.ForEach(m => this.Marketplaces.Add(m));


            #region Timers
            this._timer = new(10000);
            this._timer.Elapsed += (e, s) =>
            {
                this._timer.Stop();

                Thread thread = new(() => this.Summary());
                thread.Start();
            };
            this._timer.Start();
            #endregion
        }
        #endregion
    }
}