﻿namespace ECommerce.Packaging.WpfApp.ViewModels
{
    public class OrderDetailViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        private int _completedQuantity;
        #endregion

        #region Properties
        public string Barcode { get; set; }

        public string Name { get; set; }

        public string PhotoUrl { get; set; }

        public int Quantity { get; set; }

        public int CompletedQuantity
        {
            get { return _completedQuantity; }
            set { _completedQuantity = value; OnPropertyChanged(); OnPropertyChanged("Completed"); OnPropertyChanged("Invalid"); }
        }

        public bool Completed => this.Quantity == this.CompletedQuantity;

        public bool Invalid => !this.Completed;
        #endregion
    }
}
