﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class TextileReturnWindow : Window
    {
        #region Constructors
        public TextileReturnWindow(TextileReturnWindowViewModel textileReturnWindowViewModel)
        {
            InitializeComponent();

            DataContext = textileReturnWindowViewModel;
        }
        #endregion
    }
}
