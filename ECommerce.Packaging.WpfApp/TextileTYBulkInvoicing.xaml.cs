﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class TextileTYBulkInvoicing : Window
    {
        #region Constructors
        public TextileTYBulkInvoicing(TextileTYBulkInvoicingViewModel textileTYBulkInvoicingViewModel)
        {
            InitializeComponent();

            DataContext = textileTYBulkInvoicingViewModel;
        } 
        #endregion
    }
}
