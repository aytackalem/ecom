﻿using System.Collections.Generic;

namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Marketplace
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<MarketPlaceConfiguration> MarketPlaceConfigurations { get; set; }
        #endregion
    }
}
