﻿
namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Configuration
    {
        #region Properties
        /// <summary>
        /// Ücretsiz kargo limiti
        /// </summary>
        public decimal CargoLimit { get; set; }
        /// <summary>
        /// Sabit Telefon Numarası
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Site Url Adresi
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// Sepete Çerez İsmi
        /// </summary>
        public string ShoppingCartCookie { get; set; }
        /// <summary>
        /// Sipariş Çerez İsmi
        /// </summary>
        public string OrderCookie { get; set; }

        /// <summary>
        /// Havale yapılacak Iban numarası
        /// </summary>
        public string Iban { get; set; }

        /// <summary>
        /// Şirket Adı ibanın yanında şireket ismi yazar
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Şireketin Web sitesinin kısa adı
        /// </summary>
        public string WebShortName { get; set; }

        /// <summary>
        /// Şirket Url
        /// </summary>
        public string YoutubeUrl { get; set; }

        /// <summary>
        /// İnstagram Url
        /// </summary>
        public string İnstagramUrl { get; set; }

        /// <summary>
        /// Facebook Url
        /// </summary>
        public string FacebookUrl { get; set; }

        /// <summary>
        /// Havale Yapılacak banka
        /// </summary>
        public string IbanBank { get; set; }
        #endregion
    }
}
