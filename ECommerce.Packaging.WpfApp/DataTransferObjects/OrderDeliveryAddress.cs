﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderDeliveryAddress
    {
        public string Recipient { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public Neighborhood Neighborhood { get; set; }
    }
}
