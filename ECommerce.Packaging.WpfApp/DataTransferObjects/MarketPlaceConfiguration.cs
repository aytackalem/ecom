﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class MarketPlaceConfiguration
    {
        #region Properties
        public string Key { get; set; }

        public string Value { get; set; } 
        #endregion
    }
}
