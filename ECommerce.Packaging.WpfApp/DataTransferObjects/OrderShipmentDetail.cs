﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderShipmentDetail
    {
        public int Id { get; set; }

        public int OrderShipmentId { get; set; }

        public string TrackingCode { get; set; }

        public string Weight { get; set; }

        public OrderShipment OrderShipment { get; set; }
    }
}
