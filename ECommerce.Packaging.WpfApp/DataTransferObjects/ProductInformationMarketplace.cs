﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class ProductInformationMarketplace
    {
        public string MarketPlaceId { get; set; }

        public string StockCode { get; set; }
    }
}
