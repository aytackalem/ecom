﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class District
    {
        public string Name { get; set; }

        public City City { get; set; }
    }
}
