﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Product
    {
        public string SellerCode { get; set; }

        public ProductInformation ProductInformation { get; set; }
    }
}
