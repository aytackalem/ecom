﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class ShipmentCompany
    {
        #region Properties
        public string Id { get; set; }

        public string Name { get; set; } 
        #endregion
    }
}