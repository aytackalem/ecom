﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class VariantValueTranslation
    {
        #region Properties
        public int Id { get; set; }

        public string LanguageId { get; set; }

        public int VariantValueId { get; set; }

        public string Value { get; set; }
        #endregion


        public Variant Variant { get; set; }
    }
}
