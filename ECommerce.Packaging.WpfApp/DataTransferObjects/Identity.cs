﻿using System;

namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Identity
    {
        #region Properties
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime Expiry { get; set; }
        #endregion
    }
}
