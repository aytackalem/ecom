﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderBilling
    {
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public string ReturnNumber { get; set; }

        public string ReturnDescription { get; set; }
    }
}
