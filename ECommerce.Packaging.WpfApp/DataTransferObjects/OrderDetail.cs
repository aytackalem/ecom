﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderDetail
    {
        #region Properties
        public int Quantity { get; set; }

        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal UnitCost { get; set; }

        public decimal VatRate { get; set; }

        public bool Payor { get; set; }

        public int ProductInformationId { get; set; }

        public decimal VatExcUnitPrice { get; set; }
        
        public decimal VatExcListPrice { get; set; }

        public decimal VatExcUnitCost { get; set; }

        public decimal VatExcUnitDiscount { get; set; }
        #endregion

        #region Navigation Properties
        public Product Product { get; set; }
        #endregion
    }
}
