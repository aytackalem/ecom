﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderSummary
    {
        #region Properties
        public int TodayOrdersCount { get; set; }

        public int PackedOrdersCount { get; set; }

        public int UnpackedOrdersCount { get; set; }
        #endregion
    }
}
