﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Domain
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Default { get; set; }
        #endregion
    }
}
