﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class ProductInformationPhoto
    {
        public string FileName { get; set; }
    }
}
