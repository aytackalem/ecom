﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class ArasCompanyConfiguration
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
