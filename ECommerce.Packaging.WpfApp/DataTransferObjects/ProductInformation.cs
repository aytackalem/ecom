﻿using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class ProductInformation
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string Barcode { get; set; }

        public string FullName { get; set; }
        #endregion

        #region Navigation Properties
        public ProductInformationPhoto ProductInformationPhoto { get; set; }

        public List<ProductInformationVariant> ProductInformationVariants { get; set; }
        public string ProductInformationUUId { get; set; }

        #endregion
    }
}
