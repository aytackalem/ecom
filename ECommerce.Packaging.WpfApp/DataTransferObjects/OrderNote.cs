﻿using System.Collections.Generic;

namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class OrderNote
    {
        #region Properties
        public int Id { get; set; }

        public string Note { get; set; }
        #endregion
    }
}
