﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Company
    {
        #region Properties
        public int Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }
        #endregion

        #region Navigation Properties
        public CompanyContact CompanyContact { get; set; }
        #endregion
    }
}
