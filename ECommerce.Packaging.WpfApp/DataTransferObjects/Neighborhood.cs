﻿namespace ECommerce.Packaging.WpfApp.DataTransferObjects
{
    public class Neighborhood
    {
        public string Name { get; set; }

        public District District { get; set; }
    }
}
