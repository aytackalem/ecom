﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Packaging.WpfApp.Finders
{
    public class DomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Methods
        public int FindId()
        {
            return this._domainId;
        }

        public void Set(int id)
        {
            this._domainId = id;
        }
        #endregion
    }
}
