﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Packaging.WpfApp.Finders
{
    public class CompanyFinder : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Methods
        public int FindId()
        {
            return this._companyId;
        }

        public void Set(int id)
        {
            this._companyId = id;
        }
        #endregion
    }
}
