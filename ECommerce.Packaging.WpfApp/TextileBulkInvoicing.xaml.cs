﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class TextileBulkInvoicing : Window
    {
        #region Constructors
        public TextileBulkInvoicing(TextileBulkInvoicingViewModel textileBulkInvoicingViewModel)
        {
            InitializeComponent();

            DataContext = textileBulkInvoicingViewModel;
        } 
        #endregion
    }
}
