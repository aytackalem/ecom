﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing.Base;
using Helpy.Shared.Print;
using Helpy.Shared.Print.Base;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class TrendyolEuropePrint : PrintBase<Order>
    {
        #region Fields
        private readonly string _fontFamily = "Barlow Condensed";
        #endregion

        #region Constructors
        public TrendyolEuropePrint(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void Print(Order order, PrintPageEventArgs e)
        {
            Int32 pageWidth = 100;
            Int32 bodyFontSize = 7;
            Int32 subHeaderFontSize = 10;
            Int32 headerFontSize = 12;

            Page page = new(new List<Element>
            {
                new Row(pageWidth, 10, new List<Element>
                {
                    new Cell(45, 10, new Barcode(30, 10)
                    {
                        Value = order.Id.ToString()
                    })
                    {
                        AlignCenterChild = true
                    },
                    new Cell(10, 10, new Text(10, 10)
                    {
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Value = string.Empty
                    }),
                    new Cell(45, 10, new Barcode(38, 10)
                    {
                        Value = order.OrderDetails[0].Product.ProductInformation.Barcode
                    })
                    {
                        AlignCenterChild = true
                    }
                }),
                new Row(pageWidth, 6, new List<Element>
                {
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = order.Id.ToString(),
                        Bold = true,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        AlignCenter = true
                    }),
                    new Cell(10, 6, new Text(10, 6)
                    {
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Value = string.Empty
                    }),
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = order.OrderDetails[0].Product.ProductInformation.Barcode,
                        Bold = true,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        AlignCenter = true
                    })
                }),
                new Row(pageWidth, 6, new List<Element>
                {
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = order.OrderDate.ToString("yyyy-MM-dd HH:mm"),
                        Bold = true,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        AlignCenter = true
                    }),
                    new Cell(10, 6, new Text(10, 6)
                    {
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Value = string.Empty
                    }),
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = order.OrderDetails[0].Product.SellerCode,
                        FontFamily = _fontFamily,
                        Bold = true,
                        FontSize = subHeaderFontSize,
                    })
                }),
                new Row(pageWidth, 10, new List<Element>
                {
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = string.Empty,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize
                    }),
                    new Cell(10, 6, new Text(10, 6)
                    {
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Value = string.Empty
                    }),
                    new Cell(45, 10, new Text(40, 10)
                    {
                        Value = order.OrderDetails[0].Product.ProductInformation.Name,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                    })
                }),
                new Row(pageWidth, 10, new List<Element>
                {
                    new Cell(45, 6, new Text(40, 6)
                    {
                        Value = string.Empty,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize
                    }),
                    new Cell(10, 6, new Text(10, 6)
                    {
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize,
                        Value = string.Empty
                    }),
                    new Cell(45, 10, new Text(40, 10)
                    {
                        Value = string.Join(",", order.OrderDetails[0].Product.ProductInformation?.ProductInformationVariants.Select(piv => piv.VariantValue.Name)),
                        Bold = true,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                    })
                })
            })
            {
                MarginLeft = 6
            };
            page.Print(e, 0, 0);
        }

        protected override void PrintConcreate(Order t)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }
        #endregion
    }
}
