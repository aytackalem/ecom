﻿using ECommerce.Application.Common.Constants;
using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing.Base;
using Helpy.Shared.Print;
using Helpy.Shared.Print.Base;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class Invoice80Print : PrintBase<Order>
    {
        #region Fields
        private readonly string _fontFamily = "Barlow Condensed";
        #endregion

        #region Constructors
        public Invoice80Print(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(Order order)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(Order order, PrintPageEventArgs e)
        {
            var globalVatRate = 0.18M;
            var globalVatRateString = "18";
            var micro = order.Micro;

            var firstOrderDetail = order.OrderDetails.FirstOrDefault();
            if (firstOrderDetail != null)
            {
                var _8 = Math.Round(firstOrderDetail.UnitPrice / 1.08M, 4) == firstOrderDetail.VatExcUnitPrice;
                var _18 = Math.Round(firstOrderDetail.UnitPrice / 1.18M, 4) == firstOrderDetail.VatExcUnitPrice;

                if (!_8 && !_18)
                {
                    globalVatRate = 0.2M;
                    globalVatRateString = "20";
                }
            }

            Int32 pageWidth = 70;
            Int32 bodyFontSize = 7;
            Int32 subHeaderFontSize = 10;
            Int32 headerFontSize = 12;

            Row row = null;
            if (File.Exists($@"C:\Images\{order.Company.Name}.png"))
                row = new Row(pageWidth, 7, new()
                {
                    new Cell(pageWidth, 7, new Picture(30, 7)
                    {
                        Src = $@"C:\Images\{order.Company.Name}.png"
                    })
                    {
                        AlignCenterChild = true
                    }
                });
            else
                row = new Row(pageWidth, 8, new()
                {
                    new Cell(pageWidth, 8, new Text(pageWidth, 8)
                    {
                        AlignCenter = true,
                        Value = order.Company.Name,
                        FontFamily = _fontFamily,
                        FontSize = headerFontSize,
                        Bold = true
                    })
                });

            Page page = new(new List<Element>
            {
                /* Company Name */
                row,
                /* Company Full Name */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(pageWidth, 5, new Text(pageWidth, 5)
                    {
                        AlignCenter = true,
                        Value = order.Company.FullName,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                }),
                /* Company Address - Stamp */
                new Row(pageWidth, 20, new List<Element>()
                {
                    /* Company Address */
                    new Cell(50, 20, new Text(50, 20)
                    {
                        Value = $@"ADRES: {order.Company.CompanyContact.Address}
TELEFON: {order.Company.CompanyContact.PhoneNumber}
V.D: {order.Company.CompanyContact.TaxOffice}
V.K.N: {order.Company.CompanyContact.TaxNumber}",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    }),
                    /* Stamp */
                    new Cell(20, 20, new Picture(20, 20)
                    {
                        Src = @"C:\Images\Stamp.jpeg"
                    })
                })
                {
                    MarginBottom = 2
                },
                /* Invoice Table */
                /* Invoice Date */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "FATURA TARİHİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = globalVatRateString == "18" ? "2023-07-09" : DateTime.Now.ToString("yyyy-MM-dd"),
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Invoice Number */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "FATURA NO",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.OrderInvoiceInformation.Number,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Scenario */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "SENARYO",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = "EARSIVFATURA",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Invoice Type */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "FATURA TİPİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = micro ? "ISTISNA" :"SATIS",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Created Hour */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "OLUŞTURMA ZAMANI",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = DateTime.Now.ToString("HH:mm:ss"),
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Order Date */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "SİPARİŞ TARİHİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.OrderDate.ToString("yyyy-MM-dd"),
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Order Id */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "SİPARİŞ NO",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.Id.ToString(),
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Customer */
                new Row(pageWidth, 8, new List<Element>()
                {
                    new Cell(pageWidth, 8, new Text(pageWidth, 8)
                    {
                        Value = $"{order.OrderInvoiceInformation.FirstName} {order.OrderInvoiceInformation.LastName}",
                        FontFamily = _fontFamily,
                        FontSize = headerFontSize,
                        Bold = true
                    })
                }),
                /* Company Address - Stamp */
                new Row(pageWidth, 20, new List<Element>()
                {
                    /* Company Address */
                    new Cell(pageWidth, 20, new Text(pageWidth, 20)
                    {
                        Value = $@"ADRES: {order.OrderInvoiceInformation.Address}
TELEFON: {order.OrderInvoiceInformation.Phone}",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                }),
                /* Product Lists */
                /* Header */
                new Row(pageWidth, 6, new List<Element>()
                {
                    new Cell(pageWidth, 6, new Text(pageWidth, 6)
                    {
                        AlignCenter = true,
                        Value = "ÜRÜN BİLGİLERİ",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                }),
            });

            /* Order Details */
            for (int i = 0; i < order.OrderDetails.Count; i++)
            {
                var orderDetail = order.OrderDetails[i];

                page.Elements.Add(new Row(pageWidth, 5, new()
                {
                    new Cell(5, 5, new Text(5, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = orderDetail.Quantity.ToString(),
                        FontSize = bodyFontSize,
                        AlignCenter = true
                    })
                    {
                        Border = true,
                    },
                    new Cell(50, 5, new Text(50, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = orderDetail.Product.ProductInformation.Name,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true,
                    },
                    new Cell(15, 5, new Text(15, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = $"₺ {orderDetail.Quantity * orderDetail.UnitPrice:N2}",
                        FontSize = bodyFontSize,
                        AlignCenter = true
                    })
                    {
                        Border = true,
                    }
                }));
            }

            var cargoFeeExcTotal = (order.CargoFee / globalVatRate);
            var surchargeFeeExcTotal = (order.SurchargeFee / globalVatRate);

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $@"KARGO UCRETI: ₺{cargoFeeExcTotal:N2} KDV (%{globalVatRateString})",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $@"KAPIDA ODEME UCRETI: ₺{surchargeFeeExcTotal:N2} KDV (%{globalVatRateString})",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $"MAL HİZMET TOPLAM TUTAR: ₺{order.OrderDetails.Sum(od => od.VatExcUnitPrice * od.Quantity):N2}",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $"HESAPLANAN KDV: ₺{order.CargoFee - cargoFeeExcTotal + (order.SurchargeFee - surchargeFeeExcTotal) + order.OrderDetails.Sum(od => (od.UnitPrice - od.VatExcUnitPrice) * od.Quantity):N2}",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $"VERGİLER DAH. TOPLAM TUTAR: ₺{order.Amount:N2}",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            /* Order Id Barcode */
            page.Elements.Add(new Row(pageWidth, 10, new List<Element>
            {
                new Cell(pageWidth, 10, new Barcode(pageWidth, 10)
                {
                    Value = order.Id.ToString()
                })
            }));

            /* Order Id Barcode Text */
            page.Elements.Add(new Row(pageWidth, 6, new List<Element>
            {
                new Cell(pageWidth, 6, new Text(pageWidth, 6)
                {
                    Value = order.Id.ToString(),
                    Bold = true,
                    FontFamily = _fontFamily,
                    FontSize = subHeaderFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = "* BU SATIŞ İNTERNET ÜZERİNDEN YAPILMIŞTIR.",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = "* E-ARŞİV İZNİ KAPSAMINDA ELEKTRONİK ORTAMDA GÖNDERİLMİŞTİR.",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = "* İRSALİYE YERİNE GEÇER.",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = "* LÜTFEN İADE İÇİN BU BELGEYİ SAKLAYINIZ.",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            /* Cargo Barcode */
            var orderShipment = order.OrderShipments.FirstOrDefault();
            if (orderShipment != null &&
                order.OrderSourceId == OrderSources.Pazaryeri &&
                string.IsNullOrEmpty(orderShipment.TrackingBase64))
            {
                var rows = new List<Row>
                {
                    /* Cargo Barcode */
                    new Row(pageWidth, 10, new List<Element>()
                    {
                        new Cell(pageWidth, 10, new Barcode(pageWidth, 10)
                        {
                            Value = order.OrderShipments.FirstOrDefault().TrackingCode
                        })
                    }),
                    /* Cargo Barcode Text */
                    new Row(pageWidth, 6, new List<Element>()
                    {
                        new Cell(pageWidth, 6, new Text(pageWidth, 6)
                        {
                            Bold = true,
                            Value = order.OrderShipments.FirstOrDefault().TrackingCode,
                            FontFamily = _fontFamily,
                            FontSize = subHeaderFontSize
                        })
                    })
                };

                var summary = order.OrderShipments.FirstOrDefault().ShipmentCompany.Name;

                if (!string.IsNullOrEmpty(order.MarketPlaceId))
                    summary = $"{summary} - {order.MarketPlace.Name} - {order.MarketPlaceOrderNumber}";

                /* Cargo Summary */
                rows.Add(new Row(pageWidth, 6, new List<Element>()
                {
                    new Cell(pageWidth, 6, new Text(pageWidth, 6)
                    {
                        Bold = true,
                        Value = summary,
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize
                    })
                }));

                page.Elements.InsertRange(0, rows);
            }

            page.Print(e, 0, 0);
        }
        #endregion
    }
}