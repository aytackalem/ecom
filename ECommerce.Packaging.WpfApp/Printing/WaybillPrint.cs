﻿using ECommerce.Packaging.WpfApp.Printing.Base;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Linq;
using Zen.Barcode;
using ECommerce.Packaging.WpfApp.DataTransferObjects;

namespace ECommerce.Packaging.WpfApp.Printing
{
    /// <summary>
    /// Sevk irsaliyesi yazdırma işlemini temsil eden sınıf.
    /// </summary>
    public class WaybillPrint : PrintBase<Order>
    {
        #region Fields
        private string _companyInfo;
        #endregion

        #region Constructors
        public WaybillPrint(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(Order order)
        {
            this._companyInfo = @$"{order.Company.Name}
{order.Company.CompanyContact.Address}
{order.Company.CompanyContact.PhoneNumber} {order.Company.CompanyContact.Email}";

            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(Order order, PrintPageEventArgs e)
        {
            int axisY = 0;

            /* Company Info */
            e.Graphics.DrawString(this._companyInfo, _font7, _brushBlack, new RectangleF(4, axisY + 2, 40, 20));
           
            /* Customer Info */
            string location = string.Empty;
            var district = string.Empty;
            var city = string.Empty;
            var neighborhood = string.Empty;

            if (order.OrderDeliveryAddress.Neighborhood != null &&
            !string.IsNullOrEmpty(order.OrderDeliveryAddress.Neighborhood.Name))
                neighborhood = order.OrderDeliveryAddress.Neighborhood.Name;

            if (order.OrderDeliveryAddress.Neighborhood != null &&
                order.OrderDeliveryAddress.Neighborhood.District != null &&
                !string.IsNullOrEmpty(order.OrderDeliveryAddress.Neighborhood.District.Name))
                district = order.OrderDeliveryAddress.Neighborhood.District.Name;

            if (order.OrderDeliveryAddress.Neighborhood != null &&
                order.OrderDeliveryAddress.Neighborhood.District != null &&
                order.OrderDeliveryAddress.Neighborhood.District.City != null &&
                !string.IsNullOrEmpty(order.OrderDeliveryAddress.Neighborhood.District.City.Name))
                city = order.OrderDeliveryAddress.Neighborhood.District.City.Name;

            location = $@"{neighborhood}-{district}/{city}";

#warning Degisecek
            /* Stamp */
            try
            {
                var image = (Bitmap)Image.FromFile(@"C:\Images\Stamp.jpeg").Clone();
                //e.Graphics.DrawImage(image, 36, axisY + 4, 20, 20);
                //e.Graphics.DrawImageUnscaled(image, 36, axisY + 4, 30, 30);
                e.Graphics.DrawImage(image, new RectangleF(36, axisY + 4, 20, 20), new RectangleF(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            catch { }

            e.Graphics.DrawString($"SEVK İRSALİYESİ".ToUpper(), _font12Bold, _brushBlack, 30, axisY + 25);
            e.Graphics.DrawString($"SERİ: A", _font7, _brushBlack, 73, 19);
            e.Graphics.DrawString($"SIRA NO: {order.Id}".ToUpper(), _font7, _brushBlack, 73, 22);
            e.Graphics.DrawString($"Tarih: {DateTime.Now:yyyy-MM-dd HH:mm}", _font7, _brushBlack, 73, 25);
            //e.Graphics.DrawString($"Ödeme Tarihi: {DateTime.Now:yyyy-MM-dd HH:mm}", _font7, _brushBlack, 73, axisY + 31);

            e.Graphics.DrawString($@"
SAYIN
{order.OrderDeliveryAddress.Recipient}
{order.OrderDeliveryAddress.Address} {location} 
Tel: {order.OrderDeliveryAddress.PhoneNumber}", _font8, _brushBlack, new RectangleF(4, axisY + 28, 90, 35));


            /* Header */
            axisY += 2;
            e.Graphics.DrawString($"Adet".ToUpper(), _font7, _brushBlack, 4, axisY + 53);
            e.Graphics.DrawString($"Ürün".ToUpper(), _font7, _brushBlack, 12, axisY + 53);
            //e.Graphics.DrawString($"B.F.".ToUpper(), _font7, _brushBlack, 60, axisY + 53);
            //e.Graphics.DrawString($"K.O.".ToUpper(), _font7, _brushBlack, 70, axisY + 53);
            //e.Graphics.DrawString($"K.T.".ToUpper(), _font7, _brushBlack, 80, axisY + 53);
            //e.Graphics.DrawString($"M.H.T.".ToUpper(), _font7, _brushBlack, 90, axisY + 53);

            /* Product */
            var discount = 0M;
            var productYAxis = axisY + 56;
            for (int i = 0; i < order.OrderDetails.Count; i++)
            {
                var orderDetail = order.OrderDetails[i];

                var productName = orderDetail.Product.ProductInformation.Name;
                if (productName.Length > 30)
                    productName = productName.Substring(0, 30);

                e.Graphics.DrawString($"{orderDetail.Quantity}", _font7, _brushBlack, 4, productYAxis);
                e.Graphics.DrawString("KOZMETİK ÜRÜN"/*productName.ToUpper()*/, _font7, _brushBlack, 12, productYAxis);
                //e.Graphics.DrawString($"{orderDetail.ListUnitPrice - orderDetail.ListUnitPrice * orderDetail.VatRate}", _font7, _brushBlack, 60, productYAxis);
                //e.Graphics.DrawString($"{orderDetail.VatRate * 100}", _font7, _brushBlack, 70, productYAxis);
                //e.Graphics.DrawString($"{orderDetail.ListUnitPrice * orderDetail.VatRate * orderDetail.Quantity}", _font7, _brushBlack, 80, productYAxis);
                //e.Graphics.DrawString($"{orderDetail.ListUnitPrice * orderDetail.Quantity}", _font7, _brushBlack, 90, productYAxis);

                productYAxis += 3;
            }

            axisY = productYAxis + 10;

            //e.Graphics.DrawString($"MAL HİZMET TOPLAM TUTAR: {order.OrderDetails.Sum(od => (od.ListUnitPrice - od.ListUnitPrice * od.VatRate) * od.Quantity)}".ToUpper(), _font7, _brushBlack, 5, axisY + 3);
            //e.Graphics.DrawString($"TOPLAM İSKONTO: {order.OrderDetails.Sum(od => ((od.ListUnitPrice - od.UnitPrice) * od.Quantity) - (((od.ListUnitPrice - od.UnitPrice) * od.Quantity) * od.VatRate))}".ToUpper(), _font7, _brushBlack, 5, axisY + 6);
            //e.Graphics.DrawString($"ARA TOPLAM: {order.OrderDetails.Sum(od => (od.UnitPrice - od.UnitPrice * od.VatRate) * od.Quantity)}".ToUpper(), _font7, _brushBlack, 5, axisY + 9);
            //e.Graphics.DrawString($"HESAPLANAN KDV: {order.OrderDetails.Sum(od => od.UnitPrice * od.VatRate * od.Quantity)}".ToUpper(), _font7, _brushBlack, 5, axisY + 12);
            //e.Graphics.DrawString($"VERGİLER DAH. TOPLAM TUTAR: {order.Amount}".ToUpper(), _font7, _brushBlack, 5, axisY + 15);
            //e.Graphics.DrawString($"ÖDENECEK TUTAR: {order.Amount}".ToUpper(), _font7, _brushBlack, 5, axisY + 18);

            //e.Graphics.DrawString($"İRSALİYE YERİNE GEÇER.".ToUpper(), _font7, _brushBlack, 5, axisY + 21);
            //e.Graphics.DrawString($"E-ARŞİV İZNİ KAPSAMINDA ELEKTRONİK ORTAMDA İLETİLMİŞTİR.".ToUpper(), _font7, _brushBlack, 5, axisY + 24);

            #region Barcode
            var barcodeImage = BarcodeDrawFactory.Code128WithChecksum.Draw(order.OrderShipments.FirstOrDefault().TrackingCode, 60, 2);
            var barcodeBitmap = new Bitmap(barcodeImage.Width, barcodeImage.Height);

            using (Graphics graphics = Graphics.FromImage(barcodeBitmap))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                graphics.DrawImage(barcodeImage, 0, 0);
            }

            e.Graphics.DrawImage(barcodeBitmap, 25, 110);

            e.Graphics.DrawString(order.OrderShipments.FirstOrDefault().TrackingCode, _font12Bold, _brushBlack, 35, 105);
            #endregion

            var summary = order.OrderShipments.FirstOrDefault().ShipmentCompany.Name;

            if (!string.IsNullOrEmpty(order.MarketPlaceId))
                summary = $"{summary} - {order.MarketPlace.Name} - {order.MarketPlaceOrderNumber}";

            e.Graphics.DrawString(summary, _font12Bold, _brushBlack, 5, 130);
        }
        #endregion
    }
}
