﻿using ECommerce.Packaging.WpfApp.Printing.Base;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class ShipmentPrint : PrintBase<string>
    {
        #region Constructors
        public ShipmentPrint(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(string base64)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(string base64, PrintPageEventArgs e)
        {
            if (base64.Contains("base64, "))
                base64 = base64.Split("base64, ")[1];

            var bytes = Convert.FromBase64String(base64);
            using var memoryStream = new MemoryStream(bytes);
            using var bitmap = new Bitmap(memoryStream);

            e.Graphics.DrawImage(bitmap, 0, 0, 100, 135);
        }
        #endregion
    }
}
