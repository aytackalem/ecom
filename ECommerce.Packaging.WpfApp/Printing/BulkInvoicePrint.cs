﻿using ECommerce.Packaging.WpfApp.Printing.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using ECommerce.Packaging.WpfApp.DataTransferObjects;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class BulkInvoicePrint : PrintBase<List<Order>>
    {
        #region Fields
        private string _companyInfo;
        #endregion

        #region Constructors
        public BulkInvoicePrint(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(List<Order> orders)
        {
            var order = orders.FirstOrDefault();

            this._companyInfo = @$"{order.Company.Name}
{order.Company.CompanyContact.Address}
{order.Company.CompanyContact.PhoneNumber} {order.Company.CompanyContact.Email}";

            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(List<Order> orders, PrintPageEventArgs e)
        {
            var order = orders.FirstOrDefault();

            int axisY = 0;

            /* Company Info */
            e.Graphics.DrawString(this._companyInfo, _font7, _brushBlack, new RectangleF(4, axisY + 2, 40, 20));

            /* Customer Info */
            string location = string.Empty;
            var district = string.Empty;
            var city = string.Empty;
            var neighborhood = string.Empty;

            if (order.OrderInvoiceInformation.Neighborhood != null &&
            !string.IsNullOrEmpty(order.OrderInvoiceInformation.Neighborhood.Name))
                neighborhood = order.OrderInvoiceInformation.Neighborhood.Name;

            if (order.OrderInvoiceInformation.Neighborhood != null &&
                order.OrderInvoiceInformation.Neighborhood.District != null &&
                !string.IsNullOrEmpty(order.OrderInvoiceInformation.Neighborhood.Name))
                district = order.OrderInvoiceInformation.Neighborhood.District.Name;

            if (order.OrderInvoiceInformation.Neighborhood != null &&
                order.OrderInvoiceInformation.Neighborhood.District != null &&
                order.OrderInvoiceInformation.Neighborhood.District.City != null &&
                !string.IsNullOrEmpty(order.OrderInvoiceInformation.Neighborhood.District.City.Name))
                city = order.OrderInvoiceInformation.Neighborhood.District.City.Name;

            location = $@"{neighborhood}-{district}/{city}";

#warning Degisecek
            /* Stamp */
            try
            {
                var image = (Bitmap)Image.FromFile(@"C:\Images\Stamp.jpeg").Clone();
                //e.Graphics.DrawImage(image, 36, axisY + 4, 20, 20);
                //e.Graphics.DrawImageUnscaled(image, 36, axisY + 4, 30, 30);
                e.Graphics.DrawImage(image, new RectangleF(36, axisY + 4, 20, 20), new RectangleF(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            catch { }

            e.Graphics.DrawString($"E-ARŞİV FATURASI".ToUpper(), _font12Bold, _brushBlack, 30, axisY + 25);
            e.Graphics.DrawString($"Fatura No: {order.OrderInvoiceInformation.Number}".ToUpper(), _font7, _brushBlack, 73, 22);
            e.Graphics.DrawString($"Tarih: {DateTime.Now:yyyy-MM-dd HH:mm}", _font7, _brushBlack, 73, 25);
            e.Graphics.DrawString($"Ödeme Tarihi: {DateTime.Now:yyyy-MM-dd HH:mm}", _font7, _brushBlack, 73, axisY + 31);

            e.Graphics.DrawString($@"
SAYIN
{order.OrderInvoiceInformation.FirstName}
{order.OrderInvoiceInformation.Address.Replace("\n", "").Replace("\r", "")} {location} 
Tel: {order.OrderDeliveryAddress.PhoneNumber}
Vergi Numarası: {order.OrderInvoiceInformation.TaxNumber}
Vergi Dairesi: {order.OrderInvoiceInformation.TaxOffice}", _font8, _brushBlack, new RectangleF(4, axisY + 28, 90, 35));


            /* Header */
            axisY += 2;
            e.Graphics.DrawString($"ÜrünId.".ToUpper(), _font7, _brushBlack, 4, axisY + 53);
            e.Graphics.DrawString($"VaryantId".ToUpper(), _font7, _brushBlack, 12, axisY + 53);
            e.Graphics.DrawString($"Barkod".ToUpper(), _font7, _brushBlack, 24, axisY + 53);
            e.Graphics.DrawString($"Tedarikçi Kodu".ToUpper(), _font7, _brushBlack, 38, axisY + 53);

            e.Graphics.DrawString($"Adet".ToUpper(), _font7, _brushBlack, 52, axisY + 53);
            e.Graphics.DrawString($"Ürün".ToUpper(), _font7, _brushBlack, 60, axisY + 53);
            e.Graphics.DrawString($"B.F.".ToUpper(), _font7, _brushBlack, 96, axisY + 53);
            e.Graphics.DrawString($"K.O.".ToUpper(), _font7, _brushBlack, 110, axisY + 53);
            e.Graphics.DrawString($"K.T.".ToUpper(), _font7, _brushBlack, 120, axisY + 53);
            e.Graphics.DrawString($"M.H.T.".ToUpper(), _font7, _brushBlack, 130, axisY + 53);

            /* Product */
            var productYAxis = axisY + 56;

            var orderDetails = orders
                .SelectMany(o => o.OrderDetails)
                .GroupBy(od => new
                {
                    od.ProductInformationId,
                    od.UnitPrice,
                    od.VatRate
                })
                .Select(god => new OrderDetail
                {
                    ProductInformationId = god.Key.ProductInformationId,
                    UnitPrice = god.Key.UnitPrice,
                    VatRate = god.Key.VatRate,
                    Quantity = god.Sum(od => od.Quantity),

                    ListUnitPrice = god.First().ListUnitPrice,
                    Product = god.First().Product,
                    Payor = god.First().Payor,
                    UnitCost = god.First().UnitCost,
                    VatExcListPrice = god.First().VatExcListPrice,
                    VatExcUnitCost = god.First().VatExcUnitCost,
                    VatExcUnitDiscount = god.First().VatExcUnitDiscount,
                    VatExcUnitPrice = god.First().VatExcUnitPrice
                })
                .ToList();

            for (int i = 0; i < orderDetails.Count; i++)
            {
                var orderDetail = orderDetails[i];

                var productName = orderDetail.Product.ProductInformation.Name;
                if (productName.Length > 30)
                    productName = productName.Substring(0, 30);

                e.Graphics.DrawString($"{orderDetail.Product.ProductInformation.ProductInformationUUId.Split("-")[0]}", _font7, _brushBlack, 4, productYAxis);
                e.Graphics.DrawString($"{orderDetail.Product.ProductInformation.ProductInformationUUId.Split("-")[1]}", _font7, _brushBlack, 12, productYAxis);
                
                e.Graphics.DrawString($"{orderDetail.Product.ProductInformation.Barcode}", _font7, _brushBlack, 24, productYAxis);
                e.Graphics.DrawString($"33638", _font7, _brushBlack, 38, productYAxis);

                e.Graphics.DrawString($"{orderDetail.Quantity}", _font7, _brushBlack, 52, productYAxis);
                e.Graphics.DrawString($"{productName}".ToUpper(), _font7, _brushBlack, 60, productYAxis);
                e.Graphics.DrawString($"{orderDetail.VatExcUnitPrice.ToString("N2")}", _font7, _brushBlack, 96, productYAxis);
                e.Graphics.DrawString($"{(orderDetail.VatRate * 100).ToString("N2")}", _font7, _brushBlack, 110, productYAxis);
                e.Graphics.DrawString($"{((orderDetail.UnitPrice - orderDetail.VatExcUnitPrice) * orderDetail.Quantity).ToString("N2")}", _font7, _brushBlack, 120, productYAxis);
                e.Graphics.DrawString($"{(orderDetail.VatExcUnitPrice * orderDetail.Quantity).ToString("N2")}", _font7, _brushBlack, 130, productYAxis);

                productYAxis += 3;
            }

            var cargoFee = orders.Sum(o => o.CargoFee);
            var cargoFeeExcTotal = (cargoFee / 1.18m);

            var surchargeFee = orders.Sum(o => o.SurchargeFee);
            var surchargeFeeExcTotal = (order.SurchargeFee / 1.18m);

            var amount = orders.Sum(o => o.Amount);

            axisY = productYAxis + 10;

            #region Invoice Informations
            e.Graphics.DrawString($@"KARGO UCRETI: {cargoFeeExcTotal.ToString("N2")} KDV(%18)", _font7, _brushBlack, 5, axisY + 3);


            e.Graphics.DrawString($@"KAPIDA ODEME UCRETI: {(surchargeFeeExcTotal).ToString("N2")} KDV(%18)", _font7, _brushBlack, 5, axisY + 6);
            #endregion

            axisY = productYAxis + 20;

            e.Graphics.DrawString($"MAL HİZMET TOPLAM TUTAR: {orderDetails.Sum(od => od.VatExcUnitPrice * od.Quantity).ToString("N2")}".ToUpper(), _font7, _brushBlack, 5, axisY + 9);
            e.Graphics.DrawString($"TOPLAM İSKONTO: 0".ToUpper(), _font7, _brushBlack, 5, axisY + 12);

            e.Graphics.DrawString($"HESAPLANAN KDV: {((cargoFee - cargoFeeExcTotal) + (surchargeFee - surchargeFeeExcTotal) + orderDetails.Sum(od => (od.UnitPrice - od.VatExcUnitPrice) * od.Quantity)).ToString("N2")}".ToUpper(), _font7, _brushBlack, 5, axisY + 15);

            e.Graphics.DrawString($"VERGİLER DAH. TOPLAM TUTAR: {amount.ToString("N2")}".ToUpper(), _font7, _brushBlack, 5, axisY + 18);

            e.Graphics.DrawString($"ÖDENECEK TUTAR: {amount.ToString("N2")}".ToUpper(), _font7, _brushBlack, 5, axisY + 21);

            e.Graphics.DrawString($"İRSALİYE YERİNE GEÇER.".ToUpper(), _font7, _brushBlack, 5, axisY + 24);
            e.Graphics.DrawString($"E-ARŞİV İZNİ KAPSAMINDA ELEKTRONİK ORTAMDA İLETİLMİŞTİR.".ToUpper(), _font7, _brushBlack, 5, axisY + 27);
        }
        #endregion
    }
}
