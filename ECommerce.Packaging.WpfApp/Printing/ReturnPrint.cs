﻿using ECommerce.Packaging.WpfApp.DataTransferObjects;
using ECommerce.Packaging.WpfApp.Printing.Base;
using Helpy.Shared.Print;
using Helpy.Shared.Print.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Media;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class ReturnPrint : PrintBase<Order>
    {
        #region Fields
        private string _companyInfo;

        private readonly string _fontFamily = "Barlow Condensed";

        #region Property
        private readonly Font _font = new Font("Barlow Condensed", 7, FontStyle.Regular);

        private readonly System.Drawing.Brush _brush = System.Drawing.Brushes.Black;

        public bool _returned;

        public string _description;

        public string _paymentSource;
        #endregion
        #endregion

        #region Constructors
        public ReturnPrint(bool returned, string description, string paymentSource, string printerName) : base(printerName)
        {
            this._returned = returned;
            this._description = description;
            this._paymentSource = paymentSource;
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(Order order)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(Order order, PrintPageEventArgs e)
        {
            var globalVatRate = 0.18M;
            var globalVatRateString = "18";

            var firstOrderDetail = order.OrderDetails.FirstOrDefault();
            if (firstOrderDetail != null)
            {
                var _8 = Math.Round(firstOrderDetail.UnitPrice / 1.08M, 4) == firstOrderDetail.VatExcUnitPrice;
                var _18 = Math.Round(firstOrderDetail.UnitPrice / 1.18M, 4) == firstOrderDetail.VatExcUnitPrice;

                if (!_8 && !_18)
                {
                    globalVatRate = 0.2M;
                    globalVatRateString = "20";
                }
            }

            Int32 pageWidth = 70;
            Int32 bodyFontSize = 7;
            Int32 subHeaderFontSize = 10;
            Int32 headerFontSize = 12;

            var returnTotal = 0m;

            Page page = new(new List<Element>
            {
                new Row(pageWidth, 6, new List<Element>()
                {
                    new Cell(pageWidth, 6, new Text(pageWidth, 6)
                    {
                        AlignCenter = true,
                        Value = "GİDER PUSULASI",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                }),
                /* Invoice Table */
                /* Invoice Date */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "GP NUMARASI",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.OrderBilling.ReturnNumber,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Invoice Number */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "FATURA NO",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.OrderBilling.InvoiceNumber,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Created Hour */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "İADE GELİŞ TARİHİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Order Date */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "İADE NEDENİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = this._description,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Order Id */
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "KARGO FİRMASI",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.OrderShipments[0].ShipmentCompany.Name,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "ÖDEME TİPİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = order.Payments[0].PaymentTypeName,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                new Row(pageWidth, 5, new List<Element>()
                {
                    new Cell(38, 5, new Text(38, 5)
                    {
                        Value = "WEB ADRESİ",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    },
                    new Cell(32, 5, new Text(32, 5)
                    {
                        Value = this._paymentSource,
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true
                    }
                }),
                /* Customer */
                new Row(pageWidth, 8, new List<Element>()
                {
                    new Cell(pageWidth, 8, new Text(pageWidth, 8)
                    {
                        Value = order.OrderDeliveryAddress.Recipient,
                        FontFamily = _fontFamily,
                        FontSize = headerFontSize,
                        Bold = true
                    })
                }),
                /* Product Lists */
                /* Header */
                new Row(pageWidth, 6, new List<Element>()
                {
                    new Cell(pageWidth, 6, new Text(pageWidth, 6)
                    {
                        AlignCenter = true,
                        Value = "ÜRÜN BİLGİLERİ",
                        FontFamily = _fontFamily,
                        FontSize = subHeaderFontSize,
                        Bold = true
                    })
                }),
            });



            /* Order Details */
            foreach (var orderDetail in order.OrderDetails.Where(cod => cod.Quantity != 0))
            {
                var orderReturnDetail = order.OrderReturnDetails.FirstOrDefault(x => x.ProductInformationId == orderDetail.ProductInformationId);

                if (orderReturnDetail == null) continue;

                returnTotal += orderDetail.UnitPrice * (decimal)orderReturnDetail.Quantity;

                var total = (orderDetail.UnitPrice * (decimal)(1 + orderDetail.VatRate));
                var productName = orderDetail.Product.ProductInformation.Name;

                var color = orderDetail.Product.ProductInformation.ProductInformationVariants.FirstOrDefault(x => x.VariantValue.VariantName == "Renk")?.VariantValue.Name;
                var size = orderDetail.Product.ProductInformation.ProductInformationVariants.FirstOrDefault(x => x.VariantValue.VariantName == "Beden")?.VariantValue.Name;

                page.Elements.Add(new Row(pageWidth, 5, new()
                {
                    new Cell(5, 5, new Text(5, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = orderDetail.Quantity.ToString(),
                        FontSize = bodyFontSize,
                        AlignCenter = true
                    })
                    {
                        Border = true,
                    },
                    new Cell(20, 5, new Text(20, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = orderDetail.Product.ProductInformation.Name,
                        FontSize = bodyFontSize
                    })
                    {
                        Border = true,
                    },
                    new Cell(10, 5, new Text(10, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = color,
                        FontSize = bodyFontSize,
                        AlignCenter = true
                    })
                    {
                        Border = true,
                    },
                    new Cell(10, 5, new Text(10, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = size,
                        FontSize = bodyFontSize,
                        AlignCenter = true
                    })
                    {
                        Border = true,
                    },
                    new Cell(10, 5, new Text(15, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = $"₺ {orderDetail.VatExcUnitPrice:N2}",
                        FontSize = bodyFontSize,
                        AlignCenter=true
                    })
                    {
                        Border = true,
                    },
                    new Cell(10, 5, new Text(15, 5)
                    {
                        FontFamily = _fontFamily,
                        Value = $"₺ {orderDetail.VatRate}",
                        FontSize = bodyFontSize,
                        AlignCenter=true
                    })
                    {
                        Border = true,
                    }
                }));
            }

            var cargoFeeExcTotal = (order.CargoFee / 1.18m);
            var surchargeFeeExcTotal = (order.SurchargeFee / 1.18m);

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $@"KARGO UCRETI: ₺{cargoFeeExcTotal:N2} KDV (%{globalVatRateString})",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $@"KARGO ÖDEME ÜCRETİ: ₺{0:N2} KDV (%{globalVatRateString})",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            page.Elements.Add(new Row(pageWidth, 5, new List<Element>
            {
                new Cell(pageWidth, 5, new Text(pageWidth, 5)
                {
                    Value = $@"İADE TOPLAMI: ₺{returnTotal:N2} KDV (%{globalVatRateString})",
                    FontFamily = _fontFamily,
                    FontSize = bodyFontSize
                })
            }));

            /* Order Id Barcode */
            page.Elements.Add(new Row(pageWidth, 10, new List<Element>
            {
                new Cell(pageWidth, 10, new Barcode(pageWidth, 10)
                {
                    Value = order.Id.ToString()
                })
            }));

            /* Order Id Barcode Text */
            page.Elements.Add(new Row(pageWidth, 6, new List<Element>
            {
                new Cell(pageWidth, 6, new Text(pageWidth, 6)
                {
                    Value = order.Id.ToString(),
                    Bold = true,
                    FontFamily = _fontFamily,
                    FontSize = subHeaderFontSize
                })
            }));

            if (order.OrderSource.Name == "Web")
                page.Elements.Add(new Row(pageWidth, 5, new List<Element>
                {
                    new Cell(pageWidth, 5, new Text(pageWidth, 5)
                    {
                        Value = "Lütfen müşteriye para iadesi yapınız.",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                }));

            if (order.OrderSource.Name == "Pazaryeri")
                page.Elements.Add(new Row(pageWidth, 5, new List<Element>
                {
                    new Cell(pageWidth, 5, new Text(pageWidth, 5)
                    {
                        Value = "Lütfen iadeyi pazaryeri panelinden kontrol ediniz.",
                        FontFamily = _fontFamily,
                        FontSize = bodyFontSize
                    })
                }));

            page.Print(e, 0, 0);








            //Int32 y = 5;

            //e.Graphics.DrawString("GİDER PUSULASI", _font, _brush, new RectangleF(39, y, 40, 30));

            //y += 20;

            //e.Graphics.DrawString($"GP NUMARASI: {order.OrderBilling.ReturnNumber}", _font, _brush, 5, y);

            //y += 5;

            //e.Graphics.DrawString($"FATURA NUMARASI: {order.OrderBilling.InvoiceNumber}", _font, _brush, 5, y);

            //y += 5;

            //e.Graphics.DrawString($"İADE GELİŞ TARİHİ: {DateTime.Now.ToString("yyyy-MM-dd")}", _font, _brush, 5, y);

            //y += 5;

            //e.Graphics.DrawString($"MUSTERI {order.Customer.Name.ToUpper()} {order.Customer.Surname}", _font, _brush, 5, y);

            //y += 20;

            //e.Graphics.DrawString($"ADET", _font, _brush, 5, y);
            //e.Graphics.DrawString($"ÜRÜN", _font, _brush, 10, y);
            //e.Graphics.DrawString($"RENK", _font, _brush, 50, y);
            //e.Graphics.DrawString($"BEDEN", _font, _brush, 70, y);
            //e.Graphics.DrawString($"FİYAT", _font, _brush, 80, y);
            //e.Graphics.DrawString($"KDV", _font, _brush, 90, y);
            //y += 5;

            //var retunTotal = 0m;


            //foreach (var orderDetail in order.OrderDetails.Where(cod => cod.Quantity != 0))
            //{

            //    var orderReturnDetail = order.OrderReturnDetails.FirstOrDefault(x => x.ProductInformationId == orderDetail.ProductInformationId);

            //    if (orderReturnDetail == null) continue;

            //    var total = (orderDetail.UnitPrice * (decimal)(1 + orderDetail.VatRate));
            //    var productName = orderDetail.Product.ProductInformation.Name;

            //    var color = orderDetail.Product.ProductInformation.ProductInformationVariants.FirstOrDefault(x => x.VariantValue.VariantName == "Renk")?.VariantValue.Name;
            //    var size = orderDetail.Product.ProductInformation.ProductInformationVariants.FirstOrDefault(x => x.VariantValue.VariantName == "Beden")?.VariantValue.Name;

            //    e.Graphics.DrawString(orderReturnDetail.Quantity.ToString(), _font, _brush, 5, y);
            //    e.Graphics.DrawString(productName.Length <= 25 ? productName : productName.Substring(0, 25), _font, _brush, 10, y);
            //    e.Graphics.DrawString(color, _font, _brush, 50, y);
            //    e.Graphics.DrawString(size, _font, _brush, 70, y);
            //    e.Graphics.DrawString(orderDetail.VatExcUnitPrice.ToString("N2"), _font, _brush, 80, y);
            //    e.Graphics.DrawString(orderDetail.VatRate.ToString(), _font, _brush, 90, y);
            //    retunTotal += orderDetail.UnitPrice * (decimal)orderReturnDetail.Quantity;
            //    y += 5;
            //}

            //y += 30;

            //e.Graphics.DrawString($"KARGO UCRETI: {order.CargoFee}", _font, _brush, 5, y);

            //y += 5;

            //e.Graphics.DrawString($"KAPIDA ODEME UCRETI: 0", _font, _brush, 5, y);

            //y += 5;


            //if (!this._returned)
            //{
            //    retunTotal += order.SurchargeFee + order.CargoFee;
            //}

            //e.Graphics.DrawString($"İADE TOPLAMI: {retunTotal.ToString("N2")}", _font, _brush, 5, y);

            //y += 5;

            //e.Graphics.DrawString($"KARGO FİRMASI: {order.OrderShipments[0].ShipmentCompany.Name}", _font, _brush, 5, y);

            //y += 5;

            //if (order.Payments.Count > 0)
            //    e.Graphics.DrawString($"ODEME TIPI: {order.Payments[0].PaymentTypeName}", _font, _brush, 5, y);


            //y += 5;

            //e.Graphics.DrawString($"WEB ADRESI: {this._paymentSource}", _font, _brush, 5, y);

            //y += 30;

            //e.Graphics.DrawString($"IADE NEDENI: {this._description}", _font, _brush, 5, y);



            //#region
            //#region Barcode
            //y += 30;
            //e.Graphics.DrawString($@"SIPARIS NUMARASI", _font, _brush, new RectangleF(5, y, 100, 20));
            //y += 5;
            //var image = BarcodeDrawFactory.Code128WithChecksum.Draw(order.MarketPlaceOrderNumber, 20);
            //var bitmap = new Bitmap(image.Width, image.Height);

            //using (Graphics graphics = Graphics.FromImage(bitmap))
            //{
            //    //graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //    //graphics.SmoothingMode = SmoothingMode.HighQuality;
            //    //graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
            //    //graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            //    graphics.DrawImage(image, 0, 0);
            //}

            //e.Graphics.DrawImage(bitmap, 5, y, 50, 20);

            //y += 20;

            //e.Graphics.DrawString(order.MarketPlaceOrderNumber, _font, _brush, new RectangleF(5, y, 50, 20), new StringFormat(StringFormatFlags.NoClip) { Alignment = StringAlignment.Center });
            //#endregion
            //#endregion

        }
        #endregion
    }
}