﻿using ECommerce.Packaging.WpfApp.Printing.Base;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class Base64Print : PrintBase<string>
    {
        #region Fields
        private string _companyInfo;
        #endregion

        #region Constructors
        public Base64Print(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void PrintConcreate(string base64)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        }

        protected override void Print(string base64, PrintPageEventArgs e)
        {
            var bytes = Convert.FromBase64String(base64);
            Image image;
            using (MemoryStream memoryStream = new MemoryStream(bytes))
            {
                image = Image.FromStream(memoryStream);
            }
            e.Graphics.DrawImage(image, new RectangleF(0, 0, 100, 135), new RectangleF(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
        }
        #endregion
    }
}
