﻿using ECommerce.Packaging.WpfApp.Printing.Base;
using System;
using System.Drawing;
using System.Drawing.Printing;
using Zen.Barcode;
using ECommerce.Packaging.WpfApp.DataTransferObjects;

namespace ECommerce.Packaging.WpfApp.Printing
{
    public class OrderNumberPrint : PrintBase<Order>
    {
        #region Constructors
        public OrderNumberPrint(string printerName) : base(printerName)
        {
        }
        #endregion

        #region Methods
        protected override void Print(Order order, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(order.Id.ToString(), _font12, _brushBlack, 5, 5);

            var barcodeImage = BarcodeDrawFactory.Code128WithChecksum.Draw(order.Id.ToString(), 20);
            var barcodeBitmap = new Bitmap(barcodeImage.Width, barcodeImage.Height);
            using (Graphics graphics = Graphics.FromImage(barcodeBitmap))
            {
                graphics.DrawImage(barcodeImage, 0, 0);
            }

            e.Graphics.DrawImage(barcodeBitmap, 5, 10, 50F, 20F);
        }

        protected override void PrintConcreate(Order t)
        {
            try
            {
                base._printDocument.Print();
            }
            catch (Exception exception)
            {
            }
        } 
        #endregion
    }
}
