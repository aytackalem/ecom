﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class TextileWindow : Window
    {
        #region Constructors
        public TextileWindow(TextileWindowViewModel textileViewModel)
        {
            InitializeComponent();

            DataContext = textileViewModel;
        }
        #endregion
    }
}
