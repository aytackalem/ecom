﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Packaging.WpfApp.Finders;
using ECommerce.Packaging.WpfApp.Services;
using ECommerce.Packaging.WpfApp.ViewModels;
using Helpy.Shared.Http.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class App : System.Windows.Application
    {
        #region Properties
        public IServiceProvider ServiceProvider { get; private set; }

        public IConfiguration Configuration { get; private set; }
        #endregion

        #region Methods
        private void ConfigureServices(IServiceCollection services)
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            Configuration = configurationBuilder.Build();

            services.AddSingleton<ICompanyFinder, CompanyFinder>();
            services.AddSingleton<IDomainFinder, DomainFinder>();

            services.AddScoped<IdentityService>();
            services.AddScoped<AccountingService>();
            services.AddScoped<AccountingCompanyConfigurationService>();
            services.AddScoped<CompanyConfigurationService>();
            services.AddScoped<CompanyService>();
            services.AddScoped<DomainService>();
            services.AddScoped<MarketplaceService>();
            services.AddScoped<OrderInvoiceInformationService>();
            services.AddScoped<OrderService>();
            services.AddScoped<OrderShipmentService>();
            services.AddScoped<OrderSourceService>();
            services.AddScoped<ShipmentCompanyCompanyService>();
            services.AddScoped<OrderReturnService>();

            services.AddHttpHelper(ServiceLifetime.Singleton);

            services.AddSingleton<IConfiguration>(Configuration);

            if (Configuration["Mode"] == "Textile")
            {
                services.AddTransient(typeof(TextileWindowViewModel));
                services.AddTransient(typeof(TextileWindow));
            }
            else if (Configuration["Mode"] == "TextileReturn")
            {
                services.AddTransient(typeof(TextileReturnWindowViewModel));
                services.AddTransient(typeof(TextileReturnWindow));
            }
            else if (Configuration["Mode"] == "TextileBulkInvoicing")
            {
                services.AddTransient(typeof(TextileBulkInvoicingViewModel));
                services.AddTransient(typeof(TextileBulkInvoicing));
            }
            else if (Configuration["Mode"] == "TextileTYBulkInvoicing")
            {
                services.AddTransient(typeof(TextileTYBulkInvoicingViewModel));
                services.AddTransient(typeof(TextileTYBulkInvoicing));
            }
            else if (Configuration["Mode"] == "Retail")
            {
                services.AddTransient(typeof(RetailWindowViewModel));
                services.AddTransient(typeof(RetailWindow));
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var serviceCollection = new ServiceCollection();

            ConfigureServices(serviceCollection);

            ServiceProvider = serviceCollection.BuildServiceProvider();

            if (Configuration["Mode"] == "Textile")
            {
                var retailWindow = ServiceProvider.GetRequiredService<TextileWindow>();
                retailWindow.Show();
            }
            else if (Configuration["Mode"] == "TextileReturn")
            {
                var textileWindow = ServiceProvider.GetRequiredService<TextileReturnWindow>();
                textileWindow.Show();
            }
            else if (Configuration["Mode"] == "TextileBulkInvoicing")
            {
                var textileWindow = ServiceProvider.GetRequiredService<TextileBulkInvoicing>();
                textileWindow.Show();
            }
            else if (Configuration["Mode"] == "TextileTYBulkInvoicing")
            {
                var textileWindow = ServiceProvider.GetRequiredService<TextileTYBulkInvoicing>();
                textileWindow.Show();
            }
            else if (Configuration["Mode"] == "Retail")
            {
                var textilereturnwindow = ServiceProvider.GetRequiredService<RetailWindow>();
                textilereturnwindow.Show();
            }
        }
        #endregion
    }
}