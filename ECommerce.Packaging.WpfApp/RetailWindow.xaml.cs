﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows;

namespace ECommerce.Packaging.WpfApp
{
    public partial class RetailWindow : Window
    {
        #region Constructors
        public RetailWindow(RetailWindowViewModel retailViewModel)
        {
            InitializeComponent();

            DataContext = retailViewModel;
        } 
        #endregion
    }
}
