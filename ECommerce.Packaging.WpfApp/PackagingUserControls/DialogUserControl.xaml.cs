﻿using ECommerce.Packaging.WpfApp.ViewModels;
using System.Windows.Controls;

namespace ECommerce.Packaging.WpfApp.PackagingUserControls
{
    public partial class DialogUserControl : UserControl
    {
        public DialogUserControl(DialogViewModel dialogViewModel)
        {
            InitializeComponent();

            DataContext = dialogViewModel;
        }
    }
}
