﻿using Helpy.Responses;
using MediatR.Pipeline;

namespace Helpy.Behaviours
{
    public class ExceptionBehaviour<TRequest, TResponse, TException> : IRequestExceptionHandler<TRequest, TResponse, TException>
        where TRequest : notnull
        where TResponse : notnull, Response//, new()
        where TException : Exception
    {
        public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<TResponse> state, CancellationToken cancellationToken)
        {
            //var response = new TResponse
            //{
            //    Code = ResponseCodes.Error,
            //    Message = "Bilinmeyen bir hata olustu."
            //};

            var response = (TResponse)new Response(ResponseCodes.Error, "Bilinmeyen bir hata oluştu.");

            state.SetHandled(response);

            return Task.FromResult(response);
        }
    }
}
