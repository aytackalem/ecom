﻿using FluentValidation;
using Helpy.Responses;
using MediatR;

namespace Helpy.Behaviours
{
    public class ValidationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : notnull
        where TResponse : notnull, Response//, new()
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationBehaviour(IEnumerable<IValidator<TRequest>> validators)
        {
            this._validators = validators;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            if (this._validators.Any())
            {
                var context = new ValidationContext<TRequest>(request);
                var validationResults = await Task.WhenAll(
                    _validators.Select(v => v.ValidateAsync(context, cancellationToken)));

                var failures = validationResults
                    .Where(r => r.Errors.Any())
                    .SelectMany(r => r.Errors)
                    .ToList();

                if (failures.Any())
                    return (TResponse)new Response(ResponseCodes.Bad, string.Join("", failures.Select(f => f.ErrorMessage)));
                    //return new TResponse()
                    //{
                    //    Code = ResponseCodes.Bad,
                    //    Message = string.Join("", failures.Select(f => f.ErrorMessage))
                    //};
            }

            return await next();
        }
    }
}
