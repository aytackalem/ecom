﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Text;

namespace ECommerce.Mikro.Stock.Job
{
    public class App : IApp
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;

        private readonly IAccountingProvider _accountingService;
        #endregion

        #region Constructors
        public App(IUnitOfWork unitOfWork, IAccountingProvider accountingService)
        {
            #region Fields
            this._unitOfWork = unitOfWork;
            this._accountingService = accountingService;
            #endregion
        }
        #endregion

        #region Methods
        public void Run()
        {
            var lastStockTransferOrderIdResponse = this._accountingService.ReadLastStockTransfer();
            if (!lastStockTransferOrderIdResponse.Success)
                return;

            var lastStockTransferOrderId = lastStockTransferOrderIdResponse.Data;

#warning Stok dusulebilir kaynaklar veya siparise stoktan dus koyulabilir
            var orderIds = this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Where(o => o.Id > lastStockTransferOrderId &&
                            o.OrderTypeId != OrderTypes.Iptal &&
                            o.OrderTypeId != OrderTypes.Tamamlandi &&
                            (
                                o.OrderSourceId == OrderSources.Pazaryeri ||
                                o.OrderSourceId == 6 ||
                                o.OrderSourceId == 7 ||
                                o.OrderSourceId == 8
                            )
                      )
                .Select(o => o.Id)
                .ToList();

            Console.WriteLine(orderIds.Count);
            int counter = 0;
            foreach (var oiLoop in orderIds)
            {
                counter++;
                Console.WriteLine(counter);

                var stockTransferResponse = this._accountingService.CreateStockTransfer(oiLoop);
                if (!stockTransferResponse.Success)
                    break;
            }
            
            /* ------------------------------- Cancel -------------------------------------------- */
            var dataResponse = this._accountingService.ReadStockTransfers();
            if (!dataResponse.Success)
                return;

            Console.WriteLine($"Waiting stock: {dataResponse.Data.Count}");

            counter = 0;
            dataResponse.Data.ForEach(d =>
            {
                counter++;
                Console.WriteLine(counter);

                var order = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.OrderDetails)
                    .ThenInclude(od => od.ProductInformation)
                    .FirstOrDefault(o => o.Id == d);

                if (order?.OrderTypeId == "IP")
                {
                    var response = this._accountingService.DeleteStockTransfer(d);
                    if (response.Success)
                    {
                        Console.WriteLine($"{counter} removed.");

                        //order
                        //    .OrderDetails
                        //    .ForEach(od =>
                        //    {
                        //        File.AppendAllLines($@"D:\Invoices-Sinoz-{DateTime.Now.ToString("yyyy-MM-dd")}.txt", new List<string> { $"{od.ProductInformation.StockCode};{od.Quantity}" });
                        //    });
                    }
                }
            });
        }
        #endregion
    }
}
