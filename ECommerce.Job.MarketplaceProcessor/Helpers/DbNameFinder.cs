﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Job.MarketplaceProcessor.Helpers
{
    public class DbNameFinder : IDbNameFinder
    {
        #region Fields
        private string _dbName;
        #endregion

        #region Methods
        public string FindName()
        {
            return this._dbName;
        }

        public void Set(string dbName)
        {
            this._dbName = dbName;
        } 
        #endregion
    }
}
