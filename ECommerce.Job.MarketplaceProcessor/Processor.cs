﻿using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor;
using ECommerce.Application.Common.Parameters.MarketplacesV2.Processor;

namespace ECommerce.Job.MarketplaceProcessor
{
    public class Processor
    {
        #region Fields
        private readonly MarketplaceRequestProcessorResolver _marketplaceRequestProcessorResolver;
        #endregion

        #region Constructors
        public Processor(MarketplaceRequestProcessorResolver marketplaceRequestProcessorResolver)
        {
            #region Fields
            this._marketplaceRequestProcessorResolver = marketplaceRequestProcessorResolver;
            #endregion
        }
        #endregion

        #region Methods
        public async Task RunAsync(string[] args)
        {
            var queueId = int.Parse(args[0]);
            var requestType = args[1];
            var marketplaceId = args[2];
            var trackable = bool.Parse(args[3]);

            await Console.Out.WriteLineAsync("Task started...");
            await Console.Out.WriteLineAsync($"Queue:\t{queueId}");
            await Console.Out.WriteLineAsync($"Task:\t{requestType}");
            await Console.Out.WriteLineAsync($"Marketplace:\t{marketplaceId}");
            await Console.Out.WriteLineAsync($"Trackable:\t{trackable}");

            var marketplaceRequestProcessor = this._marketplaceRequestProcessorResolver(requestType, trackable);

            await Console.Out.WriteLineAsync(marketplaceRequestProcessor is null ? "Processor not found!" : "Processor found...");

            if (marketplaceRequestProcessor is null)
            {
                return;
            }

            await marketplaceRequestProcessor.ProcessAsync(new MarketplaceRequestProcessorRequest
            {
                QueueId = queueId,
                MarketplaceId = marketplaceId
            });
        }
        #endregion
    }
}
