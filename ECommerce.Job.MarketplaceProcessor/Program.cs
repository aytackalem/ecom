﻿using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.MarketplacesV2;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor;
using ECommerce.Application.Common.Interfaces.MarketplacesV2.Processor.Base;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Infrastructure;
using ECommerce.Job.MarketplaceProcessor.Helpers;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.MarketplacesV2;
using ECommerce.Persistence.Common.MarketplacesV2.Processor;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace ECommerce.Job.MarketplaceProcessor
{
    internal class Program
    {
        #region Methods
        static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Process.GetCurrentProcess().MainModule.FileName).FullName)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: true,
                    reloadOnChange: true).Build();
        }

        static IServiceProvider ConfigureServiceCollection(IConfigurationRoot configuration)
        {
            ServiceCollection serviceCollection = new();

            serviceCollection.AddSingleton<IConfiguration>(p => configuration);

            serviceCollection.AddSingleton<IDbService, DbService>();

            serviceCollection.AddDbContext<ApplicationDbContext>();


            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<ITenantFinder>(tf => null);

            serviceCollection.AddScoped<IDomainFinder>(df => null);

            serviceCollection.AddScoped<ICompanyFinder>(cf => null);

            serviceCollection.AddScoped<IDbNameFinder, DbNameFinder>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<IHttpHelperV2, HttpHelperV2>();

            serviceCollection.AddScoped<IHttpHelperV3, HttpHelperV3>();

            #region Marketplace Request Processor Injections
            serviceCollection.AddTransient<MarketplaceRequestProcessorTrackableCreateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorUntrackableCreateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorTrackableUpdateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorUntrackableUpdateProduct>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorTrackableUpdatePrice>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorUntrackableUpdatePrice>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorTrackableUpdateStock>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorUntrackableUpdateStock>();

            serviceCollection.AddTransient<MarketplaceRequestProcessorResolver>(serviceProvider => (requestType, trackable) =>
            {
                IMarketplaceRequestProcessor p = requestType switch
                {
                    "CreateProduct" => trackable
                        ? serviceProvider.GetService<MarketplaceRequestProcessorTrackableCreateProduct>()
                        : serviceProvider.GetService<MarketplaceRequestProcessorUntrackableCreateProduct>(),
                    "UpdateProduct" => trackable
                        ? serviceProvider.GetService<MarketplaceRequestProcessorTrackableUpdateProduct>()
                        : serviceProvider.GetService<MarketplaceRequestProcessorUntrackableUpdateProduct>(),
                    "UpdatePrice" => trackable
                        ? serviceProvider.GetService<MarketplaceRequestProcessorTrackableUpdatePrice>()
                        : serviceProvider.GetService<MarketplaceRequestProcessorUntrackableUpdatePrice>(),
                    "UpdateStock" => trackable
                        ? serviceProvider.GetService<MarketplaceRequestProcessorTrackableUpdateStock>()
                        : serviceProvider.GetService<MarketplaceRequestProcessorUntrackableUpdateStock>(),
                    _ => null
                };
                return p;
            });
            #endregion

            serviceCollection.AddTransient<IMarketplaceRequestReader, MarketplaceRequestReader>();

            serviceCollection.AddTransient<IMarketplaceRequestWriter, MarketplaceRequestWriter>();

            serviceCollection.AddScoped<IMarketplaceErrorFacade, MarketplaceErrorFacade>();

            serviceCollection.AddInfrastructureServices(configuration);

            serviceCollection.AddScoped<Processor>();

            return serviceCollection.BuildServiceProvider();
        }

        static async Task Main(string[] args)
        {
            #region Checks
#if DEBUG
            args = new string[] { "1", "UpdateStock", "BY", "true" };
#endif

            if (args.Length < 3)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Processor", "Please check jos argumants.", new string[] { });
                return;
            }

            if (int.TryParse(args[0], out int i) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Processor", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "UpdatePrice", "UpdateStock", "CreateProduct", "UpdateProduct" }.Contains(args[1]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Processor", "Please check jos argumants.", new string[] { });
                return;
            }

            if (new[] { "CS", "HB", "M", "TY", "TYE", "PTT", "PA", "N11", "IK", "IS", "MN", "ML", "N11V2", "TSoft", "LCW", "BY" }.Contains(args[2]) == false)
            {
                SystemAlertHelper.SendMail("Error From Marketplace Processor", "Please check jos argumants.", new string[] { });
                return;
            }
            #endregion

            var configurationRoot = GetConfiguration();
            var serviceProvider = ConfigureServiceCollection(configurationRoot);

            var dbNames = new List<string>();

            using (var scope = serviceProvider.CreateScope())
            {
                var dbService = scope.ServiceProvider.GetRequiredService<IDbService>();
                var getNamesResponse = await dbService.GetNamesAsync();
                if (getNamesResponse.Success == false)
                {
                    SystemAlertHelper.SendMail("Error From Marketplace Exporter", "Db names not fetched.");
                    return;
                }

                dbNames = getNamesResponse.Data;
            }

            if (dbNames.HasItem())
                foreach (var theDbName in dbNames)
                {
#if DEBUG
                    if (theDbName != "Mizalle")
                        continue;
#endif

                    using (var scope = serviceProvider.CreateScope())
                    {
                        var dbNameFinder = scope.ServiceProvider.GetRequiredService<IDbNameFinder>();
                        dbNameFinder.Set(theDbName);



                        await scope.ServiceProvider.GetRequiredService<Processor>().RunAsync(args);
                    }
                }
        }
        #endregion
    }
}