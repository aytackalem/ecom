﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.MarketPlace.Common.Response;
using System;

namespace ECommerce.MarketPlace.Operation
{
    public class OrderProxy : IOrderFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        private readonly ICacheHandler _cacheHandler;

        #endregion

        #region Constructors
        public OrderProxy(IHttpHelper httpHelper, ICacheHandler cacheHandler)
        {
            _httpHelper = httpHelper;
            this._cacheHandler = cacheHandler;
        }
        #endregion

        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                //orderRequest.MarketPlaceId
                var realOrderFacade = GetOrderFacade(orderRequest.MarketPlaceId);
                var realOrderResponse = realOrderFacade.Get(orderRequest);
                if (realOrderResponse.Success)
                {
                    response.Data = realOrderResponse.Data;
                    response.Success = realOrderResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });

        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                var realOrderFacade = GetOrderFacade(orderRequest.MarketPlaceId);
                var realOrderResponse = realOrderFacade.CheckPreparing(orderRequest);
                if (realOrderResponse.Success)
                {
                    response.Data = realOrderResponse.Data;
                    response.Message = realOrderResponse.Message;
                    response.Success = realOrderResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Bağlantı kurulamıyor.";
            });

        }

        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
           
                var realOrderFacade = GetOrderFacade(orderPackaging.MarketPlaceId);
                var realOrderResponse = realOrderFacade.OrderTypeUpdate(orderPackaging);
                if (realOrderResponse.Success)
                {
                    response.Message = realOrderResponse.Message;
                    response.Success = realOrderResponse.Success;
                }
         

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var realOrderFacade = GetOrderFacade(orderRequest.MarketPlaceId);
                var realOrderResponse = realOrderFacade.GetDelivered(orderRequest);
                if (realOrderResponse.Success)
                {
                    response.Data = realOrderResponse.Data;
                    response.Success = realOrderResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var realOrderFacade = GetOrderFacade(orderRequest.MarketPlaceId);
                var realOrderResponse = realOrderFacade.GetCancelled(orderRequest);
                if (realOrderResponse.Success)
                {
                    response.Data = realOrderResponse.Data;
                    response.Success = realOrderResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {
                var realOrderFacade = GetOrderFacade(orderRequest.MarketPlaceId);
                var realOrderResponse = realOrderFacade.GetReturned(orderRequest);
                if (realOrderResponse.Success)
                {
                    response.Data = realOrderResponse.Data;
                    response.Success = realOrderResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Helpers Methods
        private IOrderFacade GetOrderFacade(string marketPlaceId)
        {
            switch (marketPlaceId)
            {
                case "TY":
                    return new Trendyol.Operation.OrderOperation(_httpHelper);
                case "N11":
                    return new N11.Operation.OrderOperation();
                case "HB":
                    return new Hepsiburada.Operation.OrderOperation(_httpHelper);
                case "CS":
                    return new CicekSepeti.Operation.OrderOperation(_httpHelper);
                case "GG":
                    return new GittiGidiyor.Operation.OrderOperation();
                case "FI":
                    return new Fidanistanbul.Operation.OrderOperation();
                case "OMNI":
                    return new Omni.Operation.OrderOperation(_httpHelper);
                case "PA":
                    return new Pazarama.Operation.OrderOperation(_httpHelper, _cacheHandler);
                case "HOPI":
                    return new Hopi.Operation.OrderOperation(_httpHelper);
                case "VO":
                    return new Vodafone.Operation.OrderOperation(_httpHelper);
                default:
                    throw new Exception("");
            }
        }
        #endregion
    }
}