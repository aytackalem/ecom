﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;

namespace ECommerce.MarketPlace.Operation
{
    public class CategoryProxy : ICategoryFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        public readonly IMemoryCache _memoryCache;

        #endregion

        #region Constructors
        public CategoryProxy(IHttpHelper httpHelper, IMemoryCache memoryCache)
        {
            _httpHelper = httpHelper;
            _memoryCache = memoryCache;
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                //orderRequest.MarketPlaceId
                var realCategoryFacade = GetCategoryFacade(configurations["MarketPlaceId"]);
                var realCategoryResponse = realCategoryFacade.Get(configurations);
                if (realCategoryResponse.Success)
                {
                    response.Data = realCategoryResponse.Data;
                    response.Success = realCategoryResponse.Success;
                }

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Helpers Methods
        private ICategoryFacade GetCategoryFacade(string marketPlaceId)
        {
            switch (marketPlaceId)
            {
                case "TY":
                    return new Trendyol.Operation.CategoryOperation(_httpHelper);
                case "N11":
                    return new N11.Operation.CategoryOperation();
                case "HB":
                    return new Hepsiburada.Operation.CategoryOperation(_httpHelper, _memoryCache);
                case "CS":
                    return new CicekSepeti.Operation.CategoryOperation(_httpHelper);
                case "IK":
                    return new Ikas.Operation.CategoryOperation(_httpHelper);
                //case "OMNI":
                //    return new Omni.Operation.CategoryOperation(_httpHelper);
                case "PA":
                    return new Pazarama.Operation.CategoryOperation(_httpHelper);
                //case "HOPI":
                //    return new Hopi.Operation.CategoryOperation(_httpHelper);
                case "VO":
                    return new Vodafone.Operation.CategoryOperation(_httpHelper);
                default:
                    throw new Exception("");
            }
        }


        #endregion
    }
}
