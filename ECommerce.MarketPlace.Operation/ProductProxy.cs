﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace ECommerce.MarketPlace.Operation
{
    public class ProductProxy : IProductFacade
    {
        #region Fields
        public readonly IHttpHelper _httpHelper;

        public readonly IMemoryCache _memoryCache;

        private readonly ICacheHandler _cacheHandler;
        #endregion

        #region Constructors
        public ProductProxy(IHttpHelper httpHelper, IMemoryCache memoryCache, ICacheHandler cacheHandler)
        {
            this._httpHelper = httpHelper;
            this._memoryCache = memoryCache;
            this._cacheHandler = cacheHandler;
        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var realProductFacade = GetProductFacade(productRequest.MarketPlaceId);
                var realOrderResponse = realProductFacade.Create(productRequest);


                response.Success = realOrderResponse.Success;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var realProductFacade = GetProductFacade(stockRequest.MarketPlaceId);
                var realOrderResponse = realProductFacade.UpdateStock(stockRequest);


                response.Success = realOrderResponse.Success;

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var realProductFacade = GetProductFacade(stockRequest.MarketPlaceId);
                var realOrderResponse = realProductFacade.UpdatePrice(stockRequest);


                response.Success = realOrderResponse.Success;

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }
        #endregion

        #region Helpers Method

        private IProductFacade GetProductFacade(string marketPlaceId)
        {
            switch (marketPlaceId)
            {
                case "TY":
                    return new Trendyol.Operation.ProductOperation(_httpHelper);
                case "N11":
                    return new N11.Operation.ProductOperation();
                case "HB":
                    return new Hepsiburada.Operation.ProductOperation(_httpHelper, _memoryCache);
                case "CS":
                    return new CicekSepeti.Operation.ProductOperation(_httpHelper);
                case "GG":
                    return new GittiGidiyor.Operation.ProductOperation();
                case "OMNI":
                    return new Omni.Operation.ProductOperation(_httpHelper);
                case "PA":
                    return new Pazarama.Operation.ProductOperation(_httpHelper, _cacheHandler);
                case "IK":
                    return new Ikas.Operation.ProductOperation(_httpHelper);
                default:
                    throw new Exception("");
            }
        }

        #endregion

    }
}
