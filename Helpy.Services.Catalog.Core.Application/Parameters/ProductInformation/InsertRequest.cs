﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects;

namespace Helpy.Services.Catalog.Core.Application.Parameters.ProductInformation
{
    public class InsertRequest
    {
        #region Properties
        public string Name { get; set; }

        public string Type { get; set; }

        public int Stock { get; set; }

        public int VirtualStock { get; set; }

        public bool OnSale { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Navigation Properties
        public Label Label { get; set; }

        public Price Price { get; set; }

        public Shelf Shelf { get; set; }

        public List<DataTransferObjects.Variant> Variants { get; set; }
        #endregion
    }
}
