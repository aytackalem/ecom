﻿namespace Helpy.Services.Catalog.Core.Application.Parameters.Category
{
    public class InsertRequest
    {
        #region Properties
        public string Name { get; set; }

        public string Slug { get; set; }
        #endregion
    }
}
