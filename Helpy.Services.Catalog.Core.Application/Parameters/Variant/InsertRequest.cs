﻿namespace Helpy.Services.Catalog.Core.Application.Parameters.Variant
{
    public class InsertRequest
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
