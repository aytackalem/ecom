﻿namespace Helpy.Services.Catalog.Core.Application.Parameters.VariantValue
{
    public class InsertRequest
    {
        #region Properties
        public string VariantId { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
