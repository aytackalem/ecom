﻿namespace Helpy.Services.Catalog.Core.Application.Parameters.Product
{
    public class InsertRequest
    {
        #region Properties
        public string Name { get; set; }

        public string SellerCode { get; set; }

        public string Slug { get; set; }
        #endregion

        #region Navigation Properties
        public DataTransferObjects.Category Category { get; set; }

        public DataTransferObjects.Brand Brand { get; set; }

        public List<ProductInformation.InsertRequest> ProductInformations { get; set; }
        #endregion
    }
}
