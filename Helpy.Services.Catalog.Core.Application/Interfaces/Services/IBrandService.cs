﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Parameters.Brand;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Core.Application.Interfaces.Services
{
    public interface IBrandService
    {
        #region Methods
        Task<DataResponse<Brand>> InsertAsync(InsertRequest request); 
        #endregion
    }
}
