﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Parameters.Variant;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Core.Application.Interfaces.Services
{
    public interface IVariantService
    {
        #region Methods
        Task<DataResponse<Variant>> GetByIdAsync(string id);

        Task<DataResponse<Variant>> InsertAsync(InsertRequest request);

        Task<DataResponse<VariantValue>> InsertVariantValueAsync(Parameters.VariantValue.InsertRequest request);
        #endregion
    }
}
