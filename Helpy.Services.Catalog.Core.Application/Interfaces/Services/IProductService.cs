﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Parameters.Product;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Core.Application.Interfaces.Services
{
    public interface IProductService
    {
        #region Methods
        Task<DataResponse<Product>> GetByIdAsync(string id);

        Task<DataResponse<Product>> InsertAsync(InsertRequest request);
        #endregion
    }
}
