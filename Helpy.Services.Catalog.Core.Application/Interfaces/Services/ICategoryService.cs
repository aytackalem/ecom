﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Parameters.Category;
using Helpy.Shared.Response;

namespace Helpy.Services.Catalog.Core.Application.Interfaces.Services
{
    public interface ICategoryService
    {
        #region Methods
        Task<DataResponse<Category>> InsertAsync(InsertRequest request);
        #endregion
    }
}
