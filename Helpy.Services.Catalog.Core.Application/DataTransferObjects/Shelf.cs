﻿namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Shelf
    {
        #region Properties
        public string Zone { get; set; }

        public string Code { get; set; }
        #endregion
    }
}
