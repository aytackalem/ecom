﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base;

namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Category : DataTransferObjectBase
    {
        #region Properties
        public string Name { get; set; }

        public string Slug { get; set; }
        #endregion
    }
}
