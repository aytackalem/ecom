﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base;

namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Brand : DataTransferObjectBase
    {
        #region Properties
        public string Name { get; set; }

        public string Slug { get; set; }
        #endregion
    }
}
