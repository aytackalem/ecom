﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base;

namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Product : DataTransferObjectBase
    {
        #region Properties
        public string Name { get; set; }

        public string SellerCode { get; set; }

        public string Slug { get; set; }
        #endregion

        #region Navigation Properties
        public Category Category { get; set; }

        public Brand Brand { get; set; }

        public List<ProductInformation> ProductInformations { get; set; }
        #endregion
    }
}
