﻿namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base
{
    public abstract class DataTransferObjectBase
    {
        #region Properties
        public string Id { get; set; }
        #endregion
    }
}
