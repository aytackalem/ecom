﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base;

namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Variant : DataTransferObjectBase
    {
        #region Properties
        public string Name { get; set; }
        #endregion

        #region Navigation Properties
        public List<VariantValue> VariantValues { get; set; }
        #endregion
    }
}
