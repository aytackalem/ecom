﻿namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class Price
    {
        #region Properties
        public decimal ListUnitPrice { get; set; }

        public decimal UnitPrice { get; set; }
        #endregion
    }
}
