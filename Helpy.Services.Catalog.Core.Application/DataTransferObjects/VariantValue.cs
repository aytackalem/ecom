﻿using Helpy.Services.Catalog.Core.Application.DataTransferObjects.Base;

namespace Helpy.Services.Catalog.Core.Application.DataTransferObjects
{
    public class VariantValue : DataTransferObjectBase
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
