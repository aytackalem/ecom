﻿using Dapper;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ECommerce.Match
{
    #region TY
    class TYMarketplaceModel
    {
        public string Id { get; set; }

        public string StockCode { get; set; }

        public string CategoryCode { get; set; }

        public List<TYMarketplaceModelDetail> TYMarketplaceModelDetails { get; set; }
    }

    class TYMarketplaceModelDetail
    {
        public string CategoryCode { get; set; }

        public string AttributeValueCode { get; set; }

        public string AttributeValue { get; set; }
    }
    #endregion

    #region CS
    class CSMarketplaceModel
    {
        public string Id { get; set; }

        public string StockCode { get; set; }

        public string CategoryCode { get; set; }

        public List<CSMarketplaceModelDetail> CSMarketplaceModelDetails { get; set; }
    }

    class CSMarketplaceModelDetail
    {
        public string CategoryCode { get; set; }

        public string AttributeValueCode { get; set; }
    }
    #endregion

    #region N11
    class N11MarketplaceModel
    {
        public string Id { get; set; }

        public string StockCode { get; set; }

        public string CategoryCode { get; set; }

        public List<N11MarketplaceModelDetail> N11MarketplaceModelDetails { get; set; }
    }

    class N11MarketplaceModelDetail
    {
        public string CategoryCode { get; set; }

        public string AttributeValueName { get; set; }
    }
    #endregion

    #region GG
    class GGMarketplaceModel
    {
        public string Id { get; set; }

        public string StockCode { get; set; }

        public string CategoryCode { get; set; }

        public List<GGMarketplaceModelDetail> GGMarketplaceModelDetails { get; set; }
    }

    class GGMarketplaceModelDetail
    {
        public string CategoryCode { get; set; }

        public string AttributeValue { get; set; }
    }
    #endregion

    class Program
    {
        static string _connectionString = "Server=185.67.204.108;Database=Dev_ECommerce;UId=HelpyMarketPlace;Pwd=!Xaytcklmx!;";

        static void Main(string[] args)
        {
            //#region TY
            ////var products = TYRead();

            ////Console.WriteLine(products.Count);

            ////var index = 1;

            ////foreach (var pLoop in products)
            ////{
            ////    Console.WriteLine($"{products.Count}/{index}");

            ////    index++;

            ////    pLoop.TYMarketplaceModelDetails = TYReadDetail(pLoop.Id);

            ////    if (pLoop.TYMarketplaceModelDetails.Count == 0)
            ////    {
            ////        continue;
            ////    }

            ////    TYMatch(pLoop);
            ////}
            //#endregion

            //#region CS
            ////var products = CSRead();

            ////Console.WriteLine(products.Count);

            ////var index = 1;

            ////foreach (var pLoop in products)
            ////{
            ////    Console.WriteLine($"{products.Count}/{index}");

            ////    index++;

            ////    pLoop.CSMarketplaceModelDetails = CSReadDetail(pLoop.StockCode);

            ////    if (pLoop.CSMarketplaceModelDetails.Count == 0)
            ////    {
            ////        continue;
            ////    }

            ////    CSMatch(pLoop);
            ////}
            //#endregion

            //#region N11
            ////var products = N11Read();

            ////Console.WriteLine(products.Count);

            ////var index = 1;

            ////foreach (var pLoop in products)
            ////{
            ////    Console.WriteLine($"{products.Count}/{index}");

            ////    index++;

            ////    pLoop.N11MarketplaceModelDetails = N11ReadDetail(pLoop.Id);

            ////    if (pLoop.N11MarketplaceModelDetails.Count == 0)
            ////    {
            ////        continue;
            ////    }

            ////    N11Match(pLoop);
            ////}
            //#endregion

            //#region GG
            //var products = GGRead();

            //Console.WriteLine(products.Count);

            //var index = 1;

            //foreach (var pLoop in products)
            //{
            //    Console.WriteLine($"{products.Count}/{index}");

            //    index++;

            //    pLoop.GGMarketplaceModelDetails = GGReadDetail(pLoop.Id);

            //    if (pLoop.GGMarketplaceModelDetails.Count == 0)
            //    {
            //        continue;
            //    }

            //    GGMatch(pLoop);
            //}
            //#endregion
        }

//        #region TY
//        static List<TYMarketplaceModel> TYRead()
//        {
//            var list = new List<TYMarketplaceModel>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<TYMarketplaceModel>(
//                    @"
//Select	TP.Id, StockCode, TP.PimCategoryId As CategoryCode
//From	ProductInformationMarketplaces As PIM
//Join	TYProducts As TP
//On		PIM.StockCode = TP.Barcode
//Where	PIM.MarketPlaceId = 'TY'")
//                .ToList();
//            }

//            return list;
//        }

//        static List<TYMarketplaceModelDetail> TYReadDetail(string id)
//        {
//            var list = new List<TYMarketplaceModelDetail>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<TYMarketplaceModelDetail>(
//                    @"
//Select	    P.PimCategoryId As CategoryCode, PCAV.CategoryAttributeValueId As AttributeValueCode, CAV.Name As AttributeValue
//From	    TYProducts As P
//Join	    TYProductCategoryAttributeValues As PCAV
//On		    P.Id = PCAV.ProductId
//Join	    TYCategoryAttributeValues As CAV
//On		    PCAV.CategoryAttributeValueId = CAV.Id
//			And PCAV.CategoryAttributeValueId != 0
//Join	    TYCategoryAttributes As CA
//On		    CAV.CategoryAttributeId = CA.Id
//		    And CA.CategoryId = P.PimCategoryId
//Where	    P.Id = '" + id + @"'

//Union

//Select	    P.PimCategoryId As CategoryCode, PCAV.CategoryAttributeValueId As AttributeValueCode, CAV.Name As AttributeValue
//From	    TYProducts As P
//Join	    TYProductCategoryAttributeValues As PCAV
//On		    P.Id = PCAV.ProductId
//Join	    TYCategoryAttributeValues As CAV
//On		    PCAV.CategoryAttributeValueId = CAV.Id
//			And PCAV.CategoryAttributeValue = CAV.Name
//Join	    TYCategoryAttributes As CA
//On		    CAV.CategoryAttributeId = CA.Id
//		    And CA.CategoryId = P.PimCategoryId
//Where	    P.Id = '" + id + @"'")
//                .ToList();
//            }

//            return list;
//        }

//        static void TYMatch(TYMarketplaceModel tYMarketplaceModel)
//        {
//            using (var applicationDbContext = new ApplicationDbContext(null, new CryptoHelper()))
//            {
//                #region Find Product Information Marketplace
//                var productInformationMarketplace = applicationDbContext
//                            .ProductInformationMarketplaces
//                            .Include(pim => pim.ProductInformationMarketplaceCategoryVariantValues)
//                            .FirstOrDefault(pim => pim.StockCode == tYMarketplaceModel.StockCode && pim.MarketplaceCompany.MarketplaceId == "TY");

//                if (productInformationMarketplace == null)
//                    return;
//                #endregion

//                #region Find Marketplace Category
//                var marketPlaceCategory = applicationDbContext
//                    .MarketplaceCategoriesV2
//                    .FirstOrDefault(mpc => mpc.MarketplaceId == "TY" && mpc.Id == tYMarketplaceModel.CategoryCode);

//                if (marketPlaceCategory == null)
//                    return;

//                productInformationMarketplace.MarketplaceCategoryId = marketPlaceCategory.Id;
//                #endregion

//                if (tYMarketplaceModel.TYMarketplaceModelDetails.Count > 0)
//                {
//                    var marketplaceCategory = applicationDbContext
//                    .MarketplaceCategories
//                    .Include(mc => mc.MarketplaceCategoryVariants)
//                    .ThenInclude(mcv => mcv.MarketplaceCategoryVariantValues)
//                    .FirstOrDefault(mc => mc.Code == tYMarketplaceModel.CategoryCode);

//                    foreach (var mmdLoop in tYMarketplaceModel.TYMarketplaceModelDetails)
//                    {
//                        Domain.Entities.MarketPlaceCategoryVariantValue marketPlaceCategoryVariantValue = null;
//                        marketplaceCategory.MarketplaceCategoryVariants.ForEach(mcv => mcv.MarketplaceCategoryVariantValues.ForEach(mcvv =>
//                        {
//                            if (!string.IsNullOrEmpty(mmdLoop.AttributeValue))
//                            {
//                                if (mmdLoop.AttributeValueCode == "0")
//                                {
//                                    if (mcvv.Value == mmdLoop.AttributeValue)
//                                        marketPlaceCategoryVariantValue = mcvv;
//                                }
//                                else
//                                {
//                                    if (mcvv.Code == mmdLoop.AttributeValueCode)
//                                        marketPlaceCategoryVariantValue = mcvv;
//                                }
//                            }
//                        }));

//                        if (marketPlaceCategoryVariantValue == null)
//                            continue;

//                        productInformationMarketplace
//                            .ProductInformationMarketplaceCategoryVariantValues
//                            .Add(new Domain.Entities.ProductInformationMarketplaceCategoryVariantValue
//                            {
//                                CreatedDate = DateTime.Now,
//                                ManagerUserId = 1,
//                                MarketPlaceCategoryVariantValueId = marketPlaceCategoryVariantValue.Id
//                            });
//                    }
//                }

//                productInformationMarketplace.Active = true;

//                applicationDbContext.ProductInformationMarketplaces.Update(productInformationMarketplace).State = EntityState.Modified;

//                applicationDbContext.SaveChanges();
//            }
//        }
//        #endregion

//        #region CS
//        static List<CSMarketplaceModel> CSRead()
//        {
//            var list = new List<CSMarketplaceModel>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<CSMarketplaceModel>(
//                    @"
//Select	CP.MainProductCode As StockCode, CP.CategoryId As CategoryCode
//From	ProductInformationMarketplaces As PIM
//Join	CSProducts As CP
//On		PIM.StockCode = CP.MainProductCode
//Where	PIM.MarketPlaceId = 'CS'")
//                .ToList();
//            }

//            return list;
//        }

//        static List<CSMarketplaceModelDetail> CSReadDetail(string stockCode)
//        {
//            var list = new List<CSMarketplaceModelDetail>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<CSMarketplaceModelDetail>(
//                    @"
//Select	P.CategoryId As CategoryCode, PCAV.CSCategoryAttributeValueId As AttributeValueCode
//From	CSProducts As P
//Join	CSProductCategoryAttributeValues As PCAV
//On		P.MainProductCode = PCAV.MainProductCode
//Join	CSCategoryAttributeValues As CAV
//On		PCAV.CSCategoryAttributeValueId = CAV.Id
//Join	CSCategoryAttributes As CA
//On		CAV.CategoryAttributeId = CA.Id
//		And CA.CategoryId = P.CategoryId
//Where	P.MainProductCode = '" + stockCode + @"'")
//                .ToList();
//            }

//            return list;
//        }

//        static void CSMatch(CSMarketplaceModel cSMarketplaceModel)
//        {
//            using (var applicationDbContext = new ApplicationDbContext(null, new CryptoHelper()))
//            {
//                #region Find Product Information Marketplace
//                var productInformationMarketplace = applicationDbContext
//                            .ProductInformationMarketplaces
//                            .Include(pim => pim.ProductInformationMarketplaceCategoryVariantValues)
//                            .FirstOrDefault(pim => pim.StockCode == cSMarketplaceModel.StockCode && pim.MarketplaceCompany.MarketplaceId == "CS");

//                if (productInformationMarketplace == null)
//                    return;
//                #endregion

//                #region Find Marketplace Category
//                var marketPlaceCategory = applicationDbContext
//                    .MarketplaceCategoriesV2
//                    .FirstOrDefault(mpc => mpc.MarketplaceId == "CS" && mpc.Id == cSMarketplaceModel.CategoryCode);

//                if (marketPlaceCategory == null)
//                    return;

//                productInformationMarketplace.MarketplaceCategoryId = marketPlaceCategory.Id;
//                #endregion

//                if (cSMarketplaceModel.CSMarketplaceModelDetails.Count > 0)
//                {
//                    var marketplaceCategory = applicationDbContext
//                    .MarketPlaceCategories
//                    .Include(mc => mc.MarketplaceCategoryVariants)
//                    .ThenInclude(mcv => mcv.MarketplaceCategoryVariantValues)
//                    .FirstOrDefault(mc => mc.Code == cSMarketplaceModel.CategoryCode);

//                    foreach (var mmdLoop in cSMarketplaceModel.CSMarketplaceModelDetails)
//                    {
//                        Domain.Entities.MarketPlaceCategoryVariantValue marketPlaceCategoryVariantValue = null;
//                        marketplaceCategory.MarketplaceCategoryVariants.ForEach(mcv => mcv.MarketplaceCategoryVariantValues.ForEach(mcvv =>
//                        {
//                            if (mcvv.Code == mmdLoop.AttributeValueCode)
//                                marketPlaceCategoryVariantValue = mcvv;
//                        }));

//                        if (marketPlaceCategoryVariantValue == null)
//                            continue;

//                        productInformationMarketplace
//                            .ProductInformationMarketplaceCategoryVariantValues
//                            .Add(new Domain.Entities.ProductInformationMarketplaceCategoryVariantValue
//                            {
//                                CreatedDate = DateTime.Now,
//                                ManagerUserId = 1,
//                                MarketPlaceCategoryVariantValueId = marketPlaceCategoryVariantValue.Id
//                            });
//                    }
//                }

//                productInformationMarketplace.Active = true;

//                applicationDbContext.ProductInformationMarketplaces.Update(productInformationMarketplace).State = EntityState.Modified;

//                applicationDbContext.SaveChanges();
//            }
//        }
//        #endregion

//        #region N11
//        static List<N11MarketplaceModel> N11Read()
//        {
//            var list = new List<N11MarketplaceModel>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<N11MarketplaceModel>(
//                    @"
//Select	N11.Id As Id, N11.ProductSellerCode As StockCode, N11.CategoryId As CategoryCode
//From	ProductInformationMarketplaces As PIM
//Join	N11Products As N11
//On		PIM.StockCode = N11.ProductSellerCode
//Where	PIM.MarketPlaceId = 'N11'")
//                .ToList();
//            }

//            return list;
//        }

//        static List<N11MarketplaceModelDetail> N11ReadDetail(string id)
//        {
//            var list = new List<N11MarketplaceModelDetail>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<N11MarketplaceModelDetail>(
//                    @"
//Select	P.CategoryId As CategoryCode, PCAV.CategoryAttributeValueName As AttributeValueName
//From	N11Products As P
//Join	N11ProductCategoryAttributeValues As PCAV
//On		P.ProductSellerCode = PCAV.ProductSellerCode
//Join	N11CategoryAttributeValues As CAV
//On		PCAV.CategoryAttributeValueName = CAV.Name
//Join	N11CategoryAttributes As CA
//On		CA.Name = CAV.CategoryAttributeName
//		And CA.CategoryId = P.CategoryId
//Where	P.Id = '" + id + "'")
//                .ToList();
//            }

//            return list;
//        }

//        static void N11Match(N11MarketplaceModel n11MarketplaceModel)
//        {
//            using (var applicationDbContext = new ApplicationDbContext(null, new CryptoHelper()))
//            {
//                #region Find Product Information Marketplace
//                var productInformationMarketplace = applicationDbContext
//                            .ProductInformationMarketplaces
//                            .Include(pim => pim.ProductInformationMarketplaceCategoryVariantValues)
//                            .FirstOrDefault(pim => pim.StockCode == n11MarketplaceModel.StockCode && pim.MarketplaceCompany.MarketplaceId == "N11");

//                if (productInformationMarketplace == null)
//                    return;
//                #endregion

//                #region Find Marketplace Category
//                var marketPlaceCategory = applicationDbContext
//                    .MarketplaceCategoriesV2
//                    .FirstOrDefault(mpc => mpc.MarketplaceId == "N11" && mpc.Id == n11MarketplaceModel.CategoryCode);

//                if (marketPlaceCategory == null)
//                    return;

//                productInformationMarketplace.MarketplaceCategoryId = marketPlaceCategory.Id;
//                #endregion

//                if (n11MarketplaceModel.N11MarketplaceModelDetails.Count > 0)
//                {
//                    var marketplaceCategory = applicationDbContext
//                    .MarketPlaceCategories
//                    .Include(mc => mc.MarketplaceCategoryVariants)
//                    .ThenInclude(mcv => mcv.MarketplaceCategoryVariantValues)
//                    .FirstOrDefault(mc => mc.Code == n11MarketplaceModel.CategoryCode);

//                    foreach (var mmdLoop in n11MarketplaceModel.N11MarketplaceModelDetails)
//                    {
//                        Domain.Entities.MarketPlaceCategoryVariantValue marketPlaceCategoryVariantValue = null;
//                        marketplaceCategory.MarketplaceCategoryVariants.ForEach(mcv => mcv.MarketplaceCategoryVariantValues.ForEach(mcvv =>
//                        {
//                            if (!string.IsNullOrEmpty(mmdLoop.AttributeValueName))
//                            {
//                                if (mcvv.Value == mmdLoop.AttributeValueName)
//                                    marketPlaceCategoryVariantValue = mcvv;
//                            }
//                        }));

//                        if (marketPlaceCategoryVariantValue == null)
//                            continue;

//                        productInformationMarketplace
//                            .ProductInformationMarketplaceCategoryVariantValues
//                            .Add(new Domain.Entities.ProductInformationMarketplaceCategoryVariantValue
//                            {
//                                CreatedDate = DateTime.Now,
//                                ManagerUserId = 1,
//                                MarketPlaceCategoryVariantValueId = marketPlaceCategoryVariantValue.Id
//                            });
//                    }
//                }

//                productInformationMarketplace.Active = true;

//                applicationDbContext.ProductInformationMarketplaces.Update(productInformationMarketplace).State = EntityState.Modified;

//                applicationDbContext.SaveChanges();
//            }
//        }
//        #endregion

//        #region GG
//        static List<GGMarketplaceModel> GGRead()
//        {
//            var list = new List<GGMarketplaceModel>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<GGMarketplaceModel>(
//                    @"
//Select	GG.ProductId As Id, GG.ProductId As StockCode, GG.CategoryCode
//From	ProductInformationMarketplaces As PIM
//Join	GGProducts As GG
//On		PIM.StockCode = Cast(GG.ProductId As NVarChar(Max))
//Where	PIM.MarketPlaceId = 'GG'")
//                .ToList();
//            }

//            return list;
//        }

//        static List<GGMarketplaceModelDetail> GGReadDetail(string id)
//        {
//            var list = new List<GGMarketplaceModelDetail>();

//            using (var sqlConnection = new SqlConnection(_connectionString))
//            {
//                list = sqlConnection
//                .Query<GGMarketplaceModelDetail>(
//                    @"
//Select	P.ProductId As Id, P.CategoryCode As CategoryCode, PCAV.CategoryAttributeValueName As AttributeValue
//From	GGProducts As P
//Join	GGProductCategoryAttributeValues As PCAV
//On		P.ProductId = PCAV.ProductId
//Join	GGCategoryAttributeValues As CAV
//On		PCAV.CategoryAttributeValueName = CAV.Value
//Join	GGCategoryAttributes As CA
//On		CA.Name = CAV.Name
//		And CA.CategoryCode = P.CategoryCode
//Where	P.ProductId = '" + id + "'")
//                .ToList();
//            }

//            return list;
//        }

//        static void GGMatch(GGMarketplaceModel gGMarketplaceModel)
//        {
//            using (var applicationDbContext = new ApplicationDbContext(null, new CryptoHelper()))
//            {
//                #region Find Product Information Marketplace
//                var productInformationMarketplace = applicationDbContext
//                            .ProductInformationMarketplaces
//                            .Include(pim => pim.ProductInformationMarketplaceCategoryVariantValues)
//                            .FirstOrDefault(pim => pim.StockCode == gGMarketplaceModel.StockCode && pim.MarketplaceCompany.MarketplaceId == "GG");

//                if (productInformationMarketplace == null)
//                    return;
//                #endregion

//                #region Find Marketplace Category
//                var marketPlaceCategory = applicationDbContext
//                    .MarketplaceCategoriesV2
//                    .FirstOrDefault(mpc => mpc.MarketplaceId == "GG" && mpc.Id == gGMarketplaceModel.CategoryCode);

//                if (marketPlaceCategory == null)
//                    return;

//                productInformationMarketplace.MarketplaceCategoryId = marketPlaceCategory.Id;
//                #endregion

//                if (gGMarketplaceModel.GGMarketplaceModelDetails.Count > 0)
//                {
//                    var marketplaceCategory = applicationDbContext
//                    .MarketPlaceCategories
//                    .Include(mc => mc.MarketplaceCategoryVariants)
//                    .ThenInclude(mcv => mcv.MarketplaceCategoryVariantValues)
//                    .FirstOrDefault(mc => mc.Code == gGMarketplaceModel.CategoryCode);

//                    foreach (var mmdLoop in gGMarketplaceModel.GGMarketplaceModelDetails)
//                    {
//                        Domain.Entities.MarketPlaceCategoryVariantValue marketPlaceCategoryVariantValue = null;
//                        marketplaceCategory.MarketplaceCategoryVariants.ForEach(mcv => mcv.MarketplaceCategoryVariantValues.ForEach(mcvv =>
//                        {
//                            if (!string.IsNullOrEmpty(mmdLoop.AttributeValue))
//                            {
//                                if (mcvv.Value == mmdLoop.AttributeValue)
//                                    marketPlaceCategoryVariantValue = mcvv;
//                            }
//                        }));

//                        if (marketPlaceCategoryVariantValue == null)
//                            continue;

//                        productInformationMarketplace
//                            .ProductInformationMarketplaceCategoryVariantValues
//                            .Add(new Domain.Entities.ProductInformationMarketplaceCategoryVariantValue
//                            {
//                                CreatedDate = DateTime.Now,
//                                ManagerUserId = 1,
//                                MarketPlaceCategoryVariantValueId = marketPlaceCategoryVariantValue.Id
//                            });
//                    }
//                }

//                productInformationMarketplace.Active = true;

//                applicationDbContext.ProductInformationMarketplaces.Update(productInformationMarketplace).State = EntityState.Modified;

//                applicationDbContext.SaveChanges();
//            }
//        }
//        #endregion
    }
}