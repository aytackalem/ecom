﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.ProductFilter;
using ECommerce.MarketPlace.N11.Common;
using ECommerce.MarketPlace.N11.Common.Stock;
using N11OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ProductService = N11ProductService;

namespace ECommerce.MarketPlace.N11.Operation
{
    public class ProductOperation : IProductFacade
    {
        #region Fields  

        private readonly ProductService.ProductServicePortClient _productServicePortClient;

        #endregion

        #region Constructors
        public ProductOperation()
        {
            _productServicePortClient = new ProductService.ProductServicePortClient();
        }
        #endregion

        #region Methods
        public Response Create(ProductRequest productRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                var request = new ProductService.SaveProductRequest();
                request.auth = new ProductService.Authentication
                {
                    appKey = productRequest.Configurations["AppKey"],
                    appSecret = productRequest.Configurations["AppSecret"]
                };

                request.product = new ProductService.ProductRequest();
                request.product.productSellerCode = productRequest.ProductItem.SellerCode;
                request.product.title = productRequest.ProductItem.StockItems.FirstOrDefault()?.Title;
                request.product.subtitle = productRequest.ProductItem.StockItems.FirstOrDefault()?.Subtitle;
                request.product.description = productRequest.ProductItem.StockItems.FirstOrDefault()?.Description;
                request.product.category = new ProductService.CategoryRequest() { id = Convert.ToInt64(productRequest.ProductItem.CategoryId) };
                request.product.price = Convert.ToDecimal(productRequest.ProductItem.StockItems[0].SalePrice);
                request.product.domestic = true;
                request.product.currencyType = "1"; //productItem.CurrencyCode
                request.product.images = productRequest.ProductItem.StockItems.SelectMany(x => x.Images).Select(im => new ProductService.ProductImage
                {
                    order = im.Order.ToString(),
                    url = im.Url
                }).ToArray();
                request.product.approvalStatus = "1";
                request.product.productCondition = "1";
                request.product.shipmentTemplate = "ARAS KARGO";

                var productAttributeRequests = new List<ProductService.ProductAttributeRequest>();
                var attributes = productRequest.ProductItem.StockItems.SelectMany(x => x.Attributes).Select(x => new { Name = x.AttributeName, Value = x.AttributeValueName }).Distinct().ToList();

                foreach (var attribute in attributes)
                {
                    var productAttributeRequest = new ProductService.ProductAttributeRequest()
                    {
                        name = attribute.Name,
                        value = attribute.Value
                    };
                    productAttributeRequests.Add(productAttributeRequest);
                }

                request.product.attributes = productAttributeRequests.ToArray();
                request.product.preparingDay = productRequest.ProductItem.PreparingDayCount.ToString();

                var productSkuRequests = new List<ProductService.ProductSkuRequest>();

                foreach (var stockItem in productRequest.ProductItem.StockItems)
                {
                    var filter = Filter(new ProductFilterRequest
                    {
                        Configurations = productRequest.Configurations,
                        Stockcode = stockItem.StockCode
                    });

                    //filter.Data.GetProductBySellerCodeResponse.result.status == "success" ürün n11 tarafında var mı?
                    //Eğer ilk defa ürün açılacaksa ürün aktif değilse açılmaz
                    if (filter.Success && (filter.Data.GetProductBySellerCodeResponse.result.status == "success" || stockItem.Active))
                    {
                        if (!stockItem.Active)
                        {
                            UpdateStock(new StockRequest
                            {
                                Configurations = productRequest.Configurations,
                                PriceStock = new MarketPlace.Common.Request.Stock.PriceStock
                                {
                                    Quantity = 0,
                                    StockCode = stockItem.StockCode
                                }
                            });
                        }

                        var productSkuRequest = new ProductService.ProductSkuRequest();
                        productSkuRequest.gtin = stockItem.Barcode;
                        productSkuRequest.quantity = stockItem.Active ? stockItem.Quantity.ToString() : "0";
                        productSkuRequest.sellerStockCode = stockItem.StockCode;
                        productSkuRequest.optionPrice = Convert.ToDecimal(stockItem.SalePrice);

                        var stProductAttributeRequests = new List<ProductService.ProductAttributeRequest>();

                        foreach (var attr in stockItem.Attributes)
                        {
                            var productAttribute = new ProductService.ProductAttributeRequest()
                            {
                                name = attr.AttributeName,
                                value = attr.AttributeValueName
                            };

                            stProductAttributeRequests.Add(productAttribute);
                        }

                        productSkuRequest.attributes = stProductAttributeRequests.ToArray();
                        productSkuRequests.Add(productSkuRequest);
                    }
                }

                request.product.stockItems = productSkuRequests.ToArray();

                var apiResponse = _productServicePortClient.SaveProductAsync(request).Result;

                if(apiResponse.SaveProductResponse.result.errorCode== "SELLER_API.notAvailableForUpdateForSixtySeconds")
                {
                    Thread.Sleep(new TimeSpan(0, 1, 10));
                    apiResponse = _productServicePortClient.SaveProductAsync(request).Result;
                }

                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdateStock(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                var getProductBySellerCodeRequest = new ProductService.GetProductBySellerCodeRequest();
                getProductBySellerCodeRequest.auth = new ProductService.Authentication
                {
                    appKey = stockRequest.Configurations["AppKey"],
                    appSecret = stockRequest.Configurations["AppSecret"]
                };
                getProductBySellerCodeRequest.sellerCode = stockRequest.PriceStock.StockCode;

                var getProductBySellerCodeResult = _productServicePortClient.GetProductBySellerCodeAsync(getProductBySellerCodeRequest).Result;
                if (getProductBySellerCodeResult.GetProductBySellerCodeResponse.result.status == "success" && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product != null && getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.stockItems.stockItem.Length > 0)
                {

                    var data = $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sch=""http://www.n11.com/ws/schemas"">
   <soapenv:Header/>
   <soapenv:Body>
      <sch:UpdateStockByStockIdRequest>
         <auth>
        <appKey>{stockRequest.Configurations["AppKey"]}</appKey>
            <appSecret>{stockRequest.Configurations["AppSecret"]}</appSecret>
         </auth>
         <stockItems>
            <!--1 or more repetitions:-->
            <stockItem>
               <id>{getProductBySellerCodeResult.GetProductBySellerCodeResponse.product.stockItems.stockItem[0].id}</id>
               <quantity>{stockRequest.PriceStock.Quantity}</quantity>
               <version></version>
               <delist></delist>
            </stockItem>
         </stockItems>
      </sch:UpdateStockByStockIdRequest>
   </soapenv:Body>
</soapenv:Envelope>";
                    var stockResponseStr = Serializer.Post(data, "https://api.n11.com/ws/ProductStockService.wsdl");
                    if (stockResponseStr.Success)
                    {

                        var stockResponse = Serializer.Deserialize<UpdateStockByStockIdResponse>(stockResponseStr.Data);
                        response.Success = stockResponse.Result.Status == "success";
                        return;
                    }
                }

                response.Success = false;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        public Response UpdatePrice(StockRequest stockRequest)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {

                if (stockRequest.PriceStock.SalePrice.HasValue)
                {

                    var priceRequest = new ProductService.UpdateProductPriceBySellerCodeRequest();

                    priceRequest.auth = new ProductService.Authentication
                    {
                        appKey = stockRequest.Configurations["AppKey"],
                        appSecret = stockRequest.Configurations["AppSecret"]
                    };

                    priceRequest.productSellerCode = stockRequest.PriceStock.StockCode;
                    priceRequest.price = stockRequest.PriceStock.SalePrice.Value;
                    var priceResponse = _productServicePortClient.UpdateProductPriceBySellerCodeAsync(priceRequest).Result;

                }



                response.Success = true;
            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });
        }

        #endregion

        #region Helper


        private DataResponse<N11ProductService.GetProductBySellerCodeResponse1> Filter(ProductFilterRequest productFilter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<N11ProductService.GetProductBySellerCodeResponse1>>((response) =>
            {

                var request = new ProductService.GetProductBySellerCodeRequest();
                request.auth = new ProductService.Authentication
                {
                    appKey = productFilter.Configurations["AppKey"],
                    appSecret = productFilter.Configurations["AppSecret"]
                };
                request.sellerCode = productFilter.Stockcode;

                response.Data = _productServicePortClient.GetProductBySellerCodeAsync(request).Result;

                response.Success = true;
            });
        }
        #endregion
    }
}
