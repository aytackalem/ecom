﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Extensions;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Facade;
using ECommerce.MarketPlace.Common.Response;
using N11CategoryService;
using N11OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.MarketPlace.N11.Operation
{
    public class CategoryOperation : ICategoryFacade
    {


        #region Constructors
        public CategoryOperation()
        {
        }
        #endregion

        #region Methods
        public DataResponse<List<CategoryResponse>> Get(Dictionary<string, string> configurations)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var n11CategoryService = new N11CategoryService.CategoryServicePortClient();

                var request = new N11CategoryService.GetTopLevelCategoriesRequest
                {
                    auth = new N11CategoryService.Authentication { appKey = configurations["AppKey"], appSecret = configurations["AppSecret"] },
                };

                var categories = n11CategoryService.GetTopLevelCategoriesAsync(request).Result;

                //string[] anyCategories = new string[] { "1002583", "1002639", "1002553" };

                string[] anyCategories = new string[] { "1002605", "1001935", "1001929", "1001873", "1002816", "1002717", "1001770", "1002663", "1000145", "1003335", "1002690", "1002742",  "1002816",  "1002717", "1191215", "1062100"};

                var data = new List<CategoryResponse>();
                int i = 1;
                foreach (var caList in categories.GetTopLevelCategoriesResponse.categoryList)
                {

                    if (!anyCategories.Contains(caList.id.ToString())) continue;

                    Console.WriteLine($"{i} {caList.name} {caList.id}");
                    i++;

                    data.AddRange(GetSubCategory(configurations, caList, null,new CategoryResponse()).Data);
                }


                response.Data = data;
                response.Success = true;
            });
        }
        #endregion

        public DataResponse<List<CategoryResponse>> GetSubCategory(Dictionary<string, string> configurations, SubCategory subCategory, string parentCategoryCode, CategoryResponse setCategories)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<CategoryResponse>>>((response) =>
            {
                var _categories = new List<CategoryResponse>();

                try
                {
                    var n11CategoryService = new N11CategoryService.CategoryServicePortClient();
                    var categoryResponse = new CategoryResponse()
                    {
                        Code = subCategory.id.ToString(),
                        Name = subCategory.name,
                        ParentCategoryCode = parentCategoryCode

                    };

                    var getCategoryAttributesIdRequest = new N11CategoryService.GetCategoryAttributesIdRequest
                    {
                        auth = new N11CategoryService.Authentication { appKey = configurations["AppKey"], appSecret = configurations["AppSecret"] },
                        categoryId = subCategory.id
                    };

                    var categoryAttributes = n11CategoryService.GetCategoryAttributesIdAsync(getCategoryAttributesIdRequest).Result;

                    if (categoryAttributes.GetCategoryAttributesIdResponse.result.status != "success")
                    {
                        response.Data = _categories;
                        response.Success = true;
                        return;
                    }



                    foreach (var catLoop in categoryAttributes.GetCategoryAttributesIdResponse.categoryProductAttributeList)
                    {
                        if (catLoop.name == "Marka") continue;

                        var categoryAttibute = new CategoryAttributeResponse
                        {
                            Code = catLoop.id.ToString(),
                            Name = catLoop.name,
                            AllowCustom = false,
                            CategoryCode = subCategory.id.ToString(),
                            Mandatory = catLoop.mandatory,
                            MultiValue = catLoop.multipleSelect
                        };


                        categoryAttibute.CategoryAttributesValues = setCategories.CategoryAttributes.SelectMany(x => x.CategoryAttributesValues).Where(x => x.VarinatCode == catLoop.id.ToString()).Select(x => new CategoryAttributeValueResponse
                        {
                            Code = x.Code,
                            Name = x.Name,
                            VarinatCode = x.VarinatCode

                        }).ToList();


                        if (categoryAttibute.CategoryAttributesValues.Count == 0)
                        {
                            var page = 0;
                            var isLoop = true;
                            while (isLoop)
                            {

                                var getCategoryAttributeValueRequest = new N11CategoryService.GetCategoryAttributeValueRequest
                                {
                                    auth = new N11CategoryService.Authentication { appKey = configurations["AppKey"], appSecret = configurations["AppSecret"] },
                                    categoryId = subCategory.id,
                                    categoryProductAttributeId = catLoop.id,
                                    pagingData = new N11CategoryService.RequestPagingData { currentPage = page, pageSize = 100 },
                                };
                                
                                var getCategoryAttributeValues = n11CategoryService.GetCategoryAttributeValueAsync(getCategoryAttributeValueRequest).Result;

                                if (getCategoryAttributeValues.GetCategoryAttributeValueResponse.result.status != "success" || getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList == null ||
                                getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList.Length == 0)
                                {
                                    isLoop = false;
                                    continue;
                                }

                                foreach (var atvLoop in getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList)
                                {
                                    categoryAttibute.CategoryAttributesValues.Add(new CategoryAttributeValueResponse
                                    {
                                        VarinatCode = catLoop.id.ToString(),
                                        Code = atvLoop.id == 0 ? $"{atvLoop.name.ReplaceChar().Replace(" ", "-")}" : atvLoop.id.ToString(),
                                        Name = atvLoop.name,
                                    });
                                }
                                page++;
                                isLoop = getCategoryAttributeValues.GetCategoryAttributeValueResponse.categoryProductAttributeValueList.Length == 100 && getCategoryAttributeValues.GetCategoryAttributeValueResponse.pagingData.currentPage != getCategoryAttributeValues.GetCategoryAttributeValueResponse.pagingData.pageCount;

                            }
                        }

                        categoryResponse.CategoryAttributes.Add(categoryAttibute);

                    }

                    _categories.Add(categoryResponse);


                    var subCategoriesRequest = new N11CategoryService.GetSubCategoriesRequest
                    {
                        auth = new N11CategoryService.Authentication { appKey = configurations["AppKey"], appSecret = configurations["AppSecret"] },
                        categoryId = subCategory.id
                    };

                    var subCategories = n11CategoryService.GetSubCategoriesAsync(subCategoriesRequest).Result;

                    if (subCategories.GetSubCategoriesResponse.result.status != "success")
                    {
                        response.Data = _categories;
                        response.Success = true;
                        return;
                    }


                    foreach (var cLoop in subCategories.GetSubCategoriesResponse.category)
                    {
                        if (cLoop.subCategoryList == null) continue;

                        foreach (var subLoop in cLoop.subCategoryList)
                        {

                            _categories.AddRange(GetSubCategory(configurations, subLoop, subCategory.id.ToString(), categoryResponse).Data);

                        }

                    }
                }
                catch (Exception ex)
                {


                }

                response.Data = _categories;
                response.Success = true;
            });
        }

    }
}
