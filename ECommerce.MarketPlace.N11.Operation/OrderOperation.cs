﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Wrappers;
using ECommerce.MarketPlace.Common.Request;
using ECommerce.MarketPlace.Common.Response;
using ECommerce.MarketPlace.Common.Response.Order;
using ECommerce.MarketPlace.Common.Facade;
using N11OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel;
using ECommerce.MarketPlace.Common.Request.Enums;
using ECommerce.Application.Common.Constants;

namespace ECommerce.MarketPlace.N11.Operation
{
    public class OrderOperation : IOrderFacade
    {


        #region Methods
        public DataResponse<OrderResponse> Get(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderResponse>>((response) =>
            {

                var n11OrderService = new OrderServicePortClient();
                response.Data = new OrderResponse();
                response.Data.Orders = new List<Order>();
                var request = new DetailedOrderListRequest
                {
                    auth = new Authentication { appKey = orderRequest.Configurations["AppKey"], appSecret = orderRequest.Configurations["AppSecret"] },
                    searchData = new OrderDataListRequest { status = "New" }
                };
                var detailedOrderListResponse = new DetailedOrderListResponse1();
                detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                try
                {
                    if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0)
                    {
                        foreach (var data in detailedOrderListResponse.DetailedOrderListResponse.orderList)
                        {
                            var orderItemList = new OrderItemDataRequest[data.orderItemList.Length];

                            for (int i = 0; i < data.orderItemList.Length; i++)
                            {
                                try
                                {
                                    Console.WriteLine();
                                    orderItemList[i] = new OrderItemDataRequest { id = data.orderItemList[i].id };

                                    Console.WriteLine($"N11 {data.orderItemList[i].id} sipariş numarası eklendi");
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                }
                            }

                            if (data.orderItemList != null && data.orderItemList.Length > 0)
                            {
                                var orderItemAccept = new OrderItemAcceptRequest
                                {

                                    auth = new Authentication { appKey = orderRequest.Configurations["AppKey"], appSecret = orderRequest.Configurations["AppSecret"] },
                                    numberOfPackages = "1",
                                    orderItemList = orderItemList
                                };
                                var orderItemAcceptResponse = n11OrderService.OrderItemAcceptAsync(orderItemAccept).Result;
                                if (orderItemAcceptResponse.OrderItemAcceptResponse.result.status == "success")
                                {
                                    Console.BackgroundColor = ConsoleColor.Green;
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine($"N11 {data.orderItemList[0].id} sipariş numarası başarılı bir şekilde onaylandı statüsünüe geçti");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                else
                                {
                                    Console.BackgroundColor = ConsoleColor.Red;
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine($"[E] [{data.orderItemList[0].id}] siparişi onayladı statüsüne geçiş yapamadı.");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;
                                }

                            }

                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex);
                }
                Console.WriteLine("N11 30 Second Waiting");
                Thread.Sleep(new TimeSpan(0, 0, 30));


                request = new DetailedOrderListRequest
                {
                    auth = new Authentication { appKey = orderRequest.Configurations["AppKey"], appSecret = orderRequest.Configurations["AppSecret"] },
                    searchData = new OrderDataListRequest
                    {
                        status = "Approved"
                        // orderNumber = "205258698354"//orderRequest.MarketPlaceOrderCode
                        //status = "Shipped"
                    }
                };

                detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0)
                {


                    foreach (var order in detailedOrderListResponse.DetailedOrderListResponse.orderList)
                    {


                        var orderDetailResult = OrderDetail(n11OrderService, order.id, orderRequest);

                        if (orderDetailResult.Success)
                        {
                            var orderDetail = orderDetailResult.Data.OrderDetailResponse;
                            var orderDetails = new List<OrderDetail>();
                            var payments = new List<Payment>();


                            switch (orderDetail.orderDetail.shippingAddress.city)
                            {
                                case "K.Maraş":
                                    orderDetail.orderDetail.shippingAddress.city = "Kahramanmaraş";
                                    break;
                                default:
                                    break;
                            }

                            foreach (var item in orderDetail.orderDetail.itemList)
                            {
                                orderDetails.Add(new OrderDetail
                                {
                                    Product = new Product
                                    {
                                        Barcode = item.shipmentInfo.shipmentCode,
                                        Name = item.productName,
                                        StockCode = item.productSellerCode,
                                    },
                                    UnitPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    ListPrice = item.sellerInvoiceAmount / Convert.ToInt32(item.quantity),
                                    TaxRate = 0.18,
                                    Quantity = Convert.ToInt32(item.quantity)
                                });
                            }

                            payments.Add(new Payment
                            {
                                Amount = order.totalAmount,
                                Date = DateTime.Now,
                                CurrencyId = "TRY",
                                PaymentTypeId = "OO"
                                //CurrencyId= order.
                            });




                            string shipmentId = "";
                            switch (orderDetail.orderDetail.itemList[0].shipmentInfo.shipmentCompany.id)
                            {
                                case 345:
                                    shipmentId = ShipmentCompanies.ArasKargo;
                                    break;
                                case 344:
                                    shipmentId = ShipmentCompanies.YurtiçiKargo;
                                    break;
                                case 381:
                                    shipmentId = ShipmentCompanies.PttKargo;
                                    break;
                                case 342:
                                    shipmentId = ShipmentCompanies.MngKargo;
                                    break;
                                case 341:
                                    shipmentId = ShipmentCompanies.SüratKargo;
                                    break;
                                case 343:
                                    shipmentId = ShipmentCompanies.Ups;
                                    break;

                            }


                            var customerName = orderDetail.orderDetail.billingAddress.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var invoiceFirstName = "";
                            var invoiceLastName = "";
                            if (customerName.Length > 1)
                            {
                                invoiceFirstName = customerName[0];
                                invoiceLastName = customerName[1];
                            }

                            customerName = orderDetail.orderDetail.buyer.fullName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            var firstName = "";
                            var lastName = "";
                            if (customerName.Length > 1)
                            {
                                firstName = customerName[0];
                                lastName = customerName[1];
                            }

                            response.Data.Orders.Add(new Order
                            {
                                Customer = new Customer
                                {
                                    FirstName = orderDetail.orderDetail.buyer.fullName,
                                    LastName = orderDetail.orderDetail.buyer.fullName,
                                    IdentityNumber = orderDetail.orderDetail.buyer.id.ToString(),
                                    Mail = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.billingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.buyer.taxId,

                                },
                                OrderShipment = new OrderShipment
                                {
                                    TrackingCode = orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber != null ? orderDetail.orderDetail.itemList[0].shipmentInfo.trackingNumber : orderDetail.orderDetail.itemList[0].shipmentInfo.campaignNumber,
                                    TrackingUrl = "",
                                    ShipmentTypeId = "3",
                                    ShipmentCompanyId = shipmentId
                                },
                                OrderCode = order.orderNumber.ToString(),
                                OrderTypeId = "OS",
                                Discount = 0,
                                ListTotalAmount = order.totalAmount,
                                OrderNote = "",
                                OrderSourceId = OrderSources.Pazaryeri,
                                MarketPlaceId = Marketplaces.N11,
                                OrderDetails = orderDetails,
                                OrderDate = DateTime.Now,
                                TotalAmount = order.totalAmount,
                                Barcode = order.citizenshipId,
                                CurrencyId = "TRY",
                                Source = "N11",
                                Shipper = "",
                                ShippingCost = 0,
                                SurchargeAmount = 0,
                                OrderDeliveryAddress = new OrderDeliveryAddress
                                {
                                    Address = orderDetail.orderDetail.shippingAddress.address,
                                    City = orderDetail.orderDetail.shippingAddress.city,
                                    Neighborhood = $"{orderDetail.orderDetail.shippingAddress.neighborhood.Replace("Mh.", "")}",
                                    Country = "",
                                    Code = "",
                                    District = orderDetail.orderDetail.shippingAddress.district,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    Email = orderDetail.orderDetail.buyer.email,
                                    FirstName = firstName,
                                    LastName = lastName
                                },
                                OrderInvoiceAddress = new OrderInvoiceAddress
                                {
                                    FirstName = invoiceFirstName,
                                    LastName = invoiceLastName,
                                    Address = orderDetail.orderDetail.billingAddress.address,
                                    City = orderDetail.orderDetail.billingAddress.city,
                                    Country = orderDetail.orderDetail.billingAddress.postalCode,
                                    District = orderDetail.orderDetail.billingAddress.district,
                                    Neighborhood = $"{orderDetail.orderDetail.billingAddress.neighborhood.Replace("Mh.", "")}",
                                    Email = orderDetail.orderDetail.buyer.email,
                                    Phone = orderDetail.orderDetail.shippingAddress.gsm,
                                    TaxNumber = orderDetail.orderDetail.billingAddress.taxId,
                                    TaxOffice = orderDetail.orderDetail.billingAddress.taxHouse
                                },
                                Payments = new List<Payment>()
                                {
                                    new Payment   {
                                        Amount = order.totalAmount,
                                        Date = DateTime.Now,
                                        CurrencyId = "TRY",
                                        PaymentTypeId = "OO"
                                    }}
                            });
                        }
                    }
                    response.Success = true;
                }


            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "";
            });

        }

        public DataResponse<CheckPreparingResponse> CheckPreparing(OrderRequest orderRequest)
        {
            return ExceptionHandler.ResultHandle<DataResponse<CheckPreparingResponse>>((response) =>
            {
                response.Data = new CheckPreparingResponse();

                // Sipariş maddesi durum bilgisi:
                //1:İşlem Bekliyor
                //2:Ödendi
                //3:Geçersiz
                //4:İptal Edilmiş
                //5:Kabul Edilmiş
                //6:Kargoda
                //7:Teslim Edilmiş
                //8:Reddedilmiş
                //9:İade Edildi
                //10:Tamamlandı
                //11: İade İptal Değişim Talep Edildi
                //12: İade İptal Değişim Tamamlandı
                //13:Kargoda İade
                //14:Kargo Yapılması Gecikmiş,
                //15:Kabul Edilmiş Ama Zamanında Kargoya Verilmemiş
                //16:Teslim Edilmiş İade, 
                //17:Tamamlandıktan Sonra İade

                var n11OrderService = new OrderServicePortClient();
                string[] cancelOrderStatus = new string[3] { "3", "4", "8" };
                var request = new DetailedOrderListRequest
                {
                    auth = new Authentication { appKey = orderRequest.Configurations["AppKey"], appSecret = orderRequest.Configurations["AppSecret"] },
                    searchData = new OrderDataListRequest
                    {
                        //status = "Approved"
                        orderNumber = orderRequest.MarketPlaceOrderCode
                        //status = "Shipped"
                    }
                };
                var detailedOrderListResponse = n11OrderService.DetailedOrderListAsync(request).Result;

                if (detailedOrderListResponse.DetailedOrderListResponse.result.status == "success" && detailedOrderListResponse.DetailedOrderListResponse.orderList != null && detailedOrderListResponse.DetailedOrderListResponse.orderList.Length > 0 && (detailedOrderListResponse.DetailedOrderListResponse.orderList[0].orderItemList.Any(x => cancelOrderStatus.Contains(x.status)) || detailedOrderListResponse.DetailedOrderListResponse.orderList[0].status == "3"))
                {
                    response.Data.IsCancel = true;
                    response.Message = $"Siparişiniz iptal statüsündedir. N11 panelinden siparişinizi kontrol ediniz.";

                }
                response.Success = true;
            }, (response, exception) =>
                {
                    response.Success = false;
                    response.Message = "Trendyol ile bağlantı kurulamıyor.";
                });

        }

        public Response OrderTypeUpdate(OrderPickingRequest orderPackaging)
        {
            return ExceptionHandler.ResultHandle<Response>((response) =>
            {
                response.Success = true;
                //#if !DEBUG



                //#endif

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Trendyol ile bağlantı kurulamıyor.";
            });
        }
        #endregion


        #region Helpers
        private DataResponse<OrderDetailResponse1> OrderDetail(OrderServicePortClient n11OrderService, long id, OrderRequest orderRequest)
        {

            return ExceptionHandler.ResultHandle<DataResponse<OrderDetailResponse1>>((response) =>
            {

                var orderDetailResponse = n11OrderService.OrderDetailAsync(new OrderDetailRequest
                {
                    orderRequest = new OrderDataRequest
                    {
                        id = id
                    },
                    auth = new Authentication { appKey = orderRequest.Configurations["AppKey"], appSecret = orderRequest.Configurations["AppSecret"] },
                });

                response.Data = orderDetailResponse.Result;
                response.Success = true;

            }, (response, exception) =>
            {
                response.Success = false;
                response.Message = "Müşteri datalarını çekerken hata aldı";
            });

        }

        public DataResponse<OrderResponse> GetDelivered(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetCancelled(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }

        public DataResponse<OrderResponse> GetReturned(OrderRequest orderRequest)
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}
