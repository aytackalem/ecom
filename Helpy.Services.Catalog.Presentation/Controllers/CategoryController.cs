using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Category;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.Catalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        #region Fields
        private readonly ICategoryService _categoryService;
        #endregion

        #region Constructors
        public CategoryController(ICategoryService categoryService)
        {
            #region Fields
            this._categoryService = categoryService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Insert(InsertRequest request)
        {
            return Ok(await this._categoryService.InsertAsync(request));
        }
        #endregion
    }
}