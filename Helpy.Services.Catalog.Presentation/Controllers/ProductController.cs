using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Product;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.Catalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        #region Fields
        private readonly IProductService _productService;
        #endregion

        #region Constructors
        public ProductController(IProductService productService)
        {
            #region Fields
            this._productService = productService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Insert(InsertRequest request)
        {
            return Ok(await this._productService.InsertAsync(request));
        }

        [HttpGet]
        public async Task<IActionResult> GetById(string id)
        {
            return Ok(await this._productService.GetByIdAsync(id));
        }
        #endregion
    }
}