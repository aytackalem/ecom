using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Brand;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.Catalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrandController : ControllerBase
    {
        #region Fields
        private readonly IBrandService _brandService;
        #endregion

        #region Constructors
        public BrandController(IBrandService brandService)
        {
            #region Fields
            this._brandService = brandService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpPost]
        public async Task<IActionResult> Insert(InsertRequest request)
        {
            return Ok(await this._brandService.InsertAsync(request));
        }
        #endregion
    }
}