using Helpy.Services.Catalog.Core.Application.DataTransferObjects;
using Helpy.Services.Catalog.Core.Application.Interfaces.Services;
using Helpy.Services.Catalog.Core.Application.Parameters.Variant;
using Microsoft.AspNetCore.Mvc;

namespace Helpy.Services.Catalog.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VariantController : ControllerBase
    {
        #region Fields
        private readonly IVariantService _variantService;
        #endregion

        #region Constructors
        public VariantController(IVariantService variantService)
        {
            #region Fields
            this._variantService = variantService;
            #endregion
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> GetByIdAsync(string id)
        {
            return Ok(await this._variantService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Insert(InsertRequest request)
        {
            return Ok(await this._variantService.InsertAsync(request));
        }

        [HttpPost("InsertVariantValue")]
        public async Task<IActionResult> InsertVariantValue(Helpy.Services.Catalog.Core.Application.Parameters.VariantValue.InsertRequest request)
        {
            return Ok(await this._variantService.InsertVariantValueAsync(request));
        }
        #endregion
    }
}