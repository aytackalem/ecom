﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Persistence.Packaging.Helpers
{
    public class FilterCompanyFinder : ICompanyFinder
    {
        #region Fields
        public int _companyId;
        #endregion

        #region Methods
        public int FindId()
        {
            return this._companyId;
        }

        public void Set(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
