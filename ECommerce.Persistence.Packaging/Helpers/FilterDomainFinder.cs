﻿using ECommerce.Application.Common.Interfaces.Helpers;

namespace ECommerce.Persistence.Packaging.Helpers
{
    public class FilterDomainFinder : IDomainFinder
    {
        #region Fields
        public int _domainId;
        #endregion

        #region Methods
        public int FindId()
        {
            return this._domainId;
        }

        public void Set(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
