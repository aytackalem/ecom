﻿using ECommerce.Application.Common.Interfaces.Accounting;
using ECommerce.Application.Common.Interfaces.Caching;
using ECommerce.Application.Common.Interfaces.Communication;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.Shipment;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Packaging.Interfaces.Services;
using ECommerce.Infrastructure;
using ECommerce.Persistence.Common.Helpers;
using ECommerce.Persistence.Common.Services;
using ECommerce.Persistence.Common.Shipment;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.Packaging.Caching;
using ECommerce.Persistence.Packaging.Helpers;
using ECommerce.Persistence.Packaging.Services;
using ECommerce.Persistence.UnitOfWorks;
using Helpy.Shared.Crypto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerce.Persistence.Packaging
{
    public static class ServiceRegistration
    {
        #region Methods
        public static void AddPersistenceServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            #region Tenant Dependencies
            serviceCollection.AddSingleton<ITenantFinder, LoginTenantFinder>();
            serviceCollection.AddSingleton<IDomainFinder, FilterDomainFinder>();
            serviceCollection.AddSingleton<ICompanyFinder, FilterCompanyFinder>();
            #endregion

            #region Config And Setting Dependencies
            serviceCollection.AddTransient<ISettingService, SettingService>();
            #endregion

            #region DbContext Dependencies
            serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("ECommerce"));
            });
            #endregion

            serviceCollection.AddMemoryCache();

            serviceCollection.AddSingleton<ICacheHandler, CacheHandler>();

            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            serviceCollection.AddScoped<IHttpHelper, HttpHelper>();

            serviceCollection.AddScoped<ICompanyConfigurationService, CompanyConfigurationService>();

            serviceCollection.AddScoped<Application.Packaging.Interfaces.Services.IMarketplaceService, Services.MarketplaceService>();
            
            serviceCollection.AddScoped<IDomainService, DomainService>();

            serviceCollection.AddScoped<IShipmentCompanyCompanyService, ShipmentCompanyCompanyService>();
            
            serviceCollection.AddScoped<IOrderSourceService, OrderSourceService>();

            serviceCollection.AddScoped<ICompanyService, CompanyService>();
            
            serviceCollection.AddScoped<IOrderInvoiceInformationService, OrderInvoiceInformationService>();
            
            serviceCollection.AddScoped<IOrderShipmentService, OrderShipmentService>();
            
            serviceCollection.AddScoped<IAccountingCompanyConfigurationService, AccountingCompanyConfigurationService>();

            serviceCollection.AddScoped<IAccountingProviderService, AccountingProviderService>();

            serviceCollection.AddSingleton<IShipmentProviderService, ShipmentProviderService>();

            serviceCollection.AddTransient<ISmsService, SmsProviderService>();

            serviceCollection.AddTransient<Application.Common.Interfaces.Marketplaces.IMarketplaceService, Common.Marketplace.MarketplaceService>();

            serviceCollection.AddInfrastructureServices(configuration);
        }
        #endregion
    }
}
