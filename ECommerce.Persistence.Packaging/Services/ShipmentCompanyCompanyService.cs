﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class ShipmentCompanyCompanyService : IShipmentCompanyCompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public ShipmentCompanyCompanyService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<List<ShipmentCompany>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<ShipmentCompany>>>(response =>
            {
                var entities = this
                    ._unitOfWork
                    .ShipmentCompanyCompanyRepository
                    .DbSet()
                    .Include(x => x.ShipmentCompany)
                    .ToList();

                response.Data = entities
                    .Select(e => new ShipmentCompany { Id = e.ShipmentCompany.Id, Name = e.ShipmentCompany.Name })
                    .ToList();

                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
