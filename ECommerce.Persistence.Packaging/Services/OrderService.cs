﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.Services;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services.Base;
using ECommerce.Application.Packaging.Parameters.Order;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public abstract class OrderService : IOrderService
    {
        #region Fields
        protected readonly IUnitOfWork _unitOfWork;

        protected readonly ISettingService _settingService;
        #endregion

        #region Constructors
        public OrderService(IUnitOfWork unitOfWork, ISettingService settingService)
        {
            #region Fields
            _unitOfWork = unitOfWork;
            _settingService = settingService;
            #endregion
        }
        #endregion

        #region Methods
        public DataResponse<Order> Get(GetParameter parameter)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Order>>(response =>
            {
                var entityQuery = 
                    _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Company.CompanyContact)
                    .Include(o => o.OrderSource)
                    .Include(o => o.OrderNotes)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(x => x.ShipmentCompany)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(os => os.OrderShipmentDetail)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City)
                    .Include(o => o.Customer)
                    .Include(o => o.OrderInvoiceInformation)
                    .Include(o => o.OrderDetails)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationMarketplaces)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationVariants)
                            .ThenInclude(piv => piv.VariantValue.VariantValueTranslations)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationVariants)
                            .ThenInclude(piv => piv.VariantValue.Variant)
                                .ThenInclude(v => v.VariantTranslations)
                    .Include(o => o.Payments)
                        .ThenInclude(pt => pt.PaymentType.PaymentTypeTranslations);
                //.FirstOrDefault(o => o.Id == id);

                Domain.Entities.Order entity = null;

                if (parameter.OrderId > 0)
                    entity = entityQuery.FirstOrDefault(eq => eq.Id == parameter.OrderId);
                else
                    entity = entityQuery.FirstOrDefault(eq => eq.OrderPicking.PackerBarcode == parameter.PackerBarcode);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş bulunamadı.";
                    return;
                }

                response.Data = new Order
                {
                    Id = entity.Id,
                    OrderTypeId = entity.OrderTypeId,
                    BulkInvoicing = entity.BulkInvoicing,
                    Amount = entity.Total,
                    CargoFee = entity.CargoFee,
                    SurchargeFee = entity.SurchargeFee,
                    CurrencyId = entity.CurrencyId,
                    OrderSourceId = entity.OrderSourceId,
                    MarketPlaceId = entity.MarketplaceId,
                    MarketPlaceOrderNumber = entity.MarketplaceOrderNumber,
                    Company = new Company
                    {
                        Id = entity.CompanyId,
                        Name = entity.Company.Name,
                        CompanyContact = new CompanyContact
                        {
                            Address = entity.Company.CompanyContact.Address,
                            Email = entity.Company.CompanyContact.Email,
                            PhoneNumber = entity.Company.CompanyContact.PhoneNumber,
                            TaxNumber = entity.Company.CompanyContact.TaxNumber,
                            TaxOffice = entity.Company.CompanyContact.TaxOffice,
                            WebUrl = entity.Company.CompanyContact.WebUrl
                        }
                    }
                };

                response.Data.Customer = new Customer
                {
                    Id = entity.Customer.Id,
                    Name = entity.Customer.Name,
                    Surname = entity.Customer.Surname
                };

                response.Data.OrderSource = new OrderSource
                {
                    Id = entity.OrderSource.Id,
                    Name = entity.OrderSource.Name
                };

                response.Data.OrderNotes = entity.OrderNotes.Select(on => new OrderNote
                {
                    Id = on.Id,
                    Note = on.Note
                }).ToList();

                response.Data.Payments = entity
                    .Payments
                    .Select(p => new Payment
                    {
                        Amount = p.Amount,
                        CreatedDate = p.CreatedDate,
                        PaymentTypeId = p.PaymentTypeId,
                        PaymentTypeName = p.PaymentType.PaymentTypeTranslations.FirstOrDefault(x => x.LanguageId == "TR")?.Name

                    }).ToList();

                response.Data.OrderDeliveryAddress = new OrderDeliveryAddress
                {
                    Address = entity.OrderDeliveryAddress.Address,
                    Email = "",
                    Neighborhood = new Neighborhood
                    {
                        Name = entity.OrderDeliveryAddress.Neighborhood.Name,
                        District = new District
                        {
                            Name = entity.OrderDeliveryAddress.Neighborhood.District.Name,
                            City = new City
                            {
                                Name = entity.OrderDeliveryAddress.Neighborhood.District.City.Name
                            }
                        }
                    },
                    PhoneNumber = entity.OrderDeliveryAddress.PhoneNumber,
                    Recipient = entity.OrderDeliveryAddress.Recipient
                };

                response.Data.OrderShipments = entity.OrderShipments.Select(os => new OrderShipment
                {
                    Id = os.Id,
                    ShipmentCompanyId = os.ShipmentCompanyId,
                    TrackingCode = os.TrackingCode,
                    TrackingUrl = os.TrackingUrl,
                    TrackingBase64 = os.TrackingBase64,
                    OrderShipmentDetails = os.OrderShipmentDetail.Select(x => new OrderShipmentDetail
                    {
                        TrackingCode = x.TrackingCode,
                        Id = x.Id,
                        OrderShipmentId = x.OrderShipmentId,
                        Weight = x.Weight
                    }).ToList(),
                    ShipmentCompany = new ShipmentCompany
                    {
                        Name = os.ShipmentCompany.Name
                    },
                    Payor = os.Payor
                }).ToList();

                response.Data.OrderInvoiceInformation = new OrderInvoiceInformation
                {
                    Id = entity.OrderInvoiceInformation.Id,
                    AccountingCompanyId = entity.OrderInvoiceInformation.AccountingCompanyId,
                    Address = entity.OrderInvoiceInformation.Address,
                    FirstName = entity.OrderInvoiceInformation.FirstName,
                    LastName = entity.OrderInvoiceInformation.LastName,
                    Mail = entity.OrderInvoiceInformation.Mail,
                    Phone = entity.OrderInvoiceInformation.Phone,
                    TaxNumber = entity.OrderInvoiceInformation.TaxNumber,
                    TaxOffice = entity.OrderInvoiceInformation.TaxOffice,
                    Guid = entity.OrderInvoiceInformation.Guid,
                    Number = entity.OrderInvoiceInformation.Number,
                    Url = entity.OrderInvoiceInformation.Url,
                };

                if (entity.Marketplace != null)
                    response.Data.MarketPlace = new Marketplace
                    {
                        Id = entity.Marketplace.Id,
                        Name = entity.Marketplace.Name
                    };

                if (entity.OrderBilling != null)
                    response.Data.OrderBilling = new OrderBilling
                    {
                        Id = entity.OrderBilling.Id,
                        ReturnDescription = entity.OrderBilling.ReturnDescription,
                        InvoiceNumber = entity.OrderBilling.InvoiceNumber,
                        ReturnNumber = entity.OrderBilling.ReturnNumber
                    };

                var marketplaceConfigurations = 
                    _unitOfWork
                    .MarketplaceConfigurationRepository
                    .DbSet()
                    .Where(mc => mc.MarketplaceCompany.MarketplaceId == entity.MarketplaceId)
                    .ToList();

                var merchantId = marketplaceConfigurations.First(mc => mc.Key == "MerchantId").Value;

                response.Data.OrderDetails = entity
                        .OrderDetails
                        .Select(od =>
                        {
                            var orderDetail = new OrderDetail
                            {
                                Payor = od.Payor,
                                ProductInformationId = od.ProductInformationId,
                                VatExcListPrice = od.VatExcListUnitPrice,
                                VatExcUnitPrice = od.VatExcUnitPrice,
                                VatExcUnitCost = od.VatExcUnitCost,
                                VatExcUnitDiscount = od.VatExcUnitDiscount,
                                UnitCost = od.UnitCost,
                                Quantity = od.Quantity,
                                UnitPrice = od.UnitPrice,
                                ListUnitPrice = od.ListUnitPrice,
                                VatRate = od.VatRate
                            };

                            var product = new Product();

                            var productInformationMarketplace = 
                                _unitOfWork
                                .ProductInformationMarketplaceRepository
                                .DbSet()
                                .FirstOrDefault(pim => pim.ProductInformationId == od.ProductInformationId &&
                                                       pim.MarketplaceId == entity.MarketplaceId);

                            var productInformation = new ProductInformation
                            {
                                Id = od.ProductInformationId,
                                Barcode = od.ProductInformation.Barcode,
                                Name = od.ProductInformation.ProductInformationTranslations.FirstOrDefault()?.Name,
                                ProductInformationUUId = productInformationMarketplace.UUId,
                                MerchantId = merchantId
                            };

                            productInformation.ProductInformationPhoto = new ProductInformationPhoto
                            {
                                FileName = $"{_settingService.ProductImageUrl}/{od.ProductInformation.ProductInformationPhotos.FirstOrDefault()?.FileName}"
                            };

                            productInformation.ProductInformationMarketplaces = od
                                            .ProductInformation
                                            .ProductInformationMarketplaces
                                            .Select(pim => new ProductInformationMarketplace
                                            {
                                                MarketPlaceId = entity.MarketplaceId,
                                                StockCode = pim.StockCode
                                            })
                                            .ToList();

                            productInformation.ProductInformationVariants = od
                                            .ProductInformation
                                            .ProductInformationVariants
                                            .Select(piv => new ProductInformationVariant
                                            {
                                                VariantValue = new VariantValue
                                                {
                                                    Name = piv.VariantValue.VariantValueTranslations.FirstOrDefault()?.Value,
                                                    VariantName = piv.VariantValue.Variant.VariantTranslations.FirstOrDefault()?.Name,
                                                }
                                            }).ToList();

                            product.ProductInformation = productInformation;

                            orderDetail.Product = product;

                            return orderDetail;
                        })
                        .ToList();

                response.Success = true;
            });
        }

        public Response Cancel(int id)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var entity = 
                    _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .FirstOrDefault(o => o.Id == id);

                if (entity.OrderTypeId == "PA")
                {
                    entity.OrderTypeId = "OS";
                }

                
                    _unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Success = true;
            });
        }

        public Response Complete(int id)
        {
            Domain.Entities.Order entity = null;

            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                entity = 
                   _unitOfWork
                   .OrderRepository
                   .DbSet()
                   .Include(x => x.OrderPicking)
                   .FirstOrDefault(o => o.Id == id);

                if (entity.OrderPicking != null)
                    entity.OrderPicking.PackerBarcode = null;

                entity.OrderTypeId = "KTE";
                entity.OrderPacking = new Domain.Entities.OrderPacking
                {
                    TenantId = entity.TenantId,
                    DomainId = entity.DomainId,
                    CompanyId = entity.CompanyId,
                    CreatedDate = DateTime.Now
                };

                
                    _unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Success = true;
            }, (response, exception) =>
            {
                File.WriteAllText($@"C:\Logs\Packaging\Complete\{id}.txt", exception.Message);

                if (entity != null)
                    
                        _unitOfWork
                        .OrderRepository
                        .Reload(entity);

                if (exception.GetType() == typeof(DbUpdateException))
                {
                    response.ErrorCode = "P01";
                    response.Message = "Bu sipariş daha önce paketlenmiştir.";
                }
                else
                {
                    response.ErrorCode = "P00";
                    response.Message = "Bilinmeyen bir hata oluştu.";
                }
            });
        }

        public Response NotAvailable(int id)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var entity = 
                    _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .FirstOrDefault(o => o.Id == id);

                entity.OrderTypeId = "TE";

                
                    _unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Success = true;
            });
        }

        public DataResponse<bool> Packed(int id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<bool>>(response =>
            {
                response.Data = _unitOfWork.OrderPackingRepository.DbSet().Any(o => o.Id == id);
                response.Success = true;
            });
        }

        public DataResponse<OrderSummary> Summary()
        {
            return ExceptionHandler.ResultHandle<DataResponse<OrderSummary>>(response =>
            {
                var todayOrdersCount = 
                    _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsNoTracking()
                    .Count(o => o.CreatedDate.Date == DateTime.Now.Date && o.OrderTypeId != "IP");

                var unpackedOrdersCount = 
                    _unitOfWork
                    .OrderRepository
                    .DbSet()
                    .AsNoTracking()
                    .Count(o => o.OrderTypeId == "OS");

                var packedOrdersCount = 
                    _unitOfWork
                    .OrderPackingRepository
                    .DbSet()
                    .AsNoTracking()
                    .Count(op => op.CreatedDate.Date == DateTime.Now.Date);

                response.Data = new OrderSummary
                {
                    TodayOrdersCount = todayOrdersCount,
                    PackedOrdersCount = packedOrdersCount,
                    UnpackedOrdersCount = unpackedOrdersCount
                };
                response.Success = true;
            });
        }

        public DataResponse<int> Reserve(ReserveParameter parameter)
        {
            SqlConnection sqlConnection = null;

            return ExceptionHandler.ResultHandle<DataResponse<int>>(response =>
            {
                using (sqlConnection = new(_settingService.ConnectionString))
                {
                    var rawSql = @"
Update Top(1)	Orders
Set				OrderTypeId = 'PA'
Output			Inserted.Id
Where			OrderTypeId = 'OS'
                And DomainId = @DomainId";

                    if (!string.IsNullOrEmpty(parameter.ShipmentCompanyId))
                        rawSql = @"
Update Top(1)	Orders
Set				OrderTypeId = 'PA'
Output			Inserted.Id
From			Orders
Join			OrderShipments
On				Orders.Id = OrderShipments.OrderId
				And OrderShipments.ShipmentCompanyId = @ShipmentCompanyId
Where			OrderTypeId = 'OS'
                And Orders.DomainId = @DomainId";

                    if (!string.IsNullOrEmpty(parameter.MarketplaceId))
                        rawSql += "\nAnd MarketplaceId = @MarketplaceId";

                    if (parameter.OrderSourceId.HasValue)
                        rawSql += "\nAnd OrderSourceId = @OrderSourceId";

                    if (parameter.OrderId.HasValue)
                        rawSql += "\nAnd Id = @Id";

                    using (SqlCommand sqlCommand = new(rawSql, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DomainId", parameter.DomainId);

                        if (!string.IsNullOrEmpty(parameter.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", parameter.MarketplaceId);

                        if (!string.IsNullOrEmpty(parameter.ShipmentCompanyId))
                            sqlCommand.Parameters.AddWithValue("@ShipmentCompanyId", parameter.ShipmentCompanyId);

                        if (parameter.OrderSourceId.HasValue)
                            sqlCommand.Parameters.AddWithValue("@OrderSourceId", parameter.OrderSourceId.Value);

                        if (parameter.OrderId.HasValue)
                            sqlCommand.Parameters.AddWithValue("@Id", parameter.OrderId.Value);

                        sqlConnection.Open();

                        var id = sqlCommand.ExecuteScalar();

                        if (id == null)
                        {
                            response.Success = false;
                            response.Message = "Sipariş bulunamadı";
                        }
                        else
                        {
                            response.Success = true;
                            response.Data = Convert.ToInt32(id);
                        }

                        sqlConnection.Close();
                    }
                }
            }, (response, exception) =>
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            });
        }
        #endregion
    }
}
