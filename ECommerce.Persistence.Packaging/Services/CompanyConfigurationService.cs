﻿using ECommerce.Application.Common.DataTransferObjects;
using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class CompanyConfigurationService : ICompanyConfigurationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CompanyConfigurationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public DataResponse<List<KeyValue<string, string>>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<KeyValue<string, string>>>>(response =>
            {
                var configrations = this
                        ._unitOfWork
                        .CompanyConfigurationRepository
                        .DbSet()
                        .OrderByDescending(x => x.Id)
                        .Select(c => new KeyValue<string, string>
                        {
                            Key = c.Key,
                            Value = c.Value
                        })
                        .ToList();

                response.Data = configrations;
                response.Success = true;
                response.Message = "Konfigurasyon başarılı bir şekilde getirildi.";
            });
        }
    }
}
