﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class OrderSourceService : IOrderSourceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderSourceService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<List<OrderSource>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<OrderSource>>>(response =>
            {
                var entities = this
                    ._unitOfWork
                    .OrderSourceRepository
                    .DbSet()
                    .ToList();

                response.Data = entities
                    .Select(e => new OrderSource { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
