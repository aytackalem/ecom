﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using System;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class OrderShipmentService : IOrderShipmentService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderShipmentService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public Response Create(OrderShipment orderShipment)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                response.Success = this
                    ._unitOfWork
                    .OrderShipmentRepository
                    .Create(new Domain.Entities.OrderShipment
                    {
                        Payor = orderShipment.Payor,
                        ShipmentCompanyId = orderShipment.ShipmentCompanyId,
                        TrackingCode = orderShipment.TrackingCode,
                        TrackingUrl = orderShipment.TrackingUrl,
                        TrackingBase64 = orderShipment.TrackingBase64,
                        OrderShipmentDetail = orderShipment.OrderShipmentDetails.Select(osd => new Domain.Entities.OrderShipmentDetail
                        {
                            TrackingCode = osd.TrackingCode,
                            Weight = osd.Weight
                        }).ToList()
                    });
                response.Message = $"Sipariş taşıma bilgisi başarılı bir şekilde {(response.Success ? "kaydedildi" : "kaydedilemedi")}.";
            });
        }

        public Response Update(OrderShipment orderShipment)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var entity = this
                    ._unitOfWork
                    .OrderShipmentRepository
                    .DbSet()
                    .FirstOrDefault(o => o.Id == orderShipment.Id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş taşıma bilgisi bulunamadı.";

                    return;
                }

                entity.TrackingCode = orderShipment.TrackingCode;
                entity.TrackingUrl = orderShipment.TrackingUrl;
                entity.TrackingBase64 = orderShipment.TrackingBase64;
                entity.OrderShipmentDetail = orderShipment
                    .OrderShipmentDetails
                    .Select(x => new Domain.Entities.OrderShipmentDetail
                    {
                        TrackingCode = x.TrackingCode,
                        Weight = x.Weight,
                        CreatedDate = DateTime.Now
                    })
                    .ToList();

                response.Success = this
                    ._unitOfWork
                    .OrderShipmentRepository
                    .Update(entity);
                response.Message = $"Sipariş taşıma bilgisi başarılı bir şekilde {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
