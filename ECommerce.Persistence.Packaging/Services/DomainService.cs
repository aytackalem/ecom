﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class DomainService : IDomainService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public DomainService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<List<Application.Packaging.DataTransferObjects.Domain>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Packaging.DataTransferObjects.Domain>>>(response =>
            {
                var entities = this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .ToList();

                response.Data = entities
                    .Select(e => new Application.Packaging.DataTransferObjects.Domain { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
