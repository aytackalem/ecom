﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class CompanyService : ICompanyService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public CompanyService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<List<Application.Packaging.DataTransferObjects.Company>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Application.Packaging.DataTransferObjects.Company>>>(response =>
            {
                var entities = this
                    ._unitOfWork
                    .CompanyRepository
                    .DbSet()
                    .Include(c => c.CompanyContact)
                    .ToList();

                response.Data = entities
                    .Select(e => new Application.Packaging.DataTransferObjects.Company
                    {
                        Id = e.Id,
                        Name = e.Name,
                        CompanyContact = new CompanyContact
                        {
                            TaxNumber = e.CompanyContact.TaxNumber,
                            PhoneNumber = e.CompanyContact.PhoneNumber,
                            TaxOffice = e.CompanyContact.TaxOffice,
                            Address = e.CompanyContact.Address,
                            Email = e.CompanyContact.Email,
                            WebUrl = e.CompanyContact.WebUrl
                        }
                    })
                    .ToList();
                response.Success = true;
                response.Message = $"Şirket kayıtları başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
