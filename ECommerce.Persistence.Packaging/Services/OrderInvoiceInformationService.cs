﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class OrderInvoiceInformationService : IOrderInvoiceInformationService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public OrderInvoiceInformationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public Response Update(OrderInvoiceInformation orderInvoiceInformation)
        {
            return ExceptionHandler.ResultHandle<Response>(response =>
            {
                var entity = this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(x => x.OrderInvoiceInformation)
                    .FirstOrDefault(o => o.Id == orderInvoiceInformation.Id);
                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Sipariş fatura bilgisi bulunamadı.";
                    return;
                }

                entity.OrderInvoiceInformation.AccountingCompanyId = orderInvoiceInformation.AccountingCompanyId;
                entity.OrderInvoiceInformation.Guid = orderInvoiceInformation.Guid;
                entity.OrderInvoiceInformation.Url = orderInvoiceInformation.Url;
                entity.OrderInvoiceInformation.Number = orderInvoiceInformation.Number;

                entity.OrderBilling = new Domain.Entities.OrderBilling
                {
                    Id = orderInvoiceInformation.Id,
                    CreatedDate = DateTime.Now,
                    InvoiceNumber = orderInvoiceInformation.Number
                };

                response.Success = this
                    ._unitOfWork
                    .OrderRepository
                    .Update(entity);

                response.Message = $"Sipariş fatura bilgisi başarılı bir şekilde {(response.Success ? "güncellendi" : "güncellenemedi")}.";
            });
        }
        #endregion
    }
}
