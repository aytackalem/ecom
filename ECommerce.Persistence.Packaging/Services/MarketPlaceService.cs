﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class MarketplaceService : IMarketplaceService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        public MarketplaceService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public DataResponse<Marketplace> Get(string id)
        {
            return ExceptionHandler.ResultHandle<DataResponse<Marketplace>>(response =>
            {
                var entity = this
                    ._unitOfWork
                    .MarketplaceCompanyRepository
                    .DbSet()
                    .Include(mp => mp.MarketplaceConfigurations)
                    .Include(x => x.Marketplace)
                    .FirstOrDefault(mp => mp.Marketplace.Id == id);

                if (entity == null)
                {
                    response.Success = false;
                    response.Message = "Pazaryeri kaydı bulunamadı.";

                    return;
                }

                response.Data = new Marketplace
                {
                    Id = entity.MarketplaceId,
                    Name = entity.Marketplace.Name,
                    MarketPlaceConfigurations = entity
                        .MarketplaceConfigurations
                        .Select(mpc => new MarketPlaceConfiguration
                        {
                            Key = mpc.Key,
                            Value = mpc.Value
                        })
                        .ToList()
                };
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }

        public DataResponse<List<Marketplace>> Get()
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<Marketplace>>>(response =>
            {
                var entities = this
                    ._unitOfWork
                    .MarketPlaceRepository
                    .DbSet()
                    .ToList();

                response.Data = entities
                    .Select(e => new Marketplace { Id = e.Id, Name = e.Name })
                    .ToList();
                response.Success = true;
                response.Message = $"Pazaryeri kaydı başarılı bir şekilde {(response.Success ? "getirildi" : "getirilemedi")}.";
            });
        }
        #endregion
    }
}
