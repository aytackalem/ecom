﻿using ECommerce.Application.Common.ExceptionHandling;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Application.Common.Wrappers;
using ECommerce.Application.Packaging.DataTransferObjects;
using ECommerce.Application.Packaging.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Persistence.Packaging.Services
{
    public class AccountingCompanyConfigurationService : IAccountingCompanyConfigurationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountingCompanyConfigurationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public DataResponse<List<AccountingCompanyConfiguration>> Get(string accountingId)
        {
            return ExceptionHandler.ResultHandle<DataResponse<List<AccountingCompanyConfiguration>>>(result =>
            {
                var entities = this
                    ._unitOfWork
                    .AccountingCompanyConfigurationRepository
                    .DbSet()
                    .Where(x => x.AccountingCompany.AccountingId == accountingId)
                    .ToList();

                if (entities?.Count == 0)
                {
                    result.Message = "Muhasebe firması ayarları başarılı bir şekilde okunamadı.";
                    return;
                }

                result.Data = entities
                    .Select(e => new AccountingCompanyConfiguration
                    {
                        Key = e.Key,
                        Value = e.Value
                    })
                    .ToList();
                result.Success = true;
                result.Message = "Muhasebe firması ayarları başarılı bir şekilde getirildi.";
            });
        }
    }
}
