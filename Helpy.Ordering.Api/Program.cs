using Asp.Versioning;
using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Persistence.Context;
using ECommerce.Persistence.UnitOfWorks;
using FluentValidation;
using Helpy.Behaviours;
using Helpy.Middlewares;
using Helpy.Ordering.Api.Finders;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;

namespace Helpy.Ordering.Api
{
#if DEBUG
    public class CustomHeaderSwaggerAttribute : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "DomainId",
                In = ParameterLocation.Header,
                Required = true,
                Schema = new OpenApiSchema
                {
                    Type = "integer"
                }
            });

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "CompanyId",
                In = ParameterLocation.Header,
                Required = true,
                Schema = new OpenApiSchema
                {
                    Type = "integer"
                }
            });
        }
    }
#endif

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

#if DEBUG
            builder.Services.AddSwaggerGen(sa =>
            {
                sa.OperationFilter<CustomHeaderSwaggerAttribute>();
                sa.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Description = "Enter the Bearer Authorization string as following: `Bearer Generated-JWT-Token`",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                sa.AddSecurityRequirement(new OpenApiSecurityRequirement
{
    {
        new OpenApiSecurityScheme
        {
            Name = "Bearer",
            In = ParameterLocation.Header,
            Reference = new OpenApiReference
            {
                Id = "Bearer",
                Type = ReferenceType.SecurityScheme
            }
        },
        new List<string>()
    }
});
            });
#endif

            builder.Services.AddHttpContextAccessor();
            builder.Services.AddDbContext<ApplicationDbContext>();
            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
            builder.Services.AddScoped<IDbNameFinder, DbNameFinder>();
            builder.Services.AddScoped<ITenantFinder, TenantFinder>();
            builder.Services.AddScoped<IDomainFinder, DomainFinder>();
            builder.Services.AddScoped<ICompanyFinder, CompanyFinder>();
            builder.Services.AddApiVersioning(options =>
            {
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
            builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            builder.Services.AddScoped(typeof(IRequestExceptionHandler<,,>), typeof(ExceptionBehaviour<,,>));
            builder.Services.AddMediatR(configuration =>
            {
                configuration.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
            });
            builder.Services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
                {
#if DEBUG
                    o.RequireHttpsMetadata = false;
#endif

                    o.Authority = builder.Configuration["IdentityServerUrl"];
                    o.Audience = "ApiResourceOrdering";
                });
            builder.Services.AddControllers();

            var app = builder.Build();

#if DEBUG
            app.UseSwagger();
            app.UseSwaggerUI(sa =>
            {
                sa.OAuthClientId("demo_api_swagger");
                sa.OAuthAppName("Demo API - Swagger");
                sa.OAuthUsePkce();
            });
#endif

            app.UseDomainIdCheck();
            app.UseAuthentication();
            app.UseAuthorization();

#if !DEBUG
            app.UseHttpsRedirection();
            app.UseHsts();
#endif

            app.MapControllers();

            app.Run();
        }
    }
}