﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderType(string Id, string Name);
}
