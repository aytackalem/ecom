﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.ReserveForWorkOrder;

public record Command(int Limit, bool Single, string? MarketplaceId) : IRequest<Response<List<Order>>>;