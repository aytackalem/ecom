﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Persistence.Context;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Helpy.Ordering.Api.Application.ReserveForWorkOrder;

public class Handler : IRequestHandler<Command, Response<List<Order>>>
{
    private readonly IConfiguration _configuration;

    private readonly IDbNameFinder _dbNameFinder;

    private readonly IDomainFinder _domainFinder;

    private readonly ICompanyFinder _companyFinder;

    private readonly ApplicationDbContext _context;

    public Handler(IConfiguration configuration, IDbNameFinder dbNameFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder, ApplicationDbContext context)
    {
        _configuration = configuration;
        _dbNameFinder = dbNameFinder;
        _domainFinder = domainFinder;
        _companyFinder = companyFinder;
        _context = context;
    }

    public async Task<Response<List<Order>>> Handle(Command request, CancellationToken cancellationToken)
    {
        Response<List<Order>> response = null;

        List<int> ids = new List<int>();

        #region Reserve
        SqlConnection sqlConnection = null;

        try
        {
            var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
            var dbName = this._dbNameFinder.FindName();
            var connectionString = string.Format(connectionStringTemplate, dbName);

            using (sqlConnection = new(connectionString))
            {

                var commandText = $@"
Select O.Id INTO #TEMP1 from  Orders O
JOIN OrderDetails OD ON O.Id=OD.OrderId
Where			OrderTypeId = 'OS'
                And O.DomainId = 1
            	And O.CompanyId = 1
                {(!string.IsNullOrEmpty(request.MarketplaceId) ? "And O.MarketplaceId='"+ request.MarketplaceId + "'" : "")}
GROUP BY O.Id
HAVING SUM(OD.Quantity) {(request.Single ? " = 1" : " > 1")}


Update Top({request.Limit})	Orders
Set				OrderTypeId = 'TO'
Output			Inserted.Id
FROM Orders O
JOIN #TEMP1 T ON O.Id=T.Id

DROP TABLE #TEMP1";

                using (SqlCommand sqlCommand = new(commandText, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                    sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());

                    sqlConnection.Open();

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        ids.Add(sqlDataReader.GetInt32(0));
                    }

                    sqlConnection.Close();
                }
            }
        }
        catch (Exception e)
        {
            if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                sqlConnection.Close();

            response = new Response<List<Order>>(
                ResponseCodes.Error,
                "Bilinmeyen bir hata oluştu.",
                null);
        }
        #endregion

        #region Domain
        var domain = await _context
                .Domains
                .Include(x => x.DomainSetting)
                .FirstOrDefaultAsync(d => d.Id == this._domainFinder.FindId(), cancellationToken: cancellationToken);

        var imageUrl = domain?.DomainSetting?.ImageUrl;
        #endregion

        response = new Response<List<Order>>(ResponseCodes.Ok, await _context
            .Orders
            .Where(_ => ids.Contains(_.Id))
            .AsNoTracking()
            .Select(_ => new Order
            {
                Id = _.Id,
                Details = _.OrderDetails.Select(_ => new OrderDetail
                {
                    Name = _.ProductInformation.ProductInformationTranslations.FirstOrDefault().Name,
                    Quantity = _.Quantity,
                    Barcode = _.ProductInformation.Barcode,
                    ModelCode = _.ProductInformation.Product.SellerCode,
                    PhotoUrl = _.ProductInformation.ProductInformationPhotos.Count > 0 ? $"{imageUrl}/product/{_.ProductInformation.ProductInformationPhotos.FirstOrDefault().FileName}" : ""
                }).ToList()
            })
            .ToListAsync(cancellationToken));

        return response;
    }
}
