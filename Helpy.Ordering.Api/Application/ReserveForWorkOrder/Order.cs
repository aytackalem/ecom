﻿namespace Helpy.Ordering.Api.Application.ReserveForWorkOrder;

public class Order
{
    public int Id { get; set; }

    public List<OrderDetail> Details { get; set; }
}
