﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Invoiced
{
    public record InvoicedCommand(int Id, string InvoiceNumber, string AccountingCompanyId) : IRequest<Response>;
}
