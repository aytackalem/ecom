﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Invoiced
{
    public class InvoicedCommandHandler : IRequestHandler<InvoicedCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public InvoicedCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(InvoicedCommand request, CancellationToken cancellationToken)
        {
            var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(x => x.OrderInvoiceInformation)
                    .Include(x => x.OrderBilling)
                    .FirstOrDefaultAsync(o => o.Id == request.Id);

            entity.OrderInvoiceInformation.AccountingCompanyId = request.AccountingCompanyId;
            entity.OrderInvoiceInformation.Number = request.InvoiceNumber;

            entity.OrderBilling = new()
            {
                Id = request.Id,
                CreatedDate = DateTime.Now,
                InvoiceNumber = request.InvoiceNumber
            };

            if (await this._unitOfWork.OrderRepository.UpdateAsync(entity))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "Fatura bilgileri güncellenemedi.");
        }
    }
}
