﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderDeliveryAddress(int Id, string Recipient, string PhoneNumber, string Address, Neighborhood Neighborhood);
}
