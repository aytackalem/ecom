﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Get
{
    public record GetQuery(int Id) : IRequest<Response<Order>>;
}
