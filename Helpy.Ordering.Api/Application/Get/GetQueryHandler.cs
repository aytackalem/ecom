﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Get
{
    public class GetQueryHandler : IRequestHandler<GetQuery, Response<Order>>
    {
        private readonly IDomainFinder _domainFinder;

        private readonly IUnitOfWork _unitOfWork;

        public GetQueryHandler(IDomainFinder domainFinder, IUnitOfWork unitOfWork)
        {
            this._domainFinder = domainFinder;
            this._unitOfWork = unitOfWork;
        }

        public async Task<Response<Order>> Handle(GetQuery request, CancellationToken cancellationToken)
        {
            Response<Order> response = null;

            var domain = await this
                ._unitOfWork
                .DomainRepository
                .DbSet()
                .Include(x => x.DomainSetting)
                .FirstOrDefaultAsync(d => d.Id == this._domainFinder.FindId(), cancellationToken: cancellationToken);

            var imageUrl = domain?.DomainSetting?.ImageUrl;

            var order = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                    .Include(o => o.Company.CompanyContact)
                    .Include(o => o.OrderPicking)
                    .Include(o => o.OrderPacking)
                    .Include(o => o.OrderSource)
                    .Include(o => o.OrderType.OrderTypeTranslations)
                    .Include(o => o.OrderNotes)
                    .Include(o => o.OrderBilling)
                    .Include(o => o.Customer)
                    .Include(o => o.Currency)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(ord => ord.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(od => od.ProductInformation.Product)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(os => os.ShipmentCompany)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.Product)
                    .Include(o => o.Payments)
                        .ThenInclude(pt => pt.PaymentType.PaymentTypeTranslations)
                .FirstOrDefaultAsync(o => o.Id == request.Id);

            OrderBilling orderBilling = order.OrderBilling == null
                ? null
                : new OrderBilling(
                    order.OrderBilling.Id,
                    order.OrderBilling.InvoiceNumber,
                    order.OrderBilling.ReturnNumber,
                    order.OrderBilling.ReturnDescription);

            OrderPacking orderPacking = order.OrderPacking == null
                            ? null
                            : new OrderPacking(
                                order.OrderPacking.CreatedDate);

            OrderPicking orderPicking = order.OrderPicking == null
                ? null
                : new OrderPicking(
                    order.OrderPicking.CreatedDate);

            var data = new Order(
                order.Id,
                new OrderType(
                    order.OrderType.Id,
                    order.OrderType.OrderTypeTranslations[0].Name),
                new OrderSource(
                    order.OrderSource.Id,
                    order.OrderSource.Name),
                new Currency(
                    order.Currency.Id,
                    order.Currency.Name),
                new Marketplace(
                    order.Marketplace.Id,
                    order.Marketplace.Name),
                new Customer(
                    order.Customer.Id,
                    order.Customer.Name,
                    order.Customer.Surname),
                new OrderDeliveryAddress(
                    order.OrderDeliveryAddress.Id,
                    order.OrderDeliveryAddress.Recipient,
                    order.OrderDeliveryAddress.PhoneNumber,
                    order.OrderDeliveryAddress.Address,
                    new Neighborhood(
                        order.OrderDeliveryAddress.Neighborhood.Id,
                        order.OrderDeliveryAddress.Neighborhood.Name,
                        new District(
                            order.OrderDeliveryAddress.Neighborhood.District.Id,
                            order.OrderDeliveryAddress.Neighborhood.District.Name,
                            new City(
                                order.OrderDeliveryAddress.Neighborhood.District.City.Id,
                                order.OrderDeliveryAddress.Neighborhood.District.City.Name,
                                new Country(
                                    order.OrderDeliveryAddress.Neighborhood.District.City.Country.Id,
                                    order.OrderDeliveryAddress.Neighborhood.District.City.Country.Name))))),
                new OrderInvoiceInformation(
                    order.OrderInvoiceInformation.Id,
                    order.OrderInvoiceInformation.FirstName,
                    order.OrderInvoiceInformation.LastName,
                    order.OrderInvoiceInformation.Address,
                    order.OrderInvoiceInformation.Phone,
                    order.OrderInvoiceInformation.Mail,
                    order.OrderInvoiceInformation.TaxOffice,
                    order.OrderInvoiceInformation.TaxNumber,
                    order.OrderInvoiceInformation.Url,
                    order.OrderInvoiceInformation.Guid,
                    order.OrderInvoiceInformation.Number,
                    new Neighborhood(
                        order.OrderInvoiceInformation.Neighborhood.Id,
                        order.OrderInvoiceInformation.Neighborhood.Name,
                        new District(
                            order.OrderInvoiceInformation.Neighborhood.District.Id,
                            order.OrderInvoiceInformation.Neighborhood.District.Name,
                            new City(
                                order.OrderInvoiceInformation.Neighborhood.District.City.Id,
                                order.OrderInvoiceInformation.Neighborhood.District.City.Name,
                                new Country(
                                    order.OrderInvoiceInformation.Neighborhood.District.City.Country.Id,
                                    order.OrderInvoiceInformation.Neighborhood.District.City.Country.Name))))),
                orderBilling,
                order.OrderShipments
                    .Select(os =>
                        new OrderShipment(
                            os.Id,
                            os.TrackingCode,
                            os.TrackingUrl,
                            os.TrackingBase64,
                            os.PackageNumber,
                            new ShipmentCompany(
                                os.ShipmentCompany.Id,
                                os.ShipmentCompany.Name)))
                    .ToList(),
                order.OrderDetails
                    .Select(od =>
                    {
                        var fileName = od?.ProductInformation?.ProductInformationPhotos?.FirstOrDefault()?.FileName;

                        if (string.IsNullOrEmpty(imageUrl) == false && string.IsNullOrEmpty(fileName) == false)
                            fileName = $"{imageUrl}/product/{fileName}";

                        return new OrderDetail(
                            od.Id,
                            od.Quantity,
                            od.ListUnitPrice,
                            od.UnitPrice,
                            od.VatRate,
                            od.VatExcUnitPrice,
                            od.VatExcListUnitPrice,
                            od.VatExcUnitDiscount,
                            (int)od.Type,
                            od.ReturnNumber,
                            od.ReturnDescription,
                            new ProductInformation(
                                    od.ProductInformation.Id,
                                    od.ProductInformation.ProductInformationTranslations[0].Name,
                                    od.ProductInformation.ProductInformationTranslations[0].VariantValuesDescription,
                                    od.ProductInformation.Barcode,
                                    od.ProductInformation.StockCode,
                                    new ProductInformationPhoto(
                                        fileName),
                                    new Product(
                                        od.ProductInformation.Product.Id,
                                        od.ProductInformation.Product.SellerCode)));
                    })
                    .ToList(),
                order.OrderNotes
                    .Select(on =>
                        new OrderNote(
                            on.Id,
                            on.Note))
                    .ToList(),
                order.OrderReturnDetails
                    .Select(ord =>
                    {
                        var fileName = ord?.ProductInformation?.ProductInformationPhotos?.FirstOrDefault()?.FileName;

                        if (string.IsNullOrEmpty(imageUrl) == false && string.IsNullOrEmpty(fileName) == false)
                            fileName = $"{imageUrl}/{fileName}";

                        return new OrderReturnDetail(
                            ord.Id,
                            ord.Quantity,
                            ord.IsReturn,
                            ord.IsLoss,
                            new ProductInformation(
                                ord.ProductInformation.Id,
                                ord.ProductInformation.ProductInformationTranslations.First().Name,
                                ord.ProductInformation.ProductInformationTranslations.First().VariantValuesDescription,
                                ord.ProductInformation.Barcode,
                                ord.ProductInformation.StockCode,
                                new ProductInformationPhoto(
                                        fileName),
                                    new Product(
                                        ord.ProductInformation.Product.Id,
                                        ord.ProductInformation.Product.SellerCode)));
                    })
                    .ToList(),
                order.Payments
                    .Select(p =>
                        new Payment(
                            p.Id,
                            p.Amount,
                            new PaymentType(
                                p.PaymentType.Id,
                                p.PaymentType.PaymentTypeTranslations[0].Name)))
                    .ToList(),
                order.Total,
                order.CargoFee,
                order.SurchargeFee,
                order.BulkInvoicing,
                order.Micro,
                order.MarketplaceOrderNumber,
                order.OrderDate,
                order.Commercial,
                            new Company(
                                order.Company.Id,
                                order.Company.Name,
                                order.Company.FullName,
                                new CompanyContact(
                                    order.Company.CompanyContact.TaxNumber,
                                    order.Company.CompanyContact.PhoneNumber,
                                    order.Company.CompanyContact.TaxOffice,
                                    order.Company.CompanyContact.Address,
                                    order.Company.CompanyContact.Email,
                                    order.Company.CompanyContact.WebUrl)),
                            orderPicking,
                            orderPacking);

            response = new Response<Order>(ResponseCodes.Ok, data);

            return response;
        }
    }
}
