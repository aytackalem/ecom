﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Picked
{
    public record PickedCommand(int Id, string Username, string PackerBarcode) : IRequest<Response>;
}
