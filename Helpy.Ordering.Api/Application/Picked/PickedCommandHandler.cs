﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Picked
{
    public class PickedCommandHandler : IRequestHandler<PickedCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PickedCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PickedCommand request, CancellationToken cancellationToken)
        {
            var entity = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderPicking)
                .FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            entity.OrderTypeId = "TD";
            entity.OrderPicking = new()
            {
                TenantId = entity.TenantId,
                DomainId = entity.DomainId,
                CompanyId = entity.CompanyId,
                CreatedDate = DateTime.Now,
                PackerBarcode = request.PackerBarcode,
                PickedUsername = request.Username
            };

            if (await this._unitOfWork.OrderRepository.UpdateAsync(entity))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "Toplama bilgileri kaydedilemedi.");
        }
    }
}
