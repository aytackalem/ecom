﻿namespace Helpy.Ordering.Api.Application
{
    public record Marketplace(string Id, string Name);
}
