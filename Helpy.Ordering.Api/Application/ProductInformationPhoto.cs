﻿namespace Helpy.Ordering.Api.Application
{
    public record ProductInformationPhoto(string FileName);
}
