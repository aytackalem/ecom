﻿namespace Helpy.Ordering.Api.Application
{
    public record City(int Id, string Name, Country Country);
}
