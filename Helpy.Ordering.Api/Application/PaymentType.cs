﻿namespace Helpy.Ordering.Api.Application
{
    public record PaymentType(string Id, string Name);
}
