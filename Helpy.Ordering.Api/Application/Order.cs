﻿namespace Helpy.Ordering.Api.Application
{
    public record Order(int Id, OrderType OrderType, OrderSource OrderSource, Currency Currency, Marketplace Marketplace, Customer Customer, OrderDeliveryAddress OrderDeliveryAddress, OrderInvoiceInformation OrderInvoiceInformation, OrderBilling OrderBilling, List<OrderShipment> OrderShipments, List<OrderDetail> OrderDetails, List<OrderNote> OrderNotes, List<OrderReturnDetail> OrderReturnDetails, List<Payment> Payments, decimal Amount, decimal CargoFee, decimal SurchargeFee, bool BulkInvoicing, bool Micro, string MarketplaceOrderNumber, DateTime OrderDate, bool Commercial, Company Company, OrderPicking OrderPicking, OrderPacking OrderPacking);
}
