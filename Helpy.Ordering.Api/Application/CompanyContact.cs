﻿namespace Helpy.Ordering.Api.Application
{
    public record CompanyContact(string TaxNumber, string PhoneNumber, string TaxOffice, string Address, string Email, string WebUrl);
}
