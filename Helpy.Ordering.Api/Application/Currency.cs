﻿namespace Helpy.Ordering.Api.Application
{
    public record Currency(string Id, string Name);
}
