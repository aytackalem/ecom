﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Ordering.Api.Application.TrendyolPackageNumbers;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.TrendyolPackages
{
    public class TrendyolPackageNumbersQueryHandler : IRequestHandler<TrendyolPackageNumbersQuery, Response<List<string>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public TrendyolPackageNumbersQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<List<string>>> Handle(TrendyolPackageNumbersQuery request, CancellationToken cancellationToken)
        {
            var data = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Where(o => o.MarketplaceId == "TYE" &&
                            o.OrderShipments.Any(os => !string.IsNullOrEmpty(os.TrackingCode)))
                .SelectMany(o => o.OrderShipments.Select(os => os.TrackingCode))
                .Distinct()
                .ToListAsync();

            return new(ResponseCodes.Ok, data);
        }
    }
}
