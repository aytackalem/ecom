﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.TrendyolPackageNumbers
{
    public record TrendyolPackageNumbersQuery() : IRequest<Response<List<string>>>;
}
