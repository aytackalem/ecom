﻿namespace Helpy.Ordering.Api.Application
{
    public record Neighborhood(int Id, string Name, District District);
}
