﻿namespace Helpy.Ordering.Api.Application
{
    public record ProductInformation(int Id, string Name, string VariantValues, string Barcode, string StockCode, ProductInformationPhoto ProductInformationPhoto, Product Product);
}
