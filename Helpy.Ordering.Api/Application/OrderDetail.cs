﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderDetail(int Id, int Quantity, decimal ListUnitPrice, decimal UnitPrice, decimal VatRate, decimal VatExcUnitPrice, decimal VatExcListPrice, decimal VatExcUnitDiscount, int Type, string ReturnNumber, string ReturnDescription, ProductInformation ProductInformation);
}
