﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Filter
{
    public class FilterQueryHandler : IRequestHandler<FilterQuery, PagingResponse<Order>>
    {
        private readonly IDomainFinder _domainFinder;

        private readonly IUnitOfWork _unitOfWork;

        public FilterQueryHandler(IDomainFinder domainFinder, IUnitOfWork unitOfWork)
        {
            this._domainFinder = domainFinder;
            this._unitOfWork = unitOfWork;
        }

        public async Task<PagingResponse<Order>> Handle(FilterQuery request, CancellationToken cancellationToken)
        {
            PagingResponse<Order> response = null;

            var iQueryable = this._unitOfWork.OrderRepository.DbSet();

            if (string.IsNullOrEmpty(request.OrderTypeId) == false)
                iQueryable = iQueryable.Where(o => o.OrderTypeId == request.OrderTypeId);

            if (string.IsNullOrEmpty(request.PackerBarcode) == false)
                iQueryable = iQueryable.Where(o => o.OrderPicking.PackerBarcode == request.PackerBarcode);

            if (string.IsNullOrEmpty(request.ProductBarcode) == false)
                iQueryable = iQueryable.Where(o => o.OrderDetails.Any(od => od.ProductInformation.Barcode == request.ProductBarcode));

            if (request.OrderKind == OrderKind.Single)
                iQueryable = iQueryable.Where(o => o.OrderDetails.Count(y => y.Quantity == 1) == 1);

            else if (request.OrderKind == OrderKind.Multiple)
                iQueryable = iQueryable.Where(o => o.OrderDetails.Count(y => y.Quantity > 1) == 1 || o.OrderDetails.Count > 1);


            if (request.OrderProductCategoryKind == OrderProductCategoryKind.SameCategory) //Aynı Kategori Ürün
            {
                iQueryable = iQueryable.Where(x => x.OrderDetails.GroupBy(g => g.ProductInformation.Product.CategoryId).Count() == 1);
            }
            else if (request.OrderProductCategoryKind == OrderProductCategoryKind.DifferentCategory) //Farklı Kategori Ürün
            {
                iQueryable = iQueryable.Where(x => x.OrderDetails.GroupBy(g => g.ProductInformation.Product.CategoryId).Count() > 1);
            }


            var ids = await iQueryable
                .Select(o => o.Id)
                .Skip(request.Page * request.PageRecordsCount)
                .Take(request.PageRecordsCount)
                .ToListAsync(cancellationToken: cancellationToken);

            if (ids.Count > 0)
            {
                var domain = await this
                    ._unitOfWork
                    .DomainRepository
                    .DbSet()
                    .Include(x => x.DomainSetting)
                    .FirstOrDefaultAsync(d => d.Id == this._domainFinder.FindId(), cancellationToken: cancellationToken);

                var imageUrl = domain?.DomainSetting?.ImageUrl;

                var orders = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                    .Include(o => o.Company.CompanyContact)
                    .Include(o => o.OrderPicking)
                    .Include(o => o.OrderPacking)
                    .Include(o => o.OrderSource)
                    .Include(o => o.OrderType.OrderTypeTranslations)
                    .Include(o => o.OrderNotes)
                    .Include(o => o.OrderBilling)
                    .Include(o => o.Customer)
                    .Include(o => o.Currency)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(ord => ord.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderReturnDetails)
                        .ThenInclude(od => od.ProductInformation.Product)
                    .Include(o => o.Marketplace)
                    .Include(o => o.OrderInvoiceInformation.Neighborhood.District.City.Country)
                    .Include(o => o.OrderDeliveryAddress.Neighborhood.District.City.Country)
                    .Include(o => o.OrderShipments)
                        .ThenInclude(os => os.ShipmentCompany)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationPhotos)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.ProductInformationTranslations)
                    .Include(o => o.OrderDetails)
                        .ThenInclude(od => od.ProductInformation.Product)
                    .Include(o => o.Payments)
                        .ThenInclude(pt => pt.PaymentType.PaymentTypeTranslations)
                    .Where(o => ids.Contains(o.Id))
                    .ToListAsync(cancellationToken: cancellationToken);

                var data = orders
                    .Select(o =>
                    {
                        OrderBilling orderBilling = o.OrderBilling == null
                            ? null
                            : new OrderBilling(
                                o.OrderBilling.Id,
                                o.OrderBilling.InvoiceNumber,
                                o.OrderBilling.ReturnNumber,
                                o.OrderBilling.ReturnDescription);

                        OrderPacking orderPacking = o.OrderPacking == null
                            ? null
                            : new OrderPacking(
                                o.OrderPacking.CreatedDate);

                        OrderPicking orderPicking = o.OrderPicking == null
                            ? null
                            : new OrderPicking(
                                o.OrderPicking.CreatedDate);

                        return new Order(
                            o.Id,
                            new OrderType(
                                o.OrderType.Id,
                                o.OrderType.OrderTypeTranslations[0].Name),
                            new OrderSource(
                                o.OrderSource.Id,
                                o.OrderSource.Name),
                            new Currency(
                                o.Currency.Id,
                                o.Currency.Name),
                            new Marketplace(
                                o.Marketplace.Id,
                                o.Marketplace.Name),
                            new Customer(
                                o.Customer.Id,
                                o.Customer.Name,
                                o.Customer.Surname),
                            new OrderDeliveryAddress(
                                o.OrderDeliveryAddress.Id,
                                o.OrderDeliveryAddress.Recipient,
                                o.OrderDeliveryAddress.PhoneNumber,
                                o.OrderDeliveryAddress.Address,
                                new Neighborhood(
                                    o.OrderDeliveryAddress.Neighborhood.Id,
                                    o.OrderDeliveryAddress.Neighborhood.Name,
                                    new District(
                                        o.OrderDeliveryAddress.Neighborhood.District.Id,
                                        o.OrderDeliveryAddress.Neighborhood.District.Name,
                                        new City(
                                            o.OrderDeliveryAddress.Neighborhood.District.City.Id,
                                            o.OrderDeliveryAddress.Neighborhood.District.City.Name,
                                            new Country(
                                                o.OrderDeliveryAddress.Neighborhood.District.City.Country.Id,
                                                o.OrderDeliveryAddress.Neighborhood.District.City.Country.Name))))),
                            new OrderInvoiceInformation(
                                o.OrderInvoiceInformation.Id,
                                o.OrderInvoiceInformation.FirstName,
                                o.OrderInvoiceInformation.LastName,
                                o.OrderInvoiceInformation.Address,
                                o.OrderInvoiceInformation.Phone,
                                o.OrderInvoiceInformation.Mail,
                                o.OrderInvoiceInformation.TaxOffice,
                                o.OrderInvoiceInformation.TaxNumber,
                                o.OrderInvoiceInformation.Url,
                                o.OrderInvoiceInformation.Guid,
                                o.OrderInvoiceInformation.Number,
                                new Neighborhood(
                                    o.OrderInvoiceInformation.Neighborhood.Id,
                                    o.OrderInvoiceInformation.Neighborhood.Name,
                                    new District(
                                        o.OrderInvoiceInformation.Neighborhood.District.Id,
                                        o.OrderInvoiceInformation.Neighborhood.District.Name,
                                        new City(
                                            o.OrderInvoiceInformation.Neighborhood.District.City.Id,
                                            o.OrderInvoiceInformation.Neighborhood.District.City.Name,
                                            new Country(
                                                o.OrderInvoiceInformation.Neighborhood.District.City.Country.Id,
                                                o.OrderInvoiceInformation.Neighborhood.District.City.Country.Name))))),
                            orderBilling,
                            o.OrderShipments
                                .Select(os =>
                                    new OrderShipment(
                                        os.Id,
                                        os.TrackingCode,
                                        os.TrackingUrl,
                                        os.TrackingBase64,
                                        os.PackageNumber,
                                        new ShipmentCompany(
                                            os.ShipmentCompany.Id,
                                            os.ShipmentCompany.Name)))
                                .ToList(),
                            o.OrderDetails
                                .Select(od =>
                                {
                                    var fileName = od?.ProductInformation?.ProductInformationPhotos?.FirstOrDefault()?.FileName;

                                    if (string.IsNullOrEmpty(imageUrl) == false && string.IsNullOrEmpty(fileName) == false)
                                        fileName = $"{imageUrl}/product/{fileName}";

                                    return new OrderDetail(
                                        od.Id,
                                        od.Quantity,
                                        od.ListUnitPrice,
                                        od.UnitPrice,
                                        od.VatRate,
                                        od.VatExcUnitPrice,
                                        od.VatExcListUnitPrice,
                                        od.VatExcUnitDiscount,
                                        (int)od.Type,
                                        od.ReturnNumber,
                                        od.ReturnDescription,
                                        new ProductInformation(
                                                od.ProductInformation.Id,
                                                od.ProductInformation.ProductInformationTranslations[0].Name,
                                                od.ProductInformation.ProductInformationTranslations[0].VariantValuesDescription,
                                                od.ProductInformation.Barcode,
                                                od.ProductInformation.StockCode,
                                                new ProductInformationPhoto(
                                                    fileName),
                                                new Product(
                                                    od.ProductInformation.Product.Id,
                                                    od.ProductInformation.Product.SellerCode
                                                    )));
                                })
                                .ToList(),
                            o.OrderNotes
                                .Select(on =>
                                    new OrderNote(
                                        on.Id,
                                        on.Note))
                                .ToList(),
                            o.OrderReturnDetails
                                .Select(ord =>
                                {
                                    var fileName = ord?.ProductInformation?.ProductInformationPhotos?.FirstOrDefault()?.FileName;

                                    if (string.IsNullOrEmpty(imageUrl) == false && string.IsNullOrEmpty(fileName) == false)
                                        fileName = $"{imageUrl}/{fileName}";

                                    return new OrderReturnDetail(
                                        ord.Id,
                                        ord.Quantity,
                                        ord.IsReturn,
                                        ord.IsLoss,
                                        new ProductInformation(
                                            ord.ProductInformation.Id,
                                            ord.ProductInformation.ProductInformationTranslations.First().Name,
                                            ord.ProductInformation.ProductInformationTranslations.First().VariantValuesDescription,
                                            ord.ProductInformation.Barcode,
                                            ord.ProductInformation.StockCode,
                                            new ProductInformationPhoto(
                                                    fileName),
                                                new Product(
                                                    ord.ProductInformation.Product.Id,
                                                    ord.ProductInformation.Product.SellerCode)));
                                })
                    .ToList(),
                            o.Payments
                                .Select(p =>
                                    new Payment(
                                        p.Id,
                                        p.Amount,
                                        new PaymentType(
                                            p.PaymentType.Id,
                                            p.PaymentType.PaymentTypeTranslations[0].Name)))
                                .ToList(),
                            o.Total,
                            o.CargoFee,
                            o.SurchargeFee,
                            o.BulkInvoicing,
                            o.Micro,
                            o.MarketplaceOrderNumber,
                            o.OrderDate,
                            o.Commercial,
                            new Company(
                                o.Company.Id,
                                o.Company.Name,
                                o.Company.FullName,
                                new CompanyContact(
                                    o.Company.CompanyContact.TaxNumber,
                                    o.Company.CompanyContact.PhoneNumber,
                                    o.Company.CompanyContact.TaxOffice,
                                    o.Company.CompanyContact.Address,
                                    o.Company.CompanyContact.Email,
                                    o.Company.CompanyContact.WebUrl)),
                            orderPicking,
                            orderPacking);
                    })
                    .ToList();

                return new PagingResponse<Order>(ResponseCodes.Ok, request.Page, ids.Count, request.PageRecordsCount, data);
            }

            return new PagingResponse<Order>(ResponseCodes.Ok, request.Page, 0, request.PageRecordsCount, null);
        }
    }
}
