﻿namespace Helpy.Ordering.Api.Application.Filter
{
    public enum OrderKind
    {
        Default = 0,
        Single = 1,
        Multiple = 2
    }
}
