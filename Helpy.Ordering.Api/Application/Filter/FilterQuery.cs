﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Filter
{
    public record FilterQuery(int Page, int PageRecordsCount, string OrderTypeId, string? PackerBarcode, string? ProductBarcode, OrderKind OrderKind = OrderKind.Default, OrderProductCategoryKind OrderProductCategoryKind = OrderProductCategoryKind.Default) : IRequest<PagingResponse<Order>>;
}
