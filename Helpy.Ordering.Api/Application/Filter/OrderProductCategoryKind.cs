﻿namespace Helpy.Ordering.Api.Application.Filter
{
    public enum OrderProductCategoryKind
    {
        Default = 0,
        SameCategory = 1,//Sipariş içinde aynı kategorili ürünleri listeler
        DifferentCategory = 2,//Sipariş içinde farklı kategorili ürünleri listeler
    }
}
