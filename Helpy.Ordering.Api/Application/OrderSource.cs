﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderSource(int Id, string Name);
}
