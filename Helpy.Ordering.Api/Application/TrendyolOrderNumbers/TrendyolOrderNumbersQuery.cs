﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.TrendyolOrderNumbers
{
    public record TrendyolOrderNumbersQuery(string PackageNumber) : IRequest<Response<List<int>>>;
}
