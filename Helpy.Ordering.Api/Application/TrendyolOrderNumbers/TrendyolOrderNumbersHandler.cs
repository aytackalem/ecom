﻿using ECommerce.Application.Common.Constants;
using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.TrendyolOrderNumbers
{
    public class TrendyolOrderNumbersHandler : IRequestHandler<TrendyolOrderNumbersQuery, Response<List<int>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public TrendyolOrderNumbersHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<List<int>>> Handle(TrendyolOrderNumbersQuery request, CancellationToken cancellationToken)
        {
            var data = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Where(o => o.MarketplaceId == Marketplaces.TrendyolEurope &&
                            o.OrderShipments.Any(os => os.TrackingCode == request.PackageNumber))
                .Select(o => o.Id)
                .ToListAsync();

            return new(ResponseCodes.Ok, data);
        }
    }
}
