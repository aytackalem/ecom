﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Reserve
{
    public record ReserveCommand(int OrderId, string MarketplaceId) : IRequest<Response<int>>;
}
