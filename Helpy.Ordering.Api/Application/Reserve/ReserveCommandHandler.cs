﻿using ECommerce.Application.Common.Interfaces.Helpers;
using ECommerce.Persistence.Context;
using Helpy.Responses;
using MediatR;
using System.Data.SqlClient;

namespace Helpy.Ordering.Api.Application.Reserve
{
    public class ReserveCommandHandler : IRequestHandler<ReserveCommand, Response<int>>
    {
        private readonly IConfiguration _configuration;

        private readonly IDbNameFinder _dbNameFinder;

        private readonly IDomainFinder _domainFinder;

        private readonly ICompanyFinder _companyFinder;

        public ReserveCommandHandler(IConfiguration configuration, IDbNameFinder dbNameFinder, IDomainFinder domainFinder, ICompanyFinder companyFinder)
        {
            _configuration = configuration;
            _dbNameFinder = dbNameFinder;
            _domainFinder = domainFinder;
            _companyFinder = companyFinder;
        }

        public async Task<Response<int>> Handle(ReserveCommand request, CancellationToken cancellationToken)
        {
            Response<int> response = null;

            SqlConnection sqlConnection = null;

            try
            {
                var connectionStringTemplate = this._configuration.GetConnectionString("Helpy");
                var dbName = this._dbNameFinder.FindName();
                var connectionString = string.Format(connectionStringTemplate, dbName);

                using (sqlConnection = new(connectionString))
                {

                    //TO
                    var rawSql = @"
Update Top(1)	Orders
Set				OrderTypeId = 'TO'
Output			Inserted.Id
Where			OrderTypeId = 'OS'
                And DomainId = @DomainId
				And CompanyId = @CompanyId
                And EstimatedPackingDate < GetDate()";

                    if (request.OrderId > 0)
                    {
                        rawSql += " And Id = @OrderId";
                    }

                    if (!string.IsNullOrEmpty(request.MarketplaceId))
                    {
                        rawSql += " And MarketplaceId = @MarketplaceId";
                    }
                    else
                        rawSql += " And MarketplaceId <> 'TYE'"; //TRENDYOL AVRUPAYI CIKAR SADECE SECERSE GELSIN

                    using (SqlCommand sqlCommand = new(rawSql, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@DomainId", this._domainFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@CompanyId", this._companyFinder.FindId());
                        sqlCommand.Parameters.AddWithValue("@OrderId", request.OrderId);

                        if (!string.IsNullOrEmpty(request.MarketplaceId))
                            sqlCommand.Parameters.AddWithValue("@MarketplaceId", request.MarketplaceId);

                        sqlConnection.Open();

                        var id = await sqlCommand.ExecuteScalarAsync();

                        if (id == null)
                            response = new Response<int>(ResponseCodes.Bad, "Sipariş bulunamadı.", default);
                        else
                            response = new Response<int>(ResponseCodes.Ok, "Sipariş bulunamadı.", Convert.ToInt32(id));

                        sqlConnection.Close();
                    }
                }
            }
            catch
            {
                if (sqlConnection != null && sqlConnection.State != System.Data.ConnectionState.Closed)
                    sqlConnection.Close();
            }

            return response;
        }
    }
}
