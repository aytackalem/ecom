﻿namespace Helpy.Ordering.Api.Application
{
    public record Payment(int Id, decimal Amount, PaymentType PaymentType);
}
