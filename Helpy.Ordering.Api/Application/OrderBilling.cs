﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderBilling(int Id, string InvoiceNumber, string ReturnNumber, string ReturnDescription);
}
