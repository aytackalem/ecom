﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderPicking(DateTime CreatedDate);
}
