﻿namespace Helpy.Ordering.Api.Application
{
    public record Product(int Id, string SellerCode);
}
