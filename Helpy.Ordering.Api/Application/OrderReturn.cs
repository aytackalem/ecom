﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderReturn(string? ReturnNumber, string? ReturnDescription, List<OrderReturnDetail> OrderReturnDetails);
}
