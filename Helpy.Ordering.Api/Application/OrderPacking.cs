﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderPacking(DateTime CreatedDate);
}
