﻿namespace Helpy.Ordering.Api.Application
{
    public record District(int Id, string Name, City City);
}
