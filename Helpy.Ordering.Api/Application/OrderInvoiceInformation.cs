﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderInvoiceInformation(int Id, string FirstName, string LastName, string Address, string? Phone, string? Mail, string? TaxOffice, string? TaxNumber, string? Url, string? Guid, string Number, Neighborhood Neighborhood);
}
