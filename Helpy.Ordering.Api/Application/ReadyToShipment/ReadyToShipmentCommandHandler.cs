﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.ReadyToShipment
{
    public class ReadyToShipmentCommandHandler : IRequestHandler<ReadyToShipmentCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReadyToShipmentCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(ReadyToShipmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            entity.OrderTypeId = "KTE";

            if (await this._unitOfWork.OrderRepository.UpdateAsync(entity))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "Sipariş kargoya hazır hale getirilemedi.");
        }
    }
}
