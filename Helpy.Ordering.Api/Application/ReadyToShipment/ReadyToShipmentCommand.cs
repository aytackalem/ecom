﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.ReadyToShipment
{
    public record ReadyToShipmentCommand(int Id) : IRequest<Response>;
}
