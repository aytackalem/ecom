﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderReturnDetail(int Id, int Quantity, bool IsReturn, bool IsLoss, ProductInformation ProductInformation);
}
