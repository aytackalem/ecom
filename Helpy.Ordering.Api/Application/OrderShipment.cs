﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderShipment(int Id, string TrackingCode, string TrackingUrl, string TrackingBase64, string PackageNumber, ShipmentCompany ShipmentCompany);
}
