﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.CannotBeSupplied
{
    public class CannotBeSuppliedCommandHandler : IRequestHandler<CannotBeSuppliedCommand, Response>
    {
        public Task<Response> Handle(CannotBeSuppliedCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
