﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.CannotBeSupplied
{
    public record CannotBeSuppliedCommand(int id) : IRequest<Response>;
}
