﻿using MediatR;
using Helpy.Responses;

namespace Helpy.Ordering.Api.Application.Cancel
{
    public record CancelCommand(int id) : IRequest<Response>;
}
