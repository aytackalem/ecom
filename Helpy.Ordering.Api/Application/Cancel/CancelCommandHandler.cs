﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Cancel
{
    public class CancelCommandHandler : IRequestHandler<CancelCommand, Response>
    {
        public Task<Response> Handle(CancelCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
