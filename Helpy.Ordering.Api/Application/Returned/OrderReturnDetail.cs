﻿namespace Helpy.Ordering.Api.Application.Returned
{
    public class OrderReturnDetail
    {
        public bool IsLoss { get; set; }

        public bool IsReturn { get; set; }

        public int Quantity { get; set; }


        public int OrderDetailId { get; set; }

        public ProductInformation ProductInformation { get; set; }
    }
}
