﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Returned
{
    public record ReturnedCommand(int Id, string ReturnNumber, string ReturnDescription, List<OrderReturnDetail> OrderReturnDetails) : IRequest<Response>;
}
