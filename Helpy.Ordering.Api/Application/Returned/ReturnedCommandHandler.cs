﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using ECommerce.Domain.Entities.Enum;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Returned
{
    public class ReturnedCommandHandler : IRequestHandler<ReturnedCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReturnedCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(ReturnedCommand request, CancellationToken cancellationToken)
        {
            if (request.Id <= 0)
                return new Response(ResponseCodes.Bad, "Sipariş bulunamadı.");

            var entity = await this
                    ._unitOfWork
                    .OrderRepository
                    .DbSet()
                        .Include(x => x.OrderBilling)
                        .Include(x => x.OrderDetails)
                    .FirstOrDefaultAsync(o => o.Id == request.Id);

            if (entity == null)
                return new Response(ResponseCodes.Bad, "Sipariş bulunamadı.");




            var orderDetails = new List<ECommerce.Domain.Entities.OrderDetail>();

            foreach (var odLoop in entity.OrderDetails)
            {
                var orderReturnDetail = request
                   .OrderReturnDetails.FirstOrDefault(x => x.OrderDetailId == odLoop.Id);

                if (orderReturnDetail != null)
                {
                    var quantity = odLoop.Quantity - orderReturnDetail.Quantity;

                    if (quantity > 0)
                    {
                        odLoop.Quantity = quantity;
                        orderDetails.Add(odLoop);
                    }
                    else
                    {
                        odLoop.ReturnNumber = request.ReturnNumber;
                        odLoop.Type = orderReturnDetail.IsLoss ? OrderDetailType.Loss : OrderDetailType.Return;
                        odLoop.ReturnDescription = request.ReturnDescription;
                        odLoop.ReturnDate = DateTime.Now;
                    }
                }

            }

            foreach (var odLoop in orderDetails)
            {
                var orderReturnDetail = request
                 .OrderReturnDetails.First(x => x.OrderDetailId == odLoop.Id);

                entity.OrderDetails.Add(new ECommerce.Domain.Entities.OrderDetail
                {
                    UUId = odLoop.UUId,
                    CreatedDate = DateTime.Now,
                    OrderId = odLoop.OrderId,
                    ProductInformationId = odLoop.ProductInformationId,
                    Quantity = orderReturnDetail.Quantity,
                    Type = orderReturnDetail.IsLoss ? OrderDetailType.Loss : OrderDetailType.Return,
                    ReturnDate = DateTime.Now,
                    ReturnDescription = request.ReturnDescription,
                    ReturnNumber = request.ReturnNumber,
                    Payor = odLoop.Payor,
                    ListUnitPrice = odLoop.ListUnitPrice,
                    UnitPrice = odLoop.UnitPrice,
                    UnitDiscount = odLoop.UnitDiscount,
                    UnitCargoFee = odLoop.UnitCargoFee,
                    UnitCommissionAmount = odLoop.UnitCommissionAmount,
                    VatExcListUnitPrice = odLoop.VatExcListUnitPrice,
                    UnitCost = odLoop.UnitCost,
                    VatExcUnitDiscount = odLoop.VatExcUnitDiscount,
                    VatExcUnitPrice = odLoop.VatExcUnitPrice,
                    VatExcUnitCost = odLoop.VatExcUnitCost,
                    VatRate = odLoop.VatRate
                });
            }

            var _orderDetails = entity.OrderDetails.Where(x => x.Type == OrderDetailType.Success);

            entity.Total = _orderDetails.Sum(x => x.Quantity * x.UnitPrice);
            entity.ListTotal = _orderDetails.Sum(x => x.Quantity * x.ListUnitPrice);
            entity.Discount = _orderDetails.Sum(x => x.Quantity * x.UnitDiscount);
            entity.VatExcListTotal = _orderDetails.Sum(x => x.Quantity * x.VatExcListUnitPrice);
            entity.VatExcTotal = _orderDetails.Sum(x => x.Quantity * x.VatExcUnitPrice);
            entity.VatExcDiscount = _orderDetails.Sum(x => x.Quantity * x.VatExcUnitDiscount);
            entity.CommissionAmount = _orderDetails.Sum(x => x.Quantity * x.UnitCommissionAmount);


            entity.OrderTypeId = entity.OrderDetails.Where(x => x.Type == OrderDetailType.Return || x.Type == OrderDetailType.Loss).Sum(x => x.Quantity) == entity.OrderDetails.Sum(x => x.Quantity) ? "IA" : entity.OrderTypeId;


            entity.OrderBilling.ReturnDescription = request.ReturnDescription;
            entity.OrderBilling.ReturnNumber = request.ReturnNumber;



            entity.OrderReturnDetails = request
                .OrderReturnDetails
                .Select(ord => new ECommerce.Domain.Entities.OrderReturnDetail
                {
                    CompanyId = entity.CompanyId,
                    DomainId = entity.DomainId,
                    TenantId = entity.TenantId,
                    ProductInformationId = ord.ProductInformation.Id,
                    IsLoss = ord.IsLoss,
                    IsReturn = ord.IsReturn,
                    Quantity = ord.Quantity,
                    CreatedDate = DateTime.Now
                })
                .ToList();

            if (await this._unitOfWork.OrderRepository.UpdateAsync(entity))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "İade bilgileri güncellenemedi.");
        }
    }
}
