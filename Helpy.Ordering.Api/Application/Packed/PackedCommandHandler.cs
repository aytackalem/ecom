﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Graph;

namespace Helpy.Ordering.Api.Application.Packed
{
    public class PackedCommandHandler : IRequestHandler<PackedCommand, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PackedCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PackedCommand request, CancellationToken cancellationToken)
        {
            if (request.Id == 0)
                return new Response(ResponseCodes.Bad, "Sipariş bulunamadı.");

            var order = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                    .Include(o => o.OrderPicking)
                .FirstOrDefaultAsync(o => o.Id == request.Id);

            if (order == null)
                return new Response(ResponseCodes.Bad, "Sipariş bulunamadı.");

            if (order.OrderPicking != null)
                order.OrderPicking.PackerBarcode = null;

            order.OrderPacking = new()
            {
                TenantId = order.TenantId,
                DomainId = order.DomainId,
                CompanyId = order.CompanyId,
                CreatedDate = DateTime.Now,
                PackedUsername = request.Username
            };

            if (await this._unitOfWork.OrderRepository.UpdateAsync(order))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "Paketleme bilgileri kaydedilemedi.");
        }
    }
}
