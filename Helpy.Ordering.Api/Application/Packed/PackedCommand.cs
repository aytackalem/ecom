﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Packed
{
    public class PackedCommand : IRequest<Response>
    {
        public int Id { get; set; }

        public string? Username { get; set; }
    }
}
