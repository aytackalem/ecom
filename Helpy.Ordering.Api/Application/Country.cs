﻿namespace Helpy.Ordering.Api.Application
{
    public record Country(int Id, string Name);
}
