﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Create
{
    public record CreateCommand() : IRequest<Response<int>>;
}
