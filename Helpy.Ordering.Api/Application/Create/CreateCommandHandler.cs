﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Create
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, Response<int>>
    {
        public Task<Response<int>> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
