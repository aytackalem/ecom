﻿namespace Helpy.Ordering.Api.Application
{
    public record OrderNote(int Id, string Note);
}
