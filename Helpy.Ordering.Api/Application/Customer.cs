﻿namespace Helpy.Ordering.Api.Application
{
    public record Customer(int Id, string Name, string Surname);
}
