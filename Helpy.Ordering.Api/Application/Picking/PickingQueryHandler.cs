﻿using ECommerce.Application.Common.Interfaces.UnitOfWorks;
using Helpy.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Helpy.Ordering.Api.Application.Picking
{
    public class PickingQueryHandler : IRequestHandler<PickingQuery, Response>
    {
        private readonly IUnitOfWork _unitOfWork;

        public PickingQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> Handle(PickingQuery request, CancellationToken cancellationToken)
        {
            var entity = await this
                ._unitOfWork
                .OrderRepository
                .DbSet()
                .Include(o => o.OrderPicking)
                .FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            entity.OrderTypeId = "TD";

            if (await this._unitOfWork.OrderRepository.UpdateAsync(entity))
                return new Response(ResponseCodes.Ok);

            return new Response(ResponseCodes.Bad, "Toplama bilgileri kaydedilemedi.");
        }
    }
}
