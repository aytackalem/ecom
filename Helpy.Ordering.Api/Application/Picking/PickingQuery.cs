﻿using Helpy.Responses;
using MediatR;

namespace Helpy.Ordering.Api.Application.Picking
{
    public record PickingQuery(int Id) : IRequest<Response>;
}
