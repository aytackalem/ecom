﻿namespace Helpy.Ordering.Api.Application
{
    public record Company(int Id, string Name, string FullName, CompanyContact CompanyContact);
}
