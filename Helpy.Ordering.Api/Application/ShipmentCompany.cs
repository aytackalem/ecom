﻿namespace Helpy.Ordering.Api.Application
{
    public record ShipmentCompany(string Id, string Name);
}