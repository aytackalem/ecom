using Asp.Versioning;
using Helpy.Ordering.Api.Application.Cancel;
using Helpy.Ordering.Api.Application.CannotBeSupplied;
using Helpy.Ordering.Api.Application.Create;
using Helpy.Ordering.Api.Application.Filter;
using Helpy.Ordering.Api.Application.Get;
using Helpy.Ordering.Api.Application.Invoiced;
using Helpy.Ordering.Api.Application.Packed;
using Helpy.Ordering.Api.Application.Picked;
using Helpy.Ordering.Api.Application.Picking;
using Helpy.Ordering.Api.Application.ReadyToShipment;
using Helpy.Ordering.Api.Application.Reserve;
using Helpy.Ordering.Api.Application.ReserveForWorkOrder;
using Helpy.Ordering.Api.Application.Returned;
using Helpy.Ordering.Api.Application.TrendyolOrderNumbers;
using Helpy.Ordering.Api.Application.TrendyolPackageNumbers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Helpy.Ordering.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion(1.0)]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrdersController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var response = await this._mediator.Send(new GetQuery(id));
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpGet]
        public async Task<IActionResult> Filter([FromQuery] FilterQuery query)
        {
            var response = await this._mediator.Send(query);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("picking")]
        public async Task<IActionResult> Picking([FromQuery] PickingQuery query)
        {
            var response = await this._mediator.Send(query);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpGet("reserve")]
        public async Task<IActionResult> Reserve([FromQuery] ReserveCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("picked")]
        public async Task<IActionResult> Picked([FromBody] PickedCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("packed")]
        public async Task<IActionResult> Packed([FromBody] PackedCommand command)
        {
            command.Username = HttpContext?.User?.FindFirst("name")?.Value;

            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("cancel")]
        public async Task<IActionResult> Cancel([FromBody] CancelCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("cannotbesupplied")]
        public async Task<IActionResult> CannotBeSupplied([FromBody] CannotBeSuppliedCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("readyforshipment")]
        public async Task<IActionResult> ReadyForShipment([FromBody] ReadyToShipmentCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("invoiced")]
        public async Task<IActionResult> Invoiced([FromBody] InvoicedCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPatch("returned")]
        public async Task<IActionResult> Returned([FromBody] ReturnedCommand command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpGet("trendyolpackagenumbers")]
        public async Task<IActionResult> Returned([FromQuery] TrendyolPackageNumbersQuery query)
        {
            var response = await this._mediator.Send(query);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpGet("trendyolordernumbers")]
        public async Task<IActionResult> Returned([FromQuery] TrendyolOrderNumbersQuery query)
        {
            var response = await this._mediator.Send(query);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }

        [HttpPost("ReserveForWorkOrder")]
        public async Task<IActionResult> ReserveForWorkOrder([FromBody] Command command)
        {
            var response = await this._mediator.Send(command);
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }
    }
}