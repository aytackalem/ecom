﻿namespace Helpy.Shared.Finder
{
    public interface IDbNameFinder
    {
        #region Methods
        int Find();
        #endregion
    }
}