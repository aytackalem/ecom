﻿namespace Helpy.Shared.Finder
{
    public interface IDomainFinder
    {
        #region Methods
        int Find();
        #endregion
    }
}