﻿namespace Helpy.Shared.Finder
{
    public interface ICompanyFinder
    {
        #region Methods
        int Find();
        #endregion
    }
}