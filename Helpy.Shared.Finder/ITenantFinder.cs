﻿namespace Helpy.Shared.Finder
{
    public interface ITenantFinder
    {
        #region Methods
        int Find();
        #endregion
    }
}